// Karma configuration
// Generated on Fri Mar 10 2017 01:40:49 GMT+0300 (MSK)

module.exports = function (config) {
	config
		.set({

				// base path that will be used to resolve all patterns (eg. files, exclude)
				basePath: '',

				plugins: [
					'karma-jasmine',
					'karma-chrome-launcher',
					'karma-fixture',
					'karma-json-fixtures-preprocessor',
					'karma-environments'
				],

				// list of files / patterns to load in the browser
				files: [
					{pattern: 'project/front/**/*.mock.json'}
				],


				/* Files are now managed by environment definitions.
				* It's recommended to leave this empty. */
				frameworks: ['environments'],


				environments: {
					/* Matcher for "Environment Definition Files" */
					definitions: ['**/.karma.env.+(js)'],
					/* Matcher for test Files relative to definition files. */
					tests: ['*spec.+(js)', 'test.*.+(js)'],
					/* Matcher for template files relative to definition files. */
					templates: ['app/*.html'],
					/* Templates are wrapped with a div. Its class and id will use this prefix. */
					templateNamespace: 'ke',
					/* Timeout for asynchronous tasks. */
					asyncTimeout: 5000,
					/* Set true if environments should also be definable inside header comments of test files. */
					headerEnvironments: false,
					/* If you feel better with a delay between single environment runs, increase this value. */
					pauseBetweenRuns: 0,
					/* Extend the environment object used in definition files. */
				},


				// list of files to exclude
				exclude: [],


				// preprocess matching files before serving them to the browser
				// available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
				preprocessors: {
					'project/front/**/*.mock.json': ['json_fixtures']
				},


				jsonFixturesPreprocessor: {
					// strip this from the file path \ fixture name
					stripPrefix: '.+mock-data/',
					variableName: 'mocks',
					camelizeFilenames: false,
				},


				// test results reporter to use
				// possible values: 'dots', 'progress'
				// available reporters: https://npmjs.org/browse/keyword/karma-reporter
				reporters: ['progress'],


				// web server port
				port: 9876,


				// enable / disable colors in the output (reporters and logs)
				colors: true,


				// level of logging
				// possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
				logLevel: config.LOG_INFO,


				// enable / disable watching file and executing tests whenever any file changes
				autoWatch: true,


				// start these browsers
				// available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
				browsers: ['Chrome'],


				// Continuous Integration mode
				// if true, Karma captures browsers, runs the tests and exits
				singleRun: false,

				// Concurrency level
				// how many browser should be started simultaneous
				concurrency: Infinity,

				proxies: {
	//				"/moveBoard/": "http://movecalc.local/",
				}
			}
		)
};
