function requireAll(requireContext) {
	return requireContext.keys().map(requireContext);
}

requireAll(require.context('./app/', true, /\.\/.*\module.js$/));

require('../moveBoard/app/storages/storages.module.js');
require('../moveBoard/app/template-builder/templateBuilder.module.js');
require('../moveBoard/app/settings/setting.module.js');
require('../moveBoard/app/newInventories/newInventory.module.js');
require('../moveBoard/app/calculator/calc.module.js');
require('../moveBoard/app/pending-info/pending-info.module.js');
require('../moveBoard/app/commercial-move-calculator/commercial-move.services/commercial-move.module.js');
require('../moveBoard/app/pending-info/pending-info.module.js');
require('../moveBoard/app/valuation/valuation.module.js');

require('../shared/import');

require('./app/requestpage/directives/account-faq/account-faq.directive.js');

require('./app/requestpage/filters/er-cubicfoot-to-pound.filter.js');

require('./app/data/datacontext.js');
require('../moveBoard/app/core/common.js');
require('./app/blocks/logger/logger.js');

require('../moveBoard/app/core/shared/services/session.services.js');
require('../moveBoard/app/core/shared/services/authentication.services.js');

require('./app/account/services/clients.services.js');

require('./app/messages/services/messages.services.js');

require('./app/inventories/services/inventories.service.js');
require('./app/inventories/services/editinventory.service.js');

require('../moveBoard/app/calculator/sevices/calculate.services.js');
require('../moveBoard/app/core/shared/services/sharedRequest.services.js');
require('../moveBoard/app/requests/services/requestHelper.service.js');

require('./app/requests/services/dataRequests.services.js');
require('./app/requests/services/modalEditrequest.services.js');
require('./app/requests/modals/modals.services.js');

require('../moveBoard/app/requests/services/parser.services.js');
require('../moveBoard/app/common/services/momentWrapper.services');


require('../moveBoard/app/requests/services/calcRequest.services');
require('../moveBoard/app/common/services/fuel-surcharge/calculate-fuel-surcharge.service');

require('./app/requestpage/services/request.services.js');

require('../moveBoard/app/common/services/init-request/InitRequest.service');
require('../moveBoard/app/common/services/CheckRequest.service');

require('../moveBoard/app/requests/directives/payment/payment.services.js');
require('../moveBoard/app/requests/directives/coupon/services/coupon.service.js');

require('../moveBoard/app/requests/directives/invoices/invoice.services.js');

require('../moveBoard/app/settings/services/settings.services.js');

require('../moveBoard/app/commercial-move-calculator/commercial-move.services/commercial-move.module.js');
require('../moveBoard/app/commercial-move-calculator/commercial-move.services/commercial-move-calc.service.js');

require('../moveBoard/app/storages/storage/services/storageService.js');

require('./app/requestpage/directives/packing-service/packing.directive.js');
require('./app/requestpage/directives/packing-service/packing.service.js');

require('./app/contractpage/directives/extra-services/extraServicesTable.js');

require('./app/contractpage/services/SettingsService.js');
require('./app/contractpage/services/contract.factory.js');
require('./app/contractpage/services/contract.rest.js');
require('./app/contractpage/services/contract.calc.js');

require('../moveBoard/app/requests/directives/new-log/newLogService.js');

require('../moveBoard/app/template-builder/services/emailBuilderRequest.service.rest.js');
require('../moveBoard/app/template-builder/services/sendEmail.service.js');
require('../moveBoard/app/template-builder/services/templateFolder.service.js');
require('../moveBoard/app/template-builder/services/templateBuilder.service.js');

require('../moveBoard/app/services/er-device-detector/er-device-detector.service');

require('./app/custom-blocks/service/blocks.service.js');

require('./app/contractpage/components/tripData/tripData.js');

require('./app/requestpage/directives/valuation-select-block');
require('./app/requestpage/directives/confirmation-valuation-block');
require('../moveBoard/app/valuation/valuation.source');
require('../moveBoard/app/tests-common/test-helper.service.js');

require('./app/requestpage/directives/notes/notes.directive.js');
require('./app/contractpage/directives/review');
require('../moveBoard/app/requests/services/edit-request-calculator/edit-request-calculator.service.js');
