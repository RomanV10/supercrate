require("angular");
require("angular-animate");
require("angular-material");
require('angular-material-data-table');
require('angular-svg-round-progressbar');
require('angular-route');
require('angular-flippy');
require("angular-ui-bootstrap");
require('angular-debounce');

require('angular-material-icons');
require('ng-lazy-render');
require('oi.select');
require("../moveBoard/vendor/localstorage/angular-local-storage.min.js");
require("../moveBoard/vendor/angular-file-saver/angular-file-saver.bundle.min.js");

require("./vendor/angular/angular-cookies.min.js");
require("./vendor/ui-router/angular-ui-router.min.js");

require("./vendor/angular/angular-animate.min.js");

require("./vendor/ng-lightbox/angular-bootstrap-lightbox.min.js");

require("./vendor/plugins/dataTables/js/angular-datatables.js");
require("./vendor/plugins/dataTables/js/angular-datatables.columnfilter.js");

require("./vendor/plugins/switchery/ui-switchery.js");

require("./vendor/plugins/editors/textAngular-sanitize.js");
require("./vendor/plugins/editors/textAngular.js");
require("./vendor/plugins/editors/textAngular-dropdownToggle.js");

require("./vendor/plugins/editors/bootstrap-colorpicker-module.min.js");

require("./vendor/ng-mask/ngMask.js");

require("./vendor/ngGeolocation/ngGeolocation.min.js");

require("./vendor/checklist/checklist-model.js");

require("./vendor/angular-marquee/angular-marquee.min.js");

require("./vendor/plugins/angular-base64-upload/angular-base64-upload.min.js");

require("./vendor/sweet-alert/SweetAlert.js");

require("./vendor/ng-draggable/ngDraggable.js");

require("../moveBoard/vendor/angular-credit-cards/release/angular-credit-cards.js");

require("./vendor/angular-image-compress/angular-image-compress.js");

require("../moveBoard/vendor/angular-dragula/angular-dragula.js");

require("../moveBoard/vendor/angular-print/angularPrint.js");
require("../moveBoard/vendor/ngPrint/ngPrint.js");
require("../moveBoard/vendor/angular-print/angularPrint.js");
