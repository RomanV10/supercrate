import './importAccount.styl';

require("@babel/polyfill");

function requireAll(requireContext) {
	return requireContext.keys().map(requireContext);
}

//require('angular-svg-round-progressbar');
require('lodash');
require('../shared/import');
require('./app/requests/requests.module.js');
require('./app/app.module.js');
// require("./dist/app.inventories.js")

require('./app/core/core.module.js');
require('../moveBoard/app/core/common.js');

require('../moveBoard/app/core/shared/services/session.services.js');
require('../moveBoard/app/core/shared/services/authentication.services.js');


require('./app/MainAccCtrl.js');

require('./app/core/filters.js');
require('./app/core/session-injector/session-injector.service');
require('./app/core/config.js');
//require("./app/core/constants.js")
require('./app/core/shared/directives/templatedirectives.js');
require('./app/core/shared/directives/autoGrow.directives.js');
require('./app/blocks/logger/logger.module.js');
require('./app/blocks/logger/logger.js');
require('./app/blocks/router/router.module.js');
require('./app/blocks/router/routehelper.js');
require('./app/data/data.module.js');
require('./app/data/datacontext.js');

require('./app/layout/layout.module.js');
require('./app/layout/shell.js');
require('./app/layout/sidebar.js');

require('./app/account/account.module.js');
require('./app/account/config.route.js');
require('./app/account/services/clients.services.js');
require('./app/account/shared/directives/user.block.directive.js');
require('./app/account/shared/directives/clientEdit.directive.js');
require('./app/account/areas/login/login.controllers.js');
require('./app/constants/contractConstant.js');
require('./app/constants/accountConstant.js');

require('./app/score/score.module.js');
require('./app/score/directives/request-score.directive.js');

require('./app/history/history.module.js');
require('./app/history/config.route.js');
require('./app/history/history.js');

require('./app/messages/messages.module.js');
require('./app/messages/directives/messages.directive.js');
require('./app/messages/directives/messages.page.directive.js');
require('./app/messages/services/messages.services.js');
require('./app/messages/directives/messages.request.table.js');
require('./app/messages/config.route.js');
require('./app/messages/messages.controller.js');
require('../moveBoard/app/messages/directives/sms-message-header/sms-message-header.directive');
require('../moveBoard/app/requests/filters/sms-statuses/sms-stautses.filter.js');
require('../shared/messages');

require('../moveBoard/app/newInventories/newInventories.source.js');

require('./app/inventories/inventories.module.js');
require('./app/inventories/config.route.js');
require('./app/inventories/controllers/inventories.controller.js');
require('./app/inventories/directives/list-inventories/listInventories.directive.js');
require('./app/inventories/directives/table-inventories/tableInventories.directive.js');
require('./app/inventories/services/inventories.service.js');
require('./app/inventories/services/editinventory.service.js');

require('../moveBoard/app/storages/storages.module.js');
require('../moveBoard/app/calculator/calc.module.js');
require('../shared/shared-api/constants/apiConstants.js');
require('../moveBoard/app/calculator/sevices/calculate.services.js');
require('../moveBoard/app/core/shared/services/sharedRequest.services.js');
require('../moveBoard/app/requests/services/requestHelper.service.js');
require('../moveBoard/app/core/hrmin.js');

require('./app/requests/requests.module.js');
require('./app/requests/config.route.js');
require('./app/requests/requestsController.js');
require('./app/requests/services/dataRequests.services.js');
require('./app/requests/services/modalEditrequest.services.js');
require('./app/requests/modals/modals.services.js');
require('./app/requests/directives/details/details.directive.js');
require('./app/requests/directives/log/requestLog.directive.js');
require('./app/requests/directives/form-inputs/form-inputs.directive.js');
require('./app/requests/directives/datepicker/dateRangepicker.directive.js');
require('./app/requests/directives/options/options.directive.js');
require('./app/requests/directives/datepicker/datePicker.direcitve.js');
require('../moveBoard/app/requests/services/parser.services.js');
require('../moveBoard/app/common/services/momentWrapper.services');


require('../moveBoard/app/requests/services/calcRequest.services');
require('../moveBoard/app/common/services/fuel-surcharge/calculate-fuel-surcharge.service');
require('./app/requestpage/request.module.js');
require('./app/requestpage/config.route.js');
require('./app/requestpage/directives');
require('./app/requestpage/requestController.js');
require('./app/requestpage/services/request.services.js');
require('./app/requestpage/services/policy-modal.services');
require('../moveBoard/app/common/services/init-request/InitRequest.service');
require('../moveBoard/app/common/services/CheckRequest.service');

require('./app/requestpage/reservationController.js');

require('../moveBoard/app/requests/directives/payment/payment.services.js');
require('../moveBoard/app/requests/directives/payment/controllers/payment-controllers.source');
require('../moveBoard/app/requests/directives/payment/payment.directive.js');

require('./app/requestpage/invoiceController.js');

require('./app/requestpage/couponController.js');
require('../moveBoard/app/requests/directives/coupon/services/coupon.service.js');
require('../moveBoard/app/requests/directives/coupon/directives/coupon.directive.js');

require('../moveBoard/app/requests/directives/invoices/invoice.directive.js');
require('../moveBoard/app/requests/directives/invoices/invoiceTable.directive');
require('../moveBoard/app/requests/directives/invoices/invoice.services.js');

require('../moveBoard/app/settings/setting.module.js');
require('../moveBoard/app/settings/services/settings.services.js');

require('../moveBoard/app/commercial-move-calculator/commercial-move.services/commercial-move.module.js');
require('../moveBoard/app/commercial-move-calculator/commercial-move.services/commercial-move-calc.service.js');
require('../moveBoard/app/commercial-move-calculator/commercial-move-request.component/commercial-move-sizes.request.directive.js');

require('../moveBoard/app/storages/storage/services/storageService.js');

require('./app/contractpage/config.route.js');
require('./app/contractpage/contract.module.js');

require('./app/contractpage/controllers/ContractCtrl.js');
require('./app/contractpage/controllers/ContractSettingsCtrl.js');

require('./app/contractpage/components/printDownload/printDownload.js');
require('./app/contractpage/components/tripData/tripData.js');
require('./app/contractpage/components/tripDataSettings/tripDataSettings.js');
require('./app/contractpage/components/sendLetterPdf/sendLetterPdf.js');

require('./app/contractpage/directives/flat-rate/flatRateMoves.js');
require('./app/contractpage/directives/local-moves/localMoves.js');
require('./app/contractpage/directives/extra-services/extraServicesTable.js');
require('./app/contractpage/directives/packing/packingTable.js');
require('./app/contractpage/directives/payments/payments.js');
require('./app/contractpage/directives/custom-content/customContent.js');
require('./app/contractpage/directives/details-labor/detailsLabor.js');

require('./app/contractpage/directives/stringToNumber.js');
require('./app/contractpage/directives/ccTravelTimeFloat.js');
require('./app/contractpage/directives/autoResizeTexarea.js');

require('./app/contractpage/directives/additional-contracts/additionalContractSettings.directive.js');
require('./app/contractpage/directives/additional-contracts/additionalContractPages.directive.js');

require('./app/contractpage/directives/contract.signature/signatureBox.js');
require('./app/contractpage/directives/contract.signature/signatureModal.js');

require('./app/contractpage/directives/inventory/contractInventoryMoving.js');
require('./app/contractpage/directives/inventory/signature/signatureInventoryBox.js');

require('./app/contractpage/directives/lease-agreement/leaseAgreement.directive.js');

require('./app/contractpage/directives/release-form/releaseForm.js');
require('./app/contractpage/directives/release-form/oldVersion/releaseForm.js');
require('./app/contractpage/directives/release-form/releaseFormParse.directive.js');
require('./app/contractpage/directives/release-form/releaseFormSettings.directive.js');
require('./app/contractpage/directives/release-form/signature/signatureRealaseBox.js');

require('./app/contractpage/directives/storage-fit/parseShowStorageService.js');
require('./app/contractpage/directives/storage-fit/parseConnectedService.js');
require('./app/contractpage/directives/storage-fit/parser.js');
require('./app/contractpage/directives/storage-fit/sfTextBox.js');

require('./app/contractpage/directives/starRatingDirective.js');
require('./app/contractpage/services/SettingsService.js');
require('./app/contractpage/services/contract.factory.js');
require('./app/contractpage/services/contract.rest.js');
require('./app/contractpage/services/contract.calc.js');
require('./app/contractpage/services/contractObjectFactory.js');
require('./app/contractpage/services/contractObjectFinance.js');
require('./app/contractpage/services/contractPageData.js');
require('./app/contractpage/services/contractSymbols.js');
require('./app/contractpage/services/contractValue.js');
require('./app/contractpage/services/arrivy.factory.js');

require('./app/requestpage/storageTenantController.js');
require('../moveBoard/app/requests/directives/new-log/newLogService.js');

require('../moveBoard/app/template-builder/templateBuilder.module.js');
require('../moveBoard/app/template-builder/services/emailBuilderRequest.service.rest.js');
require('../moveBoard/app/template-builder/services/sendEmail.service.js');
require('../moveBoard/app/template-builder/services/templateFolder.service.js');
require('../moveBoard/app/template-builder/services/templateBuilder.service.js');

// modals
require('./app/requestpage/modals/global-review.modal.controller.js');
require('./app/requestpage/modals/success-review.modal.controller.js');
require('./app/requestpage/modals/fail-review.modal.controller.js');

require('./app/requestpage/filters/er-cubicfoot-to-pound.filter.js');
require('../moveBoard/app/services/er-only-digit/erOnlyDigit.directive.js');

require('../moveBoard/app/services/er-device-detector/er-device-detector.service');

require('../moveBoard/app/elromcoDots/elromcoDots');
require('../moveBoard/app/services/er-device-detector/er-device-detector.service');

require('./app/custom-blocks/custom-blocks.source');

require('../moveBoard/app/pending-info/pending-info.source');
require('../moveBoard/app/requests/services/edit-request-calculator/edit-request-calculator.service.js');
require('../moveBoard/app/valuation/valuation.source');
require('./app/contractpage/directives/review');
require('../shared/signatures');
