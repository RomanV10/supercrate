(function () {
  
    angular
        .module('move.requests')
        .directive('modalToggle', modalToggle)
    
  
    function modalToggle($rootScope,EditRequestServices) {
    return {
        restrict: 'A',
        scope: {
            'nid': '=nid',            
        },
        link: function(scope, element) {

        element.click(function() {

                
                var content =  $(this).parent().parent().parent().find('.tab-wrappe');
                content.slideToggle("fast"), $(this).toggleClass("fa-chevron-down fa-chevron-up");
                var backdrop = $(this).parent().parent().parent().parent().parent().addClass("toggled");
                var modal = $(this).parent().parent().parent().parent().parent().addClass("toggled");
              
    
                
                if( $(this).hasClass("fa-chevron-up")){

                        backdrop.addClass("toggled");
                        $("body").removeClass("modal-open");
                          
                        addModalInstance(scope.nid);  
                        repositionModals();
               
                }
                else {

                        backdrop.removeClass("toggled");
                        $("body").addClass("modal-open");
                        removeModalInstance(scope.nid);
                        repositionModals();
                }

            
            
            });
            
            

     
        
        function addModalInstance(nid){                 
        
            var modal = [];
            modal.nid = nid;
            modal.modal = '.modalId_'+nid;
            $rootScope.modalInstances[nid] = modal;           
            
       }
        
       function removeModalInstance(nid){    
          
           $rootScope.modalInstances.splice(nid, 1);      
      
       }
        function getModalInstance(nid) {

        return $rootScope.modalInstances[nid];


      }  
           
      function getModalInstances() {

        return $rootScope.modalInstances;


      }  
        
        function repositionModals(){                 
        
                
                    var modals = service.getModalInstances();
                    var count = 0;
                    var i = 0;
                    angular.forEach(modals, function(modal, i) {

                        count++;
                        
                     });
                   // delete old styles
                      angular.forEach(modals, function(modal, nid) {
                            
                            i++;
                            var cc = count - 1;
                            var style = Math.round(100 / cc);
                                          
                            $(modal.modal).removeClass("toggled"+style+" pos"+i);

                    });
                    // add new styles
                        angular.forEach(modals, function(modal, nid) {
                            
                            i++;
                          
                            var style = Math.round(100 / count);
                                          
                            $(modal.modal).addClass("toggled"+style+" pos"+i);

                         });
                  
                 
        }    
        
       
            
            
            
        }
    }

    
    };
    
});
    