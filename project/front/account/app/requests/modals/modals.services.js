angular
	.module('move.requests')

	.factory('requestModals',requestModals)
	.directive('modalToggle', modalToggle);

requestModals.$inject = ['RequestServices','EditRequestServices','$uibModal','logger','config'];

function modalToggle(RequestServices,EditRequestServices) {
	return {
		restrict: 'A',
		scope: {
			'nid': '@nid',
		},
		link: function(scope, element) {
			element.click(function() {

				var content = $(this).parent().parent().parent().find('.tab-wrappe');
				content.slideToggle('fast'), $(this).toggleClass('fa-chevron-down fa-chevron-up');
				var backdrop = $(this).parent().parent().parent().parent().parent().addClass('toggled');
				var modal = $(this).parent().parent().parent().parent().parent().addClass('toggled');

				if( $(this).hasClass('fa-chevron-up')){
					backdrop.addClass('toggled');
					$('body').removeClass('modal-open');
					EditRequestServices.addModalInstance(scope.nid);
					EditRequestServices.repositionModals();
					$('.modalId_' + scope.nid + ' .reqheader').show();
				}
				else {
					backdrop.removeClass('toggled');
					$('body').addClass('modal-open');
					EditRequestServices.removeModalInstance(scope.nid);
					EditRequestServices.repositionModals();
					$('.modalId_' + scope.nid + ' .reqheader').hide();
				}
			});
		}
	};
}

function requestModals(RequestServices,EditRequestServices,$uibModal,logger,config) {

	var service = {};

	service.confirmUpdate = confirmUpdate;
	service.alertModal = alertModal;

	return service;

	//ALERT MODAL
	function alertModal(reason) {
		var alertInstance = $uibModal.open({
			template: require('./templates/alert-modal.html'),
			controller: AlertInstanceCtrl,
			resolve: {
				reason: function () {
					return reason;
				}
			},
		});
	}
	function AlertInstanceCtrl($scope,$uibModalInstance,reason) {
		$scope.reason = reason;
		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}

	//CONFIRM MODAL
	function confirmUpdate(request,option) {

		var modalInstance = $uibModal.open({
			template: require('./templates/update-confirm.html'),
			controller: ModalInstanceCtrl,
			resolve: {
				request: function () {
					return request;
				},
				option: function () {
					return option;
				}
			},
		});

		modalInstance.result.then(function ($scope) {});


	}
	/* @ngInject */
	function ModalInstanceCtrl($scope, $uibModalInstance,option,request,$rootScope) {

		$scope.messages = [];
		$scope.request = request;
		$scope.option = option;
		$scope.update = function() {
			$scope.busy = true;
			var nid = $scope.request.nid;
			var data = {
				'options': option,
				'requestId':nid
			};

			var promise = RequestServices.sendOption(data);
			promise.then(function(data) {
				$scope.busy = false;
				$rootScope.$broadcast('option.choosed', $scope.option);
				$uibModalInstance.dismiss('cancel');

			}, function(reason) {

				logger.error(reason, reason, 'Error');

			});

		};
		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}
}
