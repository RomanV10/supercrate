(function () {
    'use strict';

    angular
        .module('move.requests')
        .controller('RequestsController', RequestsController);

    RequestsController.$inject = ['$routeParams', 'DTOptionsBuilder', 'DTColumnBuilder', 'RequestServices',
        'datacontext', 'logger', 'AuthenticationService', 'MomentWrapperService'];

    function RequestsController($routeParams, DTOptionsBuilder, DTColumnBuilder, RequestServices,
                                datacontext, logger, AuthenticationService, MomentWrapperService) {

        // Inititate the promise tracker to track form submissions.

        var vm = this;
        var dataFields = datacontext.getFieldData();
        var serviceType = $routeParams.id;
        var statusType = $routeParams;
        AuthenticationService.getUniqueUser();

        // vm.requests = [{id:0,name:'Searching...'}];
        //  vm.requests = RequestServices.getRequests();
        var dateFrom = new Date();
        dateFrom.setMonth(dateFrom.getMonth() - 1);

        dateFrom = $.datepicker.formatDate('yy-mm-dd', new Date(dateFrom), {});
        var dateTo = $.datepicker.formatDate('yy-mm-dd', new Date(), {});
        vm.busy = true;

        activate(dateFrom, dateTo, serviceType);

        vm.dtAllRequests = [];


        vm.filterFunction = function (element) {
            return element.service_type_id.match(1) ? true : false;
        };


        function activate(dateFrom, dateTo, filtering) {

            //Defauult Loading
            var data = {
                pagesize: 3000,
                filtering: {
                    date_from: dateFrom,
                    date_to: dateTo,
                }
            }
            // Load requests only for set serviceType
            if (angular.isDefined(serviceType)) {
                var data = {
                    pagesize: 3000,
                    filtering: {
                        date_from: MomentWrapperService.getZeroMoment(dateFrom).startOf('day').unix(),
                        date_to: MomentWrapperService.getZeroMoment(dateTo).endOf('day').unix(),
                    },
                    condition: {
                        field_move_service_type: serviceType
                    }
                }

            }

            var promise = RequestServices.getAllRequestsDateRange(data);

            promise.then(function (data) {
                vm.busy = false;
                //  RequestServices.addRequest(data);
                vm.requests = data.nodes;

            }, function (reason) {

                vm.busy = false;
                logger.error(reason, reason, "Error");
            });
        }


    }


})();
