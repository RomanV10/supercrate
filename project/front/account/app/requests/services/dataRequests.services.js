(function () {

    angular
        .module('move.requests')

        .factory('RequestServices', RequestServices);

    RequestServices.$inject = ['$http', '$rootScope', '$q', 'config', 'AuthenticationService', 'datacontext', 'MomentWrapperService', 'Session'];

    function RequestServices($http, $rootScope, $q, config, AuthenticationService, datacontext, MomentWrapperService, Session) {
		var service = {};

        service.EntityTypes = {
            MOVEREQUEST: 0,
            STORAGEREQUEST: 1,
            LDREQUEST: 2,
            SYSTEM: 3
        };

        service.getAllRequests = getAllRequests;
        service.getAllRequestsDateRange = getAllRequestsDateRange;
        service.getRequest = getRequest;

        service.updateRequest = updateRequest;
        service.removeImage = removeImage;
        service.getDateRequests = getDateRequests;
        service.saveDateRequests = saveDateRequests;
        service.storageDateRequests = storageDateRequests;
        service.getRequestsByNid = getRequestsByNid;
        service.saveRequests = saveRequests;
        service.getRequests = getRequests;
        service.addRequest = addRequest;
        service.getStatusRequests = getStatusRequests;
        service.getAllRequestsMonthSchedule = getAllRequestsMonthSchedule;
        service.sendOption = sendOption;
        service.saveDetails = saveDetails;

        service.getManagers = getManagers;
        service.setManager = setManager;
        service.getManager = getManager;
        service.getPayroll = getPayroll;
        service.saveReqData = saveReqData;
        service.getLogs = getLogs;
        service.postLogs = postLogs;
        service.sendLogs = sendLogs;
        service.sendServiceLogs = sendServiceLogs;
        service.UpdatePackingRequest = UpdatePackingRequest;
        service.checkRequestTypeSetUserId = checkRequestTypeSetUserId;
        service.saveRequestDataContract = saveRequestDataContract;

        return service;

        function getPayroll(nid, contractType) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/payroll/get_all_payroll_info',
                data: {
                    nid: nid,
                    contract_type: contractType
                }
            }).then(function (data) {
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });
            return defer.promise;
        }

        function saveDetails(nid, details) {

            var data = {};
            data.data = {};
            data.data.details = details;
            var jsonDetails = JSON.stringify(data);
            var deferred = $q.defer();
            $http.post(config.serverUrl + 'server/move_invertory/' + nid, jsonDetails)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    // data.success = false;
                    deferred.reject(data);

                });
            return deferred.promise;

        }

        function sendOption(data) {

            var deferred = $q.defer();

            $http
                .post(config.serverUrl + 'server/move_request/get_options_for_pickup', data)
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    deferred.reject(data);

                });

            return deferred.promise;

        }

        function removeImage(nid, data) {
            var deferred = $q.defer();

            $http
                .put(config.serverUrl + 'server/move_request/' + nid, data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });

            return deferred.promise;
        }

        function getManager(nid, callback) {

            var data = {};

            var deferred = $q.defer();

            $http
                .post(config.serverUrl + 'server/move_request/request_manager/' + nid)
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    deferred.reject(data);

                });

            return deferred.promise;

        }

        function getManagers(callback) {

            var data = {};

            var deferred = $q.defer();

            $http
                .post(config.serverUrl + 'server/move_request/managers')
                .success(function (data, status, headers, config) {

                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    deferred.reject(data);

                });

            return deferred.promise;

        }


        function setManager(uid, nid, callback) {

            var data = {nid: nid, uid: uid};


            var deferred = $q.defer();

            $http
                .post(config.serverUrl + 'server/move_request/assign_manager', data)
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    deferred.reject(data);

                });

            return deferred.promise;

        }


        function updateRequest(nid, request, callback) {

            var data = {};
            data.data = request;

            var deferred = $q.defer();

            $http
                .put(config.serverUrl + 'server/move_request/' + nid, data)
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    deferred.reject(data);

                });

            return deferred.promise;

        }

        function getRequestsByNid(nids, callback) {


            var deferred = $q.defer();

            var data = {
                pagesize: 500,
                condition: {
                    nid: nids,
                },
            };


            $http.post(config.serverUrl + 'server/move_request/search', data)
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    // service.saveRequests(data);
                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    deferred.reject(data);

                });

            return deferred.promise;

        }

        function getStatusRequests(status, callback) {
            var data = {
                pagesize: 500,
                condition: {
                    field_approve: status,
                },
            };
            return $http.post(config.serverUrl + 'server/move_request/search', data)
                .success(function (resolve, status, headers, config) {

                    //data.success = true;
                    // service.saveRequests(data);
                    //deferred.resolve(data);
                    resolve.data
                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    //deferred.reject(data);

                });
        }


        function getDateRequests(date, callback) {
            var deferred = $q.defer();

            var data = {
                pagesize: 1000,
                condition: {
                    field_approve: [2, 3],
                },
                filtering: {
                    field_date: MomentWrapperService.getZeroMoment(date).startOf('day').unix(),
                    // created :date
                },

            };


            $http.post(config.serverUrl + 'server/move_request/search', data)
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    service.saveDateRequests(date, data);
                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    deferred.reject(data);

                });

            return deferred.promise;

        }

        function saveDateRequests(date, data) {

            if (angular.isUndefined($rootScope.dateRequests)) {

                $rootScope.dateRequests = [];

            }

            $rootScope.dateRequests[date] = data;


        }

        function storageDateRequests(date) {


            return $rootScope.dateRequests[date];


        }


        function getAllRequests(callback) {


            var deferred = $q.defer();


            $http.get(config.serverUrl + 'server/move_request?pagesize=100')
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    service.saveRequests(data);
                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    deferred.reject(data);

                });

            return deferred.promise;

        }

        function getAllRequestsMonthSchedule(satuses, from, to, callback) {
            var deferred = $q.defer();

            var data = {
                pagesize: 1000,
                condition: {
                    field_approve: satuses,
                },
                filtering: {
                    field_date_from: MomentWrapperService.getZeroMoment(from).startOf('day').unix(),
                    field_date_to: MomentWrapperService.getZeroMoment(to).endOf('day').unix(),
                }
            };


            $http.post(config.serverUrl + 'server/move_request/search', data)
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    service.saveRequests(data);
                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    deferred.reject(data);

                });

            return deferred.promise;

        }

        function getAllRequestsDateRange(filtering, callback) {


            var deferred = $q.defer();


            $http.post(config.serverUrl + 'server/move_request/search', filtering)
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    service.saveRequests(data);
                    deferred.resolve(data);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;
                    deferred.reject(data);

                });

            return deferred.promise;

        }


        function getRequest(nid) {


            var deferred = $q.defer();


            $http.get(config.serverUrl + 'server/move_request/' + nid)
                .success(function (data, status, headers, config) {

                    //data.success = true;
                    var request = data;

                    service.addRequest(data.nid, data);
                    deferred.resolve(request);

                })
                .error(function (data, status, headers, config) {

                    // data.success = false;

                    deferred.reject(data);

                });

            return deferred.promise;

        }

        function saveRequests(data) {

            $rootScope.requests = data;
            $rootScope.dateRequests = [];

        }

        function addRequest(nid, data) {

            var requests = service.getRequests();

            if (angular.isUndefined(requests)) {

                $rootScope.requests = data;

            }
            else {

                requests[nid] = data;

            }

            service.saveRequests(requests);


        }

        function getRequests() {

            return $rootScope.requests;


        }

        function saveReqData(nid, info, userId) {
            var deferred = $q.defer();
            var data = {};
            data.data = {
                'all_data': info
            };

            if(userId) {
            	data.global_user_uid = userId;
			}

            $http
                .put(config.serverUrl + 'server/move_request/' + nid, data)
                .success(function (data) {
                    var data_info = {
                        "all_data": info,
                        "nid": nid,
                    };
                    $rootScope.$broadcast('request.all_data', data_info);
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

	    function saveRequestDataContract(request) {
		    let deferred = $q.defer();
		    let nid = request.nid;
		    let info = request.request_all_data;
		    let userId = service.checkRequestTypeSetUserId(request);
		    let data = {
			    data: {
				    'all_data': info
			    }
		    };

		    if (userId) {
			    data.global_user_uid = userId;
		    }

		    $http
			    .put(config.serverUrl + 'server/move_request/' + nid, data)
			    .success(function (data) {
				    var data_info = {
					    "all_data": info,
					    "nid": nid,
				    };
				    $rootScope.$broadcast('request.all_data', data_info);
				    deferred.resolve(data);
			    })
			    .error(function (data) {
				    deferred.reject(data);
			    });

		    return deferred.promise;
	    }

        function getLogs(nid, type) {
            var MOVEREQUEST = 0;
            var STORAGEREQUEST = 1;
            var LDREQUEST = 2;
            var SYSTEM = 3;
            var entity_type = SYSTEM;
            if (type == 6 || type == 2) {
                entity_type = STORAGEREQUEST;
            } else if (type == 5 || type == 7) {
                entity_type = LDREQUEST;
            } else {
                entity_type = MOVEREQUEST;
            }
            var deferred = $q.defer();
            $http.get(config.serverUrl + 'server/new_log?entity_id=' + nid + '&entity_type=' + entity_type)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function postLogs(data) {
            var deferred = $q.defer();
            $http.post(config.serverUrl + 'server/new_log', data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function sendLogs(data, title, nid, type) {
            var currUser = $rootScope.currentUser;
            var userRole = '';
            if (_.isEmpty(currUser)) {
                currUser = [];
                currUser.userRole = ['anonymous'];
                currUser.userId = [];
                currUser.userId.field_user_first_name = 'anonymous';
                currUser.userId.field_user_last_name = '';
            }
            if (currUser.userRole.length == 1 && currUser.userRole.indexOf('authenticated user') >= 0) {
                userRole = 'client';
            }
            if (currUser.userRole.indexOf('administrator') >= 0) {
                userRole = 'administrator';
            } else if (currUser.userRole.indexOf('manager') >= 0) {
                userRole = 'manager';
            } else if (currUser.userRole.indexOf('sales') >= 0) {
                userRole = 'sales';
            } else if (currUser.userRole.indexOf('foreman') >= 0) {
                userRole = 'foreman';
            }
            var entity_type = service.EntityTypes[type];
            var msg = {
                entity_id: nid,
                entity_type: entity_type,
                data: [{
                    details: [{
                        activity: userRole,
                        title: title,
                        text: [],
                        date: moment().unix()
                    }],
                    source: currUser.userId.field_user_first_name + ' ' + currUser.userId.field_user_last_name
                }]
            };
            var info = {
                browser: AuthenticationService.get_browser(),
            };
            msg.data[0].details[0].info = info;
            if (userRole == 'client')
                msg.data[0].source = 'Client';
            _.forEach(data, function (obj) {
                msg.data[0].details[0].text.push(obj);
            });
            var deferred = $q.defer();
            $http.post(config.serverUrl + 'server/new_log', msg)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function sendServiceLogs(data, nid, type) {
            var currUser = $rootScope.currentUser;
            var MOVEREQUEST = 0;
            var STORAGEREQUEST = 1;
            var LDREQUEST = 2;
            var SYSTEM = 3;
            var userRole = '';
            if (currUser.userRole.length == 1 && currUser.userRole.indexOf('authenticated user') >= 0) {
                userRole = 'client';
            }
            if (currUser.userRole.indexOf('administrator') >= 0) {
                userRole = 'administrator';
            } else if (currUser.userRole.indexOf('manager') >= 0) {
                userRole = 'manager';
            } else if (currUser.userRole.indexOf('sales') >= 0) {
                userRole = 'sales';
            } else if (currUser.userRole.indexOf('foreman') >= 0) {
                userRole = 'foreman';
            }
            var entity_type = SYSTEM;
            if (type == 6 || type == 2) {
                entity_type = STORAGEREQUEST;
            } else if (type == 5 || type == 7) {
                entity_type = LDREQUEST;
            } else {
                entity_type = MOVEREQUEST;
            }
            var msg = {
                entity_id: nid,
                entity_type: entity_type,
                data: []
            };
            var info = {
                browser: AuthenticationService.get_browser(),
            };
            _.forEach(data, function (obj) {
                _.forEach(obj, function (details) {
                    _.forEach(details, function (detail) {
                        detail.activity = userRole;
                        detail.info = info;
                    });
                });
                obj.source = currUser.userId.field_user_first_name + ' ' + currUser.userId.field_user_last_name;
            });
            msg.data = data;
            var deferred = $q.defer();
            $http.post(config.serverUrl + 'server/new_log', msg)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

	    function UpdatePackingRequest(packingId, additionalInfo) {
		    //get packing request
		    getRequestsByNid(packingId).then(resolve => {
			    //updateFields
			    let allData = _.head(resolve.nodes).request_all_data;
			    if (!_.isUndefined(allData.additionalInfo)) {
				    allData.additionalInfo = additionalInfo;
				    saveReqData(packingId, allData);
			    }
		    });
	    }

		function checkRequestTypeSetUserId(request) {
			let serviceType = _.get(request, 'service_type.raw');
			let toStorage = request.request_all_data.toStorage;
			let session = Session.get();

			if ((serviceType == 2 || serviceType == 6) && toStorage) {
				return _.get(session, 'userId.uid');
			}
		}

    }
})();
