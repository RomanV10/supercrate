(function () {

    angular
        .module('move.requests')

        .factory('EditRequestServices', EditRequestServices);

    EditRequestServices.$inject = ['$rootScope', '$uibModal', '$filter', 'common', 'logger', 'config', 'RequestServices', 'datacontext', 'InventoriesServices', 'SweetAlert', '$q', 'apiService', 'moveBoardApi'];

    function EditRequestServices($rootScope, $uibModal, $filter, common, logger, config, RequestServices, datacontext, InventoriesServices, SweetAlert, $q, apiService, moveBoardApi) {

        var service = {};

        service.openModal = openModal;
        service.updateRequestLive = updateRequestLive;
        service.requestEditModal = requestEditModal;
        service.addModalInstance = addModalInstance;
        service.removeModalInstance = removeModalInstance;
        service.getModalInstances = getModalInstances;
        service.getModalInstance = getModalInstance;
        service.repositionModals = repositionModals;
        service.getQuickBookFlag = getQuickBookFlag;
        $rootScope.modalInstances = {};


        return service;


      function getQuickBookFlag(request) {
        var showQBFlag = apiService.postData(moveBoardApi.quickbooks.checkTokenDisconnectButton);
        var qbFlag = apiService.getData(moveBoardApi.quickbooks.moveQuickbooks + request.nid);

        $q.all({showQBFlag, qbFlag})
          .then(function (response) {
            request.showQuickBooksStatus = _.head(response.showQBFlag.data);
            let quickBooksFlag = _.head(response.qbFlag.data);

            if (quickBooksFlag == 0) {
              request.quickBookFlag = "not in";
            } else if (quickBooksFlag == 1) {
              request.quickBookFlag = "in";
            } else {
              request.quickBookFlag = "partly in";
            }
          });
      }


        function addModalInstance(nid) {

            var modal = [];
            modal.nid = nid;
            modal.modal = '.modalId_' + nid;
            $rootScope.modalInstances[nid] = modal;

        }
        function removeModalInstance(nid) {

            var modals = service.getModalInstances();
            var count = 0;
            var i = 0;
            angular.forEach(modals, function (modal, i) {

                count++;

            });
            // delete old styles
            angular.forEach(modals, function (modal, nid) {

                i++;
                var style = Math.round(100 / count);
                for (var k = 1; k <= count; k++)
                    $(modal.modal).removeClass("toggled" + style + " pos" + k);


            });


            delete $rootScope.modalInstances[nid];


        }
        function getModalInstance(nid) {

            return $rootScope.modalInstances[nid];


        }
        function getModalInstances() {

            return $rootScope.modalInstances;


        }
        function repositionModals() {


            var modals = service.getModalInstances();
            var count = 0;
            var i = 0;
            angular.forEach(modals, function (modal, i) {

                count++;

            });
            // delete old styles
            angular.forEach(modals, function (modal, nid) {

                i++;
                var cc = count - 1;
                var style = Math.round(100 / cc);

                $(modal.modal).removeClass("toggled" + style + " pos" + i);

            });
            var i = 0;
            // add new styles
            angular.forEach(modals, function (modal, nid) {

                i++;

                var style = Math.round(100 / count);

                $(modal.modal).addClass("toggled" + style + " pos" + i);

            });


        }

		 function updateRequestLive(OldRequest, NewRequest) {
			 var fieldData = datacontext.getFieldData();

			 angular.forEach(NewRequest, function (value, field_name) {
				 angular.forEach(OldRequest, function (request_field, request_field_name) {
					 if (angular.isDefined(request_field) && request_field != null
						 && angular.isDefined(request_field.field) && request_field.field == field_name) {

						 if (request_field.subfield) {
							 OldRequest[request_field_name].value = value[request_field.subfield];
						 } else {
							 if (request_field.field == 'field_date') {
								 OldRequest[request_field_name].raw = moment(value.date).unix();
								 OldRequest[request_field_name].value = moment(value.date).format("MM/DD/YYYY");
							 } else if (request_field.field == "field_commercial_extra_rooms") {

							 } else if (request_field.field == "field_extra_furnished_rooms") {
								 OldRequest[request_field_name].raw = value;
							 } else {
								 OldRequest[request_field_name].raw = value;

								 if (angular.isDefined((fieldData.field_lists[field_name]))) {
									 OldRequest[request_field_name].value = fieldData.field_lists[field_name][value];
								 } else {
									 OldRequest[request_field_name].value = value;
								 }
							 }
							 if (request_field.field == "field_approve") {
								 OldRequest[request_field_name].raw = value;
							 }
						 }
					 }
				 });
			 });

			 return OldRequest;
		 }

        function requestEditModal(request) {

            if (angular.isDefined(request.nid)) {
                //GET SAME DATE REQUESTS 07/31/2015
                var date = request.date.value;


                var promise = RequestServices.getDateRequests(date);

                promise.then(function (data) {


                    var requests = data;
                    service.openModal(request, requests);


                }, function (reason) {


                    logger.error(reason, reason, "Error");

                });
            }
            else {

                var promise = RequestServices.getRequest(request);
                promise.then(function (data) {

                    var requests = data;
                    service.requestEditModal(data);
                }, function (reason) {

                    logger.error(reason, reason, "Error");

                });

            }



        };

        function openModal(request, requests) {

            var modalInstance = $uibModal.open({
                templateUrl: 'app/requests/services/templates/editRequestTemplate.html?2',
                controller: ModalInstanceCtrl,
                size: 'lg',
                backdrop: false,
                windowClass: 'requestModal status_' + request.status.raw + ' modalId_' + request.nid,
                resolve: {
                    request: function () {
                        return request;
                    },
                    requests: function () {
                        return requests;
                    },

                },
            });



            modalInstance.result.then(function ($scope) {




            });

        };



        //ModalCntroller

		/* @ngInject */
        function ModalInstanceCtrl($scope, $uibModalInstance, request, requests) {

            $scope.request = request;
            $scope.requests = requests;
            $scope.editrequest = {};
            $scope.oldRequest = angular.copy($scope.request);

            $scope.tabs =
                [{ name: 'Request #' + request.nid, url: 'app/requests/services/templates/editFormTemplate.html', selected: true },
                    { name: 'Inventory', url: 'app/requests/services/templates/inventoryRequestForm.html', selected: false },
                    { name: 'Details', url: 'app/requests/services/templates/detailsRequestForm.html', selected: false },
                    { name: 'Messages', url: 'app/requests/services/templates/messagesRequestForm.html', selected: false },
                    { name: 'Client', url: 'app/requests/services/templates/clientRequestForm.html', selected: false },
                    { name: 'Log', url: 'app/requests/services/templates/logRequestForm.html', selected: false },
                    { name: 'Options', url: 'app/requests/services/templates/optionsTab.html', selected: false },
                    { name: 'Settings', url: 'app/requests/services/templates/request_settings.html', selected: false },

                ];

            $scope.template = $scope.tabs[0];

            $scope.select = function (tab) {

                angular.forEach($scope.tabs, function (tab) {
                    tab.selected = false;
                });

                tab.selected = true;
                $scope.template = tab;
            }

            $scope.cancel = function () {

                //check if there is ane change editRequest is not Empty
                //$scope.editrequest  $scope.request_old
                var changes = common.count($scope.editrequest);
                if (changes != 0) {
                    SweetAlert.swal({
                        title: "You haven't saved the changes!",
                        text: "You have unsaved changes! Do you want to cancel them?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Save",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                        function (isConfirm) {
                            if (isConfirm) {

                                //save then close
                                service.removeModalInstance($scope.request.nid);
                                service.repositionModals();
                            }
                            else {
                                // $scope.request.status.raw  = $scope.request.status.old ;
                                $scope.request = Reset($scope.request);
                                $uibModalInstance.dismiss('cancel');
                                service.removeModalInstance($scope.request.nid);
                                service.repositionModals();

                            }
                        });
                } else {
                    $uibModalInstance.dismiss('cancel');
                    service.removeModalInstance($scope.request.nid);
                    service.repositionModals();


                }


            };
            if ($scope.request.inventory.inventory_list != null)
                $scope.request.inventory_list_old = $scope.request.inventory.inventory_list;
            else
                $scope.request.inventory_list_old = [];

            $scope.request.inventory_weight = InventoriesServices.calculateTotals($scope.request.inventory.inventory_list);

            function Reset(request) {

                angular.forEach(request, function (field, value) {


                    if (angular.isDefined(field))
                        if (angular.isDefined(field.old)) {

                            field.value = field.old;
                            field.raw = field.old;

                        }


                });

                return request;


            }

        }


        function PrepareRequestArray(request) {

            var move_request = {};


            request.moveDate = moment(request.moveDateTime).format('YYYY-MM-DD');
            if (request.flatRate) {
                request.serviceType = 5; // FLAT RATE
                request.flatRate = 1;
            }

            move_request.data = {
                status: 1,
                title: 'Move Request',
                uid: request.current_user.uid,
                field_first_name: request.current_user.field_user_first_name,
                field_last_name: request.current_user.field_user_last_name,
                field_e_mail: request.current_user.mail,
                field_phone: request.current_user.field_primary_phone,
                field_additional_phone: request.current_user.field_user_additional_phone,
                field_approve: request.status,
                field_move_service_type: request.serviceType,
                field_date: { date: request.moveDate, time: '15:10:00' }, //"06/16/2015",
                field_size_of_move: request.move_size.raw,
                field_type_of_entrance_from: request.typeFrom,
                field_type_of_entrance_to_: request.typeTo,

                field_start_time: request.preferedTime,
                field_price_per_hour: request.rate,
                field_movers_count: request.crew,
                field_distance: Math.round(request.distance),
                field_travel_time: request.travelTime,
                field_minimum_move_time: request.min_time,
                field_maximum_move_time: request.max_time,
                field_duration: request.duration,

                field_extra_furnished_rooms: request.checkedRooms.rooms,
                field_moving_to: {
                    country: "US",
                    administrative_area: request.addressTo.state,
                    locality: request.addressTo.city,
                    postal_code: request.addressTo.postal_code,
                    thoroughfare: request.adrTo,
                    premise: ""
                },
                field_apt_from: request.aptFrom,
                field_apt_to: request.apt_to.value,
                field_moving_from: request.field_moving_from,


                field_dummy_send_mail: request.dummyMail, // CHECK IF NEED SEND MAILS TO CLIENT
                field_storage_price: request.storage_price,
            };

            return move_request;

        }




    }
})();
