(function () {
    'use strict';

    angular.module('move.requests')
        .run(routeConfig);

    routeConfig.$inject = ['routehelper'];
    /* @ngInject */
    function routeConfig(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/requests/:id',
                config: {
                     templateUrl: 'app/requests/areas/allrequests.html',
                    title: 'requests',
                }
            },
            {
                url: '/requests',
                config: {
                    templateUrl: 'app/requests/areas/allrequests.html',
                    title: 'requests',
                }
            },
              {
               url: '/requests/:id/:status',
                config: {
                     templateUrl: 'app/requests/areas/allrequests.html',
                    title: 'requests',
                }
            },
            {
                url: '/request/contract',
                config: {
                    templateUrl: 'app/contractpage/templates/contractSettings.html?4',
                    title: 'contractpage',
                }
            }
        ];
    }
})();