(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccOptions', ccOptions);

    ccOptions.$inject = ['$http','$sce','config','logger'];

 
    function ccOptions ($http,$sce,config,logger) {
        var directive = {
            link: link,
            scope: {
                'request': '=',
            },
            templateUrl: 'app/requests/directives/options/options.template.html',
            restrict: 'A'
        };
        return directive;
    
    
    
        function link(scope, element, attrs) {
             
            var nid = scope.request.nid;
            scope.busy = true;
            scope.loadOptions  = loadOptions;
            scope.active = 0;
            scope.editMode = false;
            
            scope.options = [];
            initOption();
         
            
            scope.loadOptions(nid);
            

            ////////////////////////////////////
             function loadOptions(nid){
             
                 $http.get(config.serverUrl+'server/move_invertory/'+nid)
                .success(function(data, status, headers, config) {
                     scope.busy = false;
                     scope.details = data.move_details;
                     if( scope.details != null) {
	                     if(angular.isDefined(scope.details.options)) {
		                     scope.options = scope.details.options;
                         }
                     }
                });
             }
            
              scope.addOption = function(option){
                 
                   // validate
                    var option = scope.option;
                  
                    scope.options.push(option);
                    initOption();
                  
                   

             }
             scope.updateOption = function(option,index){          
                   // validate
                    scope.options[index] = option;
                    scope.editMode = false;
                    scope.active = index;
                     initOption();

             }  
              scope.deleteOption = function(index){          
              
                    scope.options.splice(index,1);
                    scope.editMode = false;
                    initOption();
                   

             }  
              scope.editOption = function(option,index){          
                   // validate
                    scope.option = option;
                    scope.editMode = true;
                    scope.active = index;

             }  
              
             function initOption (){
             
             
                   scope.option = { 
                    "pickup" : '', 
                    "delivery" : '', 		
                    "picktime1" : 0,
                    "picktime2" : 0,
                    "deltime1" : 0,
                    "deltime2" : 0,
                    "rate" : '',
                    "text" : ''
                    }

             
             }  
             scope.saveOptions = function(){
                 
                 var nid = scope.request.nid;
                 var data ={};
                 data.data = {};
                 data.data.details = scope.details;
                 data.data.details['options'] = scope.options;
                 var jsonDetails= JSON.stringify(data);
                 
                 scope.busy = true;
                 
                 $http.post(config.serverUrl+'server/move_invertory/'+nid,jsonDetails)
                .success(function(data, status, headers, config) {
                    
                     scope.busy = false;
                     scope.logs = data;
                     scope.changed = false;
                     logger.success("Details were updated", data, "Success");

                });

             }
             
            
             
              element.find(".delivery_time_select_from").on("change", function() {
	 
                var time = $(this).val();
                element.find(".delivery_time_select_to option").prop("disabled", false).show();
                for(var i = 0; i <=time; i++){
                    element.find(".delivery_time_select_to option[value='"+i+"']").prop("disabled", true).hide();
                }

             });

              element.find(".pickup_time_select_from").on( "change", function() {

                var time = $(this).val();
                element.find(".pickup_time_select_to option").prop("disabled", false).show();
                for(var i = 0; i <=time; i++){
                    element.find(".pickup_time_select_to option[value='"+i+"']").prop("disabled", true).hide();
                }

             });
            
  

             
        
        }
        
        

    
    
    }
})();