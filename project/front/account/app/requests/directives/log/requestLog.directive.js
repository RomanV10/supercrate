(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccRequestLog', ccRequestLog);

    ccRequestLog.$inject = ['$http','$sce','config'];

 
    function ccRequestLog ($http,$sce,config) {
        var directive = {
           link: link,
            scope: {
                'nid': '=nid',
        
            },
            templateUrl: 'app/requests/directives/log/saleslog.template.html',
            restrict: 'A'
        };
        return directive;
    
    
    
        function link(scope, element, attrs) {
             
             scope.open = false;
			 scope.admin_name = attrs.adminName;
            scope.busy = true;
            
            scope.loadLog = function(nid){
             
                 $http.get(config.serverUrl+'server/move_sales_log/'+nid)
                .success(function(data, status, headers, config) {
                    
                     scope.busy = false;
                     scope.logs = data;

                });

             }
  
             scope.loadLog(scope.nid);
        
             scope.logOpen = function() {
                
                 scope.open = !scope.open;
             
             
             }
			 
			 scope.toTrustedHTML = function( html ){
				if(angular.isString(html)){
					
						return $sce.trustAsHtml( html );
					
					}
			}
             
             scope.addMessage = function() {
			 	
				if(scope.message.length){
					
					var d = new Date();
					var n = d.getTime();
					var message = {
					text: scope.message, 
					date : Math.round(n / 1000),
					event_type:'custom_log',
					user_name : scope.admin_name,
					};					
						
					
					scope.logs.unshift(message);
					// add to DB
					var message = {
                        text: scope.message, 
                        nid : scope.nid,
					};	
                    
				    $http.post(config.serverUrl+'server/move_sales_log',message).success(function(data, status, headers, config) {

						data.success = true;            
														   

				    });

					 
					 
					 scope.message = '';
					
				}
			 
			 }
			 
             
             
             scope.getIconType = function (type) {
             
                    
                 switch(type) {
                        case 'mail':
                            return 'fa-envelope-o';
                            break;
                        case 'request':
                            return 'fa-folder-o';
                            break;
                        case 'custom_log':
                            return 'fa-comment-o';
                            break;
                        default:
                            return 'fa-bookmark';
                    }

             
             
             } 
             
        
        }
        
        

    
    
    }
})();