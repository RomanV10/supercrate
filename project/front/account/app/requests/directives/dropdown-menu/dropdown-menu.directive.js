(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('erDropDown', erDropDown);

 
    function erDropDown ($rootScope) {
        var directive = {
            link: link,
            scope: {
                'request': '=',
            },
            templateUrl: 'app/requests/directives/dropdown-menu/dropdown-menu.template.html',
            restrict: 'E'
        };
        return directive;
    
    
    
        function link(scope, element, attrs) {
        
            
            scope.listVisible = false;
            
            scope.show = function() {
				scope.listVisible = true;
			};
            
            
			$rootScope.$on("documentClicked", function(inner, target) {
				if (!$(target[0]).is(".dropdown-display.clicked") && !$(target[0]).parents(".dropdown-display.clicked").length > 0)
					scope.$apply(function() {
						scope.listVisible = false;
					});
			});
        
        }
    
    
    }
})();