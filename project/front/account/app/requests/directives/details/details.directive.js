(function () {
	'use strict';

	angular
		.module('move.requests')
		.directive('ccDetails', ccDetails)
		.directive('ccListDetails', ccListDetails);

	ccDetails.$inject = ['$http', '$rootScope', 'config', 'CalculatorServices', 'common', 'datacontext', 'RequestServices', 'InitRequestFactory', 'apiService', 'moveBoardApi', '$q'];

	function ccDetails($http, $rootScope, config, CalculatorServices, common, datacontext, RequestServices, InitRequestFactory, apiService, moveBoardApi, $q) {
		var directive = {
			link: link,
			controller: DetailsController,
			scope: {
				'request': '=',
			},
			templateUrl: 'app/requests/directives/details/details.template.html?4',
			restrict: 'A'
		};

		DetailsController.$inject = ['$scope'];

		return directive;

		function DetailsController($scope) {
			$scope.addCommentPlaceHolder = 'Please include information on heavy, oversized or fragile items, narrow stairways or halls, long walk ways, extra stops (including address), or anything else that may affect the moving time.';
		}

		function link($scope) {
			const FULL_DATE_FORMAT = 'MMM DD, YYYY';
			const TIME_ZONE_OFFSET = - new Date().getTimezoneOffset() * 60;
			var nid = $scope.request.nid;
			$scope.service = $scope.request.service_type.raw;
			$scope.status = $scope.request.status.raw;

			$scope.range = common.range;
			var fieldData = datacontext.getFieldData();
			var enums = fieldData.enums;
			$scope.longdistanceSettings = angular.fromJson(fieldData.longdistance);

			$scope.changed = false;
			$scope.from = $scope.request.from;
			$scope.to = $scope.request.to;
			$scope.entarence_origin = $scope.request.type_from.value;
			$scope.entarence_dest = $scope.request.type_to.value;
			$scope.busy = false;
			$scope.loadDetails = loadDetails;
			$scope.details = InitRequestFactory.initRequestDetails($scope.request, $scope.request.inventory.move_details, true);
			$scope.initialDetails = InitRequestFactory.initRequestDetails($scope.request, $scope.request.inventory.move_details, true);
			$scope.set_change = function () {
				$scope.changed = true;
			};
			$scope.longDistance = $scope.request.service_type.raw == 7;
			$scope.flatRate = $scope.request.service_type.raw == 5;
			$scope.logMsg = {};

			loadDetails(nid);

			////////////////////////////////////
			function loadDetails(nid) {
				$http.get(config.serverUrl + 'server/move_invertory/' + nid)
					.success(function (data, status, headers, config) {
						$scope.busy = false;
						$scope.details = InitRequestFactory.initRequestDetails($scope.request, data.move_details, true);
						initDefaults();
					});
			}

			function initDefaults() {
				if ($scope.details.delivery == '') {
					$scope.delivery_disable = true;
				}
			}

			$scope.$on('on-update-date-of-request-details', function () {
				$scope.set_change();
			});

			$scope.details_change = function (title, value, field) {
				if ($scope.longDistance) {
					$scope.changed = $scope.details.current_door != 1 && $scope.details.new_door != 1
                        && $scope.details.current_permit != 'None' && $scope.details.new_permit != 'None';
				} else {
					$scope.changed = true;
				}
				var parking = {
					None: 'Please Select',
					PR: 'Street Parking',
					PM: 'Loading Dock',
					LZA: 'Commercial Loading Zone',
					PDW: 'Private Driveway',
					DK: 'Don\'t Know'
				};
				var to = '';
				var html = '';
				if (title.toLowerCase().search('distance') >= 0) {
					to = value + ' feet';
					if (value == 1) {
						to = 'Select approximate distances';
					}
					if (value == 2) {
						to = 'Don\'t Know';
					}
					if (value == 301) {
						to = '>250 feet';
					}
				} else if (title.toLowerCase().search('stairs') >= 0) {
					to = value + '  steps';
					if (value == 1) {
						to = 'Select number of steps';
					}
					if (value == 2) {
						to = 'Don\'t Know';
					}
				} else if (title.toLowerCase().search('parking') >= 0) {
					to = parking[value];
				} else if (title.toLowerCase().search('comments') >= 0) {
					html = value;
				}
				var msg = {
					text: title,
					to: to,
					html: html
				};
				$scope.logMsg[field] = msg;
			};

			$scope.saveDetails = function () {
				var nid = $scope.request.nid;
				var data = {};
				data.data = {};
				data.data.details = $scope.details;
				var lastDDay = CalculatorServices.getLongDistanceWindow($scope.request, $scope.details.delivery, $scope.request.inventory.move_details.delivery_days);
				if ($scope.request.service_type.raw != 5) {
					data.data.field_ld_sit_delivery_dates = {
						value: moment($scope.details.delivery).unix() + TIME_ZONE_OFFSET,
						value2: moment(lastDDay).unix() + TIME_ZONE_OFFSET
					};
				}
				var jsonDetails = JSON.stringify(data);
				var logArr = [];
				_.forEach($scope.logMsg, function (item) {
					logArr.push(item);
				});
				//If storage save for storage also
				if ($scope.request.service_type.raw == 2) {
					$http.post(config.serverUrl + 'server/move_invertory/' + $scope.request.storage_id, jsonDetails)
						.success(function (data, status, headers, config) {
							$scope.logMsg = {};
							if (!_.isEmpty(logArr)) {
								RequestServices.sendLogs(logArr, 'Request was updated', $scope.request.storage_id, 'MOVEREQUEST');
							}
						});
				}

				if (!_.isUndefined($scope.request.request_all_data.packing_request_id)) {
					let promise = apiService.postData(moveBoardApi.inventory.setInventory + $scope.request.request_all_data.packing_request_id, jsonDetails);
					$q.all(promise).then(function () {
						if (!_.isEmpty(logArr)) {
							RequestServices.sendLogs(logArr, 'Request was updated', $scope.request.request_all_data.packing_request_id, 'MOVEREQUEST');
						}
					});
				}

				$scope.busy = true;

				$http
					.post(config.serverUrl + 'server/move_invertory/' + nid, jsonDetails)
					.success(function (data, status, headers, config) {
						$scope.busy = false;
						$scope.logs = data;
						var details = $scope.details;
						var logArr = [];
						_.forEach($scope.logMsg, function (item) {
							logArr.push(item);
						});
						if (!_.isEmpty(logArr)) {
							let data = {
								node: { nid: $scope.request.nid },
								notification: {
									text: ''
								}
							};
							let notificationMsg = '';
							_.forEach(logArr, function(msg){
								notificationMsg += msg.text;
								if(msg.from && msg.to){
									notificationMsg += 'changed from: ' + msg.from + ', to: ' + msg.to;
								}else if(msg.to){
									notificationMsg += 'changed to: ' + msg.to;
								}else if(!msg.to && msg.html){
									let regex = /(<([^>]+)>)/ig;
									let result = msg.html.replace(/<\/p>/g, '\n').replace(regex, '');
									notificationMsg += 'changed to: ' + result;
								}
								notificationMsg += '\n';
							});
							data.notification.text = notificationMsg;
							let dataNotify = {
								type: enums.notification_types.CUSTOMER_DO_CHANGE_WITH_REQUEST,
								data: data,
								entity_id: $scope.request.nid,
								entity_type: enums.entities.MOVEREQUEST
							};
							apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
							RequestServices.sendLogs(logArr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
						}
						$scope.logMsg = {};
						$rootScope.$broadcast('details.changed', details);
						$scope.changed = false;
					}).finally(() => {
						$rootScope.$broadcast('changeStatusToPendingInfo');
					});
			};

			function setToLogsActualTime (dates, text, field) {
				if (_.isNumber(dates)) dates = dates.toString();
				let datesByTypes = dates.split(',');
				let log = '';
				let isNeedDash = datesByTypes.length > 1;
				_.forEach(datesByTypes, (date, key) => {
					log += moment(+date).format(FULL_DATE_FORMAT);
					if (_.isEqual(key, 0) && isNeedDash) log += ' - ';
				});
				$scope.logMsg[field] = {
					text,
					to: log
				};
			}

			$scope.$on('details-dates-updated', (_, data) => {
				setToLogsActualTime(data.dates, data.text, data.field);
			});
		}
	}

	function ccListDetails() {
		var directive = {
			link: link,
			scope: {
				'details': '=',
			},
			templateUrl: 'app/requests/directives/details/listdetails.template.html',
			restrict: 'A'
		};

		return directive;

		function link(scope) {
			scope.parking = [];
			scope.parking['PR'] = 'Street Parking';
			scope.parking['PM'] = 'Loading Dock';
			scope.parking['LZA'] = 'Commercial Loading Zone';
			scope.parking['PDW'] = 'Private Driveway';
			scope.parking['DK'] = 'Don\'t Know';
		}
	}
})();
