(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccRangePicker', ccRangePicker)

    ccRangePicker.$inject = ['$rootScope'];

    function ccRangePicker($rootScope) {
        return {
            restrict: 'A',
            template: ' <input><div></div>',
            scope: {
                'request': '=',
                'message': '=',
                'editrequest': '=',
                'details':'=',
            },
            link: function (scope, element, attrs, ngModel) {
                var rangepicker = element.find('div');
                var rangeinput = element.find('input');
                var cur = -1, prv = -1;
	            var pickupDates = '';
                var dateFormat = 'd M, yy';
                var momentFormat = 'D MMM, YYYY'

                if(attrs.type == 'pickup')
                    initPossibleDates(scope.details.pickup);
                else
                    initPossibleDates(scope.details.delivery);



                rangepicker
                  .datepicker({

                        dateFormat: dateFormat,
                        numberOfMonths: 2,
                        changeMonth: false,
                        changeYear: false,
                        showButtonPanel: true,
                        minDate: new Date(),
                        beforeShowDay: function ( date ) {
                            if(date.getTime() ==  Math.min(prv, cur) || date.getTime() == Math.max(prv, cur))
                                  return [true, ( (date.getTime() == Math.min(prv, cur) || date.getTime() == Math.max(prv, cur)) ? 'date-range-selected range' : '')];
                            else
                              return [true, ( (date.getTime() > Math.min(prv, cur) && date.getTime() < Math.max(prv, cur)) ? 'date-range-selected' : '')];

                           },

            onSelect: function ( dateText, inst ) {
                  var d1, d2;
				  var ddd1,ddd2;

				  if( prv != -1 && cur != -1 && prv !=cur ) {
					cur = -1;
					prv = -1;
					}

                  cur = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();
                  if ( prv == -1 || prv == cur ) {
                     prv = cur;
					 pickupDates = cur;
                     rangeinput.val( dateText );

					  d1 = $.datepicker.formatDate(dateFormat, new Date(Math.min(prv,cur)), {} );
                      updateModel(pickupDates);
					//  rangepicker.datepicker( "option", "minDate",d1);


                  } else {
                     d1 = $.datepicker.formatDate( dateFormat, new Date(Math.min(prv,cur)), {} );
					//  element.datepicker( "option", "minDate",d1);
                     d2 = $.datepicker.formatDate( dateFormat, new Date(Math.max(prv,cur)), {} );



                     rangeinput.val( d1+' - '+d2 );
					 ddd1 = cur;
                     ddd2 = prv;
					 pickupDates = ddd1+','+ddd2;
                     updateModel(pickupDates);
                  }

                $rootScope.$broadcast('on-update-date-of-request-details');
               },

            onChangeMonthYear: function ( year, month, inst ) {
                  //prv = cur = -1;
               },

            onAfterUpdate: function ( inst ) {
                  $('<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" data-handler="hide" data-event="click">Done</button>')
                     .appendTo( rangepicker.find('.ui-datepicker-buttonpane'))
                     .on('click', function () { rangepicker.hide(); });
               }
            })
              .position({
                    my: 'left top',
                    at: 'left bottom',
                    of: rangepicker
                 })
              .hide();



            rangeinput.on('focus', function (e) {
                     var v = this.value,
                         d;

                     try {
                        if ( v.indexOf(' - ') > -1 ) {
                           d = v.split(' - ');

                           prv = $.datepicker.parseDate( dateFormat, d[0] ).getTime();
                           cur = $.datepicker.parseDate( dateFormat, d[1] ).getTime();

                        } else if ( v.length > 0 ) {
                           prv = cur = $.datepicker.parseDate( dateFormat, v ).getTime();
                        }
                     } catch ( e ) {
                        cur = prv = -1;
                     }

                     if ( cur > -1 )
                        rangepicker.datepicker('setDate', new Date(cur));

                      rangepicker.datepicker('refresh').show();
                  });

             element.on('mouseout','.ui-datepicker-calendar td', function(event) {

			 $(".ui-datepicker-calendar td").removeClass("highlight");

             })
             .on('mouseover','.ui-datepicker-calendar td', function(event) {

                var year = $(this).attr("data-year");
                            var month = $(this).attr("data-month");
                            var day = $(this).children().text();
                            var tocurtime = (new Date(year,month,day)).getTime();
                            // compare with prv date

                        if (tocurtime > cur && cur != -1 && prv == cur)	{
                            // higlight if true
                                $(".ui-datepicker-calendar td").each(function() {

                                    var year = $(this).attr("data-year");
                                    var month = $(this).attr("data-month");
                                    var day = $(this).children().text();
                                    var curtime = (new Date(year,month,day)).getTime();
                                    if (curtime > cur && curtime < tocurtime && cur != -1)
                                    $(this).addClass("highlight");

                                });

                        }
                        if (tocurtime < cur && cur != -1 && prv == cur)	{
                            // higlight if true
                                $(".ui-datepicker-calendar td").each(function() {

                                    var year = $(this).attr("data-year");
                                    var month = $(this).attr("data-month");
                                    var day = $(this).children().text();
                                    var curtime = (new Date(year,month,day)).getTime();
                                    if (curtime < cur && curtime > tocurtime && cur != -1)
                                    $(this).addClass("highlight");

                                });

                        }

            });




            $(document).mouseup(function (e)
            {
                var container = rangepicker;
                var target = e.target;

                if (!container.is(e.target) // if the target of the click isn't the container...
                    && container.has(e.target).length === 0 && e.target['localName'] != 'input') // ... nor a descendant of the container
                {
                    container.hide();
                }

            });

				function updateModel(dates) {
					if (attrs.type == 'pickup') {
						scope.details.pickup = dates;
						$rootScope.$broadcast('details-dates-updated', {
							dates: scope.details.pickup,
							text: 'Preferred Pick Up dates:',
							field: 'pickup'
						});
					} else {
						scope.details.delivery = dates;
						$rootScope.$broadcast('details-dates-updated', {
							dates: scope.details.delivery,
							text: 'Preferred Delivery dates:',
							field: 'delivery'
						});
					}
				}

         /*   function updateMessage(date,from){

                    if(angular.isDefined(scope.message)){
                        var date = moment(date).format('MM/DD/YYYY');

                        if(from){
                            scope.field = scope.request.delivery_date_from;
                            scope.lastVal =  scope.request.delivery_date_from.value;
                        }
                        else{
                            scope.field = scope.request.delivery_date_to;
                             scope.lastVal =  scope.request.delivery_date_to.value;
                        }
                        scope.editrequest[scope.field.field] = {date: date, time: '15:10:00'} ;
                        scope.message[scope.field.field] = {
                            lable : scope.field.lable,
                            oldValue : scope.lastVal,
                            newValue : date,

                        }

                    }

                }
            */

            function initPossibleDates(dates){

                	if(dates){
                        var deli = [];
                        var isNumberDates = angular.isNumber(dates);
                        if(!isNumberDates) {
                            deli = dates.split(",");
                            var firstDate = parseInt(deli[0]);
                            if (!(angular.isNumber(firstDate) && !isNaN(firstDate)) || !angular.isNumber(parseInt(deli[1]))) {
                                rangeinput.val(dates);
                                return;
                            }
                        } else {
                            deli[0] = dates;
                            deli[1] = dates;
                        }

                        var c1 = $.datepicker.formatDate( dateFormat, new Date(Math.min(deli[0],deli[1])), {} );
                        var c2 = $.datepicker.formatDate( dateFormat, new Date(Math.max(deli[0],deli[1])), {} );
                        if(c1 != c2){


                               rangeinput.val(c1+' - '+c2);

                            }
                        else
                            {

                                  rangeinput.val(c1);

                            }



                        }


            }



            }
        }


    };
})();
