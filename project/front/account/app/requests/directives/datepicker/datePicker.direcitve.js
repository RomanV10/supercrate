(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccDaPicker', ccDaPicker)
        .directive('ccRatePicker', ccRatePicker)


    ccDaPicker.$inject = ['$rootScope'];
    //Date Picker Directive
    function ccDaPicker($rootScope) {
        return {
            restrict: 'A',
            scope: {           
                'ngModel' : '=ngModel'
            },
             link: function (scope, element, attrs, ngModel) {
                 var minDate = new Date();
                 if(angular.isDefined(attrs.mindate)){
                     minDate = moment(attrs.mindate).add(1, 'day').format('MM/DD/YYYY');
                     minDate = new Date(minDate);
                 }
                 var dateFormat = 'MM d, yy';
                element.datepicker({
                    dateFormat: dateFormat,
                    numberOfMonths: 2,
                    changeMonth: false,
                    changeYear: false,
                    setDate : new Date(),
                    minDate: minDate,
                    showButtonPanel: true,
                    onSelect: function (date, obj) {
                        scope.date = date;

                        var curdate = $.datepicker.formatDate(dateFormat, new Date(date), {} );
                        scope.ngModel = curdate;
                        $rootScope.$broadcast('on-update-date-of-request-details');

                        scope.$apply();
                    },
                    beforeShow: function() {
                        $("#ui-datepicker-div").addClass(attrs.class);
                    }

                });
            }
        };
	}
    
     
        

     
    //Date Picker Directive
    function ccRatePicker($rootScope,RequestServices,logger,datacontext,common) {
      return {
            restrict: 'A',
            scope: {           
                    'ngModel' : '=ngModel'
            },
             link: function (scope, element, attrs, ngModel) {
                
                 var calendar =  datacontext.getFieldData().calendar;
                 var settings =  datacontext.getSettingsData();
      
                 
                element.datepicker({
                    dateFormat: 'M d, yy',
                    numberOfMonths: 2,
                    changeMonth: false,
                    changeYear: false,
                    setDate : new Date(),
                    showButtonPanel: true,
                    onSelect: function (date, obj) {
    
                        var dprv = (new Date(date)).getTime();
                        
                        var date = $.datepicker.formatDate( 'mm/dd/yy', new Date(dprv), {} );
                        var curdate = $.datepicker.formatDate( 'M d, yy', new Date(dprv), {} );
                        scope.ngModel = curdate;
                        $rootScope.$broadcast('isDate', curdate);
                    

                    },
                    beforeShowDay : function(date){

                         return checkDate(date,calendar);

                    }
                });

                 function checkDate(date,calendar) {	


                        var day = moment(date).format('DD');
                        var month = moment(date).format('MM');
                        var year = date.getFullYear();			
                        var date = year+'-'+month+'-'+day;	
  
                     
                     
                        if(calendar[year][date]=='block_date')
                            return [false, 'not-available', 'This date is NOT available'];
                        else
                            return [true, settings.calendartype[calendar[year][date]], settings.calendartype[calendar[year][date]]];

                   }  


            }
        };
	};

       
})();