import './cc-rooms.styl';

(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccSelect', ccSelect)
        .directive('ccInput', ccInput)
        .directive('ccInputTime', ccInputTime)
        .directive('ccInputAddress', ccInputAddress)
        .directive('ccDatePicker', ccDatePicker)
        .directive('ccFaPicker', ccFaPicker)
        .directive('ccRooms', ccRooms)
        .directive('ccStartTime', ccStartTime)
        .directive('ccReadMore', ccReadMore)
        .directive('ccTravelTime', ccTravelTime)
        .directive('ccCurrency', ccCurrency)
        .directive('ccPercent', ccPercent);

     //Date Picker Directive

    //Date Picker Directive
    function ccFaPicker(RequestServices,logger) {
        return {
            restrict: 'A',
            scope: {

                    'calendar' : '=calendar'

            },
             link: function (scope, element, attrs, ngModel) {
                element.datepicker({
                    dateFormat: 'M d, yy',
                    numberOfMonths: 2,
                    changeMonth: false,
                    changeYear: false,
                    showOn: 'button',
			        buttonImageOnly: true,
                    setDate : attrs.date,
                    showButtonPanel: true,
                    onSelect: function (date, obj) {
                        scope.date = date;
                        var dprv = (new Date(date)).getTime();

                        var date = $.datepicker.formatDate( 'mm/dd/yy', new Date(dprv), {} );
                        var curdate = $.datepicker.formatDate( 'MM d, yy', new Date(dprv), {} );

                        //REINIT SCHEDULE
                        scope.$parent.parklotControl.reinitParklot(date);
                        $(".curdate").text(curdate);


                    },
                    beforeShowDay : function(date){

                         return checkDate(date,scope.calendar);

                    }
                });

                 function checkDate(date,calendar) {



                        var day = (date.getDate().toString()).slice(-2);
                        var month = ((date.getMonth() + 1).toString()).slice(-2);
                        var year = date.getFullYear();
                        var date = year+'-'+month+'-'+day;
                        if(calendar[year][date]=='block_date')
                            return [false, 'not-available', 'This date is NOT available'];
                        else
                            return [true, calendar[year][date], calendar[year][date]];

                   }


            }
        };
	};

    function ccDatePicker(datacontext, SweetAlert) {
        return {
            restrict: 'A',
            scope: {
                    'message': '=',
                    'field' : '=',
                    'editrequest': '=',
                    'calendar' : '='

            },
             link: function (scope, element, attrs) {

                var refresh = (attrs.refresh === "true");
                element.datepicker({
                    dateFormat: 'M d, yy',
                    numberOfMonths: 2,
                    changeMonth: false,
                    changeYear: false,
                    showButtonPanel: true,
                    minDate: new Date(),
                    onSelect: function (date, obj) {
                        if(obj.settings.beforeShowDay(new Date(moment(date, 'MMMM D, YYYY')))[1] == "Block this day"){
                            SweetAlert.swal({title: "Error!", text: "This day is blocked", type: "error", timer: 2000,});
                            while(obj.settings.beforeShowDay(new Date(moment(date, 'MMMM D, YYYY')))[1] == "Block this day"){
                                date = moment(date, 'MMMM D, YYYY').add(1, 'day').format('MMMM D, YYYY');
                            }
                            obj.selectedDay =(new Date(date)).getDate();
                            obj.selectedMonth=(new Date(date)).getMonth();
                            obj.selectedYear=(new Date(date)).getFullYear();
                            element[0].value = date;
                        }

                        scope.date = date;
                        var dprv = (new Date(date)).getTime();
                        var dateFormated = $.datepicker.formatDate( 'yy-mm-dd', new Date(dprv), {} );

                        scope.field.value = dateFormated;
                        scope.field.raw = moment(scope.field.value).unix();

                        scope.editrequest[scope.field.field] = {date: dateFormated, time: '15:10:00'} ;
                        scope.message[scope.field.field] = {
                            label : scope.field.label,
                            oldValue : obj.lastVal,
                            newValue : element.val(),
                        };

                        //INIT CHEDULE FOR THIS DAY
                        var curdate = $.datepicker.formatDate( 'MM d, yy', new Date(dprv), {} );
                        if(refresh){
                            scope.$parent.parklotControl.reinitParklot(dateFormated);
                            $(".curdate").text(curdate);
                        }

                        scope.$apply();
                    },
                    beforeShowDay : function(date){

                         return checkDate(date,scope.calendar);
                    }
                });

                 function checkDate(date,calendar) {


                     var settings =  datacontext.getFieldData();

                     var day = (date.getDate().toString()).slice(-2);
                     if (day < 10) day = '0' + day;

                     var month = ((date.getMonth() + 1).toString()).slice(-2);
                     if (month < 10) month = '0' + month;

                     var year = date.getFullYear();
                     var dates = year + '-' + month + '-' + day;

                     if(calendar[year][dates]=='block_date')
                         return [false, 'not-available', 'This date is NOT available'];
                     else
                         return [true, settings.calendartype[calendar[year][dates]], settings.calendartype[calendar[year][dates]]];
                   }


            }
        };
	};

    function ccRooms() {
        return {
            restrict: 'A',
            scope: {
                'message': '=message',
                'field' : '=field',
                'editrequest': '=editrequest'

            },
            link: function (scope, element) {
                scope.rooms = ['living room','dining room','office','extra room','basement','garage','patio','play room'];

                 //Selecte Rooms
                jQuery.each(scope.field.raw, function(index, item) {
                    element.find("option[value="+item+"]").attr('selected', 'selected');
                });
                element.chosen({disable_search_threshold: 10});

                element.bind('change', function () {
                    //BIND CHANGE FUNCTION
                    scope.editrequest[scope.field.field] = element.val() || [];
                    scope.field.raw = element.val() || [];
                    scope.message[scope.field.field] = {
                        label: scope.field.label,
                        oldValue: [],
                        newValue: []
                    };
                    var count = 0;
                    _.forEach(scope.field.old, function (val, i) {
                        count++;
                        scope.message[scope.field.field].oldValue += scope.rooms[scope.field.old[i] - 1];
                        if (scope.field.old.length > 1 && scope.field.old.length != count)
                            scope.message[scope.field.field].oldValue += ', ';
                    });
                    var newVal = element.val() || [];
                    count = 0;
                    _.forEach(newVal, function (val, j) {
                        count++;
                        scope.message[scope.field.field].newValue += scope.rooms[val - 1];
                        if (newVal.length > 1 && newVal.length != count)
                            scope.message[scope.field.field].newValue += ', ';
                    });



                });

            }
        };

    };



       function ccStartTime(common) {
        return {
            restrict: 'AE',
            scope: {
                'message': '=message',
                'field' : '=field',
                'editrequest': '=editrequest',
                'isolated': '=isolated',
                'contract': '=contract',
                'addContract': '=',
                'value': '=ngValue',
                'type': '@fieldType',
                'index': '=index'

            },
             link: function (scope, element) {
              element.timepicker({
                'disableTouchKeyboard':true,
                'minTime': '12:00am',
                'maxTime': '11:45pm',
                'timeFormat': 'g:i A',
                  step: 15
                });
             element.bind('change', function() {
                 if(scope.contract)
                     scope.$parent.changeCrewTime(scope.index+1, scope.type,scope.value);
                 if(scope.addContract){
                     scope.$parent.changeTime(scope.type, element.val());
                 }
                 if (!scope.isolated) {
                     //BIND CHANGE FUNCTION
                     scope.editrequest[scope.field.field] = element.val();
                     scope.message[scope.field.field] = {
                         label : scope.field.label,
                         oldValue :  scope.field.old,
                         newValue : element.val(),

                     };
                 }
             });
            }
        };

    };

     function ccTravelTime(common) {
        return {
            restrict: 'A',
             link: function (scope, element) {
                 element.timepicker({
                    'disableTouchKeyboard':true,
                    'minTime': '00:00',
                    'maxTime': '03:00',
                    'timeFormat': 'H:i',
                    'step': 15,
                });

            }
        };

    };

    function ccReadMore() {
        return {
            restrict: 'A',
            scope: {
                text: "=TextTruncate",
            },
            link: function (scope, element,attrs) {

                var default_height = 50;
                if(angular.isDefined(attrs.height)){
                    default_height = parseInt(attrs.height);
                }
                scope.$watch( "text", function() {
                    element.readmore({
                        collapsedHeight: default_height,
                        moreLink: '<a href="#" style="float: right;margin-top: -10px;font-size: 10px;" class="comment_more">More</a>',
                        lessLink: '<a href="#" style="float: right;margin-top: -10px;font-size: 10px;" class="comment_more">Less</a>',
                    });

                } );


            }
        };

    };


    function ccInputTime(common) {
        return {
            restrict: 'A',
            scope: {
                'message': '=',
                'field' : '=',
                'editrequest': '=',
                'request': '=request',
            },
            link: function (scope, element) {

                element.timepicker({
                    'disableTouchKeyboard':true,
                    'minTime': '00:30',
                    'maxTime': '24:00',
                    'timeFormat': 'H:i',
                    'step': 15,
                });

                 element.bind('change', function() {

                                 //BIND CHANGE FUNCTION

                    scope.editrequest[scope.field.field] = Number(common.convertStingToIntTime(element.val()).toFixed(2));
                    scope.message[scope.field.field] = {
                            lable : scope.field.label,
                            oldValue :  scope.field.old,
                            newValue : element.val(),

                        }
                    scope.field.value = element.val();

                    scope.$parent.parklotControl.updateWorkTime(scope.field.field , element.val());
                    scope.$parent.updateQuote();
                });

            }
        };

    };

     function ccInput() {
        return {
            restrict: 'A',
            scope: {
                'message': '=',
                'field' : '=',
                'editrequest': '=',

            },
             link: function (scope, element) {


                 element.bind('change', function() {

                        //BIND CHANGE FUNCTION
                scope.editrequest[scope.field.field] = element.val();
                 var label = '';
                 var words = _.words(scope.field.label.replace(/_/g, ' '));
                 _.forEach(words, function(word,i){
                     word = word.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
                     label += word + ' ';
                 });
                     scope.message[scope.field.field] = {
                         label: label,
                         oldValue: scope.field.old,
                         newValue: element.val()
                     };
                scope.field.value = element.val();
                     if(scope.field.field == 'field_price_per_hour'){
                         scope.$parent.updateQuote();
                         scope.$parent.request.reservation_rate = element.val();
                         $("#reserv_price").val(element.val());
                     }
                });

            }
        };

    };


	ccInputAddress.$inject = ['CalculatorServices', 'SweetAlert', '$q', 'geoCodingService'];
	function ccInputAddress(CalculatorServices, SweetAlert, $q, geoCodingService) {
		return {
			restrict: 'A',
			scope: {
				'message': '=',
				'field': '=',
				'subfield': '@',
				'type': '@',
				'editrequest': '='
			},
			link: function (scope, element) {
				let previousState;
				const ENTRANCE_TYPES = _.get(scope.$root, 'fieldData.field_lists.field_type_of_entrance_from', {});
				element
					.bind('focus', () => {
						previousState = element.val();
					})
					.bind('change', function () {
						scope.editrequest[scope.type] = scope.field;
						scope.editrequest[scope.type][scope.subfield] = element.val();
						scope.message[scope.type] = {};
						let isDropOffOrPickUp = scope.type == 'field_extra_pickup' || scope.type == 'field_extra_dropoff';

						let label = '';
						let words = _.words(scope.type.replace(/_/g, ' '));

						_.forEach(words, function (word, i) {
							if (i > 0) {
								word = word.replace(/\w\S*/g, function (txt) {
									return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
								});
								label += word + ' ';
							}
						});

						scope.message[scope.type].label = label;
						let message = '';

						if (isDropOffOrPickUp) {
							if (scope.field.thoroughfare && scope.field.thoroughfare != null && scope.field.thoroughfare != '')
								message += scope.field.thoroughfare + ' ';
							if (scope.field.premise && scope.field.premise != null && scope.field.premise != '')
								message += scope.field.premise + ' ';
							if (scope.field.locality && scope.field.locality != null && scope.field.locality != '')
								message += scope.field.locality + ' ';
							if (scope.field.administrative_area && scope.field.administrative_area != null && scope.field.administrative_area != '')
								message += scope.field.administrative_area;
							if (scope.field.postal_code && scope.field.postal_code != null && scope.field.postal_code != '')
								message += ', ' + scope.field.postal_code;
							if (scope.field.organisation_name && scope.field.organisation_name != null && scope.field.organisation_name != '')
								message += ', ' + ENTRANCE_TYPES[scope.field.organisation_name];
						} else if (scope.type == 'field_moving_from') {
							if (scope.field.thoroughfare && scope.field.thoroughfare != null && scope.field.thoroughfare != '')
								message += scope.field.thoroughfare + ' ';
							if (scope.field.locality && scope.field.locality != null && scope.field.locality != '')
								message += scope.field.locality + ' ';
							if (scope.field.administrative_area && scope.field.administrative_area != null && scope.field.administrative_area != '')
								message += scope.field.administrative_area;
							if (scope.field.postal_code && scope.field.postal_code != null && scope.field.postal_code != '')
								message += ', ' + scope.field.postal_code;
							if (scope.field.organisation_name && scope.field.organisation_name != null && scope.field.organisation_name != '')
								message += ', ' + scope.field.organisation_name;
						} else if (scope.type == 'field_moving_to') {
							if (scope.field.thoroughfare && scope.field.thoroughfare != null && scope.field.thoroughfare != '')
								message += scope.field.thoroughfare + ' ';
							if (scope.field.locality && scope.field.locality != null && scope.field.locality != '')
								message += scope.field.locality + ' ';
							if (scope.field.administrative_area && scope.field.administrative_area != null && scope.field.administrative_area != '')
								message += scope.field.administrative_area;
							if (scope.field.postal_code && scope.field.postal_code != null && scope.field.postal_code != '')
								message += ', ' + scope.field.postal_code;
							if (scope.field.organisation_name && scope.field.organisation_name != null && scope.field.organisation_name != '')
								message += ', ' + scope.field.organisation_name;
						}

						if (message != '')
							scope.message[scope.type].newValue = message;

						if (scope.subfield == 'postal_code') {
							let postal_code = element.val();
							element.removeClass('error');

							if (postal_code.length === 5) {
								let promiseGeoCode = geoCodingService.geoCode(element.val());
								let promiseAreaCode = CalculatorServices.getLongDistanceCode(element.val());
								$q.all([promiseGeoCode, promiseAreaCode])
									.then(result => {
										scope.field.locality = result[0].city;
										scope.field.administrative_area = result[0].state;
										scope.field.postal_code = element.val();

										if (!isDropOffOrPickUp) {
											scope.field.premise = result[1];
										}

										if (isDropOffOrPickUp && _.isNull(scope.field.organisation_name)) {
											scope.field.organisation_name = 1;
										}

										scope.editrequest[scope.type] = scope.field;

									}, () => {
										const TEXT = 'You entered the wrong zip code.';
										revertToPreviousState(TEXT);
									});
							} else {
								const TEXT = 'Please enter valid zip code';
								revertToPreviousState(TEXT);
							}
						}

						scope.$apply();
					});


				element.bind('keyup', function () {
					if (!scope.editrequest[scope.type]) {
						scope.editrequest[scope.type] = scope.field;
						scope.editrequest[scope.type][scope.subfield] = element.val();
					}
					if (_.isEmpty(element.val())) {
						scope.editrequest[scope.type] = scope.field;
						scope.editrequest[scope.type][scope.subfield] = element.val();
						scope.$apply();
					}
				});

				function revertToPreviousState (text) {
					SweetAlert.swal(text, '', 'error');
					delete scope.editrequest[scope.type];
					delete scope.message[scope.type];
					scope.field.postal_code = previousState;
					element.val(previousState);
				}
			}
		};
	}

	/* @ngInject */
	function ccSelect(InventoriesServices) {
		return {
			restrict: 'A',
			scope: {
				'editrequest': '=editrequest',
				'message': '=message',
				'field': '=field',
			},
			link: function (scope, element) {
				element.bind('change', function () {
					if (scope.field.field == 'field_size_of_move') {
						InventoriesServices.checkIfChangeMoveSize(scope.$parent.request.nid, element.val())
							.then(result => {
								let hasError = result.status_code != 200;

								if (hasError) {
									element.val(scope.field.raw);
								} else {
									continueProcessChange();
								}
							});
					} else {
						continueProcessChange();
					}

					function continueProcessChange() {
						//BIND CHANGE FUNCTION
						let dataField = scope.$root.fieldData.field_lists[scope.field.field];

						if (!dataField) {
							dataField = scope.$root.fieldData.field_lists['field_type_of_entrance_to_'];
						}

						let label = '';
						let words = _.words(scope.field.field.replace(/_/g, ' '));

						_.forEach(words, (word, i) => {
							if (i > 0) {
								word = word.replace(/\w\S*/g, function (txt) {
									return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
								});
								label += word + ' ';
							}
						});

						let value = getCurrentValue();

						scope.editrequest[scope.field.field] = value;
						scope.field.raw = value;

						scope.message[scope.field.field] = {
							label: label,
							oldValue: scope.field.value,
							newValue: dataField[value]
						};

						scope.$parent.edit = true;
					}
				});

				function getCurrentValue() {
					let value = element.val();

					if (value != ''
						&& (scope.field.field == 'field_type_of_entrance_to_' || scope.field.field == 'field_type_of_entrance_from')
						&& _.isNaN(Number(value))) { // if it is string

						value = value.trim();

						let newValue = _.findKey(scope.$root.fieldData.field_lists.field_type_of_entrance_from, function (o) {
							return o.toLowerCase() == value.toLowerCase();
						});

						value = newValue + '';
					}

					return value;
				}

				element.bind('click', function () {
					if (scope.$parent.isInvalidParsingRequest) {
						if (_.isUndefined(scope.$parent.editrequest[scope.field.field])) {
							scope.$parent.editrequest[scope.field.field] = getCurrentValue();
							scope.$apply();
						}
					}
				});
			}

		};
	}

    function ccCurrency($filter) {
        return {
            restrict: 'A',
            require: '^ngModel',
            scope: true,
            link: function (scope, el, attrs, ngModelCtrl) {

                var reservedVal = 0;

                function formatter(value) {
                    value = value ? parseFloat(value.toString().replace(/[^0-9._-]/g, '')) || 0 : 0;
                    var formattedValue = $filter('currency')(value);
                    el.val(formattedValue);

                    ngModelCtrl.$setViewValue(value);
                   // scope.$apply();

                    return formattedValue;
                }

                ngModelCtrl.$formatters.push(formatter);

                el.bind('focus', function () {
                    reservedVal = el.val();
                    el.val('');
                });

                el.bind('blur', function () {
                    var val = el.val() != ''
                        ? el.val()
                        : reservedVal;

                    formatter(val);
                });
            }

        };

    }

    function ccPercent($filter) {
        return {
            restrict: 'A',
            require: '^ngModel',
            scope: true,
            link: function (scope, el, attrs, ngModelCtrl) {
                var reservedValue;
                function formatter(value) {
                    value = value ? parseFloat(value.toString().replace(/[^0-9._-]/g, '')) || 0 : 0;
                    var formattedValue = $filter('number')(parseFloat(value), 0) + '%';
                    el.val(formattedValue);

                    ngModelCtrl.$setViewValue(value);
                    // scope.$apply();

                    return formattedValue;
                }

                ngModelCtrl.$formatters.push(formatter);

                el.bind('focus', function () {
                    reservedValue = el.val();
                    el.val('');
                });

                el.bind('blur', function () {
                    var value = el.val() || 0;
                    if (!el.val()) {
                        value = reservedValue;
                    }
                    formatter(value);
                });
            }
        };
    }

})();
