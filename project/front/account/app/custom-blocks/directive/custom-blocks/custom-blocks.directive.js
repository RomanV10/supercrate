'use strict';

import {
	CM_SETTING_NAME
} from '../../../../../moveBoard/app/settings/accountPage/constants/account-page.constants';
import './custom-blocks.styles.styl';

angular.module('custom-blocks').directive('customBlocks', customBlocks);

customBlocks.$inject = ['BlocksService', '$location'];

function customBlocks(BlocksService, $location) {
	return {
		restrict: 'E',
		scope: {
			request: '<'
		},
		template: require('./custom-blocks.tpl.pug'),
		link: customBlockLink,
	};
	
	function customBlockLink($scope) {
		const COMMERCIAL_INDEX = '9'; // commercial index in settings
		let currentRequest = {
			service_type: $scope.request.service_type.raw,
			status: $scope.request.status.raw == 11 ? COMMERCIAL_INDEX : $scope.request.status.raw,
		};
		const PATH = $location.path();
		const RESERVATION = 'reservation';
		const CONTRACT = 'contract';
		$scope.isReservation = ~PATH.indexOf(RESERVATION) || ~PATH.indexOf(CONTRACT);
		
		function init() {
			BlocksService.getBlocks(CM_SETTING_NAME).then(resolve => {
				if (!_.get(resolve, 'customBlocks', false)) return;
				
				$scope.customBlocks = BlocksService.fetchCustomBlocks(resolve.customBlocks, currentRequest, $scope.isReservation);
			})
		}
		
		init();
	}
}
