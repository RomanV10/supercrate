'use strict';

import {
	GCB_SETTING_NAME
} from '../../../../../moveBoard/app/settings/accountPage/constants/account-page.constants';
import './general-blocks.style.styl';

angular.module('custom-blocks').directive('generalBlocks', generalBlocks);

/* @ngInject */
function generalBlocks(BlocksService, $location, erEstimateSignatureProcessor) {
	return {
		restrict: 'E',
		scope: {
			request: '<',
		},
		template: require('./general-blocks.tpl.pug'),
		link: generalBlocksLink,
	};
	
	function generalBlocksLink($scope) {
		const currentRequest = {
			status: Number($scope.request.status.raw),
			service_type: Number($scope.request.service_type.raw)
		};
		const PATH = $location.path();
		const RESERVATION = 'reservation';
		const CONTRACT = 'contract';
		$scope.isReservation = ~PATH.indexOf(RESERVATION) || ~PATH.indexOf(CONTRACT);
		
		function init() {
			BlocksService.getBlocks(GCB_SETTING_NAME).then(resolve => {
				if (!_.get(resolve, 'generalCustomBlock', false)) return;
				
				$scope.generalCustomBlocks = BlocksService.fetchGeneralBlocks(resolve.generalCustomBlock, currentRequest, $scope.isReservation);
				parseSignatures();
			});
		}
		
		init();

		function parseSignatures() {
			angular.forEach($scope.generalCustomBlocks, (block) => {
				block.body = erEstimateSignatureProcessor.parseGeneralCustomBlockSignatures(block.body, $scope.request);
			});
		}
	}
}
