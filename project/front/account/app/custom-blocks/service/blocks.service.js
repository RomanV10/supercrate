'use strict';

angular.module('custom-blocks').factory('BlocksService', BlocksService);

BlocksService.$inject = ['apiService', 'moveBoardApi', '$q'];

function BlocksService(apiService, moveBoardApi, $q) {
	return {
		getBlocks: getBlocks,
		fetchCustomBlocks: fetchCustomBlocks,
		fetchGeneralBlocks: fetchGeneralBlocks,
	};
	
	function getBlocks(variableKey) {
		let defer = $q.defer();
		
		apiService.postData(moveBoardApi.core.getVariable, {
			name: variableKey
		}).then(resolve => {
			let response = angular.fromJson(resolve.data[0] || resolve.data);
			defer.resolve(response);
		});
		
		return defer.promise;
	}
	
	function fetchCustomBlocks(customBlocks, request, reservation = false) {
		if (!_.get(customBlocks, `[${request.service_type}][${request.status}]`)) return [];
		
		return customBlocks[request.service_type][request.status].filter(block => {
			if (!block.showOnConfirmationPage && reservation) return;
			if (!block.showCustomBlock && !reservation) return;
			
			return block;
		});
	}
	
	function fetchGeneralBlocks(generalBlocks, request, reservation = false) {
		return generalBlocks.filter(block => {
			if (!block.showCustomBlock && !reservation) return;
			if (!block.showOnConfirmationPage && reservation) return;
			if (!block.useForCommercial && request.service_type == 11) return;
			
			let isActualServiceType = _.find(block.where.serviceType, {
				'show': true,
				'service_type': request.service_type
			});
			let isActualStatus = _.find(block.where.status, {
				'show': true,
				'request_status': request.status
			});
			
			if (isActualServiceType
				&& isActualStatus) {
				return true;
			}
		});
	}
}
