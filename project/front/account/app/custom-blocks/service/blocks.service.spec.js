describe('Custom Blocks (account)', () => {
	let BlocksService,
		testHelperService,
		customBlockMock,
		generalCustomBlock,
		request,
		requestLocalMove;
	
	beforeEach(inject((_BlocksService_, _testHelperService_) => {
		BlocksService = _BlocksService_;
		testHelperService = _testHelperService_;
		customBlockMock = testHelperService.loadJsonFile('custom-blocks.mock');
		generalCustomBlock = testHelperService.loadJsonFile('general-custom.mock').generalCustomBlock;
		request = testHelperService.loadJsonFile('request-for-blocks.mock');
		requestLocalMove = testHelperService.loadJsonFile('request-for-blocks-local-move.mock');
	}));
	
	it('Service have to be defined', () => {
		expect(BlocksService).toBeDefined();
	});
	
	describe('Custom Blocks', () => {
		let customRequest;
		let resultAccount;
		let resultReservation;
		let emptyResult = [];
		beforeEach(() => {
			customRequest = testHelperService.loadJsonFile('custom-block-result-local-move.mock');
			resultAccount = BlocksService.fetchCustomBlocks(customBlockMock, requestLocalMove, false);
			resultReservation = BlocksService.fetchCustomBlocks(customBlockMock, requestLocalMove, true);
		});
		
		describe('On Account', () => {
			it('On init should get data from variable resultAccount', () => {
				expect(resultAccount).toEqual(customRequest);
			});
		});
		
		describe('On Reservation', () => {
			it('On init should get data from variable resultReservation', () => {
				expect(resultReservation).toEqual(emptyResult);
			})
		})
	});
	
	describe('General Custom Blocks', () => {
		let generalCustomRequest;
		let resultAccount;
		let resultReservation;
		beforeEach(() => {
			generalCustomRequest = testHelperService.loadJsonFile('general-custom-request.mock');
			resultAccount = BlocksService.fetchGeneralBlocks(generalCustomBlock, request, false);
			resultReservation = BlocksService.fetchGeneralBlocks(generalCustomBlock, request, true);
		});
		describe('On Account', () => {
			it('On init should get data from variable resultAccount', () => {
				expect(resultAccount).toEqual(generalCustomRequest);
			});
		});
		
		describe('On Reservation', () => {
			it('On init should get data from variable resultReservation', () => {
				expect(resultReservation).toEqual(generalCustomRequest);
			});
		})
	});
});
