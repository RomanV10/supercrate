import configFront from '../../shared/config.json';

(function () {
	'use strict';

	var Raven = require('raven-js');

	Raven
		.config('https://a713f7dde17a4801b0a798b220263cc5@sentry.moversboard.net:9005/7', {
			"release": configFront.release
		})
		.addPlugin(require('raven-js/plugins/angular'), angular)
		.install();

	angular.module('app', [
		'app.core',
		'ngRaven',
		'app.account',
		'app.score',
		'app.layout',
		'app.storages',
		'move.requests',
		'app.tplBuilder',
		'app.history',
		'app.data',
		'ngMaterial',
		'app.messages',
		'app.inventories',
		'newInventories',
		'app.calculator',
		'ngMdIcons',
		'app.request',
		'app.commercialMove',
		'ngCookies',
		'app.settings',
		'flow',
		'contractModule',
		'naif.base64',
		'ngCroppie',
		'ui.router',
		'credit-cards',
		'ngImageCompress',
		'custom-blocks',
		'shared-geo-coding',
		'shared-service-types',
		'pending-info',
		'commonServices',
		'shared-customer-online',
		'rt.debounce',
		'shared-sockets',
		'phone-numbers',
		'review-shared',
	    require('angular-svg-round-progressbar'),
		'labor',
		'email',
		'shared-api',
		'er-signatures',
		'additionalServices',
		'log',
    ]).config(accountConfig);

	accountConfig.$inject = ['$compileProvider'];

	function accountConfig($compileProvider) {
		if (isProduction) {
			$compileProvider.debugInfoEnabled(false);
		}
	}
})();
