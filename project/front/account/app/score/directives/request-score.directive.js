import '../tamplates/request-score.styl';
(function () {
	'use strict';


	angular
		.module('app.score')
		.directive('requestScore', requestScore);
	requestScore.$inject = ['$rootScope'];

	function requestScore() {
		var directive = {
			link: link,
			scope: {
				'requestStatus': '<',
				'requestReceipts': '<',
				'isInventory': '<',
				'isDetails': '<',
				'isOptional': '<',
				'photos': '<'
			},
			template: require('../tamplates/request-score.tpl.pug'),
			restrict: 'E'
		};
		return directive;


		function link(scope) {
			let requestReceiptsWatcher,
				inventoryWatcher,
				detailsWatcher,
				requestStatusWatcher,
				photosWatcher;

			function getScore() {
				let newScore = 25;
				if (scope.isInventory) {
					newScore += 40;
				}
				if (scope.isDetails) {
					newScore += 20;
				}
				if (!scope.isOptional) {
					if (scope.isReservation) {
						newScore += 15
					}
				} else {
					if (scope.photosAdded) {
						newScore += 15
					}
				}

				scope.score =  newScore;
			}

			function getIsReservation () {
				let reservation = false;
				angular.forEach(scope.requestReceipts, function (receipt) {
					let isReservationReceipt = receipt.payment_flag == "Reservation" || receipt.payment_flag == "Reservation by client" || receipt.type == "reservation";

					if (isReservationReceipt && !receipt.pending) {
						reservation = true;
						return false;
					}
				});

				return reservation;
			}

			inventoryWatcher = scope.$watch('isInventory', function(newV, oldV){
				if (newV != oldV) {
					getScore();
				}
			}, true);

			detailsWatcher = scope.$watch('isDetails', function(newV, oldV){
				if (newV != oldV) {
					getScore();
				}
			}, true);
			requestStatusWatcher = scope.$watch('requestStatus', function(newV, oldV){
				if (newV != oldV) {
					scope.isReservation = scope.requestStatus == 3 || getIsReservation();
					getScore();
				}
			}, true);

			if (!scope.isOptional) {
				requestReceiptsWatcher = scope.$watchCollection('requestReceipts', function(newV, oldV){
					if (!_.isEqual(newV, oldV)) {
						scope.isReservation = scope.requestStatus == 3 || getIsReservation();
						getScore();
					}
				}, true);
			} else {
				photosWatcher = scope.$watchCollection('photos', function (newV, oldV) {
					if (!_.isEqual(newV, oldV)) {
						scope.photosAdded = !_.isEmpty(scope.photos);
						getScore();
					}
				}, true);
			}

			scope.$on('destroy' , function() {
				inventoryWatcher();
				detailsWatcher();
				requestStatusWatcher();
				if (!scope.isOptional) {
					requestReceiptsWatcher();
				} else {
					photosWatcher();
				}
			});

			if (!scope.isOptional) {
				scope.isReservation = scope.requestStatus == 3 || getIsReservation();
			} else {
				scope.photosAdded = !_.isEmpty(scope.photos);
			}

			getScore()
		}
	}
})();
