(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('common', common);

    common.$inject = ['$location', '$q', '$rootScope', '$timeout', 'logger'];

    /* @ngInject */
    function common($location, $q, $rootScope, $timeout, logger) {
        var throttles = {};

        var service = {
            // common angular dependencies
            $broadcast: $broadcast,
            $q: $q,
            $timeout: $timeout,
            getTimeArray : getTimeArray,
            getDateTime : getDateTime,
            // generic
            createSearchThrottle: createSearchThrottle,
            debouncedThrottle: debouncedThrottle,
            isNumber: isNumber,
            logger: logger, // for accessibility
            replaceLocationUrlGuidWithId: replaceLocationUrlGuidWithId,
            textContains: textContains,
            convertStingToIntTime : convertStingToIntTime,
            convertToHHMM : convertToHHMM,
            convertTime : convertTime,
            count : count,
            objToArray :objToArray,
            range:range,
            toDatesArr: toDatesArr,
            toUSAtime: toUSAtime,
    
       
        };

        return service;
        //////////////////////

        function $broadcast() {
            return $rootScope.$broadcast.apply($rootScope, arguments);
        }

        function createSearchThrottle(viewmodel, list, filteredList, filter, delay) {
            // After a delay, search a viewmodel's list using
            // a filter function, and return a filteredList.

            // custom delay or use default
            delay = +delay || 300;
            // if only vm and list parameters were passed, set others by naming convention
            if (!filteredList) {
                // assuming list is named sessions, filteredList is filteredSessions
                filteredList = 'filtered' + list[0].toUpperCase() + list.substr(1).toLowerCase(); // string
                // filter function is named sessionFilter
                filter = list + 'Filter'; // function in string form
            }

            // create the filtering function we will call from here
            var filterFn = function () {
                // translates to ...
                // vm.filteredSessions
                //      = vm.sessions.filter(function(item( { returns vm.sessionFilter (item) } );
                viewmodel[filteredList] = viewmodel[list].filter(function (item) {
                    return viewmodel[filter](item);
                });
            };

            return (function () {
                // Wrapped in outer IIFE so we can use closure
                // over filterInputTimeout which references the timeout
                var filterInputTimeout;

                // return what becomes the 'applyFilter' function in the controller
                return function (searchNow) {
                    if (filterInputTimeout) {
                        $timeout.cancel(filterInputTimeout);
                        filterInputTimeout = null;
                    }
                    if (searchNow || !delay) {
                        filterFn();
                    } else {
                        filterInputTimeout = $timeout(filterFn, delay);
                    }
                };
            })();
        }

        function objToArray(obj){
                
            var output = [];
            for (var key in obj) {
              // must create a temp object to set the key using a variable
              var tempObj = {};
              tempObj = obj[key];
              output.push(tempObj);
            }

            return output;
        
        
        }
        
        function debouncedThrottle(key, callback, delay, immediate) {
            // Perform some action (callback) after a delay.
            // Track the callback by key, so if the same callback
            // is issued again, restart the delay.

            var defaultDelay = 1000;
            delay = delay || defaultDelay;
            if (throttles[key]) {
                $timeout.cancel(throttles[key]);
                throttles[key] = undefined;
            }
            if (immediate) {
                callback();
            } else {
                throttles[key] = $timeout(callback, delay);
            }
        }

        function isNumber(val) {
            // negative or positive
            return (/^[-]?\d+$/).test(val);
        }

        function replaceLocationUrlGuidWithId(id) {
            // If the current Url is a Guid, then we replace
            // it with the passed in id. Otherwise, we exit.
            var currentPath = $location.path();
            var slashPos = currentPath.lastIndexOf('/', currentPath.length - 2);
            var currentParameter = currentPath.substring(slashPos - 1);

            if (isNumber(currentParameter)) {
                return;
            }

            var newPath = currentPath.substring(0, slashPos + 1) + id;
            $location.path(newPath);
        }

        function textContains(text, searchText) {
            return text && -1 !== text.toLowerCase().indexOf(searchText.toLowerCase());
        }
        
        function getDateTime(request){
            
            var day = request.day;
            var month = request.month;
            var year = request.year;  
            var date = year+'-'+month+'-'+day;
            var _date = new Date(date);
            
            return _date.getTime();
        
        }
        
        function getTimeArray (){
        
            var time = [{value: '1', type: 'AM'},
                        {value: '2', type: 'AM'},
                        {value: '3', type: 'AM'},
                        {value: '4', type: 'AM'},
                        {value: '5', type: 'AM'},
                        {value: '6', type: 'AM'},
                        {value: '7', type: 'AM'},
                        {value: '8', type: 'AM'},
                        {value: '9', type: 'AM'},
                        {value: '10', type: 'AM'},
                        {value: '11', type: 'AM'},
                        {value: '12', type: 'AM'},
                        {value: '1', type: 'PM'},
                          {value: '2', type: 'PM'},
                          {value: '3', type: 'PM'},
                          {value: '4', type: 'PM'},
                          {value: '5', type: 'PM'},
                          {value: '6', type: 'PM'},
                          {value: '7', type: 'PM'},
                          {value: '8', type: 'PM'},
                          {value: '9', type: 'PM'},
                          {value: '10', type: 'PM'},
                          {value: '11', type: 'PM'},
                          {value: '12', type: 'PM'},  
                       ];
            
            
            return time;
        
        
        
        }
        
               
        function convertStingToIntTime(time){
            if(time == '') return 0;
            var hours = Number(time.match(/^(\d+)/)[1]);
            var minutes = Number(time.match(/:(\d+)/)[1]);

            minutes = minutes / 60;

            time = hours + minutes

            return time;

       }
     
        function convertToHHMM(info) {
          var hrs = parseInt(Number(info));
          var min = Math.round((Number(info)-hrs) * 60)
          if( min != 0)
            return hrs+' hrs '+min+' min';
          else if(min == 0 && hrs ==1)
            return hrs+' hr';
          else
            return hrs+' hrs';

        }
        function convertTime(time){
        if(time == '') return 0;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if(AMPM == "PM" && hours<12) hours = hours+12;
        if(AMPM == "AM" && hours==12) hours = hours-12;

        hours = hours*2;
        if(minutes == 30) hours++;

        return hours;

        }
        
        
        function count(object){
        
            var count = 0;
            
            angular.forEach(object, function(value, key) {
             
                    count++;
            });
            
            return count;
 
        }
        function range(min, max, step){
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) input.push(i);
            return input;
        };

        function toUSAtime(time){

            var amtime;
            if(time > 12)  {
                amtime = time - 12;
                if(amtime == 12) return "12AM";
                return amtime+"PM";
            }
            else {

                if(time == 12) return "12PM";
                return time+"AM";

            }


        }
        
        function toDatesArr(calendar){
            
                var array = {};
             _.forEach(calendar, function(n, key) {
                  _.forEach(calendar[key], function(n1, key1) {
                      _.forEach(calendar[key][key1], function(n2, key2) {
                           _.forEach(calendar[key][key1][key2], function(n3, key3) {

                                array[key3] = n3;

                            });
                    });         
                });
            });
            
            
            return array;
        
        }
    
        
        
    }
})();