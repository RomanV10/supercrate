angular
	.module('app.core')
	.factory('sessionInjector', sessionInjector);

/*@ngInject*/
function sessionInjector(Session, ContractValue, $window, $location, $rootScope) {
	var sessionInjector = {
		request: function (config) {
			config.headers['X-CSRF-Token'] = Session.getToken();
			return config;
		},
		response: function (response) {
			if (_.get(response, 'data.data.status') === ContractValue.forbiddenStatus) {
				if ($rootScope.currentUser.userRole.length == 1 && ~$rootScope.currentUser.userRole[0].indexOf('user')) {
					$window.location.href = `${$location.protocol()}://${$location.host()}:${$location.port()}/account/#/login/`;
				} else {
					$window.location.href = `${$location.protocol()}://${$location.host()}:${$location.port()}/moveBoard/#/login/`;
				}

				return Promise.reject();
			}
			return response;
		}
	};
	return sessionInjector;
}
