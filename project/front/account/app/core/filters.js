(function () {
    'use strict';

    angular
        .module('app.core')
        .filter('rooms', function (RequestPageServices) {
            return function (input) {
                if(input != null)
                return  RequestPageServices.getRoomsString(input);
            };
        })
        .filter('tel', function () {
                return function (tel) {
                    if (!tel) { return ''; }

                    var value = tel.toString().trim().replace(/^\+/, '');

                    if (value.match(/[^0-9]/)) {
                        return tel;
                    }

                    var country, city, number;

                    switch (value.length) {
                        case 10: // +1PPP####### -> C (PPP) ###-####
                            country = 1;
                            city = value.slice(0, 3);
                            number = value.slice(3);
                            break;

                        case 11: // +CPPP####### -> CCC (PP) ###-####
                            country = value[0];
                            city = value.slice(1, 4);
                            number = value.slice(4);
                            break;

                        case 12: // +CCCPP####### -> CCC (PP) ###-####
                            country = value.slice(0, 3);
                            city = value.slice(3, 5);
                            number = value.slice(5);
                            break;

                        default:
                            return tel;
                    }

                    if (country == 1) {
                        country = "";
                    }

                    number = number.slice(0, 3) + '-' + number.slice(3);

                    return (country + " (" + city + ") " + number).trim();
                };
    })
      .filter('reverse', function() {
          return function(items) {
            return items.slice().reverse();
          };
        })
        .filter('currentdate',['$filter',  function($filter) {
            return function() {
                return $filter('date')(new Date(), 'yyyy');
            };
        }])

    .filter('usatime',function(){

        return function(time){

                var amtime;
                if(time > 12)  {
                amtime = time - 12;
                    if(amtime == 12) return "12AM";
                    return amtime+"PM";
                 }
                 else {

                     if(time == 12) return "12PM";
                     return time+"AM";

                }


        }



    })
    .filter('range', function() {
          return function(input, total) {
            total = parseInt(total);

            for (var i=0; i<total; i++) {
              input.push(i);
            }

            return input;
          };
        })

    .filter('decToTime', function() {
          return function(input) {

            var hrs = parseInt(Number(input));
            var min = Math.round((Number(input)-hrs) * 60);
            if(hrs < 1)
                return min + ' min';
            else
                return hrs+' h : '+min+ ' min';

          };
        })
     .filter('ago', function() {
          return function(input) {


                var date = moment.unix(input).utc()
                return moment(date).fromNow();


          };
        })
        .filter('dateRange',function(){
            return function(dates){
                var dateFormat = 'd M, yy';
                if(dates) {
                    var deli = [];
                    var isNumberDates = angular.isNumber(dates);

                    if(!isNumberDates) {
                        deli = dates.split(",");
                        var firstDate = parseInt(deli[0]);
                        if (!(angular.isNumber(firstDate) && !isNaN(firstDate)) || !angular.isNumber(parseInt(deli[1]))) {
                            return dates;
                        }
                    } else {
                        deli[0] = dates;
                        deli[1] = dates;
                    }

                    var c1 = $.datepicker.formatDate( dateFormat, new Date(Math.min(deli[0],deli[1])), {} );
                    var c2 = $.datepicker.formatDate( dateFormat, new Date(Math.max(deli[0],deli[1])), {} );
                    if(c1 != c2) {
                        return c1+' - '+c2;
                    } else {
                        return c1;
                    }

                }

            };


        })
        .filter('momentDate',function(){
            return function(date,format){
                var date = moment.unix(date);
                 return date.format(format)
            };
        })
        .filter("toArray", function(){
            return function(obj) {
                var result = [];
                _.forEach(obj, function(val, key) {
                    result.push(val);
                });
                return result;
            };
        })



})();
