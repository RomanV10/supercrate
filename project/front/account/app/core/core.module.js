(function () {
    'use strict';

    angular.module('app.core', [
        'ui.bootstrap',
        'ngAnimate',
        'ngSanitize',
        'datatables',
         'LocalStorageModule',
        'datatables.columnfilter',
        'blocks.logger',
        'blocks.router',
        'flow',
        'textAngular',
        'ui.switchery',
        'ngMask',
        'oitozero.ngSweetAlert',
        'checklist-model',
        'bootstrapLightbox',
        'ngMarquee',
        'AngularPrint',
        'ngGeolocation'
    ]);
})();