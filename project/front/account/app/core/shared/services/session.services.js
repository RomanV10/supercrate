(function () {
  'use strict';


    var core = angular.module('app.core');

    core.factory('Session', Session);

  Session.$inject = ['$rootScope','config','$cookies'];

  function Session($rootScope,config,$cookies) {
  
      var service = {};
      
    service.create  = create;
    service.get  = get;
    service.destroy = destroy;
    service.getToken = getToken;
    service.saveToken = saveToken;
    return service;
      

      
    
   function saveToken(data) {
  
     $rootScope.token = data;
       $cookies.token = data;

   }
    
  function getToken() {

      if(angular.isDefined($rootScope.token)){
      
          return $rootScope.token;
    
      }
       else 
       {
           if(angular.isDefined($cookies.token)){


               return $cookies.token;

           }
           else
            return 0;
  
       }
  }  
      
      
    function get(sessionId, userId, userRole) {
        
        return   $rootScope.currentUser;
        
    };  
      
    function create (sessionId, userId, userRole) {
        
    this.id = sessionId;
    this.userId = userId;
    this.userRole = userRole;
        
        $rootScope.currentUser = this;
        
    };
      
      
   function destroy() {
       
    this.id = null;
    this.userId = null;
    this.userRole = null;
       
     $rootScope.currentUser = null;
       
  };
      
      
      
  }


})();