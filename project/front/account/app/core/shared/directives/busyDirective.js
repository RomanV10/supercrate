/**
 * @module coreModule
 * @class busyDirective
 */
define('core/shared/directives/busyDirective',["core/coreModule",
  "core/shared/services/resourceStringService"
], function() {
  "use strict";
  angular.module("core").directive("busy", function($timeout, resourceStringService) {

    // so we can cancel our timeout on destroy
    var timeOutPromise = null;
    var BUSY_TIMEOUT = 300000;

    function killTimer() {
      if (timeOutPromise !== null) {
        $timeout.cancel(timeOutPromise);
        timeOutPromise = null;
      }
    }

    function setMessage(scope) {
      if (scope.busy && scope.busy.message) {
        scope.busyMessage = resourceStringService.get(scope.busy.message);
      } else {
        scope.busyMessage = resourceStringService.get("loading");
      }
    }

    function setTimer(scope) {
      // make sure the busy symbol goes away at some point
      if (timeOutPromise === null) {
        timeOutPromise = $timeout(function() {
          scope.showBusy = false;
          timeOutPromise = null;
        }, BUSY_TIMEOUT);
      }
    }

    return {
      restrict: "A",
      replace: true,
      scope: {},
      templateUrl: "core/shared/templates/busyTemplate.html",
      link: function($scope) {

        // handle our begin event
        $scope.$on("busy.begin", function(evt, data) {

          // bail if we are already busy
          if ($scope.showBusy) {
            return;
          }

          $scope.showBusy = true;
          $scope.busy = data;

          setMessage($scope);
          setTimer($scope);
        });

        // set up our subscription for the unimplemented control
        $scope.$on("busy.progress", function(evt, data) {
          // save our new data
          $scope.busy = data;
        });

        // set up our subscription for the unimplemented control
        $scope.$on("busy.end", function(evt, data) {
          $scope.showBusy = false;
          killTimer();
        });

        $scope.$on("destroy", function() {
          killTimer();
        });
      }
    };
  });
});

