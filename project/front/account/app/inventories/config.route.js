(function () {
    'use strict';

    angular
        .module('app.inventories')
        .run(routeConfig);

    routeConfig.$inject = ['routehelper'];
    
    function routeConfig(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [ 
            {
                url: '/inventories',
                config: {
                    title: 'inventories',
                    templateUrl: 'app/inventories/templates/inventories.html',
                }
            }
        ];
    }
})();
