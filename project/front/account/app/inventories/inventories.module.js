(function () {
    'use strict';

    angular.module('app.inventories', ['flow', 'ngDraggable']);

})();