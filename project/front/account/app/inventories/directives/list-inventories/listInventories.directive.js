(function () {
    'use strict';

    angular
        .module('app.inventories')
        .filter('highlight', function () {
            return function (input) {
                if (input.toLowerCase().indexOf('piano') != -1) {
                    var res = "<span class='red-piano'>" + input + "</span>";
                    return res
                }
                return input
            }
        })
        .directive('listInventories', listInventories);

    listInventories.$inject = ['InventoriesServices', '$http', 'config'];

    function listInventories(InventoriesServices, $http, config) {

        var directive = {
            link: link,
            scope: {
                'rooms': '=rooms',
                'listInventories': '=listinventories',
                'removedItems': '=removed',
                'decreaseItems': '=decrease',
                'calculateFilter': '='
            },
            templateUrl: 'app/inventories/directives/list-inventories/listinventories.html',
            restrict: 'A'
        };

        return directive;
    }

    function link(scope, element, attrs) {
        scope.filtredInventories = {};
        scope.removedItems = [];
        scope.decreaseItems = [];
        scope.decreaseItem = decreaseItem;
        scope.onDropComplete = onDropComplete;
        scope.onDragSuccess = onDragSuccess;
        scope.hasRoomBoxes = hasRoomBoxes;
        scope.hasRoomCustoms = hasRoomCustoms;

        function decreaseItem(inventory) {
            if (inventory.count) {
                inventory.count--;
                if (inventory.count == 0) {
                    scope.removedItems.push(inventory);
                    var delIndex = getInventoryIndex(inventory);
                    scope.listInventories.splice(delIndex, 1);
                    if (!_.isEmpty(scope.decreaseItemsLog)) {
                        var ind = _.findIndex(scope.decreaseItems, { id: inventory.id });
                        scope.decreaseItems.splice(ind, 1);
                    }
                } else {
                    var oldCount = _.clone(inventory.count);
                    var objForLog = _.clone(inventory);
                    if (_.isEmpty(scope.decreaseItems)) {
                        objForLog.oldCount = oldCount;
                        scope.decreaseItems.push(objForLog);
                    }
                    var count = 0;
                    _.forEach(scope.decreaseItems, function (el, ind) {
                        if (el.id == inventory.id) {
                            scope.decreaseItems[ind].count = objForLog.count;
                            count++;
                        }
                    });
                    if (!count) {
                        objForLog.oldCount = oldCount;
                        scope.decreaseItems.push(objForLog);
                    }
                }
            }
        }


        function getInventoryIndex(inventory) {
            var index = -1;

            scope.listInventories.some(function (element, ind) {
                if (inventory.id && element.id) {
                    if (inventory.id == element.id && element.rid == inventory.rid) {
                        index = ind;
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    if (customEquals(inventory, element)) {
                        index = ind;
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            return index;
        }

        scope.$watch('listInventories', onChanged, true);

        function onChanged(newValue, oldValue) {
            resetRooms();

            scope.filtredInventories.boxes = [];
            scope.filtredInventories.customs = [];
            scope.filtredInventories.other = []
            angular.forEach(scope.listInventories, function (inventory) {

                if (inventory.fid == 22 && inventory.count > 0) {
                    getRoom(inventory.rid).hasBox = true;
                    scope.filtredInventories.boxes.push(inventory);
                } else if (inventory.fid == 23 && inventory.count > 0) {
                    getRoom(inventory.rid).hasCustom = true;
                    scope.filtredInventories.customs.push(inventory)
                } else if (inventory.count > 0) {
                    scope.filtredInventories.other.push(inventory);
                }
            });

            if (scope.calculateFilter) {
                scope.calculateFilter(scope.listInventories);
            }
        }

        function onDropComplete(data, evt, rid) {
            if (data && data.rid != rid) {
                if (!data.item.id) {
                    data.item.rid = rid;
                } else {
                    var element = getInventory(data.item.id, rid);
                    if (element.inventory) {
                        data.item.count = data.item.count + element.inventory.count;
                        scope.listInventories.splice(element.index, 1);
                        data.item.rid = rid;
                    } else {
                        data.item.rid = rid;
                    }
                }
            }
        }

        function onDragSuccess(data, evt, rid) {
            //data.inventory.rid = rid;
        }

        function customEquals(first, second) {
            return (first.title == second.title
                && first.cf == second.cf && first.rid == second.rid);
        }

        function hasRoomBoxes(room) {
            var has = false;
            scope.listInventories.some(function (element, item) {
                if (room.id == element.rid && element.fid == 22) {
                    has = true;
                    return true;
                } else {
                    return true;
                }
            });

            return has;
        }

        function hasRoomCustoms(room) {
            var has = false;
            scope.listInventories.some(function (element, item) {
                if (room.id == element.rid && element.fid == 23) {
                    has = true;
                    return true;
                } else {
                    return true;
                }
            });

            return has;
        }

        function getInventory(id, rid) {
            var inventory = {};
            scope.listInventories.some(function (element, index) {
                if (id == element.id && rid == element.rid) {
                    inventory.inventory = element;
                    inventory.index = index;
                    return true;
                } else {
                    return false;
                }
            });

            return inventory;
        }

        function getRoom(rid) {
            var room = null;
            scope.rooms.some(function (element, index) {
                if (element.id == rid) {
                    room = element;
                    return true;
                } else {
                    return false;
                }
            });
            return room;
        }

        function resetRooms() {
            angular.forEach(scope.rooms, function (element, index) {
                element.hasBox = false;
                element.hasCustom = false;
            });
        }
    }

})();

