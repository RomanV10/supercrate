(function () {

    angular
        .module('app.inventories')

    .factory('EditInventoryServices', EditInventoryServices);

    EditInventoryServices.$inject = ['$rootScope','$uibModal','$filter','common','logger','config', '$q'];

    function EditInventoryServices($rootScope,$uibModal,$filter,common,logger,config, $q) {

        var service = {};
        service.openModal = openModal;
        $rootScope.modalInstances = [];

        return service;

        function openModal(inventory) {

            var deferred = $q.defer();

            var modalInstance = $uibModal.open({
                        templateUrl: 'app/inventories/templates/editInventoryTemplate.html',
                        controller: ModalInstanceCtrl,
                        size: 'md',
                        windowClass:'edit-inventory-modal',
                        resolve: {
                            inventory: function () {
                              return inventory;
                            }
                        }
            });

            angular.forEach($rootScope.modalInstances, function(inst, id) {
                        inst.dismiss('cancel');
            });

            $rootScope.modalInstances = [];
            $rootScope.modalInstances.push(modalInstance);

           deferred.resolve(modalInstance.result);

            return deferred.promise;
         };

		/* @ngInject */
     function ModalInstanceCtrl($scope, $uibModalInstance, inventory, InventoriesServices) {

         $scope.isAdd = inventory.id == null;

         $scope.title = $scope.isAdd ? "Add new inventory" : "Edit inventory";
         loadFilters();
         $scope.inventory = inventory;

         $scope.imageStrings = [];
         $scope.result = {inventory: $scope.inventory, add: false, remove: false, update: false };

         $scope.processFiles = function(files){
             angular.forEach(files, function(flowFile, i){
                 var fileReader = new FileReader();
                 fileReader.onload = function (event) {
                     var uri = event.target.result;
                     $scope.imageStrings[i] = uri;
                 };
                 fileReader.readAsDataURL(flowFile.file);
             });
         };

         $scope.cancel = function() {
				$uibModalInstance.dismiss('cancel');
         };

         $scope.saveInventory = saveInventory;
         $scope.deleteInventory = deleteInventory;

         //changed
         function saveInventory(){
             InventoriesServices.addInventory($scope.inventory).then( function(inventory){
                $scope.inventory = inventory;
             });

             $scope.result.add = $scope.isAdd;
             $scope.result.update = !$scope.isAdd;
				$uibModalInstance.close($scope.result);
         }

         //changed
         function deleteInventory(){
             InventoriesServices.deleteInventory($scope.inventory);
             $scope.result.remove = true;
				$uibModalInstance.close($scope.result);
         }

         function loadFilters(){
            InventoriesServices.getFilters().then(function(filters){
                $scope.filters = filters;
            }, function(reason){
                logger.error(reason, reason, "Error");
            });
         }
       }
 }})();
