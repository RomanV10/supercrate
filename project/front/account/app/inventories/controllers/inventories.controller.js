(function () {
    'use strict';

    angular
        .module('app.inventories')
        .controller('InventoriesController', InventoriesController)

    InventoriesController.$inject = ['InventoriesServices', 'ContractService', 'logger','$browser', '$location','$scope', '$uibModal', '$rootScope', 'CalculatorServices', 'SweetAlert', 'ContractFactory', 'datacontext', 'RequestServices', 'Session'];

    function InventoriesController(InventoriesServices, ContractService, logger, $browser,$location, $scope, $uibModal, $rootScope, CalculatorServices, SweetAlert, ContractFactory, datacontext, RequestServices, Session) {

        var vm = this;
		let session = Session.get();
        vm.busy = true;
        vm.fieldData = datacontext.getFieldData();
        vm.basicSettings = angular.fromJson(vm.fieldData.basicsettings);

        _.forEach(vm.amoutValuation, function(item, ind){
            vm.amoutValuation[ind] = Number(item);
        });
        vm.init = init;
        vm.saveListInventories = saveListInventories;
        vm.saveAddListInventories = saveAddListInventories;
        vm.listInventories = [];
        vm.busyInventory = false;
        vm.totalInv = 0;
        vm.decreaseItemsLog = [];
        vm.increaseItemsLog = [];
        vm.removedItemsLog = [];
        vm.removedItems = [];
        vm.decreaseItems = [];
        vm.itemsLog = [];
        vm.fieldName = '';
        var request = {};

        function calculateValuation(data) {
        	return 0;
            var valuation = {};
            valuation.amount = new Object();
            valuation.amount.deductible = 0;
            valuation.charge = 0;
            valuation.liability = 1;
            var dataSend= {};
            var additionalInv = {};
            additionalInv.cfs = 0;
            var charge = 0;
            var weight = 0;
            if(vm.request.request_all_data.additional_inventory)
                additionalInv = InventoriesServices.calculateTotals(vm.request.request_all_data.additional_inventory);
            if(angular.isDefined(data.inventory_weight)){
                weight = (data.inventory_weight.cfs + additionalInv.cfs) * 7;
            }else if(angular.isDefined(data.total_weight)){
                if(angular.isDefined(data.total_weight.weight))
                    weight = data.total_weight.weight*7
            }else{
                if(angular.isDefined(data.total_weight.cfs))
                    weight = (data.total_weight.cfs + additionalInv.cfs) *7
            }
            valuation.weight = weight;
            valuation.amount.liability = weight*6;
            if(data.request_all_data.valuation){
                if(data.request_all_data.valuation.deductable_level){
                    valuation.liability == '1';
                    charge = data.request_all_data.valuation.deductable_level;
                }else{
                    valuation.liability == '0';
                }
                if(data.request_all_data.valuation.selected.valuation_charge){
                    valuation.charge = data.request_all_data.valuation.selected.valuation_charge;
                }
            }
            var valCharges = Object.keys(vm.valuationCharges);
            var searchValuation = Object.keys(vm.valuationCharges);
            var chargeItem = 0;
            for(var i in valCharges){
                var min = _.min(searchValuation);
                if(valuation.amount.liability < min){
                    chargeItem = min;
                    continue;
                }else{
                    searchValuation.splice(searchValuation.indexOf(min), 1);
                }
            }
            vm.reqCharges = new Object();
            if(angular.isUndefined(vm.valuationCharges[chargeItem])){
                return false;
            }
            vm.reqCharges[chargeItem] = angular.copy(vm.valuationCharges[chargeItem]);
            var perc = vm.reqCharges[chargeItem][vm.amoutValuation.indexOf(Number(charge))];
            valuation.charge = parseFloat((perc * Number(valuation.amount.liability) / 100).toFixed(2));
            if(valuation.liability == 1){
                dataSend.deductable_level=  parseFloat((Number(charge)).toFixed(2));
                dataSend.valuation_charge = parseFloat((Number(valuation.charge)).toFixed(2));
            }
            if(valuation.liability == 0){
                dataSend.deductable_level=  0;
                dataSend.valuation_charge = 0;
            }
            return dataSend;
        }

        function reCalcValuation() {
            var data = {
                valuationOld: {},
                valuationNew: {},
            };
            if(vm.request.request_all_data.invoice) {
                if (vm.request.request_all_data.invoice.request_all_data) {
                    data.valuationNew = calculateValuation(vm.request);
                    if (data.valuationNew == false) {
                        return false;
                    }
                    data.valuationOld = vm.request.request_all_data.invoice.request_all_data.valuation;
                    return data;
                } else {
                    vm.request.request_all_data.invoice.request_all_data = {};
                    data.valuationNew = calculateValuation(vm.request);
                    if (data.valuationNew == false) {
                        return false;
                    }
                    data.valuationOld = vm.request.request_all_data.invoice.request_all_data.valuation;
                    return data;
                }
            }else{
                vm.request.request_all_data.invoice = {};
                vm.request.request_all_data.invoice.request_all_data = vm.request.request_all_data;
                data.valuationNew = calculateValuation(vm.request);
                if (data.valuationNew == false) {
                    return false;
                }
                data.valuationOld = vm.request.request_all_data.invoice.request_all_data.valuation;
                return data;
            }
        }

        function saveAddListInventories(type, list ){
            vm.totalInv = 0;
            if (list) {
                vm.listInventories = list;
            }
            vm.request.inventory_weight = InventoriesServices.calculateTotals(vm.request.inventory.inventory_list);
            var inventory_weight = InventoriesServices.calculateTotals(vm.listInventories);
            vm.totalInv = parseFloat((inventory_weight.cfs * Number(vm.basicSettings.storage_rate.min)).toFixed(2));
            if (vm.request.service_type.raw == 2) {
                if(!type) {
	                ContractService.saveContractRequestData(vm.request);
                    $rootScope.$broadcast('add.inventory.added', {
                        inventory: vm.listInventories,
                        weight: inventory_weight,
                        fieldName: vm.fieldName
                    });
                    $rootScope.$broadcast('add.inventory', vm.request.request_all_data);
                }
            } else if (vm.request.service_type.raw != 2) {
                vm.request.request_all_data.inventory = vm.listInventories;
                ContractService.saveReqData($scope.request.nid, vm.request.request_all_data);
                if(!type)
                    $rootScope.$broadcast('add.inventory.added.moving', {inventory: vm.listInventories, weight: inventory_weight});
            }
            var details = [];
            if (!_.isEmpty(vm.listInventories))
                _.forEach(vm.listInventories, function (item) {
                    var activity = '';
                    if (item.count < item.oldCount && item.count != 0) {
                        activity = 'decreased';
                    } else if (item.count > item.oldCount && item.count != 0 && item.oldCount != 0) {
                        activity = 'increased';
                    } else if (item.count == 0 && item.oldCount > 0) {
                        activity = 'removed';
                    } else if ((item.oldCount == 0 || !item.oldCount) && item.count > 0) {
                        activity = 'added';
                    }
                    if (activity != '') {
                        var msg = {
                            text: 'Inventory ' + item.title + ' was ' + activity + '.',
                            from: item.oldCount || "0",
                            to: item.count || "0"

                        };
                        details.push(msg);
                    }
                });

            if (!_.isEmpty(details)) {
                RequestServices.sendLogs(details, 'Request was updated', vm.rid, 'MOVEREQUEST');
            }
        }

        function saveListInventories(isCustom){
            vm.busyInventory = true;

            var req = $scope.$parent.request;
            var isInventoryChange = false;
            if ((req.service_type.raw == 2
                || req.service_type.raw == 6)
                && !_.isEqual(req.storage_id, 0)) {

                InventoriesServices.saveListInventories(req.storage_id, vm.listInventories);
            }
            var details = [];
            if (!_.isEmpty(vm.decreaseItemsLog)) {
                _.forEach(vm.decreaseItemsLog, function (item) {
                    var msg = {
                        text: 'Inventory ' + item.title + ' was decreased.',
                        from: item.oldCount,
                        to: item.count
                    };
                    details.push(msg);
                });
            }
            if (!_.isEmpty(vm.increaseItemsLog)) {
                _.forEach(vm.increaseItemsLog, function (item) {
                    var msg = {
                        text: 'Inventory ' + item.title + ' was increased.',
                        to: item.count
                    };
                    details.push(msg);
                });
            }
            if (!_.isEmpty(vm.removedItemsLog)) {
                _.forEach(vm.removedItemsLog, function (item) {
                    var msg = {
                        text: 'Inventory ' + item.title + ' was removed.'
                    };
                    details.push(msg);
                });
            }
            if (!_.isEmpty(vm.removedItems)) {
                _.forEach(vm.removedItems, function (item) {
                    var msg = {
                        text: 'Inventory ' + item.title + ' was removed.'
                    };
                    details.push(msg);
                });
            }
            if (!_.isEmpty(vm.decreaseItems)) {
                _.forEach(vm.decreaseItems, function (item) {
                    var msg = {
                        text: 'Inventory ' + item.title + ' was decreased.',
                        from: item.oldCount,
                        to: item.count
                    };
                    details.push(msg);
                });
            }
	        if (!_.isEmpty(vm.listInventories)) {
		        let removedItem = [];

		        _.forEach(vm.listInventories, function (item) {
			        var activity = '';
			        if (item.count < item.oldCount && item.count != 0) {
				        activity = 'decreased';
			        } else if (item.count > item.oldCount && item.count != 0 && item.oldCount != 0) {
				        activity = 'increased';
			        } else if (item.count == 0 && item.oldCount > 0) {
				        activity = 'removed';
				        removedItem.push(angular.copy(item));
			        } else if ((item.oldCount == 0 || !item.oldCount) && item.count > 0) {
				        activity = 'added';
			        }

			        if (activity != '') {
				        isInventoryChange = true;
				        var msg = {
					        text: 'Inventory ' + item.title + ' was ' + activity + '.',
							from: item.oldCount || "0",
							to: item.count || "0"

				        };
				        details.push(msg);
			        }
		        });

		        vm.listInventories = vm.listInventories.filter(item => !_.find(removedItem, {id: item.id}));
	        }
            if (isInventoryChange) {
                // RequestServices.sendLogs(details, 'Request updated', vm.rid, 'MOVEREQUEST');
            }
            InventoriesServices.saveListInventories(vm.rid, vm.listInventories);
            vm.busyInventory = false;
            $scope.$parent.request.inventory.inventory_list = vm.listInventories;
            $scope.$parent.request.field_useweighttype.value = 2;
            $scope.request.inventory_weight = InventoriesServices.calculateTotals(vm.listInventories);
            $rootScope.$broadcast('inventory_update', details)
        }

        function init(rid, type, field) {
            vm.fieldName = field;
            loadRooms(rid);
            vm.request = rid;
            request = _.clone(rid);
            vm.rid = rid.nid;
            if (type == 'contract') {
                if (rid.service_type.raw == 2) {
                    if (rid.request_all_data) {
                        if(vm.fieldName == 'addInventoryMoving') {
                            if (rid.request_all_data.additional_inventory == null || !_.isArray(rid.request_all_data.additional_inventory))
                                vm.listInventories = [];
                            else
                                vm.listInventories = rid.request_all_data.additional_inventory;
                        }else if(vm.fieldName == 'secondAddInventoryMoving'){
                            if (rid.request_all_data.second_additional_inventory == null || !_.isArray(rid.request_all_data.second_additional_inventory) || !rid.request_all_data.second_additional_inventory)
                                vm.listInventories = [];
                            else
                                vm.listInventories = rid.request_all_data.second_additional_inventory;
                        }
                    }
                } else if (rid.service_type.raw != 2) {
                    if (rid.request_all_data.inventory == null || !_.isArray(rid.request_all_data.inventory)) vm.listInventories = rid.inventory.inventory_list || [];
                    else vm.listInventories = rid.request_all_data.inventory;
                }
                vm.busy = false;
            } else {
                if (rid.request_all_data)
                    if (rid.request_all_data.additional_inventory == null || !_.isArray(rid.request_all_data.additional_inventory))

                        vm.listInventories = [];
                    else
                        vm.listInventories = rid.request_all_data.additional_inventory;
                vm.busy = false;
            }
        }

        function loadRooms(rid){
            vm.rooms = InventoriesServices.getRooms(rid);
        }

    }

})();

