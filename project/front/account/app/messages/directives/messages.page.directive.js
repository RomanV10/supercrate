(function () {
    'use strict';

    angular
        .module('app.messages')
        .directive('messagesPage', messagesPage);

    messagesPage.$inject = ['MessagesServices','Session','common','logger'];

 
    function messagesPage (MessagesServices,Session,common,logger) {
        var directive = {
           link: link,
            scope: {
                'requests': '=requests',
            },
            templateUrl: 'app/messages/directives/messages.page.template.html',
            restrict: 'A'
        };
        return directive;
    
    
    
        function link(scope, element, attrs) {
            
            
             scope.$watch('requests', function() {

                        if( Object.keys(scope.requests).length) {

                        scope.active = common.objToArray(scope.requests)[0].nid;

                    }
                    else {

                        scope.active = 0;

                    }
                 });
          
            
    
        }
        
        

    
    
    }
})();