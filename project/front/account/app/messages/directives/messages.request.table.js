(function () {
    'use strict';

    angular
        .module('app.messages')
        .directive('ccMessagesDatatable', ccMessagesDatatable);

    
    ccMessagesDatatable.$inject = ['$uibModal','common','DTOptionsBuilder','DTColumnBuilder','RequestServices','EditRequestServices','logger','MessagesServices'];
    
    function ccMessagesDatatable ($uibModal,common,DTOptionsBuilder,DTColumnBuilder,RequestServices,EditRequestServices,logger,MessagesServices) {
        var directive = {
            scope: {
                'requests': '=requests',
                'active' : '=active'
               
            },
            controller: requestsController,
            replace: true,
            compile: compile,
            templateUrl: 'app/messages/templates/messagesTable.html',
            restrict: 'A'
        };
        return directive;
    
        function compile(tElement, tAttrs, transclude) {
            
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                    
                    scope.dtinstance = {};
                
                    scope.dtOptions = DTOptionsBuilder.newOptions()
                    .withPaginationType('simple')
                    .withDisplayLength(100);
    
                },
          
        
            
         }
         
        }
        
      
        
        function requestsController($scope,$uibModal){
        
                $scope.nid = 0;
                 $scope.$watch('requests', function() {
                 
                       if( Object.keys($scope.requests).length) {
                
                            $scope.nid = common.objToArray($scope.requests)[0].nid;
                
                        }
                     
                 //   $scope.showComments(request);
               
                 });
              
                $scope.status = 0;
                $scope.busy = false;
                //HEADER DATEPICKER SETTINGS
               
                $scope.requestEditModal = requestEditModal;
         
                function requestEditModal(request){
            
                    
                        //GET SAME DATE REQUESTS 07/31/2015
                        var date = request.date.value;
                        $scope.busy = true;
                    
                        var promise = RequestServices.getDateRequests(date);
                 
                          promise.then(function(data) {

                               $scope.busy = false;
                              var requests = data;
                               EditRequestServices.openModal(request,requests);


                          }, function(reason) {

                                $scope.busy = false;
                                 logger.error(reason, reason, "Error");

                          });

                       

                };
            
             $scope.showComments = function(request){
             
                $scope.nid = request.nid;
                $scope.active =  request.nid;
   
             }

        
        
        
        
        
        }
        
    
    
    }
})();
