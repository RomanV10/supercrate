import './messages_thread.styl';

(function () {
    'use strict';

    angular
        .module('app.messages')
        .directive('messageThread', messageThread);

    messageThread.$inject = ['MessagesServices', 'Session', 'common', 'logger', 'RequestServices', 'datacontext', 'apiService', 'moveBoardApi'];


    function messageThread(MessagesServices, Session, common, logger, RequestServices, datacontext, apiService, moveBoardApi) {
        var directive = {
            link: link,
            scope: {
                'nid': '=nid',
                'type': '@',
                'manager': '=',
                'reloadMessages': '=onReloadMessages'
            },
            templateUrl: 'app/messages/directives/messages_thread.html?2',
            restrict: 'A'
        };
        return directive;


        function link(scope, element, attrs) {
            let fieldData = datacontext.getFieldData(),
                enums = fieldData.enums;
            scope.busy = true;
            scope.loadMessages = loadMessages;
            scope.messages = [];
            scope.messagesObj = {};
            scope.reloadMessages = loadMessages;
            scope.isEmptyHTMLString = isEmptyHTMLString;

            var commentsFirst = true;

            scope.$watch('nid', function () {
                scope.busy = true;
                if (scope.nid)
                    loadMessages(scope.nid);

            });

            common.$timeout(function () {
                loadMessages(scope.nid);
            }, 1500);

            function loadMessages(nid) {

                var promise = MessagesServices.getMessagesThread(nid);


                promise.then(function (data) {

                    scope.busy = false;

                    var diff = common.diff(data, scope.messagesObj);
                    if (commentsFirst) {
                        scope.messages = common.objToArray(data);
                        scope.messagesObj = data;
                        commentsFirst = false;
                    }
                    else if (!commentsFirst && diff.length) {
                        scope.messages.push(data[diff]);
                        scope.messagesObj[diff] = data[diff];
                    }

                    var newmessages = 0;
                    if (angular.isDefined(scope.$parent.$parent.vm.request)) {
                        angular.forEach(data, function (m, cid) {

                            if (!m.read && m.admin) {
                                newmessages++;
                            }
                        });


                    }
                    scope.$parent.$parent.vm.messages = data;
                    scope.$parent.$parent.vm.newMessages = newmessages;


                }, function (reason) {

                    scope.busy = false;
                    logger.error(reason, reason, "Error");

                });


            }

            scope.addMessage = function () {
                if (isEmptyHTMLString(scope.message)) return;

                var format = 'full_html';
                if (angular.isDefined(scope.message)) {
                    var promise = MessagesServices.addMessage(scope.nid, scope.message, format);
                }
                else {
                    return;
                }

                var session = Session.get();
                var admin = Session.isAdmin();
                var message = {
                    comment_body: angular.copy(scope.message),
                    admin: admin,
                    user_first_name: session.userId.field_user_first_name,
                    user_last_name: session.userId.field_user_last_name,
                    created: moment().unix()
                };
                scope.messages.push(message);
                var msg = {
                    html: angular.copy(scope.message)
                };
                var arr = [];
                arr.push(msg);
                scope.message = "";
                promise.then(function (data) {
                    sendNotification(message);
                    RequestServices.sendLogs(arr, 'Message was successfully sent', scope.nid, scope.type);
                    var cid = data[0];
                    scope.messagesObj[cid] = {};

                }, function (reason) {
                    RequestServices.sendLogs(arr, 'Message was not sent', scope.nid, scope.type);
                    //scope.busy = false;
                    logger.error(reason, reason, "Error");

                });

            }


            scope.readMessage = function (message) {

                if (!message.read)
                    MessagesServices.setCommentReaded(message.cid);

            }

            function sendNotification(message) {
                let regex = /(<([^>]+)>)/ig;
                let data = {
                    node: {nid: scope.nid},
                    notification: {
                        text: 'New message: ' + message.comment_body.replace(/<\/p>/g, '\n').replace(regex, '')
                    }
                };
                let dataNotify = {
                    type: enums.notification_types.CUSTOMER_SEND_MESSAGE,
                    data: data,
                    entity_id: scope.nid,
                    entity_type: enums.entities.MOVEREQUEST
                };
                apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
            }

	        function isEmptyHTMLString(text) {
		        text = text || '';

		        return text.replace(/\s|&nbsp;/g, '')
				        .replace(/<(?:.|\n)*?>/gm, '').length == 0;
	        }
        }


    }
})();
