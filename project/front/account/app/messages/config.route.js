(function () {
    'use strict';

    angular.module('move.requests')
        .run(routeConfig);

    routeConfig.$inject = ['routehelper'];
    /* @ngInject */
    function routeConfig(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/messages',
                config: {
                    templateUrl: 'app/messages/templates/messages-page.html',
                    title: 'messages',
                }
            },
        ];
    }
})();