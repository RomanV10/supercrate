(function () {
    'use strict';

    angular
        .module('app.messages')
    
     .controller('MessagesController', function (MessagesServices) {

             // Inititate the promise tracker to track form submissions.

             var vm = this; 
             vm.busy = true;
             vm.unreadComments = [];
             activate();
 
        
         function activate() {

                        var promise = MessagesServices.getAllMessages();

                          promise.then(function(data) {

                             vm.busy = false;
                             vm.unreadComments = data;

                          }, function(reason) {

                                  vm.busy = false;
                                 logger.error(reason, reason, "Error");

                          });
             }


    })

    


})();
    