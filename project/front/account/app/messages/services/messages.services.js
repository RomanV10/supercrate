(function () {

    angular
        .module('app.messages')

    .factory('MessagesServices',MessagesServices);

    MessagesServices.$inject = ['$http','$rootScope','$q','config','RequestServices'];

    function MessagesServices($http,$rootScope,$q,config,RequestServices) {

        var service = {};

        service.getMessagesThread = getMessagesThread;
        service.addMessage = addMessage;
        service.getUnreadCount = getUnreadCount;
        service.getUnreadComments = getUnreadComments;
        service.setCommentReaded = setCommentReaded;
        service.getAllMessages = getAllMessages;

        return service;

function setCommentReaded(nid,callback) {

            var deferred = $q.defer();



            $http
                        .post(config.serverUrl+'server/move_request_comments/set_read_comment/'+nid)
                        .success(function(data, status, headers, config) {

                                    //data.success = true;
                                    deferred.resolve(data);

                                  })
                         .error(function(data, status, headers, config) {

                                    // data.success = false;
                                     deferred.reject(data);

                                });



            return deferred.promise;

        }

function getMessagesThread(nid,callback) {

            var deferred = $q.defer();



            $http
                        .get(config.serverUrl+'server/move_request_comments/'+nid)
                        .success(function(data, status, headers, config) {

                                    //data.success = true;
                                    deferred.resolve(data);

                                  })
                         .error(function(data, status, headers, config) {

                                    // data.success = false;
                                     deferred.reject(data);

                                });



            return deferred.promise;

        }


function addMessage(nid,message,format,callback) {


            var deferred = $q.defer();

        /*        var message = {
                    text: "What a relief to find such a great service. the crew came to my apartment right on time, quickly moved my furniture to their track, brought it to my new place, put them into the rooms as i requested, and left. No damage to the furniture, no missing things, no damages to the walls!",
                    name: "Mikel",
                    admin :true,

                }
        */
            var data = {};
            data.data = {
                message:message,
                nid: nid,
                format:format,
			}



            $http.post(config.serverUrl+'server/move_request_comments',data)
            .success(function(data, status, headers, config) {


                                    deferred.resolve(data);

            })
            .error(function(data, status, headers, config) {

                                    // data.success = false;
                                     deferred.reject(data);

             });

            return deferred.promise;

}



function getUnreadCount(uid,callback) {
              var deferred = $q.defer();

            $http.post(config.serverUrl+'server/move_request_comments/unread_count')
            .success(function(data, status, headers, config) {


                                    deferred.resolve(data);


            })
            .error(function(data, status, headers, config) {


                                     deferred.reject(data);

             });

            return deferred.promise;

}

function getUnreadComments(uid,callback) {
    var deferred = $q.defer();

    $http.post(config.serverUrl+'server/move_request_comments/get_new_comments')
        .success(function(data, status, headers, config) {

            let nids = getRequestNids(data);

            if(!_.isEmpty(nids)) {
                var promise = RequestServices.getRequestsByNid(nids);
                promise.then(function(requests) {
                    deferred.resolve(requests);
                });

            }
            else {
                deferred.reject(data);
            }
        })
        .error(function(data, status, headers, config) {
            deferred.reject(data);
        });

    return deferred.promise;
}

function getRequestNids(requests) {
    let result = [];

    if (_.isArray(requests)
        && !_.isEmpty(requests)) {

        angular.forEach(requests, function (request, index) {
            result.push(request.nid);
        })
    }

    return result;
}

function getAllMessages(callback) {
              var deferred = $q.defer();

            $http.post(config.serverUrl+'server/move_request_comments/admin_all_comments')
            .success(function(data, status, headers, config) {

                        var promise = RequestServices.getRequestsByNid(data);
                         promise.then(function(requests) {

                            deferred.resolve(requests);

                         });





            })
            .error(function(data, status, headers, config) {

                                    // data.success = false;
                                     deferred.reject(data);

             });

            return deferred.promise;

}


 }})();
