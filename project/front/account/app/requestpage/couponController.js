angular
	.module('app.request')
	.controller('CouponController', CouponController);

/* @ngInject */
function CouponController($scope, $rootScope, $routeParams, MoveCouponService, logger, RequestServices, config, datacontext, SweetAlert, CalculatorServices, PaymentServices, $uibModal, Session) {
	var vm = this;
	var hash = $routeParams.id;
	$scope.config = config;
	vm.coupon = {};
	vm.fieldData = datacontext.getFieldData();
	vm.basicSettings = angular.fromJson(vm.fieldData.basicsettings);
	vm.company_logo_url = vm.basicSettings.company_logo_url;
	vm.entitytypes = vm.fieldData.enums.entities;
	vm.busy = true;
	vm.entitytype = vm.fieldData.enums.entities.COUPON;
	vm.lodashEmpty = _.isEmpty;
	vm.requestNid = Session.getRequestNid();

	vm.couponPayment = couponPayment;

	MoveCouponService.getCouponByHash(hash).then(function (response) {
		vm.coupon = response;
		vm.busy = false;
	}, function (reason) {
		vm.busy = false;
		logger.error(reason, reason, "Error");
	});

	function couponPayment() {
		var data = {};
		data.total = vm.coupon.price;
		data.discount = vm.coupon.discount;
		data.hash = vm.coupon.hash;
		data.nid = vm.coupon.id;
		data.payment_flag = 'coupon';
		data.uid = '';
		data.name = '';
		data.phone = '';
		data.email = '';
		data.zip = '';
		data.requestNid = vm.requestNid;
		data.receiptDescription = '$' + vm.coupon.price + ' for $' + vm.coupon.discount + ' Deal at ' + vm.basicSettings.company_name;

		let currentUser = _.get($rootScope, 'currentUser.userId');

		if (currentUser) {
			data.name = currentUser.field_user_first_name + ' ' + currentUser.field_user_last_name;
			data.uid = currentUser.uid;
			data.phone = currentUser.field_primary_phone;
			data.email = currentUser.mail;
		}

		PaymentServices.openAuthPaymentModal(data, undefined, undefined, vm.entitytype);
	}

	$scope.$on('coupon.purchased', function () {
		vm.coupon.coupons_left = Number(vm.coupon.coupons_left) - 1;
	});
}
