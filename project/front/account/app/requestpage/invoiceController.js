'use strict';

import "./templates/invoice.styl";

angular
	.module('app.request')
	.controller('InvoiceController', InvoiceController);

/* @ngInject */
function InvoiceController($scope, $rootScope, $routeParams, logger, RequestServices, RequestPageServices,
          common, Session, config, datacontext,
          $location, SweetAlert, CalculatorServices, PaymentServices, $uibModal, InvoiceServices, StorageService, Raven) {

	// Inititate the promise tracker to track form submissions.
	var vm = this;
	var hash = $routeParams.id;
	$scope.config = config;
	vm.admin = false;
	vm.invoice = {};
	vm.showNotes = false;
	vm.packings = [];
	vm.valuationCharge = 0;
	vm.fuelSurcharge = 0;
	vm.fieldData = datacontext.getFieldData();
	vm.basicSettings = angular.fromJson(vm.fieldData.basicsettings);
	vm.company_logo_url = vm.basicSettings.company_logo_url;
	vm.invoiceFlags = vm.fieldData.enums.invoice_flags;
	vm.invoicesFlagsArray = [];
	vm.busy = true;
	vm.invoice_notavalable = false;
	vm.payments = [];
	vm.invoices = [];
	vm.prepaidCredit = 0;
	vm.entitytype = vm.fieldData.enums.entities.MOVEREQUEST;
	vm.lodashEmpty = _.isEmpty;
	vm.allInvoices = [];
	vm.invoiceId = '';
	vm.closeBalance = false;
	vm.closeBalanceStorage = false;
	vm.invoicePaid = false;

	vm.Abs = Math.abs;

	angular.forEach(vm.invoiceFlags, function (id, flagname) {
		vm.invoicesFlagsArray[id] = flagname;
	});

	InvoiceServices.hashInvoice(hash).then(function (response) {
		if (_.isEmpty(response)) {
			Raven.captureException(`InvoiceController: get empty invoice by hash: ${hash}`);

			return;
		}

		vm.invoice = response.data;
		vm.invoiceId = response.id;

		if (vm.invoice.storage_invoice)
			vm.entitytype = vm.fieldData.enums.entities.STORAGEREQUEST;

		vm.invoicesAnother = response.another_invoices || [];

		_.forEachRight(vm.invoicesAnother, function (invoice, ind) {
			if (invoice.data.flag == vm.invoiceFlags.PAID) {
				if (_.isArray(vm.invoicesAnother)) vm.invoicesAnother.splice(ind, 1);
				else delete vm.invoicesAnother[ind];
			}
			else
				vm.invoicesAnother[ind].selected = true;
		});

		vm.allInvoices = angular.copy(vm.invoicesAnother);

		if (vm.entitytype == vm.fieldData.enums.entities.STORAGEREQUEST) {
			vm.invoicesAnother = [];
			vm.closeBalance = response.closeBalance;
			vm.closeBalanceStorage = response.closeBalance;
		}

		vm.totalBalance = response.balance;
		if (vm.invoice.entity_id == vm.fieldData.enums.entities.LDCARRIER && vm.invoice.partially_paid && vm.invoice.partially_paid != vm.invoice.totalInvoice) {
			vm.totalBalance = parseFloat((vm.invoice.totalInvoice - vm.invoice.partially_paid).toFixed(2));
		}
		if (vm.invoice.flag == vm.invoiceFlags.PAID)
			vm.invoicesAnother = {};
		if (vm.invoice == false) {
			vm.invoice_notavalable = true;
			return false;
		}
		vm.invoice.id = response.id;

		vm.busy = false;
		if (vm.totalBalance < 0 && vm.invoice.flag != vm.invoiceFlags.PAID) {
			if (vm.calculateGrandTotal() > Math.abs(vm.totalBalance)) {
				vm.prepaidCredit = Math.abs(vm.totalBalance);
			}
		} else if (vm.totalBalance >= 0 && vm.invoice.flag != vm.invoiceFlags.PAID && vm.payments.length) {
			vm.invoice.flag = vm.invoiceFlags.PAID;
			var data = {};
			data.data = vm.invoice;
			var id = vm.invoice.id;
			InvoiceServices.editInvoice(id, data);
		}
		if (vm.invoice.flag == vm.invoiceFlags.PAID) {
			vm.invoicePaid = true;
			if (vm.invoice.entity_type == vm.fieldData.enums.entities.LDCARRIER
				&& vm.invoice.partially_paid && vm.invoice.partially_paid != vm.invoice.totalInvoice) {
				vm.invoicePaid = false;
			}
		}


		//Update Invoice check flag set flag to viewed
		setViewedInvoiceFlag();
	}, function (reason) {
		vm.busy = false;
		logger.error(reason, reason, "Error");
	});

	function setViewedInvoiceFlag() {
		if (vm.invoice.flag != vm.invoiceFlags.PAID && vm.invoice.flag != vm.invoiceFlags.VIEWED) {
			vm.invoice.flag = vm.invoiceFlags.VIEWED;
			var data = {};
			var id = vm.invoice.id;
			data.data = vm.invoice;
			InvoiceServices.editInvoice(id, data);
		}
	}

	// Calculates the sub total of the invoice
	vm.invoiceSubTotal = function () {
		var total = 0.00;
		angular.forEach(vm.invoice.charges, function (charge, key) {
			total += (parseFloat(charge.qty >= 0 ? charge.qty : charge.quantity) * parseFloat(charge.cost >= 0 ? charge.cost : (charge.unit_cost || charge.cost)));
		});
		return Number(total.toFixed(2));
	};

	vm.invoiceSubTotalAddInv = function (charges) {
		var total = 0.00;
		_.forEach(charges, function (charge, key) {
			total += (parseFloat(charge.qty >= 0 ? charge.qty : charge.quantity) * parseFloat(charge.cost >= 0 ? charge.cost : (charge.unit_cost || charge.cost)));
		});
		return Number(total.toFixed(2));
	};
	vm.invoiceTotalAddInv = function (invoice) {
		var total = vm.invoiceSubTotalAddInv(invoice.charges);
		var grandTotal = total - total * invoice.discount / 100 + (total - total * invoice.discount / 100) * invoice.tax / 100;
		return Number(grandTotal.toFixed(2));
	};

	vm.calculateGrandTotalBalance = function (type) {
		if (vm.invoice.entity_type == vm.fieldData.enums.entities.LDCARRIER && vm.invoice.partially_paid) {
			return parseFloat((vm.invoice.totalInvoice - vm.invoice.partially_paid).toFixed(2));
		}
		if (type == 'paid')
			return Number((vm.totalAdditionalInvoices() + vm.invoiceSubTotal() - vm.invoiceSubTotal() * vm.invoice.discount / 100 + (vm.invoiceSubTotal() - vm.invoiceSubTotal() * vm.invoice.discount / 100) * vm.invoice.tax / 100).toFixed(2));
		if (vm.invoice.flag == vm.invoiceFlags.PAID)
			return 0;
		if (vm.prepaidCredit)
			return vm.prepaidCredit;
		return Number((vm.totalAdditionalInvoices() + vm.invoiceSubTotal() - vm.invoiceSubTotal() * vm.invoice.discount / 100 + (vm.invoiceSubTotal() - vm.invoiceSubTotal() * vm.invoice.discount / 100) * vm.invoice.tax / 100).toFixed(2));
	};

	vm.totalAdditionalInvoices = function () {
		var total = 0;
		_.forEach(vm.invoicesAnother, function (invoice, ind) {
			if (invoice.selected)
				total += parseFloat(vm.invoiceTotalAddInv(invoice.data));
		});
		return parseFloat(total.toFixed(2));
	};

	// Calculates the grand total of the invoice
	vm.calculateGrandTotal = function (type) {
		//  saveInvoice();
		if (type == 'paid') {
			if (vm.invoice.entity_type == vm.fieldData.enums.entities.LDCARRIER && vm.invoice.partially_paid && vm.invoice.partially_paid != vm.invoice.totalInvoice) {
				return parseFloat((vm.invoice.partially_paid).toFixed(2));
			}
			return Number((vm.invoiceSubTotal() - vm.invoiceSubTotal() * vm.invoice.discount / 100 + (vm.invoiceSubTotal() - vm.invoiceSubTotal() * vm.invoice.discount / 100) * vm.invoice.tax / 100).toFixed(2));
		}
		if (vm.invoice.entity_type == vm.fieldData.enums.entities.LDCARRIER && vm.invoice.partially_paid && vm.invoice.partially_paid != vm.invoice.totalInvoice) {
			return parseFloat((vm.invoice.totalInvoice - vm.invoice.partially_paid).toFixed(2));
		}
		if (vm.invoice.flag == vm.invoiceFlags.PAID)
			return 0;
		if (vm.prepaidCredit)
			return vm.prepaidCredit;
		return Number((vm.invoiceSubTotal() - vm.invoiceSubTotal() * vm.invoice.discount / 100 + (vm.invoiceSubTotal() - vm.invoiceSubTotal() * vm.invoice.discount / 100) * vm.invoice.tax / 100).toFixed(2));
	};

	vm.invoicePayment = function () {
		let invoicePay = angular.copy(vm.invoice);
		invoicePay.accountInvoice = true;
		if (vm.prepaidCredit)
			invoicePay.total = vm.prepaidCredit;
		else
			invoicePay.total = vm.calculateGrandTotalBalance();
		if (!invoicePay.id && invoicePay.entity_id)
			invoicePay.id = invoicePay.entity_id;
		if (invoicePay.entitytype)
			vm.entitytype = invoicePay.entitytype;
		if (invoicePay.entity_type)
			vm.entitytype = invoicePay.entity_type;
		invoicePay.nid = invoicePay.entity_id;
		if (vm.entitytype == vm.fieldData.enums.entities.STORAGEREQUEST && vm.closeBalance && vm.closeBalanceStorage) invoicePay.total = Math.abs(vm.totalBalance);

		PaymentServices.openAuthPaymentModal(invoicePay, undefined, undefined, vm.entitytype);
	};

	function setPaidInvoices() {
		if (!vm.invoice.request_invoice && vm.invoicesAnother)
			StorageService.getStorageReqByID(vm.invoice.id || vm.invoice.entity_id).then(function ({data}) {
				vm.busy = false;
				vm.payments = data.payments;
				vm.invoices = data.invoices;
				var total = Number((PaymentServices.calcPayment(vm.payments) - PaymentServices.calcInvoices(vm.invoices)).toFixed(2));
				if (total >= 0 && vm.payments.length) {
					_.forEach(vm.invoices, function (item) {
						if (item.data.flag != vm.invoiceFlags.PAID && item.id != vm.invoice.id) {
							item.data.flag = vm.invoiceFlags.PAID;
							var data = {};
							data.data = item.data;
							var id = item.id;
							InvoiceServices.editInvoice(id, data);
						}
					});
				}
			});
	}

	$scope.$on('payment.received', function (evt, response) {
		vm.invoice.flag = vm.invoiceFlags.PAID;
		vm.invoicePaid = true;

		if (vm.invoice.entity_type == vm.fieldData.enums.entities.LDCARRIER && vm.invoice.partially_paid) {
			vm.invoice.partially_paid = Number(vm.invoice.partially_paid) + parseFloat((vm.invoice.totalInvoice - vm.invoice.partially_paid).toFixed(2));
		}

		var data = {};
		data.data = vm.invoice;
		var id = vm.invoiceId;

		let userRoles = _.get($rootScope, 'currentUser.userId.roles', []);
		let hastPemissionsForUpdateInvoice = userRoles.length > 1;

		if (hastPemissionsForUpdateInvoice) {
			InvoiceServices.editInvoice(id, data);
		}

		if (vm.entitytype == vm.fieldData.enums.entities.STORAGEREQUEST && vm.closeBalance && vm.closeBalanceStorage) {
			vm.invoicesAnother = angular.copy(vm.allInvoices);
			vm.totalBalance = 0;
		}


		_.forEach(vm.invoicesAnother, function (invoice) {
			if (invoice.selected) {
				var data = {};
				invoice.data.flag = vm.invoiceFlags.PAID;
				data.data = invoice.data;
				var id = invoice.id;
				InvoiceServices.editInvoice(id, data);
			}
		});
		vm.invoicesAnother = {};
	});

	$scope.$on('receipt.ledger', function (evt, receipt) {

	});


}
