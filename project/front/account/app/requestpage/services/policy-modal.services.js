'use strict';

angular
	.module('app.request')
	.factory('policyModalServices', policyModalServices);

policyModalServices.$inject = ['$window', '$uibModal', 'datacontext'];

function policyModalServices($window, $uibModal, datacontext) {
	let service = {};
	let fieldData = datacontext.getFieldData();
	let basicSettings = angular.fromJson(fieldData.basicsettings);

	service.openPolicyModal = openPolicyModal;

	function openPolicyModal(policyType, disabledConfirmationPrintCallback = ()=>{}) {
		const CANCELATION_POLICY_TYPE = 'cancelation';
		const COMPANY_POLICY_TYPE = 'company';
		const CANCELATION_POLICY_TEMPLATE = 'app/requestpage/templates/cancelation-policy.html';
		const COMPANY_POLICY_TEMPLATE = 'app/requestpage/templates/company-policy.html';

		if(policyType === COMPANY_POLICY_TYPE && !basicSettings.own_company_policy.show) {
			$window.open(basicSettings.company_policy_url, '_blank');
			return;
		}

		$window.scrollTo(0, 0);

		if (policyType === CANCELATION_POLICY_TYPE) {
			showPolicyModal(CANCELATION_POLICY_TEMPLATE, disabledConfirmationPrintCallback);
		}
		if (policyType === COMPANY_POLICY_TYPE) {
			showPolicyModal(COMPANY_POLICY_TEMPLATE, disabledConfirmationPrintCallback);
		}
	}

	function showPolicyModal(url, disabledConfirmationPrintCallback){
		disabledConfirmationPrintCallback(true);
		let modalInstance = $uibModal.open({
			templateUrl: url,
			controller: openPolicyModalController,
			windowClass: 'policy-modal-window',
		});

		modalInstance.closed.then(function () {
			disabledConfirmationPrintCallback(false);
		});
	}

	function openPolicyModalController($scope, $uibModalInstance) {
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}
	return service;
}
