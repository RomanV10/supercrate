angular
	.module('move.requests')
	.factory('RequestPageServices', RequestPageServices);

/*@ngInject*/
function RequestPageServices($uibModal, $http, $q, config, datacontext, CalculatorServices, common, InventoriesServices,
	Session, $rootScope, SweetAlert, EditRequestServices, RequestServices, ParserServices, CheckRequestService,
	newLogService, apiService, moveBoardApi, geoCodingService, valuationService, grandTotalService) {

	let service = {};
	service.initMap = initMap;
	service.countTrucks = countTrucks;
	service.getTotalTime = getTotalTime;
	service.getQuote = getQuote;
	service.calculateTime = calculateTime;
	service.ModalInstanceCtrl = ModalInstanceCtrl;
	service.checkAddresses = checkAddresses;
	service.openCongratsModal = openCongratsModal;
	service.getRoomsString = getRoomsString;
	service.setRequest = setRequest;
	service.addUserLog = addUserLog;
	service.addForemanLog = addForemanLog;
	service.openAddressModal = openAddressModal;
	service.getCardImage = getCardImage;
	service.prepareRequest = prepareRequest;
	service.reservationSignatureModal = reservationSignatureModal;
	service.createReview = createReview;
	service.getCurrentReview = getCurrentReview;
	service.getNotes = getNotes;
	service.getAccountVariable = getAccountVariable;

	let disableAddressTooltipText = 'To change the address, please contact your relocation manager';

	return service;

	/** INIT **/

	function setRequest(request) {
		if (!!request) {
			if (!$rootScope.currentUser.userId.nodes[request.nid]) {
				$rootScope.currentUser.userId.nodes[request.nid] = {};
			}

			$rootScope.currentUser.userId.nodes[request.nid] = request;
		}
	}

	function createReview(data) {
		return apiService.postData(moveBoardApi.reviewMoveboard.createReview, data);
	}

	function getCurrentReview(data) {
		let defer = $q.defer();
		apiService.postData(moveBoardApi.reviewMoveboard.getReview, data)
			.then(resolve => {
				defer.resolve(resolve.data);
			})
			.catch(error => {
				defer.reject(error);
			});

		return defer.promise;
	}


	function getCardImage(receiptID) {
		var deferred = $q.defer();
		$http
			.post(config.serverUrl + 'server/move_request/get_card/' + receiptID)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function getQuote(request) {
	}

	function getRoomsString(rooms) {

		var room_now = 0;
		var room_count = rooms.length;
		var extra_room_text = '';
		var roomsList = datacontext.getFieldData().field_lists.field_extra_furnished_rooms;

		angular.forEach(rooms, function (value, key) {
			room_now++;
			if (room_now == 1 && room_count == room_now) {
				extra_room_text = '(with ' + roomsList[value] + ')';
			}
			else if (room_now == 1 && room_count == 2) {
				extra_room_text = '(with ' + roomsList[value] + ' and ';
			}
			else if (room_now == 2 && room_count == 2) {
				extra_room_text = extra_room_text + roomsList[value] + ')';
			}
			else if (room_now == 1 && room_count > 2) {
				extra_room_text = '(with ' + roomsList[value] + ', ';
			}
			else if (room_now == room_count) {
				extra_room_text = extra_room_text + roomsList[value] + ')';
			}
			else if (room_now + 1 == room_count) {
				extra_room_text = extra_room_text + roomsList[value] + ' and ';
			}
			else {
				extra_room_text = extra_room_text + roomsList[value] + ', ';
			}
		});
		return extra_room_text;
	}

	function getTotalTime(request) {

		//LOCAL MOVE WORK TIME
		var work_time_min = request.maximum_time.raw + request.duration.value + request.travel_time.raw;
		var work_time_max = request.minimum_time.raw + request.duration.value + request.travel_time.raw;
		// GET RATE
		var rate = request.rate.value;

		if (results.quote_max <= results.min_hours) {
			results.quote_max = results.min_hours;
		}
		if (results.quote_max == results.quote_min) {
			results.small_job = true;
		}
		results.total_quote_min = results.rate * results.quote_min;
		results.total_quote_max = results.rate * results.quote_max;
	}

	function countTrucks(trucks) {
		return (_.keys(trucks)).length;
	}

	function initMap(from, to) {
		var myOptions = {
			draggable: true,
			navigationControl: true,
			scrollwheel: true,
			streetViewControl: true,
			zoom: 13,
			maxZoom: 14,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: new google.maps.LatLng(40.84, 14.25),
		};


		var elm = document.getElementById('ermap');
		var mapObject = new google.maps.Map(elm, myOptions);

		var directionsService = new google.maps.DirectionsService();
		var directionsRequest = {
			origin: from,
			destination: to,
			travelMode: google.maps.DirectionsTravelMode.DRIVING,
			unitSystem: google.maps.UnitSystem.METRIC
		};
		directionsService.route(
			directionsRequest,
			function (response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					new google.maps.DirectionsRenderer({
						map: mapObject,
						directions: response
					});
				}
				else
					$("#error").append("Unable to retrieve your route<br />");
			}
		);
	}

	function calculateTime(request, weight) {
		var total_cf = [];
		var calcSettings = angular.fromJson($rootScope.fieldData.calcsettings);

		total_cf.weight = weight;
		request.typeFrom = request.type_from.raw;
		request.typeTo = request.type_to.raw;
		request.serviceType = request.service_type.raw;

		var calcResults = CalculatorServices.calculateTime(request, total_cf);
		var min_hours = calcResults.min_hours;
		var moversCount = calcResults.movers_count;
		var trucksCount = calcResults.trucks;
		var minWorkTime = calcResults.work_time.min;
		var maxWorkTime = calcResults.work_time.max;

		if (!calcSettings.doubleDriveTime && !calcSettings.isTravelTimeSameAsDuration) {
			minWorkTime += parseFloat(request.duration.value);
			maxWorkTime += parseFloat(request.duration.value);
		}

		minWorkTime = CalculatorServices.getRoundedTime(minWorkTime);
		maxWorkTime = CalculatorServices.getRoundedTime(maxWorkTime);

		request.minimum_time.raw = minWorkTime;
		request.minimum_time.value = common.decToTime(minWorkTime);

		request.maximum_time.raw = maxWorkTime;
		request.maximum_time.value = common.decToTime(maxWorkTime);

		request.crew.value = moversCount;

		var truckNeeded = CalculatorServices.getTrucks(weight);

		request.truckCount = truckNeeded;
		var date = '';
		if (typeof request.date.value != 'object') {
			date = request.date.value;
		} else {
			date = request.date.value.date;
		}

		let flatRateLongDistance = request.service_type.raw == 7 || request.service_type.raw == 5;
		let updateReservationDataLM = !flatRateLongDistance && request.status.raw != 3;

		if (updateReservationDataLM) {
			request.rate.value = CalculatorServices.getRate(date, moversCount, truckNeeded);

			let travelTime = calcSettings.travelTime ? request.travel_time.raw : 0;
				travelTime = calcSettings.doubleDriveTime ? request.field_double_travel_time.raw : travelTime;

			request.quote.max = (request.maximum_time.raw + travelTime) * request.rate.value;
		}

		if (_.isNull(request.rate.value)) {
			request.rate.value = CalculatorServices.getRate(date, moversCount, truckNeeded);
		}

		return request;
	}

	function _prepareRequest(request) {
		const DEFAULT_VALUE = '';
		var new_request = {};
		var user = request.uid;
		let userMainPhone = _.has(user, 'field_primary_phone.und')
			? _.get(user, 'field_primary_phone.und', DEFAULT_VALUE)
			: _.get(user, 'field_primary_phone', DEFAULT_VALUE);
		var userField_user_additional_phone;
		var field_extra_dropoff = [];

		if (user.field_user_additional_phone != null
			&& user.field_user_additional_phone.hasOwnProperty('und')) {

			userField_user_additional_phone = user.field_user_additional_phone.und[0].value;
		}

		if (angular.isDefined(request.field_extra_dropoff)) {
			field_extra_dropoff = request.field_extra_dropoff;
		}

		new_request.data = {
			status: 1,
			title: 'Move Request',
			uid: user.uid,
			field_first_name: user.field_user_first_name,
			field_last_name: user.field_user_last_name,
			field_e_mail: user.mail || '',
			field_phone: userMainPhone,
			field_additional_phone: userField_user_additional_phone || '',
			field_approve: request.status.raw,
			field_move_service_type: request.service_type.raw, //Move & storage
			field_date: {
				date: moment.unix(request.date.raw).format('YYYY-MM-DD'),
				time: '15:10:00'
			},
			field_size_of_move: request.move_size.raw,
			field_type_of_entrance_from: request.type_from.raw,
			field_type_of_entrance_to_: request.type_to.raw,

			field_start_time: request.start_time.raw,
			field_price_per_hour: request.rate.value,
			field_movers_count: request.crew.value,

			field_distance: request.distance.value,
			field_travel_time: request.travel_time.raw,
			field_minimum_move_time: request.minimum_time.raw,
			field_maximum_move_time: request.maximum_time.raw,
			field_extra_dropoff: field_extra_dropoff,

			field_extra_furnished_rooms: request.rooms.raw, //?
			field_moving_to: {
				country: "US",
				administrative_area: request.field_moving_to.administrative_area,
				locality: request.field_moving_to.locality,
				postal_code: request.field_moving_to.postal_code,
				thoroughfare: request.field_moving_to.thoroughfare,
				premise: ""
			},
			field_apt_from: request.apt_from.value,
			field_apt_to: request.apt_to.value,
			field_moving_from: {
				country: "US",
				administrative_area: request.field_moving_from.administrative_area,
				locality: request.field_moving_from.locality,
				postal_code: request.field_moving_from.postal_code,
				thoroughfare: request.field_moving_from.thoroughfare,
				premise: ""
			},

			field_storage_price: request.storage_rate.value,
			field_reservation_price: request.reservation_rate.value,
			field_to_storage_move: request.nid,
			field_useweighttype: request.field_useweighttype.value
		};

		if (request.move_size.raw == 11) {
			new_request.data.field_commercial_extra_rooms = request.commercial_extra_rooms.value;
			new_request.data.field_useweighttype = request.field_useweighttype.value;
		}

		return new_request;
	}

	function _createRequest(request) {
		var deferred = $q.defer();
		$http
			.post(config.serverUrl + 'server/move_request', request)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function checkAddresses(req) {
		if (req.service_type.raw == 3 && (_.isEmpty(req.field_moving_from.administrative_area) || _.isEmpty(req.field_moving_from.postal_code)))
			return true;
		else if (req.service_type.raw == 4 && (_.isEmpty(req.field_moving_to.administrative_area) || _.isEmpty(req.field_moving_to.postal_code)))
			return true;
		else if ((req.service_type.raw != 4 && req.service_type.raw != 3) && (_.isEmpty(req.field_moving_from.administrative_area) || _.isEmpty(req.field_moving_from.postal_code) || _.isEmpty(req.field_moving_to.administrative_area) || _.isEmpty(req.field_moving_to.postal_code)))
			return true;
		return false;
	}

	/* @ngInject */
	function ModalInstanceCtrl($scope, $uibModalInstance, initial_request, RequestServices, EditRequestServices, SweetAlert, $rootScope, newLogService, CheckRequestService) {
		var basicsettings = angular.fromJson($rootScope.fieldData.basicsettings);
		var enums = $rootScope.fieldData.enums;
		let commercialSetting = angular.fromJson($scope.fieldData['commercial_move_size_setting']);

		$scope.manageQuote = true;
		$scope.busy = false;
		$scope.rooms = ['living room', 'dining room', 'office', 'extra room', 'basement', 'garage', 'patio', 'play room'];
		$scope.servicesTypes = CheckRequestService.serviceTypes()

		$scope.delete = false;
		$scope.request = angular.copy(initial_request);
		$scope.startTime = datacontext.getFieldData().field_lists.field_start_time;
		$scope.serviceType = datacontext.getFieldData().field_lists.field_move_service_type;

		$scope.allowedMoveSizes = angular.copy($rootScope.fieldData.field_lists.field_size_of_move);

		$scope.disableChangeAddressTooltipText = disableAddressTooltipText;
		$scope.showTooltipDisabledChangeAddress = $scope.request.status.raw == 3;

		let isCommercial = initial_request.move_size.raw == 11;
		if (!_.isUndefined(commercialSetting) && !_.isNull(commercialSetting) && !commercialSetting.isEnabled && !isCommercial) {
			let commercial = Object.keys($scope.allowedMoveSizes).find(key => {
				return $scope.allowedMoveSizes[key] === commercialSetting.name
			});

			delete $scope.allowedMoveSizes[commercial];
		}

		$scope.objectKeys = function (obj) {
			return Object.keys(obj);
		};

		$scope.confirmed = (initial_request.status.raw == 3);
		$scope.useCalculator = initial_request.field_usecalculator.value || 1;


		if ($scope.request.field_moving_from.thoroughfare) {
			if ($scope.request.field_moving_from.thoroughfare.toLowerCase() == 'enter address') {
				$scope.request.field_moving_from.thoroughfare = '';
			}
		}

		if ($scope.request.field_moving_to.thoroughfare) {
			if ($scope.request.field_moving_to.thoroughfare.toLowerCase() == 'enter address') {
				$scope.request.field_moving_to.thoroughfare = '';
			}
		}

		if (!!$scope.request.field_extra_pickup.postal_code) {
			if ($scope.request.field_extra_pickup.postal_code.length) {
				$scope.extraPickup = true;
			}
		}

		$scope.isInValidRequest = !ParserServices.validateParserRequest($scope.request, $scope.editrequest);

		if ($scope.isInValidRequest) {
			let isValidateTo = ParserServices.isValidateTo($scope.request);
			let isValidateFrom = ParserServices.isValidateFrom($scope.request);

			$scope.isInvalidParsingRequest = true;
			$scope.companyName = basicsettings.company_name;
			$scope.isInvalidRequestDate = ParserServices.isInvalidFieldDate($scope.request);
			$scope.isInvalidFieldMovingFrom = ParserServices.isInvalidFieldMovingFrom($scope.request) && isValidateFrom;
			$scope.isInvalidMoveSize = ParserServices.isInvalidFieldMoveSize($scope.request);

			if ($scope.isInvalidFieldMovingFrom) {
				$scope.request.type_from.raw = "";
			}

			$scope.isInvalidFieldMovingTo = ParserServices.isInvalidFieldMovingTo($scope.request) && isValidateTo;

			if ($scope.isInvalidFieldMovingTo) {
				$scope.request.type_to.raw = "";
			}

			$scope.isInvalidTypeTo = ParserServices.isInvalidTypeTo($scope.request) && isValidateTo;
			$scope.isInvalidTypeFrom = ParserServices.isInvalidTypeFrom($scope.request) && isValidateFrom;


			$scope.$watchCollection('editrequest', function (newVal, oldVal) {
				$scope.isInValidRequest = !ParserServices.validateParserRequest($scope.request, $scope.editrequest);

				if ($scope.editrequest.field_date) {
					$scope.isInvalidRequestDate = ParserServices.isInvalidEditFieldDate($scope.editrequest);
				}

				if ($scope.editrequest.field_moving_from && isValidateTo) {
					$scope.isInvalidFieldMovingFrom = ParserServices.isInvalidFieldMovingFrom($scope.editrequest);
				}

				if ($scope.editrequest.field_moving_to && isValidateFrom) {
					$scope.isInvalidFieldMovingTo = ParserServices.isInvalidFieldMovingTo($scope.editrequest);
				}

				if (!_.isUndefined($scope.editrequest.field_type_of_entrance_to_) && isValidateTo) {
					$scope.isInvalidTypeTo = ParserServices.isInvalidFieldTypeTo($scope.editrequest);
				}

				if (!_.isUndefined($scope.editrequest.field_type_of_entrance_from) && isValidateFrom) {
					$scope.isInvalidTypeFrom = ParserServices.isInvalidFieldTypeFrom($scope.editrequest);
				}

				if (!_.isEqual(newVal.field_size_of_move, oldVal.field_size_of_move)) {
					$scope.isInvalidMoveSize = false;
					changeSizeOfMove();
				}

			});
			//customized modal greetings
			$scope.customizedGreetings = basicsettings.flatRateAccountSettings.greetingParserText;
			//
		} else {
			$scope.$watchCollection('editrequest', function (newVal, oldVal) {
				if (!_.isEqual(newVal.field_size_of_move, oldVal.field_size_of_move)) {
					changeSizeOfMove();
				}
			});
		}

		function changeSizeOfMove() {
			var roomsElement = angular.element("#edit-furnished-rooms");
			var moveSize = $scope.editrequest.field_size_of_move;
			var rooms = $scope.request.rooms.raw || [];

			if (moveSize == 3 || moveSize == 4 || moveSize == 5 || moveSize == 6 || moveSize == 7
				|| moveSize == 8 || moveSize == 9 || moveSize == 10) {
				if (rooms.indexOf('1') < 0) {
					rooms.push('1');
				}
			}

			if (moveSize == 8 || moveSize == 9 || moveSize == 10) {
				if (rooms.indexOf('2') < 0) {
					rooms.push('2');
				}
			}

			if (!!rooms.length) {
				jQuery.each(rooms, function (index, item) {
					roomsElement.find("option[value=" + item + "]").attr('selected', 'selected');
					roomsElement.trigger("chosen:updated");
				});

				$scope.editrequest.field_extra_furnished_rooms = rooms;
			}
		}

		if (!!$scope.request.field_extra_dropoff.postal_code) {
			if ($scope.request.field_extra_dropoff.postal_code.length) {
				$scope.extraDropoff = true;
			}
		}

		$scope.pickupOnly = ($scope.request.service_type.raw == 3 || $scope.request.service_type.raw == 4);
		$scope.fieldData = datacontext.getFieldData();
		$scope.getSettings = datacontext.getSettingsData();
		$scope.changeMail = false;
		$scope.editrequest = {};
		$scope.message = {};
		var date = '';

		if (!_.isObject($scope.request.date.value)) {
			date = $scope.request.date.value;
		} else {
			date = $scope.request.date.value.date;
		}

		$scope.moveDateInput = $.datepicker.formatDate('M d, yy', new Date(date), {});
		//detect storage and real requests
		var storageRequest = null;

		if (!!initial_request.storage_id) {
			if (!initial_request.request_all_data.toStorage) {
				RequestServices
					.getRequestsByNid(initial_request.storage_id)
					.then(function (response) {

						storageRequest = response.nodes[0];
					}, function (errorResponse) {
						storageRequest = null;
					});
			} else {
				storageRequest = angular.copy(initial_request);

				RequestServices
					.getRequestsByNid(storageRequest.storage_id)
					.then(function (response) {

						storageRequest = response.nodes[0];
					}, function (errorResponse) {
						storageRequest = null;
					});
			}
		}

		$scope.origin = ($scope.request.service_type.raw != 2 && !$scope.request.storage_id)
			|| ($scope.request.service_type.raw == 2 && !$scope.request.request_all_data.toStorage);
		$scope.destination = ($scope.request.service_type.raw != 2 && !$scope.pickupOnly)
			|| ($scope.request.service_type.raw == 2 && $scope.request.request_all_data.toStorage);

		if ($scope.request.service_type.raw == 3 || $scope.request.service_type.raw == 8) {
			$scope.destination = false;
			$scope.origin = true;
		} else if ($scope.request.service_type.raw == 4) {
			$scope.destination = true;
			$scope.origin = false;
		}

		if ($scope.request.service_type.raw == 6 && $scope.request.from_storage == 1) {
			$scope.destination = true;
			$scope.origin = false;
		} else if ($scope.request.service_type.raw == 6 && $scope.request.from_storage == 2) {
			$scope.destination = false;
			$scope.origin = true;
		}

		$scope.update = function () {
			$scope.busy = true;
			updateRequest($scope, initial_request, $rootScope, storageRequest, $uibModalInstance);
		};

		$scope.cancel = function () {
			$scope.editrequest = [];
			$uibModalInstance.dismiss('cancel');
		};
	}

	function openCongratsModal(request) {
		var modalInstance = $uibModal.open({
			templateUrl: 'app/requestpage/services/congrats.html?4',
			controller: CongragtsInstanceCtrl,
			size: 'lg',
			backdrop: true,
			resolve: {
				request: function () {
					return request;
				}
			}
		});

		modalInstance.result.then(function ($scope) {

		});

		function CongragtsInstanceCtrl($scope, $uibModalInstance, request, datacontext) {
			$scope.request = request;
			var fieldData = datacontext.getFieldData();
			$scope.basicSettings = angular.fromJson(fieldData.basicsettings);

			$scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};
		}
	}


	function addUserLog(message, nid, uid) {
		if (message.length) {
			var d = new Date();
			var n = d.getTime();

			var message = {
				text: message,
				nid: nid,
				type: 'client_activity'
			};

			//Date "message 1" messgae 2"
			$http.post(config.serverUrl + 'server/move_sales_log', message).success(function (data) {
				data.success = true;
			});
		}
	}

	function addForemanLog(message, nid, uid) {
		if (message.length) {
			var d = new Date();
			var n = d.getTime();

			var message = {
				text: message,
				nid: nid,
				type: 'foreman_activity'
			};

			//Date "message 1" messgae 2"
			$http.post(config.serverUrl + 'server/move_sales_log', message).success(function (data) {
				data.success = true;
			});
		}
	}

	function openAddressModal(request, withoutUpdate, manageQuote) {
		var modalInstance = $uibModal.open({
			templateUrl: 'app/requestpage/services/addressesModal.html?669',
			controller: addressModalInstanceCtrl,
			size: 'lg',
			backdrop: true,
			resolve: {
				request: function () {
					return request;
				},
				withoutUpdate: function () {
					return withoutUpdate;
				},
				manageQuote: function () {
					return manageQuote;
				}
			}
		});

		/* @ngInject */
		function addressModalInstanceCtrl($scope, $uibModalInstance, request, withoutUpdate, manageQuote, datacontext, SweetAlert, EditRequestServices, RequestServices, CalculatorServices, InventoriesServices, $rootScope, apiService, moveBoardApi) {
			$scope.request = angular.copy(request);
			$scope.requestOld = angular.copy(request);
			$scope.editrequest = {};
			$scope.message = {};
			$scope.moving = {};
			$scope.manageQuote = !!manageQuote;
			$scope.disableChangeAddressTooltipText = disableAddressTooltipText;
			$scope.showTooltipDisabledChangeAddress = true;

			if ($scope.request.field_moving_from.thoroughfare) {
				if ($scope.request.field_moving_from.thoroughfare.toLowerCase() == 'enter address') {
					$scope.moving.from = _.clone($scope.request.field_moving_from.thoroughfare);
					$scope.request.field_moving_from.thoroughfare = '';
				}
			}

			if ($scope.request.field_moving_to.thoroughfare) {
				if ($scope.request.field_moving_to.thoroughfare.toLowerCase() == 'enter address') {
					$scope.moving.to = _.clone($scope.request.field_moving_to.thoroughfare);
					$scope.request.field_moving_to.thoroughfare = '';
				}
			}

			var fieldData = datacontext.getFieldData();
			$scope.basicSettings = angular.fromJson(fieldData.basicsettings);
			var enums = fieldData.enums;

			$scope.objectKeys = function (obj) {
				return Object.keys(obj);
			};

			var storageRequest = null;

			if (!!request.storage_id) {
				if (!request.request_all_data.toStorage) {
					RequestServices
						.getRequestsByNid(request.storage_id)
						.then(function (response) {

							storageRequest = response.nodes[0];
						}, function (errorResponse) {
							storageRequest = null;
						})
				} else {
					storageRequest = angular.copy(request);
					RequestServices
						.getRequestsByNid(storageRequest.storage_id)
						.then(function (response) {

							storageRequest = response.nodes[0];
						}, function (errorResponse) {
							storageRequest = null;
						})
				}
			}

			$scope.confirmed = ($scope.request.status.raw == 3);
			$scope.useCalculator = $scope.request.field_usecalculator.value || 1;
			$scope.pickupOnly = ($scope.request.service_type.raw == 3 || $scope.request.service_type.raw == 4);
			$scope.origin = ($scope.request.service_type.raw != 2 && !$scope.request.storage_id) || ($scope.request.service_type.raw == 2 && !$scope.request.request_all_data.toStorage);
			$scope.destination = ($scope.request.service_type.raw != 2 && !$scope.pickupOnly) || ($scope.request.service_type.raw == 2 && $scope.request.request_all_data.toStorage);

			if ($scope.request.service_type.raw == 3 || $scope.request.service_type.raw == 8) {
				$scope.destination = false;
				$scope.origin = true;
			} else if ($scope.request.service_type.raw == 4) {
				$scope.destination = true;
				$scope.origin = false;
			}

			if ($scope.request.service_type.raw == 6
				&& $scope.request.from_storage == 1) {
				$scope.destination = true;
				$scope.origin = false;
			} else if ($scope.request.service_type.raw == 6
				&& $scope.request.from_storage == 2) {
				$scope.destination = false;
				$scope.origin = true;
			}

			$scope.update = function (client) {
				if (withoutUpdate) {
					setRequest($scope.request);

					if (!_.isUndefined($scope.editrequest.field_moving_from) || !_.isUndefined($scope.editrequest.field_moving_to)) {
						$rootScope.$broadcast('origin_address', {
							field_moving_to: $scope.request.field_moving_to,
							field_moving_from: $scope.request.field_moving_from
						});

						request.field_extra_dropoff = $scope.request.field_extra_dropoff;
						request.field_extra_pickup = $scope.request.field_extra_pickup;
						request.field_moving_from = $scope.request.field_moving_from;
						request.field_moving_to = $scope.request.field_moving_to;

						$scope.editrequest.field_moving_from = $scope.request.field_moving_from;
						$scope.editrequest.field_moving_to = $scope.request.field_moving_to;
					}

					$uibModalInstance.close($scope.editrequest);
				} else {
					$scope.busy = true;
					updateRequest($scope, request, $rootScope, storageRequest, $uibModalInstance);
				}
			};

			$scope.cancel = function () {
				if ($scope.request.field_moving_from.thoroughfare == '') {
					$scope.request.field_moving_from.thoroughfare = $scope.moving.from;
				}

				if ($scope.request.field_moving_to.thoroughfare == '') {
					$scope.request.field_moving_to.thoroughfare = $scope.moving.to;
				}

				$scope.editrequest = [];
				$uibModalInstance.dismiss('cancel');
			};

			$scope.editAddress = function (title, field, value) {

			}
		}

		return modalInstance.result;
	}

	function isValidMovingField(field, property) {
		return field[property]
			&& !_.isNull(field[property])
			&& !_.isEmpty(field[property])
	}

	function getMovingFieldMessageOfRequest(request, property, direction_property) {
		var result = '';

		if (isValidMovingField(request[property], 'thoroughfare')) {
			result += request[property].thoroughfare + ' ';
		}

		if (!_.isUndefined(direction_property)
			&& isValidMovingField(request[direction_property], 'value')) {

			result += request[direction_property].value + ' ';
		}

		if (isValidMovingField(request[property], 'locality')) {
			result += request[property].locality + ' ';
		}

		if (isValidMovingField(request[property], 'administrative_area')) {
			result += request[property].administrative_area;
		}

		if (isValidMovingField(request[property], 'postal_code')) {
			result += ', ' + request[property].postal_code + '.';
		}

		return result;
	}

	function updateRequest($scope, initial_request, $rootScope, storageRequest, $uibModalInstance) {
		var basicsettings = angular.fromJson($rootScope.fieldData.basicsettings);
		var calcSettings = angular.fromJson($rootScope.fieldData.calcsettings);
		var enums = $rootScope.fieldData.enums;

		$scope.alertmessage = {};
		var count = common.count($scope.editrequest);

		if (!CheckRequestService.isValidRequestZip($scope.request)) {
			SweetAlert.swal("Please enter the correct zip code");
			$scope.busy = false;
			return false;
		}

		if (!count) {
			SweetAlert.swal("Nothing to Update!");
			$scope.busy = false;
			return false;
		}

		function forceReloadAccount() {
			$rootScope.$broadcast('force_reload_account');
		}

		var services = [1, 2, 3, 4, 6, 8];

		var isDoubleDriveTime = calcSettings.doubleDriveTime
			&& initial_request.field_double_travel_time
			&& !_.isNull(initial_request.field_double_travel_time.raw)
			&& services.indexOf(parseInt($scope.request.service_type.raw)) >= 0;
		var minCATravelTime = basicsettings.minCATavelTime ? CalculatorServices.getRoundedTime(basicsettings.minCATavelTime / 60) : 0;
		var alertMessage = '';

		function checkStatusToPendingInfo() {
			let allowToPendingInfo = !!+$scope.request.field_allow_to_pending_info;
			let isNotConfirmed = $scope.request.status.raw == 2;
			if (allowToPendingInfo && isNotConfirmed) {``
				$scope.editrequest.field_approve = "17";
				$scope.request.status['raw'] = "17";
				$scope.request.status['value'] = 'Pending Info';
				initial_request.status['raw'] = "17";
				initial_request.status['value'] = 'Pending Info';
				alertMessage = "We will recalculate your quote, based on new info!";
			}
		}

		calculate(angular.copy(initial_request))
			.then(function () {
				if (($scope.editrequest.field_type_of_entrance_from || $scope.editrequest.field_type_of_entrance_to_) && $scope.request.status.raw != 1) {
					checkStatusToPendingInfo();
				}
				if ($scope.request.status.raw == 2) {
					let isChangeMoveTime = angular.isDefined($scope.editrequest['field_maximum_move_time'])
						|| angular.isDefined($scope.editrequest['field_minimum_move_time'])
						|| angular.isDefined($scope.editrequest['field_double_travel_time']);

					let isChangePostalCode = (angular.isDefined($scope.editrequest.field_moving_from)
						&& $scope.editrequest.field_moving_from.postal_code != initial_request.field_moving_from.postal_code)
						|| (angular.isDefined($scope.editrequest.field_moving_to)
						&& $scope.editrequest.field_moving_to.postal_code != initial_request.field_moving_to.postal_code);

					if (_.isNull(initial_request.field_useweighttype.value)) {
						initial_request.field_useweighttype.value = 1;
					}

					let isChangeMoveSize = angular.isDefined($scope.editrequest.field_size_of_move)
						&& (_.isEmpty(initial_request.inventory)
						&& initial_request.field_useweighttype.value == 1
						|| _.isEmpty(initial_request.inventory.inventory_list));

					if (isChangeMoveTime
						&& (isChangePostalCode || isChangeMoveSize)) {

						if (!$scope.request.field_reservation_received.value) {
							checkStatusToPendingInfo();
						}
					}
				}

				if ($scope.request.status.raw == 5
					|| $scope.request.status.raw == 14
					|| $scope.request.status.raw == 12) {

					if (angular.isDefined($scope.editrequest.field_date)) {
						var current = moment().unix();
						var setDate = moment($scope.editrequest.field_date.date).unix();

						if (setDate > current)
							$scope.editrequest.field_approve = "1";
					}
					else {
						var current = moment().unix();
						var setDate = moment($scope.request.date.value).unix();

						if (setDate > current) {
							$scope.editrequest.field_approve = "1";
						}
					}

				}

				$scope.busy = true;

							let reservationPaidChangedManually = !$scope.request.field_reservation_received.value &&
																 !$scope.request.request_all_data.reservationChangedManually;

							let updateReservationLocalMoves = $scope.request.service_type.raw != 7 &&
															  $scope.request.service_type.raw != 5 && $scope.request.status.raw != 3;

							if (reservationPaidChangedManually && ($scope.request.service_type.raw == 7 || updateReservationLocalMoves)) {
								if ($scope.editrequest.field_price_per_hour) {
									$scope.request.rate.value = $scope.editrequest.field_price_per_hour;
								}

								grandTotalService.getReservationRate($scope.request);

					if (initial_request.reservation_rate.value != $scope.request.reservation_rate.value) {
						$scope.editrequest['field_reservation_price'] = _.get($scope.request, 'reservation_rate.value', 0);
					}
				}

				if (!_.isUndefined($scope.editrequest['field_date'])
					&& initial_request.status.raw != '1'
					&& initial_request.status.raw != '17') {
					let allowToPendingInfo = !!+$scope.request.field_allow_to_pending_info;
					let isNotConfirmed = $scope.request.status.raw == 2;
					if (allowToPendingInfo) {
						if (isNotConfirmed) {
							$scope.editrequest['field_approve'] = '17';
						}
					} else if (isNotConfirmed) {
						$scope.editrequest['field_approve'] = '2';
					} else {
						$scope.editrequest['field_approve'] = '1';
					}
				}

				$scope.messages = Object.values($scope.message);

				let arr = [];

				_.forEach($scope.messages, function (message) {
					var msg = {
						from: '',
						to: '',
						text: ''
					};
					if ((angular.isUndefined(message.oldValue) || !message.oldValue) && message.label != 'custom') {
						msg.text = message.label;
						msg.to = message.newValue;
					}

					if (message.oldValue) {
						if (!message.oldValue.length && message.label != 'custom') {
							msg.text = message.label;
							msg.to = message.newValue;
						} else if (message.oldValue.length && message.label != 'custom') {
							msg.text = message.label;
							msg.to = message.newValue;
							msg.from = message.oldValue;
						}
					}

					if (message.label == 'custom') {
						msg.text = message.newValue;
					}

					if (message.label == 'Date Confirmed') {
						msg.text = message.label;
						msg.to = moment.unix(message.newValue).format('MMM DD, YYYY');
						msg.from = message.oldValue;
					}

					arr.push(msg);
				});

				let data = {
					node: {
						nid: $scope.request.nid
					},
					notification: {
						text: ''
					}
				};

				let notificationMsg = '';

				_.forEach(arr, function (msg) {
					notificationMsg += msg.text;

					if (msg.from && msg.to) {
						notificationMsg += 'changed from: ' + msg.from + ', to: ' + msg.to;
					} else if (msg.to) {
						notificationMsg += 'changed to: ' + msg.to;
					}

					notificationMsg += '\n';
				});

				data.notification.text = notificationMsg;
				let dataNotify = {
					type: enums.notification_types.CUSTOMER_DO_CHANGE_WITH_REQUEST,
					data: data,
					entity_id: $scope.request.nid,
					entity_type: enums.entities.MOVEREQUEST
				};

				apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
				RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');

				if (angular.isDefined($scope.editrequest.field_size_of_move)) {
					initial_request.move_size.value = $scope.editrequest.field_size_of_move;
				}

				if (angular.isDefined($scope.editrequest.field_extra_furnished_rooms)) {
					initial_request.rooms.value = $scope.editrequest.field_extra_furnished_rooms;
				}

				let valuationCharge = _.get(initial_request, 'request_all_data.valuation.selected.valuation_charge');

				if (!_.isUndefined(valuationCharge)) {
					valuationService.reCalculateValuation(initial_request, undefined, 'request');
				} else {
					RequestServices.saveRequestDataContract(initial_request);
				}
				//Move and Storage
				if ($scope.request.service_type.raw == '2' || $scope.request.service_type.raw == '6') {
					if (!storageRequest) { // If no storage request then create new storage request
						CalculatorServices
							.calculateStorageRequest($scope.request, storageRequest, $scope.editrequest)
							.then((calculatedData) => {
								//Create new one
								var newRequest = _prepareRequest(calculatedData.From.request);

								if (calculatedData.From.all_data) {
									newRequest.all_data = calculatedData.From.all_data;
								}

								newRequest.data.field_request_settings = newRequest.data.field_request_settings ||
									{
										storage_rate: basicsettings.storage_rate,
										inventoryVersion: $scope.request.field_request_settings.inventoryVersion
									};

								_createRequest(newRequest)
									.then(function (response) {

										if (!!response) {
											var newId = response.nid;
											calculatedData.To.editrequest.field_from_storage_move = newId;
											//Update original requests
											let fieldData = $rootScope.fieldData.field_lists;

											let results = angular.copy(newRequest.data);
											let request = results;
											request.move_size = {raw: results.field_size_of_move};
											request.rooms = {
												raw: results.field_extra_furnished_rooms,
												value: results.field_extra_furnished_rooms
											};
											request.inventory = {inventory_list: $scope.request.inventory.inventory_list};
											request.inventory_weight = $scope.request.inventory_weight;
											request.custom_weight = {value: results.field_customweight};
											request.field_useweighttype = {value: results.field_useweighttype};

											let weight = CalculatorServices.getTotalWeight(request).weight;

											let arr = newLogService.createLogs('Client Creating Request', fieldData,
												newRequest.data, parseFloat(weight));

											if (!_.isEmpty(arr)) {
												RequestServices.sendLogs(arr, 'Request was created', newId,
													'MOVEREQUEST');
												let data = {
													node: {nid: $scope.request.nid},
													notification: {
														text: ''
													}
												};
												let notificationMsg = '';

												_.forEach(arr, function (msg) {
													notificationMsg += msg.text;
													if (msg.from && msg.to) {
														notificationMsg += 'changed from: ' + msg.from + ', to: ' + msg.to;
													} else if (msg.to) {
														notificationMsg += 'changed to: ' + msg.to;
													}
													notificationMsg += '\n';
												});

												data.notification.text = notificationMsg;
												let dataNotify = {
													type: enums.notification_types.CUSTOMER_DO_CHANGE_WITH_REQUEST,
													data: data,
													entity_id: $scope.request.nid,
													entity_type: enums.entities.MOVEREQUEST
												};

												apiService.postData(moveBoardApi.notifications.createNotification,
													dataNotify);
											}

											if (calculatedData.To.all_data) {
												RequestServices.saveRequestDataContract($scope.request);
											}

											var promise1 = RequestServices.updateRequest(initial_request.nid,
												calculatedData.To.editrequest);
											var promise2 = RequestServices.getRequestsByNid(newId);

											$q.all([promise1, promise2]).then(function (responseArr) {
												forceReloadAccount();

												$rootScope.$broadcast('origin_address', {
													field_moving_to: $scope.request.field_moving_to,
													field_moving_from: $scope.request.field_moving_from
												});

												let msgLog = newLogService.createLogs('Request service type changed',
													fieldData, calculatedData.To.editrequest);

												if (!_.isEmpty(msgLog)) {
													RequestServices.sendLogs(msgLog, 'Request was updated',
														initial_request.nid, 'MOVEREQUEST');
													let data = {
														node: {nid: $scope.request.nid},
														notification: {
															text: ''
														}
													};
													let notificationMsg = '';

													_.forEach(arr, function (msg) {
														notificationMsg += msg.text;

														if (msg.from && msg.to) {
															notificationMsg += 'changed from: ' + msg.from + ', to: ' + msg.to;
														} else if (msg.to) {
															notificationMsg += 'changed to: ' + msg.to;
														}

														notificationMsg += '\n';
													});

													data.notification.text = notificationMsg;
													let dataNotify = {
														type: enums.notification_types.CUSTOMER_DO_CHANGE_WITH_REQUEST,
														data: data,
														entity_id: $scope.request.nid,
														entity_type: enums.entities.MOVEREQUEST
													};
													apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
												}

												EditRequestServices.updateRequestLive(initial_request, calculatedData.To.editrequest);
												initial_request.storage_id = newId;

												if (angular.isDefined(initial_request.inventory.inventory_list)) {
													InventoriesServices.saveListInventories(newId, initial_request.inventory.inventory_list);
												}

												if (angular.isDefined(initial_request.inventory.move_details)) {
													InventoriesServices.saveListInventories(newId, initial_request.inventory.move_details);
												}

												initial_request.field_extra_dropoff = $scope.request.field_extra_dropoff;
												initial_request.field_extra_pickup = $scope.request.field_extra_pickup;
												initial_request.field_moving_from = $scope.request.field_moving_from;
												initial_request.field_moving_to = $scope.request.field_moving_to;
												setRequest(initial_request); //?
												storageRequest = responseArr[1][newId];
												setRequest(storageRequest);

												$scope.request = initial_request;

												$scope.editrequest = [];
												$scope.busy = false;
												toastr.success('Updated', 'Success');
												common.$timeout(() => $uibModalInstance.close($scope.request), 1000);
											});
										}
									}, function (errorResponse) {
										logger.error(errorResponse || 'error', null, '');
									});
							});
					} else {
						//if we have changes except service type
						RequestServices.updateRequest(initial_request.nid, $scope.editrequest).then(function () {
							forceReloadAccount();
							$rootScope.$broadcast('origin_address', {
								field_moving_to: $scope.request.field_moving_to,
								field_moving_from: $scope.request.field_moving_from
							});

							EditRequestServices.updateRequestLive(initial_request, $scope.editrequest);

							initial_request.field_extra_dropoff = $scope.request.field_extra_dropoff;
							initial_request.field_extra_pickup = $scope.request.field_extra_pickup;
							initial_request.field_moving_from = $scope.request.field_moving_from;
							initial_request.field_moving_to = $scope.request.field_moving_to;

							saveParserRequestAllData(initial_request);
							setRequest(initial_request);

							$scope.busy = false;
							$scope.editrequest = [];
							SweetAlert.swal('Updated!', '', 'success');

							common.$timeout(function () {
								$uibModalInstance.close($scope.request);
							}, 1000);
						});
					}
				} else {
					var message = 'User Update Request : ';
					_.forEach($scope.editrequest, function (item, name_field) {
						var words = _.words(name_field.replace(/_/g, ' '));
						_.forEach(words, function (word, i) {
							if (i == 1) {
								word = word.replace(/\w\S*/g, function (txt) {
									return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
								});
							}

							message += word + ' ';
						});

						message += 'changed to ';

						if (name_field != 'field_extra_furnished_rooms'
							&& name_field != 'field_extra_pickup'
							&& name_field != 'field_extra_dropoff'
							&& name_field != 'field_moving_from'
							&& name_field != 'field_moving_to'
							&& name_field != 'field_apt_from'
							&& name_field != 'field_apt_to'
							&& name_field != 'field_commercial_extra_rooms') {

							if (name_field == 'field_type_of_entrance_to_'
								|| name_field == 'field_type_of_entrance_from') {
								message += $scope.fieldData.field_lists.field_size_of_move[item];
							} else if (name_field == 'field_start_time') {

								message += $scope.fieldData.field_lists.field_start_time[item];

								switch (item) {
								case '1':
									initial_request.start_time1.value = '8:00 AM';
									initial_request.start_time2.value = '8:00 PM';
									break;
								case '2':
									initial_request.start_time1.value = '8:00 AM';
									initial_request.start_time2.value = '10:00 AM';
									break;
								case '3':
									initial_request.start_time1.value = '11:00 AM';
									initial_request.start_time2.value = '2:00 PM';
									break;
								case '4':
									initial_request.start_time1.value = '1:00 PM';
									initial_request.start_time2.value = '4:00 PM';
									break;
								case '5':
									initial_request.start_time1.value = '3:00 PM';
									initial_request.start_time2.value = '7:00 PM';
									break;
								}

								$scope.editrequest.field_actual_start_time = initial_request.start_time1.value;
								$scope.editrequest.field_start_time_max = initial_request.start_time2.value;
							} else if (name_field == 'field_size_of_move') {
								message += $scope.fieldData.field_lists.field_size_of_move[item];
							} else if (name_field == 'field_move_service_type') {
								_.forEach($scope.servicesTypes, function (service) {
									if (service.id == item) {
										message += service.name;
									}
								});
							} else {
								if (_.isNull(item) || _.isUndefined(item)) return;
								message += item.toString();
							}
						} else if (name_field == 'field_extra_furnished_rooms') {
							message = message.slice(0, -1);
							message += ':';

							_.forEach(item, function (room) {
								message += $scope.rooms[room] + ', ';
							});

							message = message.slice(0, -1);
						} else if (name_field == 'field_extra_pickup' || name_field == 'field_extra_dropoff') {
							if (item.thoroughfare
								&& item.thoroughfare != null
								&& item.thoroughfare != '') {

								message += item.thoroughfare + ' ';
							}

							if (item.premise
								&& item.premise != null
								&& item.premise != '') {

								message += item.premise + ' ';
							}

							if (item.locality
								&& item.locality != null
								&& item.locality != '') {

								message += item.locality + ' ';
							}

							if (item.administrative_area
								&& item.administrative_area != null
								&& item.administrative_area != '') {

								message += item.administrative_area;
							}

							if (item.postal_code
								&& item.postal_code != null
								&& item.postal_code != '') {

								message += ', ' + item.postal_code;
							}
						} else if (name_field == 'field_moving_from') {
							if (item.thoroughfare
								&& item.thoroughfare != null
								&& item.thoroughfare != '') {

								message += item.thoroughfare + ' ';
							}

							if ($scope.editrequest.field_apt_from
								&& $scope.editrequest.field_apt_from != null
								&& $scope.editrequest.field_apt_from != '') {

								message += $scope.editrequest.field_apt_from + ' ';
							}

							if (item.locality
								&& item.locality != null
								&& item.locality != '') {

								message += item.locality + ' ';
							}

							if (item.administrative_area
								&& item.administrative_area != null
								&& item.administrative_area != '') {

								message += item.administrative_area;
							}

							if (item.postal_code
								&& item.postal_code != null
								&& item.postal_code != '') {

								message += ', ' + item.postal_code;
							}
						} else if (name_field == 'field_moving_to') {
							if (item.thoroughfare
								&& item.thoroughfare != null
								&& item.thoroughfare != '') {

								message += item.thoroughfare + ' ';
							}

							if ($scope.editrequest.field_apt_to
								&& $scope.editrequest.field_apt_to != null
								&& $scope.editrequest.field_apt_to != '') {

								message += $scope.editrequest.field_apt_to + ' ';
							}

							if (item.locality
								&& item.locality != null
								&& item.locality != '') {

								message += item.locality + ' ';
							}

							if (item.administrative_area
								&& item.administrative_area != null
								&& item.administrative_area != '') {

								message += item.administrative_area;
							}

							if (item.postal_code
								&& item.postal_code != null
								&& item.postal_code != '') {

								message += ', ' + item.postal_code;
							}
						}
						// message = message.slice(0, -1);
						message += '; ';
					});

					message = message.slice(0, -2);
					message += '.';

					if (angular.isDefined($scope.editrequest['field_double_travel_time'])) {
						initial_request.field_double_travel_time.raw = $scope.editrequest['field_double_travel_time'];
					}

					// if we change service type
					if (storageRequest) {
						RequestServices
							.updateRequest(initial_request.nid, $scope.editrequest)
							.then(function () {
								forceReloadAccount();
								$rootScope.$broadcast('origin_address', {
									field_moving_to: $scope.request.field_moving_to,
									field_moving_from: $scope.request.field_moving_from
								});

								EditRequestServices.updateRequestLive(initial_request, $scope.editrequest);

								$scope.request = initial_request;

								initial_request.field_extra_dropoff = $scope.request.field_extra_dropoff;
								initial_request.field_extra_pickup = $scope.request.field_extra_pickup;
								initial_request.field_moving_from = $scope.request.field_moving_from;
								initial_request.field_moving_to = $scope.request.field_moving_to;
								saveParserRequestAllData(initial_request);

								setRequest($scope.request);
								$scope.editrequest = [];
								$scope.busy = false;
								SweetAlert.swal('Updated!', '', 'success');

								common.$timeout(function () {
									$uibModalInstance.close($scope.request);
								}, 1000);
							});
					} else {
						RequestServices
							.updateRequest(initial_request.nid, $scope.editrequest)
							.then(function () {
								$rootScope.$broadcast('origin_address', {
									field_moving_to: $scope.request.field_moving_to,
									field_moving_from: $scope.request.field_moving_from
								});

								EditRequestServices.updateRequestLive(initial_request, $scope.editrequest);

								$scope.request = initial_request;

								saveParserRequestAllData(initial_request);
								setRequest($scope.request);

								if (angular.isDefined($scope.editrequest.field_extra_dropoff)) {
									initial_request.field_extra_dropoff = $scope.editrequest.field_extra_dropoff;
								}

								if (angular.isDefined($scope.editrequest.field_extra_pickup)) {
									initial_request.field_extra_pickup = $scope.editrequest.field_extra_pickup;
								}

								if (angular.isDefined($scope.editrequest.field_moving_from)) {
									initial_request.field_moving_from = $scope.editrequest.field_moving_from;
								}

								if (angular.isDefined($scope.editrequest.field_moving_to)) {
									initial_request.field_moving_to = $scope.editrequest.field_moving_to;
								}

								$scope.editrequest = [];

								$scope.busy = false;
								SweetAlert.swal('Updated!', '', 'success');
								common.$timeout(function () {
									$uibModalInstance.close($scope.request);
								}, 1000);
							});
					}

					storageRequest = null;
				}

				$rootScope.$broadcast('addresses_added', $scope.request);

			});

		function saveParserRequestAllData(request) {
			if ($scope.isInvalidParsingRequest) {
				ParserServices.saveEntrance(request, $scope.editrequest);
			}
		}

		// calculate need for prepare some edit request field
		function calculate(request) {
			var deferred = $q.defer();
			var tempRequest = EditRequestServices.updateRequestLive(request, $scope.editrequest);

			if (angular.isDefined($scope.editrequest.field_moving_to)) {
				tempRequest.field_moving_to = $scope.editrequest.field_moving_to;
			}

			if (angular.isDefined($scope.editrequest.field_moving_from)) {
				tempRequest.field_moving_from = $scope.editrequest.field_moving_from;
			}

			if (!request.field_usecalculator.value) {
				deferred.resolve(true);

				return deferred.promise;
			}

			var calcDistancePromise = CalculatorServices.getDuration(tempRequest.field_moving_from.postal_code, tempRequest.field_moving_to.postal_code, tempRequest.service_type.raw);
			var promises = [calcDistancePromise];

			if ((!_.isString($scope.request.field_moving_to.administrative_area)
				|| _.isEmpty($scope.request.field_moving_to.administrative_area))
				&& _.isString($scope.request.field_moving_to.postal_code)
				&& $scope.request.field_moving_to.postal_code.length == 5) {

				promises.push(geoCodingService.geoCode($scope.request.field_moving_to.postal_code));
			} else {
				promises.push({});
			}

			if ((!_.isString($scope.request.field_moving_from.administrative_area)
				|| _.isEmpty($scope.request.field_moving_from.administrative_area))
				&& _.isString($scope.request.field_moving_from.postal_code)
				&& $scope.request.field_moving_from.postal_code.length == 5) {

				promises.push(geoCodingService.geoCode($scope.request.field_moving_from.postal_code));
			} else {
				promises.push({});
			}

			$q.all(promises)
				.then(result => {
					$scope.request.request_all_data.request_distance = result[0];
					var travelTime = result[0].duration;
					let calcDistanceResult = result[0];

					var flatRate = (initial_request.service_type.raw == 5 || initial_request.service_type.raw == 7);
					var requestStorage = (initial_request.service_type.raw == 2 || initial_request.service_type.raw == 6)
					var distance = calcDistanceResult.distances.AB.distance;
					let duration = calcDistanceResult.distances.AB.duration;
					var localFlatMiles = basicsettings.local_flat_miles;

					if (distance > localFlatMiles && !flatRate && !requestStorage) {
						travelTime += duration;
						duration = 0;
					}

					if (tempRequest.field_moving_from.postal_code != initial_request.field_moving_from.postal_code
						|| tempRequest.field_moving_to.postal_code != initial_request.field_moving_to.postal_code
						|| $scope.isInvalidParsingRequest) {

						$scope.editrequest[request.travel_time.field] = travelTime;

						if (isDoubleDriveTime) {
							let duration = result[0].distances.AB.duration;
							duration = Math.max(duration * 2, minCATravelTime);
							duration = CalculatorServices.getRoundedTime(duration);

							$scope.editrequest['field_double_travel_time'] = duration;
							tempRequest.field_double_travel_time.raw = duration;
						}
					}

					$scope.editrequest['field_distance'] = distance;
					$scope.editrequest['field_duration'] = duration;

					tempRequest.distance.value = distance;
					tempRequest.duration.value = duration;

					if (!_.isEmpty(result[2]) && _.get(result, '[2].state')) {
						$scope.request.field_moving_to.administrative_area = result[2].state;
					}

					if (!_.isEmpty(result[3]) && _.get(result, '[3].state')) {
						$scope.request.field_moving_from.administrative_area = result[3].state;
					}

					//check for long distance job.
					var longdistance = angular.fromJson($rootScope.fieldData.longdistance);
					var basicSettings = angular.fromJson($rootScope.fieldData.basicsettings);

					if (tempRequest.service_type.raw == 7) {
						var state = $scope.request.field_moving_to.administrative_area.toLowerCase();
						var baseState = $scope.request.field_moving_from.administrative_area.toUpperCase();

						if (basicSettings.islong_distance_miles
							&& calcDistanceResult.distance > basicSettings.long_distance_miles
							&& basicSettings.long_distance_miles) {

							let isNotParserRequest = !ParserServices.isParserRequest(request);

							if (isNotParserRequest && !longdistance.acceptAllQuotes) {
								if (!longdistance.stateRates[baseState][state].longDistance
									|| (!longdistance.stateRates[baseState][state]['state_rate']
									&& (!longdistance.stateRates[baseState][state]['rate'] || _.isEmpty(longdistance.stateRates[baseState][state]['rate'])))) {

									SweetAlert.swal("We don't do moves to this state!");
									return false;
								}
							}
						}
					}

					//if pickup only
					if (tempRequest.service_type.raw == 3) {
						tempRequest.distance.value = 0;
						tempRequest.duration.value = 0;
						$scope.editrequest['field_distance'] = 0;
						$scope.editrequest['field_duration'] = 0;

						if (tempRequest.field_extra_dropoff.postal_code && tempRequest.field_extra_dropoff.postal_code.length) {
							$scope.editrequest['field_extra_dropoff'] = tempRequest.field_extra_dropoff;
							$scope.editrequest['field_extra_dropoff']['postal_code'] = '';
						}
					}

					if (tempRequest.service_type.raw == 4) {
						tempRequest.distance.value = 0;
						tempRequest.duration.value = 0;
						$scope.editrequest['field_distance'] = 0;
						$scope.editrequest['field_duration'] = 0;

						if (tempRequest.field_extra_pickup.postal_code && tempRequest.field_extra_pickup.postal_code.length) {
							$scope.editrequest['field_extra_pickup'] = tempRequest.field_extra_pickup;
							$scope.editrequest['field_extra_pickup']['postal_code'] = '';
						}
					}

					//!important set date
					var _date = new Date(request.date.raw + 1000);
					request.moveDateTime = _date.getTime();
					request.moveDateFormat = moment(_date).format('MM/DD/YYYY');

					var total_weight = CalculatorServices.getRequestCubicFeet(tempRequest);
					var initial_weight = CalculatorServices.getRequestCubicFeet(request);
					var inventory_weight = InventoriesServices.calculateTotals(request.inventory.inventory_list);

					if (inventory_weight.cfs > 0) {
						total_weight.weight = inventory_weight.cfs;
					}

					// TODO Not Forget Make functional for California
					var results = service.calculateTime(tempRequest, total_weight.weight);

					if (!flatRate) {
						$scope.request.quote.max = results.quote.max;
					}

					if (initial_request.rate.value != results.rate.value
						|| $scope.isInvalidParsingRequest) {
						$scope.editrequest['field_price_per_hour'] = results.rate.value;
						$scope.alertmessage += "Rate will be change!";
					}

					if (initial_request.crew.value != results.crew.value) {
						$scope.editrequest['field_movers_count'] = results.crew.value;
					}

					if (initial_weight != total_weight) {
						$scope.alertmessage += "Rate will change!";
					}

					if (tempRequest.minimum_time.raw != initial_request.minimum_time.raw
						|| tempRequest.maximum_time.raw != initial_request.maximum_time.raw
						|| $scope.isInvalidParsingRequest) {

						$scope.editrequest['field_maximum_move_time'] = results.maximum_time.raw;
						$scope.editrequest['field_minimum_move_time'] = results.minimum_time.raw;
						$scope.alertmessage += "Estimated Time will change!";
					}

					deferred.resolve(true);
				}, () => {
					SweetAlert.swal("You entered the wrong zip code.", '', 'error');
					deferred.reject();
				});

			return deferred.promise;
		}
	}

	function prepareRequest(request) {
		var defer = $q.defer();
		var settings = angular.fromJson((datacontext.getFieldData()).basicsettings);
		var globalSettings = datacontext.getFieldData();
		var new_request = {};
		var user = request.uid;

		new_request.data = {
			status: 1,
			title: 'Move Request',
			uid: user.uid,
			field_approve: Number((_.invert(globalSettings.field_lists.field_approve))[' Pending']),
			field_move_service_type: (_.invert(globalSettings.field_lists.field_move_service_type))["Moving & Storage"],
			field_date: {
				date: moment(request.date.raw * 1000).format('YYYY-MM-DD'),
				time: '15:10:00'
			},
			field_size_of_move: request.move_size.raw,
			field_type_of_entrance_from: request.type_from.raw,
			field_type_of_entrance_to_: (_.invert(globalSettings.field_lists.field_type_of_entrance_from))["No Stairs - Ground Floor"],

			field_start_time: request.start_time.raw,
			field_price_per_hour: request.rate.value,
			field_movers_count: request.crew.value,

			field_distance: request.distance.value,
			field_travel_time: request.travel_time.raw,
			field_minimum_move_time: request.minimum_time.raw,
			field_maximum_move_time: request.maximum_time.raw,
			field_duration: request.duration.value,
			field_extra_furnished_rooms: request.rooms.raw,
			field_moving_to: {
				country: "US",
				administrative_area: request.field_moving_to.administrative_area,
				locality: request.field_moving_to.locality,
				postal_code: request.field_moving_to.postal_code,
				thoroughfare: request.field_moving_to.thoroughfare,
				premise: ""
			},
			field_apt_to: request.apt_to.value,
			field_apt_from: '',
			field_moving_from: {
				country: "US",
				administrative_area: settings.main_state || '',
				locality: settings.storage_city,
				postal_code: settings.parking_address,
				thoroughfare: settings.company_name,
				premise: ""
			},
			field_extra_pickup: {
				postal_code: null
			},
			field_extra_dropoff: {
				postal_code: null
			},
			field_storage_price: request.storage_rate.value,
			field_reservation_price: request.reservation_rate.value,
			field_request_settings: angular.copy(request.field_request_settings)
		};

		new_request.all_data = {toStorage: true};

		if (user.hasOwnProperty('field_user_first_name')) {
			new_request.data.field_first_name = user.field_user_first_name;
			new_request.data.field_last_name = user.field_user_last_name;
			new_request.data.field_e_mail = user.mail;
			new_request.data.field_phone = user.field_primary_phone;
			if (user.field_user_additional_phone != null) {
				new_request.data.field_additional_phone = user.field_user_additional_phone;
			}
		}

		geoCodingService.geoCode(settings.parking_address).then(function (result) {
			new_request.data.field_moving_from.administrative_area = result.state;

			var p1 = CalculatorServices.getDuration(new_request.data.field_moving_from.postal_code, new_request.data.field_moving_to.postal_code, (_.invert(globalSettings.field_lists.field_move_service_type))["Moving & Storage"]);

			p1.then(function (data) {
				new_request.data.field_travel_time = data['duration'];
				new_request.data.field_duration = data['distances'].AB.duration;
				new_request.data.field_distance = data['distances'].AB.distance;

				new_request.all_data.request_distance = data;

				delete new_request.data.field_extra_pickup;
				delete new_request.data.field_extra_dropoff;
				defer.resolve(new_request);
			});
		}, function () {
			SweetAlert.swal("You entered the wrong zip code.", '', 'error');
			new_request.field_moving_from.administrative_area = '';

			var p1 = CalculatorServices.getDuration(new_request.data.field_moving_from.postal_code, new_request.data.field_moving_to.postal_code, (_.invert(globalSettings.field_lists.field_move_service_type))["Moving & Storage"]);

			p1.then(function (data) {
				new_request.data.field_travel_time = data['duration'];
				new_request.data.field_duration = data['distances'].AB.duration;
				new_request.data.field_distance = data['distances'].AB.distance;

				new_request.all_data.request_distance = data;

				delete new_request.data.field_extra_pickup;
				delete new_request.data.field_extra_dropoff;
				defer.resolve(new_request);
			});
		});
		return defer.promise;

	}

	function reservationSignatureModal() {
		let deferred = $q.defer();
		var modalInstance = $uibModal.open({
			templateUrl: 'app/requestpage/services/signatureModal.html?4',
			controller: SignatureCtrl,
			size: 'lg',
			backdrop: 'static',
			keyboard: false,
		});

		modalInstance.result.then(function (resSign) {
			if (resSign) deferred.resolve(resSign);
			else deferred.reject('Error');
		});

		function SignatureCtrl($scope, $uibModalInstance) {
			$scope.canvas = document.getElementById('signatureCanvasReservation');

			function goSecondStep() {
				setTimeout(function () {
					$scope.canvas = document.getElementById('signatureCanvasReservation');
					if ($scope.canvas != null) {
						$scope.canvas.width = angular.element('#signatureReservationPad').find('.modal-body').innerWidth() - 60;
						$scope.signaturePad = new SignaturePad($scope.canvas);
					}
					else
						goSecondStep();
				}, 750);
			}

			goSecondStep();

			function getCropedSignatureValue() {
				let url;
				let originImgData;
				let xi, yi;
				let imageData, nCanvas;
				let ctx = $scope.signaturePad._ctx;
				let canvasWidth = $scope.canvas.width;
				let canvasHeight = $scope.canvas.height;
				let minX = canvasWidth;
				let minY = canvasHeight;
				let maxX = 0;
				let maxY = 0;
				let padding = 10;
				originImgData = ctx.getImageData(0, 0, canvasWidth, canvasHeight).data;
				for (let i = 0; i < originImgData.length; i += 4) {
					if (originImgData[i] + originImgData[i + 1] + originImgData[i + 2] + originImgData[i + 3] > 0) {
						yi = Math.floor(i / (4 * canvasWidth));
						xi = Math.floor((i - (yi * 4 * canvasWidth)) / 4);
						if (xi < minX) minX = xi;
						if (xi > maxX) maxX = xi;
						if (yi < minY) minY = yi;
						if (yi > maxY) maxY = yi;
					}
				}
				imageData = ctx.getImageData(minX, minY, maxX - minX, maxY - minY);
				nCanvas = document.createElement('canvas');
				nCanvas.setAttribute('id', 'nc');
				nCanvas.width = 2 * padding + maxX - minX;
				nCanvas.height = 2 * padding + maxY - minY;
				nCanvas.getContext('2d').putImageData(imageData, padding, padding);
				url = nCanvas.toDataURL().split(',')[1];
				$("canvas#nc").remove();
				return url;
			}

			function isCanvasBlank(elementID) {
				let canvas = document.getElementById(elementID);
				let blank = document.createElement('canvas');
				blank.width = canvas.width;
				blank.height = canvas.height;
				let context = canvas.getContext("2d");
				let perc = 0, area = canvas.width * canvas.height;
				if (canvas.toDataURL() == blank.toDataURL()) {
					return canvas.toDataURL() == blank.toDataURL();
				} else {
					let data = context.getImageData(0, 0, canvas.width, canvas.height).data;
					for (var ct = 0, i = 3, len = data.length; i < len; i += 4) if (data[i] > 50) ct++;
					perc = parseFloat((100 * ct / area).toFixed(2));
					return (perc < 0.1);
				}
			}

			$scope.saveSignature = function () {
				let signatureValue = getCropedSignatureValue();
				let SIGNATURE_PREFIX = 'data:image/gif;base64,';
				let signature = SIGNATURE_PREFIX + signatureValue;
				let blank = isCanvasBlank('signatureCanvasReservation');
				if (blank) {
					SweetAlert.swal('Failed!', 'Sign please', 'error');
					return false;
				} else {
					$scope.busy = true;
					$uibModalInstance.close(signature);
				}
			};

			$scope.clearSignature = function () {
				$scope.signaturePad.clear();
			};

			$scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};
		}

		return deferred.promise;
	}

	function getNotes(nid, data) {
		return {note: apiService.postData(moveBoardApi.requestNotes.getNotes + nid, data)};
	}

	function getAccountVariable(setting) {
		let deferred = $q.defer(),
			data = {
				name: setting
			};

		apiService.postData(moveBoardApi.core.getVariable, data).then(function (res) {
			let response = angular.fromJson(res.data[0] || res.data);
			deferred.resolve(response);
		});

		return deferred.promise;

	}
}
