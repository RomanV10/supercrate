'use strict';

angular
	.module('app.request')
	.directive('confirmationSignedInfo', confirmationSignedInfo);

confirmationSignedInfo.$inject = [];

function confirmationSignedInfo() {
	return {
		restrict: 'E',
		scope: {
			confirmationData: '<',
		},
		template: require('./confirmation-signed-info.tpl.html'),
	};
}
