import './read-more.styl'

(function () {
	'use strict';

	angular
		.module('app')
		.directive('readMore', readMore);

	function readMore() {
		return {
			restrict: 'EA',
			template: require('./readMore.html'),
			scope: {
				textforshow: '=',
				request: '<?',
			},
			link: readMoreLink
		};
		function readMoreLink ($scope, $element){
			const SMALL_HEIGHT = 50;
			let container = $element.find('div[move-board-compile="textforshow"]');
			$scope.isMore = false;
			$scope.isSmallText = false;

			$scope.$watch('textforshow', checkMinHeight);

			function checkMinHeight() {
				$scope.isSmallText = container[0].scrollHeight <= SMALL_HEIGHT;
			}

			$scope.toggleShow = function(){
				let height = container[0].scrollHeight;
				if (!$scope.isMore) {
					container.animate({
						'maxHeight': height + 'px'
					}, 500);
				} else {
					container.animate({
						'maxHeight': `${SMALL_HEIGHT}px`
					}, 500);
				}

				$scope.isMore = !$scope.isMore;
			};
		}
	}
})();
