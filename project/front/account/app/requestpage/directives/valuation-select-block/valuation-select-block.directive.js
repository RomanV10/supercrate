'use strict';

import './valuation-select-block.styl';

angular
	.module('app')
	.directive('valuationSelectBlock', valuationSelectBlock);

function valuationSelectBlock() {
	return {
		restrict: 'E',
		scope: {
			afterChange: '&',
			request: '=',
			invoice: '=',
			requestInterval: '=',
			isAdmin: '=',
		},
		template: require('./valuation-select-block.tpl.html'),
		controller: 'valuationSelectBlockCtrl',
	};
}
