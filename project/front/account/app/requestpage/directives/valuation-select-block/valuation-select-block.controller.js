angular
	.module('app')
	.controller('valuationSelectBlockCtrl', valuationSelectBlockCtrl);

/* @ngInject */
function valuationSelectBlockCtrl($scope, valuationService, $controller, SweetAlert, $uibModal, erEstimateSignatureProcessor) {
	const CONFIRMED = 3;

	$controller('ValuationBaseController', {
		$scope: $scope,
		request: $scope.request,
	});

	let valuationTitles = {
		1: {},
		2: {},
	};;

	$scope.openValuationAccountModalForFullValue = openValuationAccountModalForFullValue;

	init();

	function init() {
		$scope.baseCtrlInit(false);

		valuationTitles = angular.copy($scope.valuationTitles);

		parseSignatures();

		$scope.edit_amount_of_valuation = valuationService.getSetting('edit_amount_of_valuation');
		$scope.valuation_proposal_to_contact = valuationService.getSetting('valuation_proposal_to_contact');
		$scope.is_not_edit_amount_of_liability = !valuationService.getSetting('edit_amount_of_liability');
		$scope.valuation_account_show_setting = valuationService.getSetting('valuation_account_show_setting');
		$scope.service_types = valuationService.getSetting('service_types');
		$scope.valuationTypes = valuationService.VALUATION_TYPES;
		$scope.readOnly = $scope.isAdmin || !$scope.edit_amount_of_valuation;
		$scope.hideSelectBlock = _.isEqual($scope.valuation.selected.valuation_type, $scope.valuationTypes.FULL_VALUE)
			&& !$scope.edit_amount_of_valuation
			&& !$scope.valuation.other.set_by_manager;

		$scope.$watch('valuation', onChangeValuation, true);
		$scope.$watch('request.status.raw', changeStatus);
		changeStatus($scope.request.status.raw);
		initValuationCheckbox($scope.request.request_all_data.valuation.selected.valuation_type);
	}

	function onChangeValuation(current, original) {
		if (!_.isEqual(current, original)) {
			let valuation_type = $scope.valuation.selected.valuation_type;
			let title = $scope.valuationTitles[valuation_type].title;
			let text = [];
			let isChangedValuationType = current.selected.valuation_type != original.selected.valuation_type;

			if (isChangedValuationType) {
				text.push({simpleText: `Client updated the valuation plan to ${title}`});
			}

			let isChangedDeductibleLevel = current.selected.deductible_level != original.selected.deductible_level;

			if (isChangedDeductibleLevel) {
				text.push({simpleText: `Client changed deductible level to ${current.selected.deductible_level}`});
			}

			let isSelectedFullValueProtection = current.selected.valuation_type == $scope.valuationTypes.FULL_VALUE;
			let isChangedLiabilityAmount = current.selected.liability_amount != original.selected.liability_amount;

			if (isSelectedFullValueProtection && isChangedLiabilityAmount) {
				text.push({simpleText: `Client changed liability amount to ${current.selected.liability_amount}`});
			}

			$scope.afterChange({text});
			initValuationCheckbox(valuation_type);
		}
	}

	function changeStatus (current) {
		$scope.isCannotEditValuation = current == CONFIRMED || !!$scope.isAdmin;
	}

	function initValuationCheckbox(valuation_type) {
		$scope.is_checked_valuation_per_pound = valuation_type == $scope.valuationTypes.PER_POUND;
		$scope.is_checked_valuation_full_value_protection = valuation_type == $scope.valuationTypes.FULL_VALUE;
	}

	function openValuationAccountModalForFullValue () {
		let valuationModal = $uibModal.open({
			template: require('./valuation-modals/full-value-protection.tpl.html'),
			controller: 'FullValueProtectionController',
			size: 'lg',
			backdrop: false,
			resolve: {
				request: () => $scope.request,
				readOnly: () => $scope.readOnly,
			},
		});

		valuationModal.result
			.then(result => {
				$scope.request.request_all_data.valuation = result.valuation;
				$scope.valuation = result.valuation;
			})
			.finally(() => {
				initValuationCheckbox($scope.request.request_all_data.valuation.selected.valuation_type);
			});
	}

	$scope.$watch('request.generalCustomBlockReservationSignature.value', parseSignatures);

	function parseSignatures() {
		$scope.valuationTitles[1].explanation = erEstimateSignatureProcessor.parseGeneralCustomBlockSignatures(valuationTitles[1].explanation, $scope.request)
		$scope.valuationTitles[2].explanation = erEstimateSignatureProcessor.parseGeneralCustomBlockSignatures(valuationTitles[2].explanation, $scope.request)
	}
}
