'use strict';

import './full-value-protection.styl';

angular
	.module('app')
	.controller('FullValueProtectionController', FullValueProtectionController);

FullValueProtectionController.$inject = ['$scope', '$uibModalInstance', 'request', 'readOnly', 'valuationService', '$controller', 'SweetAlert'];

function FullValueProtectionController ($scope, $uibModalInstance, request, readOnly, valuationService, $controller, SweetAlert) {
	$controller('ValuationBaseController', {
		$scope: $scope,
		request: request,
	});
	$scope.clickSave = clickSave;
	$scope.clickClose = clickClose;
	
	function init () {
		$scope.baseCtrlInit(true);
		$scope.is_not_edit_amount_of_liability = !valuationService.getSetting('edit_amount_of_liability');
		$scope.valuation_account_show_setting = valuationService.getSetting('valuation_account_show_setting');
		$scope.valuation_proposal_to_contact = valuationService.getSetting('valuation_proposal_to_contact');
		$scope.edit_amount_of_valuation = valuationService.getSetting('edit_amount_of_valuation');
		
		if ($scope.valuation.selected.valuation_type !== $scope.valuationTypes.FULL_VALUE) {
			$scope.setValuationType($scope.valuationTypes.FULL_VALUE);
		}
		
		$scope.readOnly = readOnly;
	}
	
	function clickSave() {
		if (readOnly)
			clickClose();
		else
			$uibModalInstance.close({
				valuation: $scope.valuation,
			});
	}
	
	function clickClose() {
		$uibModalInstance.dismiss('cancel');
	}
	
	init();
}
