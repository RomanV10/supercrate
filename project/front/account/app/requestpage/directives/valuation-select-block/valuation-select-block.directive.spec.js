describe('directive: valuationSelectBlock,', () => {
	let $parentScope, $scope, element, valuationService, moveBoardApi, CalculatorServices,
		request;

	function compileElement() {
		element = $compile(`<valuation-select-block
			after-change="afterChangeValuation(text)"
			request="vm.request"
			invoice="vm.invoice"
			is-admin="isAdmin"></valuation-select-block>`)($parentScope);
		$scope = element.isolateScope();
		$scope.$apply();
	}
	
	beforeEach(inject((_valuationService_, _moveBoardApi_, _CalculatorServices_) => {
		CalculatorServices = _CalculatorServices_;
		moveBoardApi = _moveBoardApi_;
		valuationService = _valuationService_;
		request = testHelper.loadJsonFile('valuation_select_block_request.mock');

		spyOn(valuationService, 'getSetting').and.callThrough();
		$parentScope = $rootScope.$new();
		$parentScope.afterChangeValuation = () => {};
		$parentScope.vm = {
			request,
			invoice: {}
		};
		$parentScope.isAdmin = false;
		compileElement();
	}));

	describe('After init,', () => {
		it('$scope.valuationTitles should be defined', () => {
			expect($scope.valuationTitles).toBeDefined();
		});
		it('Should call SettingServices.getSettings', () => {
			expect(valuationService.getSetting).toHaveBeenCalled();
		});
		it('Should write value from variable to scope if setted', () => {
			expect($scope.valuationTitles[1].title).toEqual('60 cents per pound');
		});
		it('Should write default value to scope if not exist', () => {
			expect($scope.valuationTitles[2].title).toEqual('Full Value Protection');
		});
		it('Should is_checked_valuation_per_pound be false', () => {
			expect($scope.is_checked_valuation_per_pound).toBe(false);
		});
		it('Should is_checked_valuation_full_value_protection be true', () => {
			expect($scope.is_checked_valuation_full_value_protection).toBe(true);
		});
		it('Should is_checked_valuation_full_value_protection be set', () => {
			//given
			let expectedResult = '<h1>To get an insurance, please contact your relocation manager</h1>'

			//when

			//then
			expect($scope.valuation_proposal_to_contact).toBe(expectedResult);
		});
		it('Should isCannotEditValuation to be false', () => {
			//given
			let expectedResult = false;

			//when

			//then
			expect($scope.isCannotEditValuation).toBe(expectedResult);
		});
	});

	describe('function setValuationType() 60 cents per pound,', () => {
		let SIXTY_CENTS_PER_POUND_TYPE;
		beforeEach(() => {
			SIXTY_CENTS_PER_POUND_TYPE = '1';
			let LIABILITY_AMOUNT = '1';
			$scope.request.request_all_data.valuation.selected.lability_amount = LIABILITY_AMOUNT;
			spyOn($parentScope, 'afterChangeValuation');
			spyOn(CalculatorServices, 'getTotalWeight').and.callFake(() => {return 555});
			$scope.setValuationType(SIXTY_CENTS_PER_POUND_TYPE);
			$scope.$apply();
		});

		it('Should call parent\'s method for saving', () => {
			expect($parentScope.afterChangeValuation).toHaveBeenCalledWith([
				{simpleText: 'Client updated the valuation plan to 60 cents per pound'}
			]);
		});

		it('Should set property request.valuation.valuation_type', () => {
			expect($parentScope.vm.request.request_all_data.valuation.selected.valuation_type).toEqual(SIXTY_CENTS_PER_POUND_TYPE);
		});

		it('is_checked_valuation_per_pound to be true', () => {
			expect($scope.is_checked_valuation_per_pound).toBe(true);
		});

		it('is_checked_valuation_full_value_protection to be false', () => {
			expect($scope.is_checked_valuation_full_value_protection).toBe(false);
		});
	});

	describe('function setValuationType() full value protection,', () => {
		let FULL_VALUE_TYPE;
		beforeEach(() => {
			FULL_VALUE_TYPE = '2';
			let LIABILITY_AMOUNT = '1';
			$scope.request.request_all_data.valuation.selected.lability_amount = LIABILITY_AMOUNT;
			spyOn($parentScope, 'afterChangeValuation');
			spyOn(CalculatorServices, 'getTotalWeight').and.callFake(() => {return 555});
			$scope.setValuationType(FULL_VALUE_TYPE);
			$scope.$apply();
		});

		it('Should call parent\'s method for saving', () => {
			expect($parentScope.afterChangeValuation).toHaveBeenCalledWith([]);
		});

		it('Should set property request.valuation.valuation_type', () => {
			expect($parentScope.vm.request.request_all_data.valuation.selected.valuation_type).toEqual(FULL_VALUE_TYPE);
		});

		it('is_checked_valuation_per_pound to be false', () => {
			expect($scope.is_checked_valuation_per_pound).toBe(false);
		});

		it('is_checked_valuation_full_value_protection to be true', () => {
			expect($scope.is_checked_valuation_full_value_protection).toBe(true);
		});
	});

	describe('isCannotEditValuation', () => {
		describe('should be true when', () => {
			let expectedResult = true;

			it('admin open account', () => {
				//given
				$parentScope.isAdmin = true;

				//when
				$scope.request.status.raw = 2;
				$scope.$apply();


				//then
				expect($scope.isCannotEditValuation).toBe(expectedResult);
			});

			it('when status is not pending & not not confirmed', () => {
				//given
				$parentScope.isAdmin = false;

				//when
				$scope.request.status.raw = 3;
				$scope.$apply();

				//then
				expect($scope.isCannotEditValuation).toBe(expectedResult);
			});
		});

		describe('should be false when', () => {
			let expectedResult = false;

			it('customer open account', () => {
				//given
				$parentScope.isAdmin = false;

				//when
				$scope.request.status.raw = 2;
				$scope.$apply();

				//then
				expect($scope.isCannotEditValuation).toBe(expectedResult);
			});

			it('when status is pending', () => {
				//given
				$parentScope.isAdmin = false;

				//when
				$scope.request.status.raw = 1;
				$scope.$apply();

				//then
				expect($scope.isCannotEditValuation).toBe(expectedResult);
			});

			it('when status is not confirmed', () => {
				//given
				$parentScope.isAdmin = false;

				//when
				$scope.request.status.raw = 2;
				$scope.$apply();

				//then
				expect($scope.isCannotEditValuation).toBe(expectedResult);
			});
		});
	});

	describe('when change weight and init should not write log when selected released value', () => {
		beforeEach(() => {
			request.request_all_data.valuation.selected.valuation_type = 1;
			$scope.$apply();
			spyOn($parentScope, 'afterChangeValuation');
		});

		it('should not write log amount of liability when selected released value', () => {
			//given
			let expectedResult = [];

			//when
			request.request_all_data.valuation.selected.liability_amount = 1000;
			$scope.$apply();

			//then
			expect($parentScope.afterChangeValuation).toHaveBeenCalledWith(expectedResult);
		});
	});
});
