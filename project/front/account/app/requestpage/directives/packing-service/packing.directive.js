(function () {
    'use strict';

    angular
        .module('app.request')
        .directive('ccPackingLink', ccPackingLink);


    function ccPackingLink (PaymentServices,$location, datacontext, SettingServices) {
        var directive = {
           link: link,
            scope: {
                'request': '=',
            },
            templateUrl: 'app/requestpage/directives/packing-service/packingLink.html',
            restrict: 'A'
        };
        return directive;



        function link(scope, element, attrs) {

            scope.busy = true;
            scope.value = 0;
            scope.active = false;
            scope.packingSettings = [];
            scope.fieldData = datacontext.getFieldData();
            scope.busy = false;
            scope.packingSettings = angular.fromJson(scope.fieldData.packing_settings);
            scope.settings =  scope.fieldData.basicsettings;
            scope.settings =  angular.fromJson(scope.settings);
            if(angular.isUndefined(scope.settings.packing_settings)){
                scope.packing = new Object();
                scope.packing.account = false;
                scope.packing.labor = false;
                var packing_settings = {
                    packingAccount: false,
                    packingLabor: false,
                }
                scope.settings.packing_settings = packing_settings;
                var setting = scope.settings;
                var setting_name = 'basicsettings';
                // SettingServices.saveSettings(setting,setting_name);
            }else{
                scope.packing = new Object();
                scope.packing.account = false;
                scope.packing.labor = false;
                if(angular.isDefined(scope.settings.packing_settings.packingAccount))
                    scope.packing.account = scope.settings.packing_settings.packingAccount;
                if(angular.isDefined(scope.settings.packing_settings.packingLabor))
                    scope.packing.labor = scope.settings.packing_settings.packingLabor;
            }
            // scope.requestType = (scope.request.service_type.raw == '5' || scope.request.service_type.raw == '7');
            // if(angular.isDefined(scope.request.request_data))
            //     if(angular.isDefined(scope.request.request_data.value))
            //         if(scope.request.request_data.value != "")
            //             scope.packingSettings = angular.fromJson(scope.request.request_data.value).packings;
            // angular.forEach(scope.packingSettings, function(value, key){
            //     if(value.laborRate){
            //         scope.packingSettings[key].total = Number(value.quantity) * (Number(value.rate) + Number(value.laborRate));
            //     }else{
            //         scope.packingSettings[key].total = Number(value.quantity) * Number(value.rate);
            //     }

            // });






        }





    }
})();
