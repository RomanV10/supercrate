(function () {
  
    angular
        .module('app.request')
    .factory('PackingServices',PackingServices);

    PackingServices.$inject = ['$http','$rootScope','$q','SweetAlert', 'datacontext'];
    
    function PackingServices($http,$rootScope,$q,SweetAlert, datacontext) {
        
        var service = {};
        service.getPackingSettings = getPackingSettings;
        return service;

        function getPackingSettings(type){
            return datacontext.getSettings(type);
        }
        
}
});