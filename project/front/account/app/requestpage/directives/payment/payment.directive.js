(function () {
    'use strict';

    angular
        .module('app.request')
        .directive('ccPaymentLink', ccPaymentLink);

    function ccPaymentLink (PaymentServices,$location) {
        var directive = {
           link: link,
            scope: {
                'request': '=',
            },
            templateUrl: 'app/requestpage/directives/payment/paymentLink.html?3',
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {

            //load payments

            scope.button_name = "PROCEED TO SECURE PAYMENT";
            if(angular.isDefined(attrs.name)){
                scope.button_name =attrs.name;
            }

            scope.company = "";
            if(angular.isDefined(attrs.company)){
                scope.company ='BFRM';
            }

            scope.host = $location.host();

            scope.busy = true;
            scope.value = 0;
            scope.active = false;

            attrs.$observe('amount',function (nval) {

                if(nval.length)
                    nval = parseFloat(nval);
                else
                    return;

                if(angular.isDefined(nval) && angular.isNumber(nval)){
                    scope.busy = true;
                    PaymentServices.getPaymentFingerPrint(nval).then(function(data){
                        scope.busy = false;
                        scope.value = nval;
                        scope.active = true;
                        scope.button_name = "PROCEED TO SECURE PAYMENT";
                        scope.fingerPrint = data.fingerPrint;
                        scope.api_login_id = data.api_login_id;
                        scope.fp_timestamp = data.fp_timestamp;
                        scope.fp_sequence = data.fp_sequence;
                    });
                }
            });
        }
    }
})();
