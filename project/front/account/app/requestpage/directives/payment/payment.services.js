(function () {

    angular
        .module('app.request')
    .factory('PaymentServices',PaymentServices);

    PaymentServices.$inject = ['$http','$rootScope','$q','config','$uibModal',
    'RequestServices','common','SweetAlert'];

    function PaymentServices($http,$rootScope,$q,config,$uibModal,RequestServices,common,SweetAlert) {

        var service = {};

        var extraServices = [];
        var authModalInstance = [];

        service.getPaymentFingerPrint = getPaymentFingerPrint;
        service.openPaymentModal = openPaymentModal;
        service.openAddServicesModal = openAddServicesModal;
        service.addExtraService = addExtraService;
        service.removeExtraService = removeExtraService;
        service.getExtraServiceTotal = getExtraServiceTotal;
        service.saveExtraServices = saveExtraServices;
        service.getPaymentArray = getPaymentArray;
        service.calcPayment = calcPayment;
        service.openReceiptModal = openReceiptModal;
        service.openAuthPaymentModal = openAuthPaymentModal;
        service.closeAuthModal = closeAuthModal;
        service.calculateRowTotal = calculateRowTotal;

        return service;

        function calcPayment(payments){

            var total = 0;
            angular.forEach(payments,function(payment,id){

                if(!payment.pending)
                    total += parseFloat(payment.amount);

            });


            return total;
        }
        function getPaymentArray(){

            var receipt =
            {
                account_number: "XXXX1111",
                amount: "25.00",
                auth_code: "V0T6XO",
                card_type: "Visa",
                date: "December 20, 2015, 10:28 pm",
                description: "Move Reservation #61992",
                email: "roma4ke@gmail.com",
                first_name: "Roman",
                invoice_id: "61992",
                last_name: "Halavach",
                payment_method: "CC",
                phone: "(201) 680-8055",
                transaction_id: "2247203984",
                pending:false,
            };
            return receipt;
        }
        function saveExtraServices(nid,value) {
            var deferred = $q.defer();
            var data = {};
            data.nid = nid;
            data.data = value;

            $http
                .post(config.serverUrl+'server/move_request/set_payment',data)
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }
        function getPaymentFingerPrint(value) {

            var deferred = $q.defer();

            var data = {
                'amount': value,
            }

            $http
                .post(config.serverUrl+'server/move_request/fingerprint',data)
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(data) {
                    deferred.reject(data);
                });

            return deferred.promise;

        }
        function openPaymentModal(request, entity) {

                   var modalInstance = $uibModal.open({
                        templateUrl: 'app/requests/directives/payment/paymentModal.html?2',
                        controller: PaymentModalCtrl,
                        size: 'lg',
                          resolve: {
							request: function () {
								return request;
							},
							entity: function() {
								return entity
							}
                        },
                    });
                    modalInstance.result.then(function ($scope) {
                        });

        }
        function PaymentModalCtrl($scope, $uibModalInstance,request, datacontext, entity) {

            $scope.fieldData = datacontext.getFieldData();
            $scope.basicSettings =  angular.fromJson($scope.fieldData.basicsettings);
		    $scope.request = request;
		    $scope.entity = entity;
		    $scope.receipts = request.reciepts;
            $scope.activeRow = -1;
            var searchPayment = false;
            $scope.busy = true;
            $scope.receiptLoader = false;
            function sortRefundReceipts() {
                var refundsArr = [];
                _.forEachRight($scope.receipts, function (receipt, ind) {
                    if(receipt.refunds){
                        refundsArr.push(receipt);
                        $scope.receipts.splice(ind, 1);
                    }
                });
                _.forEach(refundsArr, function (refund) {
                    var ind = _.findIndex($scope.receipts, {id: refund.receipt_id.toString() });
                    $scope.receipts.splice(ind+1, 0,refund);
                });
                $scope.busy = false;
                $scope.receiptLoader = false;
            }
            sortRefundReceipts();
            $scope.showReceipt = function(id){
                service.openReceiptModal($scope.receipts[id]);
            };
            $scope.addAuthPayment = function(){
                service.openAuthPaymentModal(request);
                activate();
            };

            $scope.dateFormat = function (date) {
                return _.isNaN(moment.unix(date)._i) ? moment(date).format('DD MMM, YYYY') : moment.unix(date).format('DD MMM, YYYY h:mm a');
            };



                $scope.calculatePayments = function (){
                    return service.calcPayment($scope.receipts);
                }
                   $scope.addPayment = function() {

                       //update Request
                       // Redirect to Payment Link

                   }
                    $scope.cancel = function() {
                        $uibModalInstance.dismiss('cancel');
                        searchPayment = true;
                    };

            $uibModalInstance.result.then(function () {
                // not called... at least for me
            }, function () {
                searchPayment = true;
            });

            $scope.customType = function (receipt) {
                switch (receipt.type) {
                    case "cash":
                        return '(cash)';
                        break;
                    case 'check':
                        return '(check)';
                        break;
                    case 'creditcard':
                        return '(card)';
                        break;
                    default:
                        return 'Payment';
                }
            };

            function activate(){

                common.$timeout(function () {
                    var nid = $scope.request.nid;
                    RequestServices.getRequestsByNid(nid).then(function(data) {

                        //check if we have new receipts !
                        //if not reactiveate
                        // broadcast stop activeation when close window
                        //
                        if($scope.receipts.length < data[nid].reciepts.length){
                            $scope.receipts = data[nid].reciepts;
                            SweetAlert.swal("Payment was received!", "", "success");
                            searchPayment = true;
                            sortRefundReceipts();
                            service.closeAuthModal();
                            $rootScope.$broadcast('request.payment_receipts',$scope.receipts);
                        }
                         else {
                            if(!searchPayment)
                                activate();
                        }


                    });
                }, 3500);

            }
            $scope.$on('receipt_sent', function() {
                $scope.receiptLoader = true;
            });

       }

        // ADD SERVICES MODULES
        function openAddServicesModal(request) {

            var modalInstance = $uibModal.open({
                templateUrl: 'app/requests/directives/payment/addServicesModal.html',
                controller: ModalInstanceCtrl,
                size: 'lg',
                resolve: {
                    request: function () {
                        return request;
                    },
                },
            });

            modalInstance.result.then(function ($scope) {
            });

        }
		/* @ngInject */
        function ModalInstanceCtrl($scope, $uibModalInstance,request) {
            $scope.request = request;
            $scope.total_services = 0;
            $scope.add_extra_charges = [];
            if(angular.isDefined(request.extraServices)){
                $scope.add_extra_charges  =  angular.fromJson(request.extraServices);
            }
            //Functions
            $scope.addExtraCharges = addExtraCharges;
            $scope.removeCharge =  removeCharge;
            $scope.calculateTotals = calculateTotals;
            $scope.calculateRowTotal = calculateRowTotal;
            $scope.extra_charges = [];
            var custom_extra =
                {
                    name: 'Custom Extra',
                    edit: true,
                    extra_services: [
                        {
                            services_default_value: 1,
                            services_name: "Cost",
                            services_read_only: false,
                            services_type: "Amount"
                        },
                        {
                            services_default_value: 1,
                            services_name: "Value",
                            services_read_only: false,
                            services_type: "Number"
                        },
                        {
                            services_default_value: 1,
                            services_name: "Weight",
                            services_read_only: false,
                            services_type: "Weight"
                        },
                    ]
                };
            function addExtraCharges(extra_charge){
                var charge = angular.copy(extra_charge);
                $scope.add_extra_charges.push(charge);
                calculateTotals();
            }
            function removeCharge(index){
                $scope.add_extra_charges.splice(index, 1);
                calculateTotals();

            }

            function calculateRowTotal(service){
                var total = 0;
                var one = 1;
                angular.forEach(service.extra_services, function(charge,index){
                    var value = parseFloat(charge.services_default_value);
                    one *=value;
                });
                total = one;
                return total;
            }

            function calculateTotals(extraServices){
                    var add_extra_charges = $scope.add_extra_charges;
                    if(angular.isDefined(extraServices)){
                        add_extra_charges = extraServices;
                    }
                    $scope.total_services = 0;
                    //Calculate Function
                    angular.forEach(add_extra_charges, function(charge,index){
                        $scope.total_services += calculateRowTotal(charge);
                    });

                    return parseFloat($scope.total_services,2);
            }





            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            $scope.save  = function(){
                $scope.request.extraServices = $scope.add_extra_charges;
                saveExtraServices($scope.request.nid,$scope.add_extra_charges);
                $rootScope.$broadcast('request.payment_update',$scope.add_extra_charges);
                $uibModalInstance.dismiss('cancel');
            }
        }
        function addExtraService(name,value){
            //1.load extra services for this request
            var extraService = initBasicService(name,value);

        }
        function removeExtraService(request,name){
            //1.load extra services for this request
            _.find(request.extraServices, function(charge, chargeIdx){
                if(charge.name == name){
                    removeCharge(chargeIdx);
                    return true;
                };
            });

        }
        function getExtraServiceTotal(extraServices){
            var add_extra_charges = [];
            if(angular.isDefined(extraServices)){
                add_extra_charges = angular.fromJson(extraServices);
            }
            var total_services = 0;
            //Calculate Function
            angular.forEach(add_extra_charges, function(charge,index){
                total_services += calculateRowTotal(charge);
            });

            return parseFloat(total_services);
        }
        function calculateRowTotal(service){
            var total = 0;
            var one = 1;
            angular.forEach(service.extra_services, function(charge,index){
                var value = parseFloat(charge.services_default_value);
                one *=value;
            });
            total = one;
            return total;
        }



        //RECEIPT MODAL
        function openReceiptModal(receipt) {

            var modalInstance = $uibModal.open({
                templateUrl: 'app/requestpage/directives/payment/receiptModal.html',
                controller: ReceiptModalCtrl,
                resolve: {
                    receipt: function () {
                        return receipt;
                    },
                },
            });
            modalInstance.result.then(function ($scope) {
            });
        }
        function ReceiptModalCtrl($scope, $uibModalInstance,receipt) {

            $scope.receipt = receipt;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };


            $scope.printDiv = function(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var popupWin = window.open('', '_blank',  'width=800,height=600');
                popupWin.document.open()
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
                popupWin.document.close();
            }

            $scope.dateFormat = function (date) {
                return _.isNaN(moment.unix(date)._i) ? moment(date).format('DD MMM, YYYY') : moment.unix(date).format('DD MMM, YYYY h:mm a');
            };

        }
        //Auth MODAL

        function saveAuthModal(modalInstance){
            authModalInstance = modalInstance;
        }
        function closeAuthModal(){
            authModalInstance.dismiss('cancel');
        }
        function openAuthPaymentModal(request) {

            var modalInstance = $uibModal.open({
                templateUrl: '/app/requests/directives/payment/authModal.html',
                controller: AuthModalCtrl,
                resolve: {
                    request: function () {
                        return request;
                    }
                }
            });
            saveAuthModal(modalInstance);
            modalInstance.result.then(function ($scope) {
            });
        }
        function AuthModalCtrl($scope, $uibModalInstance,request) {

            $scope.request = request;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };

        }




 }})();
