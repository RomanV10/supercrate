'use strict';

import '../../../../../moveBoard/app/commercial-move-calculator/commercial-move-request.component/commercial-move-sizes.styl';

angular.module('app.account').directive('commercialMoveSizeSelect', commercialMoveSizeSelect);

commercialMoveSizeSelect.$inject = ['CommercialCalc', 'datacontext', '$rootScope', 'RequestServices'];

function commercialMoveSizeSelect(CommercialCalc, datacontext, $rootScope, RequestServices) {
	return {
		restrict: 'AE',
		scope: {
			editRequest: '=',
			message: '=?',
			request: '=?',
			confirmed: '='
		},
		template: require('./commercial-move-size-select.tpl.pug'),
		link: commercialMoveSizeSelectLink
	};
	
	function commercialMoveSizeSelectLink($scope) {
		let fieldData = datacontext.getFieldData();
		let calcSetting = angular.fromJson(fieldData.calcsettings);
		$scope.commercialMoveSizes = calcSetting.commercialExtra;
		
		$scope.updateExtraCommercialField = updateExtraCommercialField;
		
		function init() {
			
			angular.element('#commercial-select').addClass('full-width');
			CommercialCalc.getCommercialSetting().then(resolve => {
				$scope.commercialMoveSizes = resolve.extra;
				
				if ($scope.request.field_custom_commercial_item.name != null) {
					$scope.commercialMoveSizes.push($scope.request.field_custom_commercial_item);
				}
				
				switch (Number($scope.request.field_useweighttype.value)) {
					case 2:
						$scope.commercialSize = CommercialCalc.getCurrentCommercialForInventory(
							$scope.request.commercial_extra_rooms.value,
							$scope.request.field_custom_commercial_item
						);
						break;
					case 3:
						$scope.commercialSize = $scope.request.field_custom_commercial_item.id;
						break;
					case 1:
					default:
						$scope.commercialSize = $scope.request.commercial_extra_rooms.value[0];
						break;
				}
			});
		}
		
		function updateExtraCommercialField(current) {
			
			let isCustom = $scope.request.field_custom_commercial_item.id === current;
			let pairedId = $scope.request.storage_id || $scope.request.request_all_data.packing_request_id;
			
			if (isCustom) {
				$scope.request.custom_weight.value = Number($scope.request.field_custom_commercial_item.cubic_feet);
				$scope.editRequest[$scope.request.custom_weight.field] = $scope.request.custom_weight.value;
				$scope.editRequest.field_commercial_extra_rooms = [];
				$scope.editRequest.field_custom_commercial_item = $scope.request.field_custom_commercial_item;
				$scope.request.field_custom_commercial_item.is_checked = true;
				$scope.editRequest.field_custom_commercial_item = angular.copy($scope.request.field_custom_commercial_item);
				
				$scope.request.field_useweighttype.value = 3;
				$scope.editRequest.field_useweighttype = 3;
				throwCalculatedCommercialToAccount();
			} else {
				$scope.request.field_custom_commercial_item.is_checked = false;
				
				$scope.editRequest.field_custom_commercial_item = angular.copy($scope.request.field_custom_commercial_item);
				if (_.find(calcSetting.commercialExtra, {id: current})) {
					$scope.request.commercial_extra_rooms.value = [current];
					$scope.editRequest.field_commercial_extra_rooms = [current];
					$scope.request.total_weight = CommercialCalc.getCommercialCubicFeet(current, calcSetting, $scope.request.field_custom_commercial_item);
				} else {
					$scope.request.commercial_extra_rooms.value = [];
					$scope.editRequest.field_commercial_extra_rooms = [];
					$scope.request.total_weight = CommercialCalc.getCommercialCubicFeet($scope.request.commercial_extra_rooms.value, calcSetting, $scope.request.field_custom_commercial_item);
				}

				$scope.request.field_useweighttype.value = 1;
				$scope.editRequest.field_useweighttype = 1;
				throwCalculatedCommercialToAccount();
			}
			
			if (!_.isUndefined(pairedId)) {
				RequestServices.updateRequest(pairedId, {
					field_commercial_extra_rooms: $scope.request.commercial_extra_rooms.value,
					field_custom_commercial_item: $scope.request.field_custom_commercial_item,
					field_useweighttype: $scope.request.field_useweighttype.value
				});
			}
		}

		function throwCalculatedCommercialToAccount() {
			$rootScope.$broadcast('getCommercialWeight', {
				weightType: $scope.request.field_useweighttype.value,
				currentExtra: $scope.request.commercial_extra_rooms,
			});
		}
		
		init();
	}
}