'use strict';

angular
	.module('app')
	.directive('confirmationPageValuation', confirmationPageValuation);

confirmationPageValuation.$inject = [];

function confirmationPageValuation() {
	return {
		restrict: 'E',
		template: require('./confirmation-page-valuation.tpl.html'),
		scope: {
			request: '=',
		},
		controller: 'confirmationPageValuationCtrl'
	};
}