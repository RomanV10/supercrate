describe('confirmation-page-valuation.controller:', () => {
	let request, valuationPlans, valuationService, CalculatorServices, $scope, $controller;
	
	beforeEach(inject((_valuationService_, _CalculatorServices_, _$controller_) => {
		$controller = _$controller_;
		valuationService = _valuationService_;
		CalculatorServices = _CalculatorServices_;
		valuationService.VALUATION_TYPES = {
			PER_POUND: 1, FULL_VALUE: 2,
		};
		let fakeSettings = {
			valuation_titles: {
				1: {title: 'title1'}, 2: {title: 'title2'}
			},
			show_valuation_confirmation_page_global_setting: {
				show: true
			},
			show_valuation_confirmation_page_setting: true,
			confirmation_table_show: false,
		};
		spyOn(valuationService, 'getSetting').and.callFake((name) => fakeSettings[name]);
		spyOn(CalculatorServices, 'getTotalWeight').and.callFake(() => {
			return {weight: 400};
		});
		spyOn(valuationService, 'calculateValuation').and.callFake(paramsForCalculating => {
			return {valuation_charge: Number(paramsForCalculating.liability_amount) + Number(paramsForCalculating.deductible_level)};
		});
		$scope = $rootScope.$new();
		request = testHelper.loadJsonFile('confirmation-valuation-block_request.mock');
		$scope.request = request;
		$controller('confirmationPageValuationCtrl', {
			$scope: $scope
		});
	}));
	
	describe('After init,', () => {
		it('Should define $scope.weight', () => {
			expect($scope.weight).toEqual(400);
		});
		it('Should define $scope.valuationTitles', () => {
			expect($scope.valuationTitles).toBeDefined();
		});
		it('Should define $scope.show_valuation_global', () => {
			expect($scope.show_valuation_global).toBeDefined();
		});
		it('Should define $scope.show_valuation_settings', () => {
			expect($scope.show_valuation_settings).toBeDefined();
		});
		it('Should define $scope.isPerPoundValuation', () => {
			expect($scope.isPerPoundValuation).toBeFalsy();
		});
		it('Should define $scope.isFullAmount', () => {
			expect($scope.isFullAmount).toBeTruthy();
		});
		it('Should define $scope.selectedDeductible', () => {
			expect($scope.selectedDeductible).toBeTruthy();
		});
		it('Should define $scope.confirmation_table_show', () => {
			expect($scope.confirmation_table_show).toBeDefined();
		});
		it('Should define $scope.currentPlan', () => {
			expect($scope.currentPlan).toBeDefined();
		});
		describe('When valuation.deductible_data NOT exists,', () => {
			it('Should define $scope.calculatedValuations', () => {
				expect($scope.calculatedValuations).toEqual([7200, 7300, 7400]);
			});
			it('Should define $scope.headers', () => {
				expect($scope.headers).toEqual(['200', '300', '400']);
			});
		});
		describe('When valuation.deductible_data exists,', () => {
			beforeEach(() => {
				$scope.request.request_all_data.valuation.deductible_data = {
					deductible_level: ['111', '222', '333'],
					valuation_charge: ['123', '456', '789']
				};
				$controller('confirmationPageValuationCtrl', {
					$scope: $scope
				});
			});
			it('Should define $scope.calculatedValuations', () => {
				expect($scope.calculatedValuations).toEqual(['123', '456', '789']);
			});
			it('Should define $scope.headers', () => {
				expect($scope.headers).toEqual(['111', '222', '333']);
			});
		});
	});
});