angular
	.module('app')
	.controller('confirmationPageValuationCtrl', confirmationPageValuationCtrl);

/* @ngInject */
function confirmationPageValuationCtrl($scope, valuationService, CalculatorServices, erEstimateSignatureProcessor) {
	const DEFAULT_VALUE = 0;
	const ROUND_UP = 2;
	const DEFAULT_VALUATION_CHARGE_VALUE = 0;
	let valuation = $scope.request.request_all_data.valuation;
	let grandTotal = angular.copy($scope.request.request_all_data.grandTotalRequest);
	let valuationTitles = {
		1: {},
		2: {},
	};
	const SERVICE_TYPE = {
		flatRate: '5',
		longDistance: '7',
	};

	init();
	parseSignatures();

	function init() {
		$scope.valuationTypes = valuationService.VALUATION_TYPES;
		$scope.weight = CalculatorServices.getTotalWeight($scope.request).weight;
		valuationService.fillEmptyFields(valuation, $scope.weight);
		$scope.valuationTitles = valuationService.getSetting('valuation_titles');
		$scope.show_valuation_global = valuationService.getSetting('show_valuation_confirmation_page_global_setting').show;
		$scope.confirmation_table_show = valuationService.getSetting('confirmation_table_show');
		$scope.isFullAmount = valuation.selected.valuation_type === valuationService.VALUATION_TYPES.FULL_VALUE;
		$scope.isPerPoundValuation = valuation.selected.valuation_type === $scope.valuationTypes.PER_POUND;
		$scope.show_valuation_settings = valuationService.getSetting('show_valuation_confirmation_page_setting');
		$scope.selectedDeductible = !isNaN(+$scope.request.request_all_data.valuation.selected.deductible_level);
		valuationTitles = angular.copy($scope.valuationTitles);
		let valuationPlans = valuationService.getValuationPlan();
		let tableName = valuationService.TABLE_NAMES[valuation.settings.valuation_plan];
		$scope.currentPlan = valuationPlans[tableName];
		$scope.calculatedValuations = _.get(valuation, 'deductible_data.valuation_charge', []);
		let emptyCalculatedValuations = $scope.calculatedValuations.every(valuationCharge => valuationCharge === DEFAULT_VALUATION_CHARGE_VALUE);

		if (emptyCalculatedValuations) {
			$scope.calculatedValuations = $scope.currentPlan.header
				.map(deductibleLevel => calculateValuation(deductibleLevel).valuation_charge);
		}

		$scope.headers = _.get(valuation, 'deductible_data.deductible_level', $scope.currentPlan.header);

		let defaultSelected = $scope.headers
			.map(deductibleLevel => deductibleLevel == valuation.selected.deductible_level);

		$scope.selected = _.get(valuation, 'deductible_data.selected', defaultSelected);
		prepareGrandTotal();
	}

	function calculateValuation(deductibleLevel = valuation.selected.deductible_level) {
		let paramsForCalculating = {
			valuation_type: $scope.valuationTypes.FULL_VALUE,
			liability_amount: valuation.selected.liability_amount,
			deductible_level: deductibleLevel,
			valuation_plan: valuation.settings.valuation_plan,
			weight: $scope.weight
		};
		return valuationService.calculateValuation(paramsForCalculating);
	}

	function prepareGrandTotal() {
		let charge = _.get(valuation, 'selected.valuation_charge', DEFAULT_VALUE);
		let totalEstimate = getOtherGrandTotal(charge);
		$scope.totalEstimatePlus = getValuationWithGrandTotal(totalEstimate);
	}

	function getOtherGrandTotal(charge) {
		let result;
		switch($scope.request.service_type.raw) {
		case SERVICE_TYPE.flatRate:
			result = _.get(grandTotal, 'flatrate', DEFAULT_VALUE) - charge;
			break;
		case SERVICE_TYPE.longDistance:
			result = _.get(grandTotal, 'longdistance', DEFAULT_VALUE) - charge;
			break;
		default:
			result = {
				min: _.get(grandTotal, 'min', DEFAULT_VALUE) - charge,
				max: _.get(grandTotal, 'max', DEFAULT_VALUE) - charge,
			};
		}
		return result;
	}

	function getValuationWithGrandTotal(totalEstimate) {
		let result = [];
		_.forEach($scope.calculatedValuations, (charge) => {
			if (_.isObject(totalEstimate)) {
				let min = charge + totalEstimate.min;
				let max = charge + totalEstimate.max;
				if (_.isEqual(min, max)) {
					result.push(`$ ${_.round(max, ROUND_UP)}`);
				} else {
					result.push(`$ ${_.round(min, ROUND_UP)} - $ ${_.round(max, ROUND_UP)}`);
				}
			} else {
				let currentTotal = charge + totalEstimate;
				result.push(`$ ${_.round(currentTotal, ROUND_UP)}`);
			}
		});
		return result;
	}

	$scope.$watch('request.generalCustomBlockReservationSignature.value', parseSignatures);

	function parseSignatures() {
		$scope.valuationTitles[1].explanation = erEstimateSignatureProcessor.parseGeneralCustomBlockSignatures(valuationTitles[1].explanation, $scope.request)
		$scope.valuationTitles[2].explanation = erEstimateSignatureProcessor.parseGeneralCustomBlockSignatures(valuationTitles[2].explanation, $scope.request)
	}
}
