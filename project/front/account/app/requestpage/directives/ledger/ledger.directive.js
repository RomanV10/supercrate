'use strict';

angular
	.module('app.storages')
	.directive('ccLedger', ccLedger);

/*@ngInject*/
function ccLedger($filter, datacontext) {
	return {
		link: link,
		scope: {
			'request': '=request'
		},
		template: require('./ledger.html'),
		restrict: 'AE'
	};


	function link($scope, element, attrs) {
		$scope.payments = $scope.request.payments;
		$scope.invoices = $scope.request.invoices;
		$scope.bills = [];
		var invoices = {};
		$scope.fieldData = datacontext.getFieldData();
		var STORAGEREQUEST = $scope.fieldData.enums.entities.STORAGEREQUEST;
		$scope.entitytype = STORAGEREQUEST;
		invoices.entity_type = $scope.entitytype;
		invoices.entity_id = $scope.request.nid;
		$scope.clicked = '';
		var billObj;

		function initLedger() {
			$scope.bills = [];
			_.forEach($scope.invoices, function (item) {
				billObj = {
					date: parseInt(item.created),
					description: item.data.charges[0].description,
					charge: item.data.totalInvoice,
					payment: '',
					balance: 0,
					type: 'invoice',
					bill_id: 'invoice_' + item.id,
					id: item.id,
					hash: item.hash
				};
				$scope.bills.push(billObj);
			});
			_.forEach($scope.payments, function (item) {
				billObj = {
					date: item.created ? parseInt(item.created) : (parseInt(item.date_ledger) || moment(
						item.date).unix()),
					description: item.description,
					charge: '',
					payment: item.amount,
					balance: 0,
					type: 'payment',
					bill_id: 'payment_' + item.id,
					id: item.id,
					refunds: item.refunds,
					method: item.payment_method
				};
				$scope.bills.push(billObj);
			});
			$scope.bills = $filter('orderBy')($scope.bills, 'date', true);
			var totalBalance = 0;
			_.forEachRight($scope.bills, function (item) {
				if (item.type == 'payment' && !item.refunds) {
					item.balance = totalBalance + Number(Number(item.payment).toFixed(2));
					totalBalance += Number(Number(item.payment).toFixed(2));
				} else if (item.refunds) {
					item.balance = totalBalance - Number(Number(item.payment).toFixed(2));
					totalBalance -= Number(Number(item.payment).toFixed(2));
				}
				if (item.type == 'invoice') {
					item.balance = totalBalance - Number(Number(item.charge).toFixed(2));
					totalBalance -= Number(Number(item.charge).toFixed(2));
				}
			});
		}

		initLedger();

		$scope.calculatePayments = function () {
			return PaymentServices.calcPayment($scope.payments) - PaymentServices.calcInvoices($scope.invoices);
		};

		$scope.dateFormat = function (date, type) {
			return _.isNaN(moment.unix(date)._i) ? moment(date).format('DD MMM, YYYY') : moment.unix(date).format(
				'DD MMM, YYYY h:mm a');
		};

		$scope.paymentType = function (payment) {
			var type = payment.method;
			switch (type) {
			case 'creditcard' :
				return 'credit card';
				break;
			case 'cash':
				return 'cash';
				break;
			case 'check':
				return 'check';
				break;
			default:
				return 'custom';
			}
		};
		$scope.$watchCollection('payments', function (newValue, oldValue) {
			$scope.request.payments = $scope.payments;
			initLedger();
		});
		$scope.$watchCollection('invoices', function (newValue, oldValue) {
			$scope.request.invoices = $scope.invoices;
			initLedger();
		});
	}
}
