(function () {
    'use strict';

    angular
    .module('app.request')
   .controller('erDirectionMap', function($element, $attrs, $scope) {
 
        var from = $scope.from;
        var to = $scope.to;

        $scope.control.init = function(from,to){
                init(from,to);
        }

        init(from,to);

        function init(from,to) {
                var myOptions = {
                    draggable: false,
                    navigationControl: false,
                    scrollwheel: false,
                    streetViewControl: false,
                    zoom: 13,
                    maxZoom: 14,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: new google.maps.LatLng(40.84, 14.25),
                }


                var elm = document.getElementById('ermap');
                var mapObject = new google.maps.Map(elm, myOptions);

                var directionsService = new google.maps.DirectionsService();
                var directionsRequest = {
                    origin: from,
                    destination: to,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC
                };
                directionsService.route(
                    directionsRequest,
                    function (response, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            new google.maps.DirectionsRenderer({
                                map: mapObject,
                                directions: response
                            });
                        }
                        else
                            $("#error").append("Unable to retrieve your route<br />");
                    }
                );
        }

  })
    
  .directive('erMap', function() {

        return {
         controller: 'erDirectionMap',
          restrict: 'A',
          scope: {
            from: '=',
            to: '=',
              control:'='
          },
        };

        });
    
    
})();

