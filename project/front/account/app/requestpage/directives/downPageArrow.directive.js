(function () {
    'use strict';

    angular
        .module('app.request')
        .directive('downPageArrow', function() {

        return {
            restrict: 'E',
            scope: {
            },
            link: link,
            templateUrl: 'app/requestpage/templates/downArrowPage.template.html'
        };

        function link(scope, element, attrs) {
            var paymentBlock = angular.element("#payment_block");

            element.on('click', onClick);

            function onClick() {
                if (!paymentBlock.length) {
                    setPaymentBlock();
                }

                angular.element('html,body').animate({scrollTop: paymentBlock.offset().top }, "slow");
            }

            function setPaymentBlock() {
                paymentBlock = angular.element("#payment_block");
            }
        }
    });
})();

