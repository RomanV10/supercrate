describe('Directive: accountFaq', function () {
	var element, scope;
	let $httpBackend,
		moveBoardApi,
		testHelper,
		elementScope,
		$timeout,
		spy;
	let faqSetting;

	function compileElement(elementHtml) {
		element = $compile(angular.element(elementHtml))(scope);
		elementScope = element.isolateScope();
	}

	beforeEach(inject(function (_$httpBackend_, _testHelperService_, _moveBoardApi_, _$timeout_) {
		testHelper = _testHelperService_;
		scope = $rootScope.$new();
		$timeout = _$timeout_;
		$httpBackend = _$httpBackend_;
		moveBoardApi = _moveBoardApi_;

		faqSetting = testHelper.loadJsonFile('faq-directive.mock');
		$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.faq.getByUsefulness)).respond(200, faqSetting);

		compileElement('<account-faq></account-faq>');

		$httpBackend.flush();
	}));

	describe('When init', () => {
		it('should not render empty html', function () {
			expect(element.html()).not.toEqual('');
		});

		it('should question arr be like faq', function () {
			expect(elementScope.questionArr.length).toEqual(2);
		});
	});

	describe('When set rate,', () => {
		it ("should not set usefulness", () => {
			let data = {
				faq_id: 1
			};
			let item = {
				quizDid: false,
				id: 1
			};
			let set = 'upvote';

			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.faq.upvoteUsefulness),data).respond(200);
			elementScope.setUsefulness(item, set);
			$httpBackend.flush();
			expect(item.quizDid).toBeTruthy();
		});
	});
});
