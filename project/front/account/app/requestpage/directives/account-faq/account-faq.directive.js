'use strict';
import './account-faq.styl';

angular
	.module('app.account')
	.directive('accountFaq', accountFaq);

accountFaq.$inject = ['moveBoardApi', 'apiService'];

function accountFaq(moveBoardApi, apiService){
	return {
		restrict: 'E',
		template: require('./account-faq.pug'),
		scope: {
			isAdmin: '='
		},
		link: linkFunction
	};

	function linkFunction($scope) {
		$scope.setUsefulness = setUsefulness;

		$scope.questionArr = [];

		function setUsefulness(item, set) {
			let upwote = 'upvote';
			let downvote = 'downvote';

			if(set === upwote){
				usefulnessRequest(moveBoardApi.faq.upvoteUsefulness, item);
			}
			if(set === downvote){
				usefulnessRequest(moveBoardApi.faq.downvoteUsefulness, item);
			}
		}

		function usefulnessRequest(url,item) {
			item.quizDid = true;
			apiService.postData(url, {faq_id: item.id}).then(() => {
			});
		}

		function getFaqByUsefulness() {
			apiService.postData(moveBoardApi.faq.getByUsefulness).then(data => {
				$scope.questionArr = data.data;
			});
		}

		getFaqByUsefulness();
	}
}
