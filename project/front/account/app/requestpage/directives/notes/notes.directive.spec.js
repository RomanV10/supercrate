describe('notes directive', () => {
	let $scope;
	let element;
	let elementScope;
	let locationPathSpy;
	let RequestPageServices;
	let getFieldData = {contract_page: {}};
	let getNotes = {
		note: {
			then(callback) {
				callback(getNotesResponse);
				return this;
			},
			catch(callback) {
				callback();
			}
		}
	};
	let getNotesResponse = {data: {}};

	function compileElement() {
		element = angular.element('<notes-directive request-note="requestNote" admin="admin" is-foreman="isForeman"></notes-directive>');
		$compile(element)($scope);
		elementScope = element.isolateScope();
	}

	beforeEach(inject((_datacontext_, _RequestPageServices_, _$location_) => {
		RequestPageServices = _RequestPageServices_;
		spyOn(_datacontext_, 'getFieldData')
			.and
			.returnValue(getFieldData);

		spyOn(RequestPageServices, 'getNotes')
			.and
			.returnValue(getNotes);
		locationPathSpy = spyOn(_$location_, 'path');

		$scope = $rootScope.$new();
		$scope.admin = false;
		$scope.isForeman = false;
		$scope.requestNote = {nid: 1000};
	}));

	describe('after init', () => {
		it('when not selected foreman notes showNotes should be false', () => {
			//given
			let expectedResult = false;

			//when
			compileElement();

			//then
			expect(elementScope.showNotes).toEqual(expectedResult);
		});

		it('when error should show error', () => {
			//given
			let expectedResult = 'Cannot get note';
			spyOn(toastr, 'error');

			//when
			compileElement();

			//then
			expect(toastr.error).toHaveBeenCalledWith(expectedResult);
		});

		it('when client should get note with client role', () => {
			//given
			let expectedResult = [1000, {role_note : ['client_notes']}];

			//when
			compileElement();

			//then
			expect(RequestPageServices.getNotes).toHaveBeenCalledWith(...expectedResult);
		});

		it('when request page should get note with client and foreman role', () => {
			//given
			let expectedResult = [1000, {role_note : ['client_notes', 'foreman_notes']}];
			locationPathSpy
				.and
				.returnValue('reservation');

			//when
			compileElement();

			//then
			expect(RequestPageServices.getNotes).toHaveBeenCalledWith(...expectedResult);
		});
	});
});