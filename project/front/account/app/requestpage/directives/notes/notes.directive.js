'use strict';

angular.module('move.requests')
	.directive('notesDirective', notesDirective);

notesDirective.$inject = ['RequestPageServices', '$location', 'datacontext'];

function notesDirective(RequestPageServices, $location, datacontext) {
	return {
		restrict: 'E',
		template: require('./notes.tmpl.html'),
		scope: {
			requestNote: '<',
			admin: '<',
			isForeman: '<'
		},
		link: noteLink
	};

	function noteLink($scope) {
		const CLIENT = 'client_notes';
		const FOREMAN = 'foreman_notes';
		const CURRENT_PAGE = $location.path();
		const RESERVATION_PATH = 'reservation';
		const CONTRACT = 'contract';

		let rout_type = '';
		let contractPage = angular.fromJson(datacontext.getFieldData().contract_page);

		$scope.showNotes = _.get(contractPage, 'confirmationShowForemanNotes.selected', false);
		$scope.isReservationPage = false;

		setRoutType(RESERVATION_PATH);
		setRoutType(CONTRACT);

		function setRoutType(type) {
			if (CURRENT_PAGE && CURRENT_PAGE.indexOf(type) >= 0) {
				rout_type = type;
			}
		}

		if (_.isEqual(rout_type, RESERVATION_PATH) || _.isEqual(rout_type, CONTRACT)) {
			getCurrentNote([CLIENT, FOREMAN]);
			$scope.isReservationPage = true;
		} else {
			getCurrentNote([CLIENT]);
		}

		function getCurrentNote(role_note) {
			let data = {
				role_note,
			};

			RequestPageServices.getNotes($scope.requestNote.nid, data)
				.note
				.then(({data}) => {
					$scope.noteClient = _.get(data, 'client_notes', '');

					if (_.isEqual(rout_type, RESERVATION_PATH) || _.isEqual(rout_type, CONTRACT)) {
						$scope.noteForeman = _.get(data, 'foreman_notes', '');
					}
				})
				.catch(() => {
					toastr.error('Cannot get note');
				});
		}
	}
}