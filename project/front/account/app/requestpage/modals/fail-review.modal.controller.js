(function () {
    'use strict';

    angular.module('app.request').controller('failReviewCtrl', failReviewCtrl);

    failReviewCtrl.$inject = ['$scope', '$q', '$uibModalInstance', 'reviewSettings', 'emailMark', 'review', 'RequestPageServices'];

    function failReviewCtrl($scope, $q, $uibModalInstance, reviewSettings, emailMark, review, RequestPageServices) {
        $scope.failText = reviewSettings.failReviewText;
        $scope.emailReview = !_.isUndefined(emailMark) ? true : false;
        $scope.claimReview = '';

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.updateReview = function () {
            review.text_review = $scope.claimReview;
            var promise = RequestPageServices.createReview(review);
            $q.all(promise).then(function () {});
            $uibModalInstance.dismiss('cancel');
        }
    }
})();