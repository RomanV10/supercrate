'use strict';

angular
	.module('app.request')
	.controller('ReviewModalCtrl', ReviewModalCtrl);

ReviewModalCtrl.$inject = ['$scope', '$uibModalInstance', 'reviewSettings', 'request', 'RequestPageServices', '$q', '$uibModal', 'reviewSharedService'];

function ReviewModalCtrl($scope, $uibModalInstance, reviewSettings, request, RequestPageServices, $q, $uibModal, reviewSharedService) {
	let entityType;
	const LD_REQUEST = request.service_type.raw == 7;
	const STORAGE_REQUEST = request.service_type.raw == 2 || request.service_type.raw == 6;
	const ENTITY_TYPE = reviewSharedService.ENTITY_TYPE;

	if (LD_REQUEST) {
		entityType = ENTITY_TYPE.LDREQUEST;
	} else if (STORAGE_REQUEST) {
		entityType = ENTITY_TYPE.STORAGEREQUEST;
	} else {
		entityType = ENTITY_TYPE.MOVEREQUEST;
	}

	$scope.reviewRequest = '';

	// On init user cannot leave review.
	// Only after server has response we enable/disable functional.
	$scope.allowSendReview = true;

	$scope.stars = [
		{index: 1, selected: false},
		{index: 2, selected: false},
		{index: 3, selected: false},
		{index: 4, selected: false},
		{index: 5, selected: false},
	];

	$scope.welcomeTitle = reviewSettings.welcomeReviewText;
	$scope.starMark = 0;
	$scope.changeMark = function (chosenStar) {
		_.forEach($scope.stars, star => {
			star.selected = star.index <= chosenStar;
		});
		return $scope.starMark = chosenStar;
	};

	function tryGetReview() {
		RequestPageServices.getCurrentReview({
			entity_type: entityType,
			entity_id: request.nid
		})
			.then(resolve => {
				if (!_.isArray(resolve)) {
					$scope.allowSendReview = true;
					$scope.reviewRequest = resolve.text_review;
					$scope.changeMark(resolve.count_stars);
				} else {
					$scope.allowSendReview = false;
				}
			});
	}

	$scope.apply = function () {
		if (_.isEqual($scope.starMark, 0) || $scope.reviewRequest.length < 3) {
			toastr.warning('Please, fill in all fields');
			return true;
		} else {
			$scope.allowSendReview = true;
			const REVIEW = {
				entity_type: entityType,
				entity_id: request.nid,
				count_stars: $scope.starMark,
				text_review: $scope.reviewRequest,
				user_id: request.uid.uid
			};
			RequestPageServices.createReview(REVIEW);

			$uibModalInstance.dismiss('cancel');
			toastr.success('Review was sent');

			if ($scope.starMark >= reviewSettings.current_stars_count) {
				onSuccessReview();
			} else if ($scope.starMark <= reviewSettings.current_negative_stars_count) {
				onFailReview(REVIEW);
			}
		}
	};

	function onSuccessReview() {
		$uibModal.open({
			templateUrl: './app/requestpage/templates/review-success.tpl.html',
			controller: 'successReviewCtrl',
			backdrop: false,
			size: 'lg',
			resolve: {
				reviewSettings: () => reviewSettings,
			}
		});
	}

	function onFailReview(REVIEW) {
		$uibModal.open({
			templateUrl: './app/requestpage/templates/review-fail.tpl.html',
			controller: 'failReviewCtrl',
			backdrop: false,
			size: 'lg',
			resolve: {
				reviewSettings: () => reviewSettings,
				emailMark: () => undefined,
				review: () => REVIEW,
			}
		});
	}


	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	tryGetReview();
}
