(function () {
    'use strict';

    angular.module('app.request').controller('successReviewCtrl', successReviewCtrl);

    successReviewCtrl.$inject = ['$scope', '$uibModalInstance', 'reviewSettings'];

    function successReviewCtrl($scope, $uibModalInstance, reviewSettings) {
        $scope.successText = reviewSettings.successReviewText;

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();