describe("Filter: Cubic Foot to Pound", () => {
	let $filter;

	beforeEach(inject(function (_$filter_) {
		$filter = _$filter_;
	}));

	it('should return 0 when cubic foot is undefined', function () {
		let cubicFoot = undefined, result;

		result = $filter('cubicFootToPounds')(cubicFoot);

		expect(result).toEqual(0);
	});

	it('should return 0 when cubic foot less than 0', function () {
		let cubicFoot = -10, result;

		result = $filter('cubicFootToPounds')(cubicFoot);

		expect(result).toEqual(0);
	});

	it('should return 7 when cubic foot 1', function () {
		let cubicFoot = 1, result;

		result = $filter('cubicFootToPounds')(cubicFoot);

		expect(result).toEqual(7);
	});

	it('should return 7 when cubic foot "1"', function () {
		let cubicFoot = 1, result;

		result = $filter('cubicFootToPounds')(cubicFoot);

		expect(result).toBe(7);
	})
});
