angular
	.module('app.request')
	.filter('cubicFootToPounds', function () {
		return function (cf = 0) {
			let rate = 7;

			if (cf < 0) {cf = 0}

			return cf * rate;
		}
	});
