(function () {
    'use strict';

    angular.module('app.request')
        .run(routeConfig);

    routeConfig.$inject = ['routehelper','AuthenticationService'];
    /* @ngInject */
    function routeConfig(routehelper,AuthenticationService) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/request/:id',
                config: {
                    templateUrl: 'app/requestpage/templates/request_page_layout.html?34',
                    title: 'requestpage'
                }
            },
            {
                url: '/request/:id/chat',
                config: {
                    templateUrl: 'app/requestpage/templates/request_page_layout.html?34',
                    title: 'requestpage'
                }
            },
            {
                url: '/request/:id/receipt',
                config: {
                    templateUrl: 'app/requestpage/templates/request_page_layout.html?34',
                    title: 'requestpage'
                }
            },
             {
                url: '/request/:id/reservation',
                config: {
                    templateUrl: 'app/requestpage/templates/reservation_page.html?7',
                    title: 'reservationpage'
                }
            },
            {
                url: '/request/:id/cancelation_policy',
                config: {
                    templateUrl: 'app/requestpage/templates/cancelation_policy.html?2',
                    title: 'Cancellation Policy'

                }
            },
            {
                url: '/request/:id/contract_print',
                config: {
                    templateUrl: 'app/requestpage/templates/contract_print.html',
                    title: 'Print Contract page'

                }
            },
            {
                url: '/request/:id/invoice',
                config: {
                    templateUrl: 'app/requestpage/templates/invoice.html',
                    title: 'Invoice'

                }
            },
            {
                url: '/invoice/:id',
                config: {
                    templateUrl: 'app/requestpage/templates/invoice.html',
                    title: 'Invoice'

                }
            },
            {
              url: '/coupon/:id',
              config: {
                templateUrl: 'app/requestpage/templates/buy_coupon.html',
                title: 'Buy Coupon'
              }
            },
            {
                url: '/storage_tenant/:id',
                config: {
                    templateUrl: 'app/requestpage/templates/storageTenant.html',
                    title: 'Storage Tenant'
                }
            },
            {
                url: '/request/:id/review',
                config: {
                    templateUrl: 'app/requestpage/templates/request_page_layout.html?34',
                    title: 'requestpage'
                }
            },
            {
                url: '/request/:id/mark/:star',
                config: {
                    templateUrl: 'app/requestpage/templates/request_page_layout.html?34',
                    title: 'requestpage'
                }
            },
            {
                url: '/request/:id/contract_proposal_additional_pages_read',
                config: {
                    templateUrl: 'app/requestpage/templates/proposalAdditionalPages.html',
                    title: 'Proposal additional pages'
                }
            },
            {
                url: '/request/:id/contract_main_page_read',
                config: {
                    templateUrl: 'app/requestpage/templates/mainContractPage.html',
                    title: 'Main cotract page'
                }
            }
        ];
    }
})();
