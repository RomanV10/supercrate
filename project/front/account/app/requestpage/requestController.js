import {
	converInventoryToOldVersion,
	updateRequestExtraServicesByItems
} from '../../../moveBoard/app/newInventories/directive/newVersion/additionalFunctions.js';

'use strict';

angular
	.module('app.request')
	.controller('Request', Request);

/* @ngInject */
function Request($scope, $rootScope, $routeParams, logger, RequestServices, RequestPageServices, ClientsServices,
	common, Session, config, InventoriesServices, datacontext, CalculatorServices, SweetAlert, Lightbox, $location,
	requestModals, $uibModal, MessagesServices, AuthenticationService, PaymentServices, sharedRequestServices,
	ContractFactory, ContractService, ContractCalc, $q, $sce, InvoiceServices, MoveCouponService,
	RequestHelperService, $interval, ParserServices, InitRequestFactory, FuelSurchargeService, CheckRequestService,
	additionalServicesFactory, apiService, moveBoardApi, $route, CommercialCalc, geoCodingService,
	serviceTypeService, Raven, $window, valuationService, grandTotalService,
	policyModalServices, $controller, inventoryExtraService, erExtraSignaturePage) {

	$controller('SharedCustomerOnline', {$scope});

	$scope.uploader = {};
	// Inititate the promise tracker to track form submissions.
	let vm = this;
	let nid = $routeParams.id;
	let rout_type = '';
	let inventoryEditRequest = {};
	let reactiveTime = 15000;
	if (angular.isDefined($routeParams.type)) {
		rout_type = $routeParams.type;
	}
	vm.rootBusy = true;
	vm.packingUpdating = false;

	vm.request = {};

	// TODO Make this functional using router
	const CHAT_PATH = 'chat';
	const RECEIPT_PATH = 'receipt';
	const REVIEW_PATH = 'review';
	const MARK_REVIEW_PATH = 'mark';
	const WEIGHT_TYPE_ROOMS = 1;
	const WEIGHT_TYPE_INVENTORY = 2;
	const WEIGHT_TYPE_CUSTOM = 3;
	const ONE_DAY_IN_SECONDS = 86400;
	const WELCOME_MODAL_FIRST = 'first';
	const WELCOME_MODAL_EACH = 'each';
	const WELCOME_MODAL_BY_TIME = 'noActivityPeriod';

	var session = Session.get();
	var path = $location.path();

	setRoutType(CHAT_PATH);
	setRoutType(RECEIPT_PATH);
	setRoutType(REVIEW_PATH);
	setRoutType(MARK_REVIEW_PATH);

	function setRoutType(type) {
		if (path && path.indexOf(type) >= 0) {
			rout_type = type;
		}
	}

	let userRole = _.get(session, 'userRole', []);
	let isAuthorizedClient = userRole.length == 1 && ~userRole.indexOf('authenticated user');

	if (!session && !AuthenticationService.isAuthenticated()) {
		if (AuthenticationService.checkAutoLogin()) {
			$rootScope.$broadcast('autoLogin');
			$window.location.reload();
		}

		return;
	}

	vm.request_statuses = {};
	vm.statusText = [];
	vm.inventoryEdit = false;
	vm.detailsEdit = false;
	vm.activate = false;
	var firstInit = false;
	vm.admin = false;
	vm.newMessages = 0;
	vm.messages = {};
	vm.areaCode = 0;
	vm.packing_service = 0;
	vm.details = {};
	vm.request = {};
	$scope.request = {};
	var initialExtraService = [];
	var isInitExtraServices = false;
	var initialPackingSettings = [];
	var isInitPackingSettings = false;
	var isPackingSettingsChanged = false;
	var NAME_FULL_PACKING = 'Estimate Full Packing';
	var NAME_PARTIAL_PACKING = 'Estimate Partial Packing';
	vm.packingNames = {
		partial: NAME_PARTIAL_PACKING,
		full: NAME_FULL_PACKING
	};
	vm.trustAsHtml = $sce.trustAsHtml;
	$scope.reservationSignature = [];
	vm.addedImages = [];
	vm.decreaseItemsLog = [];
	vm.increaseItemsLog = [];
	vm.removedItemsLog = [];
	vm.removedItems = [];
	vm.decreaseItems = [];
	var uid = 1;
	vm.unusedCoupons = [];
	vm.couponPayments = [];
	vm.serviceExplanation = [];
	vm.generalCustomArr = [];
	vm.invoice = {};
	vm.isNotConfirmed = false;
	vm.isConfirmed = false;
	vm.showReviewButton = false;
	vm.showCoupons = false;
	vm.packingSettings = [];
	let copyRequestAllData;


	$('body').scrollTop(0);

	vm.request_statuses[1] = {
		name: 'Pending',
		template: 'app/requestpage/templates/statuses/default.html',
		text: 'We are in the process of checking our availability for your upcoming relocation. Once we confirm our availability, you will have the option to proceed and reserve your move. In the meantime, please update your inventory, move details and addresses.',
		flat: 'We see your readiness to move with us. Allow us to double-check our availability, and we will update your page with a confirmation link shortly.'
	};
	vm.request_statuses[17] = {
		name: 'Pending Info',
		template: 'app/requestpage/templates/statuses/default.html',
		text: 'Thank you for updating your move information. Give us a moment to check our schedule to make sure we can accommodate your updated moving needs',
		flat: 'We see your readiness to move with us. Allow us to double-check our availability, and we will update your page with a confirmation link shortly.'
	};
	vm.request_statuses[2] = {
		name: 'Not Confirmed',
		template: 'app/requestpage/templates/statuses/notconfirmed.html',
		text: ''
	};
	vm.request_statuses[3] = {
		name: 'Your move is confirmed and scheduled',
		template: 'app/requestpage/templates/statuses/confirm.html',
		text: ''
	};
	vm.request_statuses[14] = {
		name: 'Spam',
		template: 'app/requestpage/templates/statuses/default.html',
		text: 'This request is spam'
	};
	vm.request_statuses[9] = {
		name: 'Flat Rate Step 1',
		template: 'app/requestpage/templates/statuses/flat_rate_steps.html',
		text: 'This request is spam'
	};
	vm.request_statuses[10] = {
		name: 'Flat Rate Step 2',
		template: 'app/requestpage/templates/statuses/flat_rate.html?22',
		text: ''
	};
	vm.request_statuses[11] = {
		name: 'Flat Rate Step 3',
		template: 'app/requestpage/templates/statuses/flat_rate.html?22',
		text: ''
	};
	vm.request_statuses[12] = {
		name: 'Expired',
		template: 'app/requestpage/templates/statuses/default.html',
		text: 'Request is expired. Call to office to check availability.'
	};
	vm.request_statuses[13] = {
		name: 'We are not available',
		template: 'app/requestpage/templates/statuses/default.html',
		text: ''
	};

	vm.tabs = [
		{
			name: 'Move Overview',
			url: 'app/requestpage/templates/overview.html?12',
			selected: true,
			icon: 'fa fa-list-alt'
		},
		{
			name: 'Inventory',
			url: '',
			selected: false,
			icon: 'fa fa-list-ol'
		},
		{
			name: 'Details',
			url: 'app/requestpage/templates/details.html',
			selected: false,
			icon: 'fa fa-newspaper-o'
		},
		{
			name: 'Upload Photos',
			url: 'app/requestpage/templates/photos.html',
			selected: false,
			icon: 'fa fa-camera'
		},
		{
			name: 'Additional Inventory',
			url: 'app/requestpage/templates/additionalInventory.html',
			selected: false,
			icon: 'fa fa-list-ol'
		},
		{
			name: '',
			url: 'app/requestpage/templates/unusedCoupons.html',
			selected: false
		}
	];
	vm.tabs2 = [
		{
			name: 'Messages',
			url: 'app/requestpage/templates/messages.html',
			selected: false,
			icon: 'fa fa-envelope'
		},

	];
	vm.tabs3 = [
		{
			name: 'Moving Insurance',
			url: 'app/requestpage/templates/insurance.html?2',
			selected: false,
			icon: 'icon-docs'
		},

	];
	vm.tabs4 = [
		{
			name: 'Payment Receipts',
			url: 'app/requestpage/templates/receipts.html?2',
			selected: false,
			icon: 'icon-wallet'
		},

	];
	vm.tabs5 = [
		{
			name: 'Not Interested in Move ?',
			url: 'app/requestpage/templates/cancelation.html?2',
			selected: false,
			icon: 'icon-close'
		}

	];
	vm.tabs6 = [
		{
			name: 'Packing Service',
			selected: false,
			icon: 'fa fa-archive'
		},

	];
	vm.tabs7 = [
		{
			name: 'Additional Inventory',
			selected: false,
			icon: 'fa fa-archive'
		},

	];
	vm.tabs8 = [
		{
			name: 'Coupons',
			selected: false,
			icon: 'fa fa-tag'
		},

	];


	vm.LiveUpdate = true;
	$scope.$on('$routeChangeStart', function (e, next, current) {
		vm.LiveUpdate = false;
	});


	vm.data = ContractFactory.factory;
	vm.fieldData = datacontext.getFieldData();
	vm.basicSettings = angular.fromJson(vm.fieldData.basicsettings);
	vm.reviewSettings = angular.fromJson(vm.fieldData.reviews_settings);
	vm.localdiscount = parseInt(vm.basicSettings.localDistount);
	vm.calcSettings = angular.fromJson(vm.fieldData.calcsettings);
	vm.progressBarSettings = angular.fromJson(vm.fieldData.progressbarsettings);
	vm.min_hours = parseFloat(vm.calcSettings.min_hours);
	vm.longdistanceSettings = angular.fromJson(vm.fieldData.longdistance);
	vm.quoteSettings = angular.fromJson(vm.fieldData.customized_text);
	vm.commercialMoveSizeSetting = angular.fromJson(vm.fieldData.commercial_move_size_setting);
	vm.currentManagerData = {};
	vm.tooltipData = {};
	const STATUS_NAMES = vm.fieldData.field_lists.field_approve;

	const PACKING_SETTINGS = vm.basicSettings.packingAdvancedSettings;

	var enums = vm.fieldData.enums;
	const ENTITY_TYPE = _.get(enums, 'entities', {});


	var importedFlags = $rootScope.availableFlags;
	var Job_Completed = 'Completed';
	var MAX_SIGN_COUNT = 7;
	vm.contractSubbmited = false;

	let oldInventory = [];
	let contract_discount = 0;
	let contract_discountType = '';
	let saveInventoryPromise = false;
	let valuationServiceTypes = valuationService.getSetting('service_types');
	let valuationShowSetting = valuationService.getSetting('valuation_account_show_setting');


	vm.addExtraCharges = addExtraCharges;
	vm.removeCharge = removeCharge;
	vm.updateAddPackingService = updateAddPackingService;
	vm.calculatePackingServices = calculatePackingServices;
	vm.calculateExtraServiceTotal = calculateExtraServiceTotal;
	vm.getPackingTotal = getPackingTotal;
	vm.isVisiblePackingTotal = isVisiblePackingTotal;
	vm.dateFormat = dateFormat;
	vm.openAddressModal = openAddressModal;
	vm.isEqualLocalMoveMaxMinTime = isEqualLocalMoveMaxMinTime;
	vm.calculateLocalMoveMinQuote = calculateLocalMoveMinQuote;
	vm.calculateLocalMoveMaxQuote = calculateLocalMoveMaxQuote;
	vm.calculateLocalMoveQuoteByMinHours = calculateLocalMoveQuoteByMinHours;
	vm.calculateLocalMoveMinQuoteByUprate = calculateLocalMoveMinQuoteByUprate;
	vm.calculateLocalMoveMaxQuoteByUprate = calculateLocalMoveMaxQuoteByUprate;
	vm.calculateLocalMoveQuoteByMinHoursAndUprate = calculateLocalMoveQuoteByMinHoursAndUprate;
	vm.calculateLocalMoveMinQuoteWithDiscount = calculateLocalMoveMinQuoteWithDiscount;
	vm.calculateLocalMoveMaxQuoteWithDiscount = calculateLocalMoveMaxQuoteWithDiscount;
	vm.calculateLocalMoveDiscount = calculateLocalMoveDiscount;
	vm.calculateLocalMoveDiscountPercent = calculateLocalMoveDiscountPercent;
	vm.calculateLocalMoveQuoteWithDiscountAndMinHours = calculateLocalMoveQuoteWithDiscountAndMinHours;
	vm.calculateLocalMoveDiscountByMinHours = calculateLocalMoveDiscountByMinHours;
	vm.calculateLocalMoveMinJobTime = calculateLocalMoveMinJobTime;
	vm.calculateLocalMoveMaxJobTime = calculateLocalMoveMaxJobTime;
	vm.isEqualMaxMinLaborTime = isEqualMaxMinLaborTime;
	vm.saveAddListInventories = saveAddListInventories;
	vm.isVisibleExtraServiceTotal = isVisibleExtraServiceTotal;
	vm.savePhotos = savePhotos;
	vm.remove = remove;
	vm.goToRequest = goToRequest;
	vm.goStorageTenant = goStorageTenant;
	vm.chooseOption = chooseOption;
	vm.choosePackingService = choosePackingService;
	vm.savePackingService = savePackingService;
	vm.calculateTotals = calculateTotals;
	vm.select = select;
	vm.createReview = createReview;
	vm.saveListInventories = saveListInventories;
	vm.goToNewRequest = goToNewRequest;
	vm.getGrandTotal = getGrandTotal;
	vm.openExtraSignaturePage = openExtraSignaturePage;

	$scope.openEditModal = openEditModal;
	$scope.openLightboxModal = openLightboxModal;
	$scope.calculateAddServices = calculateAddServices;
	$scope.calculateRowTotal = calculateRowTotal;
	$scope.saveStep = saveStep;
	$scope.submitFlatRate = submitFlatRate;
	$scope.showReceipt = showReceipt;
	$scope.cancelMove = cancelMove;
	$scope.openInventoryModal = openInventoryModal;
	$scope.openPolicyModal = policyModalServices.openPolicyModal;

	let isOpenEditModal = false;
	let invalidParserRequest = false;

	var messagesObj = {};
	var messages = [];
	var commentsFirst = true;

	vm.fuelSurcharge = 0;

	vm.listInventories = [];
	vm.packing = {};
	vm.packing.account = false;
	vm.packing.labor = false;
	vm.busyInventory = false;

	function loadMessages() {
		$scope.busy = true;
		var promise = MessagesServices.getMessagesThread(nid);
		promise.then(function (data) {
			$scope.busy = false;
			var diff = common.diff(data, messagesObj);

			if (commentsFirst) {
				messages = common.objToArray(data);
				messagesObj = data;
				commentsFirst = false;
			} else if (!commentsFirst && diff.length) {
				messages.push(data[diff]);
				messagesObj[diff] = data[diff];
			}

			var newmessages = 0;
			if (angular.isDefined(vm.request)) {
				angular.forEach(data, function (m, cid) {
					if (!m.read && m.admin) {
						newmessages++;
					}
				});
			}

			vm.messages = data;
			vm.request.messages = newmessages;

			if (vm.reloadMessages) {
				vm.reloadMessages(nid);
			}
		});
	}

	vm.packingService = {
		LM: {
			full: 0.5,
			partial: 50
		},
		LD: {
			full: 0.7,
			partial: 44
		}
	};

	if (angular.isDefined(vm.basicSettings.packing_settings.packingAccount)) {
		vm.packing.account = vm.basicSettings.packing_settings.packingAccount;
	}

	if (angular.isDefined(vm.basicSettings.packing_settings.packingLabor)) {
		vm.packing.labor = vm.basicSettings.packing_settings.packingLabor;
	}

	if (angular.isDefined(vm.basicSettings.packing_settings.packing)) {
		if (angular.isDefined(vm.basicSettings.packing_settings.packing.LM)) {
			vm.packingService.LM = vm.basicSettings.packing_settings.packing.LM;
		}
	}

	if (angular.isDefined(vm.basicSettings.packing_settings.packing)) {
		if (angular.isDefined(vm.basicSettings.packing_settings.packing.LD)) {
			vm.packingService.LD = vm.basicSettings.packing_settings.packing.LD;
		}
	}

	function sendPackingLogs() {
		let packingMsg;
		switch (+vm.packing_service) {
			case 1:
				packingMsg = `User have chosen ${vm.packingNames.partial}.`;
				break;
			case 2:
				packingMsg = `User have chosen ${vm.packingNames.full}.`;
				break;
			default:
				packingMsg = 'User will be packaging itself';
		}
		RequestServices.sendLogs([{
			simpleText: packingMsg
		}], 'Request page', $scope.request.nid, 'MOVEREQUEST');
	}

	function savePackingService() {
		let defer = $q.defer();
		vm.packingUpdating = true;


		if (!angular.equals(initialPackingSettings, vm.packingSettings)) {
			checkPacking(vm.packingSettings);
			angular.copy(vm.packingSettings, initialPackingSettings);
			isPackingSettingsChanged = true;
			let reqData = vm.request.request_data.value != '' ? angular.fromJson(vm.request.request_data.value) : {};
			reqData.packings = vm.packingSettings;
			vm.request.request_data.value = reqData;
			RequestServices.updateRequest(vm.request.nid, {
				request_data: {
					value: vm.request.request_data.value
				}
			})
				.then(() => {
					defer.resolve();
				}, () => {
					defer.reject();
				}
				)
				.finally(() => {
					vm.packingUpdating = false;
				});
		} else {
			isPackingSettingsChanged = false;
			vm.packingUpdating = false;
			defer.resolve();
		}

		calculateLongDistanceTotals();

		return defer.promise;
	}

	function checkPacking(current) {
		let fullPacking = _.findIndex(current, {originName: NAME_FULL_PACKING});
		let partialPacking = _.findIndex(current, {originName: NAME_PARTIAL_PACKING});

		if (vm.request.request_all_data.removedPacking == 2 && fullPacking >= 0) {
			current.splice(fullPacking, 1);
		}

		if (vm.request.request_all_data.removedPacking == 1 && partialPacking >= 0) {
			current.splice(partialPacking, 1);
		}
	}

	function saveExtraService() {
		let promises = [];
		calculateLongDistanceTotals();

		if (!angular.equals(initialExtraService, vm.extraServices)) {
			angular.copy(vm.extraServices, initialExtraService);
			vm.request.extraServices = angular.copy(vm.extraServices);
			promises.push(additionalServicesFactory.saveExtraServices(vm.request.nid, vm.extraServices));

			if (vm.Invoice) {
				vm.invoice.extraServices = angular.copy(vm.extraServices);
				promises.push(saveRequestData());
			}
		}

		return $q.all(promises);
	}

	function recalculateRequestWeightByMoveSize() {
		let promises = [];
		let oldWeight = vm.request.total_weight.weight;
		vm.request.total_weight = CalculatorServices.getRequestCubicFeet(vm.request);
		let newWeight = vm.request.total_weight.weight;
		let weightChanged = oldWeight != newWeight;

		if (weightChanged) {
			if (vm.request.service_type.raw == 7) {
				if (vm.Invoice) {
					vm.invoice.closing_weight.value = CalculatorServices.getTotalWeight(vm.request).weight;
					vm.request.request_all_data.invoice = angular.copy(vm.invoice);
					RequestServices.saveRequestDataContract(vm.request);
				}

				promises.push(calculateLongDistanceRate());
				calculateLongDistanceTotals();
			}

			if (vm.request.service_type.raw == 6) {
				promises.push(RequestHelperService.recalculateCountOvernightExtraService(vm.request));

				if (vm.request.storage_id != 0 && !_.isUndefined(session.userId.nodes[vm.request.storage_id])) {
					promises.push(RequestHelperService.recalculateCountOvernightExtraService(
						session.userId.nodes[vm.request.storage_id]));
				}
			}
		}

		return $q.all(promises);
	}

	function recalculateRequestWeightByInventory(newValue, oldValue) {
		let promises = [];
		let oldWeight = InventoriesServices.calculateTotals(oldValue).cfs;
		vm.request.inventory_weight = InventoriesServices.calculateTotals(newValue);
		let newWeight = vm.request.inventory_weight.cfs;
		let weightChanged = oldWeight != newWeight;

		if (weightChanged) {
			if (vm.request.service_type.raw == 7) {
				if (vm.Invoice) {
					vm.invoice.inventory.inventory_list = angular.copy(vm.request.inventory.inventory_list);

					let closingWeight = CalculatorServices.getTotalWeight(vm.request).weight;
					_.set(vm, 'invoice.closing_weight.value', closingWeight);

					vm.request.request_all_data.invoice = angular.copy(vm.invoice);
					RequestServices.saveRequestDataContract(vm.request);
				}

				promises.push(calculateLongDistanceRate());
				calculateLongDistanceTotals();
			}

			if (vm.request.service_type.raw == 6) {
				promises.push(RequestHelperService.recalculateCountOvernightExtraService(vm.request));

				if (vm.request.storage_id != 0 && !_.isUndefined(session.userId.nodes[vm.request.storage_id])) {
					promises.push(RequestHelperService.recalculateCountOvernightExtraService(session.userId.nodes[vm.request.storage_id]));
				}
			}
		}

		return $q.all(promises);
	}

	if (vm.admin) {
		session.userId.nodes = {};
	}

	let currentRequest = _.get(session, `userId.nodes[${nid}]`);

	if (angular.isUndefined(currentRequest)) {
		userRole = _.get(session, 'userRole', []);

		if (AuthenticationService.isAuthorized(userRole)) {
			vm.admin = true;
			getRequestByNid();
		} else {
			$location.path('/pages-404.html');
		}
	} else {
		activate(currentRequest, session.userId.nodes);
		tryToRegisterCustomerOnline();
		getRequestByNid();
	}

	function getRequestByNid() {
		RequestServices.getRequestsByNid(nid).then(function (data) {
			vm.busy = false;

			angular.forEach(data.nodes, function (request, nid) {
				activate(request, session.userId.nodes);
			});

		});
	}


	function dateFormat(date) {
		return _.isNaN(moment.unix(date)._i)
			? moment(date).format('DD MMM, YYYY')
			: moment.unix(date).format('DD MMM, YYYY h:mm a');
	}


	function openAddressModal() {
		RequestPageServices.openAddressModal(vm.request, false, true);
	}


	function calculateLongDistanceGrandTotal() {
		var result = vm.longDistanceQuote
			+ calculatePackingServices()
			+ calculateAddServices()
			+ parseFloat(vm.request.request_all_data.valuation.selected.valuation_charge || 0)
			+ parseFloat(vm.fuelSurcharge);
		result -= getCouponDiscount();

		return parseFloat(result.toFixed(2));
	}

	function calculateLongDistanceGrandTotalDiscount() {
		let longDistanceQuote = CalculatorServices.getLongDistanceQuote(vm.request);

		longDistanceQuote += calculateLocalMoveBaseQuote();

		if (contract_discount) {
			longDistanceQuote = longDistanceQuote - Number(contract_discount);
		}
		let discount = vm.request.request_all_data.add_money_discount + longDistanceQuote * vm.request.request_all_data.add_percent_discount / 100;
		return parseFloat((longDistanceQuote - discount).toFixed(2));
	}


	function isEqualLocalMoveMaxMinTime() {
		return vm.request.maximum_time.raw == vm.request.minimum_time.raw;
	}


	function calculateLocalMoveBaseQuote() {
		return calculatePackingServices()
			+ calculateAddServices()
			+ parseFloat(vm.request.request_all_data.valuation.selected.valuation_charge || 0)
			+ parseFloat(vm.fuelSurcharge);
	}


	function calculateLocalMoveMinQuote() {
		return vm.request.quote.min;
	}


	function calculateLocalMoveMaxQuote() {
		return vm.request.quote.max;
	}

	function calculateLocalMoveQuoteByMinHours() {
		var result = 0;

		if (vm.request.request_all_data.add_rate_discount) {
			result = vm.min_hours * Number(vm.request.request_all_data.add_rate_discount) + calculateLocalMoveBaseQuote();
		} else {
			result = vm.min_hours * vm.request.rate.value + calculateLocalMoveBaseQuote();
		}

		return result;
	}


	function calculateLocalMoveMinQuoteByUprate() {
		return calculateLocalMoveMinJobTime() * vm.uprate + calculateLocalMoveBaseQuote();
	}


	function calculateLocalMoveMaxQuoteByUprate() {
		return calculateLocalMoveMaxJobTime() * vm.uprate + calculateLocalMoveBaseQuote();
	}


	function calculateLocalMoveQuoteByMinHoursAndUprate() {
		return vm.min_hours * vm.uprate + calculateLocalMoveBaseQuote();
	}


	function calculateLocalMoveMinQuoteWithDiscount() {
		let result = vm.request.quote.min + calculateLocalMoveBaseQuote() - vm.request.request_all_data.add_money_discount - vm.perecent_discount_min;
		result -= getCouponDiscount();

		return result;
	}


	function calculateLocalMoveMaxQuoteWithDiscount() {
		let result = vm.request.quote.max + calculateLocalMoveBaseQuote() - vm.request.request_all_data.add_money_discount - vm.perecent_discount_max;
		result -= getCouponDiscount();

		return result;
	}

	function getCouponDiscount() {
		return Number(_.get(vm.request.request_all_data, 'couponData.discount', 0));
	}

	function calculateLocalMoveDiscount() {
		var maxJobTime = calculateLocalMoveMaxJobTime();
		var newMoneyDiscount = 0;

		if (vm.request.request_all_data.add_money_discount == 0 && vm.request.request_all_data.add_percent_discount != 0) {
			newMoneyDiscount = (vm.request.quote.max + calculateLocalMoveBaseQuote()) * vm.request.request_all_data.add_percent_discount / 100;
		}

		vm.moneyDiscount = vm.request.request_all_data.add_money_discount || newMoneyDiscount;

		var result = maxJobTime * vm.uprate - vm.request.quote.max + vm.request.request_all_data.add_money_discount + newMoneyDiscount;

		return result;
	}


	function calculateLocalMoveDiscountPercent() {
		// return vm.discountAmount + vm.request.request_all_data.add_percent_discount;
		let total = 0;
		let discount = 0;

		if (vm.min_hours < vm.calculateLocalMoveMaxJobTime()) {
			total = vm.calculateLocalMoveMinQuoteByUprate();
			discount = vm.calculateLocalMoveDiscount();
		} else if (vm.min_hours >= vm.calculateLocalMoveMaxJobTime()) {
			total = vm.calculateLocalMoveQuoteByMinHoursAndUprate();
			discount = vm.calculateLocalMoveDiscountByMinHours();
		}

		let realDiscount = parseFloat((Number(discount) * 100 / Number(total)).toFixed(2));
		return vm.localdiscount > realDiscount ? vm.localdiscount : realDiscount;
	}


	function calculateLocalMoveQuoteWithDiscountAndMinHours() {
		if (vm.request.request_all_data.add_rate_discount) {
			return vm.min_hours * Number(
				vm.request.request_all_data.add_rate_discount) + calculateLocalMoveBaseQuote() - vm.request.request_all_data.add_money_discount - vm.perecent_discount_min;
		}

		return vm.min_hours * vm.request.rate.value + calculateLocalMoveBaseQuote() - vm.request.request_all_data.add_money_discount - vm.perecent_discount_min;
	}


	function calculateLocalMoveDiscountByMinHours() {
		return vm.min_hours * vm.uprate - vm.min_hours * vm.request.rate.value + vm.request.request_all_data.add_money_discount + vm.perecent_discount_max;
	}

	function calculateLocalMoveMinJobTime() {
		var travelTime = vm.travelTimeSetting ? Number(vm.request.travel_time.raw) : 0;
		return vm.request.minimum_time.raw + (vm.isDoubleDriveTime
			? $scope.request.field_double_travel_time.raw
			: travelTime);
	}

	function calculateLocalMoveMaxJobTime() {
		var travelTime = vm.travelTimeSetting ? Number(vm.request.travel_time.raw) : 0;
		return vm.request.maximum_time.raw + (vm.isDoubleDriveTime
			? $scope.request.field_double_travel_time.raw
			: travelTime);
	}


	function isEqualMaxMinLaborTime() {
		return _.isEqual(vm.request.minimum_time.raw, vm.request.maximum_time.raw);
	}


	function saveAddListInventories() {
		vm.totalInv = 0;
		var inventory_weight = InventoriesServices.calculateTotals(vm.request.request_all_data.additional_inventory);
		vm.totalInv = parseFloat((inventory_weight.cfs * Number(vm.basicSettings.storage_rate.min)).toFixed(2));
		SweetAlert.swal({
			title: 'It will costs $ ' + vm.totalInv + ' additional.',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes, I agree'
		},
		(isConfirm) => {
			if (isConfirm) {
				vm.busyInventory = true;
				vm.request.request_all_data.needsAddInventory = true;
				vm.request.request_all_data.additional_inventory_cost = vm.totalInv;

				if (vm.Invoice) {
					if (!_.isUndefined(_.get(vm.invoice.request_all_data, 'valuation.selected.valuation_charge'))
							&& vm.invoice.field_useweighttype.value == 2) {
						valuationService.reCalculateValuation(vm.request, vm.invoice, 'invoice');
					}
				} else {
					if (vm.request.request_all_data.valuation && !_.isUndefined(
						vm.request.request_all_data.valuation.selected.valuation_charge)
							&& vm.request.field_useweighttype.value == 2) {
						valuationService.reCalculateValuation(vm.request, vm.invoice, 'request');
					}
				}

				RequestServices.saveRequestDataContract(vm.request)
					.then(function () {
						vm.busyInventory = false;
						$rootScope.$broadcast('add.inventory', vm.request.request_all_data);
						ContractFactory.init();
						ContractFactory.factory.inventoryMoving.currentStep = 5;
						ContractFactory.openInventorySignaturePad('customer');
					}, function () {
						vm.busyInventory = false;
					});
			}
		});
	}


	function isCanvasBlank(elementID) {
		var canvas = document.getElementById(elementID);
		var blank = document.createElement('canvas');
		blank.width = canvas.width;
		blank.height = canvas.height;
		var context = canvas.getContext('2d');
		var perc = 0, area = canvas.width * canvas.height;

		if (canvas.toDataURL() == blank.toDataURL()) {
			return canvas.toDataURL() == blank.toDataURL();
		} else {
			var data = context.getImageData(0, 0, canvas.width, canvas.height).data;

			for (var ct = 0, i = 3, len = data.length; i < len; i += 4) {
				if (data[i] > 50) {
					ct++;
				}
			}

			perc = parseFloat((100 * ct / area).toFixed(2));

			return perc < 0.1;
		}
	}


	function saveStep() {
		var blank = isCanvasBlank('signatureInventoryCanvas');

		if (blank) {
			SweetAlert.swal('Failed!', 'Sign please', 'error');
		} else {
			var signatureValue = ContractFactory.getCropedInventorySignatureValue();
			var signature = {
				owner: 'customer',
				value: 'data:image/gif;base64,' + signatureValue,
				date: moment().format('x')
			};
			$scope.request.request_all_data.additional_inventory_sign = signature;
			RequestServices.saveRequestDataContract($scope.request);
			ContractFactory.clearSignaturePad();
			$('#signatureInventoryPad').modal('hide');
		}
	}


	function calculatePackingServices() {
		var serviceType = vm.request.service_type.raw == '5' || vm.request.service_type.raw == '7';
		var result = PaymentServices.getPackingTotal(vm.request.request_data.value, vm.packing.labor, serviceType);
		return parseFloat(result);
	}


	function updateAddPackingService() {
		let defer = $q.defer();
		let needToSave = false;
		let weight = fetchWeight();
		let fullPacking = false;
		let partialPacking = false;
		let myselfPacking = true;
		let indexService = '';
		let rateName = vm.request.service_type.raw == '5' || vm.request.service_type.raw == '7' ? 'LDRate' : 'rate';

		function recalculateTotals(packings, rateName, labor) {
			labor = labor && rateName == 'LDRate' ? 1 : 0;
			_.each(packings, function (packing) {
				packing.laborRate = packing.laborRate || 0;
				let newTotal = parseFloat((packing.quantity * (parseFloat(packing[rateName]) + parseFloat(packing.laborRate * labor))).toFixed(2));
				if (packing.total != newTotal) {
					packing.total = newTotal;
					needToSave = true;
				}
			});
		}

		recalculateTotals(vm.packingSettings, rateName, vm.basicSettings.packing_settings.packingLabor);

		for (var service in vm.packingSettings) {
			if (vm.packingSettings[service].name == NAME_PARTIAL_PACKING) {
				partialPacking = true;
				myselfPacking = false;
				indexService = service;
			}
			if (vm.packingSettings[service].name == NAME_FULL_PACKING) {
				fullPacking = true;
				myselfPacking = false;
				indexService = service;
			}
		}

		if (myselfPacking) {
			defer.resolve();
			return defer.promise;
		}

		if (partialPacking || fullPacking) {
			let initialPackingDefaultValue = vm.packingSettings[indexService].quantity;
			vm.packingSettings[indexService].quantity = weight.weight;
			vm.packingSettings[indexService].total = parseFloat(
				(vm.packingSettings[indexService].quantity * vm.packingSettings[indexService][rateName]).toFixed(2));

			if (initialPackingDefaultValue != weight.weight) {
				needToSave = true;
			}
		}

		if (needToSave) {
			savePackingService()
				.then(
					res => {
						defer.resolve();
					},
					rej => {
						defer.reject();
					}
				);
		} else {
			defer.resolve();
		}

		return defer.promise;
	}

	if (!vm.admin) {
		reactiveTime = 1000;
	}


	function reactivate() {
		$interval.cancel($scope.reactivateRequestInterval);

		if (!vm.admin) {
			reactiveTime = 15000;
		}

		$scope.reactivateRequestInterval = $interval(() => {
			onForceReloadAccount();

			$interval.cancel($scope.reactivateRequestInterval);
		}, reactiveTime);
	}

	$scope.$on('force_reload_account', onForceReloadAccount);

	function onForceReloadAccount() {
		RequestServices.getRequestsByNid(nid).then(function (data) {
			angular.forEach(data.nodes, function (request, nid) {
				if (vm.LiveUpdate) {
					if (request.status.raw != 9) {
						if (angular.isDefined(session.userId.nodes)) {
							activate(request, session.userId.nodes);
						} else {
							activate(request);
						}
					}
				}
			});
		}).finally(() => {
			vm.packingUpdating = false;
		});

	}

	userRole = _.get(session, 'userRole', []);

	if (isAuthorizedClient) {
		let msgVal = 'User has viewed their account page';
		var msg = {
			simpleText: msgVal
		};
		var arr = [];
		arr.push(msg);
		RequestServices.sendLogs(arr, 'Request page', $scope.request.nid, 'MOVEREQUEST');
		let data = {
			node: {
				nid: vm.request.nid
			},
			notification: {
				text: msgVal
			}
		};
		let dataNotify = {
			type: enums.notification_types.CUSTOMER_VIEW_REQUEST,
			data: data,
			entity_id: vm.request.nid,
			entity_type: enums.entities.MOVEREQUEST
		};

		apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
	}


	//Look for UNPAID INVOICES

	var invoiceRequest = {};
	vm.unpaidInvoice = [];
	invoiceRequest.entity_type = vm.fieldData.enums.entities.MOVEREQUEST;
	var invoiceFlags = vm.fieldData.enums.invoice_flags;
	invoiceRequest.entity_id = nid;

	InvoiceServices.getInvoices(invoiceRequest).then(function (response) {
		$scope.busy = false;
		$scope.invoices = response;

		angular.forEach($scope.invoices, function (invoice, item) {
			if (invoice.data.flag != invoiceFlags.PAID && invoice.data.flag != invoiceFlags.DRAFT) {
				vm.unpaidInvoice.push(invoice);
			}
		});
	}, function (reason) {
		$scope.busy = false;
		logger.error(reason, reason, 'Error');
	});

	function isVisiblePackingTotal() {
		return vm.packingSettings.length != 0 && vm.packingSettings.length > 1;
	}

	function getAddressOfField(field) {
		var result = getFieldValue(field, 'locality') + ', ' + getFieldValue(field,
			'administrative_area') + ', ' + getFieldValue(field, 'postal_code');

		if (!_.isEqual(field.thoroughfare, 'Enter Address') && !_.isEmpty(field.thoroughfare)) {
			result = getFieldValue(field, 'thoroughfare') + ', ' + result;
		}

		return result;
	}

	function getFieldValue(field, property) {
		return _.isUndefined(field[property]) || _.isNull(field[property]) ? '' : field[property];
	}

	/////////////////////
	function activate(initital_request, user_requests) {
		vm.usedCoupon = 0;

		if (!_.isUndefined(initital_request.request_all_data.couponData)) {
			vm.usedCoupon = Number(initital_request.request_all_data.couponData.discount || 0);
		}

		vm.useCalculator = _.get(initital_request, 'field_usecalculator.value', true);
		getCurrentServiceTypeName(initital_request);

		if (session) {
			vm.activate = true;

			let currentUserRole = _.get(session, 'userRole', []);
			vm.admin = AuthenticationService.isAuthorized(currentUserRole);

			if (!vm.admin) {
				reactivate();
				vm.user = session.userId;
			} else {
				var client = {
					field_user_first_name: initital_request.first_name,
					field_user_last_name: initital_request.last_name,
					mail: initital_request.email,
					field_primary_phone: initital_request.phone,
					admin: true
				};
				vm.user = client;
			}

			loadMessages();

			var workers;

			InitRequestFactory.initRequestFields(initital_request, vm.calcSettings.travelTime);

			InitRequestFactory.clearFields(vm.request);
			if (vm.Invoice) {
				InitRequestFactory.clearFields(vm.request.request_all_data.invoice);
			}

			_.merge(vm.request, initital_request);

			vm.flatRate = vm.request.status.raw == 9 || vm.request.status.raw == 10 || vm.request.status.raw == 11;
			vm.longDistance = vm.request.service_type.raw == 7;
			vm.flatRateService = vm.request.service_type.raw == 5;
			vm.packingRequest = vm.request.service_type.raw == 8;
			vm.localMoves = vm.request.service_type.raw == 1 || vm.request.service_type.raw == 3 || vm.request.service_type.raw == 4;
			vm.storageMoves = vm.request.service_type.raw == 2 || vm.request.service_type.raw == 6;
			vm.flatRateLocalMove = _.get(vm.request, 'field_flat_rate_local_move.value');
			vm.isShowExtraSignatureButton = erExtraSignaturePage.isEnableExtraSignature(vm.request.service_type.raw);

			copyRequestAllData = angular.copy(vm.request.request_all_data);

			if (vm.progressBarSettings.enabled) {
				let progressBarSettingForCurrentServiceType = _.find(vm.progressBarSettings.serviceTypes,
					serviceType => serviceType.id == vm.request.service_type.raw);
				vm.isOptional = progressBarSettingForCurrentServiceType.isOptional;
			}

			vm.localMoveRequest = vm.request.service_type.raw == 1 || vm.request.service_type.raw == 3 || vm.request.service_type.raw == 4 || vm.request.service_type.raw == 8;

			let mapFrom = getAddressOfField(vm.request.field_moving_from);
			let mapTo = getAddressOfField(vm.request.field_moving_to);

			if (!firstInit) {
				RequestPageServices.initMap(mapFrom, mapTo);

				showReservationSignature();
			}

			$scope.request = vm.request;

			vm.Invoice = vm.request.status.raw == 3;

			vm.deposit = 0;
			angular.forEach(vm.request.receipts, function (receipt) {
				if ((receipt.payment_flag == 'Reservation' || receipt.payment_flag == 'Reservation by client' || receipt.type == 'reservation')
					&& !receipt.pending) {
					vm.deposit += parseFloat(receipt.amount);
				}
			});

			if (vm.Invoice) {
				setUpInvoiceVariable();
			}

			if (_.get($scope, 'request.request_all_data.invoice.discount')) {
				contract_discount = $scope.request.request_all_data.invoice.discount;
				contract_discountType = $scope.request.request_all_data.invoice.discountType;
			}

			if (!_.isEmpty($scope.request.contract_info)) {
				contract_discount = $scope.request.contract_info.discount;
				contract_discountType = $scope.request.contract_info.discountType;
			}

			let requestHasSelfTravelTime = angular.isDefined(vm.request.request_all_data.travelTime);
			vm.travelTimeSetting = requestHasSelfTravelTime
				? vm.request.request_all_data.travelTime
				: vm.calcSettings.travelTime;

			$scope.serviceType = vm.request.service_type.raw == '5' || vm.request.service_type.raw == '7';
			vm.requests = user_requests || {};
			vm.interested = vm.request.status.raw != 5 && vm.request.status.raw != 18;
			vm.isNotConfirmed = vm.request.status.raw == _.invert(vm.fieldData.field_lists.field_approve)[' Not Confirmed'];
			vm.isConfirmed = vm.request.status.raw == _.invert(vm.fieldData.field_lists.field_approve)[' Confirmed'];
			vm.notinterested = !vm.interested;
			vm.isHidePackingService = (vm.request.service_type.raw == 6 || vm.request.service_type.raw == 2) && vm.request.request_all_data.toStorage;
			vm.isPackingDayRequest = vm.request.service_type.raw == 8;
			vm.config = config;
			var services = [1, 2, 3, 4, 6, 8];
			vm.isDoubleDriveTime = vm.calcSettings.doubleDriveTime
				&& vm.request.field_double_travel_time
				&& !_.isNull(vm.request.field_double_travel_time.raw)
				&& services.indexOf(parseInt(vm.request.service_type.raw)) >= 0;

			var now = moment().unix();
			var created = angular.copy(vm.request.created);
			vm.request.company_logo_url = vm.basicSettings.company_logo_url;

			if (vm.basicSettings.globalShowCoupons) {
				vm.showCoupons = vm.basicSettings.showCoupons <= moment.unix(now).diff(moment.unix(created), 'days');
			}

			if (angular.isDefined(vm.request.request_all_data.showCoupons)) {
				vm.showCoupons = vm.request.request_all_data.showCoupons;
			}

			if (!vm.request.inventory.inventory_list && !firstInit) {
				vm.listInventories = [];
				if (vm.useCalculator && !vm.admin) {
					_updateRequestTime();
				}
			} else if (!firstInit || !vm.inventoryEdit && !vm.busyInventory) {
				vm.listInventories = vm.request.inventory.inventory_list;
			}

			if (!vm.listInventories) {
				vm.listInventories = [];
				if (vm.useCalculator && !vm.admin) {
					_updateRequestTime();
				}
			}

			vm.localdiscount = parseInt(vm.request.request_all_data.localdiscount);

			$scope.extra_discount_expiration = false;

			if (vm.request.status.raw != 3 && angular.isDefined(
				vm.request.request_all_data.add_rate_discount_expiration)) {

				var now = moment().unix();
				var then = moment(vm.request.request_all_data.add_rate_discount_expiration).unix();

				if (then < now) {
					$scope.extra_discount_expiration = true;
					vm.request.request_all_data.add_rate_discount = 0;
				}
			}

			if (vm.request.status.raw == 21 || vm.request.status.raw == 14) {
				vm.request.status.raw = 12;
				vm.request.status.value = 'Expired';
			}

			//INCREASE RESERVATION IF RULSE
			if (vm.request.status.raw == 2) {
				var confirmation = CalculatorServices.checkExperation(vm.request);

				if (confirmation) {
					//change status
					//update to expired
					vm.request.status['raw'] = '12';
					vm.request.status['value'] = 'Expired';
					vm.statusTemplate = vm.request_statuses[12].template;
					vm.statusText = vm.request_statuses[12].text;
					var editrequest = {field_approve: 12};
					RequestServices.updateRequest(vm.request.nid, editrequest);
				}
			}

			$scope.detailsDone = false;

			if ((!firstInit || !vm.detailsEdit)
				&& vm.request.inventory.move_details) {

				vm.details = vm.request.inventory.move_details;
				vm.ifDetails = CheckRequestService.isMoveDetailsChanged(vm.details, vm.request.service_type.raw);
				vm.detailHasComment = vm.details.addcomment.length;
				$scope.detailsDone = vm.ifDetails;
			}


			vm.delivery_info_text = vm.details.deliveryExplanation || vm.longdistanceSettings.delivery_info_text;
			vm.longdistancePendingInfo = vm.longdistanceSettings.pending_info_text;
			vm.longdistanceNotConfirmedInfo = vm.longdistanceSettings.not_confirmed_info_text;

			if (vm.useCalculator && !vm.admin) {
				_updateRequestTime();
			}

			var request_data = {};
			vm.request_data = {};

			if (vm.request.request_data.value != '') {
				vm.switchers = vm.request.request_data.value.switchers || null;
				vm.request_data = request_data = angular.fromJson(vm.request.request_data.value);
			}

			if (vm.switchers) {
				ContractService.getUsersByRole({ //get all selected users
					active: 1,
					role: ['helper', 'foreman']
				})
					.then(function (users) {
						workers = users;
					});
			}

			ContractCalc.initData({request: vm.request});
			vm.data.crews[0].rate = vm.request.rate.value;

			if (vm.Invoice && vm.invoice.rate && vm.invoice.rate.value) {
				vm.data.crews[0].rate = vm.invoice.rate.value;
			}

			if (vm.request.request_all_data.add_rate_discount) {
				vm.data.crews[0].rate = vm.request.request_all_data.add_rate_discount;
			}

			if (vm.data.crews.length > 1) {
				for (var i = 1, l = vm.data.crews.length; i < l; i++) {
					var crew = vm.data.crews[i];
					crew.rate = ContractCalc.getRate(vm.request.date.raw * 1000,
						request_data.crews.additionalCrews[i - 1].helpers.length,
						request_data.crews.additionalCrews[i - 1].trucks);
				}
			}

			if (!ParserServices.validateParserRequest(vm.request) && !isOpenEditModal) {
				invalidParserRequest = true;
				openEditModal();
			}

			let userRole = _.get(session, 'userRole', []);
			var isAdmin = ~userRole.indexOf('administrator') || ~userRole.indexOf('foreman');

			ContractFactory.getData({
				permission: isAdmin,
				nid: nid
			},
			(contract) => { //cb
				ContractFactory.initData(contract);

				if (vm.request_data.paymentType == 'FlatFee') {
					var total = 0;
					angular.forEach(vm.request_data.pricePerHelper, function (i) {
						total += i;
					});
					ContractCalc.finance.fixedTotal = total;
				}

				if (angular.isUndefined(importedFlags.company_flags)) {
					importedFlags.company_flags = {};
				}

				vm.data.isSubmitted = vm.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[Job_Completed]);
				vm.data.isPickupSubmitted = vm.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags['pickupSubmitted']);
				vm.maxSignCount = vm.data.crews.length > 1
					? MAX_SIGN_COUNT - 2 + vm.data.crews.length * 2
					: MAX_SIGN_COUNT;
				vm.contractSubbmited = vm.data.isSubmitted;
			});

			initFuelSurcharge();

			initExtraServices();

			let requestPacking = [];

			if (vm.request.request_data.value != '') {
				requestPacking = vm.request.request_data.value.packings || [];
			}

			// Need for init packing
			if (!_.isEmpty(requestPacking) && !isInitPackingSettings) {
				isInitPackingSettings = true;
				angular.copy(requestPacking, initialPackingSettings);
			}

			// if bellow need for update extraService from admin panel
			if (!_.isEmpty(requestPacking) && !angular.equals(requestPacking, vm.packingSettings) && !isPackingSettingsChanged) {
				vm.packingSettings = requestPacking;
				angular.copy(vm.packingSettings, initialPackingSettings);
			} else {
				if (isPackingSettingsChanged) {
					isPackingSettingsChanged = false;
				}
			}

			vm.packingAdvancedSettings = isPackingHide();

			angular.forEach(vm.packingSettings, function (value, key) {
				if (vm.request.service_type.raw == '5' || vm.request.service_type.raw == '7') {
					if (angular.isUndefined(value.laborRate)) {
						value.laborRate = 0;
					}

					if (vm.packing.labor) {
						vm.packingSettings[key].total = Number(value.quantity) * (Number(value.laborRate) + Number(value.LDRate));
					} else {
						vm.packingSettings[key].total = Number(value.quantity) * Number(value.LDRate);
					}
				} else {
					vm.packingSettings[key].total = Number(value.quantity) * Number(value.rate);
				}
			});

			vm.packing_service = _.get(vm.request, 'request_all_data.requiredPackingService', 0);

			vm.moveDate = moment(vm.request.date.value).format('MMMM DD, YYYY');

			if (vm.request.ddate.value == '') {
				vm.deliveryDate = '';
			} else {
				vm.deliveryDate = moment(vm.request.ddate.value).format('MMMM DD, YYYY');
			}

			vm.homeEstimateDate = vm.request.home_estimate_date.value
				? moment(vm.request.home_estimate_date.value).format('MMMM DD, YYYY')
				: '';

			if (vm.request.ddate_second.value == '') {
				vm.deliveryDateSecond = '';
			} else {
				vm.deliveryDateSecond = moment(vm.request.ddate_second.value).format('MMMM DD, YYYY');
			}

			$scope.images = vm.request.field_move_images;

			//disable menu
			vm.disable_menu = vm.request.status.raw == 3;
			$scope.disable_menu = vm.request.status.raw == 3;
			$scope.ldbefore24 = false;

			if (vm.longDistance) {
				//if long distancee close menu if more then 24 hours before pickup
				var pickupDate = moment(vm.request.date.value);
				let now = moment();
				var hours = pickupDate.diff(now, 'hours');

				if (hours > 24) {
					vm.disable_menu = false;
					$scope.disable_menu = false;
					$scope.ldbefore24 = true;
				}

				let isConfirmedAccountMenuEnabledGlobal = _.get(vm.longdistanceSettings, 'isConfirmedAccountMenuEnabled', true);
				let isConfirmedAccountMenuEnabled = vm.isConfirmed && !_.get(vm.request, 'request_all_data.localInventoryDetails', isConfirmedAccountMenuEnabledGlobal);
				vm.disable_menu = isConfirmedAccountMenuEnabled;
				$scope.disable_menu = isConfirmedAccountMenuEnabled;
			}

			//DISCOUNT SETTINGS
			vm.DiscountApply = false;

			if ((vm.localdiscount != 0 || vm.request.request_all_data.add_percent_discount || vm.request.request_all_data.add_money_discount || vm.request.request_all_data.add_rate_discount) && vm.request.rate.value != vm.request.request_all_data.add_rate_discount) {
				//LocalDiscount Rate

				vm.DiscountApply = true;
				vm.discountAmount = parseInt(vm.localdiscount);
				vm.uprate = vm.request.rate.value / (1 - vm.discountAmount / 100);

				if (vm.request.request_all_data.add_rate_discount) {
					vm.request.rate.value = vm.request.request_all_data.add_rate_discount;
					vm.discountAmount = (1 - vm.request.request_all_data.add_rate_discount / vm.uprate) * 100;
				}

				if (vm.request.request_all_data.add_money_discount) {
					var total = calculateTotalBalance('max')
						+ calculatePackingServices()
						+ calculateAddServices()
						+ parseFloat(vm.request.request_all_data.valuation.selected.valuation_charge || 0)
						+ parseFloat(vm.fuelSurcharge);
					vm.discountAmount = vm.request.request_all_data.add_money_discount / total * 100;
				}

				vm.perecent_discount_min = 0;
				vm.perecent_discount_max = 0;

				if (vm.request.request_all_data.add_percent_discount) {
					var total_min = calculateTotalBalance('min')
						+ calculatePackingServices()
						+ calculateAddServices()
						+ parseFloat(vm.request.request_all_data.valuation.selected.valuation_charge || 0)
						+ parseFloat(vm.fuelSurcharge);
					var total_max = calculateTotalBalance('max')
						+ calculatePackingServices()
						+ calculateAddServices()
						+ parseFloat(vm.request.request_all_data.valuation.selected.valuation_charge || 0)
						+ parseFloat(vm.fuelSurcharge);
					vm.perecent_discount_min = total_min * vm.request.request_all_data.add_percent_discount / 100;
					vm.perecent_discount_max = total_max * vm.request.request_all_data.add_percent_discount / 100;
				}
			}

			vm.flatOptionText = '';
			let moveDetailsOptions = _.get(vm, 'request.inventory.move_details.options');

			if (vm.flatRateService && moveDetailsOptions && !_.isEmpty(moveDetailsOptions)) {
				angular.forEach(moveDetailsOptions, (option, id) => {
					var delivery = moment(option.delivery).format('MM/DD/YYYY');
					var pickup = moment(option.pickup).format('MM/DD/YYYY');

					if (delivery == vm.request.ddate.value && pickup == vm.request.date.value) {
						vm.flatOptionText = option.text;
					}
				});
			}

			if (vm.flatRateService && angular.isUndefined(vm.request.flatRateTotal)) {
				vm.request.flatRateTotal = 0;
			}

			if (vm.storageMoves) {
				let now = vm.request.date.raw;
				let then;
				let storageNid = vm.request.storage_id || vm.request.request_all_data.baseRequestNid;
				vm.interval = 0;

				if (angular.isDefined(vm.requests) && storageNid != 0) {
					if (!_.isUndefined(vm.requests[storageNid])) {
						then = vm.requests[storageNid].date.raw;

						if (!vm.request.request_all_data.toStorage) {
							vm.interval = (then - now) / ONE_DAY_IN_SECONDS;
						} else {
							vm.interval = (now - then) / ONE_DAY_IN_SECONDS;
						}

						vm.storage_rate = CalculatorServices.getStorageRate(vm.request, vm.interval);
						vm.interval = CalculatorServices.convertDateIntoMonths(vm.interval);
					} else {
						RequestServices.getRequestsByNid(storageNid)
							.then((data) => {
								then = data.nodes[0].date.raw;

								if (_.isUndefined(vm.requests)) {
									vm.requests = {};
								}

								vm.requests[storageNid] = data.nodes[0];

								if (!vm.request.request_all_data.toStorage) {
									vm.interval = (then - now) / ONE_DAY_IN_SECONDS;
								} else {
									vm.interval = (now - then) / ONE_DAY_IN_SECONDS;
								}

								vm.storage_rate = CalculatorServices.getStorageRate(vm.request, vm.interval);
								vm.interval = CalculatorServices.convertDateIntoMonths(vm.interval);
							});
					}
				}
			}

			let greetingTextSettings = vm.basicSettings.flatRateAccountSettings.greetingText;
			let showPeriod = greetingTextSettings.showPeriod;
			vm.topTextSettings = greetingTextSettings.topTextSettings;

			let isShowFirstTime = _.isEqual(showPeriod, WELCOME_MODAL_FIRST) && !vm.user.field_isnew && !vm.admin;
			let isShowEachTime = _.isEqual(showPeriod, WELCOME_MODAL_EACH) && !session.isShowedGreetingModal && !vm.admin;
			let isShowAbsentTime = _.isEqual(showPeriod, WELCOME_MODAL_BY_TIME) && !vm.admin && !session.isShowedGreetingModal;

			//Show Congrats Dialog if first time user & period setting - first time
			if (isShowFirstTime) {
				setFieldIsNew();
				openCongratsModal();
				vm.user.field_isnew = true;
			}

			//Show Congrats Dialog if period setting - each time
			if (isShowEachTime) {
				if (!vm.user.field_isnew) {
					setFieldIsNew();
				}

				session.isShowedGreetingModal = true;
				openCongratsModal();
			}

			//Show Congrats Dialog if period setting - no activity period time
			if (isShowAbsentTime) {
				session.isShowedGreetingModal = true;
				if (!vm.user.field_isnew) {
					setFieldIsNew();
				}

				let now = moment();
				let then = moment.unix(vm.user.field_previous_login);
				let different = now.diff(then, 'days');

				if (different >= greetingTextSettings.absentingPeriod) {
					openCongratsModal();
				}
			}

			$scope.showQuote = vm.request.request_all_data.showQuote || vm.request.status.raw != 1 && vm.request.status.raw != 4;
			$scope.inventoryDone = false;

			calculateTotals();
		}

		$scope.extraPickup = false;
		vm.request.extraPickup = false;
		$scope.extraDropoff = false;
		vm.request.extraDropoff = false;

		if (vm.request.field_extra_pickup.postal_code != null && vm.request.field_extra_pickup.postal_code.length) {
			$scope.extraPickup = true;
			vm.request.extraPickup = true;
		}

		if (vm.request.field_extra_dropoff.postal_code != null && vm.request.field_extra_dropoff.postal_code.length) {
			$scope.extraDropoff = true;
			vm.request.extraDropoff = true;
		}

		vm.pickupOnly = vm.request.service_type.raw == 3 || vm.request.service_type.raw == 4;
		let allowInSettings = valuationServiceTypes[vm.request.service_type.raw];
		let requestHasWeight = vm.request.total_weight.weight && vm.request.total_weight.weight != 0
			|| vm.request.total_weight.cfs && vm.request.total_weight.cfs != 0;
		let valuationShow = valuationShowSetting.cent_per_pound || valuationShowSetting.full_value_protection;
		vm.valuationShow = allowInSettings && requestHasWeight && valuationShow;
		$scope.inventoryTotals = calculateTotals();

		//GET LONG DISTANCE RATE
		if (vm.longDistance) {
			let weight = fetchWeight().weight;

			if (weight == 0) {
				weight = CalculatorServices.getCubicFeet(vm.request);
				weight = weight.weight;
			}

			let linfo = CalculatorServices.getLongDistanceInfo(vm.request);
			let min_weight = linfo.min_weight;

			if (angular.isDefined(vm.request.request_all_data.min_weight)) {
				min_weight = vm.request.request_all_data.min_weight;
			}

			if (min_weight < weight) {
				min_weight = weight;
			}

			checkForUpdateLongDistanceExtraServices(vm.request, min_weight);

			if (vm.request.field_moving_to.premise == null && !vm.areaCode) {
				GetAreaCode()
					.then(function () {
						calculateLongDistance();
					}, function () {
						logger.error(err || 'error', null, 'Error get area code!');
					});
			} else {
				if (vm.request.field_moving_to.premise) {
					vm.areaCode = vm.request.field_moving_to.premise;
				}

				vm.request.field_moving_to.premise = vm.areaCode;
				calculateLongDistance();
			}
		} else {
			let weight = InventoriesServices.calculateTotals(vm.request.inventory.inventory_list);
			weight = weight.cfs;

			if (weight == 0) {
				weight = CalculatorServices.getCubicFeet(vm.request);
				weight = weight.weight;
			}

			checkForUpdateLongDistanceExtraServices(vm.request, weight);
		}

		if (!firstInit) {
			vm.ifDetails = CheckRequestService.isMoveDetailsChanged(vm.details, vm.request.service_type.raw);
			vm.detailHasComment = vm.details.addcomment.length;
		}

		vm.rooms = InventoriesServices.getRooms(vm.request);

		if (angular.isDefined(vm.request_statuses[vm.request.status.raw])) {
			vm.statusTemplate = vm.request_statuses[vm.request.status.raw].template;
			vm.statusText = vm.request_statuses[vm.request.status.raw].text;

			if (vm.flatRateService) {
				vm.statusText = vm.request_statuses[vm.request.status.raw].flat;
			}
		}

		if (vm.request.status.raw == 4) {
			let time = vm.request.home_estimate_actual_start.value != vm.request.home_estimate_start_time.value
				? `${vm.request.home_estimate_actual_start.value} - ${vm.request.home_estimate_start_time.value}`
				: vm.request.home_estimate_actual_start.value;
			getInHomeEstimateText(vm.request.home_estimate_status.value, time);
		}

		if (angular.isUndefined(vm.statusTemplate)) {
			vm.statusTemplate = 'app/requestpage/templates/statuses/default.html';

			if (angular.isDefined(vm.request_statuses[vm.request.status.raw])) {
				vm.statusText = vm.request_statuses[vm.request.status.raw].text;
			}
		}

		//CHECK STEPS FOR FLAT RATE
		vm.flatStepOne = false;

		if (vm.request.status.raw == 9) {
			vm.flatStepOne = true;
			vm.request.flatStepOne = true;
			//1. Move Details First
			angular.forEach(vm.tabs, function (tab) {
				tab.selected = false;
			});

			//TODO have to refactor this place
			if (!$scope.detailsDone && !$scope.inventoryDone) {
				vm.tabs[2].selected = true;
			} else if ($scope.detailsDone && !$scope.inventoryDone) {
				vm.tabs[1].selected = true;
			} else if (!$scope.detailsDone && $scope.inventoryDone) {
				vm.tabs[2].selected = true;
			} else {
				vm.tabs[0].selected = true;
			}
		}

		if (rout_type == CHAT_PATH && !firstInit) {
			select(vm.tabs2[0]);
		}

		if (rout_type == RECEIPT_PATH && !firstInit) {
			select(vm.tabs4[0]);
		}

		if (rout_type == REVIEW_PATH && !firstInit) {
			createReview();
		}

		if (rout_type == MARK_REVIEW_PATH && !firstInit) {
			tryGetReview();
		}

		uid = vm.request.uid.uid;

		if (vm.admin) {
			MoveCouponService.getUnusedUserCouponsAdmin(uid).then(function (data) {
				vm.unusedCoupons = data;
			});

			loadingDataIfAdmin();
		} else {
			MoveCouponService.getUnusedUserCoupons().then(function (data) {
				vm.unusedCoupons = data;
			});
		}

		// Variables only for long distance
		calculateLongDistanceTotals();

		if (!firstInit) {
			var isUnsetAdministrativeAreaTo = (!_.isString(vm.request.field_moving_to.administrative_area)
				|| _.isEmpty(vm.request.field_moving_to.administrative_area))
				&& _.isString(vm.request.field_moving_to.postal_code)
				&& vm.request.field_moving_to.postal_code.length == 5;
			var isUnsetAdministrativeAreaFrom = (!_.isString(vm.request.field_moving_from.administrative_area)
				|| _.isEmpty(vm.request.field_moving_from.administrative_area))
				&& _.isString(vm.request.field_moving_from.postal_code)
				&& vm.request.field_moving_from.postal_code.length == 5;

			if (isUnsetAdministrativeAreaTo || isUnsetAdministrativeAreaFrom) {
				let promises = [];

				if (isUnsetAdministrativeAreaTo) {
					promises.push(geoCodingService.geoCode(vm.request.field_moving_to.postal_code));
				} else {
					promises.push({});
				}

				if (isUnsetAdministrativeAreaFrom) {
					promises.push(geoCodingService.geoCode(vm.request.field_moving_from.postal_code));
				} else {
					promises.push({});
				}

				$q.all(promises)
					.then(function (result) {
						let editrequest = {};

						if (!_.isEmpty(result[0])) {
							if (result[0].isWrong) {
								SweetAlert.swal('You entered the wrong zip code.', '', 'error');
								return false;
							}
							editrequest.field_moving_to = vm.request.field_moving_to;
							editrequest.field_moving_to.administrative_area = result[0].state;
							editrequest.field_moving_to.locality = result[0].city;
						}

						if (!_.isEmpty(result[1])) {
							if (result[1].isWrong) {
								SweetAlert.swal('You entered the wrong zip code.', '', 'error');
								return false;
							}

							editrequest.field_moving_from = vm.request.field_moving_from;
							editrequest.field_moving_from.administrative_area = result[1].state;
							editrequest.field_moving_from.locality = result[1].city;
						}

						if (!_.isEmpty(editrequest)) {
							RequestServices.updateRequest(vm.request.nid, editrequest);
						}
					});
			}
		}

		if (vm.request.move_size.raw == 11) {
			getCommercialWeight();
			getCommercialTitle();
		}

		getQuoteExplanation();
		setCurrentManagerData();
		initRequestQuote();
		showButtonReview();
		setTooltipData();
		setLdTooltip();
		checkFlatRateSettings();
		checkPickup();
		vm.rootBusy = false;

		if (!firstInit && vm.storageMoves && !vm.Invoice) {
			updateFuelSurcharge();
		}

		firstInit = true;
	}

	function getCurrentServiceTypeName(request) {
		vm.currentServiceType = serviceTypeService.getCurrentServiceTypeName(request);
	}

	function initExtraServices() {
		// Need for init extra services
		if (angular.isDefined(vm.request.extraServices) && !isInitExtraServices) {
			isInitExtraServices = true;
			vm.extraServices = angular.fromJson(vm.request.extraServices);
			angular.copy(vm.extraServices, initialExtraService);
		}

		// if bellow need for update extraService from admin panel
		if (angular.isDefined(vm.request.extraServices)
			&& !angular.equals(angular.fromJson(vm.request.extraServices), vm.extraServices)) {
			vm.extraServices = angular.fromJson(vm.request.extraServices);
			angular.copy(vm.extraServices, initialExtraService);
		}

		// display only request extra service not closing
	}

	function setFieldIsNew() {
		ClientsServices.updateClient(vm.user.uid, {
			data: {
				field_isnew: 1
			}
		});
	}

	if (!!session && !vm.admin && vm.request.request_all_data) {
		vm.request.request_all_data.statistic.user_visit_account = true;

		if (_.isUndefined(vm.request.request_all_data.statistic.first_date_visit_account)) {
			vm.request.request_all_data.statistic.first_date_visit_account = moment().unix();
		}

		vm.request.request_all_data.userVisit = moment().unix();

		saveRequestData();
	}


	function checkPickup() {
		if (vm.request.inventory.move_details.showPickupRangeInAccount) {
			let pickupDate = vm.request.inventory.move_details.pickup;

			let isDateRange = _.isString(pickupDate) && _.indexOf(pickupDate, ',') !== -1;

			if (isDateRange) {
				let result = pickupDate.split(',');
				let firstPickupDate = Number(result[0]);
				let lastAccountDate = Number(result[1]);

				if (Number(result[0]) > Number(result[1])) {
					firstPickupDate = Number(result[1]);
					lastAccountDate = Number(result[0]);
				}

				vm.moveDate = `${moment(firstPickupDate).format('MMM DD, YYYY')} - ${moment(lastAccountDate).format('MMM DD, YYYY')}`;
			} else {
				vm.moveDate = `${moment(pickupDate).format('MMM DD, YYYY')}`;
			}
		}
	}

	function getCommercialWeight(weightType = vm.request.field_useweighttype.value) {
		let defer = $q.defer();

		let baseWeight = Number(vm.request.total_weight.min),
			typeOfWeight = 'Total',
			additional = '';

		switch (Number(weightType)) {
			case 2:
				baseWeight = vm.request.inventory_weight.cfs;
				typeOfWeight = 'Inventory';
				let addText = CommercialCalc.getCurrentCommercial(
					vm.request.commercial_extra_rooms.value,
					vm.request.field_custom_commercial_item,
					vm.calcSettings.commercialExtra
				);
				if (addText && addText.name && addText.name.length) {
					additional = `(With ${addText.name})`;
				}
				break;
			case 3:
				baseWeight = Number($scope.request.field_custom_commercial_item.cubic_feet);
				if (!_.isNull($scope.request.field_custom_commercial_item.name)) {
					typeOfWeight = $scope.request.field_custom_commercial_item.name;
				}
				break;
			case 1:
			default:
				let chosenCommercialExtra = _.find(vm.calcSettings.commercialExtra,
					{id: _.head(vm.request.commercial_extra_rooms.value)});
				if (!_.isUndefined(chosenCommercialExtra)) {
					baseWeight = Number(chosenCommercialExtra.cubic_feet);
					typeOfWeight = chosenCommercialExtra.name;
				}
				break;
		}

		vm.commercialWeight = {
			explanation: `(${typeOfWeight} ${baseWeight || 0} c.f. / ${baseWeight * 7 || 0} lbs) ${additional}`
		};

		if (vm.useCalculator && !vm.admin) {
			_updateRequestTime()
				.then(
					() => {
						defer.resolve();
					},
					() => {
						defer.reject();
					}
				);
		} else {
			defer.resolve();
		}

		return defer.promise;
	}

	$scope.$on('getCommercialWeight', function (ev, {weightType, currentExtra}) {
		vm.request.commercial_extra_rooms = currentExtra;
		getCommercialWeight(weightType);
	});

	function getCommercialTitle() {
		vm.commercialTitle = vm.request.move_size.value;
		let isCustomName = !_.isUndefined(vm.request.request_all_data.commercialCustomName)
			&& vm.request.request_all_data.commercialCustomName.length;
		if (isCustomName) {
			vm.commercialTitle = vm.request.request_all_data.commercialCustomName;
		}
	}

	function showButtonReview() {
		let showReviewBtn = vm.reviewSettings.currentStateShowReviewButton.value;
		let isShowWhenConfirmed = _.isEqual(showReviewBtn, 2) && _.isEqual(vm.request.status.raw, 3);
		let isShowWhenClosed = _.isEqual(showReviewBtn, 3) && vm.isConfirmed;
		let isAlwaysShow = _.isEqual(showReviewBtn, 1) && vm.request.status.raw >= 1;
		vm.showReciewBtn = isShowWhenConfirmed || isShowWhenClosed || isAlwaysShow;
	}

	function checkFlatRateSettings() {
		var flatRateSettings = vm.basicSettings.flatRateAccountSettings.accountSettings;
		$scope.showFlatRateFuel = true;
		$scope.showFlatRateAddService = true;
		$scope.showFlatRateQuote = true;
		$scope.showFlatRateExplanation = true;
		let isFlatRate = vm.request.service_type.raw == '5';
		let status = Number(vm.request.status.raw);
		let flatrateSettingsStatuses = [1, 9, 10, 11];
		let isCorrectStatus = flatrateSettingsStatuses.indexOf(status) >= 0;
		let isNotParsingRequest = !ParserServices.isParserRequest(vm.request);

		if (isFlatRate && isCorrectStatus && isNotParsingRequest) {
			$scope.showFlatRateFuel = flatRateSettings.fuelSurcharge;
			$scope.showFlatRateAddService = flatRateSettings.additionalServices;
			$scope.showFlatRateQuote = flatRateSettings.quote;
			$scope.showFlatRateExplanation = flatRateSettings.explanation;
		}
	}

	function getQuoteExplanation() {
		var serviceType = vm.request.service_type.raw;
		let isCommercial = vm.request.move_size.raw == 11;

		if (serviceType > 7) {
			serviceType--;
		}

		if (isCommercial) {
			vm.quote_note = vm.request.field_quote_explanationn || vm.quoteSettings.quoteExplanation['8'];
		} else {
			vm.quote_note = vm.request.field_quote_explanationn || vm.quoteSettings.quoteExplanation[serviceType];
		}
	}


	function getPackingTotal() {
		var result = 0;

		angular.forEach(vm.packingSettings, function (packing, key) {
			result += packing.total;
		});

		return result;
	}


	function initFuelSurcharge() {
		let defer = $q.defer();
		vm.fuelSurcharge = vm.request.request_all_data.surcharge_fuel;

		calculateLongDistanceTotals();

		saveRequestData()
			.then(
				res => {
					defer.resolve();
				},
				rej => {
					defer.reject();
				}
			);

		return defer.promise;
	}

	$scope.afterChangeValuation = afterChangeValuation;

	function afterChangeValuation(text) {
		let type = vm.Invoice ? 'invoice' : 'request';
		valuationService.reCalculateValuation(vm.request, vm.invoice, type);

		if (!_.isEmpty(text)) {
			valuationService.sendValuationLogs(text, $scope.request.nid);
			valuationService.sendValuationNotification(text, $scope.request.nid);
		}

		if (vm.Invoice) {
			getGrandTotal('invoice', vm.invoice);
		} else {
			getGrandTotal('min', vm.request);
			getGrandTotal('max', vm.request);
		}
		calculateLongDistanceTotals();

		reactivate();
	}

	function calculateLongDistanceTotals() {
		if (vm.longDistance) {
			calculateLongDistance();
			vm.longDistanceGrandTotal = calculateLongDistanceGrandTotal();
			vm.longDistanceGrandTotalDiscount = calculateLongDistanceGrandTotalDiscount();
			vm.longDistanceDiscount = vm.longDistanceQuoteUp - calculateLongDistanceGrandTotalDiscount();
			let realDiscount = vm.longDistanceDiscount * 100 / vm.longDistanceQuoteUp;
			vm.longDistanceDiscountPercent = vm.localdiscount > realDiscount ? vm.localdiscount : realDiscount;

			vm.request.request_all_data.grandTotalRequest.longdistance = vm.DiscountApply
				? vm.longDistanceGrandTotalDiscount
				: vm.longDistanceGrandTotal;
		}
	}

	function calculateLongDistanceRate() {
		let defer = $q.defer();
		if (vm.useCalculator) {
			let promises = [];
			vm.longDistanceRate = CalculatorServices.recalculateLongDistanceRate(vm.request);

			if (vm.longDistanceRate != vm.request.field_long_distance_rate.value &&
				!+_.get($scope.request, 'request_all_data.field_long_distance_rate.value', 0)) {
				vm.request.field_long_distance_rate.value = vm.longDistanceRate;
				let updateRequestPromise = RequestServices.updateRequest(vm.request.nid, {
					field_long_distance_rate: vm.longDistanceRate
				});
				promises.push(updateRequestPromise);
				if (vm.Invoice) {
					vm.invoice.field_long_distance_rate.value = vm.request.field_long_distance_rate.value;
					let updateInvoicePromise = saveRequestData();
					promises.push(updateInvoicePromise);
				}
			}
			$q.all(promises)
				.then(
					res => {
						defer.resolve();
					},
					rej => {
						defer.reject();
					}
				);
		} else {
			defer.resolve();
		}

		return defer.promise;
	}

	function calculateLongDistance() {
		vm.longDistanceQuote = CalculatorServices.getLongDistanceQuote(vm.request);

		if (vm.localdiscount != 0 || vm.request.request_all_data.add_percent_discount ||
			vm.request.request_all_data.add_money_discount || vm.request.request_all_data.add_rate_discount) {

			vm.discountAmount = parseInt(vm.localdiscount);
			vm.longDistanceQuoteUp = Math.round(calculateLongDistanceGrandTotal() / (1 - vm.discountAmount / 100));
		} else {
			vm.longDistanceQuoteUp = calculateLongDistanceGrandTotal();
		}


		//Delivery Window
		var linfo = CalculatorServices.getLongDistanceInfo(vm.request);
		vm.ld_day = linfo.days;
		vm.average_ld_day = linfo.average_days;
		//delivery days
		if (angular.isDefined(vm.request.inventory.move_details)
			&& !_.isNull(vm.request.inventory.move_details)
			&& _.isUndefined(vm.request.inventory.move_details.delivery_days)) {

			vm.request.inventory.move_details.delivery_days = vm.ld_day;
		}
		//Delivery Window
		if (angular.isDefined(vm.request.inventory.move_details)) {
			if (vm.request.inventory.move_details != null) {
				if (angular.isDefined(vm.request.inventory.move_details.delivery)) {
					if (vm.request.inventory.move_details.delivery.length) {
						vm.lastDeliveryDay = CalculatorServices.getLongDistanceWindow(vm.request,
							vm.request.inventory.move_details.delivery,
							vm.request.inventory.move_details.delivery_days);
						vm.lastDeliveryDay = moment(vm.lastDeliveryDay).format('MMMM DD, YYYY');
					}
					else {
						vm.request.inventory.move_details.delivery = '';
						vm.lastDeliveryDay = 0;
					}
				}
			}
		}
	}

	/// END OF ACTIVATE FUNCTION

	function isVisibleExtraServiceTotal() {
		return vm.extraServices.length != 0 && vm.extraServices.length > 1;
	}


	function checkForUpdateLongDistanceExtraServices(request, weight) {
		let defer = $q.defer();
		if (vm.request.service_type.raw == 7 && _.get($scope.request, 'field_usecalculator.value')) {
			let longDistanceExtraServices = additionalServicesFactory.getLongDistanceExtra(request, weight);

			for (let i = vm.extraServices.length - 1; i >= 0; i--) {
				let currentService = vm.extraServices[i];

				if (currentService.autoService
					&& !currentService.fee_all_services
					&& !_.find(longDistanceExtraServices, {'name': currentService.name})
					&& !(request.request_all_data.isChangedExtraservices
					&& request.request_all_data.isChangedExtraservices[additionalServicesFactory.getExtraServicePropertyByName(currentService.name)])) {
					vm.extraServices.splice(i, 1);
				}
			}

			angular.forEach(longDistanceExtraServices, function (service, key) {
				if (vm.extraServices.length == 0) {
					vm.extraServices.push(service);
				} else {
					_.each(vm.extraServices, function (ser, id) {
						if (ser.name == service.name) {
							vm.extraServices[id] = service;
						}
					});
					if (!_.find(vm.extraServices, {'name': service.name})) {
						vm.extraServices.push(service);
					}
				}
			});
		}

		if (!angular.equals(initialExtraService, vm.extraServices)) {
			saveExtraService()
				.then(
					res => {
						defer.resolve();
					},
					rej => {
						defer.reject();
					}
				);
		} else {
			defer.resolve();
		}

		return defer.promise;
	}


	function calculateTotals() {
		var totals = InventoriesServices.calculateTotals(vm.listInventories);
		$scope.inventoryDone = vm.flatRate && totals.counts > 0;

		return totals;
	}


	function saveListInventories(isCustom, list, request, previousInventory) {
		saveInventoryPromise = $q.defer();
		if (list) {
			vm.listInventories = list;
		}

		let pairedId = $scope.request.storage_id || $scope.request.request_all_data.packing_request_id;

		checkAndSaveWeightType();
		continueSavingInventory().finally(() => {
			saveInventoryPromise.resolve();
		});

		function checkAndSaveWeightType() {
			let checkAndSaveWeightTypeDefer = $q.defer();
			let weightTypeChanged = false;

			let isEmptyInventory = _.isEmpty(vm.listInventories.filter(item => item.count));
			if (!isEmptyInventory) {
				let useInventoryWeight = vm.request.field_useweighttype.value == 2;
				if (!useInventoryWeight) {
					setWeightTypeTo(WEIGHT_TYPE_INVENTORY);
				}
			} else {
				let useCustomWeight = vm.request.field_useweighttype.value == 3;
				if (!useCustomWeight) {
					setWeightTypeTo(WEIGHT_TYPE_ROOMS);
				}
				let useCommercialMove = vm.request.move_size.raw == 11;
				if (useCommercialMove) {
					inventoryEditRequest.field_commercial_extra_rooms = vm.request.commercial_extra_rooms.value;
					$scope.request.total_weight = CommercialCalc.getCommercialCubicFeet(
						vm.request.commercial_extra_rooms.value[0], vm.calcSettings,
						vm.request.field_custom_commercial_item);
					let customCommercialChecked = vm.request.field_custom_commercial_item.is_checked;
					if (customCommercialChecked) {
						setWeightTypeTo(WEIGHT_TYPE_CUSTOM);
					}
				}
			}

			if (weightTypeChanged) {
				saveWeightType();
			}

			return checkAndSaveWeightTypeDefer.promise;

			function setWeightTypeTo(value) {
				value = value.toString();
				inventoryEditRequest.field_useweighttype = value;
				vm.request.field_useweighttype.value = +value;
				weightTypeChanged = true;
			}

			function saveWeightType() {
				let requestPromise = [];
				//Save this data in last request
				//requestPromise.push(RequestServices.updateRequest(vm.request.nid, inventoryEditRequest));

				if (pairedId) {
					requestPromise.push(RequestServices.updateRequest(pairedId, inventoryEditRequest));
				}

				checkAndSaveWeightTypeDefer.resolve();
			}
		}

		function continueSavingInventory() {
			let continueSavingInventoryDefer = $q.defer();

			let isNotConfirmed = vm.request.status.raw == 2;
			let isConfirmed = vm.request.status.raw == 3;
			let isFlatRateInventoryNeeded = vm.request.status.raw == 9;
			let isNewInventory = _.get(vm.request, 'field_request_settings.inventoryVersion', 1) == 2;

			if (isNotConfirmed) {
				askForDropToPendingInfo(isConfirm => {
					if (isConfirm) {
						checkStatusToPendingInfo(true);
						saveInventoryAndLogs();
					} else {
						revertInventory();
					}
				});
			} else if (vm.useCalculator && isConfirmed) {
				if (!$scope.ldbefore24) {
					SweetAlert.swal('You can not change inventory',
						'You request status is confirmed. Please contact office for any changes.', 'error');
					revertInventory();
				} else {
					saveInventoryAndLogs();
				}
			} else if (vm.useCalculator && isFlatRateInventoryNeeded) {

				if (!vm.listInventories.length) {
					SweetAlert.swal('You must add at least 1 item', '', 'error');
					continueSavingInventoryDefer.resolve();
				} else {
					if ($scope.detailsDone) {
						submitFlatRate(inventoryEditRequest);
						select(vm.tabs[0]);
						continueSavingInventoryDefer.resolve();
					} else {
						continueSavingInventoryDefer.resolve();
					}
				}
			} else {
				saveInventoryAndLogs();
			}

			return continueSavingInventoryDefer.promise;

			function askForDropToPendingInfo(callback) {
				SweetAlert.swal({
					title: 'Your quote will update based on your inventory',
					text: 'You add inventory and estimate time will change according your inventory.',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Update',
					cancelButtonText: 'Cancel',
					closeOnConfirm: true,
					closeOnCancel: true
				}, callback);
			}

			function revertInventory() {
				if (isNewInventory && !_.isUndefined(previousInventory)) {
					//promise without after
					InventoriesServices
						.revertToPreviousInventory(vm.request.nid, previousInventory)
						.then(() => {
							RequestServices.sendLogs([{
								simpleText: 'Inventory items were restored',
							}], 'Request was not updated', vm.request.nid, 'MOVEREQUEST');
							$rootScope.$broadcast('onReloadInventoryTable');
						});
					vm.listInventories = converInventoryToOldVersion(previousInventory);
					continueSavingInventoryDefer.resolve();
					_saveInventory().finally(() => {
						let oldExtraServices = angular.copy(vm.request.extraServices);
						updateRequestExtraServicesByItems(previousInventory, vm.request);

						let isSaveExtraService = inventoryExtraService.isChangeInventoryExtraService(vm.request.extraServices, oldExtraServices);

						if (isSaveExtraService) {
							additionalServicesFactory.saveExtraServices(vm.request.nid, vm.request.extraServices);
							let extraServiceLog = inventoryExtraService.makeExtraServiceLog(vm.request.extraServices, oldExtraServices);
							RequestServices.sendLogs(extraServiceLog, 'Restore Inventory Additional Services', $scope.request.nid, 'MOVEREQUEST');
						}

						continueSavingInventoryDefer.resolve();
					});
				} else {
					continueSavingInventoryDefer.resolve();
				}
			}

			function saveInventoryAndLogs() {
				if (isNewInventory) {
					_saveInventoryLog();
				} else {
					saveInventoryLog();
				}
				_saveInventory().finally(() => {
					continueSavingInventoryDefer.resolve();
				});
			}
		}
	}

	function _saveInventoryLog() {
		let details = [];

		let msg = {
			text: 'Inventory weight was changed.',
			from: InventoriesServices.calculateTotals(oldInventory).cfs.toFixed() + ' cfs.',
			to: InventoriesServices.calculateTotals(vm.listInventories).cfs.toFixed() + ' cfs.'
		};

		let msgToNotify = '';

		if (msg.from != msg.to) {
			details.push(msg);
			msgToNotify = 'Inventory weight was updated. From:' + msg.from + ' To: ' + msg.to;
		}

		oldInventory = angular.copy(vm.listInventories);

		if (!_.isEmpty(vm.listInventories)) {
			let removedItem = [];

			_.forEach(vm.listInventories, function (item) {
				var activity = '';

				if (item.count < item.oldCount && item.count != 0) {
					activity = 'decreased';
				} else if (item.count > item.oldCount && item.count != 0 && item.oldCount != 0) {
					activity = 'increased';
				} else if (item.count == 0 && item.oldCount > 0) {
					activity = 'removed';
					removedItem.push(angular.copy(item));
				} else if ((item.oldCount == 0 || !item.oldCount) && item.count > 0) {
					activity = 'added';
				}

				if (activity != '') {
					let msg = {
						text: 'Inventory ' + item.title + ' was ' + activity + '.',
						from: item.oldCount || '0',
						to: item.count || '0'
					};

					details.push(msg);
				}
			});

			vm.listInventories = vm.listInventories.filter(item => !_.find(removedItem, {id: item.id}));
		}

		if (!_.isEmpty(msgToNotify) && !_.isEmpty(details)) {
			RequestServices.sendLogs(details, 'Request was updated', vm.request.nid, 'MOVEREQUEST');

			let data = {
				node: {nid: vm.request.nid},
				notification: {
					text: ''
				}
			};

			data.notification.text = msgToNotify;

			let dataNotify = {
				type: enums.notification_types.CUSTOMER_SUBMIT_INVENTORY,
				data: data,
				entity_id: vm.request.nid,
				entity_type: enums.entities.MOVEREQUEST
			};

			apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
		}
	}

	function saveInventoryLog() {
		var details = [];
		if (!_.isEmpty(vm.decreaseItemsLog)) {
			_.forEach(vm.decreaseItemsLog, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was decreased.',
					from: item.oldCount,
					to: item.count
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(vm.increaseItemsLog)) {
			_.forEach(vm.increaseItemsLog, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was increased.',
					to: item.count
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(vm.removedItemsLog)) {
			_.forEach(vm.removedItemsLog, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was removed.'
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(vm.removedItems)) {
			_.forEach(vm.removedItems, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was removed.'
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(vm.decreaseItems)) {
			_.forEach(vm.decreaseItems, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was decreased.',
					from: item.oldCount,
					to: item.count
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(details)) {
			RequestServices.sendLogs(details, 'Request was updated', vm.request.nid, 'MOVEREQUEST');
		}

		vm.decreaseItemsLog = [];
		vm.increaseItemsLog = [];
		vm.removedItemsLog = [];
		vm.removedItems = [];
		vm.decreaseItems = [];
	}

	function _saveInventory() {
		let _saveInventoryDefer = $q.defer();

		vm.busyInventory = true;

		//IF storage Save to Storage same inventory
		if (vm.storageMoves) {
			//promise without calcs after
			InventoriesServices.saveListInventories(vm.request.storage_id, vm.listInventories);
		}

		if (vm.request.request_all_data.packing_request_id) {
			//promise without calcs after
			InventoriesServices.saveListInventories(vm.request.request_all_data.packing_request_id, vm.listInventories);
		}

		//recalculate data
		InventoriesServices.saveListInventories(vm.request.nid, vm.listInventories).then(whenSaved, whenNotSaved);

		return _saveInventoryDefer.promise;

		function whenSaved() {
			let oldInventoryList = angular.copy(vm.request.inventory.inventory_list);
			vm.request.inventory.inventory_list = angular.copy(vm.listInventories);
			recalculateRequestWeightByInventory(vm.request.inventory.inventory_list, oldInventoryList)
				.finally(() => {

					if (vm.useCalculator && !vm.admin) {
						_updateRequestTime(inventoryEditRequest).finally(() => {
							if (!vm.flatRate) {
								//show alert if change time
								if (vm.request.status != 3) {
									SweetAlert.swal('We will update quote based on your inventory',
										'You add inventory and estimate time was change according your inventory',
										'success');
								}
								sharedRequestServices.calculateQuote(vm.request);
							}
							updatePackingsServicesAndValuation()
								.finally(finishRecalcInventoryChain);
						});
					} else {
						updatePackingsServicesAndValuation()
							.finally(finishRecalcInventoryChain);
					}

					if (!vm.useCalculator && !vm.admin && !_.isEmpty(inventoryEditRequest)) {
						RequestServices.updateRequest(vm.request.nid, inventoryEditRequest);
						inventoryEditRequest = {};
					}
				});
		}

		function finishRecalcInventoryChain(success = true) {
			vm.busyInventory = false;
			if (success) {
				_saveInventoryDefer.resolve();
			} else {
				_saveInventoryDefer.reject();
			}
		}

		function whenNotSaved(reason) {
			logger.error(reason, reason, 'Error');
			finishRecalcInventoryChain(false);
		}
	}

	function updatePackingsServicesAndValuation() {
		let afterUpdatingDefer = $q.defer();
		let promises = [];
		promises.push(updateAddPackingService());

		let type = vm.Invoice ? 'invoice' : 'request';
		if (_.get(vm[type], 'request_all_data.valuation')
			&& !_.isUndefined(vm[type].request_all_data.valuation.selected.valuation_charge)
			&& vm[type].field_useweighttype.value == 2) {
			let valuationPromise = valuationService.reCalculateValuation(vm.request, vm.invoice, type);
			promises.push(valuationPromise);
		}

		$scope.inventoryTotals = calculateTotals();
		promises.push(updateExtraServices());

		$q.all(promises)
			.finally(() => {
				afterUpdatingServicesAndPackingsAndValuation()
					.finally(() => {
						afterUpdatingDefer.resolve();
					});
			});

		return afterUpdatingDefer.promise;
	}

	function afterUpdatingServicesAndPackingsAndValuation() {
		let defer = $q.defer();
		initExtraServices();
		getCommercialWeight(vm.request.field_useweighttype.value).finally(() => {
			updateRequestQuote()
				.finally(() => {
					defer.resolve();
				});
		});
		return defer.promise;
	}

	$scope.$on('storage.request.created', function (ev, data) {
		vm.request.request_all_data.storage_request_id = data;
		saveRequestData();
	});

	$scope.$on('details.changed', function (event, data) {
		vm.details = data;
		vm.ifDetails = CheckRequestService.isMoveDetailsChanged(vm.details, vm.request.service_type.raw);
		vm.detailHasComment = vm.details.addcomment.length;

		if (angular.isUndefined(vm.request.inventory.move_details)) {
			vm.request.inventory.move_details = _.clone(vm.details);
		}

		if (angular.isDefined(vm.request.inventory.move_details) && vm.request.inventory.move_details != null) {
			if (vm.request.inventory.move_details.delivery != '' && vm.request.inventory.move_details.delivery != null) {
				vm.lastDeliveryDay = CalculatorServices.getLongDistanceWindow(vm.request, vm.request.inventory.move_details.delivery);
				vm.lastDeliveryDay = moment(vm.lastDeliveryDay).format('MMMM DD, YYYY');
			} else {
				vm.request.inventory.move_details.delivery = '';
				vm.lastDeliveryDay = 0;
			}
		}

		if (vm.flatRateService) {
			select(vm.tabs[1]);
			$scope.detailsDone = true;
			vm.request.inventory.move_details = vm.details;
		} else {
			select(vm.tabs[0]);
		}

		updateExtraServices();
		onForceReloadAccount();
	});

	//PHOTOS
	function savePhotos(files) {
		$scope.uploadedBusy = true;
		let imagesArr = {
			field_move_images: {
				add: []
			}
		};
		let loadPhotoPromises = [];

		angular.forEach(files, function (flowFile, i) {
			let fileReader = new FileReader();
			let defer = $q.defer();

			fileReader.onload = (function (file, defer) {
				return function (e) {
					let photo = {
						uri: e.target.result,
						title: file.title
					};
					imagesArr['field_move_images']['add'].push(photo);
					endFileReadHandler(i + 1);
					let imageToShow = {
						url: e.target.result,
						title: file.title
					};
					vm.addedImages.push(imageToShow);
				};
			})(flowFile, defer);

			loadPhotoPromises.push(defer.promise);

			fileReader.readAsDataURL(flowFile.file);
		});

		$q.all(loadPhotoPromises)
			.then(() => {
				endFileReadHandler();
			});

		function endFileReadHandler() {
			if (imagesArr['field_move_images']['add'].length > 0) {
				RequestServices.updateRequest(vm.request.nid, imagesArr)
					.then(function (response) {
							var msg = {
								simpleText: 'Photos on a client\'s account page were successfully added'
							};
							var arr = [];
							arr.push(msg);
							let data = {
								notification: {
									text: 'Photos on a client\'s account page was successfully added'
								},
								node: {
									nid: vm.request.nid
								}
							};
							let dataNotify = {
								type: enums.notification_types.CUSTOMER_DO_CHANGE_WITH_REQUEST,
								data: data,
								entity_id: vm.request.nid,
								entity_type: enums.entities.MOVEREQUEST
							};
							apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
							RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
							SweetAlert.swal('Upload photo', 'Successfully Saved', 'success');
							$scope.uploader.flow.files = [];

							angular.forEach(vm.addedImages, function (newImg) {
								$scope.images.push(newImg);
								$scope.uploadedBusy = false;
							});

							vm.addedImages = [];
						}
					).catch((error) => {
					SweetAlert.swal('Problem with uploading photos', 'Please try load less than 10 photos or contact your relocation manager', 'error');
				});
			}
		}
	}


	function remove(fid, index) {
		$scope.uploadedBusy = true;
		var obj = {
			'data': {
				'field_move_images': {'del': [{'fid': fid}]}
			}
		};
		RequestServices.removeImage(vm.request.nid, obj).then(function (response) {
			$scope.images.splice(index, 1);
			var msg = {
				simpleText: 'Photo on request page was successfully removed'
			};
			var arr = [];
			arr.push(msg);
			let data = {
				node: {nid: $scope.request.nid},
				notification: {
					text: 'Photo on request page was successfully removed'
				}
			};
			let dataNotify = {
				type: enums.notification_types.CUSTOMER_DO_CHANGE_WITH_REQUEST,
				data: data,
				entity_id: $scope.request.nid,
				entity_type: enums.entities.MOVEREQUEST
			};
			apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
			RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
			$scope.uploadedBusy = false;
		}, function (err) {

		});
	}


	if (firstInit) {
		vm.template = vm.tabs[0];
	}

	function select(tab) {
		angular.forEach(vm.tabs, function (tab) {
			tab.selected = false;
		});
		angular.forEach(vm.tabs2, function (tab) {
			tab.selected = false;
		});
		angular.forEach(vm.tabs3, function (tab) {
			tab.selected = false;
		});
		angular.forEach(vm.tabs4, function (tab) {
			tab.selected = false;
		});
		angular.forEach(vm.tabs5, function (tab) {
			tab.selected = false;
		});
		angular.forEach(vm.tabs6, function (tab) {
			tab.selected = false;
		});
		angular.forEach(vm.tabs8, function (tab) {
			tab.selected = false;
		});

		tab.selected = true;
		vm.template = tab;


		if (tab.name == 'Messages') {
			//read all new comments
			angular.forEach(vm.messages, function (m, cid) {

				var read = !m.read;
				var admin = !!m.admin;

				if (read && admin) {
					MessagesServices.setCommentReaded(m.cid);
				}

			});
			vm.request.messages = 0;
		}

		if (tab.name == 'Inventory') {
			$interval.cancel($scope.reactivateRequestInterval);
			openInventoryModal();
			vm.inventoryEdit = true;
			oldInventory = angular.copy(vm.request.inventory.inventory_list);
		}
		else {
			vm.inventoryEdit = false;
		}

		vm.detailsEdit = tab.name == 'Details';
	}


	function openInventoryModal() {
		$scope.modalInstance = $uibModal.open({
			templateUrl: 'app/requestpage/templates/inventory.html',
			windowClass: 'inventory_modal',
			controller: inventoryModalCtrl,
			backdrop: false,
			keyboard: false,
			resolve: {
				vm: () => vm,
				cancel: () => $scope.closeInventoryModal
			}
		});
	}

	function inventoryModalCtrl($scope, vm, cancel) {
		$scope.vm = vm;
		$scope.cancel = cancel;
	}

	$scope.closeInventoryModal = () => {
		$scope.modalInstance.close();
		select(vm.tabs[0]);
		if (saveInventoryPromise) {
			vm.busy = true;
			saveInventoryPromise.promise
				.then(() => {
					saveInventoryPromise = false;
					vm.busy = false;
					reactivate();
					tryToRegisterCustomerOnline();
				});
		} else {
			reactivate();
			tryToRegisterCustomerOnline();
		}

		if (saveInventoryPromise && !$scope.request.request_all_data.reservationChangedManually && $scope.request.service_type.raw == 7) {
			grandTotalService.getReservationRate($scope.request);
			RequestServices.updateRequest($scope.request.nid, {field_reservation_price: $scope.request.reservation_rate.value});
		}
	};


	function openEditModal() {
		$interval.cancel($scope.reactivateRequestInterval);
		isOpenEditModal = true;

		let modalObject = {
			templateUrl: 'app/requestpage/services/request-edit-form.html',
			controller: RequestPageServices.ModalInstanceCtrl,
			resolve: {
				initial_request: function () {
					return vm.request;
				}
			}
		};

		if (invalidParserRequest) {
			invalidParserRequest = false;
			modalObject.backdrop = 'static';
			modalObject.keyboard = false;
		}

		var modalInstance = $uibModal.open(modalObject);

		var msg = {
			simpleText: 'User click \'Manage Quote\''
		};

		var arr = [];
		arr.push(msg);
		RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');

		modalInstance.result.then(function (request) {
			vm.request = request;
			recalculateRequestWeightByMoveSize().finally(() => {
				_updateRequestTime().finally(() => {
					updatePackingsServicesAndValuation().finally(() => {
						isOpenEditModal = false;
						reactivate();
						tryToRegisterCustomerOnline();
					});
				});
			});
		});
	}


	function openLightboxModal(index) {
		Lightbox.openModal(vm.request.field_move_images, index);
	}


	function openCongratsModal() {
		RequestPageServices.openCongratsModal(vm.request);
	}


	function submitFlatRate(editrequest) {
		let submitFlatRateDeferred = $q.defer();
		let data = {};

		if (angular.isDefined(editrequest)) {
			data = editrequest;
		}

		if (vm.longDistance) {
			data['field_approve'] = 1;
			vm.flatStepOne = false;

			InventoriesServices.saveListInventories(vm.request.nid, vm.listInventories)
				.then(function (d) {
					if (vm.useCalculator && !vm.admin) {
						_updateRequestTime(data);
					}

					select(vm.tabs[0]);
				});

			SweetAlert.swal('Submited!', 'Thank you for submitting  long distance request', 'success');
			vm.request.status.raw = 1;
			vm.request.status.value = 'Pending';
			vm.statusTemplate = vm.request_statuses[1].template;
			vm.statusText = vm.request_statuses[1].text;
			vm.flatRate = false;
			vm.longDistanceQuote = CalculatorServices.getLongDistanceQuote(vm.request);
			//Delivery Window
			if (angular.isDefined(vm.request.inventory.move_details)) {
				if (angular.isDefined(vm.request.inventory.move_details.delivery)) {
					if (vm.request.inventory.move_details.delivery.length) {
						vm.lastDeliveryDay = CalculatorServices.getLongDistanceWindow(vm.request,
							vm.request.inventory.move_details.delivery,
							vm.request.inventory.move_details.delivery_days);
						vm.lastDeliveryDay = moment(vm.lastDeliveryDay).format('MMMM DD, YYYY');
					} else {
						vm.request.inventory.move_details.delivery = '';
						vm.lastDeliveryDay = 0;
					}
				}
			}

		} else {
			data = {
				field_approve: 10
			};
			vm.flatStepOne = false;
			var pi = InventoriesServices.saveListInventories(vm.request.nid, vm.listInventories);
			pi.then(function (d) {
				RequestServices.updateRequest(vm.request.nid, data);
				inventoryEditRequest = {};
			});
			SweetAlert.swal('Submited!', 'Thank you for submiting flat rate request', 'success');
			vm.request.status.raw = 10;
			vm.statusTemplate = vm.request_statuses[10].template;
			vm.statusText = vm.request_statuses[10].text;
		}

		activate(vm.request);

		return submitFlatRateDeferred.promise;
	}


	function goToRequest(nid) {
		$location.path('/request/' + nid);
		let anotherRequest = _.get(vm.requests, nid);
		if (anotherRequest) $scope.reconnectCustomer(vm.request.uid.uid, nid, anotherRequest);
	}

	function goToNewRequest(nid) {
		vm.busy = true;
		RequestServices.getRequest(nid)
			.then(data => {
				vm.busy = false;
				goToRequest(data.nid);
			});
	}


	function goStorageTenant(id) {
		$location.path('/storage_tenant/' + id);
	}


	function chooseOption(option) {
		requestModals.confirmUpdate(vm.request, option);
	}

	$rootScope.$on('payment.received', onPaymentReceived);
	$rootScope.$on('payment.reservation', onPaymentReceived);

	function onPaymentReceived() {
		vm.request.status.raw = 3;
		vm.request.status.value = 'Confirmed';
		vm.statusTemplate = vm.request_statuses[vm.request.status.raw].template;
		vm.statusText = vm.request_statuses[vm.request.status.raw].text;
		onForceReloadAccount();
		$location.path('/request/' + vm.request.nid);
	}

	$scope.$on('option.choosed', function (event, data) {

		SweetAlert.swal('Success', '', 'success');
		vm.request.status.raw = 1;
		vm.request.status.value = 'Pending';
		vm.statusTemplate = vm.request_statuses[1].template;
		vm.statusText = vm.request_statuses[1].flat;
		vm.flatRate = false;
		vm.request.flat_rate_quote.value = data.rate;
		vm.moveDate = data.pickup;
		vm.deliveryDate = data.delivery;
		vm.request.start_time1.value = common.toUSAtime(data.picktime1);
		vm.request.start_time2.value = common.toUSAtime(data.picktime2);
		vm.request.dstart_time1.value = common.toUSAtime(data.deltime1);
		vm.request.dstart_time2.value = common.toUSAtime(data.deltime2);

		common.$timeout(function () {
			RequestPageServices.initMap(vm.request.field_moving_from.postal_code,
				vm.request.field_moving_to.postal_code);
		}, 500);

		updateFuelSurcharge();
	});

	$scope.$watch('vm.request.status.raw', statusChanged);
	function statusChanged(newStatus, oldStatus) {
		if (oldStatus == undefined) return;
		if (angular.isDefined(vm.request_statuses[newStatus])) {
			vm.statusTemplate = vm.request_statuses[newStatus].template;
			vm.statusText = vm.request_statuses[newStatus].text;

			if (vm.flatRateService) {
				vm.statusText = vm.request_statuses[newStatus].flat;
			}
		}

		if (angular.isUndefined(vm.statusTemplate)) {
			vm.statusTemplate = 'app/requestpage/templates/statuses/default.html';
			if (angular.isDefined(vm.request_statuses[newStatus])) {
				vm.statusText = vm.request_statuses[newStatus].text;
			}
		}

		if (newStatus == 3) {
			vm.Invoice = true;
			setUpInvoiceVariable();
			updateFuelSurcharge();
		}

		vm.request.status.value = STATUS_NAMES[newStatus];
		tryToRegisterCustomerOnline();
	}

	$scope.$watchCollection('vm.request', requestChanged);

	function requestChanged(newVal, oldVal) {
		if (angular.isDefined(newVal)
			&& angular.isDefined(oldVal)
			&& !_.isEmpty(newVal)
			&& !_.isEmpty(oldVal)) {

			if (angular.isDefined(vm.requests[newVal.nid])) {
				vm.requests[vm.request.nid] = vm.request;
			}

			if (newVal.service_type.raw != oldVal.service_type.raw) {

				// only for request to storage
				if (vm.request.service_type.raw == 6 && !vm.request.request_all_data.toStorage) {
					RequestHelperService.addOvernightExtraService(vm.request);

					if (vm.request.storage_id != 0
						&& !_.isUndefined(vm.requests[vm.request.storage_id])
						&& !vm.requests[vm.request.storage_id].request_all_data.toStorage) {

						RequestHelperService.addOvernightExtraService(vm.requests[vm.request.storage_id]);
					}
				} else {
					RequestHelperService.removeOvernightExtraService(vm.request);

					if (vm.storageMoves) {
						if (!_.isUndefined(vm.requests[vm.request.storage_id])) {
							RequestHelperService.removeOvernightExtraService(vm.requests[vm.request.storage_id]);
						} else {
							RequestServices
								.getRequestsByNid(vm.request.storage_id)
								.then(function (data) {
									if (_.isUndefined(vm.requests)) {
										vm.requests = {};
									}

									vm.requests[vm.request.storage_id] = data.nodes[0];

									RequestHelperService.removeOvernightExtraService(
										vm.requests[vm.request.storage_id]);
								});
						}
					}
				}
			}

			if (!_.isEqual(newVal.date.value, oldVal.date.value)) {
				vm.moveDate = moment(newVal.date.value).format('MMMM DD, YYYY');
			}

			let postalFromChanged = !_.isEqual(newVal.field_moving_from.postal_code, oldVal.field_moving_from.postal_code);
			let postalToChanged = !_.isEqual(newVal.field_moving_to.postal_code, oldVal.field_moving_to.postal_code);
			if (postalFromChanged || postalToChanged) {
				if (vm.longDistance) {
					CalculatorServices.updateLdMinWeightSettings(vm.request, true);
					if (vm.Invoice) {
						vm.invoice.field_moving_to = angular.copy(vm.request.field_moving_to);
						vm.invoice.field_moving_from = angular.copy(vm.request.field_moving_from);
						CalculatorServices.updateLdMinWeightSettings(vm.invoice, true);
					}
					calculateLongDistanceRate();
					saveRequestData();
				}
				RequestPageServices.initMap(vm.request.field_moving_from.postal_code, vm.request.field_moving_to.postal_code);
				updateRequestQuote();
			}
		}
	}

	function showReceipt(id) {
		PaymentServices.openReceiptModal(vm.request, vm.request.receipts[id], true);
	}


	function calculateRowTotal(service) {
		return additionalServicesFactory.calculateRowTotal(service);
	}


	function calculateExtraServiceTotal() {
		var result = 0;

		angular.forEach(vm.extraServices, function (extraService, key) {
			result += calculateRowTotal(extraService);
		});

		return result;
	}


	function calculateAddServices() {
		var result = 0;

		if (angular.isDefined(vm.request)) {
			result = parseFloat(additionalServicesFactory.getExtraServiceTotal(vm.extraServices));
		}

		return result;
	}

	if (!vm.admin) {
		var promise = MessagesServices.getUnreadComments();

		promise.then(function (data) {
			vm.busy = false;
			vm.unreadComments = data;
		}, function (reason) {
			vm.busy = false;
		});
	}


	function GetAreaCode() {
		var defer = $q.defer();
		var promise = CalculatorServices.getLongDistanceCode(vm.request.field_moving_to.postal_code);
		promise.then(function (data) {
			vm.request.field_moving_to.premise = data;
			let editrequest = {};
			editrequest.field_moving_to = vm.request.field_moving_to;
			RequestServices.updateRequest(vm.request.nid, editrequest);
			vm.areaCode = data;
			defer.resolve();
		}, function () {
			defer.reject();
		});
		return defer.promise;
	}


	function cancelMove(cancel) {
		if (vm.interested != cancel) {
			return;
		}

		if (cancel) {
			SweetAlert.swal({
				title: 'Not interested in move ?',
				text: 'Please tell us a reason:',
				type: 'input',
				showCancelButton: true,
				closeOnConfirm: false,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Submit',
				animation: 'slide-from-top',
				inputPlaceholder: 'Your reason'
			},
			function (inputValue) {
				if (inputValue === false) {
					return false;
				}
				if (inputValue.length < 15) {
					swal.showInputError('You need to write minimum 15 characters!');
					return false;
				}

				var editrequest = {field_approve: 18};
				vm.interested = false;
				vm.notinterested = !vm.interested;
				vm.request.status['raw'] = '18';
				vm.request.status['value'] = 'Not Interested';
				vm.statusTemplate = vm.request_statuses[1].template;
				vm.statusText = '';

				RequestServices.updateRequest(vm.request.nid, editrequest)
					.then(function () {
						let msgNotify = 'Customer doesn\'t is interested in moving. Reason: ' + inputValue;
						sendNotification(msgNotify);
						if (angular.isUndefined(vm.request.inventory.move_details)) {
							vm.request.inventory.move_details = {};
						}
						vm.request.inventory.move_details.cancel_reason = inputValue;
						RequestServices.saveDetails(vm.request.nid, vm.request.inventory.move_details);
					});

						swal('You can always change your move status!', 'Just click \'YES\' or change the move information', 'success');
					});
			} else {
				var editrequest = {field_approve: 1};
				RequestServices.updateRequest(vm.request.nid, editrequest);
				vm.request.status['raw'] = '1';
				vm.request.status['value'] = 'Pending';
				let msgNotify = 'Customer is interested in moving.';
				sendNotification(msgNotify);
				vm.interested = true;
				vm.statusTemplate = vm.request_statuses[1].template;
				vm.statusText = vm.request_statuses[1].text;
				swal('Welcome Back!', 'Your status is pending. You can update any information now.', 'success');
			}
		}

	function sendNotification(text) {
		let data = {
			node: {nid: vm.request.nid},
			notification: {
				text: text
			}
		};
		let dataNotify = {
			type: enums.notification_types.CUSTOMER_SEND_MESSAGE,
			data: data,
			entity_id: vm.request.nid,
			entity_type: enums.entities.MOVEREQUEST
		};

		apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
	}

	function _updateRequestTime(editrequest = {}) {
		let _updateRequestTimeDefer = $q.defer();

		let isNotUpdateTime = !vm.useCalculator || vm.admin;

		if (isNotUpdateTime) {
			_updateRequestTimeDefer.resolve();

			return _updateRequestTimeDefer.promise;
		}

		if (_.isUndefined(vm.listInventories)) {
			vm.listInventories = [];
		}

		vm.request.inventory_weight = InventoriesServices.calculateTotals(vm.listInventories);

		if (vm.request.inventory.inventory_list != vm.listInventories) {
			vm.request.inventory.inventory_list = vm.listInventories;
		}

		let total_cf = fetchWeight();

		if (vm.request.duration.value == null) {
			vm.request.duration.value = 0;
		}

		let isFlatRate = vm.request.service_type.raw == 5 || vm.request.service_type.raw == 7;
		let isStorage = vm.request.service_type.raw == 2 || vm.request.service_type.raw == 6;
		let distance = parseFloat(vm.request.distance.value);
		let duration = parseFloat(vm.request.duration.value);
		let settingMiles = parseFloat(vm.basicSettings.local_flat_miles);

		if (!!settingMiles && distance > settingMiles && !isFlatRate && !isStorage || isFlatRate) {
			duration = 0;
		}

		//Calculate tne time base on inventory
		vm.request.typeFrom = vm.request.type_from.raw;
		vm.request.typeTo = vm.request.type_to.raw;
		vm.request.serviceType = vm.request.service_type.raw;
		let results = CalculatorServices.calculateTime(vm.request, total_cf);
		let rate = CalculatorServices.getRate(vm.request.date.value, results.movers_count, results.trucks);

		if (!vm.calcSettings.doubleDriveTime && !vm.calcSettings.isTravelTimeSameAsDuration) {
			results.work_time.max = results.work_time.max + duration;
			results.work_time.min = results.work_time.min + duration;

			results.work_time.max = CalculatorServices.getRoundedTime(results.work_time.max);
			results.work_time.min = CalculatorServices.getRoundedTime(results.work_time.min);
		}

		let isUpdateRateStatus = !vm.Invoice && vm.request.status.raw != 2;

		if (rate != vm.request.rate.value && vm.request.request_all_data.add_rate_discount != vm.request.rate.value && isUpdateRateStatus) {
			editrequest['field_price_per_hour'] = rate;
			vm.request.rate.value = rate;
		}

		if (results.movers_count != vm.request.crew.value) {
			editrequest['field_movers_count'] = results.movers_count;
			vm.request.crew.value = results.movers_count;
		}

		if (results.work_time.max != vm.request.maximum_time.raw) {
			editrequest['field_maximum_move_time'] = results.work_time.max;
			vm.request.maximum_time.raw = results.work_time.max;
			vm.request.maximum_time.value = common.decToTime(results.work_time.max);

		}

		if (results.work_time.min != vm.request.minimum_time.raw) {
			editrequest['field_minimum_move_time'] = results.work_time.min;
			vm.request.minimum_time.raw = results.work_time.min;
			vm.request.minimum_time.value = common.decToTime(results.work_time.min);

		}

		if (results.trucks != vm.request.truckCount) {
			vm.request.truckCount = results.trucks;
		}

		if (!_.isEmpty(editrequest)) {
			RequestServices.updateRequest(vm.request.nid, editrequest);
			inventoryEditRequest = {};
			updateRequestQuote()
				.then(
					() => _updateRequestTimeDefer.resolve(),
					() => _updateRequestTimeDefer.reject()
				);
		} else {
			_updateRequestTimeDefer.resolve();
		}

		return _updateRequestTimeDefer.promise;
	}

	function calculateTotalBalance(type) {
		var total = 0;
		$scope.total_min = 0;
		$scope.total_max = 0;

		if (angular.isDefined(vm.request)) {
			if (type == 'min') {
				total = vm.request.quote.min;
				$scope.total = vm.request.quote.min;

			} else if (type == 'max') {
				$scope.total_max = vm.request.quote.max;
				total = $scope.total_max;
			} else if (type == 'longdistance') {
				total = vm.longDistanceQuote + calculateAddServices() - vm.request.request_all_data.add_money_discount;
			}
		}

		return total;
	}

	function updateExtraServices() {
		let promises = [];
		let weight = CalculatorServices.getTotalWeight(vm.request).weight;

		if (weight == 0) {
			weight = CalculatorServices.getCubicFeet(vm.request);
			weight = weight.weight;
		}

		promises.push(checkForUpdateLongDistanceExtraServices(vm.request, weight));
		promises.push(recalculateEquipmentFee());

		return $q.all(promises);
	}

	function recalculateEquipmentFee() {
		let defer = $q.defer();
		additionalServicesFactory.recalculateEquipmentFee($scope.request, $scope.invoice, wasChanged => {
			if (wasChanged) {
				saveExtraService()
					.then(
						res => {
							defer.resolve();
						},
						rej => {
							defer.reject();
						}
					);
			} else {
				defer.resolve();
			}
		});
		return defer.promise;
	}

	function isPackingHide() {
		let serviceType = _.get(PACKING_SETTINGS, `serviceType[${vm.request.service_type.raw}].show`);
		let status = _.get(PACKING_SETTINGS, `status[${vm.request.status.raw}].show`);

		return PACKING_SETTINGS.hidePrice && serviceType && status;
	}

	function choosePackingService(value, clicked) {
		let isAllowToPendingInfo = !!+vm.request.field_allow_to_pending_info
			&& value != 0
			&& vm.request.status.raw == 2;

		if (vm.packingUpdating
			|| _.isEqual(value, vm.request.request_all_data.requiredPackingService)) {
			return;
		}

		if (isAllowToPendingInfo) {
			SweetAlert.swal({
				title: 'Status will be changed to "Pending-info" to determine the price',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'No',
				confirmButtonText: 'Yes, I agree'
			}, (confirm) => {
				if (!confirm) {
					vm.packing_service = vm.request.request_all_data.requiredPackingService;
					return;
				}

				checkStatusToPendingInfo();
				continueChangePacking(value, clicked);
			});
		} else {
			continueChangePacking(value, clicked);
		}
	}

	function continueChangePacking(value, clicked) {
		vm.request.request_all_data.requiredPackingService = value;
		saveRequestData();
		isPackingSettingsChanged = true;
		let weight = fetchWeight();
		var fullPackingIndex = _.findIndex(vm.packingSettings, {name: NAME_FULL_PACKING});
		var partialPackingIndex = _.findIndex(vm.packingSettings, {name: NAME_PARTIAL_PACKING});

		value = parseInt(value);
		sendPackingLogs();
		if (!vm.packingAdvancedSettings) {
			switch (+vm.request.request_all_data.requiredPackingService) {
			case 0:
				if (fullPackingIndex >= 0) {
					removeCharge(fullPackingIndex);
					savePackingService();
					removePackingSendLog();
				}

				if (partialPackingIndex >= 0) {
					removeCharge(partialPackingIndex);
					savePackingService();
					removePackingSendLog();
				}
				break;
			}
		} else {
			switch (value) {
			case 0:
				if (fullPackingIndex >= 0) {
					removeCharge(fullPackingIndex);
					savePackingService();
					removePackingSendLog();
				}

				if (partialPackingIndex >= 0) {
					removeCharge(partialPackingIndex);
					savePackingService();
					removePackingSendLog();
				}

				break;
			case 1:
			case 2:

				mergePackingService(value, weight.weight, clicked);
				break;
			}
		}
	}


	function removePackingSendLog() {
		isPackingSettingsChanged = false;
		var msg = {
			simpleText: 'Packing is not required'
		};
		var arr = [msg];
		let data = {
			node: {nid: vm.request.nid},
			notification: {
				text: 'Packing is not required'
			}
		};
		let dataNotify = {
			type: enums.notification_types.CUSTOMER_DO_CHANGE_WITH_REQUEST,
			data: data,
			entity_id: vm.request.nid,
			entity_type: enums.entities.MOVEREQUEST
		};
		apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
		RequestServices.sendLogs(arr, 'Request was updated', vm.request.nid, 'MOVEREQUEST');
	}

	function mergePackingService(value, weight, clicked) {
		var fullPackingIndex = _.findIndex(vm.packingSettings, {name: NAME_FULL_PACKING});
		var partialPackingIndex = _.findIndex(vm.packingSettings, {name: NAME_PARTIAL_PACKING});
		var index = -1;
		var oldWeight = 0;
		var oldName = '';

		if (partialPackingIndex >= 0) {
			index = partialPackingIndex;
		}

		if (fullPackingIndex >= 0) {
			index = fullPackingIndex;
		}

		if (index >= 0) {
			oldWeight = vm.packingSettings[index].quantity;
			oldName = vm.packingSettings[index].name;
		}

		var isLongDistance = vm.request.service_type.raw == '5' || vm.request.service_type.raw == '7';
		var name = value == 1 ? NAME_PARTIAL_PACKING : NAME_FULL_PACKING;
		var parkingSettingLM = vm.packingService['LM'];
		var costLM = value == 1 ? Number(parkingSettingLM.partial) / 100 : Number(parkingSettingLM.full);
		var fullLM = value == 1 ? Number(parkingSettingLM.full) : 1;
		var rateValueLM = fullLM * costLM;
		var parkingSettingLD = vm.packingService['LD'];
		var costLD = value == 1 ? Number(parkingSettingLD.partial) / 100 : Number(parkingSettingLD.full);
		var fullLD = value == 1 ? Number(parkingSettingLD.full) : 1;
		var rateValueLD = fullLD * costLD;

		var custom_extra = {
			rate: rateValueLM,
			name: name,
			originName: name,
			quantity: weight,
			LDRate: rateValueLD,
			laborRate: 0,
			showInContract: true,
			custom: true,
			total: parseFloat(((isLongDistance ? rateValueLD : rateValueLM) * weight).toFixed(2))
		};

		if (oldWeight != weight || name != oldName) {
			var typeSelect = ' was selected';

			if (index >= 0) {
				typeSelect = ' was auto updated';
				vm.packingSettings[index] = custom_extra;
			} else {
				vm.packingSettings.push(custom_extra);
			}

			var msg = {
				simpleText: custom_extra.name + typeSelect
			};

			var arr = [msg];
			let data = {
				node: {nid: vm.request.nid},
				notification: {
					text: msg.simpleText
				}
			};
			let dataNotify = {
				type: enums.notification_types.CUSTOMER_DO_CHANGE_WITH_REQUEST,
				data: data,
				entity_id: vm.request.nid,
				entity_type: enums.entities.MOVEREQUEST
			};
			if (clicked) {
				apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
			}
			RequestServices.sendLogs(arr, 'Request was updated', vm.request.nid, 'MOVEREQUEST');

			savePackingService();
		} else {
			isPackingSettingsChanged = false;
		}
	}


	function addExtraCharges(name, value, weight, full) {
		var custom_extra = {
			name: name,
			extra_services: [
				{
					services_default_value: value,
					services_name: 'Cost',
					services_read_only: true,
					services_type: 'Amount'
				},
				{
					services_default_value: full,
					services_name: 'Value',
					services_read_only: true,
					services_type: 'Number'
				},
				{
					services_default_value: weight,
					services_name: 'Weight',
					services_read_only: true,
					services_type: 'Weight'
				},
			]
		};
		var msg = {
			simpleText: custom_extra.name + ' was selected'
		};
		var arr = [];
		arr.push(msg);
		let data = {
			node: {nid: vm.request.nid},
			notification: {
				text: msg.simpleText
			}
		};
		let dataNotify = {
			type: enums.notification_types.CUSTOMER_DO_CHANGE_WITH_REQUEST,
			data: data,
			entity_id: vm.request.nid,
			entity_type: enums.entities.MOVEREQUEST
		};
		apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
		RequestServices.sendLogs(arr, 'Request was updated', vm.request.nid, 'MOVEREQUEST');
		vm.extraServices.push(custom_extra);
		savePackingService();
	}


	function removeCharge(index) {
		vm.packingSettings.splice(index, 1);
	}

	vm.hideCouponButton = true;
	MoveCouponService.getCouponsByAdmin()
		.then((data) => {
			_.forEach(data, (item) => {
				if (item.value.active && item.value.coupons_count != 0) {
					vm.hideCouponButton = false;
					return false;
				}
			});
		});

	$scope.$on('origin_address', function (event, address) {
		vm.request.field_moving_to = _.clone(address.field_moving_to);
		vm.request.field_moving_from = _.clone(address.field_moving_from);
	});


	function flatrateQuoteRequestChange() {
		var total = 0;

		if (angular.isDefined(vm.request.flat_rate_quote)
			&& angular.isDefined(vm.request.flat_rate_quote.value)) {

			total += Number(vm.request.flat_rate_quote.value);
		}

		return total;
	}


	function updateFuelSurcharge() {
		let defer = $q.defer();
		let typeCalculate = vm.Invoice ? 'invoice' : 'request';

		if (_.isUndefined(vm[typeCalculate].request_all_data)) {
			return;
		}

		let quote = 0;

		if (!vm[typeCalculate].service_type) {
			vm[typeCalculate].service_type = vm.request.service_type;
		}

		if (vm[typeCalculate].service_type.raw == 5) {
			quote = flatrateQuoteRequestChange();
		} else {
			if (vm[typeCalculate].service_type.raw == 7) {
				if (!vm.longDistanceQuote) {
					vm.longDistanceQuote = CalculatorServices.getLongDistanceQuote(vm.request);
				}

				quote = vm.longDistanceQuote;
			} else {
				quote = +((calculateTotalBalance('min') + calculateTotalBalance('max')) / 2).toFixed(2);
			}
		}

		FuelSurchargeService.updateFuelSurcharge(vm, quote, typeCalculate, '', true)
			.then(function () {
				if (typeCalculate == 'invoice' && $scope.request.service_type.raw == 7) {
					vm.request.request_all_data.surcharge_fuel = vm.invoice.request_all_data.surcharge_fuel;
				}

				initFuelSurcharge();
				defer.resolve();
			}, function () {
				defer.reject();
			});

		return defer.promise;
	}


	function updateRequestQuote() {
		let defer = $q.defer();
		initRequestQuote();

		updateFuelSurcharge()
			.then(
				() => {
					defer.resolve();
				},
				() => {
					defer.reject();
				});
		return defer.promise;
	}

	function initRequestQuote() {
		if (vm.longDistance) {
			vm.request.quote = CalculatorServices.getLongDistanceQuote(vm.request);
			calculateLongDistance();
		} else {
			sharedRequestServices.calculateQuote(vm.request);
		}
	}


	function saveRequestData() {
		let defer = $q.defer();

		if (vm.Invoice) {
			vm.request.request_all_data.invoice = angular.copy(vm.invoice);
		}

		if (!angular.equals(vm.request.request_all_data, copyRequestAllData)) {
			copyRequestAllData = angular.copy(vm.request.request_all_data);

			RequestServices.saveRequestDataContract(vm.request)
				.then(() => defer.resolve())
				.catch(() => defer.reject());
		}

		return defer.promise;
	}


	function setUpInvoiceVariable() {
		InitRequestFactory.initInvoiceRequest(vm.request, vm.invoice);

		vm.request.request_all_data.grand_total = grandTotalService.getGrandTotal('invoice', vm.invoice);
		vm.request.request_all_data.grandTotalRequest.invoice = angular.copy(vm.request.request_all_data.grand_total);
	}

	function setLdTooltip() {
		vm.ldTooltip = vm.longdistanceSettings.avarageDeliveryDays;
	}

	function setTooltipData() {
		vm.tooltipData = vm.basicSettings.tooltipsSettings;
		angular.forEach(vm.tooltipData, function (item) {
			if (item.isDisplay) {
				// remove all html tags for check content length
				let tooltipValue = item.value.replace(/<\/?[^>]+(>|$)/g, '');
				// check if content has image
				let hasImage = item.value.match(/(<img[^>]+>)/g) ? true : false;
				if (!tooltipValue.length && !hasImage) {
					item.isDisplay = false;
				}
			}
		});
	}

	function setCurrentManagerData() {
		vm.currentManagerData = {
			photo: getCurrentManagerPhoto(),
			title: getCurrentManagerTitle(),
			text: getCurrentManagerText(),
			id: getCurrentManagerId(),
			isShowManagerInfo: isShowManagerInfo()
		};
	}

	function getCurrentManagerId() {
		var id = 0;

		if (vm.request.current_manager && vm.request.current_manager) {
			id = vm.request.current_manager.uid;
		}

		return id;
	}

	function getCurrentManagerPhoto() {
		var picsDir = 'sites/default/files/pictures/';
		var result = 'app/account/shared/directives/user_pic.jpg';

		if (vm.request.current_manager && vm.request.current_manager.user_picture) {
			result = config.serverUrl + picsDir + vm.request.current_manager.user_picture.filename;
		}

		return result;
	}

	function getCurrentManagerTitle() {
		var title = 'Your Moving Specialist';

		if (isSetManagerSetting()
			&& vm.request.current_manager.settings.accountDetails.title) {
			title = vm.request.current_manager.settings.accountDetails.title;
		}

		return title;
	}

	function getCurrentManagerText() {
		var text = '';

		if (isSetManagerSetting()
			&& vm.request.current_manager.settings.accountDetails.additionalInformation
			&& isNotOnlyTags(vm.request.current_manager.settings.accountDetails.additionalInformation)) {
			text = vm.request.current_manager.settings.accountDetails.additionalInformation;
		}

		return text;
	}

	function isNotOnlyTags(str) {
		return !!str.replace(/<[^>]+>/g, '');
	}

	function isShowManagerInfo() {
		var result = isSetManagerSetting()
			&& vm.request.current_manager.settings.accountDetails.isShowSpecialistOnAccount;

		return result;
	}

	function isSetManagerSetting() {
		return _.get(vm, 'request.current_manager.settings.accountDetails');
	}

	function createReview() {
		$uibModal.open({
			templateUrl: './app/requestpage/templates/review.tpl.html',
			controller: 'ReviewModalCtrl',
			backdrop: false,
			size: 'lg',
			resolve: {
				reviewSettings: () =>vm.reviewSettings,
				request: () => vm.request,
			}
		});
	}

	function createAutoReview(entityType) {
		var mark = $route.current.params.star;
		var review = {
			entity_type: entityType,
			entity_id: vm.request.nid,
			count_stars: mark,
			text_review: 'Review from email',
			user_id: vm.request.uid.uid
		};

		if (mark >= vm.reviewSettings.current_stars_count) {
			$uibModal.open({
				templateUrl: './app/requestpage/templates/review-success.tpl.html',
				controller: 'successReviewCtrl',
				backdrop: false,
				size: 'lg',
				resolve: {
					reviewSettings: () => vm.reviewSettings
				}
			});
			RequestPageServices.createReview(review)
				.then(() => {
					toastr.success('Review was sent');
				})
				.catch(error => {
					Raven.captureException(`Review was not created: ${error}`);
				});
		} else if (mark <= vm.reviewSettings.current_negative_stars_count) {
			$uibModal.open({
				templateUrl: './app/requestpage/templates/review-fail.tpl.html',
				controller: 'failReviewCtrl',
				backdrop: false,
				size: 'lg',
				resolve: {
					reviewSettings: () => vm.reviewSettings,
					emailMark: () => $route.current.params.star,
					review: () => review
				}
			});
		}
	}

	function tryGetReview() {
		let entityType;
		let ldRequest = vm.request.service_type.raw == 7;
		let storageRequest = vm.request.service_type.raw == 2 || vm.request.service_type.raw == 6;

		if (ldRequest) {
			entityType = ENTITY_TYPE.LDREQUEST;
		} else if (storageRequest) {
			entityType = ENTITY_TYPE.STORAGEREQUEST;
		} else {
			entityType = ENTITY_TYPE.MOVEREQUEST;
		}

		RequestPageServices.getCurrentReview({
			entity_type: entityType,
			entity_id: vm.request.nid
		})
			.then(function (resolve) {
				if (!_.isArray(resolve)) {
					toastr.info('You have already left review');
				} else {
					createAutoReview(entityType);
				}
			});
	}

	function loadingDataIfAdmin() {
		let total_cf = fetchWeight();
		let truckCount = CalculatorServices.getTrucks(total_cf);

		if (truckCount != vm.request.truckCount && vm.useCalculator) {
			vm.request.truckCount = truckCount;
		}
	}

	function fetchWeight() {
		//this check for commercial move already exist in CalculatorServices.getTotalWeight. It must to be refactored
		// later...
		let weight;

		if (vm.request.move_size.raw == 11 && vm.request.field_useweighttype.value == 1) {
			weight = CommercialCalc.getCommercialCubicFeet(
				vm.request.commercial_extra_rooms.value[0],
				vm.calcSettings,
				vm.request.field_custom_commercial_item);
		} else {
			weight = CalculatorServices.getTotalWeight(vm.request);
		}

		return weight;
	}

	function getInHomeEstimateText(homeEstimateStatus = 1, time) {
		switch (Number(homeEstimateStatus)) {
		case 2:
			vm.statusText = 'Your In-home Estimate has been done.';
			break;
		case 3:
			vm.statusText = 'Your In-home Estimate was canceled. Your work will soon be managed by our manager.';
			break;
		case 1:
		default:
			vm.statusText = `Your In-home Estimate is scheduled for ${vm.homeEstimateDate} with arrival time between ${time}. Your In-home Estimator is ${vm.request.home_estimator.first_name} ${vm.request.home_estimator.last_name}.`;
			break;
		}
	}

	$scope.$on('changeStatusToPendingInfo', () => {
		checkStatusToPendingInfo();
	});

	function checkStatusToPendingInfo(isInventory = false) {
		let allowToPendingInfo = !!+vm.request.field_allow_to_pending_info;
		let isNotConfirmed = vm.request.status.raw == 2;
		if (!allowToPendingInfo || !isNotConfirmed) return;

		changeStatusToPendingInfo(isInventory);
	}

	function changeStatusToPendingInfo(isInventory) {
		RequestServices.updateRequest(vm.request.nid, {field_approve: 17})
			.then(() => {
				vm.request.status['raw'] = '17';
				vm.request.status['value'] = 'Pending Info';
				vm.statusTemplate = vm.request_statuses[17].template;
				vm.statusText = vm.request_statuses[17].text;

				let logs = [
					{
						text: 'Status was changed.',
						from: 'Not confirmed',
						to: 'Pending info'
					}
				];

				RequestServices.sendLogs(logs, 'Request page', $scope.request.nid, 'MOVEREQUEST')

				if (isInventory) {
					saveInventoryLog();
					_saveInventory();
				}
			}, () => {
				if (isInventory) {
					saveInventoryPromise.resolve();
				}
			})
			.catch(() => {
				Raven.captureException(`Status was not changed to "Pending-info" in request #${vm.request.nid}`);
			});
	}

	function tryToRegisterCustomerOnline() {
		if (vm.admin) return;
		$scope.addOnline(vm.request.uid.uid, vm.request.nid, vm.request);
	}

	$scope.onMuster(tryToRegisterCustomerOnline);

	function getGrandTotal(type, request) {
		let grandTotal = grandTotalService.getGrandTotal(type, request);
		vm.request.request_all_data.grandTotalRequest[type] = grandTotal;

		return grandTotal;
	}

	function openExtraSignaturePage() {
		erExtraSignaturePage.openPage(vm.request);
	}

	function showReservationSignature() {
		if (vm.request.request_all_data.reservation_receipt) {
			RequestPageServices.getCardImage(vm.request.request_all_data.reservation_receipt).then(function (data) {
				$scope.reservationSignature = data;
				setRequestReservationSignatureForGeneralCustomBlock();
			});
		} else if (!vm.request.request_all_data.reservation_receipt) {
			if (vm.request.request_all_data.reservation_sign) {
				$scope.reservationSignature.push(vm.request.request_all_data.reservation_sign);
				setRequestReservationSignatureForGeneralCustomBlock();
			} else {
				angular.forEach(vm.request.receipts, function (receipt, id) {
					if (receipt.payment_flag == 'Reservation' || receipt.payment_flag == 'Reservation by client' || receipt.type == 'reservation') {
						vm.reservReceipt = receipt;
						vm.request.request_all_data.reservation_receipt = receipt.id;
						RequestServices.saveRequestDataContract(vm.request);
						RequestPageServices.getCardImage(vm.request.request_all_data.reservation_receipt)
							.then(function (data) {
								$scope.reservationSignature = data;
								setRequestReservationSignatureForGeneralCustomBlock();
							});

						return false;
					}
				});
			}
		}
	}

	function setRequestReservationSignatureForGeneralCustomBlock() {
		let date = _.get(vm.request, 'request_all_data.confirmationData.date', '');

		if (date) {
			date = moment(date, 'LLL').format('x');
		}

		vm.request.generalCustomBlockReservationSignature = {
			value: $scope.reservationSignature[0] || '',
			date,
		};
	}
}

moment.createFromInputFallback = function (config) {
	config._d = new Date(config._i);
};
