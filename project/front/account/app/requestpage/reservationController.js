import './templates/reservation_page/LDandFlat_TotalDetails.styl';
import './print-reservation-page.styl';

const POUND_PER_CUBIC_FEET = 7;

'use strict';

angular
	.module('app.request')

	.controller('ReservationController', ReservationController);

/*@ngInject*/
function ReservationController($scope, $rootScope, $routeParams, RequestServices, RequestPageServices, common,
	Session, config, InventoriesServices, AuthenticationService, datacontext, $location, SweetAlert, CalculatorServices,
	PaymentServices, $uibModal, $sce, $q, sharedRequestServices, InitRequestFactory, CheckRequestService, apiService,
	moveBoardApi, CommercialCalc, $window, serviceTypeService, editRequestCalculatorService, valuationService,
	SettingServices, additionalServicesFactory, policyModalServices, $controller, erExtraSignaturePage) {

	$controller('SharedCustomerOnline', {$scope});

	// Inititate the promise tracker to track form submissions.
	var vm = this;
	var nid = $routeParams.id;
	$scope.config = config;
	vm.rootBusy = true;
	vm.admin = false;
	vm.printMode = false;
	vm.packings = [];
	vm.valuationCharge = 0;
	vm.fuelSurcharge = 0;
	vm.decreaseItemsLog = [];
	vm.increaseItemsLog = [];
	vm.removedItemsLog = [];
	vm.removedItems = [];
	vm.decreaseItems = [];
	vm.generalCustomArr = [];
	vm.customBlocks = [];
	vm.isHomeEstimator = false;
	vm.isHideConfirmPrintPage = false;
	vm.openCardPhoto = openCardPhoto;
	$scope.calculateAddServices = calculateAddServices;
	vm.calculatePackingServices = calculatePackingServices;
	vm.calculateLocalMoveMinQuoteWithDiscount = calculateLocalMoveMinQuoteWithDiscount;
	vm.calculateLocalMoveMaxQuoteWithDiscount = calculateLocalMoveMaxQuoteWithDiscount;
	vm.getRequestWeight = getRequestWeight;
	vm.switchConfirmationPrintPage = switchConfirmationPrintPage;
	vm.openPolicyModal = policyModalServices.openPolicyModal;
	var session = Session.get();

	vm.fieldData = datacontext.getFieldData();
	vm.longdistanceSettings = angular.fromJson(vm.fieldData.longdistance);
	vm.basicsettings = angular.fromJson(vm.fieldData.basicsettings);
	vm.quoteSettings = angular.fromJson(vm.fieldData.customized_text);
	vm.settingsQuote = _.clone(vm.basicsettings);
	vm.contract_page = angular.fromJson(vm.fieldData.contract_page);
	vm.calcSettings = angular.fromJson(vm.fieldData.calcsettings);
	vm.quoteSettings = angular.fromJson(vm.fieldData.customized_text);
	vm.additionalBloks = [];
	var enums = vm.fieldData.enums;
	let isRegistredOnline = false;

	$scope.reservationSignature = [];

	if (!session && !AuthenticationService.isAuthenticated()) {
		if (AuthenticationService.checkAutoLogin()) {
			$rootScope.$broadcast('autoLogin');
			$window.location.reload();
		}

		return;
	}

	let currentRequest = _.get(session, `userId.nodes[${nid}]`);

	if (angular.isUndefined(currentRequest)) {
		let userRole = _.get(session, 'userRole', []);

		if (AuthenticationService.isAuthorized(userRole)) {
			vm.admin = true;

			if (~userRole.indexOf('foreman')) {
				vm.isForeman = true;
			}

			if (~userRole.indexOf('home-estimator')) {
				vm.isHomeEstimator = true;
				vm.admin = false;
			}

			RequestServices.getRequest(nid).then(function (request) {
				vm.busy = false;
				init(request);

			});
		} else {
			$location.path('/pages-404.html');
		}
	} else {
		init(currentRequest);
	}

	vm.trustAsHtml = $sce.trustAsHtml;

	function updateQuote() {
		sharedRequestServices.calculateQuote(vm.request);
	}


	function init(request) {
		InitRequestFactory.initRequestFields(request, vm.calcSettings.travelTime);
		getCurrentServiceTypeName(request);
		vm.request = request;
		tryToRegisterCustomerOnline();
		vm.isCommercial = vm.request.move_size.raw == 11;
		vm.commercialName = vm.request.field_commercial_company_name.value;
		vm.useOldPaymentBlock = _.isUndefined(request.request_all_data.isCustomConfirmationText);

		vm.travelTimeSetting = angular.isDefined(vm.request.request_all_data.travelTime) ? vm.request.request_all_data.travelTime : vm.calcSettings.travelTime;

		vm.currentCredentials = _.get($rootScope, 'currentUser.userId.credentials', {});
		if (vm.request.request_all_data.reservation_receipt) {
			RequestPageServices.getCardImage(vm.request.request_all_data.reservation_receipt).then(function (data) {
				$scope.reservationSignature = data;
				setRequestReservationSignatureForGeneralCustomBlock();
			});
		} else if (!vm.request.request_all_data.reservation_receipt) {
			if (vm.request.request_all_data.reservation_sign) {
				$scope.reservationSignature.push(vm.request.request_all_data.reservation_sign);
				setRequestReservationSignatureForGeneralCustomBlock();
			} else {
				angular.forEach(vm.request.receipts, function (receipt, id) {
					if (receipt.payment_flag == 'Reservation' || receipt.payment_flag == 'Reservation by client' || receipt.type == 'reservation') {
						vm.reservReceipt = receipt;
						vm.request.request_all_data.reservation_receipt = receipt.id;
						RequestServices.saveRequestDataContract(vm.request);
						RequestPageServices.getCardImage(vm.request.request_all_data.reservation_receipt)
							.then(function (data) {
								$scope.reservationSignature = data;
								setRequestReservationSignatureForGeneralCustomBlock();
							});

						return false;
					}
				});
			}
		}

		function setRequestReservationSignatureForGeneralCustomBlock() {
			let date = _.get(vm.request, 'request_all_data.confirmationData.date', '');

			if (date) {
				date = moment(date, 'LLL').format('x');
			}

			vm.request.generalCustomBlockReservationSignature = {
				value: $scope.reservationSignature[0] || '',
				date,
			};
		}

		if (angular.isDefined(vm.request.request_all_data.valuation)) {
			if (vm.request.request_all_data.valuation.selected.valuation_charge) {
				vm.valuationCharge = vm.request.request_all_data.valuation.selected.valuation_charge;
				vm.deductableLevel = vm.request.request_all_data.valuation.deductable_level;
			}
		}

		vm.fuelSurcharge = vm.request.request_all_data.surcharge_fuel;

		if (vm.request.service_type.raw != _.get(vm.request, 'request_all_data.invoice.service_type.raw')) {
			_.set(vm.request, 'request_all_data.invoice.service_type', vm.request.service_type);
		}

		if (vm.request.status.raw == 3 && !_.get(vm.request, 'request_all_data.invoice.request_all_data.valuation.selected')) {
			const POUND_COEFFICIENT = 7;
			let weight = CalculatorServices.getTotalWeight(vm.request.request_all_data.invoice).weight * POUND_COEFFICIENT;
			valuationService.fillEmptyFields(vm.request.request_all_data.invoice.request_all_data.valuation, weight);
		}

		if (vm.request.status.raw == 3 && _.get(vm.request, 'request_all_data.invoice.request_all_data.valuation.selected.valuation_charge')) {
			vm.valuationCharge = vm.request.request_all_data.invoice.request_all_data.valuation.selected.valuation_charge;
			vm.deductableLevel = vm.request.request_all_data.invoice.request_all_data.valuation.selected.deductible_level;
		}

		if (angular.isDefined(vm.request.request_data)) {
			if (angular.isDefined(vm.request.request_data.value)) {
				if (vm.request.request_data.value != '') {
					vm.packings = angular.copy(angular.fromJson(vm.request.request_data.value).packings);
				}
			}
		}


		vm.moveDate = moment(new Date(vm.request.date.value)).format('MMMM DD, YYYY');
		vm.deliveryDate = 'Date is not set';
		if (vm.request.ddate.value.length) {
			vm.deliveryDate = moment(new Date(vm.request.ddate.value)).format('MMMM DD, YYYY');
		}
		vm.deliveryDateSecond = moment(new Date(vm.request.ddate_second.value)).format('MMMM DD, YYYY');
		vm.truckCount = RequestPageServices.countTrucks(vm.request.trucks.value);
		vm.flatRateService = vm.request.service_type.raw == 5;
		vm.request.company_logo_url = vm.basicsettings.company_logo_url;
		vm.ownCancelationPolicy = vm.basicsettings.own_cancelation_policy;
		vm.ownCompanyPolicy = vm.basicsettings.own_company_policy;

		vm.delivery_info_text = vm.request.inventory.move_details.deliveryExplanation || vm.longdistanceSettings.delivery_info_text;
		vm.longdistancePendingInfo = vm.longdistanceSettings.pending_info_text;
		vm.longdistanceNotConfirmedInfo = vm.longdistanceSettings.not_confirmed_info_text;
		vm.move_details = vm.request.inventory.move_details;
		vm.ifDetails = CheckRequestService.isMoveDetailsChanged(vm.move_details, vm.request.service_type.raw);
		vm.detailHasComment = vm.move_details.addcomment.length;
		var services = [1, 2, 3, 4, 6, 8];
		vm.isDoubleDriveTime = vm.calcSettings.doubleDriveTime
			&& vm.request.field_double_travel_time
			&& !_.isNull(vm.request.field_double_travel_time.raw)
			&& services.indexOf(parseInt(vm.request.service_type.raw)) >= 0;

		vm.listInventories = vm.request.inventory.inventory_list;

		vm.isEmptyFieldAdditionalUser = !!(_.get(vm.request, 'field_additional_user.mail', false) || _.get(vm.request, 'field_additional_user.phone', false));
		vm.fieldAdditionalUser = _.get(vm.request, 'field_additional_user', {});

		if (angular.isUndefined(vm.listInventories)) {
			vm.listInventories = null;
		}

		$scope.inventoryTotals = calculateTotals();

		vm.request_url = config.host + 'request/' + vm.request.nid;
		$scope.cancelation_policy = '#/request/' + vm.request.nid + '/cancelation_policy';
		vm.rooms = InventoriesServices.getRooms(vm.request);
		vm.request.test = vm.basicsettings.sandboxPayment;
		if ($location.path().search('reservation') > -1) {
			let data = {
				node: {nid: vm.request.nid},
				notification: {
					text: 'User has viewed the confirmation page'
				}
			};
			let dataNotify = {
				type: enums.notification_types.CUSTOMER_VIEW_CONFORMATION_PAGE,
				data: data,
				entity_id: vm.request.nid,
				entity_type: enums.entities.MOVEREQUEST
			};
			apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
			reservationLogs('User has viewed the confirmation page');
		}

		vm.localdiscount = parseInt(vm.request.request_all_data.localdiscount);

		// show only sales tab extra service
		vm.extraServices = angular.fromJson(vm.request.extraServices);

		vm.extraPickup = false;
		vm.extraDropoff = false;

		if (vm.request.field_extra_pickup.postal_code != null) {
			if (vm.request.field_extra_pickup.postal_code.length) {
				vm.extraPickup = true;
			}
		}

		if (vm.request.field_extra_dropoff.postal_code != null) {
			if (vm.request.field_extra_dropoff.postal_code.length) {
				vm.extraDropoff = true;
			}
		}

		vm.deposit = 0;
		angular.forEach(vm.request.receipts, function (receipt, id) {
			if ((receipt.payment_flag == 'Reservation' || receipt.payment_flag == 'Reservation by client' || receipt.type == 'reservation')
				&& !receipt.pending) {

				vm.deposit += parseFloat(receipt.amount);
				vm.reservReceipt = receipt;
			}
		});
		vm.packing = {};
		vm.packing.account = false;
		vm.packing.labor = false;
		if (angular.isUndefined(vm.basicsettings.packing_settings)) {
			var packing_settings = {
				packingAccount: false
			};
			vm.basicsettings.packing_settings = packing_settings;
			var setting = vm.basicsettings;
			var setting_name = 'basicsettings';
			SettingServices.saveSettings(setting, setting_name);
		} else {
			if (angular.isDefined(vm.basicsettings.packing_settings.packingAccount)) {
				vm.packing.account = vm.basicsettings.packing_settings.packingAccount;
			}
			if (angular.isDefined(vm.basicsettings.packing_settings.packingLabor)) {
				vm.packing.labor = vm.basicsettings.packing_settings.packingLabor;
			}
		}

		vm.min_hours = parseFloat(vm.calcSettings.min_hours);
		angular.forEach(vm.packings, function (value, key) {
			if (vm.request.service_type.raw == '5' || vm.request.service_type.raw == '7') {
				if (angular.isUndefined(value.laborRate)) {
					value.laborRate = 0;
				}
				if (vm.packing.labor) {
					vm.packings[key].total = (Number(value.quantity) * (Number(value.laborRate) + Number(value.LDRate))).toFixed(2);
				} else {
					vm.packings[key].total = (Number(value.quantity) * Number(value.LDRate)).toFixed(2);
				}
			} else {
				vm.packings[key].total = (Number(value.quantity) * Number(value.rate)).toFixed(2);
			}

		});

		vm.request.inventory_weight = InventoriesServices.calculateTotals(vm.listInventories);
		vm.request.total_weight = CalculatorServices.getRequestCubicFeet(vm.request);

		if (vm.request.service_type.raw == 7) {
			vm.longDistanceRate = CalculatorServices.getLongDistanceRate(vm.request);
			vm.longDistanceQuote = CalculatorServices.getLongDistanceQuote(vm.request);
			vm.longDistanceTotal = vm.longDistanceQuote;
			vm.longDistancePackingTotal = calculatePackingServices();
			vm.longDistanceServicesTotal = calculateAddServices();

			vm.longDistanceTotal += vm.longDistancePackingTotal + vm.longDistanceServicesTotal + vm.valuationCharge + vm.fuelSurcharge;

			vm.longDistanceDiscount = vm.request.request_all_data.add_money_discount + vm.longDistanceTotal * vm.request.request_all_data.add_percent_discount / 100;

			vm.longDistanceTotal -= vm.longDistanceDiscount;

			//Delivery Window
			var linfo = CalculatorServices.getLongDistanceInfo(vm.request);
			vm.ld_day = linfo.days;
			vm.average_ld_day = linfo.average_days;
			//delivery days
			if (angular.isDefined(vm.request.inventory.move_details)) {
				if (vm.request.inventory.move_details != null) {
					if (angular.isUndefined(vm.request.inventory.move_details.delivery_days)) {
						vm.request.inventory.move_details.delivery_days = vm.ld_day;
					}
				}
			}

			if (angular.isDefined(vm.request.inventory.move_details) && vm.request.inventory.move_details != null) {
				if (angular.isDefined(vm.request.inventory.move_details.delivery)) {
					if (vm.request.inventory.move_details.delivery.length) {
						vm.lastDeliveryDay = CalculatorServices.getLongDistanceWindow(vm.request, vm.request.inventory.move_details.delivery, vm.request.inventory.move_details.delivery_days);
						vm.lastDeliveryDay = moment(new Date(vm.lastDeliveryDay)).format('MMMM DD, YYYY');
					} else {
						vm.request.inventory.move_details.delivery = 'Don\'t know';
						vm.lastDeliveryDay = 0;
					}
				}
			} else {
				vm.request.inventory.move_details = [];
				vm.request.inventory.move_details.delivery = 'Don\'t know';
				vm.lastDeliveryDay = 0;
			}
		}

		if (vm.localdiscount != 0 || vm.request.request_all_data.add_percent_discount ||
			vm.request.request_all_data.add_money_discount || vm.request.request_all_data.add_rate_discount) {
			//LocalDiscount Rate


			vm.DiscountApply = true;
			vm.discountAmount = vm.localdiscount;

			vm.uprate = vm.request.rate.value / (1 - vm.discountAmount / 100);

			if (vm.request.request_all_data.add_rate_discount) {
				vm.request.rate.value = vm.request.request_all_data.add_rate_discount;
				vm.discountAmount = (1 - vm.request.request_all_data.add_rate_discount / vm.uprate) * 100;
			}

			if (vm.request.request_all_data.add_money_discount) {
				var total = calculateTotalBalance('min') + calculatePackingServices() + calculateAddServices() + vm.valuationCharge + vm.fuelSurcharge;
				vm.discountAmount = vm.request.request_all_data.add_money_discount / total * 100;
			}

			vm.perecent_discount_min = 0;
			vm.perecent_discount_max = 0;
			if (vm.request.request_all_data.add_percent_discount) {
				var total_min = calculateTotalBalance('min') + calculatePackingServices() + calculateAddServices() + vm.valuationCharge + vm.fuelSurcharge;
				var total_max = calculateTotalBalance('max') + calculatePackingServices() + calculateAddServices() + vm.valuationCharge + vm.fuelSurcharge;
				vm.perecent_discount_min = total_min * vm.request.request_all_data.add_percent_discount / 100;
				vm.perecent_discount_max = total_max * vm.request.request_all_data.add_percent_discount / 100;

			}
		}

		vm.flatOptionText = '';

		if (angular.isDefined(vm.request.inventory.move_details) && vm.request.inventory.move_details != null) {
			if (vm.flatRateService && angular.isDefined(vm.request.inventory.move_details.options)) {
				if (vm.request.inventory.move_details.options.length) {
					angular.forEach(vm.request.inventory.move_details.options, function (option, id) {
						var delivery = moment(new Date(option.delivery)).format('MM/DD/YYYY');
						var pickup = moment(new Date(option.pickup)).format('MM/DD/YYYY');

						if (delivery == vm.request.ddate.value && pickup == vm.request.date.value) {
							vm.flatOptionText = option.text;
						}
					});
				}
			}
		}

		updateQuote();

		editRequestCalculatorService
			.increaseReservationPrice(vm.request)
			.then(() => RequestServices.saveRequestDataContract(vm.request));

		//CANCELATION
		var year = parseInt(moment(new Date(vm.moveDate)).format('YYYY'));
		var date = moment(new Date(vm.moveDate)).format('YYYY-MM-DD');

		var cc = vm.fieldData.calendar[year][date];
		var type = vm.fieldData.calendartype[cc];
		$scope.flexible = false;
		$scope.moderate = false;
		$scope.strict = false;
		$scope.superstrict = false;

		if (type == 'Discount') {
			$scope.flexible = true;
		}

		if (type == 'Regular') {
			$scope.moderate = true;
		}

		if (type == 'SubPeak' || type == 'Peak' || type == 'HighPeak') {
			$scope.strict = true;
		}

		if (vm.request.service_type.raw == 7) {
			// IF LONG DISTANCE
			$scope.superstrict = true;
			$scope.flexible = false;
			$scope.moderate = false;
			$scope.strict = false;
		}

		//END OF CANCELATIOn
		vm.pickupOnly = false;

		if (vm.request.service_type.raw == 3 || vm.request.service_type.raw == 4) {
			vm.pickupOnly = true;
		}

		vm.checkCancel = false;
		vm.checkTerms = false;

		if (!vm.admin) {
			if (_.isUndefined(vm.request.request_all_data.statistic.user_visit_account)) {
				vm.request.request_all_data.statistic.user_visit_account = true;
			}

			if (_.isUndefined(vm.request.request_all_data.statistic.first_visit_confirmation_page)) {
				vm.request.request_all_data.statistic.first_visit_confirmation_page = moment().unix();
			}

			vm.request.request_all_data.statistic.user_visit_confirmation_page = true;
			RequestServices.saveRequestDataContract(vm.request);
		}

		if (vm.request.move_size.raw == 11) {
			getCommercialTitle();
			getCommercialWeight();
		}

		getQuoteExplanation();
		getAdditionalBlock();

		setTooltipData();
		setLdTooltip();
		checkPickup();

		vm.discountLabel = choiceDiscountLabel();
		vm.discountValue = choiceDiscountValue();
		vm.localMoveMinQuoteWithDiscount = calculateLocalMoveMinQuoteWithDiscount();
		vm.localMoveMaxQuoteWithDiscount = calculateLocalMoveMaxQuoteWithDiscount();

		vm.totalEstimated = getRequestWeight(vm.request);
		vm.localMoveRequest = vm.request.service_type.raw == 1 || vm.request.service_type.raw == 3 || vm.request.service_type.raw == 4 || vm.request.service_type.raw == 8;
		vm.rootBusy = false;
	}

	function getCommercialWeight(weightType = vm.request.field_useweighttype.value) {
		let baseWeight = Number(vm.request.total_weight.min),
			typeOfWeight = 'Total',
			additional = '';

		switch (Number(weightType)) {
		case 2:
			baseWeight = vm.request.inventory_weight.cfs;
			typeOfWeight = 'Inventory';
			let addText = CommercialCalc.getCurrentCommercial(
				vm.request.commercial_extra_rooms.value,
				vm.request.field_custom_commercial_item,
				vm.calcSettings.commercialExtra
			);
			if (addText && addText.name && addText.name.length) {
				additional = `(With ${addText.name})`;
			}
			break;
		case 3:
			baseWeight = Number(vm.request.field_custom_commercial_item.cubic_feet);
			typeOfWeight = vm.request.field_custom_commercial_item.name;
			break;
		case 1:
		default:
			let chosenCommercialExtra = _.find(vm.calcSettings.commercialExtra, {id: vm.request.commercial_extra_rooms.value[0]});
			if (!_.isUndefined(chosenCommercialExtra)) {
				baseWeight = Number(chosenCommercialExtra.cubic_feet);
				typeOfWeight = chosenCommercialExtra.name;
			}
			break;
		}

		vm.commercialWeight = {
			explanation: `(${typeOfWeight} ${baseWeight || 0} c.f. / ${baseWeight * 7 || 0} lbs) ${additional}`
		};
	}

	function checkPickup() {
		if (vm.request.inventory.move_details.showPickupRangeInAccount) {
			let pickupDate = vm.request.inventory.move_details.pickup;

			let isDateRange = _.isString(pickupDate) && _.indexOf(pickupDate, ',') !== -1;

			if (isDateRange) {
				let result = pickupDate.split(',');
				let firstPickupDate = Number(result[0]);
				let lastAccountDate = Number(result[1]);

				if (Number(result[0]) > Number(result[1])) {
					firstPickupDate = Number(result[1]);
					lastAccountDate = Number(result[0]);
				}

				vm.moveDate = `${moment(firstPickupDate).format('MMM DD, YYYY')} - ${moment(lastAccountDate).format('MMM DD, YYYY')}`;
			} else {
				vm.moveDate = `${moment(pickupDate).format('MMM DD, YYYY')}`;
			}
		}
	}

	function getCurrentServiceTypeName(request) {
		vm.currentServiceType = serviceTypeService.getCurrentServiceTypeName(request);
	}

	function getCommercialTitle() {
		vm.commercialTitle = vm.request.move_size.value;
		let isCustomName = !_.isUndefined(vm.request.request_all_data.commercialCustomName)
			&& vm.request.request_all_data.commercialCustomName.length;
		if (isCustomName) {
			vm.commercialTitle = vm.request.request_all_data.commercialCustomName;
		}
	}

	function getQuoteExplanation() {
		var service_type = vm.request.service_type.raw;
		let isCommercial = vm.request.move_size.raw == 11;

		if (service_type > 7) {
			service_type--;
		}
		if (isCommercial) {
			vm.quote_note = vm.request.field_quote_explanationn || vm.quoteSettings.quoteExplanation['8'];
		} else {
			vm.quote_note = vm.request.field_quote_explanationn || vm.quoteSettings.quoteExplanation[service_type];
		}
	}

	function getAdditionalBlock() {
		if (angular.isDefined(vm.request.request_all_data.additionalBlock)) {
			angular.forEach(vm.request.request_all_data.additionalBlock, function (item) {
				if (item.confirmationPage && item.isVisible) {
					vm.additionalBloks.push(item);
				}
			});
		}
	}

	function setLdTooltip() {
		vm.ldTooltip = vm.longdistanceSettings.avarageDeliveryDays;
	}

	function setTooltipData() {
		vm.tooltipData = vm.basicsettings.tooltipsSettings;
	}

	function calculateAddServices() {
		return parseFloat(additionalServicesFactory.getExtraServiceTotal(vm.extraServices));
	}

	function calculatePackingServices() {
		var serviceType = vm.request.service_type.raw == '5' || vm.request.service_type.raw == '7';
		var result = PaymentServices.getPackingTotal(vm.request.request_data.value, vm.packing.labor, serviceType) || 0;

		return parseFloat(result);
	}

	function saveReservation(data) {
		data.field_approve = 3;

		vm.busy = true;
		if (!vm.request.request_all_data.toStorage && vm.request.storage_id && (!vm.request.phone || vm.request.phone == '')) {
			SweetAlert.swal('Failed!', 'Please, add your phone number', 'error');
			return false;
		}

		if (data.field_moving_from
			|| data.field_apt_from
			|| data.field_moving_to
			|| data.field_apt_to) {

			var msgToLog = vm.request.name;

			if (data.field_moving_from
				|| data.field_apt_from) {

				msgToLog += ' changed origin address to: ' + getMovingFieldMessageOfRequest(vm.request, 'field_moving_from', 'apt_from');
			}

			if (data.field_moving_to
				|| data.field_apt_to) {

				if (data.field_moving_from
					|| data.field_apt_from) {

					msgToLog += ' And changed';
				} else {
					msgToLog += ' changed';
				}

				msgToLog += ' destination address to: ' + getMovingFieldMessageOfRequest(vm.request, 'field_moving_to', 'apt_to');
			}

			var arr = [];
			var msg = {
				simpleText: msgToLog
			};

			arr.push(msg);

			RequestServices.sendLogs(arr, 'Request was updated', vm.request.nid, 'MOVEREQUEST');
		}

		saveBookedStatistic();

		RequestServices.updateRequest(vm.request.nid, data).then(function () {
			SweetAlert.swal('Success', '', 'success');
			vm.request.status.raw = 3;
			vm.request.status.value = 'Confirmed';

			$rootScope.$broadcast('force_reload_account');

			let data = {
				notification: {
					text: 'Customer complete confirmation process'
				},
				nid: vm.request.nid
			};
			let typeEnum = enums.notification_types.CUSTOMER_APPROVE_DEPOSIT;
			let dataNotify = {
				type: typeEnum,
				data: data,
				entity_id: vm.request.nid,
				entity_type: enums.entities.MOVEREQUEST
			};
			apiService.postData(moveBoardApi.notifications.createNotification, dataNotify).then(function () {
				typeEnum = enums.notification_types.CUSTOMER_BOOK_JOB;
				data.notification.text = 'Request is confirmed';
				dataNotify = {
					type: typeEnum,
					data: data,
					entity_id: vm.request.nid,
					entity_type: enums.entities.MOVEREQUEST
				};
				apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
				vm.busy = false;
				if (vm.isHomeEstimator) {
					returnToInHomeEstimatePortal();
				} else {
					$location.path('/request/' + vm.request.nid);
				}
			});
		});
	}

	function reservationLogs(txt) {
		if (!vm.admin) {
			var details = [];
			var msg = {
				simpleText: txt
			};
			details.push(msg);
			RequestServices.sendLogs(details, 'User activity', nid, 'MOVEREQUEST');
		}
	}

	$scope.confirmReservation = function (data) {
		if (!vm.checkTerms) {
			$scope.highlightTerms = true;
			let el = $('.confirm-company-policies');
			$('html, body').animate({
				scrollTop: el.offset().top
			}, 1000);
			return;
		}
		if (!CheckRequestService.isValidRequestAddress(vm.request, true)) {
			showSweetAlertEnterAddressMessage(!vm.reservReceipt)
				.then(function (editReq) {
					$scope.confirmReservation(editReq);
				});

			return false;
		}

		if (erExtraSignaturePage.isNeedExtraSignature(vm.request)) {
			let editReq = data;

			erExtraSignaturePage.openPage(vm.request)
				.then(() => {
					$scope.confirmReservation(editReq);
				});

			return false;
		}

		if (vm.reservReceipt) {
			let txt = 'Client opens reservation payment modal';
			reservationLogs(txt);
			vm.request.payment_flag = 'Reservation';
			vm.request.openReservSign = true;
			vm.request.reservReceipt = vm.reservReceipt;
			PaymentServices.openAuthPaymentModal(vm.request, 'step5', vm.request.receipts, 0);
			saveBookedStatistic();

			return false;
		}
		if (_.isUndefined(data)) {
			data = {};
		}

		if (!vm.reservReceipt) {
			//open modal window for signature and save to request_all_data.reservation_sign
			let txt = 'Client opens reservation sign modal';
			reservationLogs(txt);
			RequestPageServices.reservationSignatureModal().then(function (sign) {
				vm.request.request_all_data.reservation_sign = sign;
				if (session.userId && session.userId.nodes) {
					if (session.userId.nodes[vm.request.nid]) {
						session.userId.nodes[vm.request.nid].request_all_data = vm.request.request_all_data;
					}
				}
				saveReservation(data);
				let txt = 'Client signed the reservation';
				reservationLogs(txt);
			}, function (err) {
				console.error(err);
				SweetAlert.swal('Oops...', 'Please try again!', 'error');
			});
			return false;
		}

		saveReservation(data);

	};

	function saveBookedStatistic() {
		if (_.isUndefined(vm.request.request_all_data.statistic.user_visit_account)) {
			vm.request.request_all_data.statistic.user_visit_account = true;
		}

		if (_.isUndefined(vm.request.request_all_data.statistic.user_visit_confirmation_page)) {
			vm.request.request_all_data.statistic.user_visit_confirmation_page = true;
		}

		if (_.isUndefined(vm.request.request_all_data.statistic.booked_date)) {
			vm.request.request_all_data.statistic.booked_date = moment().unix();
		}

		vm.request.request_all_data.isCustomConfirmationText = !!vm.contract_page.useCustomConfirmationText;

		vm.request.request_all_data.statistic.booked_by = 'user';

		setSignedCredentials();
		RequestServices.saveRequestDataContract(vm.request);
	}

	/// END OF ACTIVATE FUNCTION
	vm.calculateTotals = calculateTotals;

	function calculateTotals() {
		var totals = {counts: 0, cfs: 0, boxes: 0, custom: 0};

		angular.forEach(vm.listInventories, function (element, index) {
			if (element.fid == 14) {
				totals.boxes += element.count;
			}

			if (element.fid == 23) {
				totals.custom += element.count;
			}
			totals.counts += element.count;
			totals.cfs = totals.cfs + element.cf * element.count;
		});

		$scope.inventoryDone = vm.flatRate && totals.counts > 0;

		return totals;
	}

	function calculateTotalBalance(type) {
		updateQuote();
		var total = 0;
		$scope.total_min = 0;
		$scope.total_max = 0;

		if (angular.isDefined(vm.request)) {
			if (type == 'min') {
				total = vm.request.quote.min;
				$scope.total = vm.request.quote.min;

			}
			else if (type == 'max') {
				$scope.total_max = vm.request.quote.max;
				total = $scope.total_max;
			}
			else if (type == 'longdistance') {
				total = vm.longDistanceQuote + calculateAddServices() - vm.request.request_all_data.add_money_discount;
			}
		}

		return total;
	}

	function setConfirmationText() {
		vm.request.request_all_data.isCustomConfirmationText = !!vm.contract_page.useCustomConfirmationText;
	}

	function setSignedCredentials() {
		vm.request.request_all_data.confirmationData = {
			date: moment().format('LLL'),
			ip: _.get(vm.currentCredentials, 'ip', ''),
			browser: _.get(vm.currentCredentials, 'browser.name', '')
		};
	}

	function sendConfirmationData() {
		setConfirmationText();
		setSignedCredentials();
		RequestServices.saveReqData(vm.request.nid, vm.request.request_all_data);
	}

	$scope.search = false;

	$scope.checkPayment = function () {
		sendConfirmationData();
		activate();
		$scope.search = true;
	};

	$scope.addReservationPayment = function () {
		if (!vm.checkTerms) {
			$scope.highlightTerms = true;
			return;
		}

		$scope.highlightTerms = false;

		sendConfirmationData();

		if (!CheckRequestService.isValidRequestAddress(vm.request, true)) {
			showSweetAlertEnterAddressMessage(false)
				.then(function () {
					$scope.addReservationPayment();
				});

			return false;
		}

		if (erExtraSignaturePage.isNeedExtraSignature(vm.request)) {
			erExtraSignaturePage.openPage(vm.request)
				.then(() => {
					$scope.addReservationPayment();
				});

			return false;
		}

		if (vm.request.status.raw == 2) {
			vm.request.reservation = true;
			vm.request.clientPay = true;
			activate();
			let txt = 'Client opens reservation payment modal';
			reservationLogs(txt);
			PaymentServices.openAuthPaymentModal(vm.request, 'step5', vm.request.receipts, 0);
		}
	};

	function showSweetAlertEnterAddressMessage(withoutUpdate) {
		let defer = $q.defer();

		SweetAlert.swal({
			title: 'Please enter addresses!'
		}, function (isConfirm) {
			RequestPageServices
				.openAddressModal(vm.request, withoutUpdate)
				.then(function (editrequest) {
					defer.resolve(editrequest);
				}, function () {
					defer.reject();
				});
		});

		return defer.promise;
	}

	var searchPayment = false;

	function activate() {
		common.$timeout(function () {
			var nid = vm.request.nid;
			RequestServices.getRequestsByNid(nid).then(function (data) {
				if (vm.request.receipts.length < data.nodes[0].receipts.length) {
					//  SweetAlert.swal("Payment received!", "", "success");
					// vm.busy = true;
					searchPayment = true;
					// $rootScope.$broadcast('request.payment_receipts',data[nid].receipts);
					// $location.path('/request/' + nid);
				} else {
					if (!searchPayment) {
						activate();
					}
				}
			});
		}, 3500);
	}

	$scope.calculateRowTotal = function (service) {
		return additionalServicesFactory.calculateRowTotal(service);
	};

	function openCardPhoto(img) {
		var modalInstance = $uibModal.open({
			templateUrl: './app/contractpage/templates/CardPhoto.html?2',
			controller: CardPhotoModalCtrl,
			resolve: {
				img: function () {
					return img;
				}
			},
		});
		modalInstance.result.then(function ($scope) {
		});
	}

	function CardPhotoModalCtrl($scope, $uibModalInstance, img, common) {
		$scope.img = img;

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}

	vm.calculateLongDistanceGrandTotal = function () {
		var result = vm.longDistanceQuote + calculatePackingServices() + calculateAddServices() + vm.valuationCharge + vm.fuelSurcharge;

		return result;
	};

	vm.isShowTime = function () {
		var travelTime = 0;
		var result = 0;
		if (vm.isDoubleDriveTime) {
			travelTime = vm.request.field_double_travel_time.raw;
		} else {
			if (vm.travelTimeSetting) {
				travelTime = vm.request.travel_time.raw;
			} else {
				travelTime = 0;
			}
		}

		if (vm.min_hours < vm.request.maximum_time.raw + travelTime) {
			return true;
		} else {
			return false;
		}
	};

	function calculateLongDistanceGrandTotalQuoteUp() {
		var result = vm.DiscountApply ? vm.longDistanceQuoteUp : vm.calculateLongDistanceGrandTotal();

		return result;
	}

	vm.calculateLongDistanceGrandTotalDiscount = function () {
		return calculateLongDistanceGrandTotalQuoteUp() - vm.calculateLongDistanceDiscount();
	};

	vm.calculateLongDistanceDiscount = function () {
		var result = Number(vm.calculateLongDistanceDiscountPercent()) / 100 * Number(calculateLongDistanceGrandTotalQuoteUp());

		return result;
	};

	vm.calculateLongDistanceDiscountPercent = function () {
		var percentOfMoneyDiscount = 0;

		if (vm.request.request_all_data.add_money_discount != 0) {
			percentOfMoneyDiscount = vm.request.request_all_data.add_money_discount / calculateLongDistanceGrandTotalQuoteUp() * 100;
		}

		if (vm.localdiscount) {
			percentOfMoneyDiscount += vm.localdiscount;
		}

		return Number(vm.request.request_all_data.add_percent_discount) + Number(percentOfMoneyDiscount);
	};

	vm.calculateFlatRateTotal = function () {
		vm.flat_rate_quote_value = _.clone(vm.request.flat_rate_quote.value);
		if (vm.flat_rate_quote_value == null) {
			vm.flat_rate_quote_value = 0;
		}
		vm.flatRatePackingTotal = calculatePackingServices();
		vm.flatRateServicesTotal = calculateAddServices();

		//result is grand total for FlatRate
		var result = parseFloat(vm.flat_rate_quote_value) + vm.flatRatePackingTotal + vm.flatRateServicesTotal + parseFloat(vm.valuationCharge) + parseFloat(vm.fuelSurcharge);

		if (vm.request.flatRateTotal != result) {
			vm.request.flatRateTotal = result;
		}

		vm.flatRateDiscount = vm.request.request_all_data.add_money_discount + result * vm.request.request_all_data.add_percent_discount / 100;

		return result - vm.flatRateDiscount;
	};

	vm.isEqualLocalMoveMaxMinTime = function () {
		return vm.request.maximum_time.raw == vm.request.minimum_time.raw;
	};

	function calculateLocalMoveBaseQuote() {
		return Number(calculatePackingServices() + calculateAddServices() + vm.valuationCharge + vm.fuelSurcharge);
	}

	vm.calculateLocalMoveMinQuote = function () {
		return Number(vm.request.quote.min);
	};

	vm.calculateLocalMoveMaxQuote = function () {
		return Number(vm.request.quote.max);
	};

	vm.calculateLocalMoveQuoteByMinHours = function () {
		if (vm.request.request_all_data.add_rate_discount) {
			return Number(vm.min_hours) * Number(vm.request.request_all_data.add_rate_discount) + calculateLocalMoveBaseQuote();
		}
		return vm.min_hours * vm.request.rate.value + calculateLocalMoveBaseQuote();
	};

	vm.calculateLocalMoveMinQuoteByUprate = function () {
		return vm.calculateLocalMoveMinJobTime() * Number(vm.uprate) + calculateLocalMoveBaseQuote();
	};

	vm.calculateLocalMoveMaxQuoteByUprate = function () {
		return vm.calculateLocalMoveMaxJobTime() * Number(vm.uprate) + calculateLocalMoveBaseQuote();
	};

	vm.calculateLocalMoveQuoteByMinHoursAndUprate = function () {
		return Number(vm.min_hours) * Number(vm.uprate) + Number(calculateLocalMoveBaseQuote());
	};

	function calculateLocalMoveMinQuoteWithDiscount() {
		return vm.request.quote.min + calculateLocalMoveBaseQuote() - Number(vm.request.request_all_data.add_money_discount || 0) - Number(vm.perecent_discount_min || 0);
	}

	function calculateLocalMoveMaxQuoteWithDiscount() {
		return vm.request.quote.max + calculateLocalMoveBaseQuote() - Number(vm.request.request_all_data.add_money_discount || 0) - Number(vm.perecent_discount_max || 0);
	}

	vm.calculateLocalMoveDiscount = function () {
		var minJobTime = vm.calculateLocalMoveMinJobTime();
		var newMoneyDiscount = 0;

		if (vm.request.request_all_data.add_money_discount == 0 && vm.request.request_all_data.add_percent_discount != 0) {
			newMoneyDiscount = (Number(vm.request.quote.min) + Number(calculateLocalMoveBaseQuote())) * Number(vm.request.request_all_data.add_percent_discount) / 100;
		}

		var result = Number(minJobTime) * Number(vm.uprate) - Number(vm.request.quote.min) + Number(vm.request.request_all_data.add_money_discount) + Number(newMoneyDiscount);

		return result;
	};

	vm.calculateLocalMoveDiscountPercent = function () {
		return Number(vm.discountAmount) + Number(vm.request.request_all_data.add_percent_discount);
	};

	vm.calculateLocalMoveQuoteWithDiscountAndMinHours = function () {
		if (vm.request.request_all_data.add_rate_discount) {
			return vm.min_hours * Number(vm.request.request_all_data.add_rate_discount) + calculateLocalMoveBaseQuote() - Number(vm.request.request_all_data.add_money_discount) - Number(vm.perecent_discount_min || 0);
		}
		return vm.min_hours * vm.request.rate.value + calculateLocalMoveBaseQuote() - Number(vm.request.request_all_data.add_money_discount) - Number(vm.perecent_discount_min || 0);
	};

	vm.calculateLocalMoveDiscountByMinHours = function () {
		return Number(vm.min_hours * vm.uprate) - Number(vm.min_hours * vm.request.rate.value) + Number(vm.request.request_all_data.add_money_discount);
	};

	vm.calculateLocalMoveMinJobTime = function () {
		var travelTime = vm.travelTimeSetting ? Number(vm.request.travel_time.raw) : 0;
		return Number(vm.request.minimum_time.raw) + (vm.isDoubleDriveTime ? vm.request.field_double_travel_time.raw : travelTime);
	};

	vm.calculateLocalMoveMaxJobTime = function () {
		var travelTime = vm.travelTimeSetting ? Number(vm.request.travel_time.raw) : 0;
		return Number(vm.request.maximum_time.raw) + (vm.isDoubleDriveTime ? vm.request.field_double_travel_time.raw : travelTime);
	};

	vm.getMinLaborTime = function () {
		return vm.request.minimum_time.raw;
	};

	vm.getMaxLaborTime = function () {
		return vm.request.maximum_time.raw;
	};

	vm.isEqualMaxMinLaborTime = function () {
		return _.isEqual(vm.request.minimum_time.raw, vm.request.maximum_time.raw);
	};
	$scope.$on('payment.reservation', function (event, data) {
		vm.request.receipts = data.receipts;
		angular.forEach(vm.request.receipts, function (receipt, id) {
			if ((receipt.payment_flag == 'Reservation' || receipt.payment_flag == 'Reservation by client' || receipt.type == 'reservation')
				&& !receipt.pending) {
				vm.deposit += parseFloat(receipt.amount);
				vm.reservReceipt = receipt;
				let data = {
					node: {nid: vm.request.nid},
					notification: {
						text: 'Reservation received'
					}
				};
				let dataNotify = {
					type: enums.notification_types.CUSTOMER_PAY_DEPOSIT,
					data: data,
					entity_id: vm.request.nid,
					entity_type: enums.entities.MOVEREQUEST
				};
				apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
			}
		});
		if (session.userId && session.userId.nodes) {
			if (session.userId.nodes[vm.request.nid]) {
				session.userId.nodes[vm.request.nid].request_all_data = vm.request.request_all_data;
			}
		}
	});

	$scope.$on('addresses_added', function (ev, data) {
		vm.request = data;
	});

	function getMovingFieldMessageOfRequest(request, property, direction_property) {
		var result = '';

		if (isValidMovingField(request[property], 'thoroughfare')) {
			result += request[property].thoroughfare + ' ';
		}

		if (!_.isUndefined(direction_property)
			&& isValidMovingField(request[direction_property], 'value')) {

			result += request[direction_property].value + ' ';
		}

		if (isValidMovingField(request[property], 'locality')) {
			result += request[property].locality + ' ';
		}

		if (isValidMovingField(request[property], 'administrative_area')) {
			result += request[property].administrative_area;
		}

		if (isValidMovingField(request[property], 'postal_code')) {
			result += ', ' + request[property].postal_code + '.';
		}

		return result;
	}

	function isValidMovingField(field, property) {
		return field[property]
			&& !_.isNull(field[property])
			&& !_.isEmpty(field[property]);
	}


	vm.printModeOn = function () {
		vm.printMode = !vm.printMode;
		angular.element('header.navbar').hide();
		vm.printZoom = 1;
	};

	vm.addZoom = function () {
		vm.printZoom += 0.1;
		angular.element('.ibox.printSection').css('zoom', vm.printZoom);
	};
	vm.minusZoom = function () {
		vm.printZoom -= 0.1;
		angular.element('.ibox.printSection').css('zoom', vm.printZoom);
	};

	function choiceDiscountLabel() {
		if (vm.localdiscount != 0) {
			return 'Additional Discount';
		} else {
			return 'Discount';
		}
	}

	function choiceDiscountValue() {
		var result = '';
		if (vm.localdiscount != 0) {
			result = '-';
		}
		if (vm.request.request_all_data.add_percent_discount == 0 && vm.request.request_all_data.add_money_discount != 0) {
			result += `$${vm.request.request_all_data.add_money_discount}`;
		}
		if (vm.request.request_all_data.add_money_discount == 0 && vm.request.request_all_data.add_percent_discount != 0) {
			result += `${vm.request.request_all_data.add_percent_discount}%`;
		}
		return result;
	}

	function conversionCubicFootToPounds(cf) {
		return cf * POUND_PER_CUBIC_FEET;
	}

	function getRequestWeight(request) {
		let result;
		if (request.move_size.raw != 11 || vm.request.field_useweighttype.value == 2) {
			result = CalculatorServices.getTotalWeight(request);
		} else {
			result = CommercialCalc.getCommercialCubicFeet(
				vm.request.commercial_extra_rooms.value[0],
				vm.calcSettings,
				vm.request.field_custom_commercial_item
			);
		}
		result.lbs = conversionCubicFootToPounds(result.weight);

		return result;
	}

	function returnToInHomeEstimatePortal() {
		let url = `/moveBoard/#/inhome-estimator/request/${vm.request.nid}`;
		$window.open(url, '_self');
	}

	function switchConfirmationPrintPage(trigger) {
		vm.isHideConfirmPrintPage = trigger;
	}

	$scope.$on('redirect-to-in-home-estimate-request', returnToInHomeEstimatePortal);

	function tryToRegisterCustomerOnline() {
		if (vm.admin) return;
		$scope.addOnline(vm.request.uid.uid, vm.request.nid, vm.request);
	}
	$scope.onMuster(tryToRegisterCustomerOnline);
}
