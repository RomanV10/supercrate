(function () {
    'use strict';

    angular
        .module('app.request')

        .controller('StorageTenantController', function ($scope, $rootScope, $routeParams, logger, RequestServices, RequestPageServices, common, Session, config, datacontext, $location, SweetAlert, CalculatorServices, PaymentServices, $uibModal, StorageService) {

            var vm = this;
            var id = $routeParams.id;

            $scope.config = config;
            vm.admin = false;
            vm.storageTenant = {};
            vm.fieldData = datacontext.getFieldData();
            vm.basicSettings = angular.fromJson(vm.fieldData.basicsettings);
            vm.company_logo_url = vm.basicSettings.company_logo_url;
            vm.busy = true;
            vm.storage_notavalable = false;
            vm.entitytype = vm.fieldData.enums.entities.STORAGEREQUEST;
            vm.storageList = [];
            vm.storageForRequest = {};

            StorageService.getStorageReqByID(id).then(function({data}){
                vm.storageTenant = data;
                    vm.busy = false;
                    vm.storage_notavalable = true;
            });

            vm.dateFormatter = dateFormatter;
            vm.storagePayment = storagePayment;
            vm.storageTenantTotalBalance = storageTenantTotalBalance;


            function storagePayment() {
                var data = angular.copy(vm.storageTenant);
                data.total = Math.abs(storageTenantTotalBalance());
                data.nid = data.id;
                data.reservation = false;
                data.clientPay = true;
                data.payForStorage = true;
                PaymentServices.openAuthPaymentModal(data, undefined, undefined, vm.entitytype);
            }

            function storageTenantTotalBalance(){
                var totalInvoice = 0.00, totalPayment = 0.00;

                _.forEach(vm.storageTenant.invoices, function(invoice){
                    _.forEach(invoice.data.charges, function(charge){
                        totalInvoice += (parseFloat(charge.qty >= 0 ? charge.qty : charge.quantity) * parseFloat(charge.cost >= 0 ? charge.cost : charge.unit_cost));
                    });
                });

                _.forEach(vm.storageTenant.payments, function(payment){
                    totalPayment += parseFloat(payment.amount);
                });

                return Number(totalPayment.toFixed(2)) - Number(totalInvoice.toFixed(2));
            }

            function dateFormatter(date){
                return moment(moment.utc(moment.unix(date)).format("DD-MM-YYYY"), "DD-MM-YYYY").format("MMM DD, YYYY");
            }

            $scope.$on('receipt_sent', function(ev, reciept){
                vm.storageTenant.payments.push(reciept);
            })

        })
})();
