(function () {
    'use strict';

    angular
        .module('app.history')
        .run(routeConfig);

    routeConfig.$inject = ['routehelper'];
    /* @ngInject */
    function routeConfig(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/history',
                config: {
                    templateUrl: 'app/history/history.html',
                    title: 'history',
                }
            }
        ];
    }
})();