'use strict';

angular
	.module('app.history')
	.controller('History', History);

History.$inject = ['$location', 'Session', 'serviceTypeService'];

function History($location, Session, serviceTypeService) {
	var vm = this;
	vm.viewRequest = viewRequest;
	vm.getCurrentServiceTypeName = serviceTypeService.getCurrentServiceTypeName;

	function initHistory() {
		var session = Session.get();
		vm.user = [];

		if (session) {
			vm.user = session.userId;
			vm.uid = session.userId.uid;
			vm.requests = session.userId.nodes;
			vm.current_requests = [];
			vm.past_requests = [];

			angular.forEach(vm.requests, function (request, nid) {
				var movedate = moment(request.date.value);

				if (moment().diff(movedate, 'day') > 0) {
					vm.past_requests.push(request);
				} else {
					vm.current_requests.push(request);
				}
			});
		}
	}

	function viewRequest(nid) {
		$location.path('/request/' + nid);
	}

	initHistory();

}
