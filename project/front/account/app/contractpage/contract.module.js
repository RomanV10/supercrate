/**
 * Master module
 * @type {module}
 */
angular.module('contractModule', ['ui.bootstrap', 'app.data','app.core', 'angular-flippy', 'colorpicker.module'])
    .run(ContractRun);

    function ContractRun($rootScope, $location){

        //check user permissions @TODO:add in router
        if (!!$rootScope.currentUser) {
            if ($rootScope.currentUser.roles.indexOf('administrator') == -1 &&
                $rootScope.currentUser.roles.indexOf('foreman') == -1) {
                $location.path('/');
            }
        }

    }
