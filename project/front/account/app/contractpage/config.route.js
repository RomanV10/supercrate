(function () {
    'use strict';

    angular.module('app.request')
        .run(routeConfig);

    routeConfig.$inject = ['routehelper'];
    /* @ngInject */
    function routeConfig(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/request/:id/contract',
                config: {
                    templateUrl: 'app/contractpage/templates/contract.html?4',
                    title: 'contractpage'
                }
            }
        ];
    }
})();