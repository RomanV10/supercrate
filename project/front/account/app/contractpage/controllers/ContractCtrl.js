'use strict';

angular.module('contractModule')
	.controller('ContractCtrl', ContractCtrl);

/*@ngInject*/
function ContractCtrl($scope, $uibModal, SweetAlert, $location, $rootScope, $routeParams, AuthenticationService,
					  ContractFactory, $window, $q, Session, RequestServices, datacontext, ContractService,
					  ContractCalc, logger, PaymentServices, common, $geolocation, sharedRequestServices, $timeout,
					  CalculatorServices, InventoriesServices, StorageService, apiService, moveBoardApi, ContractValue,
					  arrivyService, valuationService, InitRequestFactory, accountConstant, additionalServicesFactory, emailService,
					  contractDispatchLogs, logsViewTextIds, Raven) {

	const SIGNATURE_PREFIX = 'data:image/png;base64,';
	const tmpPdfId = -9999;
	let maxSignCount = 7;
	let session = Session.get();
	let importedFlags = $rootScope.availableFlags;
	let nid = Number($routeParams.id);
	let unbindEv = [];
	let workers = null;
	let fieldData = datacontext.getFieldData();
	let calcSettings = angular.fromJson(fieldData.calcsettings);
	let basicSettings = angular.fromJson(fieldData.basicsettings);
	let contract_page = angular.fromJson(fieldData.contract_page);
	let enums = fieldData.enums;
	let contractTypes = fieldData.enums.contract_type;

	$scope.contractPage = contract_page;
	const HUNDRED_PERCENT = ContractValue.hundredPercent, MOVEREQUEST = fieldData.enums.entities.MOVEREQUEST,
		PICK_UP_PERCENT = contract_page.pickUpPercent, MIN_PAYROLL_HOURS = contract_page.minPayrollHours,
		MIN_HOUR_RATE = parseFloat(calcSettings.min_hours);
	const mainContractID = 0;
	const mainCotractTitle = 'Main contract page';

	$scope.uid = session.userId.uid;
	$scope.isEmpty = _.isEmpty;
	$scope.showDeclarVal = _.findIndex(contract_page.declarations, {show: true}) > -1;

	var Job_Completed = 'Completed';
	var pickupJobCompleted = 'Pickup Completed';
	var deliveryJobCompleted = 'Delivery Completed';
	var Payroll_Done = 'Payroll Done';
	var pickupPayrollDone = 'Pickup Payroll Done';
	var deliveryPayrollDone = 'Delivery Payroll Done';
	var signatureLabelsLocalMove = ContractValue.signatureLabelsLocalMove;
	var signatureLabelsFlatRate = ContractValue.signatureLabelsFlatRate;

	function getUserRole() {
		return _.get(session, 'userRole', []);
	}

	if (getUserRole().indexOf('foreman') > -1) {
		let data = {
			node: {
				nid: nid
			}, notification: {
				text: 'Foreman has opened the contract.'
			}
		};
		let dataNotify = {
			type: $scope.fieldData.enums.notification_types.FORMENT_VIEW_CONTACT,
			data: data,
			entity_id: nid,
			entity_type: $scope.fieldData.enums.entities.MOVEREQUEST
		};
		apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
	}

	var _setStepError = function (nextStep) {
		if (nextStep == 'checkout_btn') {
			$('#errorSign_' + nextStep).addClass('errorArrow');
			var el = $('.' + nextStep);
			el.addClass('error');
			if (!contract_page.lessInitialContract && $scope.navigation.active == _.indexOf($scope.navigation.pages, 'Bill of lading')) $('html, body').animate({
				scrollTop: el.offset().top
			}, 1000);
			$('#signaturePad').modal('hide');
			return false;
		}
		var el = $('#step_' + nextStep);

		if (el.length) {
			el.addClass('error');
			if ($scope.navigation.active == _.indexOf($scope.navigation.pages, 'Bill of lading')) $('html, body').animate({
				scrollTop: el.offset().top
			}, 1000);
		}

		$('#signaturePad').modal('hide');
		return false;
	};

	$scope.busy = true;
	$scope.request_data = {};
	$scope.payment_received = false;
	$scope.balance_paid = 0;
	$scope.openPay = false;
	$scope.openStorageAndTransit = false;
	$scope.imContract = '';
	$scope.rangeArr = [];
	$scope.navigation = ContractFactory.navigation;
	$scope.services = undefined;
	$scope.data = ContractFactory.factory;
	$scope.frontSide = false;
	$scope.releaseFormSettings = contract_page.releaseForm;
	$scope.declarationValue = contract_page.declarations;
	$scope.terms = contract_page.terms;
	$scope.content = contract_page.textContent;
	$scope.companyName = basicSettings.company_name;
	$scope.packing = {};
	$scope.packing.account = false;
	$scope.packing.labor = false;
	$scope.oldVersionOfReleaseForm = contract_page.releaseFormOldVersion;
	$scope.tabs = contract_page.additionalContract || [];
	$scope.loadAddPages = false;
	$scope.loadCustom = false;
	$scope.mainContract = true;
	$scope.lastPageSubmit = false;
	$scope.initCountAdditionalPage = -1;
	$scope.mainTab = 0;
	$scope.disableLinks = false;
	$scope.isFamiliarization = ContractValue.isFamiliarization;
	$scope.isPrint = ContractValue.isPrint;
	$scope.tabs.unshift(ContractValue.defaultMainTabData);
	$scope.activeTab = ContractValue.defaultActiveTab;
	$scope.isDoubleDriveTime = calcSettings.doubleDriveTime;
	$scope.viewAdditionalContractPages = [];
	var services = ContractValue.sericesArr;

	if (basicSettings.packing_settings) {
		$scope.packingService = angular.copy(ContractValue.packingServiceData);

		if (basicSettings.packing_settings.packingAccount) {
			$scope.packing.account = basicSettings.packing_settings.packingAccount;
		}
		if (basicSettings.packing_settings.packingLabor) {
			$scope.packing.labor = basicSettings.packing_settings.packingLabor;
		}
		if (_.get(basicSettings.packing_settings.packing, 'LM', false)) {
			$scope.packingService.LM = basicSettings.packing_settings.packing.LM;
		}
		if (_.get(basicSettings.packing_settings.packing, 'LD', false)) {
			$scope.packingService.LD = basicSettings.packing_settings.packing.LD;
		}
	}

	$scope.calcData = ContractCalc.finance;
	$scope.calculation = {
		calculateWork: ContractCalc.calculateWork,
		totalHourlyCharge: ContractCalc.totalHourlyCharge,
		getCrewTotalDuration: ContractCalc.getCrewTotalDuration,
		canCalc: ContractCalc.canCalc,
		totalCost: ContractCalc.totalCost,
		totalCostForFlatRate: ContractCalc.totalCostForFlatRate,
		getMonthlyStorageRate: ContractCalc.getMonthlyStorageRate,
		totalClosingFlatRateCost: ContractCalc.totalClosingFlatRateCost
	};

	$scope.stepsData = ContractFactory.stepsData;
	$scope.flatStepsData = ContractFactory.flatStepsData;
	$scope.descreptiveSymbols = ContractFactory.descreptiveSymbols;
	$scope.exceptionSymbols = ContractFactory.exceptionSymbols;
	$scope.locationSymbols = ContractFactory.locationSymbols;
	$scope.saveContractTips = saveContractTips;
	$scope.saveToLocalStorage = saveToLocalStorage;
	$scope.getFromLocalStorage = getFromLocalStorage;
	$scope.saveDataRequest = saveDataRequest;
	$scope.saveDataContract = saveDataContract;
	$scope.sendReceipt = sendReceipt;
	$scope.removeDeclarSelectError = removeDeclarSelectError;
	$scope.flipPage = flipPage;
	$scope.selectTab = selectTab;
	$scope.createPdf = createPdf;
	$scope.getTips = getTips;
	$scope.saveSignature = saveSignature;

	function saveSignature() {
		$scope.$broadcast('signatureAgreement');
	}

	function getTips() {
		let totalTips = 0;
		let isOnlyPickupTips = $scope.requestType && !$scope.data.isPickupSubmitted && !$scope.request.field_flat_rate_local_move.value;
		if (isOnlyPickupTips) {
			totalTips = $scope.calcData.pickupTips;
		} else {
			totalTips = $scope.calcData.tip;
		}
		return totalTips;
	}

	function createPdf(val, type, tmpPdfId) {
		var defer = $q.defer();
		let htmlToPdf = val.replace(/disabled/g, '');
		htmlToPdf = htmlToPdf.replace(/type="number"/g, '');
		htmlToPdf = htmlToPdf.replace(/class="show-me-when-print\sng-binding"\sstyle="display:\snone;"/g, '');
		htmlToPdf = htmlToPdf.replace(/hide-field-discount-before-print/g, 'style="display:none;"');
		htmlToPdf = htmlToPdf.replace(/\$ /g, '$');
		htmlToPdf = htmlToPdf.replace(/<input type="text".*?\svalue="(.*?)".*?>/gi, `<span>$1</span>`);
		htmlToPdf = htmlToPdf.replace(/<div\sclass="icheckbox_flat-grey( checked)?".*?>(.*?)<\/div>/gi, `<span class="icheckbox_flat-grey $1">$2</span>`);

		let pdf = {
			'html': htmlToPdf,
			'entity_id': $scope.request.nid,
			'entity_type': $scope.fieldData.enums.entities.MOVEREQUEST,
			'page_id': tmpPdfId ||$scope.tabs[$scope.activeTab].id || mainContractID,
			'allow_attach': type ? +JSON.parse($rootScope.fieldData.contract_page).allow_attach_to_email || 0 : +$scope.tabs[$scope.activeTab].allowAttachPage || 0
		};

		if (_.isNull(pdf.allow_attach)) {
			pdf.allow_attach = 0;
		}

		ContractService.sendDocumentForPDF(pdf)
			.then((data) => defer.resolve(data))
			.catch((err) => {
				SweetAlert.swal('Failed!', 'Error of creating pdf', 'error');
				defer.reject(err);
			});

		return defer.promise;
	}

	function selectTab(index) {
		$scope.activeTab = index;
	}

	(function testPaymentSettings() {
		if (!contract_page.paymentOptions || _.findIndex(contract_page.paymentOptions, {selected: true}) < 0) {

			SweetAlert.swal('Failed!', 'Please contact to your administrator, and set payment options', 'error');
		}
	})();

	function removeDeclarSelectError() {
		var declaration = $('#step_declaration');
		if (declaration.find('select') && declaration.find('select').hasClass('error')) declaration.find('select').removeClass('error');
	}

	function flipPage() {
		$rootScope.$broadcast('FLIP');
		$scope.frontSide = !$scope.frontSide;
	}

	function contractTouring() {
		var ln = $scope.data.signatures.length;
		var nextStep = ln;
		//remove errors
		if ($scope.navigation.active == _.indexOf($scope.navigation.pages, 'Bill of lading')) {
			for (var i = 0; i < $scope.maxSignCount; i++) {
				var arrow = $('#errorSign_' + i);
				var el = $('#step_' + i);
				if (el.hasClass('error')) {
					el.removeClass('error');
				}
				if (arrow.hasClass('errorArrow')) {
					arrow.removeClass('errorArrow');
				}
			}
			var btnPay = $('.checkout_btn');
			if (btnPay.hasClass('error')) {
				btnPay.removeClass('error');
			}
			var btnPayArrow = $('#errorSign_checkout_btn');
			if (btnPayArrow.hasClass('errorArrow')) {
				btnPayArrow.removeClass('errorArrow');
			}
			var declaration = $('#step_declaration');
			if (declaration.hasClass('error')) {
				declaration.removeClass('error');
				if (declaration.find('select') && declaration.find('select').hasClass('error')) declaration.find('select').removeClass('error');
			}
			var declarationArrow = $('#errorSign_declaration');
			if (declarationArrow.hasClass('errorArrow')) {
				declarationArrow.removeClass('errorArrow');
			}
			//set error
			if ($scope.requestType) {
				if ($scope.request.service_type.raw == 5 || $scope.request.service_type.raw == 7) {
					if (ln == 3 && $scope.totalLessDeposit() != 0) {
						saveReqDataInvoice();
						if (contract_page.lessInitialContract && $scope.data.declarationValue.selected.length == 0 && $scope.showDeclarVal) {
							var declaration = $('.contract li select.declar');
							declaration.addClass('error');
							return false;
						}
						_setStepError('checkout_btn');
					} else if (((contract_page.lessInitialContract && ln == 4 && _.isEmpty($scope.data.req_comment) && $scope.data.isPickupSubmitted) || (ln == 5 && $scope.data.isPickupSubmitted)) && $scope.totalLessDeposit() != 0) {
						_setStepError('checkout_btn');
					} else if (ln < $scope.maxSignCount) _setStepError(nextStep);
				}
			} else {
				if (ln == 2) {
					if ($scope.data.declarationValue.selected.length == 0) {
						var declaration = $('.contract li select.declar');
						declaration.addClass('error');
					}
					return _setStepError('declaration');
				}
				if (ln < $scope.maxSignCount - 2) {
					_setStepError(nextStep);
				}
				if (ln == $scope.maxSignCount - 2 && $scope.totalLessDeposit() != 0) {
					_setStepError('checkout_btn');
					saveReqDataInvoice();
				}
				let totalLessDepositValue = $scope.totalLessDeposit();
				if (ln >= $scope.maxSignCount - 2 && totalLessDepositValue == 0) {
					if (contract_page.goToReview && _.isUndefined($scope.data.feedBack.visited) && !$scope.data.feedBack.subbmited) {
						$scope.data.feedBack.visited = true;
						$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Review');
					}
					_setStepError(nextStep);
				}
			}
		}
	}

	ContractService.getExtraServices('additional_settings')
		.then((jsonData) => $scope.additionalServices = angular.fromJson(jsonData.data[0]))
		.catch(() => SweetAlert.swal('Failed!', 'Error of getting extra services', 'error'));

	$scope.calculation.totalCost = function () {
		var total = ContractCalc.totalCost();
		if ($scope.request.service_type == 5 || $scope.request.service_type == 7) total = ContractCalc.totalCostForFlatRate();
		return total;
	};
	$scope.calculation.totalFlatRateCost = function () {
		return ContractCalc.totalFlatRateCost($scope.data.isPickupSubmitted);
	};
	$scope.request = {};
	$scope.uploader = {};
	$scope.images = [];
	$scope.addedImages = [];
	$scope.requestReady = false;
	$scope.flatMiles = false;
	$scope.travelTime = 0;

	var baseRequestAllData = {};

	$scope.saveStep = saveStep;
	$scope.saveContract = saveContract;
	$scope.addCrew = addCrew;
	$scope.applyPayment = applyPayment;
	$scope.showPayment = showPayment;
	$scope.calcPaymentContract = calcPaymentContract;
	$scope.submitContractFlat = submitContractFlat;
	$scope.calculateAddServices = calculateAddServices;
	$scope.removeSignature = removeSignature;
	$scope.totalLessDeposit = totalLessDeposit;
	$scope.saveStorageFit = saveStorageFit;
	$scope.saveReqDataInvoice = saveReqDataInvoice;
	$scope.hideInventoryTab = hideInventoryTab;
	$scope.hideRentalTab = hideRentalTab;
	$scope.hideAddInventoryTab = hideAddInventoryTab;
	$scope.hideSecondAddInventoryTab = hideSecondAddInventoryTab;
	$scope.submitAddContract = submitAddContract;
	$scope.sendLetterPdfAddPage = sendLetterPdfAddPage;
	$scope.sendLetterPdf = sendLetterPdf;
	$scope.downloadPdfPage = downloadPdfPage;
	$scope.downloadPdfAddPage = downloadPdfAddPage;
	$scope.calculateAddContractData = calculateAddContractData;
	$scope.loadInventory = loadInventory;
	$scope.copy = copy;
	$scope.getAddMainPage = getAddMainPage;
	$scope.checkRequestTypeSetUserId = checkRequestTypeSetUserId;

	unbindEv.push($rootScope.$on('resetCalc', function (e, crew) {
		ContractCalc.calculateWork(crew);
		saveContract();
	}));

	function calculateAddContractData(index) {
		let additionalContractPage = $scope.data.additionalContractPages[index];
		if (additionalContractPage.rate_per_hour) {
			$scope.request.request_all_data.invoice.rate.value = additionalContractPage.rate_per_hour;
			$scope.request.request_all_data.invoice.rate.old = additionalContractPage.rate_per_hour;
		}
		if (additionalContractPage.crew_number) {
			$scope.request.request_all_data.invoice.crew.value = additionalContractPage.crew_number;
			$scope.request.request_all_data.invoice.crew.old = additionalContractPage.crew_number;
		}
		if (additionalContractPage.address_from) $scope.request.request_all_data.invoice.field_moving_from.thoroughfare = additionalContractPage.address_from;
		if (additionalContractPage.address_to) $scope.request.request_all_data.invoice.field_moving_to.thoroughfare = additionalContractPage.address_to;
		if (additionalContractPage.address_pickup) $scope.request.request_all_data.invoice.field_extra_pickup.thoroughfare = additionalContractPage.address_pickup;
		if (additionalContractPage.address_dropoff) $scope.request.request_all_data.invoice.field_extra_dropoff.thoroughfare = additionalContractPage.address_dropoff;
		if (additionalContractPage.city_from) $scope.request.request_all_data.invoice.field_moving_from.locality = additionalContractPage.city_from;
		if (additionalContractPage.city_to) $scope.request.request_all_data.invoice.field_moving_to.locality = additionalContractPage.city_to;
		if (additionalContractPage.city_pickup) $scope.request.request_all_data.invoice.field_extra_pickup.locality = additionalContractPage.city_pickup;
		if (additionalContractPage.city_dropoff) $scope.request.request_all_data.invoice.field_extra_dropoff.locality = additionalContractPage.city_dropoff;

		if (additionalContractPage.state_from) $scope.request.request_all_data.invoice.field_moving_from.administrative_area = additionalContractPage.state_from;
		if (additionalContractPage.state_to) $scope.request.request_all_data.invoice.field_moving_to.administrative_area = additionalContractPage.state_to;
		if (additionalContractPage.state_pickup) $scope.request.request_all_data.invoice.field_extra_pickup.administrative_area = additionalContractPage.state_pickup;
		if (additionalContractPage.state_dropoff) $scope.request.request_all_data.invoice.field_extra_dropoff.administrative_area = additionalContractPage.state_dropoff;
		if (additionalContractPage.zip_from) $scope.request.request_all_data.invoice.field_moving_from.postal_code = additionalContractPage.zip_from;
		if (additionalContractPage.zip_to) $scope.request.request_all_data.invoice.field_moving_to.postal_code = additionalContractPage.zip_to;
		if (additionalContractPage.zip_pickup) $scope.request.request_all_data.invoice.field_extra_pickup.postal_code = additionalContractPage.zip_pickup;
		if (additionalContractPage.zip_dropoff) $scope.request.request_all_data.invoice.field_extra_dropoff.postal_code = additionalContractPage.zip_dropoff;

		if (additionalContractPage.labor_time) {
			$scope.request.request_all_data.invoice.work_time = common.decToTime(additionalContractPage.labor_time);
			$scope.request.request_all_data.invoice.work_time_int = additionalContractPage.labor_time;
			$scope.request.request_all_data.invoice.work_time_old = common.decToTime(additionalContractPage.labor_time);
		}
		if (additionalContractPage.start_time) {
			$scope.request.request_all_data.invoice.start_time1.value = additionalContractPage.start_time;
		}
		if (additionalContractPage.end_time) {
			$scope.request.request_all_data.invoice.start_time2.value = additionalContractPage.end_time;
		}
		if (additionalContractPage.travel_fee > 0 && additionalContractPage.rate_per_hour > 0) {
			let travelTime = _.round(additionalContractPage.travel_fee / additionalContractPage.rate_per_hour, accountConstant.roundUp);

			let travelTimeObj = {
				raw: travelTime,
				value: common.decToTime(travelTime),
				old: common.decToTime(travelTime)
			};
			$scope.travelTime = travelTimeObj;
			if ($scope.isDoubleDriveTime) {
				let doubleTravelTime = _.get($scope.request, 'request_all_data.invoice.field_double_travel_time');

				if (angular.isUndefined(doubleTravelTime)) {
					_.set($scope.request, 'request_all_data.invoice.field_double_travel_time', travelTimeObj);
				}

				$scope.request.request_all_data.invoice.field_double_travel_time.raw = travelTimeObj.raw;
				$scope.request.request_all_data.invoice.field_double_travel_time.value = travelTimeObj.value;
				$scope.request.request_all_data.invoice.field_double_travel_time.old = travelTimeObj.value;
			} else {
				if (angular.isUndefined($scope.request.request_all_data.invoice.travel_time)) {
					$scope.request.request_all_data.invoice.travel_time = travelTimeObj;
				}
				$scope.request.request_all_data.invoice.travel_time.raw = travelTimeObj.raw;
				$scope.request.request_all_data.invoice.travel_time.value = travelTimeObj.value;
				$scope.request.request_all_data.invoice.travel_time.old = travelTimeObj.value;
			}
		}
		if (additionalContractPage.valuation) ContractCalc.finance.valuation = additionalContractPage.valuation;
		if (additionalContractPage.fuel_surcharge) ContractCalc.finance.fuel = additionalContractPage.fuel_surcharge;
		if (additionalContractPage.tips) ContractCalc.finance.tip = additionalContractPage.tips;
		if (additionalContractPage.contract_total) ContractCalc.finance.totalCost = additionalContractPage.contract_total;
		ContractCalc.finance.workDuration = $scope.request.request_all_data.invoice.work_time_int;

		let contractInputIndex = 0;
		let contractInputName = 'simple_contract_input_' + contractInputIndex;
		while (angular.isDefined(additionalContractPage[contractInputName])) {
			if (!$scope.request.request_all_data.contractInputs) $scope.request.request_all_data.contractInputs = {};
			$scope.request.request_all_data.contractInputs[contractInputName] = Number(additionalContractPage[contractInputName]);
			contractInputIndex++;
			contractInputName = 'simple_contract_input_' + contractInputIndex;
		}

		if ($scope.request.service_type.raw == 7) {
			$scope.request.request_all_data.invoice.field_long_distance_rate.value = ContractFactory.longDistanceRate;
		}

		if ($scope.request.service_type.raw == 5) {
			$scope.request.request_all_data.invoice.flat_rate_quote.value = +additionalContractPage.flat_rate_quote;
		}

		$scope.request.request_all_data.invoice.request_all_data.add_money_discount = ContractCalc.finance.moneyDiscount;
		$scope.request.request_all_data.invoice.request_all_data.add_percent_discount = ContractCalc.finance.percentDiscount;
		$scope.request.request_all_data.invoice.request_all_data.valuation.selected.valuation_charge = ContractCalc.finance.valuation;
		$scope.request.request_all_data.invoice.request_all_data.surcharge_fuel = ContractCalc.finance.fuel;
		$scope.request.request_all_data.req_tips = ContractCalc.finance.tip;

		$scope.calcData.valuation = ContractCalc.finance.valuation;
		$scope.calcData.fuel = ContractCalc.finance.fuel;
		$scope.calcData.tip = ContractCalc.finance.tip;
		$scope.calcData.totalCost = ContractCalc.finance.totalCost;

		if (!_.isEqual(baseRequestAllData, $scope.request.request_all_data)) {
			baseRequestAllData = angular.copy($scope.request.request_all_data);
			RequestServices.saveRequestDataContract($scope.request);
		}
	}

	function submitAddContract(indexTab) {
		let additionalContractPage = getPage(indexTab);
		let indexPage = _.findIndex($scope.data.additionalContractPages, (page) => _.get(page, 'page_id', -1) == additionalContractPage.page_id);

		if ($scope.tabs[indexTab + 1].mainContract && additionalContractPage.balance_due != 0) {
			SweetAlert.swal('Failed!', 'Balance due isn\'t 0', 'error');
			return false;
		}

		let remainderInputIndex = 0;
		let remainderInputName = 'additional_remainder_email_' + remainderInputIndex;
		while (angular.isDefined(additionalContractPage[remainderInputName])) {
			if (!$scope.request.request_all_data.additionalRemainderEmail) $scope.request.request_all_data.additionalRemainderEmail = [];

			$scope.request.request_all_data.additionalRemainderEmail.push(additionalContractPage[remainderInputName]);
			remainderInputIndex++;
			remainderInputName = 'additional_remainder_email_' + remainderInputIndex;
		}
		if (!$scope.tabs[indexTab + 1].mainContract) {
			saveContractCreatePdf(indexTab, indexPage);
		}
		//save time data if it uses as main contract
		if ($scope.tabs[indexTab + 1].mainContract) {
			$rootScope.$broadcast('recalc.customTax');
			$scope.lastPageSubmit = true;
			calculateAddContractData(indexPage);
			//send payroll
			saveContractCreatePdf(indexTab, indexPage, true);
		}
	}

	function sendLetterPdfAddPage(index) {
		$scope.busy = true;

		return createPdf(angular.element(`#additional-contract-page_${index}`)[0].innerHTML, undefined, tmpPdfId)
			.then(({data}) => sendPdfPage(data))
			.catch((err) => SweetAlert.swal('Failed!', 'Error creating pdf', 'error'))
			.finally(() => $scope.busy = false);
	}

	function sendLetterPdf() {
		$scope.busy = true;

		return createPdf(angular.element('flippy-front')[0].innerHTML, 'mainPage', tmpPdfId)
			.then(({data}) => sendPdfPage(data))
			.catch((err) => SweetAlert.swal('Failed!', 'Error creating pdf', 'error'))
			.finally(() => $scope.busy = false);
	}

	function downloadPdfAddPage(index) {
		$scope.busy = true;

		return createPdf(angular.element(`#additional-contract-page_${index}`)[0].innerHTML, undefined, tmpPdfId)
			.then(({data: [pdfNid]}) => {
				ContractService.getPdfByTab(pdfNid)
					.then(({data: {url}}) => {
						$window.open(url, '_blank');
					})
					.catch((err) => {
						SweetAlert.swal('Failed!', err, 'error');
					});
			})
			.catch((err) => SweetAlert.swal('Failed!', 'Error creating pdf', 'error'))
			.finally(() => $scope.busy = false);
	}

	function downloadPdfPage() {
		$scope.busy = true;

		return createPdf(angular.element('flippy-front')[0].innerHTML, 'mainPage', tmpPdfId)
			.then(({data: [pdfNid]}) => {
				ContractService.getPdfByTab(pdfNid)
					.then(({data: {url}}) => {
						$window.open(url, '_blank');
					})
					.catch((err) => {
						SweetAlert.swal('Failed!', err, 'error');
					});
			})
			.catch((err) => SweetAlert.swal('Failed!', 'Error creating pdf', 'error'))
			.finally(() => $scope.busy = false);
	}

	function sendPdfPage(data) {
		ContractService.getPdfByTab(data[0])
			.then(({data}) => {
				let emails = getEmails();

				if (emails) {
					return emailService.submitLetterPdf($scope.request.nid, emails, data.url);
				} else {
					return Promise.reject(`Email wasn't find`);
				}
			})
			.then(() => SweetAlert.swal('Success!', 'E-mail was successfully sent', 'success'))
			.catch((err) => {
				if (_.isObject(err)) {
					err = 'Problem with submit letter current contract';
				}
				SweetAlert.swal('Failed!', err, 'error');
			});
	}

	function saveContractCreatePdf(indexTab, indexPage, addMain) {
		$scope.data.additionalContractPages[indexPage].isSubmitted = true;
		$scope.busy = true;
		createPdf(document.getElementById('additional-contract-page_' + indexTab).innerHTML)
			.then(({data}) => ContractService.getPdfByTab(data[0]))
			.then(({data}) => {
				$scope.data.additionalContractPages[indexPage].pdfData = data;
				if ($scope.lastPageSubmit) {
					return backsidePdf();
				} else {
					return Promise.resolve();
				}
			})
			.then(() => {
				let params = {
					additionalPageSubmit: true
				};

				if ($scope.request.service_type.raw == 5 && addMain) {
					params.additionalFlatRate = true;
				} else if($scope.request.service_type.raw == 7 && addMain) {
					params.longDistance = true;
				} else if (addMain) {
					params.isBtn = true;
					params.addContract = true;
				}

				return saveContract(params);
			})
			.then(() => {
				if (!_.isEqual(baseRequestAllData, $scope.request.request_all_data)) {
					baseRequestAllData = angular.copy($scope.request.request_all_data);
					return RequestServices.saveRequestDataContract($scope.request);
				} else {
					return Promise.resolve();
				}
			})
			.then(() => {
				let NotFlatRateLongDistance = $scope.request.service_type.raw != 5 && $scope.request.service_type.raw != 7;
				let flatRateMain = $scope.request.service_type.raw == 5 && !addMain;
				let longDistanceMain = $scope.request.service_type.raw == 7 && !addMain;

				if (NotFlatRateLongDistance || flatRateMain || longDistanceMain) {
					SweetAlert.swal('Success!', 'Successfully submitted', 'success');
				}
				contractDispatchLogs.assignLogs($scope.request);
			})
			.catch((err) => SweetAlert.swal({title: 'Failed!', text: 'Save contract create pdf', type: 'error'}))
			.finally(() => $scope.busy = false);
	}

	function backsidePdf() {
		return createPdf($scope.terms, 'mainPage')
			.then(({data}) => ContractService.getPdfByTab(data[0]))
			.then(({data}) => {
				$scope.data.backSidePagePdf = data;
				return Promise.resolve();
			}).catch((err) => {
				SweetAlert.swal({title: 'Failed!', text: 'Creating backside pdf', type: 'error'});
				return Promise.reject();
			});
	}

	function hideSecondAddInventoryTab() {
		let access = contract_page.useInventory;
		let toStorage = _.get($scope.request, 'request_all_data.toStorage');
		let isOpenSecondAddInventory = $scope.data.secondAddInventoryMoving.open;
		let isSubmittedAddInventory = $scope.data.addInventoryMoving.isSubmitted;
		let useInventory = _.get($scope.request, 'request_all_data.useInventory');

		if (useInventory) {
			access = useInventory;
		}
		if (isSubmittedAddInventory) {
			return !isOpenSecondAddInventory;
		} else if ($scope.ifStorage && toStorage && isOpenSecondAddInventory || $scope.ifStorage && !toStorage && isOpenSecondAddInventory && isSubmittedAddInventory) {
			return false;
		} else {
			return true;
		}
	}

	function hideAddInventoryTab() {
		let access = contract_page.useInventory;
		let isSubmittedInventory = $scope.data.inventoryMoving.isSubmitted;
		let isOpenAddInventory = $scope.data.addInventoryMoving.open;
		let useInventory = _.get($scope.request, 'request_all_data.useInventory');
		let toStorage = _.get($scope.request, 'request_all_data.toStorage');
		let inventoryList = _.get($scope.request, 'inventory.inventory_list', []);
		let isEmptyInventoryList = _.isEmpty(inventoryList);

		if (useInventory) {
			access = useInventory;
		}

		if ($scope.ifStorage && !access || !$scope.ifStorage) {
			return true;
		} else if ($scope.ifStorage && toStorage && (isEmptyInventoryList || isOpenAddInventory) || $scope.ifStorage && !toStorage && (isEmptyInventoryList || (isOpenAddInventory && isSubmittedInventory))) {
			return false;
		} else if (isSubmittedInventory) {
			return !isOpenAddInventory;
		} else {
			return true;
		}
	}

	function hideInventoryTab() {
		let access = contract_page.useInventory;
		let isOpenInventory = $scope.data.inventoryMoving.open;
		let useInventory = _.get($scope.request, 'request_all_data.useInventory');
		let inventoryList = _.get($scope.request, 'inventory.inventory_list', []);

		if (angular.isDefined(useInventory)) {
			access = useInventory;
		}

		if (!$scope.ifStorage && access && isOpenInventory || $scope.ifStorage && !_.isEmpty(inventoryList) || isOpenInventory) {
			return false;
		} else if ($scope.ifStorage && !access || $scope.ifStorage && _.isEmpty(inventoryList) || !isOpenInventory) {
			return true;
		} else if ($scope.ifStorage) {
			return false;
		}
	}

	function hideRentalTab() {
		if ($scope.isOvernight) return true;

		var access = contract_page.useInventory;
		if (_.get($scope.request.request_all_data, 'useInventory', false)) {
			access = $scope.request.request_all_data.useInventory;
		}
		if (!$scope.ifStorage) {
			return $scope.data.inventoryMoving.isSubmitted ? false : !$scope.data.openRental;
		}
		if (!access && $scope.ifStorage) {
			return $scope.request.request_all_data.toStorage ? true : false;
		}
		if (_.isEmpty($scope.request.inventory.inventory_list))
			if ($scope.ifStorage && !$scope.request.request_all_data.toStorage) {
				if (!$scope.data.addInventoryMoving.isSubmitted || ($scope.data.addInventoryMoving.isSubmitted && !$scope.data.secondAddInventoryMoving.isSubmitted && $scope.data.secondAddInventoryMoving.open)) {
					return true;
				} else {
					return false;
				}
			}
		if ($scope.ifStorage && !_.isEmpty($scope.request.inventory.inventory_list)) {
			if (!$scope.request.request_all_data.toStorage) {
				return (!$scope.data.inventoryMoving.isSubmitted) || ($scope.data.inventoryMoving.isSubmitted && !$scope.data.addInventoryMoving.isSubmitted && $scope.data.addInventoryMoving.open) || ($scope.data.inventoryMoving.isSubmitted && $scope.data.addInventoryMoving.isSubmitted && !$scope.data.secondAddInventoryMoving.isSubmitted && $scope.data.secondAddInventoryMoving.open);
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	function isSubmittedInventory() {
		var access = contract_page.useInventory;
		if ($scope.request.request_all_data)
			if (angular.isDefined($scope.request.request_all_data.useInventory)) access = $scope.request.request_all_data.useInventory;
		if ($scope.ifStorage && !access) {
			if ((_.isEmpty($scope.request.inventory) && !$scope.data.addInventoryMoving.isSubmitted) || ($scope.data.inventoryMoving.isSubmitted && !$scope.data.addInventoryMoving.isSubmitted && $scope.data.addInventoryMoving.open)) {
				SweetAlert.swal({
					title: 'Failed!', text: 'Submit Inventory before pay', type: 'error'
				}, function () {
					$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Add. Inventory');
					$('body').scrollTop(0);
				});
				return false;
			} else if (!$scope.data.inventoryMoving.isSubmitted && !_.isEmpty($scope.request.inventory)) {
				SweetAlert.swal({
					title: 'Failed!', text: 'Submit Inventory before pay', type: 'error'
				}, function () {
					$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Inventory');
					$('body').scrollTop(0);
				});
				return false;
			} else if ($scope.data.addInventoryMoving.isSubmitted && $scope.data.secondAddInventoryMoving.open && !$scope.data.secondAddInventoryMoving.isSubmitted && $scope.data.addInventoryMoving.open) {
				SweetAlert.swal({
					title: 'Failed!', text: 'Submit Inventory before pay', type: 'error'
				}, function () {
					$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Add. Inventory') + 1;
					$('body').scrollTop(0);
				});
				return false;
			}
			if (!$scope.data.agreement.isSubmitted && contract_page.rentalAgreementForm) {
				SweetAlert.swal({
					title: 'Please, make storage payment first', text: 'Pay storage fee on rental agreement', type: 'error'
				}, function () {
					$scope.$broadcast('inventory.submit');
					$('body').scrollTop(0);
				});
				return false;
			}
		}
		return true;
	}

	function showService(type) {
		var service = _.find($scope.content.storageFit, {connected_service: type});
		if ($scope.services)
			if ($scope.services[type]) return false;
		return _.get(service, 'show', false);
	}

	function checkLeaseAgreement() {
		if ($scope.request.request_all_data.agreementSave) {
			if ($scope.request.request_all_data.agreementSave.storageAgreementSign) {
				applyPayment('step4', $scope.request.request_all_data.agreementSave.receipt);
				$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Rental agreement');
			} else {
				applyPayment('step3', $scope.request.request_all_data.agreementSave.receipt);
				$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Rental agreement');
			}
		}
	}

	function saveReqDataInvoice(type) {
		var defer = $q.defer();
		resetTravelTime();

		$scope.request.request_all_data.invoice.request_all_data.add_money_discount = $scope.calcData.moneyDiscount;
		$scope.request.request_all_data.invoice.request_all_data.add_percent_discount = $scope.calcData.percentDiscount;
		$scope.request.request_all_data.invoice.request_all_data.valuation.selected.valuation_charge = $scope.calcData.valuation;
		if (type == 'fuel') {
			$scope.request.request_all_data.isFuelSuchrargeChangeOnContract = true;
			$scope.request.request_all_data.invoice.request_all_data.surcharge_fuel = $scope.calcData.fuel;
		}

		if ($scope.request.request_all_data.invoice.additionalWorkTime && !$scope.mainContract) {
			let hoursWork = common.decToTime($scope.request.request_all_data.invoice.additionalWorkTime);
			$scope.request.request_all_data.invoice.work_time = hoursWork;
			$scope.request.request_all_data.invoice.work_time_int = $scope.request.request_all_data.invoice.additionalWorkTime;
			$scope.request.request_all_data.invoice.work_time_old = hoursWork;
		} else {
			let workDuration = $scope.calculation.getCrewTotalDuration();
			ContractCalc.finance.workDuration = workDuration;

			if ($scope.calculation.canCalc() && !$scope.requestType) {
				let travelTime = $scope.isDoubleDriveTime ? $scope.travelTime.raw : ($scope.travelTimeSetting ? $scope.travelTime.raw : 0);
				workDuration -= travelTime;
			}

			$scope.request.request_all_data.invoice.work_time = common.decToTime(workDuration || $scope.request.request_all_data.invoice.work_time_int);
			$scope.request.request_all_data.invoice.work_time_int = workDuration || $scope.request.request_all_data.invoice.work_time_int;
			$scope.request.request_all_data.invoice.work_time_old = common.decToTime(workDuration || $scope.request.request_all_data.invoice.work_time_int);
		}

		$scope.request.request_all_data.req_tips = ContractCalc.finance.tip;
		$scope.request.request_all_data.invoice.moneyDiscount = ContractCalc.finance.moneyDiscount;
		$scope.request.request_all_data.invoice.discount = ContractCalc.finance.discount;
		$scope.request.request_all_data.invoice.discountType = $scope.data.discountType;
		$scope.request.request_all_data.customTax = ContractCalc.finance.customTax;

		let reqTotal = 0;
		if ($scope.request.service_type == 5 || $scope.request.service_type == 7) reqTotal = $scope.calculation.totalCostForFlatRate(); else
			reqTotal = parseFloat($scope.calculation.totalCost().toFixed(2));
		if ($scope.request.request_all_data.grand_total != reqTotal) $scope.request.request_all_data.grand_total = reqTotal;

		if (!_.isEqual(baseRequestAllData, $scope.request.request_all_data)) {
			baseRequestAllData = angular.copy($scope.request.request_all_data);
			RequestServices.saveRequestDataContract($scope.request)
				.then(() => {
					defer.resolve();
				})
				.catch((err) => {
					defer.reject(err);
				});
		} else {
			defer.resolve();
		}

		return defer.promise;
	}

	function calculateAddServices() {
		return additionalServicesFactory.getExtraServiceTotal($scope.request.extraServices);
	}

	function totalLessDeposit() {
		var total = 0;
		if ($scope.request.service_type.raw != 5 && $scope.request.service_type.raw != 7) {
			total = parseFloat(($scope.calculation.totalCost() - $scope.calcData.deposit - $scope.balance_paid).toFixed(2));
		} else {
			total = parseFloat(($scope.calculation.totalFlatRateCost() - $scope.calcData.deposit - $scope.balance_paid).toFixed(2));
		}
		if (total < 1 && total > -1) total = 0;
		return parseFloat(total.toFixed(2));
	}

	$scope.savePhotos = function (files) {
		$scope.busy = true;
		var imagesArr = {
			field_contract_images: {
				add: []
			}
		};
		angular.forEach(files, function (flowFile, i) {
			var fileReader = new FileReader();
			fileReader.onload = (function (file) {
				return function (e) {
					var photo = {
						uri: e.target.result, title: file.title
					};
					imagesArr['field_contract_images']['add'].push(photo);
					endFileReadHandler(i + 1);
					var imageToShow = {
						url: e.target.result, title: file.title
					};
					$scope.addedImages.push(imageToShow);
				};
			})(flowFile);
			fileReader.readAsDataURL(flowFile.file);
		});

		function endFileReadHandler(processCount) {
			if (processCount != files.length) return false;

			if (imagesArr['field_contract_images']['add'].length > 0) {
				RequestServices.updateRequest($scope.request.nid, imagesArr)
					.then(function (response) {
						if (_.head(response) == false) {
							SweetAlert.swal('Images didn\'t upload!', 'Something happens...', 'error');
							return false;
						}
						SweetAlert.swal('Images have been uploaded!', 'Successfully Saved', 'success');
						$scope.uploader.flow.files = [];
						var msgToLog = '';
						msgToLog += 'To contract was added ';
						if ($scope.addedImages.length == 1) {
							msgToLog += ' image';
						} else {
							msgToLog += $scope.addedImages.length + ' images';
						}
						var firstTitle = 0;
						angular.forEach($scope.addedImages, function (newImg) {
							if (angular.isDefined(newImg.title)) {
								if (firstTitle == 0) {
									msgToLog += ': ';
									firstTitle++;
								}
								msgToLog += newImg.title + ', ';
							}
							$scope.images.push(newImg);
							$scope.busy = false;
						});
						if (firstTitle != 0) msgToLog = msgToLog.slice(0, -2);
						msgToLog += '.';
						ContractService.addForemanLog(msgToLog, 'Images have been uploaded!', $scope.request.nid, 'MOVEREQUEST');
						$scope.addedImages = [];
					});
			}
		}
	};
	$scope.removePhoto = function (fid, index) {
		$scope.busy = true;
		var obj = {
			'data': {
				'field_contract_images': {'del': [{'fid': fid}]}
			}
		};
		RequestServices.removeImage($scope.request.nid, obj).then(function (response) {
			var msgToLog = 'Image deleted from contract.';
			ContractService.addForemanLog(msgToLog, 'Photos on contract page', $scope.request.nid, 'MOVEREQUEST');
			$scope.images.splice(index, 1);
			$scope.busy = false;
			if (localStorage[nid]) {
				delete localStorage[nid];
			}
		}, function (err) {
			saveToLocalStorage();
			logger.error(err || 'error', null, 'Something going wrong with contract document');
		});
	};

	function sendReceiptToServer(data_receipt) {
		if (data_receipt.nid != $scope.request.nid) {
			return false;
		}
		if (angular.isDefined(data_receipt.tips) && data_receipt.tips != 0) {
			$scope.calcData.tip += parseFloat(data_receipt.tips);
			saveContract();
		}
		if (data_receipt.receipt.id) {
			if ($scope.request.request_all_data.save_receipt) {
				_.forEach($scope.request.receipts, function (receipt_req) {
					if (receipt_req.id == data_receipt.receipt.id && localStorage['save_receipt']) {
						delete localStorage['save_receipt'];
						return false;
					}
				});
				data_receipt = angular.fromJson($scope.request.request_all_data.save_receipt);
			}
			if (data_receipt.receipt.transaction_id == 'Custom Payment') {
				//set receipt
				PaymentServices.set_receipt(data_receipt.nid, data_receipt.receipt).then(function (data) {
					if (data[0] == false) {
						var msg = '';
						if (data.text) msg = data.text;
						SweetAlert.swal({
							title: 'Failed!', text: msg, type: 'error', html: true
						});
						return false;
					}
					data_receipt.receipt.id = _.head(data);
					localStorage['save_receipt'] = angular.toJson(data_receipt);
					delete localStorage['save_receipt'];
					PaymentServices.get_receipt($scope.request.nid, MOVEREQUEST).then(function (new_receipts) {
						$scope.request.receipts = _.clone(new_receipts);
						$scope.balance_paid = calcPaymentContract(new_receipts);
						ContractCalc.initData({finance: $scope.calcData});
						ContractCalc.initData({request: $scope.request, travelTime: $scope.travelTime});
					});
				});
			} else {
				if (!data_receipt.signature && data_receipt.sign.length == 0) {
					applyPayment('step3', data_receipt);
					return false;
				} else if (data_receipt.signature && data_receipt.images.length == 0) {
					applyPayment('step4', data_receipt);
					return false;
				} else {
					if (data_receipt.sign.length != 0) data_receipt.images.push(data_receipt.sign[0]);
					PaymentServices.saveCardImage(data_receipt.receipt.id, data_receipt.images).then(function () {
						delete localStorage['save_receipt'];
					});
				}
			}
		}
	}

	function initMoveDateOfPairedRequest(request, storageReq) {
		$scope.pairedRequestDate = moment($scope.pairedRequest.date.value, 'MM/DD/YYYY').format('MMMM DD, YYYY');
		let now = request.date.raw;
		$scope.interval = 0;

		if (request.storage_id != 0) {
			let then = storageReq.date.raw;
			if (!request.request_all_data.toStorage) {
				$scope.interval = (then - now) / 86400;
			} else {
				$scope.interval = (now - then) / 86400;
			}
			$scope.storage_rate = CalculatorServices.getStorageRate(request, $scope.interval);
		}
	}

	function loadPairedRequest(req) {
		if (_.isEqual(req.storage_id, 0)) {
			$scope.pairedRequest = undefined;
		} else {
			var promise = RequestServices.getRequest(req.storage_id);
			$scope.busy = true;
			promise.then(function (data) {
				var request = data;
				setUpisDoubleDriveTime();
				$scope.pairedRequest = request;
				initMoveDateOfPairedRequest(req, request);
				$scope.busy = false;
			}, function (reason) {
				logger.error(reason, reason, 'Error');
				$scope.busy = false;
			});
		}
	}

	function loadInventory(data) {
		$scope.inventoryCount = {};
		$scope.inventoryList = {};
		$scope.inventoryLength = 0;
		$scope.addInventoryCount = {};
		$scope.addInventoryList = {};
		$scope.addInventoryLength = 0;
		$scope.secondAddInventoryCount = {};
		$scope.secondAddInventoryList = {};
		$scope.secondAddInventoryLength = 0;
		if (data.request_all_data) {
			if (data.request_all_data.additional_inventory) {
				_.forEach(data.request_all_data.additional_inventory, function (item) {
					if (angular.isUndefined($scope.addInventoryCount[item.id])) $scope.addInventoryCount[item.id] = 0;
					$scope.addInventoryCount[item.id] += item.count;
					if (angular.isUndefined($scope.addInventoryList[item.id])) {
						$scope.addInventoryList[item.id] = item;
					} else {
						$scope.addInventoryList[item.id].count += item.count;
					}
					$scope.addInventoryLength += item.count;
				});
			}
			if (data.request_all_data.second_additional_inventory) {
				_.forEach(data.request_all_data.second_additional_inventory, function (item) {
					if (angular.isUndefined($scope.secondAddInventoryCount[item.id])) $scope.secondAddInventoryCount[item.id] = 0;
					$scope.secondAddInventoryCount[item.id] += item.count;
					if (angular.isUndefined($scope.secondAddInventoryList[item.id])) {
						$scope.secondAddInventoryList[item.id] = item;
					} else {
						$scope.secondAddInventoryList[item.id].count += item.count;
					}
					$scope.secondAddInventoryLength += item.count;
				});
			}
		}
		let inventory = [];

		if (_.get(data, 'inventory.inventory_list')) {
			inventory = angular.copy(data.inventory.inventory_list);
		}

		if (_.get(data, 'request_all_data.inventory')) {
			inventory = inventory.concat(angular.copy(data.request_all_data.inventory));
		}

		if (!_.get(data, 'request_all_data.inventory')) {
			_.set(data, 'request_all_data.inventory', []);
		}

		_.forEach(inventory, function (item) {
			if (angular.isUndefined($scope.inventoryCount[item.id])) {
				$scope.inventoryCount[item.id] = 0;
			}

			$scope.inventoryCount[item.id] += item.count;

			if (angular.isUndefined($scope.inventoryList[item.id])) {
				$scope.inventoryList[item.id] = item;
			} else {
				$scope.inventoryList[item.id].count += item.count;
			}

			$scope.inventoryLength += item.count;
		});
	}

	function calculateMaxCountSign() {
		if ($scope.flatMiles) {
			maxSignCount = 9;
			$scope.maxSignCount = $scope.data.crews.length > 1 ? maxSignCount - 4 + ($scope.data.crews.length * 4) : maxSignCount;
		} else {
			$scope.maxSignCount = $scope.data.crews.length > 1 ? maxSignCount - 2 + ($scope.data.crews.length * 2) : maxSignCount;
		}
		if ($scope.requestType) $scope.maxSignCount = 7;
	}

	//**** @Construct
	//_resetContract();

	!function () {

		if (!_.isNumber(nid) && !_.isFinite(nid)) $location.path('/');
		var defer = $q.defer();
		if (session.userId.nodes.hasOwnProperty(nid)) {
			defer.resolve(session.userId.nodes[nid]);
		} else {
			if (AuthenticationService.isAuthorized(getUserRole())) {
				RequestServices.getRequest(nid).then(function (data) {
					defer.resolve(data);
				});
			} else {
				defer.reject();
				$location.path('/pages-404.html');
			}
		}
		defer.promise.then(function (data) {
			if (data.service_type.raw == 7) {
				let longDistanceInvoiceRate = _.get(data, 'request_all_data.invoice.field_long_distance_rate.value');
				let longDistanceRate = _.get(data, 'field_long_distance_rate.value');

				if (!_.isUndefined(longDistanceInvoiceRate) && !_.isEmpty(longDistanceInvoiceRate)) {
					ContractFactory.longDistanceRate = longDistanceInvoiceRate;
				} else if(!_.isUndefined(longDistanceRate) && !_.isEmpty(longDistanceRate)) {
					ContractFactory.longDistanceRate = longDistanceRate;
				}
			}

			InitRequestFactory.initRequestFields(data, calcSettings.travelTime);
			InitRequestFactory.initInvoiceRequest(data, data.request_all_data.invoice, undefined, true);
			$scope.proposalPages = [];
			$scope.familiarizationPages = [];
			let j, obj, tabsCount = $scope.tabs.length;
			var request_data = {};

			$scope.travelTimeSetting = ((angular.isDefined(data.request_all_data.travelTime) ? data.request_all_data.travelTime : calcSettings.travelTime));
			//Moving and Storage, without Overnight
			$scope.ifStorage = data.service_type.raw == 2;

			$scope.isOvernight = data.service_type.raw == 6;
			// loadPairedRequest(data);
			loadInventory(data);
			if (data.request_data.value != '') {
				$scope.request_data = request_data = angular.fromJson(data.request_data.value);
			}
			if (data.inventory)
				if (data.inventory.inventory_list) data.inventory_weight = InventoriesServices.calculateTotals(data.inventory.inventory_list);

			$scope.request = data;
			$scope.request = sharedRequestServices.calculateQuote($scope.request);
			setUpisDoubleDriveTime();
			setTravelTime();
			baseRequestAllData = angular.copy($scope.request.request_all_data);

			for (j = 0; j < tabsCount; j++) {
				if ($scope.tabs[j].show && $scope.tabs[j].proposalPage) {
					obj = Object.assign($scope.tabs[j], {index: j});
					$scope.proposalPages.push(obj);
				}
			}

			$scope.clientRoute = {
				mainPage: `http://${$location.host()}:${$location.port()}/account/#/request/${$scope.request.nid}/contract_main_page_read`,
				get additionalPages() {
					return $scope.proposalPages.length ? `http://${$location.host()}:${$location.port()}/account/#/request/${$scope.request.nid}/contract_proposal_additional_pages_read` : void 0;
				}
			};

			$scope.familiarizationPages = angular.copy($scope.proposalPages);

			if ($scope.tabs[0].name === mainCotractTitle) {
				obj = Object.assign($scope.tabs[0], {index: 0});
				$scope.familiarizationPages.unshift(obj);
			}

			if (angular.isDefined($scope.request.request_all_data.flatMiles)) {
				$scope.flatMiles = $scope.request.request_all_data.flatMiles;
			} else if ($scope.request.distance && !!basicSettings.local_flat_miles && parseFloat($scope.request.distance.value) > parseFloat(basicSettings.local_flat_miles)) {
				$scope.flatMiles = true;
				$scope.request.request_all_data.flatMiles = true;
			}
			if (angular.isDefined($scope.request.field_contract_images)) $scope.images = $scope.request.field_contract_images;
			if (angular.isUndefined($scope.request.request_all_data.invoice)) {
				$scope.request.request_all_data.invoice = {
					request_all_data: {
						valuation: {
							valuation_charge: 0
						}
					}
				};
			}
			if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data)) {
				$scope.request.request_all_data.invoice.request_all_data = {};
				$scope.request.request_all_data.invoice.request_all_data = angular.copy($scope.request.request_all_data);
				$scope.request.request_all_data.invoice.request_all_data.valuation = angular.copy($scope.request.request_all_data.valuation);
			} else if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data.valuation) && angular.isDefined($scope.request.request_all_data.valuation)) {
				$scope.request.request_all_data.invoice.request_all_data.valuation = angular.copy($scope.request.request_all_data.valuation);
			} else if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data.valuation)) {
				$scope.request.request_all_data.invoice.request_all_data.valuation = {
					valuation_charge: 0
				};
			}
			if ((angular.isUndefined($scope.request.request_all_data.invoice) || $scope.request.request_all_data.length == 0) && $scope.request.request_all_data.surcharge_fuel) {
				if (_.isArray($scope.request.request_all_data)) $scope.request.request_all_data = {};
				$scope.request.request_all_data.invoice = {
					request_all_data: {
						surcharge_fuel: parseFloat($scope.request.request_all_data.surcharge_fuel.toFixed(2))
					}
				};
			} else if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data) && angular.isDefined($scope.request.request_all_data.surcharge_fuel)) {
				$scope.request.request_all_data.invoice.request_all_data = {};
				$scope.request.request_all_data.invoice.request_all_data.surcharge_fuel = parseFloat($scope.request.request_all_data.surcharge_fuel.toFixed(2));
			} else if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data.surcharge_fuel) && angular.isDefined($scope.request.request_all_data.surcharge_fuel)) {
				$scope.request.request_all_data.invoice.request_all_data.surcharge_fuel = parseFloat($scope.request.request_all_data.surcharge_fuel.toFixed(2));
			} else if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data.surcharge_fuel)) {
				$scope.request.request_all_data.invoice.request_all_data.surcharge_fuel = 0;
			}
			if (angular.isDefined($scope.request.request_all_data.add_money_discount)) {
				if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data.add_money_discount) && angular.isDefined($scope.request.request_all_data.add_money_discount)) $scope.request.request_all_data.invoice.request_all_data.add_money_discount = $scope.request.request_all_data.add_money_discount;
				$scope.calcData.moneyDiscount = $scope.request.request_all_data.invoice.request_all_data.add_money_discount;
			}

			setValuation($scope.request);

			$scope.calcData.fuel = $scope.request.request_all_data.invoice.request_all_data.surcharge_fuel;

			setTravelTime();

			$scope.requestType = ($scope.request.service_type.raw == 5 || $scope.request.service_type.raw == 7);
			$scope.switchers = request_data.switchers || null;
			if ($scope.switchers) {
				ContractService.getUsersByRole({  //get all selected users
					active: 1,
					role: ['helper', 'foreman', 'driver']
				})
					.then(({data}) => {
						workers = data;
					});
			}
			ContractCalc.initData({request: data, travelTime: $scope.travelTime});
			$scope.data.crews[0].rate = data.rate.value;
			if (_.get($scope.request.request_all_data, 'invoice.rate.value', false)) {
				$scope.data.crews[0].rate = $scope.request.request_all_data.invoice.rate.value;
			}
            if ($scope.request.request_all_data.add_rate_discount)
                $scope.data.crews[0].rate = $scope.request.request_all_data.add_rate_discount;
            if ($scope.data.crews.length > 1) {
                for (var i = 1, l = $scope.data.crews.length; i < l; i++) {
                    var crew = $scope.data.crews[i];
                    crew.rate = CalculatorServices.getRateAdditionalHelper(data.date.raw * 1000, request_data.additionalCrews[i - 1]);
                }
            }
            var currentFlags = _.values($scope.request.field_company_flags.value);
            var isAdmin = (getUserRole().indexOf('administrator') > -1 || getUserRole().indexOf('foreman') > -1);

            checkForeman();

            if ($scope.request.request_all_data.save_receipt && !localStorage['save_receipt']) {
                var data_receipt = angular.fromJson($scope.request.request_all_data.save_receipt);
                sendReceiptToServer(data_receipt);
            }

            //check if agreement signed and upload images
            checkLeaseAgreement();

            ContractFactory.getData({permission: isAdmin, nid: nid},
                function (contract) { //cb
                    if ((_.isEmpty($scope.request_data) || !$scope.request_data.crews) && !$scope.isFamiliarization && !$scope.isPrint) {
                        SweetAlert.swal("Failed!", "This job isn't assigned", "error");
                    }

                    $scope.$broadcast('data_loaded');
                    ContractFactory.initData(contract);
                    if(_.get($scope.request.request_all_data, 'invoice.rate.value', false)) {
						$scope.data.crews[0].rate = $scope.request.request_all_data.invoice.rate.value;
					}
                    if ($scope.request.request_all_data.add_rate_discount)
                        $scope.data.crews[0].rate = $scope.request.request_all_data.add_rate_discount;

					if (angular.isDefined(contract.finance)) {
                        if (contract.finance.discount) {
                            $scope.calcData.discount = contract.finance.discount;
                        }
                        if(contract.finance.requestTipsPaid)
                            $scope.calcData.requestTipsPaid = contract.finance.requestTipsPaid;
                        if(contract.finance.requestTipsPickupPaid)
                            $scope.calcData.requestTipsPickupPaid = contract.finance.requestTipsPickupPaid;
                        if(contract.finance.requestTips)
                            $scope.calcData.requestTips = contract.finance.requestTips;
                        if(contract.finance.requestTipsPickup)
                            $scope.calcData.requestTipsPickup = contract.finance.requestTipsPickup;

                        if(contract.finance.pickupTips)
                            $scope.calcData.pickupTips = contract.finance.pickupTips;
                        if(contract.finance.tip)
                            $scope.calcData.tip = contract.finance.tip;

                        $scope.calcData.creditTax = contract.finance.creditTax || 0;
                        $scope.calcData.cashDiscount = contract.finance.cashDiscount || 0;
                        $scope.calcData.creditTaxPickup = contract.finance.creditTaxPickup || 0;
                        $scope.calcData.cashDiscountPickup = contract.finance.cashDiscountPickup || 0;
                        $scope.calcData.customPayments = contract.finance.customPayments;
                    }
					if($scope.request.request_all_data.req_tips && $scope.calcData.tip == 0) {
						if (!$scope.calcData.requestTips)
							$scope.calcData.requestTips = parseFloat($scope.request.request_all_data.req_tips);
						if (!$scope.calcData.requestTipsPickup) {
							$scope.calcData.requestTipsPickup = _.round(parseFloat($scope.request.request_all_data.req_tips) * PICK_UP_PERCENT / HUNDRED_PERCENT, 2);
							$scope.calcData.pickupTips = _.round(parseFloat($scope.request.request_all_data.req_tips) * PICK_UP_PERCENT / HUNDRED_PERCENT, 2);
						}
					}
					$scope.calcData.tip = $scope.calcData.tip || $scope.request.request_all_data.req_tips || 0;
					if (!$scope.pushTips) $scope.calcData.pickupTips = parseFloat((parseFloat($scope.calcData.tip) * PICK_UP_PERCENT / HUNDRED_PERCENT).toFixed(2));
                    if ($scope.request_data.paymentType == 'FlatFee') {
                        var total = 0;
                        angular.forEach($scope.request_data.pricePerHelper, function (i) {
                            total += i;
                        });
                        ContractCalc.finance.fixedTotal = total;
                    }
                    $scope.services = {};

                    if (angular.isDefined(contract.factory)) {
                        if (contract.factory.openStorageAndTransit)
                            $scope.openStorageAndTransit = contract.factory.openStorageAndTransit;
                        if (contract.factory.showDiscount)
                            $scope.showDiscount = contract.factory.showDiscount;
                        if (angular.isDefined(contract.factory.storageServices))
                            if (!_.isEmpty(contract.factory.storageServices))
                                $scope.services = _.clone(contract.factory.storageServices);
                        if (angular.isDefined(contract.factory.storageFit) && _.isArray(contract.factory.storageFit)) {
                            contract.factory.storageFit = {};
                            $scope.data.storageFit = {};
                        }
                        if (angular.isDefined(contract.factory.feedBack) && !_.isEmpty(contract.factory.feedBack))
                            $scope.data.feedBack = contract.factory.feedBack;

                        if (angular.isDefined(contract.factory.releaseData)) {
                            //check if it's old version of release form
                            if (angular.isDefined(contract.factory.releaseData.releaseInitials)) {
                                if (!$scope.oldVersionOfReleaseForm) {
                                    ContractFactory.factory.releaseData = angular.copy(ContractFactory.oldReleaseFormObj);
                                    ContractFactory.factoryObj.releaseData = angular.copy(ContractFactory.oldReleaseFormObj);
                                }
                                $scope.data.releaseData.releaseInitials = contract.factory.releaseData.releaseInitials;
                                if (angular.isDefined(contract.factory.releaseData.itemsRelease))
                                    $scope.data.releaseData.itemsRelease = contract.factory.releaseData.itemsRelease;
                                $scope.oldVersionOfReleaseForm = true;
                            } else {
                                _.forEach(contract.factory.releaseData, function (item, field) {
                                    if (angular.isDefined(item))
                                        $scope.data.releaseData[field] = item;
                                });
                                $scope.$broadcast('release.init');
                            }
                            $scope.data.releaseData.isSubmitted = contract.factory.releaseData.isSubmitted;
                            $scope.data.releaseData.currentStep = contract.factory.releaseData.currentStep;
                        }
                        if (angular.isDefined(contract.factory.additionalContractPages)) {
                            ContractService.getPdfByRequest(nid , $scope.fieldData.enums.entities.MOVEREQUEST)
                                .then(({data}) => {
                                    _.forEach(contract.factory.additionalContractPages, page => {
                                        page.pdfData = _.find(data, pdf => pdf.page_id === page.page_id);
                                    });

                                    if ($scope.data.isSubmitted) {
										$scope.data.mainContractPagePdf = _.find(data, (d) => d.page_id == mainContractID);
									}
                                })
                                .catch((err) => SweetAlert.swal({title: "Failed!", text: "Get pdf by request", type: "error"}));

                            $scope.data.additionalContractPages = contract.factory.additionalContractPages;
                            $scope.loadAddPages = true;
                        }
                        if(angular.isDefined(contract.factory.customContractSignature)) {
                            $scope.data.customContractSignature = contract.factory.customContractSignature;
                            $scope.loadCustom = true;
                        }
                        $scope.$broadcast('data_loaded');
						if (contract.factory.contract_image && $scope.data.isSubmitted) {
							//$scope.imContract = contract.factory.contract_image;
						}
                        if (contract.factory.isFirstSubmittedPage) {
							$scope.tabs = contract.factory.tabs;
							$scope.data.isFirstSubmittedPage = contract.factory.isFirstSubmittedPage;
							$scope.data.isFirstAddMainId = contract.factory.isFirstAddMainId || -1;
                        }
                    } else {
                        $scope.$broadcast('release.init');
                    }
                    firstInitAdditionalPages();
					checkProposalPages();
                    $scope.loadAddPages = true;
                    $scope.loadCustom = true;
                    $scope.data.currentStep = $scope.data.signatures.length - 1;
                    $scope.data.isSubmitted = (currentFlags.indexOf('Completed') > -1 && $scope.request.service_type.raw != 5) || ($scope.request.service_type.raw == 5 && currentFlags.indexOf(deliveryJobCompleted) > -1);
                    $scope.data.isPickupSubmitted = currentFlags.indexOf(pickupJobCompleted) > -1;

                    $scope.$broadcast('data_loaded');
                    if ($scope.flatMiles) {
                        maxSignCount = 9;
                        $scope.maxSignCount = $scope.data.crews.length > 1 ? maxSignCount - 4 + ($scope.data.crews.length * 4) : maxSignCount;
                    } else {
                        $scope.maxSignCount = $scope.data.crews.length > 1 ? maxSignCount - 2 + ($scope.data.crews.length * 2) : maxSignCount;
                    }
                    if ($scope.requestType)
                        $scope.maxSignCount = 7;
                    for (var field in localStorage) {
                    	let testFielsIsNumber = +field;

                    	if (_.isNumber(testFielsIsNumber) && !_.isNaN(testFielsIsNumber) && field != nid) {
                    		delete localStorage[field];
							continue;
	                    }

                        if (field == nid) {
                            getFromLocalStorage();
                        } else if (field.search("receipt_") > -1) {
                            sendReceipt(localStorage[field]);
                        } else if (field == 'save_receipt') {
                            var data_receipt = angular.fromJson(localStorage['save_receipt']);
                            sendReceiptToServer(data_receipt);
                        }
                    }
                    if(contract_page.first_signature == false && !$scope.data.signatures[0] ||
					   contract_page.lessInitialContract && !$scope.data.signatures[0]) {
						var data_sign = {
							date: "",
							owner: "customer",
							value: ""
						};
						$scope.data.signatures[0] = data_sign;
						$scope.data.signatures.length += 1;
					}
                    if ($scope.request.service_type.raw != 5 && $scope.request.service_type.raw != 7) {
	                    $scope.grandTotalLog = $scope.calculation.totalCost();
                    } else {
	                    $scope.grandTotalLog = $scope.calculation.totalClosingFlatRateCost();
                    }
                    if ($scope.request.request_all_data.couponData && !$scope.data.discountUsed) {
                        $scope.data.discountUsed = true;
                        $scope.calcData.discount = $scope.request.request_all_data.couponData.discount;
                        $scope.data.discountType = $scope.request.request_all_data.couponData.discountType;
                    }

                    var declarValue = {};
                    var declarCount = 0;
                    _.forEach($scope.declarationValue, function (declarObj) {
                        if (declarObj.show) {
                            declarCount++;
                            declarValue = declarObj.index;
                        }
                    });
                    if (declarCount == 1) {
                        $scope.data.declarationValue.selected = declarValue;
                    }
                    storagePayment();
					contractDispatchLogs.initRequestLog($scope.request, $scope.data, $scope.calcData, undefined, logsViewTextIds.logType.contractPageSteps);
					$scope.requestReady = true;
                });
            if ($scope.request.receipts)
                $scope.balance_paid = calcPaymentContract($scope.request.receipts);
        }, function (error) {
            saveToLocalStorage();
            logger.error('error', null, 'Something going wrong with contract document');
        }).finally(function () {
            $scope.busy = false;
        });
    }();

    function checkProposalPages() {
		$scope.proposalPages = [];
		$scope.familiarizationPages = [];
		let j,
			obj,
			tabsCount = $scope.tabs.length;

		for (j = 0; j < tabsCount; j++) {
			if($scope.tabs[j].show && $scope.tabs[j].proposalPage) {
				obj = Object.assign($scope.tabs[j], {index: j})
				$scope.proposalPages.push(obj);
			}
		}

		$scope.clientRoute = {
			mainPage: `http://${$location.host()}:${$location.port()}/account/#/request/${$scope.request.nid}/contract_main_page_read`,
			get additionalPages() {
				return $scope.proposalPages.length ?
					`http://${$location.host()}:${$location.port()}/account/#/request/${$scope.request.nid}/contract_proposal_additional_pages_read` :
					void 0;
			}
		};

		$scope.familiarizationPages = angular.copy($scope.proposalPages);

		if ($scope.tabs[0].name === mainCotractTitle) {
			obj = Object.assign($scope.tabs[0], {index: 0});
			$scope.familiarizationPages.unshift(obj);
		}
	}

    function calcPaymentContract(payments) {
        var total = 0;
        angular.forEach(payments, function (payment) {
            if (!payment.pending && payment.payment_flag == "contract")
                total += parseFloat(payment.amount);
        });
        return total;
    }

    function pickupTimer(type) {
        if ($scope.requestType) return false;

        var active = $scope.navigation.activeCrew;
        var dateRaw = $('#edit-' + type + '-pickup-time-' + active).val().split(' ');
        var dateTime = dateRaw[0].split(':');
        if (!dateRaw[1]) return false;
        var dateAmPm = dateRaw[1].toLowerCase();
        // Convert 12h to 24h...
        if (dateAmPm == 'pm') {
            if (parseInt(dateTime[0]) < 12) dateTime[0] = (parseInt(dateTime[0]) + 12).toString();
        }
        else {
            if (parseInt(dateTime[0]) == 12) dateTime[0] = 0;
        }

        var selectedTime = new Date();
        selectedTime.setHours(dateTime[0]);
        selectedTime.setMinutes(dateTime[1]);
        selectedTime.setSeconds(0);
        var now = new Date();
        var diffMs = Math.abs(selectedTime - now);
        var minTime = Math.round(diffMs / 60000);


        $scope.data.crews[active].timer.lastAction = type;
        if (type == 'start') {
            $scope.data.crews[active].timer.startPickUp = $('#edit-' + type + '-pickup-time-' + active).val();
        } else if (type == 'end') {
            $scope.data.crews[active].timer.stopPickUp = $('#edit-' + type + '-pickup-time-' + active).val();
        }
        if ($scope.data.crews[active].timer.stop != 0)
            $scope.data.crews[active].timer.stop = 0;
        if ($scope.data.crews[active].timer.start != 0)
            $scope.data.crews[active].timer.start = 0;
        ContractCalc.calculateWork($scope.data.crews[active]);

        saveContract({
            errorCb: function () {
            }
        });
    }

    function unloadTimer(type) {
        if ($scope.requestType) return false;

        var active = $scope.navigation.activeCrew;
        var dateRaw = $('#edit-' + type + '-unload-time-' + active).val().split(' ');
        var dateTime = dateRaw[0].split(':');
        if (!dateRaw[1]) return false;
        var dateAmPm = dateRaw[1].toLowerCase();
        // Convert 12h to 24h...
        if (dateAmPm == 'pm') {
            if (parseInt(dateTime[0]) < 12) dateTime[0] = (parseInt(dateTime[0]) + 12).toString();
        }
        else {
            if (parseInt(dateTime[0]) == 12) dateTime[0] = 0;
        }

        var selectedTime = new Date();
        selectedTime.setHours(dateTime[0]);
        selectedTime.setMinutes(dateTime[1]);
        selectedTime.setSeconds(0);
        var now = new Date();
        var diffMs = Math.abs(selectedTime - now);
        var minTime = Math.round(diffMs / 60000);


        $scope.data.crews[active].timer.lastAction = type;
        if (type == 'start') {
            $scope.data.crews[active].timer.startUnload = $('#edit-' + type + '-unload-time-' + active).val();
        } else if (type == 'end') {
            $scope.data.crews[active].timer.stopUnload = $('#edit-' + type + '-unload-time-' + active).val();
        }
        if ($scope.data.crews[active].timer.stop != 0)
            $scope.data.crews[active].timer.stop = 0;
        if ($scope.data.crews[active].timer.start != 0)
            $scope.data.crews[active].timer.start = 0;
        ContractCalc.calculateWork($scope.data.crews[active]);

        saveContract({
            errorCb: function () {
            }
        });
    }

    function setLessSignatures(arr) {
        var data_sign = {
            date: "",
            owner: "customer",
            value: ""
        };
        _.forEach(arr, function (val) {
            $scope.data.signatures[val] = data_sign;
            $scope.data.signatures.length += 1;
            $scope.data.currentStep = $scope.data.signatures.length - 1;
        });
        if (!$scope.flatMiles) {
            startTimer();
            stopTimer();
        } else {
            pickupTimer('start');
            pickupTimer('end');
            unloadTimer('start');
            unloadTimer('end');
        }
        return $scope.data.signatures.length;
    }

    function checkLessSignatures(nextStep, pickUpClose, deliveryClose) {
        var declarStep = 2, pickupCommentsStep = 4, deliveryCommentsStep = 6;
        var data_sign = {
            date: "",
            owner: "customer",
            value: ""
        };
        if (contract_page.lessInitialContract && (declarStep == nextStep || pickUpClose && !deliveryClose || deliveryClose)) {
            var val = '';
            if (declarStep == nextStep)
                val = declarStep;
            if (pickUpClose && !deliveryClose && _.isEmpty($scope.data.req_comment))
                val = pickupCommentsStep;
            if (deliveryClose && _.isEmpty($scope.data.req_commentDelivery))
                val = deliveryCommentsStep;
            if (val) {
                $scope.data.signatures[val] = data_sign;
                $scope.data.signatures.length += 1;
                $scope.data.currentStep = $scope.data.signatures.length - 1;
            }
        }
        return $scope.data.signatures.length;
    }

    function saveStep() {
        var blank = isCanvasBlank('signatureCanvas');
        if (blank) {
            SweetAlert.swal("Failed!", 'Sign please', "error");
        } else {
            var signatureValue = ContractFactory.getCropedSignatureValue();
            var ln = $scope.data.signatures.length;
            var nextStep = ln;

            if ($scope.activeTab != 0) {
                if (ContractFactory.factory.additionalContractPagesSignature != -1) {
                    let signatureObj = {
                        value: SIGNATURE_PREFIX + signatureValue,
                        date: moment().format('x')
                    };
                    let fieldName = 'signature_field_' + ContractFactory.factory.additionalContractPagesSignature;

					let index;

					if (!_.isEmpty($scope.addMainPage) && $scope.activeTab === 1) {
						index = _.findLastIndex($scope.data.additionalContractPages, (addPages) =>
							addPages.page_id === $scope.addMainPage.page_id);
						if (~index) {
							$scope.data.additionalContractPages[index][fieldName] = signatureObj;
							$scope.data.additionalContractPages[$scope.activeTab - 1][fieldName] = signatureObj;
						}
					} else {
						let isNotMainPage = $scope.getPageId > $scope.activeTab - 1;
						let isMainPage = $scope.activeTab - 1 == $scope.getPageId;
						let isFirstMainPage = $scope.getPageId === 1;
						let isNotExistPageId = _.isUndefined($scope.getPageId) || !~$scope.getPageId;
						let currentActiveTab = $scope.activeTab - 1;
						let isExistsCurrentTab = !!_.get($scope, `data.additionalContractPages[${currentActiveTab}]`);

						if (isNotMainPage || isMainPage || isFirstMainPage || isNotExistPageId || isExistsCurrentTab) {
							$scope.data.additionalContractPages[$scope.activeTab - 1][fieldName] = signatureObj;
						} else {
							$scope.data.additionalContractPages[$scope.activeTab - 2][fieldName] = signatureObj;
						}
					}

                    $scope.data.additionalContractPagesSignature = -1;
                    ContractFactory.factory.additionalContractPagesSignature = -1;
                    $scope.busy = true;
                    saveContract()
						.finally(() => $scope.busy = false);

					return false;
				}
			}

			if (ContractFactory.factory.customContractSignature.currentCustomType) {
				let signatureObj = {
					value: SIGNATURE_PREFIX + signatureValue, date: moment().format('x')
				};

				$scope.data.customContractSignature[ContractFactory.factory.customContractSignature.currentCustomType][$scope.data.customContractSignature.currentSignature] = signatureObj;

				$scope.data.customContractSignature.currentCustomType = '';
				$scope.data.customContractSignature.currentSignature = '';
				ContractFactory.factory.customContractSignature.currentCustomType = '';
				ContractFactory.factory.customContractSignature.currentSignature = '';
				$scope.busy = true;
				saveContract()
					.finally(() => $scope.busy = false);

				return false;
			}

			var stepId = Number($scope.data.currentStep);
			if (isNaN(stepId)) return false;

			if ($scope.data.signatures.hasOwnProperty(stepId)) {
				logger.warning('You can\'t perform this action', 'Something going wrong', stepId, $scope.data.signatures.length, $scope.data.signatures);
				return false;
			}

			if (stepId == 2) {
				if ($scope.data.declarationValue.selected.length == 0) {
					logger.error('Please Select Declaration', null, 'Error');
					return _setStepError('declaration');
				} else {
					var $declaration = $('.contract li select');
					if ($declaration.hasClass('error')) {
						$declaration.removeClass('error');
					}
				}
			}

			if (ln) { //Check validation
				var c = 0, keepGoing = true;
				angular.forEach($scope.data.signatures, function (sign, index) {
					if (keepGoing) {
						if (index != c) {
							nextStep = c;
							keepGoing = false;
						}
						c++;
					}
				});
				if (stepId == 1 && !$scope.showDeclarVal && !$scope.data.signatures[2]) {
					var data_sign = {
						date: '', owner: 'customer', value: ''
					};
					$scope.data.signatures[2] = data_sign;
				}
				if (nextStep != stepId) {
					if (!contract_page.lessInitialContract && stepId != $scope.maxSignCount - 1 && !$scope.requestType) {
						if (nextStep == $scope.stepsData.startTimeStep || nextStep == $scope.stepsData.stopTimeStep) {
							$scope.navigation.activeCrew = $scope.data.crews.length - 1;
						}
						return _setStepError(nextStep);
					}
				} else {
					var $sbox = $('.signature-box');
					if ($sbox.hasClass('error')) {
						$sbox.removeClass('error');
					}
				}
			} else if (!ln && stepId != 0) {
				return _setStepError(nextStep);
			}

			if (!$scope.flatMiles && $scope.request.service_type.raw != 5) {
				if (stepId == $scope.stepsData.startTimeStep) {
					var rightTime = startTimer();
					if (rightTime == false)
						return false;
				}
				if (stepId == $scope.stepsData.stopTimeStep) {
					var rightTime = stopTimer();
					if (rightTime == false)
						return false;
				}
			} else if ($scope.flatMiles && $scope.request.service_type.raw != 5) {
				if (stepId == $scope.flatStepsData.pickupStartTimeStep) {
					var rightTime = pickupTimer('start');
					if (rightTime == false)
						return false;
				}
				if (stepId == $scope.flatStepsData.pickupStopTimeStep) {
					var rightTime = pickupTimer('end');
					if (rightTime == false)
						return false;
				}
				if (stepId == $scope.flatStepsData.unloadStartTimeStep) {
					var rightTime = unloadTimer('start');
					if (rightTime == false)
						return false;
				}
				if (stepId == $scope.flatStepsData.unloadStopTimeStep) {
					var rightTime = unloadTimer('end');
					if (rightTime == false)
						return false;
				}
			}

			var signature = {
				owner: $scope.data.currentOwner, value: SIGNATURE_PREFIX + signatureValue, date: moment().format('x')
			};
			if ($scope.prevSignature) {
				signature.owner = $scope.data.signatures[stepId - 1].owner;
				signature.value = $scope.data.signatures[stepId - 1].value;
			}

			$scope.data.signatures[stepId] = signature;
			$scope.data.signatures.length += 1;
			nextStep = $scope.data.signatures.length;
			if (stepId == 1 && !$scope.showDeclarVal) {
				$scope.data.signatures.length += 1;
				++stepId;
				ln = $scope.data.signatures.length;
				nextStep = ln;
				$scope.data.currentStep = stepId;
			}
			ContractFactory.clearSignaturePad();
			if (((nextStep == 3 && !$scope.showDeclarVal) || nextStep == 2) && contract_page.lessInitialContract && !$scope.requestType) {
				var arr = {};
				if (!$scope.flatMiles) {
					$scope.stepsData.declar = 2;
					arr = $scope.stepsData;
				} else {
					$scope.flatStepsData.declar = 2;
					arr = $scope.flatStepsData;
				}
				nextStep = setLessSignatures(arr);
				if ($scope.stepsData.declar) delete $scope.stepsData.declar;
				if ($scope.flatStepsData.declar) delete $scope.flatStepsData.declar;
			} else if (contract_page.lessInitialContract && $scope.requestType) {
				nextStep = checkLessSignatures(nextStep);
			}
			var msgToLog = '';
			msgToLog += signature.owner.replace(/\w\S*/g, function (txt) {
					return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
				}) + ' sign step #' + stepId;
			ContractService.addForemanLog(msgToLog, 'Contract signature', $scope.request.nid, 'MOVEREQUEST');
			sendSignatureNotification(stepId);

			//Save in local storage for keep away other issues
			var pendingSignatures = JSON.parse(localStorage['pendingSignatures']);
			pendingSignatures[stepId] = signature;

			try {
				localStorage['pendingSignatures'] = JSON.stringify(pendingSignatures);
			} catch (error) {
				let errorStorageLog = _.isObject(error) ? angular.toJson(error): error;

				Raven.captureException(errorStorageLog);
			}

			saveContract({
				callback: function () {
					localStorage['pendingSignatures'] = JSON.stringify({});
				}
			}).finally(() => $scope.busy = false);
			$scope.prevSignature = false;
			$('#signaturePad').modal('hide');
		}
	}

	function isCanvasBlank(elementID) {
		var canvas = document.getElementById(elementID);
		var blank = document.createElement('canvas');
		blank.width = canvas.width;
		blank.height = canvas.height;
		var context = canvas.getContext('2d');
		var perc = 0, area = canvas.width * canvas.height;
		if (canvas.toDataURL() == blank.toDataURL()) {
			return canvas.toDataURL() == blank.toDataURL();
		} else {
			var data = context.getImageData(0, 0, canvas.width, canvas.height).data;
			for (var ct = 0, i = 3, len = data.length; i < len; i += 4) if (data[i] > 50) ct++;
			perc = parseFloat((HUNDRED_PERCENT * ct / area).toFixed(2));
			return (perc < 0.1);
		}
	}

	function sendSignatureNotification(stepId, removed) {
		let data = {
			notification: {
				text: ''
			}, nid: $scope.request.nid
		};
		let type = enums.notification_types.CONTRACT_ACTIONS;
		if (!$scope.requestType) {
			if (stepId < 3) {
				data.notification.text = signatureLabelsLocalMove[stepId];
			} else if (stepId >= $scope.maxSignCount - 2) {
				let labelInd = (stepId == $scope.maxSignCount - 2) ? signatureLabelsLocalMove.length - 2 : signatureLabelsLocalMove.length - 1;
				data.notification.text = signatureLabelsLocalMove[labelInd];
			} else if (stepId >= 3 && stepId < $scope.maxSignCount - 2) {
				if (stepId % 2 == 1) {
					data.notification.text = signatureLabelsLocalMove[3] + ' start';
				} else {
					data.notification.text = signatureLabelsLocalMove[3] + ' end';
				}
				data.notification.text += ' labor time crew #' + ($scope.navigation.activeCrew + 1);
			}
		} else {
			if ($scope.request.field_flat_rate_local_move.value) {
				if (stepId < $scope.maxSignCount - 4) {
					data.notification.text = signatureLabelsFlatRate[stepId];
				} else if (stepId >= $scope.maxSignCount - 4) {
					let fRLabelInd = (stepId == $scope.maxSignCount - 4) ? signatureLabelsLocalMove.length - 2 : signatureLabelsLocalMove.length - 1;
					data.notification.text = signatureLabelsLocalMove[fRLabelInd];
				}
			} else {
				data.notification.text = signatureLabelsFlatRate[stepId];
			}
		}
		if (removed) {
			let removeSignature = 'Signature "' + data.notification.text + '" removed';
			data.notification.text = removeSignature;
		}
		let dataNotify = {
			type: type, data: data, entity_id: $scope.request.nid, entity_type: MOVEREQUEST
		};
		apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
		contractDispatchLogs.assignLogs($scope.request, {signatureNumber: stepId, signatureRemoved: removed});
	}

	function sendSubmitNotification(type) {
		let data = {
			notification: {
				text: 'Contract was submitted'
			}, nid: $scope.request.nid
		};
		let typeEnum = enums.notification_types.FORMENT_CLOSE_CONTRACT;
		if (type) {
			data.notification.text = 'Contract ' + type + ' was submitted';
		}
		let dataNotify = {
			type: typeEnum, data: data, entity_id: $scope.request.nid, entity_type: MOVEREQUEST
		};
		apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
	}

	function startTimer() {
		if ($scope.requestType) return false;

		var active = $scope.navigation.activeCrew;
		var dateRaw = $('#edit-start-time-' + active).val().split(' ');
		var dateTime = dateRaw[0].split(':');
		if (!dateRaw[1]) return false;
		var dateAmPm = dateRaw[1].toLowerCase();
		// Convert 12h to 24h...
		if (dateAmPm == 'pm') {
			if (parseInt(dateTime[0]) < 12) dateTime[0] = (parseInt(dateTime[0]) + 12).toString();
		} else {
			if (parseInt(dateTime[0]) == 12) dateTime[0] = 0;
		}

		var selectedTime = new Date();
		selectedTime.setHours(dateTime[0]);
		selectedTime.setMinutes(dateTime[1]);
		selectedTime.setSeconds(0);
		var now = new Date();
		var diffMs = Math.abs(selectedTime - now);
		var minTime = Math.round(diffMs / 60000);

		$scope.data.crews[active].timer.lastAction = 'start';
		$scope.data.crews[active].timer.start = $('#edit-start-time-' + active).val();

		ContractCalc.calculateWork($scope.data.crews[active]);

		saveContract({
			errorCb: function () {
			}
		});
	}

	function stopTimer() {
		if ($scope.requestType) return false;

		var active = $scope.navigation.activeCrew;
		var prevAction = _.clone($scope.data.crews[active].timer.lastAction);
		var time = $('#edit-end-time-' + active).val();
		$scope.data.crews[active].timer.lastAction = 'stop';
		$scope.data.crews[active].timer.stop = time;

		ContractCalc.calculateWork($scope.data.crews[active]);

		$scope.saveContract({
			errorCb: function (rollback) {

			}
		});
	}

	function addCrew() {
		if (!$scope.request_data.crews.additionalCrews.length) {
			return false;
		}
		let i = $scope.data.crews.push(angular.copy(ContractValue.crewObj));
		$scope.data.stepSpace += 2;
		$scope.maxSignCount += 2;
		$scope.stepsData.startTimeStep += 2;
		$scope.stepsData.stopTimeStep += 2;
		if ($scope.flatMiles == true) {
			$scope.data.stepSpace += 2;
			$scope.maxSignCount += 2;
		}

		var index = i - 2; //into request_data.additional Crews, base crew not exist
		var rate = $scope.request_data.crews.additionalCrews[index].rate;
		if (!rate) {
			var totalWorkers = (function () {
				var obj = {
					helpers: 0, trucks: 0
				};
				var ii = index;
				while (ii >= 0) {
					obj.helpers += $scope.request_data.crews.additionalCrews[ii].helpers.length;
					obj.trucks += $scope.request_data.crews.additionalCrews[ii].trucks;
					ii--;
				}
				obj.helpers += $scope.request_data.crews.baseCrew.helpers.length + 1;//1 = foreman
				return obj;
			})();
			rate = CalculatorServices.getRateAdditionalHelper($scope.request.date.raw * 1000, $scope.request_data.crews.additionalCrews[index]);
		}

		$scope.data.crews[i - 1].rate = rate;
	}

	function calculateTotalBalance(type) {
		var total = 0;
		$scope.total_min = 0;
		$scope.total_max = 0;
		if (angular.isDefined($scope.request)) {
			if (angular.isUndefined($scope.request.request_all_data.add_money_discount)) {
				$scope.request.request_all_data.add_money_discount = 0;
			}
			if (angular.isUndefined($scope.request.request_all_data.add_percent_discount)) {
				$scope.request.request_all_data.add_percent_discount = 0;
			}

			if (type == 'min') {
				total = $scope.request.quote.min + parseFloat(calculateAddServices()) - $scope.request.request_all_data.add_money_discount + ContractCalc.finance.packingSum;

			} else if (type == 'max') {
				total = $scope.request.quote.max + parseFloat(calculateAddServices()) - $scope.request.request_all_data.add_money_discount + ContractCalc.finance.packingSum;
			}
			if ($scope.request.request_all_data.valuation)
				if ($scope.request.request_all_data.valuation.selected.valuation_charge) total = total + Number($scope.request.request_all_data.valuation.selected.valuation_charge);

			if ($scope.request.request_all_data.surcharge_fuel) total += $scope.request.request_all_data.surcharge_fuel;
		}

		return total;
	}


	function GeneratePayrollReq(params) {

		var self = this;
		var workDuration = $scope.calculation.getCrewTotalDuration(ContractFactory.factory.laborOnly, !$scope.mainContract);
		var min_hour_rate = false;
		let time = $scope.isDoubleDriveTime ? $scope.travelTime.raw : ($scope.travelTimeSetting ? $scope.travelTime.raw : 0);

		if (workDuration < MIN_HOUR_RATE) {
			workDuration = MIN_HOUR_RATE;
			min_hour_rate = true;
		}
		var totalCost = ContractCalc.finance.totalCost;
		var workTimesForSwitchers = params.workTimesForSwitchers;

		var dividedFee = 0;
		if ($scope.request_data.crews && $scope.request_data.crews.baseCrew) {
			if (!!$scope.request_data.crews.baseCrew.unpaidHelpers && $scope.request_data.crews.baseCrew.unpaidHelpers.length) {
				dividedFee = +(workDuration * ($scope.request_data.crews.baseCrew.helpers.length - ($scope.request_data.crews.baseCrew.unpaidHelpers || []).length + 1)).toFixed(2);
				dividedFee = +(dividedFee / ($scope.request_data.crews.baseCrew.helpers.length + 1)).toFixed(2);
				dividedFee = MIN_PAYROLL_HOURS > dividedFee ? MIN_PAYROLL_HOURS : dividedFee;
			}
		}
		workDuration = MIN_PAYROLL_HOURS > workDuration ? MIN_PAYROLL_HOURS : workDuration;

		self.isFlatFee = $scope.request_data.paymentType == 'FlatFee';

		if (self.isFlatFee) {
			var pricePerHelper = $scope.request_data.pricePerHelper;
			var flatFeeOpt = $scope.request_data.selectedFlatFeeOption.baseCrew;

			if (dividedFee) {
				switch (flatFeeOpt) {
					case 'money':
						dividedFee = (totalCost / ($scope.request_data.crews.baseCrew.helpers.length + 1)).toFixed(2);
						break;
					case 'percent':
						break;
				}
			}
		}

		var payrollReq = {
			materials: 0,
			extra_service_total: 0,
			tips_total: 0,
			moneyDiscount: 0,
			discount: 0,
			discountType: '',
			grand_total: 0,
			hours: 0,
			laborHours: 0,
			progress: 0,
			foreman: {},
			helper: {},
			fuel: 0,
			valuation: 0,
			min_hour_rate: min_hour_rate,
			estimate_total: parseFloat(((parseFloat(calculateTotalBalance('min')) + parseFloat(calculateTotalBalance('max'))) / 2).toFixed(2))
		};
		var isLaberOnly = $scope.request_data.paymentType == 'Labor';

		function getTotalForPayroll(type) {
			let total = ContractCalc.calculateTotalWithoutTaxDiscount();
			let multiplier = 1;
			let cashDiscountAmount = 0;
			let managerTotal = 0;
			let managerTotalMultiplier = 1;

			if (!type || type == 'addContract') {
				cashDiscountAmount = $scope.calcData.cashDiscount;
			}
			if (type == 'pickup') {
				multiplier = $scope.request.field_flat_rate_local_move.value ? 1 : PICK_UP_PERCENT / HUNDRED_PERCENT;
				cashDiscountAmount = $scope.calcData.cashDiscountPickup;
			}
			if (type == 'delivery') {
				multiplier = (HUNDRED_PERCENT - PICK_UP_PERCENT) / HUNDRED_PERCENT;
				cashDiscountAmount = $scope.calcData.cashDiscount - $scope.calcData.cashDiscountPickup;
			}

			let isCashChargeOnPickup = cashDiscountAmount > 0 && contract_page.paymentTax.cashCharge.state && type == 'pickup' && !$scope.request.field_flat_rate_local_move.value;

			if (isCashChargeOnPickup) {
				managerTotalMultiplier -= contract_page.paymentTax.cashCharge.value / HUNDRED_PERCENT;
				managerTotal = _.round(total * managerTotalMultiplier, 2);
			} else if (cashDiscountAmount > 0 && (!type || type == 'addContract' || (type == 'pickup' && $scope.request.field_flat_rate_local_move.value))) {
				managerTotal = _.round(total - cashDiscountAmount, 2);
			} else if (type !== 'delivery') {
				managerTotal = total;
			}

			return {
				multiplier: _.round(multiplier, 2),
				total: _.round(total * multiplier - cashDiscountAmount, 2),
				managerTotal: managerTotal
			};
		}

		function generate(type) {
			let {total, multiplier, managerTotal} = getTotalForPayroll(type);

			let time = $scope.isDoubleDriveTime ? $scope.travelTime.raw : ($scope.travelTimeSetting ? $scope.travelTime.raw : 0);
			let laborHours = parseFloat(workDuration) - parseFloat(time);
			if ($scope.calcData.paymentType == 'LaborAndTravel') {
				laborHours = parseFloat(workDuration);
			}
			payrollReq.materials = _.round(ContractCalc.finance.packingSum * multiplier, 2);
			payrollReq.extra_service_total = _.round(ContractCalc.finance.serviceSum * multiplier, 2);
			payrollReq.tips_total = (type == 'pickup') ? _.round(ContractCalc.finance.pickupTips, 2) : _.round((ContractCalc.finance.tip - ContractCalc.finance.pickupTips), 2);
			payrollReq.moneyDiscount = _.round(ContractCalc.finance.moneyDiscount * multiplier, 2);
			payrollReq.discount = _.round(ContractCalc.finance.discount * multiplier, 2);
			payrollReq.discountType = $scope.data.discountType;
			payrollReq.grand_total = _.round(total, 2);
			payrollReq.hours = $scope.calculation.getCrewTotalDuration(ContractFactory.factory.laborOnly, !$scope.mainContract);
			payrollReq.laborHours = laborHours;
			payrollReq.fuel = _.round(ContractCalc.finance.fuel * multiplier, 2);
			payrollReq.valuation = _.round(ContractCalc.finance.valuation * multiplier, 2);
			payrollReq.progress = 0;

			if (!type || type == 'pickup' || type == 'addContract') {
				payrollReq.manager_total = managerTotal;
			}

			if (ContractFactory.factory.laborOnly) {
				time = 0;
			}

			let payroll = CalculatorServices.generateWorkerPayroll($scope.request_data, $scope.switchers, workTimesForSwitchers, workDuration, dividedFee, payrollReq.grand_total, time, type, $scope.data);

			payrollReq.foreman = payroll.foreman;
			payrollReq.helper = payroll.helper;

			return payrollReq;
		}

		return {
			generate: generate
		};
	}

	function saveContractTips() {
		let allData = {
			factory: _.clone($scope.data), finance: _.clone($scope.calcData)
		};
		ContractService.saveContract(allData, nid)
			.then(function () {
				$scope.busy = false;
			}, function (err) {
				$scope.busy = false;
				saveToLocalStorage();
			});
	}

	function saveContract(params) {
		var defer = $q.defer();

		if ($scope.calcData.discount) {
			$scope.calcData.discount = parseFloat($scope.calcData.discount);
		}
		saveStorageFit();

		if (!$scope.lastPageSubmit) {
			$scope.lastPageSubmit = _.get(params, 'lastPageSubmit', false);
		}

		let submitCheck = !$scope.data.isFirstSubmittedPage && _.isEmpty($scope.data.tabs);

		if(submitCheck && _.get(params, 'additionalPageSubmit', false) || submitCheck && $scope.lastPageSubmit) {
			$scope.data.tabs = $scope.tabs;
			$scope.data.isFirstSubmittedPage = true;
			$scope.data.isFirstAddMainId = $scope.mainContractInd;
		}

		var allData = {
			factory: _.clone($scope.data), finance: _.clone($scope.calcData)
		};

		var workTimesForSwitchers = null;
		var isBtn = _.get(params, 'isBtn', false);
		var addContract = _.get(params, 'addContract', false);
		var pickUpClose = _.get(params, 'pickup', false);
		var deliveryClose = _.get(params, 'delivery', false);
		var isSwitcher = !_.isEmpty($scope.switchers);
		var workDuration = ContractCalc.finance.workDuration;
		var isFlatFee = $scope.request_data.paymentType == 'FlatFee';
		let time = $scope.isDoubleDriveTime ? $scope.travelTime.raw : ($scope.travelTimeSetting ? $scope.travelTime.raw : 0);
		let missModalClose = _.get(params, 'missModalClose', false);
		let notFlatRateLongDistance = $scope.request.service_type.raw != 5 && $scope.request.service_type.raw != 7;
		let closePickUpDeliveryOpen = pickUpClose && !deliveryClose && $scope.request.field_flat_rate_local_move.value;
		let additionalFlatRate = _.get(params, 'additionalFlatRate' , false);
		let longDistance = _.get(params, 'longDistance' , false);

		checkLessSignatures(0, pickUpClose, deliveryClose);

		if (ContractFactory.factory.laborOnly) {
			workDuration -= _.round(time, accountConstant.roundUp);
		}

		if (workDuration < MIN_HOUR_RATE) {
			workDuration = MIN_HOUR_RATE;
		}

		calculateMaxCountSign();
		let signaturesLength = ($scope.data.signatures.length == $scope.maxSignCount);
		if (pickUpClose && !deliveryClose) signaturesLength = ($scope.data.signatures.length == $scope.maxSignCount - 2);
		if (isBtn && (signaturesLength || addContract) || additionalFlatRate || longDistance) {
			var request_data = $scope.request_data;
			var dividedHours = 0;
			let addFlatMainNotLocal = additionalFlatRate && !$scope.request.field_flat_rate_local_move.value;

			if (request_data.crews && request_data.crews.baseCrew) {
				if (!!request_data.crews.baseCrew.unpaidHelpers && request_data.crews.baseCrew.unpaidHelpers.length) {
					dividedHours = (workDuration / (request_data.crews.baseCrew.helpers.length + 1)).toFixed(2);
				}
			}
			(() => {
				if (isSwitcher && !isFlatFee) {
					var modalInstance = $uibModal.open({
						templateUrl: 'app/contractpage/templates/switcherModal.html',
						size: 'md',
						controller: switcherModalCtrl,
						resolve: {
							data: () => {
								return {
									switchers: $scope.switchers,
									workers: workers,
									request: $scope.request,
									workDuration: dividedHours || workDuration
								};
							}
						}
					});
					return modalInstance.result;
				} else {
					return $q.resolve();
				}
			})().then((workTimes) => {
				workTimesForSwitchers = workTimes;
				$scope.busy = true;
				if (addFlatMainNotLocal) {
					pickUpClose = true;
				}
				return generatePayrollRequest(pickUpClose, deliveryClose, addContract);
			})
			.then(() => postprocessingPayrollRequest())
			.then(() => {
				if (addFlatMainNotLocal) {
					$scope.busy = true;
					deliveryClose = true;
					return generatePayrollRequest(pickUpClose, deliveryClose, addContract);
				} else {
					return Promise.resolve();
				}
			})
			.then(() => {
				if (addFlatMainNotLocal) {
					return postprocessingPayrollRequest()
				} else {
					return Promise.resolve();
				}
			})
			.then(() => SweetAlert.swal('Success!', 'Successfully submitted', 'success'))
			.catch(() => SweetAlert.swal('Failed!', 'Error of submitting contract', 'error'))
			.finally(() => {
				$scope.busy = false;
			});

			function generatePayrollRequest(pickUpClose, deliveryClose, addContract) {
				$scope.busy = true;
				let contractPage = $('#contract-page');
				$('html, body').animate({
					scrollTop: contractPage.offset().top
				}, 100);
				var workerClass = new GeneratePayrollReq({
					workTimesForSwitchers: workTimesForSwitchers
				});
				let flag = '';
				if (pickUpClose && !deliveryClose) flag = 'pickup'; else if (deliveryClose) flag = 'delivery'; else if (addContract) flag = 'addContract';
				var payrollReq = workerClass.generate(flag);

				let contractType = contractTypes.PAYROLL;
				if (pickUpClose && !deliveryClose && !$scope.request.field_flat_rate_local_move.value) contractType = contractTypes.PICKUP_PAYROLL;
				if (deliveryClose) contractType = contractTypes.DELIVERY_PAYROLL;

				let field_flags = [];
				if (pickUpClose && !deliveryClose && !$scope.request.field_flat_rate_local_move.value) {
					field_flags = field_flags.concat(importedFlags.company_flags[pickupJobCompleted], importedFlags.company_flags[pickupPayrollDone]);
				} else if (deliveryClose) {
					field_flags = field_flags.concat(importedFlags.company_flags[pickupJobCompleted], importedFlags.company_flags[pickupPayrollDone], importedFlags.company_flags[deliveryJobCompleted], importedFlags.company_flags[deliveryPayrollDone], importedFlags.company_flags[Job_Completed]);
				} else {
					field_flags = field_flags.concat(importedFlags.company_flags[Job_Completed], importedFlags.company_flags[Payroll_Done], importedFlags.company_flags[deliveryJobCompleted]);
				}

				if (field_flags.indexOf('1') != -1 && field_flags.indexOf('5') != -1 && field_flags.indexOf('6') != -1 && field_flags.indexOf('7') != -1 && field_flags.indexOf('8') != -1) {
					field_flags.push('3');
				}

				let allowCreatePdfSubmit = !addContract && ((notFlatRateLongDistance && isBtn && signaturesLength) || deliveryClose || closePickUpDeliveryOpen);

				return saveReqDataInvoice()
					.then(() => {
						_.forEach(field_flags, function (item) {
							$scope.request.field_company_flags.value[item] = _.invert(importedFlags.company_flags)[item];
						});
						if (localStorage[nid]) {
							getFromLocalStorage();
							saveDataContract();
						}
						if (!addContract) {
							if (allowCreatePdfSubmit) {
								$scope.data.isSubmitted = true;
							}
							if (pickUpClose && !deliveryClose && !$scope.request.field_flat_rate_local_move.value) {
								$scope.data.isPickupSubmitted = true;
								contractTouring();
							}
						}

						if (allowCreatePdfSubmit) {
							return createContractPdf();
						}
					})
					.then(() => ContractService.sendPayroll(nid, payrollReq, contractType))
					.then(() => RequestServices.updateRequest(nid, {field_company_flags: field_flags}))
					.catch((err) => {
						saveToLocalStorage();
						logger.error(err || 'error', null, 'Something going wrong with contract document');
					})
					.finally(() => $scope.busy = false);
			}

			function postprocessingPayrollRequest() {
				$scope.busy = true;
				if ((notFlatRateLongDistance) || deliveryClose || (closePickUpDeliveryOpen)) {
					if (deliveryClose) sendSubmitNotification('delivery'); else
						sendSubmitNotification();
					$scope.data.isSubmitted = true;
				}
				if (pickUpClose && !deliveryClose && !$scope.request.field_flat_rate_local_move.value) {
					sendSubmitNotification('pickup');
					$scope.data.isPickupSubmitted = true;
					$scope.busy = false;
				}
				var saveData = {
					factory: _.clone($scope.data), finance: _.clone($scope.calcData)
				};

				if ($scope.lastPageSubmit) {
					saveData.factory.indexOfLastAdditionalPage = $scope.initCountAdditionalPage;
				}
				return ContractService.saveContract(saveData, nid);
			}
		}

		if (!params || !params.data) {
			contractTouring();
		}

		if ($scope.lastPageSubmit) {
			allData.factory.indexOfLastAdditionalPage = $scope.initCountAdditionalPage;
		}

		ContractService.saveContract(allData, nid)
			.then(() => {
				if (!missModalClose) {
					arrivyService.sendStatus(allData, nid, $scope.flatMiles);
					$rootScope.$broadcast('close.modal.service');
				}
				if (localStorage[nid]) {
					return saveDataRequest();
				}
			})
			.then(() => {
				if (_.get(params, 'callback', false)) {
					params.callback();
				}
				defer.resolve();
			})
			.catch((err) => {
				saveToLocalStorage();
				logger.error('Can\'t store time', 'Something going wrong', 'error');
				if (notFlatRateLongDistance) {
					$scope.data.isSubmitted = false;
				}
				if (_.get(params, 'errorCb', false)) {
					!!params.errorCb && params.errorCb();
				}
				defer.reject();
			});

		return defer.promise;
	}

	function _resetContract() {
		ContractService.saveContract({}, nid)
			.then(function (success) {
				if (localStorage[nid]) {
					saveDataRequest();
				}
				localStorage['pendingSignatures'] = JSON.stringify({});
			}, function () {
				saveToLocalStorage();
				logger.error('Error', 'Something going wrong', 'Error');
			})
			.finally(function () {
				$scope.busy = false;
			});

		RequestServices.updateRequest(nid, {
			field_company_flags: []
		});
		return false;
	}

	function switcherModalCtrl($scope, $uibModalInstance, data) {

		$scope.workers = [];
		$scope.workDuration = data.workDuration;
		$scope.workTimes = {};

		//**** @Constructor
		angular.forEach(data.switchers, function (to, from) {
			var obj = {from: {}, to: {}};

			angular.forEach(data.workers, (roles) => {
				angular.forEach(roles, (worker, uid) => {
					if (uid == from) {
						obj.from = worker;
					}

					if (uid == to) {
						obj.to = worker;
					}

					if (!_.isEmpty(obj.from) && !_.isEmpty(obj.to)) {
						$scope.workers.push(_.clone(obj));
						obj.from = {};
						obj.to = {};
					}
				});
			});
		});

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};

		$scope.saveWorkHours = function () {
			$uibModalInstance.close($scope.workTimes);
		};
	}

	function applyPayment(step, data_receipt) {
		if (step === true || $scope.isFamiliarization ||
			$scope.isPrint || $scope.data.isSubmitted) {
			return false;
		}

		if (contract_page.lessInitialContract && !isSubmittedInventory()) {
			return false;
		}

		///fix for dialog window delay
		if ($scope.pay_clicked) return false;
		$scope.pay_clicked = true;
		$scope.openPay = true;

		var btnPay = $('.checkout_btn');
		if (btnPay.hasClass('error')) {
			btnPay.removeClass('error');
		}
		var btnPayArrow = $('#errorSign_checkout_btn');
		if (btnPayArrow.hasClass('errorArrow')) {
			btnPayArrow.removeClass('errorArrow');
		}
		$scope.request.reservation = false;
		$scope.request.payment_flag = 'contract';

		if (!$scope.requestType) {
			$scope.request.total_amount = totalLessDeposit();
			$scope.request.total_amount_tax = parseFloat($scope.calculation.totalCost().toFixed(2)) - $scope.calcData.creditTax + $scope.calcData.cashDiscount;
			$scope.request.total_for_tip = parseFloat(($scope.request.total_amount_tax - $scope.calcData.tip).toFixed(2));
			if (!$scope.calcData.requestTipsPaid) {
				$scope.request.requestTips = $scope.calcData.requestTips;
				$scope.request.requestTipsPaid = $scope.calcData.requestTipsPaid;
				$scope.request.total_amount -= $scope.request.requestTips;
				$scope.request.total_amount_tax -= $scope.request.requestTips;
			}
		} else {
			let tips = $scope.calcData.tip;
			$scope.request.total_amount = totalLessDeposit();
			let totalForTip = parseFloat($scope.calculation.totalFlatRateCost().toFixed(2)) - $scope.calcData.creditTax + $scope.calcData.cashDiscount;
			if ($scope.request.service_type.raw == 5) {
				if (!$scope.data.isPickupSubmitted && !$scope.request.field_flat_rate_local_move.value) {
					tips = $scope.calcData.pickupTips;
				}
				if ($scope.data.isPickupSubmitted) {
					let pickupTotal = ContractCalc.totalFlatRateCost() - $scope.calcData.pickupTips - $scope.calcData.creditTaxPickup + $scope.calcData.cashDiscountPickup;
					let deliveryTotal = ContractCalc.totalFlatRateCost(true) - $scope.calcData.tip - $scope.calcData.creditTax + $scope.calcData.cashDiscount;
					totalForTip = parseFloat((deliveryTotal - pickupTotal).toFixed(2));
					tips = 0;
				}
			}
			$scope.request.total_amount_tax = totalForTip;
			$scope.request.total_for_tip = parseFloat(($scope.request.total_amount_tax - tips).toFixed(2));
			if (!$scope.calcData.requestTipsPickupPaid && !$scope.data.isPickupSubmitted && !$scope.request.field_flat_rate_local_move.value) {
				$scope.request.requestTips = $scope.calcData.requestTipsPickup;
				$scope.request.requestTipsPaid = $scope.calcData.requestTipsPickupPaid;
				$scope.request.total_amount -= $scope.request.requestTips;
				$scope.request.total_amount_tax -= $scope.request.requestTips;
			}
			if (!$scope.calcData.requestTipsPaid && ($scope.data.isPickupSubmitted || $scope.request.field_flat_rate_local_move.value)) {
				$scope.request.requestTips = $scope.calcData.requestTips;
				$scope.request.requestTipsPaid = $scope.calcData.requestTipsPaid;
				if ($scope.data.isPickupSubmitted) {
					$scope.request.requestTips -= $scope.calcData.requestTipsPickup;
				}
				$scope.request.total_amount -= $scope.request.requestTips;
				$scope.request.total_amount_tax -= $scope.request.requestTips;
			}
		}
		PaymentServices.openAuthPaymentModal($scope.request, step, data_receipt, MOVEREQUEST, $scope);
	}

	function showPayment() {
		if ($scope.isFamiliarization || $scope.isPrint || $scope.data.isSubmitted) return;
		PaymentServices.openContractPaymentModal($scope.request);
	}

	$scope.canAddCrew = function (index) {
		if ($scope.request && $scope.request_data.crews) {
			return index == 0 && !$scope.data.isSubmitted && (_.last($scope.data.crews)).timer.start && (_.last($scope.data.crews)).timer.stop && $scope.request_data.crews.additionalCrews.length >= $scope.data.crews.length;
		}

	};

	$scope.getMovers = function (index) {
		if (!(Object.keys($scope.request_data) ).length || !$scope.request_data.crews) return 0;

		index = angular.isUndefined(index) ? $scope.data.crews.length - 1 : index;
		var total = $scope.request_data.crews.baseCrew.helpers.length + 1;
		while (index) {
			total += $scope.request_data.crews.additionalCrews[index - 1].helpers.length;
			index--;
		}
		return total;
	};

	$scope.setService = function (serviceAlias) {
		if (!!serviceAlias) {
			if ($scope.data.storageFit[serviceAlias].length == 2) { //Completed Size
				if ($scope.services.hasOwnProperty(serviceAlias)) {
					$scope.services[serviceAlias] && $scope.$broadcast('event.addService', serviceAlias);
				}
			}
		}
		saveContract();
	};

	function submitContractFlat() {
		return ((ContractCalc.totalFlatRateCost($scope.data.isPickupSubmitted) - $scope.calcData.deposit - $scope.balance_paid) != 0);
	}

	function removeSignature() {
		$scope.request.request_all_data.signature.value = '';
		request.request_all_data.signature.date = '';
		RequestServices.saveRequestDataContract($scope.request)
			.then(function () {
				if (localStorage[nid]) {
					saveDataContract();
				}
			}, function (err) {
				saveToLocalStorage();
				logger.error(err || 'error', null, 'Something going wrong with contract document');
			});
	}

	function saveStorageFit() {
		$scope.data.storageServices = _.clone($scope.services);
	}

	function reInitReqContract(request, contract) {
		var request_data = {};
		if (request.request_data.value != '') {
			$scope.request_data = request_data = angular.fromJson(request.request_data.value);
		}
		$scope.request = request;
		baseRequestAllData = angular.copy($scope.request.request_all_data);
		$scope.request = sharedRequestServices.calculateQuote($scope.request);
		setUpisDoubleDriveTime();
		setTravelTime();
		if (angular.isDefined($scope.request.request_all_data.flatMiles)) {
			$scope.flatMiles = $scope.request.request_all_data.flatMiles;
		} else if ($scope.request.distance) {
			if (!!basicSettings.local_flat_miles && parseFloat($scope.request.distance.value) > parseFloat(basicSettings.local_flat_miles)) {
				$scope.flatMiles = true;
				$scope.request.request_all_data.flatMiles = true;
			}
		}
		if (angular.isDefined($scope.request.field_contract_images)) $scope.images = $scope.request.field_contract_images;
		if (angular.isUndefined($scope.request.request_all_data.invoice)) {
			$scope.request.request_all_data.invoice = {
				request_all_data: {
					valuation: {
						valuation_charge: 0
					}
				}
			};
		}
		if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data)) {
			$scope.request.request_all_data.invoice.request_all_data = {};
			$scope.request.request_all_data.invoice.request_all_data.valuation = angular.copy($scope.request.request_all_data.valuation);
		} else if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data.valuation) && angular.isDefined($scope.request.request_all_data.valuation)) {
			$scope.request.request_all_data.invoice.request_all_data.valuation = angular.copy($scope.request.request_all_data.valuation);
		} else if (angular.isUndefined($scope.request.request_all_data.valuation)) {
			$scope.request.request_all_data.invoice.request_all_data.valuation = {
				valuation_charge: 0
			};
		}
		if ((angular.isUndefined($scope.request.request_all_data.invoice) || $scope.request.request_all_data.length == 0) && $scope.request.request_all_data.surcharge_fuel) {
			$scope.request.request_all_data.invoice = {
				request_all_data: {
					surcharge_fuel: parseFloat($scope.request.request_all_data.surcharge_fuel.toFixed(2))
				}
			};
		} else if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data) && angular.isDefined($scope.request.request_all_data.surcharge_fuel)) {
			$scope.request.request_all_data.invoice.request_all_data = {};
			$scope.request.request_all_data.invoice.request_all_data.surcharge_fuel = parseFloat($scope.request.request_all_data.surcharge_fuel.toFixed(2));
		} else if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data.surcharge_fuel) && angular.isDefined($scope.request.request_all_data.surcharge_fuel)) {
			$scope.request.request_all_data.invoice.request_all_data.surcharge_fuel = parseFloat($scope.request.request_all_data.surcharge_fuel.toFixed(2));
		} else if (angular.isUndefined($scope.request.request_all_data.surcharge_fuel)) {
			$scope.request.request_all_data.invoice.request_all_data.surcharge_fuel = 0;
		}
		if (angular.isDefined($scope.request.request_all_data.add_money_discount)) {
			if (angular.isUndefined($scope.request.request_all_data.invoice.request_all_data.add_money_discount) && angular.isDefined($scope.request.request_all_data.add_money_discount)) $scope.request.request_all_data.invoice.request_all_data.add_money_discount = $scope.request.request_all_data.add_money_discount;
			$scope.calcData.moneyDiscount = $scope.request.request_all_data.invoice.request_all_data.add_money_discount;
		}

		setValuation($scope.request);

		$scope.calcData.fuel = $scope.request.request_all_data.invoice.request_all_data.surcharge_fuel;

		setTravelTime();
		$scope.requestType = ($scope.request.service_type.raw == 5 || $scope.request.service_type.raw == 7);
		if (request_data.crews) {
			$scope.switchers = request_data.crews.switchers || null;
		}
		if ($scope.switchers) {
			ContractService.getUsersByRole({  //get all selected users
				active: 1,
				role: ['helper', 'foreman', 'driver']
			})
				.then(({data}) => {
					workers = data;
				});
		}
		ContractCalc.initData({request: request, travelTime: $scope.travelTime});
		if (request_data.crews) {
			$scope.data.crews[0].rate = request.rate.value;
			if (_.get(request.request_all_data, 'invoice.rate.value', false)) {
				$scope.data.crews[0].rate = request.request_all_data.invoice.rate.value;
			}
			if (request.request_all_data.add_rate_discount) $scope.data.crews[0].rate = request.request_all_data.add_rate_discount;
			if ($scope.data.crews.length > 1) {
				for (var i = 1, l = $scope.data.crews.length; i < l; i++) {
					var crew = $scope.data.crews[i];
					crew.rate = CalculatorServices.getRateAdditionalHelper(request.date.raw * 1000, request_data.crews.additionalCrews[i - 1]);
				}
			}
		}
		var currentFlags = _.values($scope.request.field_company_flags.value);
		var isAdmin = (getUserRole().indexOf('administrator') > -1 || getUserRole().indexOf('foreman') > -1);

		checkForeman();

		ContractCalc.finance = contract.finance;
		ContractCalc.factory = contract.factory;
		ContractFactory.initData(contract);
		if (_.get(request.request_all_data, 'invoice.rate.value', false)) {
			$scope.data.crews[0].rate = request.request_all_data.invoice.rate.value;
		}
		if (request.request_all_data.add_rate_discount) $scope.data.crews[0].rate = request.request_all_data.add_rate_discount;
		if (contract.finance) {
			$scope.calcData.tip = contract.finance.tip;
			$scope.calcData.pickupTips = contract.finance.pickupTips;
			if (contract.finance.discount) {
				$scope.calcData.discount = contract.finance.discount;
			}
		}
		if ($scope.request_data.paymentType == 'FlatFee') {
			var total = 0;
			angular.forEach($scope.request_data.pricePerHelper, function (i) {
				total += i;
			});
			ContractCalc.finance.fixedTotal = total;
		}
		$scope.services = {};
		if (_.get(contract, 'factory.releaseData.releaseInitials', false)) {
			if (!$scope.oldVersionOfReleaseForm) {
				$scope.data.releaseData = angular.copy(ContractFactory.oldReleaseFormObj);
				ContractFactory.factory.releaseData = angular.copy(ContractFactory.oldReleaseFormObj);
				ContractFactory.factoryObj.releaseData = angular.copy(ContractFactory.oldReleaseFormObj);
			}
			$scope.data.releaseData.releaseInitials = contract.factory.releaseData.releaseInitials;
			if (contract.factory.releaseData.itemsRelease) $scope.data.releaseData.itemsRelease = contract.factory.releaseData.itemsRelease;
			$scope.oldVersionOfReleaseForm = true;
		} else {
			_.forEach(contract.factory.releaseData, function (item, field) {
				if (item) $scope.data.releaseData[field] = item;
			});
		}
		if (contract.factory.feedBack && !_.isEmpty(contract.factory.feedBack)) $scope.data.feedBack = contract.factory.feedBack;

		$scope.data.isSubmitted = (currentFlags.indexOf('Completed') > -1 && $scope.request.service_type.raw != 5) || ($scope.request.service_type == 5 && currentFlags.indexOf(deliveryJobCompleted) > -1);
		$scope.data.isPickupSubmitted = currentFlags.indexOf(pickupJobCompleted) > -1;

		if ($scope.flatMiles) {
			maxSignCount = 9;
			$scope.maxSignCount = $scope.data.crews.length > 1 ? maxSignCount - 4 + ($scope.data.crews.length * 4) : maxSignCount;
		} else {
			$scope.maxSignCount = $scope.data.crews.length > 1 ? maxSignCount - 2 + ($scope.data.crews.length * 2) : maxSignCount;
		}

		$scope.$broadcast('data_loaded');

	}


	function saveToLocalStorage() {
		resetTravelTime();

		var toLocalStorage = {
			request: $scope.request,
			contract: {
				factory: _.clone($scope.data),
				finance: _.clone($scope.calcData)
			}
		};

		try {
			localStorage[nid] = angular.toJson(toLocalStorage);
		} catch (error) {
			let errorStorageLog = _.isObject(error) ? angular.toJson(error): error;
			Raven.captureException(errorStorageLog);
		}
	}

	function getFromLocalStorage() {
		var data = angular.fromJson(localStorage[nid]);
		$scope.request = _.clone(data.request);
		setUpisDoubleDriveTime();
		$scope.data = _.clone(data.contract.factory);
		$scope.calcData = _.clone(data.contract.finance);
		reInitReqContract(data.request, data.contract);
		var field_flags = _.keys($scope.request.field_company_flags.value) || [];
		var promise1 = RequestServices.updateRequest(nid, {field_company_flags: field_flags});
		var promise2 = ContractService.saveContract(data.contract, nid);
		var promise3 = RequestServices.saveRequestDataContract($scope.request);
		$q.all([promise1, promise2, promise3]).then(function () {
			delete localStorage[nid];
		});
	}

	function sendReceipt(receipt) {
		var data = angular.fromJson(receipt);
		if ($scope.request.nid != data.nid)
			return false;
		PaymentServices.saveCardImage(data.ID, data.images).then(function () {
			$scope.busy = false;
			ContractService.addForemanLog('Image successfully saved.', 'Contract', $scope.request.nid, 'MOVEREQUEST');
			PaymentServices.get_receipt(data.nid, MOVEREQUEST).then(function (receipts) {
				_.forEach(receipts, function (payment, i) {
					if (payment.id == data.ID) {
						receipts[i].payment_flag = 'contract';
						receipts[i].entity_id = $scope.request.nid;
						receipts[i].entity_type = MOVEREQUEST;

						PaymentServices.update_receipt(data.nid, receipts[i], MOVEREQUEST).then(function () {
							delete localStorage['receipt_' + payment.id];
							PaymentServices.get_receipt(data.nid, MOVEREQUEST).then(function (new_receipts) {
								$scope.request.receipts = _.clone(new_receipts);
								$scope.balance_paid = calcPaymentContract(new_receipts);
								ContractCalc.initData({finance: $scope.calcData});
								ContractCalc.initData({request: $scope.request, travelTime: $scope.travelTime});
							});
						});
					}
				});
			});
		}, function (reason) {
			$scope.busy = false;
			ContractService.addForemanLog('Error: ' + reason + '. Receipt id:' + data.ID, 'File saving error', $scope.request.nid, 'MOVEREQUEST');
			SweetAlert.swal('Failed!', reason, 'error');
		});
	}

	function saveDataRequest() {
		var data = angular.fromJson(localStorage[nid]);
		$scope.request = _.clone(data.request);
		setUpisDoubleDriveTime();
		var field_flags = _.keys($scope.request.field_company_flags.value) || [];
		var promise1 = RequestServices.updateRequest(nid, {field_company_flags: field_flags});
		var promise3 = RequestServices.saveRequestDataContract($scope.request);
		$q.all([promise1, promise3]).then(function () {
			delete localStorage[nid];
		});
	}

	function saveDataContract() {
		var data = angular.fromJson(localStorage[nid]);
		ContractService.saveContract(data.contract, nid).then(function () {
			delete localStorage[nid];
		});
	}

	function storagePayment() {
		if (localStorage['save_agreement']) {
			var localObj = angular.fromJson(localStorage['save_agreement']);
			var entitytype = fieldData.enums.entities.MOVEREQUEST;
			var requestDataPayment = angular.copy(localObj.request);
			requestDataPayment.agreement_repeat = true;
			if (requestDataPayment.move_request != $scope.request.nid) return false;
			PaymentServices.openAuthPaymentModal(requestDataPayment, '', '', entitytype);
			$timeout(function () {
				SweetAlert.swal('Storage payment error', 'Please, try again', 'error');
			}, 350);
		}
	}

	$scope.showRental = function (index) {
		return false;
		if (!$scope.request)
			return true;
		if ($scope.services)
			if ($scope.services.monthly_storage_fee)
				return false;
		return true;
	};

	$scope.$on('remove.signature', function (e, idS) {
		sendSignatureNotification(idS, true);
	});

	$scope.$on('close.payment.modal', function () {
		$scope.pay_clicked = false;
	});

	$scope.$on('$destroy', function () {
		for (var i = 0; i < unbindEv.length; i++) {
			unbindEv[i].call();
		}
	});

	$scope.$on('openPay', function () {
		$scope.openPay = false;
	});


	$rootScope.$on('payment.received', function (event, data, request, tips, creditTax, cashDiscount, requestTips, customPayment) {
		if (request.receipts) $scope.request.receipts = _.clone(request.receipts);
		$scope.balance_paid = 0;
		$scope.balance_paid = calcPaymentContract($scope.request.receipts);
		$scope.calcData = ContractCalc.finance;
		if (angular.isUndefined($scope.request.request_all_data.req_tips)) $scope.request.request_all_data.req_tips = 0;
		if (angular.isUndefined($scope.request.request_all_data.creditTax)) $scope.request.request_all_data.creditTax = 0;
		if (angular.isUndefined($scope.request.request_all_data.cashDiscount)) $scope.request.request_all_data.cashDiscount = 0;

		RequestServices.saveRequestDataContract($scope.request)
			.then(function () {
				if (localStorage[nid]) {
					saveDataContract();
				}
			}, function (err) {
				saveToLocalStorage();
				console.log(err);
			});

		if (angular.isDefined(tips)) {
			$scope.calcData.tip += parseFloat(tips.toFixed(2));
			if ($scope.requestType && $scope.request.service_type.raw == 5 && !$scope.data.isPickupSubmitted) {
				if (!$scope.calcData.pickupTips) $scope.calcData.pickupTips = 0;
				$scope.calcData.pickupTips += parseFloat(tips.toFixed(2));
			}
		}
		if ($scope.requestType) {
			if (!$scope.calcData.requestTipsPickupPaid && !$scope.data.isPickupSubmitted && !$scope.request.field_flat_rate_local_move.value && requestTips) {
				$scope.calcData.requestTipsPickupPaid = true;
				$scope.request.requestTipsPaid = true;
			}
			if (!$scope.calcData.requestTipsPaid && ($scope.data.isPickupSubmitted || $scope.request.field_flat_rate_local_move.value) && requestTips) {
				$scope.calcData.requestTipsPaid = true;
				$scope.request.requestTipsPaid = true;
			}
		} else {
			if (!$scope.calcData.requestTipsPaid && requestTips) {
				$scope.calcData.requestTipsPaid = true;
				$scope.request.requestTipsPaid = true;
			}
		}
		if (angular.isDefined(creditTax)) {
			$scope.calcData.creditTax += parseFloat(creditTax.toFixed(2));
			if ($scope.requestType && !$scope.data.isPickupSubmitted) $scope.calcData.creditTaxPickup += parseFloat(creditTax.toFixed(2));
		}
		if (angular.isDefined(cashDiscount)) {
			$scope.calcData.cashDiscount += parseFloat(cashDiscount.toFixed(2));
			if ($scope.requestType && !$scope.data.isPickupSubmitted) $scope.calcData.cashDiscountPickup += parseFloat(cashDiscount.toFixed(2));
		}
		if (customPayment) {
			if (_.get($scope, `calcData.customPayments[${customPayment.name}]`)) {
				$scope.calcData.customPayments[customPayment.name].value += customPayment.value;
			} else {
				if (_.isArray($scope.calcData.customPayments)) {
					$scope.calcData.customPayments = {};
				}
				$scope.calcData.customPayments[customPayment.name] = {
					value: customPayment.value || 0
				};
			}
		}

		$scope.request.request_all_data.req_tips = angular.copy($scope.calcData.tip);
		$scope.request.request_all_data.creditTax = angular.copy($scope.calcData.creditTax);
		$scope.request.request_all_data.cashDiscount = angular.copy($scope.calcData.cashDiscount);
		$scope.request.request_all_data.customPayments = angular.copy($scope.calcData.customPayments);

		ContractCalc.initData({request: $scope.request, travelTime: $scope.travelTime});
		$scope.saveContractTips();
		$scope.payment_received = true;
		contractTouring();
	});

	$rootScope.$on('sever_error.payment.received', function (event, charge_value, selectedTips, creditTax, cashDiscount) {
		$scope.balance_paid += parseFloat((Number(charge_value)).toFixed(2));

		if (angular.isDefined(selectedTips)) $scope.calcData.tip += parseFloat((Number(selectedTips)).toFixed(2));
		if (angular.isDefined(creditTax)) {
			$scope.calcData.creditTax += parseFloat((Number(creditTax)).toFixed(2));
			if ($scope.requestType && !$scope.data.isPickupSubmitted) $scope.calcData.creditTaxPickup += parseFloat((Number(creditTax)).toFixed(2));
		}
		if (angular.isDefined(cashDiscount)) {
			$scope.calcData.cashDiscount += parseFloat((Number(cashDiscount)).toFixed(2));
			if ($scope.requestType && !$scope.data.isPickupSubmitted) $scope.calcData.cashDiscountPickup += parseFloat((Number(cashDiscount)).toFixed(2));
		}

		ContractCalc.initData({request: $scope.request, travelTime: $scope.travelTime});
		$scope.saveContractTips();
		contractTouring();
	});

	$scope.$on('receipt.rental', function (event, data) {
		if (!$scope.request.receipts) $scope.request.receipts = [];
		$scope.request.receipts.push(data);
		$scope.balance_paid = calcPaymentContract($scope.request.receipts);
	});

	$scope.$on('coupon.used', function (event, data) {
		if (data) {
			$scope.data.discountType = 'website';
			$scope.data.discountUsed = true;
			$scope.calcData.discount = parseFloat(data.discount);
			$scope.saveContract();
			$scope.request.request_all_data.couponData = {};
			$scope.request.request_all_data.couponData.discount = data.discount;
			$scope.request.request_all_data.couponData.discountType = 'website';
			$scope.request.request_all_data.couponData.discountId = data.id;
		} else {
			$scope.data.discountType = '';
			$scope.data.discountUsed = false;
			$scope.calcData.discount = 0;
			$scope.saveContract();
			delete $scope.request.request_all_data.couponData;
		}

		RequestServices.saveRequestDataContract($scope.request);
	});

	$scope.$on('storage.request.created', function (ev, data) {
		if (!$scope.request.request_all_data) $scope.request.request_all_data = {};
		$scope.request.request_all_data.storage_request_id = data;
		$scope.busy = false;
		$rootScope.$broadcast('refresh.agreement');
		RequestServices.saveRequestDataContract($scope.request);
	});

	$scope.$on('rental.finished', function () {
		$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Bill of lading');
	});

	$scope.$on('inventory.submit', function () {
		if (!$scope.data.agreement.isSubmitted && !$scope.request.request_all_data.toStorage && !hideRentalTab()) {
			$scope.data.openRental = true;
			$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Rental agreement');
			$scope.saveContract();
		}
	});

	$scope.$on('add.inventory.added.moving', function (e, data) {
		var access = angular.isDefined($scope.request.request_all_data.useInventory) ? $scope.request.request_all_data.useInventory : contract_page.useInventory;
		if (access) {
			$scope.request.request_all_data.inventory = data.inventory || [];
			loadInventory($scope.request);
		}
	});

	$scope.$on('inventory.reset', function () {
		var access = angular.isDefined($scope.request.request_all_data.useInventory) ? $scope.request.request_all_data.useInventory : contract_page.useInventory;
		if (access) loadInventory($scope.request);
	});

	$scope.$on('go.to.addInv', function () {
		if (contract_page.useInventory) $scope.navigation.active = _.indexOf($scope.navigation.pages, 'Add. Inventory');
	});
	$scope.$on('go.to.secAddInv', function () {
		if (contract_page.useInventory) $scope.navigation.active = _.indexOf($scope.navigation.pages, 'Add. Inventory') + 1;
	});

	function setValuation(request) {
		$scope.calcData.valuation = 0;

		if (request.request_all_data.invoice.request_all_data.valuation.selected.valuation_type == valuationService.VALUATION_TYPES.FULL_VALUE) {
			$scope.calcData.valuation = request.request_all_data.invoice.request_all_data.valuation.selected.valuation_charge || 0;
		}
	}

	$scope.$on('change.valuation', function (e, data) {
		setValuation(data);

		let reqTotal = 0;
		if ($scope.request.service_type.raw != 5 && $scope.request.service_type.raw != 7) reqTotal = $scope.calculation.totalCost(); else
			reqTotal = ContractCalc.totalCostForFlatRate();
		$scope.calcData.totalCost = parseFloat(reqTotal.toFixed(2));
		$scope.saveContract({missModalClose: true});
	});

	$scope.$on('init.contract.tour', function (e, data) {
		contractTouring();
	});

	$scope.$on('recalc.contract', function (e, data) {
		$scope.request.request_all_data = data;
		saveReqDataInvoice();
	});

	$rootScope.$on('signature.removed', function (e) {
		saveContract();
	});

	function setTravelTime() {
		if ($scope.isDoubleDriveTime) {
			let doubleTravelTime = _.get($scope.request, 'request_all_data.invoice.field_double_travel_time');

			if (doubleTravelTime && !_.isNull($scope.request.request_all_data.invoice.field_double_travel_time.raw)) {
				$scope.travelTime = angular.copy($scope.request.request_all_data.invoice.field_double_travel_time);
			} else {
				$scope.travelTime = angular.copy($scope.request.field_double_travel_time);
			}
		} else {
			if ($scope.request.request_all_data.invoice.travel_time) {
				$scope.travelTime = angular.copy($scope.request.request_all_data.invoice.travel_time);
			} else {
				$scope.travelTime = angular.copy($scope.request.travel_time);
			}
		}
	}

	function setUpisDoubleDriveTime() {
		$scope.isDoubleDriveTime = calcSettings.doubleDriveTime && $scope.request && $scope.request.field_double_travel_time && !_.isNull($scope.request.field_double_travel_time.raw) && services.indexOf(parseInt($scope.request.service_type.raw)) >= 0;
	}

	function resetTravelTime() {
		if (_.isNumber($scope.travelTime.value)) $scope.travelTime.value = common.decToTime($scope.travelTime.value);
		var valid = [];

		if ($scope.travelTime.value) {
			valid = $scope.travelTime.value.split(':');
		}

		if (2 == valid.length) {
			var h, m;
			h = valid[0].slice(-2);
			m = valid[1].substring(0, 2);
			$scope.travelTime.value = h + ':' + m;
		}
		if (typeof $scope.travelTime.raw == 'string') $scope.travelTime.raw = Number($scope.travelTime.raw);
		if ($scope.isDoubleDriveTime) {
			let doubleTravelTime = _.get($scope.request, 'request_all_data.invoice.field_double_travel_time');

			if (angular.isUndefined(doubleTravelTime)) {
				_.set($scope.request, 'request_all_data.invoice.field_double_travel_time', angular.copy($scope.travelTime));
			}

			$scope.request.request_all_data.invoice.field_double_travel_time.raw = Number(common.convertStingToIntTime($scope.travelTime.value).toFixed(2));
			$scope.request.request_all_data.invoice.field_double_travel_time.value = $scope.travelTime.value;
			$scope.request.request_all_data.invoice.field_double_travel_time.old = $scope.travelTime.value;
		} else {
			if (angular.isUndefined($scope.request.request_all_data.invoice.travel_time)) {
				$scope.request.request_all_data.invoice.travel_time = angular.copy($scope.travelTime);
			}
			$scope.request.request_all_data.invoice.travel_time.raw = Number(common.convertStingToIntTime($scope.travelTime.value).toFixed(2));
			$scope.request.request_all_data.invoice.travel_time.value = $scope.travelTime.value;
			$scope.request.request_all_data.invoice.travel_time.old = $scope.travelTime.value;
		}
	}

	function createContractPdf() {
		let defer = $q.defer();

		$scope.busy = true;
		let contractTab = angular.element('#local_moves');

		if (!contractTab.hasClass('active')) {
			contractTab.addClass('active');
		}

		angular.element('html, body').animate({
			scrollTop: angular.element('#contract-page').offset().top
		}, 1000);

		$rootScope.$broadcast('createContractPdf');

		let element = $('flippy-front');
		let htmlToPdf = element[0].innerHTML;

		saveContract()
			.then(() => {
				$scope.busy = true;
				return createPdf(htmlToPdf, 'mainPage');
			})
			.then(({data}) => {
				if ($scope.navigation.active != _.indexOf($scope.navigation.pages, 'Bill of lading')) {
					contractTab.removeClass('active');
				}

				return ContractService.getPdfByTab(data[0]);
			})
			.then(({data}) => {
				$scope.data.mainContractPagePdf = data;
				return createPdf($scope.terms, 'mainPage');
			})
			.then(({data}) => ContractService.getPdfByTab(data[0]))
			.then(({data}) => {
				$scope.data.backSidePagePdf = data;
				defer.resolve();
			})
			.catch((err) => {
				SweetAlert.swal('Failed!', 'Creating pdf for main contract page', 'error');
				defer.reject();
			});

		return defer.promise;
    }

    function firstInitAdditionalPages() {
        $scope.initCountAdditionalPage = $scope.tabs.length;
        if(_.get($scope, 'data.indexOfLastAdditionalPage') > -1) {
            $scope.tabs = $scope.tabs.slice(0, $scope.data.indexOfLastAdditionalPage);
        }

        if (_.isEmpty($scope.data.additionalContractPages)) {
            _.forEach($scope.tabs, function (tab, ind) {
                if (ind != 0)
                    $scope.data.additionalContractPages.push({});
            })
        } else if ($scope.data.additionalContractPages.length < $scope.tabs.length - 1) {
            _.forEach($scope.tabs, function (tab, ind) {
                if (ind > $scope.data.additionalContractPages.length)
                    $scope.data.additionalContractPages.push({});
            })
        }
        _.forEach($scope.data.additionalContractPages, function (contract, ind) {
            if (_.isArray(contract))
                $scope.data.additionalContractPages[ind] = {};
        });

		_.forEach($scope.data.additionalContractPages, (contractPage) => {
			if(contractPage.isSubmitted && contractPage.pdfData) {
				let tabIndex = _.findIndex($scope.tabs, (tab) => contractPage.page_id === tab.id);
				if(tabIndex) {
					$scope.tabs[tabIndex].show = true;
				}
			}
		});

        $scope.mainContractInd = _.findIndex($scope.tabs, {mainContract: true});
        if ($scope.mainContractInd > 0) {
            $scope.mainContract = false;
            $scope.mainTab = 1;
            $scope.activeTab = 1;
            $scope.tabs[0].show = false;
            let tabContract = $scope.tabs[$scope.mainContractInd];
            $scope.tabs.splice($scope.mainContractInd, 1);
            $scope.tabs.splice(1, 0, tabContract);
			$scope.addMainPage = _.findLast($scope.data.additionalContractPages, (addPage) => addPage.page_id == tabContract.id);
			if(_.isUndefined($scope.addMainPage)) {
				$scope.addMainPage = {};
			}
			$scope.getPageId = $scope.data.isFirstSubmittedPage ? $scope.data.isFirstAddMainId : $scope.mainContractInd;
        } else {
			$scope.tabs[0].mainContract = true;
		}


		for (let i = 0; i < $scope.data.additionalContractPages.length; i++) {
			$scope.viewAdditionalContractPages.push(getPage(i));
		}
	}

	function checkForeman() {
		$scope.isForeman = getUserRole().indexOf('foreman') > -1;
		if ($scope.isForeman) {
			writeForemanLog();
		}
	}

	function writeForemanLog() {
		let details = [{
			simpleText: `Foreman has visited account`
		}];
		RequestServices.sendLogs(details, 'Foreman', nid, 'MOVEREQUEST');
	}

	function copy(type) {
		let data = type === 'mainLink' ? angular.element('#main-page') : angular.element('#additional-pages');
		data.select();

		let result = document.execCommand('copy');
		if (result) {
			SweetAlert.swal('Success!', 'Successfully copy data to clipboard', 'success');
		} else {
			SweetAlert.swal('Failed!', 'Error of copy link to clipboard', 'error');
		}
	}

	function getAddMainPage() {
		return $scope.addMainPage;
	}

	function getPage($index) {
		let pageDefault = $scope.data.additionalContractPages[$index];

		if ($scope.mainContract || $index === 0 ||
		    $index === $scope.getPageId || !~$scope.getPageId < $index) {
			return pageDefault;
		} else if ($scope.getPageId > $index) {
			return $scope.data.additionalContractPages[$index - 1];
		}
	}

	function checkRequestTypeSetUserId() {
		if($scope.request.service_type.raw == 2 && $scope.request.request_all_data.toStorage) {
			return $scope.uid;
		}
	}

	function getEmails() {
		let emails = [];

		if ($scope.request.email) {
			emails.push($scope.request.email);
		}

		if (_.get($scope.request, 'field_additional_user.mail')) {
			emails.push($scope.request.field_additional_user.mail);
		}

		return emails;
	}
}
