'use strict';

angular.module('contractModule')
    .controller('ContractSettingsCtrl', ContractSettingsCtrl);

ContractSettingsCtrl.$inject = ['$scope', 'datacontext', 'SettingsService', 'SweetAlert', 'ContractService',
								'contractConstant', 'ContractValue', 'ContractPageData'];

function ContractSettingsCtrl($scope, datacontext, SettingsService, SweetAlert, ContractService,
							  contractConstant, ContractValue, ContractPageData) {
    let vm = this;

    vm.isSubmitted = false;
    vm.busy = false;
    vm.extraServices = '';
	vm.terms = '';
    vm.tabs = ContractValue.tabs;
    vm.rentalAgreement = ContractValue.rentalAgreement;
	vm.settings = angular.fromJson((datacontext.getFieldData()).basicsettings);
	vm.contract_page = angular.fromJson((datacontext.getFieldData()).contract_page);
	vm.countBasePayment = contractConstant.countBasePayment;
	vm.ContractPageData = ContractPageData;

    vm.save = save;
    vm.addNewStorageFit = addNewStorageFit;
    vm.rentalIcons = rentalIcons;
    vm.setDefaultStorage = setDefaultStorage;
    vm.selectTab = selectTab;
    vm.checkEmptySettings = checkEmptySettings;

    ContractService.getExtraServices('additional_settings')
		.then((jsonData) => $scope.additionalServices = angular.fromJson(jsonData.data[0]))
		.catch((err) => SweetAlert.swal('Failed!', 'Error of getting extra services', 'error'));

    function selectTab(field) {
        _.forEach(vm.tabs, function (item) {
            item.selected = false;
        });
        vm.tabs[field].selected = true;
    }

    function save() {
        vm.busy = true;
        saveReleaseFormSettings();

        _.forEach(vm.contract_page.textContent.storageFit, (item, id) => {
            if (item.connected_service == '')
                item.connected_service = id.toString();
            if (typeof item.connected_service == 'string')
                item.connected_service = (item.connected_service.toLowerCase()).replace(/\s/g, '_');
        });


        _.forEach(vm.contract_page.additionalContract, (tab, ind) => {
            for(let i = 0; i < vm.contract_page.usingTabs.length; i++) {
                if(vm.contract_page.usingTabs[i] === ind) {
                    vm.contract_page.additionalContract[ind].page = document.getElementById('additional-contract_'+ind).querySelectorAll('[contenteditable]')[0].innerHTML;
                }
            }
        });

        SettingsService.saveSettings(vm.contract_page, 'contract_page')
            .then(() => SettingsService.saveAccountContractSettings(vm.contract_page))
            .then((settings) => {
                vm.contract_page = settings;
                datacontext.getFieldData()['contract_page'] = vm.contract_page;
                vm.busy = false;
                SweetAlert.swal("Success!", "Successfully saved!", 'success');
            })
            .catch((err) => {
                vm.busy = false;
                SweetAlert.swal("Failed!", "Error of saving settings", 'error');
            });
    }

    function saveReleaseFormSettings() {
        vm.contract_page.releaseForm.header = document.getElementById('header').querySelectorAll('[contenteditable]')[0].innerHTML;
        vm.contract_page.releaseForm.footer = document.getElementById('footer').querySelectorAll('[contenteditable]')[0].innerHTML;
        _.forEach(vm.contract_page.releaseForm.services, function (tab, ind) {
            vm.contract_page.releaseForm.services[ind].body = document.getElementById('release-textarea_'+ind).querySelectorAll('[contenteditable]')[0].innerHTML;
        });
    }

    function addNewStorageFit() {
        let newStorage = {
            active: true,
            connected_service: '',
            text: ''
        };

        vm.contract_page.textContent.storageFit.push(newStorage);
    }

    function setDefaultStorage() {
        vm.contract_page.textContent.storageFit = ContractValue.storageFit;
        save(false);
    }

    function rentalIcons(id) {
        switch (id) {
            case 'creditPayment':
                return 'fa-credit-card';
                break;
            case 'cashPayment':
                return 'fa-money';
                break;
            case 'checkPayment':
                return 'fa-list-alt';
                break;
            default:
                return 'fa-list-alt';
        }
    }

	function checkEmptySettings(prop) {
		if(!vm.contract_page.editableFields[prop].trim().length) {
			vm.contract_page.editableFields[prop] = vm.ContractPageData.contractPageEditableFields[prop];
		}
	}

}
