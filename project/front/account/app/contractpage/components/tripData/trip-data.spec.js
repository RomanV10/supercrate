describe('trip data', () => {
	let scope;
	let elementScope;
	let element;

	function compileElement() {
		element = angular.element('<trip-data request="request" name="{{name}}" data="data"></trip-data>');
		element = $compile(element)(scope);
		elementScope = element.isolateScope();
	}

	beforeEach(() => {
		scope = $rootScope.$new();
		scope.request = {};
		scope.name = '';
		scope.data = {};
	});

	describe('When init', () => {
		it('should not render empty html', function () {
			//given
			let notExpectedResult = '';

			//when
			compileElement();

			//then
			expect(element.html()).not.toEqual(notExpectedResult);
		});
	});

	describe('test controller, should be', () => {
		it('direction undefined', () => {
			//given
			let direction;

			//when
			compileElement();
			direction = elementScope.$ctrl.direction;

			//then
			expect(direction).toBeUndefined();
		});

		it('direction from', () => {
			//given
			scope.name = 'field_extra_pickup';
			let direction;
			let expectedResult = 'from';

			//when
			compileElement();
			direction = elementScope.$ctrl.direction;

			//then
			expect(direction).toBe(expectedResult);
		});

		it('direction to', () => {
			//given
			scope.name = 'field_extra_dropoff';
			let direction;
			let expectedResult = 'to';

			//when
			compileElement();
			direction = elementScope.$ctrl.direction;

			//then
			expect(direction).toBe(expectedResult);
		});
	})
});
