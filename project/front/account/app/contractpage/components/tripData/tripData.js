'use strict';

class TripData {
	constructor() {}

	$onInit() {
		if(this.name === 'field_extra_pickup') {
			this.direction = 'from';
		} else if(this.name === 'field_extra_dropoff') {
			this.direction = 'to';
		}
	}
}


angular.module('contractModule')
	.component('tripData', {
		template: require('./tripData.html'),
		controller: TripData,
		controllerAs: '$ctrl',
		bindings: {
			request: '<',
			name: '@',
			data: '<'
		}
	});
