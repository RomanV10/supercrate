'use strict';

class PrintDownload {
	constructor() {}
}

angular
	.module('contractModule')
	.component('printDownload', {
		template: require('./printDownload.html'),
		controller: PrintDownload,
		controllerAs: '$ctrl',
		bindings: {
			url: '<'
		}
	});
