'use strict';

class SendLetterPdf {
	constructor() {}
}

angular.module('contractModule')
	.component('sendLetterPdf', {
		template: require('./sendLetterPdf.html'),
		controller: SendLetterPdf,
		controllerAs: '$ctrl',
		bindings: {
			index: '<?',
			sendPdfPage: '&',
			downloadPdfPage: '&',
		}
	});
