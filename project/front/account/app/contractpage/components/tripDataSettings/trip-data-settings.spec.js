describe('trip data settings component', () => {
	let scope;
	let elementScope;
	let element;
	let elementController;

	function compileElement() {
		element = angular.element('<trip-data contract-page="contractPage" type="{{type}}"></trip-data>');
		element = $compile(element)(scope);
		elementScope = element.isolateScope();
		elementController = elementScope.$ctrl;
	}

	beforeEach(() => {
		scope = $rootScope.$new();
		scope.contractPage = testHelper.loadJsonFile('account-trip-data-settings_contract-page.mock');
		scope.type = 'origin';
	});

	describe('When init', () => {
		it('should not render empty html', function () {
			//given
			let notExpectedResult = '';

			//when
			compileElement();

			//then
			expect(element.html()).not.toEqual(notExpectedResult);
		});
	});
});
