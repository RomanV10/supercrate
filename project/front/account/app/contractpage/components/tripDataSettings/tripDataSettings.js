'use strict';

class TripDataSettings {
	constructor(ContractPageData) {
		this.ContractPageData = angular.copy(ContractPageData);
	}

	$onInit() {
		this.ContractPageData.contractPageOriginDestination.name = this.type;
	}

	checkEmpty(prop) {
		if(!this.contract_page[this.type][prop].trim().length) {
			this.contract_page[this.type][prop] = this.ContractPageData.contractPageOriginDestination[prop];
		}
	}
}

angular.module('contractModule')
	.component('tripDataSettings', {
		template: require('./tripDataSettings.html'),
		controller: TripDataSettings,
		controllerAs: '$ctrl',
		bindings: {
			contract_page: '=contractPage',
			type: '@'
		}
	});

	TripDataSettings.$inject = ['ContractPageData'];
