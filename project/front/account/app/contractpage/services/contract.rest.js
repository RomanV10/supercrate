!function () {

    'use strict';

    angular.module('contractModule')
        .factory('ContractService', ContractService);

	/*@ngInject*/
    function ContractService($http, config, $q, $rootScope, RequestServices, AuthenticationService,
							 RequestPageServices, ContractValue, Session, apiService, moveBoardApi) {

	    function checkRequestTypeSetUserId(request) {
		    let serviceType = request.service_type.raw;
		    let toStorage = request.request_all_data.toStorage;
		    let session = Session.get();

		    if ((serviceType == 2 || serviceType == 6) && toStorage) {
			    return _.get(session, 'userId.uid');
		    }
	    }

        return {
            getExtraServices(type) {
                return $http.post(`${config.serverUrl}server/system/get_variable`, {name: type});
            },

            getUsersByRole(filtering) {
                return $http.post(`${config.serverUrl}server/move_users_resource/get_user_by_role`, filtering);
            },

            getSignatures(nid) {
                return $http.post(`${config.serverUrl}server/move_request/get_contract/${nid}`);
            },

            saveContract(data, nid) {
                return $http.post(`${config.serverUrl}server/move_request/set_contract`, {nid: nid, value: data});
            },

            savePayment(nid, value) {
                return $http.post(`${config.serverUrl}server/move_request/set_payment`, {nid :nid, data: value});
            },

            sendPayroll(nid, data, contractType) {
				let request = {
							nid: nid,
							value: data,
							contract_type: contractType
							};

                return $http.post(`${config.serverUrl}server/payroll/set_contract_info`, request);
            },

            getContractInfo(nid) {
                return $http.post(`${config.serverUrl}server/payroll/get_contract_info`, {nid: nid});
            },

            saveCardImage(nid, data) {
                return $http.post(`${config.serverUrl}server/move_request/set_card/${nid}`, {data: data});
            },

            getCardImage(nid) {
                return $http.post(`${config.serverUrl}server/move_request/get_card/${nid}`);
            },
            saveReqData(nid,info, userId) {
				let data = {
					data: {
						'all_data': info
					}
				};

				if(userId) {
					data.global_user_uid = userId;
				}

				return $http.put(`${config.serverUrl}server/move_request/${nid}`, data);
        	},
	        saveContractRequestData(request) {
            	let userId = checkRequestTypeSetUserId(request);
		        let nid = request.nid;
		        let info = request.request_all_data;
		        let data = {
			        data: {
				        'all_data': info
			        }
		        };

		        if(userId) {
			        data.global_user_uid = userId;
		        }

		        return $http.put(`${config.serverUrl}server/move_request/${nid}`, data);
	        },
            addForemanLog(message, title, nid, type) {
                var currUser = $rootScope.currentUser;
                var userRole = '';

                function getUserRole() {
	                return _.get(currUser, 'userRole', []);
                }

                if (getUserRole().indexOf('administrator') >= 0) {
                    userRole = 'administrator';
                } else if (getUserRole().indexOf('manager') >= 0) {
                    userRole = 'manager';
                } else if (getUserRole().indexOf('sales') >= 0) {
                    userRole = 'sales';
                } else if (getUserRole().indexOf('foreman') >= 0) {
                    userRole = 'foreman';
                } else if(getUserRole().length == 1 && getUserRole().indexOf('authenticated user') >= 0) {
					userRole = 'client';
				}
                var entity_type = ContractValue.entityTypes[type];
                var msg = {
                    entity_id: nid,
                    entity_type: entity_type,
                    data: [{
                        details: [{
                            title: title,
                            text: [],
                            activity: userRole,
                            date: moment().unix()
                        }],
                        source: currUser.userId.field_user_first_name + ' ' + currUser.userId.field_user_last_name
                    }]
                };
                var obj = {
                    text: message
                };
                if (_.isObject(message))
                    obj = message;
                var info = {
                    browser: AuthenticationService.get_browser(),
                };
                msg.data[0].details[0].info = info;
                msg.data[0].details[0].text.push(obj);
                RequestServices.postLogs(msg);
            },
			createRequest(request) {
				return RequestPageServices.prepareRequest(request)
					.then((data) => {
						data.all_data.storage_request_id = request.request_all_data.storage_request_id;
						data.all_data.additionalBlock = request.request_all_data.additionalBlock;
						data.all_data.baseRequestNid = request.nid;
						data.all_data.toStorage = true;

						return data;
					})
					.then(sendCreatingData);

				function sendCreatingData(data) {
					return apiService.postData(moveBoardApi.request.base, data);
				}
			},

            sendDocumentForPDF(pdf) {
                pdf.serverUrl = config.drupalUrlForNodeJs;
                return $http({
                            url: `${config.nodeJsServerUrl}api/pdf`,
                            withCredentials: true,
                            method: 'POST',
                            data: pdf
                        });
            },

            getPDFByIdForDisplay(url) {
                let deferred = $q.defer();

                $http({method: 'GET', url: url, responseType: 'arraybuffer'})
					.then((response) => {
						let file = new Blob([response], {type: 'application/pdf'});
						let fileURL = URL.createObjectURL(file);
						deferred.resolve(fileURL);
					})
					.catch((err) => deferred.reject(err));

                return deferred.promise;
            },

            getPdfByTab(index) {
                return $http.get(`${config.serverUrl}server/move_mpdf/${index}`);
            },

            getPdfByRequest(entityId, entityType) {
                return $http.get(`${config.serverUrl}server/move_mpdf?entity_id=${entityId}&entity_type=${entityType}`);
            },

            saveMainInventory(nid, listInventories) {
            	let data = {
            		data: {
						items: listInventories
					}
				};

                let jsonDetails = JSON.stringify(data);

                return $http.post(`${config.serverUrl}server/move_invertory/${nid}`, jsonDetails);
            }
        }
    }

}();
