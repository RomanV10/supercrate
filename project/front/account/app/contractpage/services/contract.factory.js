!function () {

    'use strict';

    angular.module('contractModule')
        .factory('ContractFactory', ContractFactory);

	/*@ngInject*/
    function ContractFactory(ContractService, $rootScope, logger, SweetAlert, common, $uibModal, datacontext,
							 apiService, moveBoardApi, ContractObjectFactory, ContractSymbols, ContractValue, Session,
							 contractDispatchLogs) {

		const INTERVAL_FQ = 5000;
    	let service = {};
        let intervalStop;
        let canvas,
			signaturePad,
			canvasRelease,
			signatureReleasePad,
			signatureInventoryPad,
			canvasInventory,
			signatureAddInventoryPad,
			canvasAddInventory,
			canvasAgreement,
			signatureAgreementPad;

		let fieldData = datacontext.getFieldData();
		let contract_page = angular.fromJson(fieldData.contract_page);
		let session = Session.get();
		let userRole = _.get(session, 'userRole', []);

        service.factory = ContractObjectFactory.factory;
		service.factory.laborOnly = !!_.find(contract_page.paymentTypes, (type) => type.name === 'Labor only.' && type.selected);
        service.factoryObj = angular.copy(ContractObjectFactory.factory);
        service.oldReleaseFormObj = ContractValue.oldReleaseFormObj;
        service.storage = ContractValue.storage;
        service.isAdminManager = (~userRole.indexOf('administrator') || ~userRole.indexOf('manager')) ? true : false;
        service.isAdmin = (~userRole.indexOf('administrator')) ? true : false;

        if(contract_page.releaseFormOldVersion) {
            service.factory.releaseData = angular.copy(service.oldReleaseFormObj);
            service.factoryObj.releaseData = angular.copy(service.oldReleaseFormObj);
        }

        service.extra_data = ContractValue.extra_data;
        service.navigation = ContractValue.navigation;
        service.stepsData = ContractValue.stepsData;
        service.flatStepsData = ContractValue.flatStepsData;
		service.permission = ContractValue.permission;
		service.nid = ContractValue.nid;
        service.descreptiveSymbols = ContractSymbols.descreptiveSymbols;
        service.exceptionSymbols = ContractSymbols.exceptionSymbols;
        service.locationSymbols = ContractSymbols.locationSymbols;

        service.paymentReceivedEvent = '';
        service.longDistanceRate = 0;

        service.init = init;
        service.initData = initData;
        service.getData = getData;
        service.clearSignaturePad = clearSignaturePad;
        service.removeSignature = removeSignature;
        service.openSignaturePad = openSignaturePad;
        service.getCropedSignatureValue = getCropedSignatureValue;
        service.openReleaseSignaturePad = openReleaseSignaturePad;
        service.getCropedReleaseSignatureValue = getCropedReleaseSignatureValue;
        service.openInventorySignaturePad = openInventorySignaturePad;
        service.getCropedInventorySignatureValue = getCropedInventorySignatureValue;
        service.openCondition = openCondition;
        service.openTransferInventory = openTransferInventory;
        service.openAdditionalInventory = openAdditionalInventory;
        service.openAddInventorySignaturePad = openAddInventorySignaturePad;
        service.getCropedAddInventorySignatureValue = getCropedAddInventorySignatureValue;
        service.getCropedAgreementSignatureValue = getCropedAgreementSignatureValue;
        service.transferInventoryContractData = transferInventoryContractData;
        service.getSignatureUrl = getSignatureUrl;
        service.transferInventory = transferInventory;

        //**** @Construct
        function init() {

            common.$timeout(function () {
                    canvas = document.getElementById('signatureCanvas');
                    canvasRelease = document.getElementById('signatureReleaseCanvas');
                    canvasInventory = document.getElementById('signatureInventoryCanvas');
                    canvasAddInventory = document.getElementById('signatureAddInventoryCanvas');
                    canvasAgreement = document.getElementById('signatureAgreementCanvas');
                    if (canvas != null)
                        signaturePad = new SignaturePad(canvas);
                    if (canvasRelease != null)
                        signatureReleasePad = new SignaturePad(canvasRelease);
                    if (canvasInventory != null)
                        signatureInventoryPad = new SignaturePad(canvasInventory);
                    if (canvasAddInventory != null) {
                        if (canvasAddInventory.width < window.innerWidth)
                            canvasAddInventory.width = window.innerWidth;
                        if (canvasAddInventory.height < window.innerHeight - 400)
                            canvasAddInventory.height = window.innerHeight - 400;
                        signatureAddInventoryPad = new SignaturePad(canvasAddInventory);
                    }
                    if (canvasAgreement != null)
                        signatureAgreementPad = new SignaturePad(canvasAgreement);
                },
                500);
        }

        function getData(props, cb) {
            angular.forEach(props, function (prop, k) {
                if (_.has(service, k)) service[k] = prop;
            });

            ContractService.getSignatures(props.nid)
                .then(({data}) => !!cb && cb(data))
				.catch(() => {
                    let signaturesStorage = JSON.parse(localStorage['pendingSignatures']);
                    if (_.size(signaturesStorage)) {
                        service.factory.signatures = signaturesStorage;
                    }
                })
                .finally(function () {
                    //checking local storage existence
                    if (typeof localStorage['pendingSignatures'] === 'undefined') {
                        localStorage['pendingSignatures'] = JSON.stringify({});
                    }
                    if (!intervalStop) { //move to controller
                        intervalStop = $interval(function () {
                            let signaturesStorage = angular.fromJson(localStorage['pendingSignatures']);
                            if (_.size(_.keys(signaturesStorage))) {
                                service.factory.signatures = signaturesStorage;
                                saveContract(ContractFactory.signatures, nid)
                                    .then(() => {
                                        localStorage['pendingSignatures'] = JSON.stringify({});
                                    }).catch(() => SweetAlert.swal('Failed!', 'Cant store data in database', 'error'));
                            }
                        }, INTERVAL_FQ);
                    }
                });
        }

        function initData(props) {
            if (props.hasOwnProperty('factory')) {
                angular.forEach(props.factory, function (d, k) {
                    if (_.has(service.factory, k)) service.factory[k] = d;
                });
            }
            if (props.hasOwnProperty('extra_data')) {
                for (var i = 0, l = props.extra_data.length; i < l; i++) {
                    service.extra_data.services[props.extra_data[i].alias] = props.extra_data[i];
                }
            }
        }

        function clearSignaturePad() {
            if(signaturePad) {
                signaturePad.clear();
            }
            if(signatureReleasePad) {
                signatureReleasePad.clear();
            }
            if(signatureInventoryPad)
                signatureInventoryPad.clear();
            if(signatureAddInventoryPad)
                signatureAddInventoryPad.clear();
            if(signatureAgreementPad)
                signatureAgreementPad.clear();
        }

        function removeSignature(stepId, nid, uid, flatMiles) {
            var msgToLog = '';
            msgToLog += 'Signature of step#' + stepId + ' deleted.';
            ContractService.addForemanLog(msgToLog,'Signature removed on contract', nid, 'MOVEREQUEST');
            var activeC = service.factory.crews[service.navigation.activeCrew];
            delete service.factory.signatures[stepId];
            service.factory.signatures.length -= 1;
			contractDispatchLogs.assignLogs({nid: nid}, {signatureNumber: stepId, signatureRemoved: true});
            if(!flatMiles) {
                if(stepId == service.stepsData.startTimeStep) {
                    activeC.timer.start = 0;
                    activeC.timer.lastAction = 'cancelStart';
                    service.factory.currentStep -= 1;
                }
                if(stepId == service.stepsData.stopTimeStep) {
                    activeC.timer.stop = 0;
                    activeC.timer.lastAction = 'cancelStop';
                    service.factory.currentStep -= 1;
                }
            }
            if(flatMiles) {
            	switch(stepId) {
					case service.flatStepsData.pickupStartTimeStep:
					case service.flatStepsData.unloadStartTimeStep:
						activeC.timer.start = 0;
						activeC.timer.lastAction = 'cancelStart';
						service.factory.currentStep -= 1;
						break;
					case service.flatStepsData.pickupStopTimeStep:
					case service.flatStepsData.unloadStopTimeStep:
						activeC.timer.stop = 0;
						activeC.timer.lastAction = 'cancelStop';
						service.factory.currentStep -= 1;
						break;
					default:
						break;
				}
            }

            return service.factory;
        }

        function openSignaturePad(owner) {
			getOpenSignatureHelper('#signaturePad', owner);
        }

        function getCropedAgreementSignatureValue() {
			return getSignatureUrl(signatureAgreementPad._ctx);
        }

        function openReleaseSignaturePad(owner) {
			getOpenSignatureHelper('#signatureReleasePad', owner);
        }

        function getCropedReleaseSignatureValue() {
			return getSignatureUrl(signatureReleasePad._ctx);
        }

        function openInventorySignaturePad(owner) {
            if(canvasInventory) {
            	canvasInventory = angular.element('#signatureInventoryCanvas');
            }
			getOpenSignatureHelper('#signatureInventoryPad', owner, 'inventory');
        }

        function getCropedInventorySignatureValue() {
        	return getSignatureUrl(signatureInventoryPad._ctx);
        }

        function openAddInventorySignaturePad(owner) {
			getOpenSignatureHelper('#signatureAddInventoryPad', owner, 'addInventory');
        }

        function getCropedAddInventorySignatureValue() {
        	return getSignatureUrl(signatureAddInventoryPad._ctx);
        }

        function getCropedSignatureValue() {
			return getSignatureUrl(signaturePad._ctx);
        }

        function openCondition(inventory, exceptionSymbols, locationSymbols, key) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/contractpage/directives/inventory/conditionModal.html',
                controller: conditionModalController,
                size: 'lg',
                backdrop: false,
                resolve: {
                    inventory: function () {
                        return inventory;
                    },
                    exceptionSymbols: function () {
                        return exceptionSymbols;
                    },
                    locationSymbols: function () {
                        return locationSymbols;
                    },
                    key: function () {
                        return key;
                    }
                }
            });
        }

        function conditionModalController($scope, $rootScope, inventory, exceptionSymbols, locationSymbols, key, $uibModalInstance) {
            $scope.inventory = inventory;
            $scope.exceptionSymbols = exceptionSymbols;
            $scope.locationSymbols = locationSymbols;
            $scope.condition = true;
            $scope.newCondition = {condition: '', location: {}};

            $scope.cancel = cancel;
            $scope.addCondition = addCondition;
            $scope.addLocation = addLocation;
            $scope.removeCondition = removeCondition;
            $scope.Save = Save;
            $scope.SaveExit = SaveExit;
            $scope.returnBack = returnBack;

            if (angular.isUndefined($scope.inventory.originCondition))
                $scope.inventory.originCondition = {};

            function addCondition(key) {
                $scope.condition = false;
                $scope.newCondition.condition = key;
            }

            function addLocation(key) {
                if (angular.isDefined($scope.newCondition.location[key])) {
                    delete $scope.newCondition.location[key];
                } else {
                    $scope.newCondition.location[key] = key;
                }
            }

            function removeCondition(ind) {
                delete $scope.inventory.originCondition[ind];
                $rootScope.$broadcast('save.inventory_condition', {inventory: $scope.inventory, id: key});
            }

            function Save() {
                if ($scope.newCondition.condition != '') {
                    $scope.inventory.originCondition[$scope.newCondition.condition] = $scope.newCondition;
                    $scope.condition = true;
                }
                $scope.newCondition = {condition: '', location: {}};
                $rootScope.$broadcast('save.inventory_condition', {inventory: $scope.inventory, id: key});
            }

            function SaveExit() {
                if ($scope.newCondition.condition != '')
                    $scope.inventory.originCondition[$scope.newCondition.condition] = $scope.newCondition;
                $rootScope.$broadcast('save.inventory_condition', {inventory: $scope.inventory, id: key});
                $uibModalInstance.dismiss('cancel');
            }

            function cancel() {
                $uibModalInstance.dismiss('cancel');
            }

            function returnBack() {
                $scope.newCondition = {condition: '', location: {}};
                $scope.condition = true;
            }
        }

        function openTransferInventory(data, request, fieldName) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/contractpage/directives/inventory/transferInventory.html',
                controller: transferModalController,
                size: 'lg',
                backdrop: false,
                resolve: {
                    data: function () {
                        return data;
                    },
                    request: function () {
                        return request;
                    },
                    fieldName: function () {
                        return fieldName;
                    }
                }
            });
        }

		function transferModalController($scope, $rootScope, data, request, fieldName, $uibModalInstance) {
			let dataContract = data;
		    $scope.requestNid = '';
		    $scope.cancel = cancel;
		    $scope.transfer = transfer;

		    function cancel() {
			    $uibModalInstance.dismiss('cancel');
		    }

		    function transfer() {
			    if (_.isEmpty($scope.requestNid)) {
				    toastr.warning('Please! Enter request nid.');
			    } else {
				    transferInventory();
			    }
		    }

		    function transferInventory() {
			    $scope.busy = true;

			    service.transferInventory(dataContract, fieldName, $scope.requestNid, request.nid)
				    .then(function (response) {
					    $scope.busy = false;

					    if (response.data.status_code !== 200) {
						    toastr.error(response.data.status_message);
					    } else {
						    $uibModalInstance.dismiss('cancel');
						    $rootScope.$broadcast('transfer.inventory');

						    // this code perform on backend, I don't need update request all data
						    if (!request.request_all_data.transferedTo) {
							    request.request_all_data.transferedTo = [];
						    }

						    request.request_all_data.transferedTo.push($scope.requestNid);
						    toastr.success(`Inventory was transferred to request #${$scope.requestNid}`);
					    }
				    }, function () {
					    $scope.busy = false;
				    });
		    }
	    }

	    function transferInventory(dataContract, fieldName, entity_id_to, entity_id_from) {
		    let from_inventory_tab_name = 'inventoryMoving';

		    if (dataContract.addInventoryMoving.open && fieldName == 'inventoryMoving') {
			    from_inventory_tab_name = 'addInventoryMoving';
		    }

		    if (dataContract.secondAddInventoryMoving.open && fieldName == 'addInventoryMoving') {
			    from_inventory_tab_name = 'secondAddInventoryMoving';
		    }

		    return apiService.postData(moveBoardApi.inventory.request.transferInventory, {
			    from_inventory_tab_name,
			    entity_id_to,
			    entity_id_from
		    });
	    }

        function openAdditionalInventory(inventory, request, fieldName) {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/contractpage/directives/inventory/additionalInventoryModal.html',
                controller: additionalInventoryCtrl,
                size: 'inventory',
                backdrop: false,
                resolve: {
                    inventory: function () {
                        return inventory;
                    },
                    request: function () {
                        return request;
                    },
                    fieldName: function () {
                        return fieldName;
                    }
                }
            });
        }

        function additionalInventoryCtrl($scope, $rootScope, inventory, request, fieldName, $uibModalInstance, ContractService) {
            $scope.inventory = inventory;
            $scope.requestNid = '';
            $scope.request = request; //inventory.inventory_list;
            $scope.fieldName = fieldName;
            $scope.Save = Save;
            $scope.cancel = cancel;

            function cancel() {
                $uibModalInstance.dismiss('cancel');
            }

            function Save(){
                if($scope.chooseInventoryOption == 'client'){
                    $scope.request.request_all_data.needsAddInventory = true;

	                ContractService.saveContractRequestData($scope.request)
		                .finally(cancel);
                }
            }

            $scope.$on('add.inventory',function (evt, data) {
	            _.set($scope, `request.request_all_data.invoice.${fieldName}.doneWithInventory`, false);
                cancel();
            });

            $scope.$on('add.inventory.added.moving',function (evt, data) {
	            _.set($scope, `request.request_all_data.invoice.${fieldName}.doneWithInventory`, false);
                cancel();
            });
        }

        function transferInventoryContractData(dataContract, fieldName, storage_id, financeObj) {
            ContractService.getSignatures(storage_id).then(function ({data}) {
                if(_.isEmpty(data) && _.isArray(data)) data = {};
                if(angular.isUndefined(data.factory)){
                    data.factory = service.factoryObj;
                    data.factory.declarationValue = dataContract.declarationValue;
                }
                if (angular.isUndefined(data.finance)) {
                    data.finance = financeObj;
                }

                data.factory[fieldName] = angular.copy(dataContract[fieldName]);
                data.factory[fieldName].tranfer = true;
                data.factory[fieldName].isSubmitted = false;

                if(dataContract.addInventoryMoving.open && fieldName == 'inventoryMoving')
                    data.factory.addInventoryMoving = dataContract.addInventoryMoving;
                else if(fieldName == 'inventoryMoving') data.factory.addInventoryMoving.open = dataContract.addInventoryMoving.open;
                if (dataContract.secondAddInventoryMoving.open && fieldName == 'addInventoryMoving')
                    data.factory.secondAddInventoryMoving = dataContract.secondAddInventoryMoving;
                else if(fieldName == 'addInventoryMoving') data.factory.secondAddInventoryMoving.open = dataContract.secondAddInventoryMoving.open;

                if(fieldName == 'inventoryMoving'){
                    data.factory.addInventoryMoving.rangeStart = +dataContract[fieldName].numberThru + 1;
                    data.factory.addInventoryMoving.nosFrom = data.factory.addInventoryMoving.rangeStart;
                    data.factory.addInventoryMoving.numberThru = data.factory.addInventoryMoving.rangeStart;
                }
                if(fieldName == 'addInventoryMoving'){
                    data.factory.secondAddInventoryMoving.rangeStart = +dataContract[fieldName].numberThru + 1;
                    data.factory.secondAddInventoryMoving.nosFrom = data.factory.secondAddInventoryMoving.rangeStart;
                    data.factory.secondAddInventoryMoving.numberThru = data.factory.secondAddInventoryMoving.rangeStart;
                }

	            ContractService.saveContract({
		            factory: _.clone(data.factory),
		            finance: _.clone(data.finance)
	            }, storage_id)
		            .then(function () {
			            dataContract[fieldName].isSubmitted = true;

			            if (dataContract.secondAddInventoryMoving.open && fieldName == 'addInventoryMoving') {
				            $rootScope.$broadcast('inventory.transferred', dataContract);
				            $rootScope.$broadcast('go.to.secAddInv');
				            return false;
			            }

			            if (!dataContract.addInventoryMoving.open
				            || (dataContract.addInventoryMoving.open && dataContract.addInventoryMoving.isSubmitted && dataContract.inventoryMoving.isSubmitted)) {
				            $rootScope.$broadcast('inventory.submit');
			            } else if (fieldName == 'inventoryMoving' && dataContract.addInventoryMoving.open) {
				            $rootScope.$broadcast('go.to.addInv');
			            }

			            $rootScope.$broadcast('inventory.transferred', dataContract);
		            })
		            .catch(() => SweetAlert.swal('Failed!', 'Error of saving contract', 'error'));
            });
        }

		function getSignatureUrl(ctxData, allow) {
			let url;
			let originImgData;
			let xi, yi;
			let imageData, nCanvas;
			let ctx = ctxData;
			let canvasWidth = canvas.width;
			let canvasHeight = canvas.height;
			let minX = canvasWidth;
			let minY = canvasHeight;
			let maxX = 0;
			let maxY = 0;
			let padding = 10;
			if(canvasWidth != 0 && canvasHeight != 0 || allow) {
				originImgData = ctx.getImageData(0, 0, canvasWidth, canvasHeight).data;
				for (let i = 0; i < originImgData.length; i += 4) {
					if (originImgData[i] + originImgData[i + 1] + originImgData[i + 2] + originImgData[i + 3] > 0) {
						yi = Math.floor(i / (4 * canvasWidth));
						xi = Math.floor((i - (yi * 4 * canvasWidth) ) / 4);
						if (xi < minX) minX = xi;
						if (xi > maxX) maxX = xi;
						if (yi < minY) minY = yi;
						if (yi > maxY) maxY = yi;
					}
				}
			}

			imageData = ctx.getImageData(minX, minY, maxX - minX, maxY - minY);
			nCanvas = document.createElement('canvas');
			nCanvas.setAttribute('id', 'nc');
			nCanvas.width = 2 * padding + maxX - minX;
			nCanvas.height = 2 * padding + maxY - minY;
			nCanvas.getContext('2d').putImageData(imageData, padding, padding);
			url = nCanvas.toDataURL().split(',')[1];
			$("canvas#nc").remove();
			return url;
		}

		function getOpenSignatureHelper(type, owner, inventoryType) {
			clearSignaturePad();
			service.factory.currentOwner = owner;
			angular.element(type)
				.modal({backdrop: 'static'})
				.on('shown.bs.modal', _setWitdh.bind({canvas: canvas}));
			function _setWitdh() {
				if(inventoryType) {
					if(inventoryType === 'inventory') {
						canvasInventory.width = angular.element(type).find('.modal-body').innerWidth();
					} else if(inventoryType === 'addInventory') {
						canvasAddInventory.width = angular.element(type).find('.modal-body').innerWidth();
					}
				} else {
					canvas.width = angular.element(type).find('.modal-body').innerWidth();
				}
			}
		}

        return service;
    }
}();
