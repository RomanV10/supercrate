'use strict';

angular.module('contractModule')
	.service('ContractObjectFactory', [ function() {
		this.factory = {
			signatures: { //Array like
				length: 0
			},
			openRental: false,
			crews: [
				{
					timer: {
						start: 0,
						stop: 0,
						timeOff: 0,
						lastAction: '',
						startPickUp: 0,
						stopPickUp: 0,
						startUnload: 0,
						stopUnload: 0,
					},
					rate: 0,
					workDuration: 0
				}
			],
			declarationValue: {
				data: {},
				selected: '',
				declarCustomer:''
			},
			storageFit: {},
			storageServices : {},
			currentStep: 0,
			currentOwner: '',
			req_comment: '',
			req_commentDelivery: '',
			stepSpace: 0,
			discountType: 'yelp',
			discountUsed: false,
			isSubmitted: false,
			isPickupSubmitted: false,
			isPaid: false,
			releaseData: {
				itemsRelease: [],
				releaseService: {},
				footer: {},
				header:{},
				isSubmitted: false,
				currentStep: 0
			},
			feedBack: {},
			openStorageAndTransit: false,
			showDiscount: false,
			inventoryMoving: {
				open: false,
				pageNumber: 1,
				numberOfPages: 1,
				nosFrom: 1,
				numberThru: 25,
				tapeColor: '',
				signatures: {
					length: 0
				},
				currentStep: 0,
				numberedItems: 0,
				tapeNumbers: 0,
				isSubmitted: false,
				inventory: {},
				remarks: '',
				contractGbl: '',
				govtService: '',
				vanNumb: '',
				rangeStart: 1
			},
			addInventoryMoving: {
				open: false,
				pageNumber: 1,
				numberOfPages: 1,
				nosFrom: 1,
				numberThru: 1,
				tapeColor: '',
				signatures: {
					length: 0
				},
				currentStep: 0,
				numberedItems: 0,
				tapeNumbers: 0,
				isSubmitted: false,
				inventory: {},
				remarks: '',
				contractGbl: '',
				govtService: '',
				vanNumb: '',
				rangeStart: 1,
				newInventoryAgreement:{
					originalCost: '',
					originalWeight: '',
					addInventoryCost: '',
					totalInventoryCost: ''
				}

			},
			secondAddInventoryMoving: {
				open: false,
				pageNumber: 1,
				numberOfPages: 1,
				nosFrom: 1,
				numberThru: 1,
				tapeColor: '',
				signatures: {
					length: 0
				},
				currentStep: 0,
				numberedItems: 0,
				tapeNumbers: 0,
				isSubmitted: false,
				inventory: {},
				remarks: '',
				contractGbl: '',
				govtService: '',
				vanNumb: '',
				rangeStart: 1,
				newInventoryAgreement:{
					originalCost: '',
					originalWeight: '',
					addInventoryCost: '',
					totalInventoryCost: ''
				}

			},
			agreement:{
				signatures: {},
				phone: '',
				address: '',
				zipCode: '',
				state: '',
				city:''
			},
			additionalContractPages: [],
			additionalContractPagesSignature: -1,
			indexOfLastAdditionalPage: -1,
			mainContractPagePdf: {},
			backSidePagePdf: {},
			customContractSignature: {
				currentCustomType: '',
				currentSignature: '',
				customContent1: {
					customContent1Signature0: {}
				},
				customContent2: {
					customContent2Signature0: {}
				},
				customContent3: {
					customContent3Signature0: {}
				}
			},
			mainContractPDFId: '',
			isFirstSubmittedPage: false,
			isFirstAddMainId: -1,
			tabs: [],
			laborOnly: false,
		};

	}]);
