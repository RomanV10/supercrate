'use strict';

angular.module('move.requests')
	.value('ContractPageData', {
		contractPageOriginDestination : {
			apt: 'apt:',
			city: 'city:',
			customerName: 'Customer Name:',
			elevator: 'elevator:',
			firstCellPhone: 'Cell Phone 1',
			flightsOrigin: 'flights at origin:',
			secondCellPhone: 'Cell phone 2',
			state: 'state:',
			street: 'street:',
			zip: 'zip:'
		},
		contractPageEditableFields: {
			declarationValue: 'declaration of value',
			detailsLaborCharges: 'details of labor charges',
			localHourlyRatesOption: 'Local hourly rates option',
			longDistanceFlatRatesOption: 'Long distance flat rates option',
			reservationReceivid: 'Reservation Received:',
			servicesTitle: 'storage in transit (sit)',
			totalBalancePaid: 'Total balance paid:',
			totalCost: 'Total cost:',
			totalLessDepositReceived: 'Total less deposit received:'
		}
	});
