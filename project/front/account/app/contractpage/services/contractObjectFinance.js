'use strict';

angular.module('contractModule')
	.service('ContractObjectFinance', [ function () {
		this.finance = {
			requestTips: 0,
			requestTipsPaid: false,
			requestTipsPickup: 0,
			requestTipsPickupPaid: false,
			tip: 0,
			discount: 0,
			moneyDiscount: 0,
			percentDiscount: 0,
			workDuration: 0,
			totalCost: 0,
			balanceDueAtDelivery: 0,
			packingSum: 0,
			serviceSum: 0,
			deposit: 0,
			fixedTotal: 0, //this for flat fee
			paymentType: 'LaborAndTravel',
			valuation: 0,
			fuel: 0,
			creditTax: 0,
			cashDiscount: 0,
			creditTaxPickup: 0,
			cashDiscountPickup: 0,
			pickupTips: 0,
			customPayments: {},
			customTax: 0
		};

	}]);
