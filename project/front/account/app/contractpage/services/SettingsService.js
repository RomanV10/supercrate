/**
 * Settings service
 */
angular.module('contractModule')
       .factory('SettingsService', SettingsService);

SettingsService.$inject = ['$q', '$http', 'config', 'datacontext', 'ContractValue'];

function SettingsService($q, $http, config, datacontext, ContractValue) {
    let service = {};
    const moveBoardContractFields = ContractValue.moveBoardContractFields;

    service.saveSettings = saveSettings;
    service.getSettings = getSettings;
    service.saveAccountContractSettings = saveAccountContractSettings;

    function saveSettings(settings, type) {
        let data = {
			name: type,
			value: angular.toJson(settings)
		};
        return $http.post(`${config.serverUrl}server/settings/set_variable`, data);
    }

    function getSettings(type) {
        return datacontext.getSettings(type);
    }

    function saveAccountContractSettings(data) {
        let deferred = $q.defer();
        let contract_page = data;
        let getSettings = {};
        service.getSettings('contract_page')
			.then((data) => {
				if (_.isArray(data) && !_.isEmpty(data)) {
					data = _.head(data);
				}

				getSettings = angular.fromJson(data);
				_.forEach(getSettings, function (val, field) {
					if(_.indexOf(moveBoardContractFields, field) == -1) {
						getSettings[field] = contract_page[field];
					}
				});

				deferred.resolve(getSettings);
        	}).catch((err) => deferred.reject(err));

        return deferred.promise;
    }

	return service;
}
