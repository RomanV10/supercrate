'use strict';

angular.module('contractModule')
   .factory('HttpService', HttpService);

    HttpService.$inject = ['$http', 'config'];

    function HttpService($http, config) {
        return {
            getExtraServices(type) {
                return $http.post(`${config.serverUrl}server/system/get_variable`, {name: type});
            },
            getSignatures(nid) {
                return $http.post(`${config.serverUrl}server/move_request/get_contract/${nid}`);
            },
            saveContract(data, nid) {
                return $http.post(`${config.serverUrl}server/move_request/set_contract`, {nid: nid, value: data});
            },
            savePayment(nid,value) {
                return $http.post(`${config.serverUrl}server/move_request/set_payment`, {nid: nid, data: value});
            }
        }

    }
