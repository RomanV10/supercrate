(function() {
    'use strict';

    angular.module('contractModule')
        .factory('ContractCalc', ContractCalc);

	/*@ngInject*/
    function ContractCalc (ContractFactory, CalculatorServices, InventoriesServices, datacontext,
						   ContractObjectFinance, laborService, accountConstant) {
        var service = {};
        var factory = ContractFactory.factory;
        var request = null;
        var isFlatRate = false;
        var fieldData = datacontext.getFieldData(),
            contract_page = angular.fromJson(fieldData.contract_page),
            calcSettings = angular.fromJson(fieldData.calcsettings);
        var isDoubleDriveTime = calcSettings.doubleDriveTime,
            travelTime = {};

        var travelTimeSetting = calcSettings.travelTime;

        const HUNDRED_PERCENT = 100,
            PICK_UP_PERCENT = contract_page.pickUpPercent,
            MIN_HOUR_RATE = parseFloat(calcSettings.min_hours);

        let additionalCrews;

        service.finance = ContractObjectFinance.finance;
        service.financeObj = angular.copy(ContractObjectFinance.finance);

        service.calculateWork = calculateWork;
        service.getWorkingHours = getWorkingHours;
        service.totalFlatRateCost = totalFlatRateCost;
        service.totalClosingFlatRateCost = totalClosingFlatRateCost;
        service.getRate = getRate;
        service.getMonthlyStorageRate = getMonthlyStorageRate;
        service.canCalc = canCalc;
        service.totalHourlyCharge = totalHourlyCharge;
        service.totalCost = totalCost;
        service.totalCostForFlatRate = totalCostForFlatRate;
        service.initData = initData;
        service.calculateDurationTime = calculateDurationTime;
        service.calculateTotalWithoutTaxDiscount = calculateTotalWithoutTaxDiscount;
        service.calcCustomPayments = calcCustomPayments;
        service.getCrewTotalDuration = getCrewTotalDuration;

        return service;

        function initData (props) {
            if (props.hasOwnProperty('finance')) {
            	let copyFinance = angular.copy(props.finance);

            	delete copyFinance.paymentType;

            	angular.forEach(copyFinance, function(propertyValue, key) {
                    if (_.has(service.finance, key)) {
                    	service.finance[key] = propertyValue;
                    }
                });
            }

            if (props.hasOwnProperty('request')) {
                request = props.request;
                travelTimeSetting = angular.isDefined(request.request_all_data.travelTime) ? request.request_all_data.travelTime : calcSettings.travelTime;
                travelTime = props.travelTime;
                isFlatRate = request.service_type.raw == 7  || request.service_type.raw == 5;
                if (request.request_all_data.hasOwnProperty('add_money_discount')) {
                    service.finance.moneyDiscount = request.request_all_data.add_money_discount;
                }
                if (request.request_all_data.hasOwnProperty('add_percent_discount')) {
                    service.finance.percentDiscount = request.request_all_data.add_percent_discount;
                }
				if(_.get(request.request_all_data.invoice, 'request_all_data', false)) {
					if (request.request_all_data.invoice.request_all_data.hasOwnProperty('add_money_discount')) {
						service.finance.moneyDiscount = request.request_all_data.invoice.request_all_data.add_money_discount;
					}
					if (request.request_all_data.invoice.request_all_data.hasOwnProperty('add_percent_discount')) {
						service.finance.percentDiscount = request.request_all_data.invoice.request_all_data.add_percent_discount;
					}
				}
                if (request.request_data.value != ''){
                    var request_data = angular.fromJson(request.request_data.value);
                    if (request_data.paymentType && request_data.paymentType.length) {
                        service.finance.paymentType = request_data.paymentType;
                    }

					additionalCrews = _.get(request_data, 'crews.additionalCrews');
                }
                service.finance.deposit = getDeposit();
            }
            if (_.has(props, 'factory')) {
            	var f = angular.copy(props.factory);
                if (f.crews.length) {
                    for (var i = 0; i < f.crews.length; i++) {
                        calculateWork(f.crews[i]);
                    }
                }
            }
        }

        function getWorkingHours(crew) {
            let time = getTime();
            var startFrom = moment.unix(((factory.signatures[0].date || factory.signatures[1].date)/ 1000).toFixed(0));
            var endTo = moment.unix((factory.signatures[2].date / 1000).toFixed(0));
            var m = endTo.diff(startFrom, 'minutes');
            var h = Number((m / 60).toFixed(accountConstant.roundUp));

            var request_data = angular.fromJson(request.request_data.value);
            if (request_data.paymentType) {
                var paymentType = request_data.paymentType[crew];
                if (paymentType == 'FlatFee') return false;

                else if (paymentType == 'LaborAndTravel') {
                    h += time;
                }
            }
            return h;
        }

        function getRate (date, helpers, trucks) {
            return CalculatorServices.getRate(date, helpers, trucks);
        }

        function getMonthlyStorageRate () {
           return CalculatorServices.getStorageRate(request , 30);
        }

        function totalHourlyCharge () {
            let total = 0;
            let duration = 0;
            let time = getTime();

            if (canCalc() && !isFlatRate && factory.crews.length) {
                let finalRateCrew = 0;

				for (let i = 0; i < factory.crews.length; i++) {
					finalRateCrew = laborService.getCrewRate(i, factory.crews, additionalCrews);
					duration += factory.crews[i].workDuration;
					total += finalRateCrew * factory.crews[i].workDuration;
				}

				duration += time;
				total += finalRateCrew * time;

				if (duration < MIN_HOUR_RATE) {
					total += finalRateCrew * (MIN_HOUR_RATE - duration);
				}
            }

            return parseFloat(total.toFixed(accountConstant.roundUp));
        }

        function calculateTotalWithAllServices(total) {
			let s = service.finance;
			let totalCost = total;
			totalCost += _.round(Number(s.fixedTotal + s.serviceSum + s.packingSum - s.discount - s.moneyDiscount + s.valuation + s.fuel), accountConstant.roundUp);
            totalCost -= (totalCost * s.percentDiscount / HUNDRED_PERCENT);

			return totalCost;
		}

        function calculateTotalWithoutTaxDiscount() {
            let totalCost = 0;
			if(request) {
				if (request.service_type.raw == 5 || request.service_type.raw == 7) {
					totalCost = calculateTotalCostFlatRateLongDistance();
				} else if(canCalc()) {
					totalCost = Number(totalHourlyCharge());
                }
			}
			totalCost = calculateTotalWithAllServices(totalCost);

			return totalCost;
		}

        function totalCost () {
            let { totalCost, creditTax, cashDiscount, tip } = service.finance;
			totalCost = calculateTotalWithoutTaxDiscount();
            if(_.get(contract_page, 'paymentTax.customTax.state', false)) {
                service.finance.customTax = _.round(totalCost*(contract_page.paymentTax.customTax.value / HUNDRED_PERCENT), accountConstant.roundUp);
                totalCost += service.finance.customTax;
            }

            totalCost += calcCustomPayments();
            totalCost = totalCost + creditTax - cashDiscount;
            totalCost += tip;

            return totalCost;
        }

        function calcHalfOfTotal(total, s, divisor){
            let totalCost = parseFloat((total).toFixed(accountConstant.roundUp));
			totalCost = calculateTotalWithAllServices(totalCost);
            if(_.get(contract_page, 'paymentTax.customTax.state', false)) {
                service.finance.customTax = _.round(totalCost*(contract_page.paymentTax.customTax.value / HUNDRED_PERCENT), accountConstant.roundUp);
                totalCost += service.finance.customTax;
            }

            totalCost = parseFloat((totalCost * divisor / HUNDRED_PERCENT).toFixed(accountConstant.roundUp));

            if(divisor == PICK_UP_PERCENT){
                if(s.pickupTips)
                    totalCost += s.pickupTips;
                totalCost = totalCost + s.creditTaxPickup - s.cashDiscountPickup;
            }else{
                totalCost = totalCost + s.creditTax - s.cashDiscount;
                totalCost += s.tip;
            }

            return parseFloat(totalCost.toFixed(accountConstant.roundUp));
        }

        function calculateTotalCostFlatRateLongDistance() {
			return calcHelperFlatRateLongDistance('totalCost');
		}

		 function calculateTotalClosingCostFlatRateLongDistance() {
			 return calcHelperFlatRateLongDistance();
		 }

        function totalFlatRateCost (isPickupSubmitted) {
			let totalCost = calculateTotalCostFlatRateLongDistance();
            let {finance} = service;
            if (request) {
                if(request.service_type.raw == 5 && request.request_all_data && request.request_all_data.invoice && !request.field_flat_rate_local_move.value){
                    if(!isPickupSubmitted) totalCost = calcHalfOfTotal(totalCost, finance, PICK_UP_PERCENT);
                    if(isPickupSubmitted) totalCost = calcHalfOfTotal(totalCost, finance, HUNDRED_PERCENT);
                }
                else totalCost = calcHalfOfTotal(totalCost, finance, HUNDRED_PERCENT);
                finance.totalCost = totalCost;
            }
            totalCost += calcCustomPayments();

			return totalCost;
		}

		function totalClosingFlatRateCost () {
			let totalCost = calculateTotalClosingCostFlatRateLongDistance();
			let {finance} = service;
			if(request) {
				totalCost = calcHalfOfTotal(totalCost, finance, HUNDRED_PERCENT);
				finance.totalCost = totalCost;
            }
            totalCost += calcCustomPayments();

			return totalCost;
		}

        function totalCostForFlatRate () {
			return calcHalfOfTotal(calculateTotalCostFlatRateLongDistance(), service.finance, HUNDRED_PERCENT);
		}

        function calculateDurationTime(startTime, stopTime, offTime){
            let timeOff = 0;
            let dateRaw = startTime.split(' ');
			let [startHours, startMinutes] = getHoursMinutes(dateRaw);
            let start = new Date();
            start.setHours(startHours);
            start.setMinutes(startMinutes);
            start.setSeconds(0);
            dateRaw = stopTime.split(' ');
			let [endHours, endMinutes] = getHoursMinutes(dateRaw);
            let end = new Date();
            end.setHours(endHours);
            end.setMinutes(endMinutes);
            end.setSeconds(0);
            if (offTime != 0) {
                var h = Number(offTime.split(':')[0]) || 0,
                    m = Number(offTime.split(':')[1]) || 0;
                if (h || m) {
                    let msInHours = 0;
                    let msInMinutes = 0;
                    if (h) {
                    	msInHours = h * 60 * 60 * 1000;
					}
                    if (m) {
                        msInMinutes = m * 60 * 1000;
                    }

					timeOff = msInHours + msInMinutes;
                }
            }
            if (end.getTime() <= start.getTime()) {
                end = end.setHours(end.getHours() + 24);
            }
            let duration =  end - start;
            return parseFloat((Number((duration - timeOff)/ 1000 / 60 / 60)).toFixed(accountConstant.roundUp));
        }

        function calculateWork(crew) {
            crew.workDuration = 0;
            if (crew.timer.stop && crew.timer.start) {
                 var timeOff = fixedTime(crew.timer.timeOff);
                 crew.workDuration = calculateDurationTime(crew.timer.start, crew.timer.stop, timeOff);
            }else if(crew.timer.startPickUp && crew.timer.stopPickUp && crew.timer.startUnload && crew.timer.stopUnload){
                var timeOff = 0;
                var pickUpTime = calculateDurationTime(crew.timer.startPickUp, crew.timer.stopPickUp, 0);
                var unloadTime = calculateDurationTime(crew.timer.startUnload, crew.timer.stopUnload, 0);
                if (crew.timer.timeOff != 0) {
                    var h = Number((crew.timer.timeOff.split(':')[0]).slice(-2)) || 0,
                        m = Number((crew.timer.timeOff.split(':')[1]).substring(0, 2)) || 0;
                    if (h || m) {
                        timeOff =  h + m/60;
                    }
                }
                crew.workDuration = parseFloat((pickUpTime + unloadTime - timeOff).toFixed(accountConstant.roundUp));
            }

        }

        function getDeposit () {
            if (request) {
                let deposit = 0;
                angular.forEach(request.receipts, function (receipt) {
                    if (receipt.payment_flag != 'contract' && !receipt.pending) {
						if (receipt.refunds) {
							deposit -= _.round(receipt.amount, accountConstant.roundUp);
						} else {
							deposit += _.round(receipt.amount, accountConstant.roundUp);
						}
					}
				});

                return deposit;
            }
        }

		function canCalc () {
			if (request) {
				let currentCrewTimer = factory.crews[0].timer;

				return currentCrewTimer.start && currentCrewTimer.stop
					|| currentCrewTimer.startPickUp && currentCrewTimer.stopPickUp && currentCrewTimer.startUnload && currentCrewTimer.stopUnload;
			}
		}

        function fixedTime(data){
            if(data == 0)
                return data;
            var valid = data.split(':');

            if(2 == valid.length){
                var h,m;
                h = valid[0].slice(-2);
                m = valid[1].substring(0, 2);

                return h+":"+m;
            }
        }

        function calcCustomPayments() {
            let customPaymentsTotal = 0;
            if (_.get(service, 'finance.customPayments') && !_.isEmpty(service.finance.customPayments)) {
                for (let payment in service.finance.customPayments) {
                    customPaymentsTotal += service.finance.customPayments[payment].value;
                }
            }

            return customPaymentsTotal;
        }

		function calcHelperFlatRateLongDistance(subType) {
			let totalCost = 0;
			if (request) {
				if(request.service_type.raw == 5) {
					totalCost  = Number(request.flat_rate_quote.value);
					if(request.request_all_data.invoice && request.request_all_data.invoice.flat_rate_quote && request.request_all_data.invoice.flat_rate_quote.value) {
						totalCost  = Number(request.request_all_data.invoice.flat_rate_quote.value);
					}
				}
				if(request.service_type.raw == 7) {
					if('totalCost' == subType) {
						request.inventory_weight = InventoriesServices.calculateTotals(request.inventory.inventory_list);
					}
					totalCost = CalculatorServices.getLongDistanceQuote(request.request_all_data.invoice, true);
				}
			}

			return totalCost;
		}

		function getTime(laborOnly) {
        	if (!laborOnly && (isDoubleDriveTime || travelTimeSetting)) {
        		let time = +travelTime.raw || 0;
		        time = CalculatorServices.roundTimeInterval(time);
				return time;
			} else {
        		return 0;
			}
		}

		function getHoursMinutes(dateRaw) {
			let dateTime = dateRaw[0].split(':');
			let dateAmPm = dateRaw[1].toLowerCase();

			if(dateAmPm == 'pm' && parseInt(dateTime[0]) < 12) {
				dateTime[0] = (parseInt(dateTime[0])+12).toString();
			} else if(dateAmPm != 'pm' && parseInt(dateTime[0]) == 12) {
				dateTime[0] = 0;
			}

			return [dateTime[0], dateTime[1]];
		}

		function getCrewTotalDuration(laborOnly, addMainPage) {
			let totalDuration = 0;

			if (canCalc() && !isFlatRate || addMainPage) {
				let travelTime = getTime(laborOnly);
				totalDuration = laborService.getAllCrewDuration(factory.crews, travelTime);
			}

			return totalDuration;
		}
    }
})();
