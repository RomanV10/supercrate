'use strict';

angular.module('contractModule')
	.value('ContractValue', {
		oldReleaseFormObj: {
			itemsRelease: {},
			releaseInitials: {},
			isSubmitted: false,
			currentStep: ''
		},

		navigation: {
			pages: ['Confirmation Page',
				'Bill of lading',
				'Rental agreement',
				'Inventory',
				'Add. Inventory',
				'Add. Inventory',
				'Photos',
				'Release form',
				'Review'],
			active: 0,
			activeCrew: 0
		},

		extra_data: {
			selectedPackings: [],
			services: {} //all services
		},

		stepsData: {
			startTimeStep: 3,
			stopTimeStep: 4
		},

		flatStepsData: {
			pickupStartTimeStep: 3,
			pickupStopTimeStep: 4,
			unloadStartTimeStep: 5,
			unloadStopTimeStep: 6
		},

		tabs: {
			contract: {
				selected: true,
				name: 'Contract Settings'
			},
			ownPages:{
				selected: false,
				name: 'Additional Pages'
			},
			releaseForm:{
				selected: false,
				name: 'Release Form'
			},
			rentalAgreement: {
				selected: false,
				name: 'Storage Agreement'
			},
			terms: {
				selected: false,
				name: 'Terms and Conditions'
			}
		},

		rentalAgreement: {
			noticeOfLien: '',
			paymentSettings: [
				{
					selected:false,
					name: 'Check',
					alias: 'checkPayment'
				}, {
					selected:false,
					name: 'Online Payment',
					alias: 'creditPayment'
				},{
					selected:false,
					name: 'Cash',
					alias: 'cashPayment'
				}
			]
		},

		signatureLabelsLocalMove: [
			'Shipper or agent has signed at the origin',
			'Customer has signed the local hourly rates option',
			'Shipper has signed the declaration of value',
			'Customer has signed',
			'Customer has signed the contract',
			'Carrier has signed the contract'
		],

		signatureLabelsFlatRate: [
			'Shipper or agent has signed at the origin',
			'Customer has signed the long distance flat rate option',
			'Shipper has signed the declaration of value',
			'Customer has signed the pickup',
			'Carrier has signed the pickup',
			'Customer has signed the delivery',
			'Carrier has signed the delivery'
		],

		permission: false,
		nid: 0,
		isFamiliarization: false,
		isPrint: false,
		sericesArr: [1, 2, 3, 4, 6, 8],
		defaultActiveTab: 0,
		defaultMainTabData: {
			name: 'Main contract page',
			selected: true,
			show: true,
			page: '',
			removedPage: false,
			mainContract: false
		},
		packingServiceData: {
			LM: {
				full: 0.5,
				partial: 50
			},
			LD: {
				full: 0.7,
				partial: 44
			}
		},
		crewObj: {
			timer: {
				start: 0,
				stop: 0,
				timeOff: 0,
				lastAction: '',
				startPickUp: 0,
				stopPickUp: 0,
				startUnload: 0,
				stopUnload: 0,
			},
			workDuration: 0,
			rate: 0
		},
		entityTypes: {
			MOVEREQUEST: 0,
			STORAGEREQUEST: 1,
			LDREQUEST: 2,
			SYSTEM: 3
		},
		moveBoardContractFields: [
			'minPayrollHours',
			'pushTips',
			'lessInitialContract',
			'goToReview',
			'useInventory',
			'paymentTypes',
			'paymentOptions'
		],
		storageFit: [
			{
				"active":true,
				"connected_service":"overnight_storage",
				"text":"I understand that storage fees will be an additional $%input_number_value% per night if stored on one of our vehicles + an additional trip charge. %initial_input% ",
				"show":true,
				"unremovable": true
			},
			{
				"active":true,
				"connected_service":"monthly_storage_fee",
				"text":"I understand that if this shipment must be placed in storage facility , the hourly rate will continue through the unload." +
				"I understand that %COMPANY NAME% monthly storage fee $%monthly_total% for volume %volume_cuft%  due by the 10th of each month (by credit card only) minimum storage fee is two (2) weeks, moving into storage or moving out of storage. %initial_input% ",
				"show":true,
				"unremovable": true
			}
		],
		settingsMenu: {
			COMPANY_NAME: {
				title: 'Company name',
				value: '%COMPANY NAME%'
			},
			NUMBER_INPUT: {
				title: 'Number input',
				value: '%input_number_value%'
			},
			INITIAL_INPUT: {
				title: 'Initial input',
				value: '%initial_input%'
			},
			MONTHLY_TOTAL: {
				title: 'Monthly Total',
				value: '%monthly_total%'
			},
			VOLUME_CUFT: {
				title: 'Volume, cuft',
				value: '%volume_cuft%'
			}
		},
		storage: {
			storageTotal: 0,
			storageVolume: 0,
			storageRateCuft: 0
		},
		requestVariable: [
			'rate_per_hour',
			'request_id',
			'address_from',
			'address_to',
			'city_from',
			'city_to',
			'address_pickup',
			'address_dropoff',
			'city_pickup',
			'city_dropoff',
			'%state_dropoff%',
			'%state_pickup%',
			'%zip_dropoff%',
			'%zip_pickup%',
			'%state_to%',
			'%state_from%',
			'%zip_to%',
			'%zip_from%',
			'crew_number',
			'truck_number'
		],
		contractVariable: [
			'start_time',
			'end_time',
			'time_off',
			'labor_time',
			'travel_fee',
			'contract_total',
			'fuel_surcharge',
			'valuation',
			'tips',
			'balance_due',
			'flat_rate_quote'
		],
		hundredPercent: 100,
		couponTypes: [
			{
				name: 'Yelp Coupon',
				value: 'yelp'
			},
			{
				name: 'Website Coupon',
				value: 'website'
			},
			{
				name: 'Manager',
				value: 'manager'
			}
		],
		alphabetNumber: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
		foremanMaxRateBefore: 31,
		forbiddenStatus: 403
	});
