'use strict';

angular.module('contractModule')
	.factory('arrivyService', arrivyService);

arrivyService.$inject = ['apiService', 'moveBoardApi'];

function arrivyService(apiService, moveBoardApi) {
	let service = {};
	let startArrivyStatus = 'STARTED';
	let completeArrivyStatus = 'COMPLETE';

	service.sendStatus = sendStatus;

	function sendStatus(data, nid, flatMiles) {
		if(data.factory && data.factory.currentStep === '0'){
			sendStart(nid);
		}
		if(flatMiles && data.factory.currentStep === '7' || !flatMiles && data.factory.currentStep === '5'){
			sendComplete(nid);
		}
	}

	function sendStart(requestId) {
		let data = {
			request_id: requestId,
			status_type: startArrivyStatus
		};
		sendRequest(data);
	}

	function sendComplete(requestId) {
		let data = {
			request_id: requestId,
			status_type: completeArrivyStatus
		};
		sendRequest(data);
	}

	function sendRequest(data){
		apiService.postData(moveBoardApi.arrivy.sendTaskStatus, data);
	}

	return service;
}
