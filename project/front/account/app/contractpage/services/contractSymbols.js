'use strict';

angular.module('contractModule')
	.value('ContractSymbols', {
		descreptiveSymbols: [
			{key: "B/W", name: 'black & white tv'},
			{key: "C", name: 'color tv'},
			{key: "CP", name: 'carrier packed'},
			{key: "PBO", name: 'packed by owner'},
			{key: "CD", name: 'carrier disassembled'},
			{key: "SW", name: 'switch wrappered'},
			{key: "DBO", name: 'disassembled by owner'},
			{key: "PB", name: 'professional books'},
			{key: "PE", name: 'professional equipment'},
			{key: "PP", name: 'professional papers'},
			{key: "MCU", name: 'mechanical condition unknown'}
		],

		exceptionSymbols: {
			"BE": 'bent',
			"BR": 'broken',
			"BU": 'burned',
			"CH": 'chipped',
			"CU": 'contents & condition unknown',
			"D": 'dented',
			"F": 'faded',
			"G": 'gouged',
			"L": 'loose',
			"M": 'married',
			"MI": 'mildew',
			"MO": 'motheaten',
			"P": 'peeling',
			"R": 'rubbed',
			"RU": 'rusted',
			"SC": 'scratched',
			"SH": 'short',
			"SO": 'soiled',
			"ST": 'stained',
			"S": 'streitched',
			"T": 'torn',
			"W": 'badly worn',
			"Z": 'cracked'
		},

		locationSymbols: [
			{key: '1', val: 'arm'},
			{key: '2', val: 'bottom'},
			{key: '3', val: 'corner'},
			{key: '4', val: 'front'},
			{key: '5', val: 'left'},
			{key: '6', val: 'legs'},
			{key: '7', val: 'pear'},
			{key: '8', val: 'right'},
			{key: '9', val: 'side'},
			{key: '10', val: 'top'},
			{key: '11', val: 'veneer'},
			{key: '12', val: 'edge'},
			{key: '13', val: 'center'},
			{key: '14', val: 'inside'},
			{key: '15', val: 'seat'},
			{key: '16', val: 'drawer'},
			{key: '17', val: 'door'},
			{key: '18', val: 'shelf'},
			{key: '19', val: 'hardware'}
		]
	});
