!function() {

    let app = angular.module('contractModule');

    /**
     * Flat rate and long distance directive
     */

    app.directive("flatRateMoves", flatRateMoves);

	/*@ngInject*/
    function flatRateMoves(ContractFactory, ContractService, ContractCalc, $rootScope, RequestServices, common,
						   datacontext, SweetAlert, logger, $uibModal, contractConstant, ContractValue, contractDispatchLogs) {

        return {
                restrict: 'E',
				scope: {
					request: '=',
					balance_paid: '=balancePaid',
					services: '=',
					additionalServices: '=',
					maxSignCount: '=',
					navigation: '=',
					imContract: '<',
					loadCustom: '<',
					showDeclarVal: '<',
					declarationValue: '=',
					requestReady: '<',
					openPay: '<',
					tabs: '<',
					busy: '=',
					grandTotalLog: '=',
					saveContract: '&',
					mainFlipPage: '&flipPage',
					getTips: '&',
					saveReqDataInvoice: '&',
					showPayment: '&',
					applyPayment: '&',
					sendLetterPdf: '&',
					downloadPdfPage: '&',
				},
                templateUrl: 'app/contractpage/directives/flat-rate/flatRate.html',
                controller: flatRateCtrl
            };

            function flatRateCtrl ($scope) {
				$scope.totalFlatRate = totalFlatRate;
                $scope.totalFlatRateClosing = totalFlatRateClosing;
                $scope.submitContract = submitContract;
                $scope.totalLessDeposit = totalLessDeposit;
                $scope.showFirstSinature = showFirstSinature;
                $scope.flipPage = flipPage;
                $scope.paymentButton = paymentButton;
                $scope.sendContractLog = sendContractLog;
                $scope.submitContractBtn = submitContractBtn;
                $scope.showTransit = showTransit;
                $scope.showService = showService;
                $scope.changeTip = changeTip;
                $scope.openService = openService;

				$scope.fieldData = datacontext.getFieldData();
				$scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);
				$scope.contract_page = angular.fromJson($scope.fieldData.contract_page);
				$scope.content = $scope.contract_page.textContent;
				$scope.showDiscount = false;
				$scope.data = ContractFactory.factory;
				$scope.finance = ContractCalc.finance;
				$scope.cardImages = [];
				$scope.countBasePayment = contractConstant.countBasePayment;
				$scope.minLengthCustomBlock = 7;
				$scope.isFamiliarization = ContractValue.isFamiliarization;
				$scope.isPrint = ContractValue.isPrint;
				$scope.storageTotal = ContractFactory.storage.storageTotal;
				$scope.storageVolume = ContractFactory.storage.storageVolume;
				$scope.storageRateCuft = ContractFactory.storage.storageRateCuft;
				$scope.isAdminManager = ContractFactory.isAdminManager;

				const HUNDRED_PERCENT = 100;
				const PICK_UP_PERCENT = $scope.contract_page.pickUpPercent;
				const SIGNATURE_PREFIX = 'data:image/gif;base64,';
				let pickupPayrollReq = null;

                angular.element(document).ready(function(){
                    ContractFactory.init();
                });

                ContractService.getContractInfo($scope.request.nid)
                    .then (function(data) {
                        pickupPayrollReq = data;
                    });
                ContractService.getCardImage($scope.request.nid)
                    .then (function(data) {
                        $scope.cardImages = data;
                    });

                function changeTip() {
                    if(!$scope.contract_page.pushTips) {
                    	$scope.finance.pickupTips = parseFloat((parseFloat($scope.finance.tip) * PICK_UP_PERCENT / HUNDRED_PERCENT).toFixed(2));
					}
                }

                function showService(type) {
                    if(type == 'monthly_storage_fee') return true;
                    var service = _.find($scope.content.storageFit, {connected_service: type});
                    if($scope.services)
                        if($scope.services[type]) return false;
                    return _.get(service, 'show', false);
                }

                function openServiceModal(service, step) {
                    var connectedServiceInstance = $uibModal.open({
                        templateUrl: 'app/contractpage/directives/storage-fit/storageFit.html',
                        controller: connectedServiceCtrl,
                        size: 'lg',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            storageTotal: function () {
                                return $scope.storageTotal;
                            },
                            storageVolume: function () {
                                return $scope.storageVolume;
                            },
                            storageRateCuft: function () {
                                return $scope.storageRateCuft;
                            },
                            service: function () {
                                return service;
                            },
                            step: function () {
                                return step;
                            },
                            services: function () {
                                return $scope.additionalServices;
                            },
                            request: function () {
                                return $scope.request;
                            }
                        }
                    });
                }

                function openService(type) {
                	if ($scope.data.isSubmitted) {
                		return false;
					}

                    let service = _.find($scope.content.storageFit, {connected_service: type});
					let step = type == 'monthly_storage_fee' ? 0 : 1;
					openServiceModal(service, step);
                }

                function connectedServiceCtrl($scope, $rootScope, $uibModalInstance, storageTotal, storageVolume, storageRateCuft, service, step, services, request, StorageService, ContractFactory) {
                    $scope.storageTotal = storageTotal;
                    $scope.storageVolume = storageVolume;
                    $scope.storageRateCuft = storageRateCuft;
                    $scope.services = services;
                    $scope.service = service;
                    $scope.request = request;
                    $scope.steps = [false, false];
                    $scope.storage = false;
                    if(step == 1) goSecondStep();
                    if(step == 0){ $scope.storage = true; $scope.steps[0] = true;}

                    if(step == 0)
                        StorageService.getDefaultStorage().then(function ({data}) {
                            if(!data.id || !data.value) {
                                SweetAlert.swal("Failed! No default storages", '', "error");
                                return false;
                            }
                            $scope.storageRateCuft = Number(data.value.rate_per_cuft);
                            $scope.storageMinCuft = Number(data.value.min_cuft);
                        });
                    var getCropedSignatureValue = function () {
						return ContractFactory.getSignatureUrl($scope.signaturePad._ctx, true);
                    };

                    function isCanvasBlank(elementID) {
                        var canvas = document.getElementById(elementID);
                        var blank = document.createElement('canvas');
                        blank.width = canvas.width;
                        blank.height = canvas.height;
                        var context = canvas.getContext("2d");
                        var perc = 0, area = canvas.width * canvas.height;
                        if (canvas.toDataURL() == blank.toDataURL()) {
                            return canvas.toDataURL() == blank.toDataURL();
                        } else {
                            var data = context.getImageData(0, 0, canvas.width, canvas.height).data;
                            for (var ct = 0, i = 3, len = data.length; i < len; i += 4) if (data[i] > 50) ct++;
                            perc = parseFloat((100 * ct / area).toFixed(2));
                            return (perc < 0.1);
                        }
                    }

                    $scope.goSecondStep = goSecondStep;
                    $scope.returnStep = returnStep;
                    $scope.changeVolume = changeVolume;
                    $scope.saveService = saveService;
                    $scope.cancel = cancel;

                    function goSecondStep() {
                        if(!$scope.storageVolume && step == 0){
                            $scope.errorStorageVolume = true;
                            return false;
                        }
                        setTimeout(function () {
                            $scope.canvas = document.getElementById('signatureCanvasService');
                            $scope.signaturePad = new SignaturePad($scope.canvas);
                            $scope.$apply();
                        }, 1000);
                        $scope.steps[0] = false;
                        $scope.steps[1]=true;
                    }

                    function returnStep(stepInd) {
                        _.forEach($scope.steps, function (item, ind) {
                            $scope.steps[ind] = false;
                        });
                        $scope.steps[stepInd] = true;
                    }

                    function changeVolume() {
                        if(Number($scope.storageMinCuft) < Number($scope.storageVolume)) $scope.storageTotal = Number(($scope.storageVolume * $scope.storageRateCuft).toFixed(2));
                        else $scope.storageTotal = Number((Number($scope.storageMinCuft) * $scope.storageRateCuft).toFixed(2));
                    }

                    function saveService() {
                        var signatureValue = getCropedSignatureValue();
                        var signature = {
                            value: SIGNATURE_PREFIX + signatureValue,
                            date: moment().format('x')
                        };
                        var blank = isCanvasBlank('signatureCanvasService');
                        if(blank){
                            SweetAlert.swal("Failed!", 'Sign please', "error");
                        }else{
                            $scope.busy = true;
                            $rootScope.$broadcast('save.service', { service: $scope.service, signature: signature, request: $scope.request, data:{volume: $scope.storageVolume, total: $scope.storageTotal}});
                        }
                    }

                    function cancel() {
                        $uibModalInstance.dismiss('cancel');
                    }

                    $scope.$on('close.modal.service', function () {
                        $scope.cancel();
                    })
                }

                function paymentButton(){
                    return (parseFloat(totalLessDeposit()) == 0)
                }

                function sendContractLog(msgToLog) {
                    var data = [];
                    var totalcost = {
                        text: 'Total cost changed',
                        from: '$'+ (Number($scope.grandTotalLog)).toFixed(2),
                        to: '$'+ (Number($scope.totalFlatRateClosing())).toFixed(2)
                    };
                    var newMsg = {
                        text: msgToLog
                    };
                    if(_.isArray(msgToLog)){
                        data.push(totalcost);
                        _.forEach(msgToLog, function (log) {
                            data.push(log);
                        });
                    }else{
                        data.push(newMsg, totalcost);
                    }
                    RequestServices.sendLogs(data, 'Contract changed', $scope.request.nid, 'MOVEREQUEST').then(function () {
                        $scope.grandTotalLog = $scope.totalFlatRateClosing();
                    });
                }

                function flipPage() {
                    $scope.mainFlipPage();
                    $('body').scrollTop(0);
                }

                function showFirstSinature(){
                    if($scope.contract_page.first_signature === false) return false;
                    if(angular.isDefined($scope.contract_page.first_signature) && $scope.contract_page.first_signature){
                        return $scope.contract_page.lessInitialContract ? false : true;
                    }else {
                        return $scope.contract_page.lessInitialContract ? false : true;
                    }
                }

                function totalLessDeposit() {
                    let total = 0;

                    if (common.count($scope.data.signatures) > 1 && angular.isDefined($scope.data.signatures[2])) {
                        total = parseFloat((totalFlatRate() - $scope.finance.deposit - $scope.balance_paid).toFixed(2));
                        if (total < 1 && total > 0) {
							total = 1;
						}
                    }

                    return total;
                }


                function submitContract() {
                    if(!$scope.isAdminManager && parseFloat(totalLessDeposit()) != 0)
                        return true;
                    if($scope.request.field_flat_rate_local_move.value) {
                        if ($scope.contract_page.lessInitialContract) {
                            if (!$scope.data.req_comment) {
                                return ($scope.data.signatures.length < $scope.maxSignCount - 4);
                            } else if($scope.data.req_comment && $scope.data.req_comment != '') {
                                return ($scope.data.signatures.length < $scope.maxSignCount - 3);
                            }
                        } else {
                            return ($scope.data.signatures.length < $scope.maxSignCount - 2)
                        }
                    }else {
                        if ($scope.contract_page.lessInitialContract) {
                            //check pickup
                            if(!$scope.data.isPickupSubmitted){
                                if(!$scope.data.req_comment){
                                    return ($scope.data.signatures.length < $scope.maxSignCount - 3);
                                }else if($scope.data.req_comment && $scope.data.req_comment != ''){
                                    return ($scope.data.signatures.length < $scope.maxSignCount - 2);
                                }
                            }else if($scope.data.isPickupSubmitted && !$scope.data.isSubmitted){ //check delivery
                                if(!$scope.data.req_commentDelivery){
                                    return ($scope.data.signatures.length < $scope.maxSignCount - 1);
                                }else if($scope.data.req_commentDelivery && $scope.data.req_commentDelivery != ''){
                                    return ($scope.data.signatures.length < $scope.maxSignCount);
                                }
                            }
                        }else{
                            if(!$scope.data.isPickupSubmitted){
                                return ($scope.data.signatures.length < $scope.maxSignCount - 2);
                            }else if($scope.data.isPickupSubmitted && !$scope.data.isSubmitted){ //check delivery
                                return ($scope.data.signatures.length < $scope.maxSignCount);
                            }
                        }
                    }
                }

                function _setStepError(nextStep) {
                    if(nextStep == 'checkout_btn'){
                        $('#errorSign_'+ nextStep).addClass('errorArrow');
                        var el = $('.' + nextStep);
                        el.addClass('error');
                        if(!$scope.contract_page.lessInitialContract && $scope.navigation.active == _.indexOf($scope.navigation.pages, 'Bill of lading'))
                            $('html, body').animate({scrollTop: el.offset().top}, 1000);
                        $('#signaturePad').modal('hide');
                        return false;
                    }
                    var el = $('#step_' + nextStep);
                    el.addClass('error');
                    if($scope.navigation.active == _.indexOf($scope.navigation.pages, 'Bill of lading'))
                        $('html, body').animate({
                            scrollTop: el.offset().top
                        }, 1000);
                    $('#signaturePad').modal('hide');
                    return false;
                }
                function removeError(){
                    var ln = $scope.data.signatures.length;
                    for(var i=0; i<ln; i++){
                        var arrow = $('#errorSign_'+ i);
                        var el = $('#step_' + i);
                        if (el.hasClass('error')) {
                            el.removeClass('error');
                        }
                        if (arrow.hasClass('errorArrow')) {
                            arrow.removeClass('errorArrow');
                        }
                    }
                    var btnPay = $('.checkout_btn');
                    if (btnPay.hasClass('error')) {
                        btnPay.removeClass('error');
                    }
                    var btnPayArrow = $('#errorSign_checkout_btn');
                    if (btnPayArrow.hasClass('errorArrow')) {
                        btnPayArrow.removeClass('errorArrow');
                    }
                    var declaration = $('#step_declaration');
                    if (declaration.hasClass('error')) {
                        declaration.removeClass('error');
                    }
                    var declarationArrow = $('#errorSign_declaration');
                    if (declarationArrow.hasClass('errorArrow')) {
                        declarationArrow.removeClass('errorArrow');
                    }
                }

                function submitContractBtn (param) {
					if(!$scope.isAdminManager) {
						_setStepError('checkout_btn');
						if (parseFloat(totalLessDeposit()) != 0) {
							SweetAlert.swal("Failed!", "Total less deposit received isn't 0", "error");
							return false;
						}
					}

					if(submitContract()) {
						$scope.saveContract({data: true});
						return false;
					}
					removeError();
					if(localStorage[$scope.request.nid]){
						SweetAlert.swal("Internet connection problem. Please contact your manager.", "", "error");
						return false;
					}

					ContractFactory.factory.showDiscount = true;
					ContractFactory.factory.openStorageAndTransit = true;
					$scope.showDiscount = true;
					$scope.openStorageAndTransit = true;
					$scope.saveContract(param)
						.then(() => {
							ContractFactory.factory.isSubmitted =  _.get(param, 'delivery', $scope.request.field_flat_rate_local_move.value);
							ContractFactory.factory.isPickupSubmitted = _.get(param, 'pickup', false);
							sendContractLog("Contract submitted");
							contractDispatchLogs.assignLogs($scope.request);
						});
                }

                function showTransit(){
                    $scope.openStorageAndTransit = !$scope.openStorageAndTransit;
                    $scope.data.openStorageAndTransit = $scope.openStorageAndTransit;
					$scope.saveContract({data: true, missModalClose: true});
                }

                function totalFlatRate () {
                    var a = ContractCalc.totalFlatRateCost($scope.data.isPickupSubmitted);
                    return parseFloat(a.toFixed(2));
                }

				function totalFlatRateClosing () {
					let totalClosing = ContractCalc.totalClosingFlatRateCost();
					return _.round(totalClosing, 2);
				}

				$scope.$on('event:save.SIT.service', function( ev, model, service){
					if(_.isEmpty($scope.services)){
						$scope.services = {};
					}
					$scope.services[model] = service;
					if(!_.isEmpty($scope.data.storageServices)) {
						$scope.data.storageServices[model] = service;
					} else{
						$scope.data.storageServices = {};
						$scope.data.storageServices[model] = service;
					}
					if(model == 'monthly_storage_fee'){
						$scope.data.openRental = true;
						$scope.navigation.active = _.indexOf($scope.navigation.pages,'Rental agreement');
					}
					$scope.saveContract();
				});
            }
        }
}();
