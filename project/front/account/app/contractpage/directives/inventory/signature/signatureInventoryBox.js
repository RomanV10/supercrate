angular.module('contractModule')
    .directive("signatureInventoryBox", signatureInventoryBox);

signatureInventoryBox.$inject = ['ContractFactory', '$window', 'SweetAlert', '$rootScope'];

function signatureInventoryBox(ContractFactory, $window, SweetAlert, $rootScope) {
    return {
        restrict: "EA",
        scope: {
            stepId: "@",
            owner: "@",
            admin: '=',
	        fieldName: '<',
			request: '=?',
			boxType: '@'
		},
        template: ['<div class="signatured-box" style="cursor:default;" ng-if="data[fieldName].signatures[stepId]">',
            '<img alt="Signatures" ng-src="{{ data[fieldName].signatures[stepId].value }}" disabled/>',
            '<a href="javascript:;" ng-show="showCancel()" ng-click="removeSignature(stepId)"><span class="cancel" > &times;</span></a>',
            '</div>',
			'<div class="signature-box" id="step_{{fieldName}}_{{stepId}}" ng-click="clickOpenInventorySignature()" ng-if="!data[fieldName].signatures[stepId]">',
            '<div class="empty-signature"><i class="glyphicon glyphicon-pencil"></i> Click here to sign</div>',
            '<div id="errorSign_{{ stepId }}" style="display:none;"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>',
            '</div>'
        ].join(''),
        link: linkFn
    };

    function linkFn(scope) {
        scope.basicSettings = scope.$parent.basicSettings;
        scope.data = scope.$parent.data;
        scope.inventoryLength = scope.$parent.inventoryLength;

        scope.clickOpenInventorySignature = function () {
            var numArr = '';
            var count = 0;
            _.forEachRight(scope.data[scope.fieldName].inventory, function (item, key) {
                if (item) {
                    if (!item.article) {
                        delete scope.data[scope.fieldName].inventory[key];
                    } else {
                        count++;
                        if (!item.checkshipper && item.article) {
                            numArr = key + ', ' + numArr;
                        } else if (item.checkshipper && !item.article) {
                            delete scope.data[scope.fieldName].inventory[key];
                        }
                    }
                }
            });
            if ((scope.stepId == 2 || scope.stepId == 3)) {
                if (numArr != '') {
                    SweetAlert.swal({
                        title: "Items at numbers: " + numArr + " aren't checked",
                        type: "info",
                        showCancelButton: true,
                        cancelButtonText: "No",
                        confirmButtonText: "Continue"
                    }, function (isConfirm) {
                        if (isConfirm) {
                            ContractFactory.factory[scope.fieldName].currentStep = scope.stepId;
                            ContractFactory.openInventorySignaturePad(scope.owner);
                        } else {
                            return false;
                        }
                    });
                } else {
					signatureOpen();
				}
            }
            if (scope.stepId == 0 || scope.stepId == 1) {
	            let isNotDoneWithInventory = !_.get(scope, `request.request_all_data.invoice.${scope.fieldName}.doneWithInventory`);

                if (_.filter(scope.data[scope.fieldName].inventory, 'article').length != scope.$parent.inventoryLength && isNotDoneWithInventory) {
                    SweetAlert.swal("Error", "Please choose all articles or click 'Done with inventory'", 'error');
                    return false;
                }
                _.forEach(scope.data[scope.fieldName].inventory, function(item) {
					if (item && !item.crRef && item.article) {
						SweetAlert.swal("Error", "Please set CR Reference", 'error');
						return false;
					}
                });
                if (!scope.data[scope.fieldName].tapeNumbers || scope.data[scope.fieldName].tapeNumbers == 0 || _.isNull(scope.data[scope.fieldName].tapeNumbers)) {
                    SweetAlert.swal("Error", "Please set Tape Lot No.", 'error');
                    return false;
                }
                if (!scope.data[scope.fieldName].tapeColor || scope.data[scope.fieldName].tapeColor == '' || _.isNull(scope.data[scope.fieldName].tapeColor)) {
                    SweetAlert.swal("Error", "Please set Tape Color", 'error');
                    return false;
                }
				signatureOpen();
			}
        };

        function signatureOpen() {
			ContractFactory.factory[scope.fieldName].currentStep = scope.stepId;
			if(scope.fieldName == 'addInventoryMoving' || scope.fieldName == 'secondAddInventoryMoving') {
				ContractFactory.openAddInventorySignaturePad(scope.owner);
			} else {
				ContractFactory.openInventorySignaturePad(scope.owner);
			}
		}

        scope.showCancel = function () {
            if (!scope.data[scope.fieldName].isSubmitted && scope.admin) {
                return true;
            }
        };

        scope.removeSignature = function (stepId) {
            $rootScope.$broadcast('removed.inventory.signature', stepId);
            delete scope.data[scope.fieldName].signatures[stepId];
            scope.data[scope.fieldName].signatures.length--;
            scope.data[scope.fieldName].currentStep--;
        };
    }
}
