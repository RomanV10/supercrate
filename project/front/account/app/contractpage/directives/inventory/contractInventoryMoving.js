var app = angular.module('contractModule');
app.directive('contractInventoryMoving', contractInventoryMoving);

/*@ngInject*/
function contractInventoryMoving(ContractFactory, ContractCalc, logger, datacontext, ContractService,
								 SweetAlert, common, InventoriesServices, $rootScope, $timeout,
								 StorageService, RequestServices, $uibModal, newInventoriesServices, valuationService, accountConstant) {
	return {
		restrict: 'E',
		templateUrl: 'app/contractpage/directives/inventory/inventoryForMoving.html',
		scope: {
			fieldName: '@name',
			data: '=',
			request: '=',
			inventoryList: '=list',
			inventoryLength: '=invLength',
			inventoryCount: '=invCount',
			ifStorage: '<',
			isOvernight: '<',
			navigation: '=',
			saveContract: '&',
			loadInventory: '&',
		},
		controller: contractInventoryMovingCtrl
	};

	function contractInventoryMovingCtrl($scope) {
		var SIGNATURE_PREFIX = 'data:image/gif;base64,';

		$scope.countInventory = countInventory;
		$scope.saveStep = saveStep;
		$scope.saveInventory = saveInventory;
		$scope.nextPage = nextPage;
		$scope.prevPage = prevPage;
		$scope.changeNosFrom = changeNosFrom;
		$scope.openCondition = openCondition;
		$scope.changeNumberFrom = changeNumberFrom;
		$scope.openTransferModal = openTransferModal;
		$scope.openAdditionalInventory = openAdditionalInventory;
		$scope.doneWithInventory = doneWithInventory;
		$scope.reInitList = reInitList;
		$scope.checkArticle = checkArticle;
		$scope.calcAddInventoryWeight = calcAddInventoryWeight;
		$scope.calcAddInventoryCost = calcAddInventoryCost;
		$scope.calcTotalInventoryCost = calcTotalInventoryCost;
		$scope.blockChosen = blockChosen;
		$scope.initSignatureCanvas = initSignatureCanvas;
		$scope.cancelInventory = cancelInventory;

		$scope.fieldData = datacontext.getFieldData();
		$scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);

		_.forEach($scope.amoutValuation, function (item, ind) {
			$scope.amoutValuation[ind] = Number(item);
		});
		$scope.rangeStart = 1;
		$scope.rangeStop = 25;
		$scope.rangeArr = _.range($scope.rangeStart, $scope.rangeStop + 1, 1);
		$scope.descreptiveSymbols = ContractFactory.descreptiveSymbols;
		$scope.exceptionSymbols = ContractFactory.exceptionSymbols;
		$scope.locationSymbols = ContractFactory.locationSymbols;
		$scope.originalWeight = 0;
		$scope.originalCost = 0;
		$scope.defStorage = {
			min: 0,
			rate: 0
		};
		$scope.additionalInventory = [];
		$scope.isAdminManager = ContractFactory.isAdminManager;
		$scope.isAdmin = ContractFactory.isAdmin;

		if (!$scope.ifStorage && angular.isUndefined($scope.data[$scope.fieldName].delivery)) {
			$scope.data[$scope.fieldName].delivery = false;
		} else {
			$scope.data[$scope.fieldName].delivery = $scope.request.request_all_data.toStorage;
		}

		angular.element(document).ready(function () {
			ContractFactory.init();
		});

		let inventory = newInventoriesServices.getSelectedInventory($scope.request, $scope.data);

		$scope.originalWeight = InventoriesServices.calculateTotals(inventory).cfs;

		StorageService.getDefaultStorage().then(function ({data}) {
			$scope.busy = false;
			if (!data.id || !data.value) {
				SweetAlert.swal('Failed! No default storages', '', 'error');
				return false;
			}
			$scope.defStorage.min = data.value.min_cuft;
			$scope.defStorage.rate = data.value.rate_per_cuft;
			if (Number(data.value.min_cuft) < Number($scope.originalWeight) || Number($scope.originalWeight) == 0) {
				$scope.originalCost = $scope.originalWeight * data.value.rate_per_cuft;
			} else {
				$scope.originalCost = Number(data.value.min_cuft) * data.value.rate_per_cuft;
			}
		});

		function initSignatureCanvas() {
			ContractFactory.init();
		}

		function getInventoryMoving() {
			let inventory = [];

			if (_.get($scope.request, 'request_all_data.inventory')) {
				inventory = angular.copy($scope.request.request_all_data.inventory);
			}
			if (_.get($scope.request, 'inventory.inventory_list')) {
				inventory = inventory.concat(angular.copy($scope.request.inventory.inventory_list));
			}

			return inventory;
		}

		function cancelInventory() {
			$scope.data[$scope.fieldName].open = false;
			if ($scope.fieldName == 'addInventoryMoving') {
				$scope.request.request_all_data.additional_inventory = [];
			} else if ($scope.fieldName == 'secondAddInventoryMoving') {
				$scope.request.request_all_data.second_additional_inventory = [];
			}

			$scope.loadInventory($scope.request);
			$scope.data[$scope.fieldName].inventory = {};
			$scope.data[$scope.fieldName].isSubmitted = false;
			$scope.data[$scope.fieldName].tapeColor = '';
			$scope.data[$scope.fieldName].tapeNumbers = '';
			$scope.data[$scope.fieldName].signatures = {};

			$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Bill of lading');
			$scope.saveContract();

			$rootScope.$broadcast('inventory.submit');
		}

		function blockChosen() {
			if ($scope.data[$scope.fieldName].delivery) {
				return (!_.isEmpty(
					$scope.data[$scope.fieldName].signatures[2]) && !_.isEmpty(
					$scope.data[$scope.fieldName].signatures[3])) ? true : false;
			}
		}

		function countInventory() {
			return (_.filter($scope.data[$scope.fieldName].inventory, 'article').length == $scope.inventoryLength);
		}

		function recountArticlesSize() {
			$scope.inventoryCount = {};
			if ($scope.fieldName == 'inventoryMoving') {
				let inventory = getInventoryMoving();

				if (!_.isEmpty($scope.data[$scope.fieldName].inventory)) {
					_.forEachRight($scope.data[$scope.fieldName].inventory, function (item, key) {
						if (item) {
							var isExist = _.findIndex(inventory, {id: item.article});
							if (isExist < 0) {
								delete $scope.data[$scope.fieldName].inventory[key];
							}
						} else if (!item) delete $scope.data[$scope.fieldName].inventory[key];
					});
				}
				_.forEach(inventory, function (item) {
					if (angular.isUndefined($scope.inventoryCount[item.id])) {
						$scope.inventoryCount[item.id] = 0;
					}
					$scope.inventoryCount[item.id] += item.count;
				});
			}

			if ($scope.fieldName == 'addInventoryMoving' || $scope.fieldName == 'secondAddInventoryMoving') {
				var field = '';
				if ($scope.fieldName == 'addInventoryMoving') {
					field = 'additional_inventory';
				} else if ($scope.fieldName == 'secondAddInventoryMoving') field = 'second_additional_inventory';
				if ($scope.request.request_all_data[field]) {
					if (!_.isEmpty($scope.data[$scope.fieldName].inventory)) {
						_.forEachRight($scope.data[$scope.fieldName].inventory, function (item, key) {
							if (item) {
								var isExist = _.findIndex($scope.request.request_all_data[field], {id: item.article});
								if (isExist < 0) {
									delete $scope.data[$scope.fieldName].inventory[key];
								}
							} else if (!item) delete $scope.data[$scope.fieldName].inventory[key];
						});
					}
					_.forEach($scope.request.request_all_data[field], function (item) {
						if (angular.isUndefined($scope.inventoryCount[item.id])) {
							$scope.inventoryCount[item.id] = 0;
						}
						$scope.inventoryCount[item.id] += item.count;
					});
				}
			}
			_.forEach($scope.data[$scope.fieldName].inventory, function (data) {
				_.forEach($scope.inventoryCount, function (item, id) {
					if (data) {
						if (id == data.article && $scope.inventoryCount[id] != 0) {
							$scope.inventoryCount[id]--;
						}
					}
				});
			});
			$scope.data[$scope.fieldName].numberThru = Number($scope.data[$scope.fieldName].rangeStart)
				+ _.filter($scope.data[$scope.fieldName].inventory, 'article').length - 1;
		}

		function isCanvasBlank(elementID) {
			var canvas = document.getElementById(elementID);
			var blank = document.createElement('canvas');
			blank.width = canvas.width;
			blank.height = canvas.height;
			var context = canvas.getContext('2d');
			var perc = 0, area = canvas.width * canvas.height;
			if (canvas.toDataURL() == blank.toDataURL()) {
				return canvas.toDataURL() == blank.toDataURL();
			} else {
				var data = context.getImageData(0, 0, canvas.width, canvas.height).data;
				for (var ct = 0, i = 3, len = data.length; i < len; i += 4) if (data[i] > 50) ct++;
				perc = parseFloat((100 * ct / area).toFixed(2));
				return (perc < 0.1);
			}
		}

		function saveStep() {
			var blank = false;
			if (!$scope.prevSignature) {
				if ($scope.fieldName == 'addInventoryMoving' || $scope.fieldName == 'secondAddInventoryMoving') {
					blank = isCanvasBlank('signatureAddInventoryCanvas');
				} else {
					blank = isCanvasBlank('signatureInventoryCanvas');
				}
			}
			if (blank) {
				SweetAlert.swal('Failed!', 'Sign please', 'error');
			} else {
				var stepId = Number($scope.data[$scope.fieldName].currentStep);
				if (isNaN(stepId)) return false;
				if ($scope.fieldName == 'addInventoryMoving' || $scope.fieldName == 'secondAddInventoryMoving') {
					var signatureValue = ContractFactory.getCropedAddInventorySignatureValue();
				} else {
					var signatureValue = ContractFactory.getCropedInventorySignatureValue();
				}
				var ln = $scope.data[$scope.fieldName].signatures.length;
				var nextStep = ln;

				if ($scope.data[$scope.fieldName].signatures.hasOwnProperty(stepId)) {
					if (!_.isEmpty($scope.data[$scope.fieldName].signatures[stepId])) {
						logger.warning('You can\'t perform this action', 'Something going wrong');
						return false;
					}
				}

				if (ln) { //Check validation
					var c = 0, keepGoing = true;
					angular.forEach($scope.data[$scope.fieldName].signatures, function (sign, index) {
						if (keepGoing) {
							if (index != c) {
								nextStep = c;
								keepGoing = false;
							}
							c++;
						}
					});
				}
				var signature = {
					owner: $scope.data.currentOwner,
					value: SIGNATURE_PREFIX + signatureValue,
					date: moment().format('x')
				};
				if (stepId == 5) {
					$scope.request.request_all_data.additional_inventory_sign = signature;
					ContractService.saveContractRequestData($scope.request);
					ContractFactory.clearSignaturePad();
					$('#signatureInventoryPad').modal('hide');
					return false;
				}

				var msgToLog = (($scope.fieldName == 'addInventoryMoving' || $scope.fieldName == 'secondAddInventoryMoving')
								? 'Add.Inventory signed '
								: 'Inventory signed ') + (stepId % 2 == 0
								? 'contractor, carrier or authorized agent(driver)'
								: 'owner or authorized agent');
				ContractService.addForemanLog(msgToLog, 'Сontract', $scope.request.nid, 'MOVEREQUEST');

				$scope.data[$scope.fieldName].signatures[stepId] = signature;
				$scope.data[$scope.fieldName].signatures.length += 1;
				$scope.data[$scope.fieldName].currentStep += 1;
				nextStep = $scope.data[$scope.fieldName].signatures.length;
				ContractFactory.clearSignaturePad();
				//Save in local storage for keep away other issues
				var pendingSignatures = JSON.parse(localStorage['pendingSignatures']);
				pendingSignatures[stepId] = signature;
				localStorage['pendingSignatures'] = JSON.stringify(pendingSignatures);
				$scope.data[$scope.fieldName] = $scope.data[$scope.fieldName];
				$scope.saveContract({
					callback: function () {
						localStorage['pendingSignatures'] = JSON.stringify({});
					}
				});
				$scope.prevSignature = false;
				if ($scope.fieldName == 'addInventoryMoving' || $scope.fieldName == 'secondAddInventoryMoving') {
					$('#signatureAddInventoryPad').modal('hide');
				} else {
					$('#signatureInventoryPad').modal('hide');
				}
			}
		}

		function removeStep(stepId) {
			var msgToLog = (($scope.fieldName == 'addInventoryMoving' || $scope.fieldName == 'secondAddInventoryMoving')
				? 'Signature on Add.Inventory removed by ' : 'Signature on Inventory removed by ')
				+ (stepId % 2 == 0 ? 'contractor, carrier or authorized agent(driver)' : 'owner or authorized agent');
			ContractService.addForemanLog(msgToLog, 'Сontract', $scope.request.nid, 'MOVEREQUEST');
		}

		function validatePickUp() {
			let isNotDoneWithInventory = !_.get($scope,
				`request.request_all_data.invoice.${$scope.fieldName}.doneWithInventory`);

			if (_.filter($scope.data[$scope.fieldName].inventory, 'article').length != $scope.inventoryLength &&
				isNotDoneWithInventory) {
				SweetAlert.swal('Error', 'Please choose all articles or click \'Done with inventory\'', 'error');
				return false;
			}
			_.forEach($scope.data[$scope.fieldName].inventory, function (item, key) {
				if (item) {
					if (!item.crRef) {
						SweetAlert.swal('Error', 'Please set CR Reference', 'error');
						return false;
					}
				}
			});
			if (!$scope.data[$scope.fieldName].tapeNumbers || $scope.data[$scope.fieldName].tapeNumbers == 0
				|| _.isNull($scope.data[$scope.fieldName].tapeNumbers)) {
				SweetAlert.swal('Error', 'Please set Tape Lot No.', 'error');
				return false;
			}
			if (!$scope.data[$scope.fieldName].tapeColor || $scope.data[$scope.fieldName].tapeColor == ''
				|| _.isNull($scope.data[$scope.fieldName].tapeColor)) {
				SweetAlert.swal('Error', 'Please set Tape Color', 'error');
				return false;
			}
			return true;
		}

		function saveDeliverySignature() {
			ContractService.getSignatures($scope.request.storage_id).then(({data}) => {
				if (angular.isUndefined(data.factory)) {
					data.factory = ContractFactory.factoryObj;
				}
				if (angular.isUndefined(data.finance)) {
					data.finance = ContractCalc.financeObj;
				}
				data.factory[$scope.fieldName] = angular.copy($scope.data[$scope.fieldName]);
				var allData = {
					factory: _.clone(data.factory),
					finance: _.clone(data.finance)
				};
				ContractService.saveContract(allData, $scope.request.storage_id)
					.then(function () {
						$scope.data[$scope.fieldName].isSubmitted = true;
						$scope.data[$scope.fieldName] = $scope.data[$scope.fieldName];
						$scope.saveContract();

						var msgToLog = (($scope.fieldName == 'addInventoryMoving' || $scope.fieldName == 'secondAddInventoryMoving')
							? 'Add.Inventory submitted'
							: 'Inventory submitted');
						ContractService.addForemanLog(msgToLog, 'Сontract', $scope.request.nid, 'MOVEREQUEST');

						if ($scope.fieldName == 'inventoryMoving' && $scope.data.addInventoryMoving.open) {
							$rootScope.$broadcast(
								'go.to.addInv');
						} else if ($scope.fieldName == 'inventoryMoving' && $scope.data.secondAddInventoryMoving.open) {
							$rootScope.$broadcast(
								'go.to.secAddInv');
						} else if ($scope.fieldName != 'inventoryMoving' && _.isEmpty($scope.request.inventory)) {
							$rootScope.$broadcast('inventory.submit');
						} else {
							$rootScope.$broadcast('rental.finished');
						}
						$scope.busy = false;
					}, function (err) {
						$scope.busy = false;
						logger.error(err || 'error', null, 'Something going wrong with contract document');
					});
			});
		}

		function saveInventory(type) {
			if (type == 'add.inventory') {
				if ($scope.data.inventoryMoving.isSubmitted && $scope.fieldName == 'inventoryMoving') {
					ContractFactory.transferInventoryContractData($scope.data, $scope.fieldName,
						$scope.request.storage_id, ContractCalc.financeObj);
				}
				if ($scope.data.addInventoryMoving.isSubmitted && $scope.fieldName == 'addInventoryMoving') {
					ContractFactory.transferInventoryContractData($scope.data, $scope.fieldName,
						$scope.request.storage_id, ContractCalc.financeObj);
				}
				if ($scope.data.addInventoryMoving.open && $scope.data.inventoryMoving.isSubmitted && $scope.fieldName == 'inventoryMoving') {
					$rootScope.$broadcast('go.to.addInv');
				} else if ($scope.data.secondAddInventoryMoving.open &&
						   $scope.data.addInventoryMoving.isSubmitted &&
						   $scope.fieldName == 'addInventoryMoving') {
					$rootScope.$broadcast('go.to.secAddInv');
				}
			}
			if (type == 'submit') {
				if ($scope.ifStorage) {
					if ($scope.request.storage_id && !$scope.data[$scope.fieldName].delivery ||
						$scope.request.request_all_data.withoutStorageRequest) {
						if (!validatePickUp()) return false;
						if (!_.isEmpty($scope.data[$scope.fieldName].signatures[0]) && !_.isEmpty(
							$scope.data[$scope.fieldName].signatures[1])) {
							$scope.busy = true;

							if (!$scope.request.request_all_data.withoutStorageRequest) {
								RequestServices.getRequest($scope.request.storage_id).then(function (data) {
									if (!data.request_all_data) data.request_all_data = {};

									data.request_all_data.inventory = $scope.request.request_all_data.inventory;
									data.request_all_data.additional_inventory = $scope.request.request_all_data.additional_inventory;
									data.request_all_data.second_additional_inventory = $scope.request.request_all_data.second_additional_inventory;

									if (!data.request_all_data.transferedFrom) data.request_all_data.transferedFrom = [];
									data.request_all_data.transferedFrom.push($scope.request.nid);

									if (!$scope.request.request_all_data.transferedTo) $scope.request.request_all_data.transferedTo = [];
									$scope.request.request_all_data.transferedTo.push($scope.request.storage_id);

									ContractService.saveContractRequestData(data);
									ContractService.saveContractRequestData($scope.request);

									if (data.request_all_data.toStorage && $scope.fieldData.field_lists.field_approve[data.status.raw] == ' Confirmed') {
										var dataUpdate = {
											field_approve: (_.invert(
												$scope.fieldData.field_lists.field_approve))[' Pending-info'],
											global_user_uid: _.get($scope.request, 'current_manager.uid', 1)
										};
										RequestServices.updateRequest($scope.request.storage_id, dataUpdate);
										$scope.busy = false;
									}
								});
							}

							if ($scope.fieldName == 'inventoryMoving' || $scope.fieldName == 'addInventoryMoving') {
								var field = '';
								if ($scope.fieldName == 'inventoryMoving') {
									field = 'addInventoryMoving';
								} else if ($scope.fieldName == 'addInventoryMoving') field = 'secondAddInventoryMoving';
								SweetAlert.swal({
									title: 'Do you want to add more inventory?',
									type: 'info',
									showCancelButton: true,
									cancelButtonText: 'No',
									confirmButtonText: 'Yes'
								}, function (isConfirm) {
									if (isConfirm) {
										$scope.data[field].open = true;
										ContractFactory.transferInventoryContractData($scope.data, $scope.fieldName,
											$scope.request.storage_id, ContractCalc.financeObj);
									} else {
										$scope.data[field].open = false;
										ContractFactory.transferInventoryContractData($scope.data, $scope.fieldName,
											$scope.request.storage_id, ContractCalc.financeObj);
									}
								});
							}
							if ($scope.fieldName == 'secondAddInventoryMoving') {
								ContractFactory.transferInventoryContractData($scope.data, $scope.fieldName,
									$scope.request.storage_id, ContractCalc.financeObj);
							}
							$rootScope.$broadcast('inventory.submit');
						} else {
							$scope.busy = false;
							SweetAlert.swal('Failed!', 'Please sign', 'error');
						}
					} else {
						if (!_.isEmpty($scope.data[$scope.fieldName].signatures[2]) &&
							!_.isEmpty($scope.data[$scope.fieldName].signatures[3])) {
							$scope.data[$scope.fieldName].isSubmitted = true;
							$scope.busy = true;
							$scope.data[$scope.fieldName].numberThru = Number(
							$scope.data[$scope.fieldName].rangeStart) + _.filter(
							$scope.data[$scope.fieldName].inventory, 'article').length - 1;
							$scope.data[$scope.fieldName] = $scope.data[$scope.fieldName];
							saveDeliverySignature();
							$scope.busy = false;
							SweetAlert.swal('Success!', 'Delivery was submitted', 'success');
						} else {
							$scope.busy = false;
							SweetAlert.swal('Failed!', 'Sign please', 'error');
						}
					}
				} else {
					if (!_.isEmpty($scope.data[$scope.fieldName].signatures[0]) &&
						!_.isEmpty($scope.data[$scope.fieldName].signatures[1])) {
						$scope.data[$scope.fieldName].isSubmitted = true;

						let msgToLog = (($scope.fieldName == 'addInventoryMoving' || $scope.fieldName == 'secondAddInventoryMoving')
							? 'Add.Inventory submitted'
							: 'Inventory submitted');
						ContractService.addForemanLog(msgToLog, 'Сontract', $scope.request.nid, 'MOVEREQUEST');

						$scope.busy = true;
						$scope.data[$scope.fieldName].numberThru = Number($scope.data[$scope.fieldName].rangeStart) +
							_.filter($scope.data[$scope.fieldName].inventory, 'article').length - 1;
						$scope.data[$scope.fieldName] = $scope.data[$scope.fieldName];

						let inventory = newInventoriesServices.getSelectedInventory($scope.request, $scope.data);
						let weight = InventoriesServices.calculateTotals(inventory).cfs;

						$rootScope.$broadcast('submit.inv.moving', weight);

						if (!$scope.isOvernight) {
							$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Rental agreement');
						} else {
							$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Bill of lading');
						}

						$('body').scrollTop(0);
						$scope.saveContract();
						$scope.busy = false;
					} else {
						$scope.busy = false;
						SweetAlert.swal('Failed!', 'Sign please', 'error');
					}
				}
			}
			else {
				$scope.busy = true;
				$scope.data[$scope.fieldName].numberedItems = _.filter($scope.data[$scope.fieldName].inventory,
					'article').length;
				$scope.data[$scope.fieldName].numberThru = Number($scope.data[$scope.fieldName].rangeStart) +
					_.filter($scope.data[$scope.fieldName].inventory, 'article').length - 1;
				$scope.data[$scope.fieldName] = $scope.data[$scope.fieldName];
				$scope.saveContract();
				$scope.busy = false;
			}
		}

		function nextPage(num) {
			if (angular.isUndefined(num)) {
				num = 1;
				$scope.rangeStart = $scope.data[$scope.fieldName].pageNumber * 25 + Number(
						$scope.data[$scope.fieldName].rangeStart);
				if (($scope.data[$scope.fieldName].pageNumber * 25 + Number(
						$scope.data[$scope.fieldName].rangeStart)) > $scope.data[$scope.fieldName].numberThru) {
					$scope.rangeStop = $scope.data[$scope.fieldName].numberThru + 25;
				} else {
					$scope.rangeStop = $scope.data[$scope.fieldName].pageNumber * 25 + Number(
							$scope.data[$scope.fieldName].rangeStart) + 25;
				}
			} else {
				$scope.rangeStart = ($scope.data[$scope.fieldName].pageNumber - 1) * 25;
				$scope.rangeStop = ($scope.data[$scope.fieldName].pageNumber - 1) * 25 + 25;
			}
			$scope.rangeArr = _.range($scope.rangeStart, $scope.rangeStop, 1);
			$scope.data[$scope.fieldName].pageNumber++;
		}

		function prevPage() {
			$scope.data[$scope.fieldName].pageNumber--;
			$scope.rangeStart = Number($scope.rangeStart) - 25;
			if (($scope.rangeStop - 25) < 25) {
				$scope.rangeStop = 25;
			} else {
				$scope.rangeStop = Number($scope.rangeStart) + 25;
			}
			$scope.rangeArr = _.range($scope.rangeStart, $scope.rangeStop, 1);
		}

		function reWriteInventoryID() {
			if (!_.isEmpty($scope.data[$scope.fieldName].inventory)) {
				var tmp = {};
				var ind = $scope.data[$scope.fieldName].nosFrom;
				_.forEach($scope.data[$scope.fieldName].inventory, function (item) {
					if (item) {
						tmp[ind] = item;
						ind++;
					}
				});
				$scope.data[$scope.fieldName].inventory = tmp;
			}
		}

		function changeNosFrom(type) {
			if (type == 'stop') {
				$scope.data[$scope.fieldName].numberOfPages = Math.ceil(
					$scope.data[$scope.fieldName].numberThru / 25);
			}
		}

		function openCondition(inventory, key) {
			if (inventory) {
				ContractFactory.openCondition(inventory, $scope.exceptionSymbols, $scope.locationSymbols, key);
			} else {
				SweetAlert.swal('Failed!', 'Please choose article', 'error');
			}
		}

		function changeNumberFrom(item) {
			$scope.rangeArr = _.range(Number(item), Number(item) + 25, 1);
			$scope.data[$scope.fieldName].nosFrom = Number($scope.data[$scope.fieldName].rangeStart);
			$scope.data[$scope.fieldName].numberThru = Number($scope.data[$scope.fieldName].rangeStart)
				+ _.filter($scope.data[$scope.fieldName].inventory, 'article').length - 1;
			reWriteInventoryID();
		}

		function openTransferModal(fieldName) {
			ContractFactory.openTransferInventory($scope.data, $scope.request, fieldName);
		}

		function openAdditionalInventory() {
			$scope.busy = true;
			$scope.request.request_all_data.needsAddInventory = true;

			if (!$scope.ifStorage) {
				$scope.busy = false;
				let inventory = getInventoryMoving();

				ContractFactory.openAdditionalInventory(inventory, $scope.request, $scope.fieldName);

			} else if ($scope.ifStorage) {
				let field = '';
				if ($scope.fieldName == 'addInventoryMoving') {
					field = 'additional_inventory';
				} else if ($scope.fieldName == 'secondAddInventoryMoving') field = 'second_additional_inventory';
				if (!$scope.request.request_all_data[field]) $scope.request.request_all_data[field] = [];
				$scope.data[$scope.fieldName] = $scope.data[$scope.fieldName];
				$scope.saveContract();

				ContractService.saveContractRequestData($scope.request)
					.then(function () {
						$scope.busy = false;
						ContractFactory.openAdditionalInventory($scope.data[$scope.fieldName], $scope.request,
							$scope.fieldName);
					});
			}
		}

		function doneWithInventory() {
			var noError = true;
			_.forEach($scope.data[$scope.fieldName].inventory, function (item, key) {
				if (item) {
					if (!item.crRef && item.article) {
						SweetAlert.swal('Error', 'Please set CR Reference', 'error');
						noError = false;
						return false;
					}
				}
			});

			if (!noError) return false;

			SweetAlert.swal({
					title: 'Do you want to remove the remaining items?',
					type: 'warning',
					showCancelButton: true,
					cancelButtonText: 'No',
					confirmButtonText: 'Yes'
				},
				function (isConfirm) {
					if (isConfirm) {
						$scope.busy = true;

						_.set($scope, `request.request_all_data.invoice.${$scope.fieldName}.doneWithInventory`,
							true);

						let isInventoryMoving = $scope.fieldName == 'inventoryMoving';

						if (isInventoryMoving) {
							$scope.request.request_all_data.inventory = getDoneInventory('addInventory');
						}

						let isAdditionalInventory = $scope.fieldName == 'addInventoryMoving';
						let isSecondAdditionalInventory = $scope.fieldName == 'secondAddInventoryMoving';

						if (isAdditionalInventory || isSecondAdditionalInventory) {
							let field = isAdditionalInventory
								? 'additional_inventory'
								: 'second_additional_inventory';

							if ($scope.request.request_all_data && !_.isEmpty($scope.request.request_all_data[field])) {
								_.forEach($scope.inventoryCount, function (inv, id) {
									_.forEachRight($scope.request.request_all_data[field], function (item) {
										if (id == item.id && inv != 0) {
											item.count = item.count - inv;
										}
									});
								});

								_.forEachRight($scope.request.request_all_data[field], function (item, ind) {
									if (item.count == 0) {
										$scope.request.request_all_data[field].splice(ind, 1);
									}
								});
							}
						}

						ContractService.saveContractRequestData($scope.request)
							.then(() => {
								reInitList();
								recountArticlesSize();
								reCalcValuation();

								$rootScope.$broadcast('inventory.reset');

								if (isAdditionalInventory || isSecondAdditionalInventory) {
									openServiceModal('done.inventory');
								}
							})
							.catch(
								(err) => SweetAlert.swal('Failed!', 'Done with inventory has finished with error',
									'error'))
							.finally(() => $scope.busy = false);
					}
				});
		}

		function getDoneInventory(type) {
			let inventory = [];

			if (type) {
				if (type == 'addInventory') {
					if (_.get($scope.request, 'request_all_data.inventory')) {
						inventory = angular.copy($scope.request.request_all_data.inventory);
					}
				} else if (type == 'requestInventory') {
					if (_.get($scope.request, 'inventory.inventory_list')) {
						inventory = angular.copy($scope.request.inventory.inventory_list);
					}
				}

				_.forEach($scope.inventoryCount, function (inv, id) {
					_.forEachRight(inventory, function (item) {
						if (id == item.id && inv != 0) {
							item.count = item.count - inv;
						}
					});
				});

				_.forEachRight(inventory, function (item, ind) {
					if (item.count == 0) {
						inventory.splice(ind, 1);
					}
				});
			}

			return inventory;
		}

		function reInitList() {
			$scope.inventoryCount = {};
			$scope.inventoryList = {};
			$scope.inventoryLength = 0;
			if ($scope.fieldName == 'addInventoryMoving' || $scope.fieldName == 'secondAddInventoryMoving') {
				var field = '';
				if ($scope.fieldName == 'addInventoryMoving') {
					field = 'additional_inventory';
				} else if ($scope.fieldName == 'secondAddInventoryMoving') field = 'second_additional_inventory';
				if ($scope.request.request_all_data) {
					if ($scope.request.request_all_data[field]) {
						_.forEach($scope.request.request_all_data[field], function (item) {
							if (angular.isUndefined($scope.inventoryCount[item.id])) {
								$scope.inventoryCount[item.id] = 0;
							}
							$scope.inventoryCount[item.id] += item.count;
							if (angular.isUndefined($scope.inventoryList[item.id])) {
								$scope.inventoryList[item.id] = item;
							} else {
								$scope.inventoryList[item.id].count += item.count;
							}
							$scope.inventoryLength += item.count;
						});
					}
				}
			}

			if ($scope.fieldName == 'inventoryMoving') {
				let inventory = getInventoryMoving();

				_.forEach(inventory, function (item) {
					if (angular.isUndefined($scope.inventoryCount[item.id])) {
						$scope.inventoryCount[item.id] = 0;
					}
					$scope.inventoryCount[item.id] += item.count;
					if (angular.isUndefined($scope.inventoryList[item.id])) {
						$scope.inventoryList[item.id] = item;
					} else {
						$scope.inventoryList[item.id].count += item.count;
					}

					$scope.inventoryLength += item.count;
				});
			}
		}

		function checkArticle(num) {
			if (!$scope.data[$scope.fieldName].inventory[num]) {
				delete $scope.data[$scope.fieldName].inventory[num];
				return false;
			}
			if ($scope.data[$scope.fieldName].inventory[num].article == '') {
				delete $scope.data[$scope.fieldName].inventory[num];
				return false;
			}
			if ($scope.data[$scope.fieldName].inventory[num].article) {
				$scope.data[$scope.fieldName].inventory[num].article = Number(
					$scope.data[$scope.fieldName].inventory[num].article);
			}
		}

		function reCalcValuation() {
			if ($scope.request.request_all_data.invoice) {
				if (!$scope.request.request_all_data.invoice.request_all_data) {
					$scope.request.request_all_data.invoice.request_all_data = {};
				}
			} else {
				$scope.request.request_all_data.invoice = {};
				$scope.request.request_all_data.invoice.request_all_data = $scope.request.request_all_data;
			}
			let invoice = $scope.request.request_all_data.invoice;
			let inventory = newInventoriesServices.getSelectedInventory($scope.request, $scope.data);
			let weight = InventoriesServices.calculateTotals(inventory).cfs;
			valuationService.reCalculateValuationOnContract(invoice, weight);
			ContractService.saveContractRequestData($scope.request);
			$rootScope.$broadcast('change.valuation', $scope.request);
		}

		function calcAddInventoryWeight() {
			return Number(InventoriesServices.calculateTotals(
				$scope.request.request_all_data.additional_inventory || []).cfs);
		}

		function calcTotalInventoryCost() {
			let total = 0;
			let inventory = newInventoriesServices.getNewStorageInventory($scope.request, $scope.data);
			let totalWeight = InventoriesServices.calculateTotals(inventory).cfs;

			if (Number($scope.defStorage.min) < Number(totalWeight) || Number(totalWeight) == 0) {
				total = totalWeight * $scope.defStorage.rate;
			} else {
				total = Number($scope.defStorage.min) * $scope.defStorage.rate;
			}

			return _.round(total, accountConstant.roundUp);
		}

		function calcAddInventoryCost() {
			return _.round(calcTotalInventoryCost() - $scope.originalCost, accountConstant.roundUp);
		}

		// ctrl of modal for agreement about new weight inventory & price
		function openServiceModal(type) {
			var newInventoryAgreement = $uibModal.open({
				templateUrl: 'app/contractpage/directives/lease-agreement/storageFeeAgreement.modal.html',
				controller: newInventoryAgreementCtrl,
				size: 'lg',
				backdrop: 'static',
				keyboard: false,
				resolve: {
					originalCost: function () {
						return $scope.originalCost;
					},
					originalWeight: function () {
						return $scope.originalWeight;
					},
					totalInventoryCost: function () {
						return $scope.calcTotalInventoryCost();
					},
					addInventoryCost: function () {
						return $scope.calcAddInventoryCost();
					},
					fieldName: function () {
						return $scope.fieldName;
					},
					type: function () {
						return type;
					}
				}
			});
		}

		function newInventoryAgreementCtrl($scope, $rootScope, $uibModalInstance, originalCost, originalWeight,
										   addInventoryCost, totalInventoryCost, fieldName, type, ContractFactory) {
			$scope.originalCost = originalCost;
			$scope.originalWeight = originalWeight;
			$scope.addInventoryCost = addInventoryCost;
			$scope.totalInventoryCost = totalInventoryCost;
			$scope.fieldName = fieldName;

			setTimeout(function () {
				$scope.canvas = document.getElementById('signatureCanvasAgreement');
				$scope.signaturePad = new SignaturePad($scope.canvas);
				$scope.$apply();
			}, 500);

			var getCropedSignatureValue = function () {
				return ContractFactory.getSignatureUrl($scope.signaturePad._ctx, true);
			};

			function isCanvasBlank(elementID) {
				var canvas = document.getElementById(elementID);
				var blank = document.createElement('canvas');
				blank.width = canvas.width;
				blank.height = canvas.height;
				var context = canvas.getContext('2d');
				var perc = 0, area = canvas.width * canvas.height;
				if (canvas.toDataURL() == blank.toDataURL()) {
					return canvas.toDataURL() == blank.toDataURL();
				} else {
					var data = context.getImageData(0, 0, canvas.width, canvas.height).data;
					for (var ct = 0, i = 3, len = data.length; i < len; i += 4) if (data[i] > 50) ct++;
					perc = parseFloat((100 * ct / area).toFixed(2));
					return (perc < 0.1);
				}
			}

			$scope.saveService = saveService;
			$scope.cancel = cancel;

			function saveService() {
				var signatureValue = getCropedSignatureValue();
				var SIGNATURE_PREFIX = 'data:image/gif;base64,';
				var signature = {
					value: SIGNATURE_PREFIX + signatureValue,
					date: moment().format('x')
				};
				var blank = isCanvasBlank('signatureCanvasAgreement');
				if (blank) {
					SweetAlert.swal('Failed!', 'Sign please', 'error');
				} else {
					$scope.busy = true;

					//save object to contract
					var data = {
						newInventoryAgreement: {
							originalCost: $scope.originalCost,
							originalWeight: $scope.originalWeight,
							addInventoryCost: $scope.addInventoryCost,
							totalInventoryCost: $scope.totalInventoryCost
						},
						signature: signature,
						fieldName: fieldName,
						done: false
					};
					if (type == 'done.inventory') data.done = true;
					$rootScope.$broadcast('new.inventory.agree', data);
				}
			}

			function cancel() {
				$rootScope.$broadcast('new.inventory.agree.canceled');
				$uibModalInstance.dismiss('cancel');
			}

			$scope.$on('close.modal.service', function () {
				$scope.cancel();
			});
		}

		$scope.$watch('data[fieldName].inventory', function (newValue, oldValue) {
			if (!$scope.atFirst) {
				$scope.rangeArr = _.range($scope.data[$scope.fieldName].rangeStart,
					Number($scope.data[$scope.fieldName].rangeStart) + 25, 1);
				$scope.atFirst = true;
			}
			if (angular.isDefined(newValue) && !_.isEmpty(newValue)) {
				recountArticlesSize();
				if (common.count($scope.data[$scope.fieldName].inventory) == $scope.data[$scope.fieldName].pageNumber * 25
					&& $scope.inventoryLength > 25) {
					$scope.data[$scope.fieldName].numberOfPages += 1;
				}
			}
		}, true);

		$scope.$on('save.inventory_condition', function (evt, data) {
			$scope.data[$scope.fieldName] = $scope.data[$scope.fieldName];
			$scope.saveContract();
		});

		$scope.$on('transfer.inventory', function (evt) {
			$scope.data[$scope.fieldName].tranfer = true;
			$scope.data[$scope.fieldName].pickup = true;
			$scope.data[$scope.fieldName] = $scope.data[$scope.fieldName];
			$scope.saveContract();
		});

		$scope.$on('add.inventory', function () {
			reInitList();
			recountArticlesSize();
			reCalcValuation();
		});

		$scope.$on('go.to.addInv', function () {
			if ($scope.fieldName == 'inventoryMoving' && !$scope.data[$scope.fieldName].delivery) {
				$timeout(function () {
					$('body').scrollTop(0);
				}, 500);
			}
		});
		$scope.$on('go.to.secAddInv', function () {
			if ($scope.fieldName == 'addInventoryMoving' && !$scope.data[$scope.fieldName].delivery) {
				$timeout(function () {
					$('body').scrollTop(0);
				}, 500);
			}
		});

		$scope.$on('add.inventory.added', function (ev, data) {
			if (data.fieldName == 'addInventoryMoving') {
				$scope.additionalInventory = data.inventory;
			} else if (data.fieldName == 'secondAddInventoryMoving') {
				$scope.additionalInventory = data.inventory;
			}

			let inventory = newInventoriesServices.getSelectedInventory($scope.request, $scope.data);

			$scope.originalWeight = InventoriesServices.calculateTotals(inventory).cfs;

			if (Number($scope.defStorage.min) < Number($scope.originalWeight)
				|| Number($scope.originalWeight) == 0) {
				$scope.originalCost = $scope.originalWeight * $scope.defStorage.rate;
			} else {
				$scope.originalCost = Number($scope.defStorage.min) * $scope.defStorage.rate;
			}

			reInitList();
			recountArticlesSize();
			reCalcValuation();
			if ($scope.ifStorage && !$scope.request.request_all_data.toStorage) {
				openServiceModal();
			}
		});

		$scope.$on('add.inventory.added.moving', function (e, data) {
			$scope.request.request_all_data.inventory = data.inventory;
			let inventory = newInventoriesServices.getSelectedInventory($scope.request, $scope.data);

			$scope.originalWeight = InventoriesServices.calculateTotals(inventory).cfs;
			if (Number($scope.defStorage.min) < Number($scope.originalWeight)
				|| Number($scope.originalWeight) == 0) {
				$scope.originalCost = $scope.originalWeight * $scope.defStorage.rate;
			} else {
				$scope.originalCost = Number($scope.defStorage.min) * $scope.defStorage.rate;
			}

			reInitList();
			recountArticlesSize();
			reCalcValuation();
		});

		$scope.$on('inventory.transferred', function (ev, data) {
			$scope.data = data;
			$scope.data = data;
			$scope.saveContract();
			$scope.busy = false;
		});

		$scope.$on('new.inventory.agree', function (ev, data) {
			if (!data.done) {
				if (data.fieldName == 'addInventoryMoving') {
					$scope.request.request_all_data.additional_inventory = $scope.additionalInventory;
					$scope.request.request_all_data.additional_inventory = $scope.additionalInventory;
					$scope.additionalInventory = [];
				} else if (data.fieldName == 'secondAddInventoryMoving') {
					$scope.request.request_all_data.second_additional_inventory = $scope.additionalInventory;
					$scope.request.request_all_data.second_additional_inventory = $scope.additionalInventory;
					$scope.additionalInventory = [];
				}

				ContractService.saveContractRequestData($scope.request);

				$rootScope.$broadcast('inventory.reset');
			}
			$scope.data[data.fieldName].newInventoryAgreement = data.newInventoryAgreement;
			$scope.data[data.fieldName].signatures['6'] = data.signature;
			reInitList();
			recountArticlesSize();
			reCalcValuation();
			$scope.data = $scope.data;
			$scope.saveContract();
			$scope.busy = false;
		});

		$scope.$on('new.inventory.agree.canceled', function (ev) {
			$scope.additionalInventory = [];
			$scope.busy = false;
		});

		$scope.$on('removed.inventory.signature', function (ev, stepId) {
			removeStep(stepId);
			$scope.saveContract();
		});
	}
}
