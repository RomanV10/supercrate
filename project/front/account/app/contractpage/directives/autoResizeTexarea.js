angular.module('contractModule')
    .directive("autoResizeTexarea", autoResizeTexarea);

function autoResizeTexarea() {

    return {
        restrict: "EA",
        require: 'ngModel',
        scope: {
            'ngModel': '='
        },
        link: linkFn
    };

    function linkFn(scope, el, attr) {
        var textarea = el;
        textarea.on('keydown', autosize);
        function autosize(){
            setTimeout(function(){
                el[0].style.cssText = 'height:auto; padding-top:0 !important;padding-bottom:0 !important;';
                el[0].style.cssText = 'height:' + (el[0].scrollHeight + 15) + 'px;';
            },0);
        }
    }
}