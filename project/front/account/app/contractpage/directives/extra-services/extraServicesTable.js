angular.module('contractModule')
	.directive('extraServicesTable', extraServicesTable);

/*@ngInject*/
function extraServicesTable(ContractService, ContractFactory, ContractCalc, $rootScope, datacontext, ContractValue,
	additionalServicesFactory) {

	return {
		restrict: 'EA',
		scope: {
			request: '=',
			pageSubmitted: '<?',
			saveContract: '&',
		},
		templateUrl: 'app/contractpage/directives/extra-services/services.directive.html',
		link: linkFn
	};

	function linkFn(scope) {
		scope.addPacking = [];
		scope.editPacking = [];
		scope.removePacking = [];
		scope.dummyArray = new Array(101);
		scope.additionalServices = [];
		scope.isFamiliarization = ContractValue.isFamiliarization;
		scope.isPrint = ContractValue.isPrint;

		if (angular.isUndefined(scope.request.request_all_data.invoice)) {
			scope.request.request_all_data.invoice = {};
			scope.request.request_all_data.invoice.extraServices = scope.request.extraServices;

		}
		if (angular.isUndefined(scope.request.request_all_data.invoice.extraServices)) {
			scope.request.request_all_data.invoice.extraServices = scope.request.extraServices;
		}
		scope.additionalServices = angular.fromJson(scope.request.request_all_data.invoice.extraServices);

		scope.showAdditionalServicesRef = {
			show: false
		};
		scope.data = ContractFactory.factory;

		scope.addService = addService;
		scope.removeCharge = removeCharge;
		scope.saveServices = saveServices;
		scope.sumServicesChanged = sumServicesChanged;
		scope.changeName = changeName;
		scope.editPackingLog = editPackingLog;
		scope.showAddServicesBtn = showAddServicesBtn;
		scope.fieldData = datacontext.getFieldData();
		scope.basicSettings = angular.fromJson(scope.fieldData.basicsettings);
		scope.packingSettings = {};
		scope.packingSettings.account = false;
		scope.packingSettings.labor = false;

		if (angular.isDefined(scope.basicSettings.packing_settings)) {
			scope.packingService = {};
			scope.packingService.LM = {};
			scope.packingService.LD = {};
			scope.packingService = {
				LM: {
					full: 0.5,
					partial: 50
				},
				LD: {
					full: 0.7,
					partial: 44
				}
			};
			if (angular.isDefined(scope.basicSettings.packing_settings.packingAccount)) {
				scope.packingSettings.account = scope.basicSettings.packing_settings.packingAccount;
			}
			if (angular.isDefined(scope.basicSettings.packing_settings.packingLabor)) {
				scope.packingSettings.labor = scope.basicSettings.packing_settings.packingLabor;
			}
			if (angular.isDefined(scope.basicSettings.packing_settings.packing)) {
				if (angular.isDefined(scope.basicSettings.packing_settings.packing.LM)) {
					scope.packingService.LM = scope.basicSettings.packing_settings.packing.LM;
				}
			}
			if (angular.isDefined(scope.basicSettings.packing_settings.packing)) {
				if (angular.isDefined(scope.basicSettings.packing_settings.packing.LD)) {
					scope.packingService.LD = scope.basicSettings.packing_settings.packing.LD;
				}
			}
		}

		additionalServicesFactory.getAvailableAdditionalServices().then(function (res) {
			try {
				let data = angular.fromJson(_.head(res));
				ContractFactory.initData({extra_data: data});
				scope.additionalServicesRef = angular.copy(data);
			} catch (e) {
				throw new Error(e.name + ',' + e.message);
			}
		}, function (reason) {
			// logger.error(reason, reason, "Error");
		});

		scope.$on('event.addService', function (e, s) {
			s = ContractFactory.extra_data.services[s];
			s.extra_services[1].services_default_value = 1;
			addService(s);
		});

		function addService(s) {
			scope.showAdditionalServicesRef.show = false;
			if (!!s) {
				scope.additionalServices.push(angular.copy(s));
				scope.addPacking.push(s);
			}
			else {
				scope.additionalServices.push({
					name: 'Custom Service',
					alias: 'custom_service',
					extra_services: [
						{
							services_read_only: false,
							services_default_value: 1
						}
					]
				});
				scope.addPacking.push({
					name: 'Custom Service',
					alias: 'custom_service',
					extra_services: [
						{
							services_read_only: false,
							services_default_value: 1
						}
					]
				});
			}
			scope.sumServicesChanged();
			saveServices();
		}

		function changeName(service) {
			service.alias = _generateAlias(service.name);
			saveServices();
		}

		function sumServicesChanged() {
			let sumService = additionalServicesFactory.getExtraServiceTotal(scope.additionalServices);
			ContractCalc.finance.serviceSum = sumService;
			return sumService.toFixed(2);
		}

		function editPackingLog(title, extra_charge, val) {
			var msg = {
				text: title + ' on ' + extra_charge.name + ' was changed on contract.',
				to: val
			};
			scope.editPacking.push(msg);
		}

		function showAddServicesBtn() {
			if (_.isUndefined(scope.pageSubmitted)) {
				return !scope.data.isSubmitted && !scope.isFamiliarization && !scope.isPrint;
			} else {
				return !scope.pageSubmitted && !scope.isFamiliarization && !scope.isPrint;
			}
		}

		function saveServices() {
			let monthlyStorageFeeDeleted;
			var details = {
				title: 'Additional services',
				text: [],
				date: moment().unix()
			};
			_.forEach(scope.addPacking, function (pack) {
				var msg = {
					text: 'Service ' + pack.name + ' was added on contract.'
				};
				details.text.push(msg);
			});
			_.forEach(scope.removePacking, function (pack) {
				if (pack.alias === 'monthly_storage_fee') {
					monthlyStorageFeeDeleted = true;
				}
				var msg = {
					text: 'Service ' + pack.name + ' was removed on contract.'
				};
				details.text.push(msg);
			});
			_.forEach(scope.editPacking, function (log) {
				details.text.push(log);
			});
			if (!_.isEmpty(details.text)) {
				$rootScope.$broadcast('contract.changed', details.text);
			}
			scope.addPacking = [];
			scope.editPacking = [];
			scope.removePacking = [];
			scope.request.request_all_data.invoice.extraServices = _.clone(scope.additionalServices);
			if (monthlyStorageFeeDeleted) {
				scope.request.request_all_data.monthlyStorageFeeDeleted = monthlyStorageFeeDeleted;
			}
			$rootScope.$broadcast('recalc.contract', scope.request.request_all_data);
		}

		function removeCharge(index) {
			if (!scope.additionalServices[index].alias) {
				var alias = scope.additionalServices[index].name;
				scope.additionalServices[index].alias = alias.toLowerCase().replace(/\s/g, '_');
			}
			var model = scope.additionalServices[index].alias;
			$rootScope.$broadcast('event:remove.SIT.service', model);
			scope.removePacking.push(scope.additionalServices[index]);
			scope.additionalServices.splice(index, 1);
			saveServices();
		}

		function _generateAlias(name) {
			var alias = name;
			if (name.length) {
				alias = (alias.toLowerCase()).replace(/\s/g, '_');
			}
			return alias;
		}

		var s = $rootScope.$on('event:change-service-value', function (e, model, value) {
			var keep = true;
			if (value != 'null') {
				angular.forEach(scope.additionalServices, function (item, i) {
					item.alias = _generateAlias(item.name);
					if (keep) {
						if (item.hasOwnProperty('alias')) {
							if (item.alias === model) {
								scope.additionalServices[i].extra_services[0].services_default_value = Number(value);
								keep = false;
							}
						}
					}
				});
			}
			if (value == 'null' || value == '' || !value) {
				angular.forEach(scope.additionalServices, function (item, i) {
					item.alias = _generateAlias(item.name);
					if (item.hasOwnProperty('alias')) {
						if (item.alias === model) {
							scope.removePacking.push(scope.additionalServices[i]);
							var details = {
								title: 'Additional services',
								text: [],
								date: moment().unix()
							};
							_.forEach(scope.removePacking, function (pack) {
								var msg = {
									text: 'Service ' + pack.name + ' was removed on contract.'
								};
								details.text.push(msg);
							});
							if (!_.isEmpty(details.text)) {
								$rootScope.$broadcast('contract.changed', details.text);
							}
							scope.removePacking = [];
							scope.additionalServices.splice(index, 1);
							scope.request.request_all_data.invoice.extraServices = _.clone(scope.additionalServices);
							ContractService.saveContractRequestData(scope.request);
						}
					}
				});
				saveServices();
				scope.saveContract();
			}
		});

		$rootScope.$on('event:save.SIT.service', function (evt, model, service) {
			var count = 0;
			var serviceNewID = undefined;
			var name = model.replace(/_/g, ' ').charAt(0).toUpperCase() + model.replace(/_/g, ' ').slice(1);
			_.forEach(scope.additionalServices, function (item, i) {
				item.alias = _generateAlias(item.name);
				if (item.hasOwnProperty('alias')) {
					if (item.alias === model) {
						scope.additionalServices[i].extra_services = service.extra_services;
						count++;
						saveServices();
					}
				}
			});
			_.forEach(scope.additionalServicesRef, function (item, i) {
				item.alias = _generateAlias(item.name);
				if (item.hasOwnProperty('alias')) {
					if (item.alias === model) {
						serviceNewID = i;
					}
				}
			});
			if (count == 0) {
				var newService = {};
				if (angular.isDefined(serviceNewID)) {
					newService = scope.additionalServicesRef[serviceNewID];
				} else {
					var nameServ = '';
					if (model == 'overnight_storage') {
						nameServ = 'Overnight Storage';
					} else if (model == 'monthly_storage_fee') {
						nameServ = 'Monthly Storage Fee';
					}
					newService = {
						name: nameServ,
						alias: model,
						extra_services: service.extra_services
					};
				}
				scope.addPacking.push(newService);
				var details = {
					title: 'Additional services',
					text: [],
					date: moment().unix()
				};
				_.forEach(scope.addPacking, function (pack) {
					var msg = {
						text: 'Service ' + pack.name + ' was added on contract.'
					};
					details.text.push(msg);
				});
				if (!_.isEmpty(details.text)) {
					$rootScope.$broadcast('contract.changed', details.text);
				}
				scope.addPacking = [];
				scope.additionalServices.push(newService);
				saveServices();
			}
		});

		$rootScope.$on('event:initial-storage-request-remove', function (evt, model) {
			_.forEach(scope.additionalServices, function (item, i) {
				item.alias = _generateAlias(item.name);
				if (item.hasOwnProperty('alias')) {
					if (item.alias === model) {
						scope.additionalServices.splice(i, 1);
						saveServices();
						scope.saveContract();
					}
				}
			});
		});

		$rootScope.$on('event:initial-value', function (e, model) {
			var count = 0;
			var serviceNewID = undefined;
			_.forEach(scope.additionalServicesRef, function (item, i) {
				item.alias = _generateAlias(item.name);
				if (item.hasOwnProperty('alias')) {
					if (item.alias === model) {
						serviceNewID = i;
					}
				}
			});
			_.forEach(scope.additionalServices, function (item, i) {
				item.alias = _generateAlias(item.name);
				if (item.hasOwnProperty('alias')) {
					if (item.alias === model) {
						count++;
					}
				}
			});
			if (count == 0 && angular.isDefined(serviceNewID)) {
				scope.addPacking.push(scope.additionalServicesRef[serviceNewID]);
				var details = {
					title: 'Additional services',
					text: [],
					date: moment().unix()
				};
				_.forEach(scope.addPacking, function (pack) {
					var msg = {
						text: 'Service ' + pack.name + ' was added on contract.'
					};
					details.text.push(msg);
				});
				if (!_.isEmpty(details.text)) {
					$rootScope.$broadcast('contract.changed', details.text);
				}
				scope.addPacking = [];
				scope.additionalServices.push(scope.additionalServicesRef[serviceNewID]);
				scope.request.request_all_data.invoice.extraServices = _.clone(scope.additionalServices);
				ContractService.saveContractRequestData(scope.request);
			}
			saveServices();
			scope.saveContract();
		});

		$rootScope.$on('event:release-service', function (e, model, value, method) {
			var serviceNewID = undefined;
			var count = 0;
			if (method == 'del') {
				angular.forEach(scope.additionalServices, function (item, i) {
					item.alias = _generateAlias(item.name);
					if (item.hasOwnProperty('alias')) {
						if (item.alias === model) {
							removeCharge(i);
						}
					}
				});
				//remove extra service
			} else if (method == 'add') {
				_.forEach(scope.additionalServicesRef, function (item, i) {
					item.alias = _generateAlias(item.name);
					if (item.hasOwnProperty('alias')) {
						if (item.alias === model) {
							serviceNewID = i;
						}
					}
				});
				_.forEach(scope.additionalServices, function (item, i) {
					item.alias = _generateAlias(item.name);
					if (item.hasOwnProperty('alias')) {
						if (item.alias === model) {
							count++;
						}
					}
				});
				if (count == 0 && angular.isDefined(serviceNewID)) {
					scope.additionalServices.push(scope.additionalServicesRef[serviceNewID]);
				}
				angular.forEach(scope.additionalServices, function (item, i) {
					item.alias = _generateAlias(item.name);
					if (item.hasOwnProperty('alias')) {
						if (item.alias === model) {
							scope.additionalServices[i].extra_services[0].services_default_value = Number(value);
						}
					}
				});
			}
			saveServices();
			scope.saveContract();
		});

		scope.$on('$destroy', function () {
			if (_.isFunction(s)) {
				s();
			}
		});
	}
}
