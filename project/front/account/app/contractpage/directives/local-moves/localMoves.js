!function() {

    let app = angular.module('contractModule');

    /**
     * Local Moves directive
     */

    app.directive("localMoves", localMoves);

	/*@ngInject*/
    function localMoves(ContractFactory, ContractCalc, $window, logger, datacontext,ContractService,
						SweetAlert, $http, RequestServices, $timeout, MoveCouponService, $rootScope,
						$uibModal, StorageService, ContractValue, contractDispatchLogs) {
            return {
                restrict: 'E',
				scope: {
					request: '=',
					request_data: '=requestData',
					balance_paid: '=balancePaid',
					services: '=',
					flatMiles: '<',
					mainBusy: '=busy',
					ifStorage: '<',
					maxSignCount: '=',
					additionalServices: '=',
					loadCustom: '<',
					showDeclarVal: '<',
					isDoubleDriveTime: '<',
					declarationValue: '=',
					requestReady: '<',
					navigation: '=',
					travelTimeSetting: '<',
					travelTime: '=',
					tabs: '<',
					imContract: '<',
					grandTotalLog: '=',
					saveContract: '&',
					saveReqDataInvoice: '&',
					mainFlipPage: '&flipPage',
					showPayment: '&',
					applyPayment: '&',
					removeDeclarSelectError: '&',
					getTips: '&',
					sendLetterPdf: '&',
					downloadPdfPage: '&',
				},
                templateUrl: 'app/contractpage/directives/local-moves/localMoves.html',
                controller: localMovesCtrl
            };

            function localMovesCtrl($scope) {
				const SIGNATURE_PREFIX = 'data:image/gif;base64,';

                $scope.saveStep = saveStep;
                $scope.submitContractBtn = submitContractBtn;
                $scope.submitContract = submitContract;
                $scope.totalLessDeposit = totalLessDeposit;
                $scope.paymentButton = paymentButton;
                $scope.showTransit = showTransit;
                $scope.showFirstSinature = showFirstSinature;
                $scope.sendContractLog = sendContractLog;
                $scope.openCouponModal = openCouponModal;
                $scope.removeDiscount = removeDiscount;
                $scope.showService = showService;
                $scope.openService = openService;
                $scope.openInventory = openInventory;
                $scope.flipPage = flipPage;
                $scope.storageButton = storageButton;
                $scope.startTimer = startTimer;
                $scope.stopTimer = stopTimer;

				$scope.calculation = {
					calculateWork: ContractCalc.calculateWork,
					totalCost: ContractCalc.totalCost
				};

                $scope.finance = ContractCalc.finance;
				$scope.cardImages = [];
                $scope.fieldData = datacontext.getFieldData();
                $scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);
                $scope.contract_page = angular.fromJson($scope.fieldData.contract_page);
                $scope.calcsettings = angular.fromJson($scope.fieldData.calcsettings);
                $scope.minHours = Number($scope.calcsettings.min_hours);
                $scope.content = $scope.contract_page.textContent;
                $scope.minLengthCustomBlock = 7;
                $scope.data = ContractFactory.factory;
				$scope.isFamiliarization = ContractValue.isFamiliarization;
                $scope.isPrint = ContractValue.isPrint;
				$scope.storageTotal = ContractFactory.storage.storageTotal;
				$scope.storageVolume = ContractFactory.storage.storageVolume;
				$scope.storageRateCuft = ContractFactory.storage.storageRateCuft;
				$scope.isAdminManager = ContractFactory.isAdminManager;

                angular.element(document).ready(function(){
                    ContractFactory.init();
                });

                $scope.$on('contract.changed', function(event, data){
                    $timeout(function () {
                        sendContractLog({msgToLog: data});
                    },300);
                });

                $scope.$on('event:save.SIT.service', function( ev, model, service){
                    if(_.isEmpty($scope.services))
						$scope.services = {};
					$scope.services[model] = service;
                    if(!_.isEmpty($scope.data.storageServices)) $scope.data.storageServices[model] = service;
                    else{
						$scope.data.storageServices = {};
						$scope.data.storageServices[model] = service;
                    }
                    if(model == 'monthly_storage_fee'){
						$scope.data.openRental = true;
                        $scope.navigation.active = _.indexOf($scope.navigation.pages,'Rental agreement');
                    }
                    $scope.saveContract();
                });

                $scope.$on('event:remove.SIT.service', function( ev, model){
                    delete $scope.services[model];
                    if($scope.data.storageServices) delete $scope.data.storageServices[model];
					$scope.saveContract();
                    if(model == 'monthly_storage_fee'){
                        //remove storage request
                        if($scope.request.request_all_data.storage_request_id)
                            StorageService.removeReqByID($scope.request.request_all_data.storage_request_id).then(function () {
                                delete $scope.request.request_all_data.storage_request_id;
	                            RequestServices.saveRequestDataContract($scope.request);
                            });
                    }
                });

                $scope.$on('createContractPdf', openDiscountStorageTransit);

                $scope.$watch('navigation.activeCrew', function(newValue, oldValue, scope) {
                    if(angular.isDefined(oldValue)){
                        if(newValue > oldValue){
                            if($scope.flatMiles){
								ContractFactory.flatStepsData.pickupStartTimeStep += 4;
								ContractFactory.flatStepsData.pickupStopTimeStep += 4;
								ContractFactory.flatStepsData.unloadStartTimeStep += 4;
								ContractFactory.flatStepsData.unloadStopTimeStep += 4;
                            }else{
								ContractFactory.stepsData.startTimeStep += 2;
								ContractFactory.stepsData.stopTimeStep += 2;
                            }
                        }
                        if(newValue < oldValue){
                            if($scope.flatMiles){
								ContractFactory.flatStepsData.pickupStartTimeStep -= 4;
								ContractFactory.flatStepsData.pickupStopTimeStep -= 4;
								ContractFactory.flatStepsData.unloadStartTimeStep -= 4;
								ContractFactory.flatStepsData.unloadStopTimeStep -= 4;
                            }else{
								ContractFactory.stepsData.startTimeStep -= 2;
								ContractFactory.stepsData.stopTimeStep -= 2;
                            }
                        }
                    }
                }, true);

                function flipPage() {
                    $scope.mainFlipPage();
                    $('body').scrollTop(0);
                }

                function storageButton() {
                    let access = angular.isDefined($scope.request.request_all_data.useInventory) ?
						$scope.request.request_all_data.useInventory : $scope.contract_page.useInventory;

                    if (!$scope.ifStorage) {
                        if (!access) {
                            return !$scope.data.openRental;
                        }
                        if (_.isEmpty($scope.request.inventory) && $scope.ifStorage) {
                            return false;
                        } else {
                            return !$scope.data.inventoryMoving.open;
                        }
                    } else {
                        return true;
                    }
                }

                function openInventory() {
                	if ($scope.data.isSubmitted) {
                		return false;
					}

                    let access = angular.isDefined($scope.request.request_all_data.useInventory) ?
						$scope.request.request_all_data.useInventory : $scope.contract_page.useInventory;

                    if (!access) {
						$scope.data.openRental = !$scope.data.openRental;
                        if ($scope.data.openRental) {
							$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Rental agreement');
                            $('body').scrollTop(0);
                        }
						$scope.saveContract();
                        return false;
                    }
                    if (_.isEmpty($scope.request.inventory) && $scope.ifStorage) {
						$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Add. Inventory');
                        $('body').scrollTop(0);
                    } else {
						$scope.data.inventoryMoving.open = !$scope.data.inventoryMoving.open;
                        if($scope.data.inventoryMoving.open) {
							$scope.navigation.active = _.indexOf($scope.navigation.pages, 'Inventory');
                            $('body').scrollTop(0);
                        }
                    }
					$scope.saveContract();
                }

                function showService(type) {
                    var service = _.find($scope.content.storageFit, {connected_service: type});
                    if($scope.services)
                        if($scope.services[type]) return false;
                    return _.get(service, 'show', false);
                }

                function showFirstSinature(){
                    if($scope.contract_page.first_signature === false) return false;
                    if(angular.isDefined($scope.contract_page.first_signature) && $scope.contract_page.first_signature){
                        return $scope.contract_page.lessInitialContract ? false : true;
                    }else {
                        return $scope.contract_page.lessInitialContract ? false : true;
                    }
                }

                function setLessSignatures(arr){
                    var data_sign = {
                            date:"",
                            owner:"customer",
                            value:""
                    };
                    _.forEach(arr, function(val){
						$scope.data.signatures[val] = data_sign;
						$scope.data.signatures.length += 1;
						$scope.data.currentStep = $scope.data.signatures.length - 1;
                    });
                    if(!$scope.flatMiles){
                        startTimer();
                        stopTimer();
                    }else{
                        pickupTimer('start');
                        pickupTimer('end');
                        unloadTimer('start');
                        unloadTimer('end');
                    }
                    return $scope.data.signatures.length;
                }

                function showTransit(){
                    $scope.openStorageAndTransit = !$scope.openStorageAndTransit;
					$scope.data.openStorageAndTransit = $scope.openStorageAndTransit;
					$scope.saveContract({data: true, missModalClose: true});
                }

                function openDiscountStorageTransit() {
					ContractFactory.factory.showDiscount = true;
					ContractFactory.factory.openStorageAndTransit = true;
                }

                function isCanvasBlank(elementID) {
                    var canvas = document.getElementById(elementID);
                    var blank = document.createElement('canvas');
                    blank.width = canvas.width;
                    blank.height = canvas.height;
                    var context = canvas.getContext("2d");
                    var perc = 0, area = canvas.width*canvas.height;
                    if(canvas.toDataURL() == blank.toDataURL()){
                        return canvas.toDataURL() == blank.toDataURL();
                    }else{
                        var data = context.getImageData(0, 0, canvas.width, canvas.height).data;
                        for (var ct=0, i=3, len=data.length; i<len; i+=4) if (data[i]>50) ct++;
                        perc = parseFloat((100 * ct / area).toFixed(2));
                        return (perc < 0.1);
                    }
                }

                function calculateTotal(){
                    if($scope.contract_page.lessInitialContract) return true;
                    else return ($scope.data.signatures.length >= $scope.maxSignCount - 2);
                }

                function totalLessDeposit() {
                    let total = 0;

                    if (calculateTotal()) {
                        total = parseFloat(($scope.calculation.totalCost() - ContractCalc.finance.deposit - $scope.balance_paid).toFixed(2));
                        if (total < 1 && total > -1) {
							total = 0;
						}
                    }

                    return total;
                }
                function paymentButton() {
                    return (parseFloat(totalLessDeposit()) == 0)
                }

                function submitContract(){
                        if($scope.contract_page.lessInitialContract){
                            if($scope.data.req_comment && $scope.data.req_comment != ''){
                                return ($scope.data.signatures.length > $scope.maxSignCount-1);
                            }else{
                                return ($scope.data.signatures.length > $scope.maxSignCount-2);
                            }
                        }else{
                            return ($scope.data.signatures.length >= $scope.maxSignCount)
                        }
                }

                function saveStep () {
                    var blank = false;
                    if(!$scope.prevSignature)
                        blank = isCanvasBlank('signatureCanvas');
                    if(blank){
                        SweetAlert.swal("Failed!", 'Sign please', "error");
                    }else{
                        var stepId = Number($scope.data.currentStep);
                        if (isNaN(stepId)) return false;

                        var signatureValue = ContractFactory.getCropedSignatureValue();
                        var ln = $scope.data.signatures.length;
                        var nextStep = ln;

                        if ($scope.data.signatures.hasOwnProperty(stepId)) {
                            logger.warning('You can\'t perform this action', 'Something going wrong',stepId, $scope.data.signatures.length,$scope.data.signatures);
                            return false;
                        }

                        if (stepId == 2) {
                            if ($scope.data.declarationValue.selected.length == 0) {
                                logger.error('Please Select Declaration', null, 'Error');
                                return _setStepError('declaration');
                            }else{
                                var $declaration = $('.contract li select');
                                if ($declaration.hasClass('error')) {
                                    $declaration.removeClass('error');
                                }
                            }
                        }

                        if (ln) { //Check validation
                            var c = 0, keepGoing = true;
                            angular.forEach ($scope.data.signatures, function (sign, index) {
                                if (keepGoing) {
                                    if (index != c) {
                                        nextStep = c;
                                        keepGoing = false;
                                    }
                                    c++;
                                }
                            });
                            if (nextStep != stepId) {
                                if(!$scope.contract_page.lessInitialContract && stepId != $scope.maxSignCount - 1){
                                    if (nextStep == ContractFactory.stepsData.startTimeStep || nextStep == ContractFactory.stepsData.stopTimeStep) {
										$scope.navigation.activeCrew = $scope.data.crews.length - 1
                                    }
                                    return _setStepError(nextStep);
                                    }
                            } else {
                                var $sbox = $('.signature-box');
                                if ($sbox.hasClass('error')) {
                                    $sbox.removeClass('error');
                                }
                            }
                        }
                        else if (!ln && stepId != 0) {
                            return _setStepError(nextStep);
                        }

                        if(!$scope.flatMiles){
                            if (stepId == ContractFactory.stepsData.startTimeStep) {
                                var rightTime = startTimer();
                                if(rightTime == false)
                                    return false;
                            }
                            if (stepId == ContractFactory.stepsData.stopTimeStep) {
                                var rightTime = stopTimer();
                                if(rightTime == false)
                                    return false;
                            }
                        }else if($scope.flatMiles){
                            if (stepId == ContractFactory.flatStepsData.pickupStartTimeStep ) {
                                var rightTime = pickupTimer('start');
                                if(rightTime == false)
                                    return false;
                            }
                            if (stepId == ContractFactory.flatStepsData.pickupStopTimeStep ) {
                                var rightTime = pickupTimer('end');
                                if(rightTime == false)
                                    return false;
                            }
                            if (stepId == ContractFactory.flatStepsData.unloadStartTimeStep) {
                                var rightTime = unloadTimer('start');
                                if(rightTime == false)
                                    return false;
                            }
                            if (stepId == ContractFactory.flatStepsData.unloadStopTimeStep) {
                                var rightTime = unloadTimer('end');
                                if(rightTime == false)
                                    return false;
                            }
                        }

                        var signature = {
                            owner: $scope.data.currentOwner,
                            value: SIGNATURE_PREFIX + signatureValue,
                            date: moment().format('x')
                        };
                        if($scope.prevSignature){
                            signature.owner = $scope.data.signatures[stepId-1].owner;
                            signature.value = $scope.data.signatures[stepId-1].value;
                        }

						$scope.data.signatures[stepId] = signature;
						$scope.data.signatures.length += 1;
                        nextStep = $scope.data.signatures.length;
                        ContractFactory.clearSignaturePad();
                        if(nextStep == 2 && $scope.contract_page.lessInitialContract){
                            var arr = {};
                            if(!$scope.flatMiles){
								ContractFactory.stepsData.declar = 2;
                                arr = ContractFactory.stepsData;
                            }else{
								ContractFactory.flatStepsData.declar = 2;
                                arr = ContractFactory.flatStepsData;
                            }
                            nextStep = setLessSignatures(arr);
                            if(ContractFactory.stepsData.declar)
                                delete ContractFactory.stepsData.declar;
                            if(ContractFactory.flatStepsData.declar)
                                delete ContractFactory.flatStepsData.declar;
                        }
                        var msgToLog = '';
                        msgToLog += signature.owner.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})+ ' sign step #'+ stepId;
                        ContractService.addForemanLog(msgToLog,"Contract signature",$scope.request.nid, 'MOVEREQUEST');

                        //Save in local storage for keep away other issues
                        var pendingSignatures = JSON.parse(localStorage['pendingSignatures']);
                            pendingSignatures[stepId] = signature;

                            try {
	                            localStorage['pendingSignatures'] = JSON.stringify(pendingSignatures);
                            } catch (error) {
	                            let errorStorageLog = _.isObject(error) ? angular.toJson(error): error;

	                            Raven.captureException(errorStorageLog);
                            }

						$scope.saveContract( {
                            callback: function () {
                                localStorage['pendingSignatures'] = JSON.stringify({});
                            }
                        });
                        $scope.prevSignature = false;
                        $('#signaturePad').modal('hide');
                    }
                }

                function _setStepError(nextStep) {
                    if(nextStep == 'checkout_btn'){
                        $('#errorSign_'+ nextStep).addClass('errorArrow');
                        var el = $('.' + nextStep);
                        el.addClass('error');
                        if(!$scope.contract_page.lessInitialContract && $scope.navigation.active == _.indexOf($scope.navigation.pages, 'Bill of lading'))
                            $('html, body').animate({scrollTop: el.offset().top}, 1000);
                        $('#signaturePad').modal('hide');
                        return false;
                    }
                    var el = $('#step_' + nextStep);
                    el.addClass('error');
                    if($scope.navigation.active == _.indexOf($scope.navigation.pages, 'Bill of lading'))
                        $('html, body').animate({
                            scrollTop: el.offset().top
                        }, 1000);
                    $('#signaturePad').modal('hide');
                    return false;
                }

                function pickupTimer(type){
                    var active = $scope.navigation.activeCrew;
                    var dateRaw = $('#edit-'+type+'-pickup-time-'+active).val().split(' ');
                    var dateTime = dateRaw[0].split(':');
                    if(!dateRaw[1]) return false;
                    var dateAmPm = dateRaw[1].toLowerCase();
                    // Convert 12h to 24h...
                    if(dateAmPm == 'pm') {
                        if(parseInt(dateTime[0]) < 12) dateTime[0] = (parseInt(dateTime[0])+12).toString();
                    }
                    else {
                        if(parseInt(dateTime[0]) == 12) dateTime[0] = 0;
                    }

                    var selectedTime = new Date();
                    selectedTime.setHours(dateTime[0]);
                    selectedTime.setMinutes(dateTime[1]);
                    selectedTime.setSeconds(0);

                    $scope.data.crews[active].timer.lastAction = type;
                    if(type == 'start'){
                        $scope.data.crews[active].timer.startPickUp = $('#edit-'+type+'-pickup-time-'+active).val();
                    }else if(type == 'end'){
                        $scope.data.crews[active].timer.stopPickUp = $('#edit-'+type+'-pickup-time-'+active).val();
                    }
                    if($scope.data.crews[active].timer.stop != 0)
                        $scope.data.crews[active].timer.stop = 0;
                    if($scope.data.crews[active].timer.start != 0)
                        $scope.data.crews[active].timer.start = 0;
					$scope.calculation.calculateWork($scope.data.crews[active]);

                    $scope.saveContract( {
                        errorCb: function () {}
                    });
                }

                function unloadTimer(type){
                    var active = $scope.navigation.activeCrew;
                    var dateRaw = $('#edit-'+type+'-unload-time-'+active).val().split(' ');
                    var dateTime = dateRaw[0].split(':');
                    if(!dateRaw[1]) return false;
                    var dateAmPm = dateRaw[1].toLowerCase();
                    // Convert 12h to 24h...
                    if(dateAmPm == 'pm') {
                        if(parseInt(dateTime[0]) < 12) dateTime[0] = (parseInt(dateTime[0])+12).toString();
                    }
                    else {
                        if(parseInt(dateTime[0]) == 12) dateTime[0] = 0;
                    }

                    var selectedTime = new Date();
                    selectedTime.setHours(dateTime[0]);
                    selectedTime.setMinutes(dateTime[1]);
                    selectedTime.setSeconds(0);

                    $scope.data.crews[active].timer.lastAction = type;
                    if(type == 'start'){
                        $scope.data.crews[active].timer.startUnload = $('#edit-'+type+'-unload-time-'+active).val();
                    }else if(type == 'end'){
                        $scope.data.crews[active].timer.stopUnload = $('#edit-'+type+'-unload-time-'+active).val();
                    }
                    if($scope.data.crews[active].timer.stop != 0)
                        $scope.data.crews[active].timer.stop = 0;
                    if($scope.data.crews[active].timer.start != 0)
                        $scope.data.crews[active].timer.start = 0;
					$scope.calculation.calculateWork($scope.data.crews[active]);

                    $scope.saveContract( {
                        errorCb: function () {}
                    });
                }

                function startTimer() {
                    var active = $scope.navigation.activeCrew;

                    var dateRaw = $('#edit-start-time-'+active).val().split(' ');
                    var dateTime = dateRaw[0].split(':');
                    if(!dateRaw[1]) return false;
                    var dateAmPm = dateRaw[1].toLowerCase();

                    // Convert 12h to 24h...
                    if(dateAmPm == 'pm') {
                        if(parseInt(dateTime[0]) < 12) dateTime[0] = (parseInt(dateTime[0])+12).toString();
                    }
                    else {
                        if(parseInt(dateTime[0]) == 12) dateTime[0] = 0;
                    }

                    var selectedTime = new Date();
                    selectedTime.setHours(dateTime[0]);
                    selectedTime.setMinutes(dateTime[1]);
                    selectedTime.setSeconds(0);

                    $scope.data.crews[active].timer.lastAction = 'start';
                    $scope.data.crews[active].timer.start = $('#edit-start-time-'+active).val();

					$scope.calculation.calculateWork($scope.data.crews[active]);

                    $scope.saveContract( {
                        errorCb: function () {}
                    });
                }

                function stopTimer() {
                    var active = $scope.navigation.activeCrew;
                    var time =  $('#edit-end-time-'+active).val();
					$scope.data.crews[active].timer.lastAction = 'stop';
					$scope.data.crews[active].timer.stop = time;

					$scope.calculation.calculateWork($scope.data.crews[active]);

					$scope.saveContract( {
                        errorCb: function(rollback) {}
                    });
                }

                function submitContractBtn (param) {
					if (!$scope.isAdminManager) {
						_setStepError('checkout_btn');
						if (parseFloat(totalLessDeposit()) != 0) {
							SweetAlert.swal("Failed!", "Total less deposit received isn't 0", "error");
							return false;
						}
					}
					removeError();
					if (localStorage[$scope.request.nid]) {
						SweetAlert.swal("Internet connection problem. Please contact your manager.", "", "error");
						return false
					}
					if ($scope.contract_page.lessInitialContract) {
						var arr = [];
						if (!$scope.data.req_comment) {
							arr.push($scope.maxSignCount - 1);
							setLessSignatures(arr);
						}
					}
					ContractFactory.factory.showDiscount = true;
					ContractFactory.factory.openStorageAndTransit = true;
					$scope.saveContract(param)
						.then(() => {
							ContractFactory.factory.isSubmitted = true;
							sendContractLog({msgToLog: "Contract was submitted"});
							contractDispatchLogs.assignLogs($scope.request);
						});
				}

                function removeError(){
                    var ln = $scope.data.signatures.length;
                    for(var i=0; i<ln; i++){
                        var arrow = $('#errorSign_'+ i);
                        var el = $('#step_' + i);
                        if (el.hasClass('error')) {
                            el.removeClass('error');
                        }
                        if (arrow.hasClass('errorArrow')) {
                            arrow.removeClass('errorArrow');
                        }
                    }
                    var btnPay = $('.checkout_btn');
                    if (btnPay.hasClass('error')) {
                        btnPay.removeClass('error');
                    }
                    var btnPayArrow = $('#errorSign_checkout_btn');
                    if (btnPayArrow.hasClass('errorArrow')) {
                        btnPayArrow.removeClass('errorArrow');
                    }
                    var declaration = $('#step_declaration');
                    if (declaration.hasClass('error')) {
                        declaration.removeClass('error');
                    }
                    var declarationArrow = $('#errorSign_declaration');
                    if (declarationArrow.hasClass('errorArrow')) {
                        declarationArrow.removeClass('errorArrow');
                    }
                }
                function sendContractLog({msgToLog}) {
                    var data = [];
                    var totalcost = {
                        text: 'Total cost changed',
                        from: `$${_.round($scope.grandTotalLog, 2)}`,
                        to: `$${_.round($scope.calculation.totalCost(), 2)}`
                    };
                    var newMsg = {
                        text: msgToLog
                    };
                    if(_.isArray(msgToLog)){
                        data.push(totalcost);
                        _.forEach(msgToLog, function (log) {
                            data.push(log);
                        });
                    }else{
                        data.push(newMsg, totalcost);
                    }
                    RequestServices.sendLogs(data, 'Contract changed', $scope.request.nid, 'MOVEREQUEST').then(function () {
                        $scope.grandTotalLog = $scope.calculation.totalCost();
                    });
                }

                function openCouponModal(){
                    SweetAlert.swal({
                            title: "Enter promo code of your coupon",
                            text: "",
                            type: "input",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            confirmButtonText: "Use coupon",
                            animation: "slide-from-top",
                            inputPlaceholder: "Promo code",
                            inputType: "text",
                            showLoaderOnConfirm: true
                        },
                        function (inputValue) {
                            if (inputValue === false) return false;
                            if (inputValue === "") {
                                swal.showInputError("You need to write something!");
                                return false
                            }
                            $scope.promoCode = inputValue;
                            MoveCouponService.getCouponByPromo($scope.promoCode).then(
                                function (data) {
                                    if(_.isEmpty(data)) {
                                        swal.showInputError("Error", "This promo code isn't exist", "error");
                                        return false;
                                    }
                                    var info  = _.head(data);
                                    if (info.used == 1) {
                                        swal.showInputError("Error", "Coupon is used already", "error");
                                        return false;
                                    } else {
                                        info.used = 1;
                                        MoveCouponService.updateUserCoupon(info, info.id).then(
                                            function () {
                                                SweetAlert.swal("Success", "Coupon has been successfully used", "success");
                                                $rootScope.$broadcast('coupon.used', info);
                                            },
                                            function (error) {
                                                SweetAlert.swal("Error", error, "error");
                                            }
                                        )
                                    }
                                },
                                function (error) {
                                  swal.showInputError("Error", "This promo code isn't exist", "error");
                                }
                            )
                        }
                    );
                }

                function removeDiscount() {
                    $scope.mainBusy = true;
                    MoveCouponService.getUserCoupon($scope.request.request_all_data.couponData.discountId).then(
                        function (data) {
                            if(!data){SweetAlert.swal("Error", "", "error"); return false;}
                            var coupon  = data;
                            coupon.used = 0;
                            MoveCouponService.updateUserCoupon(coupon, coupon.id).then(
                                function () {
									$scope.mainBusy = false;
                                    SweetAlert.swal("Success", "Coupon has been successfully removed", "success");
                                    $rootScope.$broadcast('coupon.used');
                                },
                                function (error) {
									$scope.mainBusy = false;
                                    SweetAlert.swal("Error", error, "error");
                                }
                            )
                        }
                    );
                }

                function openServiceModal(service, step) {
                    var connectedServiceInstance = $uibModal.open({
                        templateUrl: 'app/contractpage/directives/storage-fit/storageFit.html',
                        controller: connectedServiceCtrl,
                        size: 'lg',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            storageTotal: function () {
                                return $scope.storageTotal;
                            },
                            storageVolume: function () {
                                return $scope.storageVolume;
                            },
                            storageRateCuft: function () {
                                return $scope.storageRateCuft;
                            },
                            service: function () {
                                return service;
                            },
                            step: function () {
                                return step;
                            },
                            services: function () {
                                return $scope.additionalServices;
                            },
                            request: function () {
                                return $scope.request;
                            }
                        }
                    });
                }

                function openService(type) {
                	if ($scope.data.isSubmitted) {
						return false;
					}

                    let service = _.find($scope.content.storageFit, {connected_service: type});
                    let step = type == 'monthly_storage_fee' ? 0 : 1;
					openServiceModal(service, step);
                }

                function connectedServiceCtrl($scope, $rootScope, $uibModalInstance, storageTotal, storageVolume,
											  storageRateCuft, service, step, services, request, StorageService,
											  ContractFactory) {
                    $scope.storageTotal = storageTotal;
                    $scope.storageVolume = storageVolume;
                    $scope.storageRateCuft = storageRateCuft;
                    $scope.services = services;
                    $scope.service = service;
                    $scope.request = request;
                    $scope.steps = [false, false];
                    $scope.storage = false;
                    if(step == 1) goSecondStep();
                    if(step == 0){ $scope.storage = true; $scope.steps[0] = true;}

                    if(step == 0)
                        StorageService.getDefaultStorage().then(function ({data}) {
                            if(!data.id || !data.value) {
                                SweetAlert.swal("Failed! No default storages", '', "error");
                                return false;
                            }
                            $scope.storageRateCuft = Number(data.value.rate_per_cuft);
                            $scope.storageMinCuft = Number(data.value.min_cuft);
                        });
                    var getCropedSignatureValue = function () {
						return ContractFactory.getSignatureUrl($scope.signaturePad._ctx, true);
                    };

                    function isCanvasBlank(elementID) {
                        var canvas = document.getElementById(elementID);
                        var blank = document.createElement('canvas');
                        blank.width = canvas.width;
                        blank.height = canvas.height;
                        var context = canvas.getContext("2d");
                        var perc = 0, area = canvas.width * canvas.height;
                        if (canvas.toDataURL() == blank.toDataURL()) {
                            return canvas.toDataURL() == blank.toDataURL();
                        } else {
                            var data = context.getImageData(0, 0, canvas.width, canvas.height).data;
                            for (var ct = 0, i = 3, len = data.length; i < len; i += 4) if (data[i] > 50) ct++;
                            perc = parseFloat((100 * ct / area).toFixed(2));
                            return (perc < 0.1);
                        }
                    }

                    $scope.goSecondStep = goSecondStep;
                    $scope.returnStep = returnStep;
                    $scope.changeVolume = changeVolume;
                    $scope.saveService = saveService;
                    $scope.cancel = cancel;

                    function goSecondStep() {
                        if(!$scope.storageVolume && step == 0){
                            $scope.errorStorageVolume = true;
                            return false;
                        }
                        setTimeout(function () {
                            $scope.canvas = document.getElementById('signatureCanvasService');
							if($scope.canvas != null){
								$scope.signaturePad = new SignaturePad($scope.canvas);
							}
							else {
								goSecondStep();
							}
                        }, 1000);
                        $scope.steps[0] = false;
                        $scope.steps[1] = true;
                    }

                    function returnStep(stepInd) {
                        _.forEach($scope.steps, function (item, ind) {
                            $scope.steps[ind] = false;
                        });
                        $scope.steps[stepInd] = true;
                    }

                    function changeVolume() {
                        if(Number($scope.storageMinCuft) < Number($scope.storageVolume)) $scope.storageTotal = Number(($scope.storageVolume * $scope.storageRateCuft).toFixed(2));
                        else $scope.storageTotal = Number((Number($scope.storageMinCuft) * $scope.storageRateCuft).toFixed(2));
                    }

                    function saveService() {
                        var signatureValue = getCropedSignatureValue();
                        const SIGNATURE_PREFIX = 'data:image/gif;base64,';
                        var signature = {
                            value: SIGNATURE_PREFIX + signatureValue,
                            date: moment().format('x')
                        };
                        var blank = isCanvasBlank('signatureCanvasService');
                        if(blank){
                            SweetAlert.swal("Failed!", 'Sign please', "error");
                        }else{
							$scope.mainBusy = true;
                            $rootScope.$broadcast('save.service', { service: $scope.service, signature: signature, request: $scope.request, data:{volume: $scope.storageVolume, total: $scope.storageTotal}});
                        }
                    }

                    function cancel() {
							  $uibModalInstance.dismiss('cancel');
                    }

                    $scope.$on('close.modal.service', function () {
                        $scope.cancel();
                    });
                }
			}
        }
}();
