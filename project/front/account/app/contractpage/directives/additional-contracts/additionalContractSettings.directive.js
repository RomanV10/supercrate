'use strict';

angular
    .module('contractModule')
    .directive('additionalContractSettings', additionalContractSettings);
	additionalContractSettings.$inject = ['ContractValue'];

function additionalContractSettings(ContractValue) {
    /* implementation details */
    let directive = {
        link: link,
        templateUrl: 'app/contractpage/directives/additional-contracts/additionalContractSettings.directive.html',
        scope: {
            contractSettings: '='
        },
        restrict: 'EA'
    };
    return directive;

    function link(scope, element, attrs) {
        scope.tabs = [];
        scope.contractSettings.usingTabs = [];

        _.forEach(scope.contractSettings.additionalContract, (tab, index) => {
            if (!tab.removedPage) {
                tab.index = index;
                scope.tabs.push(tab);

                scope.contractSettings.usingTabs.push(index);
            }
        });

        scope.addNewTab = addNewTab;
        scope.selectTab = selectTab;
        scope.removePage = removePage;
        scope.changeMainContract = changeMainContract;
        scope.changeShow = changeShow;
        scope.indexFirstTab = _.get(scope, 'tabs[0].index', 0);
        scope.activeTab = scope.indexFirstTab;

        function addNewTab() {
            let isEmptyAdditionalContract = _.isEmpty(scope.contractSettings.additionalContract);
            let index = 0;

            if (!isEmptyAdditionalContract) {
            	let lastAddContractItemIndex = scope.contractSettings.additionalContract.length - 1;
                index = _.get(scope.contractSettings.additionalContract, `[${lastAddContractItemIndex}].index`, 0) + 1;
            }

            let obj = {
                id: createRandomId(),
                name: '',
                selected: false,
                show: true,
                page:'',
                mainContract: false,
                allowAttachPage: false,
				allowSendLetterPdf: false,
                removedPage: false,
                printDownloadPage: false,
                proposalPage: false,
                index: index
            };

            scope.tabs.push(obj);
            scope.contractSettings.additionalContract.push(obj);
            scope.activeTab = _.get(scope, `tabs[${scope.tabs.length - 1}].index`, 0);
            scope.contractSettings.usingTabs.push(index);
        }

        function selectTab(index) {
            if (_.isUndefined(index)) return;
            scope.activeTab = index;
            _.forEach(scope.tabs, (tab) => {
                tab.selected = false;
            });
            getTabByIndex(index).selected = true;
        }

        function removePage(index) {
            let currentTab = getTabByIndex(index);

            currentTab.proposalPage = false;
            currentTab.mainContract = false;
            currentTab.removedPage = true;
            let selectIndex = index == scope.indexFirstTab ? nextPageIndex(index) : previousPageIndex();

            selectTab(selectIndex);
        }

		function changeShow(index) {
			let tab = getTabByIndex(index);
			if(!tab.show && tab.mainContract) {
				tab.mainContract = false;
			}
		}

        function changeMainContract(index) {
        	let tab = getTabByIndex(index);
            if(tab.mainContract) {
                _.forEach(scope.tabs, (tab) => {
                    tab.mainContract = false;
                });
				tab.show = true;
				tab.mainContract = true;
            }
        }

        function previousPageIndex() {
            return _.find(scope.tabs, (tab) => tab).index;
        }

        function nextPageIndex(index) {
            return _.find(scope.tabs, (tab) => tab.index > index).index;
        }

        function getTabByIndex(index) {
            return _.find(scope.tabs, (tab) => tab.index == index);
        }
    }

    function createRandomId() {
        let id = '';
        let symbols = ContractValue.alphabetNumber;

        for(let i = 0; i < 32; i++) {
            id += symbols.charAt(Math.floor(Math.random() * symbols.length));
        }

        return id;
    }
}
