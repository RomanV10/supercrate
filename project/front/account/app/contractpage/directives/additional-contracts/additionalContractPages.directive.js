'use strcit';

angular
    .module('contractModule')
    .directive('additionalContractPages', additionalContractPages);

/*@ngInject*/
function additionalContractPages(datacontext, $compile, $rootScope, $filter, ContractCalc,
								 PaymentServices, ContractValue, ContractFactory, CalculatorServices,
								 accountConstant) {
    let directive = {
        link: link,
        transclude: 'true',
        scope: {
            contractPage: '=',
            request: '=',
            contractTab: '=',
            contractIndex: '=',
            travelTime: '=',
			isFirstSubmittedPage: '<',
			calculateAddContractData: '&',
			saveContract: '&',
			getAddMainPage: '&'
        },
        restrict: 'EA'
    };
    return directive;

    function link(scope, element) {
		let flatRateQuote = _.round(scope.request.request_all_data.invoice.flat_rate_quote.value, accountConstant.roundUp) ||
			_.round(scope.request.flat_rate_quote.value, accountConstant.roundUp) || scope.contractPage.flat_rate_quote || 0;

        scope.fieldData = datacontext.getFieldData();
        scope.basicSettings = angular.fromJson(scope.fieldData.basicsettings);
        scope.contract_page = angular.fromJson(scope.fieldData.contract_page);
		scope.calcSettings = angular.fromJson(scope.fieldData.calcsettings);
		scope.contractPage.page_id = _.get(scope, 'contractPage.page_id', _.get(scope, 'contractTab.id'));

		scope.changeTime = changeTime;
		scope.changeTravelFee = changeTravelFee;
		scope.changeRatePerHour = changeRatePerHour;
		scope.changeContractTotal = changeContractTotal;
		scope.totalLessDeposit = totalLessDeposit;
		scope.paymentButton = paymentButton;
		scope.applyPayment = applyPayment;
		scope.totalBalancePaid = totalBalancePaid;
		scope.getLaborTotal = getLaborTotal;
		scope.numberFormatting = numberFormatting;
		scope.changeFlatRateQuote = changeFlatRateQuote;

        let requestRate;
		let companyName = scope.basicSettings.company_name;
        let companyPhone = scope.basicSettings.company_phone;
        let companyEmail = scope.basicSettings.company_email;
        let companyAddress = scope.basicSettings.company_address;
        let companyState = scope.basicSettings.main_state;
        let initialsValue = scope.request.name;
		let reservationReceived = ContractCalc.finance.deposit;
		let isMainPage = scope.contractTab.mainContract && (!scope.contractPage.isSubmitted || scope.isFirstSubmittedPage);

		if (isMainPage && (scope.request.service_type.raw == 7 || scope.request.service_type.raw == 5)) {
			requestRate = 0;
		} else {
			requestRate = scope.request.rateDiscount ||
				+_.get(scope.request, 'request_all_data.invoice.rate.value', scope.request.rate.value);
		}

        scope.isAdminManager = ContractFactory.isAdminManager;
        scope.openPay = false;
		scope.isFamiliarization = ContractValue.isFamiliarization;
		scope.isPrint = ContractValue.isPrint;
		scope.modal = {
			showTextareaSettings: -1
		};
        const HUNDRED_PERCENT = ContractValue.hundredPercent;
        let customTax = 0;
        let requestVariable = ContractValue.requestVariable;
        let contractVariable = ContractValue.contractVariable;
        let simpleInput = '<input type="text" ng-model="" ng-value="" ng-disabled="" id="" class="input-md">';
        let contractTotalInput = '<input type="text" ng-model="" ng-value="" ng-disabled="" id="" ng-blur="changeContractTotal(); numberFormatting()" class="input-md">';
        let numberContractTotalInput = '<span ng-form=""><input name="number" ng-model="" ng-value="" ng-disabled="" id="" ng-blur="changeContractTotal(); numberFormatting()" class="input-md" ng-class="{ \'has-error\': ngForm.number.$error.number}"></span>';

        scope.settingsMenu = {
            '%COMPANY_NAME%': companyName,
            '%COMPANY_LOGO%': '<img src="' + scope.basicSettings.company_logo_url + '" width="170px" id="company_logo_img">',
            '%COMPANY_PHONE%': companyPhone,
            '%COMPANY_EMAIL%': companyEmail,
            '%COMPANY_ADDRESS%': companyAddress,
            '%COMPANY_STATE%': companyState,
            '%rate_per_hour%': '<input type="text" ng-model="" ng-value="" ng-disabled="" id="" ng-blur="changeRatePerHour(); numberFormatting();" class="input-md"/>',
            '%simple_input%': simpleInput,
            '%initial_input%': simpleInput,
            '%signature_field%': '<signature-box admin="isAdminManager" step-id=""></signature-box>',
            '%textarea_field%': '<div class="row"><div class="bottom-margin">'+
                                '<div class="text-field-block" ng-if="contractPage.isSubmitted">{{}}</div>'+
                                '<textarea auto-resize-texarea rows="5" ng-if="!contractPage.isSubmitted" class="form-control" ng-model="" ng-value="" ng-disabled="" placeholder="Enter description"></textarea>' +
                                '</div></div>',
            '%checkbox%': '<input type="checkbox" ng-model="" ng-value="" ichecks ng-disabled="isFamiliarization || isPrint || contractPage.isSubmitted" class="input-md">',
            '%request_id%': simpleInput,
            '%extra_services_packing%': '<div class="contract-box-content no-border packing-extra-services">' +
                                        '<table>' +
                                        '<tbody packing-table request="request" page-submitted="contractPage.isSubmitted"></tbody>' +
                                        '<tbody extra-services-table request="request" page-submitted="contractPage.isSubmitted" save-contract="saveContract()"></tbody>' +
                                        '</table></div>',
            '%address_from%': simpleInput,
            '%address_to%': simpleInput,
            '%city_from%': simpleInput,
            '%city_to%': simpleInput,
            '%address_pickup%': simpleInput,
            '%address_dropoff%': simpleInput,
            '%city_pickup%': simpleInput,
            '%city_dropoff%': simpleInput,
            '%customer_name%': scope.request.name,
            '%customer_phone%': scope.request.phone,
            '%customer_email%': scope.request.email,
            '%state_dropoff%': simpleInput,
            '%state_pickup%': simpleInput,
            '%zip_dropoff%': simpleInput,
            '%zip_pickup%': simpleInput,
            '%state_to%': simpleInput,
            '%state_from%': simpleInput,
            '%zip_to%': simpleInput,
            '%zip_from%': simpleInput,
            '%crew_number%': '<input type="text" ng-model="" ng-value="" ng-disabled="" id="" ng-blur="changeContractTotal()" class="input-md"/>',
            '%truck_number%': simpleInput,
            '%travel_fee%': '<input type="text" ng-model="" ng-value="" ng-disabled="" id="" ng-blur="changeTravelFee(); numberFormatting();" class="input-md"/>',
            '%labor_time%': simpleInput,
            '%labor_total%': '<span ng-bind="getLaborTotal() | currency"></span>',
            '%start_time%': '<input cc-start-time ng-disabled="" isolated="true" ng-value=""' +
                            'type="text" name="start_time" size="60" maxlength="128" class="form-text md ui-timepicker-input" id="time"' +
                            'autocomplete="off" contract="false" field-type="start_time" index="1" add-contract="true"/>',
            '%end_time%': '<input cc-start-time ng-disabled="" isolated="true" ng-value=""' +
                            'type="text" name="end_time" size="60" maxlength="128" class="form-text md ui-timepicker-input" id="time"' +
                            'autocomplete="off" contract="false" field-type="end_time" index="1" add-contract="true"/>',
            '%time_off%': '<input cc-travel-time ng-disabled="" isolated="true" ng-model="" ng-value="" ng-change="changeTime(\'time_off\', contractPage.time_off)"' +
                            'type="text" name="start_time" size="60" maxlength="128" class="form-text md ui-timepicker-input" id="time"' +
                            'autocomplete="off" contract="false" field-type="time_off" index="1" add-contract="true"/>',
            '%current_date%': moment().format('MM/DD/YYYY'),
            '%move_date%': scope.request.request_all_data.invoice.date.value,
            '%contract_total%': contractTotalInput,
            '%fuel_surcharge%': contractTotalInput,
            '%valuation%': contractTotalInput,
            '%tips%': contractTotalInput,
            '%reservation_received%': reservationReceived,
            '%balance_due%': contractTotalInput,
            '%simple_contract_input%': numberContractTotalInput,
            '%total_balance_paid%': '<span>{{totalBalancePaid()}}</span>',
            '%payment_button%': '<div ng-if="!contractPage.isSubmitted" class="animated checkout_btn payment-button" ng-click="applyPayment(paymentButton())" ng-class="{\'checkout_btn_paid\': totalLessDeposit() == 0}">' +
            '<span ng-show="paymentButton()"><i class="icon-check"></i> </span>' +
            '<span ng-hide="paymentButton()"><span ng-show="!openPay">Pay</span><span ng-show="openPay"> ' +
            '<svg  style="margin-top: 12px;left: calc(50% - 12px);" class="spinner white" width="25px" height="25px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"> ' +
            '<circle class="path_white" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle> ' +
            '</svg></span></span></div>',
            '%additional_remainder_email%': '<span ng-form=""><input ng-class="{ \'has-error\': ngForm.email.$error.email }" name="email" type="email" ng-model="" ng-value="" ng-disabled="" id=""></span>',
			'%flat_rate_quote%': contractTotalInput
        };

        function initAdditionalPages() {
            if (scope.contractTab && scope.contractTab.page && scope.contractTab.show && !scope.contractTab.removedPage) {
                if (_.isEmpty(scope.contractPage))
                    scope.contractPage = {
                        isSubmitted: false
                    };
                var page = buildHtml(scope.contractTab.page);
                element.html(page).show();
                $compile(element.contents())(scope);
            }
        }
        initAdditionalPages();

        function getTemplate(contentType) {
            var template = '';
            if (scope.settingsMenu.hasOwnProperty(contentType)) {
                template = scope.settingsMenu[contentType];
            }
            return template;
        }

        function replaceStringAtPosition(originalString, sourceString, newString, position) {
            var linkLength = sourceString.length;
            var prefix = originalString.substring(0, position);
            var afterStartPosition = position + linkLength;
            var postfix = originalString.substring(afterStartPosition, originalString.length);
            var result = prefix + newString + postfix;

            return result;
        }

        function buildHtml(content, ind) {
            let nameModel = '';
            let html = '';
            let regCase = new RegExp('\%(.*?)\%', 'g');
            let regTags = new RegExp(/(<([^>]+)>)/, 'g');
            let keys = content.match(regCase);

            if (keys) {
                _.forEach(scope.settingsMenu, function (item, field) {
                    let key = field;
                    nameModel = key.replace(/%/g, '');
                    html = getTemplate(key);
                    let templateIndex = 0;
                    if (item && _.isString(item) && item.match(regTags) && item.match(regTags).length) {
                        while (content.search(key) != -1) {
                            let searchInd = content.search(key);
                            let endModelName = '';
                            if (_.indexOf(requestVariable, nameModel) == -1 && _.indexOf(contractVariable, nameModel) == -1) endModelName = '_' + templateIndex;
                            let template = html.replace('ng-model=""', 'ng-model="contractPage.' + nameModel + endModelName + '"');
                            template = template.replace('ng-value=""', 'ng-value="contractPage.' + nameModel + endModelName + '"');
                            template = template.replace('ng-disabled=""', 'ng-disabled="contractPage.isSubmitted"');
                            template = template.replace('ng-class=""', 'ng-class="{' + 'disabled' + ':contractPage.isSubmitted}"');
                            template = template.replace('step-id=""', 'step-id="' + 'signature_field' + '_' + templateIndex + '"');
                            template = template.replace('id=""', 'id="' + nameModel + '"');
                            template = template.replace('ng-form=""', 'ng-form="' + nameModel + '_' + templateIndex + '"');
                            template = template.replace('ngForm', nameModel + '_' + templateIndex);
                            template = template.replace('numberFormatting()', `numberFormatting('${nameModel}${endModelName}', contractPage.${nameModel}${endModelName})`);
                            template = template.replace('{{}}', `{{contractPage.${nameModel}${endModelName}}}`);

                            if (angular.isUndefined(scope.contractPage[nameModel + endModelName])) {
								scope.contractPage[nameModel + endModelName] = setDefaultModelValue(key);
							}

                            content = replaceStringAtPosition(content, key, template, searchInd);
                            templateIndex++;
                        }
                    } else {
                        content = content.replace(RegExp(key, 'g'), html);
                    }
                });
            }

			if (isMainPage) {
				initAdditionalMain();
				ContractFactory.init();
			}
            return content;
        }

        function setDefaultModelValue(field) {
            switch(field) {
                case '%address_from%':
                    return scope.request.request_all_data.invoice.field_moving_from.thoroughfare;
                    break;
                case '%address_to%':
                    return scope.request.request_all_data.invoice.field_moving_to.thoroughfare;
                    break;
                case '%address_pickup%':
                    return scope.request.request_all_data.invoice.field_extra_pickup.thoroughfare;
                    break;
                case '%address_dropoff%':
                    return scope.request.request_all_data.invoice.field_extra_dropoff.thoroughfare;
                    break;
                case '%city_from%':
                    return scope.request.request_all_data.invoice.field_moving_from.locality;
                    break;
                case '%city_to%':
                    return scope.request.request_all_data.invoice.field_moving_to.locality;
                    break;
                case '%city_pickup%':
                    return scope.request.request_all_data.invoice.field_extra_pickup.locality;
                    break;
                case '%city_dropoff%':
                    return scope.request.request_all_data.invoice.field_extra_dropoff.locality;
                    break;
                case '%state_dropoff%':
                    return scope.request.request_all_data.invoice.field_extra_dropoff.administrative_area;
                    break;
                case '%state_pickup%':
                    return scope.request.request_all_data.invoice.field_extra_pickup.administrative_area;
                    break;
                case '%zip_dropoff%':
                    return scope.request.request_all_data.invoice.field_extra_dropoff.postal_code;
                    break;
                case '%zip_pickup%':
                    return scope.request.request_all_data.invoice.field_extra_pickup.postal_code;
                    break;
                case '%state_to%':
                    return scope.request.request_all_data.invoice.field_moving_to.administrative_area;
                    break;
                case '%state_from%':
                    return scope.request.request_all_data.invoice.field_moving_from.administrative_area;
                    break;
                case '%zip_to%':
                    return scope.request.request_all_data.invoice.field_moving_to.postal_code;
                    break;
                case '%zip_from%':
                    return scope.request.request_all_data.invoice.field_moving_from.postal_code;
                    break;
                case '%initial_input%':
                    return initialsValue;
                    break;
                case '%crew_number%':
                    return  scope.request.crew.value;
                    break;
                case '%truck_number%':
                    return  scope.request.trucks.raw.length;
                    break;
                case '%rate_per_hour%':
                    return  $filter('number')(requestRate, accountConstant.roundUp);
                    break;
                case '%request_id%':
                    return  scope.request.nid;
                    break;
                case '%travel_fee%':
                    return  $filter('number')((requestRate * getTravelTime()), accountConstant.roundUp);
                    break;
                case '%start_time%':
                    return  '8:00 AM';
                    break;
                case '%end_time%':
                    return  '8:00 PM';
                    break;
                case '%time_off%':
                    return  '00:00';
                    break;
                case '%labor_time%':
                    return  12;
                    break;
                case '%current_date%':
                    return  moment().format('MM/DD/YYYY');
                    break;
                case '%move_date%':
                    return  scope.request.request_all_data.invoice.date.value;
                    break;
                case '%fuel_surcharge%':
                    return  ContractCalc.finance.fuel;
                    break;
                case '%valuation%':
                    return  ContractCalc.finance.valuation;
                    break;
                case '%tips%':
                    return  ContractCalc.finance.tip;
                    break;
                case '%contract_total%':
                    return  0;
                    break;
                case '%reservation_received%':
                    return  0;
                    break;
                case '%balance_due%':
                    return  0;
                    break;
                case '%total_balance_paid%':
                    return  totalBalancePaid();
                    break;
                case '%simple_contract_input%':
                    return  0;
                    break;
				case '%flat_rate_quote%':
					return flatRateQuote;
					break;
                default:
                    return '';
            }
        }

        function changeTime(field, time) {
            scope.contractPage[field] = time;
            let start = scope.contractPage.start_time;
            let end = scope.contractPage.end_time;
            let off = scope.contractPage.time_off;
            scope.contractPage.labor_time = calculateDurationTime(start, end, off);// end - start - time off
            changeContractTotal();
            if (field != 'time_off')
                scope.$apply();
        }

        function getLaborTotal() {
            if (scope.contractPage.labor_time || scope.request.request_all_data.invoice.work_time_int && requestRate) {
                return Number(scope.contractPage.labor_time || scope.request.request_all_data.invoice.work_time_int) * Number(requestRate);
            }
        }

        function changeRatePerHour() {
        	if (scope.contractPage.travel_fee && scope.contractPage.rate_per_hour) {
				if (scope.travelTime === 0) {
					scope.changeTravelFee();
				}
				scope.contractPage.travel_fee = $filter('number')((scope.contractPage.rate_per_hour * getTravelTime()), accountConstant.roundUp);
				changeContractTotal();
			} else {
				scope.numberFormatting('rate_per_hour', scope.contractPage.rate_per_hour);
				scope.numberFormatting('travel_fee', scope.contractPage.travel_fee);
			}
		}

        function changeTravelFee() {
        	let travelFee = _.round(scope.contractPage.travel_fee, accountConstant.roundUp);
        	let ratePerHour = _.round(scope.contractPage.rate_per_hour, accountConstant.roundUp);

            if (travelFee > 0 && ratePerHour > 0) {
                scope.travelTime = _.round(travelFee / ratePerHour, accountConstant.roundUp);
            } else {
				scope.travelTime = 0;
			}
			changeContractTotal();
        }

        function calculateDurationTime(start, end, off){
            let durationTime = moment.duration(moment(end, 'hh:mm A').diff(moment(start, 'hh:mm A')))._milliseconds;
            let timeOff =  moment.duration(off)._milliseconds;
            durationTime -= timeOff;
            return moment.duration(durationTime).asHours();
        }

        function changeContractTotal() {
        	let total = 0;
			let laborTime = scope.contractPage.labor_time || scope.request.request_all_data.invoice.work_time_int;
			let	ratePerHour =  scope.contractPage.rate_per_hour || requestRate;
			let	fuelSurcharge = scope.contractPage.fuel_surcharge || ContractCalc.finance.fuel;
			let	valuation = scope.contractPage.valuation || ContractCalc.finance.valuation;
			let	tips = scope.contractPage.tips || ContractCalc.finance.tip;

			if (scope.contractTab && scope.contractTab.page && scope.contractTab.show) {
				if (scope.request.service_type.raw == 7) {
					total = CalculatorServices.getLongDistanceQuote(scope.request, undefined, true);
				} else if(scope.request.service_type.raw == 5) {
					total = +scope.contractPage.flat_rate_quote || flatRateQuote;
				} else {
					total = _.round((laborTime + getTravelTime()) * ratePerHour, accountConstant.roundUp);
				}

				total += parseFloat(ContractCalc.finance.packingSum) + parseFloat(ContractCalc.finance.serviceSum) + parseFloat(fuelSurcharge) + parseFloat(valuation);
				total += addInputValue();

				if (scope.contractTab.mainContract) {
					ContractCalc.finance.valuation = parseFloat(valuation);
					ContractCalc.finance.fuel = parseFloat(fuelSurcharge);
					ContractCalc.finance.tip = parseFloat(tips);
					scope.calculateAddContractData({index: scope.contractIndex});
					scope.saveContract({data: true});
				}

				if (scope.contract_page.paymentTax.customTax.state) {
					ContractCalc.finance.customTax = _.round(total*(scope.contract_page.paymentTax.customTax.value / HUNDRED_PERCENT), accountConstant.roundUp);
					customTax =  ContractCalc.finance.customTax;
					total += ContractCalc.finance.customTax;
				}

				let tax = ContractCalc.finance.cashDiscount - ContractCalc.finance.creditTax -
					_.round(ContractCalc.calcCustomPayments(), accountConstant.roundUp);
				total -= tax;
				total = getDiscount(total);

				ContractCalc.finance.totalCost = _.round(total, accountConstant.roundUp);

				total += parseFloat(tips);
				scope.contractPage.contract_total = _.round(total, accountConstant.roundUp);
				scope.contractPage.balance_due = _.round(scope.contractPage.contract_total - reservationReceived - totalBalancePaid(), accountConstant.roundUp);

				return _.round(total, accountConstant.roundUp);
			}
        }

        function addInputValue() {
			let index = 0, total = 0;
			let contractInputName = 'simple_contract_input_' + index;
			while(scope.contractPage[contractInputName]) {
				if (_.isString(scope.contractPage[contractInputName])) {
					total += +scope.contractPage[contractInputName].replace(/,/g, "");
				} else {
					total += scope.contractPage[contractInputName];
				}

				index++;
				contractInputName = 'simple_contract_input_' + index;
			}

			return total;
		}

        function totalBalancePaid() {
            let total = 0;
            angular.forEach(scope.request.receipts, function (payment) {
                if (!payment.pending && payment.payment_flag == "contract")
                    total += parseFloat(payment.amount);
            });

			return _.round(total, accountConstant.roundUp);
        }

        function totalLessDeposit() {
            let balancePaid = totalBalancePaid();
			let contractTotal = deleteCommas(scope.contractPage.contract_total);
            return parseFloat((contractTotal - parseFloat(reservationReceived) - balancePaid).toFixed(accountConstant.roundUp));
        }

        function paymentButton(){
            return (parseFloat(totalLessDeposit()) == 0)
        }

        function applyPayment(totalDeposit) {
            if (totalDeposit === true || scope.openPay || scope.isFamiliarization || scope.isPrint) {
                return false;
            }
            scope.openPay = true;

            var request = scope.request;
            request.reservation = false;
            request.payment_flag = 'contract';
            request.total_amount = totalLessDeposit();
            request.total_amount_tax = parseFloat(scope.contractPage.contract_total.toFixed(accountConstant.roundUp))
				- ContractCalc.finance.creditTax + ContractCalc.finance.cashDiscount;
            request.total_for_tip = parseFloat((request.total_amount_tax - (scope.contractPage.tips || 0)).toFixed(accountConstant.roundUp));
            request.request_all_data.invoice.additionalWorkTime = scope.contractPage.labor_time || scope.request.request_all_data.invoice.work_time_int;
            PaymentServices.openAuthPaymentModal(request, '', '', scope.fieldData.enums.entities.MOVEREQUEST, scope);
        }

        function initAdditionalMain() {
			scope.contractPage.rate_per_hour = setDefaultModelValue('%rate_per_hour%');
			scope.contractPage.travel_fee = scope.contractPage.rate_per_hour * getTravelTime();
			scope.contractPage.crew_number = scope.request.request_all_data.invoice.crew.value;
			scope.contractPage.start_time = scope.request.request_all_data.invoice.start_time1.value;
			scope.contractPage.end_time = scope.request.request_all_data.invoice.start_time2.value;
			scope.contractPage.labor_time = scope.request.request_all_data.invoice.work_time_int;
			scope.contractPage.flat_rate_quote = flatRateQuote;

			if (scope.contractPage.labor_time == 0 && scope.contractPage.start_time != scope.contractPage.end_time) {
				scope.contractPage.labor_time = calculateDurationTime(scope.contractPage.start_time, scope.contractPage.end_time, scope.contractPage.time_off);
			}

			let addMainPage = scope.getAddMainPage();
			let regexAddMainPage = /\d$/;

			for(let prop in addMainPage) {
				if (prop.match(regexAddMainPage)) {
					scope.contractPage[prop] = addMainPage[prop];
				}
			}

			scope.numberFormatting('travel_fee', scope.contractPage.travel_fee);
		}

        function numberFormatting(name, val) {
            let data = deleteCommas(val);
            scope.contractPage[name] = $filter('number')(data, accountConstant.roundUp);
        }

        function recalcCustomTax() {
            if (customTax > ContractCalc.finance.customTax) {
                ContractCalc.finance.customTax = customTax;
            }
        }

        function deleteCommas(data) {
        	return _.isString(data) ? data.replace(/,/g, "") : data;
		}

		function getDiscount(data) {
			if (ContractCalc.finance.percentDiscount) {
				data = _.round((data / HUNDRED_PERCENT) * (HUNDRED_PERCENT - ContractCalc.finance.percentDiscount), accountConstant.roundUp);
			}

			if (ContractCalc.finance.moneyDiscount) {
				data -= ContractCalc.finance.moneyDiscount;
			}

			if (ContractCalc.finance.discount) {
				data -= ContractCalc.finance.discount;
			}

			return data;
		}

		function getTravelTime() {
        	if (isMainPage) {
				ContractFactory.factory.crews[0].workDuration = scope.contractPage.labor_time;
			}

			ContractFactory.factory.laborOnly = ContractFactory.factoryObj.laborOnly;
        	return scope.calcSettings.travelTime ? scope.travelTime : 0;
		}

		function changeFlatRateQuote() {
			if (isMainPage) {
				changeContractTotal();
			}
		}

        if(!scope.contractPage.isSubmitted)
            changeContractTotal();

        $rootScope.$on('recalc.contract', function () {
            setTimeout(changeContractTotal, 300);
        });

        $rootScope.$on('receipt.rental', function () {
            setTimeout(changeContractTotal, 300);
        });

        $rootScope.$on('payment.received', function (event, data, request, tips, creditTax, cashDiscount, requestTips, customPayment, pageId) {
        	let isPaymentPage = scope.contractPage.page_id === pageId && event != ContractFactory.paymentReceivedEvent;

			if (angular.isDefined(tips) && isPaymentPage) {
				ContractFactory.paymentReceivedEvent = event;
				let contractTips = Number(scope.contractPage.tips || 0);
                contractTips += Number(tips.toFixed(accountConstant.roundUp));
                scope.contractPage.tips = Number(contractTips.toFixed(accountConstant.roundUp));
            }
            setTimeout(changeContractTotal, 300);
        });

        $rootScope.$on('openPay', function () {
            scope.openPay = false;
        });

        $rootScope.$on('recalc.customTax', recalcCustomTax);
    }
}
