angular
	.module('contractModule')
	.directive("payments", payments);

payments.$inject = ['contractConstant', 'ContractFactory', 'ContractCalc', 'datacontext', 'ContractValue'];

function payments(contractConstant, ContractFactory, ContractCalc, datacontext, ContractValue) {
	return {
		restrict: 'EA',
		scope: {
			showDiscount: '<',
			balance_paid: '<balancePaid',
			requestType: '<',
			saveContract: '&',
			saveReqDataInvoice: '&',
			sendContractLog: '&',
			removeDiscount: '&?',
			getTips: '&',
			openCouponModal: '&?',
			totalCost: '&?',
			totalLessDeposit: '&',
			showPayment: '&',
			totalFlatRateClosing: '&?'
		},
		templateUrl: 'app/contractpage/directives/payments/payments.html',
		link: link
	};

	function link(scope) {
		scope.showDiscountInContract = showDiscountInContract;
		scope.selectCoupon = selectCoupon;

		scope.couponTypes = ContractValue.couponTypes;
		scope.couponTypes.push({
			name: 'none',
			value: 'none'
		});
		scope.countBasePayment = contractConstant.countBasePayment;
		scope.data = ContractFactory.factory;
		scope.finance = ContractCalc.finance;
		scope.fieldData = datacontext.getFieldData();
		scope.contract_page = angular.fromJson(scope.fieldData.contract_page);

		scope.type = scope.requestType === '5' || scope.requestType === '7' ? 'flatRate' : 'localMoves';
		scope.countCustomPayment = scope.contract_page.paymentOptions.length - scope.countBasePayment;

		if (scope.type == 'localMoves') {
			scope.mainColspan = 5;
			scope.partialColspan = 2;
		} else if(scope.type == 'flatRate') {
			scope.mainColspan = 6;
			scope.partialColspan = 3;
		}

		function showDiscountInContract() {
			scope.showDiscount = !scope.showDiscount;
			scope.data.showDiscount = scope.showDiscount;
			scope.saveContract({data: true});
		}

		function selectCoupon() {
			if (scope.data.discountType == 'none') {
				scope.finance.discount = 0;
				scope.showDiscount = false;
				scope.data.showDiscount = scope.showDiscount;
				scope.saveReqDataInvoice();
			}
			scope.saveContract({data:true});
			scope.sendContractLog({msgToLog: `Discount ${scope.data.discountType} set on $${scope.finance.discount}`});
		}
	}
}
