!function() {

    var app = angular.module('contractModule');

    app.directive("leaseAgreement", leaseAgreement);
    leaseAgreement.$inject = ['ContractFactory', 'ContractCalc', '$window','logger', 'datacontext', 'ContractService',
							  'SweetAlert', '$rootScope', 'StorageService', '$routeParams', 'PaymentServices',
							  'CalculatorServices', '$timeout', 'RequestServices', '$uibModal', 'InventoriesServices',
							  'newInventoriesServices', 'geoCodingService'];
    function leaseAgreement(ContractFactory, ContractCalc, $window, logger, datacontext, ContractService,
							SweetAlert, $rootScope, StorageService, $routeParams, PaymentServices,
							CalculatorServices, $timeout, RequestServices, $uibModal, InventoriesServices,
							newInventoriesServices, geoCodingService) {
        return {
            restrict: 'AE',
			scope: {
				request: '=',
				navigation: '=',
				ifStorage: '=',
				data: '=',
				additionalServices: '=',
				content: '=',
				saveContract: '&',
			},
            templateUrl: 'app/contractpage/directives/lease-agreement/leaseAgreement.html',
			link: leaseAgreementCtrl
        };

        function leaseAgreementCtrl($scope) {
            $scope.busy = false;
            $scope.storageList = [];
            $scope.storageRequest = {};
            $scope.requestData = {};
            $scope.requestData.zip = '';
            $scope.requestData.address = '';
            $scope.storageName = '';
            $scope.noDefaultStorage = false;
            $scope.defStorage = {
                min: 0,
                rate: 0
            };
            $scope.storageTotal = ContractFactory.storage.storageTotal;
            $scope.storageVolume = ContractFactory.storage.storageVolume;
            $scope.storageRateCuft = ContractFactory.storage.storageRateCuft;
            $scope.todayDate = moment().format('MMM DD, YYYY');

            $scope.openService = openService;
            $scope.recalcRate = recalcRate;
            $scope.isInvalidForm = isInvalidForm;

            var fieldData = datacontext.getFieldData();
            var settings = angular.fromJson(fieldData.basicsettings);
            var contract_page = angular.fromJson(fieldData.contract_page);
            if (contract_page.rentalAgreement)
                $scope.rentalAgreement = contract_page.rentalAgreement.noticeOfLien;
            $scope.enums = fieldData.enums.entities;

            if ($scope.ifStorage) {
                if (!$scope.request.request_all_data.toStorage) {
                    $scope.requestData.zip = $scope.request.field_moving_from.postal_code;
                } else {
                    RequestServices.getRequestsByNid($scope.request.storage_id).then(function (requestStorage) {
                        if (!_.isEmpty(requestStorage.nodes))
                            $scope.requestData.zip = requestStorage.nodes[0].field_moving_from.postal_code;
                        else
                            $scope.requestData.zip = $scope.request.field_moving_from.postal_code;
                    });
                }
            }else {
                RequestServices.getRequestsByNid($scope.request.storage_id).then(function (requestStorage) {
                    if (!_.isEmpty(requestStorage.nodes))
                        $scope.requestData.zip = requestStorage.nodes[0].field_moving_from.postal_code;
                    else
                        $scope.requestData.zip = $scope.request.field_moving_from.postal_code;
                });
            }

            if(!$scope.request.request_all_data.storage_request_id){
                $scope.name = $scope.request.name;
                $scope.storageTotal = 0;
                $scope.storageVolume = $scope.request.inventory_weight ? $scope.request.inventory_weight.cfs : 0;
                StorageService.getDefaultStorage().then(function ({data}) {
                    $scope.busy = false;
                    if (!data.id || !data.value) {
                        $scope.noDefaultStorage = true;
                        return false;
                    }
                    $scope.defStorage.min = data.value.min_cuft;
                    $scope.defStorage.rate = data.value.rate_per_cuft;
                    if (Number(data.value.min_cuft) < Number($scope.storageVolume) || Number($scope.storageVolume) == 0) {
                    	$scope.storageTotal = $scope.storageVolume * data.value.rate_per_cuft;
					} else {
                    	$scope.storageTotal = Number(data.value.min_cuft) * data.value.rate_per_cuft;
					}
                    recalculateData();
                });
            }
            function testPaymentSettings() {
                if (!contract_page.rentalAgreement
                    || _.findIndex(contract_page.rentalAgreement.paymentSettings, {selected: true}) < 0) {
                    SweetAlert.swal("Failed!", "Please contact to your administrator, and set payment options", "error");
                }
            }

            function recalcRate(volume) {
                $scope.storageVolume = volume;
                if (Number($scope.defStorage.min) < Number($scope.storageVolume) || Number($scope.storageVolume) == 0) {
                	$scope.storageTotal  =  $scope.storageVolume * $scope.defStorage.rate;
				} else {
                	$scope.storageTotal  = Number($scope.defStorage.min) * $scope.defStorage.rate;
				}
            }

	        function recalculateData() {
		        //calculate weight inventory with additional items
		        let inventory = newInventoriesServices.getSelectedInventory($scope.request, $scope.data);

		        $scope.storageVolume = Number(InventoriesServices.calculateTotals(inventory).cfs);

		        if (Number($scope.defStorage.min) < Number($scope.storageVolume) || Number($scope.storageVolume) == 0) {
		        	$scope.storageTotal = $scope.storageVolume * $scope.defStorage.rate;
		        } else {
		        	$scope.storageTotal = Number($scope.defStorage.min) * $scope.defStorage.rate;
		        }
	        }

            function refreshAgreement(update) {
                if(!$scope.request.request_all_data.storage_request_id) return false;
                $scope.storageId = $scope.request.request_all_data.storage_request_id;
                StorageService.getStorageReqByID($scope.storageId).then(function ({data}) {
                    if(!data.rentals || !data.recurring || !data.user_info) {
                        $scope.busy = false;
                        return false;
                    }
                    $scope.storageRequest = data;
                    StorageService.getStorageByID(data.rentals.storage).then(function ({data}) {
                        $scope.busy = false;
                        $scope.defStorage.min = data.min_cuft;
                        $scope.defStorage.rate = data.rate_per_cuft;
						$scope.storageTotal = $scope.storageRequest.rentals.total_monthly;
						$scope.storageVolume = $scope.storageRequest.rentals.volume_cuft;
						$scope.storageRateCuft = $scope.storageRequest.rentals.rate_per_cuft;
                        if(update){
                            $scope.storageRequest.user_info.zip = $scope.data.agreement.zipCode;
                            $scope.storageRequest.user_info.phone1 = $scope.data.agreement.phone;
                            $scope.storageRequest.user_info.address = $scope.data.agreement.address;
                            $scope.storageRequest.user_info.rental_address = $scope.data.agreement.address;
                            $scope.storageRequest.user_info.city = $scope.data.agreement.city;
                            $scope.storageRequest.user_info.state = $scope.data.agreement.state;
                            var req = {
                                    user_info: $scope.storageRequest.user_info,
                                    rentals: $scope.storageRequest.rentals,
                                    recurring: $scope.storageRequest.recurring
                            };
                            $scope.storageRequest.user_info.lease_agreement = true;
                            req.user_info.lease_agreement = true;
                            saveStorageRequest(req);
                        }
                    });
                });
            }
            refreshAgreement();

            function isInvalidForm(){
                return ($scope.leaseAgreement &&
                        ((!$scope.storageVolume || $scope.storageVolume<=0)
						|| $scope.leaseAgreement.phoneNumber.$invalid
						|| $scope.leaseAgreement.address.$invalid
						|| $scope.leaseAgreement.zip.$invalid))
            }

            $scope.openPayment = function () {
                if (isInvalidForm()){
                    SweetAlert.swal("Failed! Please fill all fields", '', "error");
                    return false;
                }

                if($scope.payClicked) return false;
                $scope.payClicked = true;

                var entitytype = $scope.enums.STORAGEREQUEST;
                var requestDataPayment = angular.copy($scope.storageRequest);
                $scope.storageId = $scope.request.request_all_data.storage_request_id;

                if(!$scope.storageId){
                    $scope.$broadcast('storage.created.error');
                    return false;
                }

                requestDataPayment.nid = $scope.storageId;
                requestDataPayment.total = $scope.storageTotal;
                requestDataPayment.receiptDescription = 'Storage first month payment';
                requestDataPayment.agreement = true;
                requestDataPayment.move_request = $scope.request.nid;
                if(!requestDataPayment.user_info) {
                    requestDataPayment.user_info = {};
                    requestDataPayment.user_info.name = $scope.name;
                    requestDataPayment.user_info.email = $scope.request.name;
                }
                requestDataPayment.user_info.address = $scope.data.agreement.address;
                requestDataPayment.user_info.rental_address = $scope.data.agreement.address;
                requestDataPayment.user_info.zip = $scope.data.agreement.zipCode;
                requestDataPayment.user_info.phone1 = $scope.data.agreement.phone;
                requestDataPayment.user_info.city = $scope.data.agreement.city;
                requestDataPayment.user_info.state = $scope.data.agreement.state;
                requestDataPayment.payment_flag = "Lease agreement";
                PaymentServices.openAuthPaymentModal(requestDataPayment, '', '',entitytype, $scope)
            };

            $scope.changeZip = function (zip) {
                if (zip && zip.length == 5) {
					geoCodingService.geoCode(zip)
						.then(function (data) {
							$scope.data.agreement.city = data.city;
							$scope.data.agreement.state = data.state;
							$scope.saveContract({missModalClose: true});
						}, function () {
							SweetAlert.swal("You entered the wrong zip code.", '', 'error');
							$scope.data.agreement.city = '';
							$scope.data.agreement.state = '';
							$scope.saveContract({missModalClose: true});
						});
                }
            };

            function openServiceModal(service, step) {
                let connectedServiceInstance = $uibModal.open({
                    templateUrl: 'app/contractpage/directives/storage-fit/storageFit.html',
                    controller: connectedServiceCtrl,
                    size: 'lg',
                    backdrop: false,
                    keyboard: false,
                    resolve: {
                        storageTotal: function () {
                            return $scope.storageTotal;
                        },
                        storageVolume: function () {
                            return $scope.storageVolume;
                        },
                        storageRateCuft: function () {
                            return $scope.storageRateCuft;
                        },
                        service: function () {
                            return service;
                        },
                        step: function () {
                            return step;
                        },
                        services: function () {
                            return $scope.additionalServices;
                        },
                        request: function () {
                            return $scope.request;
                        },
                        user_info: function () {
                            return $scope.data;
                        }
                    }
                }).result
                .then((signature) => {
                    if(signature) {
						$scope.data.agreement.signatures[0] = signature;
                        var msgToLog = 'Rental Agreement singed';
                        ContractService.addForemanLog(msgToLog,'Сontract',$scope.request.nid, 'MOVEREQUEST');
						$scope.saveContract();
                    }
                })
                .catch((err) => SweetAlert.swal('Failed!', 'Error of saving', 'Error'));
            }


            function openService(type) {
                let service = _.find($scope.content.storageFit, {connected_service: type});
                let step = 1;
                if(type == 'monthly_storage_fee'){
                    openServiceModal(service, step);
                }
            }

            function connectedServiceCtrl($scope, $rootScope, $uibModalInstance, storageTotal, storageVolume, storageRateCuft,
										  service, step, services, request, user_info, StorageService, ContractFactory) {
                $scope.storageTotal = storageTotal;
                $scope.storageVolume = storageVolume;
                $scope.storageRateCuft = storageRateCuft;
                $scope.services = services;
                $scope.service = service;
                $scope.request = request;
                $scope.steps = [false, false];
                $scope.storage = false;
                if(step == 1) goSecondStep();
                if(step == 0){ $scope.storage = true; $scope.steps[0] = true;}

                if(step == 0)
                    StorageService.getDefaultStorage().then(function ({data}) {
                        if(!data.id || !data.value) {
                            SweetAlert.swal("Failed! No default storages", '', "error");
                            return false;
                        }
                        $scope.storageRateCuft = Number(data.value.rate_per_cuft);
                        $scope.storageMinCuft = Number(data.value.min_cuft);
                    });
                let getCropedSignatureValue = function () {
                	return ContractFactory.getSignatureUrl($scope.signaturePad._ctx, true);
                };

                function isCanvasBlank(elementID) {
                    var canvas = document.getElementById(elementID);
                    var blank = document.createElement('canvas');
                    blank.width = canvas.width;
                    blank.height = canvas.height;
                    var context = canvas.getContext("2d");
                    var perc = 0, area = canvas.width * canvas.height;
                    if (canvas.toDataURL() == blank.toDataURL()) {
                        return canvas.toDataURL() == blank.toDataURL();
                    } else {
                        var data = context.getImageData(0, 0, canvas.width, canvas.height).data;
                        for (var ct = 0, i = 3, len = data.length; i < len; i += 4) if (data[i] > 50) ct++;
                        perc = parseFloat((100 * ct / area).toFixed(2));
                        return (perc < 0.1);
                    }
                }

                $scope.goSecondStep = goSecondStep;
                $scope.returnStep = returnStep;
                $scope.changeVolume = changeVolume;
                $scope.saveService = saveService;
                $scope.cancel = cancel;

                function goSecondStep() {
                    if(!$scope.storageVolume && step == 0){
                        $scope.errorStorageVolume = true;
                        return false;
                    }
                    setTimeout(function () {
                        $scope.canvas = document.getElementById('signatureCanvasService');
                        if($scope.canvas != null)
                            $scope.signaturePad = new SignaturePad($scope.canvas);
                        else
                            goSecondStep();
                    }, 1000);
                    $scope.steps[0] = false;
                    $scope.steps[1] = true;
                }

                function returnStep(stepInd) {
                    _.forEach($scope.steps, function (item, ind) {
                        $scope.steps[ind] = false;
                    });
                    $scope.steps[stepInd] = true;
                }

                function changeVolume() {
                    if(Number($scope.storageMinCuft) < Number($scope.storageVolume)  || Number($scope.storageVolume) == 0 ) {
                    	$scope.storageTotal = Number(($scope.storageVolume * $scope.storageRateCuft).toFixed(2));
					} else {
                    	$scope.storageTotal = Number((Number($scope.storageMinCuft) * $scope.storageRateCuft).toFixed(2));
					}
                }

                function saveService() {
                    let signatureValue = getCropedSignatureValue();
                    let SIGNATURE_PREFIX = 'data:image/gif;base64,';
                    $scope.signatureLeaseAgreemant = {
                        value: SIGNATURE_PREFIX + signatureValue,
                        date: moment().format('x')
                    };
                    let blank = isCanvasBlank('signatureCanvasService');
                    if(blank){
                        SweetAlert.swal("Failed!", 'Sign please', "error");
                    }else{
                        $scope.busy = true;
                        $rootScope.$broadcast('save.service', {
                                                                service: $scope.service,
                                                                signature: $scope.signatureLeaseAgreemant,
                                                                request: $scope.request,
                                                                data: {
                                                                    volume: $scope.storageVolume,
                                                                    total: $scope.storageTotal
                                                                },
                                                                user_info
                                              });
                    }
                }

                function cancel() {
                    $uibModalInstance.close($scope.signatureLeaseAgreemant);
                    $rootScope.$$listeners['save.service'] = [];
                    $rootScope.$$listenerCount['save.service'] = 1;
                }

                $scope.$on('close.modal.service', function () {
                    $scope.cancel();
                })
            }

            function saveDocuments(request, id) {
                request.user_info.zip = $scope.data.agreement.zipCode;
                request.user_info.phone1 = $scope.data.agreement.phone;
                request.user_info.address = $scope.data.agreement.address;
                request.user_info.city = $scope.data.agreement.city;
                request.user_info.state = $scope.data.agreement.state;
                request.user_info.rental_address = $scope.data.agreement.address;
                if (!request.user_info.documents)
                    request.user_info.documents = [];
                var currUser = $rootScope.currentUser;
                var doc = {
                    date: moment().unix(),
                    name: "Lease Agreement",
                    source: 'Created By: ' + currUser.userId.field_user_first_name + ' ' + currUser.userId.field_user_last_name,
                    type: "Move in"
                };
                request.user_info.documents.push(doc);
                request.user_info.lease_agreement = true;
                var req = {
                        user_info: request.user_info,
                        rentals: request.rentals,
                        recurring: request.recurring
                };
                saveStorageRequest(req);
            }

            function arrLotNumbers() {
                var inventoryTypes = ['secondAddInventoryMoving', 'addInventoryMoving', 'inventoryMoving'];
                var arr = [];
                _.forEach(inventoryTypes, function (type) {
                    if ($scope.data[type].isSubmitted) {
                        arr.push({
                            "number": $scope.data[type].tapeNumbers,
                            "color": $scope.data[type].tapeColor,
                            "from": $scope.data[type].nosFrom,
                            "to": $scope.data[type].numberThru
                        });
                    }
                });
                return arr;
            }

            function saveStorageRequest(req) {
                $scope.busy = true;
                StorageService.getStorageReqByID($scope.storageId)
					.then(({data}) => {
						if(!data.rentals || !data.recurring || !data.user_info) {
							return false;
						}
						$scope.storageRequest = data;
						let updateRequest = _.merge({}, data, req);
						let reqData = {
							data: updateRequest
						};
						return StorageService.updateStorageReq(reqData, $scope.storageId);
					})
					.catch((err) => SweetAlert.swal('Failed!', 'Error of saving storage request', 'error'))
					.finally(() => $scope.busy = false);
            }

            $scope.$watch('navigation.active', function () {
                if($scope.navigation.active == _.indexOf($scope.navigation.pages, 'Rental agreement')) {
                    if($scope.noDefaultStorage) SweetAlert.swal("Failed! No default storages", '', "error");
                    if(!$scope.data.agreement.isSubmitted) testPaymentSettings();
                }
            });

            $scope.$on('inventory.submit', function(){
                if(!$scope.data.agreement.isSubmitted){
                    recalculateData();
                }
            });

            $scope.$on('signatureAgreement', function (ev, signature) {
                if(signature) {
					$scope.data.agreement.signatures[0] = signature;
                    var msgToLog = 'Rental Agreement singed';
                    ContractService.addForemanLog(msgToLog, 'Сontract', $scope.request.nid, 'MOVEREQUEST');
					$scope.saveContract();
                }
            });

            $scope.$on('payment.rental', function () {
                $scope.busy = true;
                var element = $('#agreement .print-section');

				$scope.data.agreement.isSubmitted = true;
				$scope.saveContract();

				if ($scope.storageId) {
					saveDocuments($scope.storageRequest, $scope.storageId);
				}

				$rootScope.$broadcast('rental.finished');

				if (!$scope.ifStorage) {
					$scope.request.field_request_settings = $scope.request.field_request_settings || {storage_rate: settings.storage_rate};
					ContractService.createRequest($scope.request)
						.then(({data}) => {
							$scope.request.request_all_data.storageRequestNid = data.nid;
							RequestServices.saveRequestDataContract($scope.request);

							let fieldName = 'inventoryMoving';

							ContractFactory.transferInventory($scope.data, fieldName, data.nid, $scope.request.nid)
								.then(() => {
									$scope.data[fieldName].isSubmitted = true;

									if ($scope.data.secondAddInventoryMoving.open && fieldName == 'addInventoryMoving') {
										$rootScope.$broadcast('inventory.transferred', $scope.data);
										$rootScope.$broadcast('go.to.secAddInv');

										return false;
									}

									if (!$scope.data.addInventoryMoving.open || ($scope.data.addInventoryMoving.open &&
										$scope.data.addInventoryMoving.isSubmitted && $scope.data.inventoryMoving.isSubmitted)) {
										$rootScope.$broadcast('inventory.submit');
									} else if (fieldName == 'inventoryMoving' && $scope.data.addInventoryMoving.open) {
										$rootScope.$broadcast('go.to.addInv');
									}

									$rootScope.$broadcast('inventory.transferred', $scope.data);
								})
								.catch(() => SweetAlert.swal('Failed!', 'Error of saving contract', 'error'));
						})
						.catch((err) => SweetAlert.swal("Failed!", "Error of creating request", "error"));
				}
            });

            $rootScope.$on('storage.request.created', function(e, id, data){
                $scope.busy = true;
                $scope.request.request_all_data.storage_request_id = id;
                $scope.storageId = id;
                $scope.storageRequest = data.data;
                $scope.storageRequest.rentals.total_monthly = $scope.storageTotal;
                $scope.storageRequest.rentals.volume_cuft = $scope.storageVolume;
                $scope.storageRequest.rentals.lot_number = arrLotNumbers();
                $scope.storageRequest.user_info.rental_address = $scope.data.agreement.address;
                var req = {
                    user_info: $scope.storageRequest.user_info,
                    rentals: $scope.storageRequest.rentals,
                    recurring: $scope.storageRequest.recurring
                };
                req.user_info.lease_agreement = true;
                saveStorageRequest(req);
                SweetAlert.swal("Storage Request has been created", "", "success");
            });

            $scope.$on('refresh.agreement', function () {
                refreshAgreement();
            });

            $scope.$on('save.agreement.payment', function(){
                if(localStorage['save_agreement']){
                    var localObj = angular.fromJson(localStorage['save_agreement']);
                    var entitytype = $scope.enums.MOVEREQUEST;
                    var requestDataPayment = angular.copy(localObj.request);
                    requestDataPayment.agreement_repeat = true;
                    PaymentServices.openAuthPaymentModal(requestDataPayment, '', '',entitytype, $scope);
                    $timeout(function () {
                        SweetAlert.swal("Storage payment error", "Please, try again", "error");
                    }, 350);
                }
            });

            $scope.$on('save.service', function (ev, data) {
                $scope.busy = true;
                $scope.storageTotal = data.data.total;
                $scope.storageVolume = data.data.volume;
                if($scope.storageId) {
                    $scope.storageRequest.rentals.total_monthly = data.data.total;
                    $scope.storageRequest.rentals.volume_cuft = data.data.volume;
                    var req = {
                        user_info: $scope.storageRequest.user_info,
                        rentals: $scope.storageRequest.rentals,
                        recurring: $scope.storageRequest.recurring
                    };
                    if(_.get(data, 'user_info.agreement.address', false)) {
						req.user_info.rental_address = data.user_info.agreement.address;
					}
                    req.user_info.lease_agreement = true;
                    saveStorageRequest(req);
                }
            });

            $rootScope.$on('storage.created.error', function (ev, msg) {
                SweetAlert.swal( msg || "Creating Error", "Storage hasn't created, please try again", "error");
                $scope.busy = false;
                delete $scope.data.agreement.signatures[0];
				$scope.saveContract();
            });

            $rootScope.$on('storage.request.exist', function (ev, msg) {
                $scope.storageRequest.rentals.lot_number = arrLotNumbers();
                var req = {
                        user_info: $scope.storageRequest.user_info,
                        rentals: $scope.storageRequest.rentals,
                        recurring: $scope.storageRequest.recurring
                };
                req.user_info.lease_agreement = true;
                saveStorageRequest(req);
            });

            $scope.$on('submit.inv.moving', function (ev, data) {
                recalcRate(data);
            });

            $scope.$on('close.payment.modal',function(){
                $scope.payClicked = false;
            });

            $scope.$on('save.agreement',function(e, id1, id2, data){
                $scope.request.request_all_data.agreementSave = {};
                $scope.request.request_all_data.agreementSave.receipt = data;
                $scope.request.request_all_data.agreementSave.storageId = id1;
                $scope.request.request_all_data.agreementSave.requestId = id2;
	            RequestServices.saveRequestDataContract($scope.request);
            });

            $scope.$on('save.agreement.signature',function(e){
                $scope.request.request_all_data.agreementSave.storageAgreementSign = true;
	            RequestServices.saveRequestDataContract($scope.request);
            });

            $scope.$on('remove.agreement.data',function(e){
                delete $scope.request.request_all_data.agreementSave;
	            RequestServices.saveRequestDataContract($scope.request);
            });
        }
    }

}();
