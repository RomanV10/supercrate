angular.module('contractModule')
	.directive('stringToNumber', stringToNumber);

function stringToNumber() {

	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function (value) {
				if (!value || value == '') return '0';
				return '' + value;
			});
			ngModel.$formatters.push(function (value) {
				return parseFloat(value, 10);
			});
		}
	};
}
