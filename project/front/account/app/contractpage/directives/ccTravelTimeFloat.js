/**
 * Make travel time to float number
 */
angular.module('contractModule')
    .directive("ccTravelTimeFloat", ccTravelTimeFloat);

ccTravelTimeFloat.$inject = ['$routeParams'];

function ccTravelTimeFloat($routeParams) {
    return {
        restrict: "A",
        link: function (scope, iElement, iAttrs) {

            var nid = Number($routeParams.id);

            iElement.timepicker({
                'disableTouchKeyboard': true,
                'minTime': '00:00',
                'maxTime': '24:00',
                'timeFormat': 'H:i',
                'step': 15
            });
            iElement.on('change', function (e) {
                var val = e.currentTarget.value;
                if (val) {
                    setValue(val);
                }
            });
            function setValue(val) {

                var toFloat = timeToFloatNum(val);
                scope.travelTime.value = val;
                scope.travelTime.raw = toFloat;
                scope.$apply();
            }

            function timeToFloatNum(value) {
                var result = 0.0;
                if (value != 0 && !!value) {
                    var toMinutes = parseFloat(value.split(':')[0] * 60 + 1 * value.split(':')[1]);
                    result = parseFloat(toMinutes / 60);

                }
                return result.toFixed(2);
            }

        }
    };
}