!function() {

    var app = angular.module('contractModule');

    /**
     * Release Form directive
     */

    app.directive("releaseFormOld", releaseFormOld);
    releaseFormOld.$inject = ['ContractFactory', 'ContractCalc', '$window','logger', 'datacontext', 'ContractService', 'SweetAlert', '$rootScope'];
    function releaseFormOld(ContractFactory, ContractCalc, $window, logger, datacontext,ContractService, SweetAlert, $rootScope) {
        return {
            restrict: "E",
            templateUrl: 'app/contractpage/directives/release-form/oldVersion/releaseForm.html',
            controller: releaseFormCtrl
        };

        function releaseFormCtrl($scope){
            var $super = $scope.$parent;
            var SIGNATURE_PREFIX = 'data:image/gif;base64,';
            $scope.itemsRelease = [];
            $scope.releaseService = {};
            $scope.releaseFormSettings = {};

            $scope.fieldData = datacontext.getFieldData();

            $scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);

            $scope.saveSignatureRelease = saveSignatureRelease;
            $scope.removeSignatureFromRelease = removeSignatureFromRelease;
            $scope.switchLocation = switchLocation;
            $scope.saveReleaseData = saveReleaseData;
            $scope.releaseExtraServices = releaseExtraServices;

            function releaseExtraServices(){
                if(angular.isUndefined($scope.itemsRelease.service.service_name))
                    $scope.itemsRelease.service.service_name = 'Hoisting Fee';
                var service_name = ($scope.itemsRelease.service.service_name.toLowerCase()).replace(/\s/g, '_');
                var sevice_cost = $scope.itemsRelease.service.cost;
                if($scope.itemsRelease.service.checkboxItem == true){
                    $rootScope.$broadcast('event:release-service', service_name, sevice_cost, 'add');
                }else{
                    $rootScope.$broadcast('event:release-service', service_name, sevice_cost, 'del');
                }
            }

            function _setStepError(nextStep) {
                var el = $('#step_' + nextStep);
                el.addClass('error');
                $window.scrollTo(200,0);
                $('html, body').animate({
                    scrollTop: el.offset().top
                }, 1000);
                $('#signatureReleasePad').modal('hide');
                logger.warning('Please fill step', null, 'Error');
                return false;
            };

            function initReleaseData(){
                $scope.itemsRelease = {
                    service:{
                        take_address: "Destination",
                        billing_city: $scope.request.field_moving_from.locality,
                        billing_state: $scope.request.field_moving_from.administrative_area,
                        billing_zip: $scope.request.field_moving_from.postal_code,
                        billing_address: $scope.request.field_moving_from.thoroughfare,
                        checkboxItem: false,
                        description: '',
                        cost: 0,
                        service_name: 'Hoisting Fee'
                    },
                    forcing:{
                        take_address: "Destination",
                        billing_city: $scope.request.field_moving_from.locality,
                        billing_state: $scope.request.field_moving_from.administrative_area,
                        billing_zip: $scope.request.field_moving_from.postal_code,
                        billing_address: $scope.request.field_moving_from.thoroughfare,
                        checkboxItem: false,
                        description: '',
                    },
                    notWrapped:{
                        take_address: "Destination",
                        billing_city: $scope.request.field_moving_from.locality,
                        billing_state: $scope.request.field_moving_from.administrative_area,
                        billing_zip: $scope.request.field_moving_from.postal_code,
                        billing_address: $scope.request.field_moving_from.thoroughfare,
                        checkboxItem: false,
                        description: '',
                    },
                    pressWoodFurniture:{
                        checkboxItem: false,
                        description: '',
                    }
                };
                $scope.releaseInitials = {
                    head:{
                        initial:'',
                        signature:{}
                    },
                    footer:{
                        initial:'',
                        signature:{}
                    }
                };
                var service_name = ($scope.itemsRelease.service.service_name.toLowerCase()).replace(/\s/g, '_');
                _.forEach($scope.additionalServices, function(item){
                    var alias = item.name;
                    if (item.name.length) {
                        item.alias = (alias.toLowerCase()).replace(/\s/g, '_');
                    }
                    if(item.alias == service_name)
                        $scope.itemsRelease.service.cost = Number(item.extra_services[0].services_default_value)
                });
                $scope.releaseInitials.head.initial = $scope.request.name;
                $scope.releaseInitials.footer.initial = $scope.request.name;
                if(angular.isDefined($scope.data.releaseData.itemsRelease) && !_.isEmpty($scope.data.releaseData.itemsRelease))
                    $scope.itemsRelease = $scope.data.releaseData.itemsRelease;
                if(angular.isDefined($scope.data.releaseData.releaseInitials)  && !_.isEmpty($scope.data.releaseData.releaseInitials))
                    $scope.releaseInitials = $scope.data.releaseData.releaseInitials;
            }
            function removeSignatureFromRelease(stepId){
                $scope.releaseInitials[stepId].signature = {};
                $scope.data.releaseData.itemsRelease = $scope.itemsRelease;
                $scope.data.releaseData.releaseInitials = $scope.releaseInitials;
                $scope.saveContract( {
                    callback: function () {
                        localStorage['pendingSignatures'] = JSON.stringify({});
                    }
                });
            }
            function isCanvasBlank(elementID) {
                var canvas = document.getElementById(elementID);
                var blank = document.createElement('canvas');
                blank.width = canvas.width;
                blank.height = canvas.height;
                var context = canvas.getContext("2d");
                var perc = 0, area = canvas.width*canvas.height;
                if(canvas.toDataURL() == blank.toDataURL()){
                    return canvas.toDataURL() == blank.toDataURL();
                }else{
                    var data = context.getImageData(0, 0, canvas.width, canvas.height).data;
                    for (var ct=0, i=3, len=data.length; i<len; i+=4) if (data[i]>50) ct++;
                    perc = parseFloat((100 * ct / area).toFixed(2));
                    return (perc < 0.1);
                }
            }

            function saveSignatureRelease (submit) {
                let blank = isCanvasBlank('signatureReleaseCanvas');
                if(blank){
                    SweetAlert.swal("Failed!", 'Sign please', "error");
                }else{
                    let stepId = $scope.data.releaseData.currentStep;
                    if(stepId == 'footer' && angular.isUndefined($scope.releaseInitials.head.signature.value)){
                        _setStepError('head');
                        return false;
                    }
                    let signatureValue = ContractFactory.getCropedReleaseSignatureValue();
                    let signature = {
                        owner: $scope.data.currentOwner,
                        value: SIGNATURE_PREFIX + signatureValue,
                        date: moment().format('x')
                    };
                    $scope.releaseInitials[stepId].signature = signature;
                    ContractFactory.clearSignaturePad();
                    $scope.data.releaseData.itemsRelease = $scope.itemsRelease;
                    $scope.data.releaseData.releaseInitials = $scope.releaseInitials;
                    $scope.saveContract( {
                        callback: function () {
                            localStorage['pendingSignatures'] = JSON.stringify({});
                        }
                    });
                    $('#signatureReleasePad').modal('hide')
                }
            }

            function switchLocation(item){
                if(!$scope.data.releaseData.isSubmitted){
                    if(item.take_address == "Destination"){
                        item.take_address = "Origin";
                        item.billing_city = $scope.request.field_moving_to.locality;
                        item.billing_state = $scope.request.field_moving_to.administrative_area;
                        item.billing_zip = $scope.request.field_moving_to.postal_code;
                        item.billing_address = $scope.request.field_moving_to.thoroughfare;
                    }
                    else {
                        item.take_address = "Destination";
                        item.billing_city = $scope.request.field_moving_from.locality;
                        item.billing_state = $scope.request.field_moving_from.administrative_area;
                        item.billing_zip = $scope.request.field_moving_from.postal_code;
                        item.billing_address = $scope.request.field_moving_from.thoroughfare;
                    }
                }
            }

            function saveReleaseData(){
                $scope.busy = true;
                $scope.data.releaseData.itemsRelease = $scope.itemsRelease;
                $scope.data.releaseData.releaseService = $scope.releaseService;
                $scope.data.releaseData.isSubmitted = true;
                $scope.saveContract( {
                    callback: function () {
                        $scope.busy = false;
                        localStorage['pendingSignatures'] = JSON.stringify({});
                    }
                });
            }

            initReleaseData();
        }
    }


}();
