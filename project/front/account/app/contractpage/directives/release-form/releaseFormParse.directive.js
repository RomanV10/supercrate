/* /app/contractpage/directives/release-form/releaseFormParse.directive.js */
/**
 * @desc parse release form settings on contract
 * @example <div release-form-parse></div>
 */
angular
    .module('contractModule')
    .directive('releaseFormParse', releaseFormParse);
releaseFormParse.$inject = ['datacontext', '$compile'];

function releaseFormParse(datacontext, $compile) {
    /* implementation details */
    let directive = {
        link: link,
        require: ['ngModel'],
        restrict: 'EA'
    };
    return directive;

    function link(scope, element, attrs, ngModelCtrl) {
        let companyName = (angular.fromJson((datacontext.getFieldData()).basicsettings)).company_name;
        let html = '';
        let regCase = new RegExp('\%(.*?)\%', 'g');
        let templates = {};
        scope.modal = {
            showTextareaSettings: -1
        };
        scope.settingsMenu = {
            COMPANY_NAME: {
                title: 'Company name',
                value: '%COMPANY NAME%'
            },
            INITIAL_INPUT: {
                title: 'Initial input',
                value: '%initial_input%'
            },
            SIGNATURE_FIELD: {
                title: 'Signature',
                value: '%signature_field%'
            },
            TEXTAREA_FIELD: {
                title: 'Textarea',
                value: '%textarea_field%'
            }
        };

        scope.model = '';
        scope.index = attrs.index;

        function render(model) {
            if (!model)
                return false;
            scope.index = attrs.index;
            scope.model = model;
            if(!_.isNaN(Number(scope.index))) {
                templates = {
                    '%COMPANY_NAME%': companyName,
                    '%initial_input%': '<input type="text" class="input-md" ng-disabled=data.releaseData.isSubmitted ng-model=itemsRelease["' + scope.index + '"].initials ng-change=saveReleaseData() ng-model-options="' + '{' + 'debounce' + ':1000}' + '">',
                    '%signature_field%': '<signature-release-box  step-id="' + scope.index + '" owner="customer" data="' + scope.data + '"></signature-release-box>',
                    '%textarea_field%': '<div class="bottom-margin">' +
                    '<textarea auto-resize-texarea class="medium form-control" placeholder="Enter description" ng-if=!data.releaseData.isSubmitted ng-model=itemsRelease["' + scope.index + '"].textarea ng-change=saveReleaseData() ng-model-options="' + '{' + 'debounce' + ':1000}' + '"></textarea>' +
                    '<blockquote style=font-size:14px ng-if=data.releaseData.isSubmitted ng-bind-html=itemsRelease["' + scope.index + '"].textarea ng-show=itemsRelease["' + scope.index + '"].textarea></blockquote>' +
                    '</div>',
                };
                html = buildHtml(angular.copy(scope.rf.body));
            }else if(scope.index == 'footer' || scope.index == 'header'){
                templates = {
                    '%COMPANY_NAME%': companyName,
                    '%initial_input%': '<input type="text" class="input-md" ng-disabled=data.releaseData.isSubmitted ng-model=data.releaseData["' + scope.index + '"].initials ng-change=saveReleaseData() ng-model-options="' + '{' + 'debounce' + ':1000}' + '">',
                    '%signature_field%': '<signature-release-box style=display:inline-block step-id="' + scope.index + '" owner="customer" data="' + scope.data + '"></signature-release-box>',
                    '%textarea_field%': '<div class="bottom-margin"><blockquote style=font-size:14px ng-if=data.releaseData.isSubmitted ng-show=data.releaseData["' + scope.index + '"].textarea ng-bind-html=data.releaseData["' + scope.index + '"].textarea></blockquote><textarea auto-resize-texarea class="medium form-control" placeholder="Enter description" ng-if=!data.releaseData.isSubmitted ng-model=data.releaseData["' + scope.index + '"].textarea ng-change=saveReleaseData() ng-model-options="' + '{' + 'debounce' + ':1000}' + '"></textarea></div>',
                };
                html = buildHtml(angular.copy(scope.releaseFormSettings[scope.index]));
            }
            element.html(html).show();
            $compile(element.contents())(scope);
        }

        function getTemplate(contentType) {
            let template = '';
            if (templates.hasOwnProperty(contentType)) {
                template = templates[contentType];
            }
            return template;
        }

        function buildHtml(content) {
            let html = '';
            let keys = content.match(regCase);

            if(keys) {
                for (var i = 0, l = keys.length; i < l; i++) {
                    html = getTemplate(keys[i]);
                    content = content.replace(keys[i], html);
                }
            }

            return content;
        }

        scope.$watch(attrs['ngModel'], render);
    }
}
