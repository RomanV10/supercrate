!function() {

    angular.module('contractModule')
    .directive("releaseForm", releaseForm);
    releaseForm.$inject = ['ContractFactory', 'ContractCalc', '$window', 'logger', 'datacontext', 'ContractService', 'SweetAlert', '$rootScope'];
    function releaseForm(ContractFactory, ContractCalc, $window, logger, datacontext, ContractService, SweetAlert, $rootScope) {
        return {
            restrict: "E",
            templateUrl: 'app/contractpage/directives/release-form/releaseForm.html',
                controller: releaseFormCtrl
            };

            function releaseFormCtrl($scope){
                var SIGNATURE_PREFIX = 'data:image/gif;base64,';
                $scope.itemsRelease = [];
                $scope.releaseService = {};

                $scope.fieldData = datacontext.getFieldData();
                $scope.contractPage = angular.fromJson($scope.fieldData.contract_page);
                $scope.releaseFormSettings = $scope.contractPage.releaseForm;

                $scope.saveSignatureRelease = saveSignatureRelease;
                $scope.removeSignatureFromRelease = removeSignatureFromRelease;
                $scope.switchLocation = switchLocation;
                $scope.saveReleaseData = saveReleaseData;
                $scope.submitReleaseData = submitReleaseData;
                $scope.releaseExtraServices = releaseExtraServices;

                function releaseExtraServices() {
                    if (angular.isUndefined($scope.releaseService.service_name))
                        $scope.releaseService.service_name = 'Hoisting Fee';
                    let service_name = ($scope.releaseService.service_name.toLowerCase()).replace(/\s/g, '_');
                    let sevice_cost = $scope.releaseService.cost;
                    if ($scope.releaseService.checkboxItem == true) {
                        $rootScope.$broadcast('event:release-service', service_name, sevice_cost, 'add');
                    } else {
                        $rootScope.$broadcast('event:release-service', service_name, sevice_cost, 'del');
                    }
                }

                function _setStepError(nextStep) {
                    let el = $('#release-step_' + nextStep);
                    el.addClass('error');
                    $window.scrollTo(200,0);
                    $('html, body').animate({
                        scrollTop: el.offset().top
                    }, 1000);
                    $('#signatureReleasePad').modal('hide');
                    logger.warning('Please fill step', null, 'Error');
                    return false;
                }

                function initReleaseData(){
                    if (!$scope.data.releaseData.header) {
                        $scope.data.releaseData.header = {};
                    }
                    if (!$scope.data.releaseData.footer) {
                        $scope.data.releaseData.footer = {};
                    }
                    let serviceArr = [];
                    let hostingFeeService = {
                        take_address: "Destination",
                        billing_city: $scope.request.field_moving_from.locality,
                        billing_state: $scope.request.field_moving_from.administrative_area,
                        billing_zip: $scope.request.field_moving_from.postal_code,
                        billing_address: $scope.request.field_moving_from.thoroughfare,
                        checkboxItem: false,
                        description: '',
                        cost: 0,
                        service_name: 'Hoisting Fee'
                    };
                    let servicesFromSettings = {
                        checkboxItem: false,
                        signature:'',
                        textarea:'',
                        initials: $scope.request.name,
                        take_address: "Destination",
                        billing_city: $scope.request.field_moving_from.locality,
                        billing_state: $scope.request.field_moving_from.administrative_area,
                        billing_zip: $scope.request.field_moving_from.postal_code,
                        billing_address: $scope.request.field_moving_from.thoroughfare
                    };

                    if($scope.releaseFormSettings){
                        _.forEach($scope.releaseFormSettings.services, function (item) {
                            let serv = angular.copy(servicesFromSettings);
                            if(item.body.search('%signature_field%') == -1)
                                delete serv.signature;
                            if(item.body.search('%initial_input%') == -1)
                                delete serv.initials;
                            if(!item.addCheckbox)
                                delete serv.checkboxItem;
                            serviceArr.push(serv);
                        });
                        if($scope.data && $scope.data.releaseData && !_.isEmpty($scope.data.releaseData.itemsRelease))
                            _.forEach(serviceArr, function (item, ind) {
                                _.forEach(item, function(v, field){
                                    if($scope.data.releaseData.itemsRelease[ind] && $scope.data.releaseData.itemsRelease[ind][field])
                                        serviceArr[ind][field] = angular.copy($scope.data.releaseData.itemsRelease[ind][field]);
                                });
                            });
                    }
                    $scope.itemsRelease = serviceArr;
                    $scope.data.releaseData.itemsRelease = angular.copy($scope.itemsRelease);

                    let serv = angular.copy(servicesFromSettings);
                    if($scope.releaseFormSettings.footer.search('%signature_field%') == -1)
                        delete serv.signature;
                    if($scope.releaseFormSettings.footer.search('%initial_input%') == -1)
                        delete serv.initials;
                    delete serv.checkboxItem;
                    _.forEach(serv, function(v, field){
                        if($scope.data.releaseData.footer[field])
                            serv[field] = angular.copy($scope.data.releaseData.footer[field]);
                    });
                    $scope.data.releaseData.footer = angular.copy(serv);

                    serv = angular.copy(servicesFromSettings);
                    if($scope.releaseFormSettings.header.search('%signature_field%') == -1)
                        delete serv.signature;
                    if($scope.releaseFormSettings.header.search('%initial_input%') == -1)
                        delete serv.initials;
                    delete serv.checkboxItem;
                    _.forEach(serv, function(v, field){
                        if($scope.data.releaseData.header[field])
                            serv[field] = angular.copy($scope.data.releaseData.header[field]);
                    });
                    $scope.data.releaseData.header = angular.copy(serv);

                    //add service Hosting fee
                    $scope.releaseService = angular.copy(hostingFeeService);
                    if(!_.isEmpty($scope.data.releaseData.releaseService))
                        $scope.releaseService = angular.copy($scope.data.releaseData.releaseService);
                    var service_name = ($scope.releaseService.service_name.toLowerCase()).replace(/\s/g, '_');
                    _.forEach($scope.additionalServices, function(item){
                        let alias = item.name;
                        if (item.name.length) {
                            item.alias = (alias.toLowerCase()).replace(/\s/g, '_');
                        }
                        if(item.alias == service_name)
                            $scope.releaseService.cost = Number(item.extra_services[0].services_default_value)
                    });
                }

                function removeSignatureFromRelease(stepId){
                    if(!_.isNaN(Number(stepId)))
                        $scope.itemsRelease[stepId].signature = {};
                    else if(stepId == 'footer' || stepId == 'header')
                        $scope.data.releaseData[stepId].signature = {};
                    $scope.data.releaseData.itemsRelease = $scope.itemsRelease;
                    removeErrors();
                    $scope.saveContract( {
                        callback: function () {
                            localStorage['pendingSignatures'] = JSON.stringify({});
                        }
                    });
                }

                function isCanvasBlank(elementID) {
                    let canvas = document.getElementById(elementID);
                    let blank = document.createElement('canvas');
                    blank.width = canvas.width;
                    blank.height = canvas.height;
                    let context = canvas.getContext("2d");
                    let perc = 0, area = canvas.width*canvas.height;
                    if(canvas.toDataURL() == blank.toDataURL()){
                        return canvas.toDataURL() == blank.toDataURL();
                    }else{
                        let data = context.getImageData(0, 0, canvas.width, canvas.height).data;
                        for (var ct=0, i=3, len=data.length; i<len; i+=4) if (data[i]>50) ct++;
                        perc = parseFloat((100 * ct / area).toFixed(2));
                        return (perc < 0.1);
                    }
                }

                function getSignature(){
                    let signatureValue = ContractFactory.getCropedReleaseSignatureValue();
                    let signature = {
                        owner: $scope.data.currentOwner,
                        value: SIGNATURE_PREFIX + signatureValue,
                        date: moment().format('x')
                    };
                    return signature;
                }

                function saveSignatureRelease (submit) {
                    let blank = isCanvasBlank('signatureReleaseCanvas');
                    if(blank){
                        SweetAlert.swal("Failed!", 'Sign please', "error");
                    }else{
                        let stepId = $scope.data.releaseData.currentStep;
                        if(!_.isNaN(Number(stepId))) {
                            if ($scope.itemsRelease[stepId - 1] && angular.isDefined($scope.itemsRelease[stepId - 1].signature) && _.isEmpty($scope.itemsRelease[stepId - 1].signature)) {
                                _setStepError(stepId - 1);
                                return false;
                            }
                            $scope.itemsRelease[stepId].signature = getSignature();
                            $scope.data.releaseData.itemsRelease = $scope.itemsRelease;
                        }else if(stepId == 'footer' || stepId == 'header') {
                            $scope.data.releaseData[stepId].signature = getSignature();
                        }
                        ContractFactory.clearSignaturePad();
                        removeErrors();
                        $scope.saveContract( {
                            callback: function () {
                                localStorage['pendingSignatures'] = JSON.stringify({});
                            }
                        });
                        $('#signatureReleasePad').modal('hide')
                    }
                }

                function switchLocation(item){
                    if(!$scope.data.releaseData.isSubmitted){
                        if(item.take_address == "Destination"){
                            item.take_address = "Origin";
                            item.billing_city = $scope.request.field_moving_to.locality;
                            item.billing_state = $scope.request.field_moving_to.administrative_area;
                            item.billing_zip = $scope.request.field_moving_to.postal_code;
                            item.billing_address = $scope.request.field_moving_to.thoroughfare;
                        }
                        else {
                            item.take_address = "Destination";
                            item.billing_city = $scope.request.field_moving_from.locality;
                            item.billing_state = $scope.request.field_moving_from.administrative_area;
                            item.billing_zip = $scope.request.field_moving_from.postal_code;
                            item.billing_address = $scope.request.field_moving_from.thoroughfare;
                        }
                        saveReleaseData();
                    }
                }

                function removeErrors() {
                    _.forEach($scope.itemsRelease, function (item, ind) {
                        let el = $("#release-block-"+ind);
                        if (el.hasClass('release-error')) {
                            el.removeClass('release-error');
                        }
                    });
                    let el = $("#release-block-header");
                    if (el.hasClass('release-error')) {
                        el.removeClass('release-error');
                    }
                    el = $("#release-block-footer");
                    if (el.hasClass('release-error')) {
                        el.removeClass('release-error');
                    }
                    el = $("#release-block-hoisting");
                    if (el.hasClass('release-error')) {
                        el.removeClass('release-error');
                    }
                }

                function setReleaseError(ind) {
                    let el = $("#release-block-"+ind);
                    el.addClass('release-error');
                    $('html, body').animate({
                        scrollTop: el.offset().top
                    }, 1000);
                    logger.warning('Please fill step', null, 'Error');
                    return false;
                }

                function validateTopBottom(field) {
                    if(angular.isDefined($scope.data.releaseData[field].checkboxItem) && !$scope.data.releaseData[field].checkboxItem) {
                        setReleaseError(field);
                        return true;
                    }
                    if(angular.isDefined($scope.data.releaseData[field].signature) && _.isEmpty($scope.data.releaseData[field].signature)) {
                        setReleaseError(field);
                        return true;
                    }
                    if(angular.isDefined($scope.data.releaseData[field].initials) && _.isEmpty($scope.data.releaseData[field].initials)) {
                        setReleaseError(field);
                        return true;
                    }
                }

                function validateReleaseData(){
                    let isError = false;

                    if(validateTopBottom("header")) return true;

                    // check hosting service
                    if($scope.contractPage.displayHoistingFee && !$scope.releaseService.checkboxItem) {
                        setReleaseError('hoisting');
                        return true
                    }

                    _.forEach($scope.itemsRelease, function (item, ind) {
                        if(angular.isDefined(item.checkboxItem) && !item.checkboxItem) {
                            setReleaseError(ind);
                            isError = true;
                            return false;
                        }
                        if(angular.isDefined(item.signature) && _.isEmpty(item.signature)) {
                            setReleaseError(ind);
                            isError = true;
                            return false;
                        }
                        if(angular.isDefined(item.initials) && _.isEmpty(item.initials)) {
                            setReleaseError(ind);
                            isError = true;
                            return false;
                        }
                        // if(angular.isDefined(item.textarea))
                    });
                    if(isError) return isError;

                    if(validateTopBottom("footer")) return true;

                    return isError;
                }

                function saveReleaseData(){
                    removeErrors();
                    $scope.data.releaseData.itemsRelease = $scope.itemsRelease;
                    $scope.data.releaseData.releaseService = $scope.releaseService;
                    $scope.saveContract( {
                        callback: function () {
                            $scope.busy = false;
                            localStorage['pendingSignatures'] = JSON.stringify({});
                        }
                    });
                }

                function submitReleaseData(){
                    if(validateReleaseData()) return false;
                    $scope.busy = true;
                    $scope.data.releaseData.isSubmitted = true;
                    saveReleaseData();
                }

                $scope.$on('release.init', function(){
                    initReleaseData();
                });

				initReleaseData();
            }
        }


}();
