/* /app/contractpage/directives/release-form/releaseFormSettings.directive.js */
/**
 * @desc add editable settings for release form on contract
 * @example <div release-form-settings></div>
 */
angular
    .module('contractModule')
    .directive('releaseFormSettings', releaseFormSettings);
releaseFormSettings.$inject = ['SettingServices', '$timeout'];

function releaseFormSettings(SettingServices, $timeout) {
    /* implementation details */
    let directive = {
        link: link,
        templateUrl: 'app/contractpage/directives/release-form/releaseFormSettings.directive.html',
        scope: {
            releaseSettings: '='
        },
        restrict: 'EA'
    };
    return directive;

    function link(scope, element, attrs) {
        let settingObj = {
            addCheckbox: false,
            checkbox: true,
            title: '',
            body: ''
        };

        scope.disabledIcheck = true;

        scope.modal = {
            showTextareaSettings: -1
        };
        scope.settingsMenu = {
            COMPANY_NAME: {
                title: 'Company name',
                value: '%COMPANY_NAME%'
            },
            INITIAL_INPUT: {
                title: 'Initial input',
                value: '%initial_input%'
            },
            SIGNATURE_FIELD: {
                title: 'Signature',
                value: '%signature_field%'
            },
            TEXTAREA_FIELD: {
                title: 'Textarea',
                value: '%textarea_field%'
            }
        };

        scope.addNewItem = addNewItem;
        scope.removeItem = removeItem;
        scope.blur = blur;
        scope.focusedTextArea = focusedTextArea;
        scope.addInput = addInput;
        scope.addedOneItemInSettings = addedOneItemInSettings;

        function addNewItem() {
            let newItem = angular.copy(settingObj);
            scope.releaseSettings.releaseForm.services.push(newItem);
        }

        function removeItem(index) {
            scope.releaseSettings.releaseForm.services.splice(index, 1);
        }

        function blur() {
            $timeout(function () {
                if (!$('.release-textarea.active').is(':focus'))
                    scope.modal.showTextareaSettings = -1;
            }, 500);
        }
        
        function focusedTextArea(ind) {
            $timeout(function () {
                    scope.modal.showTextareaSettings = ind;
            }, 500);
        }

        function addInput(input, textAreaModel) {
            let $activeTxtArea = $('.release-textarea.active textarea');
            if ($activeTxtArea.length && !addedOneItemInSettings(input, textAreaModel)) {
                var sel = window.getSelection();
                if (sel.getRangeAt && sel.rangeCount) {
                    let fullText = sel.getRangeAt(0);
                    fullText.insertNode(document.createTextNode(input.value));
                    return sel.getRangeAt(0);
                }
            }else{
                scope.modal.showTextareaSettings = -1;
            }
        }
        
        function addedOneItemInSettings(item, setting) {
            if(item.value == '%COMPANY_NAME%') return false;
            return (setting.search(item.value) > -1);
        }
    }
}
