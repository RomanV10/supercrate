angular.module('contractModule')
    .directive("signatureReleaseBox", signatureReleaseBox);

signatureReleaseBox.$inject = ['ContractFactory', 'logger'];

function signatureReleaseBox(ContractFactory, logger) {
    return {
        restrict: "EA",
        scope: {
            stepId: "@",
            owner: "@"
        },
        templateUrl: 'app/contractpage/directives/release-form/signature/signatureBoxRelease.html',
        link: linkFn
    };

    function linkFn(scope) {

        scope.data = ContractFactory.factory;

        function _setStepError(nextStep) {
            let el = $('#release-step_' + nextStep);
            el.addClass('error');
            $('html, body').animate({
                scrollTop: el.offset().top
            }, 1000);
            $('#signatureReleasePad').modal('hide');
            logger.warning('Please fill step', null, 'Error');
            return false;
        }

        function removeError(step) {
            let el = $('#release-step_' + step);
            if (el.hasClass('error')) {
                el.removeClass('error');
            }
        }

        function checkPrevSignatures() {
            let hasError = false;
            if(scope.stepId == 'header') return hasError;
            if(scope.stepId != 'header' && scope.data.releaseData.header && angular.isDefined(scope.data.releaseData.header.signature) && _.isEmpty(scope.data.releaseData.header.signature)){
                _setStepError('header');
                return true;
            }
            let condition;

            _.forEach(scope.data.releaseData.itemsRelease, function (item, ind) {
                if(scope.stepId == 'footer') condition = (scope.data.releaseData.itemsRelease[ind] && angular.isDefined(scope.data.releaseData.itemsRelease[ind].signature) && _.isEmpty(scope.data.releaseData.itemsRelease[ind].signature));
                else condition = (ind != scope.stepId && ind < scope.stepId && scope.data.releaseData.itemsRelease[ind] && angular.isDefined(scope.data.releaseData.itemsRelease[ind].signature) && _.isEmpty(scope.data.releaseData.itemsRelease[ind].signature))
                if(condition){
					_setStepError(ind);
					hasError = true;
					return false;
                }
            });
            return hasError;
        }

        scope.clickOpenSignature = function () {
            scope.data = scope.$parent.data;

            //remove all errors
            _.forEach(scope.data.releaseData.itemsRelease, function (item, ind) {
                removeError(ind);
            });
            ContractFactory.factory.releaseData.currentStep = scope.stepId;
            if(checkPrevSignatures()) return false;
            ContractFactory.openReleaseSignaturePad(scope.owner);
        };

        scope.removeReleaseSignature = function (stepId) {
            scope.$parent.removeSignatureFromRelease(stepId);
        };
    }

}
