'use strict';

angular.module('contractModule')
	.directive('parseShowStorageService', parseShowStorageService);

	parseShowStorageService.$inject = ['$rootScope', '$compile', 'datacontext'];

	function parseShowStorageService($rootScope, $compile, datacontext) {
	var companyName = (angular.fromJson((datacontext.getFieldData()).basicsettings)).company_name;

	return {
		restrict: 'AE',
		require: ['ngModel', '^?monthlyTotal', '^?volumeCuft', '^?rateCuft'],
		link: function (scope, element, attrs, ngModelCtrl) {
			var html = '';
			var regCase;
			var templates = {};
			var clearEvent, clearServEvent;
			scope.servicesForParser = scope.$parent.request.request_all_data.invoice ? angular.fromJson(scope.$parent.request.request_all_data.invoice.extraServices) : angular.fromJson(scope.$parent.request.extraServices);
			scope.additionalServices = _.clone(scope.$parent.additionalServices);
			scope.model = '';

			function render(model) {

				if (!model || (model != 'monthly_storage_fee' && model != 'overnight_storage'))
					return false;
				scope.model = model;
				templates = {
					'%COMPANY NAME%': companyName,
					'%initial_input%': '<br><div class=signatured-box style=cursor:default;height:45px;position:static;>'+
					'<img ng-src={{services["'+model+'"].signature.value}} ng-if=services["'+model+'"].signature /></div>',
					'%input_number_value%': '{{services["'+model+'"].extra_services[0].services_default_value}}',
					'%monthly_total%': '{{services["'+model+'"].extra_services[0].services_default_value}}',
					'%volume_cuft%': '{{services["'+model+'"].extra_services[0].services_default_value_volume}}'
				};
				regCase = new RegExp('\%(.*?)\%', 'g');
				html = buildHtml(angular.copy(scope.sf.text));
				element.html(html).show();
				$compile(element.contents())(scope);
				if (scope.sf.text.search('%monthly_total%') >= 0 || scope.sf.text.search('%volume_cuft%') >= 0) {
					setExtraServices(model);
				} else {
					if (!scope.services || _.isEmpty(scope.services)) {
						if (angular.isUndefined(scope.services)) {
							scope.services = {};
						}
						if (angular.isUndefined(scope.services[model])) {
							scope.services[model] = {};
						}
						if (angular.isUndefined(scope.services[model].extra_services)) {
							scope.services[model].extra_services = [];
							scope.services[model].extra_services[0] = {
								services_default_value: ''
							};
						}
						setServiceValue(model);
					} else if (angular.isUndefined(scope.services[model])) {
						scope.services[model] = {};
						scope.services[model].extra_services = [];
						scope.services[model].extra_services[0] = {
							services_default_value: ''
						};
						setServiceValue(model);
					}
				}
			}

			function changeMonthlyTotal(value, model) {
				if (!value)
					return false;
				scope.monthlyTotal = Number(value);
				changeMonthlyTotalVolumeCuft(model);
			}

			function changeVolumeCuft(value, model) {
				if (!value)
					return false;
				scope.volumeCuft = Number(value);
				changeMonthlyTotalVolumeCuft(model);
			}

			function changeMonthlyTotalVolumeCuft(model) {
				if (scope.sf.text.search('%monthly_total%') >= 0 || scope.sf.text.search('%volume_cuft%') >= 0) {
					setExtraServices(model);
					setServiceValue(model);
				}
			}

			function changeRateCuft(value) {
				if (!value)
					return false;
				scope.rateCuft = value;
			}

			scope.$watch(attrs['ngModel'], render);
			scope.$watch(attrs['monthlyTotal'], function (val) {
				if (!val) return false;
				changeMonthlyTotal(val, scope.model);
			});
			scope.$watch(attrs['volumeCuft'], function (val) {
				if (!val) return false;
				changeVolumeCuft(val, scope.model);
			});
			scope.$watch(attrs['rateCuft'], function (val) {
				if (!val) return false;
				changeRateCuft(val);
			});

			function setServiceValue(model) {
				if (angular.isUndefined(scope.services[model].extra_services)) {
					scope.services[model].extra_services = [];
					scope.services[model].extra_services[0] = {
						services_default_value: undefined
					};
				}
				if ("monthly_storage_fee" == model) {
					scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
					scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
					return false;
				}
				_.forEach(scope.additionalServices, function (service) {
					service.alias = (service.name.toLowerCase()).replace(/\s/g, '_');
					if (model == service.alias) {
						scope.services[model].extra_services[0].services_default_value = service.extra_services[0].services_default_value;
					}
				});
				_.forEach(scope.servicesForParser, function (service) {
					service.alias = (service.name.toLowerCase()).replace(/\s/g, '_');
					if (model == service.alias)
						scope.services[model].extra_services[0].services_default_value = service.extra_services[0].services_default_value;
					if ("monthly_storage_fee" == model && model == service.alias) {
						scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
						scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
					}
				});
			}

			function getTemplate(contentType) {
				var template = '';

				if (templates.hasOwnProperty(contentType)) {
					template = templates[contentType];
				}
				return template;
			}

			function buildHtml(content) {
				var html = '';
				var keys = content.match(regCase);

				if (keys) {
					for (var i = 0, l = keys.length; i < l; i++) {
						html = getTemplate(keys[i]);
						content = content.replace(keys[i], html);
					}
				}
				return content;
			}

			function setExtraServices(model) {
				if (!scope.services || _.isEmpty(scope.services)) {
					if (angular.isUndefined(scope.services)) {
						scope.services = {};
					}
					if (angular.isUndefined(scope.services[model])) {
						scope.services[model] = {};
						scope.services[model].extra_services = [];
						scope.services[model].extra_services[0] = {};
					}
					scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
					scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
				} else if (angular.isUndefined(scope.services[model])) {
					scope.services[model] = {};
					scope.services[model].extra_services = [];
					scope.services[model].extra_services[0] = {};
					scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
					scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
				}
			}

			scope.$on('$destroy', function () {
				if (_.isFunction(clearEvent))
					clearEvent();
				if (_.isFunction(clearServEvent))
					clearServEvent();
			})
		}
	};
}
