'use strict';

angular.module('contractModule')
	.directive('sfTextBox', sfTextBox);

	sfTextBox.$inject = ['$timeout', 'SettingServices', 'ContractValue'];

	function sfTextBox($timeout, SettingServices, ContractValue) {
		return {
			restrict: 'AE',
			scope: {
				data: '='
			},
			templateUrl: 'app/contractpage/directives/storage-fit/sfTextBox.html',
			link: function (scope) {
				scope.modal = {
					showTextareaSettings: -1
				};
				scope.settingsMenu = ContractValue.settingsMenu;
				SettingServices.getSettings('additional_settings')
					.then(function (data) {
						scope.extraServices = JSON.parse(data);
						_.forEach(scope.extraServices, function (services) {
							services.alias = (services.name.toLowerCase()).replace(/\s/g, '_');
						})
					});

				scope.blur = function () {
					$timeout(function () {
						if (!$('.sf-textarea.active').is(':focus')) {
							scope.modal.showTextareaSettings = -1;
						}
					}, 100);
				};

				scope.addInput = function (input, textAreaModel) {
					var $activeTxtArea = $('.sf-textarea.active');
					if ($activeTxtArea.length) {
						var cursorPos = $activeTxtArea.prop("selectionStart");
						var v = $activeTxtArea.val();
						var textBefore = v.substring(0, cursorPos);
						var textAfter = v.substring(cursorPos, v.length);
						var fullText = textBefore + input.value + textAfter;
						$activeTxtArea.val(fullText);
						textAreaModel.text = fullText;
					}
				};

				scope.removeStorage = function (index) {
					scope.data.storageFit.splice(index, 1)
				};

				scope.changeName = function (name) {
					var nameArr = name.replace(/_/g, " ").split(" ");
					var servName = '';
					_.forEach(nameArr, function (word, ind) {
						servName += word.charAt(0).toUpperCase() + word.slice(1);
						if (word != _.last(nameArr)) servName += ' ';
					});
					return servName;
				};

			}
		};
	}
