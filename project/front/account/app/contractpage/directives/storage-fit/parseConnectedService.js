'use strict';

angular.module('contractModule')
	.directive('parseConnectedService', parseConnectedService);

	parseConnectedService.$inject = ['$rootScope', '$compile', 'datacontext', 'common', 'StorageService', 'RequestServices'];

	function parseConnectedService($rootScope, $compile, datacontext, common, StorageService, RequestServices) {
		var basicSettings = angular.fromJson(datacontext.getFieldData().basicsettings);
		var companyName = basicSettings.company_name;

		return {
			restrict: 'AE',
			require: ['ngModel', '^?monthlyTotal', '^?volumeCuft', '^?rateCuft', '^?additionalServices'],
			link: function (scope, element, attrs, ngModelCtrl) {
				var html = '';
				var regCase;
				var templates = {};
				var clearEvent, clearServEvent;

				scope.additionalServices = _.clone(scope.services);
				scope.services = {};
				scope.model = '';
				scope.overnightRate = basicSettings.overnight.rate || 0;

				function render(model) {

					if (!model)
						return false;
					scope.model = model;
					templates = {
						'%COMPANY NAME%': companyName,
						'%initial_input%': '',
						'%input_number_value%': scope.overnightRate,
						'%monthly_total%': '{{services["' + model + '"].extra_services[0].services_default_value}}',
						'%volume_cuft%': '{{services["' + model + '"].extra_services[0].services_default_value_volume}}'
					};
					regCase = new RegExp('\%(.*?)\%', 'g');
					html = buildHtml(angular.copy(scope.service.text));
					element.html(html).show();
					$compile(element.contents())(scope);
					if (scope.service.text.search('%monthly_total%') >= 0 || scope.service.text.search('%volume_cuft%') >= 0) {
						setExtraServices(model);
					} else {
						if (!scope.services || _.isEmpty(scope.services)) {
							if (angular.isUndefined(scope.services)) {
								scope.services = {};
							}
							if (angular.isUndefined(scope.services[model])) {
								scope.services[model] = {};
							}
							if (angular.isUndefined(scope.services[model].extra_services)) {
								scope.services[model].extra_services = [];
								scope.services[model].extra_services[0] = {
									services_default_value: ''
								};
							}
							setServiceValue(model);
						} else if (angular.isUndefined(scope.services[model])) {
							scope.services[model] = {};
							scope.services[model].extra_services = [];
							scope.services[model].extra_services[0] = {
								services_default_value: ''
							};
							setServiceValue(model);
						}
						setServiceValue(model);
					}
				}

				function changeMonthlyTotal(value, model) {
					if (!value)
						return false;
					scope.monthlyTotal = Number(value);
					changeMonthlyTotalVolumeCuft(model);
				}

				function changeVolumeCuft(value, model) {
					if (!value)
						return false;
					scope.volumeCuft = Number(value);
					changeMonthlyTotalVolumeCuft(model);
				}


				function changeMonthlyTotalVolumeCuft(model) {
					if (scope.service.text.search('%monthly_total%') >= 0 || scope.service.text.search('%volume_cuft%') >= 0) {
						setExtraServices(model);
						setServiceValue(model);
					}
				}

				function setExtraServices(model) {
					if (!scope.services || _.isEmpty(scope.services)) {
						if (angular.isUndefined(scope.services)) {
							scope.services = {};
						}
						if (angular.isUndefined(scope.services[model])) {
							scope.services[model] = {};
							scope.services[model].extra_services = [];
							scope.services[model].extra_services[0] = {};
						}
						scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
						scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
					} else if (angular.isUndefined(scope.services[model])) {
						scope.services[model] = {};
						scope.services[model].extra_services = [];
						scope.services[model].extra_services[0] = {};
						scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
						scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
					}
				}

				function changeRateCuft(value) {
					if (!value)
						return false;
					scope.rateCuft = value;
				}

				function createStorage(request) {
					if(request.request_all_data.storage_request_id){
						$rootScope.$broadcast('storage.request.exist');
						return false;
					}
					scope.$parent.busy = true;
					if (request.service_type.raw == 2 && !request.request_all_data.withoutStorageRequest) {
						if (request.storage_id)
							if (!request.request_all_data.toStorage) {
								RequestServices.getRequestsByNid(request.storage_id).then(function (requestStorage) {
									if (!_.isEmpty(requestStorage.nodes))
										StorageService.createStorageReqObj(request, request.date.value, requestStorage.nodes[0].date.value, scope.volumeCuft, requestStorage.nodes[0], request.nid);
									else
										StorageService.createStorageReqObj(request, request.date.value, '', scope.volumeCuft, '', request.nid);
								});
							} else {
								RequestServices.getRequestsByNid(request.storage_id).then(function (requestStorage) {
									if (!_.isEmpty(requestStorage.nodes))
										StorageService.createStorageReqObj(requestStorage.nodes[0], requestStorage.nodes[0].date.value, request.date.value, scope.volumeCuft, request, request.nid);
									else
										StorageService.createStorageReqObj(request, request.date.value, '', scope.volumeCuft, '', request.nid);
								});
							}
					} else {
						RequestServices.getRequestsByNid(request.storage_id).then(function (requestStorage) {
							if (!_.isEmpty(requestStorage.nodes))
								StorageService.createStorageReqObj(requestStorage.nodes[0], requestStorage.nodes[0].date.value, request.date.value, scope.volumeCuft, requestStorage.nodes[0], request.nid);
							else
								StorageService.createStorageReqObj(request, request.date.value, '', scope.volumeCuft, '', request.nid);
						});
					}
				}

				scope.$watch(attrs['ngModel'], render);
				scope.$watch(attrs['monthlyTotal'], function (val) {
					if (!val) return false;
					changeMonthlyTotal(val, scope.model);
				});
				scope.$watch(attrs['volumeCuft'], function (val) {
					if (!val) return false;
					changeVolumeCuft(val, scope.model);
				});
				scope.$watch(attrs['rateCuft'], function (val) {
					if (!val) return false;
					changeRateCuft(val);
				});

				function setServiceValue(model) {
					if (angular.isUndefined(scope.services[model].extra_services)) {
						scope.services[model].extra_services = [];
						scope.services[model].extra_services[0] = {
							services_default_value: undefined
						};
					}
					if ("monthly_storage_fee" == model) {
						scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
						scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
						return false;
					}
					_.forEach(scope.additionalServices, function (service) {
						service.alias = (service.name.toLowerCase()).replace(/\s/g, '_');
						if (model == service.alias) {
							scope.services[model].extra_services[0].services_default_value = service.extra_services[0].services_default_value;
						}
					});
				}

				function getTemplate(contentType) {
					var template = '';

					if (templates.hasOwnProperty(contentType)) {
						template = templates[contentType];
					}
					return template;
				}

				function buildHtml(content) {
					var html = '';
					var keys = content.match(regCase);

					if (keys) {
						for (var i = 0, l = keys.length; i < l; i++) {
							html = getTemplate(keys[i]);
							content = content.replace(keys[i], html);
						}
					}

					return content;
				}

				scope.$on('$destroy', function () {
					if (_.isFunction(clearEvent))
						clearEvent();
					if (_.isFunction(clearServEvent))
						clearServEvent();
				});

				$rootScope.$on('save.service', function (evt, data) {
					var model = data.service.connected_service;
					if(model == 'monthly_storage_fee'){
						scope.services[model].extra_services[0].services_default_value_volume = data.data.volume;
						scope.services[model].extra_services[0].services_default_value = data.data.total;
						scope.services[model].signature = data.signature;
						createStorage(data.request);
						$rootScope.$broadcast('event:save.SIT.service', model, scope.services[model]);
					} else if(model == 'overnight_storage') {
						let overnight = _.find(data.request.request_all_data.invoice.extraServices, (service) => service.alias == model);

						if(_.isUndefined(overnight)) {
							overnight = _.find(data.request.extraServices, (service) => {
								return service.name.toLowerCase().replace(' ', '_') == model;
							});
						}

						scope.services[model].extra_services = _.get(overnight, 'extra_services', [
							{
								services_default_value: scope.overnightRate,
								services_name: "Cost",
								services_read_only: false,
								services_type: "Amount"
							},
							{
								services_default_value: 1,
								services_name: "Value",
								services_read_only: false,
								services_type: "Number"

							}
						]);

						scope.services[model].name = "Overnight Storage";
						scope.services[model].alias = "overnight_storage";
						scope.services[model].signature = data.signature;

						$rootScope.$broadcast('event:save.SIT.service', model, scope.services[model]);
					}
				});
			}
		};
	}
