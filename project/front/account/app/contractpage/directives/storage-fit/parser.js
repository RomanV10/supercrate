'use strict';

angular.module('contractModule')
	.directive('parser', parser);

	parser.$inject = ['$rootScope', '$compile', 'datacontext', 'common', 'StorageService', 'RequestServices', 'SweetAlert', '$timeout'];

	function parser($rootScope, $compile, datacontext, common, StorageService, RequestServices, SweetAlert, $timeout) {
		var companyName = (angular.fromJson((datacontext.getFieldData()).basicsettings)).company_name;

		return {
			restrict: 'AE',
			require: ['ngModel', '^?monthlyTotal', '^?volumeCuft', '^?rateCuft'],
			link: function (scope, element, attrs, ngModelCtrl) {
				var html = '';
				var regCase;
				var templates = {};
				var clearEvent, clearServEvent;
				scope.servicesForParser = scope.$parent.request.request_all_data.invoice ? angular.fromJson(scope.$parent.request.request_all_data.invoice.extraServices) : angular.fromJson(scope.$parent.request.extraServices);
				scope.additionalServices = _.clone(scope.$parent.additionalServices);
				scope.model = '';
				scope.isStorage = isStorage;

				function render(model) {

					if (!model)
						return false;
					scope.model = model;
					templates = {
						'%COMPANY NAME%': companyName,
						'%initial_input%': '<input type="text" ng-disabled="data.isSubmitted " ng-focus=isStorage("' + model + '",$event) ng-blur=setService("' + model + '") ng-model=data.storageFit["' + model + '"] maxlength="2" size="2" style="font-size: 18px;max-width: 50px;">',
						'%input_number_value%': '<input class="small" type="number" ng-focus=isStorage("' + model + '",$event) string-to-number ng-disabled="data.isSubmitted" ng-model=services["' + model + '"].extra_services[0].services_default_value ng-blur=changeServiceValue("' + model + '") />',
						'%monthly_total%': '<input class="medium"type="number" ng-focus=isStorage("' + model + '",$event) string-to-number ng-disabled="data.isSubmitted" ng-blur=changeMonthly(services["' + model + '"].extra_services[0].services_default_value,"' + model + '") ng-model=services["' + model + '"].extra_services[0].services_default_value >',
						'%volume_cuft%': '<input class="small" type="number" ng-focus=isStorage("' + model + '",$event) string-to-number ng-disabled="data.isSubmitted " ng-model=services["' + model + '"].extra_services[0].services_default_value_volume  ng-blur=changeVolume(services["' + model + '"].extra_services[0].services_default_value_volume,"' + model + '") />'
					};
					regCase = new RegExp('\%(.*?)\%', 'g');
					html = buildHtml(angular.copy(scope.sf.text));
					if (model != 'overnight_storage' && model != 'monthly_storage_fee') {
						element.html(html).show();
						$compile(element.contents())(scope);
					}
					if (scope.sf.text.search('%monthly_total%') >= 0 || scope.sf.text.search('%volume_cuft%') >= 0) {
						if (!scope.services || _.isEmpty(scope.services)) {
							if (angular.isUndefined(scope.services)) {
								scope.services = {};
							}
							if (angular.isUndefined(scope.services[model])) {
								scope.services[model] = {};
								scope.services[model].extra_services = [];
								scope.services[model].extra_services[0] = {};
							}
							scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
							scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
						} else if (angular.isUndefined(scope.services[model])) {
							scope.services[model] = {};
							scope.services[model].extra_services = [];
							scope.services[model].extra_services[0] = {};
							scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
							scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
						}
					} else {
						if (!scope.services || _.isEmpty(scope.services)) {
							if (angular.isUndefined(scope.services)) {
								scope.services = {};
							}
							if (angular.isUndefined(scope.services[model])) {
								scope.services[model] = {};
							}
							if (angular.isUndefined(scope.services[model].extra_services)) {
								scope.services[model].extra_services = [];
								scope.services[model].extra_services[0] = {
									services_default_value: ''
								};
							}
							setServiceValue(model);
						} else if (angular.isUndefined(scope.services[model])) {
							scope.services[model] = {};
							scope.services[model].extra_services = [];
							scope.services[model].extra_services[0] = {
								services_default_value: ''
							};
							setServiceValue(model);
						}
					}
				}

				function changeMonthlyTotal(value, model) {
					if (!value)
						return false;
					scope.monthlyTotal = Number(value);
					if (scope.sf.text.search('%monthly_total%') >= 0 || scope.sf.text.search('%volume_cuft%') >= 0) {
						if (!scope.services || _.isEmpty(scope.services)) {
							if (angular.isUndefined(scope.services)) {
								scope.services = {};
							}
							if (angular.isUndefined(scope.services[model])) {
								scope.services[model] = {};
								scope.services[model].extra_services = [];
								scope.services[model].extra_services[0] = {};
							}
							scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
							scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
						} else if (angular.isUndefined(scope.services[model])) {
							scope.services[model] = {};
							scope.services[model].extra_services = [];
							scope.services[model].extra_services[0] = {};
							scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
							scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
						}
						setServiceValue(model);
					}
				}

				function changeVolumeCuft(value, model) {
					if (!value)
						return false;
					scope.volumeCuft = Number(value);
					if (scope.sf.text.search('%monthly_total%') >= 0 || scope.sf.text.search('%volume_cuft%') >= 0) {
						if (!scope.services || _.isEmpty(scope.services)) {
							if (angular.isUndefined(scope.services)) {
								scope.services = {};
							}
							if (angular.isUndefined(scope.services[model])) {
								scope.services[model] = {};
								scope.services[model].extra_services = [];
								scope.services[model].extra_services[0] = {};
							}
							scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
							scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
						} else if (angular.isUndefined(scope.services[model])) {
							scope.services[model] = {};
							scope.services[model].extra_services = [];
							scope.services[model].extra_services[0] = {};
							scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
							scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
						}
						setServiceValue(model);
					}
				}

				function changeRateCuft(value) {
					if (!value)
						return false;
					scope.rateCuft = value;
				}

				function addStorageService(model) {
					if (scope.data.storageFit[model] && scope.data.storageFit[model] != '' && scope.data.storageFit[model] != "null")
						if (scope.services[model].extra_services[0].services_default_value && scope.services[model].extra_services[0].services_default_value_volume)
							$rootScope.$broadcast('event:initial-storage-request', model, {
								rate: scope.services[model].extra_services[0].services_default_value,
								volume: scope.services[model].extra_services[0].services_default_value_volume
							});
					if (!scope.data.storageFit[model] && scope.data.storageFit[model] == '' || scope.data.storageFit[model] == "null")
						removeService(model);
				}

				function removeService(model) {
					$rootScope.$broadcast('event:initial-storage-request-remove', model);
				}

				function createStorage() {
					$timeout(function () {
						swal({
							title: "Enter volume of your storage inventory",
							text: "",
							type: "input",
							showCancelButton: true,
							closeOnConfirm: false,
							confirmButtonText: "Ok",
							animation: "slide-from-top",
							inputPlaceholder: "Volume, cuft",
							inputType: "number",
							showLoaderOnConfirm: true
						}, function (inputValue) {
							if (inputValue === false) return false;
							if (inputValue === "") {
								swal.showInputError("You need to write something!");
								return false
							}
							var request = scope.$parent.request;
							scope.$parent.busy = true;
							if (request.service_type.raw == 2) {
								if (request.storage_id)
									if (!request.request_all_data.toStorage) {
										RequestServices.getRequestsByNid(request.storage_id).then(function (requestStorage) {
											if (!_.isEmpty(requestStorage.nodes))
												StorageService.createStorageReqObj(request, request.date.value, requestStorage.nodes[0].date.value, inputValue, requestStorage.nodes[0], request.nid);
											else
												StorageService.createStorageReqObj(request, request.date.value, '', inputValue, '', request.nid);
										});
									} else {
										RequestServices.getRequestsByNid(request.storage_id).then(function (requestStorage) {
											if (!_.isEmpty(requestStorage.nodes))
												StorageService.createStorageReqObj(requestStorage.nodes[0], requestStorage.nodes[0].date.value, request.date.value, inputValue, request, request.nid);
											else
												StorageService.createStorageReqObj(request, request.date.value, '', inputValue, '', request.nid);
										});
									}
							} else {
								RequestServices.getRequestsByNid(request.storage_id).then(function (requestStorage) {
									if (!_.isEmpty(requestStorage.nodes))
										StorageService.createStorageReqObj(requestStorage.nodes[0], requestStorage.nodes[0].date.value, request.date.value, inputValue, requestStorage.nodes[0], request.nid);
									else
										StorageService.createStorageReqObj(request, request.date.value, '', inputValue, '', request.nid);
								});
							}
						});
					}, 300);
				}

				function isStorage(model, $event) {
					if (model == 'monthly_storage_fee') {
						if (!scope.$parent.request.request_all_data.storage_request_id) {
							SweetAlert.swal({
									title: "Do you want to create Storage Request?",
									text: "",
									type: "warning",
									showCancelButton: true,
									confirmButtonColor: "#DD6B55",
									confirmButtonText: "Yes, create it!",
									closeOnConfirm: true
								},
								function (isConfirm) {
									if (isConfirm) {
										createStorage();
									} else {
										$event.target.blur();
										return false;
									}
								});
						}
					} else {
						return false;
					}
				}

				scope.$watch(attrs['ngModel'], render);
				scope.$watch(attrs['monthlyTotal'], function (val) {
					if (!val) return false;
					changeMonthlyTotal(val, scope.model);
				});
				scope.$watch(attrs['volumeCuft'], function (val) {
					if (!val) return false;
					changeVolumeCuft(val, scope.model);
				});
				scope.$watch(attrs['rateCuft'], function (val) {
					if (!val) return false;
					changeRateCuft(val);
				});

				scope.changeVolume = function (value, model) {
					scope.volumeCuft = Number(value);
					scope.monthlyTotal = Number((Number(scope.volumeCuft) * Number(scope.rateCuft)).toFixed(2));
					scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
					changeVolumeCuft(value, model);
				};

				scope.changeMonthly = function (value, model) {
					changeMonthlyTotal(value, model);
				};

				scope.changeServiceValue = function (model) {
					clearServEvent = $rootScope.$broadcast('event:change-service-value', model, scope.services[model].extra_services[0].services_default_value);
					if (scope.data.storageFit[model] != 'null' && scope.data.storageFit[model] != '' && scope.data.storageFit[model]) {
						clearEvent = $rootScope.$broadcast('event:initial-value', model);
						var details = {
							text: []
						};
						var msg = {
							text: 'Rate on ' + model.replace(/_/g, ' ') + ' was changed on contract.',
							to: scope.services[model].extra_services[0].services_default_value
						};
						details.text.push(msg);
						$rootScope.$broadcast('contract.changed', details.text);
					}
				};

				scope.setService = function (model) {
					if (angular.isDefined(scope.services[model])) {
						if (model == 'monthly_storage_fee') {
							if (angular.isDefined(scope.services[model].extra_services[0])) {
								if (scope.services[model].extra_services[0].services_default_value_volume && scope.services[model].extra_services[0].services_default_value_volume != "" && scope.services[model].extra_services[0].services_default_value_volume != "null") {
									addStorageService(model);
								} else {
									scope.data.storageFit[model] = '';
									removeService(model);
								}
							}
						} else if (angular.isDefined(scope.services[model].extra_services[0])) {
							if (angular.isDefined(scope.services[model].extra_services[0].services_default_value)) {
								if (scope.services[model].extra_services[0].services_default_value != 'null' && scope.services[model].extra_services[0].services_default_value != null && scope.services[model].extra_services[0].services_default_value != 0) {
									clearEvent = $rootScope.$broadcast('event:initial-value', model);
									if (scope.services[model])
										if (scope.services[model].extra_services)
											if (scope.services[model].extra_services[0].services_default_value != "null" && scope.data.storageFit[model] != '') {
												clearServEvent = $rootScope.$broadcast('event:change-service-value', model, scope.services[model].extra_services[0].services_default_value)
											}
								} else {
									if (_.isNaN(Number(model)))
										scope.data.storageFit[model] = '';
								}
							} else {
								clearEvent = $rootScope.$broadcast('event:initial-value', model);
							}
						}
					}
				};

				function setServiceValue(model) {
					if (angular.isUndefined(scope.services[model].extra_services)) {
						scope.services[model].extra_services = [];
						scope.services[model].extra_services[0] = {
							services_default_value: undefined
						};
					}
					if ("monthly_storage_fee" == model) {
						scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
						scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
						addStorageService(model);
						return false;
					}
					_.forEach(scope.additionalServices, function (service) {
						service.alias = (service.name.toLowerCase()).replace(/\s/g, '_');
						if (model == service.alias) {
							scope.services[model].extra_services[0].services_default_value = service.extra_services[0].services_default_value;
						}
					});
					_.forEach(scope.servicesForParser, function (service) {
						service.alias = (service.name.toLowerCase()).replace(/\s/g, '_');
						if (model == service.alias)
							scope.services[model].extra_services[0].services_default_value = service.extra_services[0].services_default_value;
						if ("monthly_storage_fee" == model && model == service.alias) {
							scope.services[model].extra_services[0].services_default_value_volume = scope.volumeCuft;
							scope.services[model].extra_services[0].services_default_value = scope.monthlyTotal;
							addStorageService(model);
						}
					});
				}

				function getTemplate(contentType) {
					var template = '';

					if (templates.hasOwnProperty(contentType)) {
						template = templates[contentType];
					}
					return template;
				}

				function buildHtml(content) {
					var html = '';
					var keys = content.match(regCase);

					if (keys) {
						for (var i = 0, l = keys.length; i < l; i++) {
							html = getTemplate(keys[i]);
							content = content.replace(keys[i], html);
						}
					}

					return content;
				}

				scope.$on('$destroy', function () {
					if (_.isFunction(clearEvent))
						clearEvent();
					if (_.isFunction(clearServEvent))
						clearServEvent();
				})
			}
		};
	}
