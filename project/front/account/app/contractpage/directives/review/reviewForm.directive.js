'use strict';

angular
	.module('contractModule')
	.directive('reviewForm', reviewForm);

/*@ngInject*/
function reviewForm() {
	return {
		restrict: 'E',
		scope: {
			request: '=',
			data: '=',
			saveContract: '&',
		},
		template: require('./reviewForm.html'),
		controller: 'ReviewFormController'
	};
}
