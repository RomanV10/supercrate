'use strict';

angular
	.module('contractModule')
	.controller('ReviewFormController', ReviewFormController);

/*@ngInject*/
function ReviewFormController($scope, RequestPageServices, reviewSharedService, SweetAlert) {
	const ENTITY_TYPE = reviewSharedService.ENTITY_TYPE;
	let entityType;

	$scope.basicSettings = reviewSharedService.BASIC_SETTINGS;
	$scope.feedBack = reviewSharedService.FEED_BACK_INITIAL;

	$scope.saveFeedBack = saveFeedBack;

	function initData() {
		$scope.feedBack.date = $scope.request.date.value;
		$scope.feedBack.job = $scope.request.nid;
		if (_.get($scope.request, 'field_foreman.old.field_user_first_name')) {
			let firstName = _.get($scope.request, `field_foreman.old.field_user_first_name.und[${0}].value`);
			let lastName = _.get($scope.request, `field_foreman.old.field_user_last_name.und[${0}].value`)
			$scope.feedBack.teamLeader = `${firstName} ${lastName}`;
		}
		$scope.feedBack.customerName = $scope.request.name;
		$scope.feedBack.customerEmail = $scope.request.email;
		if (angular.isDefined($scope.data.feedBack) && !_.isEmpty($scope.data.feedBack))
			$scope.feedBack = _.clone($scope.data.feedBack);
	}

	function tryGetReview() {
		let ldRequest = $scope.request.service_type.raw == 7;
		let storageRequest = $scope.request.service_type.raw == 2 || $scope.request.service_type.raw == 6;
		if (ldRequest) {
			entityType = ENTITY_TYPE.LDREQUEST;
		} else if (storageRequest) {
			entityType = ENTITY_TYPE.STORAGEREQUEST;
		} else {
			entityType = ENTITY_TYPE.MOVEREQUEST;
		}

		RequestPageServices.getCurrentReview({
			entity_type: entityType,
			entity_id: $scope.request.nid
		})
			.then(optimisticResponse);
	}

	function optimisticResponse(resolve) {
		if (!_.isArray(resolve)) {
			$scope.feedBack.subbmited = true;
			$scope.feedBack.comments = resolve.text_review;
			$scope.feedBack.rating = resolve.count_stars;
		} else {
			$scope.feedBack.subbmited = false;
			$scope.review = {
				entity_type: entityType,
				entity_id: $scope.request.nid,
				user_id: $scope.request.uid.uid
			};
		}
		$scope.data.feedBack = _.clone($scope.feedBack);
	}

	function saveFeedBack() {
		$scope.busy = true;
		$scope.feedBack.subbmited = true;
		if ($scope.feedBack.comments.length <= 3) {
			$scope.busy = false;
			$scope.feedBack.subbmited = false;
			SweetAlert.swal('Please write something in comments field', '', 'info');
			return;
		}

		$scope.review.count_stars = $scope.feedBack.rating;
		$scope.review.text_review = $scope.feedBack.comments;
		$scope.data.feedBack = _.clone($scope.feedBack);
		RequestPageServices.createReview($scope.review);

		$scope.saveContract({
			callback: function () {
				$scope.busy = false;
				localStorage['pendingSignatures'] = JSON.stringify({});
				SweetAlert.swal('Your feedback has been sent!', 'Thank you!', 'success');
				$scope.feedBack.subbmited = true;
			}
		});
	}

	$scope.$on('data_loaded', function () {
		initData();
		tryGetReview();
	});
}
