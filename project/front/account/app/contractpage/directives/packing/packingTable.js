angular.module('contractModule')
	.directive("packingTable", packingTable);

/*@ngInject*/
function packingTable(ContractService, ContractCalc, ContractFactory, RequestServices, logger, datacontext, $rootScope, ContractValue) {

	return {
		restrict: "EA",
		scope: {
			request: '=',
			pageSubmitted: '<?'
		},
		templateUrl: "app/contractpage/directives/packing/packing.directive.html",
		link: linkFn
	};

	function linkFn(scope) {
		scope.NAME_FULL_PACKING = "Estimate Full Packing";
		scope.NAME_PARTIAL_PACKING = "Estimate Partial Packing";
		scope.isFamiliarization = ContractValue.isFamiliarization;
		scope.isPrint = ContractValue.isPrint;

		if (angular.isUndefined(scope.request.request_all_data.invoice)) {
			scope.request.request_all_data.invoice = {};
			scope.request.request_all_data.invoice.request_data = {};
			scope.request.request_all_data.invoice.request_data.value = scope.request.request_data.value != '' ? scope.request.request_data.value : {};
		}

		if (angular.isUndefined(scope.request.request_all_data.invoice.request_data)) {
			scope.request.request_all_data.invoice.request_data = {};
			scope.request.request_all_data.invoice.request_data.value = scope.request.request_data.value != '' ? scope.request.request_data.value : {};
		}

		var request_data = scope.request.request_all_data.invoice.request_data.value != '' ? angular.fromJson(scope.request.request_all_data.invoice.request_data.value) : {};

		scope.extra = ContractFactory.extra_data;
		scope.dummyArray = new Array(101);
		scope.data = ContractFactory.factory;

		scope.packing = [];
		scope.addPackings = [];
		scope.editPacking = [];
		scope.removePacking = [];
		scope.showPackingServicesRef = {
			show: false
		};

		scope.addPacking = addPacking;
		scope.removeCharge = removeCharge;
		scope.savePackings = savePackings;
		scope.sumChanged = sumChanged;
		scope.calcAmount = calcAmount;
		scope.editPackingLog = editPackingLog;
		scope.showAddPackingBtn = showAddPackingBtn;
		scope.extra.selectedPackings = [];
		scope.reqType = (scope.request.service_type.raw == '7' || scope.request.service_type.raw == '5');

		scope.fieldData = datacontext.getFieldData();
		scope.basicSettings = angular.fromJson(scope.fieldData.basicsettings);
		if (angular.isDefined(scope.basicSettings.packing_settings)) {
			scope.packingSettings = {};
			scope.packingSettings.account = false;
			scope.packingSettings.labor = false;
			scope.packingService = ContractValue.packingServiceData;
			if (angular.isDefined(scope.basicSettings.packing_settings.packingAccount))
				scope.packingSettings.account = scope.basicSettings.packing_settings.packingAccount;
			if (angular.isDefined(scope.basicSettings.packing_settings.packingLabor))
				scope.packingSettings.labor = scope.basicSettings.packing_settings.packingLabor;
			if (angular.isDefined(scope.basicSettings.packing_settings.packing))
				if (angular.isDefined(scope.basicSettings.packing_settings.packing.LM))
					scope.packingService.LM = scope.basicSettings.packing_settings.packing.LM;
			if (angular.isDefined(scope.basicSettings.packing_settings.packing))
				if (angular.isDefined(scope.basicSettings.packing_settings.packing.LD))
					scope.packingService.LD = scope.basicSettings.packing_settings.packing.LD;
		}
		if (scope.fieldData.packing_settings) {
			var data = [];
			data[0] = angular.fromJson(scope.fieldData.packing_settings)
			_sortingPackings(data);
		}

		function addPacking(index, p) {
			scope.showPackingServicesRef.show = false;
			if (!index && index != 0) {
				scope.extra.selectedPackings.push({
					name: 'Custom Packing',
					rate: 0,
					quantity: 1,
					showInContract: false,
					custom: true
				});
				scope.addPackings.push({
					name: 'Custom Packing',
					rate: 0,
					quantity: 1,
					showInContract: false,
					custom: true
				});
			}
			else {
				var item = scope.packing[index];
				scope.extra.selectedPackings.push(item);
				scope.addPackings.push(item);
				scope.packing.splice(index, 1);
			}
			savePackings();
		}

		function sumChanged() {
			var sum = 0;
			let fullPacking = _.findIndex(scope.extra.selectedPackings, {name: scope.NAME_FULL_PACKING});
			let partialPacking = _.findIndex(scope.extra.selectedPackings, {name: scope.NAME_PARTIAL_PACKING});
			if (scope.request.service_type.raw == '7' || scope.request.service_type.raw == '5') {
				_.each(scope.extra.selectedPackings, function (p) {
					if (angular.isUndefined(p.LDRate))
						p.LDRate = 0;
					if (angular.isUndefined(p.laborRate))
						p.laborRate = 0;
					if (scope.packingSettings.labor) sum += (Number(p.LDRate) + Number(p.laborRate)) * Number(p.quantity);
					else sum += (Number(p.LDRate)) * Number(p.quantity);
				});
				if (_.isNaN(sum)) return 0;

				ContractCalc.finance.packingSum = sum;
			}
			if (scope.request.service_type.raw != '7' && scope.request.service_type.raw != '5') {
				_.each(scope.extra.selectedPackings, function (p) {
					if (angular.isUndefined(p.rate))
						p.rate = 0;

					if (_.isString(p.rate)) {
						p.rate = parseFloat(p.rate);
					}

					if (_.isString(p.quantity)) {
						p.quantity = parseFloat(p.quantity);
					}

					sum += Number(p.rate) * Number(p.quantity);
				});

				if (_.isNaN(sum)) return 0;

				ContractCalc.finance.packingSum = sum;
			}

			if (fullPacking >= 0) {
				sum -= scope.extra.selectedPackings[fullPacking].total;
			}
			if (partialPacking >= 0) {
				sum -= scope.extra.selectedPackings[partialPacking].total;
			}

			return sum.toFixed(2);
		}

		function savePackings(pack) {
			var data = {};
			if (!_.isEmpty(request_data)) {
				data = _.clone(request_data);
				data.packings = [];
			} else {
				data.packings = [];
			}
			var countPack = 0;
			var choosePackID = undefined;
			_.forEach(scope.extra.selectedPackings, function (packing) {
				if (packing.quantity > 0)
					data.packings.push(packing);
			});
			if (pack) {
				_.forEach(data.packings, function (item, id) {
					if (pack.name == item.name && countPack == 0) {
						choosePackID = id;
						countPack++;
						return false;
					}
				});
				_.forEachRight(data.packings, function (item, i) {
					if (pack.name == item.name && countPack != 0 && angular.isDefined(choosePackID) && data.packings.length > 1) {
						if (choosePackID != i)
							data.packings.splice(i, 1);
					}
				});
			}
			request_data.packings = data.packings;
			var dataArr = [];
			dataArr[0] = angular.fromJson(scope.fieldData.packing_settings);
			scope.request.request_all_data.invoice.request_data.value = angular.copy(data);

			$rootScope.$broadcast('recalc.contract', scope.request.request_all_data);

			var addMsg = [];
			var removeMsg = [];
			_.forEach(scope.addPackings, function (pack) {
				var msg = {
					text: 'Packing ' + pack.name + ' was added on contract.'
				};
				addMsg.push(msg);
			});
			_.forEach(scope.removePacking, function (pack) {
				var msg = {
					text: 'Packing ' + pack.name + ' was removed on contract.'
				};
				removeMsg.push(msg);
			});
			var details = {
				title: 'Packing',
				text: [],
				date: moment().unix()
			};
			_.forEach(addMsg, function (log) {
				details.text.push(log);
			});
			_.forEach(removeMsg, function (log) {
				details.text.push(log);
			});
			_.forEach(scope.editPacking, function (log) {
				details.text.push(log);
			});
			if (!_.isEmpty(details.text))
				$rootScope.$broadcast('contract.changed', details.text);
			scope.addPackings = [];
			scope.editPacking = [];
			scope.removePacking = [];
		}

		function removeCharge(index) {
			if (!!scope.extra.selectedPackings[index]) {
				var item = scope.extra.selectedPackings[index];
				if (!item.hasOwnProperty('custom')) {
					scope.packing.push(item);
				}
				scope.removePacking.push(scope.extra.selectedPackings[index]);
				scope.extra.selectedPackings.splice(index, 1);
				savePackings();
			}
		}

		savePackings();

		function calcAmount(rate, quantity, labor) {
			if (scope.packingSettings.labor && (scope.request.service_type.raw == '7' || scope.request.service_type.raw == '5')) {
				if (!labor)
					labor = 0;
				return (Number(rate) + Number(labor)) * Number(quantity);
			}
			else if (!scope.packingSettings.labor && (scope.request.service_type.raw == '7' || scope.request.service_type.raw == '5')) {
				return (Number(rate)) * Number(quantity);
			}
			if (scope.request.service_type.raw != '7' && scope.request.service_type.raw != '5') {
				return Number(rate) * Number(quantity);
			}
		}

		function editPackingLog(title, extra_charge, val) {
			if (scope.request.service_type.raw != '5' && scope.request.service_type.raw != '7') {
				extra_charge.total = calcAmount(extra_charge.rate, extra_charge.quantity);
			} else {
				extra_charge.total = calcAmount(extra_charge.LDRate, extra_charge.quantity, extra_charge.laborRate);
			}

			var msg = {
				text: title + ' on ' + extra_charge.name + ' was changed on contract.',
				to: val
			};

			scope.editPacking.push(msg);
		}

		function showAddPackingBtn() {
			if (_.isUndefined(scope.pageSubmitted)) {
				return !scope.data.isSubmitted && !scope.isFamiliarization && !scope.isPrint;
			} else {
				return !scope.pageSubmitted && !scope.isFamiliarization && !scope.isPrint;
			}
		}

		function _sortingPackings(data) {

			var allPackings = angular.fromJson(_.first(data)) || [];
			var selectedPacks = [];
			var initial_selects = request_data.packings || [];
			_.forEach(allPackings, function (item, i) {
				if (angular.isDefined(item.showInContract)) {
					if (item.showInContract) {
						if (scope.request.service_type.raw == '7' || scope.request.service_type.raw == '5') {
							if (angular.isDefined(item.rate))
								delete item.rate;
						}
						if (scope.request.service_type.raw != '7' && scope.request.service_type.raw != '5') {
							if (angular.isDefined(item.LDRate))
								delete item.LDRate;
							if (angular.isDefined(item.laborRate))
								delete item.laborRate;
						}
						selectedPacks.push(item);
					}
				}
			});
			_.forInRight(allPackings, function (item, i) {
				if (angular.isDefined(item.showInContract)) {
					if (item.showInContract)
						allPackings.splice(i, 1);
				}
			});
			angular.forEach(selectedPacks, function (fromAll, ii) {
				selectedPacks[ii].quantity = 0;
				if (initial_selects.length) {
					var ind = _.findIndex(initial_selects, {name: fromAll.name});
					if (ind != -1) {
						selectedPacks[ii].quantity += Number(initial_selects[ind].quantity);
						selectedPacks[ii].rate = 0;
						if (angular.isDefined(initial_selects[ind].rate))
							selectedPacks[ii].rate = initial_selects[ind].rate;
						selectedPacks[ii].LDRate = 0;
						if (angular.isDefined(initial_selects[ind].LDRate))
							selectedPacks[ii].LDRate = initial_selects[ind].LDRate;
						selectedPacks[ii].laborRate = 0;
						if (angular.isDefined(initial_selects[ind].laborRate))
							selectedPacks[ii].laborRate = initial_selects[ind].laborRate;
					}
				}
			});
			//for custom packing
			_.forEach(initial_selects, function (packing) {
				if (packing.custom) {
					selectedPacks.push(packing);
				}
			})
			if (initial_selects.length) {
				_.forInRight(allPackings, function (allPack, i) {
					angular.forEach(initial_selects, function (initPack, j) {
						if (allPack.name == initPack.name) {
							allPack.quantity = initPack.quantity;
							selectedPacks.push(allPack);
							allPackings.splice(i, 1);
						}
					});
				});
			}
			scope.packing = allPackings;
			scope.extra.selectedPackings = selectedPacks;
		}
	}
}
