angular.module('contractModule')
    .directive("signatureModal", signatureModal);

signatureModal.$inject = ['ContractFactory'];

function signatureModal(ContractFactory) {
    return {
        restrict: "E",
		scope: {
			signatureType: '@',
			saveStep: '&'
		},
        templateUrl: 'app/contractpage/templates/signatureModal.html',
        link: function(scope) {
            scope.cancel = cancel;
            scope.count = 1;
            let canvas = angular.element(`#${scope.signatureType}Canvas`);

            window.addEventListener('resize', resizeCanvas, false);

            function resizeCanvas() {
                if(scope.count){
                    canvas.width = window.innerWidth;
                    canvas.height = window.innerHeight - 400;
                    scope.count--;
                }
            }
            resizeCanvas();

            function cancel () {
                ContractFactory.clearSignaturePad();
            }
        }
    };
}
