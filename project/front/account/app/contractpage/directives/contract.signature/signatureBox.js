angular.module('contractModule')
    .directive("signatureBox", signatureBox);

signatureBox.$inject = ['ContractFactory', '$window', 'SweetAlert', '$rootScope', 'ContractValue'];

function signatureBox(ContractFactory, $window, SweetAlert, $rootScope, ContractValue) {
    return {
        restrict: "EA",
        scope: {
            stepId: "@",
            owner: "@",
            admin: '='
        },
        template: ['<div class="signatured-box" style="cursor:default;" ng-if="data.signatures[stepId].value">',
                '<img disabled class="col-dd-3" ng-src="{{ data.signatures[stepId].value }}" />',
                '<div><span class="sign-date">{{:: data.signatures[stepId].date | date: "medium"}}</span>',
                '<a class="sign-remove-signature" href="javascript:;" ng-show="showCancel()" ng-click="removeSignature()"><span class="cancel" > &times;</span></a></div>',
                '</div>',
                '<div class="signature-box" id="step_{{stepId}}" ng-click="clickOpenSignature()" ng-if="!data.signatures[stepId].value">',
                '<div class="empty-signature"><i class="glyphicon glyphicon-pencil"></i> Click here to sign</div>',
                '<div id="errorSign_{{ stepId }}" style="display:none;"><i class="fa fa-arrow-left" aria-hidden="true"></i></div>',
                '</div>'
        ].join(''),
        link: linkFn
    };

    function linkFn(scope) {
        var stepsData = ContractFactory.stepsData;
        var flatStepsData = ContractFactory.flatStepsData;
        scope.stepsData = ContractFactory.stepsData;
        scope.flatStepsData = ContractFactory.flatStepsData;
        scope.basicSettings = scope.$parent.basicSettings;
        scope.contract_page = scope.$parent.contract_page;
        var $super = scope.$parent;
        scope.data = scope.$parent.data;

        let type = scope.stepId.slice(0, -10);

        function _setStepError(nextStep) {
            if(nextStep == 'checkout_btn'){
                $('#errorSign_'+ nextStep).addClass('errorArrow');
                var el = $('.' + nextStep);
                el.addClass('error');
                if(!scope.contract_page.lessInitialContract)
                    $('html, body').animate({scrollTop: el.offset().top}, 1000);
                $('#signaturePad').modal('hide');
                return false;
            }
            $('#errorSign_'+ nextStep).addClass('errorArrow');
            var el = $('#step_' + nextStep);
            el.addClass('error');
            if(el.find('select'))
                el.find('select').addClass('error');
            $('html, body').animate({
                scrollTop: el.offset().top
            }, 1000);
            $('#signaturePad').modal('hide');
            return false;
        }

        function changeModel() {
            if (~scope.stepId.search('custom')) {
                scope.data = {
                    signatures: scope.$parent.$parent.data.customContractSignature[type]
                };
            } else if(~scope.stepId.search('signature')) {
                scope.data = {
                    signatures: scope.$parent.contractPage
                };
            }
        }
        changeModel();

        scope.clickOpenSignature = function () {
            if (_.get(scope, '$parent.contractPage.pdfData.id')) {
                return false;
            }

            if(~scope.stepId.search('custom')) {
                if(!ContractFactory.factory.customContractSignature[type]) {
                    ContractFactory.factory.customContractSignature[type] = {};
                }
                ContractFactory.factory.customContractSignature[type][scope.stepId] = {};
                ContractFactory.factory.customContractSignature.currentCustomType = type;
                ContractFactory.factory.customContractSignature.currentSignature = scope.stepId;
                ContractFactory.openSignaturePad();
                return false;
            }

            if(scope.stepId.search('signature_field') >= 0){
                ContractFactory.factory.additionalContractPagesSignature = scope.stepId.split("_")[2];
                ContractFactory.openSignaturePad();
                return false;
            }

            scope.data = scope.$parent.data;
            scope.maxSignCount = _.clone(scope.$parent.maxSignCount);
            if(scope.stepId == scope.$parent.maxSignCount - 2) {
                if(!scope.$parent.request.field_flat_rate_local_move.value && scope.$parent.request.service_type.raw == 5 && !scope.data.isPickupSubmitted){
                    SweetAlert.swal("Failed!", "Please submit pickup", "error");
                    return false;
                }
            }
            scope.$parent.data.currentStep = scope.stepId;
            var stepId = Number(scope.data.currentStep);
            if (isNaN(stepId)) return false;
            setTimeout(function(){
                var ln = scope.data.signatures.length;
                var nextStep = ln;
                if(scope.contract_page.lessInitialContract && nextStep == scope.maxSignCount - 2){
                    ContractFactory.openSignaturePad(scope.owner);
                    return false;
                }
                if((nextStep == scope.maxSignCount - 2 || nextStep == scope.maxSignCount - 1) && scope.$parent.totalLessDeposit() != 0 && !scope.admin){
                    SweetAlert.swal("Failed!", "Total less deposit received isn't 0", "error");
                    return false;
                }

                if (scope.data.signatures.hasOwnProperty(scope.stepId)) {
                    return false;
                }

                if (ln) { //Check validation
                    for(var i=0; i<ln; i++){
                        var arrow = $('#errorSign_'+ i);
                        var el = $('#step_' + i);
                        if (el.hasClass('error')) {
                            el.removeClass('error');
                        }
                        if (arrow.hasClass('errorArrow')) {
                            arrow.removeClass('errorArrow');
                        }
                    }
                    var btnPay = $('.checkout_btn');
                    if (btnPay.hasClass('error')) {
                        btnPay.removeClass('error');
                    }
                    var btnPayArrow = $('#errorSign_checkout_btn');
                    if (btnPayArrow.hasClass('errorArrow')) {
                        btnPayArrow.removeClass('errorArrow');
                    }
                    var declaration = $('#step_declaration');
                    if (declaration.hasClass('error')) {
                        declaration.removeClass('error');
                        if(declaration.find('select') && declaration.find('select').hasClass('error'))
                            declaration.find('select').removeClass('error');
                    }
                    var declarationArrow = $('#errorSign_declaration');
                    if (declarationArrow.hasClass('errorArrow')) {
                        declarationArrow.removeClass('errorArrow');
                    }
                    if(ln == scope.maxSignCount - 2 && scope.$parent.totalLessDeposit() != 0 && !scope.admin){
                        _setStepError('checkout_btn');
                    }
                    var access = angular.isDefined($super.request.request_all_data.useInventory) ? $super.request.request_all_data.useInventory : scope.contract_page.useInventory;
                    if (stepId == 4 && access) {
                        if(($super.ifStorage && !$super.data.inventoryMoving.isSubmitted && !_.isEmpty($super.request.inventory)) || ($super.ifStorage && $super.data.inventoryMoving.isSubmitted && !$super.data.addInventoryMoving.isSubmitted && $super.data.addInventoryMoving.open) || ($super.ifStorage && _.isEmpty($super.request.inventory) && !$super.data.addInventoryMoving.isSubmitted)) {
                            SweetAlert.swal({
                                    title: "Failed!",
                                    text: "Submit Inventory before you sign",
                                    type:"error"},
                                function () {
                                    if($super.ifStorage && !$super.data.inventoryMoving.isSubmitted && !_.isEmpty($super.request.inventory) || (!$super.ifStorage && !$super.data.inventoryMoving.isSubmitted)) $super.navigation.active = _.indexOf($super.navigation.pages, 'Inventory');
                                    if(($super.ifStorage && $super.data.inventoryMoving.isSubmitted && $super.data.addInventoryMoving.open) || ($super.ifStorage && _.isEmpty($super.request.inventory) && !$super.data.addInventoryMoving.isSubmitted) ) $super.navigation.active = _.indexOf($super.navigation.pages, 'Add. Inventory');
                                    $('body').scrollTop(0);
                                }
                            );
                            return false;
                        }
                    }

                    var c = 0, keepGoing = true;
                    angular.forEach (scope.data.signatures, function (sign, index) {
                        if (keepGoing) {
                            if (index != c) {
                                nextStep = c;
                                keepGoing = false;
                            }
                            c++;
                        }
                    });
                    if (nextStep != stepId) {
                        return _setStepError(nextStep);
                    }
                    else {
                        var $sbox = $('.signature-box');
                        if ($sbox.hasClass('error')) {
                            $sbox.removeClass('error');
                            var arrow = $('#errorSign_'+ nextStep);
                            if(arrow.hasClass('errorArrow'))
                                arrow.removeClass('errorArrow');
                        }
                    }
                }
                else if (!ln && stepId != 0){
                    return _setStepError(nextStep);
                }
                if (stepId == 2) {
                    if (scope.data.declarationValue.selected.length == 0 && _.findIndex(scope.contract_page.declarations, {show: true}) > -1) {
                        return _setStepError('declaration');
                    }else{
                        var $declaration = $('.contract li select');
                        if ($declaration.hasClass('error')) {
                            $declaration.removeClass('error');
                            var arrow = $('#errorSign_'+ nextStep);
                            if(arrow.hasClass('errorArrow'))
                                arrow.removeClass('errorArrow');
                        }
                    }
                }
                ContractFactory.openSignaturePad(scope.owner);
            }, 350);
        };

        scope.showCancel = function () {
            if (~scope.stepId.search('custom')) {
                return !scope.data.signatures.isSubmitted;
            }
            if (~scope.stepId.search('signature_field')) {
                return !scope.data.signatures.isSubmitted;
            }
            if ($super.request.service_type.raw == 5) {
                if ($super.request.field_flat_rate_local_move.value) {
                    if (checkFirstGroupContractSignature() && scope.data.isSubmitted) return false;
                } else {
                    if (checkFirstGroupContractSignature() && scope.data.isPickupSubmitted) return false;

                    if ((scope.stepId == 5 || scope.stepId == 6) && scope.data.isPickupSubmitted && scope.data.isSubmitted) return false;
                }
                return true;
            } else {
                if (!scope.data.isSubmitted && (scope.stepId == stepsData.startTimeStep || scope.stepId == stepsData.stopTimeStep ||
					scope.stepId == flatStepsData.pickupStartTimeStep || scope.stepId == flatStepsData.pickupStopTimeStep ||
					scope.stepId == flatStepsData.unloadStartTimeStep || scope.stepId == flatStepsData.unloadStopTimeStep) && ContractFactory.permission) {
                    return true;
                }
            }
        };

        scope.removeSignature = function() {
            if(!ContractFactory.permission || ContractValue.isFamiliarization || ContractValue.isPrint) {
            	return false;
			}

            SweetAlert.swal({
                    title: "Are you sure ? ",
                    text: 'After cancel signature will be removed',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55", confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        if(~scope.stepId.search('custom')){
                            scope.data.signatures[scope.stepId] = {};
                            $rootScope.$broadcast('signature.removed');
                        } else if(~scope.stepId.search('signature_field')){
                            scope.data.signatures[scope.stepId] = {};
                            $rootScope.$broadcast('signature.removed');
                        }else {
                            scope.data = ContractFactory.removeSignature(scope.stepId, scope.$parent.request.nid, scope.$parent.request.service_type.raw, scope.$parent.flatMiles);
                            var activeC = scope.data.crews[scope.$parent.navigation.activeCrew];
                            scope.$root.$broadcast('resetCalc', activeC);
                            scope.$root.$broadcast('init.contract.tour');
                        }
                    }
                }
            );
        };

        function checkFirstGroupContractSignature() {
        	if (scope.stepId == 0 || scope.stepId == 1 || scope.stepId == 2 ||
				scope.stepId == 3 || scope.stepId == 4) return true;
		}
    }

}
