angular
    .module('contractModule')
    .directive("customContent", customContent);
    customContent.$inject = ['$compile', 'ContractFactory'];

    function customContent($compile, ContractFactory) {
        return {
            link: link,
            transclude: 'true',
            scope: {
                content: '=',
                container: '@',
                isSubmitted: '<'
            },
            restrict: 'EA'
        };

        function link(scope, element) {
            scope.isAdminManager = ContractFactory.isAdminManager;

            let settingsMenu = {
                '%simple_input%': `<input type="text" ng-model="" ng-disabled="" id="" class="input-md">`,
                '%signature_field%': `<signature-box admin="isAdminManager" step-id=""></signature-box>`,
                '%textarea_field%': `<div class="row">
                                        <div class="bottom-margin">
                                        <textarea auto-resize-texarea 
                                                  rows="5" 
                                                  class="form-control" 
                                                  ng-model="" 
                                                  ng-disabled="" 
                                                  placeholder="Enter description">
                                                  </textarea>
                                        </div>
                                    </div>`,
                '%checkbox%': `<input type="checkbox" ng-model="" ichecks ng-disabled="" class="input-md">`,
                '%current_date%': moment().format('MM/DD/YYYY')
            };

            if(!_.isUndefined(scope.content)) {
                element.html(buildHtml(scope.content)).show();
                $compile(element.contents())(scope);
            }

            function buildHtml(content) {
                let nameModel = '';
                let html = '';
                let regCase = new RegExp('\%(.*?)\%', 'g');
                let regTags = new RegExp(/(<([^>]+)>)/, 'g');
                let keys = content.match(regCase);

                if (keys) {
                    _.forEach(settingsMenu, function (item, field) {
                        let key = field;
                        nameModel = key.replace(/%/g, '');
                        html = getTemplate(key);
                        let templateIndex = 0;
                        if (item && _.isString(item) && item.match(regTags) && item.match(regTags).length) {
                            while (content.search(key) != -1) {
                                let searchInd = content.search(key);
                                let endModelName = `_${templateIndex}`;
                                let template = html.replace('ng-model=""', `ng-model="${scope.container}.${nameModel}${endModelName}"`);
                                template = template.replace('ng-value=""', `ng-value="${scope.container}.${nameModel}${endModelName}"`);
                                template = template.replace('ng-disabled=""', `ng-disabled="${scope.container}.isSubmitted"`);
                                template = template.replace('ng-class=""', `ng-class="{disabled:${scope.container}.isSubmitted}"`);
                                template = template.replace('step-id=""', 'step-id="' + scope.container + 'signature' + templateIndex + '"');
                                template = template.replace('id=""', 'id="' + nameModel + '"');
                                template = template.replace('ng-form=""', 'ng-form="' + nameModel + '_' + templateIndex + '"');
                                template = template.replace('ngForm', nameModel + '_' + templateIndex);
                                content = replaceStringAtPosition(content, key, template, searchInd);
                                templateIndex++;
                            }
                        } else {
                            content = content.replace(RegExp(key, 'g'), html);
                        }
                    });
                }
                return content;
            }

            function getTemplate(contentType) {
                if (settingsMenu.hasOwnProperty(contentType)) {
                    return settingsMenu[contentType];
                }
            }

            function replaceStringAtPosition(originalString, sourceString, newString, position) {
                let linkLength = sourceString.length;
                let prefix = originalString.substring(0, position);
                let afterStartPosition = position + linkLength;
                let postfix = originalString.substring(afterStartPosition, originalString.length);

                return prefix + newString + postfix;
            }
        }
    }
