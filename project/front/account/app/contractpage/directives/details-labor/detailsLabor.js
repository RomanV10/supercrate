'use strict';

angular.module('contractModule')
	.directive('detailsLabor', detailsLabor);

	/*@ngInject*/
	function detailsLabor(ContractCalc, ContractFactory, RequestServices, CalculatorServices, SweetAlert,
						  ContractService, datacontext, ContractValue, $filter, laborService, accountConstant) {
		return {
			restrict: 'EA',
			scope: {
				data: '=',
				request: '=',
				request_data: '=requestData',
				navigation: '=',
				flatMiles: '<',
				contract_page: '<contractPage',
				customer: '=',
				isDoubleDriveTime: '<',
				travelTime: '=',
				travelTimeSetting: '<',
				busy: '=',
				maxSignCount: '=',
				saveReqDataInvoice: '&',
				saveContract: '&',
				pickupTimer: '&',
				unloadTimer: '&',
				startTimer: '&',
				stopTimer: '&',
				setLessSignatures: '&'
			},
			templateUrl: 'app/contractpage/directives/details-labor/detailsLabor.html',
			link: link,
		};

		function link(scope) {
			let indexFirstCrew = 0;

			scope.ctrlRate = 0;
			scope.foreman_rate = 0;
			scope.contract_rate = $filter('range')([], ContractValue.foremanMaxRateBefore);

			scope.calculation = {
				calculateWork: ContractCalc.calculateWork,
				totalHourlyCharge: ContractCalc.totalHourlyCharge,
				canCalc: ContractCalc.canCalc,
				getCrewRate: laborService.getCrewRate,
				getCrewTotalDuration: ContractCalc.getCrewTotalDuration
			};

			scope.fieldData = datacontext.getFieldData();
			scope.calcSettings = angular.fromJson(scope.fieldData.calcsettings);
			scope.isAdminManager = ContractFactory.isAdminManager;

			scope.changeRate = changeRate;
			scope.getTravelRate = getTravelRate;
			scope.disableTime = disableTime;
			scope.canAddCrew = canAddCrew;
			scope.addCrew = addCrew;
			scope.changeCrewTime = changeCrewTime;
			scope.getMovers = getMovers;
			scope.getTrucks = getTrucks;
			scope.getTotalByCrew = getTotalByCrew;

			function changeRate() {
				if (scope.foreman_rate != scope.ctrlRate) {
					scope.ctrlRate = scope.foreman_rate - scope.ctrlRate;
					scope.data.crews[indexFirstCrew].rate = _.round(scope.data.crews[indexFirstCrew].rate, 2) + _.round(scope.ctrlRate, 2);
					scope.request.request_all_data.invoice.rate.value = parseFloat(scope.request.request_all_data.invoice.rate.value) + parseFloat(scope.ctrlRate);
					scope.request.request_all_data.invoice.rate.old = parseFloat(scope.request.request_all_data.invoice.rate.old) + parseFloat(scope.ctrlRate);
				}

				scope.ctrlRate = scope.foreman_rate;
			};

			function getTravelRate() {
				let rate = _.get(scope.request, 'rate.value', 0);
				rate = _.get(scope.request, 'request_all_data.invoice.rate.value', rate);
				rate = _.get(scope.request, 'request_all_data.add_rate_discount', rate);

				if (scope.data.crews.length) {
					rate = scope.calculation.getCrewRate(scope.data.crews.length - 1, scope.data.crews, _.get(scope.request_data, 'crews.additionalCrews'));
				}

				return rate;
			};

			function disableTime(step, index){
				if(scope.contract_page.lessInitialContract){
					return scope.data.isSubmitted;
				}else{
					if(scope.data.signatures[step]){
						if(scope.data.crews.length > 1 && scope.data.signatures[step+scope.data.stepSpace]){
							return (scope.data.isSubmitted || (index == 0 ? !!scope.data.signatures[step].value : !! scope.data.signatures[step+scope.data.stepSpace].value));
						}
						return (scope.data.isSubmitted || (index == 0 ? !!scope.data.signatures[step].value : !! scope.data.signatures[step+scope.data.stepSpace]));
					}else{
						return scope.data.isSubmitted;
					}
				}
			};

			function canAddCrew(index) {
				if (scope.request_data && scope.request_data.crews) {
					return angular.isDefined(index) && !scope.data.isSubmitted && ( ((_.last(scope.data.crews)).timer.start &&
						(_.last(scope.data.crews)).timer.stop) || ((_.last(scope.data.crews)).timer.startPickUp &&
						(_.last(scope.data.crews)).timer.stopPickUp && (_.last(scope.data.crews)).timer.startUnload &&
						(_.last(scope.data.crews)).timer.stopUnload)) && scope.request_data.crews.additionalCrews.length >=  scope.data.crews.length
				}
			};

			function addCrew() {
				scope.busy = true;
				RequestServices.getRequest(scope.request.nid)
					.then((data) => {
						scope.busy = false;
						scope.request_data = data.request_data.value;
						if (!scope.request_data.crews.additionalCrews.length) {
							return false;
						}
						let i = scope.data.crews.push({
								timer: {
									start: 0,
									stop: 0,
									timeOff: 0,
									lastAction: '',
									startPickUp: 0,
									stopPickUp: 0,
									startUnload: 0,
									stopUnload: 0,
								},
								workDuration: 0,
								rate: 0
							});

						scope.maxSignCount += 2;
						scope.data.stepSpace += 2;
						if (scope.flatMiles) {
							scope.data.stepSpace += 2;
							scope.maxSignCount += 2;
						}

						let index = i - 2; //into request_data.additional Crews, base crew not exist

						scope.data.crews[i - 1].timer.start = scope.data.crews[index].timer.stop;

						let crew = scope.request_data.crews.additionalCrews[index];
						let rate = crew.rate;
						if (!rate) {
							rate = CalculatorServices.getRateAdditionalHelper(scope.request.date.raw * 1000, crew);
						}
						scope.data.crews[i - 1].rate = rate;
						scope.navigation.activeCrew = i - 1;
						setTimeout(function () {
							if (scope.contract_page.lessInitialContract) {
								let arr = {};
								if (!scope.flatMiles) {
									arr = ContractFactory.stepsData;
								} else {
									arr = ContractFactory.flatStepsData;
								}
								scope.setLessSignatures({arr: arr});
								scope.saveContract();
							}
						}, 300);
					})
					.catch((err) => SweetAlert.swal('Failed!', 'Error of getting request data', 'error'));
			};

			function changeCrewTime(crewNumber, timeType, value) {
				let msgToLog = '';
				if(timeType == 'start'){
					value = $('#edit-start-time-'+(crewNumber-1)).val();
					msgToLog += timeType.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})+ ' time ';
					msgToLog += 'crew #'+ crewNumber;
				}else if(timeType == 'end'){
					value = $('#edit-end-time-'+(crewNumber-1)).val();
					msgToLog += timeType.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})+ ' time ';
					msgToLog += 'crew #'+ crewNumber;
				}else if(timeType == 'travel'){
					if(scope.calcSettings.doubleDriveTime) timeType = 'Double Drive Time';
					value = $('#edit-travel').val();
					msgToLog += timeType.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})+ ' time ';
				}
				else if(timeType == 'off'){
					value = $('#edit-time-off-'+(crewNumber-1)).val();
					msgToLog += 'Time ' + timeType + ' ';
					msgToLog += 'crew #'+ crewNumber;
				}else if(timeType == 'startPickUp'){
					value = $('#edit-start-pickup-time-'+(crewNumber-1)).val();
					msgToLog += 'Pickup start'+ ' time ';
					msgToLog += 'crew #'+ crewNumber;
				}else if(timeType == 'stopPickUp'){
					value = $('#edit-end-pickup-time-'+(crewNumber-1)).val();
					msgToLog += 'Pickup stop'+ ' time ';
					msgToLog += 'crew #'+ crewNumber;
				}else if(timeType == 'startUnload'){
					value = $('#edit-start-unload-time-'+(crewNumber-1)).val();
					msgToLog += 'Unloading start'+ ' time ';
					msgToLog += 'crew #'+ crewNumber;
				}else if(timeType == 'stopUnload'){
					value = $('#edit-end-unload-time-'+(crewNumber-1)).val();
					msgToLog += 'Unloading stop'+ ' time ';
					msgToLog += 'crew #'+ crewNumber;
				}
				if(angular.isDefined(timeType) && angular.isDefined(value)){
					msgToLog += ' changed to ' + value;
				}
				if(msgToLog != '')
					ContractService.addForemanLog(msgToLog,'Contract changed', scope.request.nid, 'MOVEREQUEST');
				if(scope.contract_page.lessInitialContract){
					if(!scope.flatMiles){
						scope.startTimer();
						scope.stopTimer();
					}else{
						scope.pickupTimer({type: 'start'});
						scope.pickupTimer({type: 'end'});
						scope.unloadTimer({type: 'start'});
						scope.unloadTimer({type: 'end'});
					}
					scope.saveContract();
				}
			};

			function getTrucks(index) {
				if (!scope.request_data || !(Object.keys(scope.request_data)).length || !scope.request_data.crews) {
					return 0;
				}

				let trucks = 0;

				while (index >= 0) {
					trucks += Number(getTrucksCrew(index));
					index--;
				}

				if (!trucks) {
					if (scope.data.crews) {
						_.forEach(scope.data.crews, function(crew, ind) {
							if (ind == 0) {
								trucks = scope.request.trucks.raw.length;
							} else if (ind > 0 && scope.request_data.crews.additionalCrews) {
								trucks += Number(scope.request_data.crews.additionalCrews[ind - 1].trucks);
							}
						});
					}
				}

				return trucks;
			}

			function getTrucksCrew(index) {
				let total = 0;

				if (index == 0) {
					total = scope.request.trucks.raw.length;
				} else if (index > 0 && scope.request_data.crews.additionalCrews) {
					total = scope.request_data.crews.additionalCrews[index - 1].trucks;
				}

				return total;
			};

			function getMovers(index) {
				if (!scope.request_data || !(Object.keys(scope.request_data)).length || !scope.request_data.crews) {
					return 0;
				}

				let movers = 0;

				while (index >= 0) {
					movers += getMoversCrew(index);
					index--;
				}

				if (!movers) {
					if (scope.data.crews) {
						_.forEach(scope.data.crews, function(crew, ind) {
							if (ind == 0 && scope.request_data.crews.baseCrew) {
								movers = scope.request_data.crews.baseCrew.helpers.length + 1;
							} else if (ind > 0 && scope.request_data.crews.additionalCrews) {
								movers += scope.request_data.crews.additionalCrews[ind - 1].helpers.length;
							}
						});
					}
				}

				return movers;
			};

			function getMoversCrew(index) {
				let total = 0;

				if (index == 0 && scope.request_data.crews.baseCrew) {
					total = scope.request_data.crews.baseCrew.helpers.length + 1;
				} else if (index > 0 && scope.request_data.crews.additionalCrews) {
					total = scope.request_data.crews.additionalCrews[index - 1].helpers.length;
				}

				return total;
			};

			function getTotalByCrew(index) {
				let total = scope.calculation.getCrewRate(index, scope.data.crews, _.get(scope.request_data, 'crews.additionalCrews')) *
					scope.data.crews[index].workDuration;

				return _.round(total, accountConstant.roundUp);
			}

		};
	};
