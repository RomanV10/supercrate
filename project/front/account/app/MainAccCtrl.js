(function() {
    'use strict';

    angular
        .module('app')
        .controller('MainAccCtrl', MainAccCtrl);

    MainAccCtrl.$inject = ['$scope','$location', '$rootScope', 'ContractValue'];

    function MainAccCtrl($scope, $location, $rootScope, ContractValue) {
        var contractStr = '/contract';
        var couponStr = '/coupon';
        $scope.isContractPage = false;
        $scope.isCouponPage = false;
        var urlName = $location.path();
        var res = urlName.search(contractStr);
        $scope.isContractPage = res >= 0;
        var resCoupon = urlName.search(couponStr);
        $scope.isCouponPage = resCoupon >= 0;

        $rootScope.$on('$locationChangeSuccess', function (event) {
            var urlName = $location.path();
            var res = urlName.search(contractStr);
            $scope.isContractPage = (res >= 0);
			ContractValue.isFamiliarization = (~urlName.search('main_page_read') || ~urlName.search('proposal_additional_pages_read')) ? true : false;
			ContractValue.isPrint = ~urlName.search('print') ? true : false;
            var resCoupon = urlName.search(couponStr);
            $scope.isCouponPage = resCoupon >= 0;
        });
    }

})();
