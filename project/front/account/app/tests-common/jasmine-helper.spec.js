// This will run before any it function.
// Resetting a global state so the change in this function is testable

beforeEach(module('app'));
beforeEach(module('ngMockE2E'));

var $rootScope;
var $compile;
var testHelper;
var $httpBackend;

beforeEach(inject(function (_$rootScope_, _$compile_, _testHelperService_, _$httpBackend_) {
	$rootScope = _$rootScope_;
	$compile = _$compile_;
	$httpBackend = _$httpBackend_;
	testHelper = _testHelperService_;
	let getFlagsResponce = testHelper.loadJsonFile('get-flags.mock');
	let frontpageResponse = testHelper.loadJsonFile('frontpage.mock');
	let get_valuation_plan_mock = testHelper.loadJsonFile('account_get_valuation_plan.mock');
	$rootScope.fieldData = frontpageResponse;
	$rootScope.fieldData.valuation_plan_tables = get_valuation_plan_mock;
	$httpBackend.whenPOST((url) => {
		return url.indexOf('server/move_request/get_flags') != -1
	}).respond(200, getFlagsResponce);
}));
