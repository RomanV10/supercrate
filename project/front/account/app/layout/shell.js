'use strict';

angular
	.module('app.layout')
	.controller('Shell', Shell);

/* @ngInject */
function Shell($injector, $rootScope, $route, $location, $q, datacontext, common, config, AuthenticationService,
	CalculatorServices, AUTH_EVENTS, SweetAlert, ContractValue, $window) {
	/*jshint validthis: true */
	var vm = this;
	var CONTRACT_PAGES = 'contractpage';
	var currentLogin = localStorage.getItem('currentLogin');
	const LOGIN_URL = '/login';
	const HISTORY_URL = '/history';
	let isAutoLoginNow = false;

	vm.busyMessage = 'Please wait ...';
	vm.showSplash = true;
	vm.needReload = true;
	vm.showMain = false;
	vm.logoUrl = config.logoUrl;
	vm.progressLoading = 0;
	vm.currentUser = {};
	vm.Logout = logout;
	vm.displayFooter = displayFooter;
	let isFamiliarization = ContractValue.isFamiliarization;
	let isPrint = ContractValue.isPrint;

	function displayFooter() {
		if (isFamiliarization) {
			return !vm.isContractPage && isPrint;
		} else if (isPrint) {
			return !vm.isContractPage && isFamiliarization;
		} else {
			return !vm.isContractPage;
		}
	}

	window.addEventListener('storage', function () {
		if (currentLogin != localStorage.getItem('currentLogin') && vm.needReload) {
			window.location.reload();
		} else {
			vm.needReload = true;
		}
	});

	vm.isContractPage = (function () {
		return CONTRACT_PAGES == _.get($route, 'current.title');
	})();

	$rootScope.$on('$routeChangeStart', function (e, next) {
		vm.isContractPage = (function () {
			return CONTRACT_PAGES == _.get(next, '$$route.title');
		})();
	});

	vm.login = false;
	var location = $location.path();

	if (location == LOGIN_URL) {
		vm.login = true;
	}

	autoLogin();

	$rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
		activate();
		vm.login = false;
	});

	$rootScope.$on('autoLogin', function () {
		if (!isAutoLoginNow) {
			autoLogin();
		}
	});

	function autoLogin() {
		let path = $location.path();
		isAutoLoginNow = true;
		AuthenticationService.autoLogin($route.current.params)
			.then((result) => {
				if (result == 3) {
					SweetAlert.swal('Your AutoLogin Link Expired', 'We just send you a new one! Please check your email.', 'error');
				}

				activate(path);
				$location.url(path);
			}, () => {
				activate();
			})
			.finally(() => {isAutoLoginNow = false;});
	}

	function activate(path) {
		var log = {
			'request': '',
			'browser': AuthenticationService.get_browser(),
			'message_type': 'User Login',
			'message': 'User Login Initial',
			'type': 'Account Page'

		};

		var promise = datacontext.retreaveFieldsData();
		var promise2 = AuthenticationService.GetCurrent();

		$q.all([promise, promise2]).then(function (data) {
			//$scope.setCurrentUser(data.user);
			CalculatorServices.init(data[0]);
			hideSplash();

			log.request = AuthenticationService.getUser();
			vm.fieldData = datacontext.getFieldData();
			vm.basicSettings = angular.fromJson(vm.fieldData.basicsettings);

			if (path == LOGIN_URL || path == HISTORY_URL) {
				$location.path(HISTORY_URL);
			} else if (path) {
				$location.path(path);
			} else if (location == LOGIN_URL || location == HISTORY_URL) {
				$location.path(HISTORY_URL);
			} else {
				$location.path(location);
			}

		}, function (reason) {
			// If faild go to login page
			hideSplash();

			var ifInvoicePage = (location.indexOf('invoice') !== -1);
			var ifCouponPage = (location.indexOf('coupon') !== -1);

			if (!ifInvoicePage && !ifCouponPage) {
				log.request = AuthenticationService.getUser();
				$location.path(LOGIN_URL);
				vm.login = true;
			}
		});
	}

	function hideSplash() {
		vm.showSplash = false;

		common.$timeout(function () {
			vm.showMain = true;
		}, 800);
	}

	function logout() {
		$injector.get('sharedSockets').disconnect();
		$location.path(LOGIN_URL);
		AuthenticationService.Logout()
			.then(function (data) {
				vm.needReload = false;
				localStorage.setItem('currentLogin', (new Date()).getTime());
			});
	}
}
