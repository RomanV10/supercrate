(function () {
    'use strict';

    angular.module('app.layout', [])
        .config(function($httpProvider) {
            $httpProvider.interceptors.push(function () {
                return {
                    'request': function (config) {
                        config.url = addTimeVersionUrl(config.url);
                        return config;
                    }
                };
            });

            function addTimeVersionUrl(url) {
                // only do for html templates of this app
                // NOTE: the path to test for is app dependent!
                var isNotContainApp = url.indexOf('app/') < 0;
                var isNotContainDist = url.indexOf('dist/') < 0;
                var isNotContainCSSFolder = url.indexOf('css/') < 0;
                var isNotContainHTML = url.indexOf('.html') < 0;
                var isNotContainJS = url.indexOf('.js') < 0;
                var isNotContainCSS = url.indexOf('.css') < 0;

                if (!url
                    || (isNotContainApp && isNotContainDist && isNotContainCSSFolder)
                    || (isNotContainHTML && isNotContainJS && isNotContainCSS)) {

                    return url;
                }
                // create a URL param that changes every minute
                // and add it intelligently to the template's previous url
                var version = (isProduction) ? buildDate : ~~(Date.now() / 60000) % 10000;
                var param = 'v=' + version; // 4 unique digits every minute
                if (url.indexOf('?') > 0) {
                    if (url.indexOf('v=') > 0) return url.replace(/v=[0-9](4)/, param);
                    return url + '&' + param;
                }
                return url + '?' + param;
            }
        });

})();