(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('Sidebar', Sidebar);

    Sidebar.$inject = [
        '$rootScope','$scope','$location', '$route','routehelper','datacontext','MessagesServices'
    ];

    function Sidebar($rootScope,$scope,$location, $route,routehelper,datacontext,MessagesServices) {
        /*jshint validthis: true */
        var vm = this;
        vm.isCurrent = isCurrent;
        vm.routes = routehelper.getRoutes(); 
         vm.goToPage = goToPage;
         vm.unreadCount = 0;
        vm.services_type = [];
        vm.statuses = [];
        var promiseInit = datacontext.init();
            
             promiseInit.then(function(){ 
                 
                 $rootScope.$broadcast('data.init');
            
        });
        
        $rootScope.$on('data.init', function() {
             vm.services_type = datacontext.getFieldData().field_lists.field_move_service_type;
            vm.statuses = datacontext.getFieldData().field_lists.field_approve;
        });
        
        
        var promise = MessagesServices.getUnreadCount();

         promise.then(function(data) {
            vm.unreadCount = data[0];
         });
       
        function goToPage(url) {
            $location.url(url);
        }
        
          
        function isCurrent(route) {
            var menuName = route;
            return $route.current.title.substr(0, menuName.length) === menuName ? 'active' : '';
        }
      
     }
    
})();