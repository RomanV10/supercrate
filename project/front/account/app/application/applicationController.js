'use strict';

angular.module('app.core')
	.controller('ApplicationController', ApplicationController);

ApplicationController.$inject = ['$scope', '$state', 'config', 'USER_ROLES', 'AuthenticationService'];

function ApplicationController($scope, $state, config, USER_ROLES, AuthenticationService) {
	$scope.currentUser = null;
	$scope.userRoles = USER_ROLES;
	$scope.isAuthorized = AuthenticationService.isAuthorized;
	$scope.logoUrl = config.logoUrl;
	$scope.busy = true;

	var promise = AuthenticationService.GetCurrent();

	promise.then(function (data) {
		$scope.setCurrentUser(data.user);
		$scope.busy = false;

		$state.go('/');

	});

	$scope.setCurrentUser = function (user) {
		$scope.currentUser = user;
	};
}
