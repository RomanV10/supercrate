(function () {
    'use strict';

    angular
      .module('app.account')
      .controller('AccountController', AccountController);

    AccountController.$inject = [
      '$routeParams',
      'AuthenticationService'
    ];

    function AccountController ($routeParams, AuthenticationService) {
      AuthenticationService.getUniqueUser();
    }
})();
