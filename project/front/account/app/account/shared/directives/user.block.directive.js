(function () {
	'use strict';

	angular
	.module('app.account')
	.directive('ccUserBlock', ccUserBlock);

	function ccUserBlock($location, Session, $uibModal, ClientsServices) {
		var directive = {
			link: link,
			scope: {
				'client': '=',
				'user': '=',
				'subtitle': '=',
				'rightText': '@',
				'allowCollapse': '@',
				'companyName': '=?',
				'moveSize': '=?',
				'nid': '=?'
			},
			templateUrl: 'app/account/shared/directives/user-block.html?3',
			restrict: 'E'
		};
		return directive;


		function link(scope) {
			scope.hideHistory = false;

			if ($location.$$url == '/history') {
				scope.hideHistory = true;
			}

			scope.openModal = function () {

				var modalInstance = $uibModal.open({
					templateUrl: 'app/account/shared/directives/client-edit-form.html',
					controller: ModalInstanceCtrl,
					resolve: {
						client: function () {
							return scope.user;
						},
						companyName: function () {
							return scope.companyName;
						},
						moveSize: function () {
							return scope.moveSize;
						},
						nid: function () {
							return scope.nid;
						}
					},
				});

				modalInstance.result.then(function ($scope) {
					if ($scope.delete) {
						var row = $scope.client.uid;
						delete(vm.clients[row]);
					}
				});

			};

			/* @ngInject */
			function ModalInstanceCtrl($scope, $uibModalInstance, client, ClientsServices, logger, Session, companyName, moveSize, nid, RequestServices) {

				$scope.client = client;
				$scope.companyName = companyName;
				$scope.moveSize = moveSize;
				$scope.changeMail = false;
				let commercialRequest;
				_.each(client.nodes, function (node) {
					node = node
					//if request exist status confirmed or complite user can'not change email
					if (node.service_type.raw == 3 || node.service_type.raw == 8) {
						$scope.changeMail = true;
					}
					if (node.move_size.raw == 11) {
						commercialRequest = node;
					}

				});
				$scope.busy = false;
				$scope.delete = false;
				var session = Session.get();
				var requests = session.userId.nodes;


				$scope.update = function (client) {

					$scope.busy = true;

					var data = ClientsServices.prepareArray(client);
					var promise = ClientsServices.updateAllClientRequest(data);

					promise.then(function (data) {
						if (commercialRequest) {
							updateCompanyName(nid);
						}
						if (!data) {
							toastr.error('User with this email already used', 'Client info was not updated');
						}
						$scope.busy = false;
						$uibModalInstance.dismiss('cancel');

					}, function (reason) {
						toastr.error('User with this email already used', 'Client info was not updated');
						$scope.busy = false;
					});

				};

				$scope.cancel = function () {
					$uibModalInstance.dismiss('cancel');
				};

				$scope.delete = function (client) {
					$scope.busy = true;
					var promise = ClientsServices.deleteClient(client.uid);
					promise.then(function (data) {
						$scope.busy = false;
						$scope.delete = true;
						$uibModalInstance.close($scope);
					}, function (reason) {

					});
				};

				$scope.cancel = function () {
					$uibModalInstance.dismiss('cancel');
				};

				function updateCompanyName(nid) {
					RequestServices.updateRequest(nid, {
						field_commercial_company_name: $scope.companyName
					});
				}
			}
		}
	}
})();
