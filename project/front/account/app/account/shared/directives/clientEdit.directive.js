(function () {
    'use strict';

    angular
        .module('app.account')
        .directive('clientEditForm', clientEditForm);

    
    clientEditForm.$inject = ['logger','ClientsServices'];
    
    function clientEditForm (logger,ClientsServices) {
        var directive = {
            scope : {
                   'client': '=client',
            },
            controller: clientController,
            replace: true,
            templateUrl: 'app/account/shared/directives/client-edit-form.html?3',
            restrict: 'A'
        };
        return directive;
    
     
        function clientController($scope){
        
                $scope.client = $scope.client;
                $scope.busy = false;
                $scope.delete = false;




                $scope.update = function(client){

                     $scope.busy = true;
                     var data = ClientsServices.prepareArray(client);
                     var promise = ClientsServices.updateAllClientRequest(data);

                      promise.then(function(data) {
                          $scope.busy = true;
                      }, function(reason) {

                        });

                };

                $scope.delete = function(client) {

                     $scope.busy = true;
                     var promise = ClientsServices.deleteClient(client.uid);

                      promise.then(function(data) {

                          $scope.busy = false;
                          $scope.delete  = true;

                      }, function(reason) {



                        });
                };

        
                 
          
        }
        
    
    
    }
})();