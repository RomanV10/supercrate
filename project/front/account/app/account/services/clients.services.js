(function () {

  angular
    .module('app.account')

    .factory('ClientsServices', ClientsServices);

  ClientsServices.$inject = ['$http', '$rootScope', '$q', 'config', 'common'];

  function ClientsServices($http, $rootScope, $q, config, common) {

    var service = {};
    $rootScope.clients = {};
    service.getAllClients = getAllClients;
    service.saveClients = saveClients;
    service.getAllClientsStoragePages = getAllClientsStoragePages;
    service.getAllClientsStorage = getAllClientsStorage;
    service.updateClient = updateClient;
    service.deleteClient = deleteClient;
    service.prepareArray = prepareArray;
    service.getClient = getClient;
    service.updateAllClientRequest = updateAllClientRequest;
    return service;
    
    function updateAllClientRequest(data) {
	    let deferred = $q.defer();
	
	    $http
          .post(config.serverUrl + 'server/clients/update_client_all_requests', data)
          .then(resolve => {
		    deferred.resolve(resolve.data);
	    }, reject => {
		    deferred.reject(reject);
	    });
	
	    return deferred.promise;
    }




    function getClient(uid, callback) {


      $http
        .get(config.serverUrl + 'server/clients/' + uid)
        .success(function (data, status, headers, config) {

          deferred.resolve(data);

        })
        .error(function (data, status, headers, config) {

          // data.success = false;
          deferred.reject(data);

        });

      return deferred.promise;

    }

    function getAllClients(page, callback) {


      var deferred = $q.defer();
      var clients = service.getAllClientsStoragePages(page);

      if (angular.isDefined(clients)) {


        deferred.resolve(clients);

      }

      $http
        .get(config.serverUrl + 'server/clients?pagesize=20&page=' + page)
        .success(function (data, status, headers, config) {

          //data.success = true;
          service.saveClients(page, data);
          deferred.resolve(data);

        })
        .error(function (data, status, headers, config) {

          // data.success = false;
          deferred.reject(data);

        });

      return deferred.promise;

    }


    function updateClient(uid, data) {

      var deferred = $q.defer();

      $http.put(config.serverUrl + 'server/clients/' + uid, data)
        .success(function (data, status, headers, config) {


          deferred.resolve(data);

        })
        .error(function (data, status, headers, config) {


          deferred.reject(data);

        });

      return deferred.promise;


    }

    function deleteClient(uid) {

      var deferred = $q.defer();

      $http.delete(config.serverUrl + 'server/clients/' + uid)
        .success(function (data, status, headers, config) {


          deferred.resolve(data);

        })
        .error(function (data, status, headers, config) {


          deferred.reject(data);

        });

      return deferred.promise;


    }

    function prepareArray(client) {
	
      let data = {
        uid: client.uid,
        data: {
          data: {
            field_user_first_name: client.field_user_first_name,
            field_user_last_name: client.field_user_last_name,
            field_user_additional_phone: client.field_user_additional_phone,
            field_primary_phone: client.field_primary_phone,
            mail: client.mail,
            name: client.mail,
            status: client.status,
            pass: client.password
          }
        }
      };

      return data;

    }

    function saveClients(page, data) {

      if (angular.isUndefined($rootScope.clients[page])) {

        $rootScope.clients[page] = {};
      }
      $rootScope.clients[page] = data;


    }

    function getAllClientsStoragePages(page) {

      return $rootScope.clients[page];


    }

    function getAllClientsStorage() {

      return $rootScope.clients;


    }


  }
})();
