(function () {
    'use strict';

    angular.module('app.account')
        .run(routeConfig);

    routeConfig.$inject = ['routehelper'];
    /* @ngInject */
    function routeConfig(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/account/:id',
                config: {
                    templateUrl: 'app/account/areas/account_page/account.html',
                    title : 'account'
                },
               
            },
            {
                url: '/login',
                config: {
                    templateUrl: 'app/account/areas/login/login.html',
                    title : 'login'
                },

            },
            {
                url: '/restore',
                config: {
                    templateUrl: 'app/account/areas/login/restore.html',
                    title : 'Restore Password'
                },

            },
    
        ];
    }
})();