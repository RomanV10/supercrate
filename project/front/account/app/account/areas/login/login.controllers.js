(function () {
    'use strict';

    angular.module('app.core')


     .controller('LoginController', function ($scope,$rootScope,$location,AuthenticationService,AUTH_EVENTS,SweetAlert,$window) {

        $scope.email = '';
        $scope.password = '';
        $scope.busy = false;
        $scope.loading = false;
        var needReload = true;
        var currentLogin = localStorage.getItem('login');
         window.addEventListener('storage', function (e) {
           if (e.key === 'login') {
             if (currentLogin != localStorage.getItem('login') && needReload) {
               location.reload();
             } else {
               needReload = true;
             }
           }
         });
        $scope.login = function () {

            $scope.busy = true;
            $scope.loading = true;
            AuthenticationService.Login($scope.email, $scope.password, false, function (response) {
                if (response.success) {
                    $scope.busy = false;
                    $scope.loading = false;

                    needReload = false;
                    localStorage.setItem('login', (new Date()).getTime());

                     //redirect if formen or sales to admin section
	                let userRole = _.get($rootScope, 'currentUser.userRole', []);

                    if (~userRole.indexOf('foreman') || ~userRole.indexOf('sales')) {
                        $window.location.href = $window.location.origin + '/moveBoard/';
                    }

                    //redirect if formen or sales to admin section
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    $rootScope.login = false;
                } else {
                    $scope.busy = false;
                    $scope.error = response.message;
                  //  $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                    $scope.loading = false;

                }
            });
        };
            $scope.restorePassword = function () {

                $scope.busy = true;
                $scope.loading = true;
                AuthenticationService.request_new_password($scope.email, function (response) {
                    if (response.success) {

                        $scope.busy = false;
                        $scope.loading = false;
                        SweetAlert.swal("Success!", "Check your email", "success");

                    } else {
                        $scope.busy = false;
                        $scope.error = 'Your email address was not recognized.';
                        $scope.loading = false;
                    }
                });
            };


    });



})();
