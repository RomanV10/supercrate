(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('datacontext', datacontext);

    datacontext.$inject = ['$http','$q','$injector', '$rootScope', 'common', 'config'];

    function datacontext($http,$q,$injector, $rootScope,common,config) {
        //createFlags(['pickupSubmitted']);
        $rootScope.settings = {};
        $rootScope.availableFlags = {};

        getAvailableFlags();
/*

        $http.post(config.serverUrl+'server/move_request/remove_flag', {id: 13, type:'company_flags'})
        $http.post(config.serverUrl+'server/move_request/remove_flag', {id: 14, type:'company_flags'})
        $http.post(config.serverUrl+'server/move_request/remove_flag', {id: 15, type:'company_flags'})
        $http.post(config.serverUrl+'server/move_request/remove_flag', {id: 16, type:'company_flags'})
*/



        var repoNames = ['attendee', 'lookup', 'session', 'speaker'];
        var defaultFlags = ["teamAssigned", "ConfirmedbyCustomer", "Completed", "pickupSubmitted"];

        var service = {
            // functions
            init : init,
            retreaveFieldsData: retreaveFieldsData,
            saveFieldData :saveFieldData,
            getFieldData : getFieldData,
            getSettingsData : getSettingsData,
            getAvailableFlags : getAvailableFlags,
            createFlags : createFlags,
            getSettings: getSettings
        };

        return service;

        function init() {
            
            var deferred = $q.defer();
            var dataPromise = retreaveFieldsData();
            var settingsPromise = getSettings('calcsettings');
            var settingsPromise2 = getSettings('basicsettings');
            var settingsPromise3 = getSettings('longdistance');
           
            var promise4 = getTypes();    

            promise4.then(function(data) {
                
              var setting = {};
               setting.name = 'calendartype';
               saveSettingsData(data,setting.name);
                
            });
            
              $q.all([dataPromise, settingsPromise,settingsPromise2,settingsPromise3,promise4]).then(function(data){
                      deferred.resolve();
                });
            
             return deferred.promise;
            
        }
        
        function retreaveFieldsData() {
   
            var deferred = $q.defer(); 


            $http.post(config.serverUrl+'server/front/frontpage')
            .success(function(data, status, headers, config) {

                                    //data.success = true;
                                    
                                    service.saveFieldData(data);
                                    deferred.resolve(data);

            })
            .error(function(data, status, headers, config) {

                                    // data.success = false;
                                     deferred.reject(data);

             });
         
            return deferred.promise;

    
            

        }

        function getAvailableFlags() {

            var deferred = $q.defer();

            $http.post(config.serverUrl + 'server/move_request/get_flags')
                .success(function(data) {
                    if (_.isEmpty(data) || isNullFlags(data.company_flags)) {
                        createFlags(defaultFlags)
                            .then (function (data){
                                getAvailableFlags();
                            });
                    } else {
                        $rootScope.availableFlags.company_flags = _.invert(data.company_flags);
                    }
                    deferred.resolve(data);
                })
                .error(function(data, status, headers, config) {
                    deferred.reject(data);
                });

            return deferred.promise;
        }
        
        function isNullFlags(data) {
            if(data.length == 0) return true;  
            for(var key in data) {
                if(data[key] == null || data[key] == undefined) {
                    return true;
                }
            }
            return false;
        }

        function createFlags(flags) {
            /*var deferred = $q.defer();

            $http.post(config.serverUrl + 'server/move_request/create_flag', {
                    value: flags,
                    type: "company_flags"
                })
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(data, status, headers, config) {
                    deferred.reject(data);
                });

            return deferred.promise;*/
        }

        function getSettings(type,callback) {

        
            var deferred = $q.defer(); 

            var setting = {};
            setting.name = type;
 

            $http.post(config.serverUrl+'server/system/get_variable',setting)
            .success(function(data, status, headers, config) {

                                    saveSettingsData(data,setting.name);
                                    deferred.resolve(data);

            })
            .error(function(data, status, headers, config) {

                                    // data.success = false;
                                     deferred.reject(data);

             });
   

            return deferred.promise;

        }
        
        function getTypes(callback) {

        
            var deferred = $q.defer(); 
            

                $http.post(config.serverUrl+'server/settings/price_calendar_types')
                .success(function(data, status, headers, config) {


                                        deferred.resolve(data);

                })
                .error(function(data, status, headers, config) {

                                        // data.success = false;
                                         deferred.reject(data);

                 });


                return deferred.promise;

        }
        
        
        function saveFieldData(data) {
  
            $rootScope.fieldData = data;


          }
        
         function saveSettingsData(data,type) {
  
            $rootScope.settings[type] = data;


          }
        
        function getSettingsData(data) {
  
            return $rootScope.settings;


        }  

    
         function getFieldData() {

            return $rootScope.fieldData;


          }  

       

        //#endregion
    }
})();