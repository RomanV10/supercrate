(function(){

    // declare modules
  //    angular.module('Authentication', []);


    angular.module('movecalc', ['ngMask','checklist-model'])

    .constant('config', {
    appName: 'movecalc',
    appVersion: 1.0,
    serverUrl: 'http://localhost/raimond/'
    })


    //calendar = Drupal.settings.movecalc.calendar;

	.directive('moveCalculator',function(){

        return {

            restrict: 'E',
            templateUrl: 'modules/movecalculator/views/movecalculator.html?44'

        };

    })

	.controller('movecalController', function ($scope,$rootScope, $http, $log,AuthenticationService) {


			AuthenticationService.GetFront(function (response) {
                if (response.success) {

                    AuthenticationService.SetCredentials(response);
					$scope.rates = response.prices_array;

                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });

		// Load Rooms
			  $scope.rooms = [
				{id: 1, text: 'living room'},
				{id: 2, text: 'dining room'},
				{id: 3, text: 'office'},
				{id: 4, text: 'extra room'},
				{id: 5, text: 'basement/storage'},
				{id: 6, text: 'garage'},
				{id: 7, text: 'patio'},
				{id: 8, text: 'play room'},
			  ];

			 $scope.checkedRooms = {
				rooms: []
			  };

			 $scope.showRooms = function(){


				$('.form-item-Extra-Furnished-Rooms').show();

				if($scope.moveSize == 1 || $scope.moveSize == 2) {

					$scope.checkedRooms.rooms = [];
					hideRooms();

				}
				else if($scope.moveSize == 8 || $scope.moveSize == 9 || $scope.moveSize == 10) { //if House

					$scope.checkedRooms.rooms = [1,2];
					showHouseRooms();

				}
				else{

					$scope.checkedRooms.rooms.push(1);
					showRooms();

				}


			 }


			 $scope.showRoomImg = function (roomId){

				var roomVal = roomId;
				var boxes = getBoxCount($scope.moveSize);
				var boxcount = '<b>'+boxes[0]+'-'+boxes[1]+'</b>';

				$(".boxcount b").remove();
				$(".boxcount").append(boxcount);

				if(angular.isUndefined($scope.checkedRooms.rooms[roomId])){
					$("#calc-info-steps .box_info .move-size").append(ShowRoom(roomId)).show(200);

					//var src = GetBigRoomSrc(roomId);
					//preload(src);

				}
				else{

					HideRoom(roomId);

				}

				showRoomsString();

			 }


		// END  ROOMS

	})
	
		.directive('zipcode', function ($rootScope, $http) {
			return {
				restrict: 'A',
				require: '?ngModel',
				link: function (scope, element, attrs, ngModel, geoCodingService) {
				
				
					scope.$watch(attrs.ngModel, function (zip_code) {
					
					
						if (!angular.isUndefined(zip_code)) {
							if (zip_code.length == 5) {
								$(".calc-intro").hide("slow").empty();
							
								geoCodingService.geoCode(zip_code)
									.then(resolve => {
										let address = {
											'city': resolve.city,
											'state': resolve.state,
											'postal_code': resolve.postal_code,
											'country': resolve.county,
											'full_adr': `${resolve.city}, ${resolve.state} ${resolve.zip}`,
										};
										let type = attrs.ngModel;
										showAddress(address, type);
									});
							}
						}
					});
				}
			};
		})


	.directive('formdatepicker', function ($rootScope) {
    return {
        restrict: 'A',
		require: '?ngModel',
         link: function (scope, element, attrs,ngModel) {
            element.datepicker({
                dateFormat: 'MM d, yy',
				numberOfMonths: 2,
				changeMonth: false,
				changeYear: false,
				showButtonPanel: true,
				minDate: new Date(),
                onSelect: function (date) {
                    scope.date = date;
                    scope.$apply();


					// Set Min Date for Storage Service Can not storage date more then move date
					var sdp = $("#edit-date-storage-datepicker-popup-0").val();

					if(sdp.length && element[0].id == 'edit-move-date-datepicker-popup-0') {
						var mdate = new Date(date).getDate();
						var ddate = new Date(sdp).getDate();
						if(ddate >= mdate){

						$("#edit-date-storage-datepicker-popup-0").val('');

						}

					}

					if(element[0].id == 'edit-move-date-datepicker-popup-0'){

						$("#calc-info-steps .calc-intro").hide("slow").empty();
						$("#calc-info-steps .box_info .moving-date").empty().append('<h3>Move Date:</h3> <span>'+date +'</span>').show("slow");
						$("#calc-info-steps .box_info .service-type").empty().append('<h3>Type of service:</h3><span class="company-color">'+$('#edit-service option:selected').text()+'</span>').show("slow");

					}


                },
				onChangeMonthYear : function (){
				beforeshowCal(element);
				},
				beforeShow: function (){

				beforeshowCal(element);

				var ddp = $("#edit-move-date-datepicker-popup-0").val();
				var sdp = $("#edit-date-storage-datepicker-popup-0").val();
				var mdate = new Date(ddp);
				var ddate =	mdate.setDate(mdate.getDate() + 1)

				if(ddp.length && element[0].id == 'edit-date-storage-datepicker-popup-0'){
					element.datepicker( "option", "minDate",new Date(ddate));

				}

				},
				beforeShowDay : function(date){


					 return checkDate(date,$rootScope.globals.front.data.calendar);

				}
            });


		}
    };
	})


	.directive('servicetype',function($rootScope,$http) {
    return {
        restrict: 'A',
		 require: '?ngModel',
         link: function (scope, element, attrs,ngModel) {
			var firstInit = true;
			scope.$watch(attrs.ngModel,function (nval) {

			  if(!angular.isUndefined(nval)){

					if(firstInit){

						firstInit = false;
						return;

					}

					$("#calc-info-steps .calc-intro").hide("slow").empty();
					$("#calc-info-steps .box_info .service-type" ).empty().append('<h3>Type of service:</h3><span class="company-color">'+$('#edit-service option:selected').text()+'</span>').show("slow");


					applyServiceTypeChanges(nval);

			}

			});

        }
    };
	})

	.directive('sizeofmove',function() {
    return {
        restrict: 'A',
		 require: '?ngModel',
         link: function (scope, element, attrs,ngModel) {
			var firstInit = true;
			scope.$watch(attrs.ngModel,function (nval) {

			  if(!angular.isUndefined(nval)){

					$("#calc-info-steps .calc-intro").hide("slow").empty();
					$("#calc-info-steps .box_info .service-type" ).empty().append('<h3>Type of service:</h3><span class="company-color">'+$('#edit-service option:selected').text()+'</span>').show("slow");


					applySizeofMoveChanges(nval);

			}

			});

        }
    };
	})


	 function beforeshowCal(element){

		 setTimeout(function () {
				   var btn = $('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Clear</button>');

				 var buttonPane = element
						.datepicker( "widget" )
						.find( ".ui-datepicker-buttonpane" );

				buttonPane.find(".ui-datepicker-current").hide();
				buttonPane.find(".ui-datepicker-close").hide();
				buttonPane.find(".pricehelp").remove();


				var helps = "<span class='pricehelp'><li class='type dicount'>Discount</li><li class='type regular'>Regular</li><li class='type subpeak'>Subpeak</li><li class='type peak'>Peak</li><li class='type link'><a href='#pricing' >Check Pricing</a></li></span>";

				buttonPane.append(helps);

        }, 1);


	 }

	 function checkDate(date,calendar) {



			var day = (date.getDate().toString()).slice(-2);
			var month = ((date.getMonth() + 1).toString()).slice(-2);
			var year = date.getFullYear();
			var date = year+'-'+month+'-'+day;
			if(calendar[date]=='block_date')
				return [false, 'not-available', 'This date is NOT available'];
			else
				return [true, calendar[date], calendar[date]];

	}

	function showAddress(address,type){

		if(type == 'zipTo'){
		$('#edit-moving-to-state').val(address.state);
		$('#edit-moving-to-city').val(address.city);
		$('#edit-moving-to-zip').val(address.postal_code);

		var addr = address.city+', '+address.state+' '+address.postal_code;
		$("#calc-info-steps .box_info .moving-to").empty().append('<h3>Moving To:</h3><span ng-class="{'+'error-address'+' : !moveToState}">'+addr+'</span>').show("slow");
		$('input[name="movers_to_fullzip"]').val(addr);

		}
		else {
		$('#edit-moving-from-state').val(address.state);
		$('#edit-moving-from-city').val(address.city);
		$('#edit-moving-from-zip').val(address.postal_code);

		var addr = address.city+', '+address.state+' '+address.postal_code;
		$("#calc-info-steps .box_info .moving-from").empty().append('<h3>Moving From:</h3><span>'+addr+'</span>').show("slow");
		$('input[name="movers_from_fullzip"]').val(addr);
		}





	}


	function parseAddress(arrAddress){

		var city,state,country,postal_code;

					$.each(arrAddress, function (i, address_component) {

							if (address_component.types[0] == "administrative_area_level_1") {// State
								if(address_component.short_name.length < 3){

									 state = address_component.short_name;

									}
								}
							if (address_component.types[0] == "locality" || address_component.types[0] == "neighborhood") {// locality type

									  city = address_component.long_name;


							}

							if (address_component.types[0] == "country") {// locality type

									  country = address_component.short_name;


							}

							if (address_component.types[0] == "postal_code") {// "postal_code"

									  postal_code = address_component.short_name;


							}



					});


		var address =  {
		 'city': city,
		 'state': state,
		 'postal_code' : postal_code,
		 'country': country

		}

		return address;

	}


   function applyServiceTypeChanges(nval){


		if(nval == '2' || nval == '6') { // Moving and storage
					$("#edit-date-storage-datepicker-popup-0").addClass("required");
					$(".form-item-Date-Storage").show();
					$(".inputicon-storage").show();
					$(".form-item-prefered-fromstorage-time").show();
					$("#edit-prefered-fromstorage-time").addClass("required");

						if(nval == '6'){

							$("#edit-date-storage-datepicker-popup-0").removeClass("required");
							$(".form-item-Date-Storage").hide();


						}


					}
		else {

					$("#edit-date-storage-datepicker-popup-0").removeClass("required");
					$(".inputicon-storage").hide();
					$(".form-item-Date-Storage").hide();
					$("#edit-prefered-fromstorage-time").removeClass("required");

		}

		if(nval == '3' || nval == '8') {//Loading Help or Packing Day
				$("#edit-zip-code-to").attr("disabled", "disabled").addClass("disabled").removeClass("required");
				$("#edit-zip-code-from").removeAttr("disabled").removeClass("disabled").addClass("required");

				//$("#toautocomplete").attr("disabled", "disabled").addClass("disabled").removeClass("required");
				//$("#fromautocomplete").removeAttr("disabled").removeClass("disabled").addClass("required");

				$("#address_to").attr("disabled", "disabled").addClass("disabled").removeClass("required");
				$("#address_from").removeAttr("disabled").removeClass("disabled");

				$("#edit-moving-to-apt").attr("disabled", "disabled").addClass("disabled");
				$("#edit-moving-from-apt").removeAttr("disabled").removeClass("disabled");


				$("#edit-zip-code-to").val("");
				$(".info_block .moving-to").empty();
				$("#edit-type-to").attr("disabled", "disabled").addClass("disabled").removeClass("required");
				$("#edit-type-from").removeAttr("disabled").removeClass("disabled").addClass("required");


			}
			else if(nval == '4') {//UnLoading Help
				$("#edit-zip-code-from").attr("disabled", "disabled").addClass("disabled").removeClass("required");
				$("#edit-zip-code-to").removeAttr("disabled").removeClass("disabled").addClass("required");

			//	$("#fromautocomplete").attr("disabled", "disabled").addClass("disabled").removeClass("required");
			//	$("#toautocomplete").removeAttr("disabled").removeClass("disabled").addClass("required");

				$("#address_from").attr("disabled", "disabled").addClass("disabled").removeClass("required");
				$("#address_to").removeAttr("disabled").removeClass("disabled");

				$("#edit-moving-from-apt").attr("disabled", "disabled").addClass("disabled");
				$("#edit-moving-to-apt").removeAttr("disabled").removeClass("disabled");

				$("#edit-zip-code-from").val("");
				$(".info_block .moving-from").empty();
				$("#edit-type-from").attr("disabled", "disabled").addClass("disabled").removeClass("required");
				$("#edit-type-to").removeAttr("disabled").removeClass("disabled").addClass("required");
			}
			else{
				$("#edit-zip-code-from").removeAttr("disabled").removeClass("disabled").addClass("required");
				$("#edit-zip-code-to").removeAttr("disabled").removeClass("disabled").addClass("required");
				$("#edit-type-from").removeAttr("disabled").removeClass("disabled").addClass("required");
				$("#edit-type-to").removeAttr("disabled").removeClass("disabled").addClass("required");

				$("#edit-moving-to").removeAttr("disabled").removeClass("disabled");
				$("#edit-moving-from").removeAttr("disabled").removeClass("disabled");

				$("#edit-moving-to-apt").removeAttr("disabled").removeClass("disabled");
				$("#edit-moving-from-apt").removeAttr("disabled").removeClass("disabled");

				$("#address_to").removeAttr("disabled").removeClass("disabled");
				$("#address_from").removeAttr("disabled").removeClass("disabled");


			}




   }

   function applySizeofMoveChanges(nval){


   $("#calc-info-steps .calc-intro").hide("slow").empty();


	var boxes = getBoxCount($("#edit-size-move").val());
	var boxcount = '<b>'+boxes[0]+'-'+boxes[1]+'</b>';

	var enlarge_icon = '<i id="zoomimage" class="fa fa-search-plus fa-2x"></i><span class="boxcount"><i class="suitboxes"></i>'+boxcount+'</span> ';
	$("#calc-info-steps .box_info .move-size").empty().append(enlarge_icon).append(SizeOfMoveTips(nval)).show(200);


	var size = $('#edit-size-move').val();
	$('.alerttip').hide();

	if( size == 8 || size == 9 || size == 10 ){

						var title = 'In-home estimate';
						var text = 'Estimating is more like an art than a science. And we will say that it is human nature to underestimate just how much stuff you need to move. There is no better way to estimate than to have a professional mover visit your actual home. We recommend having free on-site estimate if your move is larger than 3-bedroom apartment and you think that it may be required more than one truck for your move.';

						$("#calc-results2 h2.typeofent").text(title);
						$("#calc-results2 .calc-intro_description.block2").text(text);


				}
				else{
					var title = 'Type Of Entrance';
					var text = 'If you 1st/ground floor apartment is located on a hill and involves outside stairs, please choose type of entrance accordingly to how many steps you have, NOT ground floor. 10-12 steps usually equal to one flight of stairs.';

						$("#calc-results2 h2.typeofent").text(title);
						$("#calc-results2 .calc-intro_description.block2").text(text);


				}


   }

   function getBoxCount(size){

		var i = 0;
		var count = 0;

		if( size == 8 || size == 9 || size == 10 ){

			$('#edit-extra-house-furnished-rooms input[type=checkbox]').each(function() {
				if ($(this).is(":checked")) {
					  i++;
				   }
			});

		}else
		$('#edit-extra-furnished-rooms input[type=checkbox]').each(function() {
					if ($(this).is(":checked")) {
					  i++;
				   }
		});


			 switch (size) {
			case ('1'): //  Room
				 count = 5;
				 if(i == 0) {
				 var minC = 5;
				 var maxC =10;
				return [minC,maxC];
				 }
				break;
			case ('2'): // Studio
				 count = 20;
				  if(i == 0) {
				 var minC = 20;
				 var maxC =25;
				return [minC,maxC];
				 }
				break;
			case ('3'): // 1 Small Bedroom
				 count = 10;
				break;
			case ('4'):  // 1 Large Bedroom
				 count = 20;
				break;
			case ('5'): // 2 Small Bedroom
				 count = 20;
				break;
			case ('6'): //2 Large Bedroom
				 count = 30;
				break;
			case ('7'): // 3 Bedroom
				 count = 50;
				 if(i == 1) {
				 var minC = 50;
				 var maxC = 60;
				return [minC,maxC];
				 }
				break;
			case ('8'): // House
				 count = 50;
				 break;
			case ('9'): // House
				 count = 60;
				 break;
			 case ('10'): // House
				 count = 70;
				break;
			 }
		var minC = count + i*5;
		var maxC = count + i*10;
		return [minC,maxC];
	}

  function SizeOfMoveTips(type){


		var tips_text = 'Please choose <b>Large</b> rather than <b>Small</b>, if your furniture includes a lot of bulky, over-sized and/or antique items, ones with glass, marble and/or that require a lot of disassembling/reassembling. These items require extra care and time to handle and in most cases we recommend an additional mover for greatest efficiency.'

	   switch (type) {


                    case ('1'):
                        addRequired();

						$('.form-item-Extra-Furnished-Rooms').show();
						$('.form-item-Extra-House-Furnished-Rooms').hide();
						return '<h3>Size of Move:</h3><span class="msize" data-room="room" data-href="img/rooms/roombig.png">Room or less</span><i class="rooms"></i><div class="calc-intro_description"><img class="smroom" src="img/rooms/room.jpg"><div class="desc"><em>Room or less.</em> Includes content of furniture for one room only with approximately 5-10 assorted size boxes, suitcases and totes.</div>';
                        break;
                    case ('2'):
						$('.form-item-Extra-Furnished-Rooms').show();
						$('.form-item-Extra-House-Furnished-Rooms').hide();
                        addRequired();

						return '<h3>Size of Move:</h3><span class="msize" data-room="studio" data-href="img/rooms/studiobg.png">Studio apartment</span><i class="rooms"></i><div class="calc-intro_description"><img class="studioimg" title="Studio" src="img/rooms/studio.png"><div class="desc"><em>Studio</em> size apartment typically consist of one large room, which serves as the living, dining and bedroom.</div>';
                        break;
					case ('3'):
                      addRequired();

						return '<h3>Size of Move:</h3><span class="msize" data-room="onebedroom" data-href="img/rooms/1bgsmall.png">Small 1 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="calc-intro_description"><img class="onebedrimg" title="Small 1 Bedroom Apartment" src="img/rooms/sm1sm.png"><div class="desc">Size of your apartment, Small or Large,<b> depends on the amount of furniture and miscellaneous items that have to be moved!</b> NOT on square footage!<i class="tooltips fa fa-info-circle"><div class="tips rmtips">'+tips_text+'</div></i></div>';
                        break;
					case ('4'):
                        addRequired();

						return '<h3>Size of Move:</h3><span class="msize" data-room="lonebedroom" data-href="img/rooms/1bigsm.png">Large 1 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="calc-intro_description"><img title="Large 1 Bedroom Apartment" src="img/rooms/sm1lar.png"><div class="desc">Size of your apartment, Small or Large,<b> depends on the amount of furniture and miscellaneous items that have to be moved!</b> NOT on square footage!<i class="tooltips fa fa-info-circle"><div class="tips rmtips">'+tips_text+'</div></i></div>';
                        break;
					case ('5'):
                       addRequired();

					   return '<h3>Size of Move:</h3><span class="msize" data-room="stwobedroom" data-href="img/rooms/sm2.png">Small 2 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="calc-intro_description"><img title="Small 2 Bedroom Apartment" src="img/rooms/sm2sm.png"><div class="desc">Size of your apartment, Small or Large,<b> depends on the amount of furniture and miscellaneous items that have to be moved!</b> NOT on square footage!<i class="tooltips fa fa-info-circle"><div class="tips rmtips">'+tips_text+'</div></i></div>';
                        break;
                    case ('6'):
						addRequired();

						 return '<h3>Size of Move:</h3><span class="msize" data-room="ltwobedroom" data-href="img/rooms/large2.png">Large 2 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="calc-intro_description"><img title="Large 2 Bedroom Apartment" src="img/rooms/sm2lar.png"><div class="desc">Size of your apartment, Small or Large,<b> depends on the amount of furniture and miscellaneous items that have to be moved!</b> NOT on square footage!<i class="tooltips fa fa-info-circle"><div class="tips rmtips">'+tips_text+'</div></i></div>';
                        break;
					case ('7'):
                        addRequired();

						return '<h3>Size of Move:</h3><span class="msize" data-room="3bedroom" data-href="img/rooms/3bedbg.png">3 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="calc-intro_description"><img title="3 Bedroom Apartment" src="img/rooms/sm3bed.png"><div class="desc"></div>';
						 break;
                   case ('8'):

						setHouseEntarence();
                        return '<h3>Size of Move:</h3><span class="msize" data-room="house2" data-href="img/rooms/2house.png">House/Townhouse with 2 Bedrooms</span><i class="rooms">(with living room and dining room)</i><div class="calc-intro_description"><img title="Large 2 Bedroom Apartment" src="img/rooms/sm2house.png"><div class="desc"></div>';
						 break;
					case ('9'):

						setHouseEntarence();
						return '<h3>Size of Move:</h3><span class="msize" data-room="house3" data-href="img/rooms/3house.png">House/Townhouse with 3 Bedrooms</span><i class="rooms">(with living room and dining room)</i><div class="calc-intro_description"><img title="3 Bedroom Apartment" src="img/rooms/sm3house.png"><div class="desc"></div>';
						break;
				    case ('10'):

					   setHouseEntarence();
					   return '<h3>Size of Move:</h3><span class="msize"  data-room="house4" data-href="img/rooms/4house.png">House/Townhouse with 4 Bedrooms</span><i class="rooms">(with living room and dining room)</i><div class="calc-intro_description"><img title="4 Bedroom House" src="img/rooms/sm4house.png"><div class="desc"></div>';
						default:
                        return '<div class="calc-intro"><h1 class="calc-intro_heading">Choose Size</h1><p class="calc-intro_description" > Please choose size of your move.</p></div>';

		}

	}



	function setHouseEntarence(){
		$("#edit-type-from option[value='7']").attr("selected", "selected");
		$("#edit-type-from").val('7').attr("disabled", "disabled").addClass("disabled");


		$("#edit-type-from").val('7').attr("disabled", "disabled").addClass("disabled");
		$("#edit-type-to").val('7');

	}

	function takeOfRequired(){
		//$("#edit-type-from").removeClass("required");
		//$("#edit-type-to").removeClass("required");

		//$('#edit-calculator .three_block').removeClass("apartment");
		//$('#edit-calculator .three_block').addClass("thouse");
	}
	function addRequired(){

		var service = $('#edit-service').val();

		if($('#edit-service').val() != '3' && $('#edit-service').val() != '4' ) {

		  $("#edit-type-from").removeAttr("disabled");
		  $("#edit-type-from").removeClass("disabled");

		}


	}
	function hideRooms() {
		for(var i=1; i <=9 ; i++){

			$('.form-item-Extra-Furnished-Rooms-'+i).hide();
		}
		$('.form-item-Extra-Furnished-Rooms-5').show();
	}
	function showRooms() {
	$('.form-item-Extra-Furnished-Rooms').show();

		for(var i=1; i <5 ; i++){
		 $('.form-item-Extra-Furnished-Rooms-'+i).show();
		}
		for(var i=6; i <=8 ; i++){
		 $('.form-item-Extra-Furnished-Rooms-'+i).hide();
		}

	}
	function showHouseRooms() {
		$('.form-item-Extra-House-Furnished-Rooms').show();

		for(var i=2; i <9 ; i++){
		 $('.form-item-Extra-House-Furnished-Rooms-'+i).show();
		}
	}



	function GetRoomNamyById(id){

			 switch (id) {
			case ('1'): // Living Room
				 return 'living room';
				break;
			case ('2'): // Dinnig Room
				 return 'dinnig room';
				break;
			case ('3'): // Office
				 return 'office';
				break;
			case ('4'): // Extra room
				 return 'extra room';
				break;
			case ('5'): // Basement
				 return 'basement';
				break;
			case ('6'): // Garage
				 return 'garage';
				break;
			case ('7'): // Patio
				 return 'patio';
				break;
			case ('8'): // Play
				 return 'play';
				break;
			 }
	}

	function GetBigRoomSrc(room){

			var roomSize = $("#edit-size-move").val();


			 switch (room) {
			case ('2'): // Dinnig Room
				 return 'img/rooms/dinning.png">';
				break;
			case ('3'): // Office
				 return 'img/rooms/office.png">';
				break;
			case ('4'): // Extra room
				 return 'img/rooms/extraroom.png">';
				break;
			case ('5'): // Basement
				 return 'img/rooms/basement.png">';
				break;
			case ('6'): // Garage
				 return 'img/rooms/garage.png"></a>';
				break;
			case ('7'): // Patio
				 return 'img/rooms/smpatio.png">';
				break;
			case ('8'): // Play
				 return 'img/rooms/playroom.png"></a>';
				break;
			default:
				return '';
			 }
	}

	function ShowBigRoom(room){

			var roomSize = $("#edit-size-move").val();


			 switch (room) {
			case ('2'): // Dinnig Room
				 return '<img class="big_dinning_room_'+roomSize+'" title="Dinning Room" src="img/rooms/dinning.png">';
				break;
			case ('3'): // Office
				 return '<img class="big_office_room_'+roomSize+'" title="Office" src="img/rooms/office.png">';
				break;
			case ('4'): // Extra room
				 return '<img class="big_extra_room_'+roomSize+'" title="Extra room" src="img/rooms/extraroom.png">';
				break;
			case ('5'): // Basement
				 return '<img class="big_basement_room" title="Basement" src="img/rooms/basement.png">';
				break;
			case ('6'): // Garage
				 return '<img class="big_garage_room" title="Garage" src="img/rooms/garage.png"></a>';
				break;
			case ('7'): // Patio
				 return '<img class="big_patio_room" title="Patio" src="img/rooms/smpatio.png">';
				break;
			case ('8'): // Play
				 return '<img class="big_play_room_'+roomSize+'" title="Play Room" src="img/rooms/playroom.png"></a>';
				break;
			default:
				return '';
			 }
	}

	function ShowRoom(room){

			var roomSize = $("#edit-size-move").val();


			 switch (room) {
			case (2): // Dinnig Room
				 return '<img class="dinning_room" title="Dinning Room" src="img/rooms/smdr.png">';
				break;
			case (3): // Office
				 return '<img class="office_room_'+roomSize+'" title="Office" src="img/rooms/smoffice.png">';
				break;
			case (4): // Extra room
				 return '<img class="extra_room_'+roomSize+'" title="Extra room" src="img/rooms/smextraroom.png">';
				break;
			case (5): // Basement
				 return '<img class="basement_room" title="Basement" src="img/rooms/smbasement.png">';
				break;
			case (6): // Garage
				 return '<img class="garage_room" title="Garage" src="img/rooms/garage.png">';
				break;
			case (7): // Patio
				 return '<img  class="patio_room" title="Patio" src="img/rooms/smpatio.png">';
				break;
			case (8): // Play
				 return '<img class="play_room_'+roomSize+'" title="Play" src="img/rooms/playroom.png">';
				break;
			default:
				return '';
			 }
	}

	function HideRoom(room){
		var roomVal = room.val();
		var roomSize = $("#edit-size-move").val();
		 switch (roomVal) {
			case ('2'): // Dinnig Room
				$('.dinning_room').remove();
				break;
			case ('3'): // Dinnig Room
				$('.office_room_'+roomSize).remove();
				break;
			case ('4'): // Dinnig Room
				$('.extra_room_'+roomSize).remove();
				break;
			case ('5'): // Dinnig Room
				$('.basement_room').remove();
				break;
			case ('6'): // Dinnig Room
				$('.garage_room').remove();
				break;
			case ('7'): // Dinnig Room
				$('.patio_room').remove();
				break;
			case ('8'): // Play Room
				$('.play_room_'+roomSize).remove();
				break;
			 }

	}




	function resetForm(){

    $(':input','#movecalc-moving-form')
	 .not(':button, :submit, :reset, :hidden')
	 .val('')
	 .removeAttr('checked')
	 .removeAttr('selected');
	$('#edit-zip-code-from').val("");

	$('.form-item-Date-Storage').hide();
	$(".inputicon-storage").hide();

	}



})();
