import tmpReviews from './template/review.tpl.html';

(function () {
    'use strict';

    angular.module('movecalc').directive('reviewsFront', reviewsFront);

    reviewsFront.$inject = ['$rootScope', '$interval'];

    function reviewsFront($rootScope, $interval) {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: tmpReviews,
            link: linkReviewFront
        };
        
        //TODO replace carousel into new component as soon as posible

        function linkReviewFront($scope) {
	        const TIMER = 20000;

            $scope.reviews = [
                {
                    value: "What a relief to find such a great service. the crew came to my apartment right on time, quickly moved my furniture to their track, brought it to my new place, put them into the rooms as i requested, and left. No damage to the furniture, no missing things, no damages to the walls!"
                },
            ];

            $scope.reviews2 = [
                {
                    value: "I absolutely love.I have used them three times and each time, my move was fast and easy. They always send a friendly team who takes special care of fragile items and ensures everything goes where it should. They are always on time or early and I am always very satisfied with their work. Thank you for making moving a little easier!"
                },

            ];

            $scope.randomReview = $scope.reviews[Math.floor(Math.random() * $scope.reviews.length)];
            $scope.randomReview2 = $scope.reviews2[Math.floor(Math.random() * $scope.reviews2.length)];

            $scope.changeSlideIndex = changeSlideIndex;
            $scope.nextSlide = nextSlide;
            $scope.prevSlide = prevSlide;

            $rootScope.$watch('reviewsSettings', onSetReviewSettings);
            function onSetReviewSettings() {
                if ($rootScope.reviewsSettings != undefined) {
                    $scope.greatestReviews = $rootScope.reviewsSettings.reviewsFrontSite;
                    $scope.reviewsIndex = [];
	                var totalAverage = 0;
                    angular.forEach($scope.greatestReviews, function (item, key) {
                        $scope.reviewsIndex.push(key);
	                    totalAverage += Number(item.stars);
                    });

	                $scope.totalAverage = (totalAverage / $scope.greatestReviews.length) || 0;
                    $scope.firstReview = $scope.reviewsIndex[0];
                    $scope.lastReview = $scope.reviewsIndex.length - 1;
	                $scope.slideIndex = 0;
	                play();
                }
            }

	        function play () {
				if (angular.isDefined($scope.timer)) {
					$interval.cancel($scope.timer);
				}
            	$scope.timer = $interval(function () {
					$scope.slideIndex = ($scope.greatestReviews.length - 1 > $scope.slideIndex)?++$scope.slideIndex:0;
	            }, TIMER);
	        }

	        function changeSlideIndex (index) {
		        $scope.slideIndex = index;
				play();
	        }

	        function nextSlide() {
            	if ($scope.greatestReviews.length - 1 > $scope.slideIndex) {
		            $scope.slideIndex++;
	            } else {
		            $scope.slideIndex = 0;
	            }
				play();
	        }

	        function prevSlide() {
            	if ($scope.slideIndex > 0) {
		            $scope.slideIndex--;
	            } else {
		            $scope.slideIndex = $scope.greatestReviews.length - 1;
	            }
				play();
	        }
        }
    }
})();