(function () {
    'use strict';

    angular
        .module('movecalc')
        .factory('BranchingServiceApi', BranchingServiceApi);

    BranchingServiceApi.$inject = ['$http', '$q', '$rootScope', 'Session', 'config'];

    function BranchingServiceApi($http,  $q, $rootScope, Session, config) {
        var service = {};

        service.checkApi = checkApi;
        service.getFrontByUrl = getFrontByUrl;
        service.getClosestBranch = getClosestBranch;

        return service;


        function checkApi(url) { // all function not work
            var deferred = $q.defer();

            var setting = {};
            setting.name = 'basicsettings';

            $http.post(url + '/server/system/get_variable', setting)
                 .success(function (data) {
                     deferred.resolve(data);
                 })
                 .error(function (data, status, headers, config) {
                     deferred.reject(data);
                 });

            return deferred.promise;
        }

        function getFrontByUrl(url) {
            var deferred = $q.defer();

            $http
                .post(url + 'server/front/frontpage')
                .success(function (data, status, headers, config) {
                    data.success = true;

                    $rootScope.fieldData = data;

                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    if (data == null)
                        data = [];

                    data.success = false;

                    Session.destroy();
                    deferred.reject(data);
                });

            return deferred.promise;
        }

        function getClosestBranch(zipFrom, zipTo) {
            var deferred = $q.defer();

            var data = {
                "zip_from": zipFrom,
                "zip_to": zipTo
            };

            $http
                .post(config.serverUrl + 'server/move_branch/get_closest_branch', data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });


            return deferred.promise;
        }
    }
})();
