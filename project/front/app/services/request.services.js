(function () {

    angular
        .module('movecalc')
        .factory('MoveRequestServices', MoveRequestServices);

	/*@ngInject*/
    function MoveRequestServices ($http, $rootScope, $q, $filter, config, FormHelpServices, AuthenticationService, CalculatorServices, apiService, moveBoardApi) {
        var service = {};

        var directionsService = new google.maps.DirectionsService();

        var scheduleData = [];

        service.GetRequestCredentials = GetRequestCredentials;
        service.SetRequestCredentials = SetRequestCredentials;
        service.ClearRequestCredentials = ClearRequestCredentials;
        service.SetRequestAddress = SetRequestAddress;
        service.parseZipToAddress = parseZipToAddress;
        service.Calculate = Calculate;
        service.CreateRequest = CreateRequest;
        service.UpdateStatus = UpdateStatus;
        service.CreateStorageRequest = CreateStorageRequest;

        service.getTravelTime = getTravelTime;
        service.get_readable_time = get_readable_time;
        service.UpdateRequest = UpdateRequest;
        service.getDuration = getDuration;
        service.PrepareRequestArray = PrepareRequestArray;
        service.saveSettingsData = saveSettingsData;
        service.sendLogs = sendLogs;

        return service;

        function saveSettingsData (data) {
            scheduleData = data;
        }


        function PrepareRequestArray (request) {
            var move_request = {};

            if (angular.isUndefined(request.preferedTime)) {
                request.preferedTime = 1; // Any TiME
            }

            var start_time = getStartTime(request.preferedTime);

            if (angular.isUndefined(request.moveDate)) {
                request.moveDate = request.moveDateFormat;
            }

            request.moveDate = moment(request.moveDate).format('YYYY-MM-DD');

            if (request.serviceType != 7) {
                request.premise = '';
            }

            move_request.data = {
                status: 1,
                title: 'Move Request',
                uid: request.current_user.uid,
                field_useweighttype: '1',
                field_first_name: request.current_user.field_user_first_name,
                field_last_name: request.current_user.field_user_last_name,
                field_e_mail: request.current_user.mail,
                field_phone: request.current_user.field_primary_phone,
                field_additional_phone: request.current_user.field_user_additional_phone,
                field_approve: request.status,
                field_move_service_type: request.serviceType,
                field_date: {date: request.moveDate, time: '15:10:00'},
                field_size_of_move: request.moveSize,
                field_type_of_entrance_from: request.typeFrom,
                field_type_of_entrance_to_: request.typeTo,

                field_start_time: request.preferedTime,
                field_actual_start_time: start_time.start_time1,
                field_start_time_max: start_time.start_time2,

                field_price_per_hour: request.rate,
                field_movers_count: request.crew,
	            field_delivery_crew_size: request.crew,
                field_distance: Math.round(request.distance),
                field_travel_time: request.travelTime,
                field_minimum_move_time: request.min_time,
                field_maximum_move_time: request.max_time,
                field_duration: request.duration,
                field_parser_provider_name: request.poll,

                field_extra_furnished_rooms: request.checkedRooms.rooms,
                field_moving_to: {
                    country: "US",
                    administrative_area: request.addressTo.state,
                    locality: request.addressTo.city,
                    postal_code: request.addressTo.postal_code,
                    thoroughfare: request.adrTo,
                    premise: request.premise,
                },
                field_apt_from: request.aptFrom,
                field_apt_to: request.aptTo,
                field_moving_from: {
                    country: "US",
                    administrative_area: request.addressFrom.state,
                    locality: request.addressFrom.city,
                    postal_code: request.addressFrom.postal_code,
                    thoroughfare: request.adrFrom,
                    premise: '',
                },
                field_long_distance_rate: request.ldrate,
                field_reservation_price: getReservationPrice(request),
                field_dummy_send_mail: request.dummyMail, // CHECK IF NEED SEND MAILS TO CLIENT
            };

	        if (request.moveSize == 11) {
		        move_request.data.field_extra_furnished_rooms = [];
		        if (typeof request.field_commercial_extra_rooms === 'string') request.field_commercial_extra_rooms = [request.field_commercial_extra_rooms];
		        move_request.data.field_commercial_extra_rooms = request.field_commercial_extra_rooms;
            }

            if (angular.isDefined(request.double_travel_time)) {
                move_request.data.field_double_travel_time = request.double_travel_time;
            }

            move_request.account = {
                name: request.current_user.mail,
                mail: request.current_user.mail,
                fields: {
                    field_user_first_name: request.current_user.field_user_first_name,
                    field_user_last_name: request.current_user.field_user_last_name,
                    field_user_additional_phone: request.current_user.field_user_additional_phone,
                    field_primary_phone: request.current_user.field_primary_phone,
                }
            };

            return move_request;
        }


        function getReservationPrice (req) {
            //check status
            let reservation = 0;
            let zeroReservation = 0;
            switch (req.serviceType) {
              case '7':
                reservation = scheduleData.longReservationRate;
                if (!scheduleData.longReservationRate) {
                  let persentage = parseFloat(scheduleData.longReservation || zeroReservation);
                  reservation = CalculatorServices.getLongDistanceQuote(req) * persentage / 100;
                }
                break;
              case '5':
                reservation = scheduleData.flatReservationRate;
                if (!scheduleData.flatReservationRate) {
                  let persentage = parseFloat(scheduleData.flatReservation || zeroReservation);
                  reservation = req.rate * persentage / 100;
                }
                break;
              default:
                reservation = scheduleData.localReservationRate;
                if (!scheduleData.localReservationRate) {
                  let hours = parseFloat(scheduleData.localReservation || zeroReservation);
                  reservation = hours * parseFloat(req.rate);
                }
            }
            return reservation;
        }


		function CreateStorageRequest(request) {
			let defer = $q.defer();

			apiService.postData(moveBoardApi.request.createStorage, request)
				.then(({data}) => {
					defer.resolve(data);
				})
				.catch(() => {
					defer.reject();
				});

			return defer.promise;
		}


        function CreateRequest (request, callback) {
            var deferred = $q.defer();

            $http
                .post(config.serverUrl + 'server/move_request', request)
                .success(function (data, status, headers, config) {
                    //data.success = true;
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    // data.success = false;
                    deferred.reject(data);
                });

            return deferred.promise;
        }


        function UpdateStatus (nid, request) {
            var deferred = $q.defer();

            $http
                .put(config.serverUrl + 'server/move_request/' + nid, request)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });

            return deferred.promise;
        }


        function Calculate (request, callback) {
            var deferred = $q.defer();

            $http.get(config.serverUrl + 'services/session/token').success(function (data) {
                var token = data;

                $http.defaults.headers.common['X-CSRF-Token'] = unescape(encodeURIComponent(token));


                $http
                    .post(config.serverUrl + 'server/front', request)
                    .success(function (data, status, headers, config) {
                        //data.success = true;
                        deferred.resolve(data);
                    })
                    .error(function (data, status, headers, config) {
                        // data.success = false;
                        deferred.reject(data);
                    });
            });


            return deferred.promise;
        }


        function UpdateRequest (scope) {
            var date_input = scope.request.moveDate;
            var _date = new Date(date_input);
            scope.request.moveDateTime = _date.getTime();
            scope.request.moveDateFormat = $filter('date')(_date, 'MM/dd/yyyy');

            var delivery_input = scope.request.deliveryDate;

            if (scope.request.serviceType == 6) {
                var nextDay = new Date(date_input);
                nextDay.setDate(_date.getDate() + 1);
                scope.request.deliveryDateTime = nextDay.getTime();
                scope.request.deliveryDateFormat = $filter('date')(nextDay, 'yyyy-MM-dd');
            }
            else {
                var _date = new Date(delivery_input);
                scope.request.deliveryDateTime = _date.getTime();
                scope.request.deliveryDateFormat = $filter('date')(delivery_input, 'yyyy-MM-dd');
            }

            scope.request.typeFromText = scope.entranceType[scope.request.typeFrom - 1].text;
            scope.request.typeToText = scope.entranceType[scope.request.typeTo - 1].text;
            scope.request.serviceTypeText = scope.servicesType[scope.request.serviceType - 1].text;

            scope.request.moveSizeText = scope.sizeOfMove[scope.request.moveSize - 1].text;
            scope.request.roomsText = FormHelpServices.getRoomsString(scope.request.checkedRooms.rooms, scope.rooms);
            scope.request.roomsText = FormHelpServices.getRoomsString(scope.request.checkedRooms.rooms, scope.rooms);
        }


        function SetRequestAddress (data, type) {
            if (type == 'zipFrom') {
                $rootScope.globals.request.addressFrom = data;
            }
            else {
                $rootScope.globals.request.addressTo = data;
            }
        }


        function SetRequestCredentials (request) {
            $rootScope.globals.request = request;
        }

        function GetRequestCredentials () {
            return $rootScope.globals.request;
        }


        function ClearRequestCredentials () {
            $rootScope.globals.request = {};

        }


        function parseZipToAddress (arrAddress, type) {
            var city, state, country, postal_code;

            angular.forEach(arrAddress, function (address_component, i) {
                if (address_component.types[0] == "administrative_area_level_1") {// State
                    if (address_component.short_name.length < 3) {
                        state = address_component.short_name;
                    }
                }

                if (address_component.types.indexOf("locality") > -1 || address_component.types.indexOf("neighborhood") > -1 || address_component.types.indexOf("sublocality") > -1) {// locality type
                    city = address_component.long_name;
                }

                if (address_component.types[0] == "country") {// locality type
                    country = address_component.short_name;
                }

                if (address_component.types[0] == "postal_code") {// "postal_code"
                    postal_code = address_component.short_name;
                }
            });


            var address = {
                'city': city,
                'state': state,
                'postal_code': postal_code,
                'country': country,
                'full_adr': city + ', ' + state + ' ' + postal_code

            };

            return address;
        }


        function getTravelTime (cuReq) {
            var deferred = $q.defer();
            var basicsettings = angular.fromJson($rootScope.globals.front.basicsettings);

            cuReq.flatRate = false;

            getManualTravelTime(cuReq)
                .then(function () {
                    var distance = cuReq.distance;

                    var flatRateDistance = basicsettings.isflat_rate_miles ? basicsettings.flat_rate_miles || 0 : 0;
                    var longDistanceMiles = basicsettings.islong_distance_miles ? basicsettings.long_distance_miles || 0 : 0;

                    var isFlatRate = flatRateDistance && distance > flatRateDistance;
                    var isLongDistance = longDistanceMiles && distance > longDistanceMiles;

                    if (!isFlatRate && !isLongDistance) {
                        var promise = getFlateRateTravelTime(cuReq);

                        promise.then(function (result) {
                            if (result || result == 0) {

                                cuReq.travelTime = result;
                                cuReq.flatRateTravelTime = true;
                            } else {
                                cuReq.flatRateTravelTime = false;
                            }

                            deferred.resolve();
                        });
                    } else {
                        cuReq.flatRateTravelTime = false;
                        deferred.resolve();
                    }
                });

            return deferred.promise;
        }

        function getManualTravelTime (cuReq) {
            var deferred = $q.defer();

            var calcsettings = angular.fromJson($rootScope.globals.front.calcsettings);
            var basicsettings = angular.fromJson($rootScope.globals.front.basicsettings);
            var discount_zip = {};

            angular.forEach(calcsettings.discount_zips, function (zip, id) {
                discount_zip[zip] = 1;
            });

            var discZips = discount_zip;
            var zipFrom = cuReq.zipFrom;
            var zipTo = cuReq.zipTo;

            var zip_code_from = cuReq.addressFrom.full_adr;
            var zip_code_to = cuReq.addressTo.full_adr;

            var zone_from = discZips[zipFrom];
            var zone_to = discZips[zipTo];

            // Get Time from zone to zone formula
            var time_zone = {
                '1': 0.25,
                '2': 0.5,
                '3': 0.75,
                '4': 1
            };

            if (zone_from == undefined) { //if it's far away - count time like google said From ZIP TO PARKING
                requestTravelTime(basicsettings.company_address, zip_code_from)
                    .then(function (result) {
                        if (result.status == google.maps.DirectionsStatus.OK) {
                            var data = result.result;

                            var distance_zone_to = data['routes'][0]['legs'][0]['distance']['text'];   //Distance from Zip To Zip
                            var distance_zone_to_value = data['routes'][0]['legs'][0]['distance']['value'] / 1609.34;

                            var time_zone_duration_to = (Math.round(data['routes'][0]['legs'][0]['duration']['value'] / 3600)).toFixed(2); // Apr. Driving Time.

                            time_zone_duration_to = parseFloat(time_zone_duration_to);

                            if (zone_to == undefined) { //if it's far away - count time like google said
                                requestTravelTime(zip_code_to, basicsettings.company_address)
                                    .then(function (result) {

                                        if (result.status == google.maps.DirectionsStatus.OK) {
                                            var data = result.result;

                                            var distance_zone_from = data['routes'][0]['legs'][0]['distance']['text'];   //Distance from Zip To Zip
                                            var distance_zone_from_value = data['routes'][0]['legs'][0]['distance']['value'] / 1609.34;   //Distance from Zip To Zip
                                            var time_zone_duration_from = (Math.round(data['routes'][0]['legs'][0]['duration']['value'] / 3600)).toFixed(2); // Apr. Driving Time.
                                            time_zone_duration_from = parseFloat(time_zone_duration_from);

                                            if (time_zone_duration_to <= 0.25)  time_zone_duration_to = 0.25;
                                            if (time_zone_duration_from <= 0.25) time_zone_duration_from = 0.25;

                                            var travel_time = time_zone_duration_from + time_zone_duration_to;

                                            if (cuReq.duration > 2) travel_time = travel_time - 0.25; // Discount on travel time if driving time more then 1.5 hour.

                                            cuReq.travelTime = travel_time;

                                            var flat_distance = Math.max(distance_zone_from_value, distance_zone_to_value);

                                            if (flat_distance > config.flatRateDistance) {
                                                cuReq.flatRate = true;
                                                cuReq.travelTime = Math.min(time_zone_duration_to, time_zone_duration_from);
                                            }
                                        }

                                        deferred.resolve();
                                    });
                            } else {
                                var time_zone_duration_from = time_zone[zone_to];
                                var distance_zone_from_value = 10; // for flat rate
                                if (time_zone_duration_to <= 0.25)  time_zone_duration_to = 0.25;
                                if (time_zone_duration_from <= 0.25) time_zone_duration_from = 0.25;

                                var travel_time = time_zone_duration_from + time_zone_duration_to;

                                if (cuReq.duration > 1.25) travel_time = travel_time - 0.25; // Discount on travel time if driving time more then 1.5 hour.

                                cuReq.travelTime = travel_time;


                                var flat_distance = Math.max(distance_zone_from_value, distance_zone_to_value);

                                cuReq.distance = flat_distance;

                                if (flat_distance > config.flatRateDistance) {
                                    cuReq.flatRate = true;
                                    cuReq.travelTime = Math.min(time_zone_duration_to, time_zone_duration_from);
                                }

                                deferred.resolve();
                            }
                        }
                    });

            } else {
                var time_zone_duration_to = time_zone[zone_from];

                if (zone_to == undefined) { //if it's far away - count time like google said  1 LOcal - 2 NOT
                    requestTravelTime(zip_code_to, basicsettings.company_address)
                        .then(function (result) {
                            if (result.status == google.maps.DirectionsStatus.OK) {
                                var data = result.result;
                                var distance_zone_to_value = 10;
                                var distance_zone_from = data['routes'][0]['legs'][0]['distance']['text'];   //Distance from Zip To Zip
                                var time_zone_duration_from = (Math.round(data['routes'][0]['legs'][0]['duration']['value'] / 3600)).toFixed(2); // Apr. Driving Time.
                                time_zone_duration_from = parseFloat(time_zone_duration_from);

                                var distance_zone_from_value = data['routes'][0]['legs'][0]['distance']['value'] / 1609.34;

                                if (time_zone_duration_to <= 0.25)  time_zone_duration_to = 0.25;
                                if (time_zone_duration_from <= 0.25) time_zone_duration_from = 0.25;

                                var travel_time = time_zone_duration_from + time_zone_duration_to;

                                if (cuReq.duration > 1.5) travel_time = travel_time - 0.25; // Discount on travel time if driving time more then 1.5 hour.

                                var flat_distance = Math.max(distance_zone_from_value, distance_zone_to_value);

                                cuReq.travelTime = travel_time;

                                if (flat_distance > config.flatRateDistance) {
                                    cuReq.flatRate = true;
                                    cuReq.travelTime = Math.min(time_zone_duration_to, time_zone_duration_from);
                                }
                            }

                            deferred.resolve();
                        });

                } else {  // 2 LOCAL MOVES

                    var time_zone_duration_from = time_zone[zone_to];
                    if (time_zone_duration_to <= 0.25)  time_zone_duration_to = 0.25;
                    if (time_zone_duration_from <= 0.25) time_zone_duration_from = 0.25;

                    var travel_time = time_zone_duration_from + time_zone_duration_to;

                    if (cuReq.duration > 1.25) travel_time = travel_time - 0.25; // Discount on travel time if driving time more then 1.5 hour.

                    cuReq.travelTime = travel_time;

                    deferred.resolve();
                }
            }

            return deferred.promise;
        }


        function requestTravelTime (zipFrom, zipTo) {
            var request = {
                origin: zipFrom,
                destination: zipTo,
                travelMode: google.maps.TravelMode.DRIVING
            };

            var deferred = $q.defer();

            directionsService.route(request, function (result, status) {
                deferred.resolve({result: result, status: status});
            });

            return deferred.promise;
        }


        function getFlateRateTravelTime (cuReq) {
            var deferred = $q.defer();
            var calcsettings = angular.fromJson($rootScope.globals.front.calcsettings);

            if (calcsettings.flatTravelTime.isActive
                && cuReq.serviceType == 1) {

                var centerPostalCode = calcsettings.flatTravelTime.center_postalCode;
                requestTravelTime(centerPostalCode, cuReq.zipFrom)
                    .then(function (result) {
                        if (result.status == google.maps.DirectionsStatus.OK) {
                            var data = result.result;
                            var distance = Math.round(data['routes'][0]['legs'][0]['distance']['value'] / 1609.34);   //Distance from Zip To Zip in miles
                            var radius = calcsettings.flatTravelTime.radius;

                            if (distance <= radius) {
                                cuReq.flatRateTravelTime = true;
                                deferred.resolve(calcsettings.flatTravelTime.amount / 60 * 2); // convert to hour and multiple on 2
                            } else {
                                deferred.resolve(false);
                            }
                        } else {
                            deferred.resolve(false);
                        }
                    });
            } else {
                deferred.resolve(false);
            }

            return deferred.promise;
        }


        function getDuration (cuReq) {
            var deferred = $q.defer();
            var data = [];
            var start = cuReq.zipFrom;
            var end = cuReq.zipTo;

            var request = {
                origin: start,
                destination: end,
                travelMode: google.maps.TravelMode.DRIVING
            };

            directionsService.route(request, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    var data = result;
                    var distance = Math.round(data['routes'][0]['legs'][0]['distance']['value'] / 1609.34);   //Distance from Zip To Zip in miles
                    var duration = (Math.round(data['routes'][0]['legs'][0]['duration']['value'] / 3600)); // Apr. Driving Time.

                    cuReq.distance = distance;
                    cuReq.duration = duration;

                    deferred.resolve();
                }
                else {
                    // === if we were sending the requests to fast, try this one again and increase the delay
                    if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {

                        setTimeout(function () {
                            getDirection();
                        }, 2000);
                    }
                }
            });


            return deferred.promise;
        }

        function sendLogs (data, title, nid, type, request) {
            var currUser = $rootScope.currentUser;
            var userRole = '';
            var name = '';
            if(currUser) {
                if (currUser.userRole)
                  if (currUser.userRole.indexOf('administrator') >= 0) {
                     userRole = 'administrator';
                  } else if (currUser.userRole.indexOf('manager') >= 0) {
                      userRole = 'manager';
                  } else if (currUser.userRole.indexOf('sales') >= 0) {
                      userRole = 'sales';
                  } else if (currUser.userRole.indexOf('foreman') >= 0) {
                      userRole = 'foreman';
                  }
                  name = currUser.userId.field_user_first_name + ' ' + currUser.userId.field_user_last_name;
            }else{
                userRole = 'client';
                let isRequestHasFields = request.account === undefined && request.account.fields === undefined;
	            if(!isRequestHasFields) {
		            name = `${request.account.fields.field_user_first_name} ${request.account.fields.field_user_last_name}`;
	            }
            }

            var msg = {
                entity_id: nid,
                entity_type: type,
                data: [{
                    details: [{
                        activity: userRole,
                        title: title,
                        text: [],
                        date: moment().unix()
                    }],
                    source: name
                }]
            };

            var info = {
                browser: AuthenticationService.get_browser(),
                ip: '111.111.111.111'
            };

            msg.data[0].details[0].info = {};
            msg.data[0].details[0].info = info;

            angular.forEach(data, function (obj) {
                msg.data[0].details[0].text.push(obj);
            });

            var deferred = $q.defer();

            $http.post(config.serverUrl + 'server/new_log', msg)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });

            return deferred.promise;
        }
    }


    function get_readable_time (number) {

        var zero = '0';
        var zero2 = '0';
        var minutes = 0;
        var hour = Math.floor(number);
        var fraction = number - hour;

        // 0, 15min - 0.25 , 30min - 0.5 , 45 min - 0.75
        if (fraction > 0.15 && fraction < 0.36) {
            minutes = 15;
        }

        if (fraction >= 0.36 && fraction < 0.64) {
            minutes = 30;
        }

        if (fraction >= 0.64 && fraction < 0.87) {
            minutes = 45;
        }

        if (fraction >= 0.87) {
            minutes = 0;
            hour++;
        }

        if (minutes == 0 && hour != 1)
            return hour + ' hrs';
        else if (hour == 1 && minutes == 0)
            return hour + ' hr';
        else if (hour == 1 && minutes != 0)
            return hour + ' hr ' + minutes + ' min';
        else if (hour == 0)
            return minutes + ' min';
        else
            return hour + ' hrs ' + minutes + ' min';
    }


    function getStartTime (start_time) {
        var start_time1 = '';
        var start_time2 = '';

        if (start_time == 1) {
            start_time1 = '8:00 AM';
            start_time2 = '8:00 PM';
        }

        if (start_time == 2) {
            start_time1 = '8:00 AM';
            start_time2 = '10:00 AM';
        }

        if (start_time == 3) {
            start_time1 = '12:00 PM';
            start_time2 = '3:00 PM';
        }

        if (start_time == 4) {
            start_time1 = '1:00 PM';
            start_time2 = '4:00 PM';
        }

        if (start_time == 5) {
            start_time1 = '3:00 PM';
            start_time2 = '7:00 PM';
        }

        return {
            start_time1: start_time1,
            start_time2: start_time2,
        };
    }

})();
