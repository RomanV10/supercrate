(function () {

    var scripts = document.getElementsByTagName("script");
    var currentScriptPath = scripts[scripts.length - 1].src;
    var relativePathDiretive = currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/'));
    var relativePath = relativePathDiretive.substring(0, relativePathDiretive.lastIndexOf('/') + 1);

    angular
        .module('movecalc')
        .factory('FormHelpServices', FormHelpServices);

    function FormHelpServices() {
        var service = {};

        var animating = false;
        var current_fs, next_fs, previous_fs; //fieldsets
        var images = new Array();
        var left, opacity, scale; //fieldset properties which we will

        // Load and Show Rooms on Form
        var servicesType = [
            {id: 1, text: 'Moving'},
            {id: 2, text: 'Moving & Storage'},
            {id: 3, text: 'Loading Help'},
            {id: 4, text: 'Unloading Help'},
            {id: 5, text: 'Flat Rate'},
            {id: 6, text: 'Overnight'},
            {id: 7, text: 'Long Distance'},
        ];

        service.showOnFormSizeofMoveChanges = showOnFormSizeofMoveChanges;
        service.getBoxCount = getBoxCount;
        service.hideRooms = hideRooms;
        service.showRooms = showRooms;
        service.showHouseRooms = showHouseRooms;
        service.showRoomImg = showRoomImg;
        service.nextstep = nextstep;
        service.prevstep = prevstep;
        service.preload = preload;
        service.validateStep = validateStep;
        service.validateStepSmallCalc = validateStepSmallCalc;
        service.countdown = countdown;

        service.getRoomsString = getRoomsString;

        return service;

        function getRoomsString(rooms, roomsList) {
            var room_now = 0;
            var room_count = rooms.length;
            var extra_room_text = '';

            angular.forEach(rooms, function (value, key) {
                room_now++;
                if (room_now == 1 && room_count == room_now) {
                    extra_room_text = '(with ' + roomsList[value - 1].text + ')';
                }
                else if (room_now == 1 && room_count == 2) {
                    extra_room_text = '(with ' + roomsList[value - 1].text + ' and ';
                }
                else if (room_now == 2 && room_count == 2) {
                    extra_room_text = extra_room_text + roomsList[value - 1].text + ')';
                }
                else if (room_now == 1 && room_count > 2) {
                    extra_room_text = '(with ' + roomsList[value - 1].text + ', ';
                }
                else if (room_now == room_count) {
                    extra_room_text = extra_room_text + roomsList[value - 1].text + ')';
                }
                else if (room_now + 1 == room_count) {
                    extra_room_text = extra_room_text + roomsList[value - 1].text + ' and ';
                }
                else
                    extra_room_text = extra_room_text + roomsList[value - 1].text + ', ';

            });

            return extra_room_text;
        }


        function showOnFormSizeofMoveChanges(nval) {
            angular.element("#calc-info-steps .calc-intro").hide("slow").empty();

            // var serviceType = servicesType[nval].text;

            // angular.element("#calc-info-steps .box_info .service-type" ).empty().append('<h3>Service Type:</h3><span>'+angular.element('#edit-service option:selected').text()+'</span>').show("slow");

            var boxes = getBoxCount(angular.element("#edit-size-move").val());
            var boxcount = '<b>' + boxes[0] + '-' + boxes[1] + '</b>';

	        var enlarge_icon = '<i id="zoomimage" class="fa fa-search-plus fa-2x"></i>';
	        var size = angular.element('#edit-size-move').val();
	        if (size != 11) {
		        enlarge_icon += '<span class="boxcount"><i class="suitboxes"></i>' + boxcount + '</span>';
            }


            angular.element("#calc-info-steps .box_info .move-size").empty().append(enlarge_icon).append(SizeOfMoveTips(nval)).show(200);

            angular.element('.alerttip').hide();

            if (size == 8 || size == 9 || size == 10) {
                var title = 'In-home estimate';
                var text = 'Estimating is more like an art than a science. And we will say that it is human nature to underestimate just how much stuff you need to move. There is no better way to estimate than to have a professional mover visit your actual home. We recommend having free on-site estimate if your move is larger than 3-bedroom apartment and you think that it may be required more than one truck for your move.';

                angular.element("#calc-results2 h2.typeofent").text(title);
                angular.element("#calc-results2 .calc-intro_description.block2").text(text);
            } else {
                var title = 'Type Of Entrance';
                var text = 'If you 1st/ground floor apartment is located on a hill and involves outside stairs, please choose type of entrance accordingly to how many steps you have, NOT ground floor. 10-12 steps usually equal to one flight of stairs.';

                angular.element("#calc-results2 h2.typeofent").text(title);
                angular.element("#calc-results2 .calc-intro_description.block2").text(text);
            }
        }


        function getBoxCount(size) {
            var i = 0;
            var count = 0;

            if (size == 8 || size == 9 || size == 10) {
                angular.element('#edit-extra-house-furnished-rooms input[type=checkbox]').each(function () {
                    if (angular.element(this).is(":checked")) {
                        i++;
                    }
                });
            } else {
                angular.element('#edit-extra-furnished-rooms input[type=checkbox]').each(function () {
                    if (angular.element(this).is(":checked")) {
                        i++;
                    }
                });
            }

            switch (size) {
                case ('1'): //  Room
                    count = 5;
                    if (i == 0) {
                        var minC = 5;
                        var maxC = 10;

                        return [minC, maxC];
                    }
                    break;
                case ('2'): // Studio
                    count = 20;
                    if (i == 0) {
                        var minC = 20;
                        var maxC = 25;

                        return [minC, maxC];
                    }
                    break;
                case ('3'): // 1 Small Bedroom
                    count = 10;
                    break;
                case ('4'):  // 1 Large Bedroom
                    count = 20;
                    break;
                case ('5'): // 2 Small Bedroom
                    count = 20;
                    break;
                case ('6'): //2 Large Bedroom
                    count = 30;
                    break;
                case ('7'): // 3 Bedroom
                    count = 50;
                    if (i == 1) {
                        var minC = 50;
                        var maxC = 60;

                        return [minC, maxC];
                    }
                    break;
                case ('8'): // House
                    count = 50;
                    break;
                case ('9'): // House
                    count = 60;
                    break;
                case ('10'): // House
                    count = 70;
                    break;
            }

            var minC = count + i * 5;
            var maxC = count + i * 10;

            return [minC, maxC];
        }

        function hideRooms() {
            for (var i = 1; i <= 9; i++) {
                angular.element('.form-item-Extra-Furnished-Rooms-' + i).hide();
            }

            angular.element('.form-item-Extra-Furnished-Rooms-5').show();
        }

        function showRooms() {
            jQuery(document).ready(function ($) {
                angular.element('.form-item-Extra-Furnished-Rooms').show();

                for (var i = 1; i < 5; i++) {
                    angular.element('.form-item-Extra-Furnished-Rooms-' + i).show();
                    if (i == 2)  angular.element('.form-item-Extra-Furnished-Rooms-' + i).removeClass('click-disabled');
                }
                for (var i = 6; i <= 8; i++) {
                    angular.element('.form-item-Extra-Furnished-Rooms-' + i).hide();
                }
            });

        }

        function showHouseRooms() {
            angular.element('.form-item-Extra-Furnished-Rooms').show();

            for (var i = 1; i < 9; i++) {
                if (i == 2) {
                    angular.element('.form-item-Extra-Furnished-Rooms-' + i).addClass('click-disabled');
                }

                angular.element('.form-item-Extra-Furnished-Rooms-' + i).show();
            }

            angular.element("#edit-type-from option[value='7']").attr("selected", "selected");
            angular.element("#edit-type-from").val('7').attr("disabled", "disabled").addClass("disabled");

            angular.element("#edit-type-from").val('7').attr("disabled", "disabled").addClass("disabled");
            angular.element("#edit-type-to").val('7');
        }

        function showRoomImg(roomId, rooms) {

            var roomVal = roomId;
            var boxes = getBoxCount(angular.element("#edit-size-move").val());
            var boxcount = '<b>' + boxes[0] + '-' + boxes[1] + '</b>';

            angular.element(".boxcount b").remove();
            angular.element(".boxcount").append(boxcount);

            if (IsRoom(rooms, roomId)) {
                angular.element("#calc-info-steps .box_info .move-size .size-info-block").append(ShowRoom(roomId)).show(200);
                var src = GetBigRoomSrc(roomId);
                preload(src);
            } else {
                hideRoom(roomId);
            }
        }

        function GetBigRoomSrc(room) {
            var roomSize = angular.element("#edit-size-move").val();

            switch (room) {
                case (2): // Dinnig Room
                    return relativePath + '/movecalculator/views/img/rooms/dinning.png';
                case (3): // Office
                    return relativePath + '/movecalculator/views/img/rooms/office.png';
                case (4): // Extra room
                    return relativePath + '/movecalculator/views/img/rooms/extraroom.png';
                case (5): // Basement
                    return relativePath + '/movecalculator/views/img/rooms/basement.png';
                case (6): // Garage
                    return relativePath + '/movecalculator/views/img/rooms/garage.png';
                case (7): // Patio
                    return relativePath + '/movecalculator/views/img/rooms/smpatio.png';
                case (8): // Play
                    return relativePath + '/movecalculator/views/img/rooms/playroom.png';
                default:
                    return '';
            }
        }

        function ShowBigRoom(room) {
            var roomSize = angular.element("#edit-size-move").val();

            switch (room) {
                case (2): // Dinnig Room
                    return '<img class="big_dinning_room_' + roomSize + '" title="Dinning Room" src="' + relativePath + '/rooms/dinning.png">';
                case (3): // Office
                    return '<img class="big_office_room_' + roomSize + '" title="Office" src="' + relativePath + '/rooms/office.png">';
                case (4): // Extra room
                    return '<img class="big_extra_room_' + roomSize + '" title="Extra room" src="' + relativePath + '/rooms/extraroom.png">';
                case (5): // Basement
                    return '<img class="big_basement_room" title="Basement" src="' + relativePath + '/rooms/basement.png">';
                case (6): // Garage
                    return '<img class="big_garage_room" title="Garage" src="' + relativePath + '/rooms/garage.png"></a>';
                case (7): // Patio
                    return '<img class="big_patio_room" title="Patio" src="' + relativePath + '/rooms/smpatio.png">';
                case (8): // Play
                    return '<img class="big_play_room_' + roomSize + '" title="Play Room" src="' + relativePath + '/rooms/playroom.png"></a>';
                default:
                    return '';
            }
        }

        function ShowRoom(room) {
            var roomSize = angular.element("#edit-size-move").val();

            switch (room) {
                case (2): // Dinnig Room
                    return '<img class="dinning_room" title="Dinning Room" src="' + relativePath + '/rooms/smdr.png">';
                case (3): // Office
                    return '<img class="office_room_' + roomSize + '" title="Office" src="' + relativePath + '/rooms/smoffice.png">';
                case (4): // Extra room
                    return '<img class="extra_room_' + roomSize + '" title="Extra room" src="' + relativePath + '/rooms/smextraroom.png">';
                case (5): // Basement
                    return '<img class="basement_room" title="Basement" src="' + relativePath + '/rooms/smbasement.png">';
                case (6): // Garage
                    return '<img class="garage_room" title="Garage" src="' + relativePath + '/rooms/garage.png">';
                case (7): // Patio
                    return '<img  class="patio_room" title="Patio" src="' + relativePath + '/rooms/smpatio.png">';
                case (8): // Play
                    return '<img class="play_room_' + roomSize + '" title="Play" src="' + relativePath + '/rooms/playroom.png">';
                default:
                    return '';
            }
        }

        function hideRoom(roomId) {
            var roomVal = roomId;
            var roomSize = angular.element("#edit-size-move").val();
            switch (roomVal) {
                case (2): // Dinnig Room
                    angular.element('.dinning_room').remove();
                    break;
                case (3): // Dinnig Room
                    angular.element('.office_room_' + roomSize).remove();
                    break;
                case (4): // Dinnig Room
                    angular.element('.extra_room_' + roomSize).remove();
                    break;
                case (5): // Dinnig Room
                    angular.element('.basement_room').remove();
                    break;
                case (6): // Dinnig Room
                    angular.element('.garage_room').remove();
                    break;
                case (7): // Dinnig Room
                    angular.element('.patio_room').remove();
                    break;
                case (8): // Play Room
                    angular.element('.play_room_' + roomSize).remove();
                    break;
            }
        }

        function SizeOfMoveTips(type) {
            var tips_text = 'Please choose <b>Large</b> rather than <b>Small</b>, if your furniture includes a lot of bulky, over-sized and/or antique items, ones with glass, marble and/or that require a lot of disassembling/reassembling. These items require extra care and time to handle and in most cases we recommend an additional mover for greatest efficiency.'

            angular.element("#size-intro").hide();
            angular.element("#sf-move-size").removeClass("empty");
            relativePath = 'https://themoveboard.com/elromco';

            switch (type) {
                case ('1'):
                    addRequired();
                    angular.element('.form-item-Extra-Furnished-Rooms').show();
                    angular.element('.form-item-Extra-House-Furnished-Rooms').hide();

                    return '<h3>Size of Move:</h3><span class="msize company-color" data-room="room" data-href="' + relativePath + '/rooms/roombig.png">Room or less</span><i class="rooms"></i><div class="size-info-block"><img class="smroom" src="' + relativePath + '/rooms/room.jpg"><div class="desc"><em>Room or less.</em> Includes content of furniture for one room only with approximately 5-10 assorted size boxes, suitcases and totes.</div>';
                case ('2'):
                    angular.element('.form-item-Extra-Furnished-Rooms').show();
                    angular.element('.form-item-Extra-House-Furnished-Rooms').hide();
                    addRequired();

                    return '<h3>Size of Move:</h3><span class="msize company-color" data-room="studio" data-href="' + relativePath + '/rooms/studiobg.png">Studio apartment</span><i class="rooms"></i><div class="size-info-block"><img class="studioimg" title="Studio" src="' + relativePath + '/rooms/studio.png"><div class="desc"><em>Studio</em> size apartment typically consist of one large room, which serves as the living, dining and bedroom.</div>';
                case ('3'):
                    addRequired();

                    return '<h3>Size of Move:</h3><span class="msize company-color" data-room="onebedroom" data-href="' + relativePath + '/rooms/1bgsmall.png">Small 1 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="size-info-block"><img class="onebedrimg" title="Small 1 Bedroom Apartment" src="' + relativePath + '/rooms/sm1sm.png"><div class="desc">Size of your apartment, Small or Large,<b> depends on the amount of furniture and miscellaneous items that have to be moved!</b> NOT on square footage!<i class="tooltips fa fa-info-circle company-color"><div class="tips rmtips">' + tips_text + '</div></i></div>';
                case ('4'):
                    addRequired();

                    return '<h3>Size of Move:</h3><span class="msize company-color" data-room="lonebedroom" data-href="' + relativePath + '/rooms/1bigsm.png">Large 1 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="size-info-block"><img title="Large 1 Bedroom Apartment" src="' + relativePath + '/rooms/sm1lar.png"><div class="desc">Size of your apartment, Small or Large,<b> depends on the amount of furniture and miscellaneous items that have to be moved!</b> NOT on square footage!<i elromcotooltips class="tooltips fa fa-info-circle company-color"><div class="tips rmtips">' + tips_text + '</div></i></div>';
                case ('5'):
                    addRequired();

                    return '<h3>Size of Move:</h3><span class="msize company-color" data-room="stwobedroom" data-href="' + relativePath + '/rooms/sm2.png">Small 2 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="size-info-block"><img title="Small 2 Bedroom Apartment" src="' + relativePath + '/rooms/sm2sm.png"><div class="desc">Size of your apartment, Small or Large,<b> depends on the amount of furniture and miscellaneous items that have to be moved!</b> NOT on square footage!<i class="tooltips fa fa-info-circle company-color"><div class="tips rmtips">' + tips_text + '</div></i></div>';
                case ('6'):
                    addRequired();

                    return '<h3>Size of Move:</h3><span class="msize company-color" data-room="ltwobedroom" data-href="' + relativePath + '/rooms/large2.png">Large 2 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="size-info-block"><img title="Large 2 Bedroom Apartment" src="' + relativePath + '/rooms/sm2lar.png"><div class="desc">Size of your apartment, Small or Large,<b> depends on the amount of furniture and miscellaneous items that have to be moved!</b> NOT on square footage!<i class="tooltips fa fa-info-circle company-color"><div class="tips rmtips">' + tips_text + '</div></i></div>';
                case ('7'):
                    addRequired();

                    return '<h3>Size of Move:</h3><span class="msize company-color" data-room="3bedroom" data-href="' + relativePath + '/rooms/3bedbg.png">3 Bedroom Apartment</span><i class="rooms">(with living room)</i><div class="size-info-block"><img title="3 Bedroom Apartment" src="' + relativePath + '/rooms/sm3bed.png"><div class="desc"></div>';
                case ('8'):

                    return '<h3>Size of Move:</h3><span class="msize company-color" data-room="house2" data-href="' + relativePath + '/rooms/2house.png">House/Townhouse with 2 Bedrooms</span><i class="rooms">(with living room and dining room)</i><div class="size-info-block"><img title="Large 2 Bedroom Apartment" src="' + relativePath + '/rooms/sm2house.png"><div class="desc"></div>';
                case ('9'):

                    return '<h3>Size of Move:</h3><span class="msize company-color" data-room="house3" data-href="' + relativePath + '/rooms/3house.png">House/Townhouse with 3 Bedrooms</span><i class="rooms">(with living room and dining room)</i><div class="size-info-block"><img title="3 Bedroom Apartment" src="' + relativePath + '/rooms/sm3house.png"><div class="desc"></div>';
                case ('10'):

                    return '<h3>Size of Move:</h3><span class="msize company-color"  data-room="house4" data-href="' + relativePath + '/rooms/4house.png">House/Townhouse with 4 Bedrooms</span><i class="rooms">(with living room and dining room)</i><div class="size-info-block"><img title="4 Bedroom House" src="' + relativePath + '/rooms/sm4house.png"><div class="desc"></div>';

                case ('11'):
	                return '<h3>Size of Move:</h3><span class="msize company-color" data-room="commercial" data-href="' + relativePath + '/rooms/commercial-moving-img.jpg">Commercial move</span><div class="size-info-block"><img title="Commercial Move" src="' + relativePath + '/rooms/commercial-moving-img.jpg"><div class="desc"></div>';

                default:

                    return '<div class="calc-intro"><h1 class="calc-intro_heading">Choose Size</h1><p class="size-info-block" > Please choose size of your move.</p></div>';
            }
        }

        function addRequired() {
            var service = angular.element('#edit-service').val();

            if (angular.element('#edit-service').val() != '3' && angular.element('#edit-service').val() != '4') {
                angular.element("#edit-type-from").removeAttr("disabled");
                angular.element("#edit-type-from").removeClass("disabled");
            }
        }

        function nextstep(current) {

            if (animating) {
                return false;
            }

            animating = true;

            //activate next step on progressbar using the index of next_fs

            current_fs = angular.element("#movecalc-moving-form " + current);
            next_fs = angular.element("#movecalc-moving-form " + current).next();

            angular.element("#progressbar li").eq(angular.element("#movecalc-moving-form fieldset").index(next_fs)).addClass("active");
            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale(' + scale + ')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;

                    // angular.element("#progressbar li").eq(angular.element("fieldset").index(current_fs)).removeClass("active");

                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });

            return false;
        }

        function prevstep(current) {

            if (animating) {
                return false;
            }

            animating = true;

            current_fs = angular.element("#movecalc-moving-form " + current);
            previous_fs = angular.element("#movecalc-moving-form " + current).prev();
            //de-activate current step on progressbar
            angular.element("#progressbar li").eq(angular.element("fieldset").index(current_fs)).removeClass("active");

            //de-activate current step on progressbar
            angular.element("#progressbar li").eq(angular.element("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1 - now) * 50) + "%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                    angular.element("#progressbar li").eq(angular.element("#movecalc-moving-form fieldset").index(current_fs)).removeClass("active");
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });

            return false;
        }

        function IsRoom(room, rid) {
            var i = 0, len = room.length;
            for (; i < len; i++) {
                if (room[i] == rid) {
                    return true;
                }
            }
            return false;
        }

        function validateStepSmallCalc(step) {
            var hasError = false;
            if (step === "step1") {
                angular.element("#step1").find('.required').each(function () {
                    var $this = angular.element(this);

                    var valueLength = jQuery.trim($this.val()).length;

                    if ($this.hasClass("desktopzip")) {
                        if ((valueLength == 0) || $this.hasClass("error")) {
                            hasError = true;
                            $this.removeClass('success');
                            $this.addClass('error');
                        } else {
                            $this.removeClass('error');
                            $this.addClass('success');
                        }
                    } else if ($this.hasClass("er-picker__input")) {
                        if (valueLength == 0) {
                            hasError = true;
                            $this.removeClass('success');
                            $this.addClass('error');
                        } else {
                            $this.removeClass('error');
                            $this.addClass('success');
                        }
                    } else {
                        if ((valueLength == 0) || $this.hasClass("error")) {
                            hasError = true;
                            $this.removeClass('success');
                            $this.addClass('error');
                        } else {
                            $this.removeClass('error');
                            $this.addClass('success');
                        }
                    }


                });
                return hasError;
            } else if (step === "step2") {
                angular.element("#step2").find('.required').each(function () {
                    var $this = angular.element(this);
                    var valueLength = jQuery.trim($this.val()).length;
                    if (((valueLength == 0) || $this.hasClass("error")) && ($this.attr("id") !== "sf-move-size")) {
                        hasError = true;
                        $this.removeClass('success');
                        $this.addClass('error');
                    } else {
                        $this.removeClass('error');
                        $this.addClass('success');
                    }

                    if (($this.attr("id") == "sf-move-size") && (!$this.find(".movesizeinput").hasClass("ng-hide"))) {
                        hasError = true;
                        $this.removeClass('success');
                        $this.addClass('error');
                    } else {
                        $this.removeClass('error');
                        $this.addClass('success');
                    }

                    if ($this.attr("id") == "edit-type-from") {
                        var a = $this.find(":selected").val();
                        if (a == "") {
                            hasError = true;
                            $this.removeClass('success');
                            $this.addClass('error');
                        } else {
                            $this.removeClass('error');
                            $this.addClass('success');
                        }
                    }

                });

                return hasError;
            } else if (step === "step3") {
                angular.element("#step3").find('.required').each(function () {
                    var $this = angular.element(this);
                    var valueLength = jQuery.trim($this.val()).length;

                    if ((valueLength == 0) || $this.hasClass("error")) {
                        hasError = true;
                        $this.removeClass('success');
                        $this.addClass('error');
                    } else {
                        $this.removeClass('error');
                        $this.addClass('success');
                    }

                    if ($this.attr("id") == "edit-email") {
                        if (IsEmail($this.val()) == false) {
                            hasError = true;
                            $this.removeClass('success');
                            $this.addClass('error');
                        } else {
                            $this.removeClass('error');
                            $this.addClass('success');
                        }
                    }

                    if ($this.attr("id") == "primary_phone") {
                        if (valueLength !== 14) {
                            hasError = true;
                            $this.removeClass('success');
                            $this.addClass('error');
                        } else {
                            $this.removeClass('error');
                            $this.addClass('success');
                            angular.element("#summery-alert").empty().hide();
                        }
                    }
                });
                return hasError;
            }
        }

        function validateStep(step) {
            //if (step == w_fieldsetCount) return;
            var hasError = false;
            angular.element(step).find('.required').each(function () {
                var $this = angular.element(this);
                var valueLength = jQuery.trim($this.val()).length;

                if (valueLength == 0) {
                    hasError = true;
                    $this.addClass('error');
                } else {
                    $this.removeClass('error');
                }


                if ($this.attr("id") == "edit-email") {
                    if (IsEmail($this.val()) == false) {
                        // showAlert("1");
                        $this.addClass('error');
                        hasError = true;
                    } else {
                        $this.removeClass('error');
                    }
                }

                if ($this.attr("id") == "sf-move-size") {
                    $this.removeClass('error');
                    if ($this.hasClass('empty')) {
                        // showAlert("1");
                        $this.addClass('error');
                        hasError = true;
                    }
                }

                if ($this.attr("id") == "primary_phone") {
                    if (valueLength == 14) {
                        $this.removeClass("error");
                        angular.element("#summery-alert").empty().hide();
                    } else {
                        $this.addClass("error");
                        hasError = true;
                    }
                }
            });

            return hasError;
        }

        function preload() {
            for (var i = 0; i < arguments.length; i++) {
                images[i] = new Image()
                images[i].src = arguments[i]
            }
        }


        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            return regex.test(email);
        }


        function countdown() {
            angular.element('.statistic').each(function () {
                angular.element(".sum").appear(function () {
                    var counter = angular.element(this).html();
                    angular.element(this).countTo({
                        from: 0,
                        to: counter,
                        speed: 2000,
                        refreshInterval: 60
                    });
                });
            });
        }
    }
})();
