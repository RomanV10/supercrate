'use strict';

angular
	.module('movecalc')
	.factory('CommercialCalc', CommercialCalc);

CommercialCalc.$inject = [];

function CommercialCalc() {
	return {
		getCommercialCubicFeet: getCommercialCubicFeet
	}

	function getCommercialCubicFeet(requestExtra, calcSettings) {
		let totalCommercialWeight = Number(calcSettings.size['11']);
		let extraRooms;
		if (requestExtra && requestExtra.length) {
			extraRooms = requestExtra;
		}
		if (extraRooms !== undefined) {
			let chosenCommercialItem = calcSettings.commercialExtra.find(item => {
				if (item.id === extraRooms) {
					return item;
				}
			});
			if (chosenCommercialItem && chosenCommercialItem.cubic_feet) {
				totalCommercialWeight += chosenCommercialItem.cubic_feet;
			}
		}
		return {
			weight: totalCommercialWeight,
			min: (totalCommercialWeight - 100 < 0) ? totalCommercialWeight : totalCommercialWeight - 100,
			max: totalCommercialWeight + 100
		};
	}
}
