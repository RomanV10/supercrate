(function () {
    'use strict';

    angular
        .module('movecalc')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', '$rootScope', '$q', 'Session', 'config', '$location'];

    function AuthenticationService($http, $rootScope, $q, Session, config, $location) {
        var service = {};
        service.Login = Login;
        service.Logout = Logout;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
        service.isAuthenticated = isAuthenticated;
        service.isAuthorized = isAuthorized;
        service.GetCurrent = GetCurrent;
        service.getToken = getToken;
        service.GetFront = GetFront;
        service.getClientActions = getClientActions;
        service.checkunique = checkunique;
        service.sendLog = sendLog;
        service.getUniqueUser = getUniqueUser;
        service.get_browser = get_browser;
        service.getVisitorTimer = getVisitorTimer;
        return service;

        function checkunique(mail, callback) {

            /* Dummy authentication for testing, uses $timeout to simulate api call
             ----------------------------------------------*/


            var data = {}
            data.email = mail;

            $http
                .post(config.serverUrl + 'server/clients/checkunique', data)
                .success(function (data, status, headers, config) {


                    callback(data[0]);


                })
                .error(function (data, status, headers, config) {


                    callback(data[0]);

                });


        }

        function getToken() {
            $http.get(config.serverUrl + 'services/session/token').success(function (data) {

                var token = data;
                Session.saveToken(unescape(encodeURIComponent(token)));
                return unescape(encodeURIComponent(token));

            });

        }

        function Login(username, password, callback) {
            /* Dummy authentication for testing, uses $timeout to simulate api call
             ----------------------------------------------*/

            var credentials = {username: username, password: password};

			  $http
				  .post(config.serverUrl + 'server/move_users_resource/login', credentials)
				  .then(res => {
						  if (res.data.status_code && res.data.status_code != 200) {
							  onWrongLogin(res.data);
						  } else {
							  onWriteLogin(res.data);
						  }
					  }, rej => {
						  onWrongLogin(rej);
					  });

			  function onWrongLogin(data) {
				  if (data == null) {
					  data = [];
				  }
				  data.message = 'Username or password is incorrect';
				  data.success = false;

				  callback(data);
			  }
			  function onWriteLogin(data) {
				  data.success = true;

				  Session.create(data.token, data.user, data.user.role);
				  Session.saveToken(data.token);

				  callback(data);
			  }

        }

        function Logout(callback) {
            /* Dummy authentication for testing, uses $timeout to simulate api call
             ----------------------------------------------*/
            var deferred = $q.defer();
            $http
                .post(config.serverUrl + 'server/user/logout')
                .success(function (data, status, headers, config) {
                    Session.destroy();
                    deferred.resolve(data);
                    callback(true);
                })
                .error(function (data, status, headers, config) {
                    callback(true);
                    return deferred.reject(data);
                });


            return deferred.promise;
        }

        function GetCurrent(callback) {
            var deferred = $q.defer();

            if (!this.isAuthenticated()) {

                $http.post(config.serverUrl + 'server/clients/getcurrent').success(function (data, status, headers, conf) {
                    if (data[0] == false) {
                        data.success = false;
                        deferred.reject(data);
                    }
                    else {
                      if (data == null) {
                        data = [];
                      }
                      data.success = true;
                        Session.create(data.uuid, data, data.roles);
                        callback(data);
                    }

                    deferred.resolve(data);

                }).error(function (data, status, headers, config) {

                    return deferred.reject(data);
                });

            }
            else {
                deferred.resolve(Session);
            }

            return deferred.promise;
        }


        //GET FRONT
        function GetFront(callback) {
            var deferred = $q.defer();

            $http
                .post(config.serverUrl + 'server/front/frontpage')
                .success(function (data, status, headers, config) {
                    data.success = true;
                    $rootScope.fieldData = data;
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    if (data == null) {
                        data = [];
                    }
                    data.success = false;
                    Session.destroy();
                    deferred.reject(data);
                });

            return deferred.promise;
        }


        function SetCredentials(data) {
            $rootScope.globals = {
                front: data
            };
        }

        function ClearCredentials() {
            $rootScope.globals = {};
        }


        function isAuthenticated() {
            return !!Session.userId;
        };

        function isAuthorized(authorizedRoles) {

            if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }
            return (service.isAuthenticated() &&
            authorizedRoles.indexOf(Session.userRole) !== -1);

        }

        function getVisitorTimer() {
            var session = {
                start: new Date().getTime(),
                end: getEnd()
            };

            function getEnd() {
                if (window.onbeforeunload != null) {
                    return new Date().getTime();
                }
                return null;
            }

            return session;
        }

        setInterval(function () {
            getVisitorTimer();
        }, 1000);

        function getClientActions(id, int) {
            var userAction = {
                data: {
                    type: 12,
                    form: int,
                    browser_type: getDeviceType(),
                    browser_name: get_browser(),
                    page_url: $location.absUrl(),
                    value: {
                        step: id
                    }
                }
            };

            if (userAction.data.value.step == false) {
                return false;
            }

            $http.post(config.serverUrl + 'server/move_statistics', userAction);
        }

        function getUniqueUser() {
            var newUserData = {
                data: {
                    type: 0,
                    browser_type: getDeviceType(),
                    browser_name: get_browser(),
                    value: {
                        url: $location.absUrl()
                    }
                }
            };

            $http.post(config.serverUrl + 'server/move_statistics', newUserData);
        }

        function get_browser() {
            var ua = navigator.userAgent,
                tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return {name: 'IE', version: (tem[1] || '')};
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\bOPR\/(\d+)/)
                if (tem != null) {
                    return {name: 'Opera', version: tem[1]};
                }
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/(\d+)/i)) != null) {
                M.splice(1, 1, tem[1]);
            }
            return M[0];
        }

        function getDeviceType() {
            var mobile = /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
                tablet = window.innerWidth < 700;
          if (mobile) {
            return 3;
          }
          if (tablet) {
            return 2;
          } else {
            return 1;
          }
        }

        function sendLog(log_type, log_array) {
            var deferred = $q.defer();
            deferred.resolve();
            /*
             var data =
             {
             "log_type": log_type,
             "data": log_array
             }

             $http
             .post(config.serverUrl + 'server/admin_logs', data)
             .success(function (data) {
             deferred.resolve(data);
             })
             .error(function (data) {
             return deferred.reject(data);
             });*/


            return deferred.promise;
        }

    }
})();
