(function () {
    'use strict';

    angular
        .module('movecalc')
        .controller('searchModalController', searchModalController);

    searchModalController.$inject = ['$scope','$http','$uibModalInstance','allowedstate','config'];

    function searchModalController($scope, $http, $uibModalInstance, allowedstate, config){
        $scope.getZipCode = getZipCode;
        $scope.chooseZipCode = chooseZipCode;
        $scope.allowedstate = allowedstate;

        function getZipCode() {
            var city = $scope.city;
            var state = $scope.state;
            var params = city + ',' + state;
            var searchURL = config.serverUrl+'server/front/get_geo_names';
            $scope.zipCodes = [];

            $http({
                method: 'POST',
                url: searchURL,
                data: {'params':params}
            }).success(function (data, status, headers, config) {
                    $scope.zipResults = true;
                    data = JSON.parse(data);
                    if (data.postalCodes.length) {
                        $scope.zipCodes = data.postalCodes;
                        $scope.zipMessage = 'Select a Zip';
                    }
                    else {

                        $scope.zipMessage = 'Zip Code Not Found';
                    }
                })
                .error(function (data, status, headers, config) {
                    $scope.zipMessage = 'Zip Code Not Found';
                    $scope.zipResults = false;
                });
        }
        function chooseZipCode(postalCode) {
			  $uibModalInstance.close(postalCode);
        }
    }
})();


