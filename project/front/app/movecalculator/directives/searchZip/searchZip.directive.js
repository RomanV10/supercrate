import tmpSearchZip from './searchZip.template.html';

(function () {
    'use strict';
    var scripts = document.getElementsByTagName("script")
    var currentScriptPath = scripts[scripts.length - 1].src;
    var relativePathDiretive = currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/'));
    var relativePath = relativePathDiretive.substring(0, relativePathDiretive.lastIndexOf('/') + 1);

    angular
        .module('movecalc')
        .directive('searchZip', searchZip);

    searchZip.$inject = ['$rootScope', '$http', '$uibModal'];

    function searchZip($rootScope, $http, $uibModal) {
        return {
            restrict: 'E',
            template: '<i class="fa fa-search"  ng-click="openModal()"></i>',
            scope: {
                targetZip: '=',
                allowedstate: '='
            },
            link: function (scope, element, attrs) {


                scope.zipResults = false;
                scope.zipMessage = '';
                scope.openModal = openModal;

                function openModal(){
                    var searchModal = $uibModal.open({
                        templateUrl: tmpSearchZip,
                        controller: 'searchModalController',
                        size: 'md',
                        resolve: {
                            allowedstate: function () {
                                return scope.allowedstate;
                            }
                        },
                        'windowClass': 'search-zip-modal'
                    });

                    searchModal.result.then(function (selectedZip) {
                        scope.targetZip = selectedZip;
                    }, function () {
                    });
                }
            }
        };
    }

})();
