'use strict';

angular
	.module('movecalc')
	.directive('googleMapRoute', googleMapRoute)
	.controller('googleMapRouteCtrl', googleMapRouteCtrl);

function googleMapRoute() {
	return {
		restrict: 'A',
		scope: {
			addressFrom: '=',
			addressTo: '=',
		},
		controller: 'googleMapRouteCtrl',
	};
}

googleMapRouteCtrl.$inject = ['$scope', '$element', '$timeout'];

function googleMapRouteCtrl($scope, $element, $timeout) {
	const CHECK_VISIBILITY_PERIOD = 1000;
	const DRAW_ROUTE_DELAY = 500;
	
	let directionsDisplay = new google.maps.DirectionsRenderer();
	let directionsService = new google.maps.DirectionsService();
	let route;
	let mapObject;
	initMap($element[0]);
	retrieveRoute();
	
	function makeAddress(addressObject) {
		let fullAddressString = addressObject;
		
		if (angular.isObject(addressObject)) {
			fullAddressString = Object.values(addressObject)
				.join(', ');
		} else {
			fullAddressString = `postal code: ${fullAddressString}`;
		}
		
		return fullAddressString;
	}
	
	function initMap(element) {
		var myOptions = {
			draggable: false,
			navigationControl: false,
			scrollwheel: false,
			streetViewControl: false,
			zoom: 5,
			maxZoom: 14,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: new google.maps.LatLng(41, -102),
		};
		
		mapObject = new google.maps.Map(element, myOptions);
		directionsDisplay.setMap(mapObject);
	}
	
	function retrieveRoute() {
		let fullAddressFrom = makeAddress($scope.addressFrom);
		let fullAddressTo = makeAddress($scope.addressTo);
		let directionsRequest = {
			origin: fullAddressFrom,
			destination: fullAddressTo,
			travelMode: google.maps.DirectionsTravelMode.DRIVING,
			unitSystem: google.maps.UnitSystem.METRIC
		};
		directionsService.route(directionsRequest, callbackForRoute);
	}
	
	function drawRoute() {
		if ($element.is(':visible')) {
			$timeout(() => {
				directionsDisplay.setDirections(route);
			}, CHECK_VISIBILITY_PERIOD);
		} else {
			$timeout(drawRoute, DRAW_ROUTE_DELAY);
		}
	}
	
	function callbackForRoute(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			route = result;
			drawRoute();
		} else {
			angular.element('#error').append('Unable to retrieve your route<br />');
		}
		
	}
}