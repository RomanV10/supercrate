import tmpUserMenupopup from './userMenuPopup.template.html';
(function() {
    'use strict';

    var scripts = document.getElementsByTagName("script");
    var currentScriptPath = scripts[scripts.length-1].src;
    var relativePathDiretive = currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/'));
    var relativePath = relativePathDiretive.substring(0, relativePathDiretive.lastIndexOf('/') + 1);

    angular
        .module('movecalc')
        .directive('userMenupopup',userMenuPopup);

    function userMenuPopup($rootScope,$http) {
        return {
            restrict: 'E',
            templateUrl: tmpUserMenupopup,
            link: function (scope,element,attrs) {


            }
        };

    }


})();
