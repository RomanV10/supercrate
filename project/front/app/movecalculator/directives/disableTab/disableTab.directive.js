'use strict';
angular.module('movecalc').directive('disableTab', disableTab);

function disableTab(){
	return {
		restrict: 'AC',
		controller: disableTabController,
		scope:{
			withSiblings: '=',
			id: '@'
		}
	};
	function disableTabController($scope, $element){
		$element[0].addEventListener('keydown', filterTab, true);
		$element[0].addEventListener('keyup', filterTab, true);
		$element[0].addEventListener('keypress', filterTab, true);
		function filterTab(event){
			if (event.code == 'Tab' &&
				(($scope.withSiblings && $element.find(event.target).length > 0) ||
				(!$scope.withSiblings && $element[0].id == $scope.id))) {
				event.stopPropagation();
				event.preventDefault();
			}
		}
	}
}