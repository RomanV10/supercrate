'use strict';

angular
	.module('movecalc')
	.directive('changeMoveDateProcess', changeMoveDateProcess);

function changeMoveDateProcess() {
	return {
		restrict: 'A',
		require: '?ngModel',
		link: linkFunction
	};

	function linkFunction(scope, element, attrs) {
		scope.$watch(attrs.ngModel, function (date) {
			if (!angular.isUndefined(date)) {
				if (attrs.id == 'edit-move-date-datepicker-popup-0') {
					angular.element("#edit-move-date-datepicker-popup-0")
						.removeClass('error');

					angular.element("#calc-info-steps .calc-intro")
						.hide();

					angular.element("#calc-info-steps .box_info")
						.show();

					angular.element("#calc-info-steps .box_info .moving-date")
						.empty()
						.append('<h3>Move Date:</h3> <span class="company-color">' + date + '</span>')
						.show("slow");
				}
			}
		});
	}
}
