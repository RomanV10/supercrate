export default function compatibilityFixes() {
	//for wordPress with beaverBuilder
	var badModule = angular.module("ui.bootstrap").requires;
	var index = -1;
	for (var f in badModule) {
		if (badModule[f] == "ui.bootstrap.tabs")
			index = f;
	}
	if (index >= 0) badModule.splice(index,1);
}