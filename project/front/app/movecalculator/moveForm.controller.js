import tmpCongrats from './views/congratsStep.html';
import tmpLocalmoveCalcResult from './views/localmoveCalcResult.html';
import tmpPersonalInfoStep from './views/personalInfoStep.html';
import tmpStorageTemplate from './views/storageTemplate.html';
import tmpFlatrateCalcResult from './views/flatrateCalcResult.html';
import tmpReviewSubmitStep from './views/reviewSubmitStep.html';
import tmpStorageReviewSubmit from './views/storageReviewSubmit.html';
import tmpCongratsStorage from './views/congratsStorage.html';
import compatibilityFixes from './compatibilityFixes';


'use strict';

const COMMERCIAL_MOVE_SIZE = '11';
const PENDING_STATUS = 1;
const FLAT_RATE_FIRST_STEP_STATUS = 9;
const COOKIE_UID = 'UID';
var scripts = document.getElementsByTagName('script');
var currentScriptPath = scripts[scripts.length - 1].src;
var relativePathDiretive = currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') + 1);
var relativePath = relativePathDiretive.substring(0, relativePathDiretive.lastIndexOf('/') + 1);
var b = angular.element('body');

angular.module('movecalc',
	[
		'ngMask',
		'checklist-model',
		'app.widgets', // needs core
		'angular-google-analytics',
		'app.calculator',
		'oitozero.ngSweetAlert',
		'ngSanitize',
		'slidePushMenu',
		'LocalStorageModule',
		'angular-datepicker',
		'ui.bootstrap',
		'shared-geo-coding',
		'commonServices',
		'phone-numbers',
	]);

compatibilityFixes();

angular
	.module('movecalc')
	.controller('movecalController', movecalController);

movecalController.$inject = [
	'$scope', '$rootScope', '$http', '$location', '$sce', '$anchorScroll',
	'$filter', '$q', '$window', 'Analytics', 'config', 'AuthenticationService',
	'MoveRequestServices', 'FormHelpServices', 'CalculatorServices',
	'SweetAlert', 'Session', '$timeout', 'slidePush', 'common', 'BranchingServiceApi', 'CommercialCalc'
];

function movecalController($scope, $rootScope, $http, $location, $sce, $anchorScroll,
	$filter, $q, $window, Analytics, config, AuthenticationService,
	MoveRequestServices, FormHelpServices, CalculatorServices,
	SweetAlert, Session, $timeout, slidePush, common, BranchingServiceApi, CommercialCalc) {

	const BOTTOM_FORM_RESULTS_STEP = '#edit-calculator-results';
	const BOTTOM_FORM_PERSONAL_INFO_STEP = '#edit-personal-info';

	function getCookie() {
		if (readCookie(COOKIE_UID)) {
			return false;
		} else {
			let one_year = {
				expires: 3600 * 24 * 365
			};

			$http.post(config.serverUrl + 'server/move_statistics/generate_unique_uid')
				.then((res) => setCookie(COOKIE_UID, res.data[0], one_year));
		}
	}

	getCookie();

	function readCookie(name) {
		let nameEqual = name + '=';
		let cookiesArray = document.cookie.split(';');

		for (let i = 0; i < cookiesArray.length; i++) {
			let cookie = cookiesArray[i];

			while (cookie.charAt(0) == ' ') {
				cookie = cookie.substring(1, cookie.length);
			}

			if (cookie.indexOf(nameEqual) == 0) {
				return cookie.substring(nameEqual.length, cookie.length);
			}
		}

		return null;
	}

	function setCookie(name, value, options) {
		options = options || {};
		var expires = options.expires;

		if (typeof expires == 'number' && expires) {
			var d = new Date();
			d.setTime(d.getTime() + expires * 1000);
			expires = options.expires = d;
		}

		if (expires && expires.toUTCString) {
			options.expires = expires.toUTCString();
		}

		value = encodeURIComponent(value);
		var updatedCookie = name + '=' + value;

		for (var propName in options) {
			updatedCookie += '; ' + propName;
			var propValue = options[propName];

			if (propValue !== true) {
				updatedCookie += '=' + propValue;
			}
		}

		document.cookie = updatedCookie;
	}

	AuthenticationService.getUniqueUser();

	$scope.blockCalculateSmallForm = false;
	$scope.allowedstate = [];
	$scope.move_requests_total = 55565;
	var minCATravelTime = 0;
	var isWeNotMoveToThisState = false;
	var twoDaysMs = 172800000;

	$scope.range = function (min, max, step) {
		step = step || 1;
		var input = [];

		for (var i = min; i <= max; i += step) {
			input.push(i);
		}

		return input;
	};

	$scope.toTrustedHTML = function (html) {
		if (angular.isString(html)) {
			return $sce.trustAsHtml(html);
		}
	};

	var host = '';

	function setUpHost() {
		host = $scope.basicsettings && $scope.basicsettings.client_page_url || config.host;
		$scope.host = host;
	}

	$scope.ifMobile = /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $window.innerWidth < 700;

	$scope.dateOptions = {
		format: 'mmmm dd, yyyy', // ISO formatted date
		onClose: () => {}
	};

	$scope.currentMonth = parseInt(moment().format('M'));
	$scope.currentYear = moment().format('YYYY');
	$scope.loading = true;
	let token = Session.getToken();
	$scope.newUser = !token;
	$scope.wasReset = false;
	initRequestCalcResult();
	initStorageCalcResult();

	function initStorageCalcResult() {
		$scope.storageCalcResult = {
			to: {},
			from: {}
		};
	}

	function initRequestCalcResult() {
		$scope.requestCalcResult = {};
	}

	$scope.openSlide = function () {
		slidePush.slide(angular.element('#slide_menu'), angular.element('#slid_menu_size'));
		$timeout(function () {
			showDropdown(angular.element('#edit-size-move'));
		}, 500);
	};

	var showDropdown = function (element) {
		var e = document.createEvent('MouseEvents');
		e.initMouseEvent('mousedown');
		element[0].dispatchEvent(e);
	};

	$scope.images = [
		{
			'url': '1.jpg',
			'caption': 'Optional caption',
			'thumbUrl': 'thumb1.jpg' // used only for this example
		},
		{
			'url': '2.gif',
			'thumbUrl': 'thumb2.jpg'
		},
		{
			'url': '3.png',
			'thumbUrl': 'thumb3.png'
		}
	];

	$scope.openLightboxModal = function (index) {
		Lightbox.openModal($scope.images, index);
	};

	var formNames = {};
	var localServices = [1, 2, 3, 4, 6, 8];

	$scope.localdistount = 0;

	var rateTooltips = {
		discount: {
			short: '',
			long: ''
		},
		regular: {
			short: '',
			long: ''
		},
		subPeak: {
			short: '',
			long: ''
		},
		peak: {
			short: '',
			long: ''
		},
		hiPeak: {
			short: '',
			long: ''
		}
	};
	$scope.MoveDatePickerOptions = {
		min: '',
		disable: [],
		calendar: {},
		calendarTypes: {},
		rateTooltips: rateTooltips,

	};
	$scope.StorageDatePickerOptions = {
		min: '',
		disable: [],
		calendar: {},
		calendarTypes: {},
		rateTooltips: rateTooltips,
	};
	var BlockDays = [];
	var isBlocked = function (day) {
		for (var ll = 0; ll < BlockDays.length; ll++) {
			if ((BlockDays[ll][0] == day.getFullYear()) && (BlockDays[ll][1] == day.getMonth()) && (BlockDays[ll][2] == day.getDate())) {
				return true;
			}
		}
		return false;
	};
	var setMinDate = function (Day) {
		while (isBlocked(Day)) {
			Day.setDate(Day.getDate() + 1);
		}
	};

	$scope.request = {
		'checkedRooms': {
			'rooms': []
		},
		'request_all_data': {},
		'serviceType': '',
		'email': undefined,
		'field_moving_to': {},
		'field_moving_from': {},
		'field_commercial_extra_rooms': [],
	};

	var serviceTypeId = '1';
	$scope.$watch('request.moveDate', function () {
		if ($scope.request.moveDate !== undefined) {
			var StorageDay = new Date(Date.parse($scope.request.moveDate) + twoDaysMs);
			setMinDate(StorageDay);
			$scope.StorageDatePickerOptions.min = StorageDay;
		}
	});

	function getFront() {
		var defered = $q.defer();
		// Get Data For Form
		AuthenticationService
			.GetFront()
			.then(function (response) {
				$timeout(function () {
					$scope.loading = false;
				}, 500);

				$scope.branches = response.branches;
				$scope.isActiveBranching = isActiveBranching(response);

				AuthenticationService.SetCredentials(response);

				// Do not reinit if branch url was changed
				if (angular.isUndefined($scope.rates)) {
					$scope.rates = response.prices_array;
				}

				$scope.basicsettings = angular.fromJson(response.basicsettings);
				$rootScope.reviewsSettings = angular.fromJson(response.reviews_settings);
				$scope.calcsettings = angular.fromJson(response.calcsettings);
				$scope.longdistance = angular.fromJson(response.longdistance);
				$scope.schedulesettings = angular.fromJson(response.schedulesettings);
				$scope.localdiscount = $scope.basicsettings.localDistount;
				$scope.localdiscountOn = $scope.basicsettings.localDistountOn;
				$scope.longDistanceDistountOn = $scope.basicsettings.longDistanceDistountOn;
				$scope.longDistanceDistount = $scope.basicsettings.longDistanceDistount;
				$scope.customizedText = angular.fromJson(response.customized_text);
				let commercialSetting = angular.fromJson(response.commercial_move_size_setting);
				$scope.stateLists = response.stateList;
				formNames = response.enums.form_names;

				$scope.isDoubleDriveTime = $scope.calcsettings.doubleDriveTime;

				// Do not reinit if branch url was changed
				if (angular.isUndefined($scope.promoText)) {
					$scope.promoText = $scope.basicsettings.promoText;
				}

				// Do not reinit if branch url was changed
				if (angular.isUndefined($scope.promoTextOn)) {
					$scope.promoTextOn = $scope.basicsettings.promoTextOn;
				}

				MoveRequestServices.saveSettingsData($scope.schedulesettings);

				$scope.min_hours = parseFloat($scope.calcsettings.min_hours);
				$scope.allowedstate = response.allowedstate;

				$scope.calendar = response.calendar;
				$rootScope.calendar = response.calendar;

				$scope.calendarshow = {};
				$scope.currentYear = moment().year();
				$scope.calendarshow[$scope.currentYear] = $scope.calendar[$scope.currentYear];
				$scope.calendarshow[$scope.currentYear + 1] = $scope.calendar[$scope.currentYear + 1];

				$scope.calendarTypes = response.calendartype;
				$scope.move_requests_total = response.move_requests_total;
				$scope.field_lists = response.field_lists;
				$scope.enums = response.enums;
				$scope.serviceTypes = $scope.enums.request_service_types;

				setDefaultServiceTypeId();
				setDefaultServiceTypeName();
				$scope.request.serviceType = serviceTypeId;

				if ($scope.isActiveBranching) {
					for (var i in $scope.calendar) {
						for (var date in $scope.calendar[i]) {
							if ($scope.calendar[i][date] != '1') {
								$scope.calendar[i][date] = '1';
							}
						}
					}
				} else {
					for (var i in $scope.calendar) {
						for (var date in $scope.calendar[i]) {
							if ($scope.calendar[i][date] == '5') {
								BlockDays.push([
									Number(date.substring(0, 4)),
									Number(date.substring(5, 7) - 1),
									Number(date.substring(8, 10))
								]);
							}
						}
					}
				}


				var CurrentDay = new Date();
				CurrentDay.setHours(0);
				CurrentDay.setMinutes(0);
				CurrentDay.setSeconds(0);
				CurrentDay.setMilliseconds(0);

				rateTooltips = {
					discount: {
						short: '',
						long: $scope.basicsettings.movecalcFormSettings.rateTooltips.discount
					},
					regular: {
						short: '',
						long: $scope.basicsettings.movecalcFormSettings.rateTooltips.regular
					},
					subPeak: {
						short: '',
						long: $scope.basicsettings.movecalcFormSettings.rateTooltips.subPeak
					},
					peak: {
						short: '',
						long: $scope.basicsettings.movecalcFormSettings.rateTooltips.peak
					},
					hiPeak: {
						short: '',
						long: $scope.basicsettings.movecalcFormSettings.rateTooltips.hiPeak
					},
					giant: $scope.basicsettings.movecalcFormSettings.rateTooltips.giant,
					showGiant: $scope.basicsettings.movecalcFormSettings.rateTooltips.showGiant,
					showSmall: $scope.basicsettings.movecalcFormSettings.rateTooltips.showSmall
				};

				let showBig = $scope.basicsettings.movecalcFormSettings.rateTooltips.showBig;

				rateTooltips.discount.isVisible = isEmptySettings(rateTooltips.discount.long) && showBig;
				rateTooltips.regular.isVisible = isEmptySettings(rateTooltips.regular.long) && showBig;
				rateTooltips.subPeak.isVisible = isEmptySettings(rateTooltips.subPeak.long) && showBig;
				rateTooltips.peak.isVisible = isEmptySettings(rateTooltips.peak.long) && showBig;
				rateTooltips.hiPeak.isVisible = isEmptySettings(rateTooltips.hiPeak.long) && showBig;

				let minimumMovers = 2;

				if ($scope.basicsettings.ratesSettings && !$scope.basicsettings.ratesSettings.twomoversOn) {
					if (!$scope.basicsettings.ratesSettings.threemoversOn) {
						if (!$scope.basicsettings.ratesSettings.fourmoversOn) {
							minimumMovers = 2;
							rateTooltips.showSmall = false;
						} else {
							minimumMovers = 4;
						}
					} else {
						minimumMovers = 3;
					}
				}

				rateTooltips.discount.short = minimumMovers + ' movers<br> $' + $scope.rates.Discount[minimumMovers - 2] + '/hour';
				rateTooltips.regular.short = minimumMovers + ' movers<br> $' + $scope.rates.Regular[minimumMovers - 2] + '/hour';
				rateTooltips.subPeak.short = minimumMovers + ' movers<br> $' + $scope.rates.SubPeak[minimumMovers - 2] + '/hour';
				rateTooltips.peak.short = minimumMovers + ' movers<br> $' + $scope.rates.Peak[minimumMovers - 2] + '/hour';
				rateTooltips.hiPeak.short = minimumMovers + ' movers<br> $' + $scope.rates.HighPeak[minimumMovers - 2] + '/hour';

				setMinDate(CurrentDay);
				$scope.MoveDatePickerOptions.min = CurrentDay;
				$scope.MoveDatePickerOptions.calendar = $scope.calendar;
				$scope.MoveDatePickerOptions.calendarTypes = $scope.basicsettings.ratesSettings;
				$scope.MoveDatePickerOptions.rateTooltips = rateTooltips;

				// not block days if branching is active
				if (!$scope.isActiveBranching) {
					$scope.MoveDatePickerOptions.disable = BlockDays;
				}

				var StorageDay = new Date();
				StorageDay.setDate(CurrentDay.getDate() + 2);
				setMinDate(StorageDay);
				$scope.StorageDatePickerOptions.min = StorageDay;
				$scope.StorageDatePickerOptions.calendar = $scope.calendar;
				$scope.StorageDatePickerOptions.calendarTypes = $scope.basicsettings.ratesSettings;
				$scope.StorageDatePickerOptions.rateTooltips = rateTooltips;

				// not block days if branching is active
				if (!$scope.isActiveBranching) {
					$scope.StorageDatePickerOptions.disable = BlockDays;
				}

				FormHelpServices.countdown();
				CalculatorServices.init(response);

				$scope.flatRateDistance = 0;
				$scope.longDistance = 0;

				minCATravelTime = CalculatorServices.getRoundedTime($scope.basicsettings.minCATavelTime / 60);

				if ($scope.basicsettings.isflat_rate_miles) {
					$scope.flatRateDistance = $scope.basicsettings.flat_rate_miles;
				}

				if ($scope.basicsettings.islong_distance_miles) {
					$scope.longDistance = $scope.basicsettings.long_distance_miles;
				}

				$scope.storageCity = $scope.basicsettings.storage_city;
				$scope.storageName = $scope.basicsettings.company_name;
				$scope.companyName = $scope.basicsettings.company_name;

				// only init movecalcFormSettings
				if (!$scope.movecalcFormSettings) {
					$scope.movecalcFormSettings = angular.copy($scope.basicsettings.movecalcFormSettings);
				}

				if (!$scope.ifMobile) {
					initStyles($scope.movecalcFormSettings);
				}

				setUpHost();

				if (commercialSetting.isEnabled) {
					$scope.sizeOfMove.push({
						id: COMMERCIAL_MOVE_SIZE,
						text: $scope.field_lists.field_size_of_move[COMMERCIAL_MOVE_SIZE]
					});
					$scope.commercialExtra = $scope.calcsettings.commercialExtra;
				}

				$scope.servicesType = getServiceTypes();
				defered.resolve();
			}, function (response) {
				$scope.error = response.message;
				$scope.dataLoading = false;
				$scope.loading = false;
				defered.reject();
			});

		return defered.promise;
	}

	function setDefaultServiceTypeId() {
		if ($scope.basicsettings.islong_distance_miles) {
			serviceTypeId = '7';
		}

		if ($scope.basicsettings.services.overnightStorageOn) {
			serviceTypeId = '6';
		}

		if ($scope.basicsettings.isflat_rate_miles) {
			serviceTypeId = '5';
		}

		if ($scope.basicsettings.services.unloadingHelpOn) {
			serviceTypeId = '4';
		}

		if ($scope.basicsettings.services.loadingHelpOn) {
			serviceTypeId = '3';
		}

		if ($scope.basicsettings.services.localMoveStorageOn) {
			serviceTypeId = '2';
		}

		if ($scope.basicsettings.services.localMoveOn) {
			serviceTypeId = '1';
		}
	}

	function setDefaultServiceTypeName() {
		let names = [""];
		let serviceTypes = getServiceTypes();

		for (let index = 0; index < serviceTypes.length; index++) {
			names.push(serviceTypes[index].text);
		}

		$rootScope.defaultServiceTypeNames = names;
	}

	function getServiceTypes() {
		return [
			{
				id: 1,
				text: $scope.basicsettings.services.localMoveLabel || 'Moving'
			},
			{
				id: 2,
				text: $scope.basicsettings.services.localMoveStorageLabel || 'Moving & Storage'
			},
			{
				id: 3,
				text: $scope.basicsettings.services.loadingHelpLabel || 'Loading Help'
			},
			{
				id: 4,
				text: $scope.basicsettings.services.unloadingHelpLabel || 'Unloading Help'
			},
			{
				id: 5,
				text: $scope.basicsettings.services.flatRateLabel || 'Flat Rate'
			},
			{
				id: 6,
				text: $scope.basicsettings.services.overnightStorageLabel || 'Overnight'
			},
			{
				id: 7,
				text: $scope.basicsettings.services.longDistanceLabel || 'Long Distance'
			},
			{
				id: 8,
				text: $scope.basicsettings.services.packingDayLabel || 'Packing Day'
			}
		]
	}

	function isEmptySettings(settings) {
		return angular.element(settings).text() != '';
	}

	function isActiveBranching(fieldData) {
		let isEnableSendToClosestBranch = fieldData.settingsclosestbranch && fieldData.settingsclosestbranch != 0;
		return fieldData.branches.length >= 2 && isEnableSendToClosestBranch;
	}

	getFront();

	function initStyles(movecalcFormSettings) {
		let head = document.head || document.getElementsByTagName('head')[0];
		let style = document.createElement('style');

		style.type = 'text/css';

		if (style.styleSheet) {
			style.styleSheet.cssText = '.company-bg-color { background-color: ' + movecalcFormSettings.companyColor + ' !important}';
		} else {
			style.appendChild(document.createTextNode('.company-bg-color { background-color: ' + movecalcFormSettings.companyColor + ' !important}'));
			style.appendChild(document.createTextNode('.company-color {color: ' + movecalcFormSettings.companyColor + ' !important} '));
		}

		head.appendChild(style);
	}

	function getProviderName(nodes) {
		let index = Object.keys(nodes)[0];
		let firstNode;
		if (index) firstNode = nodes[index];

		return firstNode !== undefined
			? firstNode.field_parser_provider_name
			: null;
	}

	AuthenticationService
		.GetCurrent(function (response) {
			let user_credentials = response;
			$scope.userLogin = true;
			$scope.request.current_user = {};
			$scope.request.current_user.uid = user_credentials.uid;
			$scope.request.current_user.field_user_first_name = user_credentials.field_user_first_name;
			$scope.request.current_user.field_user_last_name = user_credentials.field_user_last_name;
			$scope.request.current_user.field_primary_phone = user_credentials.field_primary_phone;
			$scope.request.current_user.field_user_additional_phone = user_credentials.field_user_additional_phone;
			$scope.request.current_user.mail = user_credentials.mail;

			if (user_credentials.uid) {
				$scope.request.first_name = user_credentials.field_user_first_name;
				$scope.request.last_name = user_credentials.field_user_last_name;
				$scope.request.email = user_credentials.mail;
				$scope.request.primaryPhone = user_credentials.field_primary_phone;
				$scope.request.poll = getProviderName(user_credentials.nodes);
			}
		});

	$scope.overlayShown = false;
	var oldtype = '';

	$scope.toggleOverlay = function (type) {
		$scope.overlayShown = !$scope.overlayShown;

		if ($scope.overlayShown) {
			angular.element('.ng-overlay .' + type).show();
			oldtype = type;
			angular.element('body').css({'overflow': 'hidden'});
			angular.element('.ng-overlay').animate({'top': '0px'}, 1500);
		} else {
			angular.element('.ng-overlay .' + oldtype).hide();
			angular.element('body').css({'overflow': 'auto'});
			angular.element('.ng-overlay').animate({'top': '1500px'}, 1500);
		}
	};

	//Get Templates For Form Steps
	$scope.templates =
		[
			{
				name: 'localmove.html',
				url: tmpLocalmoveCalcResult
			},
			{
				name: 'movingandstorage.html',
				url: tmpStorageTemplate
			},
			{
				name: 'flatrate.html',
				url: tmpFlatrateCalcResult
			},
			{
				name: 'personalInfo.html',
				url: tmpPersonalInfoStep
			},
			{
				name: 'review.html',
				url: tmpReviewSubmitStep
			},
			{
				name: 'congrats.html',
				url: tmpCongrats
			},
			{
				name: 'storageReview.html',
				url: tmpStorageReviewSubmit
			},
			{
				name: 'congratsStorage.html',
				url: tmpCongratsStorage
			},
		];
	$scope.personalInfoTemplate = $scope.templates[3]; //Personal Info
	$scope.reviewFormTemplate = $scope.templates[4]; //Personal Info
	$scope.congratsTemplate = $scope.templates[5]; //Personal Info
	$scope.currentStep = '';
	$scope.userLogin = false;
	$scope.overnightMove = false;
	$scope.storageMove = false;
	$scope.loadingImg = false;

	$scope.request = {
		'checkedRooms': {
			'rooms': []
		},
		'request_all_data': {},
		'serviceType': serviceTypeId,
		'email': undefined,
		'field_moving_to': {},
		'field_moving_from': {},
		'field_commercial_extra_rooms': [],
	};

	$scope.text = {};
	$scope.text.cinfoText = 'Your move request will be processed and we will email you as soon as possible. We strive to send out all estimates within minutes. Please expect an additional day for all weekend requests.<br>In the meantime, you can make changes or check request status in your request page.';
	//Load MoveSize

	// Load and Show Rooms on Form
	$scope.sizeOfMove = [
		{
			id: 1,
			text: 'Room or less'
		},
		{
			id: 2,
			text: 'Studio Apartment'
		},
		{
			id: 3,
			text: 'Small 1 Bedroom Apartment'
		},
		{
			id: 4,
			text: 'Large 1 Bedroom Apartment '
		},
		{
			id: 5,
			text: 'Small 2 Bedroom Apartment'
		},
		{
			id: 6,
			text: 'Large 2 Bedroom Apartment'
		},
		{
			id: 7,
			text: '3 Bedroom Apartment'
		},
		{
			id: 8,
			text: '2 Bedroom house/townhouse'
		},
		{
			id: 9,
			text: '3 Bedroom house/townhouse'
		},
		{
			id: 10,
			text: '4 Bedroom house/townhouse'
		},
	];

	$scope.entranceType = [
		{
			id: 1,
			text: 'No Stairs - Ground Floor'
		},
		{
			id: 2,
			text: 'Stairs - 2nd Floor'
		},
		{
			id: 3,
			text: 'Stairs - 3rd Floor'
		},
		{
			id: 4,
			text: 'Stairs - 4th Floor'
		},
		{
			id: 5,
			text: 'Stairs - 5th Floor'
		},
		{
			id: 6,
			text: 'Elevator'
		},
		{
			id: 7,
			text: 'Private House'
		},
	];

	$scope.rooms = [
		{
			id: 1,
			text: 'living room'
		},
		{
			id: 2,
			text: 'dining room'
		},
		{
			id: 3,
			text: 'office'
		},
		{
			id: 4,
			text: 'extra room'
		},
		{
			id: 5,
			text: 'basement/storage'
		},
		{
			id: 6,
			text: 'garage'
		},
		{
			id: 7,
			text: 'patio'
		},
		{
			id: 8,
			text: 'play room'
		},
	];

	$scope.showRooms = function () {
		var moveSize = $scope.request.moveSize;

		if (moveSize == 1 || moveSize == 2) {
			$scope.request.field_commercial_extra_rooms = [];
			$scope.request.checkedRooms.rooms = [];
			FormHelpServices.hideRooms();
		} else if (moveSize == 8 || moveSize == 9 || moveSize == 10) { //if House
			$scope.request.field_commercial_extra_rooms = [];
			$scope.request.checkedRooms.rooms = [1, 2];
			FormHelpServices.showHouseRooms();
		} else if (moveSize == COMMERCIAL_MOVE_SIZE) {
			$scope.request.field_commercial_extra_rooms = [];
			$scope.request.checkedRooms.rooms = [];
			$scope.rooms_text = FormHelpServices.getRoomsString($scope.request.field_commercial_extra_rooms, $scope.commercialExtra);
			angular.element('.form-item-Extra-Furnished-Rooms').css({display: 'block'});
		} else {
			$scope.request.checkedRooms.rooms = [];
			$scope.request.checkedRooms.rooms.push(1);
			FormHelpServices.showRooms();
		}
	};

	$scope.getFrontQuoteExplanation = function () {
		const COMMERCIAL_QUOTE_INDEX = 9;
		if (COMMERCIAL_MOVE_SIZE == $scope.request.moveSize) return $scope.basicsettings.showQuoteFront[COMMERCIAL_QUOTE_INDEX];
		return $scope.basicsettings.showQuoteFront[$scope.request.serviceType];
	};

	$scope.showRoomImg = function (roomId) {
		$timeout(function () {
			FormHelpServices.showRoomImg(roomId, $scope.request.checkedRooms.rooms);
			var rooms_text = FormHelpServices.getRoomsString($scope.request.checkedRooms.rooms, $scope.rooms);
			$scope.rooms_text = rooms_text;
			angular.element('.calc-intro_description .move-size i.rooms').text(rooms_text);
		}, 100);
	};

	$scope.getCommercialText = function () {
		$timeout(function () {
			angular.forEach($scope.commercialExtra, function (extra) {
				if (extra.id == $scope.request.field_commercial_extra_rooms) {
					$scope.rooms_text = `(with ${extra.name})`;
					angular.element('.calc-intro_description .move-size i.rooms').text($scope.rooms_text);
				}
			});
		}, 100);
	};

	$scope.chooseCommercialItem = function (id) {
		$scope.request.field_commercial_extra_rooms = [];
		$scope.request.field_commercial_extra_rooms.push(id);
	};

	//BUTTONS EVENTS
	$scope.NextStep = function (stepId) {
		FormHelpServices.nextstep(stepId);
		$scope.currentStep = stepId;
		AuthenticationService.getClientActions(stepId, formNames.FORM_CALCULATE_3);
	};

	$scope.PrevStep = function (stepId) {
		$scope.currentStep = stepId;
		let backToFirstStep = stepId == BOTTOM_FORM_RESULTS_STEP && $scope.isThirdStepFirst()
			|| stepId == BOTTOM_FORM_PERSONAL_INFO_STEP && $scope.isSecondStepFirst();
		if (backToFirstStep) {
			$scope.currentStep = '';
		}
		FormHelpServices.prevstep(stepId);
	};

	$scope.goToForm = function () {
		$scope.quoteForm = true;
		$scope.Calculate();
		var destination = angular.element('#reserve').offset().top - 150;
		angular.element('body').animate({scrollTop: destination}, 10);
		angular.element('html').animate({scrollTop: destination}, 10);
	};

	$scope.distanceRequest = function (distance) {
		var distance = parseFloat(distance);
		var amount = 0;

		if ($scope.basicsettings.fuel_surcharge.by_mileage.length) {
			angular.forEach($scope.basicsettings.fuel_surcharge.by_mileage, function (value, index) {
				if ((value.from < distance) && (distance <= value.to)) {
					amount = value.amount;
				}
			});
		}

		return amount;
	};

	//SUBMIT REQUEST STEP
	$scope.submitRequest = function () {
		var service = $scope.request.serviceType;
		$scope.loadingImg = true;

		Analytics.trackEvent('button', 'form fill', 'results page');

		if (service == 3 || service == 8) { // Loading HElP or Packing Day FROM ZIP ONLY
			$scope.request.addressTo = $scope.request.addressFrom;
			$scope.request.adrTo = $scope.request.adrFrom;
		}

		if (service == 4) { // UNLOADING HELP TO ZIP ONLY
			$scope.request.addressFrom = $scope.request.addressTo;
			$scope.request.adrFrom = $scope.request.adrTo;
		}

		if ($scope.mobileSubmit) {
			if (!$scope.movecalcFormSettings.showHowDidYouHearAboutUsField) {
				$scope.request.poll = $scope.sourceTopForm;
			}

			$scope.request.current_user = {};
			$scope.request.current_user.field_user_first_name = $scope.request.first_name;
			$scope.request.current_user.field_user_last_name = $scope.request.last_name;
			$scope.request.current_user.field_primary_phone = $scope.request.primaryPhone;
			$scope.request.current_user.field_user_additional_phone = '';
			$scope.request.current_user.mail = $scope.request.email;
		}

		if ($scope.ifMobile && !$scope.movecalcFormSettings.showHowDidYouHearAboutUsField) {
			$scope.request.poll = $scope.sourceMobileForm;
		}

		if ($scope.quoteForm) {
			//$scope.request.poll = $scope.sourceFullForm;
			//we give it from select
		}

		if (!$scope.storageMove) {  // REGULAR MOVE
			$scope.request.dummyMail = 0;
			let if_main_state = false;
			var main_state = $scope.basicsettings.main_state;
			var state = $scope.request.addressTo.state.toLowerCase();
			var baseState = $scope.request.addressFrom.state.toUpperCase();

			if (state == main_state) {
				if_main_state = true;
			}

			$scope.request.field_moving_to.administrative_area = state;
			$scope.request.field_moving_from.administrative_area = baseState;
			$scope.request.ldrate = CalculatorServices.getLongDistanceRate($scope.request);
			$scope.request.move_size = [];
			$scope.request.field_cubic_feet = [];
			$scope.request.field_long_distance_rate = [];
			$scope.request.rooms = [];
			$scope.request.field_cubic_feet.value = 1;

			if (if_main_state) {
				$scope.request.field_long_distance_rate.value = $scope.request.ldrate;
			} else {
				$scope.request.field_long_distance_rate.value = 0;
			}

			var request = MoveRequestServices.PrepareRequestArray($scope.request);
			$scope.request.request_all_data.travelTime = $scope.calcsettings.travelTime;
			request.all_data = angular.copy($scope.request.request_all_data);

			//request default settings
			request.data.field_request_settings = {
				storage_rate: $scope.basicsettings.storage_rate,
				inventoryVersion: 2
			};

			if ($scope.mobileSubmit) {
				if ($scope.ifMobile) {
					$scope.request.field_parser_provider_name = $scope.sourceMobileForm;
				}
				else {
					$scope.request.field_parser_provider_name = $scope.sourceTopForm;
				}
			}

			if ($scope.quoteForm) {
				$scope.request.field_parser_provider_name = $scope.request.poll;
			}

			var rsStart = moment();

			var log = {
				'request': request,
				'browser': AuthenticationService.get_browser(),
				'message_type': 'Creating Request',
				'message': 'Creating Request',
				'type': 'success'
			};

			AuthenticationService.sendLog('Front', log);

			var promise = MoveRequestServices.CreateRequest(request);

			promise.then(function (data) {
				$scope.loadingImg = false;
				var nid = data.nid;
				$scope.request.nid = nid;
				request.nid = nid;

				var rsEnd = moment();
				var creatingTime = rsEnd.diff(rsStart, 'seconds');
				var log = {
					'request': request,
					'browser': AuthenticationService.get_browser(),
					'message_type': 'Request was created',
					'message': 'Creating Request',
					'time': creatingTime,
					'type': 'success'
				};

				AuthenticationService.sendLog('Front', log);

				if (angular.isDefined(data.autologin)) {
					$scope.requesturl = host + 'account/#/request/' + nid + '?uid=' + data.autologin.uid + '&timestamp=' + data.autologin.timestamp + '&hash=' + data.autologin.hash;
				} else {
					// if crates, then URL = host + 'account/#/request/' + hash from responce;
					$scope.requesturl = host + 'account/#/request/' + nid;
				}

				$scope.userLogin = true;
				// Create Link To Request

				if (!$scope.mobileSubmit) {
					if (angular.isDefined(data.token)) {
						Session.saveToken(data.token);
					}

					FormHelpServices.nextstep(BOTTOM_FORM_PERSONAL_INFO_STEP);
				} else {
					$scope.loading = false;
					//slidePush
					if ($scope.newUser) {
						Session.saveToken(data.token);
					}

					$scope.showStep4();
					$scope.resetForm();
				}

				sendLogCreateRequest([request]);
			}, function (reason) {
				var log = {
					'request': request,
					'browser': AuthenticationService.get_browser(),
					'message_type': 'Request Creating FAILD',
					'message': reason,
					'type': 'error'
				};

				AuthenticationService.sendLog('Error', log);
			});
		} else {  // MOVING AND STORAGE MOVE
			$scope.congratsTemplate = $scope.templates[7];

			$scope.requestFromStorage.current_user = $scope.request.current_user;
			$scope.requestToStorage.current_user = $scope.request.current_user;

			$scope.requestFromStorage.dummyMail = 0;
			$scope.requestToStorage.dummyMail = 0;

			$scope.requestFromStorage.adrTo = $scope.request.adrTo;
			$scope.requestToStorage.adrFrom = $scope.request.adrFrom;

			if ($scope.mobileSubmit) {
				if ($scope.ifMobile) {
					$scope.requestFromStorage.poll = $scope.sourceMobileForm;
					$scope.requestToStorage.poll = $scope.sourceMobileForm;
				} else {
					$scope.requestFromStorage.poll = $scope.sourceTopForm;
					$scope.requestToStorage.poll = $scope.sourceTopForm;
				}
			} else if ($scope.quoteForm) {
				$scope.requestFromStorage.poll = $scope.request.poll;
				$scope.requestToStorage.poll = $scope.request.poll;
			}

			var requestTo = MoveRequestServices.PrepareRequestArray($scope.requestToStorage);
			var requestFrom = MoveRequestServices.PrepareRequestArray($scope.requestFromStorage);

			requestTo.data.field_moving_to.postal_code = $scope.basicsettings.parking_address;
			requestTo.data.field_moving_to.administrative_area = $scope.basicsettings.main_state;
			requestTo.data.field_moving_to.locality = $scope.basicsettings.storage_city;
			requestTo.data.field_moving_to.thoroughfare = $scope.basicsettings.company_name;
			requestTo.data.field_request_settings = {
				storage_rate: $scope.basicsettings.storage_rate,
				inventoryVersion: 2
			};

			requestFrom.data.field_moving_from.postal_code = $scope.basicsettings.parking_address;
			requestFrom.data.field_moving_from.administrative_area = $scope.basicsettings.main_state;
			requestFrom.data.field_moving_from.locality = $scope.basicsettings.storage_city;
			requestFrom.data.field_moving_from.thoroughfare = $scope.basicsettings.company_name;
			requestFrom.data.field_request_settings = {
				storage_rate: $scope.basicsettings.storage_rate,
				inventoryVersion: 2
			};

			var storageRequest = {};

			storageRequest.storage1 = requestTo;
			storageRequest.storage2 = requestFrom;
			storageRequest.storage1.all_data = $scope.requestToStorage.request_all_data;
			storageRequest.storage2.all_data = $scope.requestFromStorage.request_all_data;

			MoveRequestServices.CreateStorageRequest(storageRequest)
				.then(function (data) {
					requestTo.nid = data[0].nid;
					requestFrom.nid = data[1].nid;
					var nid = data[0].nid;
					$scope.userLogin = true;
					$scope.loadingImg = false;
					$scope.request.nid = nid;

					if (angular.isDefined(data[0].autologin)) {
						$scope.requesturl = host + 'account/#/request/' + nid + '?uid=' + data[0].autologin.uid + '&timestamp=' + data[0].autologin.timestamp + '&hash=' + data[0].autologin.hash;
					} else {
						$scope.requesturl = host + 'account/#/request/' + nid;
					}

					// Create Link To Request
					if (!$scope.mobileSubmit) {
						if (angular.isDefined(data[0].token)) {
							Session.saveToken(data[0].token);
						}

						FormHelpServices.nextstep(BOTTOM_FORM_PERSONAL_INFO_STEP);
					} else {
						$scope.loading = false;
						$scope.showStep4();

						$scope.resetForm();
					}
					sendLogCreateRequest([requestTo, requestFrom]);
				});
		}
	};

	function sendLogCreateRequest(arrReq) {
		var fieldData = $scope.field_lists;

		angular.forEach(arrReq, function (request, ind) {
			let results = request.data;
			let requestCalc = {
				moveSize: results.field_size_of_move,
				checkedRooms: {
					rooms: results.field_extra_furnished_rooms
				}
			};
			let weight;
			if (results.field_size_of_move != COMMERCIAL_MOVE_SIZE) {
				weight = CalculatorServices.getCubicFeet(requestCalc);
			} else {
				weight = CommercialCalc.getCommercialCubicFeet(results.field_commercial_extra_rooms,
					$scope.calcsettings);
			}
			var arr = createLogs('Request was created (front site)', fieldData, request.data, weight, request.all_data);
			MoveRequestServices.sendLogs(arr, 'Request was created (front site)', request.nid,
				$scope.enums.entities.MOVEREQUEST, request);
		});
	}

	function createLogs(createdFrom, fieldData, editrequest, weight, all_data) {
		let msg = {
			simpleText: createdFrom
		};
		let arr = [msg];
		let notUseFields = [
			'status',
			'field_actual_start_time',
			'field_start_time_max',
			'inventory',
			'field_useweighttype',
			'uid',
		];

		angular.forEach(editrequest, function (item, key) {
			if (!item) {
				return;
			}
			var msg = '';
			if (notUseFields.indexOf(key) == -1 && item) {
				var fieldName = key.replace(/field_/g, '').replace(/_/g, ' ').charAt(0).toUpperCase() + key.replace(/field_/g, '').replace(/_/g, ' ').slice(1);
				if (key == 'field_moving_from' || key == 'field_moving_to') {
					msg = addressLog(item, fieldName);
					let m = {
						simpleText: msg
					};
					arr.push(m);
					return;
				}
				if (fieldName == 'Date') {
					fieldName = 'Move Date';
					msg = fieldName + ': ' + moment(item.date, 'YYYY-MM-DD').format('MMM DD, YYYY');
					let m = {
						simpleText: msg
					};
					arr.push(m);
					return;
				}
				if (key === 'field_request_settings') {
					let {
						storageRate,
						inventoryVersion
					} = requestSettingsLog(item);
					arr.push({
						simpleText: storageRate
					}, {
						simpleText: inventoryVersion
					});
					return;
				}
				msg = fieldName + ': ';
				if (angular.isObject(item) && !angular.isArray(item)) {
					angular.forEach(item, function (el, i) {
						if (fieldData[key]) {
							i = fieldData[key][i];
						}
						if (el) {
							msg += i.toString().replace(/_/g, ' ').charAt(0).toUpperCase() + i.toString().replace(/_/g, ' ').slice(1) + ' - ' + el;

							if (item.length - 1 != i) {
								msg += ', ';
							}

							if (item.length - 1 == i) {
								msg += '.';
							}
						}
					});
				} else if (angular.isArray(item)) {
					angular.forEach(item, function (el, i) {
						var room = '';
						if (fieldData[key]) {
							room = fieldData[key][el];
						}
						if (el) {
							msg += room.toString().replace(/_/g, ' ').charAt(0).toUpperCase() + room.toString().replace(/_/g, ' ').slice(1);

							if (item.length - 1 != i) {
								msg += ', ';
							}

							if (item.length - 1 == i) {
								msg += '.';
							}
						}
					});
				} else {
					if (fieldData[key]) {
						item = fieldData[key][item];
					}

					if (item) {
						msg += item.toString().replace(/_/g, ' ').charAt(0).toUpperCase() + item.toString().replace(/_/g, ' ').slice(1);
					}
				}
				var m = {
					simpleText: msg
				};
				arr.push(m);
			}
		});

		if (weight) {
			let msg = {
				simpleText: 'Weight: ' + ((weight.min + weight.max) / 2).toFixed(2) + 'c.f.'
			};

			arr.push(msg);
		}

		if (all_data && all_data.surcharge_fuel) {
			let msg = {
				simpleText: 'Fuel surcharge: $' + all_data.surcharge_fuel
			};

			arr.push(msg);
		}

		return arr;
	}

	function addressLog(obj, field) {
		return (field + ': ' + obj.locality + ' ' + obj.administrative_area + ', ' + obj.postal_code);
	}

	function requestSettingsLog(setting) {
		let storageRate = `Storage Rate: $${setting.storage_rate.min}-$${setting.storage_rate.max}`;
		let inventoryVersion = `Inventory Version: ${setting.inventoryVersion}`;

		return {
			storageRate,
			inventoryVersion
		};
	}

	$scope.GoToInfo = function () {
		FormHelpServices.nextstep(BOTTOM_FORM_RESULTS_STEP);
		Analytics.trackEvent('Form', 'Go To Client Info Page', 'Client Info Step');
	};

	// SUMMERY STEP
	$scope.goToSummery = function () {
		if (!FormHelpServices.validateStep(BOTTOM_FORM_PERSONAL_INFO_STEP)) {

			Analytics.trackEvent('Form', 'Summary Page', 'Summary Step');

			if (!$scope.userLogin) {
				$scope.request.current_user = {};
				$scope.request.current_user.field_user_first_name = $scope.request.firstName;
				$scope.request.current_user.field_user_last_name = $scope.request.lastName;
				$scope.request.current_user.field_primary_phone = $scope.request.primaryPhone;
				$scope.request.current_user.field_user_additional_phone = $scope.request.additionalPhone;
				$scope.request.current_user.mail = $scope.request.email;
				$scope.request.current_user.password = $scope.request.password;
			}

			// Submit or Update Request if samessesion and zips don't change

			$scope.submitRequest();
			AuthenticationService.getClientActions('goToSummery', formNames.FORM_CALCULATE_2);
		}
	};

	$scope.longDis = false;
	$scope.zipFromSuccess = false;
	$scope.zipToSuccess = false;
	$scope.busy = false;

	$scope.$watch('request.zipFrom', onChangeZipCode);

	$scope.$watch('request.zipTo', onChangeZipCode);

	function onChangeZipCode() {
		function onlyDigits(string) {
			var result = '';

			if (string) {
				for (var i = 0; i < string.length; i++) {
					if (!isNaN(string[i])) {
						result += string[i];
					}
				}
			}

			return result;
		}

		$scope.request.zipFrom = onlyDigits($scope.request.zipFrom);
		$scope.request.zipTo = onlyDigits($scope.request.zipTo);

		if ($scope.request.zipFrom && $scope.request.zipTo) {
			if ($scope.request.zipFrom.length == 5 && $scope.request.zipTo.length == 5) {
				if ($scope.isActiveBranching) {
					$scope.busy = true;

					BranchingServiceApi
						.getClosestBranch($scope.request.zipFrom, $scope.request.zipTo)
						.then(onLoadBranchSettings, function () {
							$scope.busy = false;
							checkRequestZipCode();
						});
				} else {
					checkRequestZipCode();
				}
			} else {
				if ($scope.request.zipFrom.length != 5 || $scope.request.zipTo.length != 5) {
					if (($scope.request.serviceType == $scope.serviceTypes.FLAT_RATE || $scope.request.serviceType == $scope.serviceTypes.LONG_DISTANCE) && $scope.basicsettings.services.localMoveOn) {
						$scope.request.serviceType = $scope.serviceTypes.MOVING + '';
					}
				}
			}
		}
	}

	function onLoadBranchSettings(branch) {
		// is not current api
		if (branch.api_url && config.serverUrl.indexOf(branch.api_url) < 0) {
			var oldApiUrl = angular.copy(config.serverUrl);
			config.serverUrl = branch.api_url + '/'; // update server url

			getFront() // update settings for new branch
				.then(function () {

				}, function () {
					config.serverUrl = oldApiUrl;
				})
				.finally(function () {
					$scope.busy = false;
					checkRequestZipCode();
				});
		} else {
			$scope.busy = false;
			checkRequestZipCode();
		}
	}

	function checkRequestZipCode() {
		$scope.busy = true;
		var serviceTypePromise = CalculatorServices.getRequestServiceType($scope.request.zipFrom, $scope.request.zipTo); // get full distance
		var getArea = CalculatorServices.getLongDistanceCode($scope.request.zipTo);

		$q.all([serviceTypePromise, getArea])
			.then(function (data) {
				var serviceType = data[0][0]; // get service type

				if (angular.isUndefined($scope.request.field_moving_to)) {
					$scope.request.field_moving_to = {};
				}

				// if we change service type we not retreawe premise
				$scope.request.field_moving_to.premise = data[1];

				switch (serviceType) {
				case $scope.serviceTypes.LONG_DISTANCE: {
					if ((checkMoveToState($scope.request.addressFrom.state, $scope.request.addressTo.state, data[1]) || $scope.longdistance.acceptAllQuotes) && $scope.basicsettings.islong_distance_miles) {
						$scope.request.serviceType = $scope.serviceTypes.LONG_DISTANCE + '';
						$scope.longDis = true;
					} else {
						SweetAlert.swal('We don\'t do moves to this state!');
						$scope.request.zipTo = '';
						angular.element('#calc-info-steps .box_info .moving-to').empty();

						if ($scope.request.serviceType == $scope.serviceTypes.FLAT_RATE
							|| $scope.request.serviceType == $scope.serviceTypes.LONG_DISTANCE) {
							$scope.request.serviceType = $scope.serviceTypes.MOVING + '';
						}
					}

					$scope.calcResultTemplate = $scope.templates[2];
					break;
				}
				case $scope.serviceTypes.FLAT_RATE: {
					if ($scope.basicsettings.isflat_rate_miles) {
						$scope.calcResultTemplate = $scope.templates[2];
						$scope.request.status = FLAT_RATE_FIRST_STEP_STATUS;
						$scope.request.serviceType = $scope.enums.request_service_types.FLAT_RATE + '';
					}

					break;
				}
				case $scope.serviceTypes.MOVING: {
					if (($scope.request.serviceType == $scope.serviceTypes.FLAT_RATE || $scope.request.serviceType == $scope.serviceTypes.LONG_DISTANCE) && $scope.basicsettings.services.localMoveOn) {

						$scope.request.serviceType = $scope.serviceTypes.MOVING + '';
					}

					$scope.longDis = false;
					$scope.request.status = PENDING_STATUS;

					break;
				}
				default : {
					$scope.request.serviceType = serviceType + '';
				}
				}
			})
			.finally(function () {
				$scope.busy = false;
			});
	}

	function checkMoveToState(stateCodeFrom, stateCodeTo, areaCode) {
		stateCodeTo = stateCodeTo.toLowerCase();

		let stateSettings = $scope.longdistance.stateRates[stateCodeFrom][stateCodeTo];
		let LDEnabled = stateSettings.longDistance;
		let statePriceExist = stateSettings.state_rate > 0;
		let areaPriceExist = stateSettings.rate && stateSettings.rate[areaCode] > 0;

		return LDEnabled && (statePriceExist || areaPriceExist);
	}

	$scope.GoTop = function () {
		angular.element('html, body').animate({
			scrollTop: 0
		}, 600);
	};

	// CALCULATE RESULTS STEP
	$scope.moveSizePreview = true;

	$scope.MoveSizePreviewClick = function (value) {
		var select = document.getElementById('edit-size-move');
		$scope.moveSizePreview = false;
		select.selectedIndex = Number(value);
		$scope.request.moveSize = value;
		$scope.showRooms();
	};

	$scope.stepBookmarks = [false, false, false];

	$scope.Continue1 = function (step) {
		if (!FormHelpServices.validateStepSmallCalc(step)) {
			$scope.showStep2();

			for (let i = 0; i < 3; i++) {
				$timeout(function () {
					$scope.stepBookmarks[i] = true;
				}, i * 500);
			}

			AuthenticationService.getClientActions(step, formNames.FORM_QUOTE_1);
		}
	};

	$scope.Continue2 = function (step) {
		if (!FormHelpServices.validateStepSmallCalc(step)) {
			$scope.showStep3();
			AuthenticationService.getClientActions(step, formNames.FORM_QUOTE_2);
		}
	};


	$scope.Calculate = function (type, source) {
		//type:1-ultraSmallForm, 0-movecalculator
		$scope.mobileSubmit = (type == 1);
		$scope.quoteForm = (type == 0);

		if (type == 1) {
			AuthenticationService.getClientActions(source, formNames.FORM_QUOTE_3)
		} else {
			AuthenticationService.getClientActions(source, formNames.FORM_CALCULATE_1)
		}

		$scope.currentStep = '#edit-calculator';

		Analytics.trackEvent('Form', 'Not Validate Calculate', 'Not Validate');

		var formVal = false;
		var small_form_validate = false;

		if ($scope.mobileSubmit) {
			small_form_validate = !FormHelpServices.validateStepSmallCalc('step1') && !FormHelpServices.validateStepSmallCalc('step2') && !FormHelpServices.validateStepSmallCalc('step3');
		} else {
			formVal = !FormHelpServices.validateStep('#movecalc-moving-form #edit-calculator');
		}

		if (formVal || small_form_validate) {
			if ($scope.mobileSubmit) {
				$scope.loading = true;
			}

			//Quote Created stat
			// create a new tracking event
			Analytics.trackEvent('Form', 'Calculate', 'Submit Calculate');

			$scope.request.status = PENDING_STATUS;
			var moveSize = $scope.request.moveSize;
			var service = $scope.request.serviceType;

			// GET USER INFO IF LOGGED UPDATE INFO
			$scope.current = [];

			if (moveSize == 8 || moveSize == 9 || moveSize == 10) {
				$scope.request.typeFrom = 7;
				$scope.request.typeTo = 7;
			}

			// MOVING AND STORAGE
			if (service == 2 || service == 6) { //Moving and Storage Template
				$scope.storageMove = true;
				$scope.overnightMove = false;
				$scope.reviewFormTemplate = $scope.templates[6];

				if (!angular.equals($scope.calcResultTemplate, $scope.templates[1])) {
					$scope.calcResultTemplate = $scope.templates[1];
				}

				$scope.requestFromStorage = angular.copy($scope.request);
				$scope.requestToStorage = angular.copy($scope.request);
				$scope.requestFromStorage.zipTo = $scope.request.zipTo;
				$scope.requestFromStorage.typeTo = $scope.request.typeTo;
				$scope.requestFromStorage.typeFrom = 1;
				$scope.requestFromStorage.zipFrom = $scope.basicsettings.parking_address;
				$scope.requestToStorage.zipFrom = $scope.request.zipFrom;
				$scope.requestToStorage.typeFrom = $scope.request.typeFrom;
				$scope.requestToStorage.typeTo = 1;
				$scope.requestToStorage.zipTo = $scope.basicsettings.parking_address;

				$scope.requestFromStorage.duration = 0;
				$scope.request.duration = 0;

				// form moving and storage we copy zipto from twice
				var promise1 = CalculatorServices.getDuration($scope.requestToStorage.zipFrom, $scope.requestToStorage.zipTo, service); //FROM ADDRESS TO Storage
				var promise2 = CalculatorServices.getDuration($scope.requestFromStorage.zipFrom, $scope.requestFromStorage.zipTo, service); // FROM STORAGE to Address

				$q.all([promise1, promise2])
					.then(function (data) {
						$scope.requestToStorage.travelTime = data[0].duration;
						$scope.requestFromStorage.travelTime = data[1].duration;
						$scope.requestToStorage.distance = data[0].distances.AB.distance;
						$scope.requestFromStorage.distance = data[1].distances.AB.distance;
						var durationTo = data[0].distances.AB.duration;
						var durationFrom = data[1].distances.AB.duration;
						$scope.requestToStorage.duration = durationTo;
						$scope.requestFromStorage.duration = durationFrom;

						$scope.equipmentFeeMovingTo = CalculatorServices.getEquipmentFee(data[0].max_distance);
						$scope.equipmentFeeMovingFrom = CalculatorServices.getEquipmentFee(data[1].max_distance);

						if ($scope.isDoubleDriveTime) {
							var double_travel_time_to = Math.max(minCATravelTime, durationTo * 2);
							var double_travel_time_from = Math.max(minCATravelTime, durationFrom * 2);
							$scope.requestToStorage.double_travel_time = CalculatorServices.getRoundedTime(
								double_travel_time_to);
							$scope.requestFromStorage.double_travel_time = CalculatorServices.getRoundedTime(
								double_travel_time_from);
						}

						$scope.requestToStorage.request_all_data.request_distance = data[0];
						$scope.requestFromStorage.request_all_data.request_distance = data[1];

						if ($scope.requestFromStorage.moveSize != COMMERCIAL_MOVE_SIZE || $scope.requestToStorage.moveSize != COMMERCIAL_MOVE_SIZE) {
							// From Storage
							$scope.requestFromStorage.request_all_data.isDisplayQuoteEye = !$scope.basicsettings.showQuote[service];
							$scope.requestFromStorage.request_all_data.showQuote = $scope.basicsettings.showQuote[service];
							// To Storage
							$scope.requestToStorage.request_all_data.isDisplayQuoteEye = !$scope.basicsettings.showQuote[service];
							$scope.requestToStorage.request_all_data.showQuote = $scope.basicsettings.showQuote[service];
						} else {
							// From Storage
							$scope.requestFromStorage.request_all_data.isDisplayQuoteEye = !$scope.basicsettings.showQuote['9'];
							$scope.requestFromStorage.request_all_data.showQuote = $scope.basicsettings.showQuote['9'];
							// To Storage
							$scope.requestToStorage.request_all_data.isDisplayQuoteEye = !$scope.basicsettings.showQuote['9'];
							$scope.requestToStorage.request_all_data.showQuote = $scope.basicsettings.showQuote['9'];
						}

						$scope.requestFromStorage.request_all_data.toStorage = true;
						$scope.requestFromStorage.request_all_data.isNewRequest = true;
						$scope.requestFromStorage.request_all_data.travelTime = $scope.calcsettings.travelTime;
						$scope.requestToStorage.request_all_data.toStorage = false;
						$scope.requestToStorage.request_all_data.isNewRequest = true;
						$scope.requestToStorage.request_all_data.travelTime = $scope.calcsettings.travelTime;

						// DISCOUNT
						if ($scope.localdiscountOn) {
							$scope.requestToStorage.request_all_data.localdiscount = $scope.localdiscount;
							$scope.requestFromStorage.request_all_data.localdiscount = $scope.localdiscount;
						}

						//interval
						var now = $scope.request.moveDateTime;
						var then = $scope.request.deliveryDateTime;
						$scope.interval = (then - now) / 86400000;
						$scope.storage_rate = CalculatorServices.getStorageRate($scope.request);

						$scope.requestFromStorage.moveDate = $scope.request.deliveryDateFormat;
						$scope.requestToStorage.moveDate = $scope.request.moveDateFormat;

						if (service == 6) {
							$scope.overnightMove = true;
							$scope.request.travelTime = $scope.requestToStorage.travelTime;
							$scope.request.travelTimeFromStorage = $scope.requestFromStorage.travelTime;
						}

						if ($scope.requestFromStorage.flatRate || $scope.requestToStorage.flatRate) {
							$scope.request.flatRate = true;
							$scope.request.status = FLAT_RATE_FIRST_STEP_STATUS;
							$scope.request.distance = Math.max($scope.requestToStorage.distance,
								$scope.requestFromStorage.distance);

							if (!angular.equals($scope.calcResultTemplate, $scope.templates[2])) {
								$scope.calcResultTemplate = $scope.templates[2];
							}

							$scope.request.seviceType = serviceTypeId;
						}

						//SEND MOVING AND STORAGE CALCULATION

						var promise1 = CalculatorServices.calculate($scope.requestToStorage, $scope.calcsettings);
						var promise2 = CalculatorServices.calculate($scope.requestFromStorage, $scope.calcsettings);

						$q.all([promise1, promise2]).then(function (data) {
							$scope.loadingImg = false;

							// Update Request
							angular.extend($scope.requestToStorage, data[0]);
							angular.extend($scope.requestFromStorage, data[1]);

							calculateStorageRequests()
								.then(function () {
									if (!$scope.mobileSubmit) {
										FormHelpServices.nextstep($scope.currentStep);
									} else {
										// TO DO IF SIMPLE FORM  SUBMIT REQUEST
										$scope.submitRequest();
									}
								});
						});
					});
			}

			//LOCAL MOVE CALCULATE AND SAVE RESULTS
			if (service == 1 || service == 3 || service == 4 || service == 5 || service == 7 || service == 8) {
				// GET LOCAL MOVE TEMPLATE BY DEFAULT
				if (service != 5 && service != 7) {
					$scope.calcResultTemplate = $scope.templates[0];
				}

				$scope.storageMove = false;
				$scope.reviewFormTemplate = $scope.templates[4];

				if (service == 3 || service == 8) { // Loading HElP or Packing Day FROM ZIP ONLY
					$scope.request.zipTo = $scope.request.zipFrom;
					$scope.request.addressTo = $scope.request.addressFrom;
					$scope.request.typeTo = $scope.request.typeFrom;
				}

				if (service == 4) { // UNLOADING HELP TO ZIP ONLY
					$scope.request.zipFrom = $scope.request.zipTo;
					$scope.request.addressFrom = $scope.request.addressTo;
					$scope.request.typeFrom = $scope.request.typeTo;
				}

				var promise1 = CalculatorServices.getDuration($scope.request.zipFrom, $scope.request.zipTo, service); //FROM ADDRESS TO STORAGE

				promise1.then(function (data) {
					$scope.loading = false;
					$scope.request.distance = data.distances.AB.distance;
					$scope.request.travelTime = data.duration;

					var duration = data.distances.AB.duration;
					var aBDistance = data.distances.AB.distance;
					$scope.request.duration = duration;
					$scope.request.request_all_data.request_distance = data;

					if ($scope.isDoubleDriveTime) {
						var double_travel_time = Math.max(duration * 2, minCATravelTime);
						$scope.request.double_travel_time = CalculatorServices.getRoundedTime(double_travel_time);
					}

					$scope.equipmentFee = CalculatorServices.getEquipmentFee(data.max_distance);

					if (service == $scope.enums.request_service_types.FLAT_RATE) {
						$scope.request.status = FLAT_RATE_FIRST_STEP_STATUS;
					}

					$scope.request.moveDate = $scope.request.moveDateFormat;
					var promise = CalculatorServices.calculate($scope.request, $scope.calcsettings);
					var longDist = false;

					if ($scope.basicsettings.islong_distance_miles && aBDistance > $scope.longDistance && $scope.longDistance) {
						longDist = true;
						var if_main_state = false;
						$scope.request.status = PENDING_STATUS;
						$scope.request.serviceType = '7';
						$scope.flatRateDistance = $scope.longDistance;

						var main_state = $scope.basicsettings.main_state;
						var state = $scope.request.addressTo.state.toLowerCase();
						var baseState = $scope.request.addressFrom.state.toUpperCase();

						if (state == main_state) {
							if_main_state = true;
						}

						if ($scope.longdistance.acceptAllQuotes) {
							if (!checkMoveToState(baseState, state, $scope.request.field_moving_to.premise)) {
								$scope.request.request_all_data.isDisplayQuoteEye = true;
								$scope.request.request_all_data.showQuote = false;
								$scope.showQuoteFront = false;
							}
						}

						$scope.flatRateDistance = $scope.longDistance;
						$scope.request.field_moving_to.administrative_area = state;
						$scope.request.field_moving_from.administrative_area = baseState;
						$scope.request.ldrate = CalculatorServices.getLongDistanceRate($scope.request);
						$scope.request.move_size = [];
						$scope.request.field_cubic_feet = [];
						$scope.request.field_long_distance_rate = [];
						$scope.request.rooms = [];
						$scope.request.field_cubic_feet.value = 1;

						if (if_main_state) {
							$scope.request.field_long_distance_rate.value = $scope.request.ldrate;
						} else {
							$scope.request.field_long_distance_rate.value = 0;
						}

						$scope.request.move_size.raw = moveSize;
						$scope.request.rooms.value = $scope.request.checkedRooms.rooms;
						$scope.request.inventory_weight = CalculatorServices.getRequestCubicFeet($scope.request);

						var linfo = CalculatorServices.getLongDistanceInfo($scope.request);
						$scope.request.request_all_data.min_weight = linfo.minWeight;
						$scope.request.request_all_data.min_price = linfo.minPrice;
						$scope.request.request_all_data.min_price_enabled = linfo.minPriceEnabled;
						var min_weight = angular.copy(linfo.minWeight || 0);

						if (min_weight > $scope.request.inventory_weight.weight) {
							$scope.request.inventory_weight.cfs = min_weight;
							$scope.request.inventory_weight.weight = min_weight;
						} else {
							$scope.request.inventory_weight.cfs = $scope.request.inventory_weight.weight;
						}

						$scope.request.longDistanceQuote = CalculatorServices.getLongDistanceQuote($scope.request);
						$scope.request.extraServices = CalculatorServices.getLongDistanceExtra($scope.request, $scope.request.inventory_weight.weight);
						setUpRequestDiscount();
						calculateRequest().then(function () {
							goToStepAfterCalcResult();
						});
					}

					setUpRequestDiscount();

					promise.then(function (data) {
						$scope.loadingImg = false;
						// Update Request
						angular.extend($scope.request, data);
						$scope.request.travelTimeStr = MoveRequestServices.get_readable_time($scope.request.travelTime);
						//IF NOT LONG DISTANCE
						calculateRequest()
							.then(function () {
								if (!$scope.mobileSubmit) {
									if (!longDist) {
										goToStepAfterCalcResult();
									}
								} else {
									// TO DO IF SIMPLE FORM  SUBMIT REQUEST
									$scope.submitRequest();
								}
							});
					});

					if ($scope.request.serviceType == 7 || $scope.request.serviceType == 5) {
						if (!angular.equals($scope.calcResultTemplate, $scope.templates[2])) {
							$scope.calcResultTemplate = $scope.templates[2];
						}
					}
				}).finally(() => $scope.loading = false);
			}

			// ADD TEXT STRING FOR FIELDS
			MoveRequestServices.UpdateRequest($scope);
		} else {
			$scope.blockCalculateSmallForm = false;
		}
	};

	function goToStepAfterCalcResult() {
		$scope.loading = false;
		var stepId = '#edit-calculator';
		FormHelpServices.nextstep(stepId);
	}

	function setUpRequestDiscount() {
		let service = +$scope.request.serviceType;

		if (service !== 5 && service !== 7) {
			if ($scope.localdiscountOn) {
				$scope.request.request_all_data.localdiscount = $scope.localdiscount;
			}
		} else {
			if ($scope.longDistanceDistountOn) {
				$scope.request.request_all_data.localdiscount = $scope.longDistanceDistount;
			}
		}

		if (!isWeNotMoveToThisState || service !== 7) {
			if ($scope.request.moveSize != COMMERCIAL_MOVE_SIZE) {
				$scope.request.request_all_data.isDisplayQuoteEye = !$scope.basicsettings.showQuote[service];
				$scope.request.request_all_data.showQuote = $scope.basicsettings.showQuote[service];
				$scope.showQuoteFront = $scope.basicsettings.showQuoteFront[service];
			} else {
				$scope.request.request_all_data.isDisplayQuoteEye = !$scope.basicsettings.showQuote['9'];
				$scope.request.request_all_data.showQuote = $scope.basicsettings.showQuote['9'];
				$scope.showQuoteFront = $scope.basicsettings.showQuoteFront['9'];
			}
		}
	}

	function calculateRequest() {
		let defer = $q.defer();
		initRequestCalcResult();
		let result = {};
		let isTravelTime = $scope.calcsettings.travelTime;
		let travelTime = isTravelTime ? $scope.request.travelTime : 0;
		let isDoubleDriveTime = $scope.isDoubleDriveTime;
		let min_hours = Number($scope.min_hours);
		let equipmentFee = $scope.equipmentFee.amount;
		let localdiscount = $scope.request.request_all_data.localdiscount;
		let rate = $scope.request.rate;
		let service = +$scope.request.serviceType;

		result.discount = 1;
		result.localdiscount = localdiscount;
		result.distance = $scope.request.request_all_data.request_distance.distances.AB.distance;

		if (localdiscount) {
			result.discount = (1 - localdiscount / 100);
			result.discountRate = rate / result.discount;
		}

		if (service === 7) {
			result.extraServiceTotal = CalculatorServices.calculateExtraServiceTotal($scope.request.extraServices);
			result.quote = $scope.request.longDistanceQuote;

			let fuelData = {
				serviceType: $scope.request.serviceType,
				zipFrom: $scope.request.zipFrom,
				zipTo: $scope.request.zipTo,
				quote: result.quote,
				request_distance: $scope.request.request_all_data.request_distance
			};

			CalculatorServices.getFuelSurcharge(fuelData)
				.then(function (response) {
					$scope.request.request_all_data.surcharge_fuel_avg = response.data.surcharge_fuel_avg;
					$scope.request.request_all_data.surcharge_fuel_perc = response.data.surcharge_fuel_perc;
					$scope.request.request_all_data.surcharge_fuel = response.data.surcharge_fuel;
					result.surcharge_fuel = response.data.surcharge_fuel;
					result.quote += response.data.surcharge_fuel + equipmentFee + result.extraServiceTotal;

					if (localdiscount) {
						result.discountQuote = result.quote / result.discount;
					}

					$scope.requestCalcResult = result;
				})
				.finally(function () {
					defer.resolve();
				});
		} else {
			let flatRate = service === 5 || service === 7;
			let requestStorage = service === 2 || service === 6;
			let duration = parseFloat($scope.request.duration);
			let distance = parseFloat(result.distance);
			let settingMiles = $scope.basicsettings.local_flat_miles;

			if (!!settingMiles && distance > settingMiles && !flatRate && !requestStorage) {
				$scope.request.travelTime += duration;
				travelTime += duration;
			}

			if (isDoubleDriveTime && !flatRate) {
				travelTime = $scope.request.double_travel_time;
			}

			result.minTime = $scope.request.min_time + travelTime;
			result.maxTime = $scope.request.max_time + travelTime;

			if (result.minTime < min_hours) {
				result.minTime = min_hours;
			}

			if (result.maxTime < min_hours) {
				result.maxTime = min_hours;
			}

			result.small_job = result.minTime === result.maxTime;

			result.minQuote = result.minTime * rate;
			result.maxQuote = result.maxTime * rate;

			// For Flat rate quote 0 when create request
			let fuelQuote = service !== 5 ? (result.minQuote + result.maxQuote) / 2 : 0;

			let fuelData = {
				serviceType: $scope.request.serviceType,
				zipFrom: $scope.request.zipFrom,
				zipTo: $scope.request.zipTo,
				quote: fuelQuote,
				request_distance: $scope.request.request_all_data.request_distance
			};

			CalculatorServices.getFuelSurcharge(fuelData)
				.then((response) => {
					$scope.request.request_all_data.surcharge_fuel_avg = response.data.surcharge_fuel_avg;
					$scope.request.request_all_data.surcharge_fuel_perc = response.data.surcharge_fuel_perc;
					$scope.request.request_all_data.surcharge_fuel = response.data.surcharge_fuel;
					result.surcharge_fuel = response.data.surcharge_fuel;
					result.minQuote += response.data.surcharge_fuel + equipmentFee;
					result.maxQuote += response.data.surcharge_fuel + equipmentFee;

					if (localdiscount) {
						result.discountMinQuote = result.minQuote / result.discount;
						result.discountMaxQuote = result.maxQuote / result.discount;
					}

					$scope.requestCalcResult = result;
				})
				.finally(() => defer.resolve());
		}

		return defer.promise;
	}

	function calculateStorageRequests() {
		let defer = $q.defer();
		initStorageCalcResult();
		let isTravelTime = $scope.calcsettings.travelTime;
		let travelTimeTo = isTravelTime ? $scope.requestToStorage.travelTime : 0;
		let travelTimeFrom = isTravelTime ? $scope.requestFromStorage.travelTime : 0;
		let isDoubleDriveTime = $scope.isDoubleDriveTime;
		let min_hours = $scope.min_hours;
		let equipmentFeeTo = $scope.equipmentFeeMovingTo.amount;
		let equipmentFeeFrom = $scope.equipmentFeeMovingFrom.amount;

		let localdiscount = +$scope.requestToStorage.request_all_data.localdiscount;
		let service = +$scope.requestToStorage.serviceType;

		let result = {
			to: {},
			from: {}
		};
		result.localdiscount = localdiscount;
		result.serviceType = service;
		result.to.rate = $scope.requestToStorage.rate;
		result.from.rate = $scope.requestFromStorage.rate;

		if (localdiscount) {
			result.discount = (1 - localdiscount / 100);
			result.to.discountRate = result.to.rate / result.discount;
			result.from.discountRate = result.from.rate / result.discount;
		}

		if (service === 6) {
			result.overnightRate = parseFloat($scope.basicsettings.overnight.rate); // Overnight Storage
		} else {
			result.overnightRate = 0;
		}

		if (isDoubleDriveTime) {
			travelTimeTo = $scope.requestToStorage.double_travel_time;
			travelTimeFrom = $scope.requestFromStorage.double_travel_time;
		}

		result.to.travelTime = travelTimeTo;
		result.from.travelTime = travelTimeFrom;

		result.to.minTime = $scope.requestToStorage.min_time + travelTimeTo;
		result.to.maxTime = $scope.requestToStorage.max_time + travelTimeTo;
		result.from.minTime = $scope.requestFromStorage.min_time + travelTimeFrom;
		result.from.maxTime = $scope.requestFromStorage.max_time + travelTimeFrom;

		if (result.to.minTime < min_hours) {
			result.to.minTime = min_hours;
		}

		if (result.to.maxTime < min_hours) {
			result.to.maxTime = min_hours;
		}

		result.to.small_job = result.to.minTime === result.to.maxTime;

		if (result.from.minTime < min_hours) {
			result.from.minTime = min_hours;
		}

		if (result.from.maxTime < min_hours) {
			result.from.maxTime = min_hours;
		}

		result.from.small_job = result.from.minTime === result.from.maxTime;

		result.to.minQuote = result.to.minTime * result.to.rate;
		result.to.maxQuote = result.to.maxTime * result.to.rate;
		result.from.minQuote = result.from.minTime * result.from.rate;
		result.from.maxQuote = result.from.maxTime * result.from.rate;

		let fuelDataTo = {
			serviceType: service,
			zipFrom: $scope.requestToStorage.zipFrom,
			zipTo: $scope.requestToStorage.zipTo,
			quote: (result.to.minQuote + result.to.maxQuote) / 2,
			request_distance: $scope.requestToStorage.request_all_data.request_distance
		};

		let fuelDataFrom = {
			serviceType: service,
			zipFrom: $scope.requestFromStorage.zipFrom,
			zipTo: $scope.requestFromStorage.zipTo,
			quote: (result.from.minQuote + result.from.maxQuote) / 2,
			request_distance: $scope.requestFromStorage.request_all_data.request_distance
		};

		let fuelPromiseTo = CalculatorServices.getFuelSurcharge(fuelDataTo);
		let fuelPromiseFrom = CalculatorServices.getFuelSurcharge(fuelDataFrom);
		let fuelPromise = {
			to: fuelPromiseTo,
			from: fuelPromiseFrom
		};

		$q.all(fuelPromise)
			.then((data) => {
				let fuel = data.to.data;
				$scope.requestToStorage.request_all_data.surcharge_fuel_avg = fuel.surcharge_fuel_avg;
				$scope.requestToStorage.request_all_data.surcharge_fuel_perc = fuel.surcharge_fuel_perc;
				$scope.requestToStorage.request_all_data.surcharge_fuel = fuel.surcharge_fuel;
				result.to.surcharge_fuel = fuel.surcharge_fuel;
				result.to.minQuote += fuel.surcharge_fuel + equipmentFeeTo + result.overnightRate;
				result.to.maxQuote += fuel.surcharge_fuel + equipmentFeeTo + result.overnightRate;

				fuel = data.from.data;
				$scope.requestFromStorage.request_all_data.surcharge_fuel_avg = fuel.surcharge_fuel_avg;
				$scope.requestFromStorage.request_all_data.surcharge_fuel_perc = fuel.surcharge_fuel_perc;
				$scope.requestFromStorage.request_all_data.surcharge_fuel = fuel.surcharge_fuel;
				result.from.surcharge_fuel = fuel.surcharge_fuel;
				result.from.minQuote += fuel.surcharge_fuel + equipmentFeeFrom;
				result.from.maxQuote += fuel.surcharge_fuel + equipmentFeeFrom;

				$scope.storageCalcResult = result;
			})
			.finally(() => defer.resolve());

		return defer.promise;
	}

	// USER LOGIN BUTTON
	$scope.user_login = function (login_email, login_password) {
		$scope.loginBusy = true;

		Analytics.trackEvent('Form', 'Login From Form', 'Login Event');

		AuthenticationService.Login(login_email, login_password, function (response) {
			if (response.success) {
				let user_credentials = response.user;
				$scope.userLogin = true;
				$scope.loginBusy = false;
				angular.element('#summery-alert').empty();
				$scope.request.current_user = {};
				$scope.request.current_user.uid = user_credentials.uid;
				$scope.request.current_user.field_user_first_name = user_credentials.field_user_first_name;
				$scope.request.current_user.field_user_last_name = user_credentials.field_user_last_name;
				$scope.request.current_user.field_primary_phone = user_credentials.field_primary_phone;
				$scope.request.current_user.mail = user_credentials.mail;
			} else {
				$scope.alertmessage = response.message;
				$scope.loginBusy = false;
			}
		});
	};

	$scope.user_logout = function () {
		AuthenticationService.Logout(function (response) {
			if (response) {
				$scope.userLogin = false;
				$scope.newUser = true;
			} else {
				$scope.userLogin = false;
			}
		});
	};

	$scope.userPage = function () {
		$window.location.href = host + 'account';
	};

	$scope.userSetting = function () {
		$window.location.href = host + 'user/' + $scope.request.current_user.uid + '/edit';
	};

	//RESET FORM
	$scope.resetForm = function () {
		angular.element(':input', '#movecalc-moving-form')
			.not(':button, :submit, :reset, :hidden, #edit-service')
			.val('')
			.removeAttr('checked')
			.removeAttr('selected');

		angular.element('#edit-zip-code-from').val('');

		angular.element('.form-item-Date-Storage').hide();
		angular.element('.inputicon-storage').hide();

		angular.element(':input', '#request-form')
			.not(':button, :submit, :reset, :hidden, #edit-service')
			.val('')
			.removeAttr('checked')
			.removeAttr('selected');

		angular.element('#edit-zip-code-from').val('');

		angular.element('.form-item-Date-Storage').hide();
		angular.element('.inputicon-storage').hide();
		angular.element('.success').removeClass('success');

		$scope.request = {
			'checkedRooms': {
				'rooms': []
			},
			'data': {},
			'request_all_data': {},
			'serviceType': serviceTypeId,
			'email': undefined,
			'url': $scope.requesturl,
			'current_user': $scope.request.current_user,
			'field_moving_to': {},
			'field_moving_from': {},
			'field_commercial_extra_rooms': [],
		};
		$scope.wasReset = true;
		angular.element('#calc-info-steps .box_info>div>div').empty();
		angular.element('#calc-info-steps .box_info').hide();
		angular.element('#calc-info-steps .calc-intro').show();

	};

	$scope.isSecondStepFirst = function () {
		return angular.isDefined($scope.movecalcFormSettings) && $scope.movecalcFormSettings.formStepsOrder[0].id == 1;
	};

	$scope.isThirdStepFirst = function () {
		return angular.isDefined($scope.movecalcFormSettings) && $scope.movecalcFormSettings.formStepsOrder[0].id == 2;
	};

	$scope.$watch('request.serviceType', onChangeServiceType);

	function onChangeServiceType() {
		$scope.isDoubleDriveTime = $scope.calcsettings && $scope.calcsettings.doubleDriveTime
			&& localServices.indexOf(parseInt($scope.request.serviceType)) >= 0;
	}

	$scope.$watch('movecalcFormSettings.companyColor', function () {
		if (angular.isDefined($scope.movecalcFormSettings)) {
			$scope.companyBGColor = {'background-color': $scope.movecalcFormSettings.companyColor};
			$scope.companyColorDarker = lightenDarkenColor($scope.movecalcFormSettings.companyColor, -0.1);
			$scope.companycolorLighter = lightenDarkenColor($scope.movecalcFormSettings.companyColor, 0.1);
		}
	});

	$scope.calendarOptions = {
		format: 'yyyy-mm-dd', // ISO formatted date
		onClose: (e) => {}
	};


	function lightenDarkenColor(hex, lum) {
		// validate hex string
		hex = String(hex).replace(/[^0-9a-f]/gi, '');

		if (hex.length < 6) {
			hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
		}

		lum = lum || 0;

		// convert to decimal and change luminosity
		var rgb = '#', c, i;

		for (i = 0; i < 3; i++) {
			c = parseInt(hex.substr(i * 2, 2), 16);
			c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
			rgb += ('00' + c).substr(c.length);
		}

		return rgb;
	}

	$scope.ScrollToTop = function () {
		angular.element('body').animate({scrollTop: 0}, 'slow');
	};

	$scope.parseTags = function (text) {
		if (text !== undefined) {
			text = text.replace(/\[phone\]/g, '<a href="tel: ' + $scope.basicsettings.company_phone + '">' + $scope.basicsettings.company_phone + '</a>');
			return $scope.toTrustedHTML(text);
		}
	};
}

let disableScroll = false;

function disableScrolling() {
	disableScroll = true;
}

function enableScrolling() {
	disableScroll = false;
}

function CreateMpopup(item) {
	var modal = '<div id="imgpopup" class="bubble">' + item + '</div>';

	return modal;
}


function ShowBigRoom(room) {
	var roomSize = angular.element('#edit-size-move').val();

	switch (room) {
		case (2): // Dinnig Room
			return '<img class="big_dinning_room_' + roomSize + '" title="Dinning Room" src="app/movecalculator/views/img/rooms/dinning.png">';
		case (3): // Office
			return '<img class="big_office_room_' + roomSize + '" title="Office" src="app/movecalculator/views/img/rooms/office.png">';
		case (4): // Extra room
			return '<img class="big_extra_room_' + roomSize + '" title="Extra room" src="app/movecalculator/views/img/rooms/extraroom.png">';
		case (5): // Basement
			return '<img class="big_basement_room" title="Basement" src="app/movecalculator/views/img/rooms/basement.png">';
		case (6): // Garage
			return '<img class="big_garage_room" title="Garage" src="app/movecalculator/views/img/rooms/garage.png"></a>';
		case (7): // Patio
			return '<img class="big_patio_room" title="Patio" src="app/movecalculator/views/img/rooms/smpatio.png">';
		case (8): // Play
			return '<img class="big_play_room_' + roomSize + '" title="Play Room" src="app/movecalculator/views/img/rooms/playroom.png"></a>';
		default:
			return '';
	}
}


//you can now use $ as your jQuery object.
angular.element(document.body)
	.on('mouseover', '.calc-intro_description .tooltips,.form-block .tooltips', function () {
		angular.element(this)
			.find('.rmtips')
			.show();
	})
	.on('mouseout', '.calc-intro_description .tooltips,.form-block .tooltips', function () {
		angular.element(this)
			.find('.rmtips')
			.hide();
	});


var ZoomImage = function () {
	//get allimages load in ajax whatever
	var mslink = angular.element('.calc-intro_description .msize').attr('data-href');
	var room = angular.element('.calc-intro_description .msize').attr('data-room');
	var size = angular.element('#edit-size-move').val();
	// get links of rooms
	var i = 0;
	var images = '<img id="s-img" class="' + room + '" src="' + mslink + '"><span class="cross"><i class="fa fa-times"></i></span>';

	angular.element('.calc-intro_description img').position();

	if (size == 8 || size == 9 || size == 10) {
		angular.element('#edit-extra-house-furnished-rooms input[type=checkbox]')
			.each(function () {
				if (angular.element(this).is(':checked')) {
					if (angular.element(this).val() != 2) {
						images = images + ShowBigRoom(angular.element(this).val());
					}

					i++;
				}
			});
	} else {
		angular.element('#edit-extra-furnished-rooms input[type=checkbox]')
			.each(function () {
				if (angular.element(this).is(':checked')) {
					images = images + ShowBigRoom(angular.element(this).val());
					i++;
				}
			});
	}

	// show in window all rooms

	var boxes = angular.element('.boxcount').clone();
	boxes = '<div id="bboxes">' + boxes.html() + '</div>';
	images = images + '<div class=\'big_image_desc\'>' + angular.element('.calc-intro_description .msize').text() + ' ' + angular.element('i.rooms').text() + '</div>' + boxes;

	angular.element(document.body).append(CreateMpopup(images));
	var winH = angular.element(window).height();
	var winW = angular.element(window).width();
	//Set the popup window to center
	var top = winH / 2 - winH / 4 - 50;
	var left = winW / 2 - winW / 4;

	angular.element('#imgpopup')
		.css({
			'position': 'fixed',
			'z-index': 1000,
			'top': top + 'px',
			'left': left + 'px'
		});

	if (room == 'room') {
		angular.element('#imgpopup')
			.css({'height': '450px'});
	}

	angular.element('#imgpopup')
		.position(angular.element('.calc-intro_description img')
			.position());
	angular.element('#imgpopup')
		.animate({opacity: 1}, 800);
	angular.element(document.body)
		.append('<div class="modal-backdrop fade in" style="opacity:1";></div>')
		.css({'overflow': 'hidden'});
	disableScrolling();

	return false;
};

angular.element(document.body)
	.on('click', '#zoomimage', ZoomImage);

angular.element(document.body)
	.on('click', '.modal-backdrop,.cross', function () {
		angular.element('body')
			.css({'overflow': 'auto'});
		enableScrolling();

		angular.element('#imgpopup')
			.stop()
			.animate({opacity: 0}, 500, function () {
				angular.element('.modal-backdrop')
					.stop()
					.animate({opacity: 0.1}, 400, function () {
						angular.element('#imgpopup')
							.remove();
						angular.element(this)
							.remove();
					});

			});

		return false;
	});

angular.element('body')
	.bind('touchmove', function (e) {
		if (disableScroll) {
			e.preventDefault();
		}
	});

angular.element(document)
	.ready(function () {
		angular.element('ultrasmall-form')
			.keypress(function (e) {
				if (e.which == 13) {
					return false;
				}
			});

		angular.element('move-calculator')
			.keypress(function (e) {
				if (e.which == 13) {
					return false;
				}
			});
	});
