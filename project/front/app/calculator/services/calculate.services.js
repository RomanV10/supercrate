(function () {

    angular
        .module('movecalc')

        .factory('CalculatorServices', CalculatorServices);
// NOTICE! to add any services test in both apps / aacount and moveboard , the have common files
    CalculatorServices.$inject = ['$q', 'config', '$http', 'CommercialCalc', 'geoCodingService'];

    function CalculatorServices($q, config, $http, CommercialCalc, geoCodingService) {
        var service = {};
        var ratesSettings = [];
        var longdistance = [];
        var fieldData = [];
        var data = [];
        var calcSettings = [];
        var basicSettings = [];
        var scheduleData = [];
        var isActiveBranching = false;

        service.getLongDistanceCode = getLongDistanceCode;
        service.getLongDistanceRate = getLongDistanceRate;
        service.getLongDistanceQuote = getLongDistanceQuote;
        service.getLongDistanceInfo = getLongDistanceInfo;
        service.getLongDistanceExtra = getLongDistanceExtra;

        service.increaseReservationPrice = increaseReservationPrice;
        service.initBasicService = initBasicService;

        service.calculate = calculate;
        service.getDuration = getDuration;
        service.getTravelTime = getTravelTime;
        service.calcDistance = calcDistance;
        service.getRate = getRate;
        service.geoCode = geoCode;
        service.getRequestCubicFeet = getRequestCubicFeet;
        service.getCubicFeet = getCubicFeet;
        service.calculateTime = calculateTime;
        service.getMovers = getMovers;
        service.init = init;
        service.getTrucks = getTrucks;
        service.getStorageRate = getStorageRate;
        service.getReservationPrice = getReservationPrice;
        service.checkExperation = checkExperation;
        service.getFullDistance = getFullDistance;
        service.getRequestServiceType = getRequestServiceType;
        service.getRoundedTime = getRoundedTime;
        service.getEquipmentFee = getEquipmentFee;
        service.getFuelSurcharge = getFuelSurcharge;
        service.calculateExtraServiceTotal = calculateExtraServiceTotal;

        return service;

        function getFuelSurcharge(data) {
            var data = {
                data: data
            };

            return $http.post(config.serverUrl + 'server/move_distance_calculate/calculate_fuel_surcharge', data);
        }

        function getRequestServiceType(from, to) {
            var deferred = $q.defer();

            var data = {
                "zip_from": from,
                "zip_to": to
            };

            $http
                .post(config.serverUrl + 'server/move_distance_calculate/get_request_service_type', data)
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function () {
                    deferred.reject();
                });

            return deferred.promise;
        }

        function increaseReservationPrice(request) {
            //1.Get Current Time
            //2. Get moveDate
            //3. If it's Less then settings
            //4. And it's notConfirmed. Increase Reservation rate
            var reservation = 0;
            if (request.service_type.raw != 5 && request.service_type.raw != 7) {
                var now = moment().unix();
                var then = moment(request.date.value).unix();
                var interval = (then - now) / 3600; // in hours
                var beforeHours = scheduleData.whenReservationRateIncrease;
                var incrRate = scheduleData.ReservationRateIncreaseRate;
                var incrQuote = scheduleData.ReservationRateIncreaseRatePercent;

                if (beforeHours >= interval) {
                    reservation = incrRate;

                    if (!reservation) {
                        reservation = request.quote.max * parseFloat(incrQuote) / 100;
                    }
                }
            }

            return reservation;
        }

        function checkExperation(request) {
            var now = moment().unix();
            var then = moment(request.date.value).unix();
            var interval = (then - now) / 3600; // in hours
            var valid = false;

            var beforeHours = 24 - parseInt(scheduleData.ExperationBeforeTime);
            var experationReqType = scheduleData.ExperationReqType;

            if (experationReqType == 'morning') {
                if (request.start_time1.value == "08:00 AM" || request.start_time1.value == "07:00 AM"
                    || request.start_time1.value == "09:00 AM" || request.start_time1.value == "10:00 AM") {
                    valid = true;
                }
            }

            if (beforeHours >= interval) {
                if (valid) {
                    return true;
                }
            }

            return false;
        }

        function getStorageRate(request, interval) {
            var result = [];
            var minRate = basicSettings.storage_rate.min;
            var maxRate = basicSettings.storage_rate.max;

            if (angular.isUndefined(interval)) {
                var now = request.moveDateTime;
                var then = request.deliveryDateTime;
                interval = (then - now) / 86400000;
            }

            if (!angular.isUndefined(request.move_size)) {
                var weight = getCubicFeet(request);
            } else {
                var weight = getTotalCubicFeet(request);
            }

            result.min = weight.weight * minRate;
            result.max = weight.weight * maxRate;

            return result;
        }

        function init(response) {
            ratesSettings = response.prices_array;
            longdistance = angular.fromJson(response.longdistance);
            fieldData = response.field_lists;
            data = response;
            calcSettings = angular.fromJson(response.calcsettings);
            basicSettings = angular.fromJson(response.basicsettings);
            scheduleData = angular.fromJson(response.schedulesettings);
            isActiveBranching = response.branches.length >= 2;
        }

        function getReservationPrice(request) {
            //check status
            var reservation = 0;
			let zeroReservation = 0;
            //IF FLAT RATE
            if (request.service_type.raw == 5) {
                reservation = scheduleData.flatReservationRate;
                if (!scheduleData.flatReservationRate) {
                    var persentage = parseFloat(scheduleData.flatReservation || zeroReservation);
                    reservation = request.quote * persentage / 100;
                }
            }
            else if (request.service_type.raw == 7) {
                reservation = scheduleData.longReservationRate;
                if (!scheduleData.longReservationRate) {

                    var persentage = parseFloat(scheduleData.longReservation || zeroReservation);
                    reservation = request.quote * persentage / 100;
                }
            }
            else {
                reservation = scheduleData.localReservationRate;

                if (!scheduleData.localReservationRate) {
                    var hours = parseFloat(scheduleData.localReservation || zeroReservation);
                    reservation = hours * parseFloat(request.rate.value);
                }
            }

            return reservation;

        }

        function geoCode(zip_code) {
            var deferred = $q.defer();

			geoCodingService.geoCode(zip_code)
				.then(resolve => {
					deferred.resolve({
						'city': resolve.city,
						'state': resolve.state,
						'postal_code': resolve.postal_code,
						'country': resolve.county,
						'full_adr': `${resolve.city}, ${resolve.state} ${resolve.zip}`,
					});
				},() => {
				    deferred.reject();
                });

            return deferred.promise;
        }

        function getLongDistanceExtra(request, weight) {
            var extra_services = [];
            var ABOVE_FLOOREXTRA_FIFTH = 5;

            //2.Long carry charges
            if (request.inventory
                && request.inventory.move_details
                && request.inventory.move_details.new_door) {

                var fee = 0;
                var curFeet = request.inventory.move_details.new_door;
                var extraFeet = parseFloat(longdistance.extrafeet);
                var extraFeetRate = longdistance.extrafeetrate;
                var extraFeetFlatFee = parseFloat(longdistance.extrafeet_flatfee);

                if (curFeet > extraFeet) {
                    var k = Math.round(curFeet / extraFeet);

                    if (extraFeetRate) {
                        fee = k * (extraFeetRate * Math.round(weight / 100));
                    } else if (extraFeetFlatFee) {
                        fee = k * extraFeetFlatFee;
                    }

                    if (fee) {
                        var carryCharge = service.initBasicService('Long Carry Fee', fee);
                        extra_services.push(carryCharge);
                    }
                }
            }

            //2.a Step Rate
            if (request.inventory
                && request.inventory.move_details) {
                var steps_origin = request.inventory.move_details.steps_origin;
                var stepsInFlight = parseFloat(longdistance.stepsInflight);
                var ratePerFlight = parseFloat(longdistance.chargePerflight);
                var freeFlights = parseFloat(longdistance.freeFlights);

                if (steps_origin > 2
                    && stepsInFlight > 0) {
                    //get Flights
                    var flights_origin = Math.ceil(steps_origin / stepsInFlight);
                    var chargeFlights = flights_origin - freeFlights;
                    var fee = ratePerFlight * chargeFlights;

                    if (fee) {
                        var stepsCharge = service.initBasicService('Extra Charge For Steps (Origin)', fee);
                        extra_services.push(stepsCharge);
                    }
                }

                var steps_origin = request.inventory.move_details.steps_destination;

                if (steps_origin > 2
                    && stepsInFlight > 0) {
                    //get Flights
                    var flights_origin = Math.ceil(steps_origin / stepsInFlight);
                    var chargeFlights = flights_origin - freeFlights;
                    var fee = ratePerFlight * chargeFlights;

                    if (fee) {
                        var stepsCharge = service.initBasicService('Extra Charge For Steps (Destination)', fee);
                        extra_services.push(stepsCharge);
                    }
                }
            }

            //3.Floors Extra charges
            if ( request.typeTo != 6
                && request.typeTo != 7
                && longdistance.isCalculateFloorMovingTo
                && request.typeTo > parseFloat(longdistance.above_floorextra) - 1) {

                var fee = 0;
                var rate = parseFloat(longdistance.floorextra);
                var flatRate = parseFloat(longdistance.floorextra_flatrate);
                var diff = parseFloat(request.typeTo) - parseFloat(longdistance.above_floorextra) + 1;

                if (rate) {
                    fee = diff * rate * Math.round(weight / 100);
                } else if (flatRate) {
                    fee = diff * flatRate;
                }

                if (fee) {
                    var extraCharge = service.initBasicService('Floors Extra Fee (Destination)', fee);
                    extra_services.push(extraCharge);
                }
            }

            //.4/ Extra charge when elevator
            if (request.typeTo == 6) {
                var fee = 0;
                var rate = parseFloat(longdistance.floorextrafifth);
                var flatRate = parseFloat(longdistance.floorextrafifth_flatrate);
                var diff = parseFloat(request.typeTo) - ABOVE_FLOOREXTRA_FIFTH;

                if (rate) {
                    fee = diff * rate * Math.round(weight / 100);
                } else if (flatRate) {
                    fee = diff * flatRate;
                }

                if (fee) {
                    var extraCharge = service.initBasicService('Elevator Extra Fee (Destination)', fee);
                    extra_services.push(extraCharge);
                }
            }

            //3.Floors Extra charges
            if ( request.typeFrom != 6
                && request.typeFrom != 7
                && longdistance.isCalculateFloorMovingFrom
                && request.typeFrom > parseFloat(longdistance.above_floorextra) - 1) {

                var fee;
                var rate = parseFloat(longdistance.floorextra);
                var flatRate = parseFloat(longdistance.floorextra_flatrate);
                var diff = parseFloat(request.typeFrom) - parseFloat(longdistance.above_floorextra) + 1;

                if (rate) {
                    fee = diff * rate * Math.round(weight / 100);
                } else if (flatRate) {
                    fee = diff * flatRate;
                }

                if (fee) {
                    var extraCharge = service.initBasicService('Floors Extra Fee (Origin)', fee);
                    extra_services.push(extraCharge);
                }
            }

            //.4/ Extra charge when elevator
            if (request.typeFrom == 6) {
                var fee = 0;
                var rate = parseFloat(longdistance.floorextrafifth);
                var flatRate = parseFloat(longdistance.floorextrafifth_flatrate);
                var diff = parseFloat(request.typeFrom) - ABOVE_FLOOREXTRA_FIFTH;

                if (rate) {
                    fee = diff * rate * Math.round(weight / 100);
                } else if (flatRate) {
                    fee = diff * flatRate;
                }

                if (fee) {
                    var extraCharge = service.initBasicService('Elevator Extra Fee (Origin)', fee);
                    extra_services.push(extraCharge);
                }
            }



            return extra_services;
        }

        function getLongDistanceCode(zip) {
            var deferred = $q.defer();

            $http
                .get(config.serverUrl + 'server/get_area_code/' + zip)
                .success(function (data) {

                    var areaCode = data[0];

                    deferred.resolve(areaCode);
                })
                .error(function (data) {

                    deferred.reject(data);
                });

            return deferred.promise;

        }


        function getLongDistanceInfo(request) {
            var result = {
                days: 0,
				minWeight: 0,
				minPrice: 0,
				minPriceEnabled: false
            };

            var to = request.addressTo;
            var from = request.addressFrom;

            if (!to.state
                || !from.state) {

                return result;
            }

            var state = to.state.toLowerCase();
            var baseState = from.state.toUpperCase();
			let stateSettings = longdistance.stateRates[baseState][state];

            if((!stateSettings.longDistance && !longdistance.acceptAllQuotes)
                || (!stateSettings['state_rate'] && (!stateSettings['rate'] || stateSettings['rate'] == ""))){
                return result;
            }

            result.days = longdistance.stateRates[baseState][state]['delivery_days'] || 0;
            result.min_weight = parseFloat(longdistance.stateRates[baseState][state]['min_weight'] || 0);
			result.minPrice = stateSettings.minPrice;
			result.minPriceEnabled = stateSettings.minPriceEnabled;
			result.minWeight = stateSettings.min_weight;

            return result;
        }

		function getLongDistanceRate(request) {
			var weight = getCubicFeet(request).weight;
			var to = request.field_moving_to;
			var from = request.field_moving_from;
			var rate = 0;
			var areaCode = to.premise;

            if (!to.administrative_area
                || !from.administrative_area) {

                return 0;
            }

			var state = to.administrative_area.toLowerCase();
			var baseState = from.administrative_area.toUpperCase();

			let stateSettings = longdistance.stateRates[baseState][state];

			if (stateSettings.state_rate) {
				rate = stateSettings.state_rate;
				rate = getDiscountRate(stateSettings.discounts, weight) || rate;
			} else if (stateSettings.rate && stateSettings.rate[areaCode]) {
				rate = stateSettings.rate[areaCode];
				if (stateSettings.areaDiscounts) {
					rate = getDiscountRate(stateSettings.areaDiscounts[areaCode], weight) || rate;
				}
			}

			return rate || 0;
		}

		function getDiscountRate(discounts, weight) {
			let rate = 0;

			if (discounts) {
				let discount = discounts.filter(discount => weight >= discount.startWeight).pop();

				if (discount) {
					rate = discount.rate;
				}
			}

			return rate;
		}

		function _getLDWeight(request) {
			let weight = 0;
			if (request.moveSize == 11) {
				weight = CommercialCalc.getCommercialCubicFeet(request.field_commercial_extra_rooms, calcSettings);
			} else {
				weight = getCubicFeet(request);
			}
			return weight;
		}

		function getLongDistanceQuote(request) {
			var weight = _getLDWeight(request);

			weight = weight.weight;

			var state = request.field_moving_to.administrative_area.toLowerCase();
			var baseState = request.field_moving_from.administrative_area.toUpperCase();
			let stateSettings = longdistance.stateRates[baseState][state];

			var rate = getLongDistanceRate(request);

			var linfo = getLongDistanceInfo(request);
			var min_weight = linfo.min_weight;

			if (!request.field_cubic_feet.value) {
				request.field_cubic_feet.value = (longdistance.default_weight == 1);
			}

			if (request.field_cubic_feet.value && min_weight > weight) {
				weight = min_weight;
			}

			if (!request.field_cubic_feet.value && min_weight * 7 > weight * 7) {
				weight = min_weight * 7;
			}

			if (!request.field_long_distance_rate.value) {
				request.field_long_distance_rate.value = rate;
			}

			var minPrice = stateSettings["minPrice"];
			var minWeight = stateSettings["min_weight"];
			var longDistanceQuote;

			if (!(isNaN(minPrice) || isNaN(minWeight)) && stateSettings["minPriceEnabled"]) {
				if (Number(minWeight) < weight) {
					longDistanceQuote = Number(minPrice) + request.field_long_distance_rate.value * (weight - Number(minWeight));
				} else {
					longDistanceQuote = Number(minPrice);
				}
			} else {
				longDistanceQuote = request.field_long_distance_rate.value * weight;
			}

			return longDistanceQuote;
		}

        function getCubicFeet(request) {
            // Get default size of apartment
            if (angular.isUndefined(request.moveSize)) {
                return 0;
            }

            var move_size = parseInt(calcSettings.size[request.moveSize]);
            // Get rooms if exist
            var total_room_weight = 0;

            angular.forEach(request.checkedRooms.rooms, function (rid, key) {
                total_room_weight += parseInt(calcSettings.room_size[rid]);
            });

            //Get Total Weight
            var total = [];
            total.weight = total_room_weight + move_size + parseInt(calcSettings.room_kitchen);
            total.min = total.weight - 100;
            total.max = total.weight + 100;

            return total;
        }

        function calculateTime(request, total_cf, callback) {
            // GET MOVER COUNT BASE ON total_cf and Entrance type
            var movers_count = getMovers(request, total_cf);
            var total_time = getTime(request, total_cf, movers_count);

            return total_time;
        }

        function calculate(request, calcSettings) {
            var deferred = $q.defer();

            // CALCULAT TOTAL CUBIC FEET
            var total_cf;

	        if (request.moveSize != 11) {
		        total_cf = getTotalCubicFeet(request);
            } else {
		        total_cf = CommercialCalc.getCommercialCubicFeet(request.field_commercial_extra_rooms, calcSettings);
            }

            // GET MOVER COUNT BASE ON total_cf and Entrance type
            var movers_count = getMovers(request, total_cf);
            var data = getTime(request, total_cf, movers_count);

            var results = {};
            results.small_job = false;
            results.work_time = data.work_time;
            results.min_hours = Number(data.min_hours);
            results.total_cf = data.total_cf;
            results.truck = data.trucks;
            results.crew = data.movers_count;
            var duration = request.duration;
            var distance = request.distance;
            results.travel_time = request.travelTime;

            let isLongDistance = request.serviceType == 7 || request.serviceType == 5;
            let isStorage = request.serviceType == 2 || request.serviceType == 6;

            if (!!basicSettings.local_flat_miles && distance > basicSettings.local_flat_miles && !isLongDistance && !isStorage) {
                duration = 0;
            }

            if (isLongDistance) {
                duration = 0;
            }

            // PICKUP TIME
            results.pickup_time = {};
            results.pickup_time.min = results.work_time.min; // + Travel Time Back To Office
            results.pickup_time.max = results.work_time.max; // + Travel Time Back To Office

            //LOCAL MOVE WORK TIME
            results.min_time = results.work_time.min;
            results.max_time = results.work_time.max;

            if(!calcSettings.doubleDriveTime && !calcSettings.isTravelTimeSameAsDuration){
                results.min_time += parseFloat(duration);
                results.max_time += parseFloat(duration);
            }

            results.min_time = getRoundedTime(results.min_time);
            results.max_time = getRoundedTime(results.max_time);

            // GET RATE
            results.rate = getRate(request.moveDate, results.crew, results.truck);

            results.quote_min = parseFloat(results.min_time);
            results.quote_max = parseFloat(results.max_time);

            results.total_quote_min = results.rate * results.quote_min;
            results.total_quote_max = results.rate * results.quote_max;


            deferred.resolve(results);

            return deferred.promise;
        }

        function getMovers(request, total_cf) {
            // Get default size of apartment
            var TYPE_FROM = request.typeFrom;
            var TYPE_TO = request.typeTo;

            if (TYPE_FROM == 7) {
              TYPE_FROM = 1;
            }
            if (TYPE_TO == 7) {
              TYPE_TO = 1;
            }

            var movers_count = calcSettings.min_movers;
            //GET HOW MANY MOVERS
            for (var i = 3; i <= 8; i++) {
                if (total_cf.weight >= calcSettings.mover_size[i]) {
                    movers_count = i;
                }
                else {
                    if (total_cf.weight >= calcSettings.mover_size_with_floor[i] && ( TYPE_FROM >= calcSettings.mover_size_floor[i] || TYPE_TO >= calcSettings.mover_size_floor[i] )) {
                        movers_count = i;
                    }
                }
            }

            return movers_count;
        }

        function getTime(request, total_cf, movers_count) {
            var dispertion = 50;
            var total = {};
            total.work_time = {};
            var TYPE_FROM = request.typeFrom;
            var TYPE_TO = request.typeTo;

	        if (TYPE_FROM == 7) TYPE_FROM = 1;
	        if (TYPE_TO == 7) TYPE_TO = 1;

            if (request.serviceType == 5 || request.serviceType == 7) {
                TYPE_TO = 1;
            }
            //GET LOADING Time
            var LOADING_SPEED = calcSettings.speed[movers_count] * (1 - calcSettings.floor_kof[TYPE_FROM] / 100);
            var LOADING_TIME_MIN = ((total_cf.weight - dispertion ) / LOADING_SPEED) * 0.6;
            var LOADING_TIME_MAX = ((total_cf.weight + dispertion) / LOADING_SPEED) * 0.6;

            //GET UNLOADING Time
            var UNLOADING_SPEED = calcSettings.speed[movers_count] * (1 - calcSettings.floor_kof[TYPE_TO] / 100);
            var UNLOADING_TIME_MIN = ((total_cf.weight - dispertion) / UNLOADING_SPEED) * 0.4;
            var UNLOADING_TIME_MAX = ((total_cf.weight + dispertion) / UNLOADING_SPEED) * 0.4;


            var TOTAL_TIME_MIN = LOADING_TIME_MIN + UNLOADING_TIME_MIN;
            var TOTAL_TIME_MAX = LOADING_TIME_MAX + UNLOADING_TIME_MAX;

            total.work_time.min = getRoundedTime(TOTAL_TIME_MIN);
            total.work_time.max = getRoundedTime(TOTAL_TIME_MAX);

            if (request.serviceType == 3) {
                total.work_time.min = getRoundedTime(LOADING_TIME_MIN);
                total.work_time.max = getRoundedTime(LOADING_TIME_MAX);
            }

            if (request.serviceType == 4) {
                total.work_time.min = getRoundedTime(UNLOADING_TIME_MIN);
                total.work_time.max = getRoundedTime(UNLOADING_TIME_MAX);
            }

            total.total_cf = total_cf;
            total.movers_count = movers_count;
            total.trucks = getTrucks(total_cf);
            total.min_hours = calcSettings.min_hours;

            return total;
        }

        function getRoundedTime(time) {
            var roundTime = roundTimeInterval(time);
            var result = round(roundTime, 2);
            return result;
        }

        function roundTimeInterval(time) {
            var minutes = 0;
            var hour = Math.floor(time);
            var fraction = time - hour;

            if (fraction > 0 && fraction <= 0.3) {
                minutes = 15 / 60;
            }

            if (fraction > 0.3 && fraction <= 0.55) {
                minutes = 30 / 60;
            }

            if (fraction > 0.55 && fraction <= 0.8) {
                minutes = 45 / 60;
            }

            if (fraction > 0.8) {
                minutes = 0;
                hour++;
            }

            var result = hour + round(minutes, 2);

            return result;
        }

        function round(number, point) {
            var decimal = 10 * point;
            return Math.round(number * decimal) / decimal;
        }

        function getTrucks(total_cf) {
            var trucks = 1;

            if (total_cf.weight >= calcSettings.trucks2)
                trucks = 2;

            if (total_cf.weight >= calcSettings.trucks3)
                trucks = 3;

            if (total_cf.weight >= calcSettings.trucks4)
                trucks = 4;

            if (total_cf.weight >= calcSettings.trucks5)
                trucks = 5;

            return trucks;
        }

        function getTotalCubicFeet(request) {
            // Get default size of apartment
            var move_size = parseInt(calcSettings.size[request.moveSize]);
            // Get rooms if exist
            var total_room_weight = 0;

            angular.forEach(request.checkedRooms.rooms, function (rid, key) {
                total_room_weight += parseInt(calcSettings.room_size[rid]);
            });

            //Get Total Weight

            var total = [];
            total.weight = total_room_weight + move_size + parseInt(calcSettings.room_kitchen);

            total.min = total.weight - 100;
            total.max = total.weight + 100;

            return total;
        }

        function getRequestCubicFeet(request) {
            // Get default size of apartment
            var move_size = 0;

            if (angular.isDefined(request.move_size)) {
                move_size = parseInt(calcSettings.size[request.move_size.raw]);
            }
            // Get rooms if exist
            var total_room_weight = 0;

            if (angular.isDefined(request.rooms)) {
                angular.forEach(request.rooms.value, function (rid, key) {
                    total_room_weight += parseInt(calcSettings.room_size[rid]);
                });
            }                        //Get Total Weight

            var total = [];
            total.weight = total_room_weight + move_size + parseInt(calcSettings.room_kitchen);
            total.min = total.weight - 100;
            total.max = total.weight + 100;

            return total;
        }


        function getTravelTime(from, to, serviceType) {
            var deferred = $q.defer();

            var data = {
                "zip_from": from,
                "zip_to": to,
                "move_service_type": serviceType
            };

            $http
                .post(config.serverUrl + 'server/move_distance_calculate/check_travel_time', data)
                .then(function (response) {
                    var result = response.data.duration || 0;

                    deferred.resolve(result);
                }, function () {
                    deferred.reject();
                });

            return deferred.promise;
        }


        function getDuration(from, to, serviceType) {
            var deferred = $q.defer();

            var data = {
                "zip_from": from,
                "zip_to": to,
                "move_service_type": serviceType
            };

            $http
                .post(config.serverUrl + 'server/move_distance_calculate/check_travel_time', data)
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function () {
                    deferred.reject();
                });

            return deferred.promise;
        }

        function getFullDistance(from, to) {
            var deferred = $q.defer();

            var data = {
                "zip_from": from,
                "zip_to": to
            };

            $http
                .post(config.serverUrl + 'server/move_distance_calculate/distance_origin_to_destination', data)
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function () {
                    deferred.reject();
                });

            return deferred.promise;
        }

        function getRate(date, movers, trucks) {
            var year = parseInt(moment(date).format('YYYY'));
            var date = moment(date).format('YYYY-MM-DD');

            var cc = data.calendar[year][date];
            var type = data.calendartype[cc];

            if (angular.isUndefined(type)) {
                type = 1;
            }

            var rate = ratesSettings[type];

            // TODO Need discuss with Roman
            // Create request anyway
            if (angular.isUndefined(rate) && isActiveBranching) {
                type = data.calendartype[1];
                rate = ratesSettings[type];
            }


            var extramoverRate = ratesSettings[type][3];
            var extratruckRate = ratesSettings[type][4];

            var truckRate = 0;
            var moverRate = 0;
            var extraMoverRate = 0;
            var moversId = movers - 2;

            if (movers > 4) {
                var extraMover = movers - 4;
                var extraMoverRate = extramoverRate * extraMover;
                rate = rate[2];
            } else {
                rate = rate[moversId];

            }

            if (trucks > 1) {
                var extraTrucks = trucks - 1;
                truckRate = extratruckRate * extraTrucks;
            }

            var totalRate = rate + extraMoverRate + truckRate;

            return totalRate;
        }


        function parseZipToAddress(arrAddress) {
            var city, state, country, postal_code;

            $.each(arrAddress, function (i, address_component) {
                if (address_component.types[0] == "administrative_area_level_1") {// State
                    if (address_component.short_name.length < 3) {
                        state = address_component.short_name;
                    }
                }

                if (address_component.types.indexOf("locality") > -1 || address_component.types.indexOf("neighborhood") > -1 || address_component.types.indexOf("sublocality") > -1) {// locality type
                    city = address_component.long_name;
                }

                if (address_component.types[0] == "country") {// locality type
                    country = address_component.short_name;
                }

                if (address_component.types[0] == "postal_code") {// "postal_code"
                    postal_code = address_component.short_name;
                }
            });


            var address = {
                'city': city,
                'state': state,
                'postal_code': postal_code,
                'country': country,
                'full_adr': city + ', ' + state + ' ' + postal_code
            };

            return address;
        }


        function calcDistance(request) {
            var deferred = $q.defer();

            var extraPickup = false;
            var extraDropoff = false;
            var zip_from = request.field_moving_from.postal_code;
            var zip_to = request.field_moving_to.postal_code;
            var pickup = request.field_extra_pickup.postal_code;
            var delivery = request.field_extra_dropoff.postal_code;
            var serviceType = request.service_type.raw;
            var result = [];

            if (pickup != null)
                extraPickup = pickup.length ? true : false;

            if (delivery != null)
                extraDropoff = delivery.length ? true : false;

            if (!extraPickup && !extraDropoff) {
                service.getDuration(zip_from, zip_to, serviceType).then(function (data) {
                    deferred.resolve(data);
                });
            }
            else if (extraPickup) {
                var promise = service.getDuration(zip_from, pickup, serviceType);
                var promise2 = service.getDuration(pickup, zip_to, serviceType);

                $q.all([promise, promise2]).then(function (data) {
                    result.duration = data[0].duration + data[1].duration;
                    result.distance = data[0].distance + data[1].distance;
                    deferred.resolve(result);
                });
            }
            else if (extraDropoff) {
                var promise = service.getDuration(zip_from, delivery, serviceType);
                var promise2 = service.getDuration(delivery, zip_to, serviceType);

                $q.all([promise, promise2]).then(function (data) {
                    result.duration = data[0].duration + data[1].duration;
                    result.distance = data[0].distance + data[1].distance;
                    deferred.resolve(result);
                });
            }
            else {
                var promise = service.getDuration(zip_from, pickup, serviceType);
                var promise2 = service.getDuration(pickup, delivery, serviceType);
                var promise3 = service.getDuration(delivery, zip_to, serviceType);

                $q.all([promise, promise2, promise3]).then(function (data) {
                    result.duration = data[0].duration + data[1].duration + data[2].duration;
                    result.distance = data[0].distance + data[1].distance + data[2].distance;
                    deferred.resolve(result);
                });
            }

            return deferred.promise;
        }


        function getEquipmentFee(distance) {
            var result = {
                name: basicSettings.equipment_fee.name,
                amount: 0
            };

            var resDistance = distanceRequest(distance, basicSettings.equipment_fee);

            if (resDistance.isExist) {
                result.amount = resDistance.amount;
            }

            return result;
        }


        function distanceRequest(distance, equipment_fee) {
            var distance = parseFloat(distance);
            var amount = 0;
            var isExistVariant = false;

            angular.forEach(equipment_fee.by_mileage, function (value) {
                if ((value.from < distance) && (distance <= value.to)) {
                    amount = value.amount;
                    isExistVariant = true;
                }
            });

            return {amount: amount, isExist: isExistVariant};
        }

        function calculateExtraServiceTotal(extraService) {
            var result = 0;

            angular.forEach(extraService, function (value, key) {
                if (value.extra_services[0]) {
                    result += value.extra_services[0].services_default_value;
                }
            });

            return result;
        }

    }

    function initBasicService(name, value) {
        //1.load extra services for this request
        //2. Calculate Total
        var basicService =
        {
            name: name,
            extra_services: [
                {
                    services_default_value: value,
                    services_name: "Cost",
                    services_read_only: false,
                    services_type: "Amount"
                },
            ]
        };

        return basicService;
    }
})();
