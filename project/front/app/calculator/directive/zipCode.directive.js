'use strict';

angular
	.module('app.calculator')
	.directive('zipcode', processZipCode);

processZipCode.$inject = ['$http', 'logger', 'geoCodingService'];

// Zip Code Directive
function processZipCode($http, logger, geoCodingService) {
	return {
		restrict: 'A',
		require: '?ngModel',
		scope: {
			'address': '=',
		},
		link: function (scope, element) {
			
			element.bind('change', function () {
				if (element.val().length != 5) {
					
					element.val("");
					
				} else {
					var zip_code = element.val();
					if (!angular.isUndefined(zip_code)) {
						if (zip_code.length == 5) {
							
							geoCodingService.geoCode(zip_code)
								.then(resolve => {
									scope.address = {
										'city': resolve.city,
										'state': resolve.state,
										'postal_code': resolve.postal_code,
										'country': resolve.county,
										'full_adr': `${resolve.city}, ${resolve.state} ${resolve.zip}`,
										
									};
									
									logger.success(scope.address.full_adr, "Success", "Address Was Founded");
								});
							
							
							//FIND AREA CODE /uszip.asmx/GetInfoByZIP?USZip=string HTTP/1.1
							var searchArea = 'http://www.webservicex.net/uszip.asmx/GetInfoByZIP?USZip=' + zip_code;
							
							$http.get(searchArea)
								.success(function (data, status, headers, config) {
									
									var area = data;
									
								})
								.error(function (data, status, headers, config) {
									
									var area = 0;
									
								});
							
						}
					}
				}
			});
		}
	};
};
