(function() {
    'use strict';


angular
.module('app.widgets')
 .directive('overlayPage', overlayPage);
    
    
function overlayPage(){

return {
    restrict: 'E',
    scope: {
      show: '@',
      onClosed :'&onClosed'
    },
    replace: true, // Replace with the template below
    transclude: true, // we want to insert custom content inside the directive
    link: function($scope, element, attrs) {

      $scope.dialogStyle = {};
        
      $scope.hideOverlay = function() {
        $scope.overlayShown = false;
          $(element).animate({'top':'3000px'},1500);
          $('body').css({"overflow": "auto"});
      };
        
    },
    template: "<div class='ng-overlay' show='overlayShown'><i class='overlay-close fa fa-times fa-3x' ng-click='hideOverlay()'></i><div class='content container' ng-transclude></div></div>"
    
    
    
    
  };
}


})();
