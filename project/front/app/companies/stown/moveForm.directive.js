import tmpHomeSlide from '../../movecalculator/views/homeSlide.html';
import tmpFullcalendar from '../../movecalculator/views/fullcalendar.html';
import tmpRateBlock from '../../movecalculator/views/rateBlock.html';
import tmpSmallDesktopForm from '../../movecalculator/views/smallDesktopForm.html';
import tmpRateBlockFull from '../../movecalculator/views/rateBlockFull.html';
import tmpMovecalculator from '../../movecalculator/views/movecalculator.html';
import tmpShortCalcForm from '../../movecalculator/views/shortCalcForm.html';
import tmpUltrasmall from './ultrasmall-form.html';
import tmpUltra_form  from '../../movecalculator/views/ultra-form.html';
import tmpCalendar  from '../../movecalculator/views/calendar.template.html';
import tmpMobileCongrats  from '../../movecalculator/views/mobileCongrats.html';
import tmpSmallForm  from '../../movecalculator/views/smallForm.html';
import tmpPreferedTime  from '../../movecalculator/directives/preferedTime.html';
import tmpLocalmoveCalcResult  from '../../movecalculator/views/localmoveCalcResult.html';
import tmpButtonToTop from '../../movecalculator/views/buttonToTop.html';

(function () {
    'use strict';

    var scripts = document.getElementsByTagName("script");
    var currentScriptPath = scripts[scripts.length - 1].src;
    var relativePathDiretive = currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/'));
    var relativePath = relativePathDiretive.substring(0, relativePathDiretive.lastIndexOf('/') + 1);

    angular
        .module('movecalc')
        .directive('formdatepicker', processDatePicker)
        .directive('zipcode', processZipCode)
        .directive('sizeofmove', processSizeOfMove)
        .directive('servicetype', processServiceType)
        .directive('googleMap', processgoogleMap)
        .directive('isnotempty', isnotempty)
        .directive('passwordconfirm', passwordConfirm)
        .directive('passwordstrong', passwordStrong)
        .directive('confirmemail', confirmEmail)
        .directive('isemail', IsEmail)
        .directive('sfisemail', sfIsEmail)

        .directive('movePreferedtime', preferedTime)
        .directive('validatephone', validatePhone)
        .directive('diractionmap', diractionmap)
        .directive('elromcotooltips', elromcotooltips)
        .directive('ccRequestForm', ccRequestForm)
        .directive('ichecks', ichecks)
        .directive('mobileCongrats', mobileCongrats)
        .directive('calendar', calendar)
        .directive('showData', showData)
        .directive('ultraForm', ultraForm)
        .directive('ultrasmallForm', ultrasmallForm)



        //Directive Implementation
        .directive('homeSlide', function () {
            return {
                restrict: 'E',
                templateUrl: tmpHomeSlide
            };
        })

        .directive('calendarButton', function () {
            return {
                restrict: 'E',
                templateUrl: tmpFullcalendar
            };
        })

        .directive('rateBlock', function () {
            return {
                restrict: 'E',
                templateUrl: tmpRateBlock
            };
        })

        .directive('smallDesktopForm', function () {
            return {
                restrict: 'E',
                templateUrl: tmpSmallDesktopForm
            };
        })

        //Directive Implementation
        .directive('rateBlockFull', function () {
            return {
                restrict: 'E',
                templateUrl: tmpRateBlockFull
            };
        })

        //Directive Implementation
        .directive('moveCalculator', function () {
            let ifMobile = /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || window.innerWidth < 700;
            return ifMobile ? {
                    templateUrl: tmpButtonToTop,
                    restrict: 'E'
                } : {
                    restrict: 'E',
                    templateUrl: tmpMovecalculator
                };
        })

        .directive('moveShortform', function () {
            return {
                restrict: 'E',
                templateUrl: tmpShortCalcForm
            };
        });

    function ultrasmallForm() {
        var directive = {
            controller: ultraFormController,
            templateUrl: tmpUltrasmall,
            restrict: 'E'
        };

        return directive;

		ultraFormController.$inject = ['$scope', '$element', '$attrs'];
        function ultraFormController($scope,$element, $attrs) {
            $scope.sourceTopForm = ($attrs.source == undefined) ? 'Website' : $attrs.source;
            $scope.sourceMobileForm = ($attrs.sourcemobile == undefined) ? 'Mobile' : $attrs.sourcemobile;

            $scope.threesteps = true;
            $scope.sizeChoosen = false;

            $scope.step1 = true;
            $scope.step2 = false;
            $scope.step3 = false;
            $scope.showStep3 = function(){
                $scope.step1 = false;
                $scope.step2 = false;
                $scope.step3 = true;
            };
            $scope.showStep2 = function(){
                $scope.step1 = false;
                $scope.step2 = true;
                $scope.step3 = false;
            };
            $scope.showStep1 = function(){
                $scope.step1 = true;
                $scope.step2 = false;
                $scope.step3 = false;
            };

            $scope.$watch('request.moveSize', function () {
                if(angular.isDefined($scope.request.moveSize)){
                    $scope.sizeChoosen = true;
                    angular.element("#sf-move-size").removeClass('error');
                }

            });

            $scope.serviceneed = false;
        }
    }

    function ultraForm() {
        var directive = {
            controller: ultraFormController,
            templateUrl: tmpUltra_form,
            restrict: 'E'
        };

        return directive;

        function ultraFormController($scope) {
            $scope.step1 = true;
            $scope.step2 = false;
        }
    }

    function calendar() {
        var directive = {
            link: link,
            scope: {
                month: "=",
                year: "=",
                calendar: "=",
                types: "=",
            },
            templateUrl: tmpCalendar,
            restrict: "E",
        };

        return directive;

        function link(scope, element, attrs) {
            var admin = false;

            if (angular.isDefined(attrs.admin)) {
                admin = true;
            }

            if (parseInt(scope.month) < 10)
                var month = scope.year + '-0' + scope.month + '-01';
            else
                var month = scope.year + '-' + scope.month + '-01';
            scope.month_number = scope.month;
            scope.month = moment(month);
            scope.rate_types = scope.types;
            scope.current_popup = '';
            scope.current_day = '';
            var clicked_date = '';

            var start = scope.month.clone();

            start.date(1);
            _removeTime(start.day(0));

            _buildMonth(scope, start, scope.month);

            scope.select = function (day) {
                scope.selected = day.date;
            };

            function padDigits(number, digits) {
                return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
            }

            function _removeTime(date) {
                return date.day(0).hour(0).minute(0).second(0).millisecond(0);
            }

            function _buildMonth(scope, start, month) {
                scope.weeks = [];
                var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
                while (!done) {
                    scope.weeks.push({days: _buildWeek(date.clone(), month)});
                    date.add(1, "w");
                    done = count++ > 2 && monthIndex !== date.month();
                    monthIndex = date.month();
                }
            }

            function getType(date, calendar) {
                var year = moment(date).format('YYYY');

                if (angular.isDefined(calendar[year]))
                    return calendar[year][date];
                else
                    return '8';
            }

            function _buildWeek(date, month) {
                var days = [];

                for (var i = 0; i < 7; i++) {
                    days.push({
                        name: date.format("dd").substring(0, 1),
                        number: date.date(),
                        isCurrentMonth: date.month() === month.month(),
                        type: getType(date.format("YYYY-MM-DD"), scope.calendar),
                        isToday: date.isSame(new Date(), "day"),
                        date: date
                    });
                    date = date.clone();
                    date.add(1, "d");
                }

                return days;
            }
        }
    }

    function mobileCongrats() {
        var directive = {
            controller: mobileCongratsController,
            templateUrl: tmpMobileCongrats,
            restrict: 'E'
        };

        return directive;

        function mobileCongratsController($scope) {
            $scope.mobile = true;
        }
    }

    function ccRequestForm() {
        var directive = {
            controller: RequestFormController,
            templateUrl: tmpSmallForm,
            restrict: 'E'
        };

        return directive;

        function RequestFormController($scope) {
            $scope.mobile = true;
        }
    }

    function elromcotooltips() {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.bind('mouseover', function () {
                    element.find('.rmtips').show();
                });

                element.bind('mouseout', function () {
                    element.find('.rmtips').hide();
                });
            }
        };

    };

    function ichecks($timeout) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                return $timeout(function () {

                    var value;
                    value = attrs['value'];

                    scope.$watch(attrs['ngModel'], function () {
                        angular.element(element).iCheck('update');
                    })

                    return angular.element(element).iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_flat-grey',
                        increaseArea: '20%'
                    }).on('ifChanged', function (event) {
                        if (angular.element(element).attr('type') === 'checkbox' && attrs['ngModel']) {
                            scope.$apply(function () {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }

                        if (angular.element(element).attr('type') === 'radio' && attrs['ngModel']) {
                            return scope.$apply(function () {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    })
                });
            }
        };
    }

    function diractionmap() {
        return {
            restrict: 'E',
            replace: true,
            template: '<div></div>',
            link: function (scope, element, attrs) {
                var initializeMap = function (element, attrs) {
                    var myOptions = {
                        draggable: false,
                        navigationControl: false,
                        scrollwheel: false,
                        streetViewControl: false,
                        zoom: 13,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        center: [-33.879, 151.235]
                    };

                    var mapObject = new google.maps.Map(document.getElementById(element.id), myOptions);

                    var directionsService = new google.maps.DirectionsService();
                    var directionsRequest = {
                        origin: attrs.from,
                        destination: attrsto,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING,
                        unitSystem: google.maps.UnitSystem.METRIC
                    };
                    directionsService.route(
                        directionsRequest,
                        function (response, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                new google.maps.DirectionsRenderer({
                                    map: mapObject,
                                    directions: response
                                });
                            }
                            else
                                angular.element("#error").append("Unable to retrieve your route<br />");
                        }
                    );
                }
            }
        };
    }

    function preferedTime(MoveRequestServices) {
        return {
            restrict: 'E',
            templateUrl: tmpPreferedTime,
            link: function (scope, element) {
                var prefopen = false;
                element.find('#prefeefe').bind('click', function () {
                    angular.element("div#pref_popup").show();
                });

                angular.element("div#pref_popup .select_item").bind('click', function () {
                    angular.element("#pref_popup .select_item").removeClass('active');
                    angular.element(this).addClass('active');

                    var val = angular.element(this).attr("data-value");

                    scope.request.preferedTime = val;

                    element.removeClass("error");
                    angular.element("#prefeefe").text(angular.element(this).text()).removeClass("error").removeClass("required");

                    angular.element("#pref_popup").hide();
                });
            }
        };
    }

    function sfIsEmail(AuthenticationService, SweetAlert) {
        return {
            restrict: 'A',
            link: function (scope, element, ngModel) {
                element.bind('change', function () {
                    if (element.val().length == 0) {
                        angular.element("#edit-confirmemail").val('');
                    }
                    else {
                        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        var ismail = regex.test(element.val());

                        if (!ismail) {
                            element.addClass('error');
                            element.val('');
                        }
                    }
                });
            }
        };
    };

    function IsEmail(AuthenticationService) {
        return {
            restrict: 'A',
            link: function (scope, element, ngModel) {
                element.bind('change', function () {
                    if (element.val().length == 0) {
                        angular.element("#edit-confirmemail").val('');
                    }
                    else {
                        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        var ismail = regex.test(element.val());

                        if (!ismail) {
                            element.addClass('error');
                            angular.element("#edit-confirmemail").val('');
                        }
                    }
                });

                element.bind('copy', function (e) {
                    e.preventDefault();
                });
            }
        };
    }

    function confirmEmail() {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.bind('change', function () {
                    var email = angular.element('#edit-email').val();
                    var confEmail = element.val();
                    if (angular.element("#edit-confirmemail").val() == angular.element('#edit-email').val()) {
                        //Everything is ok!
                        angular.element("#summery-alert").hide().empty();
                        angular.element(this).removeClass('error');
                    } else {
                        angular.element("#edit-confirmemail").val('').addClass('error');

                        angular.element("#summery-alert").empty();
                        angular.element("#summery-alert").show().append("<p>The Confirmation Email must match.</p>");
                    }
                });

                element.bind('copy paste', function (e) {
                    e.preventDefault();
                });
            }
        };
    }

    function validatePhone() {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.bind('change', function () {
                    if (element.val().length < 14) {
                        angular.element("#summery-alert").empty();
                        angular.element("#summery-alert").show().append("<p>Please enter 10 digit phone number</p>");
                        element.addClass("error");
                    }
                });

                scope.$watch(attrs.ngModel, function (nval) {
                    if (!angular.isUndefined(nval)) {
                        if (element.val().length == 14) {
                            element.removeClass("error");
                            angular.element("#summery-alert").empty().hide();
                        }
                    }
                });
            }
        };
    }

    function isnotempty() {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.bind('change', function () {
                    if (element.val().length)
                        element.removeClass("error");
                });
            }
        };
    }

    function passwordStrong() {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.bind('change', function () {
                    if (element.val().length < 6) {
                        angular.element("#summery-alert").empty();
                        angular.element("#summery-alert").show().append("<p>Please enter 6 or more characters. Leading or trailing spaces will be ignored.</p>");
                        element.addClass("error");
                    }
                    else {
                        element.removeClass("error");
                        angular.element("#summery-alert").hide().empty();
                    }
                });
            }
        };
    }

    function passwordConfirm() {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.bind('change', function () {
                    if (element.val() == angular.element('#edit-passowrd-signup').val()) {
                        //Everything is ok!
                        angular.element("#summery-alert").hide().empty();
                        element.removeClass('error');
                    } else {
                        element.val('').addClass('error');

                        angular.element("#summery-alert").empty();
                        angular.element("#summery-alert").show().append("<p>Password does not match the confirm password.</p>");
                    }
                });
            }
        };
    }

    function processgoogleMap(FormHelpServices) {
        return {
            restrict: 'A',
            transclude: false,
            templateUrl: tmpLocalmoveCalcResult,
            replace: false,
            link: function (scope, element, attrs, ngModel) {
                FormHelpServices.showMap(element.id, scope.request.zipFrom, scope.request.zipTo);

                scope.$watch(attrs.request.zipFrom, function (val) {
                    alert(val);
                });
            }
        };
    }

// Size of Move Directive
    function processSizeOfMove(FormHelpServices) {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                var firstInit = true;
                scope.$watch(attrs.ngModel, function (nval) {
                    if (!angular.isUndefined(nval)) {
                        element.removeClass('error');
                        FormHelpServices.showOnFormSizeofMoveChanges(nval);
                    }
                });
            }
        };
    };

// Zip Code Directive
	function processZipCode(geoCodingService) {
		return {
			restrict: 'A',
			require: '?ngModel',
			link: function (scope, element, attrs) {
				element.bind('change', function () {
					if (element.val().length != 5) {
						element.addClass("error");
						element.val('');
					}
				});

				scope.$watch(attrs.ngModel, function (zip_code) {

					if (!angular.isUndefined(zip_code)) {
						element.removeClass('success');

						if (zip_code.length == 5) {
							element.removeClass('error');

							angular.element(".calc-intro").hide();
							angular.element("#calc-info-steps .box_info").show();
							var type = attrs.ngModel;

							geoCodingService.geoCode(zip_code)
								.then(resolve => {
									let address = {
										'city': resolve.city,
										'state': resolve.state,
										'postal_code': resolve.zip,
										'country': resolve.county,
										'full_adr': `${resolve.city}, ${resolve.state} ${resolve.zip}`,
									};
									let type = attrs.ngModel;
									if (type == 'request.zipFrom') {
										scope.request.addressFrom = address;
										element.addClass('success');
									} else {
										scope.request.addressTo = address;
										element.addClass('success');
									}
									showAddress(address, type);
								}, () => {
									let spanExplanationOfError = `<span class="error_alarm">Not found zip code ${zip_code}.</span>`;

									if (type == 'request.zipFrom') {
										angular.element("#calc-info-steps .box_info .moving-from").empty().append(`<h3>Moving From:</h3> ${spanExplanationOfError}`).show("slow");
									} else {
										angular.element("#calc-info-steps .box_info .moving-to").empty().append(`<h3>Moving To:</h3> ${spanExplanationOfError}`).show("slow");
									}

									element.addClass('error');
									element.val('');
									element.removeClass('success');
								});
						}
					}
				});
			}
		};
	}

//Date Picker Directive
    function processDatePicker($rootScope) {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.datepicker({
                    dateFormat: 'MM d, yy',
                    numberOfMonths: 2,
                    changeMonth: false,
                    changeYear: false,
                    showButtonPanel: true,
                    minDate: new Date(),
                    onSelect: function (date) {
                        scope.date = date;
                        scope.$apply();

                        element.removeClass('error');
                        // Set Min Date for Storage Service Can not storage date more then move date
                        var sdp = angular.element("#edit-date-storage-datepicker-popup-0").val();

                        if (sdp.length && element[0].id == 'edit-move-date-datepicker-popup-0') {
                            var mdate = new Date(date).getDate();
                            var ddate = new Date(sdp).getDate();

                            if (ddate >= mdate) {
                                angular.element("#edit-date-storage-datepicker-popup-0").val('');
                            }
                        }

                        if (element[0].id == 'edit-move-date-datepicker-popup-0') {
                            angular.element("#calc-info-steps .calc-intro").hide();
                            angular.element("#calc-info-steps .box_info").show();
                            angular.element("#calc-info-steps .box_info .moving-date").empty().append('<h3>Move Date:</h3> <span class="company-color">' + date + '</span>').show("slow");
                            angular.element("#calc-info-steps .box_info .service-type").empty().append('<h3>Type of service:</h3><span class="company-color">' + angular.element('#edit-service option:selected').text() + '</span>').show("slow");
                        }

                        if (element[0].id == 'edit-date-storage-datepicker-popup-0') {
                            angular.element("#calc-info-steps .calc-intro").hide();
                            angular.element("#calc-info-steps .box_info").show();
                            angular.element("#calc-info-steps .box_info .delivery-date").empty().append('<h3>Delivery Date:</h3> <span class="company-color">' + date + '</span>').show("slow");

                            //     angular.element("#calc-info-steps .box_info .delivery-date").empty().append('<h3>Delivery Date:</h3> <span>'+date +'</span>').show("slow");
                            angular.element("#edit-calculator #edit-date-storage-datepicker-popup-0").val(element.val());
                        }

                        // Short Form Fix
                        if (element[0].id == 'short-datepicker') {
                            angular.element("#calc-info-steps .calc-intro").hide();
                            angular.element("#calc-info-steps .box_info").show();
                            angular.element("#edit-move-date-datepicker-popup-0").val(element.val());
                            angular.element("#calc-info-steps .box_info .moving-date").empty().append('<h3>Move Date:</h3> <span class="company-color">' + date + '</span>').show("slow");
                            angular.element("#calc-info-steps .box_info .service-type").empty().append('<h3>Type of service:</h3><span class="company-color">' + angular.element('#edit-service option:selected').text() + '</span>').show("slow");
                        }
                    },
                    onChangeMonthYear: function () {
                        beforeshowCal(element);
                    },
                    beforeShow: function () {

                        beforeshowCal(element);

                        var ddp = angular.element("#edit-move-date-datepicker-popup-0").val();
                        var sdp = angular.element("#edit-date-storage-datepicker-popup-0").val();
                        var mdate = new Date(ddp);
                        var ddate = mdate.setDate(mdate.getDate() + 2)

                        if (angular.isDefined(ddp)) {
                            if (ddp.length && element[0].id == 'edit-date-storage-datepicker-popup-0') {
                                element.datepicker("option", "minDate", new Date(ddate));
                            }
                        }
                    },
                    beforeShowDay: function (date) {
                        return checkDate(date, $rootScope.globals.front);
                    }
                });
            }
        };
    };

    function processServiceType($rootScope, $http) {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                var firstInit = true;
                scope.$watch(attrs.ngModel, function (nval) {

                    if (!angular.isUndefined(nval)) {
                        if (firstInit) {
                            firstInit = false;

                            return;
                        }

                        applyServiceTypeChanges(nval);
                    }
                });

            }
        };
    };


    function applyServiceTypeChanges(nval) {
    	var names = ['',"Local Moving","Moving With Storage","Loading Help",
			"Unloading Help", 'Flat Rate',
			"Overnight", "Long Distance"];
        var name = names[nval];

        angular.element("#calc-info-steps .box_info .service-type").empty().append('<h3>Type of service:</h3><span class="company-color">' + name + '</span>').show("slow");

        if (nval == 2 || nval == 6) { // Moving and storage
            angular.element("#edit-date-storage-datepicker-popup-0").addClass("required");
            angular.element(".form-item-Date-Storage").show();
            angular.element("#calc-info-steps .box_info .delivery-date").show();
            angular.element(".inputicon-storage").show();
            angular.element(".form-item-prefered-fromstorage-time").show();
            angular.element("#edit-prefered-fromstorage-time").addClass("required");

            if (nval == 6) {
                angular.element("#edit-date-storage-datepicker-popup-0").removeClass("required");
                angular.element(".form-item-Date-Storage").hide();

                var moveDate = angular.element("#edit-move-date-datepicker-popup-0").val();
                var nextDay = new Date(moveDate);
                nextDay.setDate(nextDay.getDate() + 1);
                nextDay = $.datepicker.formatDate('MM d, yy', nextDay);

                angular.element("#calc-info-steps .box_info .delivery-date").empty().append('<h3>Delivery Date:</h3> <span class="company-color">' + nextDay + '</span>').show("slow");
                angular.element("#edit-date-storage-datepicker-popup-0").val(nextDay);
            }
        }
        else {
            angular.element("#edit-date-storage-datepicker-popup-0").removeClass("required");
            angular.element(".inputicon-storage").hide();
            angular.element(".form-item-Date-Storage").hide();
            angular.element("#edit-prefered-fromstorage-time").removeClass("required");
            angular.element("#calc-info-steps .box_info .delivery-date").hide();
        }

        if (nval == '3' || nval == '8') {//Loading Help or Packing Day
            angular.element("#edit-zip-code-to").attr("disabled", "disabled").addClass("disabled").removeClass("required");
            angular.element("#edit-zip-code-from").removeAttr("disabled").removeClass("disabled").addClass("required");

            //angular.element("#toautocomplete").attr("disabled", "disabled").addClass("disabled").removeClass("required");
            //angular.element("#fromautocomplete").removeAttr("disabled").removeClass("disabled").addClass("required");

            angular.element("#address_to").attr("disabled", "disabled").addClass("disabled").removeClass("required");
            angular.element("#address_from").removeAttr("disabled").removeClass("disabled");

            angular.element("#edit-moving-to-apt").attr("disabled", "disabled").addClass("disabled");
            angular.element("#edit-moving-from-apt").removeAttr("disabled").removeClass("disabled");

            angular.element("#edit-zip-code-to").val("");
            angular.element(".info_block .moving-to").empty();
            angular.element("#edit-type-to").attr("disabled", "disabled").addClass("disabled").removeClass("required");
            angular.element("#edit-type-from").removeAttr("disabled").removeClass("disabled").addClass("required");
        }
        else if (nval == '4') {//UnLoading Help
            angular.element("#edit-zip-code-from").attr("disabled", "disabled").addClass("disabled").removeClass("required");
            angular.element("#edit-zip-code-to").removeAttr("disabled").removeClass("disabled").addClass("required");

            //	angular.element("#fromautocomplete").attr("disabled", "disabled").addClass("disabled").removeClass("required");
            //	angular.element("#toautocomplete").removeAttr("disabled").removeClass("disabled").addClass("required");

            angular.element("#address_from").attr("disabled", "disabled").addClass("disabled").removeClass("required");
            angular.element("#address_to").removeAttr("disabled").removeClass("disabled");

            angular.element("#edit-moving-from-apt").attr("disabled", "disabled").addClass("disabled");
            angular.element("#edit-moving-to-apt").removeAttr("disabled").removeClass("disabled");

            angular.element("#edit-zip-code-from").val("");
            angular.element(".info_block .moving-from").empty();
            angular.element("#edit-type-from").attr("disabled", "disabled").addClass("disabled").removeClass("required");
            angular.element("#edit-type-to").removeAttr("disabled").removeClass("disabled").addClass("required");
        }
        else {
            angular.element("#edit-zip-code-from").removeAttr("disabled").removeClass("disabled").addClass("required");
            angular.element("#edit-zip-code-to").removeAttr("disabled").removeClass("disabled").addClass("required");
            angular.element("#edit-type-from").removeAttr("disabled").removeClass("disabled").addClass("required");
            angular.element("#edit-type-to").removeAttr("disabled").removeClass("disabled").addClass("required");

            angular.element("#edit-moving-to").removeAttr("disabled").removeClass("disabled");
            angular.element("#edit-moving-from").removeAttr("disabled").removeClass("disabled");

            angular.element("#edit-moving-to-apt").removeAttr("disabled").removeClass("disabled");
            angular.element("#edit-moving-from-apt").removeAttr("disabled").removeClass("disabled");

            angular.element("#address_to").removeAttr("disabled").removeClass("disabled");
            angular.element("#address_from").removeAttr("disabled").removeClass("disabled");
        }
    }


//Helpers function
    function beforeshowCal(element) {
        setTimeout(function () {
            var btn = angular.element('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">Clear</button>');
            var buttonPane = element
                .datepicker("widget")
                .find(".ui-datepicker-buttonpane");

            buttonPane.find(".ui-datepicker-current").hide();
            buttonPane.find(".ui-datepicker-close").hide();
            buttonPane.find(".pricehelp").remove();

            var helps = "<span class='pricehelp'><li class='type dicount'>Discount</li><li class='type regular'>Regular</li><li class='type subpeak'>Sub-peak</li><li class='type peak'>Peak</li><li class='type highpeak'>Hi-peak</li></span>";

            buttonPane.append(helps);
        }, 1);
    }

    function checkDate(date, data) {
        var calendartype = data.calendartype;
        var calendar = data.calendar;

        var day = moment(date).format('DD');
        var month = moment(date).format('MM');
        var year = date.getFullYear();
        date = year + '-' + month + '-' + day;

        if (calendar[year][date] == '5')
            return [false, 'not-available', 'This date is NOT available'];
        else
            return [true, calendartype[calendar[year][date]], calendartype[calendar[year][date]]];
    }

    function showAddress(address, type) {
        if (type == 'request.zipTo') {
            angular.element('#edit-moving-to-state').val(address.state);
            angular.element('#edit-moving-to-city').val(address.city);
            angular.element('#edit-moving-to-zip').val(address.postal_code);

            var addr = address.city + ', ' + address.state + ' ' + address.postal_code;
            angular.element("#calc-info-steps .box_info .moving-to").empty().append('<h3>Moving To:</h3><span class="company-color">' + addr + '</span>').show("slow");
            angular.element('input[name="movers_to_fullzip"]').val(addr);

        } else {
            angular.element('#edit-moving-from-state').val(address.state);
            angular.element('#edit-moving-from-city').val(address.city);
            angular.element('#edit-moving-from-zip').val(address.postal_code);

            var addr = address.city + ', ' + address.state + ' ' + address.postal_code;
            angular.element("#calc-info-steps .box_info .moving-from").empty().append('<h3>Moving From:</h3><span class="company-color">' + addr + '</span>').show("slow");
            angular.element('input[name="movers_from_fullzip"]').val(addr);
        }
    }

    function showData($compile) {
        var directive = {
            link: linker
        };

        return directive;

        function linker(scope, element, attrs) {
            var el;

            attrs.$observe('template', function (tpl) {
                if (angular.isDefined(tpl)) {

                    // by default wrap sting in <p> element
                    if (tpl.indexOf('</') < 0) {
                        tpl = "<p>" + tpl + "</p>";
                    }

                    // compile the provided template against the current scope
                    el = $compile(tpl)(scope);

                    // way of emptying the element
                    element.html("");

                    // add the template content
                    element.append(el);
                }
            });
        }
    }

})();
