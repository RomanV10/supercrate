window.angular = require("angular");
require('angular-sanitize');
require('angular-animate');
require('angular-ui-bootstrap');
require("../../movecalculator/moveForm.controller.js");
require("../../scripts/angular-google-analytics.min.js");
require("../../scripts/angular-local-storage.min.js");
require("../../services/authentication.services.js");
require("../../services/branchingApi.services");
require("../../services/session.services.js");
require("../../services/form.services.js");
require("../../services/request.services.js");
require("../../widgets/widgets.module.js");
require("../../widgets/overlay.directive.js");
require("../../services/common");
require('icheck');
require('../../services/filter.js');
require("../../scripts/slide/scripts/slide.and.push.js");

require("../../movecalculator/directives/moveForm.directive.js");
require("../../movecalculator/directives/searchZip/searchZip.directive.js");
require("../../movecalculator/directives/searchZip/searchModal.controller.js");
require("../../movecalculator/directives/userMenupopup/userMenuPopup.directive.js");
require("../../movecalculator/directives/salesLog/salesLog.directive.js");

require("../../movecalculator/widgets/ngMask.js");
require("../../movecalculator/widgets/checklist-model.js");
require("../../movecalculator/widgets/tooltips.js");
require('../../scripts/ng-map.min.js');

require('../../../moveBoard/app/calculator/calc.module.js');
require('../../../moveBoard/app/calculator/sevices/calculate.services.js');
require("sweetalert");
require("../../../moveBoard/vendor/sweet-alert/SweetAlert.js");

require("../../movecalculator/angular-datepicker-master/build/angular-datepicker.js");
require("../../calculator/services/calculate.services.js");
require("../../reviews/reviews.directive.js");

import '../../widgets/icheck/css/square/_all.css';
import '../../scripts/slide/styles/slide.and.push.css';
import '../../movecalculator/angular-datepicker-master/build/themes/default.css';
import '../../movecalculator/angular-datepicker-master/build/themes/default.date.css';
import '../../movecalculator/angular-datepicker-master/build/themes/default.time.css';
import '../../../moveBoard/vendor/sweet-alert/sweetalert.css';
import '../../movecalculator/views/css/movecalculator.css';
import '../../movecalculator/views/css/UltraSmallForm.css';
import '../../movecalculator/views/css/quote-calculator.css';
import '../../movecalculator/views/css/datepicker.css';
import '../../movecalculator/views/css/tooltips.css';
import '../../movecalculator/directives/searchZip/searchZip.css';
import '../../reviews/template/reviews.css';
