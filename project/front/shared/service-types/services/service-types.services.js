'use strict';

angular
	.module('shared-service-types')
	.factory('serviceTypeService', serviceTypeService);

serviceTypeService.$inject = ['datacontext'];

function serviceTypeService(datacontext) {
	
	let fieldData = datacontext.getFieldData();
	let basicsettings = angular.fromJson(fieldData.basicsettings);
	let allowedServiceTypes = fieldData.field_lists.field_move_service_type;
	
	return {
		getAllServiceTypes: getAllServiceTypes,
		getCurrentServiceTypeName: getCurrentServiceTypeName,
		getCurrentForLegend: getCurrentForLegend,
		getAllServiceTypesAsSimpleObject: getAllServiceTypesAsSimpleObject,
	};
	
	function getAllServiceTypesAsSimpleObject () {
		let serviceNames = {};
		_.forEach(getAllServiceTypes(), item => {
			serviceNames[item.id] = item.title;
		});
		return serviceNames;
	}
	
	function getAllServiceTypes() {
		var services = [];
		
		if (basicsettings.services.localMoveOn) {
			services.push({
				id: 1,
				title: _.get(basicsettings, 'services.localMoveLabel', allowedServiceTypes[1])
			});
		}
		if (basicsettings.services.localMoveStorageOn) {
			services.push({
				id: 2,
				title: _.get(basicsettings, 'services.localMoveStorageLabel', allowedServiceTypes[2])
			});
		}
		if (basicsettings.services.loadingHelpOn) {
			services.push({
				id: 3,
				title: _.get(basicsettings, 'services.loadingHelpLabel', allowedServiceTypes[3])
			});
		}
		if (basicsettings.services.unloadingHelpOn) {
			services.push({
				id: 4,
				title: _.get(basicsettings, 'services.unloadingHelpLabel', allowedServiceTypes[4])
			});
		}
		if (basicsettings.isflat_rate_miles) {
			services.push({
				id: 5,
				title: _.get(basicsettings, 'services.flatRateLabel', allowedServiceTypes[5])
			});
		}
		if (basicsettings.services.overnightStorageOn) {
			services.push({
				id: 6,
				title: _.get(basicsettings, 'services.overnightStorageLabel', allowedServiceTypes[6])
			});
		}
		if (basicsettings.islong_distance_miles) {
			services.push({
				id: 7,
				title: _.get(basicsettings, 'services.longDistanceLabel', allowedServiceTypes[7])
			});
		}
		if (basicsettings.services.packingDayOn) {
			services.push({
				id: 8,
				title: _.get(basicsettings, 'services.packingDayLabel', allowedServiceTypes[8])
			});
		}
		
		return services;
	}
	
	function getCurrentForLegend(key, name) {
		let result;
		const DISABLED_NAME = 'LD Trip';
		const LOADING_UNLOADING = 'Loading and Unloading Help';
		if (name === DISABLED_NAME) return name;
		if (name === LOADING_UNLOADING) {
			return `${_.get(basicsettings, 'services.loadingHelpLabel')} and ${_.get(basicsettings, 'services.unloadingHelpLabel')}`;
		}
		
		switch (+key) {
			case 1:
				result = _.get(basicsettings, 'services.localMoveLabel', name);
				break;
			case 2:
				result = _.get(basicsettings, 'services.localMoveStorageLabel', name);
				break;
			case 4:
				result = _.get(basicsettings, 'services.overnightStorageLabel', name);
				break;
			case 5:
				result = _.get(basicsettings, 'services.longDistanceLabel', name);
				break;
			case 6:
				result = _.get(basicsettings, 'services.flatRateLabel', name);
				break;
			case 8:
				result = _.get(basicsettings, 'services.packingDayLabel', name);
				break;
		}
		return result;
	}
	
	function getCurrentServiceTypeName(requestType) {
		let service = +requestType.service_type.raw;
		let baseName = requestType.service_type.value;
		let serviceTypeName;
		switch (service) {
			case 1:
				serviceTypeName = _.get(basicsettings, 'services.localMoveLabel', baseName);
				break;
			case 2:
				if (!requestType.request_all_data.toStorage) {
					serviceTypeName = 'Move To Storage';
				} else {
					serviceTypeName = 'Move From Storage';
				}
				break;
			case 3:
				serviceTypeName = _.get(basicsettings, 'services.loadingHelpLabel', baseName);
				break;
			case 4:
				serviceTypeName = _.get(basicsettings, 'services.unloadingHelpLabel', baseName);
				break;
			case 5:
				serviceTypeName = _.get(basicsettings, 'services.flatRateLabel', baseName);
				break;
			case 6:
				serviceTypeName = _.get(basicsettings, 'services.overnightStorageLabel', baseName);
				break;
			case 7:
				serviceTypeName = _.get(basicsettings, 'services.longDistanceLabel', baseName);
				break;
			case 8:
				serviceTypeName = _.get(basicsettings, 'services.packingDayLabel', baseName);
				break;
		}
		
		return serviceTypeName;
	}
}
