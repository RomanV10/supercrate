'use strict';

import Centrifuge from 'centrifuge';

angular
	.module('shared-sockets')
	.factory('sharedSockets', sharedSockets);

/* @ngInject */
function sharedSockets(datacontext, $rootScope, $window) {
	const CENTRIFUGE_SETTING = _.get(datacontext.getFieldData(), 'centrifugo_settings');
	const NAMESPACE = _.get(CENTRIFUGE_SETTING, 'centrifugo_namespace');
	const ADMIN = 'ADMIN';
	let cnetrifuge;

	tryConnectToCentrifuge();

	return {
		subscribeToChannel,
		getSocketClientId,
		onConnect,
		disconnect
	};

	function tryConnectToCentrifuge() {
		let user = _getCurrentUserId();

		if (user) {
			_connectToCetrifuge(user);
		} else {
			$window.location.reload();
		}
	}

	function _getCurrentUserId() {
		let user = _.get($rootScope, 'currentUser.userId');
		let userId = _.get(user, 'uid');

		if (userId && user.roles.length > 1) {
			userId = ADMIN;
		}

		return userId;
	}

	function _connectToCetrifuge(user) {
		cnetrifuge = new Centrifuge({
			user,
			url: `wss://${CENTRIFUGE_SETTING.centrifugo_url}`,
			timestamp: moment().unix().toString(),
			token: CENTRIFUGE_SETTING.centrifugo_token,
		});

		cnetrifuge.connect();
	}

	function disconnect() {
		cnetrifuge.disconnect();
	}

	function subscribeToChannel(channelName) {
		return cnetrifuge.subscribe(`${NAMESPACE}:${channelName}`);
	}

	function getSocketClientId() {
		return cnetrifuge.getClientId();
	}

	function onConnect(event, cb) {
		cnetrifuge.on(event, cb);
	}

}
