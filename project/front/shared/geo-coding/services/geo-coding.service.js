'use strict';

angular
	.module('shared-geo-coding')
	.factory('geoCodingService', geoCodingService);

geoCodingService.$inject = ['apiService', 'sharedConstantsApi', '$q'];

function geoCodingService(apiService, sharedConstantsApi, $q) {
	const DEFAULT_GEO_CODING = {
		name: 'zip',
		page: 0,
		size: 20,
	};
	
	return {
		geoCode: geoCode,
	};
	
	function geoCode(value,
					 name = DEFAULT_GEO_CODING.name,
					 page = DEFAULT_GEO_CODING.page,
					 size = DEFAULT_GEO_CODING.size) {
		
		let defer = $q.defer();
		
		if (!value) {
			defer.reject();
			return;
		}
		
		apiService.postData(sharedConstantsApi.geoCode.get, {
			field_name: name,
			data: value,
			page: page,
			page_size: size
		})
			.then(resolve => {
				
				if (resolve.data && resolve.data.status_code
					|| isEmpty(resolve.data[0])) {
					defer.reject();
					return;
				}
				
				let response = resolve.data[0];
				defer.resolve(response);
			}, () => {
				defer.reject();
			});
		
		return defer.promise;
		
	}
	
	function isEmpty (obj) {
		return !obj || Object.keys(obj).length == 0;
	}
}
