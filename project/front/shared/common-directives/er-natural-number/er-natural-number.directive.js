'use strict';

angular
	.module('commonServices')
	.directive('erNaturalNumber', erNaturalNumber);

function erNaturalNumber() {
	return {
		restrict: 'A',
		require: "ngModel",
		link: function ($scope, $element, $attrs, ngModelCtrl) {
			ngModelCtrl.$parsers.unshift(toLowercase);
			
			function toLowercase(newValue) {
				let parsed = '';
				
				if (isNaN(newValue)) {
					for (let i in newValue) {
						if (!isNaN(newValue[i]))
							parsed += newValue[i];
					}
				} else if (+newValue < 0) {
					parsed = 0;
				} else {
					parsed = newValue;
				}
				parsed = +parsed;
				
				if (parsed.toString() != newValue) {
					ngModelCtrl.$setViewValue(parsed);
					ngModelCtrl.$render();
				}
				return parsed;
			}
		}
	};
}
