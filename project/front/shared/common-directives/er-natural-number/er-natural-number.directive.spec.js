describe('Unit: er-lowercase directive,', function () {
	let element, $scope;

	beforeEach(() => {
		$scope = $rootScope.$new();
		$scope.theModel = '';
		element = $compile("<input ng-model='theModel' er-natural-number/>")($scope);
	});
	
	it ("Should leave natural numbers", () => {
		element.val('12');
		element.triggerHandler('change');
		expect($scope.theModel).toEqual(12);
	});
	
	it ("Should return zero if negative value", () => {
		element.val('-1');
		element.triggerHandler('change');
		expect($scope.theModel).toEqual(0);
	});
	
	it ("Should remove non-digits", () => {
		element.val('-1ASs-w2dv');
		element.triggerHandler('change');
		expect($scope.theModel).toEqual(12);
	});
});
