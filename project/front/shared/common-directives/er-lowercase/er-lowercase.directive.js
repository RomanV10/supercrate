'use strict';

angular
	.module('commonServices')
	.directive('erLowercase', erLowercase);

function erLowercase() {
	return {
		restrict: 'A',
		require: "ngModel",
		link: function ($scope, $element, $attrs, ngModelCtrl) {
			ngModelCtrl.$parsers.unshift(toLowercase);
			
			function toLowercase(newValue) {
				let lower = newValue.toLowerCase();
				ngModelCtrl.$setViewValue(lower);
				ngModelCtrl.$render();
				return lower;
			}
		}
	};
}
