describe('Unit: er-lowercase directive,', function () {
	let element, $scope;

	beforeEach(() => {
		$scope = $rootScope.$new();
		$scope.theModel = '';
		element = $compile("<input ng-model='theModel' er-lowercase/>")($scope);
	});
	
	it ("Should leave lower letters", () => {
		element.val('abcd123');
		element.triggerHandler('change');
		expect($scope.theModel).toEqual('abcd123');
	});
	
	it ("Should replace big letters", () => {
		element.val('ABCD123');
		element.triggerHandler('change');
		expect($scope.theModel).toEqual('abcd123');
	});
});
