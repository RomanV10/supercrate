import './er-saving-progress.styl';

angular
	.module('commonServices')
	.directive('erSavingProgress', erSavingProgress);

/* @ngInject */
function erSavingProgress($timeout) {
	return {
		restrict: 'E',
		template: require('./er-saving-progress.html'),
		scope: {
			saving: '=',
		},
		link,
	};

	function link($scope) {
		$scope.$watch('saving', onChange);

		function onChange(newValue, oldValue) {
			if (newValue == false && oldValue) {
				showSaved();
			}
		}

		function showSaved() {
			$scope.saved = true;

			$timeout(() => {
				$scope.saved = false;
			}, 1000);
		}
	}
}