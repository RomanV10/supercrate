'use strict';

angular
	.module('move-board-compile')
	.directive('moveBoardCompile', moveBoardCompile);

moveBoardCompile.$inject = ['$compile'];

function moveBoardCompile($compile) {
	return {
		restrict: 'A',
		link: moveBoardCompileLink,
	};
	
	function moveBoardCompileLink ($scope, element, attr) {
		let oldScope;
		$scope.$watch(($scope) => $scope.$eval(attr.moveBoardCompile), (current) => {
			if (oldScope) oldScope.$destroy();
			oldScope = $scope.$new();
			
			element.html(current);
			$compile(element.contents())(oldScope);
			
		});
	}
}
