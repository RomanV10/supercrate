'use strict';

angular
	.module('grand-total')
	.factory('grandTotalService', grandTotalService);

/*@ngInject*/
function grandTotalService(PaymentServices, datacontext, CalculatorServices, editRequestCalculatorService,
	additionalServicesFactory) {
	const fieldData = datacontext.getFieldData();
	const PACKING_LABOR = _.get(fieldData, 'basicsettings.packing_settings.packingLabor');
	const scheduleSettings = angular.fromJson(fieldData.schedulesettings);

	let reservation = getReservations(scheduleSettings);

	const GRAND_TOTAL_TYPES = {
		min: 'min',
		max: 'max',
		longDistance: 'longdistance',
		flatRate: 'flatrate',
		invoice: 'invoice',
	};
	const DEFAULT_VALUE = 0;
	const ROUND_UP = 2;
	const DISCOUNT_COEFFICIENT = 100;

	return {
		getGrandTotal,
		getReservationRate
	};

	function getGrandTotal(type, request) {
		let grandTotal = 0;
		let packingServices = calculatePackingServices(request);
		let additionalServices = calculateAddServices(request);
		let moneyDiscount = _.get(request, 'request_all_data.add_money_discount', DEFAULT_VALUE);
		let percentDiscount = _.get(request, 'request_all_data.add_percent_discount', DEFAULT_VALUE);

		switch (type) {
		case GRAND_TOTAL_TYPES.min:
		case GRAND_TOTAL_TYPES.max:
			grandTotal += +request.quote[type] + packingServices + additionalServices;
			break;
		case GRAND_TOTAL_TYPES.longDistance:
			let ldQuote = CalculatorServices.getLongDistanceQuote(request);
			grandTotal += +ldQuote + packingServices + additionalServices;
			break;
		case GRAND_TOTAL_TYPES.flatRate:
			grandTotal += +request.flat_rate_quote.value + packingServices + additionalServices;
			break;
		case GRAND_TOTAL_TYPES.invoice:
			if (_.get(request, 'work_time_int')) {
				grandTotal = request.work_time_int * request.rate.value + packingServices + additionalServices;
			}
			break;
		}

		// VALUATION
		grandTotal += +_.get(request, 'request_all_data.valuation.selected.valuation_charge', DEFAULT_VALUE);
		// FUEL SURCHARGE
		grandTotal += +_.get(request, 'request_all_data.surcharge_fuel', DEFAULT_VALUE);
		// DISCOUNT
		let discount = moneyDiscount + grandTotal * percentDiscount / DISCOUNT_COEFFICIENT;

		return _.round(grandTotal - discount, ROUND_UP);
	}

	function calculatePackingServices(request) {
		let serviceType = request.service_type.raw === '5' || request.service_type.raw === '7';
		let result = PaymentServices.getPackingTotal(request.request_data.value, PACKING_LABOR, serviceType);
		return Number(result);
	}

	function calculateAddServices(request) {
		if (!_.get(request, 'extraServices')) return DEFAULT_VALUE;
		let result = additionalServicesFactory.getExtraServiceTotal(request.extraServices);
		return Number(result);
	}

	function getReservationRate(request) {
		let longDistanceQuote = CalculatorServices.getLongDistanceQuote(request);

		if (request.reservation_rate.old == '-1.00' || request.field_usecalculator.value) {
			if (request.service_type.raw == '5') {
				request.reservation_rate.value = reservation.flatReservation.type == 'percent' ? request.flat_rate_quote.value / 100 * reservation.flatReservation.value : reservation.flatReservation.value;
			} else if (request.service_type.raw == '7') {
				request.reservation_rate.value = reservation.longReservation.type == 'percent' ? longDistanceQuote / 100 * reservation.longReservation.value : reservation.longReservation.value;
				increaseReservationPrice(request);
			} else {
				request.reservation_rate.value = reservation.localReservation.type == 'hour' ? Number(reservation.localReservation.value) * Number(request.rate.value) : reservation.localReservation.value;
				increaseReservationPrice(request);
			}
		} else if (request.service_type.raw == '5') {
			request.reservation_rate.value = reservation.flatReservation.type == 'percent' ? request.flat_rate_quote.value / 100 * reservation.flatReservation.value : reservation.flatReservation.value;
		}
	}

	function getReservations(reservationsList) {
		let zeroReservation = 0;
		return {
			flatReservation: {
				value: reservationsList.flatReservationRate > 0 ? reservationsList.flatReservationRate : reservationsList.flatReservation || zeroReservation,
				type: reservationsList.flatReservationRate > 0 ? 'rate' : 'percent'
			},
			localReservation: {
				value: reservationsList.localReservationRate > 0 ? reservationsList.localReservationRate : reservationsList.localReservation || zeroReservation,
				type: reservationsList.localReservationRate > 0 ? 'rate' : 'hour'
			},
			longReservation: {
				value: reservationsList.longReservationRate > 0 ? reservationsList.longReservationRate : reservationsList.longReservation || zeroReservation,
				type: reservationsList.longReservationRate > 0 ? 'rate' : 'percent'

			}
		};
	}

	function increaseReservationPrice(request) {
		if (request.request_all_data.reservationIncreased) {
			editRequestCalculatorService.increaseReservationPrice(request, request.request_all_data.reservationIncreased);
		}
	}
}
