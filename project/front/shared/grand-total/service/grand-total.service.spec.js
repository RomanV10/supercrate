describe('grand-total.service.js', () => {
	const DEFAULT_VALUE = 0;
	const ROUND_UP = 2;
	const DISCOUNT_COEFFICIENT = 100;
	const TYPES = {
		min: 'min',
		max: 'max',
		flatRate: 'flatrate',
		longDistance: 'longdistance',
		invoice: 'invoice',
	};

	let grandTotalService,
		CalculatorServices,
		flatRateRequest,
		longDistanceRequest,
		localMoveRequest,
		invoiceRequest;

	beforeEach(inject((_grandTotalService_, _CalculatorServices_) => {
		grandTotalService = _grandTotalService_;
		CalculatorServices = _CalculatorServices_;
		flatRateRequest = testHelper.loadJsonFile('flat-rate-request-grand-total.service.mock');
		longDistanceRequest = testHelper.loadJsonFile('long-distance-request-grand-total.service.mock');
		localMoveRequest = testHelper.loadJsonFile('local-move-request-grand-total.service.mock');
		invoiceRequest = testHelper.loadJsonFile('invoice-request-grand-total.service.mock');
		let frontPage = testHelper.loadJsonFile('frontpage.mock');
		CalculatorServices.init(frontPage);
	}));

	describe('on init service:', () => {
		it('getGrandTotal should be defined', () => {
			expect(grandTotalService.getGrandTotal).toBeDefined();
		});
	});

	describe('on calculate grand total:', () => {
		describe('Local Move request: ', () => {
			it('MIN grand total should be equals 480', () => {
				// given
				let expectedResult = 480;
				// when
				let result = grandTotalService.getGrandTotal(TYPES.min, localMoveRequest);
				// then
				expect(result).toEqual(expectedResult);
			});
			// max
			it('MAX grand total should be equal 480', () => {
				// given
				let expectedResult = 480;
				// when
				let result = grandTotalService.getGrandTotal(TYPES.max, localMoveRequest);
				// then
				expect(result).toEqual(expectedResult);
			});
		});

		describe('Flat Rate request', () => {
			it('FLAT RATE grand total should be equals 10876', () => {
				// given
				let expectedResult = 10876;
				// when
				let result = grandTotalService.getGrandTotal(TYPES.flatRate, flatRateRequest);
				// then
				expect(result).toEqual(expectedResult);
			});
		});

		describe('Long Distance request', () => {
			it('Long Distance grand total should be equals 52.5', () => {
				// given
				let expectedResult = 52.5;
				// when
				let result = grandTotalService.getGrandTotal(TYPES.longDistance, longDistanceRequest);
				// then
				expect(result).toEqual(expectedResult);
			});
		});

		describe('Invoice request', () => {
			it('Invoice grand total should be equals 0', () => {
				// given
				let expectedResult = 0;
				// when
				let result = grandTotalService.getGrandTotal(TYPES.invoice, invoiceRequest);
				// then
				expect(result).toEqual(expectedResult);
			});
		});
	});
});
