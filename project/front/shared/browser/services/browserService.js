'use strict';

angular.module('browser')
	.factory('browserService', browserService);

	browserService.$inject = ['$window'];

	function browserService($window) {

		function isIEorEDGE() {
			return $window.navigator.appName == 'Netscape' &&
				(~$window.navigator.appVersion.indexOf('Trident') || ~$window.navigator.appVersion.indexOf('Edge'));
		}

		return {
			isIEorEDGE
		};
	}
