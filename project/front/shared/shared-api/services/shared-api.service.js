'use strict';

angular
	.module('shared-api')
	.factory('apiService', apiService);

apiService.$inject = ['$q', '$http', 'config'];

function apiService($q, $http, config) {
	const postQuine = [];

	const getUrlWithParams = (url, params) => {
		for (let key in params) {
			if (url.indexOf('%' + key + '%') !== -1) {
				url = url.replace('%' + key + '%', params[key]);
				delete params[key];
			}
		}
		return url;
	};

	return {
		getData: getData,
		customData: customData,
		postData: postData,
		putData: putData,
		delData: delData,
	};

	function getData (api, params, serverUrl = config.serverUrl) {
		const url = getUrlWithParams(api, params);

		return $http({
			url: serverUrl + url,
			method: 'GET',
			params: params
		});
	}

	function customData(api, params, method, serverUrl = config.serverUrl) {
		const url = getUrlWithParams(api, params);
		return $http({
			url: serverUrl + url,
			method: method,
			data: params,
		});
	}

	function postData(api, params, serverUrl = config.serverUrl) {
		const url = getUrlWithParams(api, params);

		postQuine.push({
			url: serverUrl + url,
			params: params
		});

		return $http({
			url: serverUrl + url,
			method: 'POST',
			data: params,
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	function putData(api, params, serverUrl = config.serverUrl) {
		const url = getUrlWithParams(api, params);

		return $http({
			url: serverUrl + url,
			method: 'PUT',
			data: params,
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	function delData(api, params, serverUrl = config.serverUrl) {
		const url = getUrlWithParams(api, params);
		return $http({
			url: serverUrl + url,
			method: 'DELETE',
			data: params,
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}
}
