'use strict';

angular
	.module('shared-api')
	.constant('sharedConstantsApi', {
		'geoCode': {
			'get': 'server/zip_codes/find'
		},
		'email': {
			'getTemplates': 'server/template_builder',
			'sendEmailsTo': 'server/template_builder/send_email_template_to/'
		}
	});
