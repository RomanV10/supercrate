let moveBoardAPI = {
	'core': {
		'getVariable': 'server/system/get_variable',
		'getFrontpage': 'server/front/frontpage',
		'frontPage': 'server/front/frontpage',
		'managers': 'server/move_request/managers',
		'login': 'server/move_users_resource/login',
		'autoLogin': 'server/clients/link_restore'
	},
	'receipt': {
		'getReceipt': 'server/move_request/get_receipt',
		'setReceipt': 'server/move_request/set_receipt',
		'updateReceipt': 'server/move_request/update_receipt',
		'deleteReceipt': 'server/move_request/delete_receipt',
		'setOnlineReceipt': '/server/payment',
		'refund': 'server/long_distance_payments/refund_receipt'
	},
	'ldReceipt': {
		'getReceipt': 'server/long_distance_payments/get_receipt',
		'setReceipt': 'server/long_distance_payments/set_receipt',
		'updateReceipt': 'server/long_distance_payments/update_receipt',
		'deleteReceipt': 'server/long_distance_payments/delete_receipt'
	},
	'invoice': {
		'create': 'server/move_invoice',
		'update': 'server/move_invoice/%id%',
		'getInvoices': 'server/move_invoice/get_invoice_for_entity',
		'getInvoicesForDashboard': 'server/move_invoice/get_move_request_invoices'
	},
	'statistics': {
		'all_stat': 'server/move_statistics/get_statistics_all',
		'today_stat': 'server/move_request_statistics/get_today_stat',
		'get_sales_payroll': 'server/move_statistics/get_sales_payroll',
		'count_unique_site_visitors': 'server/move_statistics/count_unique_site_visitors',
		'statistics_by_sales_all': 'server/move_statistics/statistics_by_sales_all',
		'statistics_for_six_months': 'server/move_statistics/statistics_for_six_months',
		'statistics_for_sales_by_day': 'server/move_statistics/statistics_for_sales_by_day',
		'get_stat_for_months': 'server/move_request_statistics/get_stat_for_months',
		'statistics_by_sales_interval': 'server/move_statistics/statistics_by_sales_interval',
		'statistics_by_sales_interval_month': 'server/move_statistics/statistics_by_sales_interval_month',
		'uniqueRequestStatisticMethod': 'server/move_request_statistics/',
		'uniqueStatisticsMethod': 'server/move_statistics/',
		'getFirstAssignDate': 'server/move_statistics/get_first_assigned_date'
	},
	'agentFolio': {
		'getAgentAndTrip': 'server/long_distance_carrier/get_carrier_and_trip'
	},
	'inventoryOld': {
		'setInventory': 'server/move_invertory/',
		'getInventory': 'server/move_invertory/'
	},
	'search': {
		'searchRequest': 'server/move_request/search',
		'globalSearch': 'server/move_global_search?value='
	},
	'trip': {
		'create': 'server/long_distance_trip',
		'query': 'server/long_distance_trip',
		'update': 'server/long_distance_trip/%trip_id%',
		'addRequest': 'server/long_distance_trip/add_job_ld_trip',
		'getAllTrips': 'server/long_distance_trip/show_all_trips',
		'getTrip': 'server/long_distance_trip/%id%',
		'removeTrip': 'server/long_distance_trip/%tripId%',
		'removeRequest': 'server/long_distance_trip/del_job_ld_trip',
		'getTripClosing': 'server/long_distance_trip/get_trip_closing/%id%',
		'update_closing_values': 'server/long_distance_trip/update_closing_values',
		'createServices': 'server/long_distance_service',
		'getServices': 'server/long_distance_trip/get_job_services/%job_id%',
		'removeJob': 'server/long_distance_trip/del_job_ld_trip',
		'removeJobs': 'server/long_distance_trip/del_job_ld_trip_multiply',
		'getTripLog': 'server/new_log',
		'changeJobStatus': 'server/long_distance_job/update_job_status/%job_id%'
	},
	'trip_carrier': {
		'list': 'server/long_distance_carrier',
		'query': 'server/long_distance_carrier',
		'create': 'server/long_distance_carrier',
		'ratePerCf': 'server/long_distance_carrier/get_price_per_cf/%carrier_id%',
		'update': 'server/long_distance_carrier/%carrier_id%',
		'getById': 'server/long_distance_carrier/%carrier_id%',
		'withParams': 'server/long_distance_carrier/',
		'getAgentFolio': 'server/long_distance_carrier',
		'getJobs': 'server/long_distance_carrier/%id%',
		'sendPay': 'server/long_distance_payments/make_payment',
		'sendInvoice': 'server/long_distance_payments/send_invoice',
		'buildInvoice': 'server/long_distance_payments/build_invoice',
		'resendInvoice': 'server/long_distance_payments/resend_invoice/%id%',
		'paidInvoice': 'server/long_distance_payments/close_invoice/%id%',
		'getTotal': 'server/long_distance_payments/collect_jobs_info'
	},
	'carrier_payments': {
		'receipts': 'server/long_distance_carrier/get_carrier_receipt/%id%',
		'receiptDetails': 'server/long_distance_payments/get_receipt/%id%',
		'invoices': 'server/long_distance_carrier/get_carrier_invoices/%id%',
		'invoiceDetails': 'server/long_distance_payments/get_invoice/%id%',
		'getRequestReceipts': 'server/long_distance_trip/get_job_receipt/%id%',
		'setReceiptPending': 'server/long_distance_payments/pending_receipt/%id%',
		'removeRequestReceipt': 'server/long_distance_payments/delete_receipt/%id%',
		'removeRequestInvoice': 'server/long_distance_payments/delete_invoice/%id%'
	},
	'request': {
		'pickup': 'server/long_distance_request/search',
		'getOpenedJobs': 'server/move_request/open_jobs',
		'clone': 'server/move_request/clone_request',
		'createPackingDay': 'server/move_request/create_packing_day',
		'base': 'server/move_request',
		'unbindPackingDay': 'server/move_request/untie_packing_day',
		'getSuperRequest': 'server/move_request/get_super_request/',
		'unique': 'server/move_request/',
		'addServices': 'server/move_request/set_payment',
		'createStorage': 'server/move_request/storage_request',
	},
	'foreman': {
		'list': 'server/move_users_resource/get_user_by_role'
	},
	'tpDelivery': {
		'get': 'server/long_distance_tp_delivery/%id%',
		'create': 'server/long_distance_tp_delivery',
		'update': 'server/long_distance_tp_delivery/%tpId%',
		'delete': 'server/long_distance_tp_delivery/%id%',
		'calcClosing': 'server/long_distance_trip/calc_closing'
	},
	'notifications': {
		'update_sales_permission': 'server/move_notification/update_user_notification_permissions_by_entity',
		'updateUserEmailSetting': 'server/move_notification/update_user_mail_send_setting',
		'switchShowNotification': 'server/move_notification/turn_off_on_notification_for_user',
		'updateEmailSettings': 'server/move_notification/update_notification_type_mail_settings',
		'allowedNotificationTypes': 'server/move_notification/user_allowed_notification_types',
		'addNotificationsToUser': 'server/move_notification/update_notification_perm_user_multiple',
		'setAllCheck': 'server/move_notification/update_notification_status_by_types',
		'updateStatus': 'server/move_notification/update_notification_status',
		'createNotification': 'server/move_notification',
		'getAllNotifications': 'server/move_notification/get_all_notifications_with_status',
		'getNewNotifications': 'server/move_notification/get_new_notifications',
		'addUser': 'server/move_notification/update_notification_perm_user',
		'removeUser': 'server/move_notification/delete_notification_perm_user',
		'addRole': 'server/move_notification/update_notification_perm_role ',
		'removeRole': 'server/move_notification/delete_notification_perm_role',
		'getNotifications': 'server/move_notification/get_all_notification_types_with_info',
		'addTemplate': 'server/move_notification/notification_type_template',
		'update_notification_score': 'server/notification_types_score/%ntid%',
		'update_request_score': 'server/notification_types_score/add_score_to_request'
	},
	'token': {
		'list': 'server/move_notification_templates/get_tokens_list'
	},
	'user': {
		'getUserByRole': 'server/move_users_resource/get_user_by_role',
		'getCurrent': 'server/clients/getcurrent'
	},
	'ldStorages': {
		'create': 'server/long_distance_storage/',
		'update': 'server/long_distance_storage/%id%',
		'get': 'server/long_distance_storage/%id%',
		'list': 'server/long_distance_storage/',
		'setActive': 'server/long_distance_storage/update_active_or_not'
	},
	'truck': {
		'list': 'server/move_parking',
		'chooseTruck': 'server/move_parking',
		'deleteTruck': 'server/move_parking/delete_truck_from_parking',
		'deleteByEntity': 'server/move_parking/delete_parking_by_entity'
	},
	'requestSIT': {
		'getList': 'server/long_distance_storage',
		'create': 'server/long_distance_sit/%id%'
	},
	'department': {
		'testEmailConnection': 'server/mail/test_mail_connection',
		'uploadPhoto': 'server/move_users_resource/set_user_picture',
		'checkLogin': 'server/clients/check_login'
	},
	'calcService': {
		'calcFuelSurcharge': 'server/move_distance_calculate/calculate_fuel_surcharge'
	},
	'settingService': {
		'setVariable': 'server/settings/set_variable',
		'getVariable': 'server/system/get_variable'
	},
	'validations': {
		'userUnique': 'server/clients/checkunique',
		'zipValidForLD': 'server/move_distance_calculate/check_valid_ld'
	},
	'requests': {
		'allRequests': 'server/move_request?pagesize=10',
		'getParklotOfRequestDate': 'server/move_request/get_parklot',
		'getRequestContract': 'server/move_request/get_contract/',
		'getParklotOfMonth': 'server/move_request/get_month_parklot',
		'getAllBranchParklotByDay': 'server/move_request/get_all_branch_parklot',
		'getFlags': 'server/move_request/get_flags',
		'getDispatchCalendarByMonth': '/server/move_branch_trucks/get_calendar_dispatch'
	},
	'branching': {
		'createBranch': 'server/move_branch',
		'updateBranch': 'server/move_branch',
		'getBranch': 'server/move_branch',
		'deleteBranch': 'server/move_branch',
		'getAllBranch': 'server/move_branch',
		'getClosestBranch': 'server/move_branch/get_closest_branch',
		'synchronize': 'server/move_branch/synchronize',
		'updateParkingZip': 'server/move_branch/update_parking_zip',
		'updateBasedState': 'server/move_branch/update_based_state',
		'migrateToBranch': 'server/move_branch/make_migrate',
		'copyManagerToBranch': 'server/move_branch/clone_manager_to_branch',
		'setBranchLogo': '/server/move_branch/save_logo',
		'saveBranchSettings': 'server/move_branch_trucks/save_trucks_setting'
	},
	'branchingTruck': {
		'CRUD': '/server/move_branch_trucks',
		'setTruckUnvilability': '/server/move_branch_trucks/truck_unavailable_branches',
		'getMonthBranchesTruck': '/server/move_branch_trucks/get_month_parklot_branch',
		'getDispatchCalendarByMonth': '/server/move_branch_trucks/get_calendar_dispatch_branch'
	},
	'templateBuilder': {
		'getTemplatePreview': 'server/template_builder/get_template_preview'
	},
	'ldPayroll': {
		'create': 'server/long_distance_trip/update_payroll/%trip_id%',
		'update': 'server/long_distance_trip/update_payroll/%trip_id%',
		'get': 'server/long_distance_trip/get_payroll/%trip_id%',
		'submit': 'server/long_distance_trip/submit_payroll/%trip_id%',
		'revoke': 'server/long_distance_trip/revoke_payroll/%trip_id%'
	},
	'smtpSettings': {
		'testSmtp': 'server/mail/test_mail_connection',
		'getSmtpSettings': 'server/settings/get_transport_smtp_settings'
	},
	'storageInvoices': {
		'getStorageInvoices': 'server/request_storage/get_storage_invoices'
	},
	'reviewMoveboard': {
		'getAllReviews': 'server/move_reviews',
		'getReviewStatistics': 'server/move_reviews/statistics_reviews',
		'createReview': 'server/move_reviews',
		'sortReview': 'server/move_reviews/sort_reviews',
		'getReview': 'server/move_reviews/retrieve'
	},
	'homeEstimate': {
		'getRequests': 'server/move_request/search',
		'getCountSheduleRequests': 'server/move_request/get_count_estimate_schedule',
		'getHomeEstimators': 'server/move_request/get_user_for_estimate'
	},
	'inventory': {
		'request': {
			'updateItemInRequest': 'server/move_inventory_item/update_item_in_request',
			'updateCustomItemInRequest': 'server/move_inventory_item/update_custom_item_in_request',
			'addCustomItem': 'server/move_inventory_item/add_custom_item_to_request',
			'removeCustomItem': 'server/move_inventory_item/delete_custom_items',
			'addCustomRoom': 'server/move_inventory_item/add_custom_room_to_request',
			'createCustomRoom': 'server/move_inventory/create_custom_room',
			'deleteCustomRoom': 'server/move_inventory/delete_custom_room',
			'addPackageToRequest': 'server/move_inventory_package/add_package_to_request',
			'removePackageFromRequest': 'server/move_inventory_package/remove_package_from_request',
			'search': 'server/move_inventory_item/search_item',
			'getCustomItems': 'server/move_inventory_item/get_custom_items',
			'removeAddionalItems': 'server/move_inventory_item/change_inventory_type',
			'removeUnnecessaryItems': 'server/move_inventory/remove_unnecessary_items',
			'getItemsByType': 'server/move_inventory_item/get_inventory_request',
			'checkAdditionalInventory': 'server/move_inventory/check_exist_add_inventory',
			'checkAllowChangeMoveSize': 'server/move_inventory/check_if_change_move_size',
			'revertToPreviousInventory': 'server/move_inventory/cancel_inventory_changes',
			'transferInventory': 'server/move_inventory/transfer_inventory'
		},
		'room': {
			'index': 'server/move_inventory_room',
			'rooms': 'server/move_inventory_room/get_rooms',
			'create': 'server/move_inventory_room',
			'update': 'server/move_inventory_room/%id%',
			'get': 'server/move_inventory_room/%id%',
			'remove': 'server/move_inventory_room/%id%',
			'drag': 'server/move_inventory_room/sorting'
		},
		'filter': {
			'query': 'server/move_inventory_filter/get_filters',
			'create': 'server/move_inventory_filter',
			'update': 'server/move_inventory_filter/%id%',
			'get': 'server/move_inventory_filter/%id%',
			'remove': 'server/move_inventory_filter/%id%',
			'drag': 'server/move_inventory_filter/sorting'
		},
		'item': {
			'getItems': 'server/move_inventory_item/get_items',
			'create': 'server/move_inventory_item',
			'update': 'server/move_inventory_item/%id%',
			'get': 'server/move_inventory_item/%id%',
			'remove': 'server/move_inventory_item/%id%',
			'drag': 'server/move_inventory_item/sorting',
			'getMyInventory': 'server/move_inventory_item/get_inventory_request',
			'moveItem': 'server/move_inventory/move_item'
		},
		'package': {
			'query': 'server/move_inventory_package',
			'create': 'server/move_inventory_package',
			'get': 'server/move_inventory_package/%id%',
			'addItemToPackage': 'server/move_inventory_item/add_item_to_package',
			'remove': 'server/move_inventory_package/remove_item_from_package',
			'update': 'server/move_inventory_package/%id%'
		},
		'settings': {
			'search': 'server/move_inventory_item/search_item_settings'
		}
	},
	'requestNotes': {
		'getNotes': 'server/move_request/get_notes/',
		'setNotes': 'server/move_request/set_notes/',
		'getParklotNotes': 'server/move_request/get_parklot_note',
		'setParklotNotes': 'server/move_request/set_parklot_note',
	},
	'quickbooks': {
		'moveQuickbooks': 'server/move_quickbooks/',
		'postCompanyId': 'server/move_quickbooks/post_company_id',
		'getCompanyId': 'server/move_quickbooks/get_company_id',
		'qboReconnect': 'server/move_quickbooks/qbo_reconnect',
		'qboDisconnect': 'server/move_quickbooks/qbo_disconnect',
		'checkAuthDateReconnectButton': 'server/move_quickbooks/check_auth_date_reconnect_button',
		'checkTokenDisconnectButton': 'server/move_quickbooks/check_token_disconnect_button',
		'setQuickbooksSettings': 'server/move_quickbooks/set_quickbooks_settings',
		'exportAllQbo': 'server/move_quickbooks/export_all_qbo'

	},
	'googleCalendarIntegration': {
		'getGlobalStatus': 'server/google_calendars/get_status',
		'setGlobalStatus': 'server/google_calendars/update_status',
		'getSharedCalendarByUser': 'server/google_calendars/get_shared_calendars',
		'saveUserData': 'server/clients/'
	},
	'draftRequest': {
		'createDraftRequest': 'server/move_request/create_draft_move_request'
	},
	'client': {
		'changeClient': 'server/move_request/change_request_client',
		'changeClientAll': 'server/clients/update_client_all_requests',
		'setAdditionalUser': 'server/move_request/set_additional_user',
		'updateClient': 'server/clients/',
		'createNewUser': 'server/clients/create_user_update_request',
		'updateUserSettings': 'server/clients/update_user_settings',
	},
	'commercialMove': {
		'updateSetting': 'server/move_request/set_commercial_move_settings',
		'createExtraCommercialMove': 'server/move_request/create_commercial_extra_room',
		'getCommercialExtra': 'server/move_request/get_commercial_extra_room',
		'updateCommercialExtra': 'server/move_request/update_commercial_extra_room',
		'deleteCommercialExtra': 'server/move_request/delete_commercial_extra_room'
	},
	'reminders': {
		'crud': 'server/move_reminders',
		'allForNode': 'server/move_reminders/all_for_node',
		'getForUser': 'server/move_reminders/get_for_user',
		'snooze': 'server/move_reminders/snooze',
		'dismiss': 'server/move_reminders/dismiss'
	},
	'arrivy': {
		'addForeman': 'server/move_arrivy/create_foreman',
		'getLinkForDispatch': 'server/move_arrivy/get_arrivy_dashboard_link',
		'createForeman': 'server/move_arrivy/create_foreman',
		'disconnectForeman': 'server/move_arrivy/disconnect_foreman',
		'check': 'server/move_arrivy/check_arrivy_connection', //POST
		'saveKeys': 'server/move_arrivy/save_authorization_keys', //POST
		'getKeys': 'server/move_arrivy/get_authorization_keys', //POST
		'connect': 'server/move_arrivy/connect', //POST
		'disconnect': 'server/move_arrivy/disconnect', //POST
		'sendTaskStatus': 'server/move_arrivy/post_task_status' //POST
	},
	'faq': {
		'create': 'server/move_faq', //POST
		'upvoteUsefulness': 'server/move_faq/upvote_faq_usefulness',
		'downvoteUsefulness': 'server/move_faq/downvote_faq_usefulness',
		'delete': 'server/move_faq/%id%', //DELETE
		'get': 'server/move_faq', //GET
		'getByUsefulness': 'server/move_faq/faqs_sorted_by_usefulness', //POST
		'retrieve': 'server/move_faq/%id%', //GET
		'update': 'server/move_faq/%id%' //PUT
	},
	'inHomeEstimatePortal': {
		'getInHomeEstimateRequests': 'server/move_request/get_home_estimator_request'
	},
	'priceCalendar': {
		'update': 'server/settings/update_price_calendar',
		'get': 'server/settings/price_calendar_types',
	},
	'profitAndLoss': {
		'getAllInfo': 'server/profit_losses/show_all_profit_loss_info',
		'addNewExpense': 'server/profit_losses',
	},
	'valuation': {
		'saveValuationPlan': 'server/settings/set_valuation_plan',
		'getValuationPlan': 'server/settings/get_valuation_plan',
	},
	'paymentCollected': {
		'getCollectedPaymentByFilter': 'server/move_request/filter_receipts'
	},
	'comments': {
		'setRead': 'server/move_request_comments/set_read_comment/',
		'moveRequestComments': 'server/move_request_comments',
		'unReadComments': 'server/move_request_comments/unread_count',
		'getNewComments': 'server/move_request_comments/get_new_comments',
		'adminAllComments': 'server/move_request_comments/admin_all_comments',
	},
	'dispatchLogs': {
		'createLog': 'server/move_team_logs', //POST
		'getByDate': 'server/move_team_logs/get_by_date', //POST
		'getByRequest': 'server/move_team_logs/get_by_nid', //POST
		'createMultipleLogs': '/server/move_team_logs/add_multi', //POST
	},
	'dispatchCrews': {
		'all': 'server/move_team',
		'addAll': 'server/move_team/add_all_teams',
		'create': 'server/move_team/add_one_team',
		'isAvailable': 'server/move_team/is_team_is_available',
		'workLoad': 'server/move_team/get_users_work_time',
	},
	'dispatch': {
		'sendEmailTemplateTo': 'server/template_builder/send_email_template_to/',
		'assignTeam': 'server/move_team/assign_team_to_request',
	},
	'storages': {
		'getStorage': 'server/move_storage/%id%',
		'updateStorage': 'server/move_storage/%id%',
		'deleteStorage': 'server/move_storage/%id%',
		'getDefaultStorage': 'server/move_storage/get_default_storage',
		'getList': 'server/move_storage?flag=1',
		'getNameList': 'server/move_storage?flag=0',
		'addStorage': 'server/move_storage',
		'getStorageRequests': 'server/request_storage/get_request_status?flag=%flag%',
		'getStorageRequest': 'server/request_storage/%id%',
		'removeStorageRequest': 'server/request_storage/%id%',
		'createStorageRequest': 'server/request_storage',
		'updateStorageRequest': 'server/request_storage/%id%',
		'getStorageAging': 'server/request_storage/get_debtor',
		'getRecurringRevenue': 'server/request_storage/request_storage_revenue',
		'getStorageReceipts': 'server/request_storage/get_receipt_storage',
	},
	'payroll': {
		'get': 'server/payroll/get_payroll',
	},
	'smsSettings': {
		'phone': {
			'find': 'server/move_sms/phone_find',
			'buy': 'server/move_sms/phone_buy',
			'delete': 'server/move_sms/phone_delete',
			'index': 'server/move_sms/phone_numbers_all'
		},
		'companyUsage': 'server/move_sms/company_usage',
		'companyPayments': 'server/move_sms/get_payments',
		'payForSMS': 'server/move_sms/payment',
		'autoRecurring': 'server/move_sms/payment_create_customer_charge',
		'autoRecurringWithoutCreate': 'server/move_sms/payment_charge_customer',
		'getSMSByDate': 'server/move_sms/sms_all',
		'sendSMS': 'server/move_sms/sms_send',
		'smsTemplateBuilder': {
			'index': 'server/move_sms/sms_get_templates',
			'edit': 'server/move_sms/sms_update_template',
		},
	}
};

angular.module('shared-api')
	.constant('moveBoardApi', moveBoardAPI);
