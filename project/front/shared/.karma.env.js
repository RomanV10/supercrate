module.exports = function (environment) {
	environment
		.use(['jasmine'])
		// add vendor dependency
		.add([
			'../moveBoard/vendor/vendor/jquery-2.1.1.min.js',
			'../moveBoard/dist/app.import.js',
			'./../../../node_modules/angular-mocks/angular-mocks.js',
			'../moveBoard/vendor/plugins/editors/textAngular-rangy.min.js',
			'../moveBoard/vendor/plugins/editors/textAngular-sanitize.js',
			'../moveBoard/vendor/plugins/editors/textAngular.js',
			'../moveBoard/vendor/plugins/editors/textAngularSetup.js',
			'../moveBoard/vendor/plugins/editors/textAngular-dropdownToggle.js',
			
			'../moveBoard/vendor/angular-dragula/angular-dragula.js',
			'../moveBoard/vendor/plugins/switchery/switchery.min.js',
			'../moveBoard/vendor/angular-masonry/angular-masonry.js',
			'../moveBoard/vendor/plugins/dataTables/js/jquery.dataTables.js',
			'../moveBoard/vendor/plugins/dataTables/js/dataTables.bootstrap.js',
			'../moveBoard/vendor/lodash/lodash.js',
			'../moveBoard/vendor/plugins/momentjs/moment.js',
			'../moveBoard/vendor/momentjs/momentjs-business.js',
			'../moveBoard/vendor/toastr/toastr.js',
		]
		)
		
		// add source dependency
		.add([
			'../../../inline.js',
			'dist/app.test.js',
			'../moveBoard/app/core/shared/directives/templatedirectives.js',
			
			// required project files for tests
			'../moveBoard/app/core/constants.js',
		]);
};
