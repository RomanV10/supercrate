'use strict';

angular
	.module('commonServices')
	.factory('timeFormat', timeFormat);

/*@ngInject*/
function timeFormat() {
	return {
		parseFloatHours,
	};

	function parseFloatHours(floatHours) {
		let hrs = parseInt(Number(floatHours));
		let min = Math.round((Number(floatHours) - hrs) * 60);
		let result = `${hrs}h`;
		if (min) result += ` ${min}m`;
		return result;
	}
}
