angular
	.module('commonServices')
	.factory('printService', printService);

function printService() {
	let service = {
		printThisLayout
	};
	return service;

	function printThisLayout(layout, styles) {
		if (!styles) styles = '';
		let popupWin = window.open('', '_blank', 'width=800,height=600');
		let onload = 'window.opener = null;' +
			'window.document.close(); /*necessary for IE >= 10*/' +
			'window.focus(); /*necessary for IE >= 10*/' +
			'window.print();' +
			'window.close();';
		popupWin.document.write(
			'<html>' +
			'<head>' +
			'<style>' + styles + '</style>' +
			'</head>' +
			'<body>' + layout + '</body>' +
			'<script>' + onload + '</script>' +
			'</html>');
	}
}
