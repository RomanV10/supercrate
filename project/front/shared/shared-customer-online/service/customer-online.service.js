'use strict';

angular
	.module('shared-customer-online')
	.factory('customerOnlineService', customerOnlineService);

/*@ngInject*/
function customerOnlineService(sharedRequestServices, datacontext, grandTotalService) {
	const SERVICE_TYPE_ENUM = _.get(datacontext.getFieldData(), 'enums.request_service_types', {});
	const DEFAULT_VALUE = 0;
	const ROUND_UP = 2;
	const TYPES = {
		min: 'min',
		max: 'max',
		flatRate: 'flatrate',
		longDistance: 'longdistance',
		invoice: 'invoice',
	};

	return {
		prepareUserCardInfo,
	};

	function prepareUserCardInfo(request, user) {
		let firstName = _.get(user, 'field_user_first_name', '');
		let lastName = _.get(user, 'field_user_last_name', '');

		let minTime = _.get(request, 'minimum_time.value', DEFAULT_VALUE);
		let maxTime = _.get(request, 'maximum_time.value', DEFAULT_VALUE);

		return {
			name: `${firstName} ${lastName}`,
			nid: request.nid,
			phone: user.field_primary_phone,
			mail: user.mail,
			status: request.status.value,
			date: request.date.value,
			addressFrom: request.from,
			addressTo: request.to,
			estTime: `${minTime} - ${maxTime}`,
			grandTotal: getGrandTotal(request),
		};
	}

	function getGrandTotal(request) {
		let serviceType = +_.get(request, 'service_type.raw');
		let grandTotal = 0;

		switch (serviceType) {
		case SERVICE_TYPE_ENUM.FLAT_RATE:
			grandTotal = `$${_.round(grandTotalService.getGrandTotal(TYPES.flatRate, request), ROUND_UP)}`;
			break;
		case SERVICE_TYPE_ENUM.LONG_DISTANCE:
			grandTotal = `$${_.round(grandTotalService.getGrandTotal(TYPES.longDistance, request), ROUND_UP)}`;
			break;
		default:
			let min = _.round(grandTotalService.getGrandTotal(TYPES.min, request), ROUND_UP);
			let max = _.round(grandTotalService.getGrandTotal(TYPES.max, request), ROUND_UP);
			grandTotal = `$${min} - $${max}`;
		}
		return grandTotal;
	}
}
