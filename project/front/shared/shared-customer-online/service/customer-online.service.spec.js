describe('customer-online.service.js:', () => {
	let customerOnlineService,
		apiService,
		request,
		user;

	beforeEach(inject((_customerOnlineService_, _apiService_) => {
		customerOnlineService = _customerOnlineService_;
		apiService = _apiService_;
		request = testHelper.loadJsonFile('request-customer-online.service.mock');
		user = testHelper.loadJsonFile('user-customer-online.service.mock');
	}));

	describe('on init service', () => {
		it('customerOnlineService should be defined', () => {
			expect(customerOnlineService).toBeDefined();
		});

		it('customerOnlineService.prepareUserCardInfo should be defined', () => {
			expect(customerOnlineService.prepareUserCardInfo).toBeDefined();
		});
	});

	describe('on call', () => {
		let spyApiService;
		beforeEach(() => {
			spyApiService = spyOn(apiService, 'postData').and.callThrough();
		});

		it('customerOnlineService.prepareUserCardInfo should returns expectedResult', () => {
			// given
			let expectedResult = {
				name: 'Draft User',
				nid: '4486',
				phone: '1111111111',
				mail: '96user_1526368029@draft.www',
				status: ' Pending',
				date: '05/25/2018',
				addressFrom: 'Boston, MA, 02222',
				addressTo: 'Boston, MA, 02222',
				estTime: '00:45 - 01:30',
				grandTotal: '$374 - $474'
			};
			// when
			let result = customerOnlineService.prepareUserCardInfo(request, user);
			// then
			expect(result).toEqual(expectedResult);
		});
	});
});
