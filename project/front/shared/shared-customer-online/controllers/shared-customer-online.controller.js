'use strict';

angular
	.module('shared-customer-online')
	.controller('SharedCustomerOnline', SharedCustomerOnline);

SharedCustomerOnline.$inject = ['sharedSockets', '$q', '$scope'];

function SharedCustomerOnline(sharedSockets, $q, $scope) {
	let vm = this;
	const SOCKET_CHANNEL = 'onlineCustomer';
	const CENTRIFUGE = sharedSockets.subscribeToChannel(SOCKET_CHANNEL);
	const MUSTER_CHANNEL = sharedSockets.subscribeToChannel('muster');
	let connecting = $q.defer();
	sharedSockets.onConnect('connect', message => {
		client = message.client;
		connecting.resolve();
	});
	if (CENTRIFUGE._isReady() && MUSTER_CHANNEL._isReady()) {
		connecting.resolve();
	}
	let client;
	let manager;

	$scope.addOnline = addOnline;
	$scope.onEvent = onEvent;
	$scope.reconnectCustomer = reconnectCustomer;
	$scope.onMuster = onMuster;
	vm.getOnline = getOnline;

	// ACCOUNT
	function addOnline(uid, nid, request) {
		connecting.promise.then(() => {
			manager = _.get(request, 'current_manager.uid', -1);
			CENTRIFUGE.publish({
				client,
				data: {
					manager,
					uid,
					nid,
					request
				}
			});
		});
	}

	function reconnectCustomer(uid, nid, request) {
		CENTRIFUGE.publish({
			client,
			manager,
			data: {
				uid,
				nid,
				request
			}
		});
	}


	// MOVEBOARD
	function getOnline() {
		connecting.promise.then(() => {
			MUSTER_CHANNEL.publish({
				muster: 'muster'
			});
		});
	}

	function onEvent(event, callback) {
		CENTRIFUGE.on(event, callback);
	}

	function onMuster(callback) {
		MUSTER_CHANNEL.on('message', callback);
	}
}
