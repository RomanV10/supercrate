describe('laborService', () => {
	let laborService,
		testHelper,
		crews;
	let datacontext;
	let fieldDataFake = {
		calcsettings: {
			min_hours: 10,
		}
	};

	beforeEach(inject((_datacontext_) => {
		datacontext = _datacontext_;
		fieldDataFake.calcsettings.min_hours = 5;
		spyOn(datacontext, 'getFieldData').and.returnValue(fieldDataFake);
	}));

	beforeEach(inject(function (_laborService_, _testHelperService_) {
		laborService = _laborService_;
		testHelper = _testHelperService_;
		crews = testHelper.loadJsonFile('crews.mock');
	}));

	it('labor service should be injected and defined', () => {
		expect(laborService).toBeDefined();
	});

	describe('getAllCrewDuration', () => {
		it('should return value for all crews', () => {
			//given
			let expectedResult = 13;
			let result;

			//when
			result = laborService.getAllCrewDuration(crews);

			//then
			expect(result).toEqual(expectedResult);
		});
	});
});
