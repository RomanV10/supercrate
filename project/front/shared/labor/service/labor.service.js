'use strict';

angular.module('labor')
	.factory('laborService', laborService);

/* @ngInject */
function laborService(datacontext) {

	let fieldData = datacontext.getFieldData(),
		calcSettings = angular.fromJson(fieldData.calcsettings);

	const MIN_HOUR_RATE = parseFloat(calcSettings.min_hours);

	let service = {
		getAllCrewDuration,
		getCrewRate
	};

	return service;

	function getAllCrewDuration(crews = [], travelTime) {
		let total = travelTime || 0;

		crews.forEach((crew) => {
			total += crew.workDuration;
		});

		if (total < MIN_HOUR_RATE) {
			total = MIN_HOUR_RATE;
		}

		return total;
	}

	function getCrewRate(index, requestCrews, additionalCrews) {
		let rate = 0;

		if (!index || _.get(additionalCrews, `[${index - 1}].rateSaveManually`)) {
			return _.get(requestCrews, `[${index}].rate`, 0);
		} else {
			let tempAdditionalCrews = additionalCrews.slice(0, index);
			let indexCrewSaveRate = _.findLastIndex(tempAdditionalCrews, (tempAdditionalCrew) => tempAdditionalCrew.rateSaveManually);
			indexCrewSaveRate = ~indexCrewSaveRate ? indexCrewSaveRate + 1 : 0;

			for (let i = indexCrewSaveRate; i <= index; i++) {
				rate += +requestCrews[i].rate;
			}
		}

		return rate;
	}
}
