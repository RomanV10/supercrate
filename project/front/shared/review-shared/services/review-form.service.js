'use strict';

angular
	.module('review-shared')
	.factory('reviewSharedService', reviewSharedService);

/*@ngInject*/
function reviewSharedService(datacontext) {
	const ENTITY_TYPE = _.get(datacontext.getFieldData(), 'enums.entities');
	const BASIC_SETTINGS = _.get(datacontext.getFieldData(), 'basicsettings');
	const FEED_BACK_INITIAL = {
		date: '',
		job: '',
		teamLeader: '',
		customerName: '',
		customerEmail: '',
		comments: '',
		rating: 1,
		subbmited: true
	};

	return {
		ENTITY_TYPE,
		BASIC_SETTINGS: angular.fromJson(BASIC_SETTINGS),
		FEED_BACK_INITIAL,
	};
}
