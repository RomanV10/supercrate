'use strict';

angular.module('email')
	.factory('emailService', emailService);

/*@ngInject*/
function emailService(apiService, sharedConstantsApi) {
	return {
		submitLetterPdf
	};

	function submitLetterPdf(nid, emails, pdfUrl) {
		return apiService.getData(sharedConstantsApi.email.getTemplates)
			.then(({data: emailTemplates}) => {
				let template = [];
				let pdfRegex = /\[custom:contract-pdf-url\]/gi;

				for (let name in emailTemplates) {
					if (!template.length && emailTemplates[name].key_name == 'send_contract_pdf') {
						template.push(emailTemplates[name]);
					}
				}

				if (template.length) {
					template[0].template = template[0].template.replace(pdfRegex, pdfUrl);
					template[0].templateId = template[0].id;
					template[0].orderId = +template[0].id;
					template[0].subject = 'Contract';

					let data = {
						'to': emails,
						'templates': template
					};

					return apiService.postData(`${sharedConstantsApi.email.sendEmailsTo}${nid}`, {data: data});
				} else {
					return Promise.reject(`Template wasn't find`);
				}
			});
	}
}
