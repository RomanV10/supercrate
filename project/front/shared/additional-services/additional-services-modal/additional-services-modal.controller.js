'use strict';

angular
	.module('additionalServices')
	.controller('additionalServicesModal', additionalServicesModal);

/*@ngInject*/
function additionalServicesModal($scope, $uibModalInstance, request, invoice, type, grandTotal, grandTotalInvoice,
	$rootScope, datacontext, RequestServices, SettingServices, additionalServicesFactory) {

	const CHARGE_NAMES = [
		'Extra Drop off',
		'Extra Pick up',
		'Long Carry Fee',
		'Extra Charge For Steps (Origin)',
		'Extra Charge For Steps (Destination)',
		'Floors Extra Fee (Destination)',
		'Elevator Extra Fee (Destination)',
		'Floors Extra Fee (Origin)',
		'Elevator Extra Fee (Origin)'
	];

	let original_extra_charges = [];
	let importedFlags = $rootScope.availableFlags;
	let Job_Completed = 'Completed';

	$scope.addExtraCharges = addExtraCharges;
	$scope.removeCharge = removeCharge;
	$scope.calculateRowTotal = additionalServicesFactory.calculateRowTotal;
	$scope.save = save;
	$scope.request = request;
	$scope.invoice = invoice;
	$scope.type = type;
	$scope.grandTotal = grandTotal;
	$scope.grandTotalInvoice = grandTotalInvoice;
	$scope.total_services = 0;
	$scope.add_extra_charges = [];
	$scope.fieldData = datacontext.getFieldData();
	$scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);
	$scope.addCharge = [];
	$scope.removedCharge = [];
	$scope.Job_Completed = $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[Job_Completed]);

	$scope.extra_charges = [];
	let custom_extra =
		{
			name: 'Custom Extra',
			edit: true,
			extra_services: [
				{
					services_default_value: 1,
					services_name: 'Cost',
					services_read_only: false,
					services_type: 'Amount'
				},
				{
					services_default_value: 1,
					services_name: 'Value',
					services_read_only: false,
					services_type: 'Number'
				},
				{
					services_default_value: 1,
					services_name: 'Weight',
					services_read_only: false,
					services_type: 'Weight'
				}
			]
		};

	copyServicesIntoScope();
	additionalServicesFactory.getAvailableAdditionalServices()
		.then(function (data) {
			$scope.extra_charges = JSON.parse(data);
			$scope.extra_charges.unshift(custom_extra);
		});
	$scope.$watch('add_extra_charges', () => {
		$scope.total_services = additionalServicesFactory.getExtraServiceTotal($scope.add_extra_charges);
	}, true);


	function addExtraCharges(extra_charge) {
		let msg = {
			text: extra_charge.name + ' was added.'
		};
		$scope.addCharge.push(msg);
		let exist_extra_charge = _.find($scope.add_extra_charges, {name: extra_charge.name});
		let serviceHasDefaultValue = _.get(exist_extra_charge, 'extra_services[1].services_default_value');
		let serviceIsReadOnly = _.get(exist_extra_charge, 'extra_services[1].services_read_only');
		if (!serviceHasDefaultValue && !serviceIsReadOnly) {
			let charge = angular.copy(extra_charge);
			$scope.add_extra_charges.push(charge);
		} else if (serviceIsReadOnly) {
			exist_extra_charge.extra_services[1].services_default_value =
				+exist_extra_charge.extra_services[1].services_default_value + 1;
		}
	}

	function removeCharge(index) {
		let msg = {
			text: 'Service ' + $scope.add_extra_charges[index].name + ' was removed.'
		};
		$scope.removedCharge.push(msg);
		$scope.add_extra_charges.splice(index, 1);
	}

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	function buildLogs() {
		let logs = {
			title: 'Additional services',
			text: [],
			date: moment().unix()
		};

		_.forEach($scope.addCharge, function (data) {
			logs.text.push(data);
		});

		_.forEach($scope.removedCharge, function (data) {
			logs.text.push(data);
		});

		return logs;
	}

	function save() {
		$scope.busy = true;
		let details = buildLogs();
		findChangedServices();

		if ($scope.type == 'invoice') {
			$scope.invoice.extraServices = angular.copy($scope.add_extra_charges);
			$scope.request.request_all_data.invoice = $scope.invoice;
			let message = {
				simpleText: 'Additional Services was changed on closing tab. Total Services: $' + $scope.total_services
			};
			details.text.push(message);
			$uibModalInstance.dismiss('cancel');
			$rootScope.$broadcast('grandTotal.closing', {
				grand: $scope.grandTotal,
				logs: details.text
			}, $scope.request.nid);
		} else if ($scope.type == 'services') {

			$scope.invoice.services = $scope.add_extra_charges;
			$uibModalInstance.dismiss('cancel');

		} else {
			let data = {
				nid: $scope.request.nid,
				charges: $scope.add_extra_charges
			};
			$scope.request.extraServices = angular.copy($scope.add_extra_charges);
			let message = {
				simpleText: 'Additional Services was changed on sales tab. Total Services: $' + $scope.total_services
			};
			details.text.push(message);

			additionalServicesFactory.saveExtraServices($scope.request.nid, $scope.add_extra_charges).then(function () {
				$rootScope.$broadcast('grandTotal.sales', {
					grand: $scope.grandTotal,
					logs: details.text
				}, $scope.request.nid);

				if ($scope.request.status.raw == 3 && angular.isDefined($scope.invoice.extraServices)) {
					$scope.invoice.extraServices = angular.copy($scope.add_extra_charges);
					$scope.request.request_all_data.invoice = $scope.invoice;

					RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data)
						.then(() => {
							$rootScope.$broadcast('request.packing_update', '');
						});
				} else {
					RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data)
						.then(() => {
							$rootScope.$broadcast('request.packing_update', '');
						});
				}

				$rootScope.$broadcast('request.payment_update', data);
				$uibModalInstance.dismiss('cancel');
				$scope.busy = false;
			});
		}
	}

	function findChangedServices() {
		if (!_.isEmpty(original_extra_charges)) {
			let extracharges = findChargesByName(original_extra_charges);

			if (!_.isEmpty(extracharges)) {
				let diffCharges = findDiffCharges(extracharges, $scope.add_extra_charges);

				if (!_.isEmpty(diffCharges)) {
					let all_data = $scope.request.request_all_data;
					if (_.isArray(all_data)) {
						all_data = {};
					}

					if (_.isUndefined(all_data.isChangedExtraservices)) {
						all_data.isChangedExtraservices = {};
					}

					for (let i in diffCharges) {
						let property = additionalServicesFactory.getExtraServicePropertyByName(diffCharges[i].name);

						if (!_.isEmpty(property)) {
							all_data.isChangedExtraservices[property] = true;
						}

					}

					$scope.request.request_all_data = all_data;
				}
			}
		}
	}

	function findChargesByName(charges) {
		let result = [];

		_.forEach(CHARGE_NAMES, function (name) {
			_.forEach(charges, function (charge) {
				if (_.isEqual(charge.name, name)) {
					result.push(charge);
				}
			});
		});

		return result;
	}

	function findDiffCharges(filteredCharges, finalCharges) {
		let result = [];
		_.forEach(filteredCharges, function (filteredCharge) {
			let found = false;
			_.forEach(finalCharges, function (finalCharge) {
				if (_.isEqual(filteredCharge.name, finalCharge.name)) {
					found = true;
					if (!_.isEqual(filteredCharge, finalCharge)) {
						result.push(filteredCharge);
					}
				}
			});
			if (_.isEqual(found, false)) {
				result.push(filteredCharge);
			}
		});

		return result;
	}
	function copyServicesIntoScope() {
		if ($scope.type == 'invoice') {
			if (angular.isDefined(invoice.extraServices)) {
				$scope.add_extra_charges = angular.copy(angular.fromJson(invoice.extraServices));
				original_extra_charges = angular.copy($scope.add_extra_charges);
			}
		} else if ($scope.type == 'services') {
			$scope.add_extra_charges = angular.copy(invoice.services);
		} else {
			if (angular.isDefined(request.extraServices)) {
				if (_.isString(request.extraServices)) {
					$scope.add_extra_charges = angular.copy(angular.fromJson(request.extraServices));
				} else {
					$scope.add_extra_charges = angular.copy(request.extraServices);
				}
				original_extra_charges = angular.copy($scope.add_extra_charges);
			}
		}
	}
}
