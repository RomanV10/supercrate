angular
	.module('additionalServices')
	.factory('additionalServicesFactory', additionalServicesFactory);

/*@ngInject*/
function additionalServicesFactory(datacontext, $uibModal, moveBoardApi, apiService, SettingServices) {
	const PROPERTY_NAMES = {
		'Extra Drop off': 'extraDropOff',
		'Extra Pick up': 'extraPickUp',
		'Long Carry Fee': 'longCarryFee',
		'Extra Charge For Steps (Origin)': 'extraChargeForStepsOrigin',
		'Extra Charge For Steps (Destination)': 'extraChargeForStepsDestination',
		'Floors Extra Fee (Destination)': 'floorsExtraToFee',
		'Elevator Extra Fee (Destination)': 'elevatorExtraToFee',
		'Floors Extra Fee (Origin)': 'floorsExtraFromFee',
		'Elevator Extra Fee (Origin)': 'elevatorExtraFromFee',
	};

	let basicsettings;
	let longdistance;

	let service = {
		getEquipmentFeeService,
		recalculateEquipmentFee,
		getEquipmentFeeForCalcForm,
		openAddServicesModal,
		getExtraServicePropertyByName,
		getLongDistanceExtra,
		getLongDistanceExtraDelivery,
		getLongDistanceExtraPickup,
		saveExtraServices,
		initBasicService,
		getExtraServiceTotal,
		calculateRowTotal,
		getAvailableAdditionalServices
	};
	return service;

	function loadSettings() {
		basicsettings = angular.fromJson(datacontext.getFieldData().basicsettings);
		longdistance = angular.fromJson(datacontext.getFieldData().longdistance);
	}

	function getAvailableAdditionalServices() {
		return SettingServices.getSettings('additional_settings');
	}

	function openAddServicesModal(request, invoice, type, grandTotal, grandTotalInvoice) {
		let modalInstance = $uibModal.open({
			template: require('./additional-services-modal/additional-services-modal.template.html'),
			controller: 'additionalServicesModal',
			size: 'lg',
			backdrop: false,
			resolve: {
				request: function () {
					return request;
				},
				invoice: function () {
					return invoice;
				},
				type: function () {
					return type;
				},
				grandTotal: function () {
					return grandTotal;
				},
				grandTotalInvoice: function () {
					return grandTotalInvoice;
				}
			}
		});

		modalInstance.result.then();
	}

	function getEquipmentFeeForCalcForm(distance) {
		loadSettings();

		return {
			name: basicsettings.equipment_fee.name,
			amount: getEquipmentFeeCost(distance)
		};
	}

	function getEquipmentFeeCost(distance) {
		loadSettings();

		let result = 0;

		_.forEach(basicsettings.equipment_fee.by_mileage, function (value) {
			if (+value.from <= +distance && +distance <= +value.to) {
				result = value.amount;
			}
		});

		return result;
	}

	function recalculateEquipmentFee(request, invoice, callback) {
		loadSettings();

		let newEquipmentName = basicsettings.equipment_fee.name;
		let wasChanged = false;
		let newEquipmentCost = getEquipmentFeeCost(request.distance.value);
		let equipmentService = _.find(request.extraServices, {name: newEquipmentName});
		let oldAmount = _.get(equipmentService, 'extra_services[0].services_default_value', 0);
		if (oldAmount != newEquipmentCost) {
			if (equipmentService) {
				if (newEquipmentCost) {
					equipmentService.extra_services[0].services_default_value = newEquipmentCost;
				} else {
					let uselessServiceIndex = _.findIndex(request.extraServices, {name: newEquipmentName});
					request.extraServices.splice(uselessServiceIndex, 1);
				}
			} else if (newEquipmentCost) {
				request.extraServices.push(
					getEquipmentFeeService(newEquipmentCost, newEquipmentName)
				);
			}

			if (invoice) {
				invoice.extraServices = angular.copy(request.extraServices);
			}
			wasChanged = true;
		}
		callback(wasChanged);
	}

	function getEquipmentFeeService(amount, name) {
		return {
			autoService: true,
			extra_services: [
				{
					services_default_value: amount,
					services_name: 'Cost',
					services_read_only: false,
					services_type: 'Amount',
				}
			],
			fee_all_services: true,
			name: name,
		};
	}

	function getExtraServicePropertyByName(name) {
		return PROPERTY_NAMES[name] || '';
	}

	function calculateElevatorCharge(floor, weight) {
		loadSettings();

		if (floor != 6) return 0;
		let rate = +longdistance.floorextrafifth;
		let flatRate = +longdistance.floorextrafifth_flatRate;
		if (rate) {
			return rate * Math.round(weight / 100);
		} else if (flatRate) {
			return flatRate;
		}
	}

	function calculateFloorsCharge(floorsNumber, weight) {
		loadSettings();

		let isElevator = floorsNumber == 6;
		let isGroundFloor = floorsNumber == 7;
		if (floorsNumber <= +longdistance.above_floorextra - 1 || isElevator || isGroundFloor) return 0;
		let diff = floorsNumber - +longdistance.above_floorextra + 1;
		if (+longdistance.floorextra) {
			return diff * +longdistance.floorextra * Math.round(weight / 100);
		} else if (+longdistance.floorextra_flatrate) {
			return diff * +longdistance.floorextra_flatrate;
		}
	}

	function calculateStepsCharge(stepsNumber) {
		loadSettings();

		if (stepsNumber <= 2 || +longdistance.stepsInflight <= 0) return 0;
		let flightsNumber = Math.ceil(stepsNumber / +longdistance.stepsInflight);
		let paidFlights = flightsNumber - +longdistance.freeFlights;
		return +longdistance.chargePerflight * paidFlights;
	}

	function calculateLongCarryFee(distance, weight) {
		loadSettings();

		if (+longdistance.extrafeet <= 0) return 0;
		let fee = 0;
		if (distance && +distance > +longdistance.extrafeet) {
			let count = Math.round(distance / +longdistance.extrafeet);
			if (+longdistance.extrafeetrate) {
				fee = count * (+longdistance.extrafeetrate * Math.round(weight / 100));
			} else if (+longdistance.extrafeet_flatfee) {
				fee = count * +longdistance.extrafeet_flatfee;
			}
		}
		return fee;
	}

	function getLongDistanceExtraDelivery(distance) {
		loadSettings();

		let extraCharge = 0;
		if (+distance > +longdistance.extradeliverypmiles1) {
			extraCharge = longdistance.extradelivery;
		}
		if (+distance > +longdistance.extradeliverypmiles2) {
			extraCharge = longdistance.extradelivery2;
		}
		if (+distance > +longdistance.extradeliverypmiles3) {
			extraCharge = -1;
		}
		return +extraCharge;
	}

	function getLongDistanceExtraPickup(distance) {
		loadSettings();

		let extraCharge = 0;
		if (+distance > +longdistance.extrapickupmiles1) {
			extraCharge = longdistance.extrapickup;
		}
		if (+distance > +longdistance.extrapickupmiles2) {
			extraCharge = longdistance.extrapickup2;
		}
		if (+distance > +longdistance.extrapickupmiles3) {
			extraCharge = -1;
		}
		return +extraCharge;
	}

	function saveExtraServices(nid, value) {
		return apiService.postData(moveBoardApi.request.addServices, {
			nid: nid,
			data: value
		});
	}

	function initBasicService(name, value, autoService) {
		if (angular.isUndefined(autoService)) {
			autoService = true;
		}
		let basicService = {
			name: name,
			autoService: autoService,
			extra_services: [
				{
					services_default_value: value,
					services_name: 'Cost',
					services_read_only: false,
					services_type: 'Amount'
				}
			]
		};

		return basicService;
	}

	function getLongDistanceExtra(request, weight, skipCheckForChanged = false) {
		loadSettings();

		let extra_services = [];
		let isLongDistance = request.service_type.raw == 7;
		if (!isLongDistance) return extra_services;
		let isChangedExtraservices = skipCheckForChanged
			? {}
			: _.get(request, 'request_all_data.isChangedExtraservices', {});

		if (request.extraDropoff && !isChangedExtraservices.extraDropOff) {
			let fee = longdistance.extradelivery;
			let extraDropoff = initBasicService('Extra Drop off', fee);
			extra_services.push(extraDropoff);
		}

		if (request.extraPickup && !isChangedExtraservices.extraPickUp) {
			let fee = longdistance.extrapickup;
			let extraDropoff = initBasicService('Extra Pick up', fee);
			extra_services.push(extraDropoff);
		}

		if (longdistance.isCalculateFloorMovingTo) {
			if (!isChangedExtraservices.floorsExtraToFee) {
				let fee = calculateFloorsCharge(+request.type_to.raw, weight);
				if (fee > 0) {
					let extraCharge = initBasicService('Floors Extra Fee (Destination)', fee);
					extra_services.push(extraCharge);
				}
			}
			if (!isChangedExtraservices.elevatorExtraToFee) {
				let fee = calculateElevatorCharge(+request.type_to.raw, weight);
				if (fee > 0) {
					let extraCharge = initBasicService('Elevator Extra Fee (Destination)', fee);
					extra_services.push(extraCharge);
				}
			}
		}

		if (longdistance.isCalculateFloorMovingFrom) {
			if (!isChangedExtraservices.floorsExtraFromFee) {
				let fee = calculateFloorsCharge(+request.type_from.raw, weight);
				if (fee > 0) {
					let extraCharge = initBasicService('Floors Extra Fee (Origin)', fee);
					extra_services.push(extraCharge);
				}
			}
			if (!isChangedExtraservices.elevatorExtraFromFee) {
				let fee = calculateElevatorCharge(+request.type_from.raw, weight);
				if (fee > 0) {
					let extraCharge = initBasicService('Elevator Extra Fee (Origin)', fee);
					extra_services.push(extraCharge);
				}
			}
		}

		if (_.get(request, 'inventory.move_details')) {
			let stepsOrigin = +request.inventory.move_details.steps_origin;
			let stepsDestination = +request.inventory.move_details.steps_destination;
			if (!isChangedExtraservices.extraChargeForStepsOrigin) {
				let fee = calculateStepsCharge(stepsOrigin);
				if (fee > 0) {
					let stepsCharge = initBasicService('Extra Charge For Steps (Origin)', fee);
					extra_services.push(stepsCharge);
				}
			}
			if (!isChangedExtraservices.extraChargeForStepsDestination) {
				let fee = calculateStepsCharge(stepsDestination);
				if (fee > 0) {
					let stepsCharge = initBasicService('Extra Charge For Steps (Destination)', fee);
					extra_services.push(stepsCharge);
				}
			}
			if (!isChangedExtraservices.longCarryFee) {
				let longCarryFee = 0;
				let pickupFeet = request.inventory.move_details.current_door;
				let deliveryFeet = request.inventory.move_details.new_door;
				longCarryFee += calculateLongCarryFee(pickupFeet, weight);
				longCarryFee += calculateLongCarryFee(deliveryFeet, weight);
				if (longCarryFee > 0) {
					let carryCharge = initBasicService('Long Carry Fee', longCarryFee);
					extra_services.push(carryCharge);
				}
			}
		}

		return extra_services;
	}

	function getExtraServiceTotal(extraServices) {
		let add_extra_charges = [];
		if (angular.isDefined(extraServices)) {
			add_extra_charges = angular.fromJson(extraServices);
		}
		let total_services = 0;
		angular.forEach(add_extra_charges, function (charge) {
			total_services += calculateRowTotal(charge);
		});
		return _.round(total_services, 2);
	}

	function calculateRowTotal(service) {
		let result = 1;
		angular.forEach(service.extra_services, charge => {
			result *= +charge.services_default_value;
		});
		if (_.isNaN(result)) return 0;
		return _.round(result, 2);
	}
}
