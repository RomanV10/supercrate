describe('unit: additional-service service,', () => {
	let service, datacontext;
	beforeEach(inject((_datacontext_) => {
		datacontext = _datacontext_;
		let basicsettings = testHelper.loadJsonFile('basicsettings_for_additional_services.mock');
		let longdistance = testHelper.loadJsonFile('additional-services-longdistance.mock');
		spyOn(datacontext, 'getFieldData').and.callFake(() => {
			return {
				basicsettings,
				longdistance
			};
		});
	}));

	beforeEach(inject((_additionalServicesFactory_) => {
		service = _additionalServicesFactory_;
	}));

	describe('function getEquipmentFeeForCalcForm()', () => {
		it('Should return settings from first row', () => {
			expect(service.getEquipmentFeeForCalcForm(30)).toEqual({
				name: 'Equipment Fee',
				amount: 123
			});
		});

		it('Should return settings from second row', () => {
			expect(service.getEquipmentFeeForCalcForm(80)).toEqual({
				name: 'Equipment Fee',
				amount: 321
			});
		});
	});

	describe('function recalculateEquipmentFee()', () => {
		let request, invoice, callback;
		beforeEach(() => {
			callback = () => {};
			request = {
				distance: {
					value: 30
				},
				extraServices: []
			};
			invoice = {};
		});

		describe('When returns not-zero amount,', () => {
			beforeEach(() => {
				spyOn(service, 'getEquipmentFeeForCalcForm').and.callFake(() => {
					return {
						name: 'Equipment Fee',
						amount: 333
					};
				});
			});

			describe('When service already exist', () => {
				beforeEach(() => {
					request.extraServices.push({
						name: 'Equipment Fee',
						extra_services:[{services_default_value: 50}]
					});
					service.recalculateEquipmentFee(request, invoice, callback);
				});
				it('Should rewrite old amount', () => {
					expect(request.extraServices[0].extra_services[0].services_default_value).toEqual(123);
				});
				it('Should copy extraServices to invoice', () => {
					expect(invoice.extraServices).toEqual(request.extraServices);
				});
			});
			describe('When service not exist', () => {
				beforeEach(() => {
					service.recalculateEquipmentFee(request, invoice, callback);
				});
				it('Should add new service', () => {
					let etalonService = service.getEquipmentFeeService(123, 'Equipment Fee');
					expect(request.extraServices[0]).toEqual(etalonService);
				});
				it('Should copy extraServices to invoice', () => {
					expect(invoice.extraServices).toEqual(request.extraServices);
				});
			});
		});
		describe('When returns zero amount,', () => {
			beforeEach(() => {
				spyOn(service, 'getEquipmentFeeForCalcForm').and.callFake(() => {
					return {
						name: 'Equipment Fee',
						amount: 0
					};
				});
			});
			describe('When service already exist', () => {
				beforeEach(() => {
					request.extraServices.push({
						name: 'Equipment Fee',
						extra_services:[{services_default_value: 50}]
					});
					service.recalculateEquipmentFee(request, invoice, callback);
				});
				it('Should copy extraServices to invoice', () => {
					expect(invoice.extraServices).toEqual(request.extraServices);
				});
			});
			describe('When service not exist', () => {
				beforeEach(() => {
					service.recalculateEquipmentFee(request, invoice, callback);
					request.extraServices = [];
				});
				it('Should NOT add new service', () => {
					expect(request.extraServices.length).toEqual(0);
				});
				it('Should NOT copy extraServices to invoice', () => {
					expect(invoice.extraServices).not.toEqual(request.extraServices);
				});
			});
		});
	});

	describe('function getEquipmentFeeService()', () => {
		let newEquipmentService;
		beforeEach(() => {
			newEquipmentService = service.getEquipmentFeeService(333, 'customName');
		});
		it('new service should have proper name', () => {
			expect(newEquipmentService.name).toEqual('customName');
		});
		it('new service should have proper amount', () => {
			expect(newEquipmentService.extra_services[0].services_default_value).toEqual(333);
		});
	});

	describe('function getLongDistanceExtra()', () => {
		describe('when all services turned on', () => {
			let request, weight, calculatedExtras;
			beforeEach(() => {
				request = testHelper.loadJsonFile('additional-services-requestForLdExtras.mock');
				weight = 550;
				calculatedExtras = service.getLongDistanceExtra(request, weight);
			});
			it('should calculate Extra Drop off', () => {
				let extraDropOff = _.find(calculatedExtras, {name: 'Extra Drop off'});
				expect(extraDropOff.extra_services[0].services_default_value).toEqual(200);
			});
			it('should calculate Extra Pick up', () => {
				let extraDropOff = _.find(calculatedExtras, {name: 'Extra Pick up'});
				expect(extraDropOff.extra_services[0].services_default_value).toEqual('150');
			});
			it('should calculate Floors Extra Fee (Destination)', () => {
				let extraDropOff = _.find(calculatedExtras, {name: 'Floors Extra Fee (Destination)'});
				expect(extraDropOff.extra_services[0].services_default_value).toEqual(450);
			});
			it('should calculate Extra Charge For Steps (Origin)', () => {
				let extraDropOff = _.find(calculatedExtras, {name: 'Extra Charge For Steps (Origin)'});
				expect(extraDropOff.extra_services[0].services_default_value).toEqual(63);
			});
			it('should calculate Extra Charge For Steps (Destination)', () => {
				let extraDropOff = _.find(calculatedExtras, {name: 'Extra Charge For Steps (Destination)'});
				expect(extraDropOff.extra_services[0].services_default_value).toEqual(35);
			});
			it('should calculate Long Carry Fee', () => {
				let extraDropOff = _.find(calculatedExtras, {name: 'Long Carry Fee'});
				expect(extraDropOff.extra_services[0].services_default_value).toEqual(600);
			});
			it('should calculate Elevator Extra Fee (Origin)', () => {
				let extraDropOff = _.find(calculatedExtras, {name: 'Elevator Extra Fee (Origin)'});
				expect(extraDropOff.extra_services[0].services_default_value).toEqual(90);
			});

		});
	});

	describe('function calculateRowTotal()', () => {
		let additionalService;
		it('Should calculate row if all numbers exist', () => {
			additionalService = {
				extra_services: [
					{services_default_value: 3},
					{services_default_value: 2},
					{services_default_value: 5}
				]
			};
			expect(service.calculateRowTotal(additionalService)).toEqual(30);
		});
		it('Should calculate row if two numbers exist', () => {
			additionalService = {
				extra_services: [
					{services_default_value: 3},
					{services_default_value: 2}
				]
			};
			expect(service.calculateRowTotal(additionalService)).toEqual(6);
		});
		it('Should calculate row if one numbers exist', () => {
			additionalService = {
				extra_services: [
					{services_default_value: 3}
				]
			};
			expect(service.calculateRowTotal(additionalService)).toEqual(3);
		});
		it('Should calculate row if one numbers undefined', () => {
			let additionalService = {
				extra_services: [
					{services_default_value: 3},
					{services_default_value: null},
					{services_default_value: 2}
				]
			};
			expect(service.calculateRowTotal(additionalService)).toEqual(0);
		});
	});

	describe('function calculateRowTotal()', () => {
		let additionalServices;
		it('Should calculate row if all numbers exist', () => {
			additionalServices = [
				{
					extra_services: [
						{services_default_value: 3},
						{services_default_value: 2},
						{services_default_value: 5}
					]
				},
				{
					extra_services: [
						{services_default_value: 1},
						{services_default_value: 1},
						{services_default_value: 1}
					]
				},
				{
					extra_services: [
						{services_default_value: 2},
						{services_default_value: 0}
					]
				},
			];
			expect(service.getExtraServiceTotal(additionalServices)).toEqual(31);
		});
	});

	describe('function getLongDistanceExtraDelivery()', () => {
		it('Should return 0 if too small distance', () => {
			expect(service.getLongDistanceExtraDelivery(2)).toEqual(0);
		});
		it('Should return cost for first row', () => {
			expect(service.getLongDistanceExtraDelivery(7)).toEqual(200);
		});
		it('Should return cost for second row', () => {
			expect(service.getLongDistanceExtraDelivery(17)).toEqual(400);
		});
		it('Should return -1 if too big distance', () => {
			expect(service.getLongDistanceExtraDelivery(25)).toEqual(-1);
		});
	});
	describe('function getLongDistanceExtraPickup()', () => {
		it('Should return 0 if too small distance', () => {
			expect(service.getLongDistanceExtraPickup(10)).toEqual(0);
		});
		it('Should return cost for first row', () => {
			expect(service.getLongDistanceExtraPickup(17)).toEqual(150);
		});
		it('Should return cost for second row', () => {
			expect(service.getLongDistanceExtraPickup(27)).toEqual(300);
		});
		it('Should return -1 if too big distance', () => {
			expect(service.getLongDistanceExtraPickup(38)).toEqual(-1);
		});
	});
});
