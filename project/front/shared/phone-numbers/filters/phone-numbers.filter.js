'use strict';

import {
	formatNumber,
} from 'libphonenumber-js';

const COUNTRY = 'US';
const NATIONAL = 'National';

angular
	.module('phone-numbers')
	.filter('nationalPhoneFormatter', nationalPhoneFormatter);

nationalPhoneFormatter.$inject = [];

function nationalPhoneFormatter() {
	return phone => {
		if (!phone) return;

		if (phone[0] != '+') phone = '+' + phone;

		phone = phone.replace(/\+1|\D/g, '');

		return formatNumber({
			country: COUNTRY,
			phone
		}, NATIONAL);
	};
}
