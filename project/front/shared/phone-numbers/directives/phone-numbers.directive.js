'use strict';

import {
	formatNumber,
} from 'libphonenumber-js';

angular
	.module('phone-numbers')
	.directive('phoneInput', phoneInput);

phoneInput.$inject = [];

function phoneInput() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: phoneInputLink
	};

	function phoneInputLink($scope, element, attr, ngModel) {
		const COUNTRY = 'US';
		const NATIONAL = 'National';
		const E164 = 'E.164';

		ngModel.$parsers.unshift(phoneParser);
		ngModel.$formatters.unshift(phoneFormatter);

		function phoneParser(phone) {
			if (!phone) return phone;

			phone = phone.replace(/\+1|\D/g, '').substring(0,10);
			if (phone.length !== 10) {
				ngModel.$setValidity('phoneParser', false);
			} else {
				ngModel.$setValidity('phoneParser', true);
			}
			ngModel.$setViewValue(getPhone(phone, NATIONAL));
			ngModel.$render();

			return getPhone(phone, E164);
		}

		function phoneFormatter(phone) {
			if (!phone) return phone;

			phone = phone.replace(/\+1|\D/g, '');
			return getPhone(phone, NATIONAL);
		}

		function getPhone(phone, TYPE) {
			return formatNumber({
				country: COUNTRY,
				phone
			}, TYPE);
		}

	}
}
