import './er-signature-box.styl';

let uniqid = require('uniqid');

angular.module('er-signatures')
	.directive('erSignatureBox', erSignatureBox);

/* @ngInject */
function erSignatureBox(SweetAlert, $uibModal) {
	return {
		restrict: 'E',
		scope: {
			signature: '=',
			onSign: '&?',
			onRemove: '&?',
		},
		template: require('./er-signature-box.html'),
		link,
	};

	function link($scope, $element, $attr) {
		let type = 'signature' + uniqid();

		$scope.canRemove = +$attr.canRemove || 0;
		$scope.canEdit = +$attr.canEdit || 0;
		$scope.showSignatureDate = +$attr.showSignatureDate || 0;
		let isNotEmptySignatureFooter = !isEmptyHTMLString($scope.signature.footer);
		$scope.showSignatureFooter = (+$attr.showSignatureFooter || 0) && isNotEmptySignatureFooter;
		$scope.dateFormat = (+$attr.salesDateFormat || 0) ? 'mediumDate' : 'medium';

		function isEmptyHTMLString(text) {
			text = text || '';

			return text.replace(/\s|&nbsp;/g, '')
					.replace(/<(?:.|\n)*?>/gm, '').length == 0;
		}

		$scope.clickOpenSignature = function () {
			$uibModal.open({
				template: '<er-signature-modal></er-signature-modal>',
				controller: 'ErSignatureModalController',
				resolve: {
					signatureType: () => type,
				},
				windowClass: 'er-signature-modal',
				backdrop: 'static',
			})
				.result
				.then((signature) => {
					$scope.signature.value = signature.value;
					$scope.signature.date = signature.date;

					if ($scope.onSign) {
						$scope.onSign();
					}
				});
		};

		$scope.removeSignature = function () {
			SweetAlert.swal({
					title: 'Are you sure?',
					text: 'After cancel signature will be removed',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes',
					cancelButtonText: 'No',
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						$scope.signature.value = '';
						$scope.signature.date = '';

						if ($scope.onRemove) {
							$scope.onRemove();
						}
					}
				}
			);
		};
	}

}
