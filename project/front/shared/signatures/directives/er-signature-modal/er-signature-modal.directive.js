import './er-signature-modal.styl';

angular.module('er-signatures')
	.directive('erSignatureModal', erSignatureModal);

/* @ngInject */
function erSignatureModal($timeout, $window) {
	return {
		restrict: 'E',
		template: require('./er-signatures-modal.html'),
		link,
	};

	function link($scope, $element) {
		$scope.count = 1;

		$timeout(() => {
			let $canvas = angular.element(`#${$scope.signatureType}Canvas`);
			let canvas = new SignaturePad($canvas[0]);
			let $canvasParent = $canvas.closest('.modal-content');

			$scope.canvas = canvas;
			$scope.canvasElement = $canvas[0];

			$window.addEventListener('resize', resizeCanvas, false);

			function resizeCanvas() {
				if ($scope.count && $canvas) {
					$canvas[0].width = $canvasParent.width();
					$canvas[0].height = $canvasParent.height() - 100;
					$scope.count--;
				}
			}

			resizeCanvas();
		});
	}
}