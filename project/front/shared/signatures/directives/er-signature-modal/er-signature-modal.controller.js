angular.module('er-signatures')
	.controller('ErSignatureModalController', ErSignatureModalController);

/* @ngInject */
function ErSignatureModalController($scope, signatureType, $uibModalInstance, SweetAlert) {
	$scope.cancel = cancel;
	$scope.close = close;
	$scope.save = save;
	$scope.signatureType = signatureType;

	function cancel() {
		$scope.canvas.clear();
	}

	function close() {
		cancel();
		$uibModalInstance.dismiss();
	}

	function save() {
		let id = `${signatureType}Canvas`;
		let isBlank = isCanvasBlank(id);

		if (isBlank) {
			SweetAlert.swal('Failed!', 'Sign please', 'error');
		} else {
			let signatureValue = getSignatureUrl($scope.canvas._ctx);
			let signature = {
				owner: 'customer',
				value: 'data:image/gif;base64,' + signatureValue,
				date: moment().format('x')
			};

			$uibModalInstance.close(signature);
		}


	}

	function isCanvasBlank(elementID) {
		let canvas = document.getElementById(elementID);
		let blank = document.createElement('canvas');
		blank.width = canvas.width;
		blank.height = canvas.height;
		let context = canvas.getContext('2d');
		let perc = 0;
		let area = canvas.width * canvas.height;

		if (canvas.toDataURL() == blank.toDataURL()) {
			return canvas.toDataURL() == blank.toDataURL();
		} else {
			let data = context.getImageData(0, 0, canvas.width, canvas.height).data;
			let ct = 0;

			for (let i = 3, len = data.length; i < len; i += 4) {
				if (data[i] > 50) {
					ct++;
				}
			}

			perc = parseFloat((100 * ct / area).toFixed(2));

			return perc < 0.1;
		}
	}

	function getSignatureUrl(ctxData, allow) {
		let url;
		let originImgData;
		let xi, yi;
		let imageData, nCanvas;
		let ctx = ctxData;
		let canvasWidth = $scope.canvasElement.width;
		let canvasHeight = $scope.canvasElement.height;
		let minX = canvasWidth;
		let minY = canvasHeight;
		let maxX = 0;
		let maxY = 0;
		let padding = 10;

		if(canvasWidth != 0 && canvasHeight != 0 || allow) {
			originImgData = ctx.getImageData(0, 0, canvasWidth, canvasHeight).data;

			for (let i = 0; i < originImgData.length; i += 4) {
				if (originImgData[i] + originImgData[i + 1] + originImgData[i + 2] + originImgData[i + 3] > 0) {
					yi = Math.floor(i / (4 * canvasWidth));
					xi = Math.floor((i - (yi * 4 * canvasWidth) ) / 4);
					if (xi < minX) minX = xi;
					if (xi > maxX) maxX = xi;
					if (yi < minY) minY = yi;
					if (yi > maxY) maxY = yi;
				}
			}
		}

		imageData = ctx.getImageData(minX, minY, maxX - minX, maxY - minY);
		nCanvas = document.createElement('canvas');
		nCanvas.setAttribute('id', 'nc');
		nCanvas.width = 2 * padding + maxX - minX;
		nCanvas.height = 2 * padding + maxY - minY;
		nCanvas.getContext('2d').putImageData(imageData, padding, padding);
		url = nCanvas.toDataURL().split(',')[1];
		$("canvas#nc").remove();
		return url;
	}
}
