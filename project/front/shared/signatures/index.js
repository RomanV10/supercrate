require('./er-signatures.module');
require('./directives/er-signature-box/er-signature-box.directive');
require('./directives/er-signature-modal/er-signature-modal.directive');
require('./directives/er-signature-modal/er-signature-modal.controller');
require('./services/er-extra-signature-page/er-extra-signature-page.service');
require('./services/er-estimate-signature-processor/er-estimate-signature-processor.service');
require('./services/er-extra-signature-page/er-extra-signature-page-modal.controller');
