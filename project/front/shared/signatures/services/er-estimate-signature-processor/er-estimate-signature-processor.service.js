angular
	.module('er-signatures')
	.service('erEstimateSignatureProcessor', erEstimateSignatureProcessor);

/* @ngInject */
function erEstimateSignatureProcessor() {
	const CLIENT_RESERVATION_SIGNATURE_VARIABLE = '[estimate-signature:client-reservation-signature]';
	const SALES_SIGNATURE_VARIABLE = '[estimate-signature:sales-signature]';

	let service = {
		parseClientReservationSignature,
		parseSalesSignature,
		parseGeneralCustomBlockSignatures,
	};

	return service;

	function parseClientReservationSignature(template, request) {
		let result = template || '';

		let signature = _.get(request, 'generalCustomBlockReservationSignature.value');
		let signatureHTML = _.isEmpty(signature) ? '' : '<er-signature-box signature="request.generalCustomBlockReservationSignature" show-signature-date="1"></er-signature-box>';

		result = result.split(CLIENT_RESERVATION_SIGNATURE_VARIABLE)
			.join(signatureHTML);

		return result;
	}

	function parseSalesSignature(template, request) {
		let result = template || '';

		let signature = _.get(request, 'current_manager.settings.extraSignature.value');

		if (_.isEmpty(signature)) {

			result = result.split(SALES_SIGNATURE_VARIABLE)
				.join('');

			return result;
		}

		if (!_.get(request, 'current_manager.settings.extraSignature')) {
			_.set(request, 'current_manager.settings.extraSignature', {
				value: '',
				date: ''
			});
		} else {
			request.current_manager.settings.extraSignature.date = _.get(request, 'generalCustomBlockReservationSignature.date') || moment.unix(request.created).format('x');
		}

		result = result.split(SALES_SIGNATURE_VARIABLE)
			.join('<er-signature-box signature="request.current_manager.settings.extraSignature" show-signature-footer="1" sales-date-format="1" show-signature-date="1"></er-signature-box>');

		return result;
	}

	function parseGeneralCustomBlockSignatures(template, request) {
		let result = template;
		result = parseClientReservationSignature(result, request);
		result = parseSalesSignature(result, request);

		return result;
	}
}