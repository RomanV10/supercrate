import './er-extra-signature-page.styl';

angular
	.module('er-signatures')
	.service('erExtraSignaturePage', erExtraSignaturePage);

/* @ngInject */
function erExtraSignaturePage($uibModal, datacontext, $q) {
	let service = {
		openPage,
		getSettings,
		isEnableExtraSignature,
		isNeedExtraSignature,
	};

	return service;

	function openPage(request) {
		return $uibModal.open({
			template: require('./er-extra-signature-page-modal.html'),
			controller: 'ErExtraSignaturePageModalController',
			resolve: {
				request: () => request,
			},
			windowClass: 'er-signature-page-modal',
		})
			.result;
	}

	function isNeedExtraSignature(request) {
		let extraSignatures = _.get(request, 'field_estimate_signatures.value', []) || [];
		extraSignatures = angular.fromJson(extraSignatures);
		let isEmpty = !_.isEmpty(extraSignatures) && extraSignatures.some((signature) => _.isEmpty(signature.value));

		return service.isEnableExtraSignature(request.service_type.raw) && (_.isEmpty(extraSignatures) || isEmpty);
	}

	function isEnableExtraSignature(serviceType) {
		let setting = datacontext.getFieldData().extraSignaturesSettings;
		setting = angular.fromJson(setting);
		let {isEnable, template} = setting.extraSignaturesByServiceTypes[serviceType];
		let result = setting.isEnable && isEnable && template;

		return result;
	}

	function getSettings() {
		let setting = datacontext.getFieldData().extraSignaturesSettings;
		setting = angular.fromJson(setting);

		return $q.resolve(setting);
	}
}
