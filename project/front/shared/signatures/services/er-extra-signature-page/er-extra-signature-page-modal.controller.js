angular.module('er-signatures')
	.controller('ErExtraSignaturePageModalController', ErExtraSignaturePageModalController);

/* @ngInject */
function ErExtraSignaturePageModalController($scope, request, erExtraSignaturePage, $q, EmailBuilderRequestService, TemplateBuilderService, $uibModalInstance, RequestServices, printService, Session, $timeout) {
	const SALES_EXTRA_SIGNATURE = '[estimate-signature:sales]';
	const CLIENT_EXTRA_SIGNATURE = '[estimate-signature:client]';

	let session = Session.get();
	let userRole = _.get(session, 'userRole', []);
	let isAuthorizedClient = userRole.length == 1 && ~userRole.indexOf('authenticated user');
	let currentUserPermission = +isAuthorizedClient;

	$scope.busy = true;
	$scope.request = request;
	$scope.template = '<div></div>';
	$scope.enableStyle = TemplateBuilderService.enableStyle;
	$scope.currentExtraSignature = [];
	$scope.saveClientSignature = saveClientSignature;
	$scope.removeSignature = removeSignature;
	$scope.save = save;
	$scope.confirmReservation = confirmReservation;
	$scope.close = close;
	$scope.print = print;
	$scope.isNeedToSign = false;

	init();

	function saveClientSignature() {
		let signature = _.find($scope.currentExtraSignature, (signature) => !!signature.value);
		let signaturesRange = _.range($scope.currentExtraSignature.length);
		$scope.currentExtraSignature.length = 0;

		angular.forEach(signaturesRange, () => {
			$scope.currentExtraSignature.push(angular.copy(signature));
		});

		saveSignature()
			.then(() => sendExtraSignatureLog())
			.then(() => save());
	}

	function removeSignature() {
		saveSignature()
			.then(() => sendExtraSignatureLog('Estimate signature was removed'));
	}

	function saveSignature() {
		return RequestServices.updateRequest(request.nid, {
			field_estimate_signatures: angular.toJson($scope.currentExtraSignature),
		})
			.then(() => {
				_.set(request, 'field_estimate_signatures.value', angular.toJson($scope.currentExtraSignature));
				setIsNeedToSign();
			});
	}

	function sendExtraSignatureLog(text = 'Estimate signature was added') {
		let details = [{
			simpleText: text,
		}];
		RequestServices.sendLogs(details, 'Estimate Signature', request.nid, 'MOVEREQUEST');
	}

	function init() {
		erExtraSignaturePage.getSettings()
			.then((setting) => {
				const {template} = setting.extraSignaturesByServiceTypes[request.service_type.raw];
				const REQUEST_TYPE = 0;

				return EmailBuilderRequestService
					.getTemplatePreview(request.nid, REQUEST_TYPE, {template})
					.then(({data}) => {
						return data[0];
					});
			})
			.then((template) => {
				template = prepareSalesSignature(template);
				template = prepareClientSignatures(template);
				$scope.template = template;
				setIsNeedToSign();
			})
			.finally(() => {
				$scope.busy = false;
			});
	}

	function prepareSalesSignature(template) {
		let result = template;

		let signature = _.get(request, 'current_manager.settings.extraSignature.value');

		if (_.isEmpty(signature)) {
			result = result.replace(SALES_EXTRA_SIGNATURE, '');

			return result;
		}

		if (!_.get(request, 'current_manager.settings.extraSignature')) {
			_.set(request, 'current_manager.settings.extraSignature', {
				value: '',
				date: ''
			});
		} else {
			let date = _.get(request, 'request_all_data.confirmationData.date', '');

			if (date) {
				date = moment(date, 'LLL').format('x');
			} else {
				date = moment().format('x');
			}

			request.current_manager.settings.extraSignature.date = date;
		}

		result = result.split(SALES_EXTRA_SIGNATURE)
			.join('<er-signature-box signature="request.current_manager.settings.extraSignature" show-signature-footer="1" sales-date-format="1" show-signature-date="1"></er-signature-box>');

		return result;
	}

	function prepareClientSignatures(template) {
		let result = template;

		$scope.currentExtraSignature = _.get(request, 'field_estimate_signatures.value', []);

		if (_.isString($scope.currentExtraSignature)) {
			$scope.currentExtraSignature = angular.fromJson($scope.currentExtraSignature || '[]');
		}

		if (!$scope.currentExtraSignature) {
			$scope.currentExtraSignature = [];
		}

		let extraSignatures = $scope.currentExtraSignature;

		const CLIENT_EXTA_SIGNATURE_REGEXP = new RegExp('estimate-signature:client', 'g');
		let signaturesCount = (template.match(CLIENT_EXTA_SIGNATURE_REGEXP) || []).length;

		if (!signaturesCount) {
			return result;
		}

		while (extraSignatures.length < signaturesCount) {
			extraSignatures.push({});
		}

		let currentSignature = 0;
		result = result.split(CLIENT_EXTRA_SIGNATURE)
			.map((currentTemplate) => {
				if (currentSignature < signaturesCount) {
					currentTemplate += `<er-signature-box signature="currentExtraSignature[${currentSignature}]" can-edit="${currentUserPermission}" on-sign="saveClientSignature()" on-remove="removeSignature()" show-signature-date="1"></er-signature-box>`;
					currentSignature++;
				}

				return currentTemplate;
			})
			.join('');

		return result;
	}


	function save() {
		$uibModalInstance.close();
	}

	function confirmReservation() {
		$timeout(() => {
			angular.element('.er-signature-page-modal .er-signature-box__make_action:eq(0)')
				.trigger('click');
		});
	}

	function close() {
		$uibModalInstance.dismiss();
	}

	function print() {
		let template = angular.element('.er-signature-page-modal__body')[0].innerHTML;
		let printStyle = require('!raw-loader!stylus-loader!./er-extra-signature-page-print.styl');

		printService.printThisLayout(template, printStyle);
	}

	function setIsNeedToSign() {
		$scope.isNeedToSign = isAuthorizedClient && erExtraSignaturePage.isNeedExtraSignature(request);
	}
}
