describe('valuation-modal.controller,', () => {
	let $controller, $ctrl, $scope, request, invoice, valuationService, valuationPlans;
	
	let fakeSaveValuationPromiseResolve = {
		then: callback => {
			let messages = ['lala-topola'];
			callback(messages);
			return fakeSaveValuationPromiseResolve;
		}
	};
	
	beforeEach(inject((_$controller_, _valuationService_, SweetAlert) => {
		SweetAlert.swal = (params, callback) => callback(true);
		valuationService = _valuationService_;
		valuationPlans = testHelper.loadJsonFile('valuation-modal_valuationPlans.mock');
		spyOn(valuationService, 'getValuationPlan').and.callFake(() => valuationPlans);
		
		$controller = _$controller_;
		request = testHelper.loadJsonFile('valuation-modal_request.mock');
		invoice = _.clone(request.request_all_data.invoice);
		$scope = $rootScope.$new();
		$ctrl = $controller('ValuationModalCtrl', {
			$scope: $scope, $uibModalInstance: {
				dismiss: () => {},
				close: () => {},
			},
			type: 'request',
			request: request,
			invoice: invoice,
			grandTotal: '12345',
			readOnly: false,
		});
		
	}));
	
	describe('After init,', () => {
		it('Ctrl should be defined', () => {
			expect($ctrl).toBeDefined();
		});
		it('$scope.disableSave Should be falsy', () => {
			expect($scope.disableSave).toBeFalsy();
		});
		it('Should be defined function clickSave', () => {
			expect($scope.clickSave).toBeDefined();
		});
		it('Should be defined function cancel', () => {
			expect($scope.cancel).toBeDefined();
		});
		
		it('Should be defined nid', () => {
			expect($scope.nid).toBeDefined();
		});
		
		it('Should be defined nid', () => {
			expect($scope.name).toBeDefined();
		});
	});
	
	describe('When request have \'Completed\' flag,', () => {
		beforeEach(() => {
			let COMPLETE_FLAG = 3;
			request.field_company_flags = {
				value: {
					[COMPLETE_FLAG]: 'Completed'
				}
			};
			invoice = _.clone(request.request_all_data.invoice);
			$scope = $rootScope.$new();
			$ctrl = $controller('ValuationModalCtrl', {
				$scope: $scope, $uibModalInstance: () => {
					return {};
				},
				type: 'request',
				request: request,
				invoice: invoice,
				grandTotal: '12345',
				readOnly: false,
			});
		});
		it('$scope.disableSave Should be truthy', () => {
			expect($scope.disableSave).toBeTruthy();
		});
	});
	
	describe('function clickSave,', () => {
		beforeEach(() => {
			spyOn(valuationService, 'saveRequestValuation').and.callFake(() => fakeSaveValuationPromiseResolve);
			spyOn($rootScope, '$broadcast');
			$scope.clickSave();
		});
		it('Should call valuationService.saveValuation()', () => {
			expect(valuationService.saveRequestValuation).toHaveBeenCalled();
		});
		it('$scope.busy should be falsy', () => {
			expect($scope.busy).toBeFalsy();
		});
		it('Should broadcast recalculating grand total', () => {
			expect($rootScope.$broadcast).toHaveBeenCalledWith('grandTotal.sales', {
				grand: '12345',
				logs: ['lala-topola']
			}, '4297');
		});
	});
});
