'use strict';

import './valuation-modal-new.styl';

angular
	.module('valuation-shared')
	.controller('ValuationModalCtrl', ValuationModalCtrl);

ValuationModalCtrl.$inject = ['$controller', '$scope', '$rootScope', '$uibModalInstance', 'type', 'request', 'invoice', 'grandTotal', 'valuationService', 'SweetAlert', 'readOnly'];

function ValuationModalCtrl($controller, $scope, $rootScope, $uibModalInstance, type, request, invoice, grandTotal, valuationService, SweetAlert, readOnly) {
	$controller('ValuationBaseController', {
		$scope,
		request,
		invoice,
	});
	
	$scope.clickSave = clickSave;
	$scope.cancel = cancel;
	init();
	
	function init() {
		$scope.baseCtrlInit(true);
		$scope.readOnly = readOnly;
		
		$scope.nid = request.nid;
		$scope.name = request.name;
		
		let completeFlag = _.get($rootScope, 'availableFlags.company_flags[\'Completed\']');
		let requestFlags = _.get(request, 'field_company_flags.value', []);
		$scope.disableSave = _.has(requestFlags, completeFlag);
	}
	
	function clickSave() {
		SweetAlert.swal({
			title: 'Save Valuation ?',
			text: '',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#E47059',
			confirmButtonText: 'Confirm',
			closeOnConfirm: true
		}, function (isConfirm) {
			if (isConfirm) {
				saveValuation();
			}
		});
	}
	
	function saveValuation() {
		$scope.busy = true;
		$scope.valuation.other.set_by_manager = true;
		request.request_all_data.valuation = $scope.valuation;
		let paramsForSaving = {
			valuation_type: $scope.valuation.selected.valuation_type,
			title: $scope.valuation.settings.title,
			deductible_level: $scope.valuation.selected.deductible_level,
			valuation_charge: $scope.valuation.selected.valuation_charge,
		};
		valuationService.saveRequestValuation(paramsForSaving, request, invoice, type)
			.then(arr => {
				$scope.busy = false;
				let tabName = type == 'invoice' ? 'closing' : 'sales';
				$rootScope.$broadcast(`grandTotal.${tabName}`, {	//live recalculating and saving grand total
					grand: grandTotal,
					logs: arr
				}, request.nid);
				toastr.success('Valuation saved!');
				$uibModalInstance.close();
			});
	}
	
	function cancel() {
		$uibModalInstance.dismiss('cancel');
	}
}
