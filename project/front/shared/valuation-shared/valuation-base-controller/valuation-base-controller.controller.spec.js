describe('valuation-modal.controller,', () => {
	let $controller, $ctrl, $scope, request, invoice, valuationService, valuationPlans;

	beforeEach(inject((_$controller_, _valuationService_, SweetAlert) => {
		SweetAlert.swal = (params, callback) => {if (callback) callback(true)};
		valuationService = _valuationService_;
		valuationPlans = testHelper.loadJsonFile('valuation-base-controller_valuationPlans.mock');
		spyOn(valuationService, 'getValuationPlan').and.callFake(() => valuationPlans);
		
		$controller = _$controller_;
		request = testHelper.loadJsonFile('valuation-base-controller_request.mock');
		invoice = angular.copy(request.request_all_data.invoice);
		$scope = $rootScope.$new();
		$ctrl = $controller('ValuationBaseController', {
			request: request,
			$scope: $scope
		});
		
		$scope.baseCtrlInit(true);
	}));
	
	describe('After init,', () => {
		it('Ctrl should be defined', () => {
			expect($ctrl).toBeDefined();
		});
		it('$scope.valuationTitles shoud be defined', () => {
			expect($scope.valuationTitles).toBeDefined();
		});
		it('$scope.valuation shoud be defined', () => {
			expect($scope.valuation).toBeDefined();
		});
		it('Should calculate request total weight', () => {
			expect($scope.weight).toEqual(11662);
		});
		it('Should be defined function changeLiabilityAmount', () => {
			expect($scope.changeLiabilityAmount).toBeDefined();
		});
		it('Should be defined currentPlan', () => {
			expect($scope.currentPlan).toBeDefined();
		});
		it('Should be defined tableRow', () => {
			expect($scope.tableRow).toEqual(['26', '21', '16']);
		});
		it('Should remove invoice valuation when not invoice state', () => {
			//given

			//when

			//then
			expect(request.request_all_data.invoice.request_all_data.valuation).toBeUndefined();
		});
		it('Shouldn\'t remove invoice valuation when invoice state', () => {
			//given
			request.status.raw = 3;
			request.request_all_data.invoice = angular.copy(invoice);

			//when
			$ctrl = $controller('ValuationBaseController', {
				request: request,
				$scope: $scope
			});
			$scope.baseCtrlInit(true);

			//then
			expect(request.request_all_data.invoice.request_all_data.valuation).toBeDefined();
		});
	});
	
	describe('function changeLiabilityAmount,', () => {
		beforeEach(() => {
			spyOn(valuationService, 'calculateValuation').and.callThrough();
			$scope.changeLiabilityAmount();
		});
		
		it('Should call recalculate service', () => {
			let paramsForCalculating = {
				valuation_type: 2,
				liability_amount: 7000,
				deductible_level: '300',
				valuation_plan: 0,
				weight: 11662,
			};
			expect(valuationService.calculateValuation).toHaveBeenCalledWith(paramsForCalculating);
		});
		it('Should write results in $scope.valuation.selected.valuation_charge', () => {
			expect($scope.valuation.selected.valuation_charge).toEqual(1470);
		});
		it('Should write results in $scope.tableRow', () => {
			expect($scope.tableRow).toEqual(['26', '21', '16']);
		});
		it('Should make deductible_data', () => {
			let expectedData = {
				deductible_level: ['200', '300', '400'],
				valuation_charge: [1820, 1470, 1120],
				selected: [false, true, false]
			};
			expect($scope.valuation.deductible_data).toEqual(expectedData);
		});
		describe('When PER_POUND selected,', () => {
			beforeEach(() => {
				$scope.valuation.selected.valuation_type = '1';
				$scope.changeLiabilityAmount();
			});
			it('Should make deductible_data', () => {
				let expectedData = {
					deductible_level: ['200', '300', '400'],
					valuation_charge: [1820, 1470, 1120],
					selected: [false, false, false]
				};
				expect($scope.valuation.deductible_data).toEqual(expectedData);
			});
		});
	});

	describe('function changeOnlyLiabilityAmount,', () => {
		it('change_liability_amount should be true', () => {
			//given
			$scope.valuation.selected.liability_amount = 0;

			//when
			$scope.changeOnlyLiabilityAmount();

			//then
			expect($scope.valuation.change_liability_amount).toBeTruthy();
		});
	});
	
	describe('function validateAmountOfLiability', () => {
		it('should be called on changeOnlyLiabilityAmount triggered', () => {
			// given
			let validateAmountOfLiabilitySpy = spyOn($scope, 'validateAmountOfLiability').and.callThrough();
			// when
			$scope.changeOnlyLiabilityAmount();
			// then
			expect(validateAmountOfLiabilitySpy).toHaveBeenCalled();
		});
		
		it('should revert not valid value', () => {
			// given
			$scope.originalAmountOfLiability = 777;
			$scope.valuation.selected.liability_amount = 27000;
			// when
			$scope.validateAmountOfLiability();
			// then
			expect($scope.valuation.selected.liability_amount).toEqual($scope.originalAmountOfLiability);
		});
		
		it('not should revert valid value', () => {
			// given
			$scope.originalAmountOfLiability = 777;
			$scope.valuation.selected.liability_amount = 250;
			// when
			$scope.validateAmountOfLiability();
			// then
			expect($scope.valuation.selected.liability_amount).toEqual(250);
		});
	});

	describe('function revertLiabilityAmount,', () => {
		let expectLabilityAmount;

		beforeEach(() => {
			//given
			expectLabilityAmount = 7000;

			//when
			$scope.recalculateLiabilityAmount();
		});

		it('change_liability_amount should be false', () => {
			//then
			expect($scope.valuation.change_liability_amount).toBeFalsy();
		});

		it('liability_amount shoud be 6997.2', () => {

			expect($scope.valuation.selected.liability_amount).toBe(expectLabilityAmount);
		});
	});
	
	describe('function: setValuationType(),', () => {
		describe('when set to \'1\',', () => {
			beforeEach(() => {
				$scope.valuation.selected.deductible_level = '200';
				spyOn(valuationService, 'calculateAmountOfLiability').and.callFake(() => 12345);
				spyOn($scope, 'changeLiabilityAmount');
				$scope.setValuationType('1');
			});
			it('Should NOT clean selected deductible', () => {
				expect($scope.valuation.selected.deductible_level).toEqual('200');
			});
			
			it('Should recalculate $scope.valuation.liability_amount', () => {
				expect($scope.valuation.selected.liability_amount).toEqual(12345);
			});
			it('Should call valuationService.calculateAmountOfLiability', () => {
				expect(valuationService.calculateAmountOfLiability).toHaveBeenCalled();
			});
			it('Should set $scope.valuation.valuation_type to \'1\'', () => {
				expect($scope.valuation.selected.valuation_type).toEqual('1');
			});
			it('Should call $scope.changeLiabilityAmount()', () => {
				expect($scope.changeLiabilityAmount).toHaveBeenCalled();
			});
			
			describe('And then set to \'2\'', () => {
				beforeEach(() => {
					$scope.setValuationType('2');
				});
				it('Should return selected deductible_level', () => {
					expect($scope.valuation.selected.deductible_level).toEqual('200');
				});
			});
		});
	});
	
	describe('function: setDeductibleLevel(),', () => {
		beforeEach(() => {
			spyOn($scope, 'changeLiabilityAmount');
			$scope.setDeductibleLevel('333');
		});
		it('Should set $scope.valuation.deductible_level to \'333\'', () => {
			expect($scope.valuation.selected.deductible_level).toEqual('333');
		});
		it('Should call $scope.changeLiabilityAmount()', () => {
			expect($scope.changeLiabilityAmount).toHaveBeenCalled();
		});
	});

	describe('funciton: setValuationType', () => {
		describe('when select 60 cents per pound', () => {
			let SIXTY_CENTS_PER_POUND_TYPE;

			beforeEach(() => {
				//given
				spyOn($scope, 'recalculateLiabilityAmount').and.callFake(() => {});
				SIXTY_CENTS_PER_POUND_TYPE = '1';

				//when
				$scope.setValuationType(SIXTY_CENTS_PER_POUND_TYPE);
			});

			//then
			it('selected valuation type should be 1', () => {
				expect($scope.valuation.selected.valuation_type).toBe(SIXTY_CENTS_PER_POUND_TYPE);
			});

			it('recalculateLiabilityAmount to have been called', () => {
				expect($scope.recalculateLiabilityAmount).toHaveBeenCalled();
			});
		});
		
		describe('when select full value protection', () => {
			let Full_VALUE_PROTECTION_TYPE;

			beforeEach(() => {
				//given
				spyOn($scope, 'recalculateLiabilityAmount').and.callFake(() => {});
				Full_VALUE_PROTECTION_TYPE = '2';

				//when
				$scope.setValuationType(Full_VALUE_PROTECTION_TYPE);
			});

			//then
			it('selected valuation type should be 2', () => {
				expect($scope.valuation.selected.valuation_type).toBe(Full_VALUE_PROTECTION_TYPE);
			});

			it('recalculateLiabilityAmount to have been called', () => {
				expect($scope.recalculateLiabilityAmount).toHaveBeenCalled();
			});
		});
	});
});
