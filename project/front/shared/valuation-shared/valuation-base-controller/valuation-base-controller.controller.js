'use strict';

angular
	.module('valuation-shared')
	.controller('ValuationBaseController', ValuationBaseController);

ValuationBaseController.$inject = ['$scope', 'request', 'valuationService', 'CalculatorServices', 'SweetAlert'];

function ValuationBaseController($scope, request, valuationService, CalculatorServices, SweetAlert) {
	const CONFIRMED_STATUS = 3;
	let valuationPlans = {};
	$scope.changeLiabilityAmount = changeLiabilityAmount;
	$scope.setValuationType = setValuationType;
	$scope.setDeductibleLevel = setDeductibleLevel;
	$scope.changeOnlyLiabilityAmount = changeOnlyLiabilityAmount;
	$scope.recalculateLiabilityAmount = recalculateLiabilityAmount;
	$scope.baseCtrlInit = baseCtrlInit;
	$scope.validateAmountOfLiability = validateAmountOfLiability;
	
	function baseCtrlInit(safeMode = false) {
		$scope.valuationPlan = valuationService.getSetting('valuation_plan_settings');
		$scope.activePlan = _.findIndex($scope.valuationPlan, plan => plan.selected);
		$scope.calculateCharge = value => calculateValuation(value).valuation_charge;
		$scope.valuationTitles = valuationService.getSetting('valuation_titles');
		$scope.valuationTypes = valuationService.VALUATION_TYPES;

		if (safeMode) {
			$scope.valuation = angular.copy(request.request_all_data.valuation);
		} else {
			$scope.valuation = request.request_all_data.valuation;
		}

		$scope.weight = CalculatorServices.getTotalWeight(request).weight * 7;
		valuationService.fillEmptyFields($scope.valuation, $scope.weight);
		let valuationPlanTableName = valuationService.TABLE_NAMES[$scope.activePlan];
		valuationPlans = valuationService.getValuationPlan();
		$scope.currentPlan = valuationPlans[valuationPlanTableName];
		let rowNumber = valuationService.findTableRow($scope.valuation.selected.liability_amount, $scope.currentPlan);
		$scope.originalAmountOfLiability = angular.copy($scope.valuation.selected.liability_amount);
		$scope.tableRow = $scope.currentPlan.body[rowNumber].value;

		if (request.status.raw != CONFIRMED_STATUS && _.get(request, 'request_all_data.invoice.request_all_data.valuation')) {
			request.request_all_data.invoice.request_all_data.valuation = undefined;
		}
	}
	
	function changeOnlyLiabilityAmount() {
		$scope.valuation.change_liability_amount = true;
		changeLiabilityAmount();
	}
	
	function recalculateLiabilityAmount() {
		$scope.valuation.change_liability_amount = false;
		$scope.valuation.selected.liability_amount = valuationService.calculateAmountOfLiability($scope.weight);
		$scope.changeLiabilityAmount();
	}
	
	function setValuationType(type) {
		$scope.valuation.selected.valuation_type = type;
		$scope.recalculateLiabilityAmount();
	}
	
	function setDeductibleLevel(value) {
		$scope.valuation.selected.deductible_level = value;
		$scope.changeLiabilityAmount();
	}
	
	function changeLiabilityAmount() {
		$scope.validateAmountOfLiability();
		
		let result = calculateValuation();
		
		$scope.valuation.selected.valuation_charge = result.valuation_charge;
		$scope.tableRow = result.tableRow;
		let selectedArray = $scope.currentPlan.header.map(deductibleLevel => {
			return deductibleLevel == $scope.valuation.selected.deductible_level
				&& $scope.valuation.selected.valuation_type == $scope.valuationTypes.FULL_VALUE;
		});
		let calculatedValuations = $scope.currentPlan.header.map(
			deductibleLevel => calculateValuation(deductibleLevel, $scope.valuationTypes.FULL_VALUE).valuation_charge
		);
		$scope.valuation.deductible_data = {
			deductible_level: $scope.currentPlan.header,
			valuation_charge: calculatedValuations,
			selected: selectedArray,
		};
		
		$scope.valuation.settings.title = _.get($scope.valuationTitles, `${[$scope.valuation.selected.valuation_type]}.title`);
		$scope.valuation.settings.explanation = _.get($scope.valuationTitles, `${[$scope.valuation.selected.valuation_type]}.explanation`);
		$scope.valuation.settings.updated = moment().unix();
		$scope.originalAmountOfLiability = $scope.valuation.selected.liability_amount;
	}
	
	function validateAmountOfLiability () {
		let lastAmountOfLiability = +_.last($scope.currentPlan.body).name;
		
		if ($scope.valuation.selected.liability_amount > lastAmountOfLiability) {
			let accountText = `The Amount of Liability must be less than or equal to $${lastAmountOfLiability}`;
			let moveBoardText = 'This Amount of Liability does not exist in your Valuation Plan. Please add it to the Valuation Plan settings';
			SweetAlert.swal({
				title: !IS_ACCOUNT ? moveBoardText : accountText,
				text: `The Amount of Liability will be restored to $${$scope.originalAmountOfLiability}`,
				type: 'error'
			});
			$scope.valuation.selected.liability_amount = $scope.originalAmountOfLiability;
			$scope.valuation.change_liability_amount = false;
		}
	}
	
	function calculateValuation(deductibleLevel = $scope.valuation.selected.deductible_level, valuation_type = $scope.valuation.selected.valuation_type) {
		let paramsForCalculating = {
			valuation_type: valuation_type,
			liability_amount: $scope.valuation.selected.liability_amount,
			deductible_level: deductibleLevel,
			valuation_plan: $scope.activePlan,
			weight: $scope.weight,
		};
		return valuationService.calculateValuation(paramsForCalculating);
	}
}
