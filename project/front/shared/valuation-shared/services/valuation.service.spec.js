describe('valuation.service', () => {
	let $rootScope, valuationService, RequestServices, request, apiService, moment_lib;
	
	let fakePromiseResolve = {
		then: callback => {
			callback();
			return fakePromiseResolve;
		}, catch: callback => {
			return fakePromiseResolve;
		}
	};
	
	describe('when options not setted,', () => {
		let $rootScope, defaultPlans;
		
		beforeEach(inject((_$rootScope_) => {
			defaultPlans = testHelper.loadJsonFile('valuation.service.defaults_valuation_plans.mock');
		}));
		
		describe("When valuation_plan_tables not exist,", () => {
			beforeEach(inject((_$rootScope_) => {
				$rootScope = _$rootScope_;
				delete $rootScope.fieldData.valuation_plan_tables;
			}));
			beforeEach(inject((_valuationService_) => {
				valuationService = _valuationService_;
			}));
			it('Should set defaults valuation tables', () => {
				expect(valuationService.getValuationPlan()).toEqual(defaultPlans);
			});
		});
		
		describe("When valuation_plan_tables exist, but not full,", () => {
			beforeEach(inject((_$rootScope_) => {
				$rootScope = _$rootScope_;
				let brokenPlans = _.cloneDeep(defaultPlans);
				delete brokenPlans.percentTable.header;
				delete brokenPlans.fixedPriceTable.body;
				delete brokenPlans.rateTable;
				$rootScope.fieldData.valuation_plan_tables = brokenPlans;
			}));
			beforeEach(inject((_valuationService_) => {
				valuationService = _valuationService_;
			}));
			it('Should set default headers if not exist', () => {
				expect(valuationService.getValuationPlan().percentTable).toEqual(defaultPlans.percentTable);
			});
			it('Should set default body if not exist', () => {
				expect(valuationService.getValuationPlan().fixedPriceTable).toEqual(defaultPlans.fixedPriceTable);
			});
			it('Should set default full table if not exist', () => {
				expect(valuationService.getValuationPlan().rateTable).toEqual(defaultPlans.rateTable);
			});
		});
	});
	
	describe('When default settings,', () => {
		
		beforeEach(inject((_valuationService_, _RequestServices_, _apiService_, _moment_) => {
			apiService = _apiService_;
			RequestServices = _RequestServices_;
			valuationService = _valuationService_;
			request = testHelper.loadJsonFile('valuation.service_request.mock');
			moment_lib = _moment_;
		}));
		
		describe('function findTableRow,', () => {
			let table;
			beforeEach(() => {
				table = valuationService.getValuationPlan().percentTable;
			});
			it('Should return 1st row', () => {
				expect(valuationService.findTableRow(50, table)).toEqual(0);
			});
			it('Should return 2nd row', () => {
				expect(valuationService.findTableRow(1050, table)).toEqual(1);
			});
			it('Should return last row', () => {
				expect(valuationService.findTableRow(2050, table)).toEqual(2);
			});
			it('Should return last row if too big', () => {
				expect(valuationService.findTableRow(3050, table)).toEqual(2);
			});
		});
		
		describe('function saveRequestValuation,', () => {
			let paramsForSaving, result;
			
			describe('when request state', () => {
				beforeEach(() => {
					spyOn(RequestServices, 'saveReqData').and.callFake(() => fakePromiseResolve);
					paramsForSaving = {
						valuation_type: 1,
						title: 'customTitle',
						deductible_level: 'selectedDeductible',
						valuation_charge: 'selectedCahrge',
					};
					valuationService.saveRequestValuation(paramsForSaving, request, request.request_all_data.invoice,
						'request')
						.then(arr => {
							result = arr;
						});
					$timeout.flush();
				});
				it('Should call RequestServices.saveReqData', () => {
					expect(RequestServices.saveReqData).toHaveBeenCalled();
				});
				it('Should return proper message', () => {
					expect(result).toEqual(
						[{simpleText: 'Valuation was changed on sales tab. Released value(customTitle)'}]);
				});
				it('Should not copy valuation to invoice', () => {
					expect(request.request_all_data.invoice.request_all_data.valuation).not.toEqual(
						request.request_all_data.valuation);
				});
			});
			
			describe('when invoice state', () => {
				beforeEach(() => {
					spyOn(RequestServices, 'saveReqData').and.callFake(() => fakePromiseResolve);
					paramsForSaving = {
						valuation_type: 1,
						title: 'customTitle',
						deductible_level: 'selectedDeductible',
						valuation_charge: 'selectedCahrge',
					};
					request.status.raw = 3;
					valuationService.saveRequestValuation(paramsForSaving, request, request.request_all_data.invoice,
						'invoice')
						.then(arr => {
							result = arr;
						});
					$timeout.flush();
				});
				it('Should call RequestServices.saveReqData', () => {
					expect(RequestServices.saveReqData).toHaveBeenCalled();
				});
				it('Should return proper message', () => {
					expect(result).toEqual(
						[{simpleText: 'Valuation was changed on closing tab. Released value(customTitle)'}]);
				});
				it('Should copy valuation to invoice', () => {
					expect(request.request_all_data.invoice.request_all_data.valuation).toEqual(
						request.request_all_data.valuation);
				});
			});
		});
		
		describe('function calculateValuation,', () => {
			let params, result;
			beforeEach(() => {
				params = {
					liability_amount: 1234,
					deductible_level: '300',
					weight: 2000,
				};
			});
			describe('When by percent active,', () => {
				beforeEach(() => {
					params.valuation_plan = 0;
				});
				describe('And Full Value protection checked,', () => {
					beforeEach(() => {
						params.valuation_type = 2;
						result = valuationService.calculateValuation(params);
					});
					it('Should return proper tableRow', () => {
						expect(result.tableRow).toEqual(['28', '23', '18']);
					});
					it('Should return proper valuation_charge', () => {
						expect(result.valuation_charge).toEqual(283.82);
					});
				});
				describe('And 60-cents checked,', () => {
					beforeEach(() => {
						params.valuation_type = 1;
						result = valuationService.calculateValuation(params);
					});
					it('Should return proper tableRow', () => {
						expect(result.tableRow).toEqual(['28', '23', '18']);
					});
					it('Should return proper valuation_charge', () => {
						expect(result.valuation_charge).toEqual(0);
					});
				});
			});
			describe('When by value active,', () => {
				beforeEach(() => {
					params.valuation_plan = 1;
				});
				describe('And Full Value protection checked,', () => {
					beforeEach(() => {
						params.valuation_type = 2;
						result = valuationService.calculateValuation(params);
					});
					it('Should return proper tableRow', () => {
						expect(result.tableRow).toEqual(['6']);
					});
					it('Should return proper valuation_charge', () => {
						expect(result.valuation_charge).toEqual(6);
					});
				});
				describe('And 60-cents checked,', () => {
					beforeEach(() => {
						params.valuation_type = 1;
						result = valuationService.calculateValuation(params);
					});
					it('Should return proper tableRow', () => {
						expect(result.tableRow).toEqual(['6']);
					});
					it('Should return proper valuation_charge', () => {
						expect(result.valuation_charge).toEqual(0);
					});
				});
			});
			describe('When by rate active,', () => {
				beforeEach(() => {
					params.valuation_plan = 2;
				});
				describe('And Full Value protection checked,', () => {
					beforeEach(() => {
						params.valuation_type = 2;
						result = valuationService.calculateValuation(params);
					});
					it('Should return proper tableRow', () => {
						expect(result.tableRow).toEqual(['100', '101']);
					});
					it('Should return proper valuation_charge', () => {
						expect(result.valuation_charge).toEqual(124.63);
					});
				});
				describe('And 60-cents checked,', () => {
					beforeEach(() => {
						params.valuation_type = 1;
						result = valuationService.calculateValuation(params);
					});
					it('Should return proper tableRow', () => {
						expect(result.tableRow).toEqual(['100', '101']);
					});
					it('Should return proper valuation_charge', () => {
						expect(result.valuation_charge).toEqual(0);
					});
				});
			});
		});
		describe('function reCalculateValuation', () => {
			beforeEach(function () {
				spyOn(RequestServices, 'saveReqData').and.callThrough();
				spyOn(valuationService, 'fillEmptyFields').and.callThrough();
				valuationService.reCalculateValuation(request, undefined, 'request');
			});
			it('should calculate valuation charge', () => {
				expect(request.request_all_data.valuation.selected.valuation_charge).toEqual(1469.41);
			});
			it('should save request_all_data', () => {
				expect(RequestServices.saveReqData).toHaveBeenCalled();
			});
			it('should call fill empty field', () => {
				expect(valuationService.fillEmptyFields).toHaveBeenCalled();
			});
			describe('should recalculate liability amount when', () => {
				beforeEach(() => {
					request.request_all_data.valuation.selected.liability_amount = 1111;
					let today = moment('2018-03-05').startOf('date');
					spyOn(window, 'moment').and.callFake(function (parameter) {
						if (parameter) {
							return moment_lib(parameter);
						} else {
							return today;
						}
					});
				});
				it('60 cents per pound valuation type', () => {
					//given
					request.request_all_data.valuation.valuation_type = 0;
					let expectedResult = 6997.2;
					
					//when
					valuationService.reCalculateValuation(request, undefined, 'request');
					
					//then
					expect(request.request_all_data.valuation.selected.liability_amount).toBe(expectedResult);
				});
				it('not changed liability amount', () => {
					//given
					request.request_all_data.valuation.valuation_type = 1;
					request.request_all_data.valuation.change_liability_amount = false;
					let expectedResult = 6997.2;
					
					//when
					valuationService.reCalculateValuation(request, undefined, 'request');
					
					//then
					expect(request.request_all_data.valuation.selected.liability_amount).toBe(expectedResult);
				});
			});
			it('shouldn\'t recalculate liability amount', () => {
				//given
				request.request_all_data.valuation.selected.liability_amount = 1111;
				request.request_all_data.valuation.valuation_type = 1;
				request.request_all_data.valuation.change_liability_amount = true;
				let expectedResult = 1111;
				
				//when
				valuationService.reCalculateValuation(request, undefined, 'request');
				
				//then
				expect(request.request_all_data.valuation.selected.liability_amount).toBe(expectedResult);
			});
			it('should update invoice if present invoice', () => {
				//given
				request.request_all_data.valuation.settings = {updated: 1123};
				let expectedResult = testHelper.loadJsonFile(
					'valuation_recalculate_valuation_invoice_expected_result.mock');
				
				//when
				valuationService.reCalculateValuation(request, request.request_all_data.invoice, 'request');
				
				//then
				expect(request.request_all_data.invoice.request_all_data.valuation).toEqual(expectedResult);
			});
			it('Should recalculate deductible_data.valuation_charge', () => {
				//when
				valuationService.reCalculateValuation(request, request.request_all_data.invoice, 'request');
				
				//then
				expect(request.request_all_data.valuation.deductible_data.valuation_charge).toEqual(
					[1819.27, 1469.41, 1119.55]);
			});
		});
		
		describe('calculateAmountOfLiability', () => {
			it('should calculate liability amount', () => {
				//given
				let weight = 1754;
				let expectedResult = 1052.4;
				//when
				let result = valuationService.calculateAmountOfLiability(weight);
				//then
				expect(result).toEqual(expectedResult);
			});
		});
		
		describe('checkValuationChanges', () => {
			it('should return false', () => {
				//when
				let result = valuationService.checkValuationChanges('service_types');
				//then
				expect(result).toBeFalsy();
			});
			
			it('should return true', () => {
				// when
				valuationService.setSetting('service_types', {});
				let result = valuationService.checkValuationChanges('service_types');
				// then
				expect(result).toBeTruthy();
			});
		});
	});
	
});