'use strict';

import {
	CAN_EDIT_AMOUNT_OF_LIABILITY,
	CAN_EDIT_AMOUNT_OF_VALUATION_ON_ACCOUNT,
	CENT_PER_POUND,
	CONFIRMATION_TABLE_SHOW,
	REQUEST_SERVICE_TYPES,
	TABLE_NAMES,
	VALUATION_ACCOUNT_SHOW_SETTING,
	VALUATION_PLAN_SETTINGS,
	VALUATION_PROPOSAL_TO_CONTACT,
	VALUATION_RATE_COEFFICIENT,
	VALUATION_RESERVATION_PAGE_SETTING,
	VALUATION_TITLES
} from '../helper/helper';
import {VALUATION_TABS} from '../helper/valuation-tabs';

angular
	.module('valuation-shared')
	.factory('valuationService', valuationService);

valuationService.$inject = ['$rootScope', '$q', 'apiService', 'moveBoardApi', 'datacontext', 'SettingServices', 'Raven', 'RequestServices', 'CalculatorServices', '$uibModal'];

function valuationService($rootScope, $q, apiService, moveBoardApi, datacontext, SettingServices, Raven, RequestServices, CalculatorServices, $uibModal) {
	const SETTING_NAME = 'valuationSettings';
	const VALUATION_PLAN_NAME = 'valuation_plan_tables';
	const CONFIRM_STATUS = 3;
	const POUND_COEFFICIENT = 7;
	let valuationSettings = _.get(datacontext.getFieldData(), SETTING_NAME, {});
	let enums = _.get(datacontext.getFieldData(), 'enums');
	const VALUATION_TYPES = _.get(enums, 'valuation_types');

	let valuationPlans = _.get(datacontext.getFieldData(), VALUATION_PLAN_NAME, false);
	let valuationChanges = {};

	let settings = {
		service_types: _.get(valuationSettings, 'service_types', REQUEST_SERVICE_TYPES),
		edit_amount_of_valuation: _.get(valuationSettings, 'edit_amount_of_valuation', CAN_EDIT_AMOUNT_OF_VALUATION_ON_ACCOUNT),
		edit_amount_of_liability: _.get(valuationSettings, 'edit_amount_of_liability', CAN_EDIT_AMOUNT_OF_LIABILITY),
		show_valuation_confirmation_page_setting: _.get(valuationSettings, 'show_valuation_confirmation_page_setting', REQUEST_SERVICE_TYPES),
		show_valuation_confirmation_page_global_setting: _.get(valuationSettings, 'show_valuation_confirmation_page_global_setting', CAN_EDIT_AMOUNT_OF_VALUATION_ON_ACCOUNT),
		valuation_titles: _.get(valuationSettings, 'valuation_titles', VALUATION_TITLES),
		valuation_plan_settings: _.get(valuationSettings, 'valuation_plan_settings', VALUATION_PLAN_SETTINGS),
		show_valuation_confirmation_page: _.get(valuationSettings, 'show_valuation_confirmation_page', VALUATION_RESERVATION_PAGE_SETTING),
		valuation_proposal_to_contact: _.get(valuationSettings, 'valuation_proposal_to_contact', VALUATION_PROPOSAL_TO_CONTACT),
		valuation_account_show_setting: _.get(valuationSettings, 'valuation_account_show_setting', VALUATION_ACCOUNT_SHOW_SETTING),
		valuation_cent_per_pound: _.get(valuationSettings, 'valuation_cent_per_pound', CENT_PER_POUND),
		valuation_rate_coefficient: _.get(valuationSettings, 'valuation_rate_coefficient', VALUATION_RATE_COEFFICIENT),
		confirmation_table_show: _.get(valuationSettings, 'confirmation_table_show', CONFIRMATION_TABLE_SHOW),
	};

	checkMissingSettings();

	let service = {
		saveValuationPlan,
		getValuationPlan,
		getSetting,
		setSetting,
		updateSetting,
		findTableRow,
		calculateValuation,
		saveRequestValuation,
		fillEmptyFields,
		reCalculateValuation,
		reCalculateValuationOnContract,
		openValuationModal,
		calculateAmountOfLiability,
		checkValuationChanges,
		sendValuationNotification,
		sendValuationLogs,
		TABLE_NAMES,
		VALUATION_TABS,
		VALUATION_TYPES,
	};

	return service;

	function checkMissingSettings() {
		if (!valuationPlans) {
			valuationPlans = {};
		}
		_.forEach(TABLE_NAMES, name => {
			if (!valuationPlans[name])
				valuationPlans[name] = {};
		});
		_.forEach(valuationPlans, plan => {
			if (_.isEmpty(plan.header)) plan.header = [0];
			if (_.isEmpty(plan.body)) plan.body = [
				{
					name: 0,
					value: [0]
				}
			];
			_.forEach(plan.body, row => {
				if (_.isEmpty(row.value)) row.value = [0];
			});
		});
		if (!_.find(settings.valuation_plan_settings, plan => plan.selected))
			settings.valuation_plan_settings[0].selected = true;
	}

	function openValuationModal(request, invoice, type, grandTotal, readOnly) {
		$uibModal.open({
			template: require('../valuation-modal-controller/valuation-modal-new.tpl.html'),
			controller: 'ValuationModalCtrl',
			size: 'lg',
			resolve: {
				request: () => request,
				invoice: () => invoice,
				type: () => type,
				grandTotal: () => grandTotal,
				readOnly: () => readOnly,
			}
		});
	}

	function fillEmptyFields(valuation, weight) {
		let activePlan = _.findIndex(settings.valuation_plan_settings, plan => plan.selected);

		valuation.selected = _.get(valuation, 'selected', {});
		valuation.settings = _.get(valuation, 'settings', {});
		valuation.other = _.get(valuation, 'other', {});
		valuation.deductible_data = _.get(valuation, 'deductible_data', {});

		if (isUnset(valuation.selected.liability_amount)) {
			if (valuation.lability_amount) {
				valuation.selected.liability_amount = valuation.lability_amount;
				delete valuation.lability_amount;
			} else {
				valuation.selected.liability_amount = weight * settings.valuation_cent_per_pound;
			}
		}

		if (isUnset(valuation.selected.deductible_level)) {
			if (valuation.deductable_level) {
				valuation.selected.deductible_level = valuation.deductable_level;
				delete valuation.deductable_level;
			} else {
				valuation.selected.deductible_level = 'Select Valuation';
			}
		}

		if (isUnset(valuation.selected.valuation_charge)) {
			if (valuation.valuation_charge) {
				valuation.selected.valuation_charge = valuation.valuation_charge;
				valuation.selected.valuation_type = VALUATION_TYPES.FULL_VALUE;
				delete valuation.valuation_charge;
			} else {
				valuation.selected.valuation_charge = 0;
			}
		}

		if (isUnset(valuation.selected.valuation_type)) valuation.selected.valuation_type = VALUATION_TYPES.PER_POUND;

		if (isUnset(valuation.settings.valuation_plan)) valuation.settings.valuation_plan = activePlan;
		if (isUnset(valuation.settings.updated)) valuation.settings.updated = moment().unix();

		if (isUnset(valuation.other.admonition)) valuation.other.admonition = false;
		if (isUnset(valuation.other.set_by_manager)) valuation.other.set_by_manager = false;
		if (isUnset(valuation.other.showWarning)) valuation.other.showWarning = false;
	}

	function isUnset(value) {
		return _.isUndefined(value) || _.isNull(value) || _.isNaN(value);
	}

	function saveRequestValuation(params, request, invoice, type) {
		let defer = $q.defer();

		let arr = makeMessage(params, request, invoice, type);

		RequestServices.saveReqData(request.nid, request.request_all_data)
			.then(() => {
				defer.resolve(arr);
			})
			.catch(err => {
				defer.reject(err);
			});

		return defer.promise;
	}

	function makeMessage(params, request, invoice, type) {
		let tabName = type == 'invoice' ? 'closing' : 'sales';
		if (angular.isDefined(invoice) && angular.isDefined(invoice.request_all_data) && request.status.raw == CONFIRM_STATUS) {
			invoice.request_all_data.valuation = request.request_all_data.valuation;
			request.request_all_data.invoice = invoice;
		}

		let message = '';
		if (params.valuation_type == VALUATION_TYPES.PER_POUND) {
			message = `Valuation was changed on ${tabName} tab. Released value(${params.title})`;
		} else if (params.valuation_type == VALUATION_TYPES.FULL_VALUE) {
			message = `Valuation was changed on ${tabName} tab. Deductible level: $${params.deductible_level}.
				 Valuation charge: $${params.valuation_charge}`;
		}

		return [{
			simpleText: message
		}];
	}

	function calculateValuation(params) {
		let result = {};
		let tableName = service.TABLE_NAMES[params.valuation_plan];
		let currentPlan = valuationPlans[tableName];
		let rowNumber = findTableRow(params.liability_amount, currentPlan);
		let tableRow = currentPlan.body[rowNumber].value;
		result.tableRow = tableRow;
		let charge = 0;

		if (params.valuation_type == VALUATION_TYPES.FULL_VALUE) {
			let tableRowIndex = _.indexOf(currentPlan.header, params.deductible_level);
			let tableRowCharge = +tableRow[tableRowIndex];

			if (params.valuation_plan == 0) {
				charge = tableRowCharge * +params.liability_amount / 100;
			} else if (params.valuation_plan == 1) {
				charge = tableRowCharge;
			} else if (params.valuation_plan == 2) {
				charge = tableRowCharge * +params.liability_amount / settings.valuation_rate_coefficient;
			}
		} else {
			charge = 0;
		}

		result.valuation_charge = _.round(charge, 2);

		return result;
	}

	function findTableRow(amountOfLiability, table) {
		let rowCount = table.body.length;
		let tableRow = table.body.reduce((selectedRow, currentRow, index) => {
			if (+amountOfLiability > +currentRow.name)
				return +index + 1;
			return selectedRow;
		}, 0);
		if (tableRow >= rowCount) tableRow = rowCount - 1;
		return tableRow;
	}

	function getSetting(name) {
		return angular.copy(settings[name]);
	}

	function checkValuationChanges(name) {
		return _.get(valuationChanges, name, false);
	}

	function setSetting(name, value) {
		settings[name] = value;
		valuationChanges[name] = true;
	}

	function updateSetting() {
		let defer = $q.defer();

		SettingServices.saveSettings(settings, SETTING_NAME)
			.then(() => {
				valuationChanges = {};
				defer.resolve();
			})
			.catch(error => {
				Raven.captureException(`valuation.service.js: ${error}`);
				defer.reject();
			});

		return defer.promise;
	}

	function saveValuationPlan(tables) {
		datacontext.saveFieldDataType(VALUATION_PLAN_NAME, tables);
		return apiService.postData(moveBoardApi.valuation.saveValuationPlan, tables);
	}

	function getValuationPlan() {
		return valuationPlans;
	}

	function recalculateValuationInRequest(request, weight) {
		weight *= POUND_COEFFICIENT;
		let valuation = request.request_all_data.valuation;
		service.fillEmptyFields(valuation, weight);

		if (valuation.selected.valuation_type === VALUATION_TYPES.PER_POUND || !valuation.change_liability_amount) {
			valuation.selected.liability_amount = calculateAmountOfLiability(weight);
		}

		let activeValuationPlanIndex = _.findIndex(settings.valuation_plan_settings, plan => plan.selected);
		let activeValuationPlan = valuationPlans[service.TABLE_NAMES[activeValuationPlanIndex]];
		let paramsForCalculating = {
			valuation_type: valuation.selected.valuation_type,
			liability_amount: valuation.selected.liability_amount,
			deductible_level: valuation.selected.deductible_level,
			valuation_plan: activeValuationPlanIndex,
			weight: weight,
		};

		request.request_all_data.valuation.selected.valuation_charge = calculateValuation(paramsForCalculating).valuation_charge;

		valuation.deductible_data.valuation_charge = activeValuationPlan.header.map(
			deductibleLevel => calculateValuation({
				valuation_type: valuation.selected.valuation_type,
				liability_amount: valuation.selected.liability_amount,
				deductible_level: deductibleLevel,
				valuation_plan: activeValuationPlanIndex,
				weight: weight,
			}, VALUATION_TYPES.FULL_VALUE).valuation_charge);
	}

	function reCalculateValuationOnContract(invoice, weight) {
		recalculateValuationInRequest(invoice, weight);
	}

	function reCalculateValuation(request, invoice, type) {
		let reCalculateValuationDefer = $q.defer();
		let weight = CalculatorServices.getTotalWeight(request).weight;
		recalculateValuationInRequest(request, weight);

		if (type == 'invoice') {
			invoice.request_all_data.valuation = request.request_all_data.valuation;
			request.request_all_data.invoice = invoice;
		} else {
			if (angular.isDefined(invoice) && angular.isDefined(invoice.request_all_data)) {
				invoice.request_all_data.valuation = request.request_all_data.valuation;
				request.request_all_data.invoice = invoice;
			}
		}

		RequestServices.saveReqData(request.nid, request.request_all_data)
			.then(() => {
				reCalculateValuationDefer.resolve();
			}, () => {
				reCalculateValuationDefer.reject();
			});

		return reCalculateValuationDefer.promise;
	}

	function calculateAmountOfLiability (weight) {
		const PRECISION = 2;
		return _.round(weight * settings.valuation_cent_per_pound, PRECISION);
	}

	function sendValuationLogs(text, nid) {
		RequestServices.sendLogs(text, 'Request was updated', nid, 'MOVEREQUEST');
	}

	function sendValuationNotification(text, nid) {
		let data = {
			notification: {
				text: text
			},
			node: {
				nid: nid
			}
		};

		let dataNotify = {
			type: _.get(enums, 'notification_types.CUSTOMER_UPDATED_VALUATION'),
			data: data,
			entity_id: nid,
			entity_type: _.get(enums, 'entities.MOVEREQUEST')
		};

		apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
	}
}
