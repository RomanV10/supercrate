const VALUATION_TABS = [
	{
		name: 'Plan',
		directiveTemplate: '<valuation-plan-settings></valuation-plan-settings>',
		active: true,
		icon: 'fa fa-table',
		unsavedChanges: false,
	},
	{
		name: 'Titles',
		directiveTemplate: '<valuation-titles></valuation-titles>',
		active: false,
		icon: 'fa fa-header',
		unsavedChanges: false,
	},
	{
		name: 'Show valuation options',
		directiveTemplate: '<valuation-show></valuation-show>',
		active: false,
		icon: 'fa fa-check',
		unsavedChanges: false,
	},
	{
		name: 'Show Protection On Account Page',
		directiveTemplate: '<valuation-account-show-settings></valuation-account-show-settings>',
		active: false,
		icon: 'fa fa-shield',
		unsavedChanges: false,
	},
	{
		name: 'Dollars per pound (Full Value Protection)',
		directiveTemplate: '<valuation-coefficient></valuation-coefficient>',
		active: false,
		icon: 'fa fa-calculator',
		unsavedChanges: false,
	},
];

export {
	VALUATION_TABS
};
