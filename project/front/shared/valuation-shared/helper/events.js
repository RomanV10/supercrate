const ACTIONS = {
	valuationUnsavedChanges: 'valuationUnsavedChanges',
	childrenHasChangesValuationAccountShowSettings: 'childrenHasChangesValuationAccountShowSettings',
	coefficientWasChanged: 'coefficientWasChanged',
	showValuationProposalToContact: 'showValuationProposalToContact',
};

export {
	ACTIONS
};
