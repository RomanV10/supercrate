const VALUATION_PLAN_SETTINGS = [
	{
		name: 'By percent',
		selected: true,
	},
	{
		name: 'By fixed price',
		selected: false,
	},
	{
		name: 'By rate',
		selected: false,
	}
];

const TABLE_NAMES = [
	'percentTable',
	'fixedPriceTable',
	'rateTable'
];

const REQUEST_SERVICE_TYPES = {
	'1': true,
	'2': true,
	'3': true,
	'4': true,
	'5': true,
	'6': true,
	'7': true,
	'8': true,
};

const VALUATION_TITLES = {
	1: {
		title: '60 cents per pound',
		explanation: '',
	},
	2: {
		title: 'Full Value Protection',
		explanation: '',
	},
};

const CAN_EDIT_AMOUNT_OF_VALUATION_ON_ACCOUNT = false;
const CAN_EDIT_AMOUNT_OF_LIABILITY = false;

const VALUATION_RESERVATION_PAGE_SETTING = {
	show: false,
	types: {
		'1': false,
		'2': false,
		'3': false,
		'4': false,
		'5': false,
		'6': false,
		'7': false,
		'8': false,
	}
};

const VALUATION_PROPOSAL_TO_CONTACT = '<h1>To get an insurance, please contact your relocation manager</h1>';

const VALUATION_ACCOUNT_SHOW_SETTING = {
	cent_per_pound: true,
	full_value_protection: false,
};

const CENT_PER_POUND = 0.6;

const VALUATION_RATE_COEFFICIENT = 1000;

const CONFIRMATION_TABLE_SHOW = false;

export {
	VALUATION_PLAN_SETTINGS,
	TABLE_NAMES,
	REQUEST_SERVICE_TYPES,
	VALUATION_TITLES,
	CAN_EDIT_AMOUNT_OF_VALUATION_ON_ACCOUNT,
	VALUATION_RESERVATION_PAGE_SETTING,
	VALUATION_PROPOSAL_TO_CONTACT,
	VALUATION_ACCOUNT_SHOW_SETTING,
	CENT_PER_POUND,
	VALUATION_RATE_COEFFICIENT,
	CONFIRMATION_TABLE_SHOW,
	CAN_EDIT_AMOUNT_OF_LIABILITY,
};
