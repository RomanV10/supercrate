'use strict';

let logsTextDictionary = {
	0: 'Log is undefined',
	logType: {
		1: 'Dispatch Crew',
		2: 'Dispatch Options',
		3: 'Contract page steps',
	},
	crewType: {
		0: '',
		1: '(Pickup) ',
		2: '(Delivery) ',
		3: 'Additional crew: ',
	},
	dispatchCrew: {
		actions: {
			1: 'Choose crew: ',
			2: 'Choose foreman: ',
			3: 'Choose helper: ',
			4: 'Add additional crew: ',
			5: 'Add switcher to foreman: ',
			6: 'Add switcher to helper: ',
			7: 'Change additional trucks: ',
			8: 'Add additional trucks: ',
			9: 'Change foreman: ',
			10: 'Change helper: ',
			11: 'Change switcher: ',
			12: 'Unassign Foreman: ',
			13: 'Unassign Helper: ',
			14: 'Unassign Foreman Swithcer: ',
			15: 'Change foreman switcher: ',
			16: 'Chagne helper switcher: ',
			17: 'Remove foreman switcher: ',
			18: 'Remvoe helper switcher: ',
			19: 'Unassign helper switcher: ',
			20: 'Unassign additional trucks: ',
			21: 'Unassign additional crew: ',
			22: 'Remvoe additional crew: ',
			23: 'Remove additional trucks: ',
			24: 'Change additional crew: ',
			25: 'Change crew: ',
			26: 'Remove crew: ',
			27: 'Unassign crew: ',
			28: 'Remove helper: ',
			29: 'Remove foreman: ',
		},
		1: 'Crew was assigned ',
		2: 'Crew was unassigned ',
		3: 'Crew was reassigned ',
		4: 'Crew was selected ',
	},
	dispatchOptions: {
	},
	contractPage: {
		payments: {
			1: 'Payment was received cash ',
			2: 'Payment was received credit card ',
			3: 'Payment was received check payment ',
			4: 'Payment was received custom payment '
		},
		contractSubmit: {
			1: 'Contract page was submitted ',
			2: 'Main page of Local Move contract was submitted ',
			3: 'Pick Up part of Flat Rate contract was submitted ',
			4: 'Delivery part of Flat Rate contract was submitted ',
			5: 'Main page of Flat Rate contract was submitted ',
			6: 'Main page of Long Distance contract was submitted ',
			7: 'Additional page of Local Move contract was submitted ',
			8: 'Additional page of Flat Rate contract was submitted ',
			9: 'Additional page of Long Distance contract was submitted ',
		}
	}
};

angular.module('log')
	.constant('logsTextDictionary', logsTextDictionary);
