'use strict';

let logsViewTextIds = {
	undefinedShortView: 0,
	logType: {
		dispatchCrew: 1,
		dispatchOptions: 2,
		contractPageSteps: 3,
	},
	crewType: {
		usual: 0,
		pickup: 1,
		delivery: 2,
		additionalCrew: 3,
	},
	dispatchCrew: {
		action: {
			chooseCrewTeam: 1,
			chooseForeman: 2,
			chooseHelper: 3,
			addAdditionalCrew: 4,
			addSwicherToForeman: 5,
			addSwicherToHelper: 6,
			changeAdditionalTruck: 7,
			addAdditionalTruck: 8,
			changeForeman: 9,
			changeHelper: 10,
			changeSwitcher: 11,
			unassignForeman: 12,
			unassignHelper: 13,
			unassignForemanSwitcher: 14,
			changeForemanSwicher: 15,
			changeHelperSwicher: 16,
			removeForemanSwitcher: 17,
			removeHelperSwitcher: 18,
			unassignHelperSwitcher: 19,
			unassignAdditionalTruck: 20,
			unassignAdditionalCrew: 21,
			removeAdditionalCrew: 22,
			removeAdditionalTruck: 23,
			changeAdditionalCrew: 24,
			changeCrewTeam: 25,
			removeCrewTeam: 26,
			unassignCrewTeam: 27,
			removeHelper: 28,
			removeForeman: 29,
		},
		assignCrew: 1,
		unassignCrew: 2,
		reassignCrew: 3,
		notAssignCrew: 4,
	},
	dispatchOptions: {
	},
	contractPage: {
		payments: {
			paymentWasReceivedCash: 1,
			paymentWasReceivedCreditCard: 2,
			paymentWasReceivedCheck: 3,
			paymentWasReceivedCustom: 4
		},
		contractSubmit: {
			contractPageWasSubmitted: 1,
			localMovesMainPageSubmitted: 2,
			flatRatePickUpSubmitted: 3,
			flatRateDeliverySubmitted: 4,
			flatRateMainPageSubmitted: 5,
			longDistanceMainPageSubmitted: 6,
			additionalPageLocalMoveSubmitted: 7,
			additionalPageFlatRateSubmitted: 8,
			additionalPageLongDistanceSubmitted: 9
		}
	}
};

angular.module('log')
	.constant('logsViewTextIds', logsViewTextIds);
