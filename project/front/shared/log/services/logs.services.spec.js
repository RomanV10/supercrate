describe('dispatch logs service', () => {
	const DAY = 'Thursday, June 28, 2018';

	let apiService;
	let logsService;
	let postDataResult = {data: {data: {}}};
	let postDataFake = {
		then(callback) {
			callback(postDataResult);
		}
	};

	beforeEach(inject((_apiService_, _logsService_) => {
		apiService = _apiService_;
		logsService = _logsService_;
		spyOn(apiService, 'postData').and
			.returnValue(postDataFake);
	}));

	describe('createLog', () => {
		it('should be called nid and data', () => {
			//given
			let expectedData = ['/server/move_team_logs/add_multi', { nid : 10, data : ['{}'] }];
			let data = {
				nid: 10,
				data: [{}],
			};
			//when
			logsService.createLog(data);

			//then
			expect(apiService.postData).toHaveBeenCalledWith(...expectedData);
		});
	});

	describe('getLogsByDay', () => {
		it('should retrieve logs by current day', () => {
			//given
			let expectedData = ['server/move_team_logs/get_by_date', {date_from: '2018-06-28 00:00:00', date_to: '2018-06-28 23:59:59', current_page: 1}];

			//when
			logsService.getLogsByDay(DAY, 1);

			//then
			expect(apiService.postData).toHaveBeenCalledWith(...expectedData);
		});

		it('should reject when date undefined', () => {
			//given
			let expectedResult = 'reject';
			let result;

			//when
			logsService.getLogsByDay(undefined, 1)
				.then(() => result = 'resolve', () => result = 'reject');
			$timeout.flush();

			//then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('getLogsByNid', () => {
		it('should retrieve logs by current day', () => {
			//given
			let nid = 1111;
			let expectedData = ['server/move_team_logs/get_by_nid', {nid: 1111, current_page: 1}];

			//when
			logsService.getLogsByNid(nid);

			//then
			expect(apiService.postData).toHaveBeenCalledWith(...expectedData);
		});
	});

	describe('getNewLogObject', () => {
		let request = {nid: 10, service_type: {raw: 1}};

		it('should return object with shortViewId 1', () => {
			//given
			let expectedShortViewId = 1;
			let newLog;

			//when
			newLog = logsService.getNewLogObject(request, expectedShortViewId);

			//then
			expect(newLog.data.shortViewID).toBe(expectedShortViewId);
		});

		it('should set pickup crew for flat rate', () => {
			//given
			let expectedCrewType = 1;
			request.service_type.raw = 5;
			let newLog;

			//when
			newLog = logsService.getNewLogObject(request);

			//then
			expect(newLog.data.crewType).toBe(expectedCrewType);
		});
	});
});
