'use strict';

angular.module('log')
	.factory('contractDispatchLogs', contractDispatchLogs);

/*@ngInject*/
function contractDispatchLogs(logsService, logsViewTextIds) {
	let _requestsInfo = {};
	let signatureLog;

	let service = {
		initRequestLog,
		assignLogs,
	};

	return service;

	function initRequestLog(request, factory, finance, mainPurposeId, logType) {
		if (!_.get(request, 'nid')) return;

		let currentRequest = request.source || request;

		if (_requestsInfo[request.nid]) {
			_requestsInfo[request.nid].actualRequest = currentRequest;
		} else {
			let requestInfo = {
				initialRequest: angular.copy(currentRequest),
				actualRequest: currentRequest,
				initialFactory: angular.copy(factory),
				actualFactory: factory,
				initialFinance: angular.copy(finance),
				actualFinance: finance,
				log: logsService.getNewLogObject(currentRequest, mainPurposeId, logType),
			};

			_requestsInfo[request.nid] = requestInfo;
		}
	}

	function assignLogs(request = {}, signature) {
		let nid = request.nid;
		if (!_.get(_requestsInfo, nid)) return;
		signatureLog = angular.copy(signature);

		let {initialRequest, actualRequest, initialFactory, actualFactory, initialFinance, actualFinance, log} = _requestsInfo[nid];
		actualRequest = request;

		_createLogs(initialRequest, actualRequest, initialFactory, actualFactory, initialFinance, actualFinance, log);
		_updateInitData(nid, actualRequest, actualFactory, actualFinance);
	}

	function _updateInitData(nid, actualRequest, actualFactory, actualFinance) {
		_requestsInfo[nid].initialRequest = angular.copy(actualRequest);
		_requestsInfo[nid].initialFactory = angular.copy(actualFactory);
		_requestsInfo[nid].initialFinance = angular.copy(actualFinance);
	}

	function _createLogs(initialRequest, actualRequest, initialFactory, actualFactory, initialFinance, actualFinance, log) {
		_createSignatureLog(log)
			.forEach(addSimpleLog);

		_createPaymentLog(initialRequest, actualRequest, log)
			.forEach(addSimpleLog);

		_createSubmitLog(initialRequest, actualRequest, initialFactory, actualFactory, log)
			.forEach(addSimpleLog);

		log.createNewLogs(logsViewTextIds.logType.contractPageSteps);

		function addSimpleLog(simpleLog) {
			log.addSimpleLog(simpleLog);
		}
	}

	function _createSignatureLog(log) {
		let logs = [];

		if (!_.isEmpty(signatureLog)) {
			logs.push({
				signatureNumber: ++signatureLog.signatureNumber,
				signatureRemoved: signatureLog.signatureRemoved
			});
		}

		signatureLog = {};

		if (logs.length) {
			log.setMainPurpose(logs[0].signatureNumber);
		}

		return logs;
	}

	function _createPaymentLog(initialRequest, actualRequest, log) {
		let logs = [];
		let paymentMethod;
		let receiptsExists = _.get(initialRequest, `receipts`) && _.get(actualRequest, `receipts`);

		if (receiptsExists && actualRequest.receipts.length > initialRequest.receipts.length) {
			let receipts = _.differenceBy(actualRequest.receipts, initialRequest.receipts, 'created');

			_.forEach(receipts, (receipt) => {
				if (receipt.payment_flag == 'contract') {
					let logObj = {};
					paymentMethod = _getPaymentMethod(receipt.payment_method);
					logObj.method = paymentMethod;

					logs.push(_getAmountPaymentLog(angular.copy(logObj), receipt));
					logs.push(_getIdPaymentLog(logObj, receipt));
				}
			});
		}

		if (logs.length) {
			log.setMainPurpose(paymentMethod);
		}

		return logs;
	}

	function _getPaymentMethod(type) {
		if (type == 'cash') {
			return logsViewTextIds.contractPage.payments.paymentWasReceivedCash;
		} else if (type == 'creditcard') {
			return logsViewTextIds.contractPage.payments.paymentWasReceivedCreditCard;
		} else if (type == 'check') {
			return logsViewTextIds.contractPage.payments.paymentWasReceivedCheck;
		} else {
			return logsViewTextIds.contractPage.payments.paymentWasReceivedCustom;
		}
	}

	function _getAmountPaymentLog(logObj, receipt) {
		logObj.amount = receipt.amount;
		return logObj;
	}

	function _getIdPaymentLog(logObj, receipt) {
		logObj.receiptId = receipt.id;
		return logObj;
	}

	function _createSubmitLog(initialRequest, actualRequest, initialFactory, actualFactory, log) {
		let logs = [];

		let submittedPages = [];
		_.forEach(actualFactory.additionalContractPages, (actualPage) => {
			let page = _.find(initialFactory.additionalContractPages, (initPage) =>
				initPage.page_id == actualPage.page_id && initPage.isSubmitted != actualPage.isSubmitted);

			if (!_.isUndefined(page)) {
				submittedPages.push(page);
			}
		});

		if (!_.isEmpty(submittedPages)) {
			let {mainContract} = _.find(actualFactory.tabs, (tab) => submittedPages[0].page_id === tab.id);
			let logObj = {};

			if (actualRequest.service_type.raw == 5) {
				logObj.textId = mainContract ? logsViewTextIds.contractPage.contractSubmit.flatRateMainPageSubmitted :
					logsViewTextIds.contractPage.contractSubmit.additionalPageFlatRateSubmitted;
			} else if (actualRequest.service_type.raw == 7) {
				logObj.textId = mainContract ? logsViewTextIds.contractPage.contractSubmit.longDistanceMainPageSubmitted :
					logsViewTextIds.contractPage.contractSubmit.additionalPageLongDistanceSubmitted;
			} else {
				logObj.textId = mainContract ? logsViewTextIds.contractPage.contractSubmit.localMovesMainPageSubmitted :
					logsViewTextIds.contractPage.contractSubmit.additionalPageLocalMoveSubmitted;
			}

			logs.push(logObj);
		}

		let isPickUpChange = initialFactory.isPickupSubmitted != actualFactory.isPickupSubmitted;

		if (initialFactory.isSubmitted != actualFactory.isSubmitted || isPickUpChange) {
			let logObj = {};

			if (actualRequest.service_type.raw == 5) {
				if (_.get(actualRequest, 'field_flat_rate_local_move.value')) {
					logObj.textId = logsViewTextIds.contractPage.contractSubmit.flatRateMainPageSubmitted;
				} else if (isPickUpChange) {
					logObj.textId = logsViewTextIds.contractPage.contractSubmit.flatRatePickUpSubmitted;
				} else {
					logObj.textId = logsViewTextIds.contractPage.contractSubmit.flatRateDeliverySubmitted;
				}
			} else if (actualRequest.service_type.raw == 7) {
				logObj.textId = logsViewTextIds.contractPage.contractSubmit.longDistanceMainPageSubmitted;
			} else {
				logObj.textId = logsViewTextIds.contractPage.contractSubmit.localMovesMainPageSubmitted;
			}

			logs.push(logObj);
		}

		if (logs.length) {
			log.setMainPurpose(logsViewTextIds.contractPage.contractSubmit.contractPageWasSubmitted);
		}

		return logs;
	}
}
