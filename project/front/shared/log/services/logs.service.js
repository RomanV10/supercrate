'use strict';

angular
	.module('log')
	.factory('logsService', logsService);

/* @ngInject */
function logsService(apiService, moveBoardApi, logsViewTextIds, $controller, $injector, $q) {
	const DAY = 'day';
	const DAY_FORMAT = 'YYYY-MM-DD HH:mm:ss';
	const DATE_FORMAT = 'dddd, MMMM DD, YYYY';

	return {
		createLog,
		getLogsByDay,
		getLogsByNid,
		getNewLogObject,
	};

	function createLog(log) {
		let jsonLogs = log.data.map(simpleLog => angular.toJson(simpleLog));

		let data = {
			nid: log.nid,
			data: jsonLogs,
		};

		return apiService.postData(moveBoardApi.dispatchLogs.createMultipleLogs, data);
	}

	function getLogsByDay(date, current_page = 1) {
		let moment = $injector.get('moment');

		if (date) {
			let currentDate = moment(date, DATE_FORMAT);
			let date_from = currentDate.startOf(DAY)
				.format(DAY_FORMAT);
			let date_to = currentDate.endOf(DAY)
				.format(DAY_FORMAT);
			let getByDateData = {date_from, date_to, current_page};

			return apiService.postData(moveBoardApi.dispatchLogs.getByDate, getByDateData)
				.then(parseLogsData);
		} else {
			return $q.reject();
		}
	}

	function getLogsByNid(nid, current_page = 1) {
		let data = {nid, current_page};

		return apiService.postData(moveBoardApi.dispatchLogs.getByRequest, data)
			.then(parseLogsData);
	}

	function parseLogsData({data}) {
		let logs = _.get(data, 'data.logs');
		if (logs) {
			data.data.logs.forEach((log) => {
				if (log.data) {
					log.data = angular.fromJson(log.data)
				}
			});
		}

		return data.data;
	}

	function getNewLogObject(request, mainPurposeId, logType) {
		let $scope = {nid: request.nid};
		let newLog = $controller('LogCreatorController', {$scope});
		let crewType = getCrewTypeByRequest(request);
		newLog.addMainPurpose(mainPurposeId, logType, crewType);

		return newLog;
	}

	function getCrewTypeByRequest(request) {
		let crewType = logsViewTextIds.crewType.usual;

		let isFlatRate = request.service_type.raw == 5;
		if (isFlatRate) crewType = logsViewTextIds.crewType.pickup;

		return crewType;
	}
}
