'use strict';

angular
	.module('log')
	.controller('LogCreatorController', LogCreatorController);

/* @ngInject */
function LogCreatorController($scope, logsService, logsViewTextIds, $q) {
	let vm = this;
	let textIds = logsViewTextIds;

	vm.nid = $scope.nid;
	vm.logsPackage = [];

	initEmptyData();

	vm.addMainPurpose = addMainPurpose;
	vm.setMainPurpose = setMainPurpose;
	vm.setLogType = setLogType;
	vm.setCrewType = setCrewType;
	vm.addSimpleLog = addSimpleLog;
	vm.addSuperAdminLog = addSuperAdminLog;
	vm.createNewLogs = createNewLogs;
	vm.makeEmptyLog = makeEmptyLog;
	vm.addAnotherLog = addAnotherLog;

	function addMainPurpose(id, logType, crewType) {
		setMainPurpose(id);
		setLogType(logType);
		setCrewType(crewType);
	}

	function setMainPurpose(id = textIds.undefinedShortView) {
		vm.data.shortViewID = id;
	}

	function setLogType(logType = textIds.logType.dispatchCrew) {
		vm.data.logType = logType;
	}

	function setCrewType(crewType = textIds.crewType.usual) {
		vm.data.crewType = crewType;
	}

	// textId, from, to, for
	function addSimpleLog(log) {
		if (_.isUndefined(log.from)) log.from = 0;
		if (_.isUndefined(log.to)) log.to = 0;
		if (_.isUndefined(log.for)) log.for = 0;

		vm.data.fullLogs.push(log);
	}

	function makeEmptyLog() {
		vm.data.fullLogs.length = 0;
	}

	//TODO do it later
	// textId, from, to
	function addSuperAdminLog() {
		//vm.data.superFullLogs.push(log);
	}

	function createNewLogs(logType) {
		if (isNotValidLog(vm) && _.isEmpty(vm.logsPackage)) {
			return $q.reject();
		}

		let isValidLog = isValidCurrentLog(vm);

		if (isValidLog) {
			let copyLog = angular.copy(vm.data);
			vm.logsPackage.push(copyLog);
		}

		let log = {
			nid: vm.nid,
			data: vm.logsPackage,
		};

		return logsService
			.createLog(log)
			.then(() => {
				vm.logsPackage.length = 0;
				initEmptyData(logType);
			});
	}

	function isValidCurrentLog(log) {
		return !isNotValidLog(log);
	}

	function isNotValidLog(log) {
		return !log.data.shortViewID || _.isEmpty(log.data.fullLogs);
	}

	function initEmptyData(logType) {
		vm.data = {
			crewType: textIds.crewType.usual,
			logType: logType || textIds.logType.dispatchCrew,
			shortViewID: textIds.undefinedShortView,
			fullLogs: [],
			superFullLogs: [],
		};
	}

	function addAnotherLog(log) {
		let isValidLog = isValidCurrentLog(log);

		if (isValidLog) {
			let copyLog = angular.copy(log.data);
			vm.logsPackage.push(copyLog);
			log.makeEmptyLog();
		}
	}
}
