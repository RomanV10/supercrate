describe('Log creator controller', () => {
	let $controller;
	let $scope;
	let controller;
	let logsService;

	function createNewEmptyLog() {
		 return $controller('LogCreatorController', {$scope});
	}

	beforeEach(inject((_$controller_, _logsService_) => {
		$controller = _$controller_;
		logsService = _logsService_;
		$scope = $rootScope.$new();
		$scope.nid = '10';
		controller = createNewEmptyLog();
	}));

	describe('when init', () => {
		it('controller should be defined', () => {
			expect(controller).toBeDefined();
		});

		it('createNewLogs to be defined', () => {
			expect(controller.createNewLogs).toBeDefined();
		});

		it('addMainPurpose to be defined', () => {
			expect(controller.addMainPurpose).toBeDefined();
		});

		it('addSimpleLog to be defined', () => {
			expect(controller.addSimpleLog).toBeDefined();
		});

		it('addSuperAdminLog to be defined', () => {
			expect(controller.addSuperAdminLog).toBeDefined();
		});

		it('setMainPurpose to be defined', () => {
			expect(controller.setMainPurpose).toBeDefined();
		});

		it('setLogType to be defined', () => {
			expect(controller.setLogType).toBeDefined();
		});

		it('setCrewType to be defined', () => {
			expect(controller.setCrewType).toBeDefined();
		});

		it('makeEmptyLog to be defined', () => {
			expect(controller.makeEmptyLog).toBeDefined();
		});

		it('addAnotherLog', () => {
			expect(controller.addAnotherLog).toBeDefined();
		});

		it('data to be defined', () => {
			//given
			let exeptedData = {logType: 1, crewType: 0, shortViewID: 0, fullLogs: [], superFullLogs: []};

			//when

			//then
			expect(controller.data).toEqual(exeptedData);
		});

		it('nid to be defined', () => {
			expect(controller.nid).toBeDefined();
		});
	});

	describe('createNewLogs', () => {
		it('logsService createLog to have been called', () => {
			//given
			spyOn(logsService, 'createLog').and
				.returnValue({then() {}});
			controller.data.fullLogs = [{}];
			controller.data.shortViewID = 1;

			//when
			controller.createNewLogs();

			//then
			expect(logsService.createLog).toHaveBeenCalled();
		});

		it('when purpose don\'t selected shouldn\'t call create Log', () => {
			//given
			spyOn(logsService, 'createLog').and
				.returnValue({then() {}});
			controller.data.fullLogs = [{}];
			controller.data.shortViewID = 0;

			//when
			controller.createNewLogs();

			//then
			expect(logsService.createLog).not
				.toHaveBeenCalled();
		});

		describe('when empty logs', () => {
			it('should not call createLog', () => {
				//given
				spyOn(logsService, 'createLog').and
					.returnValue({then() {}});
				controller.data.fullLogs = [];
				controller.data.shortViewID = 1;

				//when
				controller.createNewLogs();

				//then
				expect(logsService.createLog).not
					.toHaveBeenCalled();
			});

			it('should return reject promise', () => {
				//given
				controller.data.fullLogs = [];
				controller.data.shortViewID = 0;
				let result = 'hello';
				let expectedResult = 'rejected';

				//when
				controller.createNewLogs()
					.then(() => {},
						() => {
							result = 'rejected';
						});
				$timeout.flush();

				//then
				expect(result).toEqual(expectedResult);
			});

			it('and not empty logsPackage should call createLog', () => {
				//given
				spyOn(logsService, 'createLog').and
					.returnValue({then() {}});
				controller.data.fullLogs = [];
				controller.data.shortViewID = 1;
				controller.logsPackage = [{}];

				//when
				controller.createNewLogs();

				//then
				expect(logsService.createLog).toHaveBeenCalled();
			});
		});
	});

	describe('addMainPurpose', () => {
		describe('should change', () => {
			let purposeId = 101;
			let logType = 1;
			let crewType = 2;

			beforeEach(() => {
				controller.addMainPurpose(purposeId, logType, crewType);
			});

			it('should change main purpose', () => {
				expect(controller.data.shortViewID).toBe(purposeId);
			});

			it('should change log type', () => {
				expect(controller.data.logType).toBe(logType);
			});

			it('should change crew type', () => {
				expect(controller.data.crewType).toBe(crewType);
			});
		});

		describe('by default should', () => {
			beforeEach(() => {
				controller.addMainPurpose();
			});

			it('main purpose by default should be 0', () => {
				//given
				let expectedResult = 0;

				//when

				//then
				expect(controller.data.shortViewID).toBe(expectedResult);
			});

			it('log type by default should be 1', () => {
				//given
				let expectedResult = 1;

				//when

				//then
				expect(controller.data.logType).toBe(expectedResult);
			});

			it('crew type by default should be 0', () => {
				//given
				let expectedResult = 0;

				//when

				//then
				expect(controller.data.crewType).toBe(expectedResult);
			});
		});
	});

	describe('setMainPurpose', () => {
		it('should set 0 by default', () => {
			//given
			let expectedResult = 0;

			//when
			controller.setMainPurpose();

			//then
			expect(controller.data.shortViewID).toBe(expectedResult);
		});

		it('should change main puprose', () => {
			//given
			let expectedResult = 110;

			//when
			controller.setMainPurpose(expectedResult);

			//then
			expect(controller.data.shortViewID).toBe(expectedResult);
		});
	});

	describe('setLogType', () => {
		it('should set 1 by default', () => {
			//given
			let expectedResult = 1;

			//when
			controller.setLogType();

			//then
			expect(controller.data.logType).toBe(expectedResult);
		});

		it('should change log type', () => {
			//given
			let expectedResult = 10;

			//when
			controller.setLogType(expectedResult);

			//then
			expect(controller.data.logType).toBe(expectedResult);
		});
	});

	describe('setCrewType', () => {
		it('should set 0 by default', () => {
			//given
			let expectedResult = 0;

			//when
			controller.setCrewType();

			//then
			expect(controller.data.crewType).toBe(expectedResult);
		});

		it('should change crew type', () => {
			//given
			let expectedResult = 5;

			//when
			controller.setCrewType(expectedResult);

			//then
			expect(controller.data.crewType).toBe(expectedResult);
		});
	});

	describe('addSimpleLog', () => {
		it('should add log to full logs', () => {
			//given
			let expectedResult = [{textId: 1, from: 0, to: 10, for: 0}];

			//when
			controller.addSimpleLog({textId: 1, from: 0, to: 10});

			//then
			expect(controller.data.fullLogs).toEqual(expectedResult);
		});

		it('should init log.from if undefined', () => {
			//given
			let expectedResult = 0;

			//when
			controller.addSimpleLog({textId: 1, to: 0});

			//then
			expect(controller.data.fullLogs[0].from).toEqual(expectedResult);
		});

		it('should init log.to if undefined', () => {
			//given
			let expectedResult = 0;

			//when
			controller.addSimpleLog({textId: 1, from: 0});

			//then
			expect(controller.data.fullLogs[0].to).toEqual(expectedResult);
		});
	});

	describe('makeEmptyLog', () => {
		it('should make empty log', () => {
			//given
			let expectedResult = 0;
			controller.data.fullLogs.push({});

			//when
			controller.makeEmptyLog();

			//then
			expect(controller.data.fullLogs.length).toEqual(expectedResult);
		});
	});

	describe('addAnotherLog', () => {
		let additionalLog;

		beforeEach(() => {
			additionalLog = createNewEmptyLog();
		});

		describe('when valid', () => {
			it('should add log', () => {
				//given
				let expectedResult = [{ crewType : 0, logType : 1, shortViewID : 1, fullLogs : [ { textId : 10, from : 0, to : 0, for : 0 } ], superFullLogs : [  ]}];
				additionalLog.setMainPurpose(1);
				additionalLog.addSimpleLog({textId: 10});

				//when
				controller.addAnotherLog(additionalLog);

				//then
				expect(controller.logsPackage).toEqual(expectedResult);
			});

			it('should clear added log', () => {
				//given
				additionalLog.setMainPurpose(1);
				additionalLog.addSimpleLog({textId: 10});
				spyOn(additionalLog, 'makeEmptyLog');

				//when
				controller.addAnotherLog(additionalLog);

				//then
				expect(additionalLog.makeEmptyLog).toHaveBeenCalled();
			});
		});

		describe('when not valid', () => {
			it('should not add empty log', () => {
				//given
				let expectedResult = [];

				//when
				controller.addAnotherLog(additionalLog);

				//then
				expect(controller.logsPackage).toEqual(expectedResult);
			});
		});
	});
});
