describe('multiple user filter', () => {
	let filter;
	let result;
	let expectedResult;

	beforeEach(() => {
		filter = $filter('erMultipleUser');
	});

	it('when init, should be defined', () => {
		expect(filter).toBeDefined();
	});

	describe('should return', () => {
		describe('empty string,', () => {
			it('when no parameter', () => {
				//given
				expectedResult = '';

				//when
				result = filter();

				//then
				expect(result).toBe(expectedResult);
			});

			it('when "0"', () => {
				//given
				expectedResult = '';
				let countUser = '0';

				//when
				result = filter(countUser);

				//then
				expect(result).toBe(expectedResult);
			});

			it('when 0', () => {
				//given
				expectedResult = '';
				let countUser = 0;

				//when
				result = filter(countUser);

				//then
				expect(result).toBe(expectedResult);
			});
		});

		it('1 person when parameter is 1', () => {
			//given
			expectedResult = '1 person';
			let countUser = 1;

			//when
			result = filter(countUser);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('2 people when parameter is 2', () => {
			//given
			expectedResult = '2 people';
			let countUser = 2;

			//when
			result = filter(countUser);

			//then
			expect(result).toEqual(expectedResult);
		});
	});
});