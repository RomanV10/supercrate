'use strict';

angular
	.module('er-multiple-filters')
	.filter('erMultipleUser', erMultipleUser);

function erMultipleUser() {
	return count => {
		let result = '';

		let isNumberStringMoreZero = !_.isEmpty(count) && +count;
		let isNumberMoreZero = _.isNumber(count) && count;
		if (isNumberStringMoreZero || isNumberMoreZero) {
			result = count > 1 ? count + ' people' : '1 person';
		}

		return result;
	};
}