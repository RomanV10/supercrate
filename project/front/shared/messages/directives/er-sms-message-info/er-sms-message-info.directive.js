import './er-sms-message-info.styl';

angular.module('er-messages')
	.directive('erSmsMessageInfo', erSmsMessageInfo);

/* @ngInject */
function erSmsMessageInfo() {
	return {
		restrict: 'E',
		template: require('./er-sms-message-info.html'),
	};
}
