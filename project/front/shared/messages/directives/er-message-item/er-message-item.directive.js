import './er-message-item.styl';

angular.module('er-messages')
	.directive('erMessageItem', erMessageItem);

/* @ngInject */
function erMessageItem() {
	return {
		restrict: 'E',
		template: require('./er-message-item.html'),
		link,
	};

	function link($scope) {

	}
}
