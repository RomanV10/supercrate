import './er-messages-thread.styl';

angular.module('er-messages')
	.directive('erMessagesThread', erMessagesThread);

/* @ngInject */
function erMessagesThread() {
	return {
		restrict: 'E',
		template: require('./er-messages-thread.html'),
		link,
	};

	function link($scope) {
		$scope.isEmpty = _.isEmpty;
		let isAccount = IS_ACCOUNT;
		$scope.isAccount = isAccount;

		$scope.readMessageInThread = readMessageInThread;
		$scope.isNewMessage = isNewMessage;
		$scope.isCanEditMessage = isCanEditMessage;
		$scope.isLeftSide = isLeftSide;

		function readMessageInThread(message) {
			if (isNewMessage(message)) {
				$scope.readMessage(message);
				message.read = 1;
			}
		}

		function isNewMessage(message) {
			if (isAccount) {
				return !message.read && message.admin;
			}

			return !message.read && !message.admin;
		}

		function isCanEditMessage(message) {
			return !message.sms && message.admin;
		}

		function isLeftSide(message) {
			return isAccount ? message.admin : !message.admin ;
		}
	}
}
