import './import.styl';
import './source.styl';

function requireAll(requireContext) {
	return requireContext.keys().map(requireContext);
}

require('./app/app.module.js');
require('./app/core/core.module.js');
require('./app/data/data.module.js');
require('./app/blocks/logger/logger.module.js');
require('./app/blocks/router/router.module.js');
require('./app/dashboard/dashboard.module.js');
require('./app/layout/layout.module.js');

require('./app/lddispatch/lddispatch.import.js');

requireAll(require.context('./app/search/', true, /\.\/.*\module.js$/));
requireAll(require.context('./app/search/', true, /^\.\/.*\.js$/));

requireAll(require.context('./app/reminders/', true, /\.\/.*\module.js$/));
requireAll(require.context('./app/reminders/', true, /^\.\/(?!.*spec).*\.js$/));

requireAll(require.context('./app/dialogs/', true, /\.\/.*\module.js$/));
requireAll(require.context('./app/dialogs/', true, /^\.\/.*\.js$/));

requireAll(require.context('./app/slidePanel/', true, /\.\/.*\module.js$/));
requireAll(require.context('./app/slidePanel/', true, /^\.\/.*\.js$/));

//   <!-- Requests Module -->

require('./app/requests/source');

// <!-- Messages Module -->

requireAll(require.context('./app/messages/', true, /\.\/.*\module.js$/));
requireAll(require.context('./app/messages/', true, /^\.\/.*\.js$/));
require('../shared/messages');

require('./app/core/hrmin.js');

require('./app/newInventories/newInventories.source.js');

// <!--Mail module -->
require('./app/mails/mails.module.js');
require('./app/mails/config.route.js');
require('./app/mails/mails.controller.js');
require('./app/mails/directives/mail-list.js');
require('./app/mails/directives/free.drag.js');
require('./app/mails/services/mail.service.factory.js');
require('./app/mails/services/mail.service.rest.js');

// <!--Templae builder module -->
require('./app/template-builder/templateBuilder.module.js');
require('./app/template-builder/config.route.js');
require('./app/template-builder/templateBuilder.controller.js');
require('./app/template-builder/blockBuilder.controller.js');
require('./app/template-builder/services/auth.service.js');
require('./app/template-builder/services/emailBuilderRequest.service.rest.js');
require('./app/template-builder/services/templateFolder.service.js');
require('./app/template-builder/services/templateBuilder.service.js');
require('./app/template-builder/services/text-ng-tmpl-custom.service.js');
require('./app/template-builder/services/mainBlock.service.js');

require('./app/template-builder/services/styleExecutor.service.js');

require('./app/template-builder/services/modalEditElement.service.js');
require('./app/requests/services/requestHelper.service.js');
require('./app/template-builder/services/editElement.service.js');
require('./app/template-builder/directives/ng-enter.directive.js');
require('./app/template-builder/directives/preview-block.directive.js');
require('./app/template-builder/directives/block.directive.js');
require('./app/template-builder/directives/template.directive.js');
require('./app/template-builder/directives/finishRender.directive.js');
require('./app/template-builder/directives/styleEditor.directive.js');
require('./app/template-builder/directives/styleExecutor.directive.js');
require('./app/template-builder/directives/file-reader.directive.js');
require('./app/template-builder/directives/template-preview.directive.js');

// <!-- Sending email directive -->
require('./app/template-builder/sending-email/sending-emails.directive.js');
require('./app/template-builder/services/sendEmail.service.js');

// <!-- Scedule Module -->
require('./app/schedule');

// <!-- Statistics Module -->
require('./app/statistics/statistics.module.js');
require('./app/statistics/config.route.js');
require('./app/statistics/statisticsController.js');
require('./app/statistics/controllers/quick-statistics.controller.js');
require('./app/statistics/controllers/sales-statistics.controller.js');
require('./app/statistics/services/quick-info-statistics.service.js');
require('./app/statistics/services/sales-statistics.service.js');
require('./app/statistics/profit-and-lose/controller/profitLoseController.js');
require('./app/statistics/directives/statistics.directive.js');
require('./app/statistics/directives/quick-info-statistics.directive.js');
require('./app/statistics/directives/sales-statistics.directive.js');
require('./app/statistics/profit-and-lose/directives/profit/profit.directive.js');
require('./app/statistics/profit-and-lose/directives/loses/loses.directive.js');
require('./app/statistics/profit-and-lose/services/statisticService.js');
require('./app/statistics/profit-and-lose/controller/expense-modal-instance.controller.js');
require('./app/statistics/profit-and-lose/controller/view-expense-modal-instance.controller.js');
require('./app/statistics/services/configStatistic.js');
require('./app/statistics/paymentsCollected/');

require('./app/dispatch');


require('./app/storages/storages.module.js');
require('./app/storages/config.route.js');
require('./app/storages/storage/storageController.js');
require('./app/storages/invoices/storage-invoice.controller.js');
require('./app/storages/storage/services/storageService.js');
require('./app/storages/pending/pendingController.js');
require('./app/storages/pending/directives/days.js');
require('./app/storages/pending/directives/tabs.js');
require('./app/storages/pending/directives/ledger.directive.js');
require('./app/storages/pending/directives/storages.documents.directive.js');
require('./app/storages/pending/directives/lotNumber.directive.js');
require('./app/storages/tenants/tenantsController.js');
require('./app/storages/pending/services/paymentStorage.service.js');
require('./app/storages/aging/agingController.js');
require('./app/storages/aging/directives/storageAging.directive.js');
require('./app/storages/storage/storage-log/storage-logs.directive.js');

require('./app/storages/recurring-revenue/RecurringRevenueController.js');
require('./app/storages/payment-collected/paymentCollectedController.js');
require('./app/storages/storage/controller/create-storage-modal.controller.js');
require('./app/storages/storage/controller/change-request.controller');

require('./app/account/account.module.js');
require('./app/account/config.route.js');
require('./app/account/areas/login/login.controllers.js');
require('./app/account/areas/restore-password/restore-password.directive.js');

require('./app/inventories/inventories.module.js');
require('./app/inventories/config.route.js');
require('./app/inventories/controllers/inventories.controller.js');


require('./app/layout/foreman/foreman.module.js');
require('./app/layout/foreman/config.route.js');
require('./app/layout/foreman/foremanController.js');
require('./app/layout/foreman/SpecificUsersService.js');
require('./app/layout/foreman/foreman-mobile-search.service.js');
require('./app/layout/foreman/foreman-mobile.directive.js');
require('./app/layout/foreman/foreman.payment.service.js');
require('./app/layout/directive/logo/branch-logo.directive.js');
require('./app/layout/directive/ngRightClick/ngRightClick.directive.js');
require('./app/layout/directive/ngRightClick/ngRightClick.controller.js');

require('./app/layout/directive/ngMiddleClick/ngMiddleClick.directive.js');


require('./app/inventories/directives/list-inventories/listInventories.directive.js');
require('./app/inventories/directives/table-inventories/tableInventories.directive.js');
require('./app/inventories/services/inventories.service.js');
require('./app/inventories/services/editinventory.service.js');


require('./app/clients/clients.module.js');
require('./app/clients/config.route.js');
require('./app/clients/clientsdetail.js');
require('./app/clients/clients.js');
require('./app/clients/sevices/clients.services.js');
require('./app/clients/directives/clientEdit.directive.js');
require('./app/clients/controllers/update-multiple-requests-user.controller.js');
require('./app/clients/controllers/change-user.controller.js');
require('./app/clients/directives/history.directive.js');


require('./app/settings/department/department.module.js');
require('./app/settings/department/config.route.js');
require('./app/settings/department/departmentController.js');
require('./app/settings/department/services/department.constant');
require('./app/settings/department/services/department.services.js');
require('./app/settings/department/directives/createUser.directive.js');
require('./app/settings/department/directives/editWorker.directive.js');
require('./app/settings/department/directives/department-google-integration/google-calendar-integration.directive');
require('./app/settings/department/directives/foremanReceiptsTypes.js');
require('./app/settings/department/controller/create-user.controller.js');
require('./app/settings/department/directives/arrivy-integration/arrivy-integration.directive.js');
require('./app/settings/department/directives/foreman-photo/foreman-photo.directive.js');


require('./app/calculator/calc.module.js');
require('./app/calculator/sevices/calculate.services.js');
require('./app/calculator/sevices/packing-serivce/calculate-packing.service');
require('./app/calculator/directive/calcForm.directive.js');
require('./app/calculator/directive/zipCode.directive.js');

require('./app/core/shared/services/sharedRequest.services.js');


require('./app/settings/setting.module.js');
require('./app/settings/config.route.js');


require('./app/settings/calculator/config.route.js');
require('./app/settings/calculator/calculator.controller.js');
require('./app/settings/calculator/ngRule/ngRule.directive.js');
require('./app/settings/services/settings.services.js');


require('./app/commercial-move-calculator/commercial-move.services/commercial-move.module.js');
require('./app/commercial-move-calculator/commercial-move-settings.component/commercial-move-calc.directive.js');
require('./app/commercial-move-calculator/commercial-move-request.component/commercial-move-sizes.request.directive.js');
require('./app/commercial-move-calculator/commercial-move.services/commercial-move-calc.service.js');
require('./app/commercial-move-calculator/new-commercial-size.component/new-commercial-size.controller.js');
require('./app/commercial-move-calculator/commercial-custom-name.component/commercial-custom-name.component.js');

require('./app/requests/directives/invoices/invoice.directive.js');
require('./app/requests/directives/invoices/invoiceTable.directive');
require('./app/requests/directives/invoices/invoice.services.js');

require('./app/requests/directives/datatable-invoice/source.js');

require('../account/app/requestpage/services/request.services');
require('./app/common/services/init-request/InitRequest.service');
require('./app/common/services/CheckRequest.service');
require('./app/common/services/InitSettings.service');
require('./app/common/services/momentWrapper.services');
require('./app/common/services/fuel-surcharge/calculate-fuel-surcharge.service');

require('./app/support/config.route.js');
require('./app/support/support.module.js');
require('./app/support/chat.room/chatroomController.js');
require('./app/support/talkto/talkto.directive.js');

require('./app/settings/general');
require('./app/settings/sms');
require('./app/settings/sms-template-builder');

require('./app/requests/directives/payment/payment.directive.js');
require('./app/requests/directives/payment/payment.services.js');
require('./app/requests/directives/payment/controllers/payment-controllers.source');
require('./app/core/shared/directives/fullscreen.directives.js');




// Long Distance
require('./app/settings/longdistance/config.route.js');
require('./app/settings/longdistance/long.distance.controller.js');
require('./app/settings/longdistance/services/longDistance.services.js');

// Long Distance Directives
require('./app/settings/longdistance/directives/list-states.js');
require('./app/settings/longdistance/directives/list-based.js');
require('./app/settings/longdistance/directives/list-print.js');
require('./app/settings/longdistance/directives/long-map.js');
require('./app/settings/longdistance/directives/long-extra.js');
require('./app/settings/longdistance/directives/long-notice.js');
require('./app/settings/longdistance/directives/long-editor.js');
require('./app/settings/longdistance/directives/long-status.js');
require('./app/settings/longdistance/directives/long-print-settings.js');
require('./app/settings/longdistance/directives/long-driver/long-driver.directive.js');
require('./app/settings/longdistance/directives/long-distance-discount');


require('./app/settings/schedule/config.route.js');
require('./app/settings/schedule/schedule.controller.js');
require('./app/tooltip/tooltip.js');
require('./app/config.route.js');
require('./app/constants/events');
require('./app/constants/dispatch-settings-initial.contants');

require('./app/settings/accountPage/account-page-settings.controller.js');
require('./app/settings/accountPage/controllers/greetint-example.controller.js');
require('./app/settings/accountPage/controllers/greeting-show-modal.controller.js');
require('./app/requests/directives/request-settings/request.account-page-settings.directive.js');
require('./app/requests/directives/request-settings/er-choose-list/erChooseList.directive.js');
require('./app/requests/directives/edit-request-page/edit-request-page.directive.js');
require('./app/requests/directives/edit-request-page/edit-request-page.controller');
require('./app/requests/directives/edit-request-page-base/edit-request-page-base.controller.js');

require('./app/statistics/directives/reviews.directive.js');
require('./app/statistics/directives/reviews-statistics.directive.js');
require('./app/statistics/directives/great-reviews.directive.js');
require('./app/statistics/services/reviews.service.js');
require('./app/statistics/controllers/modals/review-settings.modal.controller.js');

require('./app/statistics/directives/chart-tab/statistic-chart-tab.directive.js');
require('./app/statistics/directives/statistics-tables/statistic-tables.directive.js');

requireAll(require.context('./app/settings/notifications/', true, /^\.\/.*\.js$/));
//requireAll(require.context("./app/settings/leadScoring/", true, /^\.\/.*\.js$/));

require('./app/settings/leadScoring/directives/lead-scoring-rules/lead-scoring-rules.directive');
require('./app/settings/accountPage/directives/progress-bar-rules/progress-bar-rules.directive');
require('./app/settings/accountPage/directives/progress-bar-example/progress-bar-example.directive');
require('./app/settings/leadScoring/config.route');

require('./app/settings/inventory/inventory.source.js');

require('./app/settings/accountPage/directives/flat-rate-settings.directive.js');
require('./app/settings/accountPage/directives/extra-signature/extra-signatures.directive');
require('./app/settings/accountPage/directives/extra-signature/extra-signature-settings-item/extra-signature-settings-item.directive');
require('./app/settings/accountPage/directives/extra-signature/extra-signature-settings-template-builder-menu/extra-signature-settings-template-builder-menu.directive');
require('./app/settings/accountPage/directives/extra-signature/extra-signature-settings-template-builder-menu/extra-signature-settings-template-builder-menu-modal.controller');
require('../shared/signatures');

require('./app/requests/directives/request-notes/request-notes.directive.js');
require('./app/fileUploader/fileUploader.js');

require('./app/mobile-request/config.route.js');
require('./app/mobile-request/mobileRequest.controller.js');
require('./app/mobile-request/config.route.js');
require('./app/mobile-request/mobileRequest.controller.js');

require('./app/dashboard/score-filter/score-filter.module.js');
require('./app/dashboard/score-filter/directives/score-filter.directive.js');

require('./app/lead-score-input/lead-score-input.module.js');
require('./app/lead-score-input/lead-score-input.directive.js');

require('./app/blocks/logger/logger.js');


require('./app/blocks/router/routehelper.js');
require('./app/core/common.js');
require('./app/core/shared/services/session.services.js');
require('./app/core/shared/services/authentication.services.js');
require('./app/core/shared/services/permissions.services.js');
require('./app/core/filters.js');
require('./app/core/session-injector/session-injector.service');
require('./app/core/config.js');
require('./app/core/shared/directives/templatedirectives.js');


require('./app/core/shared/directives/autoGrow.directives.js');

require('./app/data/datacontext.js');
require('./app/layout/shell.js');

require('./app/layout/sidebar.js');
require('./app/dashboard/config.route.js');
require('./app/dashboard/dashboard.js');

require('./app/settings/department/controller/DepartmentControllerCtrl.js');

require('./app/elromcoNotificationsCenter/directives/notifications/elromcoNotificationsCenter.js');

require('./app/request-views-buttons/request-views.module');
require('./app/request-views-buttons/directives/request-view-button.directive');
require('./app/request-views-buttons/directives/request-multiple-button.directive');
require('./app/request-views-buttons/services/request-settings-helper.service');

require('./app/inhome-estimator-portal/connect-module-files.js');

requireAll(require.context('./app/settings/accountPage', true, /^\.\/(?!.*spec).*\.js$/));
requireAll(require.context('./app/payments/', true, /\.\/.*\module.js$/));
requireAll(require.context('./app/payments/', true, /^\.\/.*\.js$/));
requireAll(require.context('./app/services/', true, /^\.\/(?!.*spec).*\.js$/));
requireAll(require.context('./app/home-estimate-schedule/', true, /^\.\/.*\.js$/));
requireAll(require.context('./app/notificationsSources/', true, /^\.\/.*\.js$/));
requireAll(require.context('./app/moment/', true, /^\.\/.*\.js$/));

require('./app/elromcoDots/elromcoDots');
require('./app/requests-show-quote/request-show-quote.source');

require('../shared/import');
require('../shared/common-directives/er-saving-progress/er-saving-progress.directive');

require('./app/requests/services/request-observable.service.js');

require('./app/pending-info/pending-info.source');
require('./app/valuation/valuation.source');

require('./app/requests/modals/send-request-to-sit-modal/send-request-to-sit-modal.controller');
require('./app/customer-online');
require('./app/requests/directives/speedy-inventory-icon/speedy-inventory-icon.directive');
require('./app/inventory-details-settings');
require('./app/services/poll-sources/poll-sources.service');
require('../shared/common-directives/er-saving-progress/er-saving-progress.directive');
require('./app/services/only-positive-number/only-positive-number.filter.js');
