module.exports = function (environment) {
	environment
		.use(['jasmine'])
		// add vendor dependency
		.add([
				'vendor/vendor/jquery-2.1.1.min.js',
				'dist/app.import.js',
				'./../../../node_modules/angular-mocks/angular-mocks.js',
				'vendor/plugins/editors/textAngular-rangy.min.js',
				'vendor/plugins/editors/textAngular-sanitize.js',
				'vendor/plugins/editors/textAngular.js',
				'vendor/plugins/editors/textAngularSetup.js',
				'vendor/plugins/editors/textAngular-dropdownToggle.js',

				'vendor/angular-dragula/angular-dragula.js',
				'vendor/plugins/switchery/switchery.min.js',
				'vendor/angular-masonry/angular-masonry.js',
				'vendor/plugins/dataTables/js/jquery.dataTables.js',
				'vendor/plugins/dataTables/js/dataTables.bootstrap.js',
				'vendor/lodash/lodash.js',
				'vendor/plugins/momentjs/moment.js',
				'vendor/momentjs/momentjs-business.js',
				'vendor/toastr/toastr.js',
			]
		)

		// add source dependency
		.add([
			'../../../inline.js',
			'dist/app.test.js',
			'app/core/shared/directives/templatedirectives.js',

			// required project files for tests
			'app/core/constants.js'
		]);
};
