module.exports = function (config) {
	config.set({

		// base path that will be used to resolve all patterns (eg. files, exclude)
		basePath: '',


		// frameworks to use
		// available frameworks: https://npmjs.org/browse/keyword/karma-adapter
		frameworks: ['jasmine'],


		// list of files / patterns to load in the browser
		files: [
			'vendor/vendor/jquery-2.1.1.min.js',
			'dist/app.import.js',
			'./../../../node_modules/angular-mocks/angular-mocks.js',
			'vendor/plugins/editors/textAngular-rangy.min.js',
			'vendor/plugins/editors/textAngular-sanitize.js',
			'vendor/plugins/editors/textAngular.js',
			'vendor/plugins/editors/textAngularSetup.js',
			'vendor/plugins/editors/textAngular-dropdownToggle.js',
			'vendor/toastr/toastr.js',
			'vendor/angular-dragula/angular-dragula.js',
			'vendor/plugins/switchery/switchery.min.js',
			'vendor/angular-masonry/angular-masonry.js',
			'vendor/plugins/dataTables/js/jquery.dataTables.js',
			'vendor/plugins/dataTables/js/dataTables.bootstrap.js',
			'vendor/lodash/lodash.js',
			'vendor/plugins/momentjs/moment.js',
			'vendor/momentjs/momentjs-business.js',
			{pattern: 'app/**/*.png', watched: false, included: false, served: true},
			'app/**/*.spec.js',
			{pattern: 'app/**/*.mock.json'},

			// require for test
			'../../../inline.js',
			'dist/app.test.js',
			'app/core/shared/directives/templatedirectives.js',

			// required project files for tests
			'app/core/constants.js',
		],


		// list of files to exclude
		exclude: [],


		// preprocess matching files before serving them to the browser
		// available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
		preprocessors: {
			'app/**/*.mock.json': ['json_fixtures']
		},

		plugins: [
			'karma-jasmine',
			'karma-chrome-launcher',
			'karma-fixture',
			'karma-html2js-preprocessor',
			'karma-json-fixtures-preprocessor'
		],
		jsonFixturesPreprocessor: {
			// strip this from the file path \ fixture name
			stripPrefix: '.+mock-data/',
			variableName: 'mocks',
			camelizeFilenames: false,
		},

		// test results reporter to use
		// possible values: 'dots', 'progress'
		// available reporters: https://npmjs.org/browse/keyword/karmareporter
		reporters: ['progress'],


		// web server port
		port: 9876,


		// enable / disable colors in the output (reporters and logs)
		colors: true,


		// level of logging
		// possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		logLevel: config.LOG_INFO,


		// enable / disable watching file and executing tests whenever any file changes
		autoWatch: true,


		// start these browsers
		// available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
		browsers: ['Chrome'],


		// Continuous Integration mode
		// if true, Karma captures browsers, runs the tests and exits
		singleRun: false,

		// Concurrency level
		// how many browser should be started simultaneous

		concurrency: Infinity,

		customLaunchers: {
			MyHeadlessChrome: {
				base: 'Chrome',
				flags: ['--disable-translate', '--disable-extensions', '--remote-debugging-port=9223', '--headless']
			}
		},
	});
};
