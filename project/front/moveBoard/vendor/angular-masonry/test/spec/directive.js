(function() {
  describe('angular-masonry', function() {
    var controllerProvider;
    controllerProvider = null;
    beforeEach(module('wu.masonry'));
    beforeEach(module(function(_$controllerProvider_) {
      controllerProvider = _$controllerProvider_;
      return null;
    }));
    beforeEach(inject((function(_this) {
      return function($rootScope) {
        _this.scope = $rootScope.$new();
        return $.fn.masonry.reset();
      };
    })(this)));
    it('should initialize', inject((function(_this) {
      return function($compile) {
        var element;
        element = angular.element('<masonry></masonry>');
        return element = $compile(element)(_this.scope);
      };
    })(this)));
    it('should call masonry on init', inject((function(_this) {
      return function($compile) {
        var element;
        element = angular.element('<div masonry></div>');
        element = $compile(element)(_this.scope);
        return expect($.fn.masonry).toHaveBeenCalled();
      };
    })(this)));
    it('should pass on the column-width attribute', inject((function(_this) {
      return function($compile) {
        var call, element;
        element = angular.element('<masonry column-width="200"></masonry>');
        element = $compile(element)(_this.scope);
        expect($.fn.masonry).toHaveBeenCalledOnce();
        call = $.fn.masonry.firstCall;
        return expect(call.args[0].columnWidth).toBe(200);
      };
    })(this)));
    it('should pass on the item-selector attribute', inject((function(_this) {
      return function($compile) {
        var call, element;
        element = angular.element('<masonry item-selector=".mybrick"></masonry>');
        element = $compile(element)(_this.scope);
        expect($.fn.masonry).toHaveBeenCalled();
        call = $.fn.masonry.firstCall;
        return expect(call.args[0].itemSelector).toBe('.mybrick');
      };
    })(this)));
    it('should pass on any options provided via `masonry-options`', inject((function(_this) {
      return function($compile) {
        var call, element;
        element = angular.element('<masonry masonry-options="{ isOriginLeft: true }"></masonry>');
        element = $compile(element)(_this.scope);
        expect($.fn.masonry).toHaveBeenCalled();
        call = $.fn.masonry.firstCall;
        return expect(call.args[0].isOriginLeft).toBeTruthy();
      };
    })(this)));
    it('should pass on any options provided via `masonry`', inject((function(_this) {
      return function($compile) {
        var call, element;
        element = angular.element('<div masonry="{ isOriginLeft: true }"></div>');
        element = $compile(element)(_this.scope);
        expect($.fn.masonry).toHaveBeenCalled();
        call = $.fn.masonry.firstCall;
        return expect(call.args[0].isOriginLeft).toBeTruthy();
      };
    })(this)));
    it('should setup a $watch when the reload-on-show is present', inject((function(_this) {
      return function($compile) {
        var element;
        sinon.spy(_this.scope, '$watch');
        element = angular.element('<masonry reload-on-show></masonry>');
        element = $compile(element)(_this.scope);
        return expect(_this.scope.$watch).toHaveBeenCalled();
      };
    })(this)));
    it('should not setup a $watch when the reload-on-show is missing', inject((function(_this) {
      return function($compile) {
        var element;
        sinon.spy(_this.scope, '$watch');
        element = angular.element('<masonry></masonry>');
        element = $compile(element)(_this.scope);
        return expect(_this.scope.$watch).not.toHaveBeenCalled();
      };
    })(this)));
    it('should setup a $watch when the reload-on-resize is present', inject((function(_this) {
      return function($compile) {
        var element;
        sinon.spy(_this.scope, '$watch');
        element = angular.element('<masonry reload-on-resize></masonry>');
        element = $compile(element)(_this.scope);
        return expect(_this.scope.$watch).toHaveBeenCalledWith('masonryContainer.offsetWidth', sinon.match.func);
      };
    })(this)));
    it('should not setup a $watch when the reload-on-resize is missing', inject((function(_this) {
      return function($compile) {
        var element;
        sinon.spy(_this.scope, '$watch');
        element = angular.element('<masonry></masonry>');
        element = $compile(element)(_this.scope);
        return expect(_this.scope.$watch).not.toHaveBeenCalledWith('masonryContainer.offsetWidth', sinon.match.func);
      };
    })(this)));
    describe('MasonryCtrl', (function(_this) {
      return function() {
        beforeEach(inject(function($controller, $compile) {
          _this.element = angular.element('<div></div>');
          return _this.ctrl = $controller('MasonryCtrl', {
            $scope: _this.scope,
            $element: _this.element
          });
        }));
        it('should not remove after destruction', function() {
          _this.ctrl.destroy();
          _this.ctrl.removeBrick();
          return expect($.fn.masonry).not.toHaveBeenCalled();
        });
        return it('should not add after destruction', function() {
          _this.ctrl.destroy();
          _this.ctrl.addBrick();
          return expect($.fn.masonry).not.toHaveBeenCalled();
        });
      };
    })(this));
    describe('masonry-brick', (function(_this) {
      return function() {
        beforeEach(function() {
          var self;
          self = _this;
          _this.addBrick = sinon.spy();
          _this.removeBrick = sinon.spy();
          _this.scheduleMasonry = sinon.spy();
          _this.scheduleMasonryOnce = sinon.spy();
          return controllerProvider.register('MasonryCtrl', function() {
            this.addBrick = self.addBrick;
            this.removeBrick = self.removeBrick;
            this.scheduleMasonry = self.scheduleMasonry;
            this.scheduleMasonryOnce = self.scheduleMasonryOnce;
            return this;
          });
        });
        it('should register an element in the parent controller', inject(function($compile) {
          var element;
          element = angular.element('<masonry>\n  <div class="masonry-brick"></div>\n</masonry>');
          element = $compile(element)(_this.scope);
          return expect(_this.addBrick).toHaveBeenCalledOnce();
        }));
        return it('should remove an element in the parent controller if destroyed', inject(function($compile) {
          var element;
          _this.scope.bricks = [1, 2, 3];
          element = angular.element('<masonry>\n  <div class="masonry-brick" ng-repeat="brick in bricks"></div>\n</masonry>');
          element = $compile(element)(_this.scope);
          _this.scope.$digest();
          _this.scope.$apply(function() {
            return _this.scope.bricks.splice(0, 1);
          });
          expect(_this.addBrick).toHaveBeenCalledThrice();
          return expect(_this.removeBrick).toHaveBeenCalledOnce();
        }));
      };
    })(this));
    return describe('masonry-brick internals', (function(_this) {
      return function() {
        beforeEach(function() {
          return $.fn.imagesLoaded = function(cb) {
            return cb();
          };
        });
        afterEach(function() {
          return delete $.fn.imagesLoaded;
        });
        it('should append three elements to the controller', inject(function($compile) {
          var element;
          element = angular.element('<masonry>\n  <div class="masonry-brick"></div>\n  <div class="masonry-brick"></div>\n  <div class="masonry-brick"></div>\n</masonry>');
          element = $compile(element)(_this.scope);
          _this.scope.$digest();
          return expect($.fn.masonry.callCount).toBe(2 + 3);
        }));
        it('should prepend elements when specified by attribute', inject(function($compile) {
          var element;
          element = angular.element('<masonry>\n  <div class="masonry-brick" prepend="{{true}}"></div>\n</masonry>');
          element = $compile(element)(_this.scope);
          _this.scope.$digest();
          return expect($.fn.masonry.calledWith('prepended', sinon.match.any, sinon.match.any)).toBe(true);
        }));
        it('should append before imagesLoaded when preserve-order is set', inject(function($compile) {
          var element, imagesLoadedCb;
          element = angular.element('<masonry preserve-order>\n  <div class="masonry-brick"></div>\n</masonry>');
          imagesLoadedCb = void 0;
          $.fn.imagesLoaded = function(cb) {
            return imagesLoadedCb = cb;
          };
          element = $compile(element)(_this.scope);
          _this.scope.$digest();
          return expect($.fn.masonry.calledWith('appended', sinon.match.any, sinon.match.any)).toBe(true);
        }));
        it('should call layout after imagesLoaded when preserve-order is set', inject(function($compile, $timeout) {
          var element, imagesLoadedCb;
          element = angular.element('<masonry preserve-order>\n  <div class="masonry-brick"></div>\n</masonry>');
          imagesLoadedCb = void 0;
          $.fn.imagesLoaded = function(cb) {
            return imagesLoadedCb = cb;
          };
          element = $compile(element)(_this.scope);
          _this.scope.$digest();
          expect($.fn.masonry.calledWith('layout', sinon.match.any, sinon.match.any)).toBe(false);
          imagesLoadedCb();
          $timeout.flush();
          return expect($.fn.masonry.calledWith('layout', sinon.match.any, sinon.match.any)).toBe(true);
        }));
        it('should append before imagesLoaded when load-images is set to "false"', inject(function($compile) {
          var element, imagesLoadedCb;
          element = angular.element('<masonry load-images="false">\n  <div class="masonry-brick"></div>\n</masonry>');
          imagesLoadedCb = void 0;
          $.fn.imagesLoaded = function(cb) {
            return imagesLoadedCb = cb;
          };
          element = $compile(element)(_this.scope);
          _this.scope.$digest();
          return expect($.fn.masonry.calledWith('appended', sinon.match.any, sinon.match.any)).toBe(true);
        }));
        return it('should call layout before imagesLoaded when load-images is set to "false"', inject(function($compile, $timeout) {
          var element, imagesLoadedCb;
          element = angular.element('<masonry load-images="false">\n  <div class="masonry-brick"></div>\n</masonry>');
          imagesLoadedCb = void 0;
          $.fn.imagesLoaded = function(cb) {
            return imagesLoadedCb = cb;
          };
          element = $compile(element)(_this.scope);
          _this.scope.$digest();
          $timeout.flush();
          return expect($.fn.masonry.calledWith('layout', sinon.match.any, sinon.match.any)).toBe(true);
        }));
      };
    })(this));
  });

}).call(this);
