function requireAll(requireContext) {
	return requireContext.keys().map(requireContext);
}

requireAll(require.context('./app/', true, /\.\/.*\module.js$/));
requireAll(require.context('./app/services/', true, /^\.\/.*\.js$/));

requireAll(require.context('./app/reminders/', true, /^\.\/(?!.*spec).*\.js$/));
requireAll(require.context('./app/clients/', true, /^\.\/(?!.*spec).*\.js$/));

require('../shared/import');
require('./app/core/shared/services/authentication.services.js');
require('./app/core/shared/services/session.services.js');

require('./app/requests/services/dataRequests.services.js');
require('./app/data/datacontext.js');
require('./app/core/common.js');
require('./app/common/services/momentWrapper.services.js');

require('./app/dialogs/dialogsProvider.js');

require('../shared/shared-api/constants/apiConstants.js');
require('./app/constants/dispatch-settings-initial.contants');

require('./app/storages/invoices/storage-invoice.controller.js');

require('./app/calculator/sevices/calculate.services.js');

require('./app/statistics/services/quick-info-statistics.service.js');
require('./app/requests/services/modalEditrequest.services.js');
require('./app/payments/services/payment.service.js');
require('./app/blocks/logger/logger.js');

require('./app/mails/services/mail.service.factory.js');
require('./app/mails/services/mail.service.rest.js');

require('./app/template-builder/services/auth.service.js');
require('./app/template-builder/services/emailBuilderRequest.service.rest.js');
require('./app/template-builder/services/templateFolder.service.js');
require('./app/template-builder/services/templateBuilder.service.js');

require('./app/template-builder/services/styleExecutor.service.js');

require('./app/template-builder/services/modalEditElement.service.js');
require('./app/requests/services/requestHelper.service.js');
require('./app/template-builder/services/editElement.service.js');

require('./app/template-builder/services/sendEmail.service.js');
require('./app/template-builder/services/mainBlock.service.js');

require('./app/schedule/services/schedule.services.js');
require('./app/schedule/services/scheduleHelper.js');
require('./app/schedule/services/scheduleConfig.js');
require('./app/schedule/services/shceduleTitle.js');

require('./app/statistics/services/sales-statistics.service.js');

require('./app/statistics/profit-and-lose/services/statisticService.js');
require('./app/statistics/services/configStatistic.js');

require('./app/dispatch/local.dispatch/services/dispatchService.rest.js');
require('./app/dispatch/local-dispatch-logs/directives/dispatch-logs/dispatch-logs.controller');
require('./app/dispatch/local-dispatch-logs/directives/dispatch-logs/dispatch-logs.directive');
require('./app/dispatch/local-dispatch-logs/services/dispatch-logs-dispatcher/dispatch-logs-dispatcher.service');
require('./app/dispatch/local-dispatch-logs/filters/short-view-dispatch-logs/short-view-dispatch-logs.filter');
require('./app/dispatch/local-dispatch-logs/filters/full-view-dispatch-logs/full-view-dispatch-logs.filter');
require('./app/dispatch/local-dispatch-logs/controllers/dispatch-log-full-view/dispatch-log-full-view.controller.js');
require('./app/dispatch/local-dispatch-logs/directives/dispatch-logs-full-view-list/dispatch-logs-full-view-list.directive');
require('./app/dispatch/local-dispatch-logs/directives/dispatch-logs-full-view-item/dispatch-logs-full-view-item.directive');
require('./app/dispatch/local-dispatch-logs/directives/dispatch-logs-full-view-item/dispatch-logs-full-view-item.controller');
require('./app/dispatch/local-dispatch-logs/services/dispatch-logs-users/dispatch-logs-users.service');
require('./app/dispatch/local-dispatch-logs/services/dispatch-logs-changes-detector-foreman/dispatch-logs-changes-detector-foreman.service');
require('./app/dispatch/local-dispatch-logs/services/dispatch-logs-changes-detector-helper/dispatch-logs-changes-detector-helper.service');
require('./app/dispatch/local-dispatch-logs/services/dispatch-logs-changes-detector-switcher/dispatch-logs-changes-detector-switcher.service');
require('./app/dispatch/local-dispatch-logs/services/dispatch-logs-changes-detector-additional-crew/dispatch-logs-changes-detector-additional-crew.service');
require('./app/dispatch/local-dispatch-logs/services/dispatch-logs-changes-detector-crews/dispatch-logs-changes-detector-crews.service');

require('./app/dispatch/payroll/services/payrollService.rest.js');
require('./app/dispatch/payroll/services/payrollConfigs.js');
require('./app/dispatch/payroll/services/payroll.builder.js');

require('./app/storages/storage/services/storageService.js');

require('./app/storages/pending/services/paymentStorage.service.js');

require('./app/inventories/services/inventories.service.js');
require('./app/inventories/services/editinventory.service.js');

require('./app/clients/sevices/clients.services.js');

require('./app/settings/department/services/department.constant');
require('./app/settings/department/services/department.services.js');

require('./app/core/shared/services/sharedRequest.services.js');

require('./app/settings/services/settings.services.js');

require('./app/commercial-move-calculator/commercial-move.services/commercial-move.module.js');
require('./app/commercial-move-calculator/commercial-move.services/commercial-move-calc.service.js');

require('../account/app/requestpage/services/request.services');
require('./app/common/services/init-request/InitRequest.service');
require('./app/common/services/CheckRequest.service');
require('./app/common/services/InitSettings.service');
require('./app/common/services/momentWrapper.services');
require('./app/common/services/fuel-surcharge/calculate-fuel-surcharge.service');

require('./app/requests/directives/payment/payment.services.js');

require('./app/settings/longdistance/services/longDistance.services.js');
require('./app/settings/longdistance/directives/long-distance-discount');

require('./app/statistics/services/reviews.service.js');

require('./app/core/shared/services/session.services.js');
require('./app/core/shared/services/authentication.services.js');
require('./app/core/shared/services/permissions.services.js');

require('./app/requests/directives/invoices/invoice.services.js');
require('./app/requests/services/parser.services.js');

require('./app/requests/services/modalNewRequest.services.js');
require('./app/requests/services/calcRequest.services.js');
require('./app/requests/directives/new-log/newLogService.js');

require('./app/lddispatch/config.route.js');

require('./app/request-views-buttons/directives/request-view-button.directive.js');

require('./app/commercial-move-calculator/commercial-move-settings.component/commercial-move-calc.directive.js');

require('./app/settings/department/directives/department-google-integration/google-calendar-integration.directive.js');
require('./app/settings/department/controller/DepartmentControllerCtrl.js');
require('./app/settings/department/directives/editWorker.directive.js');
require('./app/settings/department/directives/arrivy-integration/arrivy-integration.directive');

require('./app/settings/leadScoring/directives/lead-scoring-rules/lead-scoring-rules.directive.js');
require('./app/settings/accountPage/directives/progress-bar-rules/progress-bar-rules.directive.js');


require('./app/requests/directives/form-inputs/source');
require('./app/lead-score-input/lead-score-input.directive.js');

require('./app/newInventories/newInventories.source.js');
require('./app/settings/inventory/directives/inventorySetting/inventorySetting.js');

require('./app/clients/directives/clientEdit.directive.js');

require('./app/settings/accountPage/directives/account-faq/account-faq-settings.directive.js');

require('./app/services/er-only-float/er-only-float.directive.js');

require('./app/storages/storage/controller/create-storage-modal.controller.js');
require('./app/storages/storage/controller/change-request.controller');

require('./app/requests/directives/datatable-invoice/datatableInvoice.directive.js');

require('./app/inhome-estimator-portal/connect-module-files.js');

require('./app/elromcoNotificationsCenter/directives/notifications/elromcoNotificationsCenter.js');
require('./app/notificationsSources/notificationsSources.js');
require('../shared/import');

require('./app/requests/directives/form-inputs/new-form-inputs/source.js');
require('./app/requests/services/form-inputs.services.js');

require('./app/lddispatch/trip-planner/tpDelivery/tp-delivery.directive.js');
require('./app/lddispatch/directives/routeHistoryService/routerTracker.js');
require('./app/lddispatch/services/states-service.directive.js');
require('./app/requests/services/edit-request-calculator/edit-request-calculator.service');

require('./app/valuation/valuation.source');
require('./app/tests-common/test-helper.service.js');
require('./app/statistics/paymentsCollected');
require('./app/requests/directives/speedy-inventory-icon/speedy-inventory-icon.directive');
require('./app/customer-online');
require('./app/settings/general');
require('./app/inventory-details-settings');
require('./app/dispatch/dispatch-crews');
require('./app/requests-show-quote/directives/request-show-quote.directive.js');
require('./app/requests/services/open-request/open-request.service');
require('./app/requests/services/er-date-parser/er-date-parser.service');
require('./app/dispatch/local.dispatch/directives/request-dispatch-notes/request-dispatch-notes.controller.js');

require('./app/dispatch/local.dispatch/directives/day-dispatch-notes/day-dispatch-notes.controller');
require('./app/dispatch/local.dispatch/services/day-dispatch-notes/day-dispatch-notes.service');