require("@babel/polyfill");

require('angular');
require('angular-stripe');
require('angular-animate');
require('angular-material');
require('md-date-range-picker');
require('angular-material-data-table');
require('angular-image-compress');
require('angular-multiple-select');
require('ng-file-upload');
require('angular-ui-bootstrap');
require('angular-debounce');

require('ui-cropper');
require('interactjs');
require('./vendor/angular-fullscreen/angular-fullscreen.js');
require('./vendor/plugins/jquery-caret/jquery.caret-1.5.2.min.js');
require('./vendor/localstorage/angular-local-storage.min.js');
require('angular-material-icons');
require('../account/vendor/plugins/dropzone/js/dropzone.min.js');
require('../account/vendor/ng-flow/ng-flow-standalone.min.js');
require('./vendor/plugins/navgoco/jquery.navgoco.min.js');
// import jQuery from "jquery";

require('./vendor/ngPrint/ngPrint.js');
require('./vendor/angular-print/angularPrint.js');
require('./vendor/angular/angular-messages.min.js');
require('./vendor/angular/angular-route.min.js');
require('./vendor/ui-router/angular-ui-router.min.js');
require('./vendor/angular-permissions/angular-permission.js');
require('./vendor/plugins/chartjs/tc-angular-chartjs.min.js');
require('./vendor/plugins/growl/angular-growl-notifications.min.js');
require('./vendor/plugins/dataTables/js/angular-datatables.js');
require('./vendor/plugins/dataTables/js/angular-datatables.columnfilter.js');
require('./vendor/plugins/ui-tree/angular-ui-tree.min.js');
require('./vendor/angular-filters/filters.min.js');
require('./vendor/ng-lightbox/angular-bootstrap-lightbox.min.js');
require('./vendor/ng-draggable/ngDraggable.js');
require('./vendor/angular-credit-cards/release/angular-credit-cards.js');

require('./vendor/sweet-alert/sweetalert.min.js');
require('./vendor/sweet-alert/SweetAlert');
require('./vendor/angular-minicolors-master/angular-minicolors.js');
require('./vendor/ng-google-maps/angular-simple-logger.light.min.js');
require('./vendor/ng-google-maps/angular-google-maps.min.js');

require('./vendor/plugins/editors/textAngular-sanitize.js');
require('./vendor/plugins/editors/textAngular.js');
require('./vendor/plugins/editors/textAngular-dropdownToggle.js');

require('./vendor/ng-loading/ngLoading.js');
require('./vendor/checklist/checklist-model.js');
require('./vendor/ng-mask/ng-currency.js');
require('./vendor/ng-mask/ngMask.js');
require('./vendor/plugins/switchery/ui-switchery.js');
require('./vendor/contextMenu/contextMenu.js');
require('./vendor/plugins/ui-events/event.js');
require('angular-bootstrap-calendar');

require('angular-moment');

require('oi.select');

require('./vendor/plugins/editors/bootstrap-colorpicker-module.min.js');
require('./vendor/angular-file-saver/angular-file-saver.bundle.min.js');
require('lodash');
