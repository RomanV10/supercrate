angular
	.module('app.core')
	.filter('searchfilter', function ($sce) {
		return function (input, query = '') {
			let onlyText = (query.match(/\w+/g) || [])
				.map((regText) => `(${regText})`)
				.join(' ');
			let r = new RegExp(onlyText, 'gi');
			let result = false;

			if (angular.isDefined(input)) {
				if (typeof input == 'object') {
					input.forEach(function (item) {
						item.replace(r, '<span class="super-class">$&</span>');
					});

					result = input;
				} else {
					result = (input + '').replace(r, '<span class="super-class">$&</span>');
				}
			}

			return result;
		};
	})
	.filter('ifSubstring', function () {
		return function (string, substring) {
			let result = string.indexOf(substring) !== -1;

			return result;
		};
	})
	.filter('tel', function () {
		return function (tel) {
			if (!tel) {
				return '';
			}

			let value = tel.toString().trim().replace(/^\+/, '');

			if (value.match(/[^0-9]/)) {
				return tel;
			}

			let country, city, number;

			switch (value.length) {
				case 10: // +1PPP####### -> C (PPP) ###-####
					country = 1;
					city = value.slice(0, 3);
					number = value.slice(3);
					break;

				case 11: // +CPPP####### -> CCC (PP) ###-####
					country = value[0];
					city = value.slice(1, 4);
					number = value.slice(4);
					break;

				case 12: // +CCCPP####### -> CCC (PP) ###-####
					country = value.slice(0, 3);
					city = value.slice(3, 5);
					number = value.slice(5);
					break;

				default:
					return tel;
			}

			if (country == 1) {
				country = '';
			}

			number = number.slice(0, 3) + '-' + number.slice(3);

			return (country + ' (' + city + ') ' + number).trim();
		};
	})
	.filter('reverse', function () {
		return function (items) {
			return items.slice().reverse();
		};
	})
	.filter('usatime', function () {
		return function (time) {
			let amtime;

			if (time > 12) {
				amtime = time - 12;

				if (amtime == 12) {
					return '12AM';
				}

				return amtime + 'PM';
			} else {
				if (time == 12) {
					return '12PM';
				}

				return time + 'AM';
			}
		};
	})
	.filter('range', function () {
		return function (input, total) {
			total = parseInt(total);

			for (let i = 0; i < total; i++) {
				input.push(i);
			}

			return input;
		};
	})
	.filter('decToTime', function () {
		return function (input) {
			let hrs = parseInt(Number(input));
			let min = Math.round((Number(input) - hrs) * 60);

			if (hrs < 1) {
				return min + ' min';
			} else {
				return hrs + ' h : ' + min + ' min';
			}
		};
	})
	.filter('numToTime', function () {
		return function (input) {
			let zero = '0';
			let zero2 = '0';
			let hrs = parseInt(Number(input));
			let fraction = Number(input) - hrs;
			let min = 0;

			// 0, 15min - 0.25 , 30min - 0.5 , 45 min - 0.75.
			if (fraction > 0.15 && fraction < 0.36) {
				min = 15;
			}

			if (fraction >= 0.36 && fraction < 0.64) {
				min = 30;
			}

			if (fraction >= 0.64 && fraction < 0.87) {
				min = 45;
			}

			if (fraction >= 0.87) {
				min = 0;
				hrs++;
			}

			if (hrs > 9) {
				zero = '';
			}

			if (min > 9) {
				zero2 = '';
			}

			if (min == 0) {
				return zero + hrs + ':00';
			} else if (hrs == 0) {
				return '00:' + min;
			} else {
				return zero + hrs + ':' + zero2 + min;
			}
		};
	})
	.filter('ago', function () {
		return function (input) {
			let date = moment.unix(input).utc();

			return moment(date).fromNow();
		};
	})
	.filter('dateRange', function () {
		return function (dates) {
			let dateFormat = 'd M, yy';

			if (dates) {
				let deli;

				if (!angular.isNumber(dates))
					deli = dates.split(',');
				else {
					deli = [];
					deli[0] = dates;
					deli[1] = dates;
				}

				let c1 = $.datepicker.formatDate(dateFormat, new Date(Math.min(deli[0], deli[1])), {});
				let c2 = $.datepicker.formatDate(dateFormat, new Date(Math.max(deli[0], deli[1])), {});

				if (c1 != c2) {
					return c1 + ' - ' + c2;
				} else {
					return c1;
				}
			}
		};
	})
	.filter('momentDate', function () {
		return function (date, format) {
			date = moment.unix(date);

			return date.format(format);
		};
	})
	.filter('filterObj', function () {
		return function (obj) {
			if (!(obj instanceof Object)) return obj;

			return _.map(obj, function (val, key) {
				return Object.defineProperty(val, '$key', {__proto__: null, value: key});
			});
		};
	})
	.filter('toArray', function () {
		return function (obj) {
			let result = [];

			angular.forEach(obj, function (val) {
				result.push(val);
			});

			return result;
		};
	});
