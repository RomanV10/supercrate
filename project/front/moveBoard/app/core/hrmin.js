(function () {
    'use strict';

    angular
        .module('app.core')
        .filter('hrmin', function() {
            return function(number) {

                var zero = '0';
                var zero2 = '0';
                var minutes = 0;
                var hour = Math.floor(number);
                var fraction =number - hour;

                // 0, 15min - 0.25 , 30min - 0.5 , 45 min - 0.75
                if(fraction > 0.15 && fraction < 0.36)
                {
                    minutes = 15;
                }
                if(fraction >= 0.36 && fraction < 0.64)
                {
                    minutes = 30;
                }
                if(fraction >= 0.64 && fraction < 0.87)
                {
                    minutes = 45;
                }
                if(fraction >= 0.87)
                {
                    minutes = 0;
                    hour++;
                }


                if(minutes == 0 && hour != 1)
                    return hour+' hrs';
                else if(hour == 1 && minutes == 0)
                    return hour+' hr';
                else if(hour == 1 && minutes != 0)
                    return hour+' hr '+minutes+' min';
                else if(hour == 0)
                    return minutes+' min';
                else
                    return hour+' hrs '+minutes+' min';



            };
        });
})();