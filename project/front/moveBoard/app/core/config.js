'use strict';

const GOOD_STATUS_CODE = 200;
const NOT_AUTHORIZED_STATUS_CODE = 403;

angular
	.module('app.core')
	.config(configure);

/*@ngInject*/
function configure($routeProvider, $httpProvider, routehelperConfigProvider) {
	const TOASTR_TIMEOUT = 3500;
	configureRouting();

	function configureRouting() {
		var routeCfg = routehelperConfigProvider;
		routeCfg.config.$routeProvider = $routeProvider;
	}

	$httpProvider.interceptors.push([
		'$q', function ($q) {
			return {
				'response': function (response) {
					checkNotAutorizeStatus(response);

					let isBadResponse = _.get(response, 'data.status_code', GOOD_STATUS_CODE) !== GOOD_STATUS_CODE;

					if (isBadResponse) {
						let data = _.get(response, 'data', response);
						return $q.reject(data);
					}

					return response;
				},
				'responseError': function (response) {
					checkNotAutorizeStatus(response);

					return $q.reject(response);
				}
			};
		}
	]);

	function checkNotAutorizeStatus(response) {
		let isNotAuthorized = _.get(response, 'data.status_code') === NOT_AUTHORIZED_STATUS_CODE
			|| _.get(response, 'data.data.status') === NOT_AUTHORIZED_STATUS_CODE
			|| _.get(response, 'status') === NOT_AUTHORIZED_STATUS_CODE;

		if (isNotAuthorized) {
			location.assign(location.protocol + '//' + location.host + location.pathname);
		}
	}

	$httpProvider.interceptors.push('sessionInjector');
	$httpProvider.defaults.withCredentials = true;

	toastr.options.timeOut = TOASTR_TIMEOUT;
}
