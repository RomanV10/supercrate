angular
	.module('app.core')
	.factory('sessionInjector', sessionInjector);

/*@ngInject*/
function sessionInjector(Session) {
	var sessionInjector = {
		request: function (config) {
			config.headers['X-CSRF-Token'] = Session.getToken();
			return config;
		}
	};

	return sessionInjector;
}
