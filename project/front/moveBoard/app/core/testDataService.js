//import frontpage from '../tests-common/mock-data/frontpage-mock.json';
//import getcurrent from '../apiBackend/server/clients/getcurrent.json';
import get_dashboard from '../apiBackend/server/move_request/get_dashboard.json';
import unread_count from '../apiBackend/server/move_request_comments/unread_count.json';
import get_variable from '../apiBackend/server/system/get_variable.json';
import template_builder from '../apiBackend/server/template_builder.json';
import retrieve_blocks_base from '../apiBackend/server/template_builder/retrieve_blocks_base.json';
import get_flags from '../apiBackend/server/move_request/get_flags.json';
import set_variable from '../apiBackend/server/system/set_variable.json';
import login from '../apiBackend/server/move_users_resource/login.json';
import long_distance_carrier from '../apiBackend/server/ldDispatch/long_distance_carrier.json';
//import googleCalendarIntegration from '../apiBackend/server/settings/general/google-calendar-integration/googleCalendarSettingsInitResponse.json';
//import departmentGoogleCalendar from '../apiBackend/server/settings/department/google-calendar/google-calendar.json';
//import ld_discounts_frontpage from '../apiBackend/server/LDdiscounts/frontpage.json';
import ld_discounts_too_small_request_on_creation from '../apiBackend/server/LDdiscounts/tooSmallRequestOnCreation.json';
import ld_discounts_medium_request_on_creation from '../apiBackend/server/LDdiscounts/mediumRequestOnCreation.json';
import move_inventory_room from '../apiBackend/server/move_inventory_room/move_inventory_room.json';
import move_inventory_room__empty from '../apiBackend/server/move_inventory_room/move_inventory_room__empty.json';
import move_inventory_room__getItems from '../apiBackend/server/move_inventory_item/getItems.json';
import move_inventory_room__getItems__empty from '../apiBackend/server/move_inventory_item/getItems__empty.json';
import request from '../apiBackend/request.json';
import get_inventory_request from '../apiBackend/server/get_inventory_request/get_inventory_request.json';

angular.module('app')
	.factory(
		"TestData", function () {
			
			return {
			//	"frontpage": frontpage,
			//	"getcurrent": getcurrent,
				"get_dashboard": get_dashboard,
				"unread_count": unread_count,
				"get_variable": get_variable,
				"template_builder": template_builder,
				"retrieve_blocks_base": retrieve_blocks_base,
				"get_flags": get_flags,
				"login": login,
				"long_distance_carrier": long_distance_carrier,
				"settings": {
			//		"general": {
				//		"googleCalendarIntergationSettings": googleCalendarIntegration
			//		},
			//		"department": {
			//			"googleCalendar": departmentGoogleCalendar
			//		}
				},
			//	"ld_discounts_frontpage": ld_discounts_frontpage,
				"ld_discounts_too_small_request_on_creation": ld_discounts_too_small_request_on_creation,
				"ld_discounts_medium_request_on_creation": ld_discounts_medium_request_on_creation,
				"move_inventory_room": move_inventory_room,
				"move_inventory_room__empty": move_inventory_room__empty,
				"move_inventory_room__getItems": move_inventory_room__getItems,
				"move_inventory_room__getItems__empty": move_inventory_room__getItems__empty,
				"get_inventory_request": get_inventory_request,
				"request": request
			}
		}
	);
