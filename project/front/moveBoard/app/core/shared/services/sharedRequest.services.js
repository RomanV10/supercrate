angular
	.module('app.calculator')
	.factory('sharedRequestServices', sharedRequestServices);
// NOTICE! to add any services test in both apps / aacount and moveboard , the have common files
sharedRequestServices.$inject = ['common', 'datacontext', 'CalculatorServices'];

function sharedRequestServices(common, datacontext, CalculatorServices) {

	var service = {};

	var fieldData = datacontext.getFieldData();
	var calcSettings = angular.fromJson(fieldData.calcsettings);
	var min_hours = parseFloat(calcSettings.min_hours);
	var services = [1, 2, 3, 4, 6, 8];


	service.calculateQuote = calculateQuote;

	return service;


	function calculateQuote(request) {
		var isDoubleDriveTime = calcSettings.doubleDriveTime && request.field_double_travel_time && !_.isNull(request.field_double_travel_time.raw) && services.indexOf(parseInt(request.service_type.raw)) >= 0;

		var rate = request.rate.value;
		var travelTimeSetting = angular.isDefined(request.request_all_data.travelTime) ? request.request_all_data.travelTime : calcSettings.travelTime;

		if (!request.request_all_data) request.request_all_data = {};
		if (angular.isUndefined(request.request_all_data.add_rate_discount)) {
			request.request_all_data.add_rate_discount = 0;
		}

		var rateDiscount = request.request_all_data.add_rate_discount;

		if (rateDiscount) {
			request.rateDiscount = request.request_all_data.add_rate_discount;
			var rate = request.request_all_data.add_rate_discount;
		}

		var maxTimeValue = request.maximum_time.value;
		var minTimeValue = request.minimum_time.value;

		if (!_.isNumber(request.maximum_time.value)) {
			maxTimeValue = _.round(common.convertStingToIntTime(request.maximum_time.value), 2);
		}

		if (!_.isNumber(request.minimum_time.value)) {
			minTimeValue = _.round(common.convertStingToIntTime(request.minimum_time.value), 2);
		}

		var maxT = maxTimeValue;
		var minT = minTimeValue;

		if (isDoubleDriveTime) {
			var dblTravelFee = request.field_double_travel_time.raw;

			maxT += parseFloat(dblTravelFee);
			minT += parseFloat(dblTravelFee);
		} else {
			if (travelTimeSetting) {
				maxT += parseFloat(request.travel_time.raw);
				minT += parseFloat(request.travel_time.raw);
			}
		}

		if (min_hours > minT) {
			minT = min_hours;
		}

		if (min_hours > maxT) {
			maxT = min_hours;
		}

		minT = CalculatorServices.getRoundedTime(minT);
		maxT = CalculatorServices.getRoundedTime(maxT);

		var min_total = rate * minT;
		var max_total = rate * maxT;
		request.quote = {};
		request.quote.min = _.round(parseFloat(min_total), 2);
		request.quote.max = _.round(parseFloat(max_total), 2);

		return request;
	}
}
