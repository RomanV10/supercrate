(function () {
    'use strict';

    angular
    .module('app.core')
    .factory('PermissionsServices', PermissionsServices);

    PermissionsServices.$inject = ['$rootScope'];

    function PermissionsServices($rootScope) {
        var service = {};

        service.sortData = sortData;
        service.hasPermission = hasPermission;
        service.canEditRequest = canEditRequest;
        service.allowedStatuses = allowedStatuses;
        service.isSuper = isSuper;
        service.allowConfirm = allowConfirm;
        service.ifAdmin = ifAdmin;

        return service;


        function allowConfirm() {
            var user_permissions = _.get($rootScope, 'currentUser.userId.settings.permissions');
            if (ifAdmin()) {
                return true;
            }
            if (angular.isUndefined(user_permissions) || !user_permissions.canSeeAllStatuses) {
                return false;
            }
            else {
                return true;
            }

        }

        function allowedStatuses() {
            var field_approve_admin = {
                1: "Pending",
                2: "Not Confirmed",
                3: "Confirmed",
                4: "In-home Estimate",
                9: "Flat Rate (Inventory needed)",
                10: "Flat Rate (Provide Options)",
                11: "Flat Rate (Wait Option)",
                12: "Expired",
                13: "We are not available",
                14: "Spam",
                5: "Canceled",
                21: "Dead Lead",
                22: "Archived"
            };

            return field_approve_admin;

        }

        function isSuper() {
            let currentUser = _.get($rootScope, 'currentUser.userId', {});
            let user_name = currentUser.name;
            let uid = currentUser.uid;

            return (user_name == 'roma4ke' && uid == '1');
        }

        function ifAdmin() {
            if (!$rootScope.currentUser) {
                return false;
            }

            var user_role = $rootScope.currentUser.userRole;
            var authorizeRoles = ['administrator'];
            var admin = false;

            angular.forEach(authorizeRoles, function (role, id) {
                if (user_role.indexOf(role) > -1) {
                    admin = true;
                }
            });


            return admin;
        }

        function canEditRequest(request) {
			let managerLastName = _.get(request, 'manager.last_name');
            let user_permissions = _.get($rootScope, 'currentUser.userId.settings.permissions');
            let name = _.get($rootScope, 'currentUser.userId.settings.last_name');
			let isCanEditRequest = ifAdmin() || angular.isDefined(managerLastName) && managerLastName == name;

            if (isCanEditRequest) {
                return true;
            }

            return _.get(user_permissions, 'canEditOtherLeads', false);
        }

        function sortData(data) {
            if (_.isNull($rootScope.currentUser)) return false;

			let user_permissions = _.get($rootScope, 'currentUser.userId.settings.permissions', []);
			let name = _.get($rootScope, 'currentUser.userId.settings.last_name');
            let sortData = {};
            let isUserHasAllPermissions = ifAdmin() || user_permissions.canSeeOtherLeads && user_permissions.canSeeUnsignedLeads;

            if (isUserHasAllPermissions) {
                return data;
            }

			_.forEach(data, request => {
				let managerLastName = _.get(request, 'manager.last_name');
				let isRequestManager = managerLastName == name;
				let isManagerCanSeeOtherLeads = !_.isUndefined(managerLastName) && managerLastName != name && user_permissions.canSeeOtherLeads;
				let isManagerCanSeeUnsingedRequest = _.isUndefined(managerLastName) && user_permissions.canSeeUnsignedLeads;

				if (isRequestManager) {
					sortData[request.nid] = request;
				}

				if (isManagerCanSeeOtherLeads) {
					sortData[request.nid] = request;
				}

				if (isManagerCanSeeUnsingedRequest) {
					sortData[request.nid] = request;
				}
			});

            return sortData;

        }

        function hasPermission(permission) {
            if (!$rootScope.currentUser) {
                return false;
            }

            var user_permissions = _.get($rootScope, 'currentUser.userId.settings.permissions');

            if (ifAdmin()) {
                return true;
            }

            if (angular.isUndefined(user_permissions)) {
                return false;
            }

            return user_permissions[permission];
        }


    }
})();
