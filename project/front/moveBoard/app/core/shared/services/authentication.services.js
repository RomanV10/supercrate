'use strict';

angular
	.module('app.core')
	.factory('AuthenticationService', AuthenticationService);

AuthenticationService.$inject = [
	'$http',
	'$rootScope',
	'$q',
	'Session',
	'config',
	'apiService',
	'moveBoardApi',
	'SweetAlert',
	'$route',
	'Raven'
];

function AuthenticationService($http, $rootScope, $q, Session, config, apiService, moveBoardApi, SweetAlert, $route, Raven) {
	var service = {};
	service.Login = Login;
	service.Logout = Logout;
	service.SetCredentials = SetCredentials;
	service.ClearCredentials = ClearCredentials;
	service.isAuthenticated = isAuthenticated;
	service.isAuthorized = isAuthorized;
	service.isAdmin = isAdmin;
	service.GetCurrent = GetCurrent;
	service.getToken = getToken;
	service.GetFront = GetFront;
	service.request_new_password = request_new_password;

	service.autoLogin = autoLogin;

	service.sendLog = sendLog;
	service.get_browser = get_browser;
	service.getUser = getUser;
	service.checkCORS = checkCORS;
	service.checkAutoLogin = checkAutoLogin;
	service.checkSession = checkSession;

	return service;


	function autoLogin(data) {
		var deferred = $q.defer();
		var auto_data = {
			"uid": data.uid,
			"hashed_pass": data.hash,
			"timestamp": data.timestamp,
		};

		if (angular.isDefined(data) && !_.isEmpty(auto_data.uid) && !_.isEmpty(auto_data.hashed_pass) && !_.isEmpty(auto_data.timestamp)) {
			apiService.postData(moveBoardApi.core.autoLogin, auto_data)
				.then((response) => {
					let token = response.data[0];
					Session.saveToken(token);
					deferred.resolve(token);
				}, () => {
					deferred.reject();
				});
		} else {
			deferred.reject();
		}

		return deferred.promise;
	}


	function request_new_password(mail, callback) {

		var credentials = {email: mail};

		$http
			.post(config.serverUrl + 'server/clients/link_restore_password', credentials)
			.success(function (data) {
				if (angular.isDefined(data.send) && data.send) {
					data.success = true;
				}
				else {
					data.success = false;
				}

				callback(data);
			})
			.error(function (data) {
				data.message = 'Error';
				data.success = false;
				callback(data);
			});

	}

	function getToken() {
		$http.get(config.serverUrl + 'services/session/token').success(function (data) {

			var token = data;
			Session.saveToken(unescape(encodeURIComponent(token)));
			return unescape(encodeURIComponent(token));

		});

	}

	function getUser() {
		return Session.get();
	}

	function Login(username, password, homeEstimate = false, callback) {

		/* Dummy authentication for testing, uses $timeout to simulate api call
		 ----------------------------------------------*/
		apiService.postData(moveBoardApi.core.login, {
			username: username,
			password: password,
			from_home_estimate: Number(homeEstimate),
		}).then(res => {
			if (_.get(res, 'data.status_code')) {
				loginRejected(res.data);
			} else {
				getOptimisticResponse(res.data);
			}

		}, rej => {
			loginRejected(rej);
		});

		function getOptimisticResponse(data) {
			data.success = true;

			if (homeEstimate && !_.get(data, 'user.settings.homeEstimator')) {
				callback(data);
				return;
			}

			data.user.roles = initHomeEstimate(data.user['field_is_home_estimate_login'], data.user.roles);

			Session.create(data.token, data.user, data.user.roles);
			Session.saveToken(data.token);
			Session.addRequestNid();
			callback(data);
		}

		function loginRejected(data) {
			if (data == null)
				data = [];
			data.message = 'Username or password is incorrect';
			data.success = false;

			callback(data);
		}
	}

	function initHomeEstimate(isHomeEstimator, initRoles) {
        isHomeEstimator = Number(isHomeEstimator);
		const MAIN_ROLE_INDEX = 1;
		const ESTIMATOR_INDEX = _.indexOf(initRoles, 'home-estimator');

		if (!!isHomeEstimator) {
			initRoles.splice(MAIN_ROLE_INDEX, 1);
		} else if (ESTIMATOR_INDEX > -1) {
			initRoles.splice(ESTIMATOR_INDEX, 1);
		}

		return initRoles;
	}

	function get_browser() {
		var ua = navigator.userAgent, tem,
			M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
		if (/trident/i.test(M[1])) {
			tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
			return {name: 'IE', version: (tem[1] || '')};
		}
		if (M[1] === 'Chrome') {
			tem = ua.match(/\bOPR\/(\d+)/)
			if (tem != null) {
				return {name: 'Opera', version: tem[1]};
			}
		}
		M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
		if ((tem = ua.match(/version\/(\d+)/i)) != null) {
			M.splice(1, 1, tem[1]);
		}
		return {
			name: M[0],
			version: M[1]
		};
	}

	function sendLog(log_type, log_array) {
		var deferred = $q.defer();
		deferred.resolve();
		/*var data =
		{
			"log_type": log_type,
			"data": log_array
		}


		 $http
			 .post(config.serverUrl + 'server/admin_logs',data)
			 .success(function (data) {
				 deferred.resolve(data);
			 })
			 .error(function (data) {
				 return deferred.reject(data);
			 });
	   */

		return deferred.promise;
	}

	function Logout() {
		/* Dummy authentication for testing, uses $timeout to simulate api call
		 ----------------------------------------------*/
		var deferred = $q.defer();
		$http
			.post(config.serverUrl + 'server/user/logout')
			.success(function (data, status, headers, config) {
				Session.destroy();
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				Session.destroy();
				return deferred.reject(data);
			});


		return deferred.promise;
	}

	function GetCurrent() {

		var deferred = $q.defer();

		if (!this.isAuthenticated()) {

			$http.post(config.serverUrl + 'server/clients/getcurrent').success(function (data, status, headers, conf) {
				if (data[0] == false) {
					data.success = false;
					deferred.reject(data);
				}
				else {
					data.success = true;

					data.roles = initHomeEstimate(data['field_is_home_estimate_login'], data.roles);

					Session.create(data.uuid, data, data.roles);
					$rootScope.$broadcast('$startReminderService');
				}

				deferred.resolve(data);

			}).error(function (data, status, headers, config) {
				Session.destroy();
				return deferred.reject(data);
			});

		}
		else {
			$rootScope.$broadcast('$startReminderService');
			deferred.resolve(Session);
		}

		return deferred.promise;
	}


	//GET FRONT
	function GetFront(callback) {


		$http.get(config.serverUrl + 'services/session/token').success(function (data) {

			var token = data;

			$http.defaults.headers.common['X-CSRF-Token'] = unescape(encodeURIComponent(token));

			Session.saveToken(unescape(encodeURIComponent(token)));

			$http
				.post(config.serverUrl + 'server/front/frontpage')
				.success(function (data, status, headers, config) {
					data.success = true;
					callback(data);
				})
				.error(function (data, status, headers, config) {
					data.message = 'Connection Error';
					data.success = false;
					callback(data);
				});


		});

	}


	function SetCredentials(data) {
		$rootScope.globals = {
			currentUser: {
				data: data
			}
		};
	}

	function ClearCredentials() {
		$rootScope.globals = {};
	}


	function isAuthenticated() {

		return !!Session.userId;

	};

	function isAuthorized(user_role) {

		var authorizeRoles = ['administrator', 'manager', 'sales', 'customer service', 'foreman', 'home-estimator'];
		var Auth = false;

		angular.forEach(authorizeRoles, function (role, id) {
			if (user_role.indexOf(role) > -1) {
				Auth = true;
			}
		});
		return Auth;
	}

	function isAdmin(user_role = []) {
		var authorizeRoles = ['administrator'];
		let isAuth = false;

		angular.forEach(authorizeRoles, function (role, id) {
			if (user_role.indexOf(role) > -1) {
				isAuth = true;
			}
		});

		return isAuth;
	}

	function checkCORS() {
		if ('withCredentials' in new XMLHttpRequest()) {
			return false;
		} else if(typeof XDomainRequest !== "undefined"){
			return false;
		} else{
			SweetAlert.swal("Check your browser's cross-domain settings");
			return true;
		}
	}

	function checkAutoLogin() {
		let session = Session.get();
		let params = _.get($route, 'current.params', {});
		let isCanAutoLogin = !session && params.uid && params.hash && params.timestamp;
		return isCanAutoLogin;
	}

	function checkSession() {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.user.getCurrent)
			.then(resolve => {
				let currentUser = _.isArray(resolve.data) ? _.head(resolve.data) : resolve.data;
				if (!currentUser) {
					destroySession();
					return defer.reject();
				}
				defer.resolve();
			}, reject => {
				destroySession();
				defer.reject();
			});

		return defer.promise;
	}

	function destroySession() {
		toastr.error('Session was closed', 'Please try to login again');
		Session.destroy();
		location.assign(location.protocol + '//' + location.host + location.pathname);
	}
}
