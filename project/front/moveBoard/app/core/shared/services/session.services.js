(function () {
  'use strict';


    var core = angular.module('app.core');

    core.factory('Session', Session);

    Session.$inject = ['$rootScope','localStorageService'];

    function Session($rootScope,localStorageService) {

      var service = {};
      
    service.create  = create;
    service.get  = get;
    service.destroy = destroy;
    service.getToken = getToken;
    service.saveToken = saveToken;
    service.isAdmin = isAdmin;
    service.addRequestNid = addRequestNid;
    service.getRequestNid = getRequestNid;
    service.removeRequestNid = removeRequestNid;
    return service;


     function isAdmin() {
         var user = service.get();
         var roles = user.userId.roles;
         if (angular.isArray(roles)) {
             return roles.indexOf("administrator") > -1;
         }
         else {
             return roles.hasOwnProperty('administrator');
         }
       }
    
   function saveToken(data) {

       $rootScope.token = data;
       localStorageService.set('eltoken', data);

   }
    
  function getToken() {
      var token =  localStorageService.get('eltoken');

      if(angular.isDefined($rootScope.token)){
      
          return $rootScope.token;
    
      }
       else 
       {
           if(angular.isDefined(token)){

               return token;
               //return $cookies.token;

           }
           else
            return 0;
  
       }
  }  
      
      
    function get(sessionId, userId, userRole) {
        
        return   $rootScope.currentUser;
        
    };  
      
    function create (sessionId, userId, userRole) {
        
    this.id = sessionId;
    this.userId = userId;

    this.userRole = userRole;

        $rootScope.currentUser = this;
        
    };
      
      
   function destroy() {
       
    this.id = null;
    this.userId = null;
    this.userRole = null;
     $rootScope.currentUser = null;
       
  };


        function addRequestNid(requestNid) {
            var nid = requestNid;
            if(!nid)
                nid = getRequestNid();
            $rootScope.requestNid = nid;
            localStorageService.set('requestNid', nid);
        }

        function getRequestNid() {
            if(angular.isDefined($rootScope.requestNid)) return $rootScope.requestNid;
            else if(angular.isDefined(localStorageService.get('requestNid'))) return localStorageService.get('requestNid');
            else
                return 0;
        }

        function removeRequestNid() {
            delete $rootScope.requestNid;
            localStorageService.set('requestNid', null);
        }
      
      
      
  }


})();