'use strict';

angular
	.module('app.core')
	.directive('fullScreen', fullScreen);

fullScreen.$inject = ['Fullscreen', 'erDeviceDetector'];

function fullScreen(Fullscreen, erDeviceDetector) {
	return {
		template: require('./fullscreen.html'),
		link: link, 
		restrict: 'E'
	};
	
	function link(scope, element) {
		scope.isMobile = erDeviceDetector.isMobile;
		element.on('click', onClick);
		
		scope.isSuportedFullScreen = Fullscreen.isSupported();
		scope.isFullScreen = true;
		
		function onClick() {
			checkIsFullScreen();
			
			if (scope.isFullScreen) {
				Fullscreen.cancel();
			} else {
				Fullscreen.all();
			}
		}
		
		function checkIsFullScreen() {
			scope.isFullScreen = Fullscreen.isEnabled();
		}
	}
}