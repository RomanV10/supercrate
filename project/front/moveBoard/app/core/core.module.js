'use strict';

angular.module('app.core', [
    'ui.bootstrap',
    'ngAnimate',
    'LocalStorageModule',
    'ngSanitize',
    'datatables',
    'datatables.columnfilter',
    'blocks.logger',
    'blocks.router',
    'tien.clndr',
    'textAngular',
    'ui.switchery',
    'ngMask',
    'checklist-model',
    'mwl.calendar',
    'oitozero.ngSweetAlert',
    'ui.bootstrap.contextMenu',
    'uiGmapgoogle-maps',
    'AngularPrint',
    'ngPrint',
    'FBAngular',
    'wu.masonry'
]);

