'use strict';

angular
	.module('app.core')
	.factory('common', common);

/* @ngInject */
function common($location, $q, $rootScope, $timeout) {
	let throttles = {};

	let service = {
		// common angular dependencies
		$broadcast: $broadcast,
		$q: $q,
		$timeout: $timeout,
		getTimeArray: getTimeArray,
		getDateTime: getDateTime,
		// generic
		createSearchThrottle: createSearchThrottle,
		debouncedThrottle: debouncedThrottle,
		isNumber: isNumber,

		replaceLocationUrlGuidWithId: replaceLocationUrlGuidWithId,
		textContains: textContains,
		convertStingToIntTime: convertStingToIntTime,
		convertToHHMM: convertToHHMM,
		convertTime: convertTime,
		decToTime: decToTime,
		count: count,
		objToArray: objToArray,
		range: range,
		toDatesArr: toDatesArr,
		toUSAtime: toUSAtime,
		diff: diff,
		diffArray: diffArray,
		diff_remove: diff_remove,
		showAnimation: showAnimation,
		filterFloat: filterFloat,
	};

	return service;
	//////////////////////

	function filterFloat(value) {
		if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value)) {
			return Number(value);
		}

		return NaN;
	}

	function showAnimation(nid) {
		$timeout(function () {
			let scaleCurve = mojs.easing.path('M0,100 L25,99.9999983 C26.2328835,75.0708847 19.7847843,0 100,0');
			let el = document.querySelector('#request_' + nid);

			if (!el) return;

			let	elSpan = el.querySelector('div'),
				timeline = new mojs.Timeline(),
				// burst animation
				tween1 = new mojs.Burst({
					parent: el,
					duration: 2000,
					shape: 'circle',
					fill: 'white',
					x: '50%',
					y: '50%',
					childOptions: {
						radius: {6: 0},
						type: 'line',
						stroke: '#87C4F3',
						strokeWidth: 2
					},
					radius: {40: 110},
					count: 20,
					isRunLess: true,
					easing: mojs.easing.bezier(0.1, 1, 0.3, 1)
				}),
				// ring animation
				tween2 = new mojs.Transit({
					parent: el,
					duration: 1200,
					type: 'circle',
					radius: {8: 60},
					fill: 'transparent',
					stroke: '#87C4F3',
					strokeWidth: {30: 0},
					x: '50%',
					y: '50%',
					isRunLess: true,
					easing: mojs.easing.bezier(0.1, 1, 0.3, 1)
				}),
				tween3 = new mojs.Tween({
					duration: 1200,
					easing: mojs.easing.bezier(0.1, 1, 0.3, 1),
					onUpdate: function (progress) {
						let scaleProgress = scaleCurve(progress);
						elSpan.style.WebkitTransform = elSpan.style.transform = 'scale3d(' + scaleProgress + ',' + scaleProgress + ',1)';
					}
				});

			// add tweens to timeline:
			timeline.add(tween1, tween2, tween3);
			// when clicking the button start the timeline/animation:
			timeline.start();
		}, 200);
	}

	function diff(source, target) {
		let result = '';

		_.each(source, function (request, index) {
			if (angular.isUndefined(target[index])) {
				result = index;
			}

			if (angular.isDefined(target[index])) {
				if (request.changed != target[index].changed) {
					result = index;
				}
			}
		});

		return result;
	}

	function diffArray(source, target) {
		let result = [];

		_.each(source, function (request, index) {
			if (angular.isUndefined(target[index])) {
				result.push(index);
			}

			if (angular.isDefined(target[index])) {
				if (request.changed != target[index].changed) {
					result.push(index);
				}
			}
		});

		return result;
	}

	function diff_remove(source, target) {
		let result = '';

		_.each(target, function (request, index) {
			if (angular.isUndefined(source[index])) {
				result = index;
			}
		});

		return result;
	}

	function toUSAtime(time) {
		let amtime;

		if (time > 12) {
			amtime = time - 12;
			if (amtime == 12) return "12AM";
			return amtime + "PM";
		} else {
			if (time == 12) return "12PM";
			return time + "AM";
		}
	}

	function $broadcast() {
		return $rootScope.$broadcast.apply($rootScope, arguments);
	}

	function createSearchThrottle(viewmodel, list, filteredList, filter, delay) {
		// After a delay, search a viewmodel's list using
		// a filter function, and return a filteredList.

		// custom delay or use default
		delay = +delay || 300;
		// if only vm and list parameters were passed, set others by naming convention
		if (!filteredList) {
			// assuming list is named sessions, filteredList is filteredSessions
			filteredList = 'filtered' + list[0].toUpperCase() + list.substr(1).toLowerCase(); // string
			// filter function is named sessionFilter
			filter = list + 'Filter'; // function in string form
		}

		// create the filtering function we will call from here
		let filterFn = function () {
			// translates to ...
			// vm.filteredSessions
			//      = vm.sessions.filter(function(item( { returns vm.sessionFilter (item) } );
			viewmodel[filteredList] = viewmodel[list].filter(function (item) {
				return viewmodel[filter](item);
			});
		};

		return (function () {
			// Wrapped in outer IIFE so we can use closure
			// over filterInputTimeout which references the timeout
			let filterInputTimeout;

			// return what becomes the 'applyFilter' function in the controller
			return function (searchNow) {
				if (filterInputTimeout) {
					$timeout.cancel(filterInputTimeout);
					filterInputTimeout = null;
				}

				if (searchNow || !delay) {
					filterFn();
				} else {
					filterInputTimeout = $timeout(filterFn, delay);
				}
			};
		})();
	}

	function objToArray(obj) {
		let output = [];

		for (let key in obj) {
			// must create a temp object to set the key using a variable
			let tempObj = {};
			tempObj = obj[key];
			output.push(tempObj);
		}

		return output;
	}

	function debouncedThrottle(key, callback, delay, immediate) {
		// Perform some action (callback) after a delay.
		// Track the callback by key, so if the same callback
		// is issued again, restart the delay.
		let defaultDelay = 1000;
		delay = delay || defaultDelay;

		if (throttles[key]) {
			$timeout.cancel(throttles[key]);
			throttles[key] = undefined;
		}

		if (immediate) {
			callback();
		} else {
			throttles[key] = $timeout(callback, delay);
		}
	}

	function isNumber(val) {
		// negative or positive
		return (/^[-]?\d+$/).test(val);
	}

	function replaceLocationUrlGuidWithId(id) {
		// If the current Url is a Guid, then we replace
		// it with the passed in id. Otherwise, we exit.
		let currentPath = $location.path();
		let slashPos = currentPath.lastIndexOf('/', currentPath.length - 2);
		let currentParameter = currentPath.substring(slashPos - 1);

		if (isNumber(currentParameter)) {
			return;
		}

		let newPath = currentPath.substring(0, slashPos + 1) + id;
		$location.path(newPath);
	}

	function textContains(text, searchText) {
		return text && -1 !== text.toLowerCase().indexOf(searchText.toLowerCase());
	}

	function getDateTime(request) {
		let day = request.day;
		let month = request.month;
		let year = request.year;
		let date = year + '-' + month + '-' + day;
		let _date = new Date(date);

		return _date.getTime();
	}

	function getTimeArray() {
		let time = [
			{value: '1', type: 'AM'},
			{value: '2', type: 'AM'},
			{value: '3', type: 'AM'},
			{value: '4', type: 'AM'},
			{value: '5', type: 'AM'},
			{value: '6', type: 'AM'},
			{value: '7', type: 'AM'},
			{value: '8', type: 'AM'},
			{value: '9', type: 'AM'},
			{value: '10', type: 'AM'},
			{value: '11', type: 'AM'},
			{value: '12', type: 'AM'},
			{value: '1', type: 'PM'},
			{value: '2', type: 'PM'},
			{value: '3', type: 'PM'},
			{value: '4', type: 'PM'},
			{value: '5', type: 'PM'},
			{value: '6', type: 'PM'},
			{value: '7', type: 'PM'},
			{value: '8', type: 'PM'},
			{value: '9', type: 'PM'},
			{value: '10', type: 'PM'},
			{value: '11', type: 'PM'},
			{value: '12', type: 'PM'},
		];

		return time;
	}

	function decToTime(input) {
		let zero = '0';
		let zero2 = '0';
		let hrs = parseInt(Number(input));
		let fraction = Number(input) - hrs;
		let min = 0;
		// 0, 15min - 0.25 , 30min - 0.5 , 45 min - 0.75.
		if (fraction > 0.15 && fraction < 0.36) {
			min = 15;
		}

		if (fraction >= 0.36 && fraction < 0.64) {
			min = 30;
		}

		if (fraction >= 0.64 && fraction < 0.87) {
			min = 45;
		}

		if (fraction >= 0.87) {
			min = 0;
			hrs++;
		}

		if (hrs > 9) {
			zero = '';
		}

		if (min > 9) {
			zero2 = '';
		}

		if (min == 0) {
			return zero + hrs + ':00';
		} else if (hrs == 0) {
			return '00:' + min;
		} else {
			return zero + hrs + ':' + zero2 + min;
		}
	}

	function convertStingToIntTime(time) {
		if (_.isEmpty(time)) return 0;

		let hours = 0;
		let minutes = 0;
		let parseHours = time.match(/^(\d+)/);
		let parseMinutes = time.match(/:(\d+)/);

		if (parseHours) {
			hours = Number(parseHours[1]);
		}

		if (parseMinutes) {
			minutes = Number(parseMinutes[1]);
		}

		minutes /= 60;
		time = hours + minutes;

		return time;
	}

	function convertToHHMM(info) {
		let hrs = parseInt(Number(info));
		let min = Math.round((Number(info) - hrs) * 60);

		if (min != 0)
			return hrs + ' hrs ' + min + ' min';
		else if (min == 0 && hrs == 1)
			return hrs + ' hr';
		else
			return hrs + ' hrs';
	}

	function convertTime(time) {
		if (_.isEmpty(time)) {
			return 0;
		}

		let hours = Number(time.match(/^(\d+)/)[1]);
		let matchMinutes = time.match(/:(\d+)/);
		let minutes = Number(matchMinutes != null ? matchMinutes[1] : 0);
		let matchAMPM = time.match(/\s(.*)$/);
		let AMPM = matchAMPM ? matchAMPM[1] : 0;

		if (AMPM == "PM" && hours < 12) {
			hours = hours + 12;
		}

		if (AMPM == "AM" && hours == 12) {
			hours = hours - 12;
		}

		hours = hours * 2;

		if (minutes == 30) {
			hours++;
		}

		return hours;
	}

	function count(object) {
		let count = 0;

		angular.forEach(object, () => count++);

		return count;
	}

	function range(min, max, step) {
		step = step || 1;
		let input = [];

		for (let i = min; i <= max; i += step) {
			input.push(i);
		}

		return input;
	}

	function toDatesArr(calendar) {
		let array = {};

		_.forEach(calendar, function (n, key) {
			_.forEach(calendar[key], function (n1, key1) {
				_.forEach(calendar[key][key1], function (n2, key2) {
					_.forEach(calendar[key][key1][key2], function (n3, key3) {
						array[key3] = n3;
					});
				});
			});
		});

		return array;
	}
}
