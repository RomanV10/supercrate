(function () {
    'use strict';

    angular
        .module('app.storages')
        .directive('storageAging', storageAging);

    storageAging.$inject = ['StorageService', 'SweetAlert', 'DTOptionsBuilder', 'PaymentServices'];


    function storageAging(StorageService, SweetAlert, DTOptionsBuilder, PaymentServices) {
        var directive = {
            link: link,
	        template: require('../templates/storage-aging.html'),
            restrict: 'AE'
        };
        return directive;


        function link($scope, element, attrs) {
            $scope.clicked = '';
            $scope.totalAmount = 0;
            $scope.busy = true;
            $scope.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('bFilter', false)
                .withOption('aaSorting', [0, 'desc'])
                .withDisplayLength(25);
            $scope.getBalance = getBalance;

            function getBalance(invoices, payments) {
                return PaymentServices.calcPayment(payments) - PaymentServices.calcInvoices(invoices);
            }

            function getAgingData(invoices, payments) {
                var obj = {
                    lessDays30: {
                        invoices:[],
                        payments:[],
                        balance: 0
                    },
                    lessDays60: {
                        invoices:[],
                        payments:[],
                        balance: 0
                    },
                    lessDays90: {
                        invoices:[],
                        payments:[],
                        balance: 0
                    },
                    moreDays90: {
                        invoices:[],
                        payments:[],
                        balance: 0
                    }
                };
                var currDay = moment().unix();
                var curMonth = moment().get('month');
                var curDate = moment().get('date');
                var days30, days60, days90;
                if(curDate == moment().endOf('month')){
                    days30 = moment(moment().set('month', curMonth - 1).set('date', moment().set('month', curMonth - 1).endOf('month'))).unix();
                    days60 = moment(moment().set('month', curMonth - 2).set('date', moment().set('month', curMonth - 1).endOf('month'))).unix();
                    days90 = moment(moment().set('month', curMonth - 3).set('date', moment().set('month', curMonth - 1).endOf('month'))).unix();
                }else {
                    days30 = moment(moment().set('month', curMonth - 1).set('date', curDate)).unix();
                    days60 = moment(moment().set('month', curMonth - 2).set('date', curDate)).unix();
                    days90 = moment(moment().set('month', curMonth - 3).set('date', curDate)).unix();
                }
                _.forEach(invoices, function (item) {
                    var dateInvoice = moment(item.data.date).unix();
                    if(dateInvoice <= currDay && dateInvoice >= days30){
                        obj.lessDays30.invoices.push(item);
                    }else if(dateInvoice < days30 && dateInvoice >= days60){
                        obj.lessDays60.invoices.push(item);
                    }else if(dateInvoice < days60 && dateInvoice >= days90){
                        obj.lessDays90.invoices.push(item);
                    }else if(dateInvoice < days90){
                        obj.moreDays90.invoices.push(item);
                    }
                });
                _.forEach(payments, function (item) {
                    var datePayment = item.created ? parseInt(item.created) : moment(item.date).unix();
                    if(datePayment <= currDay && datePayment >= days30){
                        obj.lessDays30.payments.push(item);
                    }else if(datePayment < days30 && datePayment >= days60){
                        obj.lessDays60.payments.push(item);
                    }else if(datePayment < days60 && datePayment >= days90){
                        obj.lessDays90.payments.push(item);
                    }else if(datePayment < days90){
                        obj.moreDays90.payments.push(item);
                    }
                });
                var lessDays30 = getBalance(obj.lessDays30.invoices, obj.lessDays30.payments);
                var lessDays60 = getBalance(obj.lessDays60.invoices, obj.lessDays60.payments);
                var lessDays90 = getBalance(obj.lessDays90.invoices, obj.lessDays90.payments);
                var moreDays90 = getBalance(obj.moreDays90.invoices, obj.moreDays90.payments);
                obj.lessDays30.balance = lessDays30 >= 0 ? 0 : lessDays30;
                obj.lessDays60.balance = lessDays60 >= 0 ? 0 : lessDays60;
                obj.lessDays90.balance = lessDays90 >= 0 ? 0 : lessDays90;
                obj.moreDays90.balance = moreDays90 >= 0 ? 0 : moreDays90;
                return obj;
            }

            $scope.refreshPage = function () {
                $scope.busy = true;
                StorageService.getStorageAging().then(function ({data}) {
                    $scope.clicked = '';
                    $scope.totalAmount = 0;
                    $scope.requests = data;
                    _.forEach(data, function (item) {
                        item.aging = getAgingData(item.invoices, item.payments);
                        $scope.totalAmount += Number(item.balance);
                    });
                    _.forEach($scope.requests, function(value, key) {
                        $scope.$watch('requests['+key+'].balance', function (newVal, oldVal) {
                            value.aging = getAgingData(value.invoices, value.payments);
                        });
                    });
                    $scope.busy = false;
                });
            };


            $scope.refreshPage();
            $scope.openModal = function(request, id){
                if($scope.clicked != id) {
                    $scope.clicked = id;
                    return false;
                }
                StorageService.openModal(request, id, $scope.requests);
            };
        }
    }
})();
