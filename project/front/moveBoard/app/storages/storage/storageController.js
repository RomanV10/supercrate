(function () {
    'use strict';

    angular.module('app.storages')
        .controller('StorageController', ['$scope', '$uibModal', 'StorageService', 'CalculatorServices', 'SendEmailsService',
            '$q', 'TemplateBuilderService', 'EmailBuilderRequestService', 'logger', 'geoCodingService',
            function ($scope, $uibModal, StorageService, CalculatorServices, SendEmailsService,
                      $q, TemplateBuilderService, EmailBuilderRequestService, logger, geoCodingService) {

                var self = this;

                // get all storages
                StorageService.getStorageList()
                    .then(function ({data}) {
                        self.list = _.isArray(data) ? data : [];
                    });


                var isOpened = false;
                var previewOptionsTitleOfInvoice = 'Invoice Template';
                var previewOptionsTitleOfReminder1 = 'Reminder 1 Template';
                var previewOptionsTitleOfReminder2 = 'Reminder 2 Template';
                var previewOptionsTitleOfReminder3 = 'Reminder 3 Template';
                var previewOptionsTitleOfFeeInvoice = 'Fee Invoice Template';
                var blocksMenu = {};
                var originalInvoiceStorageTemplate = {};
                var originalReminder1StorageTemplate = {};
                var originalReminder2StorageTemplate = {};
                var originalReminder3StorageTemplate = {};
                var originalFeeInvoiceTemplate = {};

                // add new storage
                self.openModal = function () {
                    if (isOpened) {
                        return;
                    }

                    var modalInstance = $uibModal.open({
	                    template: require('./templates/addStorageModal.html'),
                        controller: ['$scope', '$uibModalInstance', 'StorageService', 'storageList', 'SweetAlert', modalCreateCtrl],
                        controllerAs: 'modalSave',
                        size: 'lg',
                        windowClass: 'addingStorage requestModal',
                        backdrop: false,
                        resolve: {
                            storageList: function () {
                                return self.list || [];
                            }
                        }
                    });

                    isOpened = true;
                };


                // $uibModalInstance controller(for creating)
                function modalCreateCtrl($scope, $uibModalInstance, StorageService, storageList, SweetAlert) {

                    var modal = this;
                    isOpened = false;

	                const storageTemplates = [];

                    $scope.addNewStorage = function () {
	                    let isNotEmptyTemplates = Object.keys(storageTemplates).every(function(k){
	                        return !_.isEmpty(storageTemplates[k].blocks);
	                    });

	                    if (!isNotEmptyTemplates) {
	                        SweetAlert.swal('Check the presence of templates', '', 'error');
	                        return false;
                        }

                        var data = {
                            "data": {
                                "name": modal.name,
                                "address": modal.address,
                                "zip": modal.zip,
                                "state": modal.state,
                                "city": modal.city,
                                "phone1": modal.phone1,
                                "phone2": modal.phone2,
                                "fax": modal.fax,
                                "email": modal.email,
                                "default": modal.default || 0,
                                "tax": parseFloat(modal.tax),
                                "rate_per_cuft": parseFloat(modal.rate_per_cuft) || 0,
                                "late_fee": parseFloat(modal.late_fee) || 0,
                                "apply_late_fee_in": parseFloat(modal.apply_late_fee_in) || 0,
                                "terms": modal.terms,
                                "closeBalance": false,
                                "min_cuft": modal.min_cuft || 0,
                                "reminder1Days": modal.reminder1Days,
                                "reminder2Days": modal.reminder2Days,
                                "reminder3Days": modal.reminder3Days,
	                            "emailsTemplate": {
		                            1: !_.isEmpty($scope.invoiceStorageTemplate.blocks),
		                            2: !_.isEmpty($scope.reminder1StorageTemplate.blocks),
		                            3: !_.isEmpty($scope.reminder2StorageTemplate.blocks),
		                            4: !_.isEmpty($scope.reminder3StorageTemplate.blocks),
		                            5: !_.isEmpty($scope.feeInvoiceTemplate.blocks)
	                            }
                            }
                        };


                        StorageService.addStorage(data)
                            .then(function (response) {
                                data.id = _.head(response.data);
                                storageList.push(data);
								SweetAlert.swal('A new storage has been successfully created!');
                                $uibModalInstance.close();
                                return response.data;
                            });

                        $scope.invoiceStorageTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#invoiceTemp');
                        $scope.reminder1StorageTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#reminder1');
                        $scope.reminder2StorageTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#reminder2');
                        $scope.reminder3StorageTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#reminder3');
                        $scope.feeInvoiceTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#feeInvoiceTemp');
                        saveTemplate($scope.invoiceStorageTemplate, originalInvoiceStorageTemplate);
                        saveTemplate($scope.reminder1StorageTemplate, originalReminder1StorageTemplate);
                        saveTemplate($scope.reminder2StorageTemplate, originalReminder2StorageTemplate);
                        saveTemplate($scope.reminder3StorageTemplate, originalReminder3StorageTemplate);
                        saveTemplate($scope.feeInvoiceTemplate, originalFeeInvoiceTemplate);
                    };

                    modal.changePostalCode = function () {
                        var postal_code = modal.zip;
                        if (postal_code.length == 5) {
							geoCodingService
                                .geoCode(postal_code)
                                .then(function (result) {
                                    modal.city = result.city;
                                    modal.state = result.state;
                                }, function () {
									SweetAlert.swal("You entered the wrong zip code.", '', 'error');
                                    modal.city = '';
                                    modal.state = '';
                                });
                        }
                    };

                    // close modal without saving data
                    $scope.closeModal = function () {
                        $uibModalInstance.dismiss();
                    };


                    getStorageInvoiceTemplate()
                        .then(function (emailTemplate) {
                            $scope.invoiceStorageTemplate = emailTemplate;
	                        storageTemplates.push($scope.invoiceStorageTemplate);
                            originalInvoiceStorageTemplate = angular.copy(emailTemplate);
                            $scope.blocksMenu = blocksMenu;
                        });

                    getStorageReminder1Template()
                        .then(function (emailTemplate) {
                            $scope.reminder1StorageTemplate = emailTemplate;
	                        storageTemplates.push($scope.reminder1StorageTemplate);
                            originalReminder1StorageTemplate = angular.copy(emailTemplate);
                        });

                    getStorageReminder2Template()
                        .then(function (emailTemplate) {
                            $scope.reminder2StorageTemplate = emailTemplate;
	                        storageTemplates.push($scope.reminder2StorageTemplate);
                            originalReminder2StorageTemplate = angular.copy(emailTemplate);
                        });

                    getStorageReminder3Template()
                        .then(function (emailTemplate) {
                            $scope.reminder3StorageTemplate = emailTemplate;
	                        storageTemplates.push($scope.reminder3StorageTemplate);
                            originalReminder3StorageTemplate = angular.copy(emailTemplate);
                        });

                    getStorageFeeInvoiceTemplate()
                        .then(function (emailTemplate) {
                            $scope.feeInvoiceTemplate = emailTemplate;
	                        storageTemplates.push($scope.feeInvoiceTemplate);
                            originalFeeInvoiceTemplate = angular.copy(emailTemplate);
                        });

                    $scope.previewOptionsOfInvoice = makePreviewOptions(previewOptionsTitleOfInvoice);
                    $scope.previewOptionsOfReminder1 = makePreviewOptions(previewOptionsTitleOfReminder1);
                    $scope.previewOptionsOfReminder2 = makePreviewOptions(previewOptionsTitleOfReminder2);
                    $scope.previewOptionsOfReminder3 = makePreviewOptions(previewOptionsTitleOfReminder3);
                    $scope.previewOptionsTitleOfFeeInvoice = makePreviewOptions(previewOptionsTitleOfFeeInvoice);
                    $scope.isSendEmailPreview = true;
                }


                // update storage
                self.updateStorage = function (index) {
                    if (isOpened) {
                        return;
                    }

                    var modalInstance = $uibModal.open({
	                    template: require('./templates/addStorageModal.html'),
                        controller: ['$scope', '$uibModalInstance', 'StorageService', 'storageData', 'storageIndexDB',
                            'storageList', 'storageIndexLoop', 'SweetAlert', modalUpdateCtrl],
                        controllerAs: 'modalSave',
                        size: 'lg',
                        windowClass: 'addingStorage requestModal',
                        backdrop: false,
                        resolve: {
                            storageData: function () {
                                return StorageService.getStorageByID(self.list[index]['id']);
                            },
                            storageIndexDB: function () {
                                return self.list[index]['id'];
                            },
                            storageList: function () {
                                return self.list;
                            },
                            storageIndexLoop: function () {
                                return index;
                            }
                        }
                    });

                    isOpened = true;
                };


                // $uibModalInstance controller(for updating)
                function modalUpdateCtrl($scope, $uibModalInstance, StorageService, storageData, storageIndexDB,
					storageList, storageIndexLoop, SweetAlert) {
                    var modal = this;
                    var data = storageData.data;
                    isOpened = false;
                    modal.isDefaultStorage = false;
	                const storageTemplates = [];

                    angular.forEach(storageList, function (item) {
                        if (item['default']) {
                            modal.isDefaultStorage = true;
                        }
                    });


                    angular.forEach(data, function (value, key) {
                        modal[key] = value;
                    });

                    $scope.isUpdate = storageIndexDB;

                    $scope.deleteStorage = function () {
                        SweetAlert.swal({
                                title: "Are you sure?",
                                text: "Are you sure you want to delete this storage!",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, delete it!",
                                closeOnConfirm: false
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    storageList.splice(storageIndexLoop, 1);
                                    StorageService.deleteStorage(storageIndexDB);
                                    $uibModalInstance.close();
                                    SweetAlert.swal("The storage has been deleted successfully!");
                                }
                            });
                    };

                    modal.changePostalCode = function () {
                        var postal_code = modal.zip;

                        if (!_.isUndefined(postal_code)
                            && postal_code.length == 5) {

							geoCodingService
                                .geoCode(postal_code)
                                .then(function (result) {
                                    modal.city = result.city;
                                    modal.state = result.state;
                                }, function () {
									SweetAlert.swal("You entered the wrong zip code.", '', 'error');
                                    modal.city = '';
                                    modal.state = '';
                                });
                        }
                    };

                    $scope.saveStorage = function () {
	                    let isNotEmptyTemplates = Object.keys(storageTemplates).every(function(k){
		                    return !_.isEmpty(storageTemplates[k].blocks);
	                    });

	                    if (!isNotEmptyTemplates) {
		                    SweetAlert.swal('Check the presence of templates', '', 'error');
		                    return false;
	                    }

                        modal.busy = true;
                        var dataObj = {
                            "data": {
                                "name": modal.name,
                                "address": modal.address,
                                "zip": modal.zip,
                                "state": modal.state,
                                "city": modal.city,
                                "phone1": modal.phone1,
                                "phone2": modal.phone2,
                                "fax": modal.fax,
                                "email": modal.email,
                                "default": modal.default || false,
                                "tax": parseFloat(modal.tax) || 0,
                                "rate_per_cuft": parseFloat(modal.rate_per_cuft) || 0,
                                "late_fee": parseFloat(modal.late_fee) || 0,
                                "apply_late_fee_in": parseFloat(modal.apply_late_fee_in) || 0,
                                "terms": modal.terms,
                                "min_cuft": modal.min_cuft || 0,
                                "closeBalance" :  modal.closeBalance || false,
                                "reminder1Days": modal.reminder1Days,
                                "reminder2Days": modal.reminder2Days,
                                "reminder3Days": modal.reminder3Days,
                                "emailsTemplate": {
                                    1: !_.isEmpty($scope.invoiceStorageTemplate.blocks),
                                    2: !_.isEmpty($scope.reminder1StorageTemplate.blocks),
                                    3: !_.isEmpty($scope.reminder2StorageTemplate.blocks),
                                    4: !_.isEmpty($scope.reminder3StorageTemplate.blocks),
                                    5: !_.isEmpty($scope.feeInvoiceTemplate.blocks)
                                }
                            }
                        };


                        StorageService.updateStorage(storageIndexDB, dataObj)
                            .then(function ({data}) {
                                dataObj.id = storageIndexDB;
                                var ind = _.findIndex(storageList, {id: storageIndexDB});
                                storageList[ind].data = dataObj.data;
                                modal.busy = false;
                                return data.data;
                            });

                        $scope.invoiceStorageTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#invoiceTemp');
                        $scope.reminder1StorageTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#reminder1');
                        $scope.reminder2StorageTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#reminder2');
                        $scope.reminder3StorageTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#reminder3');
                        $scope.feeInvoiceTemplate.template = TemplateBuilderService.getCurrentHTMLTemplate('#feeInvoiceTemp');
                        saveTemplate($scope.invoiceStorageTemplate, originalInvoiceStorageTemplate);
                        saveTemplate($scope.reminder1StorageTemplate, originalReminder1StorageTemplate);
                        saveTemplate($scope.reminder2StorageTemplate, originalReminder2StorageTemplate);
                        saveTemplate($scope.reminder3StorageTemplate, originalReminder3StorageTemplate);
                        saveTemplate($scope.feeInvoiceTemplate, originalFeeInvoiceTemplate);
                    };

                    // close modal without saving data
                    $scope.closeModal = function () {
                        $uibModalInstance.dismiss();
                    };

                    getStorageInvoiceTemplate()
                        .then(function (emailTemplate) {
                            $scope.invoiceStorageTemplate = emailTemplate;
	                        storageTemplates.push($scope.invoiceStorageTemplate);
                            originalInvoiceStorageTemplate = angular.copy(emailTemplate);
                            $scope.blocksMenu = blocksMenu;
                        });

                    getStorageReminder1Template()
                        .then(function (emailTemplate) {
                            $scope.reminder1StorageTemplate = emailTemplate;
	                        storageTemplates.push($scope.reminder1StorageTemplate);
                            originalReminder1StorageTemplate = angular.copy(emailTemplate);
                        });

                    getStorageReminder2Template()
                        .then(function (emailTemplate) {
                            $scope.reminder2StorageTemplate = emailTemplate;
	                        storageTemplates.push($scope.reminder2StorageTemplate);
                            originalReminder2StorageTemplate = angular.copy(emailTemplate);
                        });

                    getStorageReminder3Template()
                        .then(function (emailTemplate) {
                            $scope.reminder3StorageTemplate = emailTemplate;
	                        storageTemplates.push($scope.reminder3StorageTemplate);
                            originalReminder3StorageTemplate = angular.copy(emailTemplate);
                        });

                    getStorageFeeInvoiceTemplate()
                        .then(function (emailTemplate) {
                            $scope.feeInvoiceTemplate = emailTemplate;
	                        storageTemplates.push($scope.feeInvoiceTemplate);
                            originalFeeInvoiceTemplate = angular.copy(emailTemplate);
                        });

                    $scope.previewOptionsOfInvoice = makePreviewOptions(previewOptionsTitleOfInvoice);
                    $scope.previewOptionsOfReminder1 = makePreviewOptions(previewOptionsTitleOfReminder1);
                    $scope.previewOptionsOfReminder2 = makePreviewOptions(previewOptionsTitleOfReminder2);
                    $scope.previewOptionsOfReminder3 = makePreviewOptions(previewOptionsTitleOfReminder3);
                    $scope.previewOptionsTitleOfFeeInvoice = makePreviewOptions(previewOptionsTitleOfFeeInvoice);
                    $scope.isSendEmailPreview = true;
                }

                function getStorageInvoiceTemplate() {
                    return getStorageTemplateByKeyName('storage_invoice_template');
                }

                function getStorageReminder1Template() {
                    return getStorageTemplateByKeyName('storage_reminder1');
                }

                function getStorageReminder2Template() {
                    return getStorageTemplateByKeyName('storage_reminder2');
                }

                function getStorageReminder3Template() {
                    return getStorageTemplateByKeyName('storage_reminder3');
                }

                function getStorageFeeInvoiceTemplate() {
                    return getStorageTemplateByKeyName('fee_invoice_template');
                }

                function getStorageTemplateByKeyName(keyName) {
                    var defer = $q.defer();

                    var emailParameters = {
                        keyNames: [keyName]
                    };

                    SendEmailsService
                        .prepareForEditEmails(emailParameters)
                        .then(function (data) {
                            var emails = data.emails;

                            if (_.isEmpty(emails)) {
                                var emptyEmailObject = SendEmailsService.createEmptyEmailObject();
                                emptyEmailObject.key_name = keyName;
                                emails.push(emptyEmailObject);
                            }

                            if (_.isEmpty(blocksMenu)) {
                                blocksMenu = data.blocksMenu;
                            }

                            defer.resolve(emails[0]);
                        }, function (error) {
                            defer.reject('error');
                        });

                    return defer.promise;
                }

                function makePreviewOptions(title) {
                    return {
                        previewTitle: title,
                        isShowCloseButton: false,
                        isShowSaveButton: false,
                        isEditTemplateName: false,
                        editBlockOptions: {
                            isShowVariableToolTip: true,
                            isShowOnlyInvoiceVariable: true
                        },
                        isShowPreviewTabs: false
                    }
                }

                function saveTemplate(template, original) {
                    var defer = $q.defer();

                    if (!_.isEqual(template, original)
                        && !_.isEmpty(template.blocks)) {

                        template.data.subject = template.subject;

                        if (_.isNull(template.id)) {
                            EmailBuilderRequestService
                                .createTemplate(template)
                                .then(function (response) {
                                    logger.success('Template was created successfully!', response.status, 'Success');
                                    template.id = _.head(response.data);
                                    template.templateId = template.id;
                                    TemplateBuilderService.updateGlobalEmailTemplates(template);
                                    copyObjectProperty(template, original);
                                    defer.resolve(response.data);
                                }, function (error) {
                                    defer.reject();
                                    logger.error('Error creating a template!', JSON.stringify(error), 'Error');
                                });
                        } else {
                            EmailBuilderRequestService
                                .updateTemplate(template)
                                .then(function (response) {
                                    defer.resolve(response.data);
                                    TemplateBuilderService.updateGlobalEmailTemplates(template);
                                    copyObjectProperty(template, original);
                                    logger.success('Template was updated successfully!', response.status, 'Success');
                                }, function (error) {
                                    defer.reject();
                                    logger.error('Error updating a template!', JSON.stringify(error), 'Error');
                                });
                        }
                    } else {
                        defer.resolve();
                    }

                    return defer.promise;
                }

                function copyObjectProperty(origin, destination) {
                    _.each(origin, function (value, property) {
                        destination[property] = angular.copy(value);
                    });
                }
            }]);
})();
