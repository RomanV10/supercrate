'use strict';

import { DEFAULT_STORAGE_REQUEST } from './default-storage-request-const';

angular
	.module('app.storages')
	.controller('CreateStorageController', CreateStorageController);

/* @ngInject */
function CreateStorageController($scope, $timeout, $uibModalInstance, StorageService, requests, SweetAlert, StoragePaymentService, RequestServices, PaymentServices, datacontext, InvoiceServices, PermissionsServices, $uibModal, SendEmailsService, request, $rootScope, $q, geoCodingService, openRequestService) {
	const LOG_TITLES = {
		create: 'Admin created Storage Request'
	};
	$scope.busy = true;
	$scope.invoices = [];
	$scope.serviceTypes = {
		1: 'Pending',
		2: 'Move in',
		3: "Move out",
		4: 'Move out Pending'
	};
	$scope.fieldData = datacontext.getFieldData();
	$scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);
	var STORAGEREQUEST = $scope.fieldData.enums.entities.STORAGEREQUEST;
	$scope.entitytype = $scope.fieldData.enums.entities.STORAGEREQUEST;
	$scope.update = false;
	$scope.msgLog = {};
	$scope.basicData = {};
	$scope.receipts = [];
	$scope.opened = {
		openedEnd: false
	};
	$scope.currDate = new Date();
	$scope.dateWasEdited = false;

	var invoices = {};
	invoices.entity_type = $scope.entitytype;

	var receiptLedger = false;
	var receiptLedgerData = null;

	$scope.todayDate = new Date();
	$scope.howOften = [
		{name: 'Weekly', id: 0, val: 1, type: 'week'},
		// {name:'Two week', id:1, val: 2, type: 'week'},
		// {name:'Tree week', id:2, val: 3, type: 'week'},
		{name: 'Monthly', id: 3, val: 1, type: 'month'},
		{name: 'Two months', id: 4, val: 2, type: 'month'},
		{name: 'Tree months', id: 5, val: 3, type: 'month'},
		{name: 'Four months', id: 6, val: 4, type: 'month'},
		{name: 'Half year', id: 7, val: 6, type: 'month'}
	];

	$scope.data = angular.copy(DEFAULT_STORAGE_REQUEST);
	$scope.isAdmin = PermissionsServices.isSuper();
	if ($scope.isAdmin) {
		$scope.howOften.push({name: 'Test', id: 1, val: 1, type: 'test'});
	}
	$scope.testingRecurring = false;

	$timeout(function () {
		$(".modal-dialog").draggable({handle: ".modal-header"});
	}, 100);

	function convertToYourTimeZone(date){
		return moment(moment.unix(date).utcOffset(0).format('YYYY-MM-DD'), 'YYYY-MM-DD').toDate();
	}

	function initStorageTenant(request) {
		$scope.request = angular.copy(request);

		if (request) {
			$scope.update = true;
			$scope.receipts = angular.copy(request.data.payments) || [];
			$scope.invoices = angular.copy(request.data.invoices) || [];
			$scope.request.nid = request.id;
			$scope.basicData = {};
		}

		if ($scope.request)
			invoices.entity_id = $scope.request.id;

		StorageService.getStorageList().then(function ({data}) {
			$scope.busy = false;
			$scope.storageList = data;
			if (!$scope.update) {
				$scope.busy = true;
				StorageService.getDefaultStorage().then(function ({data}) {
					$scope.busy = false;
					if (!data.id)
						return false;
					$scope.data.rentals.storage = data.id.toString();
					$scope.data.rentals.storage = data.id.toString();
					$scope.data.rentals.tax_rate = data.value.tax;
					$scope.data.rentals.rate_per_cuft = data.value.rate_per_cuft;
					$scope.data.rentals.late_fee = data.value.late_fee;
					$scope.data.rentals.apply_late_fee_in = data.value.apply_late_fee_in;
					$scope.data.rentals.min_cuft = data.value.min_cuft || 0;
					$scope.data.rentals.total_monthly = $scope.data.rentals.total_monthly ? $scope.data.rentals.total_monthly : $scope.data.rentals.volume_cuft * $scope.data.rentals.rate_per_cuft;
				})
			}
		});

		_.forEach($scope.data, function (item) {
			_.forEach(item, function (d, key) {
				$scope.basicData[key] = d;
			});
		});
		if (request) {
			_.forEach(request.data, function (item, key) {
				if (_.isArray(item) && _.isEmpty(item))
					request.data[key] = {};
				_.forEach(item, function (d, key) {
					$scope.basicData[key] = d;
				});
			});
		}
		if ($scope.update) {
			let reqData = angular.copy(request);
			_.forEach(reqData.data, function (mainVal, field) {
				if (!_.isObject(mainVal) && !_.isArray(mainVal)) {
					$scope.data[field] = reqData.data[field];
				} else {
					if (!$scope.data[field]) $scope.data[field] = {};
					_.forEach(mainVal, function (val, subfield) {
						$scope.data[field][subfield] = reqData.data[field][subfield];
					});
				}
				$scope.data.user_info.rental_address = _.get(request, 'data.user_info.rental_address') ? _.get(request, 'data.user_info.rental_address') :
					_.get(request, 'data.user_info.address');
			});
			if ($scope.data.recurring.start) {
				let ind = _.findIndex($scope.howOften, {id: $scope.data.recurring.how_often});
				if ($scope.howOften[ind].type == 'test' && $scope.isAdmin) {
					$scope.testingRecurring = true;
				} else {
					$scope.data.recurring.start_point = convertToYourTimeZone($scope.data.recurring.start_point);
					$scope.data.recurring.how_many = $scope.data.recurring.how_many ? convertToYourTimeZone($scope.data.recurring.how_many) : ($scope.data.recurring.how_many == 0 ? $scope.data.recurring.how_many : ($scope.data.rentals.moved_out_date ? convertToYourTimeZone($scope.data.rentals.moved_out_date) : 0));
				}
			} else {
				let ind = _.findIndex($scope.howOften, {id: $scope.data.recurring.how_often});
				if ($scope.howOften[ind].type == 'test' && $scope.isAdmin) {
					$scope.testingRecurring = true;
				} else {
					$scope.data.recurring.start_point = convertToYourTimeZone($scope.data.recurring.start_point);
					$scope.data.recurring.how_many = $scope.data.recurring.how_many ? convertToYourTimeZone($scope.data.recurring.how_many) : ($scope.data.recurring.how_many == 0 ? $scope.data.recurring.how_many : ($scope.data.rentals.moved_out_date ? convertToYourTimeZone($scope.data.rentals.moved_out_date) : 0));
				}
			}
			if (!$scope.data.recurring.next_run_on)
				$scope.data.recurring.next_run_on = StorageService.convertTimeToUtc(moment($scope.data.recurring.start_point).format('MM/DD/YYYY'));
			if (!$scope.data.recurring.next_run_on_fee)
				$scope.data.recurring.next_run_on_fee = $scope.data.recurring.next_run_on;
			if (!$scope.data.recurring.start_date)
				$scope.data.recurring.start_date = $scope.data.recurring.next_run_on;

			//rewrite date from unix
			if ($scope.data.rentals.moved_out_date)
				$scope.data.rentals.moved_out_date = convertToYourTimeZone($scope.data.rentals.moved_out_date);
			if ($scope.data.rentals.moved_in_date)
				$scope.data.rentals.moved_in_date = convertToYourTimeZone($scope.data.rentals.moved_in_date);

			$scope.data.rentals.status_flag = $scope.data.rentals.status_flag.toString();
			if (angular.isUndefined($scope.data.user_info.realRequest)) {
				if ($scope.data.rentals.move_request_id)
					RequestServices.getRequestsByNid($scope.data.rentals.move_request_id).then(function (response) {
						if (!_.isEmpty(response.nodes))
							$scope.data.user_info.realRequest = true;
					}, function () {
						$scope.data.user_info.realRequest = false;
					});
			}

			if(($scope.data.recurring.created || !$scope.data.recurring.started) && !$scope.data.recurring.start){
				$scope.data.recurring.start_point = StorageService.convertTimeToUtc(moment().format('MM/DD/YYYY'));
				let dataRecurring = {
					start: $scope.data.recurring.start,
					start_point: convertToYourTimeZone($scope.data.recurring.start_point),
					how_often: $scope.data.recurring.how_often,
					how_many: !$scope.data.recurring.how_many ? 0 : (_.isDate($scope.data.recurring.how_many) ? $scope.data.recurring.how_many : convertToYourTimeZone($scope.data.recurring.how_many)),
					next_run_on: $scope.data.recurring.start_point,
					invoice_description: "",
					auto_send: $scope.data.recurring.auto_send,
					start_date: convertToYourTimeZone($scope.data.recurring.start_point),
					next_run_on_fee: $scope.data.recurring.start_point,
					created: false,
					started: $scope.data.recurring.started || $scope.dateWasEdited
				};
				$scope.data.recurring = angular.copy(dataRecurring);
			}

			if(!$scope.data.nid) $scope.data.nid = $scope.request.nid;

		}
	}

	initStorageTenant(request);

	$scope.$watch('data.recurring.start', function (newValue, oldValue) {
		if (newValue != oldValue) {
			if (!$scope.data.recurring.how_many && $scope.data.recurring.how_many != 0)
				if ($scope.data.rentals.moved_out_date)
					$scope.data.recurring.how_many = $scope.data.rentals.moved_out_date;
			if (!$scope.data.recurring.start_point)
				$scope.data.recurring.start_point = $scope.data.rentals.moved_in_date;
			$scope.data.recurring.created = false;
		}
	});

	$scope.dateFormatter = function (date) {
		return convertToYourTimeZone(date);
	};

	$scope.proRate = function () {
		let terms = '';
		let ind = _.findIndex($scope.storageList, {id: $scope.data.rentals.storage});
		if(ind > -1)
			terms = $scope.storageList[ind].data.terms;

		let charge = {
			cost: '',
			description: '',
			name: 'Storage Pro-Rate',
			qty: 1,
			tax: 0
		};
		let dateIn, dateOut, days;
		if ($scope.request.data.rentals.moved_in_date)
			dateIn = moment(convertToYourTimeZone($scope.request.data.rentals.moved_in_date)).format('MMMM DD, YYYY');
		if (moment(dateIn).month() < moment().month() && moment(dateIn).year() <= moment().year())
			dateIn = moment().startOf('month').format('MMMM DD, YYYY');
		dateOut = moment(dateIn).endOf('month').format('MMMM DD, YYYY');
		if ($scope.request.data.rentals.moved_out_date) {
			let dateEnd = moment(convertToYourTimeZone($scope.request.data.rentals.moved_out_date)).format('MMMM DD, YYYY');
			if (moment(dateEnd).diff(moment(dateOut), 'day') < 0)
				dateOut = dateEnd;
		}
		if (moment(convertToYourTimeZone($scope.request.data.rentals.moved_in_date)).format('MMMM DD, YYYY') == moment(convertToYourTimeZone($scope.request.data.rentals.moved_out_date)).format('MMMM DD, YYYY')) {
			dateIn = moment(convertToYourTimeZone($scope.request.data.rentals.moved_in_date)).format('MMMM DD, YYYY');
			dateOut = moment(convertToYourTimeZone($scope.request.data.rentals.moved_out_date)).format('MMMM DD, YYYY');
		}
		charge.description = 'Pro rate ' + moment(dateIn).format('DD MMMM') + ' till ' + moment(dateOut).format('DD MMMM');
		days = moment(dateOut).diff(moment(dateIn), 'day') + 1;
		charge.cost = (_.round(Number($scope.request.data.rentals.total_monthly) / 30, 2) * days).toFixed(2);
		let invoice = {
			"entity_type": STORAGEREQUEST,
			"entity_id": $scope.request.id,
			"data": {
				"id": $scope.request.id,
				"charges": [charge],
				"storage_invoice": true,
				"field_moving_from": {
					"postal_code": $scope.data.user_info.zip,
					"thoroughfare": $scope.data.user_info.address,
					"locality": $scope.data.user_info.city,
					"administrative_area": $scope.data.user_info.state
				},
				"date": moment().format('MMMM DD, YYYY'),
				"client_name": $scope.data.user_info.name,
				"email": $scope.data.user_info.email,
				"phone": $scope.data.user_info.phone1,
				"uid": $scope.data.user_info.uid,
				"discount": 0,
				"tax": 0,
				"terms": terms,
				"flag": 0,
				"totalInvoice": _.round(charge.qty * charge.cost, 2),
				"total": _.round(charge.qty * charge.cost, 2),
				"entity_type": STORAGEREQUEST,
				"entity_id": $scope.request.id,
				"rental_address":  $scope.request.data.user_info.rental_address ? $scope.request.data.user_info.rental_address :
					$scope.request.data.user_info.address
			}
		};
		InvoiceServices.openInvoiceDialog($scope.data, $scope.entitytype, false, invoice, terms);
	};

	$scope.datePaidTrought = function () {
		if ($scope.request.id) {
			let dateOut;
			dateOut = moment().endOf('month').format('MMM DD, YYYY');
			if ($scope.data.rentals.moved_out_date) {
				let dateEnd = moment(convertToYourTimeZone($scope.request.data.rentals.moved_out_date)).format('MMMM DD, YYYY');
				if (moment(dateEnd).diff(moment(dateOut), 'day') < 0)
					dateOut = dateEnd;
			}

			let total = $scope.calculatePayments();

			if (total > 0) {
				let days = Math.floor(total / _.round(+$scope.data.rentals.total_monthly) / 30, 2);

				if (days > 0) {
					dateOut = moment(dateOut).add(days, 'day');
				}
			}

			return moment(dateOut).format('MMM DD, YYYY');
		}
	};

	$scope.diffDate = function () {
		let start = moment($scope.data.recurring.start_point);
		let next = moment($scope.data.recurring.how_many);
		let end = moment($scope.data.recurring.how_many);
		let ind = _.findIndex($scope.howOften, {id: $scope.data.recurring.how_often});
		if (end.diff(start, $scope.howOften[ind].type, true) > $scope.howOften[ind].val) {
			next = start.add($scope.howOften[ind].type, $scope.howOften[ind].val);
			let a = moment();
			if (next.diff(a) < 0)
				while (next.diff(a) < 0) {
					next = next.add($scope.howOften[ind].type, $scope.howOften[ind].val);
				}
		}
		if (end.diff(start, $scope.howOften[ind].type, true) == $scope.howOften[ind].val)
			next = start.add($scope.howOften[ind].type, $scope.howOften[ind].val);
	};

	$scope.changeHowOften = function (field) {
		let ind = _.findIndex($scope.howOften, {id: $scope.data.recurring.how_often});
		if ($scope.howOften[ind].type == 'test' && $scope.isAdmin) {
			$scope.testingRecurring = true;
			$scope.data.recurring.start_point = StorageService.convertTimeToUtc(moment().format('MM/DD/YYYY'));
			$scope.data.recurring.how_many = StorageService.convertTimeToUtc(moment(moment().add(2100, 's')).format('MM/DD/YYYY'));
			$scope.data.recurring.next_run_on = $scope.data.recurring.start_point;
			return false;
		} else if ($scope.isAdmin) {
			if ($scope.testingRecurring) {
				$scope.data.recurring.start_point = $scope.data.rentals.moved_in_date ? $scope.data.rentals.moved_in_date : moment().format('MMM DD, YYYY');
				$scope.data.recurring.how_many = $scope.data.rentals.moved_out_date ? $scope.data.rentals.moved_out_date : moment().format('MMM DD, YYYY');
				$scope.data.recurring.next_run_on = StorageService.convertTimeToUtc(moment($scope.data.recurring.start_point).format('MM/DD/YYYY'));
			}
			$scope.testingRecurring = false;
		}
		if ($scope.data.recurring.start_point && $scope.data.recurring.how_many && $scope.data.recurring.how_often !== '' && $scope.data.recurring.how_often >= 0) {
			$scope.diffDate();
		} else if ($scope.data.recurring.start_point && $scope.data.recurring.how_often !== '' && $scope.data.recurring.how_often >= 0) {
			let start = moment($scope.data.recurring.start_point);
			let ind = _.findIndex($scope.howOften, {id: $scope.data.recurring.how_often});
			if ($scope.howOften[ind].type == 'test')
				return false;
			let next = start.add($scope.howOften[ind].type, $scope.howOften[ind].val);
		}
	};

	$scope.changeRecurringStartDate = function () {
		let ind = _.findIndex($scope.howOften, {id: $scope.data.recurring.how_often});
		if ($scope.howOften[ind].type == 'test' && $scope.isAdmin) {
			$scope.data.recurring.next_run_on = $scope.data.recurring.start_point;
		} else {
			$scope.data.recurring.next_run_on = StorageService.convertTimeToUtc(moment($scope.data.recurring.start_point).format('MM/DD/YYYY'));
		}
	};

	$scope.removeEndDate = function () {
		if ($scope.data.recurring.start) return;

		$scope.data.recurring.how_many = 0;
		$scope.data.rentals.moved_out_date = '';
	};

	$scope.removeMoveOutDate = function () {
		$scope.data.rentals.moved_out_date = '';

		if ($scope.data.recurring.start) return;
		$scope.data.recurring.how_many = 0;
	};

	$scope.changeZip = function (value) {
		if (angular.isUndefined(value)) {
			$scope.data.user_info.city = '';
			$scope.data.user_info.state = '';
			$scope.changeField('state', $scope.data.user_info.state);
			$scope.changeField('city', $scope.data.user_info.city);
			return false;
		}
		let postal_code = value;
		if (postal_code.length == 5) {
			geoCodingService.geoCode(postal_code)
				.then(function (result) {
					$scope.data.user_info.city = result.city;
					$scope.data.user_info.state = result.state;
					$scope.changeField('state', $scope.data.user_info.state);
					$scope.changeField('city', $scope.data.user_info.city);
				}, function () {
					SweetAlert.swal("You entered the wrong zip code.", '', 'error');
					$scope.data.user_info.city = '';
					$scope.data.user_info.state = '';
					$scope.changeField('state', $scope.data.user_info.state);
					$scope.changeField('city', $scope.data.user_info.city);
				});
		}
	};

	$scope.changeVolume = function (field, value) {
		if (Number($scope.data.rentals.min_cuft) < Number($scope.data.rentals.volume_cuft)) $scope.data.rentals.total_monthly = $scope.data.rentals.volume_cuft * $scope.data.rentals.rate_per_cuft;
		else $scope.data.rentals.total_monthly = ($scope.data.rentals.min_cuft ? $scope.data.rentals.min_cuft * $scope.data.rentals.rate_per_cuft : $scope.data.rentals.volume_cuft * $scope.data.rentals.rate_per_cuft) || 0;
		if ($scope.basicData[field] == value)
			return false;
		$scope.msgLog[field] = {
			from: $scope.basicData[field],
			to: value,
			text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' was changed'
		};
		if (field == 'rate_per_cuft') {
			$scope.msgLog[field].from = '$' + $scope.basicData[field];
			$scope.msgLog[field].to = '$' + value;
		}
		$scope.changeField('total_monthly', $scope.data.rentals.total_monthly);
		$scope.data.rentals.total_monthly_change = false;
	};

	$scope.changeStorage = function (id) {
		let field = 'storage';
		let ind = _.findIndex($scope.storageList, {id: id});

		if (ind == -1) {
			return false;
		}

		$scope.msgLog[field] = {
			from: '',
			to: '',
			text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' was changed'
		};

		let storageData = $scope.storageList[ind].data;
		$scope.data.rentals.tax_rate = angular.copy(storageData.tax);
		$scope.data.rentals.min_cuft = angular.copy(storageData.min_cuft);
		$scope.data.rentals.rate_per_cuft = angular.copy(storageData.rate_per_cuft);
		$scope.data.rentals.late_fee = angular.copy(storageData.late_fee);
		$scope.data.rentals.apply_late_fee_in = angular.copy(storageData.apply_late_fee_in);
	};

	$scope.updateStorageRequest = function (info, type) {
		if ($scope.storageReq.$invalid) {
			return false;
		}

		if ($scope.data.rentals.total_monthly <= 0) {
			SweetAlert.swal('Error', 'Monthly rate must be greater than zero', 'error');
			return false;
		}

		$scope.busy = true;
		let dataToSend = {};
		_.forEach(info, function (data, key) {
			if (key != 'user_info' && key != 'rentals' && key != 'recurring')
				return false;
			if (!dataToSend[key])
				dataToSend[key] = {};
			_.forEach(data, function (item, i) {
				if (!dataToSend[key][i])
					dataToSend[key][i] = {};
				dataToSend[key][i] = _.clone(item);
			});
		});
		if (!dataToSend.rentals.move_request_id) delete dataToSend.rentals.move_request_id;
		if (!$scope.changeStartRec && !$scope.request.data.recurring.start) {
			if (!dataToSend.recurring.start_point) {
				dataToSend.recurring.start_point = StorageService.convertTimeToUtc(moment(new Date((typeof dataToSend.rentals.moved_in_date == 'string' || dataToSend.rentals.moved_in_date instanceof Date) ? dataToSend.rentals.moved_in_date : dataToSend.rentals.moved_in_date)).format('MM/DD/YYYY'));
			} else {
				dataToSend.recurring.start_point = StorageService.convertTimeToUtc(moment(new Date((typeof dataToSend.recurring.start_point == 'string' || dataToSend.recurring.start_point instanceof Date) ? dataToSend.recurring.start_point : convertToYourTimeZone(dataToSend.recurring.start_point))).format('MM/DD/YYYY'));
			}

			if (_.isNull(dataToSend.recurring.how_many) || dataToSend.recurring.how_many == "") dataToSend.recurring.how_many = 0;
			if (!dataToSend.recurring.how_many && dataToSend.recurring.how_many != 0) {
				dataToSend.recurring.how_many = $scope.testingRecurring ? dataToSend.recurring.start_point + 2100 : dataToSend.rentals.moved_in_date;
			}
			let dataRecurring = {
				start: dataToSend.recurring.start,
				start_point: parseInt(dataToSend.recurring.start_point),
				how_often: dataToSend.recurring.how_often,
				how_many: parseInt($scope.testingRecurring ? dataToSend.recurring.how_many : (!$scope.data.recurring.how_many ? 0 : StorageService.convertTimeToUtc(moment(new Date((typeof dataToSend.recurring.how_many == 'string' || dataToSend.recurring.how_many instanceof Date) ? dataToSend.recurring.how_many : convertToYourTimeZone(dataToSend.recurring.how_many))).format('MM/DD/YYYY')))),
				next_run_on: parseInt(dataToSend.recurring.start_point),
				invoice_description: "",
				auto_send: dataToSend.recurring.auto_send,
				start_date: parseInt(dataToSend.recurring.start_point),
				next_run_on_fee: parseInt(dataToSend.recurring.start_point),
				created: !!dataToSend.recurring.created,
				started: $scope.data.recurring.started || $scope.dateWasEdited
			};

			$scope.dateWasEdited = moment(dataRecurring.start_point * 1000).startOf('day').unix() != moment().utc().startOf('day').unix() ? true : false;

			if(!$scope.dateWasEdited)
				dataToSend.recurring = $scope.request.data.recurring;
			else
				dataToSend.recurring = dataRecurring;
		} else {
			$scope.changeStartRec = false;
		}
		if (dataToSend.rentals.moved_out_date) {
			dataToSend.rentals.moved_out_date = StorageService.convertTimeToUtc(moment(new Date($scope.data.rentals.moved_out_date)).format('MM/DD/YYYY'));
			dataToSend.recurring.how_many = dataToSend.rentals.moved_out_date || 0;
		}
		dataToSend.rentals.moved_in_date = StorageService.convertTimeToUtc(moment(new Date($scope.data.rentals.moved_in_date)).format('MM/DD/YYYY'));

		if (_.isDate(dataToSend.recurring.start_point)) {
			dataToSend.recurring.start_point = StorageService.convertTimeToUtc(moment(dataToSend.recurring.start_point).format('MM/DD/YYYY'));
			dataToSend.recurring.start_date = angular.copy(dataToSend.recurring.start_point);
			dataToSend.recurring.next_run_on_fee = angular.copy(dataToSend.recurring.start_point);
		}

		let data = {
			data: dataToSend
		};
		StorageService.updateStorageReq(data, $scope.request.id).then(function () {
			if ($scope.requests)
				if (info.rentals.move_request_id != requests[$scope.request.id].rentals.move_request_id && $scope.changedRequest) {
					$scope.changedRequest.request_all_data.storage_request_id = $scope.request.id;
					RequestServices.saveReqData($scope.changedRequest.nid, $scope.changedRequest.request_all_data);
					RequestServices.getRequestsByNid(requests[$scope.request.id].rentals.move_request_id).then(function (data) {
						let requestData = _.head(data.nodes);
						if (requestData.request_all_data.storage_request_id) {
							delete requestData.request_all_data.storage_request_id;
							RequestServices.saveReqData(requestData.nid, requestData.request_all_data);
						}
					});
				}
			$scope.busy = false;
			$scope.dateWasEdited = false;
			if (type != 'recurring')
				toastr.success('Storage Request was successfully updated', 'Success');
			let arr = [];
			_.forEach($scope.msgLog, function (item) {
				arr.push(item);
			});
			if (!_.isEmpty(arr))
				RequestServices.sendLogs(arr, 'Request was updated', $scope.request.id, 'STORAGEREQUEST');
			_.forEach(dataToSend, function (item) {
				_.forEach(item, function (d, key) {
					$scope.basicData[key] = d;
				});
			});
			$scope.msgLog = {};
			$scope.request.data = data.data;
			if (!requests)
				return false;
			requests[$scope.request.id].rentals = data.data.rentals;
			requests[$scope.request.id].user_info = data.data.user_info;
			requests[$scope.request.id].recurring = data.data.recurring;
			activate();
		});
	};

	$scope.changeRequestNid = function () {
		$uibModal.open({
			template: require('../../pending/templates/changeRequestNid.html'),
			controller: 'changeRequestCtrl',
			backdrop: false,
			size: 'xs',
			resolve: {
				data: function () {
					return $scope.data;
				}
			}
		});
	};

	$scope.changeTotalMonthly = function () {
		$scope.data.rentals.total_monthly_change = true;
	};

	$scope.changeStatusFlag = function (status) {
		if ($scope.storageReq.$invalid) {
			return false;
		}
		$scope.data.rentals.status_flag = status;
		$scope.updateStorageRequest($scope.data);
	};

	function createNewRequest(payload){
		StorageService.createStorageReq(payload, true)
			.then(({data}) => {
				let createdStorageData = _.head(data);
				if (createdStorageData) {
					optimisticResponseCreatedTenant(payload, createdStorageData);
				} else {
					SweetAlert.swal('Error', 'Please refresh page', 'error');
				}
			})
			.catch(err => SweetAlert.swal('Error with creating storage request', err, 'error'))
			.finally(() => $scope.busy = false);
	}

	function optimisticResponseCreatedTenant (payload, createdStorageData) {
		if ($scope.changedRequest) {
			$scope.changedRequest.request_all_data.storage_request_id = createdStorageData;
			RequestServices.saveReqData($scope.changedRequest.nid, $scope.changedRequest.request_all_data);
		}

		let logs = StorageService.createBasicLog(payload);
		if (!_.isEmpty(logs)) RequestServices.sendLogs(logs, LOG_TITLES.create, createdStorageData, 'STORAGEREQUEST');
		if (!requests) return false;

		requests[createdStorageData] = {};
		requests[createdStorageData].rentals = _.get(payload, 'data.rentals');
		requests[createdStorageData].user_info = _.get(payload, 'data.user_info');
		requests[createdStorageData].recurring = _.get(payload, 'data.recurring');
		requests[createdStorageData].payments = [];
		requests[createdStorageData].invoices = [];

		initStorageTenant({
			id: createdStorageData,
			data: requests[createdStorageData]
		});
		$scope.msgLog = {};
		toastr.success('Storage Request was successfully created', 'Success');
	}

	$scope.createNewStorageRequest = function () {
		if ($scope.storageReq.$invalid) {
			SweetAlert.swal('Error', 'Please fill all fields', 'error');
			return false;
		}

		if ($scope.data.rentals.total_monthly <= 0) {
			SweetAlert.swal('Error', 'Monthly rate must be greater than zero', 'error');
			return false;
		}

		if($scope.data.user_info.name.split(' ').length < 2){
			SweetAlert.swal("Error!", "Please add your last name", 'error');
			return false;
		}

		$scope.busy = true;
		let dataStorageRequest = angular.copy($scope.data);

		if(dataStorageRequest.field_additional_user) {
			dataStorageRequest.additional_user = {
				uid: dataStorageRequest.field_additional_user.uid
			};

			delete dataStorageRequest.field_additional_user;
		} else {
			dataStorageRequest.additional_user = {};
		}

		if (!dataStorageRequest.recurring.start_point) {
			dataStorageRequest.recurring.start_point = $scope.testingRecurring ? StorageService.convertTimeToUtc(moment(dataStorageRequest.rentals.moved_in_date).format('MM/DD/YYYY')) : dataStorageRequest.rentals.moved_in_date;
		}

		if (_.isEmpty(dataStorageRequest.recurring.how_many)) dataStorageRequest.recurring.how_many = 0;
		if (!dataStorageRequest.rentals.move_request_id) delete dataStorageRequest.rentals.move_request_id;
		let dataRecurring = {
			start: dataStorageRequest.recurring.start,
			start_point: parseInt($scope.testingRecurring ? dataStorageRequest.recurring.start_point : StorageService.convertTimeToUtc(moment(dataStorageRequest.recurring.start_point).format('MM/DD/YYYY'))),
			how_often: dataStorageRequest.recurring.how_often,
			how_many: parseInt($scope.testingRecurring ? dataStorageRequest.recurring.how_many : (!dataStorageRequest.recurring.how_many ? 0 : StorageService.convertTimeToUtc(moment(dataStorageRequest.recurring.how_many).format('MM/DD/YYYY')))),
			next_run_on: parseInt($scope.testingRecurring ? dataStorageRequest.recurring.start_point : StorageService.convertTimeToUtc(moment(dataStorageRequest.recurring.start_point).format('MM/DD/YYYY'))),
			invoice_description: "",
			auto_send: dataStorageRequest.recurring.auto_send,
			start_date: parseInt($scope.testingRecurring ? dataStorageRequest.recurring.start_point : StorageService.convertTimeToUtc(moment(dataStorageRequest.recurring.start_point).format('MM/DD/YYYY'))),
			next_run_on_fee: parseInt($scope.testingRecurring ? dataStorageRequest.recurring.start_point : StorageService.convertTimeToUtc(moment(dataStorageRequest.recurring.start_point).format('MM/DD/YYYY'))),
			created: true,
			started: $scope.data.recurring.started || $scope.dateWasEdited
		};

		dataStorageRequest.recurring = dataRecurring;
		if (dataStorageRequest.rentals.moved_out_date) {
			dataStorageRequest.rentals.moved_out_date = StorageService.convertTimeToUtc(moment(dataStorageRequest.rentals.moved_out_date).format('MM/DD/YYYY'));
			dataStorageRequest.recurring.how_many = dataStorageRequest.rentals.moved_out_date;
		}
		if (dataStorageRequest.rentals.moved_in_date)
			dataStorageRequest.rentals.moved_in_date = StorageService.convertTimeToUtc(moment(dataStorageRequest.rentals.moved_in_date).format('MM/DD/YYYY'));
		var data = {
			data: dataStorageRequest
		};

		createNewRequest(data);
	};

	$scope.closeModal = function () {
		if (_.isEmpty($scope.msgLog)) {
			$uibModalInstance.dismiss();
		} else {
			SweetAlert.swal({
					title: "You haven't saved the changes!",
					text: "You have unsaved changes! Do you want to cancel them?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55", confirmButtonText: "Yes",
					cancelButtonText: "No",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						$uibModalInstance.dismiss();
					}
				});
		}
	};

	$scope.changeField = function (field, value) {
		if (field == 'uid' || field == 'zip_from')
			return false;
		let from = $scope.basicData[field];
		let to = value;

		if (field == 'status_flag') {
			let colorRequest = angular.element('.storageModal');
			let isRecurringTurnOn = _.isEqual($scope.data.recurring.start, 1);
			let isFromMoveInStatus = from == "2";

			if (isRecurringTurnOn && isFromMoveInStatus) {
				checkEnableRecurring($scope.data, from);
				return false;
			}

			for (let i = 1; i <= 3; i++) {
				if (colorRequest.hasClass('status_' + i)) {
					colorRequest.removeClass('status_' + i);
				}
			}

			colorRequest.addClass('status_' + to);
		}
		if (from == to)
			return false;
		if (field == 'moved_in_date' || field == 'moved_out_date') {
			if (from)
				if (from == 0) from = "";
				else from = moment(convertToYourTimeZone(from)).format('MMM DD, YYYY');
			if (to)
				to = moment(to).format('MMM DD, YYYY');
		}
		if (field == 'start_point' || field == 'how_many') {
			if (from) {
				if (from == 0) {
					from = "Forever";
				} else {
					from = $scope.testingRecurring ? from : moment(convertToYourTimeZone(from)).format('MMM DD, YYYY');
				}
			}
			if (to) {
				to = $scope.testingRecurring ? to : moment(to).format('MMM DD, YYYY');
			}
		}
		if (field == 'status_flag') {
			if (from)
				from = $scope.serviceTypes[from];
			if (to)
				to = $scope.serviceTypes[to];
		}
		if (field == 'tax_rate') {
			if (from)
				from += '%';
			if (to)
				to += '%';
		}
		if (field == 'late_fee' || field == 'total_monthly') {
			if (from)
				from = '$' + from;
			if (to)
				to = '$' + to;
		}
		if (field == 'apply_late_fee_in') {
			if (from)
				from = from + ' days';
			if (to)
				to = to + ' days';
		}
		if (field == 'phone1' && to) {
			if (from.toString() == to.replace(/[\s()-]/g, ''))
				return false;
		}
		if (from || to)
			$scope.msgLog[field] = {
				from: from,
				to: to,
				text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' was changed'
			};
		if (field == 'moved_out_date_removed') {
			if ($scope.basicData[field.replace(/_removed/, "")]) {
				from = ($scope.testingRecurring && field == 'how_many_removed') ? from : moment(convertToYourTimeZone($scope.basicData[field.replace(/_removed/, "")])).format('MMM DD, YYYY');
				$scope.msgLog[field] = {
					from: from,
					to: to,
					text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1)
				};
			}
		}
		if (field == 'how_many_removed') {
			if ($scope.basicData[field.replace(/_removed/, "")]) {
				from = ($scope.testingRecurring && field == 'how_many_removed') ? from : moment(convertToYourTimeZone($scope.basicData[field.replace(/_removed/, "")])).format('MMM DD, YYYY');
				$scope.msgLog[field] = {
					from: from,
					to: to == 0 ? "Forever" : to,
					text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1)
				};
			}
		}
		let on = 'On';
		let off = 'Off';
		if (!value) {
			on = 'Off';
			off = 'On'
		}
		if (field == 'start') {
			$scope.msgLog[field].from = off;
			$scope.msgLog[field].to = on;
			$scope.msgLog[field].text = "Recurring";
		}
		if (field == 'auto_send') {
			$scope.msgLog[field].from = off;
			$scope.msgLog[field].to = on;
			$scope.msgLog[field].text = "Automatic send invoice e-mail";
		}
		if (field == 'how_often') {
			if (!$scope.msgLog[field])
				$scope.msgLog[field] = {};
			let indNew = _.findIndex($scope.howOften, {id: to});
			let indOld = _.findIndex($scope.howOften, {id: from});
			if (indNew >= 0) {
				$scope.msgLog[field].to = $scope.howOften[indNew].name;
			} else {
				$scope.msgLog[field].to = '';
			}
			if (indOld >= 0) {
				$scope.msgLog[field].from = $scope.howOften[indOld].name;
			} else {
				$scope.msgLog[field].from = '';
			}
			$scope.msgLog[field].text = "Recurring " + field.replace(/_/g, " ") + ' was changed';
		}

		if (field == 'moved_out_date') {
			$scope.data.recurring.how_many = $scope.data.rentals.moved_out_date;
		}
		if (field == 'how_many') {
			$scope.data.rentals.moved_out_date = $scope.data.recurring.how_many;
		}
	};

	function checkEnableRecurring(storageRequest, oldValue) {
		SweetAlert.swal({
			title: 'For changing status have to stop "Recurring"',
			text: 'Are you sure you want to stop?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: "#E47059",
			confirmButtonText: "Confirm",
			closeOnConfirm: true
		}, function (isConfirm) {
			if (isConfirm) {
				storageRequest.recurring.start = 0;
			} else {
				storageRequest.rentals.status_flag = oldValue;
			}
		});
	}

	$scope.openDate = function ($event) {
		$scope.opened.openedEnd = !$scope.opened.openedEnd;
	};

	//ledger tab
	$scope.openPayment = function () {
		let requestDataPayment = angular.copy($scope.request);
		let terms = '';
		let ind = _.findIndex($scope.storageList, {id: $scope.data.rentals.storage});
		if(ind > -1)
			terms = $scope.storageList[ind].data.terms;

		let entity = {
			entity_id: $scope.request.nid,
			entity_type: $scope.entitytype
		};

		StoragePaymentService.openPaymentModal(requestDataPayment, terms, entity);
	};

	$scope.requestEditModal = function (nid) {
		$scope.busy = true;

		openRequestService.open(nid)
			.finally(() => {
				$scope.busy = false;
			});
	};

	$scope.calculatePayments = function () {
		return PaymentServices.calcPayment($scope.receipts) - PaymentServices.calcInvoices($scope.invoices);
	};

	$scope.removeReceipt = function () {
		if ($scope.receipts[$scope.activeRow].transaction_id != 'Custom Payment') {
			SweetAlert.swal("Error!", "You can't remove payment!\n In this case, please use button 'Refund'", 'error');
			return false;
		}

		SweetAlert.swal({
				title: "Are you sure you want to delete receipt ?",
				text: "It will be deleted permanently",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55", confirmButtonText: "Delete!",
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					let id = $scope.receipts[$scope.activeRow].id;
					PaymentServices.delete_receipt(id).then(function (data) {
						let message = "Payment in the amount  $" + $scope.receipts[$scope.activeRow].amount + ' was removed.';
						let obj = {
							simpleText: message
						};
						let arr = [obj];
						if (!_.isEmpty(arr))
							RequestServices.sendLogs(arr, 'Payment removed', $scope.request.nid, 'STORAGEREQUEST');
						$scope.receipts.splice($scope.activeRow, 1);
						$scope.activeRow = -1;
					}, function (reason) {
						SweetAlert.swal("Failed!", reason, "error");
					});
					$rootScope.$broadcast('request.payment_receipts', $scope.receipts, $scope.request.nid);
				}
			});
	};

	$scope.showReceipt = function (id) {
		PaymentServices.openReceiptModal($scope.request, $scope.receipts[id]);
	};

	$scope.addAuthPayment = function () {
		$scope.request.reservation = false;
		let req = angular.copy($scope.request.data);
		req.nid = $scope.request.id;
		PaymentServices.openAuthPaymentModal(req, undefined, $scope.receipts, $scope.entitytype);
	};

	$scope.changePending = function (index) {
		$scope.receipts[index].entity_id = $scope.request.nid;
		$scope.receipts[index].entity_type = $scope.entitytype;

		PaymentServices.update_receipt($scope.request.nid, $scope.receipts[index]).then(function () {
			let message = "Payment in the amount  $" + $scope.receipts[index].amount + '(was changed pending option).';
			let obj = {
				simpleText: message
			};
			let arr = [obj];
			if (!_.isEmpty(arr))
				RequestServices.sendLogs(arr, 'Payment pending option', $scope.request.nid, 'STORAGEREQUEST');
		});
		$rootScope.$broadcast('request.payment_receipts', $scope.receipts, $scope.request.nid);

	};

	$scope.openRefund = function () {
		$scope.request.phone = $scope.request.user_info.phone1;
		$scope.request.email = $scope.request.user_info.email;
		$scope.request.uid = {
			uid: $scope.request.user_info.uid
		};
		let receiptData = '';
		if ($scope.activeRow != -1)
			receiptData = $scope.receipts[$scope.activeRow];
		let uid = _.get($rootScope, 'currentUser.userId.uid');
		PaymentServices.openRefundModal($scope.request, uid, $scope.entitytype, receiptData);
	};

	$scope.createInvoice = function () {
		let terms = '';
		let ind = _.findIndex($scope.storageList, {id: $scope.data.rentals.storage});
		if(ind > -1)
			terms = $scope.storageList[ind].data.terms;
		InvoiceServices.openInvoiceDialog($scope.request, $scope.entitytype, false, false, terms);
	};

	function saveRecurringData() {

		$scope.changeStartRec = false;
		let dataRecurring;
		if (!$scope.testingRecurring) {
			if ($scope.data.recurring.start) {
				$scope.data.recurring.start = 0;
			} else {
				$scope.data.recurring.start = 1;
				$scope.data.recurring.started = true;
				if (!$scope.data.recurring.next_run_on) {
					SweetAlert.swal("Failed!", 'Next run is empty', "error");
					return false;
				}
				if (!$scope.data.recurring.start_point)
					$scope.data.recurring.start_point = StorageService.convertTimeToUtc(moment(new Date($scope.data.rentals.moved_in_date)).format('MM/DD/YYYY'));
				if (_.isNull($scope.data.recurring.how_many)) $scope.data.recurring.how_many = 0;
				if (!$scope.data.recurring.how_many && $scope.data.recurring.how_many != 0)
					$scope.data.recurring.how_many = StorageService.convertTimeToUtc(moment(new Date($scope.data.rentals.moved_out_date)).format('MM/DD/YYYY'));
				$scope.changeStartRec = true;
			}
			if($scope.dateWasEdited) {
				dataRecurring = {
					start: $scope.data.recurring.start,
					start_point: StorageService.convertTimeToUtc(moment(new Date($scope.data.recurring.start_point)).format('MM/DD/YYYY')),
					how_often: $scope.data.recurring.how_often,
					how_many: !$scope.data.recurring.how_many ? 0 : StorageService.convertTimeToUtc(moment(new Date($scope.data.recurring.how_many)).format('MM/DD/YYYY')),
					next_run_on: StorageService.convertTimeToUtc(moment(new Date($scope.data.recurring.start_point)).format('MM/DD/YYYY')),
					invoice_description: "",
					auto_send: $scope.data.recurring.auto_send,
					start_date: StorageService.convertTimeToUtc(moment(new Date($scope.data.recurring.start_point)).format('MM/DD/YYYY')),
					next_run_on_fee: StorageService.convertTimeToUtc(moment(new Date($scope.data.recurring.start_point)).format('MM/DD/YYYY')),
					created: false,
					started: $scope.data.recurring.started || $scope.dateWasEdited
				};
			}else {
				if (!$scope.data.recurring.start) {
					dataRecurring = {
						start: $scope.data.recurring.start,
						start_point: $scope.request.data.recurring.start_point,
						how_often: $scope.request.data.recurring.how_often,
						how_many: !$scope.request.data.recurring.how_many ? 0 : $scope.request.data.recurring.how_many,
						next_run_on: $scope.request.data.recurring.start_point,
						invoice_description: "",
						auto_send: $scope.request.data.recurring.auto_send,
						start_date: $scope.request.data.recurring.start_point,
						next_run_on_fee: $scope.request.data.recurring.start_point,
						created: false,
						started: $scope.data.recurring.started || $scope.dateWasEdited
					};
				}else if ($scope.data.recurring.start) {
					$scope.changeRecurringStartDate();
					$scope.changeHowOften('start_point');
					dataRecurring = {
						start: $scope.data.recurring.start,
						start_point: StorageService.convertTimeToUtc(moment().format('MM/DD/YYYY')),
						how_often: $scope.request.data.recurring.how_often,
						how_many: $scope.data.recurring.how_many ? StorageService.convertTimeToUtc(moment($scope.data.recurring.how_many).format('MM/DD/YYYY')) : 0,
						next_run_on: StorageService.convertTimeToUtc(moment().format('MM/DD/YYYY')),
						invoice_description: "",
						auto_send: $scope.request.data.recurring.auto_send,
						start_date: StorageService.convertTimeToUtc(moment().format('MM/DD/YYYY')),
						next_run_on_fee: StorageService.convertTimeToUtc(moment().format('MM/DD/YYYY')),
						created: false,
						started: $scope.data.recurring.started || $scope.dateWasEdited
					};
				}
			}
		} else {
			if ($scope.data.recurring.start) {
				$scope.data.recurring.start = 0;
			} else {
				$scope.data.recurring.start = 1;
				$scope.data.recurring.started = true;
				if (!$scope.data.recurring.next_run_on) {
					SweetAlert.swal("Failed!", 'Next run is empty', "error");
					return false;
				}
				if (!$scope.data.recurring.start_point)
					$scope.data.recurring.start_point = StorageService.convertTimeToUtc(moment(new Date($scope.data.rentals.moved_in_date)).format('MM/DD/YYYY'));
				if (!$scope.data.recurring.how_many && $scope.data.recurring.how_many != 0)
					$scope.data.recurring.how_many = $scope.data.recurring.start_point + 2100;
				$scope.changeStartRec = true;
			}
			dataRecurring = {
				start: $scope.data.recurring.start,
				start_point: parseInt($scope.data.recurring.start_point),
				how_often: $scope.data.recurring.how_often,
				how_many: !$scope.data.recurring.how_many ? 0 : parseInt($scope.data.recurring.how_many),
				next_run_on: parseInt($scope.data.recurring.start_point),
				invoice_description: "",
				auto_send: $scope.data.recurring.auto_send,
				start_date: parseInt($scope.data.recurring.start_point),
				next_run_on_fee: parseInt($scope.data.recurring.start_point),
				created: false,
				started: $scope.data.recurring.started || $scope.dateWasEdited
			};
		}
		if ($scope.request.id) {
			let data = angular.copy($scope.data);
			$scope.request.data.recurring = data;
			$scope.changeField('start', data.recurring.start);
			$scope.updateStorageRequest(data, 'recurring');
			$scope.dateWasEdited = false;
		}
	}

	$scope.changeStartRecurring = function () {
		if ($scope.data.recurring.start) {
			SweetAlert.swal({
					title: "Are you sure you want to stop recurring?",
					text: "",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55", confirmButtonText: "Stop",
					cancelButtonText: "No",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm)
						saveRecurringData()
				});
		} else {
			let currentStorage = _.find($scope.storageList, {id: $scope.data.rentals.storage});
			let isHasAllTemplates = getAllStorageTemplates(currentStorage);
			if (!isHasAllTemplates) {
				let storageName = currentStorage.data.name;
				SweetAlert.swal('Storage "' + storageName + '" does not have some templates', ' Check the presence of templates', 'error');
				return false;
			}
			saveRecurringData();
		}
	};

	function getAllStorageTemplates(storage) {
		let emailTemplates = storage.data.emailsTemplate;
		if (_.isUndefined(emailTemplates)) {
			return false;
		} else {
			return Object.keys(emailTemplates).every(function(key){ return emailTemplates[key] === true; });
		}
	}

	function activate() {
		let nid = $scope.request.nid;
		StorageService.getStorageReqByID(nid).then(function ({data}) {
			$scope.receipts = data.payments;
			$scope.request.data.payments = data.payments;
			$scope.invoices = data.invoices;
			$scope.request.data.invoices = data.invoices;
			if (requests && requests[nid]) {
				requests[nid].balance = data.balance;
				if (data.payments) {
					if (!requests[nid].payments)
						requests[nid].payments = [];
					if (requests[nid].payments.length != data.payments.length)
						requests[nid].payments = data.payments;
				}
				if (data.invoices) {
					if (!requests[nid].invoices)
						requests[nid].invoices = [];
					if (requests[nid].invoices.length != data.invoices.length)
						requests[nid].invoices = data.invoices;
				}
				if (data.recurring) requests[nid].recurring = data.recurring;
			}
			$rootScope.$broadcast('payment.service', {payments: data.payments, invoices: data.invoices});
		});
	}

	function paidInvoice(data) {
		if (data.data.total <= (PaymentServices.calcPayment($scope.receipts) - PaymentServices.calcInvoices($scope.invoices)) && $scope.receipts.length) {
			data.data.flag = $scope.fieldData.enums.invoice_flags.PAID;
			let dataSend = {
				data: data.data
			};
			InvoiceServices.editInvoice(data.id, dataSend).then(function () {
				$scope.$broadcast('move_invoice.removed');
				if ($scope.request)
					activate();
			});
		} else {
			if ($scope.request)
				activate();
		}
	}

	if ($scope.request)
		activate();

	$scope.dateEdited = () => {
		$scope.dateWasEdited = true;
	};

	$scope.$on('receipt_sent', function () {
		if ($scope.request)
			activate();
	});

	$scope.$on('move_invoice.changed', function (event, data) {
		if ($scope.request)
			activate();
	});

	$scope.$on('move_invoice.created', function (evt, data) {
		paidInvoice(data);
	});

	$scope.$on('move_invoice.prorate', function (evt, data) {
		let upData = {
			data: data.invoice,
			id: data.id
		};
		paidInvoice(upData);
	});

	$scope.$on('receipt.ledger', function (evt, data) {
		receiptLedger = true;
		receiptLedgerData = data;
		activate();
	});

	$scope.$on('change.reqID', function (ev, data, changedRequest) {
		_.forEach(data.user_info, function (item, key) {
			$scope.changeField(key, item);
		});
		$scope.data.rentals.total_monthly_change = false;
		$scope.changeVolume('volume_cuft', $scope.data.rentals.volume_cuft);
		$scope.changeField('volume_cuft', $scope.data.rentals.volume_cuft);
		$scope.changeField('moved_out_date', $scope.data.rentals.moved_out_date);
		$scope.changeField('moved_in_date', $scope.data.rentals.moved_in_date);
		$scope.changeField('address', $scope.data.user_info.address);
		$scope.data.user_info.rental_address = $scope.data.user_info.rental_address || $scope.data.user_info.address;
		let field = 'move_request_id';
		$scope.msgLog[field] = {
			from: $scope.basicData[field],
			to: $scope.data.rentals[field],
			text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' was changed'
		};
		$scope.changedRequest = changedRequest;
	});

	$scope.$on('request.payment_receipts', function (event, receipts) {
		$scope.receipts = receipts;
		$scope.request.data.payments = receipts;
	});

	$scope.sendEmail = () => {
		var emailParameters = {
			keyNames: ['storage_statement_template']
		};

		SendEmailsService
			.prepareForEditEmails(emailParameters)
			.then(function (data) {
				var emails = data.emails;

				if (_.isEmpty(emails)) {
					emails.push(SendEmailsService.createEmptyEmailObject());
				}

				var isSendEmailPreview = true;
				var templateOptions = {
					rtype: 1,
					rid: $scope.request.id,
					isShowPreviewTabs: true
				};
				var openMailPreviewPromise = SendEmailsService.showEmailPreview(emails[0], data.blocksMenu, isSendEmailPreview, templateOptions);

				$q.all([openMailPreviewPromise])
					.then(function (response) {
						var email = response[0];
						if (_.isObject(email)
							&& !_.isEmpty(email.blocks)) {

							$scope.smallBusyModal = true;

							SendEmailsService
								.sendEmailsInvoice($scope.request.id, [email], $scope.request.id)
								.then(function () {
									toastr.success('Email was sent', 'Success')
								}, function () {
									toastr.success("Email wasn't sent", 'Error')
								})
								.finally(function () {
									$scope.smallBusyModal = false;
								});
						}
					}, function (reason) {
					})
			});
	};
}
