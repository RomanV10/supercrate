describe('Storages: Create storage modal controller', () => {
	let $controller, $rootScope, $scope, CreateStorageController, testHelper;
	let spySweetAlert, $uibModalInstance;

	beforeEach(inject((_$controller_, _$rootScope_, _testHelperService_, _SweetAlert_) => {
		$controller = _$controller_;
		$rootScope = _$rootScope_;
		testHelper = _testHelperService_;
		spySweetAlert = spyOn(_SweetAlert_, 'swal');
		$scope = $rootScope.$new();
		let requests = testHelper.loadJsonFile('create-storage-controller-requests.mock');
		let request = testHelper.loadJsonFile('create-storage-controller-request.mock');

		$uibModalInstance = {
			close: jasmine.createSpy('modalInstance.close'),
			dismiss: jasmine.createSpy('modalInstance.dismiss'),
			result: {
				then: jasmine.createSpy('modalInstance.result.then')
			}
		};

		CreateStorageController = $controller('CreateStorageController', {$scope, $uibModalInstance, requests, request})
	}));

	describe('Init CreateStorageController', () => {
		it('controller should be defined', () => {
			expect(CreateStorageController).toBeDefined();
		});
	});

	describe('When storage recurring turn on,', () => {
		beforeEach(() => {
			$scope.data.recurring.start = 1;
		});

		describe(' and status pending was changed on move in,', () => {
			it('should return undefined', () => {
				// Given
				$scope.data.rentals.status_flag = "1";

				// When
				let result = $scope.changeField('status_flag', "2");

				// Then
				expect(result).toBeUndefined();
			});
		});

		describe(' and status move in was changed on pending,', () => {
			let result;

			beforeEach(() => {
				// Given
				$scope.basicData.status_flag = "2";
				$scope.data.rentals.status_flag = "2";
			});

			it('should return false', () => {
				// When
				result = $scope.changeField('status_flag', "1");

				// Then
				expect(result).toBe(false);
			});

			it('should call modal', () => {
				// When
				result = $scope.changeField('status_flag', "1");

				// Then
				expect(spySweetAlert).toHaveBeenCalled();
			});

			it('should show modal and if confirm turn off recurring', () => {
				// Given
				spySweetAlert.and.callFake((options, callback) => {
					callback(true);
				});

				// When
				result = $scope.changeField('status_flag', "1");

				// Then
				expect($scope.data.recurring.start).toBe(0);
			});

			it('should show modal and if cancel get back status flag', () => {
				// Given
				spySweetAlert.and.callFake((options, callback) => {
					callback(false);
				});

				// When
				result = $scope.changeField('status_flag', "1");

				// Then
				expect($scope.data.rentals.status_flag).toBe("2");
			})
		});
	});

	describe('When close modal,', () => {
		it('and not make changes should not display alert', () => {
			// Given

			// When
			$scope.closeModal();

			// Then
			expect($uibModalInstance.dismiss).toHaveBeenCalled();
		});

		describe('and make changes,', () => {
			it('should display alert', () => {
				// Given
				$scope.changeField('status_flag', "2");

				// When
				$scope.closeModal();

				// Then
				expect(spySweetAlert).toHaveBeenCalled();
			});
		});
	})
});
