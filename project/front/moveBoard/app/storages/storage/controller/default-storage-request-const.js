export const DEFAULT_STORAGE_REQUEST = {
	user_info: {
		name: '',
		address: '',
		zip: '',
		state: '',
		city: '',
		phone1: '',
		phone2: '',
		email: ''
	},
	rentals: {
		storage: '',
		lot_number: [],
		location: '',
		moved_in_date: '',
		moved_out_date: '',
		move_request_id: '',
		volume_cuft: 0,
		total_monthly: 0,
		tax_rate: 0,
		rate_per_cuft: 0,
		late_fee: 0,
		apply_late_fee_in: 0,
		status_flag: '1',
		min_cuft: 0
	},
	recurring: {
		start: 0,
		start_point: '',
		how_often: 3,
		how_many: '',
		next_run_on: '',
		invoice_description: '',
		auto_send: 0,
		start_date: 0,
		next_run_on_fee: 0,
		created: true,
		started: false
	}
};