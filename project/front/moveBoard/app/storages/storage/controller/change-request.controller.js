'use strict';

angular
	.module('app.storages')
	.controller('changeRequestCtrl', changeRequestCtrl);

changeRequestCtrl.$inject = ['$scope', 'data', '$uibModalInstance', '$rootScope', 'RequestServices', 'CalculatorServices', 'DispatchService', 'SweetAlert', 'StorageService'];

function changeRequestCtrl($scope, data, $uibModalInstance, $rootScope, RequestServices, CalculatorServices, DispatchService, SweetAlert, StorageService) {
	$scope.buttonText = 'Search';
	$scope.requestId = '';
	
	$scope.closeModal = closeModal;
	$scope.search = search;
	$scope.changeIdValue = changeIdValue;
	
	function closeModal() {
		$uibModalInstance.dismiss('cancel');
	}
	
	function search(inputValue, type) {
		if (type == 'Search') {
			if (inputValue === false) return false;
			if (inputValue === "") {
				toastr.warning("You need to write request ID!");
				return false
			}
			$scope.busy = true;
			
			RequestServices.getRequestsByNid(inputValue).then(function (response) {
				if (_.get(response.nodes[0], 'request_all_data.storage_request_id')) return;
				
				if (!_.isEmpty(response.nodes)) {
					let requestData = _.head(response.nodes);
					$scope.changedRequest = _.head(response.nodes);
					data.user_info = {
						name: requestData.name,
						zip: requestData.field_moving_to.postal_code,
						zip_from: requestData.field_moving_from.postal_code,
						state: requestData.field_moving_to.administrative_area,
						city: requestData.field_moving_to.locality,
						phone1: requestData.uid.field_primary_phone,
						phone2: requestData.uid.field_user_additional_phone,
						email: requestData.uid.mail,
						uid: requestData.uid.uid,
						realRequest: true
					};
					
					if (requestData.field_additional_user) {
						data.field_additional_user = requestData.field_additional_user;
					}
					
					if(response.nodes["0"].field_moving_to.thoroughfare) {
						data.user_info.address = response.nodes["0"].field_moving_to.thoroughfare;
					} else if(response.nodes["0"].apt_to.value) {
						data.user_info.address = response.nodes["0"].apt_to.value;
					}
					
					data.rentals.move_request_id = Number(inputValue);
					data.rentals.moved_in_date = new Date(requestData.date.value);
					data.rentals.volume_cuft = CalculatorServices.getTotalWeight(requestData).weight;
					
					if (_.isEqual(requestData.storage_id, 0)) {
						$scope.pairedRequest = undefined;
					} else {
						let promise = RequestServices.getRequestsByNid(requestData.storage_id);
						promise.then(function (req) {
							let request = _.head(req.nodes);
							if (!request.request_all_data.toStorage) {
								data.user_info.zip_from = request.field_moving_from.postal_code;
								data.rentals.moved_in_date = new Date(request.date.value);
								data.rentals.moved_out_date = new Date(requestData.date.value);
							} else {
								data.user_info.zip = request.field_moving_to.postal_code;
								data.user_info.state = request.field_moving_to.administrative_area;
								data.user_info.city = request.field_moving_to.locality;
								data.rentals.moved_out_date = new Date(request.date.value);
								if (request.field_moving_to.thoroughfare)
									data.user_info.address = request.field_moving_to.thoroughfare;
								if (request.apt_to.value)
									data.user_info.address += ' ' + request.apt_to.value;
							}
						}, () => {
							$scope.busy = false;
						});
					}
					if (!data.recurring.start) {
						let dataRecurring = {
							start: data.recurring.start || 0,
							start_point: data.rentals.moved_in_date,
							how_often: 3,
							how_many: data.rentals.moved_out_date,
							next_run_on: StorageService.convertTimeToUtc(moment(data.rentals.moved_in_date).format('MM/DD/YYYY')),
							invoice_description: "",
							auto_send: data.recurring.auto_send || 0,
							start_date: StorageService.convertTimeToUtc(moment(data.rentals.moved_in_date).format('MM/DD/YYYY')),
							next_run_on_fee: StorageService.convertTimeToUtc(moment(data.rentals.moved_in_date).format('MM/DD/YYYY')),
							created: true,
							started: false
						};
						data.recurring = dataRecurring;
					}
				} else {
					$scope.buttonText = "Set anyway";
					$scope.busy = false;
				}
				return DispatchService.getRequestContract(inputValue);
			}).then(response => {
					
					if (_.isUndefined(response)) {
						$scope.busy = false;
						SweetAlert.swal('Failed!', 'Storage have been created for this request', 'error');
						return;
					}
					
					if(_.get(response, 'factory.agreement.address')) {
						data.user_info.address = response.factory.agreement.address;
					}
					$rootScope.$broadcast('change.reqID', data, $scope.changedRequest);
					$scope.busy = false;
					toastr.success('Request ID was successfully changed', 'Success');
					closeModal();
				})
				.catch(() =>  {
					SweetAlert.swal('Failed!', 'Error of changing request id', 'Error');
					$scope.busy = false;
				});
		} else {
			//set request id custom
			data.rentals.move_request_id = Number(inputValue);
			data.user_info.realRequest = false;
			$scope.busy = false;
			closeModal();
		}
	}
	
	function changeIdValue() {
		$scope.buttonText = 'Search';
	}
}
