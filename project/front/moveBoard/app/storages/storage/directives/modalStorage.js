'use strict';
angular.module('app.storages')
	.directive('modalStorage', function () {
		return {
			restrict: 'E',
			replace: true,
			template: require('../templates/addStorageModal.html'),
		};
	});
