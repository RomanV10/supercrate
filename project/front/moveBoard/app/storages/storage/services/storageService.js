'use strict';

angular.module('app.storages')
	.factory('StorageService', StorageService);

/*@ngInject*/
function StorageService($http, $q, config, $uibModal, RequestServices, SweetAlert, $rootScope, apiService,
	moveBoardApi) {

	function createWorker(request) {
		return apiService.postData(moveBoardApi.client.updateClient, request);
	}

	function getStorageList() {
		return apiService.getData(moveBoardApi.storages.getList);
	}

	function getStorageNameList() {
		return apiService.getData(moveBoardApi.storages.getNameList);
	}

	function addStorage(data) {
		return apiService.postData(moveBoardApi.storages.addStorage, data);
	}

	function getStorageByID(id) {
		return apiService.getData(moveBoardApi.storages.getStorage, {id});
	}

	function getDefaultStorage() {
		return apiService.postData(moveBoardApi.storages.getDefaultStorage);
	}

	function updateStorage(id, data) {
		data.id = id;
		return apiService.putData(moveBoardApi.storages.updateStorage, data);
	}

	function deleteStorage(id) {
		return apiService.delData(moveBoardApi.storages.updateStorage, {id});
	}

	function getReqStorage(flag) {
		return apiService.postData(moveBoardApi.storages.getStorageRequests, {flag});
	}

	function createStorageReq(data) {
		return apiService.postData(moveBoardApi.storages.createStorageRequest, data);
	}

	function getStorageAging() {
		return apiService.postData(moveBoardApi.storages.getStorageAging);
	}

	function getRecurringRevenue(data) {
		return apiService.postData(moveBoardApi.storages.getRecurringRevenue, data);
	}

	function convertTimeToUtc(date) {
		return moment.utc(date, 'MM/DD/YYYY').startOf('day').unix();
	}

	function checkField(field) {
		let fields = ['uid', 'status_flag', 'realRequest', 'lot_number'];
		return _.indexOf(fields, field) == -1;
	}

	function createBasicLog(request) {
		let msgLog = [];
		_.forEach(request.data, function (obj, key) {
			if (key != 'recurring') {
				_.forEach(obj, function (val, field) {
					if (_.isNumber(val)) {
						val = val.toString();
					}

					if (val && checkField(field)) {
						if (field == 'moved_in_date' || field == 'moved_out_date') {
							msgLog.push({
								text: field.replace(/_/g, ' ').charAt(0).toUpperCase() + field.replace(/_/g, ' ').slice(
									1) + ': ' + moment.unix(val).format('MMM DD, YYYY')
							});
						} else {
							msgLog.push({
								text: field.replace(/_/g, ' ').charAt(0).toUpperCase() + field.replace(/_/g, ' ').slice(
									1) + ': ' + val
							});
						}
					}
				});
			}
		});

		return msgLog;
	}

	function createStorageReqObj(requestFrom, dateIn, dateTo, weight, request, nid, typeReq) {
		let defer = $q.defer();
		if (!request) {
			request = requestFrom;
		}
		let reqId = requestFrom.nid;
		if (nid) {
			reqId = nid;
		}
		let type = typeReq;
		let data = {
			data: {
				user_info: {
					name: request.name,
					address: '',
					rental_address: '',
					zip: request.field_moving_to.postal_code,
					zip_from: requestFrom.field_moving_from.postal_code,
					state: request.field_moving_to.administrative_area,
					city: request.field_moving_to.locality,
					phone1: request.uid.field_primary_phone,
					phone2: request.uid.field_user_additional_phone,
					email: request.uid.mail,
					uid: request.uid.uid,
					realRequest: true
				},
				rentals: {
					storage: '',
					lot_number: [],
					location: '',
					moved_in_date: convertTimeToUtc(dateIn),
					moved_out_date: convertTimeToUtc(dateTo || dateIn),
					move_request_id: Number(reqId),
					volume_cuft: weight,
					total_monthly: 0,
					tax_rate: 0,
					rate_per_cuft: 0,
					late_fee: 0,
					apply_late_fee_in: 0,
					status_flag: 1,
					min_cuft: 0
				},
				recurring: {
					start: 0,
					start_point: convertTimeToUtc(dateIn),
					how_often: 3,
					how_many: convertTimeToUtc(dateTo || dateIn),
					next_run_on: convertTimeToUtc(dateIn),
					next_run_on_fee: convertTimeToUtc(dateIn),
					invoice_description: '',
					auto_send: 0,
					start_date: convertTimeToUtc(dateIn),
					created: true,
					started: false
				}
			}
		};

		if (request.address) {
			data.data.user_info.address = request.address;
		} else if (request.field_moving_to.thoroughfare) {
			data.data.user_info.address = request.field_moving_to.thoroughfare;
		} else if (request.apt_to.value) {
			data.data.user_info.address = request.apt_to.value;
		}

		getDefaultStorage()
			.then(function (res) {
				let storage = res.data;
				if (!storage.id) {
					SweetAlert.swal({
						title: 'Default Storage doesn\'t exist',
						text: 'Please, create of mark Default Storage',
						type: 'error'
					});
					$rootScope.$broadcast('storage.created.error', 'Default Storage don\'t exist');
					defer.reject();
					return false;
				}
				data.data.rentals.storage = storage.id.toString();
				data.data.rentals.tax_rate = storage.value.tax;
				data.data.rentals.rate_per_cuft = storage.value.rate_per_cuft;
				data.data.rentals.late_fee = storage.value.late_fee;
				data.data.rentals.apply_late_fee_in = storage.value.apply_late_fee_in;
				data.data.rentals.min_cuft = storage.value.min_cuft;
				if (Number(storage.value.min_cuft) < Number(data.data.rentals.volume_cuft)) {
					data.data.rentals.total_monthly = data.data.rentals.volume_cuft * data.data.rentals.rate_per_cuft;
				} else {
					data.data.rentals.total_monthly = Number(storage.value.min_cuft) * data.data.rentals.rate_per_cuft;
				}
				createStorageReq(data)
					.then(function (res) {
						let id = res.data;
						let arr = createBasicLog(data);
						let message = 'Storage Request created';
						toastr.success('Storage Request has been created');
						if (type == 'move') {
							openModal(data.data, _.head(id));
							message = 'Storage Request created from Request settings';
							if (!_.isEmpty(arr)) {
								RequestServices.sendLogs(arr, message, request.nid, 'MOVEREQUEST');
							}
							if (!_.isEmpty(arr)) {
								RequestServices.sendLogs(arr, message, _.head(id), 'STORAGEREQUEST');
							}
						} else {
							message = 'Storage Request created from Lease agreement';
							if (!_.isEmpty(arr)) {
								RequestServices.sendLogs(arr, message, request.nid, 'MOVEREQUEST')
									.then(
										(data) => RequestServices.sendLogs(arr, message, _.head(id), 'STORAGEREQUEST'))
									.catch((err) => SweetAlert.swal('Error', 'Error of sending logs', 'error'));
							}
						}
						if (request.nid != requestFrom.nid) {
							if (!request.request_all_data) request.request_all_data = {};
							request.request_all_data.storage_request_id = _.head(id);
							RequestServices.saveReqData(request.nid, request.request_all_data)
								.then(() => {
									$rootScope.$broadcast('storage.request.created', _.head(id), data);
									defer.resolve();
								})
								.catch(() => {
									SweetAlert.swal('Error of saving request data', 'error');
									defer.reject();
								});
						} else {
							$rootScope.$broadcast('storage.request.created', _.head(id), data);
							defer.resolve();
						}
					})
					.catch(function (err) {
						$rootScope.$broadcast('storage.created.error', err);
						defer.reject();
					});
			});
		return defer.promise;
	}

	function updateStorageReq(data, id) {
		data.id = id;
		return apiService.putData(moveBoardApi.storages.updateStorageRequest, data);
	}

	function getStorageReqByID(id) {
		return apiService.getData(moveBoardApi.storages.getStorageRequest, {id});
	}

	function removeReqByID(id) {
		return apiService.delData(moveBoardApi.storages.getStorageRequest, {id});
	}

	function paymentByDate(data) {
		return apiService.postData(moveBoardApi.storages.getStorageReceipts, data);
	}

	function createModal(requests) {
		var modalInstance = $uibModal.open({
			template: require('../../pending/templates/pendingStorageModal.html'),
			controller: 'CreateStorageController',
			controllerAs: 'modalSave',
			windowClass: 'pendingModal storageModal requestModal status_1',
			size: 'lg',
			backdrop: false,
			keyboard: false,
			resolve: {
				requests: function () {
					return requests;
				},
				request: function () {
					return undefined;
				}
			}
		});

		modalInstance
			.result
			.then(function (modalResult) {
				// Do something with modal result if you want
			})
			.finally(function () {
				modalInstance.$destroy();
				// After calling this, the next time open is called, everything will be re-initialized.
			});
	}

	function openModal(request, id, requests) {
		let req = {
			data: request,
			id: id
		};

		var modalInstance = $uibModal.open({
			template: require('../../pending/templates/pendingStorageModal.html'),
			controller: 'CreateStorageController',
			windowClass: 'pendingModal storageModal requestModal status_' + request.rentals.status_flag,
			backdrop: false,
			keyboard: false,
			size: 'lg',
			resolve: {
				requests: function () {
					return requests;
				},
				request: function () {
					return req;
				}
			}
		});

		modalInstance
			.result
			.then(function (modalResult) {
				// Do something with modal result if you want
			})
			.finally(function () {
				modalInstance.$destroy();
				// After calling this, the next time open is called, everything will be re-initialized.
			});
	}

	function getStorageInvoices(data) {
		return {invoices: apiService.postData(moveBoardApi.storageInvoices.getStorageInvoices, data)};
	}

	return {
		createWorker: createWorker,
		getStorageList: getStorageList,
		getStorageNameList: getStorageNameList,
		addStorage: addStorage,
		getStorageByID: getStorageByID,
		getDefaultStorage: getDefaultStorage,
		updateStorage: updateStorage,
		deleteStorage: deleteStorage,
		getReqStorage: getReqStorage,
		createStorageReq: createStorageReq,
		createStorageReqObj: createStorageReqObj,
		updateStorageReq: updateStorageReq,
		getStorageReqByID: getStorageReqByID,
		openModal: openModal,
		createModal: createModal,
		removeReqByID: removeReqByID,
		paymentByDate: paymentByDate,
		getStorageAging: getStorageAging,
		getRecurringRevenue: getRecurringRevenue,
		convertTimeToUtc: convertTimeToUtc,
		createBasicLog: createBasicLog,
		getStorageInvoices: getStorageInvoices
	};
}
