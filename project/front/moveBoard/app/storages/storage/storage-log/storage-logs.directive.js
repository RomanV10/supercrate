import './storage-logs.styl';

'use strict';

(function () {

	angular.module('app.storages').directive('storageLogs', storageLogs);

	storageLogs.$inject = [];

	function storageLogs() {
		return {
			restrict: 'E',
			template: require('./storage-logs.pug'),
			scope: {
				logs: '='
			},
			link: storageLink
		}

		function storageLink($scope) {
			
			$scope.storageLogs = angular.copy($scope.logs).reverse();

			angular.forEach($scope.storageLogs, function (log) {
				log.date = moment.unix(log.date).format('MMM DD, YYYY h:mm a');
				log.details = log.details[0];
				log.content = log.details.text[0].text;
			})
		}
	}
})();
