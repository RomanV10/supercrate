'use strict';

angular.module('app.storages').controller('StorageInvoicesController', StorageInvoicesController);

StorageInvoicesController.$inject = ['$scope', 'StorageService', '$q', '$timeout'];

function StorageInvoicesController($scope, StorageService, $q, $timeout) {
	const STATUSES = [
		{enum: 0, name: 'draft'},
		{enum: 1, name: 'sent'},
		{enum: 2, name: 'viewed'},
		{enum: 3, name: 'paid'},
		{enum: 4, name: 'autopaid'},
		{enum: 5, name: 'failed'},
	];

	$scope.sorting = 'id';
	$scope.direction = 'DESC';
	$scope.page = 0;

	$scope.busy = true;

	$scope.openRequest = openRequest;
	$scope.getNextPage = getNextPage;
	$scope.getPreviousPage = getPreviousPage;
	$scope.getDate = getDate;
	$scope.getStatus = getStatus;

	function init() {
		getInvoices($scope.sorting, $scope.direction, $scope.page);
	}

	function openRequest (id) {
		StorageService.getStorageReqByID(id).then(function ({data}) {
			var request = data;
			StorageService.openModal(request, id);
		});
	}

	function getNextPage () {
		$scope.page++;
		getInvoices($scope.sorting, $scope.direction, $scope.page);
	}

	function getPreviousPage () {
		$scope.page--;
		getInvoices($scope.sorting, $scope.direction, $scope.page);
	};

	function getInvoices(sort, direction, page) {
		var data = {
			sort: sort,
			direction: direction,
			page: page

		};
		$scope.invoices = [];

		var promise = StorageService.getStorageInvoices(data);

		$q.all(promise).then(function (resolve) {
			$scope.meta = resolve.invoices.data._meta;
			angular.forEach(resolve.invoices.data.items, function (item) {
				$scope.invoices.push({
					client_name: item.client_name,
					description: item.description,
					id: item.id,
					invoice_date: getDate(item.invoice_date),
					status: getStatus(Number(item.status)),
					storage_request_id: item.storage_request_id,
					total: _.round(item.total, 2),
					rental: {}
				})
			});
			$timeout(function () {
				$scope.busy = false;
			}, 250);
		})
	}

	function getDate(date) {
		let isNotTimeStamp = date instanceof Date;
		if (isNotTimeStamp) {
			return moment(date).format('MMM DD, YYYY');
		}

		let timeStamp = Number(date);
		return moment.unix(timeStamp).format('MMM DD, YYYY');
	}

	function getStatus(status) {
		let result = _.find(STATUSES, {enum: Number(status)});
		return result.name;
	}

	$scope.$watchCollection(function () {
		return [$scope.sorting, $scope.direction];
	}, function (newVal, oldVal) {
		if (newVal != oldVal) {
			getInvoices($scope.sorting, $scope.direction, $scope.page);
		}
	});

	init();
}
