describe('Storages: storage invoices', () => {
	let $controller, $rootScope, $scope, StorageInvoicesController;
	
	beforeEach(inject((_$controller_, _$rootScope_) => {
		$controller = _$controller_;
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		StorageInvoicesController = $controller('StorageInvoicesController', {
			$scope: $scope
		})
	}));
	
	describe('Init StorageInvoicesController', () => {
		it('controller should be defined', () => {
			expect(StorageInvoicesController).toBeDefined();
		});
	});
	
	describe('Get correct date', () => {
		it('should get "Nov 16, 2017"', () => {
			expect($scope.getDate('1510851882')).toBe('Nov 16, 2017');
			expect($scope.getDate(1510851882)).toBe('Nov 16, 2017');
			expect($scope.getDate(new Date('2017-11-16'))).toBe('Nov 16, 2017');
		});
	});
	
	describe('Get correct status', () => {
		it('should be "draft"', () => {
			expect($scope.getStatus(0)).toBe('draft');
		});
		it('should be "sent"', () => {
			expect($scope.getStatus(1)).toBe('sent');
		});
		it('should be "viewed"', () => {
			expect($scope.getStatus(2)).toBe('viewed');
		});
		it('should be "paid"', () => {
			expect($scope.getStatus(3)).toBe('paid');
		});
		it('should be "autopaid"', () => {
			expect($scope.getStatus(4)).toBe('autopaid');
		});
		it('should be "failed"', () => {
			expect($scope.getStatus(5)).toBe('failed');
		});
	});
});