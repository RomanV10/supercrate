(function () {
    'use strict';
    angular.module('app.storages')
        .controller('TenantsController', ['$scope', 'StorageService', 'SweetAlert', 'DTOptionsBuilder', 'PaymentServices',function ($scope, StorageService, SweetAlert, DTOptionsBuilder, PaymentServices) {

            $scope.serviceTypes = {
                2: 'Move in',
                3: "Move out",
	            4: 'Move out Pending'
            };
            $scope.storageFlag = '2';
            $scope.clicked = '';
            $scope.busy = true;
            $scope.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('bFilter', false)
                .withOption('aaSorting', [0, 'desc'])
                .withDisplayLength(25);
            $scope.refreshPage = function () {
                StorageService.getReqStorage($scope.storageFlag).then(function ({data}) {
                    $scope.clicked = '';
                    $scope.requests = data;
                    $scope.busy = false;
                });
            };
            $scope.refreshPage();
            $scope.changeFlag = changeFlag;
            $scope.openModal = openModal;
            $scope.dateFormatter = dateFormatter;
            $scope.calculatePayments = calculatePayments;

            function changeFlag() {
                $scope.busy = true;
                StorageService.getReqStorage($scope.storageFlag).then(function ({data}) {
                    $scope.requests = data;
                    $scope.busy = false;
                });
            }

            function openModal(request, id){
                if($scope.clicked != id) {
                    $scope.clicked = id;
                    return false;
                }
                StorageService.openModal(request, id, $scope.requests);
            }


            function dateFormatter(date){
                return moment(moment.utc(moment.unix(date)).format("DD-MM-YYYY"), "DD-MM-YYYY").format("MMM DD, YYYY");
            }

            function calculatePayments(request) {
                return PaymentServices.calcPayment(request.payments) - PaymentServices.calcInvoices(request.invoices);
            }

        }]);
})();
