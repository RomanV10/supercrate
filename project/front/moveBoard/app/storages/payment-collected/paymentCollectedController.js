'use strict';
angular
	.module('app.storages')
	.controller('PaymentCollectedController', PaymentCollectedController);

PaymentCollectedController.$inject = ['$scope', 'StorageService', 'DTOptionsBuilder'];

function PaymentCollectedController($scope, StorageService, DTOptionsBuilder) {
	$scope.busy = true;
	$scope.requests = [];
	$scope.dateFrom = moment().startOf('month').toDate();
	$scope.dateTo = moment().endOf('month').toDate();
	$scope.dateFromOptions = {
		maxDate: $scope.dateTo,
	};
	$scope.dateToOptions = {
		minDate: $scope.dateFrom,
	};
	$scope.totalAmount = 0;
	$scope.clicked = '';
	$scope.dtOptions = DTOptionsBuilder
		.newOptions()
		.withOption('bFilter', false)
		.withOption('aaSorting', [6, 'desc'])
		.withDisplayLength(25);

	$scope.changeDate = () => {
		$scope.dateToOptions.minDate = $scope.dateFrom;
		$scope.dateFromOptions.maxDate = $scope.dateTo;
		$scope.getPayments();
	};

	$scope.getPayments = function () {
		$scope.busy = true;
		var data = {
			'dateto': moment($scope.dateTo).format('YYYY-MM-DD'),
			'datefrom': moment($scope.dateFrom).format('YYYY-MM-DD')
		};

		StorageService.paymentByDate(data)
			.then(function ({data}) {
				$scope.clicked = '';
				$scope.totalAmount = 0;
				$scope.requests = [];

				_.forEach(data, function (item, key) {
					$scope.requests.push(item);

					if (item.receipt.refunds) {
						$scope.totalAmount -= Number(item.receipt.amount) || 0;
					} else if (!item.receipt.pending) {
						$scope.totalAmount += Number(item.receipt.amount) || 0;
					}
				});

				$scope.busy = false;
			});
	};

	$scope.getPayments();
	$scope.openStorageRequest = function (item_id, id) {
		if ($scope.clicked != item_id) {
			$scope.clicked = item_id;
			return false;
		} else {
			$scope.busy = true;
			StorageService.getStorageReqByID(id)
				.then(function ({data}) {
					$scope.busy = false;
					StorageService.openModal(data, id);
				});
		}
	};

	$scope.dateFormatter = function (date) {
		return moment(moment.utc(moment.unix(date)).format('DD-MM-YYYY'), 'DD-MM-YYYY').format('MMM DD, YYYY');
	};

}
