(function(){
    'use strict';
    angular.module('app.storages')
        .controller('RecurringRevenueController', ['$scope', '$uibModal', 'StorageService', 'SweetAlert', 'DTOptionsBuilder', function($scope, $uibModal, StorageService, SweetAlert, DTOptionsBuilder){

            $scope.clicked = '';
            $scope.busy = true;
            $scope.howOften = {
                0: 'Week',
                1: 'Two week',
                2: 'Tree week',
                3: 'Month',
                4: 'Two month',
                5: 'Tree month',
                6: 'Four month',
                7: 'Half year'
            };
            $scope.serviceTypes = {
                1: 'Pending',
                2: 'Move in',
                3: "Move out",
	            4: 'Move out Pending'
            };
            $scope.dtOptions = DTOptionsBuilder
                .newOptions()
                .withOption('aaSorting', [0, 'desc'])
                .withOption('bFilter', false)
                .withOption('paging', false);

            $scope.refreshPage = function () {
                $scope.busy = true;
                $scope.recurringData = {};
                StorageService.getRecurringRevenue().then(function ({data}) {
                    _.forEach(data, function (requests, date) {
                        _.forEach(requests, function (request) {
                            request.id_table = request.request_storage_id+'_'+date;
                        });
                    });
                    $scope.recurringData = data;
                    $scope.clicked = '';
                    $scope.busy = false;
                });
            };
            $scope.refreshPage();

            $scope.openStorageRequest = function(id){
                if($scope.clicked != id) {
                    $scope.clicked = id;
                    return false;
                }
                $scope.busy = true;
                StorageService.getStorageReqByID(id).then(function ({data}) {
                    $scope.busy = false;
                    StorageService.openModal(data, id);
                });
            };

            $scope.calcTotalMonthly = function (requests) {
                var total = 0;
                _.forEach(requests, function (item) {
                    total += _.round(item.total_monthly, 2);
                });

                return total.toFixed(2);
            };

            $scope.dateFormatter = function (date) {
                return moment(date, 'YYYY-MM').format('MMMM YYYY');
            };

            $scope.dateFormatterUnix = function(date){
                return moment(moment.utc(moment.unix(date)).format("DD-MM-YYYY"), "DD-MM-YYYY").format("MMM DD, YYYY");
            };

        }]);
})();
