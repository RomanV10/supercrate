(function(){

    angular.module("app.storages")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('storages', {
                        url: '/storages',
                        abstract: true,
                        template: '<ui-view/>'
                    })
                    .state('storages.storage', {
                        url: '/storage',
	                    template: require('./storage/templates/storage.html'),
                        title: 'Storage',
                        controller: 'StorageController',
                        controllerAs: 'storage',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('storages.pending', {
                        url: '/pending',
	                    template: require('./pending/templates/pending.html'),
                        title: 'Pending Storage',
                        controller: 'PendingController',
                        controllerAs: 'pending',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('storages.tenants', {
                        url: '/tenants',
	                    template: require('./tenants/templates/tenants.html'),
                        title: 'Storage Tenants',
                        controller: 'TenantsController',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('storages.aging', {
                        url: '/aging',
	                    template: require('./aging/templates/aging.html'),
                        title: 'Storage Aging',
                        controller: 'AgingController',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('storages.recurring', {
                        url: '/recurring-revenue',
	                    template: require('./recurring-revenue/templates/recurring-revenue.html'),
                        title: 'Storage Recurring Revenue',
                        controller: 'RecurringRevenueController',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('storages.payment_collected', {
                        url: '/payment-collected',
	                    template: require('./payment-collected/templates/payment-collected.html'),
                        title: 'Storage Payment Collected',
                        controller: 'PaymentCollectedController',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('storages.invoices', {
                        url: '/invoices',
	                    template: require('./invoices/templates/invoices.html'),
                        title: 'Invoices',
                        controller: 'StorageInvoicesController',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });
            }
        ]
    );
})();
