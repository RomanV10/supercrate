'use strict';

angular
	.module('app.storages')

	.factory('StoragePaymentService', StoragePaymentService);

StoragePaymentService.$inject = ['$http', '$rootScope', '$q', 'config', '$uibModal', 'SettingServices', 'StorageService',
	'RequestServices', 'common', 'SweetAlert', 'PaymentServices', 'InvoiceServices', '$timeout'];

function StoragePaymentService($http, $rootScope, $q, config, $uibModal, SettingServices, StorageService, RequestServices, common, SweetAlert, PaymentServices, InvoiceServices, $timeout) {
	var service = {};

	service.openPaymentModal = openPaymentModal;

	return service;

	function openPaymentModal(request, terms, entity) {

		var modalInstance = $uibModal.open({
			template: require('../../../requests/directives/payment/paymentModal.html'),
			controller: PaymentModalCtrl,
			size: 'lg',
			resolve: {
				request: function () {
					return request;
				},
				terms: function () {
					return terms;
				},
				entity: function () {
					return entity;
				}
			}
		});
		modalInstance.result.then(function ($scope) {
		});
	}

	function PaymentModalCtrl($scope, $uibModalInstance, request, terms, entity, datacontext, InvoiceServices) {
		var STORAGEREQUEST = 1;
		$scope.invoices = [];
		$scope.busy = true;
		$scope.receiptLoader = false;
		$scope.entity_type = STORAGEREQUEST;
		$scope.entitytype = STORAGEREQUEST;
		$scope.request = request.data;
		$scope.request.nid = request.id;
		$scope.activeRow = -1;
		$scope.activeReceiptId = '';
		let searchPayment = false;
		$scope.fieldData = datacontext.getFieldData();
		$scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);
		$scope.receipts = _.isEmpty(request.data.payments) ? [] : request.data.payments;
		$scope.terms = terms;
		$scope.entity = entity;

		function sortRefundReceipts() {
			var refundsArr = [];
			_.forEachRight($scope.receipts, function (receipt, ind) {
				if(receipt.refunds){
					refundsArr.push(receipt);
					$scope.receipts.splice(ind, 1);
				}
			});
			_.forEach(refundsArr, function (refund) {
				var ind = _.findIndex($scope.receipts, {id: refund.receipt_id.toString() });
				$scope.receipts.splice(ind + 1, 0,refund);
			});
			$scope.busy = false;
			$scope.receiptLoader = false;
		}
		sortRefundReceipts();
		$scope.prepareToDelete = function(index, id){
			$scope.activeRow = index;
			$scope.activeReceiptId = id;
		};

		$scope.customType = function (receipt) {
			switch (receipt.type) {
			case 'cash':
				return '(cash)';
				break;
			case 'check':
				return '(check)';
				break;
			case 'creditcard':
				return '(card)';
				break;
			case 'customrefund':
				return '(custom refund)';
				break;
			default:
				return 'Payment';
			}
		};

		$scope.dateFormat = function (date) {
			return _.isNaN(moment(convertToYourTimeZone(date))._i) ? moment(date).format('DD MMM, YYYY') : moment(convertToYourTimeZone(date)).format('DD MMM, YYYY h:mm a');
		};

		$scope.removeReceipt = function (){
			let ind = _.findIndex($scope.receipts, {id: $scope.activeReceiptId});
			if (ind > -1 && $scope.receipts[ind].transaction_id != 'Custom Payment') {
				SweetAlert.swal('Error!', 'You can\'t remove payment!\n In this case, please use button \'Refund\'', 'error');
				return false;
			}
			SweetAlert.swal({
				title: 'Are you sure you want to delete receipt ?',
				text: 'It will be deleted permanently',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',confirmButtonText: 'Delete!',
				cancelButtonText: 'No',
				closeOnConfirm: true,
				closeOnCancel: true },
			function(isConfirm){
				if (isConfirm) {
					var id = $scope.receipts[ind].id;
					PaymentServices.delete_receipt(id).then(function(data){
						var message = 'Payment in the amount  $' + $scope.receipts[ind].amount + ' was removed.';
						var obj = {
							simpleText: message
						};
						var arr = [obj];
						RequestServices.sendLogs(arr, 'Payment removed', $scope.request.nid, 'STORAGEREQUEST');
						$scope.receipts.splice(ind, 1);
						$scope.activeRow = -1;
						$scope.activeReceiptId = '';
					}, function(reason){
						SweetAlert.swal('Failed!', reason, 'error');
					});
					$rootScope.$broadcast('request.payment_receipts',$scope.receipts, $scope.request.nid);
				}
			});
		};

		$scope.showReceipt = function(id){
			var idx = _.findIndex($scope.receipts, {id: id});
			PaymentServices.openReceiptModal(request, $scope.receipts[idx], undefined, $scope.entity);
		};

		$scope.addAuthPayment = function(){
			request.reservation = false;
			PaymentServices.openAuthPaymentModal($scope.request, undefined, $scope.receipts, $scope.entity_type);
		};
		$scope.addCustomPayment = function () {
			var reqData = request.data;
			reqData.nid = request.data.nid;
			reqData.receipts = reqData.payments;

			PaymentServices.openCustomPaymentModal(reqData, $scope.entity_type);
		};

		function convertToYourTimeZone(date){
			return moment(moment.unix(date).utcOffset(0).format('YYYY-MM-DD'), 'YYYY-MM-DD').toDate();
		}

		$scope.proRate = function () {
			var charge = {
				cost:'',
				description:'',
				name: 'Storage Pro-Rate',
				qty: 1,
				tax: 0
			};
			var dateIn, dateOut, days;
			if($scope.request.rentals.moved_in_date)
				dateIn = moment(convertToYourTimeZone($scope.request.rentals.moved_in_date)).format('MMMM DD, YYYY');
			if(moment(dateIn).month() < moment().month() && moment(dateIn).year() <= moment().year())
				dateIn = moment().startOf('month').format('MMMM DD, YYYY');
			dateOut = moment(dateIn).endOf('month').format('MMMM DD, YYYY');
			if($scope.request.rentals.moved_out_date){
				var dateEnd = moment(convertToYourTimeZone($scope.request.rentals.moved_out_date)).format('MMMM DD, YYYY');
				if(moment(dateEnd).diff(moment(dateOut), 'day') < 0)
					dateOut = dateEnd;
			}
			if(moment(convertToYourTimeZone($scope.request.rentals.moved_in_date)).format('MMMM DD, YYYY') == moment(convertToYourTimeZone($scope.request.rentals.moved_out_date)).format('MMMM DD, YYYY')){
				dateIn = moment(convertToYourTimeZone($scope.request.rentals.moved_in_date)).format('MMMM DD, YYYY');
				dateOut = moment(convertToYourTimeZone($scope.request.rentals.moved_out_date)).format('MMMM DD, YYYY');
			}
			charge.description = 'Pro rate ' + moment(dateIn).format('DD MMMM') + ' till ' + moment(dateOut).format('DD MMMM');
			days = moment(dateOut).diff(moment(dateIn), 'day') + 1;
			charge.cost = (_.round(Number($scope.request.rentals.total_monthly) / 30, 2) * days).toFixed(2);
			var invoice = {
				'entity_type': STORAGEREQUEST,
				'entity_id': $scope.request.nid,
				'data':{
					'id': $scope.request.nid,
					'charges':[charge],
					'storage_invoice': true,
					'field_moving_from':{
						'postal_code': $scope.request.user_info.zip,
						'thoroughfare': $scope.request.user_info.address,
						'locality': $scope.request.user_info.city,
						'administrative_area':$scope.request.user_info.state
					},
					'date': moment().format('MMMM DD, YYYY'),
					'client_name': $scope.request.user_info.name,
					'email':$scope.request.user_info.email,
					'phone':$scope.request.user_info.phone1,
					'uid':$scope.request.user_info.uid,
					'discount':0,
					'tax':0,
					'terms': $scope.terms || '',
					'flag':0,
					'totalInvoice': _.round(charge.qty * charge.cost, 2),
					'total': _.round(charge.qty * charge.cost, 2),
					'entity_type': STORAGEREQUEST,
					'entity_id': $scope.request.nid
				}
			};
			InvoiceServices.openInvoiceDialog($scope.request ,$scope.entitytype,false,invoice, $scope.terms);
		};
		$scope.changePending = function(receipt) {
			receipt.entity_id = $scope.request.nid;
			receipt.entity_type = $scope.entitytype;

			PaymentServices.update_receipt($scope.request.nid, receipt).then(function(){
				var message = 'Payment in the amount  $' + receipt.amount + '(was changed pending option).';
				var obj = {
					simpleText: message
				};
				var arr = [obj];
				RequestServices.sendLogs(arr, 'Payment pending option', $scope.request.nid, 'STORAGEREQUEST');
			});
			$rootScope.$broadcast('request.payment_receipts',$scope.receipts, $scope.request.nid);

		};
		$scope.calculatePayments = function (){
			return PaymentServices.calcPayment($scope.receipts) - PaymentServices.calcInvoices($scope.invoices);
		};

		$scope.openRefund = function(){
			$scope.request.phone = $scope.request.user_info.phone1;
			$scope.request.name = $scope.request.user_info.name;
			$scope.request.email = $scope.request.user_info.email;
			$scope.request.zip = $scope.request.user_info.zip;
			$scope.request.uid = {
				uid: $scope.request.user_info.uid
			};
			var receiptData = '';
			var idx = _.findIndex($scope.receipts, {id: $scope.activeReceiptId});
			if ($scope.activeRow != -1 && idx > -1) {
				receiptData = $scope.receipts[idx];
			}
			let uid = _.get($rootScope, 'currentUser.userId.uid');
			PaymentServices.openRefundModal($scope.request, uid, $scope.entity_type, receiptData);
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
			searchPayment = true;
		};

		$scope.save = function() {
			$scope.cancel();
		};

		$scope.createInvoice = function(){
			InvoiceServices.openInvoiceDialog(request,$scope.entitytype,false, false, $scope.terms);
		};

		$uibModalInstance.result.then(function () {
		}, function () {
			searchPayment = true;
		});

		var requestData;
		$scope.$on('$destroy', function() {
			$timeout.cancel(requestData);
		});
		$scope.$on('receipt_sent', function() {
			$scope.receiptLoader = true;
		});
		$scope.$on('payment.service',function (event, data) {
			$scope.invoices = data.invoices;
			let dd = data.payments;
			if($scope.receipts.length < dd.length){
				$scope.receipts = dd;
				request.data.payments = dd;
				sortRefundReceipts();
				toastr.success('Payment was received!');
			} else {
				$scope.receipts = dd;
				request.data.payments = dd;
				sortRefundReceipts();
			}
		});

	}
}
