'use strict';

angular
	.module('app.storages')
	.directive('ccStoragesDocuments', ccStoragesDocuments);

/* @ngInject */
function ccStoragesDocuments(printService) {
	var directive = {
		link: link,
		template: require('../templates/storagesDocuments.html'),
		restrict: 'AE'
	};
	return directive;


	function link($scope, element, attrs) {
		$scope.clicked = '';

		$scope.printDocument = function (id) {
			let printContent = document.getElementById('image-print_' + id).innerHTML;
			printService.printThisLayout(printContent);
		};

		$scope.dateFormatter = function (date) {
			return moment.unix(date).format('MMM DD, YYYY');
		};

	}
}
