(function () {
    'use strict';

    angular
        .module('app.storages')
        .directive('lotNumberDirective', lotNumberDirective);

    // ccLedger.$inject = ['SweetAlert', 'InvoiceServices', '$filter', '$location', 'PaymentServices'];


    function lotNumberDirective() {
        var directive = {
            link: link,
            scope: {
                lotNumbers: '=',
                msgLog: '=',
                basicData: '='
            },
	        template: require('../templates/lotNumber.template.html'),
            restrict: 'AE'
        };

        return directive;


        function link($scope, element, attrs) {
            $scope.changeField = function (field, value) {
                var from = $scope.basicData[field];
                var to = value;
                var textFrom = prepareMSGLogText(from);
                var textTo = prepareMSGLogText(to);

                if (!_.isUndefined($scope.msgLog)) {
                    $scope.msgLog[field] = {
                        from: textFrom,
                        to: textTo,
                        text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' was changed'
                    };
                }
            };

            function prepareMSGLogText(fieldValue) {
                var result = '';

                if (fieldValue) {
                    _.forEach(fieldValue, function (obj) {
                        _.forEach(obj, function (item, key) {
                            if (angular.isUndefined(item)) {
                                return false;
                            }

                            result += key.charAt(0).toUpperCase() + key.slice(1) + ': ' + item;

                            if (key != _.findLastKey(obj)) {
                                result += ', ';
                            } else {
                                result += '.<br>';
                            }
                        });
                    });
                }

                return result;
            }

            //lot of numbers
            $scope.addColor = function() {
                $scope.lotNumbers.push({
                    "number": "",
                    "color": "",
                    "from": "",
                    "to": ""});
            };

            $scope.removeColor = function(index) {
                $scope.lotNumbers.splice(index, 1);
            };
        }
    }
})();
