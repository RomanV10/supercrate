'use strict';

angular
	.module('app.storages')
	.directive('ccLedger', ccLedger);

/*@ngInject*/
function ccLedger($filter, PaymentServices, StoragePaymentService, datacontext) {
	return {
		link: link,
		scope: {
			'payments': '=payments',
			'invoices': '=invoices',
			'request': '=request'
		},
		template: require('../templates/ledger.html'),
		restrict: 'AE'
	};

	function link($scope, element, attrs) {
		$scope.bills = [];
		var invoices = {};
		$scope.fieldData = datacontext.getFieldData();
		var STORAGEREQUEST = $scope.fieldData.enums.entities.STORAGEREQUEST;
		$scope.entitytype = STORAGEREQUEST;
		invoices.entity_type = $scope.entitytype;
		invoices.entity_id = $scope.request.nid;
		$scope.busy = true;
		$scope.clicked = '';
		var billObj;

		function initLedger() {
			$scope.bills = [];
			_.forEach($scope.invoices, function (item) {
				billObj = {
					date: parseInt(item.created),
					description: item.data.charges[0].description,
					charge: item.data.totalInvoice,
					payment: '',
					balance: 0,
					type: 'invoice',
					bill_id: 'invoice_' + item.id,
					id: item.id,
					hash: item.hash
				};
				$scope.bills.push(billObj);
			});
			_.forEach($scope.payments, function (item) {
				billObj = {
					date: item.created ? parseInt(item.created) : (parseInt(item.date_ledger) || moment(
						item.date).unix()),
					description: item.description,
					charge: '',
					payment: item.amount,
					balance: 0,
					type: 'payment',
					bill_id: 'payment_' + item.id,
					id: item.id,
					refunds: item.refunds,
					method: item.payment_method
				};
				$scope.bills.push(billObj);
			});
			$scope.bills = $filter('orderBy')($scope.bills, 'date', true);

			var totalBalance = 0;
			_.forEachRight($scope.bills, function (item) {
				if (item.type == 'payment' && !item.refunds) {
					item.balance = totalBalance + _.round(item.payment, 2);
					totalBalance += _.round(item.payment, 2);
				} else if (item.refunds) {
					item.balance = totalBalance - _.round(item.payment, 2);
					totalBalance -= _.round(item.payment, 2);
				}

				if (item.type == 'invoice') {
					item.balance = totalBalance - _.round(item.charge, 2);
					totalBalance -= _.round(item.charge, 2);
				}
			});

			$scope.busy = false;
		}

		initLedger();

		$scope.openPayment = function () {
			$scope.request.nid = $scope.request.id;
			$scope.request.payments = $scope.payments;
			$scope.request.invoices = $scope.invoices;
			var req = {
				data: $scope.request,
				id: $scope.request.id
			};
			StoragePaymentService.openPaymentModal(req);
		};
		$scope.calculatePayments = function () {
			return PaymentServices.calcPayment($scope.payments) - PaymentServices.calcInvoices($scope.invoices);
		};
		$scope.dateFormat = function (date, type) {
			return _.isNaN(moment.unix(date)._i) ? moment(date).format('DD MMM, YYYY') : moment.unix(date).format(
				'DD MMM, YYYY h:mm a');
		};
		$scope.paymentType = function (payment) {
			var type = payment.method;
			switch (type) {
			case 'creditcard' :
				return 'credit card';
				break;
			case 'cash':
				return 'cash';
				break;
			case 'check':
				return 'check';
				break;
			default:
				return 'custom';
			}
		};
		$scope.$watchCollection('payments', function (newValue, oldValue) {
			$scope.request.payments = $scope.payments;
			initLedger();
		});
		$scope.$watchCollection('invoices', function (newValue, oldValue) {
			$scope.request.invoices = $scope.invoices;
			initLedger();
		});

		$scope.openModal = function (bill_id, obj) {
			if ($scope.clicked != bill_id) {
				$scope.clicked = bill_id;
				return false;
			}
			if (obj.type == 'payment') {
				let ind = _.findIndex($scope.payments, {id: obj.id});
				let payment = $scope.payments[ind];
				let entity = {
					entity_id: $scope.request.nid,
					entity_type: $scope.fieldData.enums.entities.STORAGEREQUEST
				};
				PaymentServices.openReceiptModal($scope.request, payment, false, entity);
			}
		};
	}
}

