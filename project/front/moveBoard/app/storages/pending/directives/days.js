(function(){
    'use strict';
    angular.module('app.storages')
        .directive('ccDays', ccDays);



    function ccDays(){
        return {
            restrict: 'A',
            require:'^ngModel',
            scope: {
                ngModel: '='
            },
            link: function(scope, el, attrs, ngModelCtrl){
                var reservedVal = 0;

                function formatter(value) {
                    value = value ? parseFloat(value.toString().replace(/[^0-9._-]/g, '')) || 0 : 0;
                    var formattedValue = value +' days';
                    el.val(formattedValue);

                    ngModelCtrl.$setViewValue(value);
                    return formattedValue;
                }

                ngModelCtrl.$formatters.push(formatter);

                el.bind('focus', function () {
                    if(attrs.readonly)
                        return false;
                    reservedVal = el.val();
                    el.val('');
                });

                el.bind('blur', function () {
                    var val = el.val() != ''
                        ? el.val()
                        : reservedVal;
                    formatter(val);
                });
            }
        }
    }


})();