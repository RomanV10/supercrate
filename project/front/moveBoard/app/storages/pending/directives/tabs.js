(function(){
    'use strict';
    angular.module('app.storages')
        .directive('selectTab', tabs);

    function tabs(){
        return {
            restrict: 'A',
            controller: TabsController,
            controllerAs: 'tabs'
        }
    };
    function TabsController(){
        var self = this;
        self.tab = 1;

        self.setTab = function (tabId) {
            self.tab = tabId;
        };

        self.isSet = function (tabId) {
            return self.tab === tabId;
        };
    }

})()