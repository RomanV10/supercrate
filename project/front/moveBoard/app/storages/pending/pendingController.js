(function () {
    'use strict';
    angular.module('app.storages')
        .controller('PendingController', ['$scope', '$uibModal', 'StorageService', 'SweetAlert', 'DTOptionsBuilder', 'PaymentServices',
            function ($scope, $uibModal, StorageService, SweetAlert, DTOptionsBuilder, PaymentServices) {

                var self = this;
                self.serviceTypes = {
                    1: 'Pending',
                    2: 'Move in',
                    3: "Move out",
	                4: 'Move out Pending'
                };
                var PENDING = 1;
                var MOVEIN = 2;
                var MOVEOUT = 3;
                self.clicked = '';
                self.busy = false;
                self.dtOptions = DTOptionsBuilder
                    .newOptions()
                    .withOption('bFilter', false)
                    .withOption('aaSorting', [0, 'desc'])
                    .withDisplayLength(25);

                self.refreshPending = function () {
                    self.busy = true;
                    StorageService.getReqStorage(PENDING).then(function ({data}) {
                        self.clicked = '';
                        self.requests = data || {};
                        self.busy = false;
                    });
                };
                self.refreshPending();
                // open modal for storage request
                self.createModal = function () {
                    StorageService.createModal(self.requests);
                };

                self.openModal = function (request, id) {
                    if (self.clicked != id) {
                        self.clicked = id;
                        return false;
                    }
                    StorageService.openModal(request, id, self.requests);
                };


                self.dateFormatter = function (date) {
                    return moment(moment.utc(moment.unix(date)).format("DD-MM-YYYY"), "DD-MM-YYYY").format("MMM DD, YYYY");
                };

                self.calculatePayments = function (request) {
	                let result = 0;

	                if (request) {
		                result = PaymentServices.calcPayment(request.payments) - PaymentServices.calcInvoices(request.invoices);
	                }

	                return result;
                };
            }
        ]);
})();
