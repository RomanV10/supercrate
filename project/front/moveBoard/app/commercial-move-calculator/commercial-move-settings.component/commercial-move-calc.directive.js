'use strict';

import { COMMERCIAL_ITEM } from "../commercial-move.services/commercial-base";
import './commercial-move-calc.styl';

angular.module('app.commercialMove').directive('commercialMoveCalc', commercialMoveCalc);

commercialMoveCalc.$inject = ['CommercialCalc', 'datacontext', 'SettingServices'];

function commercialMoveCalc(CommercialCalc, datacontext, SettingServices) {
	return {
		restrict: 'AE',
		scope: {},
		template: require('./commercial-move-calc.tmpl.pug'),
		link: commercialMove
	};
	
	function commercialMove($scope) {
		let calcSettings = angular.fromJson(datacontext.getFieldData().calcsettings);
		$scope.addCommercialMoveSize = addCommercialMoveSize;
		$scope.removeCommercialMoveSize = removeCommercialMoveSize;
		$scope.updateCommercialExtra = updateCommercialExtra;
		$scope.saveCommercialSetting = saveCommercialSetting;
		init();
		
		function init() {
			CommercialCalc.getCommercialSetting().then(resolve => {
				$scope.commecialMoveSizes = {
					heading: resolve.setting,
					moveSizes: resolve.extra
				};
				updateCalcSettings();
			});
		}
		
		function saveCommercialSetting() {
			CommercialCalc.saveCommercialSetting(angular.toJson($scope.commecialMoveSizes.heading)).then(resolve => {
				init();
			})
		}
		
		function updateCommercialExtra(extraItem) {
			let isNewItem = _.isUndefined(extraItem.id);
			if (isNewItem) {
				saveCommercialItem(extraItem);
			} else {
				updateCommercialItem(extraItem);
			}
			toastr.success(`${extraItem.name} was saved`);
		}
		
		function updateCalcSettings() {
			calcSettings.commercialExtra = angular.copy($scope.commecialMoveSizes.moveSizes);
			SettingServices.saveSettings(calcSettings, 'calcsettings');
		}
		
		function addCommercialMoveSize() {
			$scope.commecialMoveSizes.moveSizes.push(angular.copy(COMMERCIAL_ITEM));
		}
		
		function removeCommercialMoveSize(setting) {
			if (!_.isUndefined(setting.id)) {
				CommercialCalc.deleteCommercialExtra(setting.id).then(resolve => {
					init();
				})
			} else {
				$scope.commecialMoveSizes.moveSizes.splice($scope.commecialMoveSizes.moveSizes.indexOf(setting), 1);
			}
			toastr.success(`${setting.name} was removed`);
		}
		
		function saveCommercialItem(commercialItem) {
			CommercialCalc.createExtraCommercialMove(commercialItem).then(resolve => {
				init();
			})
		}
		
		function updateCommercialItem(commercialItem) {
			CommercialCalc.updateCommercialExtra(commercialItem).then(resolve => {
				init();
			});
		}
	}
}