describe('Commercial Move Calculator Directive', () => {
	let element,
		isolatedScope,
		$scope;

	beforeEach(inject(function (_$rootScope_) {
        let getVariableResp = ["{\"name\":\"HOPHEYLALALAY\",\"isEnabled\":true}"];
        let getCommercialExtraRoomResp = [{"id":"54","name":"QQQ","cubic_feet":"123"}];

		$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.settingService.getVariable)).respond(200, getVariableResp);
		$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.commercialMove.getCommercialExtra)).respond(200, getCommercialExtraRoomResp);
		$httpBackend.whenGET(/.*/).passThrough();
		$httpBackend.whenPOST(/.*/).passThrough();
		$scope = _$rootScope_.$new();
		$scope.$apply();

		element = angular.element('<commercial-move-calc></commercial-move-calc>');
		$compile(element)($scope);
		isolatedScope = element.isolateScope();

		$timeout.flush();
		$scope.$apply();

	}));

	describe('Commercial Move Sized Should Be Defined', () => {
		it('Directive is defined', () => {
			expect(isolatedScope.commecialMoveSizes).toBeDefined();
			expect(isolatedScope.commecialMoveSizes.heading).toBeDefined();
			expect(isolatedScope.commecialMoveSizes.moveSizes).toBeDefined();
		});
	});
});
