export const COMMERCIAL_ITEM = {
	name: '',
	cubic_feet: 0
};
export const BASE_SETTING = {
	name: 'Commercial Move',
	isEnabled: false
};
export const COMMERCIAL_NAME = 'commercial_move_size_setting';