'use strict';

import {COMMERCIAL_NAME, BASE_SETTING} from "./commercial-base";

angular
.module('app.commercialMove')
.factory('CommercialCalc', CommercialCalc);

CommercialCalc.$inject = ['apiService', 'moveBoardApi', '$q'];

function CommercialCalc(apiService, moveBoardApi, $q) {
	return {
		saveCommercialSetting: saveCommercialSetting,
		getCommercialSetting: getCommercialSetting,
		getCommercialCubicFeet: getCommercialCubicFeet,
		createExtraCommercialMove: createExtraCommercialMove,
		updateCommercialExtra: updateCommercialExtra,
		deleteCommercialExtra: deleteCommercialExtra,
		getCurrentCommercialForInventory: getCurrentCommercialForInventory,
		getCurrentCommercial: getCurrentCommercial
	};
	
	function saveCommercialSetting(commercialSetting) {
		let deferred = $q.defer();
		apiService.postData(moveBoardApi.commercialMove.updateSetting, {
			value: commercialSetting
		}).then(resolve => {
			deferred.resolve(resolve.data);
		}, reject => {
			deferred.reject(reject);
		});
		return deferred.promise;
	}
	
	function deleteCommercialExtra(id) {
		let deferred = $q.defer();
		
		apiService.postData(moveBoardApi.commercialMove.deleteCommercialExtra, {
			room_id: id
		}).then(resolve => {
			deferred.resolve(resolve.data);
		}, reject => {
			deferred.reject(reject);
		});
		
		return deferred.promise;
	}
	
	function updateCommercialExtra(commercialItem) {
		let deferred = $q.defer();
		
		apiService.postData(moveBoardApi.commercialMove.updateCommercialExtra, {
			room_id: commercialItem.id,
			name: commercialItem.name,
			cubic_feet: commercialItem.cubic_feet
		}).then(resolve => {
			deferred.resolve(resolve.data);
		}, reject => {
			deferred.reject(reject);
		});
		
		return deferred.promise;
	}
	
	function getCommercialSetting() {
		let deferred = $q.defer();
		let extra = apiService.postData(moveBoardApi.commercialMove.getCommercialExtra);
		let setting = apiService.postData(moveBoardApi.settingService.getVariable, {name: COMMERCIAL_NAME});
		
		$q.all({extra, setting}).then((response) => {
			let res = {};
			res.extra = response.extra.data;
			res.setting = BASE_SETTING;
			if (!_.isNull(response.setting.data)) {
				res.setting = angular.fromJson(response.setting.data[0]);
			}
			deferred.resolve(res);
		});
		
		return deferred.promise;
	}
	
	function createExtraCommercialMove(data) {
		let deferred = $q.defer();
		
		apiService.postData(moveBoardApi.commercialMove.createExtraCommercialMove, data).then(resolve => {
			deferred.resolve(resolve.data);
		}, reject => {
			deferred.reject(reject);
		});
		
		return deferred.promise;
	}
	
	function getCommercialCubicFeet(requestExtra, calcSettings, custom) {
		let totalCommercialWeight = Number(calcSettings.size['11']);
		let extraRooms;
		if (requestExtra && requestExtra.length) {
			extraRooms = requestExtra;
		}
		if (!_.isUndefined(extraRooms) && !custom.is_checked) {
			if (_.isArray(extraRooms)) {
				extraRooms = extraRooms[0];
			}
			totalCommercialWeight += Number(_.find(calcSettings.commercialExtra, {id: extraRooms}).cubic_feet);
		} else if (custom.is_checked) {
			totalCommercialWeight = Number(custom.cubic_feet);
		}
		return {
			weight: totalCommercialWeight,
			min: (totalCommercialWeight - 100 < 0) ? totalCommercialWeight : totalCommercialWeight - 100,
			max: totalCommercialWeight + 100
		};
	}
	
	function getCurrentCommercialForInventory(preset, custom) {
		let result = '';
		if (custom.is_checked) {
			result = custom.id;
		} else {
			result = preset[0];
		}
		return result;
	}
	
	function getCurrentCommercial(preset, custom, commercialSetting) {
		if (custom.is_checked) {
			return custom;
		} else {
			return _.find(commercialSetting, {id: preset[0]});
		}
	}
}