'use strict';

import './commercial-custom-name.styl';

angular.module('app.commercialMove').directive('commercialCustomName', commercialCustomName);

commercialCustomName.$inject = ['RequestServices'];

function commercialCustomName(RequestServices) {
	return {
		restrict: 'AE',
		scope: {
			'request': '='
		},
		template: require('./commercial-custom-name.tpl.pug'),
		link: customNameLink
	};
	
	function customNameLink($scope) {
		$scope.customName = $scope.request.request_all_data.commercialCustomName || '';
		
		function updateSettings() {
			if ($scope.customName.length) {
				saveCustomName();
			} else {
				deleteCustomName();
			}
		}
		
		function saveCustomName() {
			$scope.request.request_all_data.commercialCustomName = $scope.customName;
			RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
		}
		
		function deleteCustomName() {
			$scope.customName = '';
			delete $scope.request.request_all_data.commercialCustomName;
			RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
		}
		
		$scope.$on('updateCommercialName', updateSettings);
		$scope.$on('$destroy', updateSettings);
	}
}