'use strict';

import './commercial-move-sizes.styl';

angular.module('app.commercialMove').directive('commercialMoveSize', commercialMoveSize);

commercialMoveSize.$inject = ['CommercialCalc', '$uibModal', 'SweetAlert', '$q', 'datacontext', '$rootScope', 'RequestServices'];

function commercialMoveSize(CommercialCalc, $uibModal, SweetAlert, $q, datacontext, $rootScope, RequestServices) {
	return {
		restrict: 'AE',
		scope: {
			editRequest: '=',
			isNewRequest: '=',
			message: '=?',
			request: '=?',
		},
		template: require('./commercial-move-sizes.tpl.pug'),
		link: comMoveSizeLink
	};
	
	function comMoveSizeLink($scope) {
		let fieldData = datacontext.getFieldData();
		let calcSetting = angular.fromJson(fieldData.calcsettings);
		$scope.commercialMoveSizes = calcSetting.commercialExtra;
		
		$scope.addCommercialSize = addCommercialSize;
		$scope.updateExtraCommercialField = updateExtraCommercialField;
		$scope.clearCommercial = clearCommercial;
		
		function init() {
			if ($scope.isNewRequest) {
				angular.element('#commercial-select').addClass('full-width');
				angular.element('#clear-btn').css({
					right: '21px',
					top: '7px'
				});
				CommercialCalc.getCommercialSetting().then(resolve => {
					$scope.commercialMoveSizes = resolve.extra;
				});
			} else {
				CommercialCalc.getCommercialSetting().then(resolve => {
					$scope.commercialMoveSizes = resolve.extra;
					if ($scope.request.field_custom_commercial_item.name != null) {
						$scope.commercialMoveSizes.push($scope.request.field_custom_commercial_item);
					}
					
					switch (Number($scope.request.field_useweighttype.value)) {
					case 2:
						$scope.commercialSize = CommercialCalc.getCurrentCommercialForInventory($scope.request.commercial_extra_rooms.value, $scope.request.field_custom_commercial_item);
						break;
					case 3:
						$scope.commercialSize = $scope.request.field_custom_commercial_item.id;
						break;
					case 1:
					default:
						$scope.commercialSize = $scope.request.commercial_extra_rooms.value[0];
						break;
					}
				});
			}
		}
		
		function updatePaired(pairedId) {
			RequestServices.updateRequest(pairedId, {
				field_commercial_extra_rooms: $scope.request.commercial_extra_rooms.value,
				field_custom_commercial_item: $scope.request.field_custom_commercial_item,
				field_useweighttype: $scope.request.field_useweighttype.value
			});
		}
		
		function updateExtraCommercialField(current) {
			if ($scope.isNewRequest) {
				$scope.editRequest.data.field_commercial_extra_rooms = [];
				$scope.editRequest.data.field_custom_commercial_item.is_checked = false;
				fetchCommercialForNewRequest(current);
			} else {
				let isCustom = $scope.request.field_custom_commercial_item.id === current;
				let pairedId = $scope.request.storage_id || $scope.request.request_all_data.packing_request_id;
				if (isCustom) {
					$scope.request.field_custom_commercial_item.is_checked = true;
					prepareCustomCommercial();
				} else {
					$scope.request.field_custom_commercial_item.is_checked = false;
					if (_.find(calcSetting.commercialExtra, {id: current})) {
						preparePresetsCommercial(current);
					} else {
						prepareEmptyCommercial();
					}
					$scope.editRequest.field_custom_commercial_item = $scope.request.field_custom_commercial_item;
					$scope.editRequest.field_custom_commercial_item.is_checked = false;
				}
				$rootScope.$broadcast('updateWeight');
				
				if (pairedId) {
					updatePaired(pairedId);
				}
			}
		}
		
		function fetchCommercialForNewRequest(current) {
			let commercialItem = _.find($scope.commercialMoveSizes, {id: current});
			if (!_.isUndefined(commercialItem) && !_.isUndefined(commercialItem.is_checked)) {
				$scope.editRequest.data.field_commercial_extra_rooms = [];
				$scope.editRequest.data.field_custom_commercial_item.is_checked = true;
			} else if (!_.isUndefined(commercialItem)
				&& _.isUndefined(commercialItem.is_checked)) {
				$scope.editRequest.data.field_commercial_extra_rooms = [current];
				$scope.editRequest.data.field_custom_commercial_item.is_checked = false;
			}
		}
		
		function prepareCustomCommercial() {
			$scope.request.custom_weight.value = Number($scope.request.field_custom_commercial_item.cubic_feet);
			$scope.request.field_useweighttype.value = 3;
			$scope.$emit('updateWeightType', {
				id: $scope.request.nid,
				type: 3
			});
			
			createCommercialMessage($scope.request.field_custom_commercial_item.name);
			$scope.editRequest[$scope.request.custom_weight.field] = $scope.request.custom_weight.value;
			$scope.editRequest[$scope.request.commercial_extra_rooms.field] = [];
			$scope.editRequest.field_custom_commercial_item = $scope.request.field_custom_commercial_item;
			$scope.editRequest.field_custom_commercial_item.is_checked = true;
		}
		
		function preparePresetsCommercial(current) {
			let message = `${_.find($scope.commercialMoveSizes, {id: current}).name} `;
			$scope.request.commercial_extra_rooms.value = current;
			$scope.request.total_weight = CommercialCalc.getCommercialCubicFeet(current, calcSetting, $scope.request.field_custom_commercial_item);
			$scope.request.field_useweighttype.value = 1;
			$scope.$emit('updateWeightType', {
				id: $scope.request.nid,
				type: 1
			});
			createCommercialMessage(message);
			$scope.editRequest[$scope.request.commercial_extra_rooms.field] = current;
		}
		
		function prepareEmptyCommercial() {
			$scope.request.commercial_extra_rooms.value = [];
			$scope.request.total_weight = CommercialCalc.getCommercialCubicFeet($scope.request.commercial_extra_rooms.value, calcSetting, $scope.request.field_custom_commercial_item);
			if (!$scope.request.inventory.inventory_list.length) {
				$scope.request.field_useweighttype.value = 1;
				$scope.$emit('updateWeightType', {
					id: $scope.request.nid,
					type: 1
				});
			}
			createCommercialMessage('None');
			$scope.editRequest[$scope.request.commercial_extra_rooms.field] = [];
		}
		
		function createCommercialMessage(value) {
			$scope.message[$scope.request.commercial_extra_rooms.field] = {
				label: $scope.request.commercial_extra_rooms.label,
				oldValue: '',
				newValue: value
			};
		}
		
		function addCommercialSize(newName) {
			let deferred = $q.defer();
			SweetAlert.swal({
				title: `${newName} size is not found.`,
				text: 'Do you wanna create new size?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#0088e4',
				confirmButtonText: 'Confirm',
				closeOnConfirm: true
			}, function (isConfirmed) {
				if (isConfirmed) {
					let newCommercialSizeModal = $uibModal.open({
						template: require('../new-commercial-size.component/new-commercial-size.tpl.pug'),
						size: 'md',
						backdrop: true,
						controller: 'newCommercialModal',
						resolve: {
							request: function () {
								return $scope.request || $scope.editRequest;
							},
							moveSizes: function () {
								return $scope.commercialMoveSizes;
							},
							name: function () {
								return newName;
							},
							isNewRequest: function () {
								return $scope.isNewRequest;
							}
						}
					});
					newCommercialSizeModal.result.then(result => {
						$scope.commercialMoveSizes = result.moveSizes;
						deferred.resolve(result.newItem);
					});
				}
			});
			return deferred.promise;
		}
		
		function clearCommercial() {
			$scope.commercialSize = [];
			updateExtraCommercialField();
		}
		
		$scope.$on('addCommercialInventory', function () {
			$scope.request.field_useweighttype.value = 2;
			$scope.$emit('updateWeightType', {
				id: $scope.request.nid,
				type: 2
			});
			$rootScope.$broadcast('updateWeight');
		});
		
		init();
	}
}
