'use strict';

import './new-commercial-size.styl';

angular.module('app.commercialMove').controller('newCommercialModal', newCommercialModal);

newCommercialModal.$inject = ['$scope', '$uibModalInstance', 'name', 'request', 'RequestServices', 'moveSizes', 'isNewRequest'];

function newCommercialModal($scope, $uibModalInstance, name, request, RequestServices, moveSizes, isNewRequest) {
	$scope.commercialItem = {
		name: name,
		cubic_feet: 0,
		is_checked: true,
		id: moment().unix()
	};
	
	moveSizes = moveSizes.filter(commercial => {
		return _.isUndefined(commercial.is_checked);
	});
	
	$scope.updateCommercialMoveSizes = updateCommercialMoveSizes;
	
	function updateCommercialMoveSizes() {
		moveSizes.push($scope.commercialItem);
		if (!isNewRequest) {
			request.field_custom_commercial_item = $scope.commercialItem;
			RequestServices.updateRequest(request.nid, {
				field_custom_commercial_item: $scope.commercialItem
			});
		} else {
			request.data.field_custom_commercial_item = $scope.commercialItem;
		}
		$uibModalInstance.close({
			newItem: $scope.commercialItem,
			moveSizes: moveSizes
		});
	}
}
