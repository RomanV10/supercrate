(function () {
	'use strict';

	angular.module('app')
		.directive('erOnlyDigit', erOnlyDigit);

	function erOnlyDigit() {
		const ZERO_KEY_CODE = 48;
		const NINE_KEY_CODE = 57;
		const BACK_SPACE_KEY_CODE = 8;
		const POINT_KEY_CODE = 46;
		const LEFT_ARROW_KEY_CODE = 37;
		const RIGHT_ARROW_KEY_CODE = 39;
		const DOT_KEY_CODE = 190;
		const DECIMAL_POIN_KEY_CODE = 110;

		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				element.on('keypress', onKeyPress);

				function onKeyPress(event) {
					let isPointKeyCode = false;
					let isDotKeyCode = false;
					let isDecimalPointKeyCode = false;
					let isNumberCharCode = event.charCode >= ZERO_KEY_CODE && event.charCode <= NINE_KEY_CODE;
					let isBackSpaceKeyCode = event.charCode == BACK_SPACE_KEY_CODE;
					let isLeftArrowKeyCode = event.charCode == LEFT_ARROW_KEY_CODE;
					let isRightArrowKeyCode = event.charCode == RIGHT_ARROW_KEY_CODE;
					if (attrs.withDecimalPoint) {
						isPointKeyCode = event.charCode == POINT_KEY_CODE;
						isDotKeyCode = event.charCode == DOT_KEY_CODE;
						isDecimalPointKeyCode = event.charCode == DECIMAL_POIN_KEY_CODE;	
					}
					let isAllowedCharCode = isNumberCharCode || isBackSpaceKeyCode || isLeftArrowKeyCode || isRightArrowKeyCode || isPointKeyCode || isDotKeyCode || isDecimalPointKeyCode;

					let isNumberKeyCode = event.keyCode >= ZERO_KEY_CODE && event.keyCode <= NINE_KEY_CODE;
					isBackSpaceKeyCode = event.keyCode == BACK_SPACE_KEY_CODE;
					isLeftArrowKeyCode = event.keyCode == LEFT_ARROW_KEY_CODE;
					isRightArrowKeyCode = event.keyCode == RIGHT_ARROW_KEY_CODE;
					if (attrs.withDecimalPoint) {
						isPointKeyCode = event.keyCode == POINT_KEY_CODE;
						isDotKeyCode = event.keyCode == DOT_KEY_CODE;
						isDecimalPointKeyCode = event.keyCode == DECIMAL_POIN_KEY_CODE;	
					}
					let isAllowedKeyCode = isNumberKeyCode || isBackSpaceKeyCode || isLeftArrowKeyCode || isRightArrowKeyCode || isPointKeyCode || isDotKeyCode || isDecimalPointKeyCode;

					return isAllowedCharCode || isAllowedKeyCode;
				}
			}
		};
	}
})();