angular.module('app')
  .directive('elromcoUserUnique', ['apiService', 'moveBoardApi', function(apiService, moveBoardApi) {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, c) {
      element.focusout(() => {
        apiService.postData(moveBoardApi.validations.userUnique, {email: element.val()}).then(data => {
         if (data.data[0]) {
           c.$setValidity('unique', false);
         } else {
           c.$setValidity('unique', true);
         }
      })
    });
    }
  }
}])
