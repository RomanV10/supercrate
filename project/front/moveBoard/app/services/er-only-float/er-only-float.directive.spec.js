describe('Unit: only float directive,', function () {
	let element, scope, $scope;

	function compileElement(decimal) {
		$scope = $rootScope.$new();

		$scope.theModel = '';
		$scope.decimal = decimal;
		element = $compile("<input ng-model='theModel' id='theInput' er-only-float decimals='{{decimal}}'/>")($scope);
		scope = element.isolateScope();
	}

	describe('When two decimal point', () => {
		beforeEach(inject(()=>{
			compileElement(2);
		}));

		it ("Should remove letters", () => {
			element.val('asdf123asdf123asdf');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('123123');
		});

		it ("Should remove extra dots", () => {
			element.val('12.12.3');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('12.12');
		});

		it ("Should remove ,", () => {
			element.val('12,123');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('12.12');
		});

		it ("Should remove extra numbers after dot", () => {
			element.val('1.12345');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('1.12');
		});

		it ("Should add 0 if . is first", () => {
			element.val('.12');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('0.12');
		});

		it ("Should add 0 if , is first", () => {
			element.val(',12');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('0.12');
		});

		it ("Should replace , to .", () => {
			element.val('1,12');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('1.12');
		});

		it ("Should save first minus", () => {
			element.val('-1,1-2');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('-1.12');
		});

		it ("Should set to 0 if empty", () => {
			element.val('');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('0');
		});
	});

	describe('When one decimal point', () => {
		beforeEach(inject(()=>{
			compileElement(1);
		}));

		it ("Should remove all number after all", () => {
			element.val('12.12');
			element.triggerHandler('change');
			expect($scope.theModel).toEqual('12.1');
		});
	})
});
