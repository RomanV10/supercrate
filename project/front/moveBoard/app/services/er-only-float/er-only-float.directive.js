'use strict';

angular
	.module('app')
	.directive('erOnlyFloat', erOnlyFloat);

function erOnlyFloat() {

	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModelCtrl) {
			let decimals = +attrs.decimals || 2;
			let onlyPositive = attrs.hasOwnProperty('onlyPositive');
			ngModelCtrl.$parsers.unshift(leaveOnlyFloat);

			function leaveOnlyFloat(newValue) {
				let isNegative = false;
				let isLastDot = false;

				if (newValue == '') {
					newValue = '0';
				}

				if (newValue[0] == '-' && !onlyPositive) {
					isNegative = true;
				}

				if (_.isString(newValue)) {
					newValue = newValue.replace(/,/g, '.');

					if (newValue[0] === '.') {
						newValue = '0' + newValue;
					}

					newValue = newValue.replace(/[^\d.]/g, '')
						.replace(/^([^.]*\.)|\./g, '$1');

					isLastDot = _.endsWith(newValue, '.');
				}

				newValue = _.floor(+newValue, decimals);
				if (isNegative) newValue = -newValue;
				newValue = '' + newValue;

				ngModelCtrl.$setViewValue(newValue + (isLastDot ? '.' : ''));
				ngModelCtrl.$render();
				return newValue;
			}
		}
	};
}
