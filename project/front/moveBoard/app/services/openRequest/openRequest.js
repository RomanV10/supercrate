'use strict';

class OpenRequest {
	constructor(SweetAlert, openRequestService){
		this.restrict = 'A';
		this.SweetAlert = SweetAlert;
		this.openRequestService = openRequestService;
	}
	link($scope, $element, $attrs) {
		$element.on('click',(event) =>
			this.openRequest($scope.$eval($attrs.openRequest)));
	}

	async openRequest(node_id){
		try {
			await this.openRequestService.open(node_id);
		}catch(err) {
			this.SweetAlert.swal('Error', 'Problem with getting request data', 'error');
		}

	}
}

angular
	.module('app')
	.directive('openRequest', (SweetAlert, openRequestService) =>
		new OpenRequest(SweetAlert, openRequestService));
