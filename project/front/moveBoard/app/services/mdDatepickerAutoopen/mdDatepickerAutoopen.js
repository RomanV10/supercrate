'use strict';

angular.module('app')
	.directive('mdDatepickerAutoopen', function ($timeout) {
		return {
			restrict: 'A',
			require: 'mdDatepicker',
			link: function (scope, element, attributes, DatePickerCtrl) {
				element.find('input').on('click', function (e) {
					$timeout(DatePickerCtrl.openCalendarPane.bind(DatePickerCtrl, e));
				});
			}
		};
	});
