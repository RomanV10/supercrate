angular.module('app')
  .directive('elromcoInputPattern', function(){
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {

        modelCtrl.$parsers.push(function (inputValue) {
          var transformedInput = inputValue.replace(new RegExp(attrs.elromcoInputPattern), '');
          modelCtrl.$setViewValue(transformedInput);
          modelCtrl.$render();
          return transformedInput;
        });
      }
    };
  });
