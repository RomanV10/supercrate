'use strict';

class BusyOverlay {
	constructor() {
		this.restrict = 'EA';
		this.transclude = true;
		this.scope = {
			busyOverlay: '<'
		};
		this.template = require('./busyOverlay.html');
	}
}

angular.module('app')
	.directive('busyOverlay', () => new BusyOverlay());
