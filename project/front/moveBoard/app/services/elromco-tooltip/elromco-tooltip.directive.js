(function () {
    'use strict';

    angular.module('app')
        .directive('elromcoTooltip', elromcotooltips);

    function elromcotooltips() {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.find('.rmtips').hide();

                element.bind('mouseover', function () {
                    element.find('.rmtips').show();
                });

                element.bind('mouseout', function () {
                    element.find('.rmtips').hide();
                });
            }
        };
    }

})();