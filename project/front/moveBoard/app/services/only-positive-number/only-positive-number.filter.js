'use strict';

angular
	.module('app')
	.filter('onlyPositiveNumber', onlyPositiveNumber);

function onlyPositiveNumber() {
	return number => {
		return Math.abs(number || 0);
	};
}