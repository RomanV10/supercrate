'use strict';

angular.module('app')
	.factory('ElromcoObserver', () => {
		
		const listeners = [];

		function findListener(event, target) {
			return listeners.findIndex(listener =>
				listener.event === event && listener.target === target);
		}

		function checkListener(event, target) {
			return listeners.find(listener =>
				 listener.event === event && listener.target === target);
		}

		class ElromcoObserver {

			addEventListener(event, callback, target) {
				listeners.push({event, callback, target});
			};
		
			removeEventListener(event, target) {
				if(checkListener(arguments)){
					listeners.splice(findListener(arguments), 1);
				}
			};
		
			dispatch(event, data) {
				listeners.forEach(listener => {
					if(listener.event === event && listener.callback){
						listener.callback(data);
					}
				});
			}
		}

		return new ElromcoObserver;
	});
