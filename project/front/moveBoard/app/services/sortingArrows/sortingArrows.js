'use strict';

class SortingArrows {
	constructor() {
		this.restrict = 'EA';
		this.transclude = true;
		this.scope = {
			sortingArrows: '<'
		};
		this.template = require('./sortingArrows.html');
	}
}

angular.module('app')
	.directive('sortingArrows', () => new SortingArrows());
