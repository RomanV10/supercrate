(function () {
    'use strict';

    angular.module('app')
        .directive('erHideIfSmallScreen', erHideSmallScreen);

    function erHideSmallScreen() {
        return {
            restrict: 'A',
            link: function (scope, element) {
                let isSmallScreen = false;

                setElementVisibility();

                angular.element(window).on('resize', setElementVisibility);

                function setElementVisibility() {
                    checkScreenSize();

                    if (isSmallScreen) {
                        element.hide();
                    } else {
                        element.show();
                    }
                }

                function checkScreenSize() {
                    isSmallScreen  = document.documentElement.clientWidth < 1024;
                }
            }
        };
    }

})();