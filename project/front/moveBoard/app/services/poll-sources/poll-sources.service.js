angular
	.module('app.settings')
	.factory('pollSourcesService', pollSourcesService);

/* @ngInject */
function pollSourcesService(datacontext) {
	const hardSources = ['Moveboard', 'Mobile', 'Website'];
	let service = {
		getAllPollSources,
		getRequestPollSources,
	};

	return service;

	function getAllPollSources() {
		let fieldData = datacontext.getFieldData();
		let basicsettings = angular.fromJson(fieldData.basicsettings);

		let parsers = _.get(basicsettings, 'parsers', {});
		let parsersSources = Object.values(parsers)
			.map(parser => parser.name.split(' ')[0]);

		let requestSources = getRequestPollSources();

		let sources = requestSources.concat(parsersSources);

		return _.uniq(sources);
	}

	function getRequestPollSources() {
		let fieldData = datacontext.getFieldData();
		let basicsettings = angular.fromJson(fieldData.basicsettings);

		let pollSources = _.get(basicsettings, 'movecalcFormSettings.pollSources.sources', []);
		let customSources = pollSources.map(item => {
			return item.name;
		});

		let sources = hardSources.concat(customSources);

		return _.uniq(sources);
	}
}
