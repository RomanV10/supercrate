'use strict';

angular
	.module('app.dashboard')
	.controller('Dashboard', Dashboard);

/*@ngInject*/
function Dashboard($scope, $interval, $rootScope, RequestServices, logger, $location, $state, tripRequest,
	erDeviceDetector, datacontext, PermissionsServices, moveBoardApi, apiService) {

	// Inititate the promise tracker to track form submissions.
	var vm = this;
	const OPEN_REQUEST_PATH = '/open_request/';
	const FLAT_RATE_WAIT_OPTION = 11;
	let path = $location.path();
	let requestId = $state.params.id;
	var timer;
	var lastPromises = [];
	const BOOKED_MAP = {
		'1': '1',
		'2': '2',
		'3': '3',
		'4': '4'
	};
	const DATES_FILTER_MAP = {
		'1': '0',
		'2': '0',
		'3': '3',
		'4': '4'
	};
	let activateFunctions = [
		activatePending,
		activateMessages,
		activateConfirmed,
		activateNotConfirmed,
		activateReserved,
		activateHomeEstimate,
		activateInvoices
	];
	// action required requests
	vm.pendingRequests = [];
	vm.confirmedRequests = [];
	vm.bookedRequests = [];
	vm.unconfirmedRequests = [];
	vm.homeEstimateNodes = [];
	vm.unreadComments = [];
	vm.unconfReserved = [];
	vm.table_title = 'Action Required Requests';
	vm.heading = false;
	vm.unreadCount = 0;
	vm.ifnomessages = 0;
	vm.invoices = {};
	vm.unconfirmedCount = 0;
	vm.pendingCount = 0;
	vm.confirmedCount = 0;
	vm.bookedCount = 0;
	vm.unconfirmedReserved = 0;
	vm.homeEstimateTotal = 0;
	vm.pendingStatus = [1, 10, 17, 16, 9, 7];
	vm.ConfirmedStatus = [3];
	vm.UnconfirmedStatus = [2];
	vm.HomeEstimateStatus = [4];
	let currTab = '';
	vm.tabs = [
		{
			name: 'Action Required Requests',
			selected: true,
			status: vm.pendingStatus
		},
		{
			name: 'Unread Messages',
			selected: false
		},
		{
			name: 'Confirmed Requests',
			selected: false,
			status: vm.ConfirmedStatus
		},
		{
			name: 'Not Confirmed Requests',
			selected: false,
			status: vm.UnconfirmedStatus
		},
		{
			name: 'Reserved Requests',
			selected: false,
			status: vm.UnconfirmedStatus
		},
		{
			name: 'In-home Estimate Requests',
			selected: false,
			status: vm.HomeEstimateStatus
		},
		{
			name: 'Total Invoices',
			selected: false
		},
	];
	vm.searchParams = {};
	vm.searchParams.currentPage = 0;
	vm.searchParams.conf_filter = $rootScope.currentUser.userId.settings.conf_filter || '1';
	vm.searchParams.dates_filter = $rootScope.currentUser.userId.settings.all_time || '0';
	vm.searchParams.sorting = 'DESC';
	vm.searchParams.sorting_fields = null;
	vm.searchParams.leadScoreFilter = {
		enabled: false,
		isChanged: false,
		key: '',
		value: {}
	};
	vm.searchParams.status = vm.tabs[0].status;
	///
	let fieldData = datacontext.getFieldData();
	let basicsettings = angular.fromJson(fieldData.basicsettings);
	vm.searchParams.leadscoringsettings = angular.fromJson(fieldData.leadscoringsettings);
	vm.canSeeInvoices = PermissionsServices.hasPermission('canSeeInvoiceDashboardTab')
		&& basicsettings.showInvoiceTabOnDashboard;

	$scope.$watch(function () {
		return vm.searchParams.conf_filter;
	}, function (current, original) {
		if (current != original) {
			vm.searchParams.dates_filter = DATES_FILTER_MAP[vm.searchParams.conf_filter] || '0';
			vm.searchParams.sorting_fields = current ? 'field_date_confirmed' : 'field_date';
			vm.searchParams.currentPage = 0;
			if (_.get($rootScope, 'currentUser.userId.uid')) {
				apiService.postData(moveBoardApi.client.updateUserSettings, {
					uid: $rootScope.currentUser.userId.uid,
					data: {
						all_time: vm.searchParams.dates_filter,
						conf_filter: vm.searchParams.conf_filter,
					},
				});
				activateConfirmed();
			}
		}
	});

	vm.select = function (t) {
		vm.searchParams.currentPage = 0;
		angular.forEach(vm.tabs, function (tab) {
			tab.selected = false;
		});
		vm.tabs[t].selected = true;
		vm.table_title = vm.tabs[t].name;
		vm.reservationReceived = t == 4;
		vm.homeEstimate = t == 5;
	};

	if (path.indexOf(OPEN_REQUEST_PATH) && requestId) {
		tripRequest
			.openRequest(requestId);
	}

	let TrashEvent = $rootScope.$on('request.added', handlerNewRequest);
	let updateRequestDestructor = $scope.$on('request.updated', handlerUpdateRequest);

	let messageAddedDestructor = $scope.$on('messaged.readed', function () {
		vm.unreadCount--;

	});

	vm.refreshDashboard = function () {
		activate();
	};

	function updateInvoicesTotals(dashboard) {
		if (!vm.tabs[6].selected) {
			vm.invoices.all = _.get(dashboard, 'invoices.all', 0);
		}
	}

	function getDashboardTotals() {
		RequestServices.getDashboard()
			.then(function (dashboard) {
				vm.pendingCount = _.get(dashboard, 'pending.count', 0);
				vm.unreadCount = _.get(dashboard, 'comments.count', 0);
				vm.confirmedCount = _.get(dashboard, 'confirmed_move_date.total', 0);
				vm.bookedCount = _.get(dashboard, 'confirmed_booked_date.total', 0);
				vm.allGreenCount = _.get(dashboard, 'confirmed_total_dates.total', 0);
				vm.unconfirmedCount = _.get(dashboard, 'unconfirmed.total', 0);
				vm.unconfirmedReserved = _.get(dashboard, 'unconfirmed_reserved.total', 0);
				vm.homeEstimateTotal = _.get(dashboard, 'home_estimate.total', 0);
				
				updateInvoicesTotals(dashboard);
				activate();
			});
	}

	if (erDeviceDetector.isDesktop) {
		if (basicsettings.intervalLoadDashboardDismissed) {
			activate();
		} else {
			timer = $interval(function () {
				getDashboardTotals();
			}, 60000);
		}
	}


	let updateRequestAllDataDestructor = $scope.$on('request.all_data', function (event, data) {
		if (vm.pendingRequests[data.nid]) {
			vm.pendingRequests[data.nid].request_all_data = data.all_data;
		} else if (vm.unconfirmedRequests[data.nid]) {
			vm.unconfirmedRequests[data.nid].request_all_data = data.all_data;
		}
	});

	let addedNewRequestDestructor = $rootScope.$on('request.addNewRequest', function (ev) {
		vm.refreshDashboard();
	});

	$scope.$on('paymentModalClosed', getDashboardTotals);

	function initialDashboard() {
		vm.busy = true;
		RequestServices.getDashboard()
		//Permissions
			.then(function (dashboard) {
				vm.pendingCount = _.get(dashboard, 'pending.count', 0);
				vm.pendingRequests = _.get(dashboard, 'pending.nodes', []);

				vm.unreadCount = _.get(dashboard, 'comments.count', 0);
				vm.ifnomessages = vm.unreadCount;
				vm.unreadComments = _.get(dashboard, 'comments.new_comments', []);

				vm.confirmedRequests = _.get(dashboard, 'confirmed_move_date.nodes', []);
				vm.confirmedCount = _.get(dashboard, 'confirmed_move_date.total', 0);

				vm.bookedRequests = _.get(dashboard, 'confirmed_booked_date.nodes', []);
				vm.bookedCount = _.get(dashboard, 'confirmed_booked_date.total', 0);

				vm.allGreenRequests = _.get(dashboard, 'confirmed_total_dates.nodes', []);
				vm.allGreenCount = _.get(dashboard, 'confirmed_total_dates.total', 0);

				vm.unconfirmedRequests = _.get(dashboard, 'unconfirmed.nodes', []);
				vm.unconfirmedCount = _.get(dashboard, 'unconfirmed.total', 0);

				vm.unconfReserved = _.get(dashboard, 'unconfirmed_reserved.nodes', []);
				vm.unconfirmedReserved = _.get(dashboard, 'unconfirmed_reserved.total', 0);

				vm.homeEstimateNodes = _.get(dashboard, 'home_estimate.nodes', []);
				vm.homeEstimateTotal = _.get(dashboard, 'home_estimate.total', 0);

				updateInvoicesTotals(dashboard);

			}, function (reason) {
				logger.error(reason, reason, 'Error');
			})
			.finally(function () {
				vm.busy = false;
			});
	}

	initialDashboard();

	function activate() {
		vm.busy = true;
		vm.reservationReceived = false;
		vm.homeEstimate = false;
		_.forEach(vm.tabs, function (tab, i) {
			if (tab.selected) {
				currTab = i;
			}
		});
		vm.searchParams.status = vm.tabs[currTab].status;
		lastPromises = [];
		activateFunctions[currTab]();
	}

	function activatePending() {
		let params = {
			status: vm.searchParams.status,
			page: vm.searchParams.currentPage,
			booked: undefined,
			sorting: vm.searchParams.sorting,
			sort_field: vm.searchParams.sorting_fields,
			leadScoreFilter: vm.searchParams.leadScoreFilter,
		};
		RequestServices.getStatusRequestsByPage(params)
			.then(function (dashboard) {
				vm.pendingRequests = dashboard.data.nodes;
				vm.pendingCount = dashboard.data.count;
			}, function (reason) {
				logger.error(reason, reason, 'Error');
			})
			.finally(function () {
				vm.busy = false;
			});
	}

	function activateMessages() {
		vm.busy = false;
		$scope.busy = false;
	}

	function activateConfirmed() {
		let params = {
			status: vm.searchParams.status,
			page: vm.searchParams.currentPage,
			booked: BOOKED_MAP[vm.searchParams.conf_filter],
			sorting: vm.searchParams.sorting,
			sort_field: vm.searchParams.sorting_fields,
			leadScoreFilter: vm.searchParams.leadScoreFilter,
			dates_filter: vm.searchParams.dates_filter
		};
		RequestServices.getStatusRequestsByPage(params)
			.then(function (dashboard) {
				if (BOOKED_MAP[vm.searchParams.conf_filter] == 1) {
					vm.bookedRequests = dashboard.data.nodes;
					vm.bookedCount = dashboard.data.count;
				} else if (BOOKED_MAP[vm.searchParams.conf_filter] == 2) {
					vm.confirmedRequests = dashboard.data.nodes;
					vm.confirmedCount = dashboard.data.count;
				} else if (BOOKED_MAP[vm.searchParams.conf_filter] == 3) {
					vm.allGreenRequests = dashboard.data.nodes;
					vm.allGreenCount = dashboard.data.count;
				} else if (BOOKED_MAP[vm.searchParams.conf_filter] == 4) {
					vm.confirmedRequests = dashboard.data.nodes;
					vm.confirmedCount = dashboard.data.count;
				}
			})
			.catch(function (reason) {
				logger.error(reason, reason, 'Error');
			})
			.finally(() => {
				vm.busy = false;
			});
	}

	function activateNotConfirmed() {
		let statuses = [
			...vm.searchParams.status,
			FLAT_RATE_WAIT_OPTION,
		];
		let params = {
			status: statuses,
			page: vm.searchParams.currentPage,
			booked: undefined,
			sorting: vm.searchParams.sorting,
			sort_field: vm.searchParams.sorting_fields,
			leadScoreFilter: vm.searchParams.leadScoreFilter,
		};
		RequestServices.getStatusRequestsByPage(params)
			.then(function (dashboard) {
				vm.unconfirmedRequests = dashboard.data.nodes;
				vm.unconfirmedCount = dashboard.data.count;
				_.forEachRight(vm.unconfirmedRequests, function (req, ind) {
					if (req.field_reservation_received.value === true) {
						vm.unconfirmedRequests.splice(ind, 1);
						vm.unconfirmedCount--;
					}
				});
			})
			.catch(function (reason) {
				logger.error(reason, reason, 'Error');
			})
			.finally(function () {
				vm.busy = false;
			});
	}

	function activateReserved() {
		vm.reservationReceived = true;
		vm.homeEstimate = false;
		let params = {
			status: vm.searchParams.status,
			page: vm.searchParams.currentPage,
			booked: undefined,
			sorting: vm.searchParams.sorting,
			sort_field: vm.searchParams.sorting_fields,
			leadScoreFilter: vm.searchParams.leadScoreFilter,
			reservation_received: true,
			currTab: currTab,
		};
		RequestServices.getStatusRequestsByPage(params)
			.then(function (dashboard) {
				vm.unconfReserved = dashboard.data.nodes;
				vm.unconfirmedReserved = dashboard.data.count;
			})
			.catch(function (reason) {
				logger.error(reason, reason, 'Error');
			})
			.finally(function () {
				vm.busy = false;
			});
	}

	function activateHomeEstimate() {
		vm.reservationReceived = false;
		vm.homeEstimate = true;
		let params = {
			status: vm.searchParams.status,
			page: vm.searchParams.currentPage,
			booked: undefined,
			sorting: vm.searchParams.sorting,
			sort_field: vm.searchParams.sorting_fields,
			leadScoreFilter: vm.searchParams.leadScoreFilter,
		};
		RequestServices.getStatusRequestsByPage(params)
			.then(dashboard => {
				console.log(dashboard);
			})
			.catch(reason => {
				console.log(reason);
			})
			.finally(() => {
				vm.busy = false;
			});
	}

	function activateInvoices() {
		vm.busy = false;
		$scope.$broadcast('updateInvoices');
	}

	function handlerNewRequest(e, data) {
		if (!!data) {
			var requestsKeys = _.keys(data);
			if (!!requestsKeys.length) {
				vm.pendingCount++;
				vm.pendingRequests.unshift(data[requestsKeys[0]]);
			}
			$scope.busy = false;
		}
	}

	function handlerUpdateRequest(event, data) {
		var nid = data.nid;

		//add request
		// if request is pending
		if (_.indexOf(vm.pendingStatus, Number(data.status.raw)) > -1) {
			vm.pendingCount++;
			vm.pendingRequests.push(data);
		}

		// if request is inhome estimate
		if (data.status.raw == 4) {
			vm.homeEstimateTotal++;
			vm.homeEstimateNodes.push(data);
		}

		// if request was inhome estimate
		if (_.indexOf(vm.HomeEstimateStatus, Number(data.status.old)) > -1) {
			vm.homeEstimateTotal--;
			let ind = _.findIndex(vm.homeEstimateNodes, {nid: nid});
			if (ind > -1) {
				vm.homeEstimateNodes.splice(ind, 1);
			}
		}

		// if request is not confirmed
		if (data.status.raw == 2) {
			if (!data.field_reservation_received.value) {
				vm.unconfirmedCount++;
				vm.unconfirmedRequests.push(data);

			}
			// if request is reserved
			if (data.field_reservation_received.value) {
				vm.unconfirmedReserved++;
				vm.unconfReserved.push(data);
			}
		}
		if (data.status.raw == 3) {
			// if request is confirmed (moved in this month)
			if (moment(data.date.value, 'MM/DD/YYYY').month() == moment().month()) {
				vm.confirmedCount++;
				vm.confirmedRequests.push(data);
			}
			// if request is booked now(in this month)
			vm.bookedCount++;
			vm.bookedRequests.push(data);
			vm.allGreenRequests.push(data);
			vm.allGreenCount++;
		}

		// if request was pending
		if (_.indexOf(vm.pendingStatus, Number(data.status.old)) > -1) {
			vm.pendingCount--;
			let ind = _.findIndex(vm.pendingRequests, {nid: nid});
			if (ind > -1) {
				vm.pendingRequests.splice(ind, 1);
			}
		}
		if (data.status.old == 3) {
			// if request was confirmed (moved in this month)
			// vm.confirmedRequests, vm.confirmedCount
			if (moment(data.date.value, 'MM/DD/YYYY').month() == moment().month()) {
				vm.confirmedCount--;
				let ind = _.findIndex(vm.confirmedRequests, {nid: nid});
				if (ind > -1) {
					vm.confirmedRequests.splice(ind, 1);
				}
			}

			// if request was booked in this month
			// vm.bookedRequests, vm.bookedCount
			if (moment.unix(data.field_date_confirmed.value).month() == moment().month()) {
				vm.bookedCount--;
				let ind = _.findIndex(vm.bookedRequests, {nid: nid});
				if (ind > -1) {
					vm.bookedRequests.splice(ind, 1);

				}
			}
			let ind = _.findIndex(vm.allGreenRequests, {nid: nid});
			if (ind > -1) {
				vm.allGreenRequests.splice(ind, 1);

			}
			vm.allGreenCount--;
		}
		if (data.status.old == 2) {
			// if request is not confirmed
			if (!data.field_reservation_received.value) {
				vm.unconfirmedCount--;
				let ind = _.findIndex(vm.unconfirmedRequests, {nid: nid});
				if (ind > -1) {
					vm.unconfirmedRequests.splice(ind, 1);
				}
			}
			// if request is reserved
			if (data.field_reservation_received.value) {
				vm.unconfirmedReserved--;
				let ind = _.findIndex(vm.unconfReserved, {nid: nid});
				if (ind > -1) {
					vm.unconfReserved.splice(ind, 1);
				}
			}
		}
	}


	$scope.$on('$destroy', function () {
		TrashEvent();
		updateRequestDestructor();
		messageAddedDestructor();
		updateRequestAllDataDestructor();
		addedNewRequestDestructor();

		$interval.cancel(timer);
		RequestServices.cancel(lastPromises);
	});
}
