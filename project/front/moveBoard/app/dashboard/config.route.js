(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('dashboard', {
                        url: '/dashboard',
                        template: require('./dashboard.html'),
                        title: 'dashboard',
                        data: {
                            permissions: {
                                except: ['anonymous','foreman','helper', 'home-estimator']
                            }
                        }
                    })
                    .state('dashboard.openRequest', {
                        url: '/open_request/:id',
	                    template: require('./dashboard.html'),
                        title: 'Requests',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper', 'home-estimator']
                            }
                        }
                    });
            }
        ]
    );
})();
