'use strict';
import './score-filter.styl'

angular
	.module('app.scoreFilter')
	.directive('scoreFilter', scoreFilter);

	scoreFilter.$inject = ['datacontext'];

	function scoreFilter(datacontext) {
		return {
			template: require('./score-filter.tpl.pug'),
			restrict: 'EA',
			scope: {
				leadScoreFilter: '=',
				getRequestsByLeadScoreFilter: '='
			},
			link: linkFunk
		};

		function linkFunk($scope) {
			let fieldData = datacontext.getFieldData();
			$scope.leadscoringsettings = angular.fromJson(fieldData.leadscoringsettings);
		}
	}
