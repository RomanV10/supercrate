'use strict';
import './tooltip.styl'

angular
	.module('app')
	.directive('mbTooltip', tooltipDirective);

tooltipDirective.$inject = ['$rootScope', 'CalculatorServices'];
function tooltipDirective($rootScope, CalculatorServices) {
	return {
		scope: {
			request: '<',
			invoice: '<',
			close: "=",
			state: "@"
		},
		template: require('./tooltip.pug'),
		restrict: 'A',
		link: ($scope, element) => {
			init();
			var broadcastWatcher;
			
			function init() {
				$scope.isClosing = $scope.state == "closing";
				broadcastWatcher = $scope.$on('updateTooltip', update);
				update();
			}
			
			function update() {
				$scope.sales = {
					cf: +$scope.request.visibleWeight,
					minWeight: +$scope.request.request_all_data.min_weight,
					minPrice: +$scope.request.request_all_data.min_price,
					minPriceEnabled: +$scope.request.request_all_data.min_price_enabled,
					rate: +_.get($scope, "request.field_long_distance_rate.value", 0),
					total: +CalculatorServices.getLongDistanceQuote($scope.request)
				};
				
				calcSales();
				if ($scope.isClosing) {
					$scope.closing = {
						cf: +_.get($scope, "invoice.closing_weight.value"),
						rate: +_.get($scope, "invoice.field_long_distance_rate.value", 0),
						total: +CalculatorServices.getLongDistanceQuote($scope.invoice, true)
					};
					
					calcClosing();
				}
			}
			
			function calcSales() {
				$scope.sales.weightWoutMinweight = $scope.sales.cf - $scope.sales.minWeight;
				if ($scope.sales.weightWoutMinweight < 0) {
					$scope.sales.weightWoutMinweight = 0;
				}
			}
			
			function calcClosing() {
				$scope.closing.originTotal = 0;
				if ($scope.sales.minPriceEnabled) {
					$scope.closing.originTotal += $scope.sales.minPrice;
				} else {
					$scope.closing.originTotal += $scope.sales.minWeight * $scope.closing.rate;
				}
				$scope.closing.originTotal += $scope.sales.weightWoutMinweight * $scope.closing.rate;
				
				$scope.closing.weightWoutMinweight = $scope.closing.cf - $scope.sales.minWeight;
				if ($scope.closing.weightWoutMinweight < 0) {
					$scope.closing.weightWoutMinweight = 0;
				}
				let salesWeight = ($scope.sales.minWeight > $scope.sales.cf)
					? $scope.sales.minWeight : $scope.sales.cf;
				$scope.closing.additionalCf = $scope.closing.cf - salesWeight;
				
				if ($scope.closing.cf < $scope.sales.minWeight) {
					$scope.closing.additionalCf += $scope.sales.minWeight - $scope.closing.cf;
				}
			}
			
			$scope.$on('$destroy', () => {
				broadcastWatcher();
			});
		}
	}
}
