describe('Directive: elromcoNotificationCenter,', () => {
	let $scope, $localScope, element;
	let allNotifsWithStatus_empty, newNotifs_empty, allNotifsWithStatus_full, newNotifs_full, userAllowedNotifs;
	let SweetAlert;

	beforeEach(inject(function (_SweetAlert_) {
		$scope = $rootScope.$new();
		SweetAlert = _SweetAlert_;

		allNotifsWithStatus_empty = testHelper.loadJsonFile('get_all_notifications_with_status_(empty).mock');
		newNotifs_empty = testHelper.loadJsonFile('get_new_notifications_(empty).mock');
		allNotifsWithStatus_full = testHelper.loadJsonFile('get_all_notifications_with_status_(full).mock');
		newNotifs_full = testHelper.loadJsonFile('get_new_notifications_(full).mock');
		userAllowedNotifs = testHelper.loadJsonFile('user_allowed_notifications.mock');
	}));

	function compileElement() {
		angular.element("body").append("<div id='js-notificaiton-audio'></div>");
		element = angular.element('<div elromco-notifications-center></div>');
		element = $compile(element)($scope);
		$localScope = element.isolateScope();
	}

	describe("feature: alert on system notifications,", () => {
		describe("When system notif already exist,", () => {
			beforeEach(() => {
				spyOn(SweetAlert, 'swal').and.callFake(() => {});
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.notifications.allowedNotificationTypes))
					.respond(200, userAllowedNotifs);
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.notifications.getAllNotifications))
					.respond(200, allNotifsWithStatus_full);
				compileElement();
				$httpBackend.flush();
				$httpBackend.flush();
			});

			it("Should set importantMessages to 1", () => {
				expect($localScope.importantMessages).toEqual(1);
			});

			it('Should show sweet alert when important notification exists', () => {
				expect(SweetAlert.swal).toHaveBeenCalled();
			});
		});

		describe("When coming new system notif,", () => {
			it("Should set importantMessages to 1", () => {
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.notifications.allowedNotificationTypes))
					.respond(200, userAllowedNotifs);
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.notifications.getAllNotifications))
					.respond(200, allNotifsWithStatus_empty);
				$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.notifications.getNewNotifications))
					.respond(200, newNotifs_full);

				window.localStorage.turnOffNotifications = 0;
				compileElement();
				$httpBackend.flush();
				$timeout.flush();
				expect($localScope.importantMessages).toEqual(1);

			})
		});

		describe("When system notif not exist,", () => {
			it("Should set importantMessages to 0", () => {
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.notifications.allowedNotificationTypes))
					.respond(200, userAllowedNotifs);
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.notifications.getAllNotifications))
					.respond(200, allNotifsWithStatus_empty);
				compileElement();
				$httpBackend.flush();
				$httpBackend.flush();
				expect($localScope.importantMessages).toEqual(0);
			})
		});

		describe("When system notif not coming,", () => {
			it("Should set importantMessages to 1", () => {
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.notifications.allowedNotificationTypes))
					.respond(200, userAllowedNotifs);
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.notifications.getAllNotifications))
					.respond(200, allNotifsWithStatus_empty);
				$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.notifications.getNewNotifications))
					.respond(200, newNotifs_empty);

				window.localStorage.turnOffNotifications = 0;
				compileElement();
				$httpBackend.flush();
				$timeout.flush();
				expect($localScope.importantMessages).toEqual(0);

			})
		});
	});

});
