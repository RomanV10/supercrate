'use strict';

import './elromcoNotificationsCenter.styl';

const GET_NEW_NOTIFICATION_TIMEOUT = 15000;
const HIDE_FIRST_NOTIFICATION_TIMEOUT = 100;
const GET_NEW_NOTIFICATIONS_TIMEOUT = 5000;
const IMPORTANT_NOTIFICATION_TYPE = 30;

angular
	.module('app')
	.directive('elromcoNotificationsCenter', elromcoNotificationsCenter);

/* @ngInject */
function elromcoNotificationsCenter(apiService, moveBoardApi, $timeout, SweetAlert, Raven) {
	return {
		scope: {},
		template: require('./elromcoNotificationsCenter.pug'),
		restrict: 'A',
		link: ($scope, element) => {
			$scope.new_message = false;
			$scope.showList = false;
			$scope.showListDerty = false;
			$scope.notifications = [];
			$scope.newNotifications = [];
			$scope.haveNewNotifications = false;
			$scope.checkFilters = 0;
			$scope.meta = [{}, {}];
			$scope.busy = false;
			$scope.updateListBusy = false;
			$scope.importantMessages = 0;
			var date = 0;
			var currentStatus = 0;
			var currentShowIndex = 0;
			var currentUrl = window.location.protocol + '//' + window.location.hostname + ':' + window.location.port;
			var currentPage = 1;
			var pageLoaded = true;
			var messages = '.elromco-notifications__message_show';
			var list = $('.elromco-notifications__list-item');
			var audio = document.getElementById('js-notificaiton-audio');
			var turnOffNotifications = window.localStorage.getItem('turnOffNotifications');
			$scope.turnOffNotifications = turnOffNotifications || 1;
			audio.volume = 0.5;

			if (element.find('.js-custom-scrollbar').length) {
				element.find('.js-custom-scrollbar').mCustomScrollbar({theme: 'dark'});
			}

			$timeout(() => {
				getNewNotifications();
			}, GET_NEW_NOTIFICATION_TIMEOUT);

			if ($scope.showList) {
				$scope.busy = true;
			}

			apiService
				.postData(moveBoardApi.notifications.allowedNotificationTypes, {})
				.then(data => {
					$scope.busy = false;
					$scope.notificationTypes = data.data;
					$scope.checkFilters = buildSelectedNotifications().length;
					getAllNotifications();
					switchAllNotification();
				});

			element
				.find('md-content')
				.on('scroll', event => {
					if (list.height() < (event.target.scrollTop + event.target.offsetHeight) && pageLoaded && $scope.meta[currentStatus].pageCount !== currentPage) {
						showNextPage();
					}
				});

			$scope.showAllNotifications = () => {
				$scope.showList = !$scope.showList;
				$scope.openFilters = false;
				$scope.showListDerty = true;
				$scope.newNotifications.forEach(not => {
					if (not.close) {
						not.close();
					}
				});
				$scope.newNotifications = [];
			};

			$scope.setViewed = notification => {
				if ($scope.showList) {
					$scope.busy = true;
				}

				notification.status = '1';

				apiService
					.postData(moveBoardApi.notifications.updateStatus, {
						type: notification.id,
						status: 1
					})
					.then(() => {
						let isImportant = notification.notification_type == IMPORTANT_NOTIFICATION_TYPE;
						if (isImportant) $scope.importantMessages--;
						$scope.busy = false;
					});
			};

			$scope.notificationSoundChange = () => {
				$scope.turnOffNotifications = $scope.turnOffNotifications == 0 ? 1 : 0;

				if (window.localStorage) {
					window.localStorage.setItem('turnOffNotifications', $scope.turnOffNotifications );
				}
			};

			$scope.setRestore = notification => {
				if ($scope.showList) {
					$scope.busy = true;
				}

				notification.status = '0';

				apiService
					.postData(moveBoardApi.notifications.updateStatus, {
						type: notification.id,
						status: 0
					})
					.then(() => {
						$scope.busy = false;
					});
			};

			$scope.changeFilters = item => {
				apiService
					.postData(moveBoardApi.notifications.switchShowNotification, {
						types: [item.ntid],
						status: item.on
					})
					.then(() => {
						getAllNotifications(true);
					});

				switchAllNotification();
				$scope.checkFilters = buildSelectedNotifications().length;
			};

			$scope.updateEmailNotification = item => {
				apiService
					.postData(moveBoardApi.notifications.updateEmailSettings, {
						how_often: item.info.mail,
						ntid: item.ntid
					});
			};

			$scope.checkAll = () => {
				if ($scope.showList) {
					$scope.busy = true;
				}

				apiService
					.postData(moveBoardApi.notifications.setAllCheck, {
						ntids: buildSelectedNotifications(),
						status: 1
					})
					.then(() => {
						$scope.busy = false;
						getAllNotifications();
					});
			};

			$scope.changeStatus = status => {
				currentStatus = status;
				getAllNotifications();
			};

			$scope.changeUserEmailSetting = item => {
				if ($scope.showList) {
					$scope.busy = true;
				}

				apiService
					.postData(moveBoardApi.notifications.updateUserEmailSetting, {
						how_often: item.mail,
						ntid: item.ntid
					})
					.then(() => {
						$scope.busy = false;
					});
			};


			function getAllNotificationIds() {
				var result = [];
				$scope.notificationTypes.forEach(type => result.push(type.ntid));
				return result;
			}

			function checkIsConfirmed(string) {
				let result = string.includes("to:  Confirmed") || string.includes("to  Confirmed") || string.includes("is confirmed");
				return result;
			}

			function showNextPage() {
				let buildedNotifications = buildSelectedNotifications();
				if (buildedNotifications.length === 0) return;
				if ($scope.showList) {
					$scope.busy = true;
					$scope.updateListBusy = true;
				}

				pageLoaded = false;
				currentPage++;

				apiService
					.postData(moveBoardApi.notifications.getAllNotifications, {
						page_size: 20,
						status: currentStatus,
						page: currentPage,
						filters: {
							notification_type: buildedNotifications
						}
					})
					.then(res => {
						if (res.data.items) {
							for (var i = 0; i < res.data.items.length; i++) {
								res.data.items[i].isStatusChangedToConfirmed = checkIsConfirmed(res.data.items[i].data.text);
								$scope.notifications.push(res.data.items[i]);
							}
						}

						$scope.busy = false;
						$scope.updateListBusy = false;
						$scope.meta[currentStatus] = res.data._meta;
						pageLoaded = true;
					});
			}

			function buildSelectedNotifications() {
				var result = [];

				if ($scope.notificationTypes) {
					for (var i = 0; i < $scope.notificationTypes.length; i++) {
						if ($scope.notificationTypes[i].on == 1) {
							result.push(Number($scope.notificationTypes[i].ntid));
						}
					}
				}

				return result;
			}

			$scope.turnAllNotifications = () => {
				$scope.notificationTypes.forEach(notification => {
					notification.on = $scope.notificationTurnAll;
				});

				apiService
					.postData(moveBoardApi.notifications.switchShowNotification, {
						types: getAllNotificationIds(),
						status: $scope.notificationTurnAll
					})
					.then(() => {
						getAllNotifications(true);
					});
			};


			function getAllNotifications() {
				let buildedNotifications = buildSelectedNotifications();
				if (buildedNotifications.length === 0) return;

				if ($scope.showList) {
					$scope.busy = true;
					$scope.updateListBusy = true;
				}

				apiService
					.postData(moveBoardApi.notifications.getAllNotifications, {
						page_size: 20,
						page: 1,
						status: currentStatus,
						filters: {
							notification_type: buildedNotifications
						}
					})
					.then(res => {
						$scope.importantMessages = 0;
						if (res.data.items) {
							$scope.notifications = res.data.items;
							_.each($scope.notifications, item => {
								item.isStatusChangedToConfirmed = checkIsConfirmed(item.data.text);
								checkImportantNotification(item);
							});
						} else {
							$scope.notifications = [];
						}

						$scope.busy = false;
						$scope.updateListBusy = false;
						$scope.meta[currentStatus] = res.data._meta;
						$scope.haveNewNotifications = res.data.count_new;
						currentPage = 1;

						if (date == 0 && $scope.notifications.length) {
							date = $scope.notifications[0].created;
						}
					});
			}

			function switchAllNotification() {
				var allCount, selectedCount;

				selectedCount = allCount = 0;
				$scope.notificationTypes.forEach(notification => {
					allCount++;
					if (notification.on == 1) {
						selectedCount++;
					}
				});

				if (allCount === selectedCount) {
					$scope.notificationTurnAll = 1;
				} else {
					$scope.notificationTurnAll = 0;
				}
			}

			function hideFirsNotification() {
				if ($scope.newNotifications.length > currentShowIndex) {
					$scope.newNotifications[currentShowIndex].show = true;
					currentShowIndex++;
				} else {
					currentShowIndex = 0;
					$scope.newNotifications = [];
				}

				$timeout(() => {
					if (document.querySelector(messages)) {
						document.querySelector(messages).removeEventListener("animationend", hideFirsNotification);
						document.querySelector(messages).addEventListener("animationend", hideFirsNotification);
					}
				}, HIDE_FIRST_NOTIFICATION_TIMEOUT);
			}


			function getNewNotifications() {
				let buildedNotifications = buildSelectedNotifications();
				if (buildedNotifications.length != 0) {
					if ($scope.showList) {
						$scope.busy = true;
					}

					apiService
						.postData(moveBoardApi.notifications.getNewNotifications, {
							date: date,
							filters: {
								notification_type: buildedNotifications
							}
						})
						.then(res => {
							if (res.data.items) {
								for (var i = 0; i < res.data.items.length; i++) {
									res.data.items[i].isStatusChangedToConfirmed = checkIsConfirmed(res.data.items[i].data.text);

									try {
										if (res.data.items[res.data.items.length - i - 1].status == '0' && "Notification" in window) {
											if (Notification.permission !== "granted") {
												Notification.requestPermission();
												$scope.newNotifications.push(res.data.items[res.data.items.length - i - 1]);
											} else {
												$scope.newNotifications.push(new Notification(res.data.items[res.data.items.length - i - 1].data.source, {
													body: res.data.items[res.data.items.length - i - 1].data.text,
													icon: currentUrl + "/moveBoard/content/img/logo.png",
													sound: currentUrl + "/moveBoard/content/audio/notification.mp3"

												}));
											}
										}
									} catch (error) {
										Raven.captureException(error);
									}

									res.data.items[res.data.items.length - i - 1].showMsg = true;
									$scope.notifications.unshift(res.data.items[res.data.items.length - i - 1]);

									if (+$scope.turnOffNotifications) {
										try {
											audio.play();
										} catch (e) {
											console.log(e);
										}
									}

									checkImportantNotification(res.data.items[i]);
								}

								if (res.data.items.length) {
									date = res.data.items[0].created;
									hideFirsNotification();
								}
							}

							$scope.busy = false;
							$scope.haveNewNotifications = res.data.count_new;

							$timeout(() => {
								getNewNotifications();
							}, GET_NEW_NOTIFICATIONS_TIMEOUT);
						});
				} else {
					$timeout(() => {
						getNewNotifications();
					}, GET_NEW_NOTIFICATIONS_TIMEOUT);
				}
			}

			function checkImportantNotification(item) {
				if (item.notification_type == IMPORTANT_NOTIFICATION_TYPE && !item.status) {
					$scope.importantMessages++;
					showImportantMessageText(item);
				}
			}
			
			function showImportantMessageText(notification) {
				let isWaitFourTimes = $scope.importantMessages % 4;

				if (isWaitFourTimes == 0 || $scope.importantMessages == 1) {
					SweetAlert.swal({
						title: notification.data.source,
						text: notification.data.text,
						type: 'info'
					});
				}
			}
		}
	};
}
