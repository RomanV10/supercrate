import './elromcoDots.styl'

angular.module('app')
	.directive('elromcoDots', elromcoDots);

function elromcoDots() {
	return {
		scope: {
			count: '@',
			currentIndex: '=',
			changeIndex: '='
		},
		restrict: 'A',
		template: require('./elromcoDots.pug'),
		link: ($scope, element) => {
			$scope.items = [];
			
			for (let i = 0; i < $scope.count; i++) {
				$scope.items.push({id: i, active: false});
			}
			
			$scope.currentIndex = $scope.currentIndex || 0;
			
			if ($scope.currentIndex > $scope.count) {
				$scope.currentIndex = 0;
			}
			
			$scope.items[$scope.currentIndex].active = true;
			$scope.currentIndex = $scope.items[$scope.currentIndex].id;
			
			$scope.setActive = item => {
				$scope.items.forEach(item => item.active = false);
				item.active = true;
				$scope.currentIndex = item.id;
				$scope.changeIndex(item.id);
			}
		}
	}
}