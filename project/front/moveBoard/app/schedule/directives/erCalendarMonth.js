(function () {
    'use strict';

    angular
        .module('app.schedule')
        .directive('erCalendarMonth', erCalendarMonth);


    function erCalendarMonth(scheduleHelper, datacontext, EditRequestServices, logger, common) {
        var directive = {
            template: require('../templates/calendarMonthView.html'),
            restrict: 'A',
            scope: {
                requests: '=requests',
                currentDay: '=',
                cellModifier: '=',
            },
            controller: ErScheduleMonthCtrl
        };

        return directive;

		/* @ngInject */
        function ErScheduleMonthCtrl($scope, scheduleHelper, scheduleConfig) {
            var vm = $scope;
            vm.parklotControl = [];
            vm.isCollapsed = false;

            vm.calendarConfig = scheduleConfig;

            $scope.$on('calendar.refreshView', function () {
                vm.weekDays = scheduleHelper.getWeekDayNames();
                vm.view = scheduleHelper.getMonthView(vm.requests, vm.currentDay, vm.cellModifier);
                vm.monthOffsets = [];

                for (var i = 0; i < 8; i++) {
                    vm.monthOffsets.push(i * 7);
                }

                //Auto open the calendar to the current day if set
                if (vm.autoOpen) {
                    vm.view.forEach(function (day) {
                        if (day.inMonth && moment(vm.currentDay).startOf('day').isSame(day.date) && !vm.openDayIndex) {
                            vm.dayClicked(day, true);
                        }
                    });
                }
            });

            $scope.$on('request.updated', function (event, request) {
                _.forEach(vm.view, function (date, ind) {
                    if (date.currentDate == request.date.value) {
                        _.forEach(vm.view[ind].trucks, function (value, index) {
                            if (value == request.trucks.value.name) {
                                _.forEach(vm.view[ind].parklot.confirmed[index], function (schedule, key) {
                                    if (schedule.nid == request.nid) {
                                        vm.view[ind].parklot.confirmed[index][key] = _.clone(request);
                                    }
                                });

                                _.forEach(vm.view[ind].parklot.unconfirmed[index], function (schedule, key) {
                                    if (schedule.nid == request.nid) {
                                        vm.view[ind].parklot.unconfirmed[index][key] = _.clone(request);
                                    }
                                })
                            }
                        })
                    }
                })
            });

            $scope.$on('shedule.closeDay', function (data) {
                vm.openRowIndex = null;
            });

            vm.dayClicked = function (day, index) {
                vm.openRowIndex = null;
                var dayIndex = vm.view.indexOf(day);

                if (dayIndex === vm.openDayIndex) { //the day has been clicked and is already open
                    vm.openDayIndex = null; //close the open day
                } else {
                    vm.openDayIndex = dayIndex;
                    vm.openRowIndex = Math.floor(dayIndex / 7);


                    vm.dayRequests = scheduleHelper.filterRequestsbyDay(vm.requests, day.date);
                    //  var modal = '.modalId_'+vm.openRowIndex;
                    // vm.parklotControl.reinitParklotRequests(vm.dayRequests,modal);
                    // EditRequestServices.openModal(vm.dayRequests[0], vm.dayRequests);
                }

                common.$timeout(function () {
                    var data = {
                        'index': index,
                        'curDate': day.date
                    };

                    $scope.$broadcast('parklot.dayClicked', data);
                });

            };

            vm.highlightEvent = function (event, shouldAddClass) {
                vm.view.forEach(function (day) {
                    delete day.highlightClass;
                    if (shouldAddClass) {
                        var dayContainsEvent = day.events.indexOf(event) > -1;
                        if (dayContainsEvent) {
                            day.highlightClass = 'day-highlight dh-event-' + event.type;
                        }
                    }
                });
            };

            vm.handleEventDrop = function (event, newDayDate) {
                var newStart = moment(event.startsAt)
                    .date(moment(newDayDate).date())
                    .month(moment(newDayDate).month());

                var newEnd = calendarHelper.adjustEndDateFromStartDiff(event.startsAt, newStart, event.endsAt);

                vm.onEventTimesChanged({
                    calendarEvent: event,
                    calendarDate: newDayDate,
                    calendarNewEventStart: newStart.toDate(),
                    calendarNewEventEnd: newEnd ? newEnd.toDate() : null
                });
            };
        }
    }


})();
