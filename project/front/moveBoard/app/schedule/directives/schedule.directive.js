'use strict';

angular
	.module('app.schedule')
	.directive('ccSchedule', ccSchedule);


function ccSchedule() {
	var directive = {
		template: require('../templates/schedule.html'),
		restrict: 'A',
		scope: {
			requests: '=requests',
			view: '=',
			viewTitle: '=?',
			currentDay: '=',
			cellModifier: '&',
		},

		controller: ErScheduleCtrl

	};
	return directive;

	/* @ngInject */
	function ErScheduleCtrl($scope, $log, $timeout, $attrs, $locale, scheduleTitle) {
		const MOMENT_PRECISION = 'day';

		var vm = $scope;
		var previousDate = moment(vm.currentDay);
		var previousView = vm.view;
		vm.requests = vm.requests || [];
		vm.changeView = function (view, newDay) {
			vm.view = view;
			vm.currentDay = newDay;
		};

		vm.drillDown = function (date) {
			var rawDate = moment(date).toDate();
			var nextView = {
				year: 'month',
				month: 'day',
				week: 'day'
			};

			if (vm.onDrillDownClick({calendarDate: rawDate, calendarNextView: nextView[vm.view]}) !== false) {
				vm.changeView(nextView[vm.view], rawDate);
			}
		};

		function refreshCalendar() {
			if (scheduleTitle[vm.view] && angular.isDefined($attrs.viewTitle)) {
				vm.viewTitle = $scope.viewTitle[vm.view](vm.currentDay);
			}

			//if on-timespan-click="calendarDay = calendarDate" is set then don't update the view as nothing needs to change
			var currentDate = moment(vm.currentDay);
			previousDate = currentDate;
			previousView = vm.view;

			let isSamePeriod = previousDate.clone().startOf(vm.view).isSame(currentDate.clone().startOf(vm.view), MOMENT_PRECISION);
			let isSameDay = previousDate.isSame(currentDate, MOMENT_PRECISION);
			let periodNotChanged = vm.view === previousView;
			let shouldUpdate = !isSamePeriod || isSameDay || !periodNotChanged;
			if (shouldUpdate) {
				// a $timeout is required as $broadcast is synchronous so if a new events array is set the calendar won't update
				$timeout(function () {
					$scope.$broadcast('calendar.refreshView');
				}, 1000);
			}
		}

		//Refresh the calendar when any of these variables change.
		$scope.$watchGroup([
			'currentDay',
			'view',
			function () {
				return moment.locale() + $locale.id; //Auto update the calendar when the locale changes
			}
		], refreshCalendar);

	}
}





