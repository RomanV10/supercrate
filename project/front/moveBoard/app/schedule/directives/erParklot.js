'use strict';

const OPEN_REQUEST_PATH = '/moveBoard/#/dashboard/open_request/';
const TARGET_NAME = '_blank';

angular
	.module('app.schedule')
	.controller('erParkLotCtrl', erParkLotCtrl)
	.directive('erParkLot', erParkLot);

/*@ngInject*/
function erParkLotCtrl($scope, $state, $attrs, $q, $element, $sce, common, datacontext, EditRequestServices,
	requestModals, SweetAlert, TrucksServices, $rootScope, $window, RequestServices, newLogService, Raven) {

	var line_height = $attrs.height;
	var parklot_left = $attrs.left;
	var localServices = [1, 2, 3, 4, 6, 8];
	$scope.cnid = 0;

	$scope.cnid = _.get($scope.request, 'nid', $scope.cnid);

	$scope.trucks = datacontext.getFieldData().taxonomy.trucks;
	$scope.trucksUnvailability = [];
	$scope.trucksSheduleUnvailability = [];
	$scope.trucksCount = 0;
	var importedFlags = $rootScope.availableFlags;
	let parklotInitRequestsAfterAction;

	if (angular.isDefined($scope.vm) && angular.isDefined($scope.vm.moveDate)) {
		$scope.curDate = $scope.vm.moveDate;
	}

	$scope.currentDay = moment($scope.curDate).format('MM/DD/YYYY');

	var promise = TrucksServices.getLocalTrucks();
	promise.then(function (data) {
		angular.forEach(data, function (truck, tid) {
			$scope.trucksSheduleUnvailability[tid] = {};
			angular.forEach(truck.unavailable, function (dateTime) {
				var date = moment.unix(dateTime).utc();
				$scope.trucksSheduleUnvailability[tid][date.format('MM/DD/YYYY')] = 1;
			});
		});

		if (parklotInitRequestsAfterAction) {
			onParklotInit(undefined, parklotInitRequestsAfterAction);
		}
	}).finally(() => {
		$rootScope.$broadcast('loading-request-is-stopped');
	});

	var trucks_available = TrucksServices.getSuperTrucks($scope.cnid);
	$scope.trucksCount = common.count(trucks_available);

	angular.forEach(trucks_available, function (truck, tid) {
		$scope.trucksUnvailability[tid] = {};
		if (!truck.available
			&& !findTruckInRequests(tid, $scope.requests)) {

			$scope.trucksUnvailability[tid][$scope.currentDay] = 1;
			$scope.trucksCount--;
		}
	});

	function findTruckInRequests(tid, requests) {
		var result = false;

		if (!_.isEmpty(requests)) {
			for (var i in requests) {
				var request = requests[i];
				var found = !_.isEmpty(request.trucks.raw)
					&& request.trucks.raw.indexOf(tid) != -1;

				if (!found
					&& checkFlatRateDeliveryDates(request, $scope.curDate)) {

					found = !_.isEmpty(request.delivery_trucks.raw)
						&& request.delivery_trucks.raw.indexOf(tid) != -1;
				}

				if (found) {
					result = true;

					return false;
				}
			}
		}

		return result;
	}

	$scope.line_height = line_height;
	$scope.time = common.getTimeArray();
	$scope.parklotControl = $scope.parklotControl ? $scope.parklotControl : {};
	$scope.$sce = $sce;
	$scope.choosen_truck = [];
	$scope.pkl_left = 0;

	if (parklot_left) {
		$scope.pkl_left = parklot_left;
	}

	$scope.fieldData = datacontext.getFieldData();
	var settings = angular.fromJson($scope.fieldData.basicsettings);
	var calcSettings = angular.fromJson($scope.fieldData.calcsettings);
	var withTravelTime = settings.withTravelTime;

	$scope.parklot_height = 30 + $scope.trucksCount * line_height + 5 * $scope.trucksCount;
	$scope.parklot_cells = '<table style="height:' + line_height + 'px;" class="tblines" cellpadding="0" cellspacing="0"><tbody><tr>';

	//Draw ParkLot Lines
	for (var i = 1; i <= $scope.time.length * 2; i++) {
		var cell = '<td class="dhx_matrix_cell first"></td>';

		if (i % 2 === 0) {
			cell = '<td class="dhx_matrix_cell second"></td>';
		}

		$scope.parklot_cells += cell;
	}

	$scope.parklot_cells += '</tr></tbody></table>';

	$scope.goToTrip = (request) => {
		if (request && request.branch) {
			SweetAlert.swal({
				title: `This request from branch ${request.branch.name}. Do you want to open that branch?`,
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: 'No',
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					let url = makeOpenDispatchUrl(request.branch.url);
					$window.open(url, TARGET_NAME);
				} else {
					request.clicked = false;
				}
			});
		} else {
			let tab = 0;
			let id = request.nid;
			$state.go('lddispatch.tripDetails', {
				id,
				tab
			});
		}
	};

	function setActiveTruck() {
		$scope.choosen_truck = [];
		let parklots = [$scope.parklot, $scope.unconparklot, $scope.fpl];
		_.each(parklots, function (parklot) {
			_.each($scope.trucks, function (truck, index) {
				if ($scope.request && !!_.find(parklot[index], {nid: $scope.request.nid})) {
					$scope.choosen_truck[index] = true;
				}
			});
		});
	}

	function unChooseTruck(tid) {
		$scope.choosen_truck[tid] = false;
		$scope.removeTruck(tid);
		$scope.parklot_access[tid] = [];
		$scope.parklot_access[tid]['access'] = [];
		$scope.flatrate_access[tid] = [];
		$scope.flatrate_access[tid]['access'] = [];
		let msg = {
			text: `Truck ${$scope.trucks[tid]} was removed.`
		};
		RequestServices.sendLogs([msg], 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
	}

	function getLastChangedMessageField() {
		let lastChanged;
		let isChanged;
		for (var key in $scope.message) {
			if ($scope.message[key].lastChangeTime && !isChanged) {
				lastChanged = key;
				isChanged = true;
			} else if (lastChanged && $scope.message[key].lastChangeTime > $scope.message[lastChanged].lastChangeTime) {
				lastChanged = key;
			}
		}
		return lastChanged;
	}

	function revertAddressInputs(field, string) {
		$scope.message[field].newValue = $scope.message[field].previousValueString;
		$scope.request[field] = $scope.message[field].previousValue;
		angular.element(`#edit-moving-${string}-state`).val($scope.message[field].previousValue.administrative_area);
		angular.element(`#edit-moving-${string}-city`).val($scope.message[field].previousValue.locality);
		angular.element(`#edit-moving-${string}-zip`).val($scope.message[field].previousValue.postal_code);
		angular.element(`#edit-moving-${string}-zip`).triggerHandler('change');
		if ($scope.message[field].newValue == $scope.message[field].oldValueString) {
			let parentField = {
				field: field
			};
			$scope.removeMessage(parentField);
		}
	}

	function revertZipCode() {
		if (!_.isUndefined($scope.message.field_moving_from) && !_.isUndefined($scope.message.field_moving_to)) {
			if ($scope.message.field_moving_from.lastChangeTime > $scope.message.field_moving_to.lastChangeTime) {
				revertAddressInputs('field_moving_from', 'from');
			} else {
				revertAddressInputs('field_moving_to', 'to');
			}
		} else if (!_.isUndefined($scope.message.field_moving_from)) {
			revertAddressInputs('field_moving_from', 'from');
		} else {
			revertAddressInputs('field_moving_to', 'to');
		}
	}

	function revertInputModelValue(field) {
		let value = angular.copy($scope.message[field].previousValue || $scope.message[field].oldValue);
		switch (field) {
		case 'field_actual_start_time':
			$scope.request.start_time1.value = value;
			$scope.request.start_time1.raw = common.convertStingToIntTime(value);
			angular.element('#edit-start-time').val(value);
			angular.element('#edit-start-time').triggerHandler('change');
			break;
		case 'field_start_time_max':
			$scope.request.start_time2.value = value;
			$scope.request.start_time2.raw = common.convertStingToIntTime(value);
			angular.element('#edit-start-time2').val(value);
			angular.element('#edit-start-time2').triggerHandler('change');
			break;
		case 'field_minimum_move_time':
			$scope.request.minimum_time.value = value;
			$scope.request.minimum_time.raw = common.convertStingToIntTime(value);
			angular.element('#edit-min-time').val(value);
			angular.element('#edit-min-time').triggerHandler('change');
			break;
		case 'field_maximum_move_time':
			$scope.request.maximum_time.value = value;
			$scope.request.maximum_time.raw = common.convertStingToIntTime(value);
			angular.element('#edit-max-time').val(value);
			angular.element('#edit-max-time').triggerHandler('change');
			break;
		case 'field_travel_time':
			$scope.request.travel_time.value = value;
			$scope.request.travel_time.raw = common.convertStingToIntTime(value);
			revertZipCode();
			angular.element('#edit-travel-time').val(value);
			angular.element('#edit-travel-time').triggerHandler('change');
			break;
		case 'field_double_travel_time':
			$scope.request.field_double_travel_time.value = value;
			$scope.request.field_double_travel_time.raw = common.convertStingToIntTime(value);
			angular.element('#edit-double-travel-time').val(value);
			angular.element('#edit-double-travel-time').triggerHandler('change');
			break;
		}
	}

	function revertMessageField(field) {
		if (!$scope.previousTemp) {
			$scope.previousTemp = {};
			$scope.previousTemp[field] = '';
		} else {
			if (!$scope.previousTemp[field]) {
				$scope.previousTemp[field] = '';
			}
		}

		if ($scope.message[field].newValue == $scope.message[field].oldValue && $scope.message[field].newValue == $scope.message[field].previousValue) {
			$scope.message[field].newValue = $scope.message[field].previousValue;
			$scope.message[field].previousValue = $scope.message[field].oldValue;
			revertInputModelValue(field);
			$scope.updateQuote();
			$scope.removeMessage(field);

		} else if (!$scope.previousTemp[field]) {
			$scope.previousTemp[field] = $scope.message[field].previousValue || $scope.message[field].oldValue;
			$scope.message[field].newValue = $scope.message[field].previousValue || $scope.message[field].oldValue;
			$scope.updateQuote();
			revertEditRequest(field);
		} else {
			$scope.message[field].newValue = $scope.previousTemp[field];
			$scope.message[field].previousValue = $scope.previousTemp[field];
			$scope.previousTemp[field] = $scope.message[field].newValue;
			$scope.updateQuote();
			revertEditRequest(field);
		}
	}

	function revertEditRequest(field) {
		if ($scope.editrequest.invoice) {
			revertEditRequestInvoiceField(field);
		} else {
			revertEditRequestField(field);
		}
	}

	function revertEditRequestField(field) {
		$scope.editrequest[field] = $scope.message[field].previousValue;
		revertInputModelValue(field);
	}

	function revertEditRequestInvoiceField(field) {
		$scope.editrequest.invoice[field] = $scope.message[field].previousValue;
		revertInputModelValue(field);
	}

	function revertLastLocalMoveTimingChanges() {
		let lastChangedField = getLastChangedMessageField();
		if (lastChangedField) {
			revertMessageField(lastChangedField);
		}
	}

	function renderRequest(request, tid, items) {
		let deferred = $q.defer();
		const SHOW_LUNCH = settings.withLunchBreak;
		const INCLUDE_TRAVEL_TIME = _.get(settings, 'withTravelTime', true);

		let travelTime = 0;
		if (INCLUDE_TRAVEL_TIME) {
			travelTime = _.isUndefined(request.travel_time.raw)
				? request.travel_time
				: request.travel_time.raw;
		}
		let work_time = common.convertStingToIntTime(request.maximum_time.value) * 2 + travelTime * 2;

		let startTime1 = common.convertTime(request.start_time1.value);
		let startTime2 = request.start_time2.value ? common.convertTime(request.start_time2.value) : common.convertTime(
			request.start_time1.value);
		let possibleTime = startTime2 - startTime1 + work_time;
		let endTime = startTime1 + possibleTime;
		let array_nid = [];

		function setLunch(request) {
			request.wt = 25 * work_time;
			request.st = 25 * startTime1;
			request.lanch = 0;

			if (SHOW_LUNCH) request.lanch = startTime1 <= 24 && startTime2 <= 28 ? 1 : 0;

			pushRequestItem(request);
		}

		function pushRequestItem(request) {
			items.push(request);
		}

		for (var t = startTime1; t <= startTime1 + endTime; t++) {
			let accessTruckByTime = $scope.parklot_access[tid]['access'][t];

			_.each(accessTruckByTime, function (num) {
				var id = _.find(array_nid, function (id) {
					return id == num;
				});

				if (!id) {
					array_nid.push(num);
				}
			});
		}

		if (!_.isEmpty(array_nid) && !_.isEmpty(items)) {
			request.lanch = 0;

			_.each(array_nid, function (num) {
				var item = _.find(items, {nid: num});
				if (item) {
					setLunch(request);
				}
			});
		} else {
			setLunch(request);
		}

		let result = [];

		items.forEach((item) => {
			if (!_.find(result, {nid: item.nid})) {
				result.push(item);
			}
		});

		deferred.resolve(result);

		return deferred.promise;
	}

	// IF PARK LOT IN DIALOG EDIT REQUEST WINDOW
	if ($attrs.dialog) {
		//Draggable time line, move left starting from 7 am
		$scope.pkl_left = -245;
		var pkl_data = $element.find('.pkl_data');

		pkl_data.css('left: -245px');
		pkl_data.draggable({axis: 'x'},
			{containment: [120, 200, 570, 200]},
			{handle: '.dhx_cal_header'},
			{
				drag: function () {

				}
			},
			{
				stop: function () {

				}
			}
		);

		if (angular.isDefined($scope.requests)) {
			var index = _.findIndex($scope.requests, findCurrentBranchRequest);

			if (index == -1) {
				$scope.requests.push($scope.request);
			} else {
				if (!_.isArray($scope.requests)) {
					$scope.requests[index] = $scope.request;
				}
			}
		}

		initParkLotArray($scope.requests);
	}

	function findCurrentBranchRequest(request) {
		return request.nid == $scope.request.nid && !request.branch;
	}

	$scope.$on('parklot.init', onParklotInit);

	function onParklotInit (event, requests) {
		parklotInitRequestsAfterAction = requests;

		if (angular.isDefined($scope.vm)
			&& angular.isDefined($scope.vm.moveDate)) {

			$scope.curDate = $scope.vm.moveDate;
		}

		$scope.currentDay = moment($scope.curDate).format('MM/DD/YYYY');

		if (angular.isUndefined(requests)) {
			if (angular.isDefined($scope.request)) {
				var index = _.findIndex($scope.requests, findCurrentBranchRequest);
				var deliveryDate = moment($scope.curDate).format('YYYY-MM-DD');
				var isValidPickupDate = deliveryDate == $scope.request.date.value;
				var isNotFoundCurrentRequest = index == -1;
				var isFlatRateDeliveryDay = checkFlatRateDeliveryDates($scope.request, $scope.curDate);
				$scope.request.flatRate = isFlatRateDeliveryDay;
				var isValidCurrentRequest = isNotFoundCurrentRequest && (isValidPickupDate || isFlatRateDeliveryDay);

				if (isValidCurrentRequest) {
					$scope.requests[index] = $scope.request;
				}
			}

			requests = $scope.requests;
		}

		var trucksCount = common.count($scope.trucks);

		if (!_.isEmpty($scope.trucksSheduleUnvailability)) {
			$scope.trucksUnvailability = angular.copy($scope.trucksSheduleUnvailability);
			angular.forEach($scope.trucks, function (name, tid) {
				if (angular.isDefined($scope.trucksUnvailability[tid])
					&& $scope.trucksUnvailability[tid][$scope.currentDay] == 1) {

					if (findTruckInRequests(tid, requests)) {
						$scope.trucksUnvailability[tid][$scope.currentDay] = 0;
					} else {
						trucksCount--;
					}
				}
			});
		}
        $scope.parklot_height = 30 + trucksCount * $scope.line_height + 5 * trucksCount;
        initParkLotArray(requests);
	}

	function checkFlatRateDeliveryDates(request, currentDate) {
		var result = request.service_type.raw == 5
			&& !_.isUndefined(request.delivery_date_from)
			&& request.delivery_date_from.raw != false;

		if (result) {
			var momentCurrentDate = moment(currentDate);
			var dateFrom = moment(request.delivery_date_from.value, 'DD-MM-YYYY');
			var dateTo = moment(request.delivery_date_to.value, 'DD-MM-YYYY');

			if (request.delivery_date_to.raw == false) {
				dateTo = moment($scope.request.delivery_date_from.value, 'DD-MM-YYYY');
			}

			result = momentCurrentDate.isBetween(dateFrom, dateTo)
				|| momentCurrentDate.isSame(dateFrom, 'day')
				|| momentCurrentDate.isSame(dateTo, 'day');
		}

		return result;
	}

	function isFlagExist(obj, flag) {
		return angular.isDefined(obj) ? !!obj && obj.value.hasOwnProperty(importedFlags.company_flags[flag]) : '';
	}

	function initParkLotArray(requests) {
		var trucks = $scope.trucks;
		$scope.dayRequests = requests;
		$scope.parklot = [];
		$scope.unconparklot = [];
		$scope.fpl = [];
		$scope.popupControl = {};
		$scope.updateStartTime = [];
		$scope.parklot_access = [];
		$scope.flatrate_access = [];

		angular.forEach(trucks, function (truck, tid) {
			$scope.parklot[tid] = [];
			$scope.unconparklot[tid] = [];
			$scope.fpl[tid] = [];
			$scope.flatrate_access[tid] = {};
			$scope.flatrate_access[tid].access = [];
			$scope.parklot_access[tid] = {};
			$scope.parklot_access[tid]['access'] = [];
		});

		var confirmed = 0;
		var unconfirmed = 0;

		// SORT ARRAY ON CONFIRM AND NOT CONFIRMED
		angular.forEach(requests, function (request) {
			if (angular.isDefined(request.trucks)) {
				angular.forEach(request.trucks.raw, function (tid) {
					var curdate = moment($scope.curDate).format('MM/DD/YYYY');

					request.flatRate = request.service_type.raw == 5 && request.date.value != curdate;
					var nid = angular.isDefined($scope.request) ? $scope.request.nid : 0;

					var isDoubleDriveTime = calcSettings.doubleDriveTime
						&& request.field_double_travel_time
						&& !_.isNull(request.field_double_travel_time.raw)
						&& localServices.indexOf(parseInt(request.service_type.raw)) >= 0;

					var travelTime = request.travel_time.raw;

					if (isDoubleDriveTime) {
						travelTime += request.field_double_travel_time.raw / 2;
					}

					let fieldCompanyFlags = _.get(request, 'field_company_flags.value', {});

					var current = {
						nid: request.nid,
						branch: request.branch,
						status: {
							raw: request.status.raw
						},
						service_type: {
							raw: request.service_type.raw
						},
						crew: {
							value: request.crew.value
						},
						move_size: {
							value: request.move_size.value
						},
						maximum_time: {
							value: request.maximum_time.value,
							raw: request.maximum_time.raw
						},
						start_time1: {
							value: request.start_time1.value
						},
						start_time2: {
							value: request.start_time2.value
						},
						driving_time: request.driving_time ? request.driving_time : 0,
						travel_time: travelTime,
						zip_from: request.zip_from.value,
						zip_to: request.zip_to.value,
						reservation: request.field_reservation_received.value,
						from: request.from,
						to: request.to,
						source: request,
						date: request.date,
						ddate: request.ddate,
						name: request.name,
						assignedCrew: isFlagExist(request.field_company_flags, 'teamAssigned'),
						jobDone: fieldCompanyFlags.hasOwnProperty(importedFlags.company_flags['Completed']),
						isShowTrack: request.status.raw == 3 || request.nid === nid,
						isCurrent: request.nid === nid,
						field_double_travel_time: request.field_double_travel_time,
						flatRate: request.flatRate,
						ld_trip: request.ld_trip,
						trip_info: request.trip_info
					};


					if ((current.status.raw == 3
						|| current.status.raw == 1
						|| nid == current.nid)
						&& !request.flatRate && !request.ld_trip) {

						if (angular.isDefined($scope.parklot[tid])) {
							$scope.parklot[tid].push(current);

							if (current.status.raw == 3) {
								confirmed++;
							}
						}
					}

					if (current.status.raw == 2
						&& nid != current.nid
						&& !request.flatRate && !request.ld_trip && $scope.unconparklot[tid]) {

						$scope.unconparklot[tid].push(current);
						unconfirmed++;
					}

					if ((request.flatRate || request.ld_trip) && !_.get(request, 'field_flat_rate_local_move.value')) {
						if (!request.delivery_trucks.raw.length && $scope.fpl[tid]) {
							//if delivery truck not exist
							if (!_.isString(request.ddate.value)
								|| _.isEmpty(request.ddate.value)
								&& $scope.request
								&& request.nid == $scope.request.nid) {

								toastr.warning('Please, set delivery date.', 'Warning!');
							} else {
								var dd = moment(request.delivery_date_from.value, 'DD-MM-YYYY').format('MM/DD/YYYY');

								if (curdate == dd
									&& _.isString(request.delivery_start_time.value)
									&& !_.isEmpty(request.delivery_start_time.value)) {

									current.dstime = 25 * common.convertTime(request.delivery_start_time.value);
								} else {
									current.dstime = 350;
								}

								$scope.fpl[tid].push(current);
							}
						} else if (request.delivery_trucks.raw.length) {
							if (!_.isUndefined(request.delivery_date_from)
								&& request.delivery_date_from.raw != false && $scope.fpl[tid]) {

								var momentCurrentDate = moment($scope.curDate);
								var dateFrom = moment(request.delivery_date_from.value, 'DD-MM-YYYY');
								var dateTo = moment(request.delivery_date_to.value, 'DD-MM-YYYY');

								if (request.delivery_date_to.raw == false) {
									dateTo = moment(request.delivery_date_from.value, 'DD-MM-YYYY');
								}

								if (momentCurrentDate.isBetween(dateFrom, dateTo)
									|| momentCurrentDate.isSame(dateFrom, 'day')
									|| momentCurrentDate.isSame(dateTo, 'day')) {

									if (momentCurrentDate.isSame(dateFrom, 'day')
										&& _.isString(request.delivery_start_time.value)
										&& !_.isEmpty(request.delivery_start_time.value)) {

										current.dstime = 25 * common.convertTime(request.delivery_start_time.value);
									} else {
										current.dstime = 350;
									}
									if (current.ld_trip) {
										$scope.fpl[tid].push(current);
									} else {
										angular.forEach(request.delivery_trucks.raw, function (tid) {
											$scope.fpl[tid].push(current);
										});
									}
								}
							}
						}
					}

					$scope.confirmedStats = confirmed;
					$scope.unconfirmedStats = unconfirmed;
				});
			}
		});

		angular.forEach(trucks, function (truck, tid) {
			$scope.parklot[tid].sort(function (a, b) {
				return common.convertTime(a['start_time1'].value) - common.convertTime(b['start_time1'].value);
			});
		});

		$scope.parklot = processParklot($scope.parklot);
		$scope.unconparklot = processParklot($scope.unconparklot);
		$scope.overlayRequests = overlayReq($scope.unconparklot);

		_.each($scope.trucks, function (name, tid) {
			_.each($scope.overlayRequests[tid], function (request, id) {
				_.each($scope.unconparklot[tid], function (request2, id2) {

					if (request.nid == request2.nid) {
						$scope.unconparklot[tid][id2].overlay = true;
					}
				});
			});
		});

		if ($attrs.dialog) {
			setActiveTruck();
		}
	}

	function overlayReq(parklot) {
		var overlayReq = [];

		_.each($scope.trucks, function (name, tid) {
			overlayReq[tid] = [];

			_.each(parklot[tid], function (request, id) {
				_.each(parklot[tid], function (request2, id2) {

					if (request.nid != request2.nid) {
						var sT1 = common.convertTime(request.start_time1.value);
						var sT2 = common.convertTime(request2.start_time1.value);
						var abs = Math.abs(sT1 - sT2);

						if (sT1 == sT2 || abs < 3) {
							var skip = false;

							_.each(overlayReq[tid], function (overReq, id2) {
								if (request.nid == overReq.nid) {
									skip = true;
								}
							});

							if (!skip) {
								overlayReq[tid].push(request);
							}
						}
					}
				});
			});
		});

		return overlayReq;
	}

	function processParklot(parklot) {
		var trucks = $scope.trucks;

		_.each(trucks, function (truck_name, tid) {
			var requests = [];

			if (parklot[tid][0]) {
				activateParklot(0, tid, parklot, requests);
			}
		});

		return parklot;
	}

	function activateParklot(num, tid, parklot, requests) {
		if (_.isUndefined(parklot)) {
			return;
		}

		var request = parklot[tid][num];

		if (request) {
			_.each(requests, function (request_data) {
				var startTime = common.convertTime(request_data.start_time1.value);

				calculateWorkTime(withTravelTime, request_data);

				request_data.startTime1 = common.convertTime(request_data.start_time1.value);
				request_data.startTime2 = common.convertTime(request_data.start_time2.value);
				request_data.possibleTime = request_data.startTime2 - request_data.startTime1 + request_data.work_time || 0;
				request_data.endTime = request_data.startTime1 + request_data.possibleTime;

				setParklotAccess(startTime, request_data, tid);
			});
		}

		renderRequest(request, tid, requests)
			.then(function (items) {
				requests = items;

				if (parklot[tid][num + 1]) {
					activateParklot(num + 1, tid, parklot, requests);
				} else {
					parklot[tid] = requests;
				}
			});
	}

	function calculateWorkTime(withTravelTime, request_data) {
		if (withTravelTime) {
			request_data.work_time = common.convertStingToIntTime(request_data.maximum_time.value) * 2 + request_data.travel_time * 2;
		} else {
			request_data.work_time = common.convertStingToIntTime(request_data.maximum_time.value) * 2;
		}
	}

	function setParklotAccess(startTime, request_data, tid) {
		for (var t = startTime; t <= request_data.endTime + startTime; t++) {
			if (!$scope.parklot_access[tid]['access'][t]) {
				$scope.parklot_access[tid]['access'][t] = [];
			}

			$scope.parklot_access[tid]['access'][t].push(request_data.nid);
		}
	}

	$scope.OpenModel = function (request) {
		if (!request) return;

		if (request.branch) {
			let href = makeOpenRequestPath(request);

			$window.open(href, TARGET_NAME);
		} else {
			EditRequestServices.requestEditModal(request.nid);
		}
	};

	function makeOpenRequestPath(request) {
		return request.branch.url + OPEN_REQUEST_PATH + request.nid;
	}

	$scope.showClicked = function (current) {
		var trucks = $scope.trucks;

		angular.forEach(trucks, function (truck, tid) {
			angular.forEach($scope.parklot[tid], function (request) {
				request.clicked = request.nid == current.nid && request.branch == current.branch;
			});
			angular.forEach($scope.fpl[tid], function (request) {
				request.clicked = request.nid == current.nid && request.branch == current.branch;
			});
		});
	};

	$scope.$on('showClicked', function (event, request) {
		$scope.showClicked(request);
	});

	$scope.showPopup = function (nid) {
		if ($attrs.dialog) {
			var request = _.find($scope.requests, {nid: nid});
			$scope.parklotControl.initPopup(request);
		}
	};

	var linkToPopupMaster = {};

	$scope.showOnlyThisPopup = function (req) {
		linkToPopupMaster.showpopup = false;
		req.showpopup = true;
		linkToPopupMaster = req;
	};

	function removeRequest(tid, request_id) {
		//отфильтровывает задания для машины удаляя выделенное
		$scope.parklot[tid] = _.filter($scope.parklot[tid], function (item) {
			return item.nid != request_id;
		});

		$scope.fpl[tid] = _.filter($scope.fpl[tid], function (item) {
			return item.nid != request_id;
		});
	}

	function isFlatRatePickupDay() {
		var today = moment($scope.curDate).format('MM/DD/YYYY');
		let pickup = moment($scope.request.date.value).format('MM/DD/YYYY');

		return !!($scope.request.service_type.raw == 5 && pickup != today);
	}

	$scope.updateTruck = function () {
		reRenderRequests();
	};

	function reRenderRequests() {
		_.each($scope.parklot, function (item, index) {
			if (!_.isUndefined(item)
				&& !!item.length && $scope.request) {

				var request = _.find(item, {nid: $scope.request.nid});

				if (request) {
					request.status.raw = $scope.request.status.raw;
					request.crew.value = $scope.request.crew.value;
					request.driving_time = $scope.driving_time ? $scope.driving_time : 0;
					request.move_size.value = $scope.request.move_size.value;
					request.maximum_time.value = $scope.request.maximum_time.value;
					request.maximum_time.raw = $scope.request.maximum_time.raw;
					request.start_time1.value = $scope.request.start_time1.value;
					request.start_time2.value = $scope.request.start_time2.value;
					request.zip_from = $scope.request.zip_from.value;
					request.zip_to = $scope.request.zip_to.value;
					request.from = $scope.request.from;
					request.to = $scope.request.to;

					let isDoubleDriveTime = calcSettings.doubleDriveTime
						&& !_.isNull($scope.request.field_double_travel_time.raw)
						&& localServices.indexOf(parseInt($scope.request.service_type.raw)) >= 0;

					var travelTime = $scope.request.travel_time.raw;

					if (isDoubleDriveTime) {
						travelTime += $scope.request.field_double_travel_time.raw / 2;
					}

					request.travel_time = travelTime;
					request.field_double_travel_time = $scope.request.field_double_travel_time;


					$scope.parklot_access[index]['access'] = [];

					var filterRequests = _.sortBy(_.filter(item, function (request_data) {
						var withTravelTime = settings.withTravelTime;

						calculateWorkTime(withTravelTime, request_data);

						request_data.startTime1 = common.convertTime(request_data.start_time1.value);
						request_data.startTime2 = common.convertTime(request_data.start_time2.value);
						request_data.possibleTime = request_data.startTime2 - request_data.startTime1 + request_data.work_time || 0;
						request_data.endTime = request_data.startTime1 + request_data.possibleTime;

						return request_data.nid != $scope.request.nid;
					}), function (num) {
						return num.startTime1;
					});

					_.each(filterRequests, function (request_data) {
						var startTime = common.convertTime(request_data.start_time1.value);
						for (var t = startTime; t <= request_data.endTime; t++) {
							if (!$scope.parklot_access[index]['access'][t]) {
								$scope.parklot_access[index]['access'][t] = [];
							}
							$scope.parklot_access[index]['access'][t].push(request_data.nid);
						}
					});

					renderRequest(request, index, filterRequests).then(function (items) {
						item = items;
					});
				}
			}
		});
	}

	function validateCurentPush(truckId) {
		var valid = true;
		var startTime1 = common.convertTime($scope.request.start_time1.value);
		var startTime2 = common.convertTime($scope.request.start_time2.value);

		if (!startTime2) {
			for (var t = startTime1; t <= startTime1 + $scope.request['maximum_time'].raw; t++) {
				if ($scope.parklot_access[truckId]['access'][t] == 1 || $scope.parklot_access[truckId]['access'][t] == 2) {
					valid = false;
					requestModals.alertModal('Can\'t place request because this time already taken.');
					return false;
				}
			}
		}

		return valid;
	}

	function checkOverbooking(request, tid, isChangeTime = false) {

		let isDoubleDriveTime = calcSettings.doubleDriveTime
			&& request.field_double_travel_time
			&& !_.isNull(request.field_double_travel_time.raw)
			&& localServices.indexOf(parseInt(request.service_type.raw)) >= 0;

		let travelTime = !_.isUndefined(request.travel_time.raw) ? request.travel_time.raw : request.travel_time;

		$scope.busy = true;
		var deferred = $q.defer();
		let data = {
			fields: {
				nid: request.nid,
				field_maximum_move_time: request.maximum_time.raw,
				field_travel_time: !isDoubleDriveTime
					? travelTime
					: travelTime + $scope.request.field_double_travel_time.raw / 2,
				field_actual_start_time: request.start_time1.value,
				field_service_type: request.status.raw,
				field_list_truck: [tid],
				field_date: $scope.request.date.value,
				branch: $scope.request.branch,
			}
		};

		TrucksServices.checkOverbooking(data).then(function (resolve) {
			$scope.busy = false;
			if (resolve[0]) {
				newLogService.sendOverbookingLogs(data.fields, resolve[0], $scope.request.nid);

				toastr.error('Please check start time or end time.', `Can't place request #${request.nid}`);

				if (isChangeTime) {
					revertLastLocalMoveTimingChanges();
				}

				deferred.reject(tid);
			} else {
				newLogService.sendOverbookingLogs(data.fields, resolve[0], $scope.request.nid);

				if (!_.isUndefined($scope.previousTemp)) {
					for (let field in $scope.previousTemp) {
						if ($scope.message[field]) {
							$scope.previousTemp[field] = $scope.message[field].previousValue;
						}
					}
				}
				reRenderRequests();
				deferred.resolve();
			}
		});

		return deferred.promise;
	}


	$scope.chooseTruck = function (tid, changeMoveDate) {
		if (!$attrs.dialog) {
			return false;
		}

		var pickupDate = moment($scope.request.date.value).format('MM/DD/YYYY');
		var today = moment($scope.currentDay).format('MM/DD/YYYY');
		var isValidFlatRateRequestDate = checkFlatRateMoveDate(today);

		if (pickupDate != today && isValidFlatRateRequestDate && !changeMoveDate) {
			let title = getChangeMoveDateTitle();

			SweetAlert
				.swal({
					title,
					type: 'warning',
					showCancelButton: true,
					cancelButtonText: 'No',
					confirmButtonText: 'Yes'
				},
				function (isConfirm) {
					if (isConfirm) {
						var formated_date = $.datepicker.formatDate('yy-mm-dd', new Date(today), {});
						$scope.editrequest[$scope.request.date.field] = {
							date: formated_date,
							time: '15:10:00'
						};
						$scope.message[$scope.request.date.field] = {
							label: $scope.request.date.label,
							oldValue: !_.isUndefined($scope.request.date.old) ? moment(new Date($scope.request.date.old)).format('MMMM D, YYYY') : '',
							newValue: moment(formated_date, 'YYYY-MM-DD').format('MMMM D, YYYY')
						};
						_.each($scope.request.trucks.raw, function (truck, index) {
							removeRequest(truck, $scope.request.nid);
							unChooseTruck(truck);
						});
						$scope.request.date.value = moment(today).format('YYYY-MM-DD');
						var formatDate = $scope.request.date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
						$scope.moveDateInput = moment($scope.request.date.value, formatDate).format('MMMM DD, YYYY');

						if ($scope.Invoice) {
							$scope.onChangeMoveDateLocalMoveClosing(moment($scope.request.date.value, formatDate));
						}

						$scope.request.trucks.raw.length = 0;
						$scope.calcResults = undefined;
						$scope.chooseTruck(tid);
					}
				});

			return 0;
		}

		//Пытаемся найти в машинах задание с главным айдишником.
		if ($scope.request && !!_.find($scope.parklot[tid], {nid: $scope.request.nid})) {
			//если находим удаляем его
			removeRequest(tid, $scope.request.nid);
			//убираем выделения трака
			unChooseTruck(tid);
		} else {
			var trucks = $scope.getTrucksCount();
			var calcTrucks = 1;

			if (angular.isDefined($scope.calcResults)) {
				calcTrucks = $scope.calcResults.trucks;
			}

			if (trucks >= calcTrucks && $scope.request.field_usecalculator.value && !isFlatRatePickupDay()) {
				SweetAlert.swal('You can not choose more trucks');
				return 0;
			}

			var isDoubleDriveTime = calcSettings.doubleDriveTime
				&& !_.isNull($scope.request.field_double_travel_time.raw)
				&& localServices.indexOf(parseInt($scope.request.service_type.raw)) >= 0;

			var travelTime = $scope.request.travel_time.raw;

			if (isDoubleDriveTime) {
				travelTime += $scope.request.field_double_travel_time.raw / 2;
			}

			//Это ФлэтРейт?
			if (!isFlatRatePickupDay()) {
				if ($scope.request && validateCurentPush(tid)) {
					//Пушим в Парклот?
					let current = {
						nid: $scope.request.nid,
						status: {
							raw: $scope.request.status.raw
						},
						service_type: {
							raw: $scope.request.service_type.raw
						},
						crew: {
							value: $scope.request.crew.value
						},
						move_size: {
							value: $scope.request.move_size.value
						},
						maximum_time: {
							value: $scope.request.maximum_time.value,
							raw: $scope.request.maximum_time.raw
						},
						start_time1: {
							value: $scope.request.start_time1.value
						},
						start_time2: {
							value: $scope.request.start_time2.value
						},
						travel_time: travelTime,
						zip_from: $scope.request.zip_from.value,
						zip_to: $scope.request.zip_to.value,
						from: $scope.request.from,
						to: $scope.request.to,
						name: $scope.request.name,
						field_double_travel_time: $scope.request.field_double_travel_time,
						isCurrent: true,
						flatRate: isFlatRatePickupDay(),
						source: $scope.request
					};

					let filterRequests = parklotRequestsPrepareToOverbooking(tid, $scope.parklot, $scope.unconparklot);

					_.each(filterRequests, function (request_data) {
						var startTime = common.convertTime(request_data.start_time1.value);

						setParklotAccess(startTime, request_data, tid);
					});

					checkOverbooking(current, tid)
						.then(() => {
							if ($scope.parklot[tid]) {
								$scope.parklot[tid].push(current);
								let logMessages = [
									{
										text: `Truck ${$scope.trucks[tid]} was chosen.`
									}
								];
								RequestServices.sendLogs(logMessages, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
							}

							setActiveTruck();
							reRenderRequests();
						}, tid => {
							unChooseTruck(tid);
						})
						.catch(error => {
							Raven.captureException(`erParklot.js:1137 (checkOverbooking): ${error}; request #${$scope.request.nid}`);
						});
				}
			} else {
				if ($scope.request && !!_.find($scope.fpl[tid], {nid: $scope.request.nid})) {
					//если находим удаляем его
					removeRequest(tid, $scope.request.nid);
					//убираем выделения трака
					unChooseTruck(tid);

					return;
				} else if (!_.get($scope.request, 'field_flat_rate_local_move.value')) {
					if (!_.isString($scope.request.delivery_start_time.value)
						|| _.isEmpty($scope.request.delivery_start_time.value)
						|| !_.isString($scope.request.delivery_date_from.value)
						|| _.isEmpty($scope.request.delivery_date_from.value)
						|| !_.isString($scope.request.delivery_date_to.value)
						|| _.isEmpty($scope.request.delivery_date_to.value)
						|| !_.isString($scope.request.ddate.value)
						|| _.isEmpty($scope.request.ddate.value)) {

						if (!_.isString($scope.request.delivery_start_time.value)
							|| _.isEmpty($scope.request.delivery_start_time.value)) {

							toastr.warning('Please, set delivery start time.', 'Warning!');
						}

						if (!_.isString($scope.request.delivery_date_from.value)
							|| _.isEmpty($scope.request.delivery_date_from.value)
							|| !_.isString($scope.request.delivery_date_to.value)
							|| _.isEmpty($scope.request.delivery_date_to.value)) {

							toastr.warning('Please, set delivery dates.', 'Warning!');
						}

						if (!_.isString($scope.request.ddate.value)
							|| _.isEmpty($scope.request.ddate.value)) {

							toastr.warning('Please, set delivery date.', 'Warning!');
						}

						return;
					}


					var current = {
						nid: $scope.request.nid,
						status: {
							raw: $scope.request.status.raw
						},
						service_type: {
							raw: $scope.request.service_type.raw
						},
						crew: {
							value: $scope.request.crew.value
						},
						move_size: {
							value: $scope.request.move_size.value
						},
						maximum_time: {
							value: $scope.request.maximum_time.value,
							raw: $scope.request.maximum_time.raw
						},
						start_time1: {
							value: $scope.request.start_time1.value
						},
						start_time2: {
							value: $scope.request.start_time2.value
						},
						driving_time: $scope.request.driving_time ? $scope.request.driving_time : 0,
						travel_time: travelTime,
						zip_from: $scope.request.zip_from.value,
						zip_to: $scope.request.zip_to.value,
						reservation: $scope.request.field_reservation_received.value,
						from: $scope.request.from,
						to: $scope.request.to,
						source: $scope.request,
						date: $scope.request.date,
						name: $scope.request.name,
						assignedCrew: isFlagExist($scope.request.field_company_flags, 'teamAssigned'),
						jobDone: $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags['Completed']),
						field_double_travel_time: $scope.request.field_double_travel_time,
						isCurrent: true,
						isShowTrack: _.get($scope.request, 'delivery_date_from.value'),
						flatRate: isFlatRatePickupDay()
					};
					var momentCurrentDate = moment($scope.curDate);
					var dateFrom = moment($scope.request.delivery_date_from.value, 'DD-MM-YYYY');
					var dateTo = moment($scope.request.delivery_date_to.value, 'DD-MM-YYYY');

					if ($scope.request.delivery_date_to.raw == false) {
						dateTo = moment($scope.request.delivery_date_from.value, 'DD-MM-YYYY');
					}

					if (momentCurrentDate.isBetween(dateFrom, dateTo)
						|| momentCurrentDate.isSame(dateFrom, 'day')
						|| momentCurrentDate.isSame(dateTo, 'day')) {

						if (momentCurrentDate.isSame(dateFrom, 'day')
							&& _.isString($scope.request.delivery_start_time.value)
							&& !_.isEmpty($scope.request.delivery_start_time.value)) {
							current.dstime = 25 * common.convertTime($scope.request.delivery_start_time.value);
						} else {
							current.dstime = 350;
						}


						$scope.fpl[tid].push(current);
					}

					setActiveTruck();
				}
			}

			//Синхронизация , добавления трака реквест
			$scope.addTruck(tid, isFlatRatePickupDay());
		}

		function checkFlatRateMoveDate() {
			var result = $scope.request.service_type.raw != 5;

			if (!result) {
				result = _.isUndefined($scope.request.delivery_date_from)
					|| $scope.request.delivery_date_from.raw == false;

				if (!result) {
					var momentCurrentDate = moment($scope.curDate);
					var dateFrom = moment($scope.request.delivery_date_from.value, 'DD-MM-YYYY');
					var dateTo = moment($scope.request.delivery_date_to.value, 'DD-MM-YYYY');

					if ($scope.request.delivery_date_to.raw == false) {
						dateTo = moment($scope.request.delivery_date_from.value, 'DD-MM-YYYY');
					}

					result = !momentCurrentDate.isBetween(dateFrom, dateTo)
						&& !momentCurrentDate.isSame(dateFrom, 'day')
						&& !momentCurrentDate.isSame(dateTo, 'day');
				}
			}

			return result;
		}

		function getChangeMoveDateTitle() {
			var dateFieldText = ' move date?';

			if ($scope.request.service_type.raw == 5
				|| $scope.request.service_type.raw == 7) {

				dateFieldText = ' pickup date?';
			}

			var result = 'Do you wanna choose ' + moment($scope.currentDay).format('MMMM D, YYYY') + dateFieldText;

			return result;
		}
	};

	function parklotRequestsPrepareToOverbooking(tid, ...args) {
		// If you don't know how works '...args' read ES6
		let result = [];

		angular.forEach(args, function (item) {
			let filterResult = _.filter(item[tid], filteringRequestsFunction);
			let sortResult = _.sortBy(filterResult, sortRequestsFunction);
			result = result.concat(sortResult);
		});

		return result;
	}

	function filteringRequestsFunction(request) {
		calculateWorkTime(withTravelTime, request);

		request.startTime1 = common.convertTime(request.start_time1.value);
		request.startTime2 = common.convertTime(request.start_time2.value);
		request.possibleTime = request.startTime2 - request.startTime1 + request.work_time || 0;
		request.endTime = request.startTime1 + request.possibleTime;

		return !_.isEqual(request.nid, $scope.request.nid);
	}

	function sortRequestsFunction(num) {
		return num.startTime1;
	}

	$scope.$on('check.overbooking', onCheckOverbooking);
	$scope.$on('re-render-parklot', reRenderRequests);

	function onCheckOverbooking() {
		let salesTrucks = _.get($scope, 'request.trucks.raw', []);
		let trucks = _.get($scope, 'invoice.trucks.raw', salesTrucks);
		let isChangeTime = true;

		angular.forEach(trucks, (tid) => {
			checkOverbooking($scope.request, tid, isChangeTime);
		});
	}
}

/*@ngInject*/
function erParkLot() {
	return {
		restrict: 'A',
		template: require('../templates/parklotTemplate.html'),
		controller: 'erParkLotCtrl'
	};
}
