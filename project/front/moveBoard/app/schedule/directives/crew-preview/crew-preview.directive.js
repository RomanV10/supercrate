'use strict';

import './crew-preview.styl';

angular
	.module('app.schedule')
	.controller('crewPreviewCtrl', crewPreviewCtrl)
	.directive('crewPreview', crewPreview);

/*@ngInject*/
function crewPreview() {
	return {
		restrict: 'A',
		template: require('./crew-preview.html'),
		scope: {
			jobId: '@',
			allRequestsInfo: '=',
			allUsersInfo: '=',
			currentDay: '=',
		},
		controller: 'crewPreviewCtrl'
	};
}

/*@ngInject*/
function crewPreviewCtrl($scope) {
	updateRequestInfo();
	$scope.$watch('requestInfo.request_data.value.crews', getCrewInfo, true);
	$scope.$watchCollection('allRequestsInfo', updateRequestInfo, true);

	function updateRequestInfo() {
		$scope.requestInfo = _.find($scope.allRequestsInfo, req => req && req.nid == $scope.jobId);
	}

	function getCrewInfo() {
		let crewInfo = _.get($scope.requestInfo, 'request_data.value.crews', {});
		if ($scope.requestInfo.service_type.raw == 5) {
			let requestMoveDate = moment($scope.requestInfo.date.value, 'MM/DD/YYYY');
			let requestDeliveryDateFrom = moment($scope.requestInfo.delivery_date_from.value, 'D-M-YYYY');
			let requestDeliveryDateTo = moment($scope.requestInfo.delivery_date_to.value, 'D-M-YYYY');
			let currentDateMoment = moment($scope.currentDay, 'MMM D, YYYY');
			if (requestMoveDate.isSame(currentDateMoment, 'day')) {
				crewInfo = crewInfo.pickedUpCrew || {};
			} else if (!requestDeliveryDateFrom.isAfter(currentDateMoment, 'day')
				&& !requestDeliveryDateTo.isBefore(currentDateMoment, 'day')) {
				crewInfo = crewInfo.deliveryCrew || {};
			}
		}
		if ($scope.requestInfo.ld_trip) {
			crewInfo.foreman = _.get($scope.requestInfo, 'trip_info.foreman.foreman_id');
			crewInfo.helpers = _.get($scope.requestInfo, 'trip_info.helper', []).map(helper => helper.helper_id);
		}
		$scope.foremanInfo = _.find($scope.allUsersInfo.foreman, foreman => foreman.uid == crewInfo.foreman);

		$scope.helpersInfo = [];
		let helpers = _.get(crewInfo, 'baseCrew.helpers') || crewInfo.helpers;
		if(helpers) {
			_.forEach(helpers, helperId => {
				let helperInfo = _.find($scope.allUsersInfo.helper, helper => helper.uid == helperId);
				if (!helperInfo) helperInfo = _.find($scope.allUsersInfo.foreman, foreman => foreman.uid == helperId);
				if (!helperInfo) helperInfo = _.find($scope.allUsersInfo.driver, driver => driver.uid == helperId);
				if (helperInfo) $scope.helpersInfo.push(helperInfo);
			});
		}
		$scope.photoUrl = _.get($scope, 'foremanInfo.photoUrl');
		$scope.photoStyle = {
			backgroundImage: `url(${$scope.photoUrl})`
		};
	}
}

