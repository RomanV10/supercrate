'use strict';

angular
	.module('app.schedule')
	.controller('erScheduleSlideBoxCtrl', function ($scope, $attrs, $element, common) {
		
		$scope.isCollapsed = true;
		$scope.$on('parklot.dayClicked', function (event, index) {
			if ($scope.index != index.index) return false;
			$scope.dayRequests = $scope.requests;
			$scope.curDate = index.curDate;
			$scope.$broadcast('parklot.init');
		});
		$scope.$watch('isOpen', function (isOpen) {
			//events must be populated first to set the element height before animation will work
			$scope.isCollapsed = !isOpen;
			$scope.dayRequests = $scope.requests;
		});
		$scope.$on('request.updated', function (event, request) {
			_.forEach($scope.requests, function (value, index) {
				if (angular.isDefined(value) && value.nid == request.nid) {
					$scope.requests[index].status = _.clone(request.status);
				}
			});
		});
		
	})
	.directive('erScheduleSlideBox', function ($injector) {
		
		return {
			restrict: 'A',
			template: require('./../templates/slideBoxTemplate.html'),
			controller: 'erScheduleSlideBoxCtrl',
			scope: {
				isOpen: '=',
				index: '=',
				requests: '=requests',
				
			},
		};
	});