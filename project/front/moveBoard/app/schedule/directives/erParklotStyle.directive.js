'use strict';

angular
	.module('app.schedule')
	.directive('erParklotStyle', function () {
	return {
		restrict: 'A',
		link: function ($scope, element) {
			$scope.$watch('current.status.raw', onChangeStatus);

			function onChangeStatus(newVal, oldVal) {
				element.removeClass(`status_${oldVal}`);
				element.addClass(`status_${newVal}`);
			}

			$scope.$watch('current.service_type.raw', onChangeServiceType);

			function onChangeServiceType(newVal, oldVal) {
				element.removeClass(`service_${oldVal}`);
				element.addClass(`service_${newVal}`);
			}
		}

	}
});