(function () {
    'use strict';

    angular
        .module('app.schedule')
        .factory('scheduleHelper', scheduleHelper);

    /* @ngInject */
    function scheduleHelper(dateFilter, scheduleConfig, datacontext, common, $timeout) {

        var service = {

            getMonthView: getMonthView,
            formatDate: formatDate,
            getWeekDayNames: getWeekDayNames,
            filterRequestsbyDay: filterRequestsbyDay,
            initParkLotArray: initParkLotArray

        };

        return service;
        //////////////////////


        function getMonthView(requests, currentDay, cellModifier) {

            var startOfMonth = moment(currentDay).startOf('month');
            var day = startOfMonth.clone().startOf('week');
            var endOfMonthView = moment(currentDay).endOf('month').endOf('week');
            var parkLot = [];
            var trucks = [];
            var trucksCount = 0;
            var fieldData = datacontext.getFieldData();

            if (angular.isDefined(fieldData)) {
                trucks = fieldData.taxonomy.trucks;
                trucksCount = common.count(trucks);
            }

            var view = [];
            var today = moment().startOf('day');

            while (day.isBefore(endOfMonthView)) {

                var inMonth = day.month() === moment(currentDay).month();

                let dayRequests = [];

                if (inMonth) {
                    dayRequests = filterRequestsbyDay(requests, day);
                }

                if (trucksCount) {
                    parkLot = service.initParkLotArray(dayRequests);
                }

                var cell = {
                    label: day.date(),
                    date: day.clone(),
                    currentDate: day.format("MM/DD/YYYY"),
                    inMonth: inMonth,
                    isPast: today.isAfter(day),
                    isToday: today.isSame(day),
                    isFuture: today.isBefore(day),
                    isWeekend: [0, 6].indexOf(day.day()) > -1,
                    trucks: trucks,
                    trucksCount: trucksCount,
                    requests: dayRequests,
                    parklot: parkLot,
                    badgeTotal: getBadgeTotal(dayRequests),

                };

                cellModifier({calendarCell: cell});

                view.push(cell);

                day.add(1, 'day');
            }

            return view;

        }

        function getWeekDayNames() {
            var weekdays = [];
            var count = 0;
            while (count < 7) {
                weekdays.push(formatDate(moment().weekday(count++), scheduleConfig.dateFormats.weekDay));
            }
            return weekdays;
        }


        function formatDate(date, format) {
            if (scheduleConfig.dateFormatter === 'angular') {
                return dateFilter(moment(date).toDate(), format);
            } else if (scheduleConfig.dateFormatter === 'moment') {
                return moment(date).format(format);
            }
        }


        function filterRequestsbyDay(requests, day) {
            var currentDay = formatDate(moment(day), scheduleConfig.dateFormats.standard);

            var todayRequests = [];
            angular.forEach(requests, function (request, key) {
                if (_.find(todayRequests, {nid: request.nid})) {
                    return;
                }

                if (request.date.value == currentDay) {
                    todayRequests.push(request);
                } else if (isFlatRateDeliveryRequest(request, day)) {
                    let copyRequest = angular.copy(request);
                    copyRequest.flatRate = true;
                    todayRequests.push(copyRequest);
                }
            });

            return todayRequests;
        }

        function isFlatRateDeliveryRequest(request, day) {
            let result = false;

            if (request.service_type.raw == 5) {
                var momentCurrentDate = moment(day);
	            var dateFrom;
	            var dateTo;

	            if (_.isEmpty(request.field_flat_rate_local_move) || (_.isObject(request.field_flat_rate_local_move) && !request.field_flat_rate_local_move.value)) {
	                dateFrom = request.delivery_date_from.value ? moment(request.delivery_date_from.value, 'DD-MM-YYYY') : '';
	                dateTo = request.delivery_date_to.value ? moment(request.delivery_date_to.value, 'DD-MM-YYYY') : '';
	                if (!request.delivery_date_to.raw) {
		                dateTo = request.delivery_date_from.value ? moment(request.delivery_date_from.value, 'DD-MM-YYYY') : '';
	                }
                } else {
                    dateFrom = request.date.value ? moment(request.date.value, 'DD-MM-YYYY') : '';
                    dateTo = request.date.value ? moment(request.date.value, 'DD-MM-YYYY'): '';
                }

                result = momentCurrentDate.isBetween(dateFrom, dateTo) || momentCurrentDate.isSame(dateFrom) || momentCurrentDate.isSame(dateTo);

            }

            return result;
        }

        function getBadgeTotal(events) {
            return events.filter(function (event) {
                return event.incrementsBadgeTotal !== false;
            }).length;
        }


        function initParkLotArray(requests) {

            var parklot = [];
            var unconparklot = [];
            var fieldData = datacontext.getFieldData();

            var trucks = fieldData.taxonomy.trucks;
            angular.forEach(trucks, function (truck, tid) {

                parklot[tid] = [];
                unconparklot[tid] = [];


            });
            var confirmed = 0;
            var unconfirmed = 0;

            let uniqueRequests = [];

            // SORT ARRAY ON CONFIRM AND NOT CONFIRMED
            angular.forEach(requests, function (request, nid) {
                if (uniqueRequests.indexOf(request.nid) >= 0) {
                    return;
                } else {
                    uniqueRequests.push(request.nid);
                }

                angular.forEach(request.trucks.raw, function (tid, value) {
                    if (request.status.raw == 3 && parklot[tid]) {
                        request.sT1 = common.convertTime(request.start_time1.value);
                        parklot[tid].push(request);

                        if (!request.flatRate) {
                            confirmed++;
                        }
                    } else if (request.status.raw == 2 && parklot[tid]) {
                        request.sT1 = common.convertTime(request.start_time1.value);
                        //  unconparklot[tid].push(request);
                        parklot[tid].push(request);
                        
                        if (!request.flatRate) {
                            unconfirmed++;
                        }
                    }
                });

            });

            //ORDER BY START TIME
            angular.forEach(trucks, function (truck, tid) {
                if (parklot[tid]) {
	                parklot[tid].sort(function (a, b) {
		                return common.convertTime(a['start_time1'].value) - common.convertTime(b['start_time1'].value)
	                });
                }

            });


            var DayParkLot = {
                confirmed: parklot,
                unconfirmed: unconparklot,
                confirmedCount: confirmed,
                unconfirmedCount: unconfirmed
            }

            return DayParkLot;

        }

    }
})();