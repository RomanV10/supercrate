(function () {
    'use strict';

    angular
        .module('app.schedule')
        .factory('scheduleTitle', scheduleTitle);

    scheduleTitle.$inject = ['scheduleConfig', 'scheduleHelper'];

    /* @ngInject */
    function scheduleTitle(scheduleConfig, scheduleHelper) {
        var service = {
            day: day,
            week: week,
            month: month,
            year: year
        };

        return service;
        //////////////////////

        function day(currentDay) {
            return scheduleHelper.formatDate(currentDay, scheduleConfig.titleFormats.day);
        }

        function week(currentDay) {
            var weekTitleLabel = scheduleConfig.titleFormats.week;
            return weekTitleLabel.replace('{week}', moment(currentDay).week()).replace('{year}', moment(currentDay).format('YYYY'));
        }

        function month(currentDay) {
            return scheduleHelper.formatDate(currentDay, scheduleConfig.titleFormats.month);
        }

        function year(currentDay) {
            return scheduleHelper.formatDate(currentDay, scheduleConfig.titleFormats.year);
        }
    }
})();