(function () {

    angular
        .module('app.schedule')

        .factory('ScheduleServices', ScheduleServices);

    ScheduleServices.$inject = ['$http', '$rootScope', '$q', 'config'];

    function ScheduleServices($http, $rootScope, $q, config) {

        var service = {};

        service.searchRequest = searchRequest;

        return service;


        function searchRequest(condition, callback) {


            var deferred = $q.defer();

            var data = {
                'condition': condition,
            };


            $http.post(config.serverUrl + 'server/move_request/search', data)
                 .success(function (data, status, headers, config) {

                     // TODO :Save to Local Storage For future use
                     //service.saveRequests(data);
                     deferred.resolve(data);

                 })
                 .error(function (data, status, headers, config) {

                     // data.success = false;
                     //SHOW ALERT LOG

                     deferred.reject(data);

                 });

            return deferred.promise;

        }


    }
})();