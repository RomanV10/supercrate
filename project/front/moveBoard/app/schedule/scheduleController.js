import { DISPATCH_LEGEND } from '../dispatch/dispatch-legend.constant';

(function () {
	'use strict';

	angular
		.module('app.schedule')
		.controller('Schedule', Schedule);

	Schedule.$inject = ['$scope', 'logger', 'common', 'apiService', 'moveBoardApi', 'MomentWrapperService', 'datacontext', 'serviceTypeService'];

	function Schedule($scope, logger, common, apiService, moveBoardApi, MomentWrapperService, datacontext, serviceTypeService) {

		// Inititate the promise tracker to track form submissions.

		var vm = this;
		//activate
		// draw calendar
		vm.calendarView = 'month';
		vm.calendarDay = new Date();

		vm.requests = [];
		vm.legend = DISPATCH_LEGEND;
		
		angular.forEach(vm.legend, (legendItem, key) => {
			legendItem.name = serviceTypeService.getCurrentForLegend(key + 1, legendItem.name);
		})

		activateSchedule(new Date());

		function activateSchedule(date) {
			vm.busy = true;

			var data = {
				date: MomentWrapperService.getZeroMoment(date).unix(),
				condition: {
					field_approve: [2, 3]
				}
			};

			let apiRequest = datacontext.getFieldData().is_common_branch_truck ? moveBoardApi.branchingTruck.getMonthBranchesTruck : moveBoardApi.requests.getParklotOfMonth;

			apiService.postData(apiRequest, data)
				.then(function (response) {
					common.$timeout(function () {
						vm.busy = false
					}, 1000);

					vm.requests = response.data;
					vm.calendarDay = date;
				}, function (reason) {
					vm.busy = false;
					logger.error(reason, reason, "Error");
				});
		}

		$scope.$on('request.updated', function (event, request) {
			_.forEach(vm.requests, function (value, index) {
				if (value.nid == request.nid) {
					vm.requests[index] = _.clone(request);
				}
			})
		});


		vm.prevMonth = function () {
			var calendarDay = moment(vm.calendarDay).subtract(1, "month").toDate();
			activateSchedule(calendarDay);
			$scope.$broadcast('shedule.closeDay', true);
		};

		vm.nextMonth = function () {
			var calendarDay = moment(vm.calendarDay).add(1, "month").toDate();
			$scope.$broadcast('shedule.closeDay', true);
			activateSchedule(calendarDay);
		}
	}

})();
