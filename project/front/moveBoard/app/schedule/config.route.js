(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('schedule', {
                        url: '/schedule',
                        abstract: true,
                        template: '<ui-view/>'
                    })
                    .state('schedule.requests', {
                        url: '/requests',
	                    template: require('./schedule.html'),
                        title: 'Schedule',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });
            }
        ]
    );
})();
