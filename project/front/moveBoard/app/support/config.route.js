(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('support', {
                        url: '/support/chatroom',
	                    template: require('./chat.room/index.html'),
                        title: 'Support',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });


            }
        ]
    );
})();
