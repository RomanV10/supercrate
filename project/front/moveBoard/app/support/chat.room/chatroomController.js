(function () {
    'use strict';
    angular

        .module('app.support')
        .controller('chatroomCtrl', chatroomCtrl);

    chatroomCtrl.$inject = ['$rootScope','datacontext','$route','$templateCache'];

    function chatroomCtrl($rootScope,datacontext,$route,$templateCache)
    {
        var vm = this;

        var fieldData = datacontext.getFieldData();
        var basicSettings =  angular.fromJson(fieldData.basicsettings);

        vm.busy = true;

        var user_name = $rootScope.currentUser.userId.field_user_first_name+' '+$rootScope.currentUser.userId.field_user_last_name;

        Tawk_API.onLoad = function(){
            vm.busy = false;
            Tawk_API.setAttributes({
                'company' :basicSettings.company_name,
                'user' :user_name
            }, function(error){});
            //place your code here
        };


        Tawk_API.onChatStarted = function(){

            Tawk_API.visitor = {
                name  : basicSettings.company_name,
                email : basicSettings.company_email
            };
            //place your code here
        };


    }
})();
