(function () {
    'use strict';

    angular.module('app.support', [])
        .run(function($templateCache) {});
})();