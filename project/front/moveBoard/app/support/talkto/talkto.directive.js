(function () {
    'use strict';

    angular
        .module('app.support')
        .directive('talkTo', talkTo);


    function talkTo($rootScope,datacontext,common) {
        var directive = {
            controller: cntrl,
	        template: require('./talkto.template.html'),
            restrict: 'E'
        };
        return directive;

        function cntrl($scope) {


            var fieldData = datacontext.getFieldData();
            var basicSettings =  angular.fromJson(fieldData.basicsettings);

            $scope.busy = true;

            var user_name = $rootScope.currentUser.userId.field_user_first_name+' '+$rootScope.currentUser.userId.field_user_last_name;

            var Tawk_API=Tawk_API||{};
            Tawk_API.onLoad = function(){
                $scope.busy = false;
                Tawk_API.setAttributes({
                    'company' :basicSettings.company_name,
                    'user' :user_name
                }, function(error){});
                //place your code here
            };


            Tawk_API.onChatStarted = function(){

                Tawk_API.visitor = {
                    name  : basicSettings.company_name,
                    email : basicSettings.company_email
                };
                //place your code here
            };



            $scope.$on('$destroy', function() {

            });

        }



    }
})();
