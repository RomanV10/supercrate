import './fileUploader.styl';

angular
	.module('app')
	.directive('fileUploader', fileUploader);

fileUploader.$inject = ["Upload", "config"];

function fileUploader(Upload, config) {
	return {
		restrict: 'A',
		scope: {
			label: '@',
			required: '@',
			url: '=',
			uri: '=',
			showImage: '='
		},
		template: require('./fileUploader.pug'),
		replace: false,

		link: ($scope, element, attrs) => {
			$scope.model = $scope.url;
			let button = element.find('button');
			let input = angular.element(element[0].querySelector('input#fileInput'));

			if ($scope.url) {
				setBackgroundImage($scope.url);
			}

			button.on('click', () => {
				input[0].click();
			});

			input.on('change', (event) => {
				Upload.upload({
					url: config.serverUrl + 'server/file_inventory',
					data: {file: event.target.files[0]}
				}).then(resolve => {
					$scope.url = resolve.data.data.url;
					$scope.uri = resolve.data.data.uri;
					$scope.model = resolve.data.data.url;
					setBackgroundImage(resolve.data.data.url);
				}, function (res) {
					console.log('Error status: ' + res.status);
				}, function (evt) {
					$scope.loadStatus = parseInt(100.0 * evt.loaded / evt.total);
				});

			});

			function setBackgroundImage(image) {
				$scope.imageHolderStyle = {
					background: `url(${image}) no-repeat center top`
				};
			}

		}
	};
}
