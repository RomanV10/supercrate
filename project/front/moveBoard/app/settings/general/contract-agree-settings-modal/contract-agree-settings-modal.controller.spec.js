describe('General settings: contract agree settings modal controller', () => {
	let $controller,
		$rootScope,
		ContractAgreeSettingsController,
		$scope,
		settingServices,
		settings,
		$uibModalInstance;

	let fakePromiseResolve = {
		then: callback => {
			callback();
		}
	};

	beforeEach(inject((_$controller_, _$rootScope_, _SettingServices_) => {
		$controller = _$controller_;
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		settingServices = _SettingServices_;

		settings = {
			useCustomConfirmationText: false,
			customConfirmationText: ''
		};

		$uibModalInstance = {
			/*eslint no-empty-function:0*/
			dismiss: () => {},
			/*eslint no-empty-function:0*/
			close: () => {},
		};

		ContractAgreeSettingsController = $controller('ContractAgreeSettings', {
			$uibModalInstance: $uibModalInstance,
			$scope: $scope,
			settings: settings
		});
	}));

	describe('Controller is defined', () => {
		it('ContractAgreeSettingsController should be defined', () => {
			expect(ContractAgreeSettingsController).toBeDefined();
		});
	});

	describe('After init', () => {
		it('Cancel function should be defined', () => {
			expect($scope.closeContractAgreeSettings).toBeDefined();
		});
		it('saveContractAgreeSettings function should be defined', () => {
			expect($scope.saveContractAgreeSettings).toBeDefined();
		});
		it('changeCancellationSetting have to be defined', () => {
			expect($scope.changeCancellationSetting).toBeDefined();
		});
	});

	describe('Save settings', () => {
		beforeEach(function () {
			spyOn(settingServices, 'saveSettings').and.returnValue(fakePromiseResolve);
			spyOn($uibModalInstance, 'dismiss');
		});
		it('saveSettings should be run', () => {
			$scope.saveContractAgreeSettings();
			expect(settingServices.saveSettings).toHaveBeenCalled();
		});
		it('useCustomConfirmationText should be truthy', () => {
			$scope.useCustomConfirmationText = true;
			$scope.saveContractAgreeSettings();
			expect(settings.useCustomConfirmationText).toBeTruthy();
		});
		it('modal window should be close',() => {
			$scope.saveContractAgreeSettings();
			expect($uibModalInstance.dismiss).toHaveBeenCalled();
		});
	});

	describe('changeCancellationSetting', () => {
		it('useDefaultText on change should disable useCustomConfirmationText', () => {
			// when
			$scope.changeCancellationSetting('useCustomConfirmationText', 'useDefaultText');
			// then
			expect($scope.useCustomConfirmationText).toBeFalsy();
		});

		it('useCustomConfirmationText on change should enable useDefaultText', () => {
			// when
			$scope.changeCancellationSetting('useDefaultText', 'useCustomConfirmationText');
			// then
			expect($scope.useDefaultText).toBeTruthy();
		});
	});
});
