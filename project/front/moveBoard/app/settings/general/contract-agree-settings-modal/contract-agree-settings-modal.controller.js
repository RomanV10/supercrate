import './contract-agree-settings.styl';

'use strict';

angular.module('app.settings')
	.controller('ContractAgreeSettings', ContractAgreeSettings);

ContractAgreeSettings.$inject = ['$scope', '$uibModalInstance', 'SettingServices', 'settings'];

function ContractAgreeSettings($scope, $uibModalInstance, SettingServices, settings) {
	const CANCELLATION_BUTTON_HTML = require('./cancellation-button.tpl.html');
	const COMPANY_BUTTON_HTML = require('./company-button.tpl.html');
	const CANCELLATION_BUTTON = '[Cancellation And Reschedule Policy]';
	const COMPANY_BUTTON = '[Company Policy]';
	$scope.saveContractAgreeSettings = saveContractAgreeSettings;
	$scope.closeContractAgreeSettings = closeContractAgreeSettings;
	$scope.changeCancellationSetting = changeCancellationSetting;

	function init() {
		$scope.useDefaultText = _.get(settings, 'useDefaultText', !settings.useCustomConfirmationText);
		$scope.useCustomConfirmationText = settings.useCustomConfirmationText;
		$scope.customConfirmationText = settings.customConfirmationText
			.split(CANCELLATION_BUTTON_HTML).join(CANCELLATION_BUTTON)
			.split(COMPANY_BUTTON_HTML).join(COMPANY_BUTTON);
	}

	function saveContractAgreeSettings() {
		$scope.busy = true;
		settings.useDefaultText = $scope.useDefaultText;
		settings.useCustomConfirmationText = $scope.useCustomConfirmationText;
		settings.customConfirmationText = $scope.customConfirmationText
			.split(CANCELLATION_BUTTON).join(CANCELLATION_BUTTON_HTML)
			.split(COMPANY_BUTTON).join(COMPANY_BUTTON_HTML);
		let setting_name = 'contract_page';
		SettingServices.saveSettings(settings, setting_name)
			.then(() => {
				$scope.busy = false;
				toastr.success('Settings updated');
				$uibModalInstance.dismiss('close');
			});
	}

	function closeContractAgreeSettings() {
		$uibModalInstance.dismiss('close');
	}

	function changeCancellationSetting(outdated, actual) {
		let outdatedValue = $scope[outdated];
		$scope[outdated] = !$scope[actual];
		$scope[actual] = outdatedValue;
	}

	init();
}
