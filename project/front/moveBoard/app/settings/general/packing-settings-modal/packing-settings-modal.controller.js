import './packing-settings-modal.styl';

(function () {
	'use strict';

	angular.module('app.settings')
		.controller('PackingModalCtrl', PackingModalCtrl);

	PackingModalCtrl.$inject = ['$scope', 'basicSettings', '$uibModalInstance', 'SettingServices', 'common'];

	function PackingModalCtrl($scope, basicSettings, $uibModalInstance, SettingServices, common) {
		$scope.advancedPackingSettingsService = basicSettings.packingAdvancedSettings.serviceType;
		$scope.advancedPackingSettingsStatus = basicSettings.packingAdvancedSettings.status;
		$scope.hidePrice = basicSettings.packingAdvancedSettings.hidePrice;

		$scope.cancel = cancel;
		$scope.save = save;
		$scope.checkAll = checkAll;
		$scope.changeSetting = changeSetting;

		function cancel() {
			$uibModalInstance.dismiss('close');
		}

		function changeSetting() {
			$scope.allSettingsService = Object.keys($scope.advancedPackingSettingsService).every(function(k){ return $scope.advancedPackingSettingsService[k].show === true; });
			$scope.allSettingsStatus = Object.keys($scope.advancedPackingSettingsStatus).every(function(k){return $scope.advancedPackingSettingsStatus[k].show === true;});

			if ($scope.allSettingsService) {
				checkAll($scope.advancedPackingSettingsService, $scope.allSettingsService);
			}

			if ($scope.allSettingsStatus) {
				checkAll($scope.advancedPackingSettingsStatus, $scope.allSettingsStatus);
			}
		}

		function save() {
			basicSettings.packingAdvancedSettings.hidePrice = $scope.hidePrice;
			basicSettings.packingAdvancedSettings.serviceType = $scope.advancedPackingSettingsService;
			basicSettings.packingAdvancedSettings.status = $scope.advancedPackingSettingsStatus;
			var setting = basicSettings;
			var _setting_name = 'basicsettings';
			var promise = SettingServices.saveSettings(setting, _setting_name);
			promise.then(function () {
				common.$timeout(function () {
					$scope.saved = false;
				}, 1000);

				$scope.saved = true;
			});
			$uibModalInstance.dismiss('close');
		}
		
		function checkAll(arr, check) {
			angular.forEach(arr, function (item) {
				if (item.name == 'Confirmed') {
					return;
				}
				item.show = check;
			})
		}

		changeSetting();
	}
})();