'use strict';
import './confirmation-titles-modal.styl';

angular.module('app.settings')
	.controller('ConfirmationTitlesModal', ConfirmationTitlesModal);

/* @ngInject */
function ConfirmationTitlesModal($scope, $uibModalInstance, serviceTypeService, SettingServices, settings) {
	$scope.save = save;
	$scope.cancel = cancel;

	$scope.confirmationPageTitles = _.clone(settings.confirmationPageTitles) || [];
	$scope.serviceTypes = serviceTypeService.getAllServiceTypes();
	$scope.activeTab = $scope.serviceTypes[0].id;

	function save() {
		$scope.busy = true;
		settings.confirmationPageTitles = $scope.confirmationPageTitles;

		SettingServices.saveSettings(settings, 'contract_page')
			.then(function () {
				$scope.busy = false;
				toastr.success('Settings updated');
				$uibModalInstance.dismiss('close');
			})
			.catch(function () {
				$scope.busy = false;
				toastr.error('Settings NOT updated');
				$uibModalInstance.dismiss('close');
			});
	}

	function cancel() {
		$uibModalInstance.dismiss('close');
	}
}
