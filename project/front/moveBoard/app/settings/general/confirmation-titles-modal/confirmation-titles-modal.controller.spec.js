describe('unit: confirmation-titles-modal.controller,', () => {
	let $controller, $scope, SettingServices, settings;
	beforeEach(inject((_$controller_) => {
		$controller = _$controller_;
		$scope = $rootScope.$new();

		SettingServices = {
			saveSettings: () => {
				let fakePromise = {
					'then': () => fakePromise,
					'catch': () => fakePromise
				};
				return fakePromise;
			}
		};
		settings = {
			confirmationPageTitles: ['ugugu']
		};

		$controller('ConfirmationTitlesModal', {
			$scope,
			$uibModalInstance: {
				dismiss: () => {}
			},
			serviceTypeService: {
				getAllServiceTypes: () => [{id: 1}]
			},
			SettingServices,
			settings
		});

	}));

	describe('After init', () => {
		it('Should have function save()', () => {
			expect($scope.save).toBeDefined();
		});
		it('Should have function cancel()', () => {
			expect($scope.cancel).toBeDefined();
		});
		it('Should have confirmationPageTitles', () => {
			expect($scope.confirmationPageTitles).toBeDefined();
		});
		it('confirmationPageTitles should be clone', () => {
			expect($scope.confirmationPageTitles).not.toBe(settings.confirmationPageTitles);
			expect($scope.confirmationPageTitles).toEqual(settings.confirmationPageTitles);
		});
		it('Should have serviceTypes', () => {
			expect($scope.serviceTypes).toBeDefined();
		});
		it('Should have activeTab = 1', () => {
			expect($scope.activeTab).toEqual(1);
		});
	});

	describe('function save()', () => {
		beforeEach(() => {
			$scope.confirmationPageTitles[0] = 'lalala';
			spyOn(SettingServices, 'saveSettings').and.callThrough();
			$scope.save();
		});
		it('Should call SettingServices.saveSettings', () => {
			expect(SettingServices.saveSettings).toHaveBeenCalled();
		});
		it('Should copy confirmationPageTitles to settings', () => {
			expect(settings.confirmationPageTitles[0]).toEqual('lalala');
			expect($scope.confirmationPageTitles).toBe(settings.confirmationPageTitles);
		});
	});
	describe('function cancel()', () => {
		beforeEach(() => {
			$scope.confirmationPageTitles[0] = 'lalala';
			spyOn(SettingServices, 'saveSettings').and.callThrough();
			$scope.cancel();
		});
		it('Should NOT call SettingServices.saveSettings', () => {
			expect(SettingServices.saveSettings).not.toHaveBeenCalled();
		});
		it('Should NOT copy confirmationPageTitles to settings', () => {
			expect(settings.confirmationPageTitles[0]).toEqual('ugugu');
			expect($scope.confirmationPageTitles).not.toBe(settings.confirmationPageTitles);
		});
	});
});
