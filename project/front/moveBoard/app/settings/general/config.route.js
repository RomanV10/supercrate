angular.module("app")
	.config(settingsConfig);

settingsConfig.$inject = ['$stateProvider'];

function settingsConfig($stateProvider) {
	$stateProvider
		.state('settings.general', {
			url: '/general',
			template: require('./templates/general-page.html'),
			title: 'General',
			data: {
				permissions: {
					except: ['anonymous', 'foreman', 'helper']
				}
			}
		});
}
