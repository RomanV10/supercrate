'use strict';

import './hourly-rates-settings.styl';
import './payment-settings.styl';

angular
	.module('app.settings')
	.controller('GeneralController', General);

/*@ngInject*/
function General($scope, $q, $rootScope, datacontext, common, SettingServices, RatesServices, SweetAlert, PaymentServices, dragulaService, PermissionsServices, MoveCouponService, $uibModal, BranchAPIService, SmtpService, CalculatorServices) {
	const HIDE_SAVED_TIMEOUT = 1000;
	const NEXT_YEAR_COUNT = 1;
	const SUCCESS_SMTP_TEST_ANSWER = 1;
	const USED_CUPON_STATUS = 1;
	const SUCCES_TEST_PARSER_CONNECTION = 200;
	const BASIC_SETTING_NAME = 'basicsettings';
	const DISPATCH_SETTINGS = {
		name: 'Dispatch',
		url: 'app/settings/general/templates/dispatch.html',
		selected: false,
		icon: 'fa fa-users'
	};
	const isSuperUser = PermissionsServices.isSuper();

	let vm = this;

	vm.updateZipCode = updateZipCode;
	vm.updateMainState = updateMainState;
	vm.removeCoupon = removeCoupon;
	vm.chooseCouponTab = chooseCouponTab;
	vm.changePromo = changePromo;
	vm.createCoupon = createCoupon;
	vm.openCoupon = openCoupon;

	function _initFieldData() {
		vm.fieldData = datacontext.getFieldData();
		vm.calendarTemp = datacontext.getFieldData().calendar;
		vm.calendarTypes = datacontext.getFieldData().calendartype;
	}

	_initFieldData();

	vm.currentYear = moment().year();
	vm.calendar = {};
	vm.calendar[vm.currentYear] = vm.calendarTemp[vm.currentYear];
	vm.calendar[vm.currentYear + NEXT_YEAR_COUNT] = vm.calendarTemp[vm.currentYear + NEXT_YEAR_COUNT];
	vm.amoutValuation = [];
	vm.addValuation = false;
	vm.PermissionsServices = PermissionsServices;

	vm.surcharge = [];
	vm.fuel_surcharge = {};
	vm.fuel_surcharge.by_mileage = [];
	vm.wayOneOptions = [
		{name: 'Please select direction', value: ''},
		{name: 'Shortest', value: 'shortest'},
		{name: 'Longest', value: 'longest'},
		{name: 'From C to A', value: 'origin'},
		{name: 'From C to B', value: 'destination'},
	];

	vm.basicSettings = angular.fromJson(vm.fieldData.basicsettings);
	vm.amoutValuation = _.clone(vm.basicSettings.valuation.columns);
	vm.valuation = _.clone(vm.basicSettings.valuation.amounts);
	vm.valuationText = _.clone(vm.basicSettings.valuation.text);
	vm.fuel_surcharge.settingsByMileage = vm.basicSettings.fuel_surcharge.settingsByMileage;
	vm.fuel_surcharge.settingsByMileageLD = vm.basicSettings.fuel_surcharge.settingsByMileageLD;
	vm.fuel_surcharge.def_local = vm.basicSettings.fuel_surcharge.def_local;
	vm.fuel_surcharge.def_ld = vm.basicSettings.fuel_surcharge.def_ld;
	vm.surcharge = vm.basicSettings.fuel_surcharge.by_mileage;
	vm.equipment_fee = vm.basicSettings.equipment_fee;
	let branches = vm.fieldData.branches;

	vm.range = common.range;

	vm.clndr_options = {
		weekOffset: 1,
		daysOfTheWeek: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
	};

	vm.saved = false;
	vm.saving = false;

	vm.cubic_feet_lm_test = 0;
	vm.cubic_feet_ld_test = 0;

	vm.tabs =
		[
			{
				name: 'Basic',
				url: 'app/settings/general/templates/basicSettings.html?7',
				selected: true,
				icon: 'icon-disc'
			},
			{
				name: 'Rates',
				url: 'app/settings/general/templates/ratesSettings.html?7',
				selected: false,
				icon: 'fa fa-dollar'
			},
			{
				name: 'Calendar',
				url: 'app/settings/general/templates/calendarSettings.html?2',
				selected: false,
				icon: 'fa fa-calendar'
			},
			{
				name: 'Trucks',
				url: 'app/settings/general/templates/trucksSettings.html',
				selected: false,
				icon: 'fa fa-truck'
			},
			{
				name: 'Payments',
				url: 'app/settings/general/templates/paymentsSettings.html',
				selected: false,
				icon: 'fa fa-dollar'
			},
			{
				name: 'Contract page',
				url: 'app/settings/general/templates/contractSettings.html?4',
				selected: false,
				icon: 'fa fa-dollar'
			},
			{
				name: 'Extra Services',
				url: 'app/settings/general/templates/extraSettings.html?4',
				selected: false,
				icon: 'icon-globe'
			},
			{
				name: 'Packing',
				url: 'app/settings/general/templates/packingSettings.html?4',
				selected: false,
				icon: 'icon-globe'
			},
			{
				name: 'Company Services',
				url: 'app/settings/general/templates/companyServices.html',
				selected: false,
				icon: 'icon-wrench'
			},
			{
				name: 'Marketing Tools',
				url: 'app/settings/general/templates/marketingSettings.html',
				selected: false,
				icon: 'icon-graph'
			},
			{
				name: 'Company Site',
				url: 'app/settings/general/templates/companySettings.html',
				selected: false,
				icon: 'icon-graph'
			},
			{
				name: 'Valuation Plan',
				url: 'app/settings/general/templates/valuation.html',
				selected: false,
				icon: 'fa fa-table'
			},
			{
				name: 'Company Flags',
				url: 'app/settings/general/templates/companyFlags.html',
				selected: false,
				icon: 'fa fa-flag'
			},
			{
				name: 'Fuel Surcharge',
				url: 'app/settings/general/templates/fuelSurcharge.html',
				selected: false,
				icon: 'fa fa-tint'
			},
			{
				name: 'Parsing',
				url: 'app/settings/general/templates/parsing.html',
				selected: false,
				icon: 'fa fa-magic'
			},
			{
				name: 'Website SMTP',
				url: 'app/settings/general/templates/web-site-smtp.html',
				selected: false,
				icon: 'fa fa-paper-plane'
			},
			{
				name: 'Integration',
				url: 'app/settings/general/templates/integration.html',
				selected: false,
				icon: 'fa fa-puzzle-piece'
			},
		];



	function init() {
		vm.template = vm.tabs[0];

		if (vm.PermissionsServices.hasPermission('administrator')) {
			let branchTab = {
				name: 'Branches',
				url: 'app/settings/general/templates/branchSettings.html',
				selected: false,
				icon: 'icon-globe'
			};

			vm.tabs.splice(1, 0, branchTab);
		}

		if (isSuperUser) vm.tabs.push(DISPATCH_SETTINGS);

		vm.currCoupons = [];
		vm.pastCoupons = [];
		vm.purchasesCoupons = [];
		vm.redeemedCoupons = [];
		vm.userArrUid = [];
		vm.couponsTabs = {
			showOverview: true,
			showPurchases: false,
			showRedeemed: false
		};

		if (_.isString(vm.basicSettings.own_cancelation_policy)) {
			vm.basicSettings.own_cancelation_policy = {
				text: vm.basicSettings.own_cancelation_policy,
				show: false
			};
		}

		if (_.isUndefined(vm.basicSettings.own_company_policy)) {
			vm.basicSettings.own_company_policy = {
				text: '',
				show: false
			};
		}

		if (_.isUndefined(vm.basicSettings.hideForemanPastJobsTab)) {
			vm.basicSettings.hideForemanPastJobsTab = false;
		}

		if (angular.isDefined(vm.fieldData.calcsettings)) {
			vm.calcSettings = angular.fromJson(vm.fieldData.calcsettings);
		}

		vm.login_id = '';
		vm.transaction_key = '';

		PaymentServices.getAuthorizeNet()
			.then(function (data) {
				vm.login_id = data.loginid;
				vm.transaction_key = data.transactionkey;
			});

		SettingServices.getSettings('website_url')
			.then(function (data) {
				vm.website_url = data;
			});

		SettingServices.getParser()
			.then(function (data) {
				$scope.parsing = data;
			});

		vm.ratesSettings = {};
		vm.storageRates = {};

		RatesServices.getRates()
			.then(function (data) {
				vm.ratesSettings = data;
			});

		RatesServices.getStorageRates()
			.then(function (data) {
				vm.storageRates = data;
			});
	}

	vm.select = function (tab) {
		angular.forEach(vm.tabs, function (tab) {
			tab.selected = false;
		});

		tab.selected = true;
		vm.template = tab;
	};

	vm.saveSettings = function (type, value) {
		vm.saving = true;
		let setting = vm.basicSettings;
		let _setting_name = BASIC_SETTING_NAME;

		if (angular.isDefined(type)) {
			_setting_name = type;
		}

		if (angular.isDefined(value)) {
			setting = value;
		}

		SettingServices.saveSettings(setting, _setting_name)
			.then(function () {
				vm.saving = false;
				showSaved();
			});
	};

	vm.saveSettingsTravelTime = function () {
		vm.saved = false;

		SettingServices.saveSettings(vm.calcSettings, 'calcsettings')
			.then(function () {
				vm.fieldData.calcsettings = vm.calcSettings;
				CalculatorServices.init(vm.fieldData);
				vm.saving = false;
				showSaved();
			});
	};

	vm.setSiteLog = function () {
		SettingServices.setSiteLog(vm.site_log_id, vm.site_log_name);
	};

	vm.saveAuthorizeNet = function (loginId, AuthKey) {
		vm.saveSettings();
		PaymentServices.setAuthorizeNet(loginId, AuthKey);
	};

	vm.saveParserSettings = function () {
		let data = {};
		data.data = $scope.parsing;
		$scope.parsing.incoming.active = 1;

		SettingServices.setParser(data)
			.then(() => {
				vm.saving = false;
				showSaved();
			});
	};

	vm.testParserSettings = function () {
		vm.busyParser = true;
		vm.saveParserSettings();

		SettingServices.testConnection()
			.then(
				(data) => {
					vm.busyParser = false;

					if (data.status == SUCCES_TEST_PARSER_CONNECTION) {
						toastr.success('Connection success');
					} else {
						SweetAlert.swal(data.statusText, '', 'error');
					}
				},
				(err) => {
					vm.busyParser = false;
					SweetAlert.swal('Error', err, 'error');
				}
			);
	};

	vm.chooseConnectionType = function (type) {
		if (type == 'tls') {
			$scope.parsing.incoming.ssl = false;
		} else {
			$scope.parsing.incoming.tls = false;
		}
	};

	vm.saveRates = function () {
		vm.saving = true;

		RatesServices.setRates(vm.ratesSettings)
			.then(() => {
				vm.saving = false;
				showSaved();
			})
			.finally(() => {
				for (let type in vm.ratesSettings) {
					vm.ratesSettings[type] = vm.ratesSettings[type].map(rate => Number(rate));
				}
				CalculatorServices.updateRateSettings(vm.ratesSettings);
			});
	};


	vm.saveStorageRates = function () {
		vm.saving = true;

		RatesServices.setStorageRates(vm.storageRates)
			.then(() => {
				vm.saving = false;
				showSaved();
			});
	};

	vm.addNewValuation = function () {
		vm.newValuation = {};
		vm.newValuation.amount = 0;
	};

	vm.changeDeductible = function (index, value) {
		vm.amoutValuation[index] = Number(value);
		vm.basicSettings.valuation.columns = _.clone(vm.amoutValuation);
		vm.saveSettings();
	};

	vm.removeColumn = function (index) {
		SweetAlert.swal(
			{
				title: 'Are you sure you want to remove this Deductible Level?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, let\'s do it!',
				cancelButtonText: 'No, cancel pls!',
				closeOnConfirm: true,
				closeOnCancel: true
			},
			(isConfirm) => {
				if (isConfirm) {
					if (vm.amoutValuation.length) {
						vm.amoutValuation.splice(index, 1);

						_.forEach(vm.valuation, function (value) {
							value.splice(index, 1);
						});

						vm.basicSettings.valuation.amounts = _.clone(vm.valuation);
						vm.basicSettings.valuation.columns = _.clone(vm.amoutValuation);
						vm.saveSettings();
					}
				}
			});
	};

	vm.saveNewColumn = function () {
		if (!vm.amoutValuation) {
			vm.amoutValuation = [];
		}

		vm.amoutValuation.push(vm.newColumn);

		_.forEach(vm.valuation, function (value) {
			value.push(0);
		});

		vm.newColumn = null;
		vm.basicSettings.valuation.columns = _.clone(vm.amoutValuation);
		vm.saveSettings();
	};

	vm.saveNewValuation = function () {
		vm.valuation[vm.newValuation.amount] = [];
		for (let i in vm.newValuation.values) {
			vm.valuation[vm.newValuation.amount][i] = vm.newValuation.values[i];
		}
		vm.addValuation = false;
		vm.basicSettings.valuation.amounts = _.clone(vm.valuation);
		vm.saveSettings();
	};

	vm.saveChanges = function (key, index) {
		vm.valuation[key][index] = Number(vm.valuation[key][index]);
		vm.basicSettings.valuation.amounts = _.clone(vm.valuation);
		vm.saveSettings();
	};


	vm.removeValuation = function (amount) {
		SweetAlert.swal(
			{
				title: 'Are you sure you want to remove this valuation?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, let\'s do it!',
				cancelButtonText: 'No, cancel pls!',
				closeOnConfirm: true,
				closeOnCancel: true
			},
			(isConfirm) => {
				if (isConfirm) {
					delete vm.valuation[amount];
					vm.basicSettings.valuation.amounts = _.clone(vm.valuation);
					vm.saveSettings();
				}
			});
	};

	vm.saveText = function () {
		vm.basicSettings.valuation.text = _.clone(vm.valuationText);
		vm.saveSettings();
	};

	vm.addNewSurcharge = function () {
		vm.newSurcharge = {};
		vm.newSurcharge.from = '';
		vm.newSurcharge.to = '';
		vm.newSurcharge.amount = 0;
	};

	vm.addNewEquipmentFee = function () {
		vm.newEquipmentFee = {};
		vm.newEquipmentFee.from = '';
		vm.newEquipmentFee.to = '';
		vm.newEquipmentFee.amount = 0;
	};

	vm.saveNewSurcharge = function () {
		vm.addSurcharge = false;
		vm.surcharge.push(vm.newSurcharge);
		vm.newSurcharge = '';
		vm.saveChangesSurcharge();
	};

	vm.saveNewEquipmentFee = function () {
		vm.addEquipmentFee = false;
		vm.equipment_fee.by_mileage.push(vm.newEquipmentFee);
		vm.newEquipmentFee = '';
		vm.saveChangesEquipmentFee();
	};

	vm.saveChangesSurcharge = function () {
		common.$timeout(() => {
			vm.fuel_surcharge.by_mileage = _.clone(vm.surcharge);
			vm.basicSettings.fuel_surcharge = _.clone(vm.fuel_surcharge);
			vm.saveSettings();
		}, 100);
	};

	vm.saveChangesEquipmentFee = function () {
		vm.basicSettings.equipment_fee = _.clone(vm.equipment_fee);
		vm.saveSettings();
	};


	vm.removeSurcharge = function (key) {
		SweetAlert.swal(
			{
				title: 'Are you sure you want to remove this surcharge?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, let\'s do it!',
				cancelButtonText: 'No, cancel pls!',
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					vm.surcharge.splice(key, 1);
					vm.saveChangesSurcharge();
				}
			});
	};


	vm.removeEquipmentFee = function (key) {
		SweetAlert.swal(
			{
				title: 'Are you sure you want to remove this equipment fee?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, let\'s do it!',
				cancelButtonText: 'No, cancel pls!',
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					vm.equipment_fee.by_mileage.splice(key, 1);
					vm.saveChangesEquipmentFee();
				}
			});
	};

	function refreshCoupons() {
		vm.currCoupons = [];
		vm.pastCoupons = [];
		vm.purchasesCoupons = [];
		vm.redeemedCoupons = [];

		MoveCouponService.getCouponsByAdmin()
			.then(function (data) {
				_.forEach(data, function (item) {
					if (item.value.active) {
						vm.currCoupons.push(item);
					} else {
						vm.pastCoupons.push(item);
					}
				});
			});

		MoveCouponService.getUserCoupons()
			.then(function (data) {
				if (_.head(data) == false) {
					return false;
				}

				_.forEach(data, function (item) {
					if (item.used == USED_CUPON_STATUS) {
						vm.redeemedCoupons.push(item);
					}
				});

				vm.purchasesCoupons = data;
			});
	}

	refreshCoupons();

	function chooseCouponTab(type) {
		_.forEach(vm.couponsTabs, function (item, key) {
			vm.couponsTabs[key] = false;
		});

		vm.couponsTabs[type] = true;
	}

	function removeCoupon(coupon) {
		SweetAlert.swal(
			{
				title: 'Are you sure, you want to remove this coupon?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, remove',
				cancelButtonText: 'Cancel',
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					MoveCouponService.deleteCouponById(coupon.id)
						.then(function () {
							toastr.success('Coupon has been successfully removed', 'Success!');
							refreshCoupons();
						});
				}
			});
	}

	function changePromo(promo) {
		return '***' + promo.substr(-3);
	}

	function createCoupon() {
		$uibModal.open({
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			template: require('./templates/createCoupon.html'),
			controller: ['$scope', '$uibModalInstance', 'MoveCouponService', 'settings', '$rootScope', 'SweetAlert', CreateCouponCtrl],
			size: 'md',
			resolve: {
				settings: () => vm.basicSettings,
			}
		});
	}

	function openCoupon(coupon) {
		$uibModal.open({
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			template: require('./templates/createCoupon.html'),
			controller: ['$scope', '$uibModalInstance', 'MoveCouponService', 'settings', '$rootScope', 'SweetAlert', 'coupon', CreateCouponCtrl],
			size: 'md',
			resolve: {
				settings: () => vm.basicSettings,
				coupon: () => coupon,
			}
		});
	}

	function CreateCouponCtrl($scope, $uibModalInstance, MoveCouponService, settings, $rootScope, SweetAlert, coupon) {
		const FIFTY_DEFAULT_CUPON_PRICE = 50;
		const ONE_HUNDRED_DEFAULT_CUPON_PRICE = 100;
		const ONE_HUNDRED_DEFAULT_CUPON_DISCOUNT = 100;
		const ONE_HUNDRED_AND_FIFTY_DEFAULT_CUPON_DISCOUNT = 150;
		$scope.settings = settings;
		$scope.busy = false;
		$scope.coupon = {
			active: 1,
			name: '',
			description: '',
			coupons_count: 0,
			coupons_left: 0,
			discount: 0,
			price: 0,
			start_date: moment().unix(),
			terms: []
		};
		$scope.couponPrice = FIFTY_DEFAULT_CUPON_PRICE;
		$scope.couponLimit = false;

		changePrice();

		if (coupon) {
			$scope.editCoupon = true;
			$scope.basicCoupon = angular.copy(coupon);
			$scope.coupon = angular.copy(coupon.value);
			if ($scope.coupon.price == FIFTY_DEFAULT_CUPON_PRICE || $scope.coupon.price == ONE_HUNDRED_DEFAULT_CUPON_PRICE) {
				$scope.couponPrice = $scope.coupon.price;
			} else {
				$scope.couponPrice = 'custom';
			}
			if ($scope.coupon.coupons_count < 0) {
				$scope.couponLimit = true;
			}
		}

		$scope.changePrice = changePrice;
		$scope.changeLimit = changeLimit;
		$scope.addTerm = addTerm;
		$scope.removeTerm = removeTerm;
		$scope.createNewCoupon = createNewCoupon;
		$scope.updateCoupon = updateCoupon;
		$scope.closeModal = closeModal;

		function changePrice() {
			if ($scope.couponPrice == FIFTY_DEFAULT_CUPON_PRICE) {
				$scope.coupon.discount = ONE_HUNDRED_DEFAULT_CUPON_DISCOUNT;
				$scope.coupon.price = FIFTY_DEFAULT_CUPON_PRICE;
			}

			if ($scope.couponPrice == ONE_HUNDRED_DEFAULT_CUPON_PRICE) {
				$scope.coupon.discount = ONE_HUNDRED_AND_FIFTY_DEFAULT_CUPON_DISCOUNT;
				$scope.coupon.price = ONE_HUNDRED_DEFAULT_CUPON_PRICE;
			}

			if ($scope.couponPrice == 'custom') {
				$scope.coupon.discount = 0;
				$scope.coupon.price = 0;
			}
			$scope.coupon.name = $scope.coupon.price + '$ for ' + $scope.coupon.discount + '$';
			$scope.coupon.description = 'You get a voucher redeemable for $' + $scope.coupon.discount;
		}

		function changeLimit() {
			if ($scope.couponLimit) {
				$scope.coupon.coupons_count = -1;
				$scope.coupon.coupons_left = -1;
			} else {
				$scope.coupon.coupons_count = 0;
				$scope.coupon.coupons_left = 0;
			}
		}

		function addTerm() {
			if (!$scope.coupon.terms) {
				$scope.coupon.terms = [];
			}
			$scope.coupon.terms.push('');
		}

		function removeTerm(ind) {
			$scope.coupon.terms.splice(ind, 1);
		}

		function createNewCoupon() {
			$scope.busy = true;
			$scope.coupon.start_date = moment().unix();
			$scope.coupon.name = $scope.coupon.price + '$ for ' + $scope.coupon.discount + '$';
			$scope.coupon.description = 'You get a voucher redeemable for $' + $scope.coupon.discount;

			MoveCouponService.createCoupon($scope.coupon)
				.then(() => {
					$scope.busy = false;
					$rootScope.$broadcast('refresh.coupons');
					$uibModalInstance.dismiss('cancel');
				});
		}

		function updateCoupon() {
			$scope.busy = true;
			$scope.coupon.name = $scope.coupon.price + '$ for ' + $scope.coupon.discount + '$';
			$scope.coupon.description = 'You get a voucher redeemable for $' + $scope.coupon.discount;

			MoveCouponService.updateCouponById($scope.coupon, $scope.basicCoupon.id)
				.then(
					() => {
						$scope.busy = false;
						$rootScope.$broadcast('refresh.coupons');
						$uibModalInstance.dismiss('cancel');
					},
					(error) => {
						$scope.busy = false;
						SweetAlert.swal('Error!', error, 'error');
					}
				);
		}

		function closeModal() {
			$uibModalInstance.dismiss('cancel');
		}
	}

	$scope.$on('refresh.coupons', function () {
		refreshCoupons();
	});

	function updateZipCode() {
		if (isActiveBranching()) {
			BranchAPIService.updateParkingZip(vm.basicSettings.parking_address);
		}
	}

	function updateMainState() {
		if (isActiveBranching()) {
			BranchAPIService.updateBasedState(vm.basicSettings.main_state);
		}
	}

	function isActiveBranching() {
		return branches.length >= 1;
	}

	function getSTMPSettings() {
		let settings = SmtpService.getSMTPSettings();
		vm.smtpEncryptionSettings = {
			ssl: false,
			tls: false
		};

		$q.all(settings).then(function (resolve) {
			vm.smtpHost = resolve.smtpSettings.data.swiftmailer_smtp_host;
			vm.smtpUser = resolve.smtpSettings.data.swiftmailer_smtp_username;
			vm.smtpPass = resolve.smtpSettings.data.swiftmailer_smtp_password;
			vm.smtpPort = resolve.smtpSettings.data.swiftmailer_smtp_port;
			vm.smtpEncryption = resolve.smtpSettings.data.swiftmailer_smtp_encryption;
			vm.siteName = resolve.siteName.data[0];
			vm.siteMail = resolve.siteMail.data[0];

			if (_.isEqual(vm.smtpEncryption, 'ssl')) {
				vm.smtpEncryptionSettings.ssl = true;
			} else if (_.isEqual(vm.smtpEncryption, 'tls')){
				vm.smtpEncryptionSettings.tls = true;
			}
		});
	}

	getSTMPSettings();

	vm.testSMTP = function () {
		let smtpTest = {
			outgoing: {
				active: true,
				ssl: vm.smtpEncryptionSettings.ssl,
				tls: vm.smtpEncryptionSettings.tls,
				server: vm.smtpHost,
				port: vm.smtpPort
			},
			name: vm.smtpUser,
			password: vm.smtpPass
		};
		let test = SmtpService.testSmtp(smtpTest);

		$q.all(test).then(function (resolve) {
			if (resolve.answer.data[0] == SUCCESS_SMTP_TEST_ANSWER) {
				SweetAlert.swal('Connection established', 'Success', 'success');
			} else {
				SweetAlert.swal('Error', resolve.answer.data[0], 'error');
			}
		});

		vm.updateSMTP();
	};

	vm.changeConnection = function (type) {
		if (_.isEqual(type, 'ssl')) {
			vm.smtpEncryptionSettings.tls = false;
			vm.smtpEncryption = vm.smtpEncryptionSettings.ssl ? 'ssl' : '';
		} else {
			vm.smtpEncryptionSettings.ssl = false;
			vm.smtpEncryption = vm.smtpEncryptionSettings.tls ? 'tls' : '';
		}
	};

	vm.updateSMTP = function () {
		let newSMTPSettings = SmtpService.setSMTPSettings(vm.smtpHost, vm.smtpPort, vm.smtpEncryption, vm.smtpUser, vm.smtpPass, vm.siteName, vm.siteMail);
		vm.saving = true;

		$q.all(newSMTPSettings)
			.then(function () {
				vm.saving = false;
				showSaved();
			});
	};

	function showSaved() {
		vm.saved = true;

		common.$timeout(function () {
			vm.saved = false;
		}, HIDE_SAVED_TIMEOUT);
	}

	init();
}
