'use strict';

angular
	.module('app.settings')
	.directive('extraService', extraService);

extraService.$inject = ['$uibModal', 'SettingServices'];

function extraService ($uibModal, SettingServices) {
	const UPDATE_EXTRA_SERVICE_ACTION = 'reload_extra_service_list';

	return {
		link: linkFn,
		restrict: "A"
	};

	function linkFn(scope) {
		scope.addService = function(data, index) {
			$uibModal.open({
				template: require('../templates/extraSettingsModal.html'),
				controller: ModalInstanceCtrl,
				resolve: {
					editData: function () {
						return data;
					},
					currentEl: function () {
						return index;
					}
				},
				size: 'md'
			});
		};

		/* @ngInject */
		function ModalInstanceCtrl($scope, $rootScope, $uibModalInstance, SweetAlert, editData, currentEl) {
			$scope.request = {};

			if(editData) {
				$scope.request.name = editData.name;
				if(editData.extra_services && editData.extra_services[0]) {
					$scope.request.parameterName1 = editData.extra_services[0].services_name;
					$scope.request.type1 = editData.extra_services[0].services_type;
					$scope.request.readOnly1 = editData.extra_services[0].services_read_only;
					$scope.request.defaultValue1 = editData.extra_services[0].services_default_value;
				}
				if(editData.extra_services && editData.extra_services[1]) {
					$scope.request.enable2 = true;
					$scope.request.parameterName2 = editData.extra_services[1].services_name;
					$scope.request.type2 = editData.extra_services[1].services_type;
					$scope.request.readOnly2 = editData.extra_services[1].services_read_only;
					$scope.request.defaultValue2 = editData.extra_services[1].services_default_value;
				}
				if(editData.extra_services && editData.extra_services[2]) {
					$scope.request.enable3 = true;
					$scope.request.parameterName3 = editData.extra_services[2].services_name;
					$scope.request.type3 = editData.extra_services[2].services_type;
					$scope.request.readOnly3 = editData.extra_services[2].services_read_only;
					$scope.request.defaultValue3 = editData.extra_services[2].services_default_value;
				}
			}

			$scope.serviceType = ['Amount'];

			if(editData) {
				$scope.editService = true;
			}

			$scope.create = function () {
				var dataToSend = {
					name: $scope.request.name,
					alias: generateAlias($scope.request.name),
					extra_services: [
						{
							services_name: $scope.request.parameterName1,
							services_type: $scope.request.type1,
							services_read_only: !!$scope.request.readOnly1,
							services_default_value: $scope.request.defaultValue1
						}
					]
				};

				if ($scope.request.enable2) {
					dataToSend.extra_services.push(
						{
							services_name: $scope.request.parameterName2,
							services_type: $scope.request.type2,
							services_read_only: !!$scope.request.readOnly2,
							services_default_value: $scope.request.defaultValue2
						});
				}

				if ($scope.request.enable3) {
					dataToSend.extra_services.push(
						{
							services_name: $scope.request.parameterName3,
							services_type: $scope.request.type3,
							services_read_only: !!$scope.request.readOnly3,
							services_default_value: $scope.request.defaultValue3
						});
				}

				let sweetAlertTitle = editData ? "Update Service ?" : "Add Service ?";
				SweetAlert.swal({
					title: sweetAlertTitle,
					text: "",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#64C564",
					confirmButtonText: "Confirm",
					closeOnConfirm: true
				}, (isConfirm) => {
					if (isConfirm) {
						$scope.busy = true;

						SettingServices.saveExtraSettings(dataToSend, 'additional_settings', currentEl)
							.then(() => {
								$rootScope.$broadcast(UPDATE_EXTRA_SERVICE_ACTION);

								$scope.busy = false;
								$scope.cancel();
							}, function () {
								$scope.busy = false;
							});
					}
				});
			};

			$scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};

			$scope.isValid = function () {
				return $scope.request.name;
			};

			function generateAlias(name) {
				let alias = name;

				if (name.length) {
					alias = alias.toLowerCase().replace(/\s/g, '_');
				}

				return alias;
			}
		}
	}
}
