import './updater-styles.styl';

(function () {
    'use strict';
    angular
        .module('app.settings')
        .directive('updater', updater);
    updater.$inject = ['datacontext', 'SettingServices', 'common'];


    function updater( datacontext, SettingServices, common) {
        var directive = {
            link: link,
            scope: {},
            template: require('./updater.template.pug'),
            restrict: "E"
        };

        return directive;


        function link(scope) {
            scope.fieldData = datacontext.getFieldData();
            scope.updaterSettings = angular.fromJson(scope.fieldData.updater_settings);

            scope.updaterToken = scope.updaterSettings.updater.token;
            scope.updaterIsOn = scope.updaterSettings.updater.isOn;

            scope.saveUpdaterSettings = function () {
                scope.updaterSettings.updater.token =  scope.updaterToken;
                scope.updaterSettings.updater.isOn =   scope.updaterIsOn;
                scope.saveSettings();
            };

            scope.saveSettings = function () {
                var promise = SettingServices.saveSettings(scope.updaterSettings, 'updater_settings');
                promise.then(function () {
                    toastr.success('Settings was updated');
                });
            };

        }

    }
})();




