'use strict';

angular
	.module('app.settings')
	.directive('packingService', packingService);

packingService.$inject = ['$uibModal', 'SettingServices'];

function packingService($uibModal, SettingServices) {
	const UPDATE_PACKING_ACTION = 'reload_packing_settings_list';

	return {
		link: linkFn,
		restrict: "A"
	};

	function linkFn(scope) {
		scope.addService = function (data, index) {
			$uibModal.open({
				template: require('../templates/packingModal.html'),
				controller: ModalInstanceCtrl,
				resolve: {
					editData: function () {
						return data;
					},
					currentEl: function () {
						return index;
					}
				},
				size: 'md'
			});
		};

		/* @ngInject */
		function ModalInstanceCtrl($scope, $rootScope, $uibModalInstance, SweetAlert, editData, currentEl) {
			$scope.request = editData || {};
			$scope.editService = !!editData;

			if (_.isString($scope.request.rate)) {
				$scope.request.rate = +$scope.request.rate;
			}

			if (_.isString($scope.request.laborRate)) {
				$scope.request.laborRate = +$scope.request.laborRate;
			}

			if (_.isString($scope.request.LDRate)) {
				$scope.request.LDRate = +$scope.request.LDRate;
			}

			//$scope.serviceType = ['Name', 'Rate', 'Show in Contract page' ];

			$scope.create = function () {
				if (!$scope.request.name || $scope.request.rate < 0 || $scope.request.laborRate < 0 || $scope.request.LDRate < 0) {
					SweetAlert.swal("Failed!", "Please check all fields", "error");
					return false;
				}

				var dataToSend = {
					name: $scope.request.name,
					rate: $scope.request.rate,
					laborRate: $scope.request.laborRate,
					LDRate: $scope.request.LDRate,
					showInContract: $scope.request.showInContract
				};
				var sweetAlertTitle = editData ? "Update Packing ?" : "Add Packing ?";
				SweetAlert.swal({
					title: sweetAlertTitle,
					text: "",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#64C564",
					confirmButtonText: "Confirm",
					closeOnConfirm: true
				}, function (isConfirm) {
					if (isConfirm) {
						$scope.busy = true;
						SettingServices.saveExtraSettings(dataToSend, 'packing_settings', currentEl)
							.then(function (data) {
								$scope.busy = false;
								$rootScope.$broadcast(UPDATE_PACKING_ACTION);
								$scope.cancel();
							}, function (reason) {
								$scope.busy = false;
							});
					}
				});
			};

			$scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};

			$scope.isValid = function () {
				return $scope.request.name;
			};
		}
	}
}
