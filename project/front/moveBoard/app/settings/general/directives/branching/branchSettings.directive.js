(function () {
    'use strict';

    angular
        .module('app.settings')
        .directive('branchSettingsDirective', branchSettingsDirective);

    branchSettingsDirective.$inject = ['PermissionsServices', 'datacontext', 'SettingServices', 'common', 'BranchAPIService'];

    function branchSettingsDirective(PermissionsServices, datacontext, SettingServices, common, BranchAPIService) {
        var CLOSEST_BRANCH_SETTINGS_NAME = 'settingsclosestbranch';
        var CURRENT_BRANCH_SETTINGS_NAME = 'current_branch';
        var BASE_BRANCH = {
            "name": '',
            "api_url": '',
            "logo": '',
            "parking_zip": '',
            "based_state": '',
            "front_url": '' // move board url
        };

        var directive = {
            link: link,
            controller: controller,
            scope: true,
	        template: require('./branchSettings.template.html'),
            restrict: "E"
        };

        /* @ngInject */
        function controller($scope) {
	        $scope.isOpenAddNewBranch = true;
        }

        return directive;

        function link($scope, element, attrs) {
            var fieldData = datacontext.getFieldData();

            $scope.busy = false;
            $scope.saving = false;
            $scope.saved = false;
            $scope.isOpenAddNewBranch = true;
            $scope.isSuperAdministrator = PermissionsServices.isSuper();
            $scope.isAdministrator = PermissionsServices.hasPermission('administrator');
            $scope.settingsclosestbranch = Number(fieldData[CLOSEST_BRANCH_SETTINGS_NAME]).toString();
            $scope.currentBranch = Number(fieldData[CURRENT_BRANCH_SETTINGS_NAME] || 0).toString();
            $scope.urlValidation = '^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}$';
            $scope.branches = fieldData.branches;
            var baseBranches = angular.copy(fieldData.branches);
            initNewBranch();

            checkIsEmptyBranch();

            $scope.createBranch = createBranch;
            $scope.saveBranchClosestSettings = saveBranchClosestSettings;
            $scope.removeBranch = removeBranch;
            $scope.updateBranch = updateBranch;
            $scope.synchronizeBranches = synchronizeBranches;
            $scope.selectCurrentBranch = selectCurrentBranch;

            function createBranch() {
                $scope.busy = true;

                BranchAPIService
                    .createBranch($scope.newBranch)
                    .then(function (response) {
                        $scope.newBranch.id = response.id;
                        $scope.newBranch.parking_zip = response.parking_zip || "";
                        $scope.newBranch.based_state = response.based_state || "";
                        $scope.branches.push($scope.newBranch);

                        checkIsEmptyBranch();
                        updateBaseBranches();
                        initNewBranch();
                    }, function (error) {
                        toastr.error("Error. Cannot create branch.");
                    })
                    .finally(function () {
                        $scope.busy = false;
                    });
            }

            function initNewBranch() {
                $scope.newBranch = angular.copy(BASE_BRANCH);
            }

            function removeBranch(branch) {
                $scope.busy = true;

                BranchAPIService
                    .removeBranch(branch)
                    .then(function () {
                        var index = _.findIndex($scope.branches, {id: branch.id});

                        if (index >= 0) {
                            $scope.branches.splice(index, 1);
                            updateBaseBranches();
                            checkIsEmptyBranch();
                            toastr.success("Branch was removed", "Success");
                        }
                    }, function () {
                        toastr.error("Error. Cannot remove branch.");
                    })
                    .finally(function () {
                        $scope.busy = false;
                    });
            }

            function updateBranch(branch, isApi) {
                var baseBranch = _.find(baseBranches, {id: branch.id});

                if (!_.isEqual(baseBranch, branch)) {
                    $scope.saving = true;

                    BranchAPIService
                        .updateBranch(branch)
                        .then(function () {
                            $scope.saving = false;
                            updateBaseBranches();
                            showSaved();
                        }, function () {
                            toastr.error('Error with update branch');
                        });
                }
            }

            function saveBranchClosestSettings() {
                $scope.saving = true;

                SettingServices
                    .saveSettings($scope.settingsclosestbranch, CLOSEST_BRANCH_SETTINGS_NAME)
                    .then(function () {
                        $scope.saving = false;

                        showSaved();
                    });
            }

            function showSaved() {
                common.$timeout(function () {
                    $scope.saved = false;
                }, 1000);

                $scope.saved = true;
            }

            function checkIsEmptyBranch() {
                $scope.isEmptyBranches = _.isEmpty($scope.branches);
                $scope.isActiveBranching = $scope.branches.length >= 2;
            }

            function synchronizeBranches() {
                $scope.busy = true;

                BranchAPIService
                    .synchronizeBranches()
                    .then(function () {
                        toastr.success('Branches were successfully synchronized');
                    }, function () {
                        toastr.error('Error. Problem with synchronize branches');
                    })
                    .finally(function () {
                        $scope.busy = false;
                    });
            }

            function selectCurrentBranch(id) {
                $scope.currentBranch = id;
                $scope.saving = true;

                SettingServices
                    .saveSettings(id, CURRENT_BRANCH_SETTINGS_NAME)
                    .then(function () {
                        $scope.saving = false;

                        showSaved();
                    });
            }

            function updateBaseBranches() {
                baseBranches = angular.copy(fieldData.branches);
            }
        }

    }
})();
