'use strict';

import './calendar.styl';

angular
	.module('app.calculator')
	.directive('calendar', calendar);

calendar.$inject = ['CalendarServices', 'SweetAlert', 'datacontext'];

function calendar(CalendarServices, SweetAlert, datacontext) {
	return {
		template: require('./calendar.pug'),
		scope: {
			month: "=",
			year: "=",
			calendar: "=",
			types: "="
		},
		link: link,
		restrict: "E"
	};

	function link(scope, element, attrs) {
		var admin = false;
		var clicked_date = '';

		if (angular.isDefined(attrs.admin)) {
			admin = true;
		}
		
		function _init() {
			var month = scope.year + '-' + scope.month + '-01';
			scope.month_number = scope.month;
			scope.month = moment(month);
			scope.rate_types = scope.types;
			scope.current_popup = '';
			scope.current_day = '';
			
			var start = scope.month.clone();
			
			start.date(1);
			_removeTime(start.day(0));
			
			_buildMonth(scope, start, scope.month);
		}
		
		_init();

		scope.select = function (day) {
			scope.selected = day.date;
		};

		scope.showPopup = function (obj, $event) {
			if (admin) {
				element.find('.type_popup').show();
				scope.current_popup = element.find('.type_popup');
				scope.current_day = $($event.target);

				var clicked_day = moment(obj.date).format("D");
				clicked_date = moment(obj.date).format("YYYY-MM-DD");
				$(".modal-backdrop-white").show();
				var type = scope.rate_types[scope.current_day.val()];
				scope.current_popup.find('.clicked_day').text(clicked_day).addClass(type);

				var position = $($event.target).position();

				element.find('.type_popup').css({
					"left": (position.left - 40) + 'px',
					"top": (position.top) + 'px',
					"position": 'absolute'
				});
				element.find('.type_popup').css({'z-index': 1500});
			}
		};


		scope.updateType = function (type) {
			clearTypes();
			CalendarServices.setType(clicked_date, type)
				.then(() => {
					scope.current_day.addClass(scope.rate_types[type]);
					scope.current_day.val(type);
					scope.calendar[scope.year][clicked_date] = type;
				}, () => {
					SweetAlert.swal('Calendar was not updated', 'Try again');
				})
				.finally(() => {
					scope.current_popup.hide();
					angular.element(".modal-backdrop-white").addClass("animated fadeout");
					angular.element(".modal-backdrop-white").hide();
					datacontext.saveFieldDataType('calendar', scope.calendar);
				});
		};


		function clearTypes() {
			angular.forEach(scope.rate_types, function (val, id) {
				scope.current_day.removeClass(val);

			});

			scope.current_day.addClass('day');
		}


		$(document).mouseup(function (e) {
			var container = scope.current_popup;
			var target = e.target;

			if (container.length) {
				if (!container.is(e.target) // if the target of the click isn't the container...
					&& container.has(e.target).length === 0) // ... nor a descendant of the container
				{

					container.hide();
					$(".modal-backdrop-white").addClass("animated fadeout");
					$(".modal-backdrop-white").hide();
				}
			}
		});


		function _removeTime(date) {
			return date.day(0).hour(0).minute(0).second(0).millisecond(0);
		}


		function _buildMonth(scope, start, month) {
			scope.weeks = [];
			var done = false, date = start.clone(), monthIndex = date.month(), count = 0;

			while (!done) {
				scope.weeks.push({days: _buildWeek(date.clone(), month)});
				date.add(1, "w");
				done = count++ > 2 && monthIndex !== date.month();
				monthIndex = date.month();
			}
		}


		function _buildWeek(date, month) {
			var days = [];

			for (var i = 0; i < 7; i++) {
				days.push({
					name: date.format("dd").substring(0, 1),
					number: date.date(),
					isCurrentMonth: date.month() === month.month(),
					type: CalendarServices.getType(date.format("YYYY-MM-DD"), scope.calendar),
					isToday: date.isSame(new Date(), "day"),
					date: date
				});
				date = date.clone();
				date.add(1, "d");
			}

			return days;
		}
	}

}
