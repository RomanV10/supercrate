import './extra-service-settings.styl';

'use strict';

angular
	.module('app.settings')
	.directive('extraServiceSettings', extraServiceSettings);

extraServiceSettings.$inject = ['SettingServices', 'common', 'SweetAlert'];

function extraServiceSettings(SettingServices, common, SweetAlert) {
	const HIDE_SAVED_TIMEOUT = 1000;
	const UPDATE_EXTRA_SERVICE_ACTION = 'reload_extra_service_list';
	const DRAGULA_DROP_ACTION = 'extra-service-bag.drop-model';
	const ADDITONAL_SETTINGS_NAME = 'additional_settings';

	controller.$inject = ['dragulaService'];

	return {
		scope: {},
		template: require('./extra-service-settings.template.html'),
		restrict: 'E',
		link: linkFunction,
		controller: controller,
	};

	function linkFunction($scope) {
		let dataToDelete;

		$scope.busy = false;
		$scope.activeRow = -1;

		$scope.$on(DRAGULA_DROP_ACTION, onDropExtraServices);
		$scope.$on(UPDATE_EXTRA_SERVICE_ACTION, onUpdateExtraServices);
		
		loadAdditionalSettings();

		$scope.removeService = removeService;
		$scope.prepareToDelete = prepareToDelete;

		function loadAdditionalSettings () {
			SettingServices.getSettings(ADDITONAL_SETTINGS_NAME)
				.then((response) => {
					$scope.busy = false;

					$scope.additionalSettings = angular.fromJson(response[0]);
				});
		}
		
		function prepareToDelete(data) {
			$scope.activeRow = data;
			dataToDelete = data;
		}

		function removeService(setting_type = ADDITONAL_SETTINGS_NAME) {
			SweetAlert.swal({
				title: 'Delete Service ?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#E47059',
				confirmButtonText: 'Confirm',
				closeOnConfirm: true
			}, (isConfirm) => {
				if (isConfirm) {
					$scope.busy = true;

					$scope.saving = true;
					SettingServices.deleteSettings(setting_type, dataToDelete)
						.then(() => {
							$scope.activeRow = -1;
							loadAdditionalSettings();
							showSaved();
						});
				}
			});
		}

		function onDropExtraServices() {
			$scope.saving = true;

			SettingServices.saveSettings($scope.additionalSettings, ADDITONAL_SETTINGS_NAME)
				.then(function () {
					showSaved();
				});
		}

		function onUpdateExtraServices() {
			loadAdditionalSettings();
			showSaved();
		}

		function showSaved() {
			$scope.saving = false;
			$scope.saved = true;

			common.$timeout(function () {
				$scope.saved = false;
			}, HIDE_SAVED_TIMEOUT);
		}
	}

	function controller(dragulaService) {
		let vm = this;

		dragulaService.options(vm, 'extra-service-bag', {
			moves: (el, container, handle) => handle.className === 'dragdrop-button',
			direction: 'vertical',
		});
	}
}
