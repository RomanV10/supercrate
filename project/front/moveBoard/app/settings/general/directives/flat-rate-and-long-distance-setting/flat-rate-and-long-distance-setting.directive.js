'use strict';

angular
	.module('app.settings')
	.directive('flatRateAndLongDistanceSetting', flatRateAndLongDistanceSetting);

flatRateAndLongDistanceSetting.$inject = [];

function flatRateAndLongDistanceSetting() {
	return {
		restrict: 'E',
		template: require('./flat-rate-and-long-distance-setting.tpl.html'),
		scope: {
			basicSettings: '='
		},
		link: flatRateAndLongDistanceSettingLink,
		controller: 'GeneralController',
		controllerAs: 'vm',
	};

	function flatRateAndLongDistanceSettingLink($scope) {
		$scope.$watchGroup(['basicSettings.flat_rate_miles', 'basicSettings.long_distance_miles'], (current) => {
			if (+current[0] >= +current[1]) {
				$scope.lessThan = current[1];
			} else {
				$scope.lessThan = current[0];
			}
		});
	}
}
