describe('Google integration settings test directive', function () {
	let $compile;
	let	$rootScope;
	let $httpBackend;
	let moveBoardApi;
	let $scope;
	let testHelper;
	let element;

	beforeEach(inject(function(_$httpBackend_, _$compile_, _$rootScope_, _moveBoardApi_, _testHelperService_) {
		$httpBackend = _$httpBackend_;
		$compile = _$compile_;
		$rootScope = _$rootScope_;
		moveBoardApi = _moveBoardApi_;
		testHelper = _testHelperService_;
		$scope = $rootScope.$new();
		let settings =testHelper.loadJsonFile('google-calendar-settings-init-response.mock');

		$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.googleCalendarIntegration.getGlobalStatus)).respond(200, settings.initResult);

		element = angular.element("<er-google-calendar-integration></er-google-calendar-integration>");
		$compile(element)($scope);
	}));

	it('initialize settings directive', function () {
		let elementScope = element.scope();

		spyOn(elementScope, '$apply');

		elementScope.$digest();
		elementScope.$apply();

		expect(elementScope.$apply).toHaveBeenCalled();
		expect(elementScope.busy).toEqual(true);
		expect(elementScope.isEnabled).toEqual(false);
	});

	it('save setting', function () {
		let elementScope = element.scope();
		elementScope.$digest();
		elementScope.$apply();

		spyOn(elementScope, 'saveSettings');

		element.find('.switchery').trigger('click');

		expect(elementScope.saveSettings).toHaveBeenCalled();
		expect(elementScope.isEnabled).toBeTruthy();
		expect(elementScope.busy).toBeTruthy();
	});
});
