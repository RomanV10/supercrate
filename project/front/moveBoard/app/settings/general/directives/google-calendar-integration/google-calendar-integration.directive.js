'use strict';

import './google-calendar-integration.styl'

angular
	.module('app.settings')
	.directive('erGoogleCalendarIntegration', erGoogleCalendarIntegration);

erGoogleCalendarIntegration.$inject = ['moveBoardApi', 'apiService', 'SweetAlert'];

function erGoogleCalendarIntegration(moveBoardApi, apiService, SweetAlert) {
	let directive = {
		link: link,
		scope: true,
		template: require('./google-calendar-integration.pug'),
		restrict: "E"
	};

	return directive;

	function link($scope, element, attrs) {
		$scope.busy = true;
		$scope.isEnabled = false;

		$scope.saveSettings = saveSettings;

		loadSettings();

		function loadSettings() {
			apiService
				.postData(moveBoardApi.googleCalendarIntegration.getGlobalStatus)
				.then(onGetGoogleCalendarStatus, onErrorResponse);
		}

		function onGetGoogleCalendarStatus(response) {
			$scope.busy = false;
			$scope.isEnabled = response.data.result.is_enabled;
		}

		function onErrorResponse() {
			$scope.busy = false;
		}

		function saveSettings() {
			$scope.busy = true;

			if (!$scope.isEnabled) {
				SweetAlert.swal({
						title: "Are you sure you want to disable google calendar?",
						text: "All users calendars will be deleted",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55", confirmButtonText: "Yes",
						cancelButtonText: "No",
						closeOnConfirm: true,
						closeOnCancel: true
					},
					function (isConfirm) {
						if (isConfirm) {
							updateSettings();
						} else {
							$scope.isEnabled = true;
							$scope.busy = false;
						}
					});
			} else {
				updateSettings();
			}

		}

		function updateSettings() {
			let data = {"is_enabled": $scope.isEnabled};

			apiService
				.postData(moveBoardApi.googleCalendarIntegration.setGlobalStatus, data)
				.then(onSuccessSaveSetting, onErrorResponse);
		}

		function onSuccessSaveSetting() {
			$scope.busy = false;

			let text = $scope.isEnabled ? 'enabled': 'disabled';
			toastr.success('Success', `Google calendar settings is ${text}`);
		}
	}

}