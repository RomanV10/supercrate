import './arrivy-settings.styl';

'use strict';

angular
	.module('app.settings')
	.directive('arrivySettings', arrivySettings);

arrivySettings.$inject = ['apiService', 'moveBoardApi'];

function arrivySettings(apiService, moveBoardApi) {
	return {
		scope: {},
		template: require('./arrivy-settings.template.pug'),
		restrict: "E",
		link: linkFunction,
	};

	function linkFunction($scope) {
		$scope.busy = false;

		$scope.save = save;
		$scope.connect = connect;
		$scope.disconnect = disconnect;

		$scope.arrivy = {};

		function init() {
			$scope.busy = true;
			apiService.postData(moveBoardApi.arrivy.getKeys)
				.then(function(res){
					$scope.busy = false;
					$scope.arrivy = res.data;
				});
		}

		function save() {
			$scope.busy = true;
			var settings = {
				auth_key: $scope.arrivy.arrivy_authorization_key,
				auth_token: $scope.arrivy.arrivy_authorization_token,
				sms_notification: $scope.arrivy.arrivy_sms_notification,
				email_notification: $scope.arrivy.arrivy_email_notification
			};
			apiService.postData(moveBoardApi.arrivy.saveKeys, settings)
				.then(function(res){
					$scope.busy = false;
					toastr.success(res.data[0]);
				});
		}

		function connect() {
			$scope.busy = true;
			var settings = {
				auth_key: $scope.arrivy.arrivy_authorization_key,
				auth_token: $scope.arrivy.arrivy_authorization_token
			};
			apiService.postData(moveBoardApi.arrivy.connect, settings)
				.then(function(res){
					$scope.busy = false;
					if(res.data.connection_status){
						$scope.arrivy.arrivy_connection_status = res.data.connection_status;
						toastr.success(res.data.message);
						save();
					} else {
						toastr.error(res.data.message, { fadeAway: 10000 });
					}
				});
		}

		function disconnect() {
			$scope.busy = true;
			apiService.postData(moveBoardApi.arrivy.disconnect)
				.then(function(res){
					$scope.busy = false;
					if(!res.data.connection_status){
						$scope.arrivy.arrivy_connection_status = res.data.connection_status;
						toastr.success(res.data.message);
						$scope.arrivy.arrivy_sms_notification = false;
						$scope.arrivy.arrivy_email_notification = false;
					}
				});
		}
		init();
	}
}
