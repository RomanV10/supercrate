describe('Directive: Arrivy Settings', function () {
	let $httpBackend,
		moveBoardApi,
		testHelper,
		elementScope,
		$rootScope,
		$compile,
		$scope,
		element;

	function compileElement(elementHtml) {
		let elem = angular.element(elementHtml);
		element = $compile(elem)($scope);
		elementScope = element.isolateScope();

	}

	beforeEach(inject(function (_$compile_, _$rootScope_, _$httpBackend_, _testHelperService_, _moveBoardApi_) {
		testHelper = _testHelperService_;
		$rootScope = _$rootScope_;
		$scope = _$rootScope_.$new();
		$compile = _$compile_;
		$httpBackend = _$httpBackend_;
		moveBoardApi = _moveBoardApi_;
		let arrivyResponse = testHelper.loadJsonFile('arrivy-disconnecting.mock');
		$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.arrivy.getKeys)).respond(200, arrivyResponse);
		compileElement(`<arrivy-settings></arrivy-settings>`);

	}));

	describe('When init', () => {
		it('should not render empty html', function () {
			expect(element.html()).not.toEqual('');
		});
	});

	describe('When arrivy', () => {
		it('should connect', () => {
			let res = {
				connection_status: true,
				message: ''
			};
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.arrivy.connect)).respond(200, res);
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.arrivy.saveKeys)).respond(200, ['saved']);
			elementScope.connect();
			$httpBackend.flush();
			expect(elementScope.arrivy.arrivy_connection_status).toBeTruthy();
		});

		it('should disconnect', () => {
			elementScope.arrivy.arrivy_connection_status = true;
			let res = {
				connection_status: true,
				message: ""
			};
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.arrivy.disconnect)).respond(200, res);
			elementScope.disconnect();
			$httpBackend.flush();
			expect(elementScope.arrivy.arrivy_connection_status).not.toBeTruthy();
		});
	});
});
