'use strict';

import './dispatch-setting.styl';

angular
	.module('app.settings')
	.directive('dispatchSetting', dispatchSetting);

/*@ngInject*/
function dispatchSetting(datacontext, SettingServices, DISPATCH_INITIAL_SETTINGS) {
	return {
		restrict: 'E',
		scope: {},
		template: require('./dispatch-setting.tpl.html'),
		link: dispatchCrewSettingLink
	};

	function dispatchCrewSettingLink($scope) {
		const SETTING_NAME = 'localDispatchSettings';
		let localDispatchSettings = _.get(datacontext.getFieldData(), SETTING_NAME);

		$scope.changeCrewAvailableState = changeCrewAvailableState;

		function init() {
			$scope.isCrewAlwaysAvailable = _.get(localDispatchSettings, 'isCrewAlwaysAvailable', DISPATCH_INITIAL_SETTINGS.isCrewAlwaysAvailable);
			$scope.busy = false;
		}

		function changeCrewAvailableState() {
			_.set(localDispatchSettings, 'isCrewAlwaysAvailable', $scope.isCrewAlwaysAvailable);
			saveDispatchSettings();
		}

		function saveDispatchSettings() {
			$scope.busy = true;
			SettingServices.saveSettings(localDispatchSettings, SETTING_NAME)
				.then(() => toastr.success('Local Dispatch Settings were successfully saved'))
				.catch(() => toastr.error('Local Dispatch Settings were not saved', 'Try again'))
				.finally(() => $scope.busy = false);
		}

		init();
	}
}
