describe('dispatch-setting.js', () => {
	let rootScope,
		element,
		$scope,
		datacontext,
		SettingServices,
		DISPATCH_INITIAL_SETTINGS;
	beforeEach(inject((_datacontext_, _SettingServices_, _DISPATCH_INITIAL_SETTINGS_) => {
		datacontext = _datacontext_;
		SettingServices = _SettingServices_;
		DISPATCH_INITIAL_SETTINGS = _DISPATCH_INITIAL_SETTINGS_;
		rootScope = $rootScope.$new();
		let directive = '<dispatch-setting></dispatch-setting>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	}));

	describe('on init', () => {
		it('$scope.changeCrewAvailableState have to be defined', () => {
			expect($scope.changeCrewAvailableState).toBeDefined();
		});

		it('$scope.isCrewAlwaysAvailable have to be defined', () => {
			expect($scope.isCrewAlwaysAvailable).toBeDefined();
		});

		it('$scope.isCrewAlwaysAvailable should be FALSY', () => {
			expect($scope.isCrewAlwaysAvailable).toBeFalsy();
		});

		it('$scope.busy should be FALSY', () => {
			expect($scope.busy).toBeFalsy();
		});
	});

	describe('on change', () => {
		it('$scope.changeCrewAvailableState have to be triggered', () => {
			// given
			let saveSettingsSpy = spyOn(SettingServices, 'saveSettings').and.callThrough();
			// when
			$scope.changeCrewAvailableState();
			// then
			expect(saveSettingsSpy).toHaveBeenCalled();
		});

		it('$scope.busy have to be TRUTHLY', () => {
			// when
			$scope.changeCrewAvailableState();
			// then
			expect($scope.busy).toBeTruthy();
		});
	});
});
