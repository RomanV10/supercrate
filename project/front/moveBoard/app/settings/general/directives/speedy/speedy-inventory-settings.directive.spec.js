describe('Directive: app.settings.speedy-inventory-settings', () => {
	let element;
	let scope;
	let SettingServices;
	let isolateScope;
	let defaultSpeedySettings;
	let SweetAlert;
	let savedSetting;
	let Raven;
	let getVariableObject = {
		then(callback) {
			callback(defaultSpeedySettings);
			return this;
		},
		catch(callback) {
			callback();
			return this;
		},
		finally(callback) {
			callback();
		}
	};
	let getVariableFake = () => getVariableObject;
	let saveSettingsFake = (value) => {
		savedSetting = value;

		return {
			then(callback) {
				callback(defaultSpeedySettings);
				return this;
			},
			catch(callback) {
				callback();
				return this;
			},
			finally(callback) {
				callback();
			}
		};
	};

	function compileElement() {
		element = angular.element('<speedy-inventory-settings></speedy-inventory-settings>');
		defaultSpeedySettings = testHelper.loadJsonFile('speedy-inventory-settings.mock');

		$compile(element)(scope);
		scope.$apply();
		isolateScope = element.isolateScope();
	}

	beforeEach(inject((_SettingServices_, _SweetAlert_, _Raven_) => {
		SettingServices = _SettingServices_;
		SweetAlert = _SweetAlert_;
		Raven = _Raven_;

		scope = $rootScope.$new();

		spyOn(SettingServices, 'getVariable')
			.and
			.callFake(getVariableFake);

		spyOn(SweetAlert, 'swal')
			.and
			.callFake(() => {});

		compileElement();
	}));

	describe('when init', () => {
		it('should not render empty html', () => {
			expect(element.html()).not.toBe('');
		});

		it('should retrieve speedy settings', () => {
			let variableName = 'speedySettings';
			expect(SettingServices.getVariable).toHaveBeenCalledWith(variableName);
		});

		it('should busy be false', () => {
			//given
			let expectedResult = false;

			//when

			//then
			expect(isolateScope.busy).toBe(expectedResult);
		});

		it('should isNotChanged be true', () => {
			//given
			let expectedResult = true;

			//when

			//then
			expect(isolateScope.isNotChanged).toBe(expectedResult);
		});

		it('is enable to be boolean', () => {
			//given
			let expectedResult = true;

			//when

			//then
			expect(isolateScope.speedySettings.enable).toBe(expectedResult);
		});

		it('should show message when can\'t receive settings', () => {
			//given
			let expectedResult = ['Error', 'Cannot receive speedy inventory settings', 'error'];

			//when

			//then
			expect(SweetAlert.swal).toHaveBeenCalledWith(...expectedResult);
		});
	});

	it('when change setting isNotChanged to be false', () => {
		//given
		let expectedResult = false;

		//when
		isolateScope.speedySettings.enable = false;
		scope.$apply();

		//then
		expect(isolateScope.isNotChanged).toBe(expectedResult);
	});

	describe('when saveChanges', () => {
		beforeEach(() => {
			spyOn(SettingServices, 'saveSettings')
				.and
				.callFake(saveSettingsFake);

			spyOn(toastr, 'error')
				.and
				.callFake(() => {});

			spyOn(toastr, 'success')
				.and
				.callFake(() => {});

			isolateScope.saveChanges();
		});

		it('should call save settings', () => {
			expect(SettingServices.saveSettings).toHaveBeenCalled();
		});

		it('should set enable to number', () => {
			// given
			let expectedResult = 1;

			// when

			// then
			expect(savedSetting.enable).toBe(expectedResult);
		});

		it('should set isNotChanged to true', () => {
			//given
			let expectedResult = true;

			//when
			isolateScope.speedySettings.enable = false;
			scope.$apply();
			isolateScope.saveChanges();

			//then
			expect(isolateScope.isNotChanged).toBe(expectedResult);
		});

		it('should show message when error saving', () => {
			//given
			let expectedResult = ['Error', 'Cannot save speedy inventory settings'];

			//when

			//then
			expect(toastr.error).toHaveBeenCalledWith(...expectedResult);
		});

		it('should busy to be false', () => {
			//given
			let expectedResult = false;

			//when

			//then
			expect(isolateScope.busy).toBe(expectedResult);
		});

		it('should busy to be false', () => {
			//given
			let expectedResult = 'Speedy inventory settings was saved';

			//when

			//then
			expect(toastr.success).toHaveBeenCalledWith(expectedResult);
		});
	});

	describe('when not init settings', () => {
		beforeEach(() => {
			getVariableObject.then = (callback) => {
				callback(null);
				return getVariableObject;
			};
			getVariableObject.catch = () => getVariableObject;
			getVariableObject.finally = () => getVariableObject;

			spyOn(Raven, 'captureException')
				.and
				.callFake(() => {});

			compileElement();
		});

		it('should show alert message', () => {
			//given
			let expectedData = ['Please turn on speedy inventory module!', '', 'info'];

			//when

			//then
			expect(SweetAlert.swal).toHaveBeenCalledWith(...expectedData);
		});
	});
});
