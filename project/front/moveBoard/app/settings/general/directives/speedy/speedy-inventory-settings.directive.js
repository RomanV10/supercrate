'use strict';

import './speedy-inventory-settings.styl';

angular.module('app.settings')
	.directive('speedyInventorySettings', speedyInventorySettings);

speedyInventorySettings.$inject = ['SettingServices', 'SweetAlert', 'Raven'];

function speedyInventorySettings (SettingServices, SweetAlert, Raven) {
	return {
		restrict: 'E',
		scope: {},
		template: require('./speedy-inventory-settings.tmplate.html'),
		link: speedyInventoryLink
	};

	function speedyInventoryLink($scope) {
		const SPEEDY_SETTING_NAME = 'speedySettings';
		$scope.speedySettings = {};
		$scope.isNotChanged = true;
		let watcherDestructor;
		$scope.$on('$destroy', onDestroy);

		$scope.saveChanges = saveChanges;

		showBusy();

		SettingServices
			.getVariable(SPEEDY_SETTING_NAME)
			.then(parseSpeedySettings)
			.catch(errorCallback)
			.finally(hideBusy);

		function parseSpeedySettings(settins) {
			$scope.speedySettings = angular.fromJson(settins) || {};

			showEmptySettingsWarning();

			$scope.speedySettings.enable = !!$scope.speedySettings.enable;
			watcherDestructor = $scope.$watchCollection('speedySettings', onChangedSpeedy);
		}

		function showEmptySettingsWarning() {
			if (_.isEmpty($scope.speedySettings)) {
				SweetAlert.swal('Please turn on speedy inventory module!', '', 'info');
			}
		}

		function errorCallback() {
			SweetAlert.swal('Error', 'Cannot receive speedy inventory settings', 'error');
		}

		function showBusy() {
			$scope.busy = true;
		}
		
		function hideBusy() {
			$scope.busy = false;
		}

		function onChangedSpeedy(newValue, oldValue) {
			if (!_.isEqual(newValue, oldValue)) {
				$scope.isNotChanged = false;
			}
		}

		function saveChanges() {
			showBusy();
			let value = prepareSettingsForSave();
			SettingServices.saveSettings(value, SPEEDY_SETTING_NAME)
				.then(onSavedChanges)
				.catch(onErrorWhenSaveChanges)
				.finally(hideBusy);
		}
		
		function prepareSettingsForSave() {
			let settings = angular.copy($scope.speedySettings);
			settings.enable = Number(settings.enable);
			settings.returnSecureToken = 'TRUE';
			return settings;
		}

		function onSavedChanges() {
			$scope.isNotChanged = true;
			toastr.success('Speedy inventory settings was saved');
		}

		function onErrorWhenSaveChanges() {
			toastr.error('Error', 'Cannot save speedy inventory settings');
		}

		function onDestroy() {
			if (watcherDestructor) {
				watcherDestructor();
			}
		}
	}
}