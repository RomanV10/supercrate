'use strict';

import './contractSettings.styl';

angular
	.module('app.settings')
	.directive('contractSettings', contractSettings);

contractSettings.$inject = ['datacontext', 'SettingServices', 'SweetAlert', '$uibModal'];

function contractSettings(datacontext, SettingServices, SweetAlert, $uibModal) {
	return {
		link: linkFn,
		restrict: 'A'
	};

	function linkFn(scope, element, attrs) {
		const USE_PAYMENTS_PHOTO = [
			{
				name: 'Use Credit Card Photo and ID on Contract',
				value: true,
			},
			{
				name: 'Use Credit Card Photo and ID on Storage',
				value: true,
			}
		];
		scope.declarationValue = [];
		scope.busy = false;
		scope.settings = angular.fromJson((datacontext.getFieldData()).basicsettings);
		scope.contract_page = angular.fromJson((datacontext.getFieldData()).contract_page);
		scope.contract_page.skippingPhoto = _.get(scope.contract_page, 'skippingPhoto', USE_PAYMENTS_PHOTO);

		scope.toggle = toggle;
		scope.save = save;
		scope.canSubmit = canSubmit;
		scope.contractAgreeTextSettingModal = contractAgreeTextSettingModal;
		scope.confirmationPageTitlesModal = confirmationPageTitlesModal;
		scope.setAvailablePaymentType = setAvailablePaymentType;

		scope.newPayment = {
			name: '',
			selected: false,
			alias: 'custom'
		};
		scope.addNewPayment = addNewPayment;
		scope.removeCustomPayment = removeCustomPayment;
		scope.percentOfTotalForPickup = percentOfTotalForPickup;

		function addNewPayment() {
			if (scope.newPayment.name == '') {
				SweetAlert.swal('Set payment name', '', 'error');
				return false;
			}
			scope.newPaymentObj = false;
			scope.contract_page.paymentOptions.push(scope.newPayment);

			scope.contract_page.paymentTax[scope.newPayment.name] = {
				state: scope.newPayment.selected,
				value: 0
			};

			scope.newPayment = {
				name: '',
				selected: false,
				alias: 'custom'
			};

			save();
		}

		function removeCustomPayment(ind) {
			delete scope.contract_page.paymentTax[scope.contract_page.paymentOptions[ind].name];
			scope.contract_page.paymentOptions.splice(ind, 1);
		}

		function canSubmit() {
			var submit = false;
			if (scope.contract_page) { //&& !scope.isSubmitted
				submit = scope.contract_page.paymentTypes.some(function (item) {
					return item.selected == true;
				});
			}
			return submit;
		}

		function toggle(index) {
			//scope.isSubmitted = false;
			scope.contract_page.declarations[index].show = !scope.contract_page.declarations[index].show;
		}

		function save() {
			scope.busy = true;
			scope.vm.saveSettings(); //update vm.basicSettings.confirmation_page settings
			SettingServices.saveMoveBoardContractSettings(scope.contract_page).then(function (settings) {
				SettingServices.saveSettings(settings, 'contract_page')
					.then(function () {
						//scope.isSubmitted = true;
					})
					.finally(function () {
						toastr.success('Settings updated');
						scope.busy = false;
					});
			}, function (err) {
				scope.busy = false;
				toastr.error('Please reload page');
			});
		}

		function percentOfTotalForPickup() {
			scope.contract_page.pickUpPercent = scope.contract_page.pickUpPercent > 100 ? 0 : scope.contract_page.pickUpPercent;
		}

		function setAvailablePaymentType(name) {
			let turnOffPayment = _.find(scope.contract_page.paymentTypes, (type) => type.name === name && !type.selected);

			if (!turnOffPayment) {
				_.forEach(scope.contract_page.paymentTypes, (type) => {
					if (type.name !== name) {
						type.selected = false;
					}
				});
			}
		}

		function contractAgreeTextSettingModal() {
			$uibModal.open({
				template: require('../contract-agree-settings-modal/contract-agree-settings-modal.tpl.html'),
				controller: 'ContractAgreeSettings',
				resolve: {
					settings: () => {
						return scope.contract_page;
					}
				},
				size: 'lg'
			});
		}

		function confirmationPageTitlesModal() {
			$uibModal.open({
				template: require('../confirmation-titles-modal/confirmation-titles-modal.html'),
				controller: 'ConfirmationTitlesModal',
				resolve: {
					settings: () => {
						return scope.contract_page;
					}
				},
				size: 'lg',
				windowClass: 'confirmation-titles-modal'
			});
		}
	}
}
