'use strict';

angular
	.module('app.settings')
	.directive('erConnectToQuickbooks', connectToQuickbooks);

connectToQuickbooks.$inject = ['$window', 'config'];

function connectToQuickbooks($window, config) {
	return {
		restrict: 'E',
		template: "<ipp:connectToIntuit></ipp:connectToIntuit>",
		scope: {
			button: '='
		},
		link:connectToQB
	};

	function connectToQB(scope) {
		var script = $window.document.createElement("script");
		script.type = "text/javascript";
		script.src = "//js.appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js";

		script.onload = function () {
			$window.intuit.ipp.anywhere.setup({
				menuProxy: '',
				grantUrl: config.serverUrl + 'qbo/oauth?context=QBOContextGlobal'
			});
			scope.connected = true;
			scope.$apply();
		};

		$window.document.body.appendChild(script);
	}
}