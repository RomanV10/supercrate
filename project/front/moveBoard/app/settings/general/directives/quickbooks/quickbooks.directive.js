'use strict';

import './quickbooks.styl'

const CLOSED_REQUEST_SELECTED_WAY = "closed";
const ALL_TIME_SELECTED_WAY = "alltime";

angular
	.module('app.settings')
	.directive('quickBooks', quickBooks);

quickBooks.$inject = ['apiService', 'moveBoardApi', '$interval'];

function quickBooks(apiService, moveBoardApi, $interval) {
	return {
		restrict: 'E',
		template: require('./quickbooks.html'),
		scope: {},
		link: QB
	};

	function QB($scope) {
		let qbOff = {"qb_post_settings": "qboff"};
		let closedJob = {"qb_post_settings": "closed"};
		let paidJob = {"qb_post_settings": "alltime"};
		let connectInterval = false;
		$scope.companyId = {"compid": null};

		getTockenForDisconnect();
		getDateForReconnect();
		getCompanyID();

		if (!$scope.showDisconnectButton) {
			checkConnectToQuickBooks();
		}

		$scope.saveCompanyId = function () {
			apiService.postData(moveBoardApi.quickbooks.postCompanyId, $scope.companyId)
				.then(function () {
					toastr.success('Company id was saved', 'Success');
				})
		};

		function getCompanyID() {
			apiService.postData(moveBoardApi.quickbooks.getCompanyId)
				.then(onLoadCompanyId);
		}

		function onLoadCompanyId(data) {
			if (data != false) {
				$scope.companyId.compid = data.data[0];
			}

			$scope.postClosedJobs = data.data[1] == CLOSED_REQUEST_SELECTED_WAY;
			$scope.postPaidJobs = data.data[1] == ALL_TIME_SELECTED_WAY;
		}

		$scope.reconnect = function () {
			apiService.postData(moveBoardApi.quickbooks.qboReconnect)
				.then(function () {
					$scope.showReconnectButton = false;
					toastr.success('You were reconnected to QuickBooks', 'Success');
				});
		};

		function disableAllQBSettings() {
			apiService.postData(moveBoardApi.quickbooks.setQuickbooksSettings, qbOff);
		}

		$scope.disconnect = function () {
			apiService.postData(moveBoardApi.quickbooks.qboDisconnect)
				.then(function () {
					$scope.showDisconnectButton = false;
					checkConnectToQuickBooks();
					toastr.success('You were disconnected from QuickBooks', 'Success');
				});
		};

		function getDateForReconnect() {
			apiService.postData(moveBoardApi.quickbooks.checkAuthDateReconnectButton)
				.then(function (data) {
					$scope.showReconnectButton = _.head(data.data);
				});
		}

		function getTockenForDisconnect() {
			apiService.postData(moveBoardApi.quickbooks.checkTokenDisconnectButton)
				.then(function (data) {
					$scope.showDisconnectButton = _.head(data.data);
				});
		}

		function checkConnectToQuickBooks() {
			$interval.cancel(connectInterval);

			connectInterval = $interval(() => {
				apiService.postData(moveBoardApi.quickbooks.checkTokenDisconnectButton)
					.then(function (data) {
						$scope.showDisconnectButton = _.head(data.data);
						if ($scope.showDisconnectButton) {
							$interval.cancel(connectInterval);
						}
					});
			}, 3000);
		}

		$scope.autoPostClosedJobsToQuickbooks = function () {
			if ($scope.postClosedJobs) {
				apiService.postData(moveBoardApi.quickbooks.setQuickbooksSettings, closedJob)
					.then(function (data) {
						$scope.postClosedJobs = _.head(data.data);
					});
			} else {
				disableAllQBSettings();
			}
		};

		$scope.autoPostPaidJobsToQuickbooks = function () {
			if ($scope.postPaidJobs) {
				apiService.postData(moveBoardApi.quickbooks.setQuickbooksSettings, paidJob)
					.then(function (data) {
						$scope.postPaidJobs = _.head(data.data);
					});
			} else {
				disableAllQBSettings();
			}
		};

		$scope.$on('$destroy', function () {
			$interval.cancel(connectInterval);
		});

	}
}
