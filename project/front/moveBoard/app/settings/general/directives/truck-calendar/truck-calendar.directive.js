'use strict';

angular
	.module('app.calculator')
	.directive('trucksCalendar', trucksCalendar);

trucksCalendar.$inject = ['TrucksServices', '$rootScope'];

function trucksCalendar(TrucksServices, $rootScope) {

	/* @ngInject */
	function truckCalendarControler($scope) {
		$scope.$parent.currentTruckId = $scope.tid;
		$scope.$on('reloadCalender', function () {
			$scope.$apply();
		});
	}

	var directive = {
		link: link,
		scope: {
			month: "=",
			year: "=",
			unavailable: "=",
			blocked: "=",
			tid: "=",
			dates: "=",
			monthDates: "="
		},
		template: require('./truck-calendar.pug'),
		controller: truckCalendarControler,
		restrict: "E"
	};

	return directive;


	function link(scope) {
		var month = scope.year + '-' + scope.month + '-01';
		scope.month_number = scope.month;
		scope.month = moment(month);
		scope.rate_types = [];
		scope.current_popup = '';
		scope.current_day = '';
		var clicked_date = '';
		var start = scope.month.clone();
		start.date(1);
		_removeTime(start.day(0));
		_buildMonth(scope, start, scope.month);


		scope.setMonth = function (weeks, month, $event) {
			var currentMonth = month._d.getMonth();
			weeks.forEach(function (week) {
				week.days.forEach(function (day) {
					var dayMonth = day.date._d.getMonth();
					scope.current_day = $($event.target);
					var formatDate = day.date.format("YYYY-MM-DD");
					if (dayMonth == currentMonth) {
						if (scope.monthStatus) {
							scope.unavailable[formatDate] = 0;
							day.type = 0;
							scope.monthDates.remove.push(formatDate);
						} else {
							scope.unavailable[formatDate] = 1;
							day.type = 1;
							scope.monthDates.add.push(formatDate);
						}
					}
				})
			});
		};

		scope.createArray = function (day, $event) {
			var date = day.date.format("YYYY-MM-DD");
			scope.current_day = $($event.target);

			if (day.type) {
				var index = scope.dates.add.indexOf(date);

				if (index > -1) {
					scope.dates.add.splice(index, 1);
				}

				scope.dates.remove.push(date);
				scope.unavailable[date] = 0;
				day.type = 0;
			} else {
				var index = scope.dates.remove.indexOf(date);

				if (index > -1) {
					scope.dates.remove.splice(index, 1);
				}

				scope.dates.add.push(date);
				scope.unavailable[date] = 1;
				day.type = 1;
			}
		};

		scope.closeDay = function (day, $event) {
			var date = day.date.format("YYYY-MM-DD");
			TrucksServices.setUnvailability(scope.tid, date);
			scope.current_day = $($event.target);
			scope.unavailable[date] = 1;

			if (day.type) {
				scope.openDay(day, $event);

				return;
			}

			day.type = 1;
		};

		$rootScope.currentTruckId = scope.tid;

		scope.openDay = function (day, $event) {
			var date = day.date.format("YYYY-MM-DD");
			TrucksServices.removeUnvailability(scope.tid, date);
			scope.current_day = $($event.target);
			scope.unavailable[date] = 0;
			day.type = 0;
		};


		function _removeTime(date) {
			return date.day(0).hour(0).minute(0).second(0).millisecond(0);
		}

		function _buildMonth(scope, start, month) {
			scope.weeks = [];
			var done = false;
			var date = start.clone();
			var monthIndex = date.month();
			var count = 0;

			while (!done) {
				scope.weeks.push({days: _buildWeek(date.clone(), month)});
				date.add(1, "w");
				done = count++ > 2 && monthIndex !== date.month();
				monthIndex = date.month();
			}
		}


		function pushDates(array, item) {
			var index = array.indexOf(item);

			if (index == -1) {
				array.push(item)
			}
		}


		function setDayTypes(day, callback) {
			var dayDetails = day.date._d.getDay();
			var formattedDate = day.date.format('YYYY-MM-DD');
			pushDates($rootScope.dayTypes.allDays, formattedDate);

			if (dayDetails == 0) {
				pushDates($rootScope.dayTypes.sundays, formattedDate);
				pushDates($rootScope.dayTypes.weekends, formattedDate);
			}

			if ((dayDetails != 0) || (dayDetails != 6)) {
				pushDates($rootScope.dayTypes.withoutWeekends, formattedDate);
			}

			if (dayDetails == 6) {
				pushDates($rootScope.dayTypes.weekends, formattedDate);
			}

			var condition = (dayDetails == 1) || (dayDetails == 2) || (dayDetails == 3) || (dayDetails == 4) || (dayDetails == 5);

			if (condition) {
				pushDates($rootScope.dayTypes.weekDays, formattedDate);
			}

			if (dayDetails != 0) {
				pushDates($rootScope.dayTypes.withoutSundays, formattedDate);
			}

			callback(true);
		}

		function _buildWeek(date, month) {
			var days = [];

			for (var i = 0; i < 7; i++) {
				var dayObject = {
					name: date.format("dd").substring(0, 1),
					number: date.date(),
					isCurrentMonth: date.month() === month.month(),
					type: TrucksServices.getType(date.format("YYYY-MM-DD"), scope.unavailable),
					blocked: TrucksServices.getType(date.format("YYYY-MM-DD"), scope.blocked),
					isToday: date.isSame(new Date(), "day"),
					date: date
				};
				days.push(dayObject);

				setDayTypes(dayObject, function (success) {
					if (success) {
						date = date.clone();
						date.add(1, "d");
					}
				});
			}

			return days;
		}


	}
}