(function () {
	'use strict';

	angular
		.module('app.settings')
		.controller('TrucksSettingsContorller', TrucksContorller);

	TrucksContorller.$inject = ['$scope', 'datacontext', 'common', 'TrucksServices', 'logger', 'SweetAlert', '$rootScope', '$timeout', 'apiService', 'moveBoardApi'];

	function TrucksContorller($scope, datacontext, common, TrucksServices, logger, SweetAlert, $rootScope, $timeout, apiService, moveBoardApi) {
		// Inititate the promise tracker to track form submissions.

		var vm = this;
		vm.fieldData = datacontext.getFieldData();
		vm.truckAvailability = [];
		vm.avail = [];
		vm.blocked = [];
		vm.blockedDateStrings = [];

		$scope.loader = false;

		$scope.dates = {
			add: [],
			remove: []
		};
		$scope.monthDates = {
			add: [],
			remove: []
		};
		$rootScope.dayTypes = {
			allDays: [],
			weekDays: [],
			weekends: [],
			sundays: [],
			withoutWeekends: [],
			withoutSundays: []
		};
		vm.trucks = {};
		vm.branchingTrucks = {};


		function getTrucks(calenderRefresh) {
			$scope.loader = true;

			var promise = TrucksServices.getTrucks();
			promise.then(function (data) {
				vm.trucks = data;
				var i = 0;

				angular.forEach(vm.trucks, function (truck, tid) {
					if (!calenderRefresh) {
						i++;
						vm.truckAvailability[tid] = i == 1;
						vm.currentTruckId = tid;
					}
					truck.tid = tid;

					vm.avail[tid] = [];
					vm.blocked[tid] = [];
					vm.blockedDateStrings[tid] = [];

					angular.forEach(truck.unavailable, function (dateTime, n) {
						var date = moment.unix(dateTime).utc();
						vm.avail[tid][date.format('YYYY-MM-DD')] = 1;
					});
					angular.forEach(truck.unavailable_from_request, function (dateTime, n) {
						var date = moment.unix(dateTime).utc();
						vm.blocked[tid][date.format('YYYY-MM-DD')] = 1;
						vm.blockedDateStrings[tid].push(date.format('YYYY-MM-DD'));
					});
				});

				$scope.unavailableDaysType.weekend = false;
				$scope.unavailableDaysType.weekdays = false;
				$scope.unavailableDaysType.sundayDays = false;
				$scope.unavailableDaysType.allDays = false;
				$scope.unavailableDaysType.allDaysOn = false;
				$scope.loader = false;

				if (calenderRefresh) {
					loadingCalenderAgain();
				}
			});
		}

		getTrucks();

		$scope.showCurrent = true;
		vm.currentYear = moment().format('YYYY');
		vm.nextYear = parseInt(vm.currentYear) + 1;

		$scope.updateBranchesTruckCalendar = updateBranchesTruckCalendar;

		vm.selectTruck = function (tid) {
			angular.forEach(vm.truckAvailability, function (value, id) {
				vm.truckAvailability[id] = false;
			});

			vm.truckAvailability[tid] = true;
			vm.currentTruckId = tid;
		};

		vm.range = common.range;

		vm.update = function (truck, tid) {
			let truckData = {
				field_ld_only: truck.field_ld_only,
				name: truck.name,
				tid: tid
			};

			if (truck.name.length) {
				var promise = TrucksServices.updateTruck(truckData);
				vm.trucks[tid].name = truck.name;

				promise
					.then(function (data) {
					}, function (reason) {
						// If faild go to login page
						logger.error(reason, reason, "Error");
					});
			}
		};


		function loadingCalenderAgain() {
			vm.truckAvailability[$rootScope.currentTruckId] = false;
			vm.currentTruckId = $rootScope.currentTruckId;
			$scope.loader = true;

			$timeout(function () {
				$scope.loader = false;
				vm.truckAvailability[$rootScope.currentTruckId] = true;
			});
		}

		function calculateDateArray(dates, callback) {
			var removeDates = [],
				addDates = [];

			angular.forEach(dates, function (value, key) {
				value.forEach(function (item) {
					if (key == 'add') {
						addDates.push(item);
					} else {
						removeDates.push(item);
					}
				});
			});

			$scope.monthDates.remove.forEach(function (item) {
				var index = addDates.indexOf(item);

				if (index > -1) {
					addDates.splice(index, 1);
				}

				removeDates.push(item);
			});

			$scope.monthDates.add.forEach(function (item) {
				var index = removeDates.indexOf(item);

				if (index > -1) {
					removeDates.splice(index, 1);
				}

				addDates.push(item);
			});

			$scope.dates.add.forEach(function (item) {
				var index = removeDates.indexOf(item);

				if (index > -1) {
					removeDates.splice(index, 1);
				}

				addDates.push(item);
			});

			$scope.dates.remove.forEach(function (item) {
				var index = addDates.indexOf(item);

				if (index > -1) {
					addDates.splice(index, 1);
				}

				removeDates.push(item);
			});

			var data = {
				add: addDates,
				remove: removeDates
			};

			callback(data);
		}


		function setUnavailable(syncDates, callback) {
			TrucksServices
				.setUnvailability($rootScope.currentTruckId, syncDates, vm.blockedDateStrings[$rootScope.currentTruckId])
				.then(function (data) {
					getTrucks(true);
					callback(true);
				});
		}


		function initializeDatesScope() {
			$scope.dates.add = [];
			$scope.dates.remove = [];
		}


		function initializeOnOffDates() {
			$scope.monthDates.add = [];
			$scope.monthDates.remove = [];
		}


		$scope.update = function () {
			$scope.loader = true;

			let days = getDaysByType();
			updateTrackAvailableDates(days);
		};

		function getDaysByType() {
			let result = {
				add: [],
				remove: []
			};

			if ($scope.unavailableDaysType.weekend) {
				result = {
					add: $rootScope.dayTypes.weekends,
					remove: $rootScope.dayTypes.weekDays
				};
			} else if ($scope.unavailableDaysType.sundayDays) {
				result = {
					add: $rootScope.dayTypes.withoutSundays,
					remove: $rootScope.dayTypes.sundays
				};
			} else if ($scope.unavailableDaysType.weekdays) {
				result = {
					add: $rootScope.dayTypes.weekDays,
					remove: $rootScope.dayTypes.weekends
				};
			} else if ($scope.unavailableDaysType.allDays) {
				result = {
					add: [],
					remove: $rootScope.dayTypes.allDays
				};
			} else if ($scope.unavailableDaysType.allDaysOn) {
				result = {
					add: $rootScope.dayTypes.allDays,
					remove: []
				};
			} else {
				result = {
					add: $scope.dates.add,
					remove: $scope.dates.remove
				}
			}

			return result;
		}

		function updateTrackAvailableDates(dates) {
			calculateDateArray(dates, function (syncDates) {
				setUnavailable(syncDates, function (success) {
					initializeDatesScope();
					initializeOnOffDates();
				});
			});
		}

		function updateBranchesTruckCalendar() {
			$scope.loader = true;

			let days = getDaysByType();
			updateBranchAvailableDates(days);
		}

		function updateBranchAvailableDates(dates) {
			calculateDateArray(dates, function (syncDates) {
				syncDates.add = TrucksServices.compareUniqArrays(syncDates.add, vm.blockedDateStrings);
				syncDates.remove = TrucksServices.compareUniqArrays(syncDates.remove, vm.blockedDateStrings);
				let data = {
					"tid": $rootScope.currentTruckId,
					"date": syncDates
				};

				apiService.postData(moveBoardApi.branchingTruck.setTruckUnvilability, data)
					.then(onSuccessSetBranchesTrackUnvilability, onErrorSetBranchUnvilability);
			});
		}

		function onSuccessSetBranchesTrackUnvilability(response) {
			$rootScope.$broadcast('onReloadBranchesTracks');
			initializeDatesScope();
			initializeOnOffDates();

			if (response.data.status != 200) {
				toastr.error(response.data.statusText, 'Error');
			}
		}

		function onErrorSetBranchUnvilability() {
			$scope.loader = false;
		}

		$scope.unavailableDaysType = {
			weekend: false,
			weekdays: false,
			sundayDays: false,
			allDays: false,
			allDaysOn: false
		};


		$scope.weekend = function () {
			$scope.unavailableDaysType.weekdays = false;
			$scope.unavailableDaysType.sundayDays = false;
			$scope.unavailableDaysType.allDays = false;
			$scope.unavailableDaysType.allDaysOn = false;

			if (!$scope.unavailableDaysType.weekend) {
				$scope.loader = true;
				getTrucks(true);
			} else {
				vm.avail[$rootScope.currentTruckId] = [];
				$rootScope.dayTypes.weekends.forEach(function (item) {
					vm.avail[$rootScope.currentTruckId][item] = 1;
				});

				loadingCalenderAgain();
			}
		};


		$scope.weekdays = function () {
			$scope.unavailableDaysType.weekend = false;
			$scope.unavailableDaysType.sundayDays = false;
			$scope.unavailableDaysType.allDays = false;
			$scope.unavailableDaysType.allDaysOn = false;

			if (!$scope.unavailableDaysType.weekdays) {
				$scope.loader = true;

				getTrucks(true);
			} else {
				vm.avail[$rootScope.currentTruckId] = [];

				$rootScope.dayTypes.weekDays.forEach(function (item) {
					vm.avail[$rootScope.currentTruckId][item] = 1;
				});

				loadingCalenderAgain();
			}
		};

		$scope.sundayDays = function () {
			$scope.unavailableDaysType.weekend = false;
			$scope.unavailableDaysType.weekdays = false;
			$scope.unavailableDaysType.allDays = false;
			$scope.unavailableDaysType.allDaysOn = false;


			if (!$scope.unavailableDaysType.sundayDays) {
				$scope.loader = true;
				getTrucks(true);
			} else {
				vm.avail[$rootScope.currentTruckId] = [];

				$rootScope.dayTypes.withoutSundays.forEach(function (item) {
					vm.avail[$rootScope.currentTruckId][item] = 1;
				});

				loadingCalenderAgain();
			}
		};

		$scope.allDays = function () {
			$scope.unavailableDaysType.weekend = false;
			$scope.unavailableDaysType.weekdays = false;
			$scope.unavailableDaysType.sundayDays = false;
			$scope.unavailableDaysType.allDaysOn = false;

			if (!$scope.unavailableDaysType.allDays) {
				$scope.loader = true;

				getTrucks(true);
			} else {
				vm.avail[$rootScope.currentTruckId] = [];

				$rootScope.dayTypes.allDays.forEach(function (item) {
					vm.avail[$rootScope.currentTruckId][item] = 0;
				});

				loadingCalenderAgain();
			}
		};


		$scope.allDaysOn = function () {
			$scope.unavailableDaysType.weekend = false;
			$scope.unavailableDaysType.weekdays = false;
			$scope.unavailableDaysType.sundayDays = false;
			$scope.unavailableDaysType.allDays = false;

			if (!$scope.unavailableDaysType.allDaysOn) {
				$scope.loader = true;

				getTrucks(true);
			} else {
				vm.avail[$rootScope.currentTruckId] = [];

				$rootScope.dayTypes.allDays.forEach(function (item) {
					vm.avail[$rootScope.currentTruckId][item] = 1;
				});

				loadingCalenderAgain();
			}
		};


		///Modal
		//CONFIRM MODAL
		$scope.deleteTruckModal = function (tid) {
			SweetAlert.swal({
				title: "Delete Truck ?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#E47059",
				confirmButtonText: "Confirm",
				closeOnConfirm: true
			}, function (isConfirm) {
				if (isConfirm) {
					//update Request
					//Busy
					$scope.busy = true;

					var promise = TrucksServices.removeTruck(tid);
					promise.then(function (data) {
						$scope.busy = false;

						if (data[0] != false) {
							delete vm.trucks[tid];
						} else {
							SweetAlert.swal("You can't delete truck!");
						}

					}, function (reason) {
						logger.error(reason, reason, "Error");

					});
				}
			});
		};
	}

})();
