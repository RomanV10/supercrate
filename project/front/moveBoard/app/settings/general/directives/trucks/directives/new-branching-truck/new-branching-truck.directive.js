'use strict';

angular
	.module('app.settings')
	.directive('newBranchingTruck', newBranchingTruck);

function newBranchingTruck() {
	NewTruckController.$inject = ['$scope', '$element', '$attrs', 'apiService', 'moveBoardApi'];

	return {
		template: require('./new-truck.pug'),
		restrict: 'E',
		scope: {
			trucks: '=',
			loading: '=',
			available: '='
		},
		controller: NewTruckController
	};

	function NewTruckController($scope, $element, $attrs, apiService, moveBoardApi) {
		// variables
		let vm = $scope;
		vm.isAddTruck = false;
		initNewTruck();

		// functions
		vm.addTruck = addTruck;
		vm.openTruckForm = openTruckForm;
		vm.closeAddTruck = closeAddTruck;

		function addTruck() {
			if (vm.new_truck.name.length) {
				vm.loading = true;

				var promise = apiService.postData(moveBoardApi.branchingTruck.CRUD, vm.new_truck);
				promise.then(function (response) {
					response = response.data;
					vm.loading = false;

					if (response.status == 200) {
						vm.trucks[response.data] = vm.new_truck;
						vm.available[response.data] = [];

						closeAddTruck();
						initNewTruck();
					} else {
						toastr.error(response.statusText, 'Error');
					}
				});
			} else {
				toastr.warning('Please Enter Branching Truck Name', 'Warning!')
			}
		}

		function initNewTruck() {
			vm.new_truck = {
				name: "",
				field_ld_only: 0
			};
		}

		function openTruckForm() {
			vm.isAddTruck = true;
		}

		function closeAddTruck() {
			vm.isAddTruck = false;
		}
	}
}