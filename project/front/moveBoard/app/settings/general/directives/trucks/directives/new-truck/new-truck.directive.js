'use strict';

angular
	.module('app.settings')
	.directive('newTruck', newTruck);
/*
 attrs: isLongDistanceTrucks
 */
function newTruck(TrucksServices) {
	NewTruckController.$inject = ['$scope', '$element', '$attrs', 'TrucksServices'];

	return {
		template: require('./new-truck.pug'),
		restrict: 'E',
		scope: {
			trucks: '=',
			loading: '='
		},
		link: link,
		controller: NewTruckController
	};

	function link($scope, $element, $attrs) {
		$scope.addTruckName = $attrs.isLongDistanceTrucks ? 'Trailer' : 'Truck';
	}

	function NewTruckController($scope, $element, $attrs, TrucksServices) {
		// variables
		let vm = $scope;
		closeAddTruck();
		initNewTruck();

		// functions
		vm.addTruck = addTruck;
		vm.openTruckForm = openTruckForm;
		vm.closeAddTruck = closeAddTruck;

		function addTruck() {
			vm.new_truck.field_ld_only = $attrs.isLongDistanceTrucks ? 1 : 0;

			if (vm.new_truck.name.length) {
				vm.loading = true;

				var promise = TrucksServices.addTruck(vm.new_truck);
				promise.then(function (data) {
					vm.loading = false;
					vm.trucks[data[0]] = vm.new_truck;
					closeAddTruck();
					initNewTruck();
				});
			} else {
				toastr.warning(`Please Enter ${vm.addTruckName} Name`, 'Warning!')
			}
		}

		function initNewTruck() {
			vm.new_truck = {name: ""};
		}

		function openTruckForm() {
			vm.isAddTruck = true;
		}

		function closeAddTruck() {
			vm.isAddTruck = false;
		}
	}
}