'use strict';

import './truck-settings.styl'

angular
	.module('app.settings')
	.directive('truckSettings', truckSettings);

truckSettings.$inject = [];

function truckSettings() {
	return {
		template: require('./truck-settings.pug'),
		restrict: 'E',
		scope: {},
		link: linkFunction,
		controller: 'TrucksSettingsContorller',
		controllerAs: 'vm'
	};

	// Нужен для управления DOM директивы
	function linkFunction($scope) {

	}
}
