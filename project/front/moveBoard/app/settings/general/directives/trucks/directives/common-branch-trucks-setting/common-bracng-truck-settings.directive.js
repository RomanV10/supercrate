'use strict';

import './common-branches-trucks-settings.styl';

angular
	.module('app.settings')
	.directive('commonBranchesTrucksSettings', commonBranchesTrucksSettings);

function commonBranchesTrucksSettings() {
	const COMMON_BRANCHES_TRUCKS_SETTING_NAME = 'is_common_branch_truck';
	const MIN_BRANCHES_COUNT = 2;

	BranchingTruckSwitcherController.$inject = ['$scope', 'apiService', 'moveBoardApi', 'logger', 'datacontext', 'SweetAlert'];

	return {
		template: require('./common-branching-trucks.pug'),
		restrict: 'E',
		scope: {
			allowTruck: '='
		},
		controller: BranchingTruckSwitcherController
	};

	function BranchingTruckSwitcherController($scope, apiService, moveBoardApi, logger, datacontext, SweetAlert) {
		// variables
		let vm = $scope;
		vm.loader = true;
		let fieldData = datacontext.getFieldData();
		let branches = fieldData.branches || [];
		vm.allowTruck = fieldData[COMMON_BRANCHES_TRUCKS_SETTING_NAME];
		vm.isAllowCommonTruck = checkAllowCountBranches();

		// functions
		vm.onChangeCommonParklotSettings = onChangeCommonParklotSettings;
		vm.showAlert = showAlert;

		function onChangeCommonParklotSettings() {
			let data = {
				name: COMMON_BRANCHES_TRUCKS_SETTING_NAME,
				value: vm.allowTruck
			};

			apiService
				.postData(moveBoardApi.branching.saveBranchSettings, data)
				.then(onSaveCommontTruckSettingsName, onErrorWhenSaveCommontTrucksSetting);
		}

		function checkAllowCountBranches() {
			return _.size(branches) >= MIN_BRANCHES_COUNT;
		}

		function onSaveCommontTruckSettingsName() {
			fieldData[COMMON_BRANCHES_TRUCKS_SETTING_NAME] = vm.allowTruck;
		}

		function onErrorWhenSaveCommontTrucksSetting(response) {
			logger.error('Error', response);
		}

		function showAlert() {
			SweetAlert.swal('You need at least two branches for create common branch. Please connect with elromco company for create new branch.', '', 'info');
		}
	}
}