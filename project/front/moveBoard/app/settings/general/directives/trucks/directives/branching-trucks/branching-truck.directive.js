'use strict';

angular
	.module('app.settings')
	.directive('branchingTruck', branchingTruck);

function branchingTruck() {
	BranchingTruckController.$inject = ['$scope', '$element', '$attrs', 'apiService', 'moveBoardApi', 'logger', 'SweetAlert'];

	return {
		template: require('./branching-truck.pug'),
		restrict: 'E',
		scope: {
			loader: '=',
			truckAvailability: '=',
			available: '=',
			blocked: '=',
			branchingTrucks: '=',
			currentTruckId: '=',
		},
		controller: BranchingTruckController
	};

	function BranchingTruckController($scope, $element, $attrs, apiService, moveBoardApi, logger, SweetAlert) {
		// variables
		let vm = $scope;

		vm.trucks = {};

		vm.edit = {};

		// functions
		vm.selectTruck = selectTruck;
		vm.update = update;
		vm.deleteTruckModal = deleteTruckModal;

		loadBranchesTracks();

		vm.$on('onReloadBranchesTracks', loadBranchesTracks);

		function loadBranchesTracks() {
			apiService
				.getData(moveBoardApi.branchingTruck.CRUD)
				.then(onLoadAllRequests)
				.finally(finalRequest);
		}

		function onLoadAllRequests(response) {
			response = response.data;

			if (response.status == 200) {
				if (_.isArray(response.data) || !response.data) {
					response.data = {};
				}

				vm.trucks = response.data;

				angular.forEach(vm.trucks, function (truck, tid) {
					vm.available[tid] = [];
					vm.blocked[tid] = [];
					truck.tid = tid;

					angular.forEach(truck.unavailable, function (dateTime, n) {
						var date = moment.unix(dateTime).utc();
						vm.available[tid][date.format('YYYY-MM-DD')] = 1;
					});
					angular.forEach(truck.unavailable_from_request, function (dateTime, n) {
						var date = moment.unix(dateTime).utc();
						vm.blocked[tid][date.format('YYYY-MM-DD')] = 1;
					});
				});
				vm.branchingTrucks = vm.trucks;
			} else {
				toastr.error(response.statusText, 'Error')
			}

		}

		function finalRequest() {
			vm.loader = false;
		}

		function selectTruck(tid) {

			vm.loader = true;

			angular.forEach(vm.truckAvailability, function (value, id) {
				vm.truckAvailability[id] = false;
			});

			vm.truckAvailability[tid] = true;
			vm.currentTruckId = tid;

			vm.loader = false;
		}

		function update(truck, tid) {
			truck.tid = tid;

			if (truck.name.length) {
				apiService
					.putData(moveBoardApi.branchingTruck.CRUD + "/" + truck.tid, truck)
					.then(function (response) {
						response = response.data;

						if (response.status == 200) {
							vm.trucks[tid].name = truck.name;
						} else {
							logger.error(response.statusText, response.statusText, "Error");
						}
					}, function (reason) {
						// If faild go to login page
						logger.error(reason, reason, "Error");
					});
			}
		}

		function deleteTruckModal(tid) {
			SweetAlert.swal({
				title: "Delete Truck ?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#E47059",
				confirmButtonText: "Confirm",
				closeOnConfirm: true
			}, function (isConfirm) {
				if (isConfirm) {
					//update Request
					//Busy
					vm.loader = true;

					apiService
						.delData(moveBoardApi.branchingTruck.CRUD + '/' + tid)
						.then(function (response) {
							response = response.data;

							if (response.status == 200) {
								delete vm.trucks[tid];
							} else {
								SweetAlert.swal(response.statusText);
							}
						}, function (reason) {
							logger.error(reason, reason, "Error");
						})
						.finally(finalRequest);
				}
			});
		}
	}
}