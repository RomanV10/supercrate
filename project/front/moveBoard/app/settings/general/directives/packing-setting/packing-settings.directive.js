import './packing-settings.styl';

'use strict';

angular
	.module('app.settings')
	.directive('packingSettings', packingSettings);

function packingSettings() {
	const HIDE_SAVED_TIMEOUT = 1000;
	const BASIC_SETTING_NAME = 'basicsettings';
	const UPDATE_PACKING_ACTION = 'reload_packing_settings_list';
	const DRAGULA_DROP_ACTION = 'packing-bag.drop-model';
	const PACKING_SETTINGS_NAME = 'packing_settings';

	controller.$inject = ['$scope', '$uibModal', 'datacontext', 'SettingServices', 'common', 'SweetAlert', 'dragulaService'];

	return {
		scope: {},
		template: require('./packing-settings.template.html'),
		restrict: 'E',
		controller: controller,
		controllerAs: 'vm',
	};

	function controller($scope, $uibModal, datacontext, SettingServices, common, SweetAlert, dragulaService) {
		let vm = this;
		let dataToDelete;

		vm.fieldData = datacontext.getFieldData();
		vm.basicSettings = angular.fromJson(vm.fieldData.basicsettings);
		vm.busy = false;
		vm.activeRow = -1;
		vm.packing = {
			account: vm.basicSettings.packing_settings.packingAccount,
			labor: vm.basicSettings.packing_settings.packingLabor
		};
		vm.packingService = {
			LM: vm.basicSettings.packing_settings.packing.LM,
			LD: vm.basicSettings.packing_settings.packing.LD
		};

		$scope.$on(DRAGULA_DROP_ACTION, onDropPacking);
		$scope.$on(UPDATE_PACKING_ACTION, onUpdatePacking);

		loadPackingSettings();

		$scope.removeService = removeService;
		$scope.prepareToDelete = prepareToDelete;

		vm.packingServiceModal = packingServiceModal;
		vm.changePackingsCoefficient = changePackingsCoefficient;
		vm.saveSettingsPacking = saveSettingsPacking;
		vm.saveSettings = saveSettings;

		dragulaService.options(vm, 'packing-bag', {
			moves: (el, container, handle) => handle.className === 'dragdrop-button',
			direction: 'vertical',
		});

		function loadPackingSettings() {
			SettingServices.getSettings(PACKING_SETTINGS_NAME)
				.then((response) => {
					vm.busy = false;

					$scope.packingSettings = angular.fromJson(response[0]);
				});
		}

		function prepareToDelete(data) {
			vm.activeRow = data;
			dataToDelete = data;
		}

		function removeService(setting_type = PACKING_SETTINGS_NAME) {
			SweetAlert.swal({
				title: 'Delete Service ?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#E47059',
				confirmButtonText: 'Confirm',
				closeOnConfirm: true
			}, (isConfirm) => {
				if (isConfirm) {
					vm.busy = true;

					vm.saving = true;
					SettingServices.deleteSettings(setting_type, dataToDelete)
						.then(() => {
							vm.activeRow = -1;
							loadPackingSettings();
							showSaved();
						});
				}
			});
		}

		function onDropPacking() {
			vm.saving = true;

			SettingServices.saveSettings($scope.packingSettings, PACKING_SETTINGS_NAME)
				.then(function () {
					showSaved();
				});
		}

		function onUpdatePacking() {
			loadPackingSettings();
			showSaved();
		}

		function showSaved() {
			vm.saving = false;
			vm.saved = true;

			common.$timeout(function () {
				vm.saved = false;
			}, HIDE_SAVED_TIMEOUT);
		}

		function packingServiceModal() {
			$uibModal.open({
				template: require('../../packing-settings-modal/packing-settings-modal.pug'),
				controller: 'PackingModalCtrl',
				resolve: {
					basicSettings: () => vm.basicSettings,
				},
				size: 'lg'
			});
		}

		function changePackingsCoefficient() {
			vm.basicSettings.packing_settings.packing = _.clone(vm.packingService);
			vm.saveSettings();
		}

		function saveSettingsPacking() {
			vm.basicSettings.packing_settings.packingAccount = vm.packing.account;
			vm.basicSettings.packing_settings.packingLabor = vm.packing.labor;
			let setting = vm.basicSettings;
			SettingServices.saveSettings(setting, BASIC_SETTING_NAME);
		}

		function saveSettings() {
			vm.saving = true;

			SettingServices.saveSettings(vm.basicSettings, BASIC_SETTING_NAME)
				.then(function () {
					vm.saving = false;
					showSaved();
				});
		}
	}
}
