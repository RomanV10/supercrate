describe('Directive: Packing Settings', function () {
	let SettingServices;
	let elementScope;
	let $scope;
	let element;
	let SweetAlert;
	let extraServiceResult = ['[{"name":"Tip","extra_services":[{"services_name":"Cost","services_type":"Amount","services_read_only":false}]}]'];
	let getSettingsValue = {
		then(callback) {
			callback(extraServiceResult);
		}
	};
	let deleteExtraServiceItem = {
		then(callback) {
			callback();
		}
	};
	let fieldData = {
		basicsettings: {
			packing_settings: {
				packingAccount: true,
				packingLabor: true,
				packing: {
					LM: 10,
					LD: 10,
				}
			}
		}
	};

	function compileElement(elementHtml) {
		let elem = angular.element(elementHtml);
		element = $compile(elem)($scope);
		elementScope = element.isolateScope();
	}

	beforeEach(inject(function (_SettingServices_, _SweetAlert_) {
		SettingServices = _SettingServices_;
		SweetAlert = _SweetAlert_;

		$scope = $rootScope.$new();

		spyOn(SettingServices, 'getSettings')
			.and
			.returnValue(getSettingsValue);
		spyOn(datacontext, 'getFieldData')
			.and
			.returnValue(fieldData);

		compileElement('<packing-settings></packing-settings>');
	}));

	describe('When init', () => {
		it('should not render empty html', function () {
			expect(element.html()).not.toEqual('');
		});

		it('should call getSettings', () => {
			expect(SettingServices.getSettings).toHaveBeenCalled();
		});

		it('active row should be -1', () => {
			//given
			let expectedResult = -1;

			//when

			//then
			expect(elementScope.vm.activeRow).toBe(expectedResult);
		});

		it('prepareToDelete to be defined', () => {
			expect(elementScope.prepareToDelete).toBeDefined();
		});

		it('removeService to be defined', () => {
			expect(elementScope.removeService).toBeDefined();
		});

		it('busy to be false', () => {
			//given
			let expectedResult = false;

			//when

			//then
			expect(elementScope.vm.busy).toBe(expectedResult);
		});

		it('packingSettings to be defined', () => {
			expect(elementScope.packingSettings).toBeDefined();
		});

		it('packingServiceModal to be defined', () => {
			expect(elementScope.vm.packingServiceModal).toBeDefined();
		});

		it('changePackingsCoefficient to be defined', () => {
			expect(elementScope.vm.changePackingsCoefficient).toBeDefined();
		});

		it('saveSettingsPacking to be defined', () => {
			expect(elementScope.vm.saveSettingsPacking).toBeDefined();
		});

		it('saveSettings to be defined', () => {
			expect(elementScope.vm.saveSettings).toBeDefined();
		});
	});

	describe('method: prepareToDelete', () => {
		it('should change activeRow', () => {
			//given
			let expectedResult = 1;

			//when
			elementScope.prepareToDelete(expectedResult);

			//then
			expect($scope.activeRow).toBe();
		});
	});

	describe('method: removeService', () => {
		beforeEach(() => {
			spyOn(SweetAlert, 'swal')
				.and
				.callFake((option, callback) => callback(true));
			spyOn(SettingServices, 'deleteSettings')
				.and
				.returnValue(deleteExtraServiceItem);

			//when
			elementScope.removeService();
		});

		it('should call sweet alert', () => {
			expect(SweetAlert.swal).toHaveBeenCalled();
		});

		it('should call delete settigns', () => {
			//given
			let expectedResult = ['packing_settings', undefined];

			//when

			//then
			expect(SettingServices.deleteSettings).toHaveBeenCalledWith(...expectedResult);
		});

		it('should set active row to -1', () => {
			expect(elementScope.vm.activeRow).toBe(-1);
		});

		it('SettingServices.getSettings to have been called twice', () => {
			expect(SettingServices.getSettings.calls.count()).toBe(2);
		});
	});
});
