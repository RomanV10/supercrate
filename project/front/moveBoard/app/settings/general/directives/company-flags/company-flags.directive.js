'use strict';

const BASIC_SETTING_NAME = 'basicsettings';
const ONE_SECOND = 1000;
const CUSTOM_SETTINGS = {
	theme: 'bootstrap',
	position: 'bottom left',
	defaultValue: '',
	animationSpeed: 50,
	animationEasing: 'swing',
	change: null,
	changeDelay: 0,
	control: 'wheel',
	hide: null,
	hideSpeed: 100,
	inline: false,
	letterCase: 'lowercase',
	opacity: false,
	show: null,
	showSpeed: 100
};
const DEFAULT_COLOR_OF_COMPANY_FLAG = '#ffffff';

angular
	.module('app.settings')
	.directive('erCompanyFlagsSettings', branchSettingsDirective);

function branchSettingsDirective() {
	let directive = {
		restrict: "E",
		scope: true,
		template: require('./company-flags.pug'),
		controller: ErCompanyFlagsSettingsController,
		controllerAs: 'vm'
	};

	return directive;
}

ErCompanyFlagsSettingsController.$inject = ['dragulaService', '$scope', 'SettingServices', 'datacontext', '$rootScope', 'common', 'logger', 'SweetAlert'];

function ErCompanyFlagsSettingsController(dragulaService, $scope, SettingServices, datacontext, $rootScope, common, logger, SweetAlert) {
	let vm = this;
	let fieldData = datacontext.getFieldData();
	let basicSettings = fieldData.basicsettings;

	vm.customSettings = CUSTOM_SETTINGS;
	vm.companyFlags = [];

	// functions
	vm.addNewFlag = addNewFlag;
	vm.saveNewFlag = saveNewFlag;
	vm.saveChangesFlag = saveChangesFlag;
	vm.removeFlag = removeFlag;

	initCompanyFlags();
	addNewFlag();

	dragulaService.options(vm, 'flags-bag', {
		moves: function (el, container, handle) {
			return handle.className === 'dragdrop-button';
		},
		direction: 'vertical'
	});

	$scope.$on('flags-bag.drop-model', function (e, el) {
		basicSettings.companyFlags = _.clone(vm.companyFlags);
		saveSettings();
	});

	function saveSettings() {
		vm.saving = true;

		let promise = SettingServices.saveSettings(basicSettings, BASIC_SETTING_NAME);

		promise.then(function () {
			datacontext.saveFieldDataType(BASIC_SETTING_NAME, basicSettings);
			vm.saving = false;

			common.$timeout(function () {
				vm.saved = false;
			}, ONE_SECOND);

			vm.saved = true;

			$rootScope.$broadcast('flag added', vm.companyFlags);
		});
	}

	function initCompanyFlags() {
		if (angular.isDefined(basicSettings.companyFlags)) {
			let companyFlagsData = angular.copy(_.values(basicSettings.companyFlags));

			angular.forEach(companyFlagsData, function (flag, id) {
				if (!_.isNull(flag)) {
					vm.companyFlags.push(flag);
				}
			});
		}

		vm.busy = true;

		SettingServices
			.getFlags()
			.then(onSuccessLoadFlags)
			.finally(disableBusyOverlay);
	}

	function onSuccessLoadFlags(response) {
		if (!response && !response.flags) {
			return false;
		}

		let flags = response.flags;

		let saveChanges = removeNotExistBasicSettingsFlags(flags);
		saveChanges = saveChanges || addNotExistingBasicSettingsFlag(flags);

		if (saveChanges) {
			basicSettings.companyFlags = angular.copy(vm.companyFlags);
			saveSettings();
		}
	}

	function removeNotExistBasicSettingsFlags(flags) {
		let result = false;
		let notExistIndex = [];

		_.each(vm.companyFlags, function (flag, index) {
			let isNotExist = true;

			angular.forEach(flags, function (value, key) {
				if (flag.id == key) {
					isNotExist = false;
					return false;
				}
			});

			if (isNotExist) {
				notExistIndex.push(index);
			}
		});

		_.each(notExistIndex, function (index) {
			vm.companyFlags.splice(index, 1);
			result = true;
		});

		return result;
	}

	function addNotExistingBasicSettingsFlag(flags) {
		let result = false;

		angular.forEach(flags, function (flag, id) {
			let isNotExist = _.findIndex(vm.companyFlags, {id}) == -1;

			if (isNotExist) {
				let newFlag = {};
				newFlag.name = flag;
				newFlag.id = id;
				newFlag.color = DEFAULT_COLOR_OF_COMPANY_FLAG;
				vm.companyFlags.push(newFlag);
				result = true;
			}
		});

		return result;
	}

	function disableBusyOverlay() {
		vm.busy = false;
	}

	function addNewFlag() {
		let name = '';
		vm.newFlag = {name, color: DEFAULT_COLOR_OF_COMPANY_FLAG};
	}

	function saveNewFlag() {
		vm.busy = true;

		SettingServices
			.createFlag(vm.newFlag)
			.then(onSuccessCreateFlug, onErrorCreateFlug);
	}

	function onSuccessCreateFlug(response) {
		vm.newFlag.id = Number(response.data[0]).toString();
		let copyNewFlag = angular.copy(vm.newFlag);
		vm.companyFlags.push(copyNewFlag);

		vm.addFlag = false;
		vm.busy = false;
		basicSettings.companyFlags = angular.copy(vm.companyFlags);
		saveSettings();
		logger.success('Flag created', '', 'Success');
	}

	function onErrorCreateFlug(error) {
		logger.error('Flag was not created', 'Flag was not created', 'Error');
	}

	function saveChangesFlag(value) {
		SettingServices.editFlag(value).then(function (data) {
			basicSettings.companyFlags = angular.copy(vm.companyFlags);
			saveSettings();
			$rootScope.$broadcast('flag added', vm.companyFlags);
		})
	}


	function removeFlag(key, flag) {
		let title = "Are you sure you want to remove this flag?";
		let text = "";
		let type = "warning";
		let showCancelButton = true;
		let confirmButtonColor = "#DD6B55";
		let confirmButtonText = "Yes, let's do it!";
		let cancelButtonText = "No, cancel pls!";
		let closeOnConfirm = true;
		let closeOnCancel = true;

		SweetAlert.swal({title, text, type, showCancelButton, confirmButtonColor, confirmButtonText, cancelButtonText, closeOnConfirm, closeOnCancel},
			function (isConfirm) {
				if (isConfirm) {
					vm.busy = true;
					let id = flag.id;
					SettingServices.removeFlag(flag).then(function (data) {
						_.forEachRight(vm.companyFlags, function (item, ind) {
							if (item.id == id) {
								vm.companyFlags.splice(ind, 1);
								return false;
							}
						});

						basicSettings.companyFlags = angular.copy(vm.companyFlags);

						saveSettings();
						vm.busy = false;
					})
				}
			});
	}
}