'use strict';

angular
	.module('app.calculator')
	.factory('CalendarServices', CalendarServices);

CalendarServices.$inject = ['$http', '$q', 'config', 'apiService', 'moveBoardApi'];

function CalendarServices($http, $q, config, apiService, moveBoardApi) {
	var service = {};
	
	service.setType = setType;
	service.getTypes = getTypes;
	service.getType = getType;
	
	return service;
	
	function getType(date, calendar) {
		var year = moment(date).format('YYYY');
		
		if (angular.isDefined(calendar[year])) {
			return calendar[year][date];
		} else {
			return '8';
		}
	}
	
	function setType(date, type) {
		let deferred = $q.defer();
		
		apiService.postData(moveBoardApi.priceCalendar.update, {
			[date]: type
		})
			.then(resolve => {
				deferred.resolve(resolve.data)
			}, reject => {
				deferred.reject(reject.data);
			});
		
		
		return deferred.promise;
	}
	
	function getTypes() {
		let deferred = $q.defer();
		
		apiService.postData(moveBoardApi.priceCalendar.get)
			.then(res => {
				deferred.resolve(res.data);
			}, rej => {
				deferred.reject(rej.data);
			});
		
		return deferred.promise;
	}
	
}
