'use strict';

const COMPANY_POLICY = `[Company Policy]`;

angular
	.module('app.tplBuilder')
	.config(provideFunction);

provideFunction.$inject = ['$provide'];

function provideFunction($provide){
	$provide.decorator('taOptions', options);

	options.$inject = ['taRegisterTool', '$delegate'];

	function options(taRegisterTool, taOptions){
		taRegisterTool('companyPolicy', {
			display: `<button class='btn btn-default' type='button' ng-disabled='showHtml()' ng-click="action()">Company Policy</button>`,
			tooltiptext: "Insert Company Policy Link",
			action: function() {
				this.$editor().wrapSelection('insertHtml', COMPANY_POLICY);
			},
			active: '[companyPolicy:]',
		});

		taOptions.toolbar[1].push('companyPolicy');
		return taOptions;
	}
}
