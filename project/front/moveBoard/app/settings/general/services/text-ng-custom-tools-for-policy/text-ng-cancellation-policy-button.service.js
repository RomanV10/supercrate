'use strict';

const CANCELLATION_POLICY = `[Cancellation And Reschedule Policy]`;

angular
	.module('app.tplBuilder')
	.config(provideFunction);

provideFunction.$inject = ['$provide'];

function provideFunction($provide){
	$provide.decorator('taOptions', options);

	options.$inject = ['taRegisterTool', '$delegate'];

	function options(taRegisterTool, taOptions){
		taRegisterTool('cancellationPolicy', {
			display: `<button class='btn btn-default' type='button' ng-disabled='showHtml()' ng-click="action()">Cancellation Policy</button>`,
			tooltiptext: "Insert Cancellation Policy link",
			action: function() {
				this.$editor().wrapSelection('insertHtml', CANCELLATION_POLICY);
			},
			active: '[cancellationPolicy:]',
		});

		taOptions.toolbar[1].push('cancellationPolicy');
		return taOptions;
	}
}
