(function () {
    'use strict';

    angular
        .module('app.settings')
        .factory('valuationConfigs', valuationConfigs);


    function valuationConfigs ()
    {
        var service = {};

        service.valuation = {
            amounts:{
                   "2000":[  
                      0, 0, 0, 0
                   ],
                   "5000":[  
                      0, 0, 0, 0
                   ],
                   "10000":[  
                      0, 0, 0, 0
                   ],
                   "15000":[  
                      0, 0, 0, 0
                   ],
                   "20000":[  
                      0, 0, 0, 0
                   ],
                   "25000":[  
                      0, 0, 0, 0
                   ]
            },
            text: 'Warning',
            columns:[
              '0','250','500','1000'
            ]
        };

        return service;
    }

})();
