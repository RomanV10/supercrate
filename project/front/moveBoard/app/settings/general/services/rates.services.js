(function () {
  
    angular
        .module('app.settings')
    
    .factory('RatesServices',RatesServices);

    RatesServices.$inject = ['$http','$q','datacontext','config'];
    
    function RatesServices($http,$q,datacontext,config) {
        
        var service = {};

        service.getRates = getRates;
        service.setRates = setRates;
        service.getStorageRates = getStorageRates;
        service.setStorageRates = setStorageRates;
    
        return service;
        

function getRates() {

      
            var deferred = $q.defer(); 


            $http.post(config.serverUrl+'server/settings/price_table_get')
            .success(function(data) {


                                    deferred.resolve(data);

            })
            .error(function(data) {

                                    // data.success = false;
                                     deferred.reject(data);

             });
   

            return deferred.promise;

   

}        
        
        
function setRates(data) {

        
            var deferred = $q.defer(); 

 

            $http.post(config.serverUrl+'server/settings/price_table_set',data)
            .success(function(data, status, headers, config) {


                                    deferred.resolve(data);

            })
            .error(function(data, status, headers, config) {

                                    // data.success = false;
                                     deferred.reject(data);

             });
   

            return deferred.promise;

}


        function getStorageRates() {


            var deferred = $q.defer();


            $http.post(config.serverUrl+'server/settings/storage_table_get')
                .success(function(data) {


                    deferred.resolve(data);

                })
                .error(function(data) {


                    deferred.reject(data);

                });


            return deferred.promise;



        }


        function setStorageRates(data) {


            var deferred = $q.defer();



            $http.post(config.serverUrl+'server/settings/storage_table_set',data)
                .success(function(data) {


                    deferred.resolve(data);

                })
                .error(function(data) {


                    deferred.reject(data);

                });


            return deferred.promise;

        }
       
 

 
 }})();