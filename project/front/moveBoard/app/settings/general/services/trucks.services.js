(function () {

    angular
        .module('app.settings')

        .factory('TrucksServices', TrucksServices);

    TrucksServices.$inject = ['$http', '$q', '$rootScope', 'config'];

    function TrucksServices($http, $q, $rootScope, config) {

        var service = {};

        $rootScope.dayTrucks = [];
        service.saveTrucks = saveTrucks;
        service.addTruck = addTruck;
        service.getSuperTrucks = getSuperTrucks;
        service.getTrucks = getTrucks;
        service.getLocalTrucks = getLocalTrucks;
        service.getType = getType;
        service.setUnvailability = setUnvailability;
        service.updateTruck = updateTruck;
        service.removeUnvailability = removeUnvailability;
        service.removeTruck = removeTruck;
        service.checkOverbooking = checkOverbooking;
        service.compareUniqArrays = compareUniqArrays;

        return service;

        function saveTrucks(nid, trucks) {
            $rootScope.dayTrucks[nid] = trucks;
        }

        function getType(date, unvailability) {
            if (unvailability != null)
                return unvailability[date];
            else
                return '';
        }

        function updateTruck(truck) {
            var deferred = $q.defer();

            var data = truck

	        $http.post(config.serverUrl + '/server/settings/truck_edit', data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    // data.success = false;
                    deferred.reject(data);
                });

            return deferred.promise;
        }

        function removeUnvailability(tid, date) {
            var deferred = $q.defer();

            var dd = [];
            dd.push(date);
            var data = {
                "tid": tid,
                date: {
                    remove: dd
                }
            };

            $http.post(config.serverUrl + 'server/settings/truck_unavailable', data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });

            return deferred.promise;
        }

        function compareUniqArrays(array1, array2) {
	        array1 = array1.filter(function(val) {
		        return array2.indexOf(val) == -1;
	        });
	        return array1;
        }

        function setUnvailability(tid, dates, blockedDates) {
	        dates.add = service.compareUniqArrays(dates.add, blockedDates);
	        dates.remove = service.compareUniqArrays(dates.remove, blockedDates);
            var deferred = $q.defer();
            var data = {
                "truck_id": tid,
                dates_op: dates
            };

            $http
                .post(config.serverUrl + 'server/move_parking/truck_date_unavailable', data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });

            return deferred.promise;
        }


        function removeTruck(tid) {
            var deferred = $q.defer();

            $http.post(config.serverUrl + 'server/settings/truck_remove/' + tid)
                .success(function (data, status, headers, config) {
                    if ($rootScope.fieldData.taxonomy && $rootScope.fieldData.taxonomy.trucks && $rootScope.fieldData.taxonomy.trucks[tid]) {
                        delete $rootScope.fieldData.taxonomy.trucks[tid];
                    }

                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });


            return deferred.promise;
        }

        function checkOverbooking(data) {
            let deferred = $q.defer();

            $http.post(config.serverUrl + 'server/move_request/check_overbooking', data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
	                deferred.reject(data);
                });

            return deferred.promise;
        }

        function addTruck(truck) {
            var deferred = $q.defer();

            var data = truck;

            $http.post(config.serverUrl + 'server/settings/truck_insert', data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });

            return deferred.promise;
        }

        function getSuperTrucks(nid) {
            return $rootScope.dayTrucks[nid];
        }

        function getTrucks() {
            var deferred = $q.defer();

            $http.post(config.serverUrl + 'server/settings/trucks_get')
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    // data.success = false;
                    deferred.reject(data);
                });

            return deferred.promise;
        }
        function getLocalTrucks() {
            var deferred = $q.defer();
            var conditions = {
                   field_ld_only: 0 
                }
            $http.post(config.serverUrl + 'server/settings/trucks_get', conditions)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    // data.success = false;
                    deferred.reject(data);
                });

            return deferred.promise;
        }
    }
})();