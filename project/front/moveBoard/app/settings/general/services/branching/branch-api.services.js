(function () {

    angular
        .module('app.calculator')
        .factory('BranchAPIService', BranchAPIService);

    BranchAPIService.$inject = ['apiService', 'moveBoardApi', '$q', '$http', 'datacontext'];

    function BranchAPIService(apiService, moveBoardApi, $q, $http, datacontext) {
        var service = {};

        service.getAllBranches = getAllBranches;
        service.createBranch = createBranch;
        service.updateBranch = updateBranch;
        service.removeBranch = removeBranch;
        service.synchronizeBranches = synchronizeBranches;
        service.updateBasedState = updateBasedState;
        service.updateParkingZip = updateParkingZip;
        service.migrateRequest = migrateRequest;
        service.getBasicSettingsByUrl = getBasicSettingsByUrl;
        service.copyWorkerToBranch = copyWorkerToBranch;
        service.uploadBranchLogo = uploadBranchLogo;

        return service;

        function getAllBranches() {
            var deferred = $q.defer();

            apiService.getData(moveBoardApi.branching.getAllBranch)
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function (reason) {
                    deferred.reject(reason);
                });

            return deferred.promise;
        }

        function createBranch(branch) {
            var deferred = $q.defer();

            var data = {"data": branch};

            apiService.postData(moveBoardApi.branching.createBranch, data)
                      .then(function (response) {
                          if (_.get(response, 'data.status_code')) {
							  deferred.reject(response.data);
                          } else {
							  deferred.resolve(response.data);
                          }
                      }, function (reason) {
                          deferred.reject(reason);
                      });

            return deferred.promise;
        }

        function updateBranch(branch) {
            var deferred = $q.defer();

            var copyBranch = angular.copy(branch);
            delete copyBranch.id;

            var data = {"data": copyBranch};

            apiService.putData(moveBoardApi.branching.updateBranch + '/' + branch.id, data)
                      .then(function (response) {
                          deferred.resolve(response.data);
                      }, function (reason) {
                          deferred.reject(reason);
                      });

            return deferred.promise;
        }

        function removeBranch(branch) {
            var deferred = $q.defer();

            apiService.delData(moveBoardApi.branching.deleteBranch + '/' + branch.id)
                      .then(function (response) {
                          deferred.resolve(response.data);
                      }, function (reason) {
                          deferred.reject(reason);
                      });

            return deferred.promise;
        }

        function synchronizeBranches() {
            var deferred = $q.defer();

            apiService.postData(moveBoardApi.branching.synchronize)
                      .then(function (response) {
                          deferred.resolve(response.data);
                          reloadBranches();
                      }, function (reason) {
                          deferred.reject(reason);
                      });

            return deferred.promise;
        }

        function reloadBranches() {
            getAllBranches()
                .then(function (branches) {
                    datacontext
                        .getFieldData()
                        .branches = branches;
                });
        }

        function updateBasedState(basedState) {
            var deferred = $q.defer();

            var data = {"based_state": basedState};

            apiService.postData(moveBoardApi.branching.updateBasedState, data)
                      .then(function (response) {
                          deferred.resolve(response.data);
                          reloadBranches();
                      }, function (reason) {
                          deferred.reject(reason);
                      });

            return deferred.promise;
        }

        function updateParkingZip(zip) {
            var deferred = $q.defer();

            var data = {"zip": zip};

            apiService.postData(moveBoardApi.branching.updateParkingZip, data)
                      .then(function (response) {
                          deferred.resolve(response.data);
                          reloadBranches();
                      }, function (reason) {
                          deferred.reject(reason);
                      });

            return deferred.promise;
        }

        function migrateRequest(nid, branchId) {
            var deferred = $q.defer();

            var data = {
                "nid": nid,
                "branch_id": branchId
            };

            apiService.postData(moveBoardApi.branching.migrateToBranch, data)
                      .then(function (response) {
                          deferred.resolve(response.data);
                      }, function (reason) {
                          deferred.reject(reason);
                      });

            return deferred.promise;
        }

        function copyWorkerToBranch(uid, branchId) {
            var deferred = $q.defer();

            var data = {
                "uid": uid,
                "branch_id": branchId
            };

            apiService.postData(moveBoardApi.branching.copyManagerToBranch, data)
                      .then(function (response) {
						  if (_.get(response, 'data.status_code')) {
							  deferred.reject(response.data);
						  } else {
							  deferred.resolve(response.data);
						  }
                      }, function (reason) {
                          deferred.reject(reason);
                      });

            return deferred.promise;
        }

        function getBasicSettingsByUrl(apiUrl) {
            var deferred = $q.defer();

            var setting = {};
            setting.name = 'basicsettings';

            $http.post(apiUrl + '/' + moveBoardApi.core.getVariable, setting)
                 .success(function (data) {
                     deferred.resolve(data);
                 })
                 .error(function (data, status, headers, config) {
                     deferred.reject(data);
                 });

            return deferred.promise;
        }

        function uploadBranchLogo(logo) {
            var deferred = $q.defer();

            var data = {
                "image": logo
            };

            apiService.postData(moveBoardApi.branching.setBranchLogo, data)
                      .then(function (response) {
                          deferred.resolve(response.data[0]);
                      }, function (reason) {
                          deferred.reject(reason);
                      });

            return deferred.promise;
        }
    }
})();
