(function () {
    'use strict';
    
    angular.module('app.settings').factory('SmtpService', SmtpService);
    
    SmtpService.$inject = ['apiService', 'moveBoardApi'];
    
    function SmtpService(apiService, moveBoardApi) {
        return {
            getSMTPSettings: getSMTPSettings,
            testSmtp: testSmtp,
            setSMTPSettings: setSMTPSettings
        };

        //Get settings
        function getSMTPSettings() {
            var name = {name: 'site_name'};
            var mail = {name: 'site_mail'}
            return {
                smtpSettings: apiService.postData(moveBoardApi.smtpSettings.getSmtpSettings),
                siteName: getSettingVariable(name),
                siteMail: getSettingVariable(mail)
            };
        }

        //Test connection
        function testSmtp(testSMTPObj) {
            return {answer: apiService.postData(moveBoardApi.smtpSettings.testSmtp, testSMTPObj)};
        }

        //Set or update settings
        function setSMTPSettings(host, port, encryption, user, pass, siteName, siteMail) {
            return {
                swiftmailer_transport: setSetting('swiftmailer_transport', 'smtp'),
                swiftmailer_smtp_host: setSetting('swiftmailer_smtp_host', host),
                swiftmailer_smtp_port: setSetting('swiftmailer_smtp_port', port),
                swiftmailer_smtp_encryption: setSetting('swiftmailer_smtp_encryption', encryption),
                swiftmailer_smtp_username: setSetting('swiftmailer_smtp_username', user),
                swiftmailer_smtp_password: setSetting('swiftmailer_smtp_password', pass),
                site_name: setSetting('site_name', siteName),
                site_mail: setSetting('site_mail', siteMail)
            };
        }

        function setSetting(name, value) {
            var data = {
                name: name,
                value: value
            };
            return apiService.postData(moveBoardApi.settingService.setVariable, data);
        }

        function getSettingVariable(value) {
            return apiService.postData(moveBoardApi.core.getVariable, value);
        }
    }
})();