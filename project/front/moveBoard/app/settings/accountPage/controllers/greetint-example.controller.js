(function () {
    'use strict';

    angular.module('app.settings').controller('exampleGreetingBlockCtrl', exampleGreetingBlockCtrl);

    exampleGreetingBlockCtrl.$inject = ['$scope', '$uibModalInstance', 'setting'];

    function exampleGreetingBlockCtrl($scope, $uibModalInstance, setting) {
        $scope.greetings = setting;

        $scope.close = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();