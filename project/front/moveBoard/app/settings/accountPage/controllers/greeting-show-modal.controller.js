'use strict';

angular.module('app.settings').controller('showPeriodSettingsModalCtrl', showPeriodSettingsModalCtrl);

	showPeriodSettingsModalCtrl.$inject = ['$scope', '$uibModalInstance', 'setting'];

	function showPeriodSettingsModalCtrl($scope, $uibModalInstance, setting) {
		$scope.greetingsSettings = angular.copy(setting);

		$scope.save = function () {
			$uibModalInstance.close($scope.greetingsSettings);
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}
