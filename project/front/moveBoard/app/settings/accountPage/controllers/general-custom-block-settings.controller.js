angular.module('app.settings')
	.controller('ModalCustomCtrl', ModalCustomCtrl);

/* @ngInject */
function ModalCustomCtrl($scope, $uibModalInstance, currentBlock, SweetAlert, $timeout, updateBlocks) {
	$scope.checkAll = checkAll;
	$scope.changeSetting = changeSetting;

	$scope.busy = true;
	$timeout(() => {
		$scope.busy = false;
	}, 250);
	$scope.index = currentBlock.index;

	$scope.serviceType = currentBlock.where.serviceType;
	$scope.statuses = currentBlock.where.status;

	$scope.showCustomBlock = currentBlock.showCustomBlock;
	$scope.showOnConfirmationPage = currentBlock.showOnConfirmationPage;
	$scope.hideTitle = currentBlock.hideTitle;
	$scope.useForCommercial = currentBlock.useForCommercial;

	function changeSetting() {
		$scope.allServices = Object.keys($scope.serviceType).every(function(k){
			return $scope.serviceType[k].show === true;
		});

		$scope.allStatuses = Object.keys($scope.statuses).every(function(k){
			return $scope.statuses[k].show === true;
		});

		if ($scope.allServices) {
			checkAll($scope.serviceType, $scope.allServices);
		}

		if ($scope.allStatuses) {
			checkAll($scope.statuses, $scope.allStatuses);
		}
	}

	function checkAll(arr, check) {
		angular.forEach(arr, function (item) {
			item.show = check;
		});
	}

	$scope.apply = function () {
		SweetAlert.swal({
			title: 'Custom blocks will be updated.',
			type: 'info',
			showCancelButton: true,
			confirmButtonText: 'Confirm',
			closeOnConfirm: true
		}, function (isConfirm) {
			if (isConfirm) {
				currentBlock.where.serviceType = $scope.serviceType;
				currentBlock.where.status = $scope.statuses;
				currentBlock.where.all[0].serviceType = $scope.allServices;
				currentBlock.where.all[1].status = $scope.allStatuses;
				currentBlock.showCustomBlock = $scope.showCustomBlock;
				currentBlock.showOnConfirmationPage = $scope.showOnConfirmationPage;
				currentBlock.hideTitle = $scope.hideTitle;
				currentBlock.useForCommercial = $scope.useForCommercial;

				updateBlocks();

				$uibModalInstance.close();
			}
		});
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	changeSetting();
}
