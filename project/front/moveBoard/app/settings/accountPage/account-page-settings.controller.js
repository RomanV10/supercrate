'use strict';

angular
	.module('app.settings')
	.controller('AccountPageController', AccountPageController);

/* @ngInject */
function AccountPageController(SettingServices, common, datacontext) {
	const HIDE_SAWED_TIMEOUT = 1000;

	let vm = this;
	let fieldData = datacontext.getFieldData();

	vm.basicSettings = angular.fromJson(fieldData.basicsettings);
	vm.longdistance = angular.fromJson(fieldData.longdistance);
	vm.customizedTextSettings = angular.fromJson(fieldData.customized_text);
	vm.allowToPendingInfo = _.get(fieldData, 'allowToPendingInfo', 0);
	vm.faAccountSettings = vm.basicSettings.flatRateAccountSettings.accountSettings;
	vm.longdistance.isConfirmedAccountMenuEnabled = _.get(vm.longdistance, 'isConfirmedAccountMenuEnabled', true);

	vm.menu =
		[
			{
				name: 'Account Settings',
				url: './app/settings/accountPage/templates/greeting-setting.html',
				selected: true,
				icon: 'fa fa-key'
			},
			{
				name: 'Flat Rate Account Settings',
				url: './app/settings/accountPage/templates/flat-rate-settings.html',
				selected: true,
				icon: 'fa fa-truck'
			},
			{
				name: 'Quote Explanation',
				url: './app/settings/accountPage/templates/quote-explanations.html',
				selected: true,
				icon: 'fa fa-file-text-o'
			},
			{
				name: 'General Custom Block',
				url: './app/settings/accountPage/templates/custom-block.html',
				selected: true,
				icon: 'fa fa-star-o'
			},
			{
				name: 'Custom Blocks',
				url: './app/settings/accountPage/templates/other-custom-blocks.html',
				selected: true,
				icon: 'fa fa-bookmark-o'
			},
			{
				name: 'Custom Tooltips',
				url: './app/settings/accountPage/templates/custom-tooltip.html',
				selected: true,
				icon: 'fa fa-info'
			},
			{
				name: 'Moving Insurance',
				url: './app/settings/accountPage/templates/moving-insurance.tmpl.html',
				selected: true,
				icon: 'fa fa-pencil-square-o'
			},
			{
				name: 'Progress Bar',
				url: './app/settings/accountPage/templates/progress-bar.html',
				selected: true,
				icon: 'fa fa-tasks'
			},
			{
				name: 'FAQ',
				url: './app/settings/accountPage/templates/faq.tmpl.html',
				selected: true,
				icon: 'fa fa-question'
			},
			{
				name: 'Pending-Info Settings',
				url: './app/settings/accountPage/templates/pending-info-settings.tpl.html',
				selected: true,
				icon: 'fa fa-clock-o'
			},
			{
				name: 'Inventory & Details Setting',
				url: './app/settings/accountPage/templates/inventory-settings.html',
				selected: true,
				icon: 'fa fa-list-ol'
			},
			{
				name: 'Account Top Text',
				url: './app/settings/accountPage/templates/account-top-text.html',
				selected: true,
				icon: 'fa fa-list-ol'
			},
			{
				name: 'Estimate Signatures',
				url: './app/settings/accountPage/templates/extra-signatures.html',
				selected: true,
				icon: 'fa icon-docs',
			},
		];

	vm.saveSettings = function (type, value) {
		vm.saving = true;
		var setting = vm.basicSettings;
		var _setting_name = 'basicsettings';

		if (angular.isDefined(type)) {
			_setting_name = type;
		}

		if (angular.isDefined(value)) {
			setting = value;
		}

		var promise = SettingServices.saveSettings(setting, _setting_name);
		promise.then(function () {
			vm.saving = false;

			common.$timeout(function () {
				vm.saved = false;
			}, HIDE_SAWED_TIMEOUT);

			vm.saved = true;
		});

		return promise;
	};

	vm.select = function (menu) {
		angular.forEach(vm.menu, function (menu) {
			menu.selected = false;
		});

		menu.selected = true;
	};
}
