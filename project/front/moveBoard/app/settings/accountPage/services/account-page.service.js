'use strict';

angular.module('app.settings').factory('AccountPageService', AccountPageService);

AccountPageService.$inject = ['apiService', 'moveBoardApi', '$q'];

function AccountPageService(apiService, moveBoardApi, $q) {
	return {
		updateSetting: updateSetting,
		getSetting: getSetting
	};

	function updateSetting(settingName, settingData) {
		let data = {
			name: settingName,
			value: angular.toJson(settingData)
		};
		return apiService.postData(moveBoardApi.settingService.setVariable, data);
	}
	
	function getSetting(settingName) {
		let defer = $q.defer();
		let data = {
			name: settingName
		};
		apiService.postData(moveBoardApi.core.getVariable, data).then(resolve => {
			let decodedSettings = angular.fromJson(resolve.data[0] ? resolve.data[0] : resolve.data);
			defer.resolve(decodedSettings);
		}, reject => {
			defer.reject(reject);
		});
		
		return defer.promise;
	}
}