export const BASE_GENERAL_CUSTOM_BLOCK = {
	title: '',
	body: '',
	showOnConfirmationPage: false,
	showCustomBlock: false,
	hideTitle: false,
	useForCommercial: false,
	index: 0,
	where: {
		serviceType: [
			{name: 'Moving', show: false, service_type: 1},
			{name: 'Moving & Storage', show: false, service_type: 2},
			{name: 'Loading Help', show: false, service_type: 3},
			{name: 'Unloading Help', show: false, service_type: 4},
			{name: 'Flat Rate', show: false, service_type: 5},
			{name: 'Overnight', show: false, service_type: 6},
			{name: 'Long Distance', show: false, service_type: 7},
			{name: 'Packing Day', show: false, service_type: 8},
		],
		status: [
			{name: 'Pending', show: false, request_status: 1},
			{name: 'Not Confirmed', show: false, request_status: 2},
			{name: 'Confirmed', show: false, request_status: 3}
		],
		all: [
			{serviceType: false},
			{status: false}
		]
	}
};

export const BASE_CUSTOM_BLOCK = {
	body: "",
	hideTitle: false,
	showCustomBlock: false,
	showOnConfirmationPage: false,
	title: ""
};

export const GCB_SETTING_NAME = 'general_custom_block';
export const CM_SETTING_NAME = 'custom_block';

export const CUSTOM_BLOCK_TABS = [
	{name: 'Pending Information', selected: true},
	{name: 'Not Confirmed Information', selected: false},
	{name: 'Confirmed Information', selected: false}
];
