'use strict';

import {
    BASE_CUSTOM_BLOCK,
    CM_SETTING_NAME,
	CUSTOM_BLOCK_TABS
} from '../constants/account-page.constants';
import './usual-custom-block.styl';

angular
	.module('app.settings')
	.directive('movingCustomBlock', movingCustomBlock);

movingCustomBlock.$inject = ['SweetAlert', 'AccountPageService'];

function movingCustomBlock(SweetAlert, AccountPageService) {
	return {
		restrict: 'E',
		template: require('./usual-custom-block.tpl.pug'),
		scope: {
			basicSettings: '='
		},
		link: linker
	};
	
	function linker($scope) {
		
		$scope.select = select;
		$scope.addNewBlock = addNewBlock;
		$scope.removeBlock = removeBlock;
		$scope.saveCustomBlockSettings = saveCustomBlockSettings;
		
		$scope.saved = false;
		$scope.services = {
			1: {name: "Moving", tabs: angular.copy(CUSTOM_BLOCK_TABS), index: 0},
			2: {name: 'Moving & Storage', tabs: angular.copy(CUSTOM_BLOCK_TABS), index: 1},
			3: {name: 'Loading Help', tabs: angular.copy(CUSTOM_BLOCK_TABS), index: 2},
			4: {name: 'Unloading Help', tabs: angular.copy(CUSTOM_BLOCK_TABS), index: 3},
			5: {name: 'Flat Rate', tabs: angular.copy(CUSTOM_BLOCK_TABS), index: 4},
			6: {name: 'Overnight', tabs: angular.copy(CUSTOM_BLOCK_TABS), index: 5},
			7: {name: 'Long Distance', tabs: angular.copy(CUSTOM_BLOCK_TABS), index: 6},
			8: {name: 'Packing Day', tabs: angular.copy(CUSTOM_BLOCK_TABS), index: 7},
			9: {name: 'Commercial Move', tabs: angular.copy(CUSTOM_BLOCK_TABS), index: 8},
		};
		
		// need for integration tests
		$scope.testClasses = [];
		angular.forEach($scope.services, function (item) {
			// remove white spaces and ampersands for testers
			// Example: 'Moving & Storage' => 'movingstorage'
			$scope.testClasses.push(item.name.toLowerCase().replace(/(\s+&\s+|\s+)/g,''));
		});
		
		function init() {
			let currentCustomBlocks = {1: [], 2: [], 3: []};
			
			AccountPageService.getSetting(CM_SETTING_NAME).then(function (response) {
				if (!_.get(response, 'customBlockMerged', false)) {
					
					saveCustomBlockSettings({
						customBlockMerged: true,
						customBlocks: $scope.basicSettings.customBlock.perServiceType
					});
					return;
				}
				
				$scope.customBlockSettings = response;
				if (_.isUndefined(response.customBlocks['8'])) {
					$scope.customBlockSettings.customBlocks['8'] = angular.copy(currentCustomBlocks);
				}
				if (_.isUndefined(response.customBlocks['9'])) {
					$scope.customBlockSettings.customBlocks['9'] = angular.copy(currentCustomBlocks);
				}
				angular.forEach($scope.customBlockSettings.customBlocks, function (item) {
					currentCustomBlocks[1].push(item[1]);
					currentCustomBlocks[2].push(item[2]);
					currentCustomBlocks[3].push(item[3]);
				});
				$scope.movingPendingBody = currentCustomBlocks[1];
				$scope.movingNotConfirmedBody = currentCustomBlocks[2];
				$scope.movingConfirmedBody = currentCustomBlocks[3];
			})
		}
		
		function select(service, tab) {
			angular.forEach(service.tabs, function (item) {
				item.selected = false;
			});
			service.tabs[tab].selected = true;
		}
		
		function addNewBlock(arr) {
			var newBlock = angular.copy(BASE_CUSTOM_BLOCK);
			arr.push(newBlock);
			saveCustomBlockSettings();
		}
		
		function removeBlock(block, arr) {
			SweetAlert.swal({
				title: "This block will be removed.",
				text: "Are you sure?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#E47059",
				confirmButtonText: "Confirm",
				closeOnConfirm: true
			}, function (isConfirm) {
				if (isConfirm) {
					arr.splice(arr.indexOf(block), 1);
					saveCustomBlockSettings();
					toastr.info('Block was removed.');
				}
			});
		}
		
		function saveCustomBlockSettings(setting = $scope.customBlockSettings) {
			$scope.saved = true;
			AccountPageService.updateSetting(CM_SETTING_NAME, setting).then(function(){
				$scope.saved = false;
				init();
			});
		}
		
		init();
		
		$scope.$on('$destroy', function () {
			saveCustomBlockSettings();
		});
		
	}
}
