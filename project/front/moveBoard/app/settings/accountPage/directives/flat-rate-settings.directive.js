(function () {
    'use strict';

    angular.module('app.settings').directive('flatRateSettings', flatRateSettings);

    flatRateSettings.$inject = ['$uibModal'];

    function flatRateSettings($uibModal) {
        return {
            restrict: 'E',
            scope: {
                saveSettings: '=onSaveBasicSettings',
                basicSettings: '=',
            },
	        template: require('../templates/greeting-settings/greeting-settings.tmpl.html'),
            link: flatRateLink
        };

        function flatRateLink($scope, attr, elem) {
            var flatRateAccountSettings = $scope.basicSettings.flatRateAccountSettings;

            $scope.greetingsSettings = flatRateAccountSettings.greetingText;
            $scope.greetingsParserSettings = flatRateAccountSettings.greetingParserText;
            $scope.greetingsNewUserSettings = flatRateAccountSettings.greetingNewUser;


            $scope.tabs = [
                {name: 'New User Greeting', selected: true},
                {name: 'Parser Greeting', selected: false},
                {name: 'Flat Rate Greeting', selected: false},
            ];

            $scope.select = function (tab) {
                angular.forEach($scope.tabs, function (item) {
                    item.selected = false;
                });
                tab.selected = true;
            };

            $scope.saved = false;

            $scope.saveFlatRateSettings = function () {
                $scope.saveSettings('basicsettings', $scope.basicSettings);
                toastr.success('Settings was saved!');
            };

            $scope.showExample = function (setting) {
                var exampleWindow = $uibModal.open({
	                template: require('../templates/greeting-settings/example-greeting.html'),
                    controller: 'exampleGreetingBlockCtrl',
                    backdrop: true,
                    size: 'lg',
                    resolve: {
                        setting: function () {
                            return setting;
                        }
                    }
                });

                exampleWindow.result.then(function () {});
            };

			$scope.openShowPeriodSettingsModal = function (setting) {
				var generalCustomModal = $uibModal.open({
					template: require('./../templates/custom-block-tmpl/greeting-custom.pug'),
					controller: 'showPeriodSettingsModalCtrl',
					backdrop: true,
					size: 'sm',
					resolve: {
						setting: function () {
							return flatRateAccountSettings.greetingText;
						}
					}
				});
				generalCustomModal.result.then(function (setting) {
					$scope.greetingsSettings = setting;
					$scope.basicSettings.flatRateAccountSettings.greetingText = setting;
					$scope.saveFlatRateSettings();
				});
			};
        }
    }
})();
