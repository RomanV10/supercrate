(function () {
    'use strict';
    angular
        .module('app.settings')
        .directive('customTooltip', customTooltip);
    customTooltip.$inject = [];
    function customTooltip() {
        return {
            restrict: 'E',
            template: require('./tooltip.tpl.pug'),
            scope: {
                saveSettings: '=onSaveBasicSettings',
                basicSettings: '='
            },
            link: tooltip
        };

        function tooltip($scope) {
	        $scope.tooltips = $scope.basicSettings.tooltipsSettings;
            let origin = true;
            let originalTooltips = angular.copy($scope.tooltips);
	        $scope.saveCustomTooltipSettings = saveCustomTooltipSettings;
	        $scope.checkTooltipsChanges = checkTooltipsChanges;


            $scope.title = {
                "jobTime" : "Job Time Tooltip",
                "laborTime" : "Labor Time Tooltip",
                "travelTime" : "Travel Time Tooltip",
                "fuelSurchrge" : "Fuel Surcharge Tooltip",
                "myselfPacking" : "Myself Packing Tooltip",
                "partialPacking" : "Partial Packing Tooltip",
                "fullPacking" : "Full Packing Tooltip",
            };
            
            function checkTooltipsChanges(text, field) {
                if (text != originalTooltips[field].value) {
                    origin = false;
                } else {
                    origin = true;
                }
            }

             function saveCustomTooltipSettings () {
                originalTooltips = angular.copy($scope.tooltips);
                origin = true;
                $scope.saveSettings('basicsettings', $scope.basicSettings);
                toastr.success('Success', 'Tooltip was saved');
            }

            $scope.$on('$destroy', function () {
                if (!origin) {
	                saveCustomTooltipSettings();
                }
            });

        }
    }
})();