'use strict';

angular.module('app.settings')
	.directive('quoteExplanationDirective', quoteExplanationDirective);

function quoteExplanationDirective() {
	return {
		restrict: 'E',
		template: require('../templates/quote-explanation-tmpl/quote-explanation.tmpl.html'),
		scope: {
			quoteExplanationSettings: '=',
			saveSettings: '=',
		},
		link: quoteExplanationLink
	};

	function quoteExplanationLink($scope) {

		/*
		 * Here there is a shift in the service type, as Long Distance is not in these settings.
		 * Displacement after service type 7.
		 * */

		let icon = 'fa fa-file-text-o';

		$scope.tabs = [
			{
				name: 'Moving',
				selected: true,
				icon: icon,
				data: $scope.quoteExplanationSettings.quoteExplanation['1']
			},
			{
				name: 'Moving & Storage',
				selected: true,
				icon: icon,
				data: $scope.quoteExplanationSettings.quoteExplanation['2']
			},
			{
				name: 'Loading Help',
				selected: true,
				icon: icon,
				data: $scope.quoteExplanationSettings.quoteExplanation['3']
			},
			{
				name: 'Unloading Help',
				selected: true,
				icon: icon,
				data: $scope.quoteExplanationSettings.quoteExplanation['4']
			},
			{
				name: 'Flat Rate',
				selected: true,
				icon: icon,
				data: $scope.quoteExplanationSettings.quoteExplanation['5']
			},
			{
				name: 'Overnight',
				selected: true,
				icon: icon,
				data: $scope.quoteExplanationSettings.quoteExplanation['6']
			},
			{
				name: 'Packing Day',
				selected: true,
				icon: icon,
				data: $scope.quoteExplanationSettings.quoteExplanation['7']
			},
			{
				name: 'Commercial Move',
				selected: true,
				icon: icon,
				data: $scope.quoteExplanationSettings.quoteExplanation['8']
			},
		];

		// need for integration tests
		$scope.testClasses = [];

		angular.forEach($scope.tabs, (item) => {
			// remove white spaces and ampersands for testers
			// Example: 'Moving & Storage' => 'movingstorage'
			$scope.testClasses.push(item.name.toLowerCase().replace(/(\s+&\s+|\s+)/g, ''));
		});

		$scope.saveQuoteExplanation = (explanation, key) => {
			$scope.quoteExplanationSettings.quoteExplanation[key + 1] = explanation.data;
			$scope.saveSettings('customized_text', $scope.quoteExplanationSettings);
			toastr.success('Quote Explanation was updated');
		};
	}
}
