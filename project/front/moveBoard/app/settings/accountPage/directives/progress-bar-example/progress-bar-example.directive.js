'use strict';

import './progress-bar-example.styl';

angular
	.module('app.settings')
	.directive('progressBarExample',  progressBarExample);

function progressBarExample() {
	return {
		restrict: 'E',
		template: require('./progress-bar-example.tpl.pug'),
		scope: {
			isOptional: '<',
		}
	}
}
