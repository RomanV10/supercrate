'use strict';

angular
	.module('app.settings')
	.directive('accountTopText', accountTopText);

/* @ngInject */
function accountTopText(serviceTypeService) {
	return {
		restrict: 'E',
		scope: {
			saveSettings: '=onSaveBasicSettings',
			basicSettings: '=',
		},
		template: require('./account-top-text.html'),
		link: accountTopTextLink
	};

	function accountTopTextLink($scope) {
		$scope.saveFlatRateSettings = saveFlatRateSettings;

		let flatRateAccountSettings = $scope.basicSettings.flatRateAccountSettings;
		if (!flatRateAccountSettings.greetingText.topTextSettings) {
			flatRateAccountSettings.greetingText.topTextSettings = [];
			$scope.saveFlatRateSettings();
		}
		$scope.topTextSettings = flatRateAccountSettings.greetingText.topTextSettings;

		$scope.serviceTypes = serviceTypeService.getAllServiceTypes();

		function saveFlatRateSettings() {
			$scope.saveSettings('basicsettings', $scope.basicSettings)
				.then(() => {
					toastr.success('Settings was saved!');
				})
				.catch(() => {
					toastr.error('Settings was NOT saved!');
				});
		}

	}
}
