'use strict';

import {
	BASE_GENERAL_CUSTOM_BLOCK,
	GCB_SETTING_NAME
} from '../constants/account-page.constants'; //constants for GCB

angular
	.module('app.settings')
	.directive('generalCustomBlock', generalCustomBlock);

generalCustomBlock.$inject = ['SweetAlert', 'AccountPageService', '$uibModal'];

function generalCustomBlock(SweetAlert, AccountPageService, $uibModal) {
	return {
		restrict: 'E',
		template: require('../templates/custom-block-tmpl/general-custom.html'),
		scope: {
			basicSettings: '='
		},
		link: link
	};

	function link($scope, element) {
		$scope.saved = false;
		$scope.generalBlock = [];

		$scope.tooltipButtons = [
			{'name': 'Client Reservation Signature', 'value': '[estimate-signature:client-reservation-signature]'},
			{'name': 'Sales Signature', 'value': '[estimate-signature:sales-signature]'},
		];

		$scope.updateBlocks = updateBlocks;
		$scope.addNewGeneral = addNewGeneral;
		$scope.replaceBlock = replaceBlock;
		$scope.openCustomBlockSettings = openCustomBlockSettings;
		$scope.removeNewGeneral = removeNewGeneral;
		$scope.insertTextInInput = insertTextInInput;

		function getAllBlocks() {
			AccountPageService.getSetting(GCB_SETTING_NAME).then(function (response) {
				if (!_.get(response, 'isMergedSettings', false)) {
					updateBlocks({
						isMergedSettings: true,
						generalCustomBlock: $scope.basicSettings.customBlock.generalCustomBlock
					});
				} else {
					$scope.generalCustomSettings = response;
					$scope.generalBlock = response.generalCustomBlock;
				}
			});
		}

		function updateBlocks(setting = $scope.generalCustomSettings) {
			$scope.saved = true;
			AccountPageService.updateSetting(GCB_SETTING_NAME, setting).then(function () {
				$scope.saved = false;
				getAllBlocks();
			});
		}

		function addNewGeneral() {
			let newBlock = BASE_GENERAL_CUSTOM_BLOCK;
			newBlock.index = $scope.generalBlock.length;
			$scope.generalBlock.push(newBlock);
			updateBlocks();
		}

		function replaceBlock(oldValue, newValue) {
			$scope.generalBlock.splice(newValue, 0 , $scope.generalBlock.splice(oldValue,1)[0]);
			angular.forEach($scope.generalBlock, function (item, key) {
				item.index = key;
			});
			if (oldValue < newValue) {
				toastr.info('Block was moved down');
			} else {
				toastr.info('Block was moved up');
			}
			updateBlocks();
		}

		function removeNewGeneral(block, arr) {
			SweetAlert.swal({
				title: 'This block will be removed.',
				text: 'Are you sure?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#E47059',
				confirmButtonText: 'Confirm',
				closeOnConfirm: true
			}, function (isConfirm) {
				if (isConfirm) {
					arr.splice(arr.indexOf(block), 1);
					angular.forEach(arr, function (item, key) {
						item.index = key;
					});
					updateBlocks();
					toastr.info('Block was removed.');
				}
			});
		}

		function openCustomBlockSettings(block) {
			$uibModal.open({
				template: require('../templates/custom-block-tmpl/general-settings.pug'),
				controller: 'ModalCustomCtrl',
				backdrop: false,
				size: 'md',
				resolve: {
					currentBlock: () => block,
					updateBlocks: () => updateBlocks,
				}
			});
		}

		getAllBlocks();


		function insertTextInInput(text) {
			let textEditor = findChildChildElementByPartId(element, 'DIV', 'taTextElement');
			textEditor.focus();
			insertTextAtCursor(text);
		}

		function findChildChildElementByPartId(baseElement, tag, partId) {
			let elementsByTag = baseElement.find(tag);

			for (let i = 0; i < elementsByTag.length; i++) {
				let wrapElement = angular.element(elementsByTag[i]);
				let id = wrapElement.attr('id');
				let tagName = wrapElement.prop('tagName');

				if (angular.isDefined(id) && angular.equals(tag, tagName) && id.indexOf(partId) >= 0) {
					return wrapElement;
				}
			}
		}

		function insertTextAtCursor(text) {
			setTimeout(function () {
				let sel, range;

				if (window.getSelection) {
					sel = window.getSelection();
					if (sel.getRangeAt && sel.rangeCount) {
						range = sel.getRangeAt(0);
						range.deleteContents();
						range.insertNode(document.createTextNode(text));
					}
				} else if (document.selection && document.selection.createRange) {
					document.selection.createRange().text = text;
				}
			}, 10);
		}
	}
}
