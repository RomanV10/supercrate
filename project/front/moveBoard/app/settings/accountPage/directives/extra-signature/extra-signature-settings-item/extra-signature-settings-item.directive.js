import './extra-signature-settings-item.styl';

angular.module('app.settings')
	.directive('extraSignatureSettingItem', extraSignatureSettingItem);

/* @ngInject */
function extraSignatureSettingItem() {
	return {
		restrict: 'E',
		template: require('./extra-signature-settings-item.html'),
		link,
	};

	function link($scope) {

	}
}