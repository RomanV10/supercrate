import './extra-signatures.styl';
import defaultSettings from './extra-signatures-settings-default.json';
import templateBuilderButtons from '../../../../template-builder/tooltip-data/templateBuilderButtons.json';
import tooltipButtons from '../../../../template-builder/tooltip-data/tooltipButtons.json';
const SETTINGS_NAME = 'extraSignaturesSettings';

angular.module('app.settings')
	.directive('extraSignaturesSetting', extraSignaturesSetting);

/* @ngInject */
function extraSignaturesSetting(apiService, moveBoardApi) {
	return {
		restrict: 'E',
		scope: {},
		template: require('./extrat-signatures.html'),
		link,
	};

	function link($scope, element) {
		$scope.tooltipButtons = [...templateBuilderButtons, ...tooltipButtons];
		$scope.tooltipButtons.push(
			{'name': 'Sales Estimate Signatures', 'value': '[estimate-signature:sales]'},
			{'name': 'Client Estimate Signatures', 'value': '[estimate-signature:client]'}
		);

		$scope.textAngularToolbar = [['bold','italics', 'underline', 'strikeThrough'],
			['ul', 'ol'], ['justifyLeft', 'justifyCenter', 'justifyRight'],
			['fontColor', 'fontName', 'fontSize'],
			['insertLink', 'insertImage', 'insertCustom'],
			['undo', 'redo', 'clear', 'html']];

		$scope.saveSetting = saveSetting;
		$scope.insertTextInInput = insertTextInInput;
		$scope.$on('saveExtraSignaturesSetting', saveSetting);

		loadSettings();

		function loadSettings() {
			$scope.busy = true;

			apiService.postData(moveBoardApi.settingService.getVariable, {name: SETTINGS_NAME})
				.then(({data}) => {
					if (data) {
						let settings = data[0];
						$scope.settings = angular.fromJson(settings);
					} else {
						$scope.settings = angular.copy(defaultSettings);
					}
				})
				.finally(() => {
					$scope.busy = false;
				});
		}

		function saveSetting() {
			$scope.saving = true;

			apiService.postData(moveBoardApi.settingService.setVariable, {name: SETTINGS_NAME, value: angular.toJson($scope.settings)})
				.then(() => {
					$scope.saving = false;
					toastr.success('Estimate signatures settings saved', 'Success', '');
				})
				.catch(() => {
					toastr.error('Cannot save estimate signatures settings', 'Error', '');
				});
		}

		function insertTextInInput(text) {
			let textEditor = findChildChildElementByPartId(element, 'DIV', 'taTextElement');
			textEditor.focus();
			insertTextAtCursor(text);
		}

		function findChildChildElementByPartId(baseElement, tag, partId) {
			let elementsByTag = baseElement.find(tag);

			for (let i = 0; i < elementsByTag.length; i++) {
				let wrapElement = angular.element(elementsByTag[i]);
				let id = wrapElement.attr('id');
				let tagName = wrapElement.prop('tagName');

				if (angular.isDefined(id) && angular.equals(tag, tagName) && id.indexOf(partId) >= 0) {
					return wrapElement;
				}
			}
		}

		function insertTextAtCursor(text) {
			setTimeout(function () {
				let sel, range;

				if (window.getSelection) {
					sel = window.getSelection();
					if (sel.getRangeAt && sel.rangeCount) {
						range = sel.getRangeAt(0);
						range.deleteContents();
						range.insertNode(document.createTextNode(text));
					}
				} else if (document.selection && document.selection.createRange) {
					document.selection.createRange().text = text;
				}
			}, 10);
		}
	}
}
