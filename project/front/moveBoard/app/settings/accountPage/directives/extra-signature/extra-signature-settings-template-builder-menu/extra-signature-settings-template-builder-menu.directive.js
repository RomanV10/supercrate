angular
	.module('app.settings')
	.directive('extraSignatureSettingsTemplateBuilderMenu', extraSignatureSettingsTemplateBuilderMenu);

/* @ngInject */
function extraSignatureSettingsTemplateBuilderMenu($uibModal, $timeout) {
	return {
		restrict: 'E',
		scope: {
			template: '=',
		},
		template: require('./extra-signature-settings-template-builder-menu.html'),
		link,
	};

	function link($scope) {
		$scope.openMenu = openMenu;

		function openMenu() {
			$uibModal
				.open({
					template: require('./extra-signature-settings-template-builder-menu-modal.html'),
					controller: 'extraSignatureSettingsTemplateBuilderMenuModalController',
					controllerAs: 'vm',
					backdrop: false,
					size: 'lg',
				})
				.result
				.then((template) => {
					$scope.template = template;

					$timeout(() => {
						$scope.$emit('saveExtraSignaturesSetting');
					});
				});
		}
	}
}