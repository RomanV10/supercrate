import './extra-signature-settings-template-builder-menu-modal.styl';

angular
	.module('app.settings')
	.controller('extraSignatureSettingsTemplateBuilderMenuModalController', extraSignatureSettingsTemplateBuilderMenuModalController);

/* @ngInject */
function extraSignatureSettingsTemplateBuilderMenuModalController($scope, EmailBuilderRequestService, TemplateBuilderService, logger, $q, $uibModalInstance) {
	const EXTRA_SIGNATURES_TEMPLATE_SELECTOR = '.extra-signature-settings-template-builder-menu-modal__body__template-preview_block';
	let vm = this;

	vm.template = {blocks: []};
	vm.templatesMenu = [];

	vm.chooseTemplate = chooseTemplate;
	$scope.cancel = cancel;
	$scope.save = save;

	loadTemplates();

	function loadTemplates() {
		vm.busy = true;
		let templateFolders = EmailBuilderRequestService.getTemplateFolders();
		let templates = EmailBuilderRequestService.getTemplates();

		$q.all({templateFolders, templates})
			.then(({templates, templateFolders}) => {
				TemplateBuilderService
					.createTemplateMenuObject(templates.data, templateFolders)
					.then(function (menu) {
						vm.templatesMenu = menu;
					});
			}, function (error) {
				logger.error('Error when loading a template!', JSON.stringify(error), 'Error');
			})
			.finally(() => {
				vm.busy = false;
			});
	}

	function chooseTemplate(template) {
		vm.template = angular.copy(template);
	}

	function cancel() {
		$uibModalInstance.dismiss('cancel');
	}

	function save() {
		vm.template.template = TemplateBuilderService.getCurrentHTMLTemplate(EXTRA_SIGNATURES_TEMPLATE_SELECTOR);

		if (vm.template.template) {
			$uibModalInstance.close(vm.template.template);
		} else {
			cancel();
		}
	}
}