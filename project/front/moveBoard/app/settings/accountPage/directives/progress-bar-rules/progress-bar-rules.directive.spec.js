describe('Progress bar settings', function () {
	var ele, scope;
	let elementScope,
		$q,
		deferred,
		SettingServices,
		config;

	function compileElement(elementHtml) {
		ele = angular.element(elementHtml);
		$compile(ele)(scope);
		elementScope = ele.scope();
	}

	beforeEach(inject(function (_$q_, _SettingServices_, _config_) {
		$q = _$q_;
		$rootScope.fieldData.leadscoringsettings = {};
		$rootScope.fieldData.progressbarsettings = testHelper.loadJsonFile('progress-bar-settings.mock');
		$rootScope.fieldData.field_lists = {
			field_move_service_type: {}
		};
		scope = $rootScope.$new();
		SettingServices = _SettingServices_;
		config = _config_;
		deferred = $q.defer();
		compileElement('<progress-bar-rules></progress-bar-rules>');
	}));

	/**
	 * @description
	 * Sample test case to check if HTML rendered by the directive is non empty
	 * */
	describe('Directive DOM', () => {
		it('should not render empty html', function () {
			expect(ele.html()).not.toBe('');
		});

	});

	describe('Save settings', () => {
		beforeEach(function () {
			spyOn(SettingServices, 'saveSettings').and.returnValue(deferred.promise);
		});
		it('when leadscoringsettings is empty', function () {
			elementScope.progressBarSettings = {
				enabled: true,
				serviceTypes: [
					{
						id: 1,
						isOptional: true
					},
					{
						id: 5,
						isOptional: false
					}
				]
			};
			elementScope.saveSettings();
			expect(SettingServices.saveSettings).toHaveBeenCalled();
			expect(SettingServices.saveSettings.calls.count()).toBe(1);
		});
		it('should do something on success', function () {
			deferred.resolve(); // Resolve the promise to replicate success scenario.
			elementScope.$apply();
			// Check for state on success.
			expect(elementScope.saving).toBeFalsy(); //testing properties
		});
	});
});
