'use strict';

import './progress-bar-rules.styl';

angular
	.module('app.settings')
	.directive('progressBarRules',  progressBarRules);
progressBarRules.$inject = ['SettingServices', 'datacontext', '$timeout', 'CheckRequestService'];

function progressBarRules(SettingServices, datacontext, $timeout, CheckRequestService) {
	return {
		restrict: 'E',
		template: require('./progress-bar-rules.tpl.pug'),
		link: lincFunc
	};
	function lincFunc($scope) {

		const SETTING_NAME = 'progressbarsettings';
		let saveDellay;
		$scope.saveSettings = saveSettings;

		function init() {
			let fieldData = datacontext.getFieldData();
			$scope.serviceTypesWithValidTitles = CheckRequestService.getServiceTypesWithValidTitles();
			$scope.progressBarSettings = angular.fromJson(fieldData.progressbarsettings);

			let arrayLength = $scope.progressBarSettings.serviceTypes.length;
			$scope.firstColumn = _.slice($scope.progressBarSettings.serviceTypes, 0,  _.round(arrayLength / 2));
			$scope.secondColumn = _.slice($scope.progressBarSettings.serviceTypes, _.ceil(arrayLength / 2), arrayLength);

		}
		function saveSettings() {
			$scope.saving = true;
			$scope.saved = false;
			$timeout.cancel(saveDellay);
			let promise = SettingServices.saveSettings($scope.progressBarSettings, SETTING_NAME);
			promise.then(function () {
				$scope.saving = false;
				saveDellay = $timeout(function () {
						$scope.saved = false;
					}, 1000);
					$scope.saved = true;
			});
		}

		init();

		$scope.$on('destroy', function() {
			$timeout.cancel(saveDellay);
		});
	}
}
