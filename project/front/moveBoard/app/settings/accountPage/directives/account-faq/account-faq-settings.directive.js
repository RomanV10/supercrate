'use strict';
import './account-faq-settings.styl';

angular
	.module('app.settings')
	.directive('accountFaqSettings', accountFaqSettings);

	accountFaqSettings.$inject = ['SweetAlert', 'moveBoardApi', 'apiService', '$uibModal'];

	function accountFaqSettings(SweetAlert, moveBoardApi, apiService, $uibModal){
		return {
			restrict: 'E',
			template: require('./account-faq-settings.pug'),
			scope: {
				basicSettings: '=',
				saveBasicSettings: '='
			},
			link: linkFunction
		};

		function linkFunction($scope) {
			$scope.busy = false;

			$scope.saveBasicSettingsFn = saveBasicSettingsFn;
			$scope.getFaqByUsefulness = getFaqByUsefulness;
			$scope.remove = remove;
			$scope.openCreateModal = openCreateModal;
			$scope.openEditModal = openEditModal;

			$scope.questionArr = [];

			$scope.sortOptions = [
				{
					title: 'default'
				},
				{
					title: 'by usefulness',
					value: '-usefulness_upvote'
				},
				{
					title: 'by uselessness',
					value: '-usefulness_downvote'
				},
			];

			function  init() {
				getFaq();
			}

			function saveBasicSettingsFn(){
				$scope.saveBasicSettings('basicsettings', $scope.basicSettings);
			}

			function create(item) {
				$scope.busy = true;
				let req = {
					'faq_question': item.faq_question,
					'faq_answer': item.faq_answer
				};
				apiService.postData(moveBoardApi.faq.create, req).then(data => {
					item.id = data.data[0];
					$scope.questionArr.splice(0,0,item);
					$scope.busy = false;
					toastr.success('Item was added!');
					item.usefulness_upvote = 0;
					item.usefulness_downvote = 0;
				});
			}

			function getFaq() {
				$scope.busy = true;
				apiService.getData(moveBoardApi.faq.get).then(res => {
					$scope.questionArr = res.data;
					$scope.busy = false;
				});
			}

			function getFaqByUsefulness() {
				$scope.busy = true;
				apiService.postData(moveBoardApi.faq.getByUsefulness).then(res => {
					$scope.questionArr = res.data;
					$scope.busy = false;
				});
			}

			function update(item){
				$scope.busy = true;
				let params = {
					'id': item.id,
					'faq_question': item.faq_question,
					'faq_answer': item.faq_answer
				};
				apiService.putData(moveBoardApi.faq.update, params)
					.then(data => {
						toastr.success('Change was saved!');
						$scope.busy = false;
					});
			}

			function remove(item) {
				SweetAlert.swal({
					title: "This block will be removed.",
					text: "Are you sure?",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#E47059",
					confirmButtonText: "Confirm",
					closeOnConfirm: true
				}, function (isConfirm) {
					if (isConfirm) {
						$scope.busy = true;
						let params = {
							id: item.id
						};
						apiService.delData(moveBoardApi.faq.delete, params).then(data => {
							let index = _.findIndex($scope.questionArr, (question) => question.id == item.id)
							$scope.questionArr.splice(index, 1);
							toastr.warning('Removed!');
							$scope.busy = false;
						});
					}
				});
			}

			function openCreateModal() {
				let item = {
					'faq_question': '',
					'faq_answer': ''
				};
				openFaqBlockModal(item);
			}

			function openEditModal(item) {
				openFaqBlockModal(item);
			}

			function openFaqBlockModal(item) {
				let itemModal = $uibModal.open({
					template: require('./account-faq-modal.tmpl.pug'),
					controller: 'accountFaqModalCtrl',
					backdrop: 'static',
					size: 'lg',
					resolve: {
						item: function () {
							return item;
						}
					}
				});
				itemModal.result.then(function (editedItem) {
					if(editedItem.id){
						item.faq_question = editedItem.faq_question;
						item.faq_answer = editedItem.faq_answer;
						update(editedItem);
					}else{
						create(editedItem);
					}
				});
			}

			init();
		}
	}
