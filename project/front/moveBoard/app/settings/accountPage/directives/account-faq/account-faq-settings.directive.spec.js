describe('Directive: accountFaqSettings', function () {
	var element, scope;
	let $httpBackend,
		moveBoardApi,
		testHelper,
		elementScope,
		$rootScope,
		$timeout,
		compile,
		spy;

	function compileElement(elementHtml) {
		element = compile(angular.element(elementHtml))(scope);
		elementScope = element.isolateScope();
	}

	beforeEach(inject(function (_$compile_, _$rootScope_, _$httpBackend_, _testHelperService_, _moveBoardApi_, _$timeout_) {
		testHelper = _testHelperService_;
		$rootScope = _$rootScope_;
		scope = _$rootScope_.$new();
		compile = _$compile_;
		$timeout = _$timeout_;
		$httpBackend = _$httpBackend_;
		moveBoardApi = _moveBoardApi_;

		scope.basicSettings = {
			faqAccount: false
		};

		scope.saveBasicSettings = function(a, b) {};

		let faq = testHelper.loadJsonFile('moveBoard/account-faq-settings.mock');

		$httpBackend.expectGET(testHelper.makeRequest(moveBoardApi.faq.get)).respond(200, faq);

		compileElement('<account-faq-settings basic-settings="basicSettings" save-basic-settings="saveBasicSettings"></account-faq-settings>');

		$httpBackend.flush();

	}));

	describe('When saving changes,', () => {
		beforeEach(() => {
			spy = spyOn(elementScope, 'saveBasicSettings');
			elementScope.saveBasicSettingsFn();
		});
		it ("Should call parent's saveBasicSettings()", () => {
			expect(spy).toHaveBeenCalled();
		});
	});

	describe('When init', () => {
		it('should not render empty html', function () {
			expect(element.html()).not.toEqual('');
		});
	});

	describe('When on account,', () => {
		beforeEach(() => {
			elementScope.basicSettings.faqAccount = true;
		});
		it('should not enabled', function () {
			elementScope.saveBasicSettingsFn();
			expect(elementScope.basicSettings.faqAccount).toBeTruthy();
		});
	});

	describe('Add New Question-Answer-Block', () => {
		let spy, $uibModal;
		let fakeModal = {
			result: {
				then: function(confirmCallback) {
					//Store the callbacks
					this.confirmCallBack = confirmCallback;
				}
			},
			close: function( item ) {
				//The user clicked OK on the modal dialog
				this.result.confirmCallBack( item );
			}
		};
		beforeEach(inject((_$uibModal_) => {
			$uibModal = _$uibModal_;
			spy = spyOn($uibModal, 'open');
			spy.and.returnValue(fakeModal);
		}));

		it('should added new block', function () {
			let faqNewItemServerResponse = ['3'];
			let item = {
				'faq_question': 'new question',
				'faq_answer': 'new answer',
			};
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.faq.create),item).respond(200, faqNewItemServerResponse);
			elementScope.openCreateModal(item);
			fakeModal.close(item);
			$httpBackend.flush();
			expect(elementScope.questionArr.length).toEqual(3);
		});
		it('should send update request', () => {
			let params = {
				'id': '3',
				'faq_question': 'title text',
				'faq_answer': 'text text'
			};
			let item = {
				'id': '3',
				'faq_question': 'title text',
				'faq_answer': 'text text'
			};

			$httpBackend.expectPUT(testHelper.makeRequest(moveBoardApi.faq.update, params)).respond(200);
			elementScope.openEditModal(item);
			fakeModal.close(item);
			$httpBackend.flush();
		});
	});
	describe('Remove block Question-Ansver', () => {
		let spy;
		beforeEach(inject((_SweetAlert_) => {
			spy = spyOn(_SweetAlert_, 'swal');
		}));

		it('should sweetAlert run', () => {
			elementScope.remove();
			expect(spy).toHaveBeenCalled();
		});

		it('should remove block', () => {
			spy.and
				.callFake(function (options, callback) {
					callback(true);
				});
			let params = {
				id: 1
			};
			let item = {
				id: 1
			};
			let index = 1;
			$httpBackend.expectDELETE(testHelper.makeRequest(moveBoardApi.faq.delete, params)).respond(200);

			elementScope.remove(item, index);
			$httpBackend.flush();

			expect(elementScope.questionArr.length).toEqual(1);
		});
	});

});
