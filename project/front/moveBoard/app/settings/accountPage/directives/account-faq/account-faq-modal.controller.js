'use strict';

angular
	.module('app.settings')
	.controller('accountFaqModalCtrl', accountFaqModalCtrl);

accountFaqModalCtrl.$inject = ['$scope', '$uibModalInstance', 'item'];

function accountFaqModalCtrl($scope, $uibModalInstance, item) {
	$scope.item = angular.copy(item);

	$scope.save = function () {
		$uibModalInstance.close($scope.item);
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}
