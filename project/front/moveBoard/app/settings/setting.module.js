'use strict';

angular.module('app.settings', [
	'app.core',
	require('angular-svg-round-progressbar'),
	'pending-info',
	'moveboard-valuation',
	'shared-service-types',
]);
