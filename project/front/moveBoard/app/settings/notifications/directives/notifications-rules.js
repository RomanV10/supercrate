'use strict';

import '../templates/notificationsRules.styl'

angular
	.module('app.settings')
	.directive('notificationsRules', function () {
		return {
			scope: true,
			restrict: 'E',
			replace: true,
			template: require('../templates/notificationsRules.html'),
		}
	});
