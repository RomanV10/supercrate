'use strict';

angular
	.module('app.settings')
	.controller('NotificationsContorller', NotificationsContorller);

function NotificationsContorller($scope, datacontext, common, $uibModal, logger, SettingServices, LongDistanceServices, apiService, moveBoardApi, $timeout) {

	var vm = this;
	var currentNotification = null;
	vm.tokens = [];
	vm.fieldData = datacontext.getFieldData();

	vm.roles = vm.fieldData.enums.roles;
	vm.user = [];

	apiService.postData(moveBoardApi.notifications.getNotifications, {}).then(data => {
		if (_.get(data, 'data.status_code')) {
			toastr.error(data.data.status_message);
			return;
		}
		vm.types = data.data.filter(_.isObject);
		vm.types.forEach(item => item.searchUserText = '')
	});

	apiService.postData(moveBoardApi.user.getUserByRole, {
		active: 1,
		role: ["administrator", "manager", "sales", "driver", "helper", "foreman", "customer service"]
	}).then(data => {
		for (var k in data.data) {
			for (var key in data.data[k]) {
				vm.user.push({
					uid: key,
					name: data.data[k][key].field_user_first_name + ' ' + data.data[k][key].field_user_last_name
				})
			}
		}
	});

	apiService.postData(moveBoardApi.token.list).then(res => {
		vm.tokens = res.data;
	});
	vm.localRules = [];

	for (var key in vm.roles) {
		vm.localRules.push({
			name: key.toLowerCase(),
			rid: vm.roles[key]
		});
	}

	vm.addRole = (rule, item) => {
		apiService.postData(moveBoardApi.notifications.addRole, {role: rule.rid, type: item.ntid}).then(data => {

		})
	};
	vm.removeRole = (rule, item) => {

	};
	vm.addUser = (user, item) => {
		apiService.postData(moveBoardApi.notifications.addUser, {uid: user.uid, type: item.ntid}).then(data => {

		})
	};
	vm.removeUser = (user, item) => {
		apiService.postData(moveBoardApi.notifications.removeUser, {uid: user.uid, type: item.ntid})
	};
	vm.querySearchUser = query => {
		return vm.user.filter(item => item.name.toLowerCase().indexOf(query.toLowerCase()) > -1)
	};

	vm.updateEmail = item => {
		apiService.postData(moveBoardApi.notifications.updateEmailSettings, {
			how_often: item.info.mail,
			ntid: item.ntid
		});

	};
	vm.querySearch = query => {
		return vm.localRules;
	};

	vm.addTemplate = item => {
		apiService.postData(moveBoardApi.notifications.addTemplate, {
			text: item.info.templates,
			type: item.ntid
		}).then(data => {

		})
	};
	vm.updateSalesPermision = item => {
		apiService.postData(moveBoardApi.notifications.update_sales_permission, {
			status: item.info.sales_permission,
			ntid: item.ntid
		});
	};

	vm.addToken = (token, index) => {
		var field = angular.element(document.querySelector('#template' + index));
		var textBefore = field.val().substring(0, field.prop('selectionStart'));
		var textAfter = field.val().substring(field.prop('selectionStart'), field.val().length);
		textBefore += '[' + token + ']';
		field.val(textBefore + textAfter);
		field.focus();
		currentNotification = index;
	};

	vm.tabs =
		{
			'basic':
				{
					name: 'Basic',
					selected: true
				}
		};

	vm.select = function (tab) {
		angular.forEach(vm.tabs, function (tab) {
			tab.selected = false;
		});

		tab.selected = true;
	};

	vm.templateBlur = item => {
		$timeout(() => {
			if (item.ntid !== currentNotification) {

				var field = angular.element(document.querySelector('#template' + item.ntid));
				item.info.templates = field.val();
				item.showTokens = false;
				currentNotification = null;
				vm.addTemplate(item);

			} else {
				currentNotification = null;
			}
		}, 200)
	};

	$scope.select = function (tab) {

		angular.forEach($scope.tabs, function (tab) {
			tab.selected = false;
		});
		tab.selected = true;
	};
}
