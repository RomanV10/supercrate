(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {

                $stateProvider
                    .state('settings.notifications', {
                        url: '/notifications',
	                    template: require('./templates/notificationsPage.html'),
                        title: 'Long Distance',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });
            }
        ]
    );
})();
