(function () {

	angular
		.module('app.settings')

		.factory('SettingServices', SettingServices);

	SettingServices.$inject = ['$http', '$q', 'datacontext', 'apiService', 'moveBoardApi', 'config'];

	function SettingServices($http, $q, datacontext, apiService, moveBoardApi, config) {

		const MOVE_BOARD_CONTRACT_FIELDS = [
			'declarations',
			'minPayrollHours',
			'pushTips',
			'lessInitialContract',
			'goToReview',
			'useInventory',
			'paymentTypes',
			'paymentOptions',
			'releaseFormOldVersion',
			'pickUpPercent',
			'confirmationShowQuote',
			'rentalAgreementForm',
			'foremanChangeRate',
			'foremanMoveDateSorting',
			'confirmationShowForemanNotes',
			'skippingPhoto',
		];

		var service = {};

		service.saveMoveBoardContractSettings = saveMoveBoardContractSettings;
		service.getSettings = getSettings;
		service.saveSettings = saveSettings;
		service.saveExtraSettings = saveExtraSettings;
		service.deleteSettings = deleteSettings;
		service.setVariable = setVariable;
		service.getVariable = getVariable;
		service.setSiteLog = setSiteLog;
		service.createFlag = createFlag;
		service.editFlag = editFlag;
		service.removeFlag = removeFlag;
		service.getFlags = getFlags;
		service.setParser = setParser;
		service.getParser = getParser;
		service.testConnection = testConnection;


		return service;

		function saveMoveBoardContractSettings(data) {
			let deferred = $q.defer();
			let contract_page = data;
			let getSettings = {};
			service.getSettings('contract_page').then(function (data) {
				if (_.isArray(data) && !_.isEmpty(data)) {
					data = _.head(data);
				}

				getSettings = angular.fromJson(data);

				_.forEach(MOVE_BOARD_CONTRACT_FIELDS, function (field) {
					getSettings[field] = contract_page[field];
				});

				deferred.resolve(getSettings);
			}, function (err) {
				deferred.reject(err);
			});

			return deferred.promise;
		}

		function testConnection() {
			var deferred = $q.defer();
			$http.post(config.serverUrl + 'server/mail/test_parser_connection')
				.success(function (data) {
					deferred.resolve(data);
				})
				.error(function (data) {
					deferred.reject(data);
				});
			return deferred.promise;
		}

		function setParser(settings) {

			var deferred = $q.defer();
			$http.post(config.serverUrl + 'server/parser', settings)
				.success(function (data) {
					deferred.resolve(data);
				})
				.error(function (data) {
					deferred.reject(data);
				});
			return deferred.promise;
		}

		function getParser() {

			var deferred = $q.defer();
			$http.get(config.serverUrl + 'server/parser')
				.success(function (data) {
					deferred.resolve(data);
				})
				.error(function (data) {
					deferred.reject(data);
				});
			return deferred.promise;
		}

		function createFlag(flag) {
			let data = {
				value: [flag.name],
				type:  'flags'
			};

			return $http.post(config.serverUrl + 'server/move_request/create_flag', data);
		}

		function editFlag(flag) {
			var deferred = $q.defer();
			var data = {
				id:    flag.id,
				value: flag.name,
				type:  'flags'
			};
			$http.post(config.serverUrl + 'server/move_request/edit_flag', data)
				.success(function (data) {
					deferred.resolve(data);
				})
				.error(function (data) {
					deferred.reject(data);
				});
			return deferred.promise;

		}

		function removeFlag(flag) {
			var deferred = $q.defer();
			var data = {
				id:   flag.id,
				type: 'flags'
			};
			$http.post(config.serverUrl + 'server/move_request/remove_flag', data)
				.success(function (data) {
					deferred.resolve(data);
				})
				.error(function (data) {
					deferred.reject(data);
				});

			return deferred.promise;
		}

		function getFlags() {
			var deferred = $q.defer();
			$http.post(config.serverUrl + 'server/move_request/get_flags')
				.success(function (data) {
					deferred.resolve(data);
				})
				.error(function (data) {
					deferred.reject(data);
				});

			return deferred.promise;
		}

		function setSiteLog(id, name) {
			var deferred = $q.defer();
			deferred.resolve();
			/*var data = {};
			 data.id = id;
			 data.site_name = name;
			 $http.post(config.serverUrl+'server/admin_logs/write_site_id',data)
			 .success(function(data) {
			 deferred.resolve(data);
			 })
			 .error(function(data) {
			 deferred.reject(data);
			 });*/

			return deferred.promise;

		}

		function getSettings(type) {
			return datacontext.getSettings(type);
		}

		function getVariable(name) {
			let deferred = $q.defer();

			apiService.postData(moveBoardApi.core.getVariable, {name})
				.then(res => {
					if (_.get(res, 'data[0]')) {
						deferred.resolve(res.data[0]);
					} else {
						deferred.reject();
					}
				}, rej => {
					deferred.reject(rej);
				});

			return deferred.promise;
		}

		function setVariable(value, name) {
			let deferred = $q.defer();

			apiService.postData(moveBoardApi.settingService.setVariable, {
				name:  name,
				value: value
			}).then(resolve => {
				deferred.resolve(resolve.data);
			}, reject => {
				deferred.reject(reject.data);
			});

			return deferred.promise;

		}

		function saveSettings(settings, type) {
			let value = type != 'settingsclosestbranch' && type != 'current_branch' ? angular.toJson(settings) : settings;

			datacontext.saveFieldDataType(type, settings);
			return setVariable(value, type);
		}

		function saveExtraSettings(settings, type, currentEl) {
			var deferred = $q.defer();

			var data = {};
			data.name = type;
			var tempSettings = [];

			datacontext.getSettings(type).then(function (aviableSettings) {

				if (aviableSettings != null) {
					if (JSON.parse(aviableSettings)[0]) {
						tempSettings = JSON.parse(aviableSettings);
					}
				}
				if (currentEl || currentEl == 0) {
					tempSettings.splice(currentEl, 1);
					tempSettings.splice(currentEl, 0, settings);
				} else {
					tempSettings.push(settings);
				}


				data.value = angular.toJson(tempSettings);
				$http.post(config.serverUrl + 'server/settings/set_variable', data)
					.success(function (newData, status, headers, config) {
						deferred.resolve(newData);

					})
					.error(function (data, status, headers, config) {
						// data.success = false;
						deferred.reject(data);
					});

			});
			return deferred.promise;
		}

		function deleteSettings(type, current) {
			var deferred = $q.defer();

			var data = {};
			data.name = type;

			datacontext.getSettings(type)
				.then(function (aviableSettings) {
					var tempSettings = JSON.parse(aviableSettings) || [];

					if (current || current == 0) {
						tempSettings.splice(current, 1);
					}

					data.value = angular.toJson(tempSettings);

					apiService.postData(moveBoardApi.settingService.setVariable, data)
						.then(newData => {
							deferred.resolve(newData);
						})
						.catch(data => {
							deferred.reject(data);
						});

				})
				.catch(() => {
					toastr.error(`Cannot get settings '${type}'`);
				});
			return deferred.promise;
		}

	}
})();
