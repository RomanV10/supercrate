'use strict';
import './createFilter.styl';

angular
	.module('app.settings')
	.directive('inventoryCreateFilter', inventoryCreateFilter);

function inventoryCreateFilter(apiService, moveBoardApi, $mdDialog, $timeout, SweetAlert) {
	return {
		scope: {
			mode: '@',
			room: '@',
			rooms: '=',
			filter: '=',
			createCallBack: '='
		},
		restrict: 'A',
		link: ($scope, element) => {
			angular.element(element)
				.on('click', ev => {
					$scope.createFilter(ev);
				});

			$scope.createFilter = (ev) => {
				$scope.rooms.forEach(item => item.selected = false);
				$mdDialog.show({
					controller: FilterController,
					template: require('./createFilter.pug'),
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose: true,
					locals: {
						room_id: $scope.room,
						mode: $scope.mode,
						filter: $scope.filter,
						rooms: $scope.rooms
					}
				})
					.then(function (answer) {
						$scope.status = 'You said the information was "' + answer + '".';
					}, function () {
						$scope.status = 'You cancelled the dialog.';
					});
			};

			function callBack(filter) {
				if ($scope.mode == 'edit') {
					updateRoomsTree(filter);
				}

				$scope.createCallBack(filter);
			}

			function updateRoomsTree(filter) {
				$scope.rooms.filter(item => item.filters)
					.forEach(item => item.filters = item.filters.filter(f => f.filter_id !== filter.filter_id));

				filter.rooms.map(item => $scope.rooms.filter(room => room.room_id == item)[0])
					.forEach(item => {
						item.filters = item.filters || [];
						item.filters.push(filter);
					});
			}

			function FilterController($scope, $mdDialog, room_id, mode, filter, rooms) {
				$scope.rooms = rooms;
				$scope.filter = {};
				$scope.mode = mode;
				$scope.filter.room_id = room_id ? room_id : null;
				$scope.filter = $scope.mode == 'edit' ? angular.copy(filter) : $scope.filter;
				$scope.originFilter = filter;
				$scope.busy = false;
				$scope.hide = () => $mdDialog.hide();

				$scope.selectRoom = item => {
					item.selected = !item.selected;
					$scope.selectRooms = '';
					$scope.createFilter.rooms.$setTouched();
					$scope.rooms.forEach(item => {
						if (item.selected) {
							$scope.selectRooms += ', ' + item.name;
						}
					});

					if ($scope.selectRooms.length > 0) {
						$scope.selectRooms = $scope.selectRooms.slice(2);
					}
				};

				$scope.cancel = () => {
					if ($scope.modeEdit && $scope.filter.name) {
						$scope.filter.name = $scope.originFilter.name;
					}

					$mdDialog.cancel();
				};

				$scope.remove = () => {
					$scope.busy = true;

					apiService.delData(moveBoardApi.inventory.filter.remove, {id: $scope.filter.filter_id})
						.then(() => {
							$scope.busy = false;
							$scope.rooms.filter(item => item.filters)
								.forEach(item => item.filters = item.filters.filter(f => f.filter_id !== $scope.filter.filter_id));

							$mdDialog.hide(filter);
						}, rej => {
							SweetAlert.swal('Error', rej.status_message, 'error');
						});
				};

				$scope.getSelectedItems = () => {
					var result = [];

					$scope.rooms.forEach(item => {
						result.push(item.room_id);
					});
				};

				$scope.chooseRooms = () => {
					$scope.filter.rooms.forEach(item => {
						$scope.rooms.forEach(room => {
							if (item == room.room_id) {
								$scope.selectRoom(room);
							}
						});
					});
				};

				$scope.create = function (filter) {
					if (!$scope.createFilter.$valid || $scope.busy) {
						$scope.createFilter.$setSubmitted();
						return false;
					}

					$scope.busy = true;
					filter.rooms_ids = $scope.rooms.filter(item => item.selected).map(item => item.room_id);
					filter.rooms = filter.rooms_ids;

					if ($scope.mode == 'edit') {
						$scope.originFilter.name = filter.name;
						$scope.originFilter.rooms = filter.rooms;
						$scope.originFilter.rooms_ids = filter.rooms_ids;
						apiService.putData(moveBoardApi.inventory.filter.update, {id: filter.filter_id, data: filter})
							.then(() => {
								$scope.busy = false;
								callBack(filter);
								$mdDialog.hide(filter);
							}, () => {
								SweetAlert.swal('Error', "Cannot save filter", 'error');
							});
					} else {
						apiService.postData(moveBoardApi.inventory.filter.create, {data: filter})
							.then(res => {
								$scope.busy = false;
								filter.filter_id = res.data.data.filter_id;
								filter.new = true;
								callBack(filter);
								$mdDialog.hide(filter);
							}, () => {
								SweetAlert.swal('Error', "Cannot create filter", 'error');
							});
					}
				};

				if ($scope.mode === 'edit') {
					$timeout(() => {
						$scope.chooseRooms();
					}, 100);
				}
			}

		}
	};
}
