'use strict';
import './createRoom.styl'

angular
	.module('app.settings')
	.directive('inventoryCreateRoom', inventoryCreateRoom);


function inventoryCreateRoom(apiService, moveBoardApi, $mdDialog) {
	return {
		scope: {
			room: '=',
			mode: '@',
			createCallBack: '='
		},
		restrict: 'A',
		link: ($scope, element) => {
			element.on('click', ev => {
				$scope.createRoom(ev)
			});

			$scope.createRoom = (ev) => {
				$mdDialog.show({
						controller: RoomController,
						template: require('./createRoom.pug'),
						parent: angular.element(document.body),
						targetEvent: ev,
						clickOutsideToClose: true,
						locals: {
							room: $scope.room,
							mode: $scope.mode
						}
					})
					.then(function (answer) {
						$scope.status = 'You said the information was "' + answer + '".';
					}, function () {
						$scope.status = 'You cancelled the dialog.';
					});
			};

			function callBack(room, type) {
				$scope.createCallBack(room, type);
			}

			function RoomController($scope, $mdDialog, room, mode, PermissionsServices, SweetAlert) {
				$scope.originRoom = angular.copy(room);
				$scope.room = mode == 'edit' ? room : {};
				$scope.mode = mode;
				$scope.busy = false;
				$scope.showRemove = PermissionsServices.isSuper();
				const ALL_ROOM_ID = 15;
				const BEDROOM_ID = 5;
				$scope.allowRemove = !room || room && room.room_id != ALL_ROOM_ID && room.room_id != BEDROOM_ID;
				$scope.allowRename = !room || room && room.room_id != ALL_ROOM_ID && room.room_id != BEDROOM_ID;
				

				$scope.hide = hide;
				$scope.cancel = cancel;
				$scope.create = create;
				$scope.remove = remove;

				function hide() {
					$mdDialog.hide();
				}

				function cancel() {
					if ($scope.mode == 'edit' && $scope.room.name) {
						$scope.room.name = $scope.originRoom.name;
						$scope.room.image_link = $scope.originRoom.image_link;
					}
					$mdDialog.cancel();
				}

				function create(room) {
					if (!$scope.createRoom.$valid) {
						$scope.createRoom.$setSubmitted();
						return false;
					}
					if (invalidRoomName()) {
						SweetAlert.swal('Please change name', "Words 'All' and 'Bedroom' are reserved and Cannot be used", 'error');
					} else {
						$scope.busy = true;
						$scope.originRoom = angular.copy(room);
						delete room.showEdit;
						delete room.opened;
						
						if ($scope.mode == 'edit') {
							apiService.putData(moveBoardApi.inventory.room.update, {
								id: room.room_id,
								data: room
							}).then(res => {
								if (res.data.status_code == 200) {
								$scope.originRoom = room;
								$mdDialog.hide(room);
								} else {
									SweetAlert.swal('Error', "Cannot save room", 'error');
								}
								$scope.busy = false;
							}, error => {
								SweetAlert.swal('Error', "Cannot connect to server", 'error');
								$scope.busy = false;
							})
						} else {
							apiService.postData(moveBoardApi.inventory.room.create, {data: room}).then(res => {
								if (res.data.status_code == 200) {
									$scope.originRoom = room;
									$mdDialog.hide(room);
									callBack(res.data.data);
								} else {
									SweetAlert.swal('Error', "Cannot create room", 'error');
								}
								$scope.busy = false;
							}, error => {
								SweetAlert.swal('Error', "Cannot connect to server", 'error');
								$scope.busy = false;
							})
						}
					}
					function invalidRoomName() {
						let isAll = room.name.toLowerCase() == 'all';
						let isBedroom = room.name.toLowerCase().indexOf('bedroom') != -1;
						return isAll || isBedroom;
					}

				}

				function remove(room) {
					if ($scope.allowRemove) {
						apiService.delData(moveBoardApi.inventory.room.remove, {id: room.room_id})
							.then(() => {
								$mdDialog.hide(room);
								callBack(room, 'remove');
							})
							.catch(({data}) => SweetAlert.swal('Error', data, 'error'))
							.finally(() => $scope.busy = false);
					}
				}
			}
		}
	}
}
