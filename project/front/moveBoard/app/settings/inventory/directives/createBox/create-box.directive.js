'use strict';
import './createBox.styl'

angular
	.module('app.settings')
	.directive('inventoryCreateBox', inventoryCreateBox);

inventoryCreateBox.$inject = ['apiService', 'moveBoardApi', '$mdDialog', '$timeout', 'PermissionsServices', 'SweetAlert'];

function inventoryCreateBox(apiService, moveBoardApi, $mdDialog, $timeout, PermissionsServices, SweetAlert) {
	return {
		scope: {
			mode: '@',
			room: '@',
			filters: '=',
			filter: '=',
			editInventoryItem: '=',
			createCallBack: '='
		},
		restrict: 'A',
		link: ($scope, element) => {
			$scope.createBox = createBox;

			element.on('click', createBox);

			function removeItemCallback(item_id) {
				$scope.$emit('removeItemFromInventory', item_id);
			}

			function moveItemCallback() {
				$scope.$emit('reloadInventoryFilter');
			}

			function createBox(ev) {
				$scope.filters.forEach(item => item.selected = false);
				$mdDialog.show({
					controller: BoxItemController,
					template: require('./createBox.pug'),
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose: true,
					locals: {
						room_id: $scope.room,
						mode: $scope.mode,
						filter: $scope.filter,
						filters: $scope.filters,
						inventoryItem: $scope.editInventoryItem
					}
				});
			}

			function callBack(item) {
				item.filters = [];

				$scope.filters.forEach(filter => {
					if (filter.selected) {
						item.filters.push(filter.filter_id);
					}
				});

				$scope.createCallBack(item);
			}

			function BoxItemController($scope, $mdDialog, room_id, mode, filter, filters, inventoryItem) {
				$scope.filters = filters;
				$scope.filter = {};
				$scope.mode = mode;
				$scope.showRemove = PermissionsServices.isSuper() || PermissionsServices.ifAdmin();

				if (!$scope.originItem) {
					$scope.originItem = {};
				}

				if ($scope.mode === 'edit') {
					$scope.originItem = inventoryItem;
					$scope.box = angular.copy(inventoryItem);
				}

				$timeout(() => {
					$(".js-custom-scrollbar").mCustomScrollbar({theme: "dark"});
				}, 10);

				$scope.busy = false;

				$scope.hide = function () {
					$mdDialog.hide();
				};

				$scope.cancel = () => {
					$mdDialog.cancel();
				};

				$scope.selectFilter = item => {
					item.selected = !item.selected;
					$scope.selectFilters = '';
					$scope.createItem.filters.$setTouched();

					$scope.filters.forEach(item => {
						if (item.selected) {
							$scope.selectFilters += ' ,' + item.name;
						}
					});

					if ($scope.selectFilters.length > 0) {
						$scope.selectFilters = $scope.selectFilters.slice(2);
					}
				};

				$scope.remove = () => {
					if (!$scope.showRemove) return false;
					$scope.busy = true;

					apiService.delData(moveBoardApi.inventory.item.remove, {id: $scope.box.item_id})
						.then(res => {
							removeItemCallback($scope.box.item_id);
							$mdDialog.hide(filter);
							$scope.busy = false;
						}, rej => {
							SweetAlert.swal("Error", "Cannot remove box", 'error');
						});
				};

				$scope.changeHandle = model => {
					$scope.handle_fee = model;
				};

				$scope.changePacking = model => {
					$scope.packing_fee = model;
				};

				$scope.changeExtraservice = model => {
					$scope.extra_service = model;
				};

				$scope.selectRoom = () => {
					$scope.box.filters.forEach(item => {
						$scope.filters.forEach(filter => {
							if (item == filter.filter_id) {
								$scope.selectFilter(filter);
							}
						});
					});
				};

				$scope.create = function (item) {
					if (!$scope.createItem.$valid) {
						$scope.createItem.$setSubmitted();
						return false;
					}

					$scope.busy = true;
					item.filters = [];

					$scope.filters.forEach(filter => {
						if (filter.selected) {
							item.filters.push(filter.filter_id)
						}
					});

					item.handle_fee = $scope.handle_fee;
					item.packing_fee = $scope.packing_fee;
					item.extra_service = $scope.extra_service;

					if ($scope.mode == 'edit') {
						$scope.originItem.filters = item.filters;
						$scope.originItem.filters = item.filters;
						$scope.originItem.handle_fee = $scope.handle_fee;
						$scope.originItem.packing_fee = $scope.packing_fee;
						$scope.originItem.extra_service = $scope.extra_service;
						$scope.originItem.extra_service_name = item.extra_service_name;
						$scope.originItem.name = item.name;
						$scope.originItem.cf = item.cf;
						$scope.originItem.image_link = item.image_link;
						$scope.originItem.uri = item.uri;

						apiService.putData(moveBoardApi.inventory.item.update, {
							id: item.item_id,
							data: item
						}).then(res => {
							$mdDialog.hide(filter);
							moveItemCallback();

							$scope.busy = false;
						}, rej => {
							SweetAlert.swal("Error", "Cannot save box", 'error');
						});
					} else {
						item.status = 0;
						item.type = 2;

						apiService.postData(moveBoardApi.inventory.item.create, {data: item})
							.then(res => {
								callBack(res.data.data);
								$mdDialog.hide(item);
								$scope.busy = false;
							}, rej => {
								SweetAlert.swal("Error", "Cannot create box", 'error');
							});
					}
				};

				if ($scope.mode === 'edit') {
					$timeout(() => {
						$scope.selectRoom();
					}, 100);
				}
			}

		}
	};
}
