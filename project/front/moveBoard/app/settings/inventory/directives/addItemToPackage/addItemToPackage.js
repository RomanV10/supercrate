'use strict';
import './addItemToPackage.styl'
(() => {
    angular
    .module('app.settings')
    .directive('packageAddItem', inventoryCreateFilter);


    function inventoryCreateFilter(apiService, moveBoardApi, $mdDialog, $timeout, SweetAlert) {
        return {
            scope: {
                mode: '@',
                packageName: '@',
                inventoryPackageItem: '=',
                filters: '=',
                room:'=',
                packageId: '@'
            },
            restrict: 'A',
            link: ($scope, element) => {

                angular.element(element).on('click', ev => {
                    $scope.createFilter(ev);
                });


                $scope.createFilter = (ev, room_id, edit, filter) => {
                    $scope.filters.forEach(item => item.selected = false);
                    $mdDialog.show({
                        controller: FilterController,
                        template: require('./addItemToPackage.pug'),
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose:true,
                        locals: {
                            mode: $scope.mode,
                            inventoryPackageItem: $scope.inventoryPackageItem,
                            filters: $scope.filters,
                            room: $scope.room,
                            packageId: $scope.packageId,
                            packageName: $scope.packageName
                        }
                    })
                    .then(function(answer) {

                    }, function() {

                    });
                };

                function callBack(filter) {
                    // filter.rooms_ids = [];
                    // $scope.rooms.forEach( item => { if (item.selected) filter.rooms_ids.push(item.room_id) });
                    $scope.createCallBack(filter);
                }

                function FilterController($scope, $mdDialog, mode, inventoryPackageItem, filters, room, packageId, packageName) {
                    $scope.filters = filters;
                    $scope.inventoryPackageItem = {};
                    $scope.packageName = packageName;
                    $scope.packageId = packageId;
                    $scope.room = room;
                    // $scope.mode = mode;
                    // $scope.filter.room_id = room_id ? room_id : null;
                    // $scope.filter = $scope.mode == 'edit' ? filter : $scope.filter;
                    // $scope.originFilter = angular.copy(filter);
                    // $scope.busy = false;
                    $scope.hide = function() {
                        $mdDialog.hide();
                    };

                    $(".js-custom-scrollbar").mCustomScrollbar(
                        {
                            theme: "dark"
                        }
                    );

                    $scope.selectRoom = item => {
                        item.selected = !item.selected;
                        $scope.selectRooms = '';
                        $scope.createFilter.rooms.$setTouched();
                        $scope.rooms.forEach( item => {
                            if (item.selected) {
                                $scope.selectRooms += ' ,' + item.name;
                            }
                        })
                        if ( $scope.selectRooms.length > 0) {
                            $scope.selectRooms = $scope.selectRooms.slice(2);

                        }
                    }

                    $scope.cancel = filter => {
                        if ( $scope.modeEdit && $scope.filter.name ) {
                            $scope.filter.name = $scope.originFilter.name;
                        }
                        $mdDialog.cancel();
                    };

                    $scope.remove = () => {
                        $scope.busy = true;
                        apiService.delData(moveBoardApi.inventory.filter.remove, { id: $scope.filter.filter_id }).then( res => {
                        	if (res.data.status_code == 200) {
										$mdDialog.hide(filter);
									} else {
										SweetAlert.swal('Error', "Cannot remove filter from package", 'error');
									}
									$scope.busy = false;
                        }, rej => {
									SweetAlert.swal('Error', "Cannot connect to server", 'error');
								})
                    }

                    $scope.getSelectedItems = () => {
                        var result = [];
                        $scope.rooms.forEach( item => {
                            result.push(item.room_id);
                        })
                    }

                    $scope.selectFilter = filter => {
                        $scope.filters.forEach(item => {
                            item.selected = false;
                        })

                        $scope.currentFilter = {};
                        $scope.selectedFilter = '';

                        if (!filter.selected) {
                            filter.selected = true;
                            $scope.currentFilter = filter;
                            $scope.selectedFilter = filter.name;
                        }
                        if (!filter.items) {
                            $scope.loadItem(filter);
                        }
                    }

						 $scope.loadItem = filter => {
							 $scope.busy = true;
							 apiService.postData(moveBoardApi.inventory.item.getItems, {
								 conditions: {
									 'package_id': $scope.packageId,
									 'room_id': $scope.room.room_id,
									 'filter_id': filter.filter_id
								 }
							 }).then(res => {
								 if (res.data.status_message == 200) {
									 filter.items = res.data.data;
								 } else {
									 SweetAlert.swal("Error", "Cannot load items for package", 'error');
								 }
								 $scope.busy = false;
							 }, rej => {
								 SweetAlert.swal("Error", "Cannot connect to server", 'error');
							 })
						 };

                    $scope.selectRoom = room => {
                        $scope.rooms.forEach(item => {
                            item.selected = false;
                            if (item.filters) {
                                item.filters.forEach(filter => {
                                    filter.selected = false;
                                })
                            }
                        })
                        $scope.selectedRoom = '';
                        $scope.currentRoom = {};
                        $scope.currentFilter = {};

                        if (!room.selected) {
                            room.selected = true;
                            $scope.currentRoom = room;
                            $scope.selectedRoom = room.name
                        }
                    }

                  $scope.chooseRooms = () => {
                        $scope.filter.rooms.forEach(item => {
                            $scope.rooms.forEach(room => {
                                if (item == room.room_id) {
                                    $scope.selectRoom(room);
                                }
                            })
                        })
                    }


                    $scope.create = function(filter) {
                        if (!$scope.createFilter.$valid) {
                            $scope.createFilter.$setSubmitted();
                            return false;
                        }
                        $scope.busy = true;
                        $scope.filter.rooms_ids = [];
                        $scope.rooms.forEach( item => { if (item.selected) $scope.filter.rooms_ids.push(item.room_id) });
                        if ($scope.mode == 'edit') {
                            apiService.putData(moveBoardApi.inventory.filter.update, { id: filter.filter_id, data: filter}).then( res => {
                            	if (res.data.status_code == 200) {
											$scope.busy = false;
											callBack(filter);
											$mdDialog.hide(filter);
										} else {
											SweetAlert.swal('Error', "Cannot save filter", 'error');
										}
                            }, rej => {
										 SweetAlert.swal('Error', "Cannot connect to server", 'error');
									 })
                        } else {
                            apiService.postData(moveBoardApi.inventory.filter.create, { data: filter}).then( res => {
                            	if (res.data.status_code == 200) {
											$scope.busy = false;
											callBack(res.data);
											$mdDialog.hide(filter);
										} else {
											SweetAlert.swal('Error', "Cannot create filter", 'error');
										}
                            }, rej => {
										 SweetAlert.swal('Error', "Cannot connect to server", 'error');
									 })
                        }
                    };

                   if ($scope.mode === 'edit') {
                        $timeout(() => {
                            // $scope.chooseRooms();
                        }, 100);
                    }
                }

            }
        }
    };
})();
