'use strict';
import './packageSettings.styl'
(() => {
    angular
    .module('app.settings')
    .directive('packageSettings', inventorySetting);


    function inventorySetting(apiService, moveBoardApi, $mdDialog, $timeout) {
        return {
            scope: {
            },
            template: require('./packageSettings.pug'),
            restrict: 'A',
            link: ($scope, element) => {

                // apiService.getData(moveBoardApi.inventory.package.query).then(res => {
                //     $scope.packages = res.data;
                // })

                $scope.createPackage = inventoryPackage => {
                    apiService.postData();
                }

                $scope.openPackage = inventoryPackage => {
                    var state = !inventoryPackage.opened;
                    $scope.currentPackage = inventoryPackage;

                    $scope.packages.forEach(item => {
                        item.opened = false;
                    })

                    inventoryPackage.opened = state;

                    $scope.busy = true;
                    $timeout(() => {
                        $scope.busy = false;
                    })
                }

                $scope.setPackage = currentPackage => {
                    $scope.currentPackage = currentPackage;
                    $scope.busy = true;
                    $timeout(() => {
                        $scope.busy = false;
                    }, 100)
                }

                function findRoom(id, rooms) {
                    return rooms.filter(room => room.room_id === id)[0];
                }
            }
        }
    };
})();
