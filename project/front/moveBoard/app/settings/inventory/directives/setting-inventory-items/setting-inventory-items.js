'use strict';
import './setting-inventory-items.styl'
(() => {

	angular
		.module('newInventories')
		.directive('settingInventoryItem', (InventoriesServices, $http, config, $timeout) => {
			return {
				scope: {
					item: '=inventoryData',
					list: "=",
					calculate: "=calculate",
					deleteCustomItem: '=',
					current: "=",
					room: "=",
					filters: "=",
					deleteItem: "&"
				},
				template: require('./setting-inventory-items.pug'),
				restrict: 'A',
				link: ($scope, element) => {

				}
			}
		});

})();
