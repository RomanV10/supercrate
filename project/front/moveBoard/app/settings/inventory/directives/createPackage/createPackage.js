'use strict';
import './createPackage.styl'
(() => {
    angular
    .module('app.settings')
    .directive('createPackage', createPackage);


    function createPackage(apiService, moveBoardApi, $mdDialog, $timeout) {
        return {
            scope: {
                mode: '@',
                inventoryPackage: '=',
                createCallBack: '='
            },
            restrict: 'A',
            link: ($scope, element) => {


                element.on('click', ev => {
                    $scope.createPackage(ev)
                });

                $scope.createPackage = (ev) => {
                    $mdDialog.show({
                        controller: PackageController,
                        template: require('./createPackage.pug'),
                        parent: angular.element(document.body),
                        targetEvent: ev,
                         locals: {
                            inventoryPackage: $scope.inventoryPackage,
                            mode: $scope.mode
                        },
                        clickOutsideToClose:true,
                    })
                    .then(function(answer) {

                    }, function() {

                    });
                };

                function callBack(inventoryPackage, mode) {
                     $scope.createCallBack(inventoryPackage, mode);
                }

                function PackageController($scope, $mdDialog, inventoryPackage, mode) {
                    $scope.originPackage = angular.copy(inventoryPackage);
                    $scope.inventoryPackage = mode == 'edit' ? inventoryPackage: {};
                    $scope.mode = mode;
                    $scope.busy = false;

                    $scope.hide = function() {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function() {
                        if ($scope.mode == 'edit' && $scope.inventoryPackage.name) {
                            $scope.inventoryPackage.name = $scope.originPackage.name;
                        }
                        $mdDialog.cancel();
                    };

                    $scope.remove = function() {
                        $scope.busy = true;
                        apiService.delData(moveBoardApi.inventory.package.remove, { id: $scope.inventoryPackage.package_id }).then( res => {
                            $scope.busy = false;
                            $mdDialog.hide();
                            callBack(inventoryPackage, 'remove');
                        })
                    }

                    $scope.create = function(inventoryPackage) {
                        if (!$scope.createPackage.$valid) {
                            $scope.createPackage.$setSubmitted();
                            return false;
                        }
                        $scope.busy = true;
    
                        if ($scope.mode == 'edit') {
                            apiService.putData(moveBoardApi.inventory.package.update, { id: inventoryPackage.package_id, data: inventoryPackage}).then( res => {
                                $scope.originPackage = inventoryPackage;
                                $mdDialog.hide();
                                $scope.busy = false;
                            }, error => {

                            })
                        } else {
                            // apiService.postData(moveBoardApi.inventory.package.create, {data: inventoryPackage}).then( res => {
                            //     $scope.originPackage = inventoryPackage;
                            //     $mdDialog.hide();
                            //     callBack(res.data);
                            //     $scope.busy = false;
                            // }, error => {
							//
                            // })
                       }
                       
                   };
               }
           }
       }
   };
})();
