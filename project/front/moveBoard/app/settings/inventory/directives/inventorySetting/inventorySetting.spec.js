describe("Unit: inventorySetting directive,", () => {
	let $inventoryScope, $element, $scope;
	let getFiltersResponse, getRoomsResponse;
	beforeEach(inject( () => {
		$scope = $rootScope.$new();

		getFiltersResponse = testHelper.loadJsonFile('getFilters.mock');
		$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.filter.query)).respond(200, getFiltersResponse);

		getRoomsResponse = testHelper.loadJsonFile('getRooms.mock');
		$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.room.rooms)).respond(200, getRoomsResponse);

		$scope.fieldData = $scope.fieldData || {};
		$scope.fieldData.inventorySetting = {
			showItemsInFilter: false
		};

		$element = $compile("<div inventory-setting></div>")($scope);
		$inventoryScope = $element.isolateScope();

		$httpBackend.flush();
	}));

	describe("Testing DOM,", () => {
		it ("Should update searchText change input", () => {
			spyOn($inventoryScope, "doSearch");
			$element.find(".search-input input").val('test text');
			$element.find(".search-input input").triggerHandler("change");
			$inventoryScope.$apply();
			expect($inventoryScope.doSearch).toHaveBeenCalled();
		});
		it ("Should call doSearch when change input", () => {
			spyOn($inventoryScope, "doSearch");
			$element.find(".search-input input").val('test text');
			$element.find(".search-input input").triggerHandler("change");
			$inventoryScope.$apply();
			expect($inventoryScope.searchText).toEqual('test text');
		});
	});

	describe('function: createItemCallBack,', () => {
		let item;
		beforeEach(() => {
			item = {
				cf: 10,
				extra_service: 20,
				extra_service_name: "extra",
				filters: ["2"],
				handle_fee: 0,
				image_link: "http://192.168.0.104/sites/default/files/inventory/2_1.jpg",
				item_id: "29",
				name: "name",
				packing_fee: 0,
				price: 0,
				status: 0,
				type: 0,
				uri: "public://inventory/2_1.jpg"
			};
		});
		describe('if current filter,', () => {
			beforeEach(() => {
				$inventoryScope.activeFilter = "2";
			});
			describe('if empty filter,', () => {
				beforeEach(() => {
					$inventoryScope.inventoryItems = [];
				});

				it('must set item.row to 0', () => {
					$inventoryScope.createItemCallBack(item);
					expect(item.row).toEqual(0);
				});
			});

			describe('if filled filter,', () => {
				beforeEach(() => {
					$inventoryScope.inventoryItems = [{name: "firstItem"}];
				});

				it('must set item.row to 1', () => {
					$inventoryScope.createItemCallBack(item);
					expect(item.row).toEqual(1);
				});
			});
		});

	});

	describe("function: doSearch,", () => {
		let getItemsResponse, searchData;
		beforeEach(() => {
			getItemsResponse = testHelper.loadJsonFile('search.getItems.mock');
			$inventoryScope.searchText = "lalala";
		});

		it("Should make search-request", () => {

			searchData = {
				"name": "lalala"
			};
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.settings.search), searchData)
				.respond(200, getItemsResponse);

			$inventoryScope.doSearch();
		});

		it("Should add items to inventoryItems", () => {
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.settings.search))
				.respond(200, getItemsResponse);

			$inventoryScope.doSearch();
			$httpBackend.flush();
			expect($inventoryScope.inventoryItems.length).toEqual(1);
		});

	});
});
