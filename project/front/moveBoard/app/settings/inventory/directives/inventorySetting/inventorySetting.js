'use strict';
import './inventorySetting.styl';

angular
	.module('app.settings')
	.directive('inventorySetting', inventorySetting);

function inventorySetting(apiService, moveBoardApi, $mdDialog, $timeout, datacontext, SettingServices, dragulaService, SweetAlert) {
	return {
		scope: {},
		template: require('./inventorySetting.pug'),
		restrict: 'A',
		link: ($scope, element) => {
			const ROOM_ALL_ID = 'room_5';
			const ROOM_BEDROOM_ID = 'room_15';

			$scope.isMobile = false;
			$scope.busy = true;
			$scope.currentRoom = {};
			$scope.state = 10000;
			$scope.selectedTabIndex = 0;
			$scope.fieldData = datacontext.getFieldData();
			$scope.inventorySetting = $scope.fieldData.inventorySetting;
			$scope.filters = [];
			$scope.rooms = [];
			$scope.searchText = '';
			$scope.inventoryItems = [];

			$scope.updateRoomCallBack = updateRoomCallBack;
			$scope.doSearch = doSearch;

			function doSearch() {
				if ($scope.searchText.length >= 2) {
					$scope.busy = true;
					$scope.inventoryItems = [];
					let searchData = {
						'name': $scope.searchText
					};

					apiService.postData(moveBoardApi.inventory.settings.search, searchData)
						.then(res => {
							$scope.inventoryItems = res.data.data;
							$scope.inventoryItems.forEach((item, index) => {
								item.row = index;
								delete item.filter_id;
							});
							$scope.busy = false;
						}, rej => {
							$scope.busy = false;
							SweetAlert.swal('Error', 'Problems with searching', 'error');
						});
				} else if ($scope.activeFilter) {
					$scope.setFilter($scope.currentFilter);
				}
			}

			loadFilters();

			apiService.postData(moveBoardApi.inventory.room.rooms)
				.then(res => {
					$scope.rooms = res.data.data;

					$scope.rooms.forEach((room, index) => {
						room.row = index;
					});

					if (!$scope.inventorySetting) {
						$scope.inventorySetting = {};
						$scope.saveSettings();
					}

					$scope.busy = false;
				}, rej => {
					SweetAlert.swal('Error', 'Cannot load inventory rooms', 'error');
					$scope.busy = false;
				});

			dragulaService.options($scope, 'inventory-room', {
				moves: function (el, container, handle) {
					let wrapHandle = angular.element(handle);
					let isDragTitle = wrapHandle.hasClass('dragula-room-title');
					let isDragImg = wrapHandle.hasClass('dragula-room-img');
					let isFrozenRoom = el.id == ROOM_ALL_ID || el.id == ROOM_BEDROOM_ID;
					return !isFrozenRoom && (isDragTitle || isDragImg);
				},
				accepts: (el, container, handle, sibling) => {
					let isFrozenRoom = sibling.id == ROOM_ALL_ID || sibling.id == ROOM_BEDROOM_ID;
					return !isFrozenRoom;
				}
			});

			$scope.$on('inventory-room.drop', () => {
				let listener = $scope.$watch('rooms', () => {
					$scope.rooms.forEach((room, index) => {
						room.row = index;
					});
					dragRoom();
					listener();
				});
			});

			dragulaService.options($scope, 'inventory-filter', {
				moves: function () {
					return true;
				}
			});

			$scope.$on('inventory-filter.drop', () => {
				let listener = $scope.$watch('currentRoom.filters', () => {
					$scope.currentRoom.filters.forEach((filter, index) => {
						filter.row = index;
					});
					dragFilter();
					listener();
				});
			});

			dragulaService.options($scope, 'inventory-item', {
				moves: function () {
					return true;
				}
			});

			$scope.$on('inventory-item.drop', () => {
				let listener = $scope.$watch('inventoryItems', () => {
					$scope.inventoryItems.forEach((item, index) => {
						item.row = index;
					});
					dragItem();
					listener();
				});
			});

			$scope.openRoom = room => {
				if (room.opened) return;

				$scope.currentRoom = room;
				$scope.rooms.forEach(item => {
					item.opened = false;
				});

				if (!_.isEmpty(room.filters)) {
					room.filters.forEach((filter, index) => {
						filter.row = index;
					});

					$scope.setFilter(room.filters[0], room.filters);
				}

				$timeout(() => {
					room.opened = true;
				}, 100);
			};

			$scope.createPackageCallBack = (inventoryPackage, mode) => {
				if (mode == 'remove') {
					$scope.selectedTabIndex = 0;
					$scope.packages = $scope.packages.filter(item => item.package_id !== inventoryPackage.package_id);
				} else {
					$scope.packages.push(inventoryPackage);
				}
			};

			$scope.addItemToPackageCallBack = () => {
			};

			$scope.createRoomCallBack = room => {
				room.row = $scope.rooms[$scope.rooms.length - 1].row + 1;
				$scope.rooms.push(room);
			};

			function updateRoomCallBack(room, type) {
				if (type == 'remove') {
					let index = _.findIndex($scope.rooms, {room_id: room.room_id});

					if (index >= 0) {
						$scope.rooms.splice(index, 1);
					}
				}
			}

			$scope.initScroll = () => {
				$timeout(() => {
					$('.js-custom-scrollbar').mCustomScrollbar(
						{
							theme: 'dark'
						}
					);
				}, 10);
			};

			$scope.saveSettings = () => {
				SettingServices.saveSettings($scope.inventorySetting, 'inventorySetting')
					.then(function () {
						$scope.fieldData.inventorySetting = $scope.inventorySetting;
					});
			};


			$scope.createFilterCallBack = (filter) => {
				let roomTmp = {};

				filter.rooms.forEach(room => {
					let tmpFilter = angular.copy(filter);
					roomTmp = findRoom(room);
					roomTmp.filters = roomTmp.filters ? roomTmp.filters : [];

					if (tmpFilter.new) {
						tmpFilter.new = false;
						tmpFilter.row = roomTmp.filters.length > 0 ? roomTmp.filters[roomTmp.filters.length - 1].row + 1 : 0;
						roomTmp.filters.push(tmpFilter);
					}
				});

				loadFilters();
			};

			$scope.createItemCallBack = item => {
				var isActive = false;

				item.filters.forEach(filter => {
					if (filter == $scope.activeFilter) {
						isActive = true;
					}
				});

				if (isActive) {
					item.row = $scope.inventoryItems.length;
					$scope.inventoryItems.push(item);
				}
			};

			$scope.$on('removeItemFromInventory', () => $scope.setFilter($scope.currentFilter));
			$scope.$on('reloadInventoryFilter', () => $scope.setFilter($scope.currentFilter));

			$scope.setFilter = (filter, filters) => {
				$scope.rooms.forEach(room => {
					if (room.filters) room.filters.forEach(item => item.active = false);
				});

				filter.active = true;
				$scope.activeFilter = filter.filter_id;
				$scope.currentFilter = filter;
				$scope.busy = true;

				apiService.postData(moveBoardApi.inventory.item.getItems, {
					conditions: {
						'filter_id': filter.filter_id,
						'room_id': $scope.currentRoom.room_id
					}
				}).then(res => {
					$scope.inventoryItems = res.data.data;
					$scope.inventoryItems.forEach((item, index) => item.row = index);
					$scope.busy = false;
				}, rej => {
					$scope.busy = false;
					SweetAlert.swal('Error', 'Cannot load items in filter', 'error');
				});
			};

			$scope.setState = state => {
				$scope.state = state;
			};

			$scope.setPackage = currentPackage => {
				$scope.currentPackage = currentPackage;
				$scope.busy = true;
				$timeout(() => {
					$scope.busy = false;
				}, 100);
			};

			function findRoom(id) {
				return $scope.rooms.filter(item => item.room_id == id)[0];
			}

			function dragRoom() {
				apiService.postData(moveBoardApi.inventory.room.drag, {data: getRoomWeight()});
			}

			function getRoomWeight() {
				return $scope.rooms.map(room => {
					return {
						id: room.room_id,
						weight: room.row
					};
				});
			}

			function dragFilter() {
				apiService.postData(moveBoardApi.inventory.filter.drag, {data: getFilterWeight()});
			}

			function loadFilters() {
				apiService.postData(moveBoardApi.inventory.filter.query, {})
					.then(res => $scope.filters = res.data.data)
					.catch(() => SweetAlert.swal('Error', 'Cannot load filter', 'error'));
			}

			function getFilterWeight() {
				return $scope.currentRoom.filters
					.map(filter => {
						return {
							id: filter.filter_id,
							weight: filter.row
						};
					});
			}

			function dragItem() {
				apiService.postData(moveBoardApi.inventory.item.drag, {data: getItemsWeight()});
			}

			function getItemsWeight() {
				return $scope.inventoryItems
					.map(item => {
						return {
							id: item.item_id,
							weight: item.row,
							parents_ids: {
								filter_id: $scope.activeFilter
							}
						};
					});
			}
		}
	};
}
