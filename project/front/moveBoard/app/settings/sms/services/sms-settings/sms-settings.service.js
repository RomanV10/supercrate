angular
	.module('app.settings.sms')
	.service('smsSettingsService', smsSettingsService);

/* @ngInject */
function smsSettingsService($timeout, $uibModal, moveBoardApi, apiService) {
	let service = {
		getCompanyUsage,
		saveSettings,
		openBuyModal,
		removePhoneNumber,
		buyPhoneNumber,
		findPhoneNumber,
		getAllPhoneNumbers,
		openBuyPhoneNumberModal,
		getSMSByDate,
		getSMSNotificationTemplates,
		getSMSPayment,
		getVariable,
	};

	return service;

	function _returnData({data}) {
		return data;
	}

	function getCompanyUsage() {
		return apiService.postData(moveBoardApi.smsSettings.companyUsage)
			.then(_returnData);
	}

	function saveSettings() {
		return $timeout(() => {

		}, 3000);
	}

	function openBuyModal() {
		return $uibModal.open({
			template: require('../../controllers/buy-sms-modal/buy-sms-modal.html'),
			controller: 'BuySMSController',
			controllerAs: 'vm',
			backdrop: false,
			size: 'lg',
		}).result;
	}

	function removePhoneNumber(number) {
		return apiService.postData(moveBoardApi.smsSettings.phone.delete, {number})
			.then(_returnData);
	}

	function buyPhoneNumber(number) {
		number = number.substr(1);

		return apiService.postData(moveBoardApi.smsSettings.phone.buy, {number})
			.then(_returnData);
	}

	function findPhoneNumber(number) {
		return apiService.postData(moveBoardApi.smsSettings.phone.find, {number})
			.then(_returnData);
	}

	function getAllPhoneNumbers() {
		return apiService.postData(moveBoardApi.smsSettings.phone.index)
			.then(_returnData);
	}

	function openBuyPhoneNumberModal() {
		return $uibModal.open({
			template: require('../../controllers/buy-phone-number-modal/buy-phone-number-modal.html'),
			controller: 'BuyPhoneNumberController',
			controllerAs: 'vm',
			backdrop: false,
			size: 'lg',
		}).result;
	}

	function getSMSByDate(phoneNumber, datefrom, dateto) {
		let number = phoneNumber;

		return apiService.postData(moveBoardApi.smsSettings.getSMSByDate, {number, datefrom, dateto})
			.then(_returnData);
	}

	function getSMSNotificationTemplates() {
		return apiService.postData(moveBoardApi.smsSettings.smsNotificationBuilder.index)
			.then(_returnData);
	}

	function getSMSPayment() {
		return apiService.postData(moveBoardApi.smsSettings.companyPayments)
			.then(_returnData);
	}

	function getVariable(name) {
		return apiService.postData(moveBoardApi.settingService.getVariable, {name})
			.then(_returnData);
	}
}