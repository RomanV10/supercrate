angular
	.module('app.settings.sms')
	.filter('smsDirection', smsDirection);

function smsDirection() {
	return (direction = '') => {

		direction = direction.replace('_', ' ');

		return direction;
	};
}