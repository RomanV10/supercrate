angular
	.module('app.settings.sms')
	.filter('smsDateSent', smsDateSent);

/* @ngInject */
function smsDateSent(moment) {
	const DATE_FORMAT = 'MMM DD, YYYY h:mm a';

	return (date = '') => {
		let result;

		if (!date) {
			result = moment().format(DATE_FORMAT);
		} else {
			result = moment(date).format(DATE_FORMAT);
		}

		return result;
	};
}