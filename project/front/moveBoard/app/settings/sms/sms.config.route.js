'use strict';

angular.module('app.settings.sms')
	.config(smsConfigRoute);

/* @ngInject */
function smsConfigRoute($stateProvider) {
	$stateProvider
		.state('settings.smsSetting', {
			url: '/sms-setting',
			template: '<sms-settings></sms-settings>',
			title: 'SMS Settings',
			data: {
				permissions: {
					except: ['anonymous', 'foreman', 'helper']
				}
			}
		});
}