import './sms-sent-table.styl';

angular
	.module('app.settings.sms')
	.directive('smsSentTable', smsSentTable);

/* @ngInject */
function smsSentTable(smsSettingsService, moment) {

	/* @ngInject */
	function controller($scope, DTOptionsBuilder) {
		$scope.dtOptions = DTOptionsBuilder
			.newOptions()
			.withDisplayLength(25)
			.withOption('order', [0, 'asc']) // Sort by department name
			.withOption('bFilter', true)
			.withOption('bInfo', false)
			.withOption('paging', true)
			.withLanguage({
				'sEmptyTable': 'You don\'t have any messages for this date.'
			});
	}

	return {
		restrict: 'E',
		template: require('./sms-sent-table.html'),
		scope: {
			phoneNumber: '<',
		},
		link,
		controller,
	};

	function link($scope) {
		const DATE_FORMAT = 'YYYY-MM-DD';

		$scope.smsList = [];
		$scope.selectedDateFrom = moment();
		$scope.selectedDateTo = moment();
		$scope.onChangeDateFrom = onChangeDateFrom;
		$scope.onChangeDateTo = onChangeDateTo;

		loadSMSByDate();

		function onChangeDateFrom(newVal, oldVal) {
			$scope.selectedDateFrom = newVal;
			loadSMSByDate();
		}

		function onChangeDateTo(newVal, oldVal) {
			$scope.selectedDateTo = newVal;
			loadSMSByDate();
		}

		function loadSMSByDate() {
			if (!$scope.phoneNumber) return;

			$scope.busy = true;
			let dateFrom = $scope.selectedDateFrom;
			let dateTo = $scope.selectedDateTo;

			if (dateFrom.isAfter(dateTo)) {
				dateTo = $scope.selectedDateFrom;
				dateFrom = $scope.selectedDateTo;
			}

			let formatedDateFrom = dateFrom.format(DATE_FORMAT);
			let formatedDateTo = dateTo.format(DATE_FORMAT);

			smsSettingsService.getSMSByDate($scope.phoneNumber, formatedDateFrom, formatedDateTo)
				.then((smsList) => {
					$scope.smsList = smsList;
				})
				.finally(() => {
					$scope.busy = false;
				});
		}
	}
}
