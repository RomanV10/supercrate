import './sms-phone-numbers-settins.styl';

angular.module('app.settings.sms')
	.directive('smsPhoneNumbersSettings', smsPhoneNumbersSettings);

/* @ngInject */
function smsPhoneNumbersSettings(smsSettingsService) {
	return {
		restrict: 'E',
		template: require('./sms-phone-numbers-settings.html'),
		scope: {
			selectedPhoneNumber: '='
		},
		link,
	};

	function link($scope) {
		$scope.busy = true;
		$scope.phoneNumbers = [];

		loadPhoneNumbers();

		$scope.removeNumber = removeNumber;
		$scope.openBuyPhoneNumber = openBuyPhoneNumber;

		function removeNumber(number) {
			$scope.busy = true;

			smsSettingsService.removePhoneNumber(number)
				.then(() => {
					let index = $scope.phoneNumbers.indexOf(number);

					$scope.selectedPhoneNumber = false;
					$scope.phoneNumbers.splice(index, 1);
				})
				.finally(onFinishRequest);
		}

		function loadPhoneNumbers() {
			$scope.busy = true;

			smsSettingsService.getAllPhoneNumbers()
				.then(numbers => {
					$scope.phoneNumbers = numbers;

					if (!_.isEmpty(numbers)) {
						$scope.selectedPhoneNumber = _.head(numbers);
					}
				})
				.finally(onFinishRequest);
		}

		function onFinishRequest() {
			$scope.busy = false;
		}

		function openBuyPhoneNumber() {
			smsSettingsService.openBuyPhoneNumberModal()
				.then((number) => {
					if (number[0] == '+') {
						number = number.substr(1);
					}

					$scope.selectedPhoneNumber = number;
					$scope.phoneNumbers.push(number);
				});
		}
	}
}
