import './er-payment-form.styl';

angular.module('app.settings')
	.directive('erPaymentForm', erPaymentForm);

/* @ngInject */
function erPaymentForm(geoCodingService, SweetAlert) {
	return {
		resrict: 'E',
		template: require('./er-payment-form.html'),
		scope: {
			payerData: '=',
			secure: '=',
			onValid: '=?',
		},
		link,
	};

	function link($scope) {
		const POSTAL_CODE_SIZE = 5;
		const ERROR_ZIP_CODE_MESSAGE = 'You entered the wrong zip code.';
		$scope.$watch('payerData.billing_zip', onChangeBillingZip);
		$scope.$watch('erPayment.$valid', onChangeValid);
		$scope.cvv = {
			Amex: false,
		};

		if (_.isEmpty($scope.secure)) {
			$scope.secure.cvc = '';
			$scope.secure.cvcAmex = '';
		}

		function onChangeBillingZip(newValue, oldValue) {
			if (!_.isEmpty(newValue) && newValue != oldValue && newValue.length == POSTAL_CODE_SIZE) {
				onChangeValid();

				geoCodingService.geoCode(newValue)
					.then(function (result) {
						$scope.payerData.billing_city = result.city;
						$scope.payerData.billing_state = result.state;
					}, function () {
						$scope.payerData.billing_city = '';
						$scope.payerData.billing_state = '';
						SweetAlert.swal(ERROR_ZIP_CODE_MESSAGE, '', 'error');
					});
			}
		}

		function onChangeValid() {
			if ($scope.onValid) {
				let zip = _.get($scope, 'payerData.billing_zip') || '';
				let isValidBillingZip = zip.length == POSTAL_CODE_SIZE;
				$scope.onValid($scope.erPayment.$valid);
			}
		}
	}
}