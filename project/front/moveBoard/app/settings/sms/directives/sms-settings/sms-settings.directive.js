import './sms-settings.styl';

angular
	.module('app.settings.sms')
	.directive('smsSettings', smsSettings);

/* @ngInject */
function smsSettings(smsSettingsService, $q, apiService, moveBoardApi) {

	/* @ngInject */
	function controller($scope, SweetAlert) {
		let vm = this;

		vm.tabs = [
			{
				name: 'Settings',
				selected: true,
			},
			{
				name: 'Payment History',
				selected: false,
			},
			{
				name: 'SMS History',
				selected: false,
			},
		];

		vm.select = select;

		function select(tab, index) {
			if (index == 2 && !$scope.phoneNumber) {
				SweetAlert.swal('', 'Please buy phone number first', 'warning');
				return;
			}

			vm.tabs.forEach(tab => tab.selected = false);
			tab.selected = true;
		}
	}

	return {
		restrict: 'E',
		template: require('./sms-settings.html'),
		scope: {},
		link,
		controller,
		controllerAs: 'vm',
	};

	function link($scope) {
		const ENABLE_SMS_SETTINGS = 'smsEnable';
		$scope.saving = false;
		$scope.smsPriceSpent = 0;
		$scope.smsPhoneNumberSpent = 0;
		$scope.smsCountSent = 0;
		$scope.balance = 0;
		$scope.phoneNumber = false;

		$scope.saveSettings = saveSettings;
		$scope.openBuyMoreSMS = openBuyMoreSMS;
		$scope.refreshSettings = refreshSettings;

		refreshSettings();

		function saveSettings() {
			if ($scope.busy) return;
			$scope.saving = true;

			apiService.postData(moveBoardApi.settingService.setVariable, {
				name: ENABLE_SMS_SETTINGS,
				value: Number($scope.isEnabled),
			})
				.finally(() => {
					$scope.saving = false;
				});
		}

		function openBuyMoreSMS() {
			smsSettingsService.openBuyModal()
				.then(refreshSettings);
		}

		function refreshSettings() {
			$scope.busy = true;
			let getCompanyUsage = smsSettingsService.getCompanyUsage();
			let getEnableSms = apiService.postData(moveBoardApi.settingService.getVariable, {name: ENABLE_SMS_SETTINGS});

			$q.all({getCompanyUsage, getEnableSms})
				.then(({getCompanyUsage, getEnableSms: {data: [enabled]}}) => {
					let {smsPrice = 0, phoneNumbersPrice = 0, smsCount = 0, balance = 0} = getCompanyUsage;
					$scope.isEnabled = !!Number(enabled);
					$scope.smsPriceSpent = smsPrice;
					$scope.smsPhoneNumberSpent = phoneNumbersPrice;
					$scope.smsCountSent = smsCount;
					$scope.balance = balance;
				})
				.finally(() => {
					$scope.busy = false;
				});

			$scope.$broadcast('onReloadSettings');
		}
	}
}
