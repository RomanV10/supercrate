import './sms-payment-history.styl';

angular.module('app.settings.sms')
	.directive('smsPaymentHistory', smsPaymentHistory);

/* @ngInject */
function smsPaymentHistory(smsSettingsService) {
	/* @ngInject */
	function controller($scope, DTOptionsBuilder) {
		$scope.dtOptions = DTOptionsBuilder
			.newOptions()
			.withDisplayLength(25)
			.withOption('order', [0, 'asc']) // Sort by department name
			.withOption('bFilter', false)
			.withOption('bInfo', false)
			.withOption('paging', true)
			.withLanguage({
				'sEmptyTable': 'You don\'t have any payment'
			});
	}

	return {
		restrict: 'E',
		template: require('./sms.payment-history.html'),
		scope: {},
		link,
		controller,
	};

	function link($scope) {
		const DATE_FORMAT = 'MMM DD, YYYY h:mm a';
		const ONE_HUNDRED_CENTS = 100;
		$scope.smsPaymentHistory = [];
		$scope.amount = 0;

		$scope.$on('onReloadSettings', loadPayment);

		loadPayment();

		function loadPayment() {
			smsSettingsService.getSMSPayment()
				.then(({amount, payments}) => {
					$scope.amount = amount / ONE_HUNDRED_CENTS;
					payments.forEach(payment => {
						payment.amount /= ONE_HUNDRED_CENTS;
						payment.created = moment.unix(payment.created)
							.format(DATE_FORMAT);
					});
					$scope.smsPaymentHistory = payments;
				});
		}
	}
}