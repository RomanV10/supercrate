import './buy-sms.styl';

angular.module('app.settings.sms')
	.controller('BuySMSController', BuySMSController);

/* @ngInject */
function BuySMSController($uibModalInstance, apiService, moveBoardApi, stripe, datacontext, $q, SweetAlert, smsSettingsService) {
	let fieldData = datacontext.getFieldData();
	let basicsettings = angular.fromJson(fieldData.basicsettings);
	const STEP1_TITLE = 'How much money would you like to charge?';
	const STEP2_TITLE = 'Payment';
	const STRIPE_CUSTOMER_EMAIL = 'stripeCustomerEmail';
	const STRIPE_CUSTOMER_ID = 'stripeCustomerId';
	const STRIPE_CUSTOMER_AUTOCHARGE = 'stripeAutoCharge';
	const STRIPE_CUSTOMER_CARD_LAST_FOUR = 'stripeCustomerCardLast4';
	const STRIPE_CUSTOMER_CARD_BRAND = 'stripeCustomerCardBrand';
	const MINIMUM_AMOUNT = 50;
	const COMPANY_CARD_BRANDS = {
		'Visa': true,
		'Mastercard': true,
		'AMEX': true,
		'Discover': true,
	};
	let vm = this;
	vm.step1 = true;
	vm.step2 = false;
	vm.price = MINIMUM_AMOUNT;
	vm.title = STEP1_TITLE;
	vm.secure = {};
	vm.payerData = {};
	vm.chargeDifferentCreditCard = false;
	vm.recurring = {
		id: 0,
		enable: false,
		amount: MINIMUM_AMOUNT,
		email: basicsettings.company_email
	};
	vm.cardLastFour = '';
	vm.cardBrand = '';
	vm.isExistCardBrand = false;

	vm.close = close;
	vm.goToStep = goToStep;
	vm.submit = submit;
	vm.onValid = onValid;
	vm.onChangeAmount = onChangeAmount;
	vm.onChangeRecurringAmount = onChangeRecurringAmount;
	vm.onChangeEnableRecurring = onChangeEnableRecurring;

	loadAllSettings();

	function loadAllSettings() {
		vm.busy = true;

		let mail = smsSettingsService.getVariable(STRIPE_CUSTOMER_EMAIL);
		let id = smsSettingsService.getVariable(STRIPE_CUSTOMER_ID);
		let autocharge = smsSettingsService.getVariable(STRIPE_CUSTOMER_AUTOCHARGE);
		let cardLastFour = smsSettingsService.getVariable(STRIPE_CUSTOMER_CARD_LAST_FOUR);
		let cardBrand = smsSettingsService.getVariable(STRIPE_CUSTOMER_CARD_BRAND);

		$q.all({mail, id, autocharge, cardLastFour, cardBrand})
			.then(({mail, id, autocharge, cardLastFour, cardBrand}) => {
				if (mail) {
					vm.recurring.email = mail[0] || basicsettings.company_email;
				}

				if (id) {
					vm.recurring.id = id[0];
				}

				if (autocharge) {
					vm.recurring.enable = !!Number(autocharge[0]);
				}

				if (cardLastFour) {
					vm.cardLastFour = cardLastFour[0] || '';
				}

				if (cardBrand) {
					vm.cardBrand = cardBrand[0];
					vm.isExistCardBrand = !!COMPANY_CARD_BRANDS[vm.cardBrand];
				}
			})
			.finally(() => {
				vm.busy = false;
			});
	}

	function close() {
		$uibModalInstance.dismiss('cancel');
	}

	function goToStep(step) {
		if (step == 1) {
			vm.step1 = true;
			vm.step2 = false;
			vm.title = STEP1_TITLE;
		}

		if (step == 2) {
			vm.step1 = false;
			vm.step2 = true;
			vm.title = STEP2_TITLE;
		}
	}

	function submit() {
		vm.busy = true;

		let promise;

		if (vm.recurring.enable && !vm.chargeDifferentCreditCard) {
			if (vm.recurring.id) {
				promise = apiService.postData(moveBoardApi.smsSettings.autoRecurringWithoutCreate, {amount: +vm.recurring.amount, customerId: ''});
			}
		}

		if (!promise) {
			promise = stripe.card.createToken({
				number: vm.payerData.card_num,
				exp_month: vm.payerData.exp_month,
				exp_year: vm.payerData.exp_year,
				cvc: vm.secure.cvc || vm.secure.cvcAmex,
			}).then((response) => {
				let token = response.id;
				let amount = vm.price;

				if (vm.recurring.enable) {
					return apiService.postData(moveBoardApi.smsSettings.autoRecurring, {token, email: vm.recurring.email, amount: +vm.recurring.amount});
				}

				return apiService.postData(moveBoardApi.smsSettings.payForSMS, {data: {token, amount}});
			});
		}

		promise.then(() => {
			toastr.success('Success');
			$uibModalInstance.close('succes');
		}).catch((error) => {
			toastr.error('Error');
			close();
		}).finally(() => {
			vm.busy = false;
		});
	}

	function onValid(isValid) {
		vm.valid = isValid;
	}

	function onChangeAmount() {
		if (vm.price < MINIMUM_AMOUNT) {
			vm.price = MINIMUM_AMOUNT;
			showWarning();
		}

		vm.recurring.amount = vm.price;
	}

	function onChangeRecurringAmount() {
		if (vm.recurring.amount < MINIMUM_AMOUNT) {
			vm.recurring.amount = MINIMUM_AMOUNT;
			showWarning();
		}
	}

	function showWarning() {
		SweetAlert.swal('', '$50 is minimum', 'warning');
	}

	function onChangeEnableRecurring() {
		if (vm.recurring.id) {
			apiService.postData(moveBoardApi.settingService.setVariable, {name: STRIPE_CUSTOMER_AUTOCHARGE, value: Number(vm.recurring.enable)});
		}
	}
}