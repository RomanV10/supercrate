import './buy-phone-number-modal.styl';

angular.module('app.settings.sms')
	.controller('BuyPhoneNumberController', BuyPhoneNumberController);

/* @ngInject */
function BuyPhoneNumberController($uibModalInstance, smsSettingsService) {
	let vm = this;
	vm.suggestNumbers = [];
	vm.selected = '';
	vm.phoneNumberPrice = 1;

	vm.close = close;
	vm.submit = submit;
	vm.searchPhoneNumbers = searchPhoneNumbers;
	vm.choosePhoneNumber = choosePhoneNumber;

	function submit() {
		if (!vm.selected) return;

		vm.busy = true;

		smsSettingsService.buyPhoneNumber(vm.selected)
			.then(() => {

			})
			.finally(() => {
				vm.busy = false;
				$uibModalInstance.close(vm.selected);
			});
	}

	function close() {
		$uibModalInstance.dismiss('cancel');
	}

	function searchPhoneNumbers() {
		if ((vm.searchNumber || 0).toString().length != 3) return;

		smsSettingsService.findPhoneNumber(vm.searchNumber)
			.then((numbers = []) => {
				vm.suggestNumbers = numbers;
			});
	}

	function choosePhoneNumber(number) {
		vm.selected = number;
	}
}