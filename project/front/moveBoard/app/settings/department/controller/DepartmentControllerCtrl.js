angular
.module('app.department').controller('DepartmentInstanceController', DepartmentInstanceController);

DepartmentInstanceController.$inject = ['$scope', '$rootScope', '$uibModalInstance', 'DepartmentServices', 'editData', 'SweetAlert', 'logger', 'modalNewRequestService', 'departmentConstants', 'PermissionsServices', 'BranchAPIService', 'config', 'apiService', 'moveBoardApi', 'datacontext', '$uibModal'];

function DepartmentInstanceController($scope, $rootScope, $uibModalInstance, DepartmentServices, editData, SweetAlert, logger, modalNewRequestService, departmentConstants, PermissionsServices, BranchAPIService, config, apiService, moveBoardApi, datacontext, $uibModal) {

	$scope.allBranches = angular.copy(datacontext.getFieldData().branches);

	function filterBranches() {
		var index = _.findIndex($scope.allBranches, function (branch) {
			return config.serverUrl.indexOf(branch.api_url) >= 0;
		});

		if (index >= 0) {
			$scope.allBranches.splice(index, 1);
		}
	}

	filterBranches();

	$scope.isActiveBranching = $scope.allBranches.length >= 1 && PermissionsServices.hasPermission('administrator');
	$scope.selectedBranchId = '';

	$scope.copyWorkerToBranch = (id) => {
		$scope.loader.busy = true;
		var branch = _.find($scope.allBranches, {id: id});

		BranchAPIService
		.copyWorkerToBranch(editData.uid, id)
		.then(function (response) {
			if (_.isEmpty(response)) {
				toastr.error('Error. When copy manager to branch ' + branch.name);
			} else {
				var firstName = response.name;
				toastr.success('Success. ' + firstName + ' was copy to branch ' + branch.name);
			}
		}, function () {
			toastr.error('User with this email already exists on another branch');
		})
		.finally(function () {
			$scope.loader.busy = false;
		});
	};

	var optionsList = angular.copy(departmentConstants.optionListSettings);
	var workerRole;
	var HOURLY_RATE_FORMAN_AS_HELPER = "Hourly Rate (as helper)";

	$scope.currentUser = $rootScope.currentUser.userId.name; // get current user name for deleting user

	angular.forEach(editData.roles, function (role) {
		if (role !== 'authenticated user' && role !== 'home-estimator') {
			workerRole = role.charAt(0).toUpperCase() + role.slice(1);

			if (_.isEqual(workerRole, 'Sales') && editData.settings.isCustomerService) {
				workerRole = 'Customer service';
			}
		}
	});

	if (editData.user_picture) {
		$scope.imageUrl = config.serverUrl + "/sites/default/files/pictures/" + editData.user_picture.filename;
	} else {
		$scope.imageUrl = "content/img/user_pic.jpg";
	}


	if (editData.roles.indexOf('foreman') > -1) {
		var index = _.findIndex(optionsList.optionList, {name: HOURLY_RATE_FORMAN_AS_HELPER});

		if (index < 0) {
			optionsList.optionList.push({
				name: HOURLY_RATE_FORMAN_AS_HELPER, optionDisabled: false
			})
		}
	}

	apiService.postData(moveBoardApi.notifications.getNotifications, {}).then(data => {
		$scope.notificationTypes = data.data;

		apiService.postData(moveBoardApi.notifications.allowedNotificationTypes, {uid: editData.uid}).then(data => {
			for (var i = 0; i < $scope.notificationTypes.length; i++) {
				for (var j = 0; j < data.data.length; j++) {
					if ($scope.notificationTypes[i].ntid == data.data[j].ntid) {
						$scope.notificationTypes[i].selected = true;
					}
				}

			}
			switchAllNotification();
		});
	});

	$scope.addNotificationsToUser = () => {
		switchAllNotification();
		editData.selectedNotifications = $scope.getSelectedNotifications();
		apiService.postData(moveBoardApi.notifications.addNotificationsToUser, {
			uid: editData.uid,
			types: $scope.getSelectedNotifications()
		});
	}

	$scope.getSelectedNotifications = () => {
		var result = [];
		for (var i = 0; i < $scope.notificationTypes.length; i++) {
			if ($scope.notificationTypes[i].selected) {
				result.push($scope.notificationTypes[i].ntid);
			}
		}
		return result;
	}

	$scope.$watch('request.workerPosition', function (newPosition, oldPosition) {
		if (newPosition && newPosition.toString() == 'Foreman') {
			var index = _.findIndex(optionsList.optionList, {name: HOURLY_RATE_FORMAN_AS_HELPER});

			if (index < 0) {
				optionsList.optionList.push({
					name: HOURLY_RATE_FORMAN_AS_HELPER, optionDisabled: false
				})
			}
		} else {
			var index = _.findIndex(optionsList.optionList, {name: HOURLY_RATE_FORMAN_AS_HELPER});

			if (index >= 0) {
				optionsList.optionList.splice(index, 1);
			}

			index = _.findIndex($scope.rateCommission, {option: HOURLY_RATE_FORMAN_AS_HELPER});

			if (index >= 0) {
				$scope.rateCommission.splice(index, 1);
			}
		}
	});

	$scope.btnName = 'Save';
	$scope.loader = {};
	$scope.request = {};
	$scope.notificationsGroup = 0;
	$scope.selectAllNotifications = [false, false, false];
	$scope.request.email_account = angular.copy(departmentConstants.emailAccount);
	$scope.busyTestEmail = true;
	// $scope.uploadPhoto = uploadPhoto; //What have to do this?
	$scope.commissionsFroSales = commissionsFroSales;

	$scope.isDisableTestButton = (form) => {
		return form.$invalid
			|| !$scope.request.email_account.outgoing.active
			|| $scope.isInvalidOutgoingServer(form)
			|| $scope.isInvalidOutgoingPort(form)
			|| $scope.isInvalidOutgoingEmail(form)
			|| $scope.isInvalidOutgoingPassword(form)
			|| !$scope.busyTestEmail
	};

	$scope.excludedAdditionalServices = function () {
		DepartmentServices.excludedAdditionalServices($scope.request.excluded_additional_services).then(function (data) {
			$scope.request.excluded_additional_services = data.data;
		});
	};

	$scope.chooseConnectionType = function (type) {
		if (type == 'tls') {
			$scope.request.email_account.outgoing.ssl = false;
		} else {
			$scope.request.email_account.outgoing.tls = false;
		}
	};

	$scope.isInvalidOutgoingServer = (form) => {
		return form.outgoingServer.$touched && form.outgoingServer.$invalid;
	};

	$scope.isInvalidOutgoingPort = (form) => {
		return form.outgoingPort.$touched && form.outgoingPort.$invalid;
	};

	$scope.isInvalidOutgoingEmail = (form) => {
		return form.outgoingEmail.$touched && form.outgoingEmail.$invalid;
	};

	$scope.isInvalidOutgoingPassword = (form) => {
		return form.outgoingPassword.$touched && form.outgoingPassword.$invalid;
	};

	$scope.request.originGoogleCalendars = [];
	$scope.request.phoneType1 = editData.settings.phoneType1 || 'Home';
	$scope.request.phoneType2 = editData.settings.phoneType2 || 'Cellular';
	$scope.request.creationDate = moment(parseInt(editData.created) * 1000).format('YYYY-MM-DD');
	$scope.request.login = editData.name;
	$scope.request.email = editData.mail;
	$scope.request.workerPosition = workerRole;
	$scope.request.firstName = editData.field_user_first_name || '';
	$scope.request.lastName = editData.field_user_last_name || '';
	$scope.request.gmail = editData.field_google_calendar_email || '';
	$scope.request.address = editData.settings.address || '';
	$scope.request.city = editData.settings.city || '';
	$scope.request.branch = editData.settings.branch || '';
	$scope.request.driverLicense = editData.settings.driver_license || '';
	$scope.request.ssn = editData.settings.ssn || '';
	$scope.request.dateOfBirth = editData.settings.date_of_birth || '';
	$scope.request.department = editData.department;
	$scope.request.dateStarted = editData.settings.date_started || '';
	$scope.request.dateLeaving = editData.settings.date_leaving || '';
	$scope.request.contactName = editData.settings.contact_name || '';
	$scope.request.relationship = editData.settings.relationship || '';
	$scope.request.secondaryEmail = editData.settings.email1 || '';
	$scope.request.message = editData.settings.message || '';
	$scope.request.zip = editData.settings.zip || '';
	$scope.request.password = editData.settings.password || '';
	$scope.request.estimatedClosingPrice = editData.settings.estimated_closing_price;
	$scope.request.employeeIsActive = editData.status;
	$scope.request.phone1 = editData.settings.field_primary_phone || '';
	$scope.request.phone2 = editData.settings.field_user_additional_phone || '';
	$scope.request.signature = editData.settings.signature || angular.copy(departmentConstants.signatureSettings);
	$scope.request.extraSignature = editData.settings.extraSignature || {};
	$scope.request.excluded_additional_services = editData.settings.excluded_additional_services || [];
	$scope.request.accountDetails = editData.settings.accountDetails || angular.copy(departmentConstants.accountDetails);
	$scope.request.homeEstimator = editData.settings.homeEstimator || false;
	$scope.request.worker = {
		administrative_area: editData.settings.administrative_area,
		locality: editData.settings.locality
	};

	if ($scope.request.workerPosition == 'Foreman') {
		$scope.request.foremanArrivyId = editData.field_arrivy_user_id;
	}

	if ($scope.request.workerPosition == 'Foreman' || $scope.request.workerPosition == 'Helper' || $scope.request.workerPosition == 'Driver') {
		$scope.request.field_notification_mail = editData.settings.field_notification_mail || editData.mail;
	}

	$scope.autoAssignment = departmentConstants.autoAssignment;

	if (!_.isUndefined(editData.settings.autoassignment)) {
		$scope.request.autoassignment = {};
		angular.copy(editData.settings.autoassignment, $scope.request.autoassignment);

		if ($scope.request.autoassignment.services.length > 0 && $scope.request.autoassignment.services.length < 8) {
			for (var i = 1; i <= 7; i++) {
				checkAssigmentService(i, $scope.request.autoassignment.services.indexOf(i) != -1);
			}
		} else {
			if ($scope.request.autoassignment.services.length == 8) {
				checkAssigmentService(0, true);
			}
		}
	} else {
		$scope.request.autoassignment = angular.copy(departmentConstants.autoAssignmentSettings);
	}

	if (angular.isArray(editData.settings.email_account)) {
		$scope.request.email_account = angular.copy(departmentConstants.emailAccount);
	} else {
		$scope.request.email_account = editData.settings.email_account;
	}

	$scope.editData = editData;
	$scope.branches = departmentConstants.branches;
	$scope.departments = departmentConstants.departments;
	$scope.positions = departmentConstants.typeWorkers;
	$scope.rateAndCommissionMS = departmentConstants.rateAndCommissionMS;
	$scope.mailDomain = (angular.fromJson((datacontext.getFieldData()).basicsettings))['mail_domain'];

	$scope.request.estimatedClosingPriceMS = editData.settings.estimated_closing_price;
	$scope.request.excludeFuelMS = !!editData.settings.exclude_fuel;
	$scope.request.excludeValuationMS = !!editData.settings.exclude_valuation;
	$scope.request.excludePackingMS = !!editData.settings.exclude_packing;
	$scope.request.excludeAdditionalMS = !!editData.settings.exclude_additional;
	$scope.request.userId = editData.uid;

	$scope.rateAndCommissions = [];
	$scope.rateCommission = [];

	if ($scope.request.workerPosition == 'Manager' || $scope.request.workerPosition == 'Sales' || $scope.request.workerPosition == 'Customer service' || $scope.request.workerPosition.split(' ')[0] == 'Customer') {
		$scope.request.permissions = _.get(editData, "settings.permissions", {});

		if (_.isArray($scope.request.permissions))
			$scope.request.permissions = _.assign({}, $scope.request.permissions);

		if (editData.settings.rateCommission && editData.settings.rateCommission.length > 0) {
			$scope.request.localMoveRateCommission1 = editData.settings.rateCommission[0].option || $scope.rateAndCommissionMS[0];
			$scope.request.localMoveRateCommissionInput1 = editData.settings.rateCommission[0].input || 0;
			$scope.request.localMoveRateCommission2 = editData.settings.rateCommission[1].option || $scope.rateAndCommissionMS[1];
			$scope.request.localMoveRateCommissionInput2 = editData.settings.rateCommission[1].input || 0;
			$scope.request.localMoveRateCommission3 = editData.settings.rateCommission[2].option || $scope.rateAndCommissionMS[2];
			$scope.request.localMoveRateCommissionInput3 = editData.settings.rateCommission[2].input || 0;

			$scope.request.longDistanceMoveRateCommission1 = editData.settings.rateCommission[3].option || $scope.rateAndCommissionMS[0];
			$scope.request.longDistanceMoveRateCommissionInput1 = editData.settings.rateCommission[3].input || 0;
			$scope.request.longDistanceMoveRateCommission2 = editData.settings.rateCommission[4].option || $scope.rateAndCommissionMS[1];
			$scope.request.longDistanceMoveRateCommissionInput2 = editData.settings.rateCommission[4].input || 0;
			$scope.request.longDistanceMoveRateCommission3 = editData.settings.rateCommission[5].option || $scope.rateAndCommissionMS[2];
			$scope.request.longDistanceMoveRateCommissionInput3 = editData.settings.rateCommission[5].input || 0;

			$scope.request.internationalRateCommission1 = editData.settings.rateCommission[6].option || $scope.rateAndCommissionMS[0];
			$scope.request.internationalRateCommissionInput1 = editData.settings.rateCommission[6].input || 0;
			$scope.request.internationalRateCommission2 = editData.settings.rateCommission[7].option || $scope.rateAndCommissionMS[1];
			$scope.request.internationalRateCommissionInput2 = editData.settings.rateCommission[7].input || 0;
			$scope.request.internationalRateCommission3 = editData.settings.rateCommission[8].option || $scope.rateAndCommissionMS[2];
			$scope.request.internationalRateCommissionInput3 = editData.settings.rateCommission[8].input || 0;
		} else {
			$scope.request.localMoveRateCommission1 = $scope.rateAndCommissionMS[0];
			$scope.request.localMoveRateCommissionInput1 = 0;
			$scope.request.localMoveRateCommission2 = $scope.rateAndCommissionMS[1];
			$scope.request.localMoveRateCommissionInput2 = 0;
			$scope.request.localMoveRateCommission3 = $scope.rateAndCommissionMS[2];
			$scope.request.localMoveRateCommissionInput3 = 0;

			$scope.request.longDistanceMoveRateCommission1 = $scope.rateAndCommissionMS[0];
			$scope.request.longDistanceMoveRateCommissionInput1 = 0;
			$scope.request.longDistanceMoveRateCommission2 = $scope.rateAndCommissionMS[1];
			$scope.request.longDistanceMoveRateCommissionInput2 = 0;
			$scope.request.longDistanceMoveRateCommission3 = $scope.rateAndCommissionMS[2];
			$scope.request.longDistanceMoveRateCommissionInput3 = 0;

			$scope.request.internationalRateCommission1 = $scope.rateAndCommissionMS[0];
			$scope.request.internationalRateCommissionInput1 = 0;
			$scope.request.internationalRateCommission2 = $scope.rateAndCommissionMS[1];
			$scope.request.internationalRateCommissionInput2 = 0;
			$scope.request.internationalRateCommission3 = $scope.rateAndCommissionMS[2];
			$scope.request.internationalRateCommissionInput3 = 0;
		}
	} else {
		if (editData.settings.rateCommission && editData.settings.rateCommission.length > 0) {
			angular.forEach(editData.settings.rateCommission, function (rateCommission, key) {
				angular.forEach(optionsList.optionList, function (item, i) {
					if (rateCommission.option == 'Materials Commission') {
						rateCommission.option = 'Packing Commission';
					}
					if (rateCommission.option == 'Advanced Service') {
						rateCommission.option = 'Additional Service';
					}
					if (item.name == rateCommission.option) {
						var optionToPush = _.clone(optionsList);
						$scope.rateAndCommissions.push(optionToPush);
						if (!$scope.rateAndCommissions[key]) {
							key = $scope.rateAndCommissions.length - 1;
						}
						$scope.rateAndCommissions[key].optionList[i].optionDisabled = true;
						$scope.rateAndCommissions[key].inputValue = rateCommission.input;
						$scope.rateCommission[key] = {};
						$scope.rateCommission[key].option = rateCommission.option;
						$scope.rateCommission[key].input = rateCommission.input;
					}
				});
			});
		} else {
			$scope.rateAndCommissions = [optionsList];
			$scope.rateCommission = [];
		}
	}

	function commissionsFroSales(arr) {
		if ($scope.request.workerPosition == 'Sales') {
			return arr.slice(0, 1);
		} else {
			return arr;
		}
	}

	$scope.testEmailConnection = function () {
		$scope.busyTestEmail = false;
		DepartmentServices.testEmailConnection($scope.request.email_account)
		.then(function (data) {
				var msg = _.head(data.data);
				if (msg == 1) {
					toastr.success('Connection success');
				} else {
					SweetAlert.swal(msg, '', "error");
				}
			},
			function (err) {
				SweetAlert.swal("Error", err, "error");
			})
		.finally(function () {
			$scope.busyTestEmail = true;
		});
	};

	$scope.hideOption = function (item) {
		return item.name != '';
	};

	$scope.setUserParser = function () {
		DepartmentServices.setUserParser(editData.uid);
	};

	$scope.getCorrespondingPhone = function () {

	};

	$scope.deleteRow = function ($index) {
		angular.forEach($scope.rateAndCommissions, function (rateAndCommissions) {
			angular.forEach(rateAndCommissions.optionList, function (item) {
				if ($scope.rateCommission[$index].option === item.name) {
					if ($scope.rateCommission[$index].option == 'Commission from total') {
						$scope.request.excluded_additional_services = [];
					}
					item.optionDisabled = false;
				}
			});
		});
		$scope.rateAndCommissions.splice($index, 1);
		$scope.rateCommission.splice($index, 1);
	};

	$scope.addRow = function () {
		if ($scope.rateAndCommissions.length === 0) {
			$scope.rateAndCommissions.push(optionsList);

			return;
		}

		var keepGoing = true;

		angular.forEach($scope.rateAndCommissions, function (rateAndCommissions) {
			angular.forEach(rateAndCommissions.optionList, function (item) {
				if (keepGoing) {
					if (item.optionDisabled === false && item.name != '') {
						$scope.rateAndCommissions.push(optionsList);
						$scope.rateCommission.push({option: item.name, input: 0});
						item.optionDisabled = true;
						keepGoing = false;
					}
				}
			});
		});
	};

	$scope.disableCommissions = function () {
		_.forEach($scope.rateAndCommissions, function (commissions) {
			_.forEach(commissions.optionList, function (option) {
				option.optionDisabled = false;
			});
		});
		_.forEach($scope.rateCommission, function (item) {
			_.forEach($scope.rateAndCommissions, function (commissions) {
				_.forEach(commissions.optionList, function (option) {
					if (item.option == option.name) {
						option.optionDisabled = true;
					}
				});
			});
		});
	};

	$scope.checkRoles = DepartmentServices.checkRoles;

	$scope.checkLogin = checkLogin;

	function checkLogin(login) {

		if (login == editData.name) {
			$scope.userIsExist = false;
		} else {
			apiService.postData(moveBoardApi.department.checkLogin, {login: login})
				.then(
					res => {
						$scope.userIsExist = _.get(res, 'data.data');
					},
					rej => {
						logger.error('Can not check login');
					}
				);
		}
	}

	$scope.create = function () {
		let isGenerateEmail = _.isUndefined($scope.request.email);

		if ($scope.request.workerPosition == 'Helper' || $scope.request.workerPosition == 'Driver') {
			var firstName = $scope.request.firstName.replace(/[,\s]+|[,\s]+/g, '');
			var lastName = $scope.request.lastName.replace(/[,\s]+|[,\s]+/g, '');
			$scope.request.email = DepartmentServices.generateEmail(firstName, lastName);
			$scope.request.login = $scope.request.email;
			$scope.request.password = $scope.request.login;
		}

		if (isGenerateEmail) {
			$scope.request.email = DepartmentServices.generateEmail($scope.request.firstName, $scope.request.lastName);
		}

		if (!($scope.request.firstName &&
				$scope.request.email &&
				$scope.request.lastName &&
				$scope.request.phone1 &&
				$scope.request.login)) {
			return;
		}

		var workerPosition = $scope.request.workerPosition ? $scope.request.workerPosition.toLowerCase() : '';
		var request = {
			address: $scope.request.address,
			city: $scope.request.city,
			creation_date: $scope.request.creationDate,
			first_name: $scope.request.firstName,
			last_name: $scope.request.lastName,
			date_started: $scope.request.dateStarted,
			date_leaving: $scope.request.dateLeaving,
			driver_license: $scope.request.driverLicense,
			ssn: $scope.request.ssn,
			date_of_birth: $scope.request.dateOfBirth,
			branch: $scope.request.branch,
			department: $scope.request.department,
			roles: [],
			password: $scope.request.password,
			email: $scope.request.email,
			contact_name: $scope.request.contactName,
			relationship: $scope.request.relationship,
			message: $scope.request.message,
			employee_is_active: Number($scope.request.employeeIsActive),
			email_account: angular.copy(departmentConstants.emailAccount),
			email1: $scope.request.secondaryEmail,
			phoneType1: $scope.request.phoneType1,
			phoneType2: $scope.request.phoneType2,
			field_primary_phone: $scope.request.phone1,
			field_user_additional_phone: $scope.request.phone2,
			exclude_fuel: $scope.request.excludeFuelMS,
			exclude_valuation: $scope.request.excludeValuationMS,
			exclude_packing: $scope.request.excludePackingMS,
			exclude_additional: $scope.request.excludeAdditionalMS,
			excluded_additional_services: $scope.request.excludeAdditionalMS ? $scope.request.excluded_additional_services : [],
			accountDetails: $scope.request.accountDetails,
			homeEstimator: $scope.request.homeEstimator
		};
		if (workerPosition == 'foreman' || workerPosition == 'helper' || workerPosition == 'driver') {
			request.field_notification_mail = $scope.request.field_notification_mail || $scope.request.email;
		}

		request.roles.push(workerPosition);

		if ($scope.request.zip) {
			request.zip = $scope.request.zip;
		}

		if (_.isObject($scope.request.worker)) {
			request.locality = $scope.request.worker.locality;
			request.administrative_area = $scope.request.worker.administrative_area;
		}

		if ($scope.request.workerPosition == 'Manager'
			|| $scope.request.workerPosition == 'Sales'
			|| $scope.request.workerPosition == 'Customer service'
			|| $scope.request.workerPosition == 'Administrator') {

			request.autoassignment = angular.copy($scope.request.autoassignment);
		}

		// checking if position manager or sales or customer sevice
		if ($scope.request.workerPosition == 'Manager' || $scope.request.workerPosition == 'Sales' || $scope.request.workerPosition == 'Customer service' || $scope.request.workerPosition == 'Administrator') {
			request.estimated_closing_price = $scope.request.estimatedClosingPriceMS;
			request.rateCommission = [
				{
					option: $scope.request.localMoveRateCommission1,
					input: $scope.request.localMoveRateCommissionInput1
				},
				{
					option: $scope.request.localMoveRateCommission2,
					input: $scope.request.localMoveRateCommissionInput2
				},
				{
					option: $scope.request.localMoveRateCommission3,
					input: $scope.request.localMoveRateCommissionInput3
				},
				{
					option: $scope.request.longDistanceMoveRateCommission1,
					input: $scope.request.longDistanceMoveRateCommissionInput1
				},
				{
					option: $scope.request.longDistanceMoveRateCommission2,
					input: $scope.request.longDistanceMoveRateCommissionInput2
				},
				{
					option: $scope.request.longDistanceMoveRateCommission3,
					input: $scope.request.longDistanceMoveRateCommissionInput3
				},
				{
					option: $scope.request.internationalRateCommission1,
					input: $scope.request.internationalRateCommissionInput1
				},
				{
					option: $scope.request.internationalRateCommission2,
					input: $scope.request.internationalRateCommissionInput2
				},
				{
					option: $scope.request.internationalRateCommission3,
					input: $scope.request.internationalRateCommissionInput3
				}
			];
			request.permissions = $scope.request.permissions;
			request.email_account = $scope.request.email_account || angular.copy(departmentConstants.emailAccount);
			request.signature = angular.copy($scope.request.signature);
			request.extraSignature = angular.copy($scope.request.extraSignature);
		} else {
			request.rateCommission = $scope.rateCommission;
			request.estimated_closing_price = $scope.request.estimatedClosingPrice;
			request.permissions = $scope.request.permissions;
			request.email_account = $scope.request.email_account || angular.copy(departmentConstants.emailAccount);
		}

		var requestForEditWorker = {
			data: {
				name: $scope.request.login,
				mail: $scope.request.email,
				status: Number($scope.request.employeeIsActive),
				pass: $scope.request.password,
				roles: [],
				metadata: {
					shared_google_calendars: $scope.request.originGoogleCalendars
				}
			},
			fields: {
				field_user_first_name: $scope.request.firstName,
				field_user_last_name: $scope.request.lastName,
				field_google_calendar_email: $scope.request.gmail
			},
			settings: {}
		};

		if (_.isEqual(workerPosition.toLocaleLowerCase(), 'customer service')) {
			workerPosition = 'sales';
			request.isCustomerService = true;
		} else {
			request.isCustomerService = false;
		}

		requestForEditWorker.settings = request;
		requestForEditWorker.data.roles.push(workerPosition);

		checkHomeEstimator($scope.request.homeEstimator, requestForEditWorker.data.roles);

		var sweetAlertTitle = editData ? "Update user info ?" : "Add User ?";

		SweetAlert.swal({
			title: sweetAlertTitle,
			text: "",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#64C564",
			confirmButtonText: "Confirm",
			closeOnConfirm: true
		}, sweetClick);

		function sweetClick(isConfirm) {
			if (isConfirm) {
				$scope.loader.busy = true;
				var promiseEditWorker = DepartmentServices.editWorker(requestForEditWorker, editData.uid);

				promiseEditWorker.then(function (data) {
					if (data.update) {

						DepartmentServices.saveEmailAccount(editData.uid, requestForEditWorker.settings.email_account);
						$rootScope.currentPosition = $scope.request.workerPosition;
						$rootScope.loaderBusy = true;
						$rootScope.workerByPosition = {};
						DepartmentServices.getWorkersByPosition($scope.request.workerPosition, $scope.request.employeeIsActive).then(function (data) {
							$rootScope.$broadcast('update.department.worker', data[Object.keys(data)[0]]);
							logger.success('Successfully saved');
							$scope.loader.busy = false;
							$rootScope.loaderBusy = false;
						});

						$rootScope.getWorkersCountsByPosition();
						$scope.cancel();

					} else {
						onBadResponce(data);
					}
				}, onBadResponce);

				function onBadResponce (response) {
					let errorMessage = _.get(response, 'data.response.error_message', "Error, can't connect to server");
					logger.error(errorMessage);
					if (response.status_code === 406) {
						$scope.userIsExist = true;
					}

					$scope.loader.busy = false;
					$rootScope.loaderBusy = false;
				}
			}
		}

	};

	$scope.deleteWorker = function () {

		// additional check for allow deleting
		if (!_.isEqual($scope.currentUser, 'roma4ke')) {
			return false;
		}

		SweetAlert.swal({
			title: 'Delete User?',
			text: "",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#e25d5d",
			confirmButtonText: "Confirm",
			closeOnConfirm: true
		}, function (isConfirm) {
			if (isConfirm) {
				$scope.loader.busy = true;

				var promiseDeleteWorker = DepartmentServices.deleteWorker(editData.uid);

				promiseDeleteWorker.then(function (data) {
					$rootScope.loaderBusy = true;
					$rootScope.workerByPosition = {};
					DepartmentServices.getWorkersByPosition($scope.request.workerPosition, $scope.request.employeeIsActive).then(function (data) {
						$rootScope.$broadcast('update.department.worker', data[Object.keys(data)[0]]);
						logger.success('Deleted Successfully');
						$scope.loader.busy = false;
						$rootScope.loaderBusy = false;
					});

					$rootScope.getWorkersCountsByPosition();
				}, function (reason) {
					$scope.loader.busy = false;
					$rootScope.loaderBusy = false;
				});

				$scope.cancel();
			}
		});
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.uploadPhotoModal = function (data, index) {
		var modalPhoto = $uibModal.open({
			template: require('../directives/uploadPhoto.html'),
			controller: UploadPhotoCtrl,
			backdrop: false,
			resolve: {
				editData: function () {
					return data;
				},
				currentEl: function () {
					return index;
				}
			},
			size: 'md'
		});

		modalPhoto.result.then(function (result) {
			$scope.imageUrl = result;
		});
	};

	UploadPhotoCtrl.$inject = ['$scope', '$uibModalInstance', 'DepartmentServices', 'logger'];

	function UploadPhotoCtrl($scope, $uibModalInstance, DepartmentServices, logger) {
		$scope.img_1Url = "content/img/manager_1.jpg";
		$scope.img_2Url = "content/img/manager_2.jpg";
		$scope.img_3Url = "content/img/manager_3.jpg";
		$scope.img_4Url = "content/img/manager_4.jpg";

		$scope.onImageAdded = function (file) {
			DepartmentServices.convertPhotoToBaseSixtyFour(file.file).then(function (result) {
				$scope.myCroppedImage = '';
				$scope.photo = result;
			})
		};

		$scope.uploadSelectedPhoto = function (photoUrl) {
			DepartmentServices.convertPhotoToBaseSixtyFour(photoUrl).then(function (result) {
				$scope.uploadPhoto(result);
			})
		};

		$scope.uploadPhoto = function (file) {
			if (file) {
				$scope.uploadedBusy = true;

				DepartmentServices.uploadUserPhoto(editData.uid, file).then(function (data) {
					$scope.uploadedBusy = false;
					$uibModalInstance.close(file);
				});
			} else {
				logger.error('please select a photo');
			}

		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}

	$scope.turnAllNotifications = () => {
		$scope.notificationTypes.forEach(notification => {
			if ($scope.notificationsGroup == notification.type) {
				notification.selected = $scope.selectAllNotifications[Number($scope.notificationsGroup)];
			}
		})
		$scope.addNotificationsToUser();
	}

	function switchAllNotification() {
		var allCount, selectedCount;
		for (var i = 0; i < 3; i++) {
			selectedCount = allCount = 0;
			$scope.notificationTypes.forEach(notification => {
				if (i == notification.type) {
					allCount++;
					if (notification.selected == true) {
						selectedCount++;
					}

				}
			})
			if (allCount === selectedCount) {
				$scope.selectAllNotifications[i] = true;
			} else {
				$scope.selectAllNotifications[i] = false;
			}
		}
	}

	$scope.pushAutoassignCheckBox = function (serviceIndex, check) {
		// TODO refactor through Object.keys()

		if (_.isEqual(serviceIndex, 0)) {
			if (_.isEqual($scope.request.autoassignment.services.length, 8)) {
				$scope.request.autoassignment.services = [];
			} else {
				$scope.request.autoassignment.services = angular.copy(departmentConstants.typeServices);
			}
		} else {
			var index = $scope.request.autoassignment.services.indexOf(serviceIndex);

			if (_.isEqual(index, -1)) {
				if (check) {
					$scope.request.autoassignment.services.push(serviceIndex);
				}
			} else {
				if (!check) {
					$scope.request.autoassignment.services.splice(index, 1);
				}
			}
		}

		checkAssigmentService(serviceIndex, check);
	};

	function checkAssigmentService(indexService, checked) {
		$scope.autoAssignment[0].checked = _.isEqual($scope.request.autoassignment.services.length, 8);

		if (_.isEqual(indexService, 0)) {
			_.each($scope.autoAssignment, function (service) {
				service.checked = checked;
			});
		} else {
			$scope.autoAssignment[indexService].checked = checked;
		}
	}

	function checkHomeEstimator(isEstimator, roles) {
		if (isEstimator) {
			roles.push('home-estimator');
		} else {
			let homeEstimatorIndex = _.findIndex(roles, 'home-estimator');
			if (homeEstimatorIndex > -1) {
				roles.splice(homeEstimatorIndex, 1);
			}
		}
	}
}
