'use strict';

angular
	.module('move.requests')
	.controller('CreateUserController', CreateUserController);

CreateUserController.$inject = ['$scope', '$uibModalInstance', '$rootScope', 'DepartmentServices', 'logger', 'datacontext', 'SweetAlert', 'departmentConstants', 'apiService', 'moveBoardApi'];

function CreateUserController($scope, $uibModalInstance, $rootScope, DepartmentServices, logger, datacontext, SweetAlert, departmentConstants, apiService, moveBoardApi) {
	var optionsList = angular.copy(departmentConstants.optionListSettings);
	var HOURLY_RATE_FORMAN_AS_HELPER = "Hourly Rate (as helper)";
	$scope.uploader = {};
	$scope.images = [];
	$scope.btnName = 'Create';
	$scope.loader = {};
	$scope.request = {};
	$scope.request.email_account = angular.copy(departmentConstants.emailAccount);
	$scope.request.signature = angular.copy(departmentConstants.signatureSettings);
	$scope.request.extraSignature = {};
	$scope.request.permissions = departmentConstants.defaultPermissions;
	$scope.busyTestEmail = true;
	$scope.notificationsGroup = 0;
	$scope.selectAllNotifications = [false, false, false];
	$scope.userIsExist = false;
	$scope.checkRoles = DepartmentServices.checkRoles;

	$scope.isDisableTestButton = (form) => {
		return form.$invalid
			|| !$scope.request.email_account.outgoing.active
			|| $scope.isInvalidOutgoingServer(form)
			|| $scope.isInvalidOutgoingPort(form)
			|| $scope.isInvalidOutgoingEmail(form)
			|| $scope.isInvalidOutgoingPassword(form)
			|| !$scope.busyTestEmail;
	};

	$scope.chooseConnectionType = function (type) {
		if (type == 'tls') {
			$scope.request.email_account.outgoing.ssl = false;
		} else {
			$scope.request.email_account.outgoing.tls = false;
		}
	};

	$scope.isInvalidOutgoingServer = (form) => {
		return form.outgoingServer.$touched && form.outgoingServer.$invalid;
	};

	$scope.isInvalidOutgoingPort = (form) => {
		return form.outgoingPort.$touched && form.outgoingPort.$invalid;
	};

	$scope.isInvalidOutgoingEmail = (form) => {
		return form.outgoingEmail.$touched && form.outgoingEmail.$invalid;
	};

	$scope.isInvalidOutgoingPassword = (form) => {
		return form.outgoingPassword.$touched && form.outgoingPassword.$invalid;
	};

	$scope.request.creationDate = moment().format('YYYY-MM-DD');
	$scope.branches = departmentConstants.branches;
	$scope.departments = departmentConstants.departments;
	$scope.positions = departmentConstants.typeWorkers;
	$scope.rateCommission = [];
	$scope.request.workerPosition = $rootScope.currentPosition || $scope.positions[0];
	$scope.request.phoneType1 = 'Home';
	$scope.request.phoneType2 = 'Cellular';
	$scope.rateAndCommissions = [optionsList];
	$scope.rateAndCommissionMS = departmentConstants.rateAndCommissionMS;
	$scope.request.employeeIsActive = true;
	$scope.request.estimatedClosingPrice = 'Closing Price';
	$scope.request.homeEstimator = false;
	$scope.request.estimatedClosingPriceMS = 'Closing Price';
	$scope.mailDomain = (angular.fromJson((datacontext.getFieldData()).basicsettings))['mail_domain'];
	$scope.autoAssignment = angular.copy(departmentConstants.autoAssignment);
	$scope.request.autoassignment = angular.copy(departmentConstants.autoAssignmentSettings);
	$scope.request.excluded_additional_services = [];
	$scope.request.accountDetails = angular.copy(departmentConstants.accountDetails);

	$scope.commissionsFroSales = commissionsFroSales;
	$scope.saveSignature = saveSignature;

	function commissionsFroSales(arr) {
		if ($scope.request.workerPosition == 'Sales') {
			return arr.slice(0, 1);
		} else {
			return arr;
		}
	}

	$scope.imageUrl = "content/img/user_pic.jpg";
	$scope.createUserRun = true;

	$scope.$watch('request.workerPosition', function (newPosition, oldPosition) {
		if (newPosition.toString() == 'Foreman') {
			optionsList.optionList.push({
				name: HOURLY_RATE_FORMAN_AS_HELPER, optionDisabled: false
			});
		} else {
			var index = _.findIndex(optionsList.optionList, {name: HOURLY_RATE_FORMAN_AS_HELPER});

			if (index >= 0) {
				optionsList.optionList.splice(index, 1);
			}

			index = _.findIndex($scope.rateCommission, {option: HOURLY_RATE_FORMAN_AS_HELPER});

			if (index >= 0) {
				$scope.rateCommission.splice(index, 1);
			}
		}
	});

	$scope.excludedAdditionalServices = function () {
		DepartmentServices.excludedAdditionalServices($scope.request.excluded_additional_services).then(function (data) {
			$scope.request.excluded_additional_services = data.data;
		});
	};

	$scope.deleteRow = function ($index) {
		angular.forEach($scope.rateAndCommissions, function (rateAndCommissions) {
			angular.forEach(rateAndCommissions.optionList, function (item) {
				if ($scope.rateCommission[$index].option === item.name) {
					if ($scope.rateCommission[$index].option == 'Commission from total') {
						$scope.request.excluded_additional_services = [];
					}
					item.optionDisabled = false;
				}
			});
		});

		$scope.rateAndCommissions.splice($index, 1);
		$scope.rateCommission.splice($index, 1);
	};

	$scope.disableCommissions = function () {
		_.forEach($scope.rateAndCommissions, function (commissions) {
			_.forEach(commissions.optionList, function (option) {
				option.optionDisabled = false;
			});
		});
		_.forEach($scope.rateCommission, function (item) {
			_.forEach($scope.rateAndCommissions, function (commissions) {
				_.forEach(commissions.optionList, function (option) {
					if (item.option == option.name) {
						option.optionDisabled = true;
					}
				});
			});
		});
	};

	$scope.addRow = function () {
		if ($scope.rateAndCommissions.length === 0) {
			$scope.rateAndCommissions.push(optionsList);

			return;
		}

		var keepGoing = true;

		angular.forEach($scope.rateAndCommissions, function (rateAndCommissions, i) {
			angular.forEach(rateAndCommissions.optionList, function (item, j) {
				if (keepGoing) {
					if (item.optionDisabled === false && item.name != '') {
						$scope.rateAndCommissions.push(optionsList);
						$scope.rateCommission.push({option: item.name, input: 0});
						item.optionDisabled = true;
						$scope.rateAndCommissions[i].optionList[j].optionDisabled = true;
						keepGoing = false;
					}
				}
			});
		});
	};

	$scope.checkLogin = checkLogin;

	function checkLogin(login) {

		apiService.postData(moveBoardApi.department.checkLogin, {login: login})
			.then(
				res => {
					$scope.userIsExist = _.get(res, 'data.data');
				},
				rej => {
					logger.error('Can not check login');
				}
			);
	}

	$scope.create = function (form) {
		let isGenerateEmail = _.isUndefined($scope.request.email);

		let validationError = 'Some fields are empty or filled incorrectly:';
		let allowCreate = true;

		if (!$scope.request.firstName) {
			validationError += "\n First Name";
			allowCreate = false;
		}
		if (!$scope.request.lastName) {
			validationError += "\n Last Name";
			allowCreate = false;
		}
		if (!allowCreate) {
			toastr.error(validationError);
			return;
		}

		if ($scope.request.workerPosition == 'Helper' || $scope.request.workerPosition == 'Driver') {
			var firstName = $scope.request.firstName.replace(/[,\s]+|[,\s]+/g, '');
			var lastName = $scope.request.lastName.replace(/[,\s]+|[,\s]+/g, '');

			$scope.request.email = DepartmentServices.generateEmail(firstName, lastName);
			isGenerateEmail = false;
			$scope.request.login = $scope.request.email;
			$scope.request.password = $scope.request.login;
		}

		if (isGenerateEmail) {
			$scope.request.email = DepartmentServices.generateEmail($scope.request.firstName, $scope.request.lastName);
		}

		var errorUnique = $scope.request.workerPosition != 'Helper'
			&& $scope.request.workerPosition != 'Driver'
			&& form.email2
			&& form.email2.$error
			&& form.email2.$error.unique;

		if (!$scope.request.login) {
			validationError += "\n Login";
			allowCreate = false;
		}
		if (!$scope.request.email) {
			validationError += "\n Email";
			allowCreate = false;
		}
		if (!$scope.request.phone1) {
			validationError += "\n Phone 1";
			allowCreate = false;
		}
		if (!$scope.request.password) {
			validationError += "\n Password";
			allowCreate = false;
		}
		if (!allowCreate) {
			toastr.error(validationError);
			return;
		}

		var workerPosition = $scope.request.workerPosition ? $scope.request.workerPosition.toLowerCase() : '';
		var request = {
			creation_date: $scope.request.creationDate,
			first_name: $scope.request.firstName,
			last_name: $scope.request.lastName,
			date_started: $scope.request.dateStarted,
			date_leaving: $scope.request.dateLeaving,
			driver_license: $scope.request.driverLicense || '',
			ssn: $scope.request.ssn || '',
			address: $scope.request.address,
			city: $scope.request.city,
			date_of_birth: $scope.request.dateOfBirth,
			branch: $scope.request.branch,
			department: $scope.request.department,
			roles: [],
			password: $scope.request.password,
			contact_name: $scope.request.contactName,
			relationship: $scope.request.relationship || '',
			message: $scope.request.message || '',
			employee_is_active: !!$scope.request.employeeIsActive ? 1 : 0,
			email_account: angular.copy(departmentConstants.emailAccount),
			email1: $scope.request.secondaryEmail,
			phoneType1: $scope.request.phoneType1,
			phoneType2: $scope.request.phoneType2,
			field_primary_phone: $scope.request.phone1,
			field_user_additional_phone: $scope.request.phone2,
			accountDetails: $scope.request.accountDetails,
			homeEstimator: $scope.request.homeEstimator,
			exclude_fuel: $scope.request.excludeFuelMS,
			exclude_valuation: $scope.request.excludeValuationMS,
			exclude_packing: $scope.request.excludePackingMS,
			exclude_additional: $scope.request.excludeAdditionalMS,
			excluded_additional_services: $scope.request.excludeAdditionalMS ? $scope.request.excluded_additional_services : []
		};
		if (_.isObject($scope.request.worker)) {
			request.locality = $scope.request.worker.locality;
			request.administrative_area = $scope.request.worker.administrative_area;
		}
		if ($scope.request.zip) {
			request.zip = $scope.request.zip;
		}

		request.roles.push(workerPosition);

		if ($scope.request.homeEstimator) {
			request.roles.push('home-estimator');
		}

		if ($scope.request.workerPosition == 'Manager'
			|| $scope.request.workerPosition == 'Sales'
			|| $scope.request.workerPosition == 'Customer service'
			|| $scope.request.workerPosition == 'Administrator') {

			request.autoassignment = $scope.request.autoassignment;
		}

		// checking if position manager or sales
		if ($scope.request.workerPosition == 'Manager' || $scope.request.workerPosition == 'Sales' || $scope.request.workerPosition == 'Customer service') {
			request.estimated_closing_price = $scope.request.estimatedClosingPriceMS;
			request.rateCommission = [
				{
					option: $scope.request.localMoveRateCommission1,
					input: $scope.request.localMoveRateCommissionInput1
				},
				{
					option: $scope.request.localMoveRateCommission2,
					input: $scope.request.localMoveRateCommissionInput2
				},
				{
					option: $scope.request.localMoveRateCommission3,
					input: $scope.request.localMoveRateCommissionInput3
				},
				{
					option: $scope.request.longDistanceMoveRateCommission1,
					input: $scope.request.longDistanceMoveRateCommissionInput1
				},
				{
					option: $scope.request.longDistanceMoveRateCommission2,
					input: $scope.request.longDistanceMoveRateCommissionInput2
				},
				{
					option: $scope.request.longDistanceMoveRateCommission3,
					input: $scope.request.longDistanceMoveRateCommissionInput3
				},
				{
					option: $scope.request.internationalRateCommission1,
					input: $scope.request.internationalRateCommissionInput1
				},
				{
					option: $scope.request.internationalRateCommission2,
					input: $scope.request.internationalRateCommissionInput2
				},
				{
					option: $scope.request.internationalRateCommission3,
					input: $scope.request.internationalRateCommissionInput3
				}
			];
			request.permissions = $scope.request.permissions;
			request.email_account = $scope.request.email_account;
			request.signature = $scope.request.signature;
			request.extraSignature = $scope.request.extraSignature;
		} else {
			request.rateCommission = $scope.rateCommission;
			request.estimated_closing_price = $scope.request.estimatedClosingPrice;
			request.email_account = $scope.request.email_account;
		}

		var requestForCreateWorker = {
			account: {
				name: $scope.request.login,
				mail: $scope.request.email,
				pass: $scope.request.password,
				status: Number($scope.request.employeeIsActive),
				fields: {
					field_user_first_name: $scope.request.firstName,
					field_user_last_name: $scope.request.lastName,
					field_primary_phone: $scope.request.phone1,
					field_user_additional_phone: $scope.request.phone2
				},
				roles: []
			}
		};

		if (_.isEqual(workerPosition.toLocaleLowerCase(), 'customer service')) {
			workerPosition = 'sales';
			request.isCustomerService = true;
		}

		requestForCreateWorker.account.settings = request;
		requestForCreateWorker.account.notifications = $scope.notificationTypes;
		requestForCreateWorker.account.roles.push(workerPosition);

		$scope.loader.busy = true;
		var promise = DepartmentServices.createWorker(requestForCreateWorker);

		function updateDepartment() {
			DepartmentServices.getWorkersByPosition($scope.request.workerPosition, $scope.request.employeeIsActive).then(function (data) {
				$rootScope.$broadcast('update.department.worker', data[Object.keys(data)[0]]);
				$scope.loader.busy = false;
				$rootScope.loaderBusy = false;
			});

			$rootScope.getWorkersCountsByPosition();
		}

		promise.then(data => {
			if (data.data) {
				if (_.isUndefined(data.data.uid)) {

					onIndentifiedError(data.data)

				} else {

					DepartmentServices.saveEmailAccount(data.data.uid, requestForCreateWorker.account.settings.email_account);
					$rootScope.loaderBusy = true;
					$rootScope.workerByPosition = {};
					$rootScope.currentPosition = $scope.request.workerPosition;

					enableNotifications(data.data.uid);

					updateDepartment();

					logger.success('Successfully created');
					$scope.cancel();
				}
			} else {
				onUnknownError();
			}
		}, rej => {
			onUnknownError();
		}).finally(() => {
			$scope.loader.busy = false;
		});

		function onIndentifiedError(data) {
			let errorMessage = "Can't connect to server";

			if (data.status_message) {
				errorMessage = data.status_message;
			}

			logger.error(errorMessage);

			if (data.status_code === 406) {
				$scope.userIsExist = true;
			}
		}

		function onUnknownError() {
			logger.error('User has not created', null, 'Error');
		}
	};

	$scope.testEmailConnection = function () {
		$scope.busyTestEmail = false;
		DepartmentServices.testEmailConnection($scope.request.email_account)
			.then(function (data) {
					var msg = _.head(data.data);
					if (msg == 1) {
						toastr.success('Connection success');
					} else {
						SweetAlert.swal(msg, '', "error");
					}
				},
				function (err) {
					SweetAlert.swal("Error", err, "error");
				})
			.finally(function () {
				$scope.busyTestEmail = true;
			});
	};

	$scope.hideOption = function (item) {
		return item.name != '' || item.optionDisabled;
	};
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.turnAllNotifications = () => {
		$scope.notificationTypes.forEach(notification => {
			if ($scope.notificationsGroup == notification.type) {
				notification.selected = $scope.selectAllNotifications[Number($scope.notificationsGroup)];
			}
		})
	};

	$scope.pushAutoassignCheckBox = function (serviceIndex, check) {
		if (_.isEqual(serviceIndex, 0)) {
			if (_.isEqual($scope.request.autoassignment.services.length, 8)) {
				$scope.request.autoassignment.services = [];
			} else {
				$scope.request.autoassignment.services = angular.copy(departmentConstants.typeServices);
			}
		} else {
			var index = $scope.request.autoassignment.services.indexOf(serviceIndex);

			if (_.isEqual(index, -1)) {
				if (check) {
					$scope.request.autoassignment.services.push(serviceIndex);
				}
			} else {
				if (!check) {
					$scope.request.autoassignment.services.splice(index, 1);
				}
			}
		}

		checkAssigmentService(serviceIndex, check);
	};

	function checkAssigmentService(indexService, checked) {
		$scope.autoAssignment[0].checked = _.isEqual($scope.request.autoassignment.services.length, 8);

		if (_.isEqual(indexService, 0)) {
			_.each($scope.autoAssignment, function (service) {
				service.checked = checked;
			});
		} else {
			$scope.autoAssignment[indexService].checked = checked;
		}
	}

	function enableNotifications(userID) {
		apiService.postData(moveBoardApi.notifications.allowedNotificationTypes, {uid: userID}).then(data => {
			for (var i = 0; i < $scope.notificationTypes.length; i++) {
				for (var j = 0; j < data.data.length; j++) {
					if ($scope.notificationTypes[i].ntid == data.data[j].ntid) {
						$scope.notificationTypes[i].selected = true;
					}
				}

			}
		});
	}

	apiService.postData(moveBoardApi.notifications.getNotifications, {}).then(data => {
		$scope.notificationTypes = data.data;
	});

	function saveSignature() {

	}
}
