describe('Department: Autoassign for managers', () => {
	let SweetAlert, $controller, $rootScope, DepartmentInstanceController, $scope, modalInstance, editData, testHelper, $compile, element, moveBoardApi, $httpBackend, $timeout;
	beforeEach(inject((_$controller_, _$rootScope_, _testHelperService_, _$compile_, _moveBoardApi_, _$httpBackend_, _$timeout_, _SweetAlert_) => {
		$timeout = _$timeout_;
		$httpBackend = _$httpBackend_;
		$compile = _$compile_;
		$controller = _$controller_;
		SweetAlert = _SweetAlert_;
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		moveBoardApi = _moveBoardApi_;
		testHelper = _testHelperService_;
		editData = testHelper.loadJsonFile('department-update-user.mock');
		let getcurrent = testHelper.loadJsonFile('getcurrent-user.mock');
		_$rootScope_.currentUser = {userId: getcurrent};

		$rootScope.getWorkersCountsByPosition = () => {};
		$rootScope.karmaTestingWorkingNow = true;

		$httpBackend.whenGET(/.*/).passThrough();
		$httpBackend.whenPOST(/.*/).passThrough();

		element = angular.element('<edit-worker></edit-worker>');
		$compile(element)($scope);
		$scope.$apply();

		modalInstance = {
			close: jasmine.createSpy('modalInstance.close'),
			dismiss: jasmine.createSpy('modalInstance.dismiss'),
			result: {
				then: jasmine.createSpy('modalInstance.result.then')
			}
		};

		DepartmentInstanceController = $controller('DepartmentInstanceController', {
			$scope: $scope,
			$uibModalInstance: modalInstance,
			editData: editData,
			currentEl: 0
		});
	}));

	describe('Controller is defined', () => {
		it('DepartmentInstanceController should be defened', () => {
			expect(DepartmentInstanceController).toBeDefined();
		});
	});

	describe('Assign manager for all', () => {
		it('Manager should be assigned for all checkboxes', () => {
			spyOn(SweetAlert, 'swal').and.callFake((params, callback) => {callback(true);});
			
			$httpBackend.expectPUT(testHelper.makeRequest(moveBoardApi.googleCalendarIntegration.saveUserData + '6304'))
				.respond(200, {
					uid: '6304',
					update: true
				});
			$scope.create();
			$httpBackend.flush();
		});
	});

	describe('Auto assign', () => {
		it('Enable "all" - every item should be 1:', () => {
			$scope.pushAutoassignCheckBox(0, 1);
			$scope.autoAssignment.forEach((item) => {
				expect(item.checked).toBe(1);
			});
		});

		it('Disable "all" - every item should be 0:', () => {
			$scope.pushAutoassignCheckBox(0, 0);
			$scope.autoAssignment.forEach((item) => {
				expect(item.checked).toBe(0);
			});
		});
	})
});
