(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('settings.department', {
                        url: '/department',
	                    template: require('./department.html'),
                        title: 'Department'

                    });
            }
        ]
    );
})();
