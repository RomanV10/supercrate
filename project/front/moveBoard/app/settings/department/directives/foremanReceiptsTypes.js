(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('foremanReceiptsTypes', foremanReceiptsTypes);

    foremanReceiptsTypes.$inject = ['datacontext', 'SettingServices', 'common'];

    function foremanReceiptsTypes(datacontext, SettingServices, common) {
        return {
            controller: ForemanReceiptsTypesController,
            controllerAs: 'vm',
            scope: {},
	        template: require('./foremanReceiptsTypes.html'),
            restrict: 'E'
        };

        function ForemanReceiptsTypesController($scope) {
            var vm = this;
            let fieldData = datacontext.getFieldData(),
                basicsettings = angular.fromJson(fieldData.basicsettings);

            vm.foremanReceiptsTypes = angular.copy(basicsettings.foremanReceiptsTypes);

            vm.addNewItem = addNewItem;
            vm.removeItem = removeItem;
            vm.disableNewItem = disableNewItem;
            vm.saveItems = saveItems;

            function disableNewItem(){
                return _.last(vm.foremanReceiptsTypes).value == '';
            }
            function addNewItem(){
                vm.foremanReceiptsTypes.push({value:''});
            }
            function removeItem(index){
                vm.foremanReceiptsTypes.splice(index, 1);
            }
            function saveItems(){
                vm.saving = true;
                basicsettings.foremanReceiptsTypes = vm.foremanReceiptsTypes;
                var _setting_name = 'basicsettings';
                var promise = SettingServices.saveSettings(basicsettings, _setting_name);
                promise.then(function () {
                    vm.saving = false;
                    common.$timeout(function () {
                        vm.saved = false;
                    }, 1000);
                    vm.saved = true;
                });
            }

        }
    }

})();
