describe('Directive: Department Settings Arrivy Integration', function () {
	let $httpBackend,
		moveBoardApi,
		testHelper,
		elementScope,
		$rootScope,
		$compile,
		$scope,
		element;

	function compileElement(elementHtml) {
		let elem = angular.element(elementHtml);
		element = $compile(elem)($scope);
		elementScope = element.isolateScope();

	}

	beforeEach(inject(function (_$compile_, _$rootScope_, _$httpBackend_, _testHelperService_, _moveBoardApi_) {
		testHelper = _testHelperService_;
		$rootScope = _$rootScope_;
		$scope = _$rootScope_.$new();
		$compile = _$compile_;
		$httpBackend = _$httpBackend_;
		moveBoardApi = _moveBoardApi_;

		$scope.foreman =  testHelper.loadJsonFile('department/foreman-out-arrivy.mock');
		compileElement(`<arrivy-integration foreman="foreman"></arrivy-integration>`);
	}));

	describe('When init', () => {
		it('should not render empty html', function () {
			expect(element.html()).not.toEqual('');
		});
	});

	describe('Foreman w/o arrivy', () => {
		it ("should create in arrivy", () => {
			let res = {
				arrivy_id: '0123456789'
			};
			let arrivyForeman = $scope.foreman;
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.arrivy.createForeman),arrivyForeman)
				.respond(200, res);
			elementScope.sendForemanToArrivy(arrivyForeman);
			$httpBackend.flush();
			expect(elementScope.arrivyForemanExist).toBeTruthy();
		});
	});

	describe('Foreman with arrivy', () => {
		beforeEach(() => {
			$scope.foreman =  testHelper.loadJsonFile('department/foreman-in-arrivy.mock');
			compileElement(`<arrivy-integration foreman="foreman"></arrivy-integration>`);
		});
		it ("should disconnect", () => {
			let res = {
				disconnect_status: true
			};
			let data = {
				foreman_id: "4300"
			};
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.arrivy.disconnectForeman),data)
				.respond(200, res);
			elementScope.removeForemanFromArrivy(data);
			$httpBackend.flush();
			expect(elementScope.arrivyForemanExist).not.toBeTruthy();
		});
	});
});
