'use strict';
import './arrivy-integration.styl';

angular
    .module('move.requests')
    .directive('arrivyIntegration', arrivyIntegration);

arrivyIntegration.$inject = ['apiService', 'moveBoardApi', 'SweetAlert'];

function arrivyIntegration(apiService, moveBoardApi, SweetAlert) {
    return {
        scope: {
            foreman: '='
        },
        template: require('./arrivy-integration.pug'),
        restrict: 'E',
        link: linkFunction
    };

    function linkFunction($scope) {
        $scope.arrivyForemanExist = !!$scope.foreman.foremanArrivyId;
        $scope.arrivyIntegration = {
            "create_new": false,
            "arrivy_id": $scope.foreman.foremanArrivyId || '',
            "arrivy_email": "",
            "arrivy_pass": "",
            "id": $scope.foreman.userId,
            "first_name": $scope.foreman.firstName,
            "last_name": $scope.foreman.lastName,
            "phone": $scope.foreman.phone1
        };

        $scope.sendForemanToArrivy = sendForemanToArrivy;
        $scope.removeForemanFromArrivy = removeForemanFromArrivy;

        function sendForemanToArrivy(foremanArrivy) {
            let message = ``;
            apiService.postData(moveBoardApi.arrivy.createForeman, foremanArrivy)
				.then(res => {
					if(res.data.arrivy_id) {
						$scope.arrivyForemanExist = true;
						foremanArrivy.create_new = false;
						foremanArrivy.arrivy_id = res.data.arrivy_id;
						message = `Foreman Arrivy ID ${res.data.arrivy_id}`;
						SweetAlert.swal("Success", message, 'success');
					} else {
						message = res.data.message;
						SweetAlert.swal("Error", message, 'error');
					}
            	});
        }

        function removeForemanFromArrivy(){
            apiService.postData(moveBoardApi.arrivy.disconnectForeman, {foreman_id: $scope.foreman.userId})
				.then(res => {
					if(res.data.disconnect_status){
						SweetAlert.swal("Success", res.data.message, 'success');
						$scope.arrivyForemanExist = false;
					} else {
						SweetAlert.swal("Error", res.data.message, 'error');
					}
            	});
        }
    }
}
