(function () {
	'use strict';

	angular
		.module('move.requests')
		.directive('optionsDisabled', function ($parse) {
			return {
				priority: 0,
				require: 'ngModel',
				link: function (scope, iElement, iAttrs) {
					function addCustomAttr(attr, element, data, fnDisableIfTrue) {
						$("option", element).each(function (i, e) {
							var locals = {};
							locals[attr] = data[i];
							$(e).attr('disabled', fnDisableIfTrue(scope, locals));
						});
					}

					var expElements = iAttrs['optionsDisabled'].match(/(.+)\s+for\s+(.+)\s+in\s+(.+)/);
					var attrToWatch = expElements[3];
					var fnDisableIfTrue = $parse(expElements[1]);

					function disableSelectedOption() {
						angular.forEach(scope.rateAndCommissions, function (rateAndCommissions) {
							angular.forEach(rateAndCommissions.optionList, function (item) {
								item.optionDisabled = false;
								angular.forEach(scope.rateCommission, function (rateCommission) {
									if (rateCommission.option === item.name) {
										item.optionDisabled = true;
									}
								});
							});
						});
					}

					function refresh() {
						disableSelectedOption();
						expElements = iAttrs['optionsDisabled'].match(/(.+)\s+for\s+(.+)\s+in\s+(.+)/);
						attrToWatch = expElements[3];
						fnDisableIfTrue = $parse(expElements[1]);
						addCustomAttr(expElements[2], iElement, attrToWatch, fnDisableIfTrue);
					}

					scope.$watch(attrToWatch, function (newValue, oldValue) {
						disableSelectedOption();
						addCustomAttr(expElements[2], iElement, newValue, fnDisableIfTrue);
					}, true);

					scope.$watch(iAttrs.ngModel, function (newValue) {
						refresh();
					}, true);
				}
			};
		})
		.directive('createUser', createUser);

	createUser.$inject = ['$uibModal'];

	function createUser($uibModal) {
		return {
			controller: CreateUserFormController,
			controllerAs: 'vm',
			scope: {},
			template: '<div ng-click="vm.openCreateUserModal()" class="btn btn-primary btn-block">Create User</div>',
			restrict: 'E'
		};

		function CreateUserFormController() {
			var vm = this;

			vm.openCreateUserModal = function () {
				var modalInstance = $uibModal.open({
					template: require('./createUser.template.html'),
					controller: 'CreateUserController',
					backdrop: false,
					size: 'md'
				});

				modalInstance.result.then(function (newRequests) {

				});
			};
		}
	}

})();
