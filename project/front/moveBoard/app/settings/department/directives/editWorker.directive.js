(function () {
	'use strict';

	angular
		.module('app.department')
		.directive('editWorker', editWorker);

	editWorker.$inject = ['$uibModal', 'DepartmentServices', 'datacontext'];

	function editWorker($uibModal, DepartmentServices, datacontext) {
		return {
			link: linkFn,
			restrict: "A"
		};

		function linkFn(scope, element, attrs) {

			function clone(obj) {
				if (null == obj || "object" != typeof obj) {
					return obj;
				}
				var copy = obj.constructor();

				for (var attr in obj) {
					if (obj.hasOwnProperty(attr)) {
						copy[attr] = obj[attr];
					}
				}

				return copy;
			}

			scope.edit = function (data, index) {
				var modalInstance = $uibModal.open({
					template: require('./createUser.template.html'),
					controller: 'DepartmentInstanceController',
					backdrop: false,
					resolve: {
						editData: function () {
							return data;
						},
						currentEl: function () {
							return index;
						}
					},
					size: 'md'
				});

				modalInstance.result.then(function ($scope) {
				});
			};
		}
	}

})();
