'use strict';
import './google-calendar-integration.styl'
import defaultSaggestedCalendar from './google-calendar-suggested-calendar.json'

angular
	.module('move.requests')
	.directive('erDepartmentGoogleCalendarIntegration', erDepartmentGoogleCalendarIntegration);

erDepartmentGoogleCalendarIntegration.$inject = ['apiService', 'moveBoardApi'];

function erDepartmentGoogleCalendarIntegration(apiService, moveBoardApi) {
	return {
		scope: {
			uid: '=',
			gmail: '=',
			busy: '=',
			originGoogleCalendars: '=',
			userRole: '='
		},
		template: require('./google-calendar-integration.pug'),
		restrict: 'E',
		link: googleCalendarIntegrationLink
	};

	function googleCalendarIntegrationLink($scope, element, attr) {
		makeSharedCalendars();
		$scope.suggestedGoogleCalendars = defaultSaggestedCalendar.suggested;
		$scope.disableForm = true;

		if ($scope.uid) {
			getSharedCalendarByUser();
		}

		getGlobalGoogleCalendarSettings();

		function getSharedCalendarByUser() {
			let data = {"uid": $scope.uid};
			apiService
				.postData(moveBoardApi.googleCalendarIntegration.getSharedCalendarByUser, data)
				.then(onSuccessGetCalendarsByUser, onError);
		}

		function onSuccessGetCalendarsByUser(response) {
			$scope.busy = false;
			$scope.originGoogleCalendars = response.data.result.shared_google_calendars;
			makeSharedCalendars();
		}

		function onError() {
			$scope.busy = false;
		}

		function getGlobalGoogleCalendarSettings() {
			apiService
				.postData(moveBoardApi.googleCalendarIntegration.getGlobalStatus)
				.then(onGetGoogleCalendarStatus);
		}

		function onGetGoogleCalendarStatus(response) {
			$scope.disableForm = !response.data.result.is_enabled;
		}

		function makeSharedCalendars() {
			$scope.sharedCalendars = [];

			angular.forEach($scope.originGoogleCalendars, function (value, key) {
				let calendarItem = _.find(defaultSaggestedCalendar.suggested, {key: value});

				$scope.sharedCalendars.push(calendarItem);
			});
		}

		$scope.$watchCollection('sharedCalendars', reinitOriginGoogleCalendars);

		function reinitOriginGoogleCalendars() {
			$scope.originGoogleCalendars = [];

			angular.forEach($scope.sharedCalendars, function (value, key) {
				$scope.originGoogleCalendars.push(value.key);
			});

			makeActionWithMultipleSelect();
		}

		function makeActionWithMultipleSelect() {
			hideIfSelectAllCalendars();
			showIfSelectNotAllCalendars();
		}

		function hideIfSelectAllCalendars() {
			let isFullCalendarArray = $scope.originGoogleCalendars.length == 5;

			if (isFullCalendarArray) {
				let li = element.find('.list-inline li:last');
				li.css({'display': 'none'}).hide();
			}
		}

		function showIfSelectNotAllCalendars() {
			let isNotFullCalendarArray = $scope.originGoogleCalendars.length != 5;
			
			if (isNotFullCalendarArray) {
				let li = element.find('.list-inline li:last');
				li.css({'display': 'inline-block;'}).show();
			}
		}
	}
}
