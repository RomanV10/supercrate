describe('Department google integration.', function () {
	let $compile;
	let	$rootScope;
	let $httpBackend;
	let moveBoardApi;
	let $scope;
	let testHelper;
	let element;
	let settings;
	let scope;

	function makeElementWithAttributes() {
		$scope.uid = settings.uid;
		$scope.userCalendars = ["pending_calendar"];
		$scope.busy = true;
		$scope.gmail = settings.saveUserData.fields.field_google_calendar_email;
		$scope.userRole = 'foreman';

		element = angular.element(`<er-department-google-calendar-integration uid="uid" origin-google-calendars="userCalendars" busy="busy" gmail="gmail" user-role="userRole"></er-department-google-calendar-integration>`);

		$compile(element)($scope);
		scope = element.isolateScope();
	}

	function makeElementWithoutAttributes() {
		$scope.busy = true;
		element = angular.element(`<er-department-google-calendar-integration busy="busy"></er-department-google-calendar-integration>`);
		$compile(element)($scope);
		scope = element.isolateScope();
	}

	beforeEach(inject(function(_$httpBackend_, _$compile_, _$rootScope_, _moveBoardApi_, _testHelperService_) {
		$httpBackend = _$httpBackend_;
		$compile = _$compile_;
		$rootScope = _$rootScope_;
		moveBoardApi = _moveBoardApi_;
		testHelper = _testHelperService_;
		settings = testHelper.loadJsonFile('google-calendar.mock');
		$scope = $rootScope.$new();

		let getCalendarByUser = testHelper.makeRequest(moveBoardApi.googleCalendarIntegration.getSharedCalendarByUser);
		$httpBackend.whenPOST(getCalendarByUser, {"uid": settings.uid}).respond(200, settings.getUserCalendar);
		$httpBackend.expectPOST(getCalendarByUser, {"uid": settings.uid}).respond(200, settings.getUserCalendar);

		let getClobalStatus = testHelper.makeRequest(moveBoardApi.googleCalendarIntegration.getGlobalStatus);
		$httpBackend.whenPOST(getClobalStatus).respond(200, {"result": {"is_enabled": false}});
		$httpBackend.expectPOST(getClobalStatus).respond(200, {"result": {"is_enabled": false}});
	}));

	it('Uid should be defined', function () {
		makeElementWithAttributes();
		expect(scope.uid).toBeDefined();
		expect(scope.gmail).toBeDefined();
		expect(scope.busy).toBeDefined();
		expect(scope.originGoogleCalendars).toBeDefined();
	});

	it("Should get calendars by user when user exists", function () {
		makeElementWithAttributes();

		$httpBackend.flush();

		expect(scope.busy).toBeFalsy();
		expect(scope.originGoogleCalendars).toEqual(settings.getUserCalendar.result.shared_google_calendars);
	});

	it("Should parse user calendars when init directive", function () {
		makeElementWithAttributes();
		let expectedCalendarName = "Pending Calendar";

		expect(scope.sharedCalendars[0].name).toEqual(expectedCalendarName);
	});

	it("Should change origin google calendars be empty if change sharedCalendars to empty", function () {
		makeElementWithAttributes();
		scope.sharedCalendars = [];

		scope.$digest();

		let expectedResult = [];
		expect(scope.originGoogleCalendars).toEqual(expectedResult);
	});

	it('Should change origin google calendars if change sharedCalendars', function () {
		makeElementWithAttributes();
		scope.sharedCalendars = [
			{
				"key": "not_confirmed_calendar",
				"name": "Not Confirmed Calendar"
			}
		];

		scope.$digest();

		let expectedResult = ["not_confirmed_calendar"];
		expect(scope.originGoogleCalendars).toEqual(expectedResult);
	});

	it('Should be enable form when global settings is enable', function () {
		makeElementWithAttributes();

		$httpBackend.flush();

		expect(scope.disableForm).toEqual(true);
	})
});
