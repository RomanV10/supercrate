'use strict';

angular
	.module('move.requests')
	.directive('foremanPhoto', foremanPhoto);

function foremanPhoto() {
	return {
		template: require('./foreman-photo.html'),
		restrict: 'E'
	};
}
