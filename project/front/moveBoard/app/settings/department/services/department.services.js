'use strict';

angular
	.module('app.department')
	.factory('DepartmentServices', DepartmentServices);

DepartmentServices.$inject = ['$http', '$q', 'config', 'moveBoardApi', 'apiService', '$uibModal', 'Blob'];

function DepartmentServices($http, $q, config, moveBoardApi, apiService, $uibModal, Blob) {

	var department = {};

	department.getWorkersByPosition = getWorkersByPosition;
	department.createWorker = createWorker;
	department.workerFullDetails = workerFullDetails;
	department.editWorker = editWorker;
	department.editWorkerFullDetails = editWorkerFullDetails;
	department.getWorkersCountsByPosition = getWorkersCountsByPosition;
	department.saveEmailAccount = saveEmailAccount;
	department.deleteWorker = deleteWorker;
	department.setUserParser = setUserParser;
	department.testEmailConnection = testEmailConnection;
	department.uploadUserPhoto = uploadUserPhoto;
	department.excludedAdditionalServices = excludedAdditionalServices;
	department.convertPhotoToBaseSixtyFour = convertPhotoToBaseSixtyFour;
	department.generateEmail = generateEmail;
	department.checkRoles = checkRoles;

	return department;

	function checkRoles(request, str) {
		let result = false;

		if (str.toLowerCase().indexOf(request.workerPosition.toLowerCase())) {
			result = true;
		}

		return result;
	}

	function generateEmail(firstName, lastName) {
		let name = firstName.replace(/[,\s]+|[,\s]+/g, '');
		let surname = lastName.replace(/[,\s]+|[,\s]+/g, '');

		return name + surname + Math.floor((Math.random() * 100000) + 1).toString() + '@themoveboard.com';
	}

	function createWorker(request) {
		var deferred = $q.defer();

		var promise = $http({
			method: "post",
			url: config.serverUrl + 'server/clients/',
			data: request,
			timeout: deferred.promise
		});
		promise.then(function (response) {
		});
		promise._httpTimeout = deferred;

		return (promise);
	}

	function workerFullDetails(request) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/move_users_resource/write_user_settings/', request)
			.then(function (data) {
				deferred.resolve(data);
			}, function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function editWorker(request, id) {
		var deferred = $q.defer();

		$http.put(config.serverUrl + 'server/clients/' + id, request)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function editWorkerFullDetails(request, id) {
		var deferred = $q.defer();

		$http.put(config.serverUrl + 'server/clients/' + id, request)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function getWorkersByPosition(position, active) {
		var request = {
			'role': [position.toLowerCase()],
			'active': active
		};
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/move_users_resource/get_user_by_role', request)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function getWorkersCountsByPosition(position) {
		var request = {
			'role': [position.toLowerCase()],
			'active': true
		};
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/move_users_resource/get_count_users_by_role', request)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function deleteWorker(id) {
		var deferred = $q.defer();
		$http.delete(config.serverUrl + 'server/move_users_resource/' + id)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function saveEmailAccount(uid, email_account) {
		var deferred = $q.defer();
		var data = {};
		data.uid = uid;
		data.data = email_account;

		if (email_account) {
			$http.post(config.serverUrl + 'server/clients/email_account', data)
				.then(function (data) {
					deferred.resolve(data);
				}, function (data) {
					deferred.reject(data);
				});
		} else {
			deferred.reject("");
		}

		return deferred.promise;
	}

	function setUserParser(uid) {
		var deferred = $q.defer();
		var data = {};
		data.uid = uid;

		$http.post(config.serverUrl + 'server/move_users_resource/set_user_parser', data)
			.then(function (data) {
				deferred.resolve(data);
			}, function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function testEmailConnection(account) {
		return apiService.postData(moveBoardApi.department.testEmailConnection, account);
	}

	function uploadUserPhoto(uid, file) {
		let data = {
			uid: uid,
			data: file
		};
		return apiService.postData(moveBoardApi.department.uploadPhoto, data);
	}

	function convertPhotoToBaseSixtyFour(photoUrl) {
		var deferred = $q.defer();
		loadPhoto(photoUrl)
			.then(function (photoUrl) {
				var fileReader = new FileReader();

				fileReader.onload = (function (file) {
					return function (e) {
						deferred.resolve(e.target.result);
					};
				})(photoUrl);

				fileReader.readAsDataURL(photoUrl);
			});
		return deferred.promise;
	}

	function loadPhoto(photoUrl) {
		var deferred = $q.defer();
		if (!_.isObject(photoUrl)) {
			$http.get(photoUrl, {responseType: 'arraybuffer'})
				.then((res) => {
						photoUrl = new Blob([res.data], {type: "image/png"});
						deferred.resolve(photoUrl);
					}, function (photoUrl) {
						deferred.reject(photoUrl);
					});
		} else {
			deferred.resolve(photoUrl);
		}

		return deferred.promise;
	}

	function excludedAdditionalServices(settings) {
		let defer = $q.defer();

		var modalInstance = $uibModal.open({
			template: require('./templates/excludedAdditionalServicesModal.html'),
			controller: ['$scope', '$uibModalInstance', 'SweetAlert', 'SettingServices', 'settings', ModalInstanceCtrl],
			backdrop: false,
			resolve: {
				settings: function () {
					return settings;
				}
			},
			size: 'md'
		});

		modalInstance.result.then(function (data) {
			defer.resolve(data);
		});
		return defer.promise;
	}

	function ModalInstanceCtrl($scope, $uibModalInstance, SweetAlert, SettingServices, settings) {
		$scope.checkList = [];
		SettingServices.getSettings('additional_settings')
			.then(function (data) {
				$scope.additionalSettings = JSON.parse(data);

				_.forEach($scope.additionalSettings, function (item, index) {
					$scope.checkList.push({
						name: item.name,
						value: false
					});
				});

				_.forEach(settings, function (item) {
					let ind = _.findIndex($scope.checkList, {name: item});
					if (ind > -1) $scope.checkList[ind].value = true;
				});
			});


		$scope.cancel = cancel;
		$scope.saveSettings = saveSettings;

		function saveSettings() {
			let arr = [];
			_.forEach($scope.checkList, function (item) {
				if (item.value) arr.push(item.name);
			});
			$uibModalInstance.close({data: arr});
		}

		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}
	}

}
