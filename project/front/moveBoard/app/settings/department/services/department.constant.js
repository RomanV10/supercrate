'use strict';

angular.module('move.requests')
	.constant('departmentConstants', {
		'typeWorkers': ['Administrator', 'Manager', 'Sales', 'Driver', 'Helper', 'Foreman', 'Customer service'],
		'emailAccount': {
			'outgoing': {
				'active': false,
				'ssl': false,
				'tls': false,
				'server': '',
				'port': ''
			},
			'name': '',
			'password': ''
		},
		'autoAssignment': [
			{checked: false, name: 'All'},
			{checked: false, name: 'Local Move'},
			{checked: false, name: 'Moving and Storage'},
			{checked: false, name: 'Pickup Only'},
			{checked: false, name: 'Delivery Only'},
			{checked: false, name: 'Flat Rate'},
			{checked: false, name: 'Overnight'},
			{checked: false, name: 'Long Distance'},
			{checked: false, name: 'Packing Day'},
		],
		'autoAssignmentSettings': {
			isAutoAssign: false, services: []
		},
		'signatureSettings': {
			firstName: '', fullName: '', textSignature: ''
		},
		'optionListSettings': {
			optionList: [
				{name: '', optionDisabled: false},
				{name: '', optionDisabled: false},
				{name: '', optionDisabled: false},
				{name: '', optionDisabled: false},
				{name: 'Commission from total', optionDisabled: false},
				{name: 'Extras Commission', optionDisabled: false},
				{name: 'Daily Rate', optionDisabled: false},
				{name: 'Hourly Rate', optionDisabled: false},
				{name: 'Packing Commission', optionDisabled: false},
				{name: 'Bonus', optionDisabled: false}
			],
			inputValue: 0
		},
		'branches': ['Boston Flat Rate Movers'],
		'departments': ['Dispatch'],
		'rateAndCommissionMS': ['Office Commission', 'Office With visual', 'Visual Estimate'],
		'typeServices': [1, 2, 3, 4, 5, 6, 7, 8],
		'accountDetails': {
			'title': 'Your Moving Specialist', 'additionalInformation': '', 'isShowInfoOnAccount': false
		},
		'defaultPermissions': {
			canAssignToOther: false,
			canAssignUnassignedLeads: false,
			canEditOtherLeads: false,
			canEditPayroll: false,
			canMoveRequestToOtherBranch: false,
			canSearchOtherLeads: false,
			canSeeAllStatuses: false,
			canSeeDispatchMenu: false,
			canSeeInvoiceDashboardTab: false,
			canSeeLongDistanceMenu: false,
			canSeeOtherLeads: false,
			canSeeScheduleMenu: false,
			canSeeSettingsMenu: false,
			canSeeStatisticsMenu: false,
			canSeeStorageMenu: false,
			canSeeUnsignedLeads: false,
			canSignedSales: false,
			canSeeWhatCustomerViewRequest: false
		}
	});

