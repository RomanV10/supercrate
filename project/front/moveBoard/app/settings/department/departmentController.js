(function () {
    'use strict';

    angular
        .module('app.department')
        .controller('Department', Department);
    Department.$inject = ['$scope', '$rootScope', 'DepartmentServices', 'departmentConstants', 'PermissionsServices'];
    function Department($scope, $rootScope, DepartmentServices, departmentConstants, PermissionsServices) {

        var vm = this;
        var workerByPosition = {};
        var workerByPositionBuffer = [];

	    vm.isAdmin = PermissionsServices.ifAdmin();
	    vm.activeRow = -1;
	    vm.tempWorkers = angular.copy(departmentConstants.typeWorkers);
	    let adminIndex = vm.tempWorkers.indexOf('Administrator');
	    if (!vm.isAdmin && ~adminIndex) {
		    //if it is not admin remove Owner tab
		    vm.tempWorkers.splice(adminIndex, 1);
	    }

        vm.workersCounts = {};
        vm.userStatus = true;
        vm.currentPosition;

        vm.getWorkersByPosition = getWorkersByPosition;
        vm.getWorkersCountsByPosition = getWorkersCountsByPosition;
        vm.changeUserStatus = changeUserStatus;
        $rootScope.getWorkersCountsByPosition = getWorkersCountsByPosition;

        function changeUserStatus() {
			getWorkersByPosition(vm.currentPosition);
        }

        function mergeObjects(obj1, obj2) {
            if (_.isArray(obj1)) {
                for (var key in obj2) {
                    let index = _.findIndex(obj1, {uid: obj2[key].uid});

                    if (index != -1) {
                        obj1[index] = obj2[key];
                    } else {
                        obj1.push(obj2[key]); // if new user
                    }
                }
            } else {
                for (var key in obj2) {
                    if (obj2.hasOwnProperty(key)) {
                        obj1[key] = obj2[key];
                    }
                }
            }
        }

        function getWorkersByPosition(position) {
			vm.currentPosition = position;
            vm.activeRow = -1;
            $rootScope.loaderBusy = true;
            DepartmentServices
                .getWorkersByPosition(position, vm.userStatus)
                .then(function (data) {
                    $rootScope.workerByPosition = {};
                    $rootScope.currentPosition = position;
                    $rootScope.loaderBusy = false;
                    workerByPositionBuffer = [];
                    mergeObjects($rootScope.workerByPosition, data[position.toLowerCase()]);
                    workerByPosition = angular.copy($rootScope.workerByPosition);
                    for (let key in workerByPosition) {
                        workerByPositionBuffer.push(workerByPosition[key]);
                    }
                    workerByPositionBuffer.sort(function (a, b) {
                        if (a.field_user_first_name && b.field_user_first_name) {
                            return a.field_user_first_name.toLowerCase().localeCompare(b.field_user_first_name.toLowerCase());
                        }

                        return 0;
                    });

                    workerByPosition = workerByPositionBuffer;
					$rootScope.workerByPosition = workerByPosition;
                });
        }

        function getWorkersCountsByPosition() {
            angular.forEach(vm.tempWorkers, function (worker, nid) {
                DepartmentServices.getWorkersCountsByPosition(worker).then(function (data) {
                    vm.workersCounts[worker] = data;
                });
            });
        }

        getWorkersCountsByPosition();

        $scope.$on('update.department.worker', function (event, obj) {
            if (!_.isUndefined(obj)) {
				$rootScope.workerByPosition = obj;
				vm.userStatus = obj[Object.keys(obj)[0]].settings.employee_is_active;
            }
        });

        $scope.getShortServiceName = function (services) {
            var result = '';

            if (_.isEqual(services.length, 0)) {
                result = 'None';
            } else {
                if (_.isEqual(services.length, 7)) {
                    result = 'All';
                } else {
                    services.sort();

                    _.each(services, function (value, key) {
                        if (!_.isEmpty(result)) {
                            result += ', ';
                        }

                        switch (value) {
                            case 1:
                                result += "Local";
                                break;
                            case 2:
                                result += "M&S";
                                break;
                            case 3:
                                result += 'Pick. Only';
                                break;
                            case 4:
                                result += 'Del. Only';
                                break;
                            case 5:
                                result += 'FR';
                                break;
                            case 6:
                                result += 'Overn.';
                                break;
                            case 7:
                                result += 'LD';
                                break;
                            case 8:
                                result += 'PD';
                                break;
                        }
                    });
                }
            }

            return result;
        }
    }

})();
