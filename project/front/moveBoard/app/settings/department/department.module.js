(function () {
	'use strict';

	angular.module('app.department', ['flow', 'uiCropper', 'multipleSelect', 'er-signatures']);

})();