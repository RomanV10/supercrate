angular
	.module('app.settings.sms-template-builder')
	.service('smsTemplateBuilderService', smsTemplateBuilderService);

/* @ngInject */
function smsTemplateBuilderService(moveBoardApi, apiService) {
	let service = {
		getTemplates,
		updateTemplate,
	};

	return service;

	function _returnData({data}) {
		return data;
	}

	function getTemplates() {
		return apiService.postData(moveBoardApi.smsSettings.smsTemplateBuilder.index)
			.then(_returnData);
	}

	function updateTemplate(id, data) {
		return apiService.postData(moveBoardApi.smsSettings.smsTemplateBuilder.edit, {id, data})
			.then(_returnData);
	}
}