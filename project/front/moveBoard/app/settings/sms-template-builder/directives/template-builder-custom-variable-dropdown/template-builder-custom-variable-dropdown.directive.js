import './template-builder-custom-variable-dropdown.styl';

angular.module('app.settings.sms-template-builder')
	.directive('templateBuilderCustomVariableDropDown', templateBuilderCustomVariableDropDown);

/* @ngInject */
function templateBuilderCustomVariableDropDown($timeout) {
	return {
		restriction: 'E',
		template: require('./template-builder-custom-variable-dropdown.html'),
		transclude: true,
		link,
	};

	function link($scope, element, attrs) {
		$scope.isShowVariable = false;
		let editedElemnt = element;
		let closeTooltipTimeout;

		if (attrs.editElementClass) {
			editedElemnt = element.find('.' + attrs.editElementClass);
		}

		editedElemnt.on('focus', showTooltip);
		editedElemnt.on('blur', closeTooltip);

		$scope.chooseVariable = chooseVariable;
		$scope.activateSearch = showTooltip;
		$scope.closeTooltip = closeTooltip;

		function chooseVariable({value}) {
			$timeout(() => {
				editedElemnt.caret(value);
				editedElemnt.trigger('change');
			});
		}

		function closeTooltip() {
			closeTooltipTimeout = $timeout(() => {
				$scope.isShowVariable = false;
			}, 1000);
		}

		function showTooltip() {
			$timeout.cancel(closeTooltipTimeout);
			$scope.isShowVariable = true;
			$scope.searchValue = '';
		}
	}
}
