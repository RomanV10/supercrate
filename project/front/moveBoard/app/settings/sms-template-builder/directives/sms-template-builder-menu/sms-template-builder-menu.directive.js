import './sms-template-builder-menu.styl';

angular
	.module('app.settings.sms-template-builder')
	.directive('smsTemplateBuilderMenu', smsTemplateBuilderMenu);

/* @ngInject */
function smsTemplateBuilderMenu(datacontext) {
	return {
		restriction: 'E',
		template: require('./sms-template-builder-menu.html'),
		scope: {
			templates: '=',
			selectedTemplate: '<',
			onChooseTemplate: '=?',
		},
		link,
	};

	function link($scope) {
		let fieldData = datacontext.getFieldData();
		let smsTemplateFolders = _.get(fieldData, 'enums.sms_notification_folders', {});
		$scope.templatesMenu = buildTemplateMenu();
		$scope.filterByKeyName = {id: ''};

		$scope.isChooseChildTemplate = isChooseChildTemplate;
		$scope.comparatorFunction = comparatorFunction;
		$scope.chooseTemplate = chooseTemplate;

		function isChooseChildTemplate (templates) {
			return _.find(templates, {'id': $scope.selectedTemplate.id});
		}

		function comparatorFunction(actual, expected) {
			return _.isEmpty(expected) || _.isEqual(actual, expected);
		}

		function chooseTemplate(template) {
			if (!$scope.onChooseTemplate) return;

			$scope.onChooseTemplate(template);
		}

		function buildTemplateMenu() {
			let menu = [];

			angular.forEach(smsTemplateFolders, (folderId, folderName) => {
				let menuItem = {
					header: folderName,
					templates: $scope.templates.filter(template => template.folder == folderId)
				};

				if (menuItem.templates.length) {
					menu.push(menuItem);
				}
			});

			return menu;
		}
	}
}