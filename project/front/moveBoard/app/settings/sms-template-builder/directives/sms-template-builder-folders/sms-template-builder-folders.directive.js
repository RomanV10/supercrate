import './sms-template-builder-folders.styl';

angular
	.module('app.settings.sms-template-builder')
	.directive('smsTemplateBuilderFolders', smsTemplateBuilderFolders);

/* @ngInject */
function smsTemplateBuilderFolders(smsTemplateBuilderService) {
	return {
		restriction: 'E',
		template: require('./sms-template-builder-folders.html'),
		scope: {},
		link,
	};

	function link($scope) {
		$scope.templates = [];

	}
}