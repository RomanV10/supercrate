import './sms-template-builder.styl';

angular
	.module('app.settings.sms-template-builder')
	.directive('smsTemplateBuilder', smsTemplateBuilder);

/* @ngInject */
function smsTemplateBuilder() {
	return {
		restriction: 'E',
		template: require('./sms-template-builder.html'),
		scope: {},
		controller: 'SMSTemplateBuilderController',
		controllerAs: 'vm',
	};
}