angular
	.module('app.settings.sms-template-builder')
	.controller('SMSTemplateBuilderController', SMSTemplateBuilderController);

/* @ngInject */
function SMSTemplateBuilderController(smsTemplateBuilderService) {
	let vm = this;
	vm.saving = false;
	vm.busy = false;
	vm.templates = [];
	vm.tabs = [
		{
			name: 'SMS Builder',
			selected: true,
		},
		{
			name: 'SMS Folder',
			selected: false,
		},
	];

	loadTemplates();

	vm.select = select;

	function loadTemplates() {
		vm.busy = true;

		smsTemplateBuilderService.getTemplates()
			.then(templates => {
				templates.forEach(template => {
					template.isActive = !!(+template.active);
				});

				vm.templates = templates;
			})
			.finally(() => {
				vm.busy = false;
			});
	}

	function select(tab) {
		vm.tabs.forEach(tab => tab.selected = false);
		tab.selected = true;
	}
}