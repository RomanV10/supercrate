import templateBuilderButtons from '../../../../template-builder/tooltip-data/templateBuilderButtons.json';
import invoiceButtons from '../../../../template-builder/tooltip-data/invoiceButtons.json';
import tooltipButtons from '../../../../template-builder/tooltip-data/tooltipButtons.json';

import './sms-template-builder-editor.styl';

angular
	.module('app.settings.sms-template-builder')
	.directive('smsTemplateBuilderEditor', smsTemplateBuilderEditor);

/* @ngInject */
function smsTemplateBuilderEditor(smsTemplateBuilderService, SweetAlert) {
	return {
		restriction: 'E',
		template: require('./sms-template-builder-editor.html'),
		scope: {
			templates: '='
		},
		link,
	};

	function link($scope) {
		const NEW_USER_TEMPLATES_VALUES = [
			'[custom:company-name]',
			'[custom:company-phone]',
			'[custom:company-email]',
			'[custom:site-url]',
			'[custom:fname]',
			'[custom:lname]',
			'[custom:client-email]',
			'[custom:client-phone]',
			'[custom:user-password]',
			'[custom:created-user-time]',
		];
		$scope.templateItem = {};
		$scope.isTemplateChanged = false;
		$scope.busy = false;
		let saveData = {};
		let originalTemplate = {};
		let allTemplates = _.union(tooltipButtons, invoiceButtons, templateBuilderButtons);
		$scope.tooltipButtons = allTemplates;

		$scope.saveChanges = saveChanges;
		$scope.onChooseTemplate = onChooseTemplate;
		$scope.onChangeActive = onChangeActive;
		$scope.onChangeTemplate = onChangeTemplate;

		function saveChanges() {
			if (_.isEmpty(saveData)) return;

			$scope.busy = true;

			smsTemplateBuilderService.updateTemplate($scope.templateItem.id, saveData)
				.then(() => {
					_setDefaultTemplateChanges();
					originalTemplate = angular.copy($scope.templateItem);
					toastr.success('Template was saved', 'Success', 'success');
				})
				.catch(() => {
					toastr.error('Template was not updated', 'Error', 'error');
					revertChanges();
					_setDefaultTemplateChanges();
				})
				.finally(() => {
					$scope.busy = false;
				});
		}

		function _setDefaultTemplateChanges() {
			$scope.isTemplateChanged = false;
			saveData = {};
		}

		function onChooseTemplate(template) {
			if (_.isEmpty(saveData)) {
				setTemplateItem(template);
			} else {
				SweetAlert.swal({
					title: 'You haven\'t saved the changes!',
					text: 'You have unsaved changes! Do you want to save them?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Save',
					cancelButtonText: 'Cancel',
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						saveChanges();
					} else {
						revertChanges();
					}

					setTemplateItem(template);
				});
			}
		}

		function revertChanges() {
			angular.forEach(originalTemplate, (value, key) => {
				$scope.templateItem[key] = value;
			});
		}

		function setTemplateItem(template) {
			_setDefaultTemplateChanges();

			if (+template.id === 1) {
				$scope.tooltipButtons = allTemplates.filter(variable => (NEW_USER_TEMPLATES_VALUES.indexOf(variable.value) > -1));
			} else {
				$scope.tooltipButtons = allTemplates;
			}

			$scope.templateItem = template;
			originalTemplate = angular.copy(template);
		}

		function onChangeActive() {
			saveData.active = Number($scope.templateItem.isActive);
			$scope.isTemplateChanged = true;
		}

		function onChangeTemplate() {
			saveData.template = $scope.templateItem.template;
			$scope.isTemplateChanged = true;
		}
	}
}