'use strict';

angular.module('app.settings.sms-template-builder')
	.config(smsConfigRoute);

/* @ngInject */
function smsConfigRoute($stateProvider) {
	$stateProvider
		.state('settings.smsTemplateBuilder', {
			url: '/sms-template-builder',
			template: '<sms-template-builder></sms-template-builder>',
			title: 'SMS Template Builder',
			data: {
				permissions: {
					except: ['anonymous', 'foreman', 'helper']
				}
			}
		});
}