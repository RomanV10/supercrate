(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('settings', {
                        url: '/settings',
                        abstract: true,
                        template: '<ui-view />'
                    });
            }
        ]
    );
})();
