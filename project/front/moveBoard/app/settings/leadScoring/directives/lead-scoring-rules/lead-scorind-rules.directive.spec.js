describe('Lead Scoring settings', function () {
	var ele, scope;
	let elementScope,
		$q,
		deferred,
		SettingServices,
		apiService,
		config;

	function compileElement(elementHtml) {
		ele = angular.element(elementHtml);
		$compile(ele)(scope);
		elementScope = ele.scope();
	}

	beforeEach(inject(function (_$q_, _SettingServices_, _apiService_, _config_) {
		$q = _$q_;
		$rootScope.fieldData.leadscoringsettings = {};
		scope = $rootScope.$new();
		SettingServices = _SettingServices_;
		apiService = _apiService_;
		config = _config_;
		deferred = $q.defer();

		let notifications = [{notification_with_score: 1, ntid: 1, score: 1}];
		$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.notifications.getNotifications, {})).respond(200, notifications);
		$httpBackend.whenGET(/.*/).passThrough();
		$httpBackend.whenPUT(/.*/).passThrough();
		compileElement('<lead-scoring-rules></lead-scoring-rules>');
		$timeout.flush();

	}));

	/**
	 * @description
	 * Sample test case to check if HTML rendered by the directive is non empty
	 * */
	describe('Directive DOM', () => {
		it('should not render empty html', function () {
			expect(ele.html()).not.toBe('');
		});

	});

	describe('Notification Score', () => {
		it('should make request on blur', function () {
			elementScope.notification =  {
				ntid: 1,
				score: 2
			};
			elementScope.changeNotificationScore(elementScope.notification);
			$httpBackend.expectPUT(testHelper.makeRequest(moveBoardApi.notifications.update_notification_score, { ntid: elementScope.notification.ntid }), {score: 2}).respond(200);
			$httpBackend.flush();
		});

	});

	describe('Save settings', () => {
		beforeEach(function () {
			spyOn(SettingServices, 'saveSettings').and.returnValue(deferred.promise);
			delete $rootScope.fieldData.leadscoringsettings;
			compileElement('<lead-scoring-rules></lead-scoring-rules>');
			$timeout.flush();
		});
		it('when leadscoringsettings is empty', function () {
			expect(SettingServices.saveSettings).toHaveBeenCalled();
			expect(SettingServices.saveSettings.calls.count()).toBe(1);
		});
		it('should do something on success', function () {
			deferred.resolve(); // Resolve the promise to replicate success scenario.
			elementScope.$digest();
			elementScope.$apply();
			// Check for state on success.
			expect(elementScope.saving).toBeFalsy(); //testing properties
		});
	});

	describe('Lead score update', () => {

		it('It should make request', function () {
			elementScope.leadScoringForm = {$valid: true};
			elementScope.leadScoringSetting = {
				enabled: true,
				scoring: {
					hot: 100,
					warm: 75,
					cold: 0
				}
			};

			let data = {
				name: 'leadscoringsettings',
				value:  angular.toJson(elementScope.leadScoringSetting)
			};
			$httpBackend.expectPOST(testHelper.makeRequest('server/settings/set_variable'), data).respond(200);
			elementScope.updateScoring();
			$httpBackend.flush();
		});
	});
});
