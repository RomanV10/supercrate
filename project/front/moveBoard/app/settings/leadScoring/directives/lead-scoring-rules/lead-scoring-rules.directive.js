'use strict';

import './lead-scoring-rules.styl';

angular
	.module('app.settings')
	.directive('leadScoringRules',  leadScoringRules);
leadScoringRules.$inject = ['SettingServices', 'datacontext', '$timeout', 'apiService', 'moveBoardApi'];

function leadScoringRules(SettingServices, datacontext, $timeout, apiService, moveBoardApi) {
	return {
		restrict: 'E',
		template: require('./lead-scoring-rules.tpl.pug'),
		link: lincFunc
	};
	function lincFunc($scope) {
		let saveDellay;
		let previousNotificationsScores = [];

		$scope.updateScoring = updateScoring;
		$scope.changeNotificationScore = changeNotificationScore;

		let fieldData = datacontext.getFieldData();
		$scope.leadScoringSetting = angular.fromJson(fieldData.leadscoringsettings);
		let previousSetting = angular.copy($scope.leadScoringSetting);
		if (!$scope.leadScoringSetting) {
			$scope.leadScoringSetting = {
				enabled: false,
				scoring: {
					hot: '100',
					warm: '50',
					cold: '0'
				}
			};
			updateScoring();
		}

		function updateScoring() {
			if ($scope.leadScoringForm.$valid && !_.isEqual( $scope.leadScoringSetting, previousSetting)) {

				previousSetting = angular.copy($scope.leadScoringSetting);
				var setting ={
					 enabled: $scope.leadScoringSetting.enabled,
					 scoring: {
						 hot: parseInt($scope.leadScoringSetting.scoring.hot),
						 warm: parseInt($scope.leadScoringSetting.scoring.warm),
						 cold: 0
					 }
				};
				var _setting_name = 'leadscoringsettings';
				$scope.saving = true;
				$scope.saved = false;
				$timeout.cancel(saveDellay);
				var promise = SettingServices.saveSettings(setting, _setting_name);
				promise.then(function () {
					$scope.saving = false;
					saveDellay = $timeout(function () {
						$scope.saved = false;
					}, 1000);

					$scope.saved = true;
				});
			}
		}
		function getColumn(notification, startPoint) {
			let columnLength = Math.ceil(notification.length / 3);
			let result = _.slice(notification, startPoint * columnLength, (startPoint + 1) * columnLength);
			return result;
		}

		function getNotifications() {
			$scope.notificationsExists = false;
			apiService.postData(moveBoardApi.notifications.getNotifications,{}).then(data => {
				$scope.notificationsExists = true;
				if (!_.isEmpty(data.data)) {
					let validLeadScoreNotifications = _.filter(data.data, function(o) { return o.notification_with_score === 1; });
					previousNotificationsScores = angular.copy(validLeadScoreNotifications);
					$scope.firstColumn = getColumn(validLeadScoreNotifications, 0);
					$scope.secondColumn = getColumn(validLeadScoreNotifications, 1);
					$scope.thirdColumn =  getColumn(validLeadScoreNotifications, 2);
				}
			})
		}

		function changeNotificationScore(item) {
			let previousNotificationValue = _.find(previousNotificationsScores, function(o) { return o.ntid == item.ntid; })

			if (previousNotificationValue.score != item.score) {

				$scope.scoreSaving = true;
				$scope.scoreSaved = false;
				item.score = item.score != 'null' ? item.score : '0';
				previousNotificationValue.score = item.score;
				var promise = apiService.putData(moveBoardApi.notifications.update_notification_score, {ntid: item.ntid, score: item.score});
				promise.then(function () {
					$scope.scoreSaving = false;
					saveDellay = $timeout(function () {
						$scope.scoreSaved = false;
					}, 1000);

					$scope.scoreSaved = true;
				})
			}
		}

		getNotifications();

		$scope.$on('destroy', function() {
			$timeout.cancel(saveDellay);
		})
	}
}
