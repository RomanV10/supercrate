(function(){

	angular.module("app")
		.config([
				"$stateProvider",
				function ($stateProvider)
				{

					$stateProvider
						.state('settings.leadScoring', {
							url: '/lead-scoring',
							template: require('./lead-scoring-page.tpl.pug'),
							title: 'Lead Scoring',
							data: {
								permissions: {
									except: ['anonymous', 'foreman', 'helper']
								}
							}
						});
				}
			]
		);
})();
