(function () {
    'use strict';

    angular
        .module('app.settings')

     .controller('LongdistanceContorller', function ($scope,datacontext,common,$uibModal,logger,SettingServices) {

            var vm = this;
            vm.longdistance = {};
            vm.saving = false;
            vm.areaCodes = [];
            vm.areaCodes['California'] = [209, 213, 310, 323, 408, 415, 424, 442, 510, 530, 559, 562, 619, 626, 650, 657, 661, 669, 707, 714, 747, 760, 805, 818, 831, 858, 909, 916, 925, 949, 951];
            vm.areaCodes['Massachusetts'] = [339, 351, 413, 508, 617, 774, 781, 857, 978];

            vm.showSidebar = false;

            var data =  SettingServices.getSettings();
             if(data.longdistance != null)
                 vm.longdistance =  angular.fromJson(data.longdistance[0]);

            vm.saveSettings = function(){

                vm.saving = true;

                 var promise = SettingServices.saveSettings(vm.longdistance,'longdistance');
                promise.then(function() {

                        vm.saving = false;


                         common.$timeout(function() {
                                    vm.saved = false;
                            },1000);

                        vm.saved = true;
                });

            }





    })


})();
