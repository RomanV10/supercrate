import "./ngRule.styl";

'use strict';
angular.module('app.settings')
	.directive('ngRule', ngRule);

function ngRule() {
	return {
		restrict: "E",
		template: require('./ngRule.pug'),
		scope: {
			ngRuleModel: '=',
			ngRuleChange: '&',
			trailingDirection: '@',
			trailingOffset: '@',
			min: '@',
			max: '@'
		},
		link: ngRuleLink
	};

	function ngRuleLink(scope, element, $attrs) {
		if (isNaN(scope.min)) {
			scope.min = 0;
		}
		if (isNaN(scope.max)) {
			scope.max = 1000;
		}

		let a;
		for (a = scope.min; a <= scope.max; a += 100) {
			element.find('.ngRule').append("<div class='ngRuleDigit' style='top:0px; left:" + a + "px'>" + a + "</div>");
		}

		scope.isGrabbed = false;
		scope.enableVertical = scope.trailingDirection.indexOf('V') != -1;
		scope.enableHorizontal = scope.trailingDirection.indexOf('H') != -1;
		scope.pointerStyle = {};
		scope.pointerStyle.cursor = scope.enableVertical?'ew-resize':'ew-resize';
		function updateStyle(model) {
			if (!isNaN(model)) {
				scope.pointerStyle.marginLeft = scope.enableHorizontal && (Number(model) + Number(scope.trailingOffset));
				scope.pointerStyle.marginTop = scope.enableVertical && (Number(model) + Number(scope.trailingOffset));
			}
		}

		updateStyle(scope.ngRuleModel);
		scope.$watch('ngRuleModel', updateStyle);
		scope.grabPointer = function (event) {
			if (event.which == 1) {
				scope.ngRuleModel = Math.round(event.clientX
					- element[0].getBoundingClientRect().left
					- scope.trailingOffset);
				scope.isGrabbed = true;
				document.body.style.cursor = 'ew-resize !important';
				document.body.addEventListener('mousemove', scope.dragPointer, true);
				document.body.addEventListener('mouseup', scope.dropPointer, true);
				document.body.addEventListener('mouseenter', scope.catchEvent, true);
				document.body.addEventListener('mouseleave', scope.catchEvent, true);
				document.body.addEventListener('selectstart', scope.catchEvent, true);
			}
		};
		let clickPos;
		scope.dragPointer = function (event) {
			event.stopPropagation();
			if (scope.isGrabbed) {
				clickPos = Math.round(event.clientX
					- element[0].getBoundingClientRect().left
					- scope.trailingOffset);
				if ((clickPos >= scope.min) && (clickPos <= scope.max)) {
					scope.ngRuleModel = clickPos;
				}
			}
		};
		scope.dropPointer = function () {
			if (event.which == 1) {
				event.stopPropagation();
				scope.isGrabbed = false;
				scope.ngRuleChange();
				document.body.style.cursor = '';
				document.body.removeEventListener('mousemove', scope.grabPointer, true);
				document.body.removeEventListener('mouseup', scope.dropPointer, true);
				document.body.removeEventListener('mouseenter', scope.catchEvent, true);
				document.body.removeEventListener('mouseleave', scope.catchEvent, true);
				document.body.removeEventListener('selectstart', scope.catchEvent, true);
			}
		};
		scope.catchEvent = function (event) {
			event.preventDefault();
			event.stopPropagation();
		};
	}
}
