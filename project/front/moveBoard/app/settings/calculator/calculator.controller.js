'use strict';

let moveCalculateDefaultSettings = require('./default-form-settings/moveCalculateDefaultSettings.json');
let longDistanceDefaultSettings = require('./default-form-settings/longDistanceDefaultFormSettings.json');
import './templates/_settings_calculator.styl';
import './templates/formsetting/top-form-settings.styl';

angular
	.module('app.settings')

	.controller('CalcContorller', CalcContorller);

CalcContorller.$inject = ['$scope', 'SettingServices', 'datacontext', 'common', 'CalculatorServices', 'SweetAlert', 'apiService', 'moveBoardApi'];

function CalcContorller($scope, SettingServices, datacontext, common, CalculatorServices, SweetAlert, apiService, moveBoardApi) {
	// Inititate the promise tracker to track form submissions.

	let vm = this;
	vm.calculatorSettings = [];

	vm.fieldData = datacontext.getFieldData();
	vm.moveSize = [];
	vm.speed = [];
	vm.currentSize = [];
	vm.size = [];
	vm.room_size = [];
	vm.packages = [];
	vm.move_size = vm.fieldData.field_lists.field_size_of_move;
	vm.serviceType = vm.fieldData.field_lists.field_move_service_type;
	vm.rooms = vm.fieldData.field_lists.field_extra_furnished_rooms;
	vm.objectKeys = function (obj) {
		return Object.keys(obj);
	};
	vm.currentMovers = 2;
	vm.range = common.range;
	vm.saved = true;
	vm.calcResults = false;
	vm.calcSettings = {};
	vm.doubleTravelTime = [0, 15, 30, 45, 60];

	if (angular.isDefined(vm.fieldData.calcsettings)) {
		vm.calcSettings = angular.fromJson(vm.fieldData.calcsettings);
	}

	vm.longdistance = {};
	if (angular.isDefined(vm.fieldData.longdistance)) {
		vm.longdistance = angular.fromJson(vm.fieldData.longdistance);
	}

	if (angular.isDefined(vm.fieldData.basicsettings)) {
		vm.basicSettings = angular.fromJson(vm.fieldData.basicsettings);
		vm.movecalcFormSettings = angular.copy(vm.basicSettings.movecalcFormSettings);
	}

	// apiService.getData(moveBoardApi.inventory.package.query).then(res => vm.packages = res.data)

	vm.allowedServiceTypes = vm.fieldData.field_lists.field_move_service_type;

	// Commercial Move
	if (!_.isUndefined(vm.fieldData.field_lists.field_size_of_move['11'])) {
		vm.allowedServiceTypes[Object.keys(vm.allowedServiceTypes).length + 1] = vm.fieldData.field_lists.field_size_of_move['11'];
	}

	vm.saved = false;
	vm.saving = false;
	vm.tabs = [
		{
			name: 'Calculator',
			url: 'app/settings/calculator/templates/calcTest.html',
			selected: true,
			icon: 'icon-calculator'
		},
		{
			name: 'Basic Settings',
			url: 'app/settings/calculator/templates/calculator-settings.html',
			selected: false,
			icon: ' icon-settings'
		},
		{
			name: 'Move Size Settings',
			url: 'app/settings/calculator/templates/move-size-settings.html',
			selected: false,
			icon: 'icon-layers'
		},
		{
			name: 'Commercial Settings',
			url: 'app/settings/calculator/templates/commercial-move-size-settings.html',
			selected: false,
			icon: 'fa fa-usd'
		},
		{
			name: 'Travel Time',
			url: 'app/settings/calculator/templates/dis-zips-settings.html',
			selected: false,
			icon: 'icon-layers'
		},
		{
			name: 'Top Form Text',
			url: 'app/settings/calculator/templates/formsetting/frontTopFormSettings.html',
			selected: false,
			icon: 'fa fa-file-text'
		},
		{
			name: 'Form Style',
			url: 'app/settings/calculator/templates/formsetting/frontFormSettings.html',
			selected: false,
			icon: 'fa fa-file-text'
		}
	];

	vm.select = function (tab) {
		angular.forEach(vm.tabs, function (tab) {
			tab.selected = false;
		});

		tab.selected = true;
		vm.template = tab;
	};


	initCalcSettingsPage();

	vm.saveBasicSettings = function () {
		let promise = SettingServices.saveSettings(vm.basicSettings, 'basicsettings');
		vm.saved = false;
		promise.then(function () {
			vm.fieldData.basicsettings = vm.basicSettings;
			vm.saving = false;

			common.$timeout(function () {
				vm.saved = false;
			}, 1000);

			vm.saved = true;
		});
	};

	vm.saveSettings = function () {
		let promise = SettingServices.saveSettings(vm.calcSettings, 'calcsettings');
		vm.saved = false;
		promise.then(function () {
			vm.fieldData.calcsettings = vm.calcSettings;
			CalculatorServices.init(vm.fieldData);
			vm.saving = false;
			common.$timeout(function () {
				vm.saved = false;
			}, 1000);

			vm.saved = true;
		});

	};

	for (let i = 2; i <= 8; i++) {
		vm.speed[i] = 100;
	}

	$scope.$on("calc.results", function (event, data) {
		vm.iscalcResults = true;
		vm.calc_results = data;
	});

	vm.isDate = false;

	function initCalcSettingsPage() {

		if (vm.calcSettings.min_hours) {
			for (let i = 0; i <= common.count(vm.fieldData.field_lists.field_type_of_entrance_from); i++) {
				vm.moveSize[i] = [];

				_.each(vm.move_size, function (size, index) {
					if (index == 11) {
						// Commercial move use his own calculator
						vm.moveSize[i].push({
							id: index,
							name: size,
							weight: 0
						});
						if (vm.calcSettings.size['11'] != 0) {
							vm.calcSettings.size['11'] = 0;
						}
						return;
					}
					vm.moveSize[i].push({
						id: index,
						name: size + ' (' + vm.calcSettings.size[index].min + '-' + vm.calcSettings.size[index].max + ' cbf)',
						weight: vm.calcSettings.size[index].max
					});
				});

				vm.currentSize[i] = vm.moveSize[i][0];
			}
		}
	}

	vm.saveLongDistanceSettings = function () {
		vm.saving = true;

		let promise = SettingServices.saveSettings(vm.longdistance, 'longdistance');
		promise.then(function () {
			vm.saving = false;

			common.$timeout(function () {
				vm.saved = false;
			}, 1000);
			vm.saved = true;
		});
	};

	vm.changeMoveCalcFormSettings = function () {
		vm.basicSettings.movecalcFormSettings = _.clone(vm.movecalcFormSettings);
		vm.saveBasicSettings();
	};

	vm.firstStep = true;
	vm.secondStep = false;
	vm.thirdStep = false;
	vm.fourthStep = false;

	$scope.selectStep = function (step) {
		vm.firstStep = _.isEqual(step, 'first');
		vm.secondStep = _.isEqual(step, 'second');
		vm.thirdStep = _.isEqual(step, 'third');
		vm.fourthStep = _.isEqual(step, 'fourth');
	};

	vm.afterReset = false;
	vm.flatRate = true;
	vm.longDistance = false;
	vm.localMove = false;
	vm.localMoveQuoteOff = false;
	vm.storage = false;
	vm.overnightStorage = true;

	$scope.selectFlatRate = function () {
		vm.flatRate = true;
		vm.longDistance = false;
		vm.localMove = false;
		vm.storage = false;
	};

	$scope.selectLongDistance = function () {
		vm.flatRate = false;
		vm.longDistance = true;
		vm.localMove = false;
		vm.localMoveQuoteOff = false;
		vm.storage = false;
	};

	$scope.selectLocalMove = function () {
		vm.flatRate = false;
		vm.longDistance = false;
		vm.localMove = true;
		vm.localMoveQuoteOff = false;
		vm.storage = false;
	};

	$scope.selectLocalMoveQuoteOff = function () {
		vm.flatRate = false;
		vm.longDistance = false;
		vm.localMove = false;
		vm.localMoveQuoteOff = true;
		vm.storage = false;
	};

	$scope.selectStorage = function () {
		vm.flatRate = false;
		vm.longDistance = false;
		vm.localMove = false;
		vm.localMoveQuoteOff = false;
		vm.storage = true;
	};

	$scope.isActiveLocalMove = function () {
		return vm.localMove || (vm.fourthStep && vm.localMoveQuoteOff);
	};

	$scope.isActiveLocalMoveHideQuote = function () {
		return vm.localMoveQuoteOff && !vm.fourthStep;
	};

	$scope.restoreToDefault = function (property) {
		SweetAlert.swal({
			title: "Are you sure?",
			text: "Your changes will be lost!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#E47059",
			confirmButtonText: "Confirm",
			closeOnConfirm: true
		}, function (isConfirm) {
			if (isConfirm) {
				if (angular.equals(property, "thirdStepRightBlockLongDistance")) {
					vm.longdistance.form_info_right = angular.copy(longDistanceDefaultSettings.form_info_right);
					vm.saveLongDistanceSettings();
				} else {
					vm.basicSettings.movecalcFormSettings[property] = angular.copy(moveCalculateDefaultSettings[property]);
					vm.movecalcFormSettings = _.clone(vm.basicSettings.movecalcFormSettings);
					vm.changeMoveCalcFormSettings();
				}
			}
		});
	};

	$scope.isDefaultMoveCalcFormSetting = function (property) {
		return angular.isDefined(vm.movecalcFormSettings[property])
			&& vm.movecalcFormSettings[property] == moveCalculateDefaultSettings[property];
	};

	$scope.isDefaultThirdStepRightBlockLongDistance = function () {
		return angular.isDefined(vm.longdistance.form_info_right) && vm.longdistance.form_info_right == longDistanceDefaultSettings.form_info_right;
	};

	$scope.isSecondStepFirst = function () {
		return vm.movecalcFormSettings.formStepsOrder[0].id == 1;
	};

	$scope.isThirdStepFirst = function () {
		return vm.movecalcFormSettings.formStepsOrder[0].id == 2;
	};

	$scope.changeStepsOrder = function () {
		let temp = vm.movecalcFormSettings.formStepsOrder[0].id;
		vm.movecalcFormSettings.formStepsOrder[0].id = vm.movecalcFormSettings.formStepsOrder[1].id;
		vm.movecalcFormSettings.formStepsOrder[1].id = temp;
		vm.changeMoveCalcFormSettings();
	};

	$scope.$on('form-step-bag.drop-model', function (e, el) {
		vm.changeMoveCalcFormSettings();
	});

	$scope.isVisibleServiceButtonPanel = function () {
		return (vm.thirdStep && $scope.isSecondStepFirst())
			|| (vm.secondStep && $scope.isThirdStepFirst())
			|| vm.fourthStep;
	};

	$scope.switchTruckSpeedEnabled = function () {
		if (vm.calcSettings.customTrackSpeed.enabledBool) {
			vm.calcSettings.customTrackSpeed.enabled = 1;
		} else {
			vm.calcSettings.customTrackSpeed.enabled = 0;
		}
		vm.saveSettings();
	};

	vm.addSource = function () {
		if (vm.newSource != "") {
			vm.movecalcFormSettings.pollSources.sources.push({
				"index": vm.movecalcFormSettings.pollSources.lastIndex.toString(),
				"name": vm.newSource
			});
			vm.movecalcFormSettings.pollSources.lastIndex += 1;
			vm.newSource = "";
			vm.changeMoveCalcFormSettings();
		}
	};

	vm.removeSource = function (index) {
		vm.movecalcFormSettings.pollSources.sources.splice(index, 1);
		vm.changeMoveCalcFormSettings();
	};

	$scope.$watch('vm.movecalcFormSettings.companyColor', function () {
		$scope.companyBGColor = {'background-color': vm.movecalcFormSettings.companyColor};
		$scope.companyColorDarker = LightenDarkenColor(vm.movecalcFormSettings.companyColor, -0.1);
		$scope.companycolorLighter = LightenDarkenColor(vm.movecalcFormSettings.companyColor, 0.1);
	});

	function LightenDarkenColor(hex, lum) {
		// validate hex string
		hex = String(hex).replace(/[^0-9a-f]/gi, '');

		if (hex.length < 6) {
			hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
		}

		lum = lum || 0;

		// convert to decimal and change luminosity
		let rgb = "#", c, i;

		for (i = 0; i < 3; i++) {
			c = parseInt(hex.substr(i * 2, 2), 16);
			c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
			rgb += ("00" + c).substr(c.length);
		}

		return rgb;
	}
}

