(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('settings.calculator', {
                        url: '/calculator',
	                    template: require('./templates/calculator-page.html'),
                        title: 'Calculator',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });
            }
        ]
    );
})();
