'use strict';

angular
	.module('app.settings')
	.controller('ScheduleContorller', ScheduleContorller);

/* @ngInject */
function ScheduleContorller(common, SettingServices) {
	const HIDE_SAVED_TIMEOUT = 1000;

	let vm = this;
	vm.saving = false;
	vm.saved = false;
	vm.showSidebar = false;
	vm.range = common.range;
	vm.busy = true;

	SettingServices.getSettings('scheduleSettings')
		.then(([data]) => {
			if (!_.isNull(data)) {
				vm.scheduleSettings = angular.fromJson(data);
			}

			vm.busy = false;
		});

	vm.saveSettings = function () {
		if (vm.busy) return;

		vm.saving = true;

		SettingServices.saveSettings(vm.scheduleSettings, 'scheduleSettings')
			.then(() => {
				vm.saving = false;

				common.$timeout(() => vm.saved = false, HIDE_SAVED_TIMEOUT);

				vm.saved = true;
			});
	};
}
