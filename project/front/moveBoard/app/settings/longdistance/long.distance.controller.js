'use strict';

import './templates/longDistanceEditor.styl';
import TABS from './tabs-for-long-distance-controller.json';

angular
	.module('app.settings')
	.controller('LongdistanceContorller', LongdistanceContorller);

LongdistanceContorller.$inject = ['$scope', 'datacontext', 'common', '$uibModal', 'logger', 'SettingServices', 'LongDistanceServices'];

function LongdistanceContorller ($scope, datacontext, common, $uibModal, logger, SettingServices, LongDistanceServices) {
	const ONE_SECOND = 1000;
	const ZERO = 0;
	let vm = this;
	vm.longdistance = {};
	vm.saving = false;
	vm.areaCodes = LongDistanceServices.getAreas();
	vm.areaImage = LongDistanceServices.getAreaImages();
	vm.useDiscountTemplate = (baseRate, discounts) => {
		let discountsTemplate = vm.longdistance.stateRates[vm.longdistance.basedState][vm.stateCode].discounts_template;
		let discountsTemplateGlobal = vm.longdistance.discounts_template;
		if (!_.isEmpty(discountsTemplate) || !_.isEmpty(discountsTemplateGlobal)) {
			LongDistanceServices.useDiscountTemplate(baseRate, discounts, discountsTemplate, discountsTemplateGlobal,
				vm.saveSettings);
		}
	};
	vm.loading_map = false;
	vm.init = false;
	vm.deliveryList = {};
	vm.stateCodes = LongDistanceServices.getCodes();
	vm.searchState = '';
	vm.searchArea = '';
	vm.isSelected = '';

	_initSettings();

	vm.getStateId = function(value) {
		return Object.keys(vm.stateCodes).find(key => vm.stateCodes[key] === value);
	};
	vm.updateAreas = function() {
		for (var cs in vm.stateCodes) {
			var x = false;
			for (var fn in vm.stateCodes) {
				if( vm.longdistance.stateRates[cs] ) {
					if(vm.longdistance.stateRates[cs][fn.toLowerCase()]) {
						if(vm.longdistance.stateRates[cs][fn.toLowerCase()].longDistance) {
							x = true;
							continue;
						}
					}
				}
			}
			if (x === false) {
				vm.deliveryList[cs] = false;
			} else {
				vm.deliveryList[cs] = true;
			}
		}
	};

	vm.updateAreas();
	vm.showStateInfo = function(from, to, toName) {
		vm.longdistance.basedState = from;
		vm.stateCode = to;
		vm.state_name = toName;
		vm.showSidebar = true;
	};

	LongDistanceServices.setVM(vm);

	var mapElement = '';

	vm.focusinControl = {};

	vm.showSidebar = false;

	vm.saveRegSettings = function (field) {
		if (field && isValidChange(field)) {
			if (!_.isUndefined(field)
                && !_.isUndefined(vm.longdistance[field])) {

				if (field == 'floorextra') {
					vm.longdistance.floorextra_flatrate = ZERO;
				}

				if (field == 'floorextra_flatrate') {
					vm.longdistance.floorextra = ZERO;
				}

				if (field == 'floorextrafifth') {
					vm.longdistance.floorextrafifth_flatRate = ZERO;
				}

				if (field == 'floorextrafifth_flatRate') {
					vm.longdistance.floorextrafifth = ZERO;
				}

				if (field == 'extrafeetrate') {
					vm.longdistance.extrafeet_flatfee = ZERO;
				}

				if (field == 'extrafeet_flatfee') {
					vm.longdistance.extrafeetrate = ZERO;
				}
			}

			vm.saving = true;
			SettingServices.saveSettings(vm.longdistance, 'longdistance')
				.then(function () {
					vm.saving = false;
					common.$timeout(() => vm.saved = false, ONE_SECOND);
					vm.saved = true;
				});
		}
	};

	vm.searchCounty = function (id) {
		if (!vm.searchArea) return true;
		else {
			var numb = false;
			for (var i in vm.areaCodes[id]) {
				var str = vm.areaCodes[id][i].toString();
				if (str.includes(vm.searchArea)) numb = true;
			}
			return numb;
		}
	};

	vm.isSelectRelatedField = function (field) {
		var result = false;

		if (_.isUndefined(vm.longdistance[field])) {
			return result;
		}

		if (field == 'floorextra') {
			result = vm.longdistance.floorextra == ZERO
                && !_.isUndefined(vm.longdistance.floorextra_flatrate)
                && vm.longdistance.floorextra_flatrate != ZERO;
		}

		if (field == 'floorextra_flatrate') {
			result = vm.longdistance.floorextra_flatrate == ZERO
                && !_.isUndefined(vm.longdistance.floorextra)
                && vm.longdistance.floorextra != ZERO;
		}

		if (field == 'floorextrafifth') {
			result = vm.longdistance.floorextrafifth == ZERO
                && !_.isUndefined(vm.longdistance.floorextrafifth_flatRate)
                && vm.longdistance.floorextrafifth_flatRate != ZERO;
		}

		if (field == 'floorextrafifth_flatRate') {
			result = vm.longdistance.floorextrafifth_flatRate == ZERO
                && !_.isUndefined(vm.longdistance.floorextrafifth)
                && vm.longdistance.floorextrafifth != ZERO;
		}

		if (field == 'extrafeetrate') {
			result = vm.longdistance.extrafeetrate == ZERO
                && !_.isUndefined(vm.longdistance.extrafeet_flatfee)
                && vm.longdistance.extrafeet_flatfee != ZERO;
		}

		if (field == 'extrafeet_flatfee') {
			result = vm.longdistance.extrafeet_flatfee == ZERO
                && !_.isUndefined(vm.longdistance.extrafeetrate)
                && vm.longdistance.extrafeetrate != ZERO;
		}

		return result;
	};

	vm.tabs = angular.fromJson(TABS);

	vm.select = function (tab) {
		angular.forEach(vm.tabs, function (tab) {
			tab.selected = false;
		});

		tab.selected = true;
	};

	vm.initLDMap = function () {
		vm.loading_map = true;
		var mapElement = angular.element('#longDistanceMap');

		if (mapElement.length) {
			LongDistanceServices.initMap(mapElement, vm.longdistance.basedState);
		}
	};

	vm.saveSettings = function () {

		vm.saving = true;
		mapElement = angular.element('#longDistanceMap');

		if (vm.longdistance.stateRates[vm.longdistance.basedState]) {
			LongDistanceServices.setColor(mapElement, vm.stateCode);
		} else {
			LongDistanceServices.removeColor(mapElement, vm.stateCode);
		}

		SettingServices.saveSettings(vm.longdistance, 'longdistance').then(() => {
			vm.saving = false;

			common.$timeout(function () {
				vm.saved = false;
			}, ONE_SECOND);

			vm.saved = true;
		}).finally(() => {
			vm.updateAreas();
		});
	};

	$scope.tabs = [
		{name: 'Pending LD information', selected: true},
		{name: 'Not Confirmed LD information', selected: false},
		{name: 'Delivery Explanation', selected: false}
	];

	$scope.select = function (tab) {

		angular.forEach($scope.tabs, function (tab) {
			tab.selected = false;
		});
		tab.selected = true;
	};

	function isValidChange(parameter) {
		var result = true;
		var changedParameter = vm.longdistance[parameter];

		if (angular.isUndefined(changedParameter)
            || changedParameter == null) {

			result = false;
		}

		if (typeof changedParameter == 'string') {
			if (changedParameter.indexOf('.') == ZERO) {
				result = false;
			}

			var floatParameter = parseFloat(changedParameter);

			if (floatParameter < ZERO) {
				result = false;
			}
		}

		if (changedParameter < ZERO) {
			result = false;
		}

		return result;
	}
	vm.currentState = vm.stateCodes[vm.main_state];
	vm.currentStateId = vm.main_state;
	vm.statesOpen = function(state, code) {
		vm.currentState = state;
		vm.currentStateId = code;
	};

	vm.bufferState = {};
	vm.bufferState.delivery_days = 3;
	vm.bufferState.average_delivery_days = 2;
	vm.bufferState.min_weight = parseInt(vm.longdistance.extradelivery);
	vm.bufferState.state_rate = parseFloat(vm.longdistance.shuttleextra_cf);
	vm.copyState = function(state) {
		vm.bufferState = state;
	};
	vm.pasteState = function(from, to) {
		vm.longdistance.stateRates[from][to].longDistance = true;
		vm.longdistance.stateRates[from][to].delivery_days = vm.bufferState.delivery_days;
		vm.longdistance.stateRates[from][to].average_delivery_days = vm.bufferState.average_delivery_days;
		vm.longdistance.stateRates[from][to].min_weight = vm.bufferState.min_weight;
		vm.longdistance.stateRates[from][to].state_rate = vm.bufferState.state_rate;
		vm.saveSettings();
		vm.updateAreas();
	};
	vm.deleteState = function(from, to) {
		vm.longdistance.stateRates[from][to].longDistance = false;
		vm.saveSettings();
		vm.updateAreas();
	};
	vm.bufferBasedState = {};
	vm.bufferBasedState.code = vm.main_state;
	vm.bufferBasedState.name = vm.stateCodes[vm.main_state];
	vm.copyBasedState = function(name, code) {
		vm.bufferBasedState.code = code;
		vm.bufferBasedState.name = name;
	};
	vm.pasteBasedState = function(id) {
		for(var key in vm.longdistance.stateRates[id])
		{
			if(vm.longdistance.stateRates[vm.bufferBasedState.code][key] ) {
				if( vm.longdistance.stateRates[vm.bufferBasedState.code][key].longDistance &&
            key.toUpperCase() != vm.currentStateId) {
					vm.longdistance.stateRates[id][key] = angular.copy(vm.longdistance.stateRates[vm.bufferBasedState.code][key]);
				}
			}
		}

		if( vm.longdistance.stateRates[vm.bufferBasedState.code][id.toLowerCase()] ) {
			if( vm.longdistance.stateRates[vm.bufferBasedState.code][id.toLowerCase()].longDistance ) {
				vm.longdistance.stateRates[id][vm.bufferBasedState.code.toLowerCase()] = angular.copy(vm.longdistance.stateRates[vm.bufferBasedState.code][id.toLowerCase()]);
			}
		}
		vm.saveSettings();
		vm.updateAreas();
	};
	vm.isPrint = false;
	vm.printPage = function () {
		var printContents = document.getElementById('toPrint').innerHTML;
		var popupWin = window.open('', '_blank', 'width=900,height=800');
		popupWin.document.open();
		popupWin.document.write('<html><head><title>Price list for ' + vm.currentState + '</title><link href="https://fonts.googleapis.com/css?family=Montserrat:600" rel="stylesheet"><link rel="stylesheet" href="content/css/main.css"></head><body onload="window.print()">' + printContents + '</body></html>');
		popupWin.document.close();
	};
	vm.showDiscounts = {};
	vm.expandDiscounts = expandDiscounts;
	vm.hideDiscounts = hideDiscounts;
	function expandDiscounts(place) {
		vm.showDiscounts[place] = true;
	}

	function hideDiscounts(place) {
		vm.showDiscounts[place] = false;
	}

	function _initSettings() {
		vm.fieldData = datacontext.getFieldData();
		vm.basicsettings = angular.fromJson(vm.fieldData.basicsettings);
		vm.main_state = vm.basicsettings.main_state;
		vm.longdistance = angular.fromJson(vm.fieldData.longdistance);
		vm.longdistance.basedState = vm.main_state;
		vm.basicInfo = angular.fromJson(vm.fieldData.basicsettings);
	}
}
