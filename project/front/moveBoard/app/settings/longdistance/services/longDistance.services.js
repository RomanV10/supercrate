angular
	.module('app.settings')
	.factory('LongDistanceServices', LongDistanceServices);
/* @ngInject */
function LongDistanceServices($http, $q, datacontext, config) {

	var service = {};
	var areaCodes = [];
	var areaImage = [];
	var stateCodes = {
		'AL': 'Alabama',
		'AK': 'Alaska',
		'AZ': 'Arizona',
		'AR': 'Arkansas',
		'CA': 'California',
		'CO': 'Colorado',
		'CT': 'Connecticut',
		'DE': 'Delaware',
		'DC': 'District Of Columbia',
		'FL': 'Florida',
		'GA': 'Georgia',
		'HI': 'Hawaii',
		'ID': 'Idaho',
		'IL': 'Illinois',
		'IN': 'Indiana',
		'IA': 'Iowa',
		'KS': 'Kansas',
		'KY': 'Kentucky',
		'LA': 'Louisiana',
		'ME': 'Maine',
		'MD': 'Maryland',
		'MA': 'Massachusetts',
		'MI': 'Michigan',
		'MN': 'Minnesota',
		'MS': 'Mississippi',
		'MO': 'Missouri',
		'MT': 'Montana',
		'NE': 'Nebraska',
		'NV': 'Nevada',
		'NH': 'New Hampshire',
		'NJ': 'New Jersey',
		'NM': 'New Mexico',
		'NY': 'New York',
		'NC': 'North Carolina',
		'ND': 'North Dakota',
		'OH': 'Ohio',
		'OK': 'Oklahoma',
		'OR': 'Oregon',
		'PA': 'Pennsylvania',
		'RI': 'Rhode Island',
		'SC': 'South Carolina',
		'SD': 'South Dakota',
		'TN': 'Tennessee',
		'TX': 'Texas',
		'UT': 'Utah',
		'VT': 'Vermont',
		'VA': 'Virginia',
		'WA': 'Washington',
		'WV': 'West Virginia',
		'WI': 'Wisconsin',
		'WY': 'Wyoming',
	};

	areaCodes['Alabama'] = [205, 251, 256, 334, 938];
	areaImage['Alabama'] = 'alabama-area-code-map.png';

	areaCodes['Alaska'] = [907];
	areaImage['Alaska'] = 'alaska-area-code-map.png';

	areaCodes['Arizona'] = [480, 520, 602, 623, 928];
	areaImage['Arizona'] = 'arizona-area-code-map.png';

	areaCodes['Arkansas'] = [479, 501, 870];
	areaImage['Arkansas'] = 'arkansas-area-code-map.png';

	areaCodes['California'] = [
		209,
		213,
		310,
		323,
		408,
		415,
		424,
		442,
		510,
		530,
		559,
		562,
		619,
		626,
		650,
		657,
		661,
		669,
		707,
		714,
		747,
		760,
		805,
		818,
		831,
		858,
		909,
		916,
		925,
		949,
		951
	];
	areaImage['California'] = 'california-area-code-map.png';


	areaCodes['Colorado'] = [303, 719, 720, 970];
	areaImage['Colorado'] = 'colorado-area-code-map.png';

	areaCodes['Connecticut'] = [203, 475, 860];
	areaImage['Connecticut'] = 'connecticut-area-code-map.png';

	areaCodes['Delaware'] = [302];
	areaImage['Delaware'] = 'delaware-area-code-map.png';

	areaCodes['Florida'] = [239, 305, 321, 352, 386, 407, 561, 727, 754, 772, 786, 813, 850, 863, 904, 941, 954];
	areaImage['Florida'] = 'florida-area-code-map.png';

	areaCodes['Georgia'] = [229, 404, 470, 478, 678, 706, 762, 770, 912];
	areaImage['Georgia'] = 'georgia-area-code-map.png';

	areaCodes['Guam'] = [671];
	areaImage['Guam'] = '';
	areaCodes['Hawaii'] = [808];
	areaImage['Hawaii'] = '';
	areaCodes['Idaho'] = [208];
	areaImage['Idaho'] = '';

	areaCodes['Illinois'] = [217, 224, 309, 312, 331, 618, 630, 708, 773, 779, 815, 847, 872];
	areaImage['Illinois'] = 'illinois-area-code-map.png';


	areaCodes['Indiana'] = [219, 260, 317, 574, 765, 812];
	areaImage['Indiana'] = 'indiana-area-code-map.png';

	areaCodes['Iowa'] = [319, 515, 563, 641, 712];
	areaImage['Iowa'] = 'iowa-area-code-map.png';


	areaCodes['Kansas'] = [316, 620, 785, 913];
	areaImage['Kansas'] = 'kansas-area-code-map.png';

	areaCodes['Kentucky'] = [270, 502, 606, 859];
	areaImage['Kentucky'] = 'kentucky-area-code-map.png';

	areaCodes['Louisiana'] = [225, 318, 337, 504, 985];
	areaImage['Louisiana'] = 'louisiana-area-code-map.png';

	areaCodes['Maine'] = [207];
	areaImage['Maine'] = '';

	areaCodes['Maryland'] = [240, 301, 410, 443, 667];
	areaImage['Maryland'] = 'maryland-area-code-map.png';

	areaCodes['Massachusetts'] = [339, 351, 413, 508, 617, 774, 781, 857, 978];
	areaImage['Massachusetts'] = 'massachusetts-area-code-map.png';

	areaCodes['Michigan'] = [231, 248, 269, 313, 517, 586, 616, 734, 810, 906, 947, 989];
	areaImage['Michigan'] = 'michigan-area-code-map.png';

	areaCodes['Minnesota'] = [218, 320, 507, 612, 651, 763, 952];
	areaImage['Minnesota'] = 'minnesota-area-code-map.png';

	areaCodes['Mississippi'] = [228, 601, 662, 769];
	areaImage['Mississippi'] = 'mississippi-area-code-map.png';

	areaCodes['Missouri'] = [314, 417, 573, 636, 660, 816];
	areaImage['Missouri'] = 'missouri-area-code-map.png';

	areaCodes['Montana'] = [406];
	areaImage['Montana'] = '';

	areaCodes['Nebraska'] = [308, 402, 531];
	areaImage['Nebraska'] = 'nebraska-area-code-map.png';

	areaCodes['Nevada'] = [702, 725, 775];
	areaImage['Nevada'] = 'nevada-area-code-map.png';

	areaCodes['New Hampshire'] = [603];
	areaImage['New Hampshire'] = '';

	areaCodes['New Jersey'] = [201, 551, 609, 732, 848, 856, 862, 908, 973];
	areaImage['New Jersey'] = 'new-jersey-area-code-map.png';

	areaCodes['New Mexico'] = [505, 575];
	areaImage['New Mexico'] = 'new-mexico-area-code-map.png';

	areaCodes['New York'] = [212, 315, 347, 516, 518, 585, 607, 631, 646, 716, 718, 845, 914, 917, 929];
	areaImage['New York'] = 'new-york-area-code-map.png';

	areaCodes['North Carolina'] = [252, 336, 704, 828, 910, 919, 980, 984];
	areaImage['North Carolina'] = 'north-carolina-area-code-map.png';

	areaCodes['North Dakota'] = [701];
	areaImage['North Dakota'] = '';

	areaCodes['Northern Mariana Islands'] = [670];
	areaImage['Northern Mariana Islands'] = '';

	areaCodes['Ohio'] = [216, 234, 330, 419, 440, 513, 567, 614, 740, 937];
	areaImage['Ohio'] = 'ohio-area-code-map.png';

	areaCodes['Oklahoma'] = [405, 539, 580, 918];
	areaImage['Oklahoma'] = 'oklahoma-area-code-map.png';

	areaCodes['Oregon'] = [458, 503, 541, 971];
	areaImage['Oregon'] = 'oregon-area-code-map.png';


	areaCodes['Pennsylvania'] = [215, 267, 272, 412, 484, 570, 610, 717, 724, 814, 878];
	areaImage['Pennsylvania'] = 'pennsylvania-area-code-map.png';

	areaCodes['Rhode Island'] = [401];
	areaImage['Rhode Island'] = '';

	areaCodes['South Carolina'] = [803, 843, 864];
	areaImage['South Carolina'] = 'south-carolina-area-code-map.png';

	areaCodes['South Dakota'] = [605];
	areaImage['South Dakota'] = '';

	areaCodes['Tennessee'] = [423, 615, 731, 865, 901, 931];
	areaImage['Tennessee'] = 'tennessee-area-code-map.png';

	areaCodes['Texas'] = [
		210,
		214,
		254,
		281,
		325,
		346,
		361,
		409,
		430,
		432,
		469,
		512,
		682,
		713,
		737,
		806,
		817,
		830,
		832,
		903,
		915,
		936,
		940,
		956,
		972,
		979
	];
	areaImage['Texas'] = 'texas-area-code-map.png';

	areaCodes['Utah'] = [385, 435, 801];
	areaImage['Utah'] = 'utah-area-code-map.png';

	areaCodes['Vermont'] = [802];
	areaImage['Vermont'] = '';
	areaCodes['Virgin Islands'] = [340];
	areaImage['Virgin Islands'] = '';

	areaCodes['Virginia'] = [276, 434, 540, 571, 703, 757, 804];
	areaImage['Virginia'] = 'virginia-area-code-map.png';

	areaCodes['Washington'] = [206, 253, 360, 425, 509];
	areaImage['Washington'] = 'washington-area-code-map.png';

	areaCodes['Washington, DC'] = [202];
	areaImage['Washington, DC'] = '';

	areaCodes['West Virginia'] = [304, 681];
	areaImage['West Virginia'] = 'west-virginia-area-code-map.png';

	areaCodes['Wisconsin'] = [262, 414, 534, 608, 715, 920];
	areaImage['Wisconsin'] = 'wisconsin-area-code-map.png';

	areaCodes['Wyoming'] = [307];
	areaImage['Wyoming'] = '';

	var stateCodePositions = //paste JSON object instead of one below //
		{
			'HI': {
				'x': 248.59089969688506,
				'y': 513.8095035582893
			},
			'AK': {
				'x': 118.05637566236149,
				'y': 522.6618119424676
			},
			'FL': {
				'x': 754.9306750122784,
				'y': 496.13892259990513
			},
			'NH': {
				'x': 865.2730478036951,
				'y': 124.09058206605681
			},
			'MI': {
				'x': 651,
				'y': 184
			},
			'VT': {
				'x': 833.9196355758833,
				'y': 124.99134828031387
			},
			'ME': {
				'x': 889.7453136698773,
				'y': 92.12132730418989
			},
			'RI': {
				'x': 876.450606370494,
				'y': 170.12519641371904
			},
			'NY': {
				'x': 812.8376976678503,
				'y': 164.97002027104708
			},
			'PA': {
				'x': 782.6000970915588,
				'y': 208.7773102734416
			},
			'NJ': {
				'x': 836.6955141585858,
				'y': 214.69531214627045
			},
			'DE': {
				'x': 829.2500165701053,
				'y': 240.90766421299742
			},
			'MD': {
				'x': 804.8244018911337,
				'y': 244.9673926433175
			},
			'VA': {
				'x': 775.1021509847131,
				'y': 286.555516091326
			},
			'WV': {
				'x': 750.3190393092434,
				'y': 259.4085551904374
			},
			'OH': {
				'x': 703.7917685103017,
				'y': 234.96076975757396
			},
			'IN': {
				'x': 643.6855270960854,
				'y': 254.15542346885726
			},
			'IL': {
				'x': 595.3083465167095,
				'y': 254.00008046282022
			},
			'CT': {
				'x': 858.3242553203565,
				'y': 176.17366653258463
			},
			'WI': {
				'x': 566.329534216415,
				'y': 157.3713814815865
			},
			'NC': {
				'x': 773.8200358812254,
				'y': 329.488481876757
			},
			'DC': {
				'x': 805.2075000000001,
				'y': 248.575
			},
			'MA': {
				'x': 874.8704170137826,
				'y': 159.58860893333966
			},
			'TN': {
				'x': 658.753850678048,
				'y': 337.5023176125468
			},
			'AR': {
				'x': 539.5927335903888,
				'y': 371.0432323404463
			},
			'MO': {
				'x': 534.2280207156356,
				'y': 298.2507717934896
			},
			'GA': {
				'x': 716.7694135441192,
				'y': 402.0317482537276
			},
			'SC': {
				'x': 757.2122532921011,
				'y': 368.7438689420568
			},
			'KY': {
				'x': 662.3357461369104,
				'y': 303.1047097429061
			},
			'AL': {
				'x': 653.8716570194158,
				'y': 410.31362987047174
			},
			'LA': {
				'x': 542.1065032642587,
				'y': 454.07491711069906
			},
			'MS': {
				'x': 593.033562502455,
				'y': 415.64911787111754
			},
			'IA': {
				'x': 524.7178736562029,
				'y': 211.8512462972334
			},
			'MN': {
				'x': 515.6488439798509,
				'y': 117.8417070725155
			},
			'OK': {
				'x': 445.62066675369624,
				'y': 351.0785885587988
			},
			'TX': {
				'x': 407.84000000000003,
				'y': 436.49
			},
			'NM': {
				'x': 301.09569989543616,
				'y': 372.2718610378876
			},
			'KS': {
				'x': 443.76842889161657,
				'y': 291.3220488920981
			},
			'NE': {
				'x': 420.95076790265824,
				'y': 224.7863675044761
			},
			'SD': {
				'x': 419.9715443215115,
				'y': 161.03509523317177
			},
			'ND': {
				'x': 418.18941888386246,
				'y': 94.97928757916497
			},
			'WY': {
				'x': 301.21533637622565,
				'y': 181.2419355996737
			},
			'MT': {
				'x': 279.11041853595583,
				'y': 89.92494873289988
			},
			'CO': {
				'x': 323.7781705390495,
				'y': 272.3908477224708
			},
			'ID': {
				'x': 185.8607022809183,
				'y': 144.75558080203172
			},
			'UT': {
				'x': 224.1640265201877,
				'y': 250.5282904836319
			},
			'AZ': {
				'x': 204.2375537530424,
				'y': 360.2158498738335
			},
			'NV': {
				'x': 143.6346202549392,
				'y': 242.1119294845405
			},
			'OR': {
				'x': 93,
				'y': 127
			},
			'WA': {
				'x': 114.67910061353884,
				'y': 54.6995505992775
			},
			'CA': {
				'x': 69.9954332080572,
				'y': 277.7076074638756
			}
		};

	var highlighted_states = {};
	var vm = {};

	service.getAreas = getAreas;
	service.getAreaImages = getAreaImages;
	service.initMap = initMap;
	service.setColor = setColor;
	service.removeColor = removeColor;
	service.setVM = setVM;
	service.getCodes = getCodes;
	service.useDiscountTemplate = useDiscountTemplate;

	return service;

	function useDiscountTemplate(baseRate, discounts, discountsTemplate, discountsTemplateGlobal, saveSettings) {
		if (+baseRate > 0) {
			let map = _.isEmpty(discountsTemplate)
				? (discountsTemplateGlobal || [] )
				: discountsTemplate;
			discounts.length = 0;
			map.forEach(map => {
				discounts.push({
					startWeight: map.startWeight,
					rate: baseRate - map.rate
				});
			});
		} else {
			discounts.length = 0;
		}
		saveSettings();
	}

	function getAreas() {
		return areaCodes;
	}

	function getCodes() {
		return stateCodes;
	}

	function getAreaImages() {
		return areaImage;
	}

	function setVM(object) {
		vm = object;
	}

	function setColor(element, code) {
		highlighted_states[code] = '#C8EEFF';
		element.vectorMap('set', 'colors', highlighted_states);
	}

	function removeColor(element, code) {
		highlighted_states[code] = '#F4F3F0';
		element.vectorMap('set', 'colors', highlighted_states);
	}


	function initMap(element, basedState) {
		highlighted_states = {};

		angular.forEach(vm.longdistance.stateRates[basedState], function (state, st) {
			if (state !== null) {
				if (state.longDistance) {
					highlighted_states[st] = '#C8EEFF';
				}
			}
		});

		if (!vm.init) {
			element.vectorMap({
				map: 'usa_en',
				backgroundColor: null,
				enableZoom: true,
				showTooltip: true,
				scaleColors: ['#C8EEFF', '#006491'],
				normalizeFunction: 'polynomial',
				colors: highlighted_states,
				onRegionClick: function (event, code, region) {
					vm.showSidebar = true;
					vm.state_name = region;
					vm.stateCode = code;
				},
				onLoad: function (event, map) {
					vm.loading_map = false;
				}
			});

			var mapg = element.find('g');

			for (var i in stateCodePositions) {
				var textNode = document.createElementNS('http://www.w3.org/2000/svg', 'text');

				textNode.setAttribute('x', stateCodePositions[i].x);
				textNode.setAttribute('y', stateCodePositions[i].y);
				textNode.setAttribute('style', 'fill:#404040;stroke:#404040;border:solid 1px #fff;padding:2px 5px');
				textNode.textContent = i;
				mapg.append(textNode);
			}

			vm.init = true;
		} else {
			for (var i in stateCodePositions) {
				var st = i.toLowerCase();
				if (_.isUndefined(highlighted_states[st])) {
					highlighted_states[st] = '#F4F3F0';
				}
			}

			element.vectorMap('set', 'colors', highlighted_states);
			vm.loading_map = false;
		}
	}
}

