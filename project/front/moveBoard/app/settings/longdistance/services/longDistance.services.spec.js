describe('unit: longDistanceServices,', () => {
	let service;
	beforeEach(inject((_LongDistanceServices_) => {
		service = _LongDistanceServices_;
	}));
	describe('function useMap(),', () => {
		let baseRate, discounts, discountsTemplate, discountsTemplateGlobal, saveSettings;
		beforeEach(() => {

			discounts = [
				{
					startWeight: 500,
					rate: 0.5
				},
				{
					startWeight: 700,
					rate: 1
				},
				{
					startWeight: 900,
					rate: 3
				},
			];
			discountsTemplate = [
				{
					startWeight: 500,
					rate: 0.5
				},
				{
					startWeight: 700,
					rate: 1
				},
				{
					startWeight: 900,
					rate: 3
				},
			];
			discountsTemplateGlobal = [];
			baseRate = 10;

			saveSettings = {
				forCall: () => {}
			};
			spyOn(saveSettings, 'forCall');

			service.useDiscountTemplate(baseRate, discounts, discountsTemplate, discountsTemplateGlobal,
				saveSettings.forCall);
		});
		it('Shoud make new discounts by mask', () => {
			expect(discounts).toEqual(
				[
					{
						startWeight: 500,
						rate: 9.5
					},
					{
						startWeight: 700,
						rate: 9
					},
					{
						startWeight: 900,
						rate: 7
					},
				]
			);
		});
		it('Should call saveSettings()', () => {
			expect(saveSettings.forCall).toHaveBeenCalled();
		});
	});
});
