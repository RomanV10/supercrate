(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('settings.longdistance', {
                        url: '/longdistance',
	                    template: require('./templates/longDistancePage.html'),
                        title: 'Long Distance',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('settings.accountPageSettings', {
                        url: '/account-page-settings',
	                    template: require('../accountPage/account-page-settings.html'),
                        title: 'accountPageSettings',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });
            }
        ]
    );
})();
