'use strict';

angular
	.module('app.settings')
	.directive('longPrint', function () {
		return {
			scope: true,
			restrict: 'E',
			replace: true,
			template: require('../templates/longDistancePrint.html'),
		}
	});
