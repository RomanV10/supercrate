'use strict';
import './longDistanceDiscount.styl';

angular
	.module('app.settings')
	.directive('erLongDistanceDiscountsTemplate', function () {
		return {
			restrict: 'E',
			replace: true,
			template: require('./long-distance-discount.template.html'),
			scope: {
				discounts: '=',
				saveSettings: '&',
				buttonText: '@',
			},
			controller: 'longDistanceDiscountTemplateCtrl'
		};
	});
