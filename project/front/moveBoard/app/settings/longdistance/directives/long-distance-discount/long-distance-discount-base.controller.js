'use strict';

angular
	.module('app.settings')
	.controller('longDistanceDiscountBaseCtrl', longDistanceDiscountBaseCtrl);

longDistanceDiscountBaseCtrl.$inject = ['$scope'];

function longDistanceDiscountBaseCtrl($scope) {
	$scope.addDiscount = addDiscount;
	$scope.removeDiscount = removeDiscount;
	$scope.sortDiscount = sortDiscount;
	
	function addDiscount(index) {
		if (!$scope.discounts) {
			$scope.discounts = [];
		}
		
		let startWeight = 0;
		let rate = 0;
		let item = {startWeight, rate};
		
		if ($scope.discounts[index]) {
			item = angular.copy($scope.discounts[index]);
		}
		
		$scope.discounts.splice(index + 1, 0, item);
		
		$scope.saveSettings();
	}
	
	function removeDiscount(index) {
		$scope.discounts.splice(index, 1);
		$scope.saveSettings();
	}
	
	function sortDiscount() {
		$scope.discounts.sort(function (a, b) {
			return a.startWeight - b.startWeight;
		});
		$scope.saveSettings();
	}
}