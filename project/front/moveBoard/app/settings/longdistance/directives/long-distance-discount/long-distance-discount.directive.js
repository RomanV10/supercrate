'use strict';
import './longDistanceDiscount.styl';

angular
	.module('app.settings')
	.directive('erLongDistanceDiscounts', function () {
		return {
			restrict: 'E',
			replace: true,
			template: require('./long-distance-discount.template.html'),
			scope: {
				discounts: '=',
				saveSettings: '&',
				disabled: '=',
				discountsTemplate: '=',
				discountsTemplateGlobal: '=',
				baseRate: '='
			},
			controller: 'longDistanceDiscountCtrl'
		};
	});
