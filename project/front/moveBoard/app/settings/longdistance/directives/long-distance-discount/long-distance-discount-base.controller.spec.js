describe('controller: long-distance-discount-base.controller,', () => {
	let $controller, $scope;
	beforeEach(inject((_$controller_) => {
		$controller = _$controller_;
		$scope = $rootScope.$new();
		
		$scope.discounts = [
			{startWeight: 750, rate: 3},
			{startWeight: 950, rate: 2},
			{startWeight: 1050, rate: 1.5},
		];
		$scope.discountsTemplate = [
			{startWeight: 500, rate: 0.5},
			{startWeight: 700, rate: 1},
			{startWeight: 900, rate: 3},
		];
		$scope.baseRate = 10;
		$scope.saveSettings = () => {};
		spyOn($scope, 'saveSettings');
		$controller('longDistanceDiscountBaseCtrl', {$scope});
	}));
	
	describe('after Init', () => {
		it('$scope.addDiscount should be defined', () => {
			expect($scope.addDiscount).toBeDefined();
		});
		it('$scope.removeDiscount should be defined', () => {
			expect($scope.removeDiscount).toBeDefined();
		});
		it('$scope.sortDiscount should be defined', () => {
			expect($scope.sortDiscount).toBeDefined();
		});
	});
	describe('function addDiscount(),', () => {
		it('Should add zero discount if first row', () => {
			$scope.addDiscount(-1);
			expect($scope.discounts[0]).toEqual({startWeight: 0, rate: 0});
		});
		it('Should copy previous row if not first', () => {
			$scope.addDiscount(1);
			expect($scope.discounts[2]).toEqual({startWeight: 950, rate: 2});
		});
		it('Should call saveSettings()', () => {
			$scope.addDiscount(1);
			expect($scope.saveSettings).toHaveBeenCalled();
		});
	});
	
	describe('function removeDiscount(),', () => {
		it('Should remove discount', () => {
			$scope.removeDiscount(1);
			expect($scope.discounts).toEqual([
				{startWeight: 750, rate: 3},
				{startWeight: 1050, rate: 1.5},
			]);
		});
		it('Should call saveSettings()', () => {
			$scope.removeDiscount(1);
			expect($scope.saveSettings).toHaveBeenCalled();
		});
	});
	
	describe('function sortDiscount(),', () => {
		beforeEach(() => {
			$scope.discounts = [
				{startWeight: 700, rate: 1},
				{startWeight: 500, rate: 0.5},
				{startWeight: 900, rate: 3},
			];
		});
		it('Should sort discounts by weight', () => {
			$scope.sortDiscount();
			expect($scope.discounts).toEqual([
				{startWeight: 500, rate: 0.5},
				{startWeight: 700, rate: 1},
				{startWeight: 900, rate: 3},
			]);
		});
		it('Should call saveSettings()', () => {
			$scope.removeDiscount(1);
			expect($scope.saveSettings).toHaveBeenCalled();
		});
	});
});