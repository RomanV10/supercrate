'use strict';

angular
	.module('app.settings')
	.controller('longDistanceDiscountTemplateCtrl', longDistanceDiscountTemplateCtrl);

longDistanceDiscountTemplateCtrl.$inject = ['$scope', '$controller'];

function longDistanceDiscountTemplateCtrl($scope, $controller) {
	
	$controller('longDistanceDiscountBaseCtrl', {$scope});
	
	initLongDistanceDiscountMapCtrl();
	
	function initLongDistanceDiscountMapCtrl() {
		$scope.buttonText = $scope.buttonText || 'Set up discounts templates';
		$scope.rateText = 'Subtract $';
	}
}