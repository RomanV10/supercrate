'use strict';

angular
	.module('app.settings')
	.controller('longDistanceDiscountCtrl', longDistanceDiscountCtrl);

/* @ngInject */
function longDistanceDiscountCtrl($scope, $controller, LongDistanceServices) {
	$scope.showMapButton = true;
	$scope.useMap = useMap;

	$controller('longDistanceDiscountBaseCtrl', {$scope});

	initLongDistanceDiscountCtrl();

	function initLongDistanceDiscountCtrl() {
		$scope.buttonText = 'Set discount for larger shipments';
		$scope.rateText = 'Set price $';
		checkForExistDiscount();
		checkTemplates();
		$scope.$watch('discounts', checkForExistDiscount);
		$scope.$watch('discountsTemplate', checkTemplates, true);
		$scope.$watch('discountsTemplateGlobal', checkTemplates, true);
	}

	function checkForExistDiscount() {
		if (_.isUndefined($scope.discounts)) $scope.discounts = [];
	}

	function checkTemplates() {
		$scope.disableUseTemplate = _.isEmpty($scope.discountsTemplate) && _.isEmpty($scope.discountsTemplateGlobal);
	}

	function useMap() {
		LongDistanceServices.useDiscountTemplate($scope.baseRate, $scope.discounts, $scope.discountsTemplate, $scope.discountsTemplateGlobal, $scope.saveSettings);
	}
}
