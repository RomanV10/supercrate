describe('controller: long-distance-discount.controller,', () => {
	let $controller, $scope, LongDistanceServices;
	beforeEach(inject((_$controller_, _LongDistanceServices_) => {
		$controller = _$controller_;
		LongDistanceServices = _LongDistanceServices_;
		$scope = $rootScope.$new();

		$scope.discounts = [
			{startWeight: 500, rate: 0.5},
			{startWeight: 700, rate: 1},
			{startWeight: 900, rate: 3},
		];
		$scope.discountsTemplate = [
			{startWeight: 500, rate: 0.5},
			{startWeight: 700, rate: 1},
			{startWeight: 900, rate: 3},
		];
		$scope.baseRate = 10;
		$scope.saveSettings = () => {};
		spyOn($scope, 'saveSettings');
		$controller('longDistanceDiscountCtrl', {$scope});
	}));

	describe('after Init', () => {
		it('$scope.useMap should be defined', () => {
			expect($scope.useMap).toBeDefined();
		});
		it('Should set $scope.disableUseTemplate to falsy', () => {
			expect($scope.disableUseTemplate).toBeFalsy();
		});
		describe('When templates not setted', () => {
			it('Should set $scope.disableUseTemplate to true', () => {
				$scope.discountsTemplateGlobal = [];
				$scope.discountsTemplate = null;
				$controller('longDistanceDiscountCtrl', {$scope});
				expect($scope.disableUseTemplate).toBeTruthy();
			});
		})
	});

	describe('function useMap(),', () => {
		beforeEach(() => {
			spyOn(LongDistanceServices, 'useDiscountTemplate');
			$scope.useMap();
		});
		it('Shoud call LongDistanceServices.useDiscountTemplate', () => {
			expect(LongDistanceServices.useDiscountTemplate).toHaveBeenCalledWith($scope.baseRate, $scope.discounts, $scope.discountsTemplate, $scope.discountsTemplateGlobal, $scope.saveSettings);
		});
	});
});
