'use strict';

angular
	.module('app.settings')
	.directive('longNotice', function () {
		return {
			template: require('../templates/longDistanceNotice.html'),
		}
	});
