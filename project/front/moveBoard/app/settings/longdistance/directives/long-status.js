'use strict';

angular
	.module('app.settings')
	.directive('longStatus', longStatus);

longStatus.$inject = ['datacontext', 'SettingServices', '$timeout'];

function longStatus(datacontext, SettingServices, $timeout) {
	return {
		scope: true,
		restrict: 'E',
		replace: true,
		template: require('../templates/longDistanceStatus.html'),
		link: function ($scope) {
			$scope.fieldData = datacontext.getFieldData();
			$scope.longdistance = angular.fromJson($scope.fieldData.longdistance);
			$scope.longDisatanceStatusData = [];
			$scope.longDisatanceStatus = [];

			$scope.newFlag = {};
			if (!$scope.longdistance.ldStatus) {
				$scope.longdistance.ldStatus = [];
				let dataForUpdate = angular.toJson($scope.longdistance);
				SettingServices.setVariable(dataForUpdate, 'longdistance');
			}

			$scope.saveNewFlag = () => {
				$scope.saving = true;
				$scope.newFlag.id = $scope.longdistance.ldStatus.length + 1;
				$scope.longdistance.ldStatus.push(angular.copy($scope.newFlag));
				$scope.newFlag = {};
				let dataForUpdate = angular.toJson($scope.longdistance);

				SettingServices.setVariable(dataForUpdate, 'longdistance')
					.then(() => {
						$scope.saving = false;
						$scope.saved = true;
						$timeout(function () {
							$scope.saved = false;
						}, 1000);

					})
			};

			$scope.saveChangesFlag = () => {
				$scope.saving = true;
				let dataForUpdate = angular.toJson($scope.longdistance);
				SettingServices.setVariable(dataForUpdate, 'longdistance')
					.then(() => {
						$scope.saving = false;
						$scope.saved = true;
						$timeout(function () {
							$scope.saved = false;
						}, 1000);
					})
			}

			$scope.removeFlag = (index) => {
				$scope.saving = true;
				$scope.longdistance.ldStatus.splice(index, 1);
				let dataForUpdate = angular.toJson($scope.longdistance);
				SettingServices.setVariable(dataForUpdate, 'longdistance')
					.then(() => {
						$scope.saving = false;
						$scope.saved = true;
						$timeout(function () {
							$scope.saved = false;
						}, 1000);
					})
			};

		}
	}
}
