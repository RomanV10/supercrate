'use strict';

angular
	.module('app.settings')
	.directive('longList', function () {
		return {
			scope: true,
			restrict: 'E',
			replace: true,
			template: require('../templates/longDistanceList.html'),
		};
	});
