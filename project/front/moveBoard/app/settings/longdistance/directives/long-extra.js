'use strict';

angular
	.module('app.settings')
	.directive('longExtra', function () {
		return {
			scope: true,
			restrict: 'E',
			replace: true,
			template: require('../templates/longDistanceExtra.html')
		}
	});
