'use strict';
import './longdistance-driver-expenses.styl';
(function(){
	angular
		.module('app.settings')
		.directive('longDriverExpenses', longDriverExpenses);

	longDriverExpenses.$inject = ['datacontext', 'SettingServices', '$timeout'];

	function longDriverExpenses(datacontext, SettingServices, $timeout) {
		return {
			scope: true,
			restrict: 'E',
			replace: true,
			template: require('./longdistance-driver-expenses.pug'),
			link: linkFunction
		};

		function linkFunction($scope) {
			$scope.driverSettingsSelect = driverSettingsSelect;
			$scope.saveNewItem = saveNewItem;
			$scope.saveChanges = saveChanges;
			$scope.removeItem = removeItem;

			function init() {
				$scope.data = {
					addExpenses: false,
					addCash: false
				};
				$scope.driverSettingsTabs = [
					{
						name: 'Driver Expenses',
						selected: true
					},
					{
						name: 'Cash Advanced and Wires',
						selected: false
					},
				];
				$scope.fieldData = datacontext.getFieldData();
				$scope.longdistance = angular.fromJson($scope.fieldData.longdistance);
				$scope.newExpenses = {};
				$scope.newCash = {};

				if (!$scope.longdistance.ldDriver) {
					$scope.longdistance.ldDriver = {
						expenses: [],
						cash: []
					};

					let isVisibleSaving = false;
					saveSettings(isVisibleSaving);
				}
			}

			function driverSettingsSelect(tab) {
				angular.forEach($scope.driverSettingsTabs, function (tab) {
					tab.selected = false;
				});

				tab.selected = true;
			}

			function saveNewItem(type, data) {
				if (!data.amount || !data.description) {
					$scope.errorMessage = true;
					return;
				}

				$scope.errorMessage = false;
				$scope.data.saving = true;
				$scope.longdistance.ldDriver[type].push(angular.copy(data));
				$scope.newExpenses = {};
				$scope.newCash = {};

				let isVisibleSaving = true;
				saveSettings(isVisibleSaving);
				$scope.data.addCash = false;
				$scope.data.addExpenses = false;
			}

			function saveChanges() {
				$scope.data.saving = true;
				let isVisibleSaving = true;
				saveSettings(isVisibleSaving);
			}

			function removeItem(type, index) {
				$scope.data.saving = true;
				$scope.longdistance.ldDriver[type].splice(index, 1);
				let isVisibleSaving = true;
				saveSettings(isVisibleSaving);
			}

			function saveSettings(isVisibleSaving) {
				SettingServices.saveSettings($scope.longdistance, 'longdistance').then(data => {
					if (isVisibleSaving) {
						$scope.data.saving = false;
						$scope.data.saved = true;
						$timeout(function () {
							$scope.data.saved = false;
						}, 1000);
					}
				});
			}

			init();
		}
	}
})();
