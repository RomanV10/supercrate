'use strict';

angular
	.module('app.settings')
	.directive('longListStates', function ($timeout) {
		return {
			scope: true,
			restrict: 'E',
			replace: true,
			template: require('../templates/longDistanceListStates.html'),
			link: function (scope, element) {
				element.bind('click', function (e) {
					updateBlock()
				});
				element.bind('change', function (e) {
					updateBlock()
				});
				var updateBlock = function () {
					setTimeout(
						function () {
							angular.element('.list-block').masonry()
						},
						400
					);
				};
			}
		}
	});
