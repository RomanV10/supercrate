'use strict';

angular
	.module('app.settings')
	.directive('longEditor', function () {
		return {
			scope: true,
			restrict: 'E',
			replace: true,
			template: require('../templates/longDistanceEditor.html'),
		}
	});
