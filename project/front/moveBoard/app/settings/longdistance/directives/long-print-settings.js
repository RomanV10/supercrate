'use strict';

angular
	.module('app.settings')
	.directive('longPrintSettings', function () {
		return {
			template: require('../templates/longDistancePrintSettings.html'),
			controller: ['$scope', ($scope) => {
				$scope.printSettingsTabs =
					[
						{name: 'Loading Receipt Notes', selected: true}
					];

				$scope.printSettingsSelect = function (tab) {
					angular.forEach($scope.printSettingsTabs, function (tab) {
						tab.selected = false;
					});
					tab.selected = true;
				};
			}]
		};
	});
