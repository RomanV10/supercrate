'use strict';

angular
	.module('app.settings')
	.directive('longMap', function () {
		return {
			template: require('../templates/longDistanceMap.html'),
		}
	});
