(function () {

	angular
	.module('app.clients')
	.factory('ClientsServices', ClientsServices);

	ClientsServices.$inject = ['$http', '$rootScope', '$q', 'config', 'apiService', 'moveBoardApi', 'RequestServices'];

	function ClientsServices($http, $rootScope, $q, config, apiService, moveBoardApi, RequestServices) {

		var service = {};
		$rootScope.clients = {};
		service.getAllClients = getAllClients;
		service.saveClients = saveClients;
		service.getAllClientsStoragePages = getAllClientsStoragePages;
		service.getAllClientsStorage = getAllClientsStorage;
		service.deleteClient = deleteClient;
		service.prepareArray = prepareArray;
		service.getClient = getClient;
		service.findClient = findClient;
		service.updateAllClientRequest = updateAllClientRequest;
		service.changeUser = changeUser;
		service.setAdditionalUser = setAdditionalUser;
		service.removeAdditionalUser = removeAdditionalUser;
		service.switchAdditionalEmail = switchAdditionalEmail;
		service.checkUnique = checkUnique;
		service.updateClient = updateClient;
		service.createNewUser = createNewUser;
		service.buildNewClient = buildNewClient;
		service.getRequests = getRequests;
		return service;

		function createNewUser(nid, newData) {
			let defer = $q.defer();

			apiService.postData(moveBoardApi.client.createNewUser, {
				nid: nid,
				data: newData
			}).then(resolve => {
				if (_.isNull(resolve.data.nid) || _.isNull(resolve.data.user)) {
					defer.reject();
				} else {
					defer.resolve(resolve.data);
				}
			});

			return defer.promise;
		}

		function buildNewClient(client) {
			let newClient = {
				account: {
					fields: {
						field_user_first_name: client.data.data.field_user_first_name,
						field_user_last_name: client.data.data.field_user_last_name,
						field_user_additional_phone: client.data.data.field_user_additional_phone,
						field_primary_phone: client.data.data.field_primary_phone,
					},
					mail: client.data.data.mail,
					name: client.data.data.name,
					status: Number(client.data.data.status)
				}
			};

			return newClient;
		}

		function updateClient(uid, data) {
			let defer = $q.defer();
			apiService.putData(moveBoardApi.client.updateClient + uid, data).then(resolve => {
				if (!resolve.data.update) {
					defer.reject(resolve.data);
				} else {
					defer.resolve(resolve.data);
				}
			})

			return defer.promise;
		}

		function checkUnique(data) {
			let deferred = $q.defer();

			apiService.postData(moveBoardApi.validations.userUnique, data).then(resolve => {
				// if returns true => user is exist
				if (!!resolve.data[0]) {
					deferred.reject(resolve.data[0]);
				} else {
					deferred.resolve();
				}
			});

			return deferred.promise;
		}

		function getRequests(uid) {
			let defer = $q.defer();

			RequestServices.getRequestsByUid(uid).then(resolve => {
				defer.resolve(resolve.data);
			}, reject => {

			});

			return defer.promise;
		}


		function updateAllClientRequest(data) {
			let deferred = $q.defer();

			apiService.postData(moveBoardApi.client.changeClientAll, data).then(resolve => {
				if (!_.isUndefined(resolve.data.data)) {
					deferred.reject(resolve.data.data);
					return;
				}
				deferred.resolve(resolve.data);
			}, reject => {
				deferred.reject(reject.data);
			});

			return deferred.promise;
		}

		function changeUser(newUserData) {
			let defer = $q.defer();
			apiService.postData(moveBoardApi.client.changeClient, newUserData).then((resolve) => {
				defer.resolve(resolve);
			}, (reject) => {
				defer.reject(reject);
			});
			return defer.promise;
		}

		function findClient(condition) {
			var data = {};
			data.data = condition;
			var deferred = $q.defer();
			$http
			.post(config.serverUrl + 'server/clients/find_client', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
			return deferred.promise;

		}

		function getClient(uid) {
			var deferred = $q.defer();
			$http
			.get(config.serverUrl + 'server/clients/' + uid)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
			return deferred.promise;

		}

		function getAllClients(page) {
			var deferred = $q.defer();
			var clients = service.getAllClientsStoragePages(page);
			if (angular.isDefined(clients)) {
				deferred.resolve(clients);
			}

			$http
			.get(config.serverUrl + 'server/clients?pagesize=20&page=' + page)
			.success(function (data) {
				service.saveClients(page, data);
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
			return deferred.promise;
		}

		function deleteClient(uid) {
			var deferred = $q.defer();
			$http.delete(config.serverUrl + 'server/clients/' + uid)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
			return deferred.promise;
		}

		function prepareArray(client) {
			let data = {
				uid: client.uid,
				data: {
					data: {
						field_user_first_name: client.field_user_first_name,
						field_user_last_name: client.field_user_last_name,
						field_user_additional_phone: client.field_user_additional_phone,
						field_primary_phone: client.field_primary_phone,
						mail: client.mail.toLowerCase(),
						name: client.mail,
						status: client.status,
						pass: client.password
					}
				}
			};

			return data;
		}

		function saveClients(page, data) {
			if (angular.isUndefined($rootScope.clients[page])) {
				$rootScope.clients[page] = {};
			}
			$rootScope.clients[page] = data;
		}

		function getAllClientsStoragePages(page) {
			return $rootScope.clients[page];
		}

		function getAllClientsStorage() {
			return $rootScope.clients;
		}

		function setAdditionalUser(data) {
			return apiService.postData(moveBoardApi.client.setAdditionalUser, data);
		}

		function removeAdditionalUser(nid, data) {
			return apiService.putData(`${moveBoardApi.request.base}/${nid}`, data);
		}

		function switchAdditionalEmail(nid, data) {
			return apiService.putData(`${moveBoardApi.request.base}/${nid}`, data);
		}
	}
})();
