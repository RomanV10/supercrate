describe('ClentServices:', () => {
	var $rootScope, $scope, testHelper, client, newClient, ClientsServices;

	beforeEach(inject((_$rootScope_, _testHelperService_, _ClientsServices_) => {
		ClientsServices = _ClientsServices_;
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		testHelper = _testHelperService_;

		client = testHelper.loadJsonFile('client.mock');
		newClient = testHelper.loadJsonFile('build-new-client.mock');
	}));

	it('buildNewClient should be returns', () => {
		expect(ClientsServices.buildNewClient(client)).toEqual(newClient);
	})
});