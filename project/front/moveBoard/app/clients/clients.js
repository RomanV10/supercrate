(function () {
    'use strict';

    angular
        .module('app.clients')
        .controller('Clients', Clients);

    Clients.$inject = ['$location', '$routeParams','$uibModal', 'common', 'config','logger', 'ClientsServices'];

    function Clients($location, $routeParams,$uibModal, common, config,logger, ClientsServices) {
        /*jshint validthis: true */
        var vm = this;
        vm.previousPage = previousPage;
        vm.nextPage = nextPage;
        vm.userEditModal = userEditModal;
        vm.goToAccountPage = goToAccountPage;

        vm.title = 'Clients';
        vm.busy= true;
        vm.clients = {};
        vm.currentPage = 0;




        activate();

        function activate() {

             var promise = ClientsServices.getAllClients(vm.currentPage);

              promise.then(function(data) {

                  vm.busy = false;
                  vm.clients = data;

              });
        }

        vm.searchClient = function(email){

            var condition = {"email": email};
                   var promise = ClientsServices.findClient(condition);
            promise.then(function(data) {
                vm.busy = false;
                vm.clients = [];
                vm.clients[0] = data;
            });


        }


        function nextPage(){

            vm.currentPage += 1;
            vm.busy = true;
            var promise = ClientsServices.getAllClients(vm.currentPage);

              promise.then(function(data) {

                  vm.busy = false;
                  vm.clients = data;
              });

        }

        function previousPage(){

            vm.currentPage -= 1;
            vm.busy = true;
            var promise = ClientsServices.getAllClients(vm.currentPage);

              promise.then(function(data) {

                  vm.busy = false;
                  vm.clients = data;

              });

        }

        function goToAccountPage(client){


                $location.path("account/"+client.uid).replace();


        }

        function userEditModal(client){


            var modalInstance = $uibModal.open({
	            template: require('./client-edit-form.html'),
                controller: ModalInstanceCtrl,
                  resolve: {
                    client: function () {
                      return client;
                    }
                  },
            });

               modalInstance.result.then(function ($scope) {

                      if($scope.delete){

                          var row = $scope.client.uid;
                          delete(vm.clients[row]);

                      }


                });


        };





    }


    function ModalInstanceCtrl($scope, $uibModalInstance,client,ClientsServices,logger) {

        $scope.client = client;
        $scope.busy = false;
        $scope.delete = false;

        $scope.update = function(client){

             $scope.busy = true;
             var data = ClientsServices.prepareArray(client);
             var promise = ClientsServices.ClientsServices.prepareArray(data);

              promise.then(function(data) {

                  $scope.busy = true;
                  logger.success("Client info was updated", data, "Success");
                  $uibModalInstance.dismiss('cancel');

              }, function(reason) {

                  // If faild go to login page
                   logger.error(reason, reason, "Error");

                });

        }

        $scope.delete = function(client) {


             $scope.busy = true;
             var promise = ClientsServices.deleteClient(client.uid);

              promise.then(function(data) {

                  $scope.busy = false;
                  $scope.delete  = true;
                  logger.success("Client was deleted", data, "Success");

                  $uibModalInstance.close($scope);

              }, function(reason) {

                  // If faild go to login page
                   logger.error(reason, reason, "Error");

                });
        };


        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    };


})();
