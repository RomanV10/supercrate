import './multiple-request-user.styl';

'use strict';

angular
	.module('app.clients')
	.controller('updateMultipleRequestsUser', updateMultipleRequestsUser);

/* @ngInject */
function updateMultipleRequestsUser($scope, UpdateInfo, nid, $uibModalInstance, ClientsServices, requestCount) {
	$scope.loading = false;
	$scope.multiRequestsUser = requestCount > 1;

	$scope.cancelUpdate = cancelUpdate;
	$scope.updateAll = updateAll;
	$scope.updateThis = updateThis;

	function cancelUpdate() {
		$scope.loading = false;

		$uibModalInstance.close({
			busy: false
		});
	}

	function updateAll() {
		$scope.loading = true;

		ClientsServices.updateAllClientRequest(UpdateInfo).then(response => {
			closeInstance(response);
		});
	}

	function updateThis() {
		$scope.loading = true;
		let newClient = ClientsServices.buildNewClient(UpdateInfo);

		ClientsServices.createNewUser(nid, newClient).then(response => {
			closeInstance(response);
		}, () => {
			$scope.loading = false;
			toastr.error(`User didn't update`);
		});
	}

	function closeInstance(response) {
		$scope.loading = false;

		$uibModalInstance.close({
			busy: false,
			response: response,
		});
	}
}
