import './change-user.styl';

'use strict';

angular.module('app.clients').controller('changeUserController', changeUserController);

changeUserController.$inject = ['$scope', '$uibModalInstance', 'ClientsServices', 'nid', 'newUID'];

function changeUserController($scope, $uibModalInstance, ClientsServices, nid, newUID) {
	$scope.loading = false;

	$scope.cancelUpdate = cancelUpdate;
	$scope.changeAll = changeAll;

	function cancelUpdate() {
		$scope.loading = false;

		$uibModalInstance.close({
			busy: false
		});
	}

	function changeAll() {
		$scope.loading = true;

		ClientsServices.changeUser({
			uid: newUID,
			nid: nid
		}).then(() => {
			ClientsServices.getClient(newUID).then(updatedClientData => {
				let updatedClient = {
					field_first_name: updatedClientData.field_user_first_name,
					field_last_name: updatedClientData.field_user_last_name,
					field_e_mail: updatedClientData.mail,
					field_phone: updatedClientData.field_primary_phone,
					field_additional_phone: updatedClientData.field_user_additional_phone
				};
				$scope.loading = false;
				$uibModalInstance.close({
					busy: false,
					response: updatedClient,
				});
			});
		})
	}
}
