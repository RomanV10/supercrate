describe('Clinets client edit directive.', function () {
	let $compile;
	let	$rootScope;
	let $httpBackend;
	let moveBoardApi;
	let $scope;
	let testHelper;
	let element;
	let scope;

	beforeEach(inject(function(_$httpBackend_, _$compile_, _$rootScope_, _moveBoardApi_, _testHelperService_) {
		$httpBackend = _$httpBackend_;
		$compile = _$compile_;
		$rootScope = _$rootScope_;
		moveBoardApi = _moveBoardApi_;
		testHelper = _testHelperService_;

		$scope = $rootScope.$new();

		$scope.request = testHelper.loadJsonFile('client-edit-request.mock');

		element = angular.element(`<div client-edit-form request="request"></div>`);
		$compile(element)($scope);
		scope = element.isolateScope();
	}));

	describe('Directive DOM', () => {
		it('should not render empty html', function () {
			expect(element.html()).not.toBe('');
		});

		it('should not empty busy image', () => {
			let image = element.find('img.busy-symbol');
			expect(image.attr('src')).not.toBe('');
		})
	});

	describe('saveAddContact', () => {

		it('saveAddContact method have been called', () => {
			spyOn(scope, 'saveAddContact');

			scope.addContact = true;
			scope.hasAdditionalUser = false;
			element.find('[ng-click="saveAddContact()"]').trigger('click');

			expect(scope.saveAddContact).toHaveBeenCalled();
		});
	});

	describe('updateAddContact', () => {

		it('updateAddContact method have been called', () => {
			spyOn(scope, 'updateAddContact');

			scope.hasAdditionalUser = true;
			scope.addContact = false;
			element.find('[ng-click="updateAddContact()"]').trigger('click');

			expect(scope.updateAddContact).toHaveBeenCalled();
		});
	});
});
