import './client-edit-form.styl';

'use strict';

angular
	.module('app.clients')
	.directive('clientEditForm', clientEditForm);

clientEditForm.$inject = ['$rootScope', 'logger', 'ClientsServices', 'SweetAlert', 'RequestServices', '$uibModal', 'PermissionsServices'];

function clientEditForm($rootScope, logger, ClientsServices, SweetAlert, RequestServices, $uibModal, PermissionsServices) {
	return {
		scope: {
			'request': '=request',
		},
		link: clientLink,
		replace: true,
		template: require('./client-edit-form.html'),
		restrict: 'A'
	};

	function clientLink($scope) {
		const DRAFT_NAME = 'Draft User';
		const EMAIL_MASK = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		$scope.client = angular.copy($scope.request.uid);
		$scope.isSuperUser = PermissionsServices.isSuper();
		let isChangeMail = false;
		let isMultiRequestUser = false;
		let currentName = `${$scope.client.field_user_first_name} ${$scope.client.field_user_last_name}`;
		let isDraftEmail = !!$scope.client.mail.match(/(\d+)@draft.www/);
		let isChangeEmailFromDraft = false;

		$scope.isDraft = _.isEqual(currentName, DRAFT_NAME) || isDraftEmail;
		$scope.clientOld = _.clone($scope.request.uid);
		$scope.logMsg = [];
		$scope.busy = false;
		$scope.delete = false;
		$scope.addContact = false;
		$scope.hasAdditionalUser = !_.isEmpty($scope.request.field_additional_user);
		$scope.twinAddContact = angular.copy($scope.request.field_additional_user);

		$scope.isCannotEditUserField = _.isUndefined($scope.client.roles) || !_.isEqual($scope.client.roles.indexOf('sales'), -1) || !_.isEqual($scope.client.roles.indexOf('administrator'), -1) || !_.isEqual($scope.client.roles.indexOf('manager'), -1);

		_initClientRequests();

		$scope.checkValidClientField = checkValidClientField;
		$scope.isOnlyInvalidClientField = isOnlyInvalidClientField;
		$scope.isRequiredRequestField = isRequiredRequestField;
		$scope.validateDraftFields = validateDraftFields;
		$scope.removeAddContact = removeAddContact;
		$scope.saveAddContact = saveAddContact;
		$scope.updateAddContact = updateAddContact;
		$scope.cancelAddContact = cancelAddContact;
		$scope.switchAdditionalEmail = switchAdditionalEmail;

		function _initClientRequests() {
			validateDraftFields($scope.client.mail, 'mail');
			validateDraftFields($scope.client.field_user_last_name, 'field_user_last_name');
			validateDraftFields($scope.client.field_user_first_name, 'field_user_first_name');

			$scope.$on('allowedNodes', function (ev, count) {
				isMultiRequestUser = count > 1;
			});
		}

		function validateDraftFields(value, field) {
			switch (field) {
			case 'field_user_first_name':
				$scope.draftFirstNameValid = _.isEqual(value, 'Draft');
				break;
			case 'field_user_last_name':
				$scope.draftLastNameValid = _.isEqual(value, 'User');
				break;
			case 'mail':
				// Check by mask
				if (!EMAIL_MASK.test(value)) return;
				$scope.draftMailValid = !!value.match(/(\d+)@draft.www/);
				break;
			}

			if (!$scope.draftFirstNameValid && !$scope.draftLastNameValid && !$scope.draftMailValid) {
				$scope.isDraft = false;
			}

			isChangeEmailFromDraft = isDraftEmail && !$scope.draftMailValid;
		}

		$scope.update = function (client) {
			$scope.submitted = true;

			if (!$scope.client.mail) {
				toastr.error('Client email is required', 'Error');
				return;
			}

			if (!$scope.client.field_primary_phone) {
				toastr.error('Client phone is required', 'Error');
				return;
			}

			$scope.busy = true;
			var newClientInfo = ClientsServices.prepareArray(client);
			var editrequest = {
				field_first_name: newClientInfo.data.data.field_user_first_name,
				field_last_name: newClientInfo.data.data.field_user_last_name,
				field_e_mail: newClientInfo.data.data.mail.toLowerCase(),
				field_phone: newClientInfo.data.data.field_primary_phone,
				field_additional_phone: newClientInfo.data.data.field_user_additional_phone
			};

			if (isChangeEmailFromDraft) {
				newClientInfo.data.data.nid = $scope.request.nid;
			}

			if (isMultiRequestUser && isChangeMail) {
				ClientsServices.checkUnique({
					email: editrequest.field_e_mail
				}).then(() => {
					updateClientViaController(newClientInfo, editrequest, isMultiRequestUser);
				}, reject => {
					changeClientViaController(reject, editrequest);
				});
			} else {
				updateAllRequests(newClientInfo, editrequest);
			}
		};

		function updateClientViaController(newClientInfo, editrequest, requestCount) {
			let updatePromise = $uibModal.open({
				template: require('../controllers/multiple-request-user.tpl.pug'),
				controller: 'updateMultipleRequestsUser',
				size: 'md',
				backdrop: false,
				resolve: {
					UpdateInfo: function () {
						return newClientInfo;
					}, requestCount: function () {
						return requestCount;
					}, nid: function () {
						return $scope.request.nid;
					}
				}
			});

			updatePromise.result.then(result => {
				$scope.busy = false;
				if (result.response) {
					$scope.clientOld = angular.copy($scope.client);
					updateUser(editrequest, $scope.clientOld.uid);
				}
			});
		}

		function changeClientViaController(newUID, editrequest) {
			let changePromise = $uibModal.open({
				template: require('../controllers/change-user.tpl.pug'),
				controller: 'changeUserController',
				size: 'md',
				backdrop: false,
				resolve: {
					newUID: function () {
						return newUID;
					}, nid: function () {
						return $scope.request.nid;
					}
				}
			});

			changePromise.result.then(resolve => {
				$scope.busy = false;
				if (resolve.response) {
					$scope.clientOld = angular.copy($scope.client);
					editrequest = resolve.response;
					updateUser(editrequest, $scope.clientOld.uid);
				}
			});
		}

		function updateAllRequests(UpdateInfo, editrequest) {
			ClientsServices.updateAllClientRequest(UpdateInfo).then(response => {
				$scope.clientOld = angular.copy($scope.client);
				$scope.busy = false;
				updateUser(editrequest, $scope.clientOld.uid);
			}, reject => {
				$scope.busy = false;
				changeClientViaController(reject, editrequest);
			});
		}

		$scope.delete = function (client) {
			SweetAlert.swal({
				title: 'Are you sure you want to delete this user?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes, let\'s do it!',
				cancelButtonText: 'No, cancel pls!',
				closeOnConfirm: true,
				closeOnCancel: true
			}, function (isConfirm) {
				if (isConfirm) {
					$scope.busy = true;
					var promise = ClientsServices.deleteClient(client.uid);

					promise.then(function (data) {
						var msg = {
							simpleText: 'Client ' + $scope.clientOld.field_user_first_name + $scope.clientOld.field_user_last_name + ' was deleted'
						};
						$scope.logMsg.push(msg);
						RequestServices.sendLogs($scope.logMsg, 'Client information was changed', $scope.request.nid, 'MOVEREQUEST');
						$scope.logMsg = [];
						$scope.logMsg.push(msg);
						$scope.busy = false;
						$scope.delete = true;
						logger.success('Client was deleted', data, 'Success');

						$rootScope.$broadcast('onRequestUserDelete');
					}, function (reason) {

						// If faild go to login page
						logger.error(reason, reason, 'Error');
					});
				}
			});
		};

		$scope.edit = function (title, field, value) {
			let msg = {
				text: `${title} was changed`
			};
			switch (title) {
			case 'Password':
				msg = {
					simpleText: `${title} was changed`
				};
				break;
			case 'Status':
			case 'Send additional email':
				angular.extend(msg, {
					to: value ? 'Active' : 'Inactive', from: value ? 'Inactive' : 'Active'
				});
				break;
			default:
				if (value == $scope.clientOld[field]) break;
				angular.extend(msg, {
					to: value, from: $scope.clientOld[field]
				});
				break;
			}
			$scope.logMsg.push(msg);
		};

		function cancelAddContact() {
			$scope.addContact = false;
			$scope.hasAdditionalUser = false;

			for (let key in $scope.request.field_additional_user) {
				$scope.request.field_additional_user[key] = null;
			}

			$scope.twinAddContact = {};
		}

		function saveAddContact() {
			if (!$scope.contacts.$valid) {
				toastr.warning('Please fill required fields', 'Error');
			} else {
				$scope.busy = true;
				ClientsServices.setAdditionalUser({
					nid: $scope.request.nid, account: {
						fields: {
							field_primary_phone: $scope.request.field_additional_user.phone,
							field_user_first_name: $scope.request.field_additional_user.first_name,
							field_user_last_name: $scope.request.field_additional_user.last_name
						}, mail: $scope.request.field_additional_user.mail, name: $scope.request.field_additional_user.mail,
					}
				})
					.then(({data: {uid}}) => {
						$scope.request.field_additional_user.uid = uid;
						$scope.addContact = false;
						$scope.hasAdditionalUser = true;

						return RequestServices.sendLogs(getbaseAddClientLog(), 'Additional contact of client was added', $scope.request.nid, 'MOVEREQUEST');
					})
					.then(() => {
						$scope.twinAddContact = $scope.request.field_additional_user;
						toastr.success('Client was updated', 'Success');
					})
					.catch(() => {
						toastr.warning('The additional email already exists in the system', 'Error')
					})
					.finally(() => {
						$scope.busy = false;
					});
			}
		};

		function updateAddContact() {
			$scope.busy = true;
			ClientsServices.updateAllClientRequest({
				data: {
					data: {
						field_primary_phone: $scope.request.field_additional_user.phone,
						field_user_additional_phone: '',
						field_user_first_name: $scope.request.field_additional_user.first_name,
						field_user_last_name: $scope.request.field_additional_user.last_name,
						mail: $scope.request.field_additional_user.mail,
						name: $scope.request.field_additional_user.mail,
						status: true
					}
				}, uid: $scope.request.field_additional_user.uid
			})
				.then(() => {
					return RequestServices.sendLogs([{
						simpleText: $scope.request.field_additional_user.first_name != $scope.twinAddContact.first_name ? 'First name change from ' + $scope.twinAddContact.first_name + ' to ' + $scope.request.field_additional_user.first_name : ''
					}, {
						simpleText: $scope.request.field_additional_user.last_name != $scope.twinAddContact.last_name ? 'Last name change from ' + $scope.twinAddContact.last_name + ' to ' + $scope.request.field_additional_user.last_name : ''
					}, {
						simpleText: $scope.request.field_additional_user.phone != $scope.twinAddContact.phone ? 'Phone change from ' + $scope.twinAddContact.phone + ' to ' + $scope.request.field_additional_user.phone : ''
					}, {
						simpleText: $scope.request.field_additional_user.mail != $scope.twinAddContact.mail ? 'Mail change from ' + $scope.twinAddContact.mail + ' to ' + $scope.request.field_additional_user.mail : ''
					}], 'Additional contact of client was updated', $scope.request.nid, 'MOVEREQUEST');
				})
				.then(() => {
					$scope.twinAddContact = $scope.request.field_additional_user;
					toastr.success('Client was updated', 'Success');
				})
				.catch(() => toastr.warning('Client was not updated', 'Error'))
				.finally(() => $scope.busy = false);
		}

		function removeAddContact() {

			SweetAlert.swal({
				title: 'Are you sure you want delete contacts?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes, let\'s do it!',
				cancelButtonText: 'No, cancel please!',
				closeOnConfirm: true,
				closeOnCancel: true
			}, (isConfirm) => {
				if (isConfirm) {
					$scope.busy = true;
					ClientsServices.removeAdditionalUser($scope.request.nid, {
						data: {
							field_additional_user: {}
						}
					})
						.then(() => switchAdditionalEmail())
						.then(() => {
							return RequestServices.sendLogs(getbaseAddClientLog(), 'Additional contact of client was removed', $scope.request.nid, 'MOVEREQUEST');
						})
						.then(() => {
							$scope.cancelAddContact();
							toastr.success('Client was removed', 'Success');
						})
						.catch(() => toastr.warning('Client was not removed', 'Error'))
						.finally(() => $scope.busy = false);
				}
			});
		};

		function updateUser(editrequest, uid) {
			logger.success('Client info was updated', $scope.request.nid, 'Success');

			if (!_.isEmpty($scope.logMsg)) {
				RequestServices.sendLogs($scope.logMsg, 'Client information was changed', $scope.request.nid, 'MOVEREQUEST');
			}
			if (editrequest) {
				$rootScope.$broadcast('client_info.updated', {
					uid: uid,
					mail: editrequest.field_e_mail,
					field_primary_phone: editrequest.field_phone,
					field_user_first_name: editrequest.field_first_name,
					field_user_last_name: editrequest.field_last_name
				});
				if ($scope.request.move_size.raw == 11) {
					editrequest.field_commercial_company_name = $scope.request.field_commercial_company_name.value;
				}
				RequestServices.updateRequest($scope.request.nid, editrequest);
			}
			$scope.logMsg = [];
			$scope.request.uid = angular.copy($scope.client);
			isChangeMail = false;
			$rootScope.$broadcast('reloadUserRequests');
		}

		function checkValidClientField(submitted, field) {
			return submitted && (field.$invalid || field.$error.required);
		}

		function isOnlyInvalidClientField(submitted, field) {
			return submitted && field.$invalid && !field.$error.required;
		}

		function isRequiredRequestField(submitted, field) {
			return submitted && field.$error.required;
		}

		function switchAdditionalEmail() {
			$scope.busy = true;
			$scope.edit('Send additional email', 'send additional email', $scope.request.send_additional_email);
			ClientsServices.switchAdditionalEmail($scope.request.nid, {
				data: {
					field_send_additional_email: $scope.request.send_additional_email
				}
			})
				.then(() => toastr.success('Done ', 'Success'))
				.catch(() => toastr.warning('Error', 'Error'))
				.finally(() => $scope.busy = false);
		}

		function prepareClientForDestroy(client) {
			let result = angular.copy(client);

			result.status = !!result.status;
			result.field_primary_phone = result.field_primary_phone.replace(/\D/g, '');

			return result;
		}

		function getbaseAddClientLog() {
			return [{
				simpleText: 'First name: ' + $scope.request.field_additional_user.first_name
			}, {
				simpleText: 'Last name: ' + $scope.request.field_additional_user.last_name
			}, {
				simpleText: 'Phone: ' + $scope.request.field_additional_user.phone
			}, {
				simpleText: 'Mail: ' + $scope.request.field_additional_user.mail
			}];
		}

		$scope.$watch('client.mail', function (current, origin) {
			if (current != origin) {
				isChangeMail = true;
			}

		});

		$scope.$on('$destroy', function () {
			let current = prepareClientForDestroy($scope.client);
			let original = prepareClientForDestroy($scope.clientOld);

			let hasChanges = !_.isEqual(current, original);
			if (hasChanges) {
				SweetAlert.swal({
					title: `You have unsaved changes`,
					text: 'Do you want to save them?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Yes, let\'s do it!',
					cancelButtonText: 'No, cancel pls!',
					closeOnConfirm: true,
					closeOnCancel: true
				}, function (isConfirmed) {
					if (isConfirmed) {
						$scope.update($scope.client);
					}
				});
			}
		});
	}
}
