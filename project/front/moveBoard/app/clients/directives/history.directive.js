'use strict';

import './history.styl';

angular
	.module('app.clients')
	.directive('requestsHistory', requestsHistory);


requestsHistory.$inject = ['RequestServices', '$rootScope'];

function requestsHistory (RequestServices, $rootScope) {
	return {
		scope : {
			'request': '=request',
		},
		controller: historyController,
		template: require('./history.template.html'),
	};


	function historyController($scope){
		function _init() {
			let promise = RequestServices.getRequestsByUid($scope.request.uid.uid);
			$scope.past_requests = [];
			$scope.current_requests = [];
			promise.then(function (data) {
				$rootScope.$broadcast('allowedNodes', data.count);
				angular.forEach(data.nodes,function(request){
					if(request.nid != $scope.request.nid){
						$scope.current_requests.push(request);
					}

				});

			});
		}

		_init();

		$scope.$on('reloadUserRequests', _init);
	}
}
