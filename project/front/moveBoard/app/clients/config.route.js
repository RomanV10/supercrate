(function(){
    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('settings.clients', {
                        url: '/clients',
	                    template: require('./clients.html'),
                        title: 'Clients',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('client', {
                        url: '/client/:id',
	                    template: require('./clientsdetails.html'),
                        title: 'Client Details',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });
            }
        ]
    );
})();
