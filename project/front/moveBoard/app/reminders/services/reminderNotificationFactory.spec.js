describe('reminder notification factory', function () {
    let service,
        originalTimeout;

    beforeEach(inject(function (_ReminderNotification_) {
        service = _ReminderNotification_;
    }));

    it('reminder notification factory should be injected and defined', function () {
        expect(service).toBeDefined();
    });

    it('reminder notification factory registration test', function() {
        let reminder = {
            id: 143,
            user_id: 1,
            subject: 'Order #4374 | anton test',
            description: 'for unit test',
            color: '13c713',
            starts_at: (Date.now() / 1000 | 0) + 5,
            ends_at: 1510777500,
            remind_for: 5,
            remind_at: null,
            dismissed_at: null,
            created_at: 1510748760,
            updated_at: null,
            snoozed_for: null,
            node_id: 4374,
            customer_name: 'anton test'
        };

        let reminderNotification = new service(reminder);

        expect(reminderNotification.remindTimeout).toBeDefined();

        reminderNotification.remindTimeout.then(() => {
            expect(Date.now() / 1000 | 0).toEqual(reminder.starts_at);
        });

    });
});
