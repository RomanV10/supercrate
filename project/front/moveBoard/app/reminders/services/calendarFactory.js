'use strict';

angular
    .module('reminders')
    .factory('Month', ($filter, moment) => {
        class Month extends Array {
            constructor(reminders) {
                super();
                let month = [];
                angular.forEach(reminders, (reminder) => {
                    let number = Number(moment(reminder.starts_at * 1000).format('D'));
                    let day = _.find((month), m => m.number == number);
                    if(day){
                        day.reminders.push(reminder);
                    } else {
                        month.push({
                            number: number,
                            name: moment(reminder.starts_at * 1000).format('dddd'),
                            reminders: [reminder]
                        })
                    }
                })
                angular.extend(this, $filter('orderBy')(month, 'number'));
            }
        }
        return Month;
    });
