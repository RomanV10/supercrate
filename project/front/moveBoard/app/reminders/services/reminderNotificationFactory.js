'use strict';

angular
    .module('reminders')
    .factory('ReminderNotification', ($window, $timeout, dialogs, Sound, $injector, moment) => {

        let notFocus;
        const sound = new Sound('notification');
        
        $window.onfocus = () => notFocus = false;
        $window.onblur = () => notFocus = true;

        function getOffsetTime(time, remindTime) {
            return Math.abs((time - remindTime) * 1000);
        }

        function NotificationService() {
            return $injector.get('NotificationService');
        }

        class ReminderNotification {
            constructor(reminder) {
                this.id = reminder.id;
                this.remindTimeout = this.registerNotification(reminder);
            }

            registerNotification(reminder) {
                let date = moment().unix();
                let offsetTime = getOffsetTime(date, reminder.remindTime);

                return $timeout(async () => {
                    dialogs.notificationBox(reminder);
                    if(Sound.notificationSound){
                        try {
	                        await sound.play();
                        } catch (e) {
                            console.log(e);
                        }
                    }
                    if(notFocus){
                        let startAlert = moment().unix();
                        alert('Reminder');
                        let endAlert = moment().unix();
                        NotificationService().recalcNotifications(startAlert, endAlert);
                    }

                    date = moment().unix();

                    if(date < reminder.remindTime){
                        this.remindTimeout = this.registerNotification(reminder);
                    } else {
                        NotificationService().cancelNotification(this.id);
                    }
                }, offsetTime);
            }
        
            cancel() {
                $timeout.cancel(this.remindTimeout);
            }
        }

        return ReminderNotification;
    });
