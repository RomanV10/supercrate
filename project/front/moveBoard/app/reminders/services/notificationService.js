'use strict';

class NotificationService {
    constructor($filter, dialogs, Reminder, ReminderNotification, moment, RemindersSocket) {
        this.$filter = $filter;
        this.dialogs = dialogs;
        this.Reminder = Reminder;
        this.ReminderNotification = ReminderNotification;
        this.moment = moment;
        this.RemindersSocket = RemindersSocket;

        this.notifications = [];

        this.endDay = moment().endOf('day').unix();
        this.startDay = moment().startOf('day').unix();

        this.condition = {
            from: moment().subtract(1, 'months').startOf('month').unix(),
            to: this.endDay
        }

        this.started = false;
    }

    async run() {
        this.started = true;
        let reminders = await this.Reminder.getByUser(this.condition);

        let upcoming = this.$filter('upcoming')(reminders, true);
        let expired  = _.filter(this.$filter('upcoming')(reminders, false), reminder => reminder.dismissed_at == null);
        
        angular.forEach((upcoming), reminder => 
            this.setNotification(reminder));

        if(expired.length){
            this.dialogs.reminderList({collection: expired});
        }

        this.connection = new this.RemindersSocket();

        this.connection.subscribe(({data}) => 
            this.setNotification(new this.Reminder(data)));
    }

    async recalcNotifications(startAlert, endAlert) {
        this.condition.from = this.moment().unix();

        let reminders = await this.Reminder.getByUser(this.condition);
        let upcoming = this.$filter('upcoming')(reminders, true);

        let missedCauseAlert = _.filter(reminders, reminder =>
                startAlert < reminder.starts_at && endAlert > reminder.starts_at ||
                startAlert < reminder.remind_at && endAlert > reminder.remind_at);

        angular.forEach((missedCauseAlert), reminder => 
            this.cancelNotification(reminder.id));

        angular.forEach((upcoming), reminder => 
            this.setNotification(reminder));

        if (missedCauseAlert.length) {
            this.dialogs.reminderList({collection: missedCauseAlert});
        }
    }

    cancelNotification(id) {
        let notification = _.remove(this.notifications, (notification) => notification.id == id)[0];
            
        if (notification) {
            notification.cancel();
        }
    }

    setNotification(reminder) {
        let notification = _.find(this.notifications, (notification) => notification.id === reminder.id);

        if (this.startDay < reminder.starts_at && this.endDay > reminder.starts_at ||
            this.startDay < reminder.remind_at && this.endDay > reminder.remind_at) {
                
            if (notification) {
                notification.cancel();
                notification = new this.ReminderNotification(reminder);                
            } else {
                this.notifications.push(new this.ReminderNotification(reminder));
            }
        } else {
            if (notification) {
                this.cancelNotification(notification.id);
            }
        }
    }
}

angular
    .module('reminders')
    .service('NotificationService', NotificationService);

NotificationService.$inject = ['$filter', 'dialogs', 'Reminder', 'ReminderNotification', 'moment', 'RemindersSocket'];
