describe('notification service', function () {
    let service,
        $httpBackend;


    beforeEach(inject(function (_NotificationService_, _$rootScope_) {
        service = _NotificationService_;
    }));

    it('notification service should be injected and defined', function () {
        expect(service).toBeDefined();
    });

    it('notification service should be started', async function() {
        service.run().then(res => {
			  expect(service.started).toBe(true);
		  });
    });

    it('notification service test', function() {
        let reminder = {
            "id":"143",
            "user_id":"1",
            "subject":"Order #4374 | anton test",
            "description":"for unit test",
            "color":"13c713",
            "starts_at": (Date.now() / 1000 | 0) + 900,
            "ends_at":"1510777500",
            "remind_for":"5",
            "remind_at":null,
            "dismissed_at":null,
            "created_at":"1510748760",
            "updated_at":null,
            "snoozed_for":null,
            "node_id":"4374",
            "customer_name":"anton test"
        };
        service.setNotification(reminder);

        expect(service.notifications.length).toBe(1);
    });

});
