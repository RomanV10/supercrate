const Centrifuge = require('centrifuge');

'use strict';

angular
    .module('reminders')
    .factory('RemindersSocket', ($rootScope, ElromcoObserver) => {
        let connection, instance, centrifugo_namespace;

        class RemindersSocket {
            constructor() {
                let user = _.get($rootScope, 'currentUser.userId.uid');
                connection = new Centrifuge({
	                user,
	                url: 'wss://' + $rootScope.fieldData.centrifugo_settings.centrifugo_url,
                    timestamp: (Date.now() / 1000 | 0).toString(),
                    token: $rootScope.fieldData.centrifugo_settings.centrifugo_token
                });

                centrifugo_namespace = $rootScope.fieldData.centrifugo_settings.centrifugo_namespace;
                connection.connect();

                return instance ? instance : instance = this;
            }

            subscribe(callback) {
                let uid = _.get($rootScope, 'currentUser.userId.uid');

                connection.subscribe(centrifugo_namespace + ':reminders#' + uid, (data) => {
                    callback(data);
                    ElromcoObserver.dispatch('reminderCalendar.update');
                });
            }

            on(event, callback) {
                connection.on(event, callback);
            }
        }

        return RemindersSocket;
    });
