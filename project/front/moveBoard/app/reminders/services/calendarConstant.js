'use strict';

angular
    .module('reminders')
    .constant('CalendarConstants', {
                month: ['January', 'February', 'March', 'April', 'May', 'June',
                            'July', 'August', 'September', 'October', 'November', 'December'],
                timesBeforeRemind: [5, 10, 15, 30, 60, 120, 180, 240, 300, 360]
    });
