'use strict';

angular
    .module('reminders')
    .factory('Sound', ($location) => {
        
        const currentUrl = `${$location.$$protocol}://${$location.$$host}:${$location.$$port}`;

        class Sound {
            constructor(name){
                this.sound = new Audio(`${currentUrl}/moveBoard/content/audio/${name}.mp3`);
            }
            
            static get notificationSound() {
                try {
	                return localStorage.getItem('reminderSound') == 'true' ? true : false;
                } catch (error) {
                    return false;
                }
            }
        
            static set notificationSound(value) {
                try {
	                localStorage.setItem('reminderSound', value);
                } catch (error) {
                    console.log(error);
                }
            }
            async play() {
                try {
	                return await this.sound.play();
                } catch (e) {
                    return await console.log(e);
                }
            }
        }

        if (!Sound.notificationSound) {
            Sound.notificationSound = true;
        }

        return Sound;
    });
