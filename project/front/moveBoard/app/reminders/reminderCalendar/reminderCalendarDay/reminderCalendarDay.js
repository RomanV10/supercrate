'use strict';

class ReminderCalendarDay {
    constructor() {}
}

angular.module('reminders.calendar')
    .component('reminderCalendarDay', {
	    template: require('./reminderCalendarDay.html'),
        controller: ReminderCalendarDay,
        controllerAs: '$ctrl',
        bindings: {
            day: '<'
        },
        transclude: {
            reminder: 'reminderMin'
        }
});
