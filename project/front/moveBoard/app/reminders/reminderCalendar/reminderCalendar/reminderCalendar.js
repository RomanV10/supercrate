'use strict';

class ReminderCalendar {
    constructor(Month, CalendarConstants, ElromcoObserver, Reminder, moment) {
        this.Month = Month;
        this.ElromcoObserver = ElromcoObserver;
        this.Reminder = Reminder;
        this.moment = moment;

        this.monthList = CalendarConstants.month.map((month, index) =>
                                                            angular.copy({month, index}));
        this.pickerDate = {
            date: new Date()
        }

        this.selectedMonth = this.pickerDate.date.getMonth();
        this.ElromcoObserver.addEventListener('reminderCalendar.update', this.updateReminders.bind(this, this.pickerDate), this);

		(async() => {
			this.busy = true;
			await this.updateReminders(this.pickerDate);
			this.busy = false;
		})();
    }

    async updateReminders({date}){
        let condition = {
            from: this.moment(date).startOf('month').unix(),
            to: this.moment(date).endOf('month').unix()
        }

        let reminders = await this.Reminder.getByUser(condition);
        this.month = new this.Month(reminders);
        this.selectedDay = this.pickerDate.date.getDate();
    }

    changeDateFromPicker() {
        if(this.selectedMonth != this.pickerDate.date.getMonth()){
            this.selectedMonth = this.pickerDate.date.getMonth();
            this.pickerDate.date.setMonth(this.selectedMonth);
            this.updateReminders(this.pickerDate);
        } else {
            this.selectedDay = this.pickerDate.date.getDate();
        }

    }

    changeDateFromSelect(){
        let tempDate = angular.copy(this.pickerDate.date)
        if(this.selectedMonth != this.pickerDate.date.getMonth()){
            this.pickerDate.date = null;
            this.pickerDate.date = new Date(tempDate.getFullYear(), this.selectedMonth, tempDate.getDate());
            this.updateReminders(this.pickerDate);
        }
    }

    $onDestroy(){
        this.ElromcoObserver.removeEventListener('reminderCalendar.update', this)
    }
}

angular
    .module('reminders.calendar')
    .component('reminderCalendar', {
	    template: require('./reminderCalendar.html'),
        controller: ReminderCalendar,
        controllerAs: '$ctrl'
    });

ReminderCalendar.$inject = ['Month', 'CalendarConstants', 'ElromcoObserver', 'Reminder', 'moment'];
