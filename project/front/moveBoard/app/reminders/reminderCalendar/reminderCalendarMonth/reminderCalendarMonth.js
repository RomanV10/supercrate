'use strict';

class ReminderCalendarMonth {
    constructor($location) {
        this.$location = $location;
    }

    scrollToDay(day){
        this.$location.hash(`day-${day}`);
    }

    $onChanges($attr){
        if(_.get($attr, 'month.currentValue')){
            if(this.day){
                this.scrollToDay(this.day);
            }
        }
        if(_.get($attr, 'day.currentValue')){
            this.scrollToDay(this.day);
        }
    }

    $onDestroy() {
        this.$location.hash('');
    }
}

angular
    .module('reminders.calendar')
    .component('reminderCalendarMonth', {
	    template: require('./reminderCalendarMonth.html'),
        controller: ReminderCalendarMonth,
        controllerAs: '$ctrl',
        bindings: {
            month: '<',
            day: '<'
        }
    });

ReminderCalendarMonth.$inject = ['$location']
