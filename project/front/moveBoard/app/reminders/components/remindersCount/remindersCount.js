'use strict';

class ReminderCount {
    constructor($filter, NotificationService) {
        this.$filter = $filter;
        this.NotificationService = NotificationService;
    }

    get remindersCount(){
        let count;
        if (this.collection) {
            count = this.$filter('upcoming')(this.collection, true).length;
        } else {
            count = this.NotificationService.notifications.length;
        }
        this.hidden = count === 0;
        return count;
    }
}

angular
    .module('reminders')
    .component('remindersCount', {
        template: `<sup ng-bind="$ctrl.remindersCount" ng-style="$ctrl.hidden && {'display':'none'}"></sup>`,
        controller: ReminderCount,
        controllerAs: '$ctrl',
        bindings: {
            collection: '=?'
        }
    });

ReminderCount.$inject = ['$filter', 'NotificationService'];
