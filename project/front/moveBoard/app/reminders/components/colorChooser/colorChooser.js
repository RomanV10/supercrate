'use strict';

class ColorChooser {
    constructor() {
        this.defaultColors = [ '#f08178', '#ffd180', '#556b8d', '#13c713',
                               '#cc9900', '#e25d5d', '#80d8ff', '#82b1ff',
                               '#b388ff', '#f8bbd0', '#368ec5', '#ffa500' ];
    }

    $onInit() {
        this.colors = this.colors || this.defaultColors;

        this.ngModelCtrl.$parsers.push((color) => color);
        this.ngModelCtrl.$formatters.push((color) => {
            if(!color){
                this.color = this.colors[_.random(this.colors.length - 1)];
                this.bindToModel(this.color);
            } else {
                this.bindToModel(color);
            }
        });
    }

    $onChanges($attrs){
        if($attrs.colors && $attrs.colors.currentValue){
            this.colors = $attrs.colors.currentValue;
        } else {
            this.colors = this.defaultColors;
        }
    }

    bindToModel(color) {
        this.color = color
        this.ngModelCtrl.$setViewValue(color);
        this.ngModelCtrl.$commitViewValue();
    }
}

angular
    .module('reminders')
    .component('colorChooser', {
        template: require('./colorChooser.html'),
        controller: ColorChooser,
        controllerAs: '$ctrl',
        require: {
            ngModelCtrl: 'ngModel'
        },
        bindings:{
            colors: '<?'
        }
    });
