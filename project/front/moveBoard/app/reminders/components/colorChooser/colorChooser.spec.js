describe('Component: colorChooser', function () {
    
    let $scope, $compile, element;

    beforeEach(inject(function(_$rootScope_, _$compile_) {
        $compile = _$compile_;

        $scope = _$rootScope_.$new();
        element = angular.element('<color-chooser ng-model="color"></color-chooser>');
    }));

    it('should return default value', function() {
        $scope.color = void 0;

        $scope.$apply(() => element = $compile(element)($scope)); 

        expect($scope.color).toBeDefined();
    });

    it('should return correct value', function(){
        $scope.color = '#f08178';
        $scope.$apply(() => element = $compile(element)($scope)); 
        
        expect($scope.color).toBe($scope.color);
    });

    it('should return default value from array', function() {
        $scope.color = void 0;
        $scope.colors = [ '#f08171', '#ffd181', '#556b81', '#13c711',
                          '#cc9901', '#e25d51', '#80d8f1', '#82b1f1',
                          '#b388f1', '#f8bbd1', '#368ec1', '#ffa501' ];

        element = angular.element('<color-chooser ng-model="color" colors="colors"></color-chooser>');

        $scope.$apply(() => element = $compile(element)($scope)); 

        expect($scope.colors.includes($scope.color)).toBe(true);
    });
});
    