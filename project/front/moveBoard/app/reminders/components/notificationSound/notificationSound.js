'use strict';

class NotificationSound {
    constructor(Sound) {
        this.Sound = Sound;
    }
}

angular
    .module('reminders')
    .component('notificationSound', {
        template: require('./notificationSound.html'),
        controller: NotificationSound,
        controllerAs: '$ctrl',
        bindings: {
            ngDisabled: '<?'
        }
    });

NotificationSound.$inject = ['Sound'];
