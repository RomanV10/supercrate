'use strict';

class ReminderMin {
    constructor($element) {
        this.$element = $element;
        this.$element.css('background-color', `${this.reminder.Color}`);
    }

    checkFocus($event){
        if(!this.focus){
            $event.stopPropagation();
            $event.preventDefault();
            this.$element.css('opacity', 0.7);
        }
        this.focus = true;
    }

    onBlur(){
        this.focus = false;
        this.$element.css('opacity', 1);
    }

	async dismiss() {
    	this.awaiter = true;
    	try {
			await this.reminder.dismiss()
		} finally {
			this.awaiter = false;
		}
	}
}

angular
    .module('reminders')
    .component('reminderMin', {
	    template: require('./reminderMin.html'),
        controller: ReminderMin,
        controllerAs: '$ctrl',
        bindings: {
            reminder: '<'
        }
    });

ReminderMin.$inject = ['$element'];
