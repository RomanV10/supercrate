'use strict';

class Notifier {
    constructor() {
        this.reminder = this.options;
        this.awaiter = false;
    }

    async snooze() {
		this.awaiter = true;
    	try {
			await this.reminder.snooze();
			this.done();
		} finally {
			this.awaiter = false;
		}
    }

    async dismiss() {
		this.awaiter = true;
    	try {
			await this.reminder.dismiss();
			this.done();
		} finally {
			this.awaiter = false;
		}

	}
}

angular
    .module('reminders')
    .component('notification', {
	    template: require('./notification.html'),
        controller: Notifier,
        controllerAs: '$ctrl',
        bindings: {
            options: '<',
            done: '&'
        }
    });
