'use strict';

class Trigger {
    constructor(dialogs){
        this.restrict = 'A';
        this.dialogs = dialogs;
    }
    link($scope, $element, $attrs) {
        let dialog = _.has($attrs, 'request') ? 'reminderRequestBox' : 'reminderBox';
        $element.on('click',(event) =>
            this.dialogs[dialog]($scope.$eval($attrs.reminderBoxInvoker)));
    }
}

angular
    .module('reminders.reminderBox')
    .directive('reminderBoxInvoker', (dialogs) => new Trigger(dialogs));
