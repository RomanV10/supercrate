describe('Component: editReminder', function () {

    let $scope, $compile, Reminder, element, controller, moment;

    beforeEach(inject(function(_$rootScope_, _$compile_, _moment_, _testHelperService_) {

        $compile = _$compile_;
        moment = _moment_;

        $scope = _$rootScope_.$new();
	    let getcurrent = _testHelperService_.loadJsonFile('getcurrent-user.mock');
	    _$rootScope_.currentUser = {userId: getcurrent, userRole: []};
        element = angular.element(`<edit-reminder options="options"></edit-reminder>`);

        $scope.options = {
            node_id: 1,
            client: 'Roman'
        };
    }));

    it('template should be not empty', function() {
        $scope.$apply(() => element = $compile(element)($scope));
        let h2 = element.find('h2');
        expect(h2.text()).toBe('Reminder');
    });

    it('should be creared', function() {
        $scope.$apply(() => element = $compile(element)($scope));
        controller = element.controller('editReminder');
        spyOn(controller.reminder, 'create');

        expect(controller.reminder.color).toBeDefined();
        expect(controller.reminder.starts_at).toBeDefined();
        expect(controller.reminder.subject).toBeDefined();

        controller.reminder.starts_at = moment().unix() + 300;
        controller.editReminder.description.$setViewValue('test description');

        expect(controller.reminder.description).toBeDefined();
        expect(controller.editReminder.$valid).toBe(true);

        element.find('.btn-success').trigger('click');

        expect(controller.reminder.create).toHaveBeenCalled();
    });

    it('should be not created', function() {
        $scope.$apply(() => element = $compile(element)($scope));
        controller = element.controller('editReminder');
        spyOn(controller.reminder, 'create');

        expect(controller.editReminder.$invalid).toBe(true);

        element.find('.btn-success').trigger('click');

        expect(controller.reminder.create).not.toHaveBeenCalled();
    });

    it('should be updated', function() {
        $scope.options = {
            node_id: 1,
            client: 'Roman',
            created_at: moment().unix(),
            description: 'test description'
        };

        $scope.$apply(() => element = $compile(element)($scope));
        controller = element.controller('editReminder');
        spyOn(controller.reminder, 'update');

        controller.reminder.starts_at = moment().unix() + 300;

        expect(controller.editReminder.$valid).toBe(true);

        element.find('.btn-success').trigger('click');

        expect(controller.reminder.update).toHaveBeenCalled();
    })
});
