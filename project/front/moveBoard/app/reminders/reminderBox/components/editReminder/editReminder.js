'use strict';

class EditReminder {
    constructor(Reminder, moment) {
        this.moment = moment;
        this.minDate = new Date();

        this.reminder = new Reminder(this.options);

        this.isOld = !!this.reminder.created_at;
        this.remindMe = this.reminder.remind_for && this.isOld;
        this.canEdit = this.reminder.canEdit || !this.isOld;
        
        this.reminder.subject =  this.reminder.subject || `Order #${this.reminder.node_id} | ${this.options.client}`;
    }

    async saveReminder() {
        this.editReminder.$setSubmitted();
        if (!this.editReminder.$valid) {
            if (this.editReminder.description.$invalid) {
                this.editReminder.description.$setTouched();
            }
            this.editReminder.$setPristine();
            toastr.error(`Error`, 'Please fill required fields');
        } else {
            if (this.reminder.starts_at < this.moment().unix()) {
                toastr.error(`Error`, 'Please set correct start time!');
                this.editReminder.$setPristine();
                return;
            } 
            try {
					this.isOld ? await this.reminder.update(this.remindMe) : await this.reminder.create(this.remindMe);
					this.editReminder.$setPristine();
					this.done();
				} catch (error) {
					toastr.error(`Error`, 'Cannot save reminder', 'Please contact to administrator');
					this.editReminder.$setPristine();
				}
        }
    }

    async dismiss() {
        await this.reminder.dismiss();
        this.done();
    }
}

angular
    .module('reminders.reminderBox')
    .component('editReminder', {
        template: require('./editReminder.html'),
        controller: EditReminder,
        controllerAs: '$ctrl',
        bindings: {
            options: '<',
            done: '&'
        }
    });

EditReminder.$inject = ['Reminder', 'moment'];
