describe('Component: reminderList', function () {
    
    let $scope, $compile, Reminder, element, controller, moment;

    beforeEach(inject(function(_$rootScope_, _$compile_, _moment_) {
        $compile = _$compile_;
        moment = _moment_;

        $scope = _$rootScope_.$new();
        element = angular.element(`<reminder-list options="options" done="done()"></reminder-list>`);
        
        $scope.done = () => {};

        $scope.options = {
            collection: []
        };
    }));

    it('template should be not empty', function() {
        $scope.$apply(() => element = $compile(element)($scope)); 
        let h2 = element.find('h2');
        expect(h2.text()).toBe('Missed Reminders');
    })

    it('component should be closed', function(){
        spyOn($scope, 'done');
        $scope.$apply(() => element = $compile(element)($scope)); 

        expect($scope.done).toHaveBeenCalled();
    });

    it('component should not be closed', function(){
        spyOn($scope, 'done');
        $scope.options = { 
            collection: [],
            request: true
        };
        $scope.$apply(() => element = $compile(element)($scope)); 
        
        expect($scope.done).not.toHaveBeenCalled();
        expect(element.find('.no-data').find('h3').text()).not.toBe('');
    });
});
    