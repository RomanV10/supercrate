'use strict';

class ReminderList {
    constructor(Reminder, dialogs) {
        this.Reminder = Reminder;
        this.dialogs = dialogs;
    }

    $onInit() {
        if(this.options.request) {
            this.dialog = 'reminderBox';
        } else  {
            this.filter = {
                dismissed_at: null
            }
            this.dialog = 'notificationBox';
        }
        this.reminders = angular.copy(this.options.collection);
    }

    openDialog(reminder) {
        this.dialogs[this.dialog](reminder);
    }
}

angular
    .module('reminders.reminderBox')
    .component('reminderList', {
        template: require('./reminderList.html'),
        controller: ReminderList,
        controllerAs: '$ctrl',
        bindings: {
            options: '<',
            done: '&'
        }
    });

ReminderList.$inject = ['Reminder', 'dialogs'];
