'use strict';

class OffsetFor {
    constructor($filter, CalendarConstants) {
        this.timesBeforeRemind = _.map(CalendarConstants.timesBeforeRemind, (time) => {
            return {
                time,
                value: $filter('beforeTime')(time, !_.isUndefined(this.forward))
            }
        });
    }

    $onInit() {
        this.ngModelCtrl.$parsers.push((offset) => offset);
        this.ngModelCtrl.$formatters.push((offset) => {
            if(!offset){
                this.offset = this.timesBeforeRemind[0].time;
                this.bindToModel();
            } else {
                this.offset = Number(offset);
            }
        });
    }

    bindToModel() {
        this.ngModelCtrl.$setViewValue(this.offset);
        this.ngModelCtrl.$commitViewValue();
    }
}

angular
    .module('reminders.reminderBox')
    .component('timeOffsetFor', {
        template: require('./timeOffsetFor.html'),
        controller: OffsetFor,
        controllerAs: '$ctrl',
        require: {
            ngModelCtrl: 'ngModel'
        },
        bindings: {
            forward: '@'
        }
    });

OffsetFor.$inject = ["$filter", "CalendarConstants"];
