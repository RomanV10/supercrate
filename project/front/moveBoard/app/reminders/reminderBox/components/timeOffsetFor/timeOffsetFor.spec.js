describe('Component: reminderList', function () {
    
    let $scope, $compile, element;

    beforeEach(inject(function(_$rootScope_, _$compile_) {
        $compile = _$compile_;

        $scope = _$rootScope_.$new();
        element = angular.element(`<time-offset-for ng-model="offset"></time-offset-for>`);
    }));

    it('should return default value', function() {
        $scope.offset = void 0;
        $scope.$apply(() => element = $compile(element)($scope)); 
        
        expect($scope.offset).toBe(5);
    })

    it('should return correct value', function() {
        $scope.offset = 15;
        $scope.$apply(() => element = $compile(element)($scope)); 
        
        expect($scope.offset).toBe(15);
    })
});
    