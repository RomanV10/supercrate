'use strict';
import './unixDatePicker.styl';

class UnixDatePicker {
	constructor() {
		this.roundTo = 5;
	}

	get ModelDate() {
		return this.date;
	}

	set ModelDate(value) {
		this.date = value;
		this.bindToModel();
	}

	get RoundTo() {
		return this.roundTo * 60;
	}

	$onInit() {
		this.timeOptions = {
			timeSecondsFormat: 'ss',
			timeStripZeroSeconds: true
		};

		this.ngModelCtrl.$parsers.push((date) => moment(date).unix());

		this.ngModelCtrl.$formatters.push((value) => {
			let date = value ? new Date(Number(value) * 1000) : new Date();
			let year = date.getFullYear();
			let month = date.getMonth();
			let day = date.getDate();
			let hours = date.getHours();
			let minutes = this.roundMinutes(date.getMinutes());
			this.ModelDate = new Date(year, month, day, hours, minutes, 0);
		});

		this.isPickerOpened = false;
	}

	switchDatepicker() {
		this.isPickerOpened = !this.isPickerOpened;
	}

	roundMinutes(minutes) {
		let result = this.roundTo * Math.ceil(minutes / this.roundTo);
		return result;
	}

	bindToModel() {
		this.ngModelCtrl.$setViewValue(this.ModelDate);
		this.ngModelCtrl.$commitViewValue();
	}
}

angular
	.module('reminders.reminderBox')
	.component('unixDatePicker', {
		template: require('./unixDatePicker.html'),
		controller: UnixDatePicker,
		controllerAs: '$ctrl',
		require: {
			ngModelCtrl: 'ngModel'
		},
		bindings: {
			minDate: '<?'
		}
	});
