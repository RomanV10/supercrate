describe('Component: unixDatePicker', function () {
    
    let $scope, $compile, element;

    function roundMinutes(minutes){
        return 5 * Math.ceil(minutes / 5);
    }

    beforeEach(inject(function(_$rootScope_, _$compile_) {
        $compile = _$compile_;

        $scope = _$rootScope_.$new();
        element = angular.element('<unix-date-picker ng-model="date"></unix-date-picker>');
    }));

    it('should return default value', function() {
        $scope.date = void 0;
        $scope.$apply(() => element = $compile(element)($scope)); 

        let date = new Date();

        let expectedDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), roundMinutes(date.getMinutes())).getTime() / 1000 | 0; 
        
        expect($scope.date).toBe(expectedDate);
    })

    it('should return correct value', function() {
        $scope.date = new Date('12/02/17 15:07').getTime() / 1000 | 0;
        $scope.$apply(() => element = $compile(element)($scope)); 
        let expectedDate = new Date('12/02/17 15:10').getTime() / 1000 | 0;
        expect($scope.date).toBe(expectedDate);

        $scope.date = new Date('12/02/17 15:13').getTime() / 1000 | 0;
        $scope.$apply(); 
        expectedDate = new Date('12/02/17 15:15').getTime() / 1000 | 0;
        expect($scope.date).toBe(expectedDate);

        $scope.date = new Date('12/02/17 15:20').getTime() / 1000 | 0;
        $scope.$apply(); 
        expect($scope.date).toBe($scope.date);
    })
});
    