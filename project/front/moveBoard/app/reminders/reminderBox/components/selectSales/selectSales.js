'use strict';

class SelectSales {
    constructor($rootScope, PermissionsServices) {
        this.PermissionsServices = PermissionsServices;
        this.$rootScope = $rootScope;

        this.managersList = this.$rootScope.managersList;
    }

    $onInit(){
        this.ngModelCtrl.$parsers.push((uid) => uid);

        this.ngModelCtrl.$formatters.push((uid) => {
            if (!uid) {
                uid = _.get(this.$rootScope, 'currentUser.userId.uid');
                this.uid = String(uid);
                this.bindToModel();
            } else {
                this.uid = String(uid);
            }
        });
    }

    bindToModel(){
        this.ngModelCtrl.$setViewValue(this.uid);
        this.ngModelCtrl.$commitViewValue();
    }
}

angular
    .module('reminders.reminderBox')
    .component('selectSales', {
        template: require('./selectSales.html'),
        controller: SelectSales,
        controllerAs: '$ctrl',
        require: {
            ngModelCtrl: 'ngModel'
        }
    });

SelectSales.$inject = ['$rootScope', 'PermissionsServices'];
