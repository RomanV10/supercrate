'use strict';

angular
    .module('reminders')
    .filter('upcoming', (moment) => {
        return (reminders, predicate) => {
            let date = moment().unix();
            return _.filter((reminders), reminder => 
                predicate ? date < reminder.remindTime && !reminder.dismissed_at : date > reminder.remindTime || reminder.dismissed_at);
        }
    });
