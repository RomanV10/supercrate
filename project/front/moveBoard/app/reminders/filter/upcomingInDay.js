'use strict';

angular
    .module('reminders')
    .filter('upcomingInDay', ($filter) => 
            (days, params) => _.filter(days, (day) =>  $filter('upcoming')(day.reminders, params).length > 0));
