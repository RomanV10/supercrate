'use strict';

angular
    .module('reminders')
    .filter('beforeTime', () =>
        (params, forward) => {
            let minutes = params % 60;
            let hours = Math.floor(params / 60);
            let postfix;
            
            if (hours >= 1) {
                postfix = forward ? ' hour' : ' hour before start';
                return hours + postfix;
            } else {
                postfix = forward ? ' minutes' : ' minutes before start';
                return minutes + postfix;
            }
        });
