export const CALCULATOR = {
	PICKED_UP_CREW: 'pickedUpCrew',
	DELIVERY_CREW: 'deliveryCrew',
	BASE_CREW: 'baseCrew',
	PICK_UP: 'pickup',
	DELIVERY: 'delivery'
};
