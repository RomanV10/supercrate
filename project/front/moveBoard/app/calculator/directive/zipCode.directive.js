angular
	.module('app.calculator')
	.directive('zipcode', processZipCode);

// Zip Code Directive
function processZipCode($http, logger) {
	return {
		restrict: 'A',
		require: '?ngModel',
		scope: {
			'address': '=',
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind('change', function () {
				if (element.val().length != 5) {
					element.val('');
				} else {
					var zip_code = element.val();

					if (!angular.isUndefined(zip_code)) {
						if (zip_code.length == 5) {
							var geocoder = new google.maps.Geocoder();
							var data = {
								address: '',
								componentRestrictions: {
									country: 'US',
									postalCode: zip_code
								},
							};

							geocoder.geocode(data, function (results, status) {
								if (status == google.maps.GeocoderStatus.OK) {
									var arrAddress = results[0]['address_components'];
									scope.address = parseZipToAddress(arrAddress);
									logger.success(scope.address.full_adr, 'Success', 'Address Was Founded');
								}
							});
						}
					}
				}
			});


			function parseZipToAddress(arrAddress) {
				var city, state, country, postal_code;

				$.each(arrAddress, function (i, address_component) {
					if (address_component.types[0] == 'administrative_area_level_1') { // State
						if (address_component.short_name.length < 3) {
							state = address_component.short_name;
						}
					}

					if (address_component.types.indexOf('locality') > -1 || address_component.types.indexOf('neighborhood') > -1 || address_component.types.indexOf('sublocality') > -1) { // locality type
						city = address_component.long_name;
					}

					if (address_component.types[0] == 'country') { // locality type
						country = address_component.short_name;
					}

					if (address_component.types[0] == 'postal_code') { // "postal_code"
						postal_code = address_component.short_name;
					}
				});
				
				var address = {
					'city': city,
					'state': state,
					'postal_code': postal_code,
					'country': country,
					'full_adr': city + ', ' + state + ' ' + postal_code
				};

				return address;
			}
		}
	};
}
