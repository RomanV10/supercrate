angular
	.module('app.calculator')
	.directive('calcForm', calcForm);

function calcForm($rootScope, $q, datacontext, CalculatorServices) {
	let directive = {
		link: link,
		template: require('../templates/calculator_form.html'),
		restrict: 'E'
	};

	return directive;

	function link(scope, element, attrs) {
		scope.request = {};
		scope.request.serviceType = 1;
		scope.fieldData = datacontext.getFieldData();
		scope.calcSettings = angular.fromJson(scope.fieldData.calcsettings);
		let travelTime = scope.calcSettings.travelTime;
		// Load and Show Rooms on Form
		scope.servicesType = scope.fieldData.field_lists.field_move_service_type;
		scope.sizeOfMove = scope.fieldData.field_lists.field_size_of_move;
		scope.entranceType = scope.fieldData.field_lists.field_type_of_entrance_from;
		scope.rooms = scope.fieldData.field_lists.field_extra_furnished_rooms;
		scope.roomsStatus = {};

		angular.forEach(scope.rooms, function (rid, key) {
			scope.roomsStatus[key] = 0;
		});

		scope.objectKeys = function (obj) {
			return Object.keys(obj);
		};

		scope.request.checkedRooms = {
			rooms: []
		};

		scope.showRooms = function () {
			if (scope.request.moveSize == 1 || scope.request.moveSize == 2) {
				scope.request.checkedRooms.rooms = [];

				angular.forEach(scope.roomsStatus, function (rid, key) {
					scope.roomsStatus[key] = 0;
				});

				scope.roomsStatus[5] = 1;
			} else if (scope.request.moveSize == 8 || scope.request.moveSize == 9 || scope.request.moveSize == 10) { //if House
				angular.forEach(scope.roomsStatus, function (rid, key) {
					scope.roomsStatus[key] = 1;
				});

				scope.request.checkedRooms.rooms = [];
				scope.request.checkedRooms.rooms[1] = '1';
				scope.request.checkedRooms.rooms[2] = '2';
				scope.roomsStatus[1] = 2;
				scope.roomsStatus[2] = 2;
			} else {
				angular.forEach(scope.roomsStatus, function (rid, key) {
					scope.roomsStatus[key] = 1;
				});

				scope.roomsStatus[8] = 0;
				scope.roomsStatus[7] = 0;
				scope.roomsStatus[6] = 0;

				scope.request.checkedRooms.rooms = [];
				scope.request.checkedRooms.rooms[1] = '1';
				scope.roomsStatus[1] = 2;
			}
		};

		scope.resetForm = function () {
			scope.request = [];
			scope.request.serviceType = 1;
			angular.forEach(scope.roomsStatus, function (rid, key) {
				scope.roomsStatus[key] = 0;
			});
		};


		scope.Calculate = function (isValid) {
			// check to make sure the form is completely valid
			if (isValid) {
				let promise1 = CalculatorServices.calculate(scope.request);
				let promise2 = CalculatorServices.getDuration(scope.request.zipFrom, scope.request.zipTo, scope.request.serviceType);
				let promise3 = CalculatorServices.getLongDistanceCode(scope.request.zipTo);

				$q.all([promise1, promise2, promise3]).then(function (data) {
					let results = {};
					results.small_job = false;
					results.request = scope.request;
					results.work_time = data[0].work_time;

					results.min_hours = data[0].min_hours;
					results.total_cf = data[0].total_cf;
					results.trucks = data[0].truck;
					results.movers_count = data[0].crew;
					results.distance = data[1].distances.AB.distance;
					results.duration = data[1].distances.AB.duration;
					results.type = data[1].type;

					if (results.type == 'flat_rate') {
						results.request.servicesType = 5;
						scope.request.serviceType = 5;
					}

					results.travel_time = data[1].duration;

					// PICKUP TIME
					results.pickup_time = {};
					results.pickup_time.min = results.work_time.min; // + Travel Time Back To Office
					results.pickup_time.max = results.work_time.max; // + Travel Time Back To Office

					//LOCAL MOVE WORK TIME
					if(!scope.calcSettings.doubleDriveTime && !scope.calcSettings.isTravelTimeSameAsDuration){
						results.work_time.min += parseFloat(results.duration);
						results.work_time.max += parseFloat(results.duration);
					}

					// GET RATE
					if (results.request.serviceType == 7) {
						results.request.field_moving_to = {
							administrative_area: results.request.To.state,
							premise: data[2]
						};
						results.request.field_moving_from = {administrative_area: results.request.From.state};

						results.rate = CalculatorServices.getLongDistanceRate(results.request, results.total_cf.weight);

						results.request.move_size = {raw: results.request.moveSize};
						results.request.field_extra_furnished_rooms = results.request.checkedRooms.rooms;
						results.request.field_cubic_feet = {value: ''};

						results.ldQutoe = CalculatorServices.getLongDistanceQuote(results.request);
					} else {
						results.rate = CalculatorServices.getRate(scope.request.moveDate, results.movers_count, results.trucks);
					}


					results.quote_min = parseFloat(results.work_time.min) + (travelTime ? results.travel_time : 0);

					if (results.quote_min <= results.min_hours) {
						results.quote_min = results.min_hours;
					}

					results.quote_max = parseFloat(results.work_time.max) + (travelTime ? results.travel_time : 0);


					if (results.quote_max <= results.min_hours) {
						results.quote_max = results.min_hours;
					}

					results.small_job = results.quote_max === results.quote_min;

					results.total_quote_min = results.rate * results.quote_min;
					results.total_quote_max = results.rate * results.quote_max;


					$rootScope.$broadcast('calc.results', results);
				});
			}
		};
	}
}
