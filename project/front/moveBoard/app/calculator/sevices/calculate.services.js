import {CALCULATOR} from '../constants/calculator.constant';

	angular
		.module('app.calculator')
		.factory('CalculatorServices', CalculatorServices);

	/* @ngInject */
	function CalculatorServices($q, config, $http, EditRequestServices, InventoriesServices, $rootScope, common,
		apiService, SweetAlert, CommercialCalc) {
		let service = {};

		let ratesSettings = [];
		let longdistance = [];
		let data = [];
		let calcSettings = [];
		let basicSettings = [];
		let scheduleData = [];
		let additionalCrews = [];
		let crews = [];

		// payroll object for set_contract_info
		let globalDataForPayroll = {};

		service.getLongDistanceCode = getLongDistanceCode;
		service.getLongDistanceWeight = getLongDistanceWeight;
		service.getLongDistanceRate = getLongDistanceRate;
		service.recalculateLongDistanceRate = recalculateLongDistanceRate;
		service.getLongDistanceWindow = getLongDistanceWindow;
		service.getLongDistanceQuote = getLongDistanceQuote;
		service.getLongDistanceInfo = getLongDistanceInfo;
		service.updateLdMinWeightSettings = updateLdMinWeightSettings;
		service.calculate = calculate;
		service.getDuration = getDuration;
		service.getTravelTime = getTravelTime;
		service.calcDistance = calcDistance;
		service.getRate = getRate;
		service.getAdditionalTruckRate = getAdditionalTruckRate;
		service.getRateAdditionalHelper = getRateAdditionalHelper;
		service.geoCode = geoCode;
		service.getRequestCubicFeet = getRequestCubicFeet;
		service.getCubicFeet = getCubicFeet;
		service.calculateTime = calculateTime;
		service.getMovers = getMovers;
		service.init = init;
		service.updateRateSettings = updateRateSettings;
		service.getTrucks = getTrucks;
		service.getStorageRate = getStorageRate;
		service.getReservationPrice = getReservationPrice;
		service.checkExperation = checkExperation;
		service.calculateStorageRequest = calculateStorageRequest;
		service.getFullDistance = getFullDistance;
		service.getRoundedTime = getRoundedTime;
		service.getRequestServiceType = getRequestServiceType;
		service.getTotalWeight = getTotalWeight;
		service.getRequestWeightBasedOnMoveSize = getRequestWeightBasedOnMoveSize;
		// TODO Only for calc result form
		service.getOvernightRate = getOvernightRate;
		service.generateWorkerPayroll = generateWorkerPayroll;
		service.convertDateIntoMonths = convertDateIntoMonths;
		service.checkZipValidForLD = checkZipValidForLD;
		service.getLinkToLdSettings = getLinkToLdSettings;
		service.roundTimeInterval = roundTimeInterval;

		return service;

		function getLinkToLdSettings() {
			return longdistance;
		}

		function checkZipValidForLD(stateCodeFrom, stateCodeTo, areaCode) {
			stateCodeTo = stateCodeTo.toLowerCase();

			let isLDEnabled = longdistance.stateRates[stateCodeFrom][stateCodeTo].longDistance;
			let isStatePriceExist = _.get(longdistance, `stateRates.${stateCodeFrom}.${stateCodeTo}.state_rate`, 0) > 0;
			let isAreaPriceExist = _.get(longdistance, `stateRates.${stateCodeFrom}.${stateCodeTo}.rate[${areaCode}]`, 0) > 0;
			let isAcceptAllQuotes = longdistance.acceptAllQuotes;

			return isLDEnabled && (isStatePriceExist || isAreaPriceExist) || isAcceptAllQuotes;
		}

		function updateLdMinWeightSettings(request, hardReload){

			if (request.request_all_data) {

				let someSettingMissed = angular.isUndefined(request.request_all_data.min_price)
					|| angular.isUndefined(request.request_all_data.min_price)
					|| angular.isUndefined(request.request_all_data.min_price_enabled);

				if (someSettingMissed || hardReload) {
					let settings = getLongDistanceInfo(request);
					request.request_all_data.min_price_enabled = settings.min_price_enabled || false;
					request.request_all_data.min_price = settings.min_price || 0;
					if (hardReload) {
						request.request_all_data.min_weight_old = request.request_all_data.min_weight;
					}
					request.request_all_data.min_weight = settings.min_weight || 0;
				}
			}
		}

		function getRequestServiceType(from, to) {
			var deferred = $q.defer();

			var data = {
				"zip_from": from,
				"zip_to": to
			};

			$http
				.post(config.serverUrl + 'server/move_distance_calculate/get_request_service_type', data)
				.then(function (response) {
					deferred.resolve(response.data);
				}, function () {
					deferred.reject();
				});

			return deferred.promise;
		}

		function checkExperation(request) {
			const DAY_PRECISION = 'day';
			let today = moment();
			let moveDate = moment(request.date.value);
			let isValidBeforeDayExpirationTime = false;
			let currentHour = moment().hour();
			let beforeHours = +scheduleData.ExperationBeforeTime;
			let expirationReqType = scheduleData.ExperationReqType;

			if (_.isEqual(expirationReqType, 'morning')) {
				let startTime = +moment(moment().format('YYYY-MM-DD:') + request.start_time1.value, ['YYYY-MM-DD:h:mm A']).format('H');

				isValidBeforeDayExpirationTime = startTime >= 7 && 10 >= startTime;
			} else {
				isValidBeforeDayExpirationTime = true;
			}

			let yesterday = moveDate.clone().subtract(1, 'd');
			let isExpireTime = beforeHours <= currentHour;
			let isDayBeforeMoveDate = today.isSame(yesterday, DAY_PRECISION) && isValidBeforeDayExpirationTime && isExpireTime;
			let isMoveDateToday = today.isSame(moveDate, DAY_PRECISION);
			let isDayAfterMoveDate = today.isAfter(moveDate, DAY_PRECISION);
			let isExpireDate = isDayBeforeMoveDate || isMoveDateToday || isDayAfterMoveDate;

			return isExpireDate;
		}


		function getStorageRate(request, interval) {
			let minRate, maxRate, result = {};
			if (_.isUndefined(request.field_request_settings)) {
				minRate = basicSettings.storage_rate.min;
				maxRate = basicSettings.storage_rate.max;
			} else {
				minRate = request.field_request_settings.storage_rate.min;
				maxRate = request.field_request_settings.storage_rate.max;
			}

			let weight = getTotalWeight(request);
			result.min = weight.weight * minRate;
			result.max = weight.weight * maxRate;

			return result;
		}

		function convertDateIntoMonths(duration) {
			const MONTH_LENGTH = 30;
			if (duration > MONTH_LENGTH) {
				let surplusDays = Math.round(duration % MONTH_LENGTH);
				let clearMonth = moment.duration(duration, 'days').humanize();
				return clearMonth + ' and ' + surplusDays + ' days';
			} else {
				return Math.round(duration) + ' days';
			}
		}


		function init(response) {
			ratesSettings = response.prices_array;
			longdistance = angular.fromJson(response.longdistance);
			data = response;
			calcSettings = angular.fromJson(response.calcsettings);
			basicSettings = angular.fromJson(response.basicsettings);
			scheduleData = angular.fromJson(response.schedulesettings);
		}

		function updateRateSettings (newPrices) {
			ratesSettings = newPrices;
		}


		function getReservationPrice(request) {
			//check status
			let zeroReservation = 0;
			var reservation = 0;
			//IF FLAT RATE
			if (request.service_type.raw == 5) {
				reservation = scheduleData.flatReservationRate;

				if (!scheduleData.flatReservationRate) {
					var persentage = parseFloat(scheduleData.flatReservation || zeroReservation);
					reservation = request.quote * persentage / 100;
				}
			} else if (request.service_type.raw == 7) {
				reservation = scheduleData.longReservationRate;

				if (!scheduleData.longReservationRate) {
					var persentage = parseFloat(scheduleData.longReservation || zeroReservation);
					reservation = request.quote * persentage / 100;
				}
			} else {
				reservation = scheduleData.localReservationRate;

				if (!scheduleData.localReservationRate) {
					var hours = parseFloat(scheduleData.localReservation || zeroReservation);
					reservation = hours * parseFloat(request.rate.value);
				}
			}

			return reservation;

        }

		function geoCode(zip_code) {
			var geocoder = new google.maps.Geocoder();
			var deferred = $q.defer();
			var data = {
				address: '',
				componentRestrictions: {
					country: 'US',
					postalCode: zip_code
				},
			};
			geocoder.geocode(data, function (results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					var arrAddress = results[0]['address_components'];
					var address = parseZipToAddress(arrAddress);
					var isWrong = (_.indexOf((_.last(results[0].address_components)).types, 'country') > -1
						&& _.last(results[0].address_components).short_name != "US");
					deferred.resolve({result: address, isWrong: isWrong});
				} else {

					deferred.reject();
				}

			});

			return deferred.promise;
		}

		function getLongDistanceCode(zip) {
			var deferred = $q.defer();

			if (_.isEmpty(zip)) {
				deferred.resolve(false);
			} else {
				$http
					.get(config.serverUrl + 'extra/get_area_code/' + zip)
					.success(function (data) {

						var areaCode = data[0];

						deferred.resolve(areaCode);
					})
					.error(function (data) {

						deferred.reject(data);
					});
			}

			return deferred.promise;
		}


		function getLongDistanceWindow(request, first_day, days) {
			let delivery_days = 0;

			if (isNotValidAdministrativeArea(request)) {
				return 0;
			}

			if (angular.isDefined(days)) {
				delivery_days = days;
			} else {
				var to = request.field_moving_to;
				var from = request.field_moving_from;

				var state = to.administrative_area.toLowerCase();
				var baseState = from.administrative_area.toUpperCase();
				delivery_days = longdistance.stateRates[baseState][state].delivery_days || 0;
			}

			var last_day = moment(first_day, "MMM DD, YYYY").businessAdd(delivery_days);
			return last_day;
		}

		function getLongDistanceWeight(request) {
			let result = {
				minPrice: 0,
				minPriceEnabled: false,
				minWeight: 0
			};

			if (isNotValidAdministrativeArea(request)) {
				return result;
			}

			let state = request.field_moving_to.administrative_area.toLowerCase();
			let baseState = request.field_moving_from.administrative_area.toUpperCase();
			let stateSettings = longdistance.stateRates[baseState][state];

			if (!stateSettings.longDistance && longdistance.acceptAllQuotes) {
				SweetAlert.swal('You are creating a Long Distance Request with locations between which we don\'t do Long Distance moves', '', 'error');
				return result;
			} else {
				return {
					minPrice: stateSettings.minPrice,
					minPriceEnabled: stateSettings.minPriceEnabled,
					minWeight: stateSettings.min_weight
				};
			}
		}

		function getLongDistanceRate(request, weight) {
			if (request.field_long_distance_rate && request.field_long_distance_rate.value) {
				return request.field_long_distance_rate.value;
			} else {
				return recalculateLongDistanceRate(request, weight);
			}
		}

		function recalculateLongDistanceRate(request, weight) {
			if (isNotValidAdministrativeArea(request)) {
				return 0;
			}

			let state = request.field_moving_to.administrative_area.toLowerCase();
			let baseState = request.field_moving_from.administrative_area.toUpperCase();
			let areaCode = request.field_moving_to.premise;

			if (isNaN(weight)) {
				weight = getTotalWeight(request).weight;
			}

			let rate = getStateRatesRate(areaCode, state, baseState, weight);

			return rate;
		}


		function getLongDistanceInfo(request) {
			let result = {
				days: 0,
				min_weight: 0,
				average_days: 0,
				min_price_enabled: false,
				min_price: 0
			};

			if (isNotValidAdministrativeArea(request)) {
				return result;
			}

			let state = request.field_moving_to.administrative_area.toLowerCase();
			let baseState = request.field_moving_from.administrative_area.toUpperCase();
			let stateSettings = longdistance.stateRates[baseState][state];

			result.days = stateSettings['delivery_days'] || 0;
			result.min_weight = parseFloat(stateSettings['min_weight'] || 0);
			result.min_price_enabled = stateSettings['minPriceEnabled'];
			result.min_price = result.min_price_enabled ? stateSettings['minPrice'] || 0 : 0;
			result.average_days = stateSettings['average_delivery_days'] || 0;

			return result;
		}

		function isNotExistsStateRate(state, baseState) {
			return !longdistance.stateRates[baseState] || !longdistance.stateRates[baseState][state];
		}

		function isNotValidStateSetting(request) {
			var state = request.field_moving_to.administrative_area.toLowerCase();
			var baseState = request.field_moving_from.administrative_area.toUpperCase();
			let result = isNotExistsStateRate(state, baseState) || !longdistance.stateRates[baseState][state].longDistance && !longdistance.acceptAllQuotes;

			return result;
		}

		function isNotValidAdministrativeArea(request) {
			let isNotLongDistance = request.field_move_service_type && request.field_move_service_type != 7 || request.service_type && request.service_type.raw != 7;
			let isNotInitAdministrativeArea = !isSetAdministrativeArea(request.field_moving_to) || !isSetAdministrativeArea(request.field_moving_from);
			let result = isNotLongDistance || isNotInitAdministrativeArea || isNotValidStateSetting(request);

			return result;
		}

		function getLongDistanceQuote(request, closing, contract) {
			let weight = _fetchLDWeight(request, closing) || 0;
			let result = _getLDQuote(request, weight, contract);
			return result;
		}

		function _fetchLDWeight(request, closing) {
			let weightType = _.get(request, 'field_useweighttype.value', 1);
			let isCommercial = request.move_size.raw == 11 && weightType == 1;
			let hasInventory = _.get(request, 'inventory.inventory_list.length') && weightType == 2;
			let result = 0;

			if (isCommercial && !hasInventory) {
				let commercialExtra = !_.isUndefined(request.commercial_extra_rooms) ? request.commercial_extra_rooms : request.field_commercial_extra_rooms;
				result = CommercialCalc.getCommercialCubicFeet(commercialExtra.value, calcSettings, request.field_custom_commercial_item).weight;
			} else {
				result = getRequestWeight(request, closing);
			}

			return result;
		}

		function _getLDRate(request, weight, contract) {
			let rate;
			let ldRate = _.get(request, 'field_long_distance_rate.value', 0);
			if (contract) {
				rate = _.get(request, 'request_all_data.invoice.field_long_distance_rate.value', 0);
			} else if (_.round(ldRate, 2)) {
				rate = parseFloat(request.field_long_distance_rate.value);
			} else {
				rate = getRequestRate(request, weight);
			}
			return rate;
		}

		function _getLDQuote(request, weight, contract) {
			var rate = _getLDRate(request, weight, contract);

			//fix for request w/o zip
			if (isNotValidAdministrativeArea(request)) {
				return 0;
			}

			var state = request.field_moving_to.administrative_area.toLowerCase();
			var baseState = request.field_moving_from.administrative_area.toUpperCase();
			let allData = request.request_all_data || {};
			let stateSettings = longdistance.stateRates[baseState][state];

			var minPriceEnabled = angular.isDefined(allData.min_price_enabled) ? allData.min_price_enabled : stateSettings["minPriceEnabled"];
			var minPrice = angular.isDefined(allData.min_price) ? allData.min_price : stateSettings["minPrice"];
			var minWeight = angular.isDefined(allData.min_weight) ? allData.min_weight : stateSettings["min_weight"];

			if (!isNaN(minPrice) && !isNaN(minWeight) && minPriceEnabled) {
				if (Number(minWeight) < weight) {
					return Number(minPrice) + rate * (weight - Number(minWeight));
				} else {
					return Number(minPrice);
				}
			} else {
				return rate * weight;
			}
		}


		function getRequestRate(request, weight) {
			var rate = 0;

			if (!isNotValidAdministrativeArea(request)) {
				var areaCode = request.field_moving_to.premise;
				var state = request.field_moving_to.administrative_area.toLowerCase();
				var baseState = request.field_moving_from.administrative_area.toUpperCase();
				var stateRate = getStateRatesRate(areaCode, state, baseState, weight);

				rate = stateRate || 0;
			}

			return rate;
		}


		function isSetAdministrativeArea(requestField) {
			return requestField && !_.isUndefined(requestField.administrative_area) && !_.isNull(requestField.administrative_area);
		}


		function getDiscountRate(discounts, weight) {
			let rate = 0;

			if (discounts) {
				let discount = discounts.filter(discount => weight >= discount.startWeight).pop();

				if (discount) {
					rate = discount.rate;
				}
			}

			return rate;
		}

		function getStateRatesRate(areaCode, state, baseState, weight) {
			var result = 0;

			let stateSettings = longdistance.stateRates[baseState][state];

			if (angular.isUndefined(stateSettings.rate)) {
				stateSettings.rate = {};
			}

			if (stateSettings.longDistance || longdistance.acceptAllQuotes) {
				if (stateSettings.state_rate) {
					result = stateSettings.state_rate;
					result = getDiscountRate(stateSettings.discounts, weight) || result;

				} else if (stateSettings.rate[areaCode]) {
					result = stateSettings.rate[areaCode];
					if (stateSettings.areaDiscounts) {
						result = getDiscountRate(stateSettings.areaDiscounts[areaCode], weight) || result;
					}
				}
			}

			return result;
		}


		function getRequestWeight(request, closing) {
			var weight = request.closing_weight && closing ? parseFloat(request.closing_weight.value) : _.get(request, 'inventory_weight.cfs', 0);

			if (weight == 0) {
				weight = getCubicFeet(request);
				weight = weight.weight;

				if (_.get(request, 'field_useweighttype.value', 1) == 3) {
					weight = parseFloat(request.custom_weight.value);
				}
			}

			if (request.field_useweighttype && !closing) {
				//in request uses default room weight
				if (request.field_useweighttype.value == 1) {
					weight = getRequestCubicFeet(request).weight;
				}
				//in request uses inventory weight
				if (request.field_useweighttype.value == 2 && request.inventory && request.inventory.inventory_list) {
					weight = InventoriesServices.calculateTotals(request.inventory.inventory_list).cfs;
				}
				//in request uses custom weight
				if (request.field_useweighttype.value == 3) {
					weight = +request.custom_weight.value;
				}
			}

			var linfo = service.getLongDistanceInfo(request);
			var min_weight = +linfo.min_weight;

			if (request.request_all_data && angular.isDefined(request.request_all_data.min_weight)) {
				min_weight = request.request_all_data.min_weight;
			}

			if (_.isEmpty(request.field_cubic_feet.value)) {
				request.field_cubic_feet.value = (longdistance.default_weight == 1);
			}

			if (request.field_cubic_feet.value && min_weight > weight) {
				weight = min_weight;
			}

			if (!request.field_cubic_feet.value && min_weight * 7 > weight * 7) {
				weight = min_weight * 7;
			}

			return parseFloat(weight);
		}


		function getCubicFeet(request) {
			// Get default size of apartment
			var move_size = parseInt(calcSettings.size[request.field_size_of_move || request.move_size.raw]);
			// Get rooms if exist
			var total_room_weight = 0;

			angular.forEach(request.field_extra_furnished_rooms || request.rooms.value, function (rid, key) {
				total_room_weight += parseInt(calcSettings.room_size[rid]);
			});

			//Get Total Weight

			var total = {};
			total.weight = total_room_weight + move_size + parseInt(calcSettings.room_kitchen);
			total.min = total.weight - 100;
			total.max = total.weight + 100;

			return total;
		}


		function getTotalWeight(request) {
			var total_weight = {weight: 0};

			var listInventories = _.get(request, 'inventory.inventory_list', []);
			let useweighttype = _.get(request, 'field_useweighttype.value', 1);

			switch (parseInt(useweighttype)) {
				case 1:
					total_weight.weight = getWeightByMoveSize(request);

					break;
				case 2:
					if (!_.isEmpty(listInventories) && request.inventory_weight) {
						total_weight.weight = request.inventory_weight.cfs;
					} else {
						total_weight.weight = InventoriesServices.calculateTotals(request.inventory.inventory_list).cfs;
					}

					break;
				case 3:
					total_weight.weight = parseFloat(request.custom_weight.value || 0);

					break;
				default:
					total_weight.weight = getWeightByMoveSize(request);
			}

			return total_weight;
		}

		function getWeightByMoveSize(request) {
			let result = 0;

			if (request.move_size.raw == 11 && request.field_useweighttype.value != 2) {
				result = CommercialCalc.getCommercialCubicFeet(request.commercial_extra_rooms.value, calcSettings, request.field_custom_commercial_item).weight;
			} else {
				result = getRequestCubicFeet(request).weight;
			}

			return result;
		}

		function getRequestWeightBasedOnMoveSize(request) {
			let weight;

			if (request.move_size.raw == 11 && request.field_useweighttype.value != 2) {
				weight = CommercialCalc.getCommercialCubicFeet(request.commercial_extra_rooms.value, calcSettings, request.field_custom_commercial_item).weight;
			} else {
				weight = getTotalWeight(request).weight;
			}

			if (request.service_type.raw == 7 && weight < Number(request.request_all_data.min_weight)) {
				weight = Number(request.request_all_data.min_weight);
			}

			return weight;
		}

		function calculateTime(request, total_cf, callback) {
			// GET MOVER COUNT BASE ON total_cf and Entrance type
			var movers_count = getMovers(request, total_cf);
			var total_time = getTime(request, total_cf, movers_count);

			return total_time;
		}


		function calculate(request, callback) {
			var deferred = $q.defer();

			// CALCULAT TOTAL CUBIC FEET
			var total_cf = getTotalCubicFeet(request);
			// GET MOVER COUNT BASE ON total_cf and Entrance type
			var movers_count = getMovers(request, total_cf);
			var data = getTime(request, total_cf, movers_count);

			var results = {};
			results.small_job = false;
			results.work_time = data.work_time;
			results.min_hours = Number(data.min_hours);
			results.total_cf = data.total_cf;
			results.truck = data.trucks;
			results.crew = data.movers_count;
			results.duration = request.duration;
			results.travel_time = request.travelTime;

			// PICKUP TIME
			results.pickup_time = {};
			results.pickup_time.min = results.work_time.min; // + Travel Time Back To Office
			results.pickup_time.max = results.work_time.max; // + Travel Time Back To Office

			//LOCAL MOVE WORK TIME
			results.min_time = results.work_time.min;
			results.max_time = results.work_time.max;

			if (!calcSettings.doubleDriveTime && !calcSettings.isTravelTimeSameAsDuration) {
				results.min_time += results.duration;
				results.max_time += results.duration;
			}

			results.min_time = getRoundedTime(results.min_time);
			results.max_time = getRoundedTime(results.max_time);

			// GET RATE
			results.rate = service.getRate(request.moveDate, results.crew, results.truck);

			results.quote_min = parseFloat(results.min_time);
			results.quote_max = parseFloat(results.max_time);

			if (results.quote_min < results.min_hours) {
				results.quote_min = results.min_hours;
			}

			if (results.quote_max < results.min_hours) {
				results.quote_max = results.min_hours;
			}

			if (results.quote_max == results.quote_min) {
				results.small_job = true;
			}

			results.total_quote_min = results.rate * results.quote_min;
			results.total_quote_max = results.rate * results.quote_max;


			deferred.resolve(results);

			return deferred.promise;
		}


		function getMovers(request, total_cf) {
			// Get default size of apartment
			var TYPE_FROM = request.typeFrom || '1';
			var TYPE_TO = request.typeTo || '1';

			// IF MOVE SIZE IS HOME ENTRANCE TYPE_FROM same as 1 FLOOR
			if (TYPE_FROM == 7) {
				TYPE_FROM = 1;
			}
			if (TYPE_TO == 7) {
				TYPE_TO = 1;
			}


			var movers_count = calcSettings.min_movers;
			//GET HOW MANY MOVERS
			let weight = !_.isUndefined(total_cf.weight) ? total_cf.weight : total_cf;
			for (var i = 3; i <= 8; i++) {
				if (weight >= calcSettings.mover_size[i]) {
					movers_count = i;
				}
				else {
					if (weight >= calcSettings.mover_size_with_floor[i]
						&& ( TYPE_FROM >= calcSettings.mover_size_floor[i]
							|| TYPE_TO >= calcSettings.mover_size_floor[i] )) {
						movers_count = i;
					}
				}
			}

			return movers_count;
		}


		function getTime(request, total_cf, movers_count) {
			var dispertion = 50;
			var total = {};
			total.work_time = {};
			var TYPE_FROM = request.typeFrom || '1';
			var TYPE_TO = request.typeTo || '1';
			if (request.serviceType == 5 || request.serviceType == 7) {
				TYPE_TO = 1;
			}

			// IF MOVE SIZE IS HOME ENTRANCE TYPE_FROM same as 1 FLOOR
			if (TYPE_FROM == 7) {
				TYPE_FROM = 1;
			}
			if (TYPE_TO == 7) {
				TYPE_TO = 1;
			}

			//GET LOADING Time
			var LOADING_SPEED = calcSettings.speed[movers_count] * (1 - calcSettings.floor_kof[TYPE_FROM] / 100);
			var LOADING_TIME_MIN = ((parseFloat(total_cf.weight) - dispertion ) / LOADING_SPEED) * 0.6;
			var LOADING_TIME_MAX = ((parseFloat(total_cf.weight) + dispertion) / LOADING_SPEED) * 0.6;

			//GET UNLOADING Time
			var UNLOADING_SPEED = calcSettings.speed[movers_count] * (1 - calcSettings.floor_kof[TYPE_TO] / 100);
			var UNLOADING_TIME_MIN = ((parseFloat(total_cf.weight) - dispertion) / UNLOADING_SPEED) * 0.4;
			var UNLOADING_TIME_MAX = ((parseFloat(total_cf.weight) + dispertion) / UNLOADING_SPEED) * 0.4;


			var TOTAL_TIME_MIN = LOADING_TIME_MIN + UNLOADING_TIME_MIN;
			var TOTAL_TIME_MAX = LOADING_TIME_MAX + UNLOADING_TIME_MAX;

			total.work_time.min = getRoundedTime(TOTAL_TIME_MIN);
			total.work_time.max = getRoundedTime(TOTAL_TIME_MAX);

			if (request.serviceType == 3) {
				total.work_time.min = getRoundedTime(LOADING_TIME_MIN);
				total.work_time.max = getRoundedTime(LOADING_TIME_MAX);
			}

			if (request.serviceType == 4) {
				total.work_time.min = getRoundedTime(UNLOADING_TIME_MIN);
				total.work_time.max = getRoundedTime(UNLOADING_TIME_MAX);
			}

			if (total.work_time.min < 0) {
				total.work_time.min = total.work_time.max;
			}

			total.total_cf = total_cf;
			total.movers_count = movers_count;
			total.trucks = getTrucks(total_cf);
			total.min_hours = parseFloat(calcSettings.min_hours);

			return total;
		}

		function getRoundedTime(time) {
			var roundTime = roundTimeInterval(time);
			var result = round(roundTime, 2);
			return result;
		}

		function roundTimeInterval(time) {
			var minutes = 0;
			var hour = Math.floor(time);
			var fraction = time - hour;

			if (fraction > 0 && fraction <= 0.3) {
				minutes = 15 / 60;
			}

			if (fraction > 0.3 && fraction <= 0.55) {
				minutes = 30 / 60;
			}

			if (fraction > 0.55 && fraction <= 0.8) {
				minutes = 45 / 60;
			}

			if (fraction > 0.8) {
				minutes = 0;
				hour++;
			}

			var result = hour + round(minutes, 2);

			return result;
		}

		function round(number, point) {
			var decimal = 10 * point;
			return Math.round(number * decimal) / decimal;
		}


		function getTrucks(total_cf) {
			var trucks = 1;

			if (total_cf.weight >= calcSettings.trucks2) {
				trucks = 2;
			}

			if (total_cf.weight >= calcSettings.trucks3) {
				trucks = 3;
			}

			if (total_cf.weight >= calcSettings.trucks4) {
				trucks = 4;
			}

			if (total_cf.weight >= calcSettings.trucks5) {
				trucks = 5;
			}

			return trucks;
		}


		function getTotalCubicFeet(request) {
			// Get default size of apartment
			var move_size = parseInt(calcSettings.size[request.moveSize]);
			// Get rooms if exist
			var total_room_weight = 0;


			angular.forEach(request.checkedRooms.rooms, function (rid, key) {
				total_room_weight += parseInt(calcSettings.room_size[rid]);
			});


			//Get Total Weight

			var total = {};
			total.weight = total_room_weight + move_size + parseInt(calcSettings.room_kitchen);
			total.min = total.weight - 100;
			total.max = total.weight + 100;

			return total;
		}


		function getRequestCubicFeet(request) {
			var move_size = 0;

			if (angular.isDefined(request.move_size)) {
				move_size = parseInt(calcSettings.size[request.move_size.raw]);
			}

			var total_room_weight = 0;

			if (angular.isDefined(request.rooms)) {
				angular.forEach(request.rooms.value, function (rid, key) {
					total_room_weight += parseInt(calcSettings.room_size[rid]);
				});
			}

			var total = {};
			total.weight = total_room_weight + move_size + parseInt(calcSettings.room_kitchen);
			total.min = total.weight - 100;
			total.max = total.weight + 100;
			return total;
		}


		function getTravelTime(from, to, serviceType) {
			var deferred = $q.defer();

			var data = {
				"zip_from": from,
				"zip_to": to,
				"move_service_type": serviceType
			};

			apiService
				.postData('server/move_distance_calculate/check_travel_time', data)
				.then(function (response) {
					var result = response.data.duration || 0;

					deferred.resolve(result);
				}, function () {
					deferred.reject();
				});


			return deferred.promise;
		}


		/*
		 Must return
		 distance
		 and duration (travel time)
		 */
		function getDuration(from, to, serviceType) {
			var deferred = $q.defer();

			var data = {
				"zip_from": from,
				"zip_to": to,
				"move_service_type": serviceType
			};

			apiService
				.postData('server/move_distance_calculate/check_travel_time', data)
				.then(function (response) {
					deferred.resolve(response.data);
				}, function () {
					deferred.reject();
				});

			return deferred.promise;
		}


		function getFullDistance(from, to) {
			var deferred = $q.defer();

			var data = {
				"zip_from": from,
				"zip_to": to
			};

			apiService
				.postData('server/move_distance_calculate/distance_origin_to_destination', data)
				.then(function (response) {
					deferred.resolve(response.data);
				}, function () {
					deferred.reject();
				});

			return deferred.promise;
		}


		function getRate(date, movers, trucks) {
			var year = parseInt(moment(date).format('YYYY'));
			var date = moment(date).format('YYYY-MM-DD');

			if (angular.isDefined(data.calendar[year])) {
				var cc = data.calendar[year][date];
			} else {
				var cc = 1;
			}

			var type = data.calendartype[cc];

			if (angular.isUndefined(type)) {
				type = 1;
			}

			var rate = ratesSettings[type];

			if (_.isUndefined(rate)) {
				return 0;
			}

			var extramoverRate = ratesSettings[type][3];
			var extratruckRate = ratesSettings[type][4];

			var truckRate = 0;
			var moverRate = 0;
			var extraMoverRate = 0;
			var moversId = movers - 2;

			if (movers > 4) {
				var extraMover = movers - 4;
				var extraMoverRate = extramoverRate * extraMover;
				rate = rate[2];
			} else {
				rate = rate[moversId] || 0;
			}

			if (trucks > 1) {
				var extraTrucks = trucks - 1;
				truckRate = extratruckRate * extraTrucks;
			}

			var totalRate = rate + extraMoverRate + truckRate;

			return totalRate;
		}

		function getAdditionalTruckRate(date, movers, trucks) {
			var year = parseInt(moment(date).format('YYYY'));
			var date = moment(date).format('YYYY-MM-DD');

			if (angular.isDefined(data.calendar[year])) {
				var cc = data.calendar[year][date];
			} else {
				var cc = 1;
			}

			var type = data.calendartype[cc];

			if (angular.isUndefined(type)) {
				type = 1;
			}

			var rate = ratesSettings[type];

			if (_.isUndefined(rate)) {
				return 0;
			}

			var extratruckRate = +ratesSettings[type][4];

			return extratruckRate;
		}

		function getRateAdditionalHelper(date, crew) {
			var calendarYear = parseInt(moment(date).format('YYYY'));
			var calendarDate = moment(date).format('YYYY-MM-DD');
			var cc = 1;
			var additionalCrewMovers = 0;
			var additionalCrewTrucks = 0;
			var type = 1;
			var truckRate = 0;
			var extraMoverRate = 0;

			if (angular.isDefined(data.calendar[calendarYear])) {
				cc = data.calendar[calendarYear][calendarDate];
			}
			if (angular.isDefined(data.calendartype[cc])) {
				type = data.calendartype[cc];
			}
			if (crew) {
				additionalCrewMovers += crew.helpers.length;
				additionalCrewTrucks += crew.trucks;
			}

			var extramoverRate = ratesSettings[type][3];
			var extratruckRate = ratesSettings[type][4];

			extraMoverRate = extramoverRate * additionalCrewMovers;

			if (additionalCrewTrucks) {
				truckRate = extratruckRate * additionalCrewTrucks;
			}

			var totalRate = extraMoverRate + truckRate;

			return totalRate;
		}


		function parseZipToAddress(arrAddress) {
			var city, state, country, postal_code;

			$.each(arrAddress, function (i, address_component) {

				if (address_component.types[0] == "administrative_area_level_1") {// State
					if (address_component.short_name.length < 3) {
						state = address_component.short_name;
					}
				}

				if (address_component.types.indexOf("locality") > -1 || address_component.types.indexOf("neighborhood") > -1 || address_component.types.indexOf("sublocality") > -1) {// locality type
					city = address_component.long_name;
				}

				if (address_component.types[0] == "country") {// locality type
					country = address_component.short_name;
				}

				if (address_component.types[0] == "postal_code") {// "postal_code"
					postal_code = address_component.short_name;
				}
			});

			var address = {
				'city': city,
				'state': state,
				'postal_code': postal_code,
				'country': country,
				'full_adr': city + ', ' + state + ' ' + postal_code
			};

			return address;
		}


		function calcDistance(request) {
			var deferred = $q.defer();

			var extraPickup = false;
			var extraDropoff = false;
			var zip_from = request.field_moving_from.postal_code;
			var zip_to = request.field_moving_to.postal_code;
			var pickup = request.field_extra_pickup.postal_code;
			var delivery = request.field_extra_dropoff.postal_code;
			var serviceType = request.service_type.raw;
			var result = {};

			if (pickup != null) {
				extraPickup = pickup.length ? true : false;
			}

			if (delivery != null) {
				extraDropoff = delivery.length ? true : false;
			}

			if (!extraPickup && !extraDropoff) {
				service.getFullDistance(zip_from, zip_to).then(function (data) {
					deferred.resolve(data);
				});
			} else if (extraPickup) {
				var promise = service.getFullDistance(zip_from, pickup);
				var promise2 = service.getFullDistance(pickup, zip_to);

				$q.all([promise, promise2]).then(function (data) {
					result.duration = data[0].duration + data[1].duration;
					result.distance = data[0].distance + data[1].distance;
					deferred.resolve(result);
				});
			} else if (extraDropoff) {
				var promise = service.getFullDistance(zip_from, delivery);
				var promise2 = service.getFullDistance(delivery, zip_to);

				$q.all([promise, promise2]).then(function (data) {
					result.duration = data[0].duration + data[1].duration;
					result.distance = data[0].distance + data[1].distance;
					deferred.resolve(result);
				});
			} else {
				var promise = service.getFullDistance(zip_from, pickup);
				var promise2 = service.getFullDistance(pickup, delivery);
				var promise3 = service.getFullDistance(delivery, zip_to);

				$q.all([promise, promise2, promise3]).then(function (data) {
					result.duration = data[0].duration + data[1].duration + data[2].duration;
					result.distance = data[0].distance + data[1].distance + data[2].distance;
					deferred.resolve(result);
				});
			}

			return deferred.promise;
		}


		function calculateStorageRequest(initial_request, storageRequest, editrequest) {
			let deferred = $q.defer();
			let requestFromStorage = {};
			let requestToStorage = {};
			let request = initial_request;

			if (storageRequest) {
				request = storageRequest;
			}

			let basicSettings = angular.fromJson($rootScope.fieldData.basicsettings);
			let calcSettings = angular.fromJson($rootScope.fieldData.calcsettings);
			let minCATravelTime = basicSettings.minCATavelTime ? getRoundedTime(basicSettings.minCATavelTime / 60) : 0;
			let services = [1, 2, 3, 4, 6, 8];

			angular.copy(request, requestFromStorage);
			angular.copy(initial_request, requestToStorage);

			requestToStorage.zipFrom = initial_request.field_moving_from.postal_code;
			requestToStorage.typeFrom = initial_request.type_from.raw;
			requestToStorage.typeTo = 1;
			requestToStorage.type_to.raw = 1;
			requestToStorage.zipTo = basicSettings.parking_address;

			requestFromStorage.zipTo = initial_request.field_moving_to.postal_code;
			requestFromStorage.typeTo = initial_request.type_to.raw;
			requestFromStorage.typeFrom = 1;
			requestFromStorage.type_from.raw = 1;
			requestFromStorage.zipFrom = basicSettings.parking_address;
			let serviceType = initial_request.service_type.raw;
			let isDoubleDriveTime = calcSettings.doubleDriveTime && services.indexOf(parseInt(serviceType)) >= 0;

			requestFromStorage.duration.value = 0;
			requestToStorage.duration.value = 0;

			let promise1 = {};
			let promise2 = {};

			if (calcSettings.isTravelTimeSameAsDuration) {
				promise1 = getDuration(requestToStorage.zipFrom, requestToStorage.zipTo, serviceType); //FROM ADDRESS TO STORAGE
				promise2 = getDuration(requestFromStorage.zipFrom, requestFromStorage.zipTo, serviceType); // FROM STORAGE TO ADDRESS
			} else {
				promise1 = getDuration(requestToStorage.zipFrom, requestToStorage.zipFrom, serviceType); //FROM ADDRESS TO STORAGE
				promise2 = getDuration(requestFromStorage.zipTo, requestFromStorage.zipTo, serviceType); // FROM STORAGE TO ADDRESS
			}

			let promise3 = getFullDistance(requestToStorage.zipFrom, requestToStorage.zipTo);
			let promise4 = getFullDistance(requestFromStorage.zipFrom, requestFromStorage.zipTo);

			$q.all([promise1, promise2, promise3, promise4]).then(function (data) {
				let editrequestTo = {};
				let editrequestFrom = {};

				angular.copy(editrequest, editrequestTo);
				angular.copy(editrequest, editrequestFrom);

				if (requestToStorage.field_extra_dropoff.postal_code != null) {
					editrequestFrom['field_extra_dropoff'] = requestToStorage.field_extra_dropoff;
					editrequestTo['field_extra_dropoff'] = requestToStorage.field_extra_dropoff;
					editrequestTo['field_extra_dropoff']['postal_code'] = '';
				}

				editrequestFrom['field_travel_time'] = data[1]['duration'];
				editrequestFrom['field_distance'] = data[1]['distance'];
				editrequestFrom['field_duration'] = data[3]['duration'];

				editrequestTo['field_travel_time'] = data[0]['duration'];
				editrequestTo['field_distance'] = data[0]['distance'];
				editrequestTo['field_duration'] = data[2]['duration'];

				requestFromStorage.travel_time.raw = data[1]['duration'];
				requestFromStorage.distance.value = data[1]['distance'];
				requestFromStorage.duration.value = data[3]['duration'];
				requestToStorage.travel_time.raw = data[0]['duration'];
				requestToStorage.distance.value = data[0]['distance'];
				requestToStorage.duration.value = data[2]['duration'];

				if (isDoubleDriveTime) {
					requestToStorage.field_double_travel_time = Math.max(minCATravelTime, data[2]['duration'] * 2);
					requestFromStorage.field_double_travel_time = Math.max(minCATravelTime, data[3]['duration'] * 2);
					requestToStorage.field_double_travel_time = getRoundedTime(requestToStorage.field_double_travel_time);
					requestFromStorage.field_double_travel_time = getRoundedTime(requestFromStorage.field_double_travel_time);
				}

				let datas = {
					To: {},
					From: {}
				};

				datas.To = calculateStorage(requestToStorage, editrequestTo);
				datas.From = calculateStorage(requestFromStorage, editrequestFrom);

				if (requestFromStorage.service_type.raw == 6) {
					requestFromStorage.date.raw = moment.unix(requestFromStorage.date.raw).add(1, 'd').unix();
				} else {
					requestFromStorage.date.raw = moment.unix(requestFromStorage.date.raw).add(2, 'd').unix();
				}

				if (!storageRequest) {
					let allDataFromStorage = angular.copy(initial_request.request_all_data);
					let allDataToStorage = angular.copy(initial_request.request_all_data);

					if (_.isUndefined(allDataToStorage.toStorage)) {
						allDataToStorage.toStorage = false;
						allDataFromStorage.toStorage = true;
					} else {
						allDataFromStorage.toStorage = !allDataToStorage.toStorage;
					}

					datas.To.all_data = allDataToStorage;
					datas.From.all_data = allDataFromStorage;
				}

				deferred.resolve(datas);
			}, function (error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}


		function calculateStorage(request, editrequest) {
			EditRequestServices.updateRequestLive(request, editrequest);
			var total_weight = getRequestCubicFeet(request);
			var inventory_weight = InventoriesServices.calculateTotals(request.inventory.inventory_list);

			if (inventory_weight.cfs > 0) {
				total_weight.weight = inventory_weight.cfs;
			}

			var results = calculateStorageRequestTame(request, total_weight.weight);

			request.rate.value = results.rate.value;
			request.crew.value = results.crew.value;
			request.maximum_time.raw = results.maximum_time.raw;
			request.minimum_time.raw = results.minimum_time.raw;

			editrequest['field_price_per_hour'] = results.rate.value;
			editrequest['field_movers_count'] = results.crew.value;
			editrequest['field_maximum_move_time'] = results.maximum_time.raw;
			editrequest['field_minimum_move_time'] = results.minimum_time.raw;

			return {
				request: request,
				editrequest: editrequest
			}
		}


		function calculateStorageRequestTame(request, weight) {
			var total_cf = {};

			total_cf.weight = weight;
			request.typeFrom = request.type_from.raw;
			request.typeTo = request.type_to.raw;
			request.serviceType = request.service_type.raw;

			var calcResults = calculateTime(request, total_cf);
			var min_hours = calcResults.min_hours;
			var moversCount = calcResults.movers_count;
			var trucksCount = calcResults.trucks;
			var minWorkTime = calcResults.work_time.min;
			var maxWorkTime = calcResults.work_time.max;

			if (!calcSettings.doubleDriveTime) {
				minWorkTime += parseFloat(request.duration.value);
				maxWorkTime += parseFloat(request.duration.value);
			}

			request.minimum_time.raw = minWorkTime;
			request.minimum_time.value = common.decToTime(minWorkTime);

			request.maximum_time.raw = maxWorkTime;
			request.maximum_time.value = common.decToTime(maxWorkTime);

			request.crew.value = moversCount;

			request.truckCount = getTrucks(weight);

			return request;
		}

		function distanceRequestCalcForm(distance, equipment_fee) {
			var distance = parseFloat(distance);
			var amount = 0;
			var isExistVariant = false;

			angular.forEach(equipment_fee.by_mileage, function (value) {
				if ((value.from <= distance) && (distance <= value.to)) {
					amount = value.amount;
					isExistVariant = true;
				}
			});

			return {amount: amount, isExist: isExistVariant};
		}
		// end only for calculator form

		function getOvernightRate(pairedRequest) {
			var result = undefined;

			// index 1 is only for overnight rate extra service
			var index = _.findIndex(pairedRequest.extraServices, {index: 1});

			if (index >= 0) {
				result = pairedRequest.extraServices[index];
			}

			return result;
		}

		function _paymentCalc(user, uid, swObj, swId) {
			if (globalDataForPayroll.isFlatFee && globalDataForPayroll.pricePerHelper) {
				switch (globalDataForPayroll.flatFeeOpt) {
					case 'hour':
						user.hours_to_pay = globalDataForPayroll.pricePerHelper[uid] ? globalDataForPayroll.pricePerHelper[uid] : 0;
						break;
					case 'money':
						user.money_to_pay = globalDataForPayroll.pricePerHelper[uid] ? globalDataForPayroll.pricePerHelper[uid] : 0;
						user.money_to_pay = _.round(user.money_to_pay, 2);
						break;
					case 'percent':
						user.money_to_pay = globalDataForPayroll.pricePerHelper[uid]
							? (globalDataForPayroll.totalCost * globalDataForPayroll.pricePerHelper[uid]) / 100
							: 0;
						break;
				}
			} else if (globalDataForPayroll.workTimesForSwitchers) {
				if (globalDataForPayroll.workTimesForSwitchers.hasOwnProperty(swId)) {
					swObj.hours_to_pay = globalDataForPayroll.workTimesForSwitchers[swId];
					user.hours_to_pay = globalDataForPayroll.dividedFee ? globalDataForPayroll.dividedFee - globalDataForPayroll.workTimesForSwitchers[swId] : globalDataForPayroll.workDuration - globalDataForPayroll.workTimesForSwitchers[swId];
				} else if (globalDataForPayroll.dividedFee) {
					user.hours_to_pay = globalDataForPayroll.dividedFee;
				} else {
					user = calculateBasicHoursPay(user, uid);
				}
			} else if (globalDataForPayroll.dividedFee) {
				user.hours_to_pay = globalDataForPayroll.dividedFee;
			} else {
				user = calculateBasicHoursPay(user, uid);
			}
			return user;
		}

		function calculateBasicHoursPay(user, uid) {
			let isAdditional = false;
			if (crews && additionalCrews) {
				for (let i = 0; i < additionalCrews.length; i++) {
					if (additionalCrews[i].helpers) {
						isAdditional = calculateByType(user, uid, 'helpers', i);
					}

					if (additionalCrews[i].foremans) {
						isAdditional = calculateByType(user, uid, 'foremans', i);
					}
				}
			}
			if (!isAdditional && !user.hours_to_pay) {
				user.hours_to_pay = globalDataForPayroll.workDuration;
			}

			return user;
		}

		function calculateByType(user, uid, type, index) {
			let isAdditional = false;

			for (let j = 0; j < additionalCrews[index][type].length; j++) {
				if (uid == additionalCrews[index][type][j]) {
					user.additional = 1;

					let hoursToPay = _.get(globalDataForPayroll, 'workDuration', 0);

					if (_.get(globalDataForPayroll, `data.crews[${index + 1}]`)) {
						hoursToPay = getCrewDurationPayroll(globalDataForPayroll.data.crews, index + 1) + _.round(globalDataForPayroll.time, 2);
					}

					user.hours_to_pay = hoursToPay;
					isAdditional = true;
					break;
				}
			}

			return isAdditional;
		}

		function _calculate(user, uid, swObj, swId) {
			user.hours_to_pay = 0;
			user.money_to_pay = 0;
			user.switcher = globalDataForPayroll.switchers && globalDataForPayroll.switchers.hasOwnProperty(uid) ? globalDataForPayroll.switchers[uid] : 0;
			user.additional = 0;

			return _paymentCalc(user, uid, swObj, swId);
		}

		function _getForeman(payrollReq) {
			if (!globalDataForPayroll.request_data || !crews) {
				return {};
			}
			var foremanId = crews.foreman;
			if (globalDataForPayroll.type == CALCULATOR.PICK_UP) {
				if (!crews.pickedUpCrew) {
					return {};
				}

				foremanId = crews.pickedUpCrew.foreman;
			} else if (globalDataForPayroll.type == CALCULATOR.DELIVERY) {
				if (!crews.deliveryCrew) {
					return {};
				}

				foremanId = crews.deliveryCrew.foreman;
			}
			payrollReq.foreman[foremanId] = {};
			if (globalDataForPayroll.switchers) {
				if (globalDataForPayroll.switchers.hasOwnProperty(foremanId)) {
					var to = globalDataForPayroll.switchers[foremanId];

					payrollReq.foreman[to] = {};
					payrollReq.foreman[to].hours_to_pay = 0;
					payrollReq.foreman[to].money_to_pay = 0;
					payrollReq.foreman[to].switcher = 0;
					payrollReq.foreman[to].additional = 0;

					_calculate(payrollReq.foreman[foremanId], foremanId, payrollReq.foreman[to], to);
				} else {
					_calculate(payrollReq.foreman[foremanId], foremanId);

				}
			} else {
				_calculate(payrollReq.foreman[foremanId], foremanId);
			}
			return payrollReq.foreman;
		}

		function _getHelpers(payrollReq) {
			let crewType = '';

			if (globalDataForPayroll.type == CALCULATOR.PICK_UP) {
				if (crews && crews.pickedUpCrew) {
					crewType = CALCULATOR.PICKED_UP_CREW;
				}
			} else if (globalDataForPayroll.type == CALCULATOR.DELIVERY) {
				if (crews && crews.deliveryCrew) {
					crewType = CALCULATOR.DELIVERY_CREW;
				}
			} else {
				if (crews && crews.baseCrew) {
					crewType = CALCULATOR.BASE_CREW;
				}
			}

			if (crewType != '') {
				angular.forEach(crews[crewType].helpers, function (helperUid, b) {
					if (helperUid) {
						payrollReq.helper[helperUid] = {};
						if (globalDataForPayroll.switchers) {
							if (globalDataForPayroll.switchers.hasOwnProperty(helperUid)) {
								var to = globalDataForPayroll.switchers[helperUid];

								payrollReq.helper[to] = {};
								payrollReq.helper[to].hours_to_pay = 0;
								payrollReq.helper[to].money_to_pay = 0;
								payrollReq.helper[to].switcher = 0;
								payrollReq.helper[to].additional = 0;
								_calculate(payrollReq.helper[helperUid], helperUid, payrollReq.helper[to], to);
							} else {
								_calculate(payrollReq.helper[helperUid], helperUid);
							}
						} else {
							_calculate(payrollReq.helper[helperUid], helperUid);
						}
					}
				});
			}
			if (crews && additionalCrews) {
				for (var i = 0, l = additionalCrews.length; i < l; i++) {
					var reqCrew = additionalCrews[i];
					angular.forEach(reqCrew.helpers, function (helperUid, b) {
						if (helperUid) {
							payrollReq.helper[helperUid] = {};
							if (!!globalDataForPayroll.switchers && globalDataForPayroll.switchers.hasOwnProperty(helperUid)) {
								payrollReq.helper[globalDataForPayroll.switchers[helperUid]] = {};
								_calculate(payrollReq.helper[globalDataForPayroll.switchers[helperUid]], globalDataForPayroll.switchers[helperUid]);
							}
							_calculate(payrollReq.helper[helperUid], helperUid)
						}
					});
				}
			}
			return payrollReq.helper;
		}

		function generateWorkerPayroll(request_data, switchers, workTimesForSwitchers, workDuration, dividedFeeJob, totalCost, time, type, data) {
			var payrollReq = {
				foreman: {},
				helper: {}
			};
			let pricePerHelper = 0;
			let flatFeeOpt = '';
			let isFlatFee = request_data.paymentType == 'FlatFee';
			let dividedFee = dividedFeeJob;
			if (isFlatFee) {
				pricePerHelper = request_data.pricePerHelper;
				flatFeeOpt = request_data.selectedFlatFeeOption.baseCrew;

				if (dividedFee && request_data.crews && request_data.crews.baseCrew && request_data.crews.baseCrew.helpers && flatFeeOpt == 'money') {
					dividedFee = (totalCost / (request_data.crews.baseCrew.helpers.length + 1)).toFixed(2);
				}
			}
			globalDataForPayroll = {
				workTimesForSwitchers: workTimesForSwitchers,
				pricePerHelper: pricePerHelper,
				flatFeeOpt: flatFeeOpt,
				isFlatFee: isFlatFee,
				dividedFee: dividedFee,
				totalCost: totalCost,
				data: data,
				request_data: request_data,
				time: time,
				switchers: switchers,
				type: type,
				workDuration: workDuration,
			};

			additionalCrews = _.get(globalDataForPayroll, 'request_data.crews.additionalCrews');
			crews = _.get(globalDataForPayroll, 'request_data.crews');

			payrollReq.foreman = _getForeman(payrollReq);
			payrollReq.helper = _getHelpers(payrollReq);

			return payrollReq;
		}

		function getCrewDurationPayroll(crews, index = 0) {
			const MIN_HOUR_RATE = parseFloat(calcSettings.min_hours);
			let duration = 0;

			for (let i = 0; i < crews.length; i++) {
				if (i < index) {
					continue;
				}

				if (i == 0 && crews[0].workDuration < MIN_HOUR_RATE) {
					duration = MIN_HOUR_RATE;
				} else {
					duration += crews[i].workDuration;
				}
			}

			return duration;
		}
	}
