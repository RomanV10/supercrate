describe('calculate.services', () => {
	let $state, $scope, CalculatorServices, moment_lib, frontpage, valuationService, get_valuation_plan_mock;
	
	beforeEach(() => {
		get_valuation_plan_mock = testHelper.loadJsonFile('valuation.service_get_valuation_plan.mock');
		$rootScope.fieldData.valuation_plan_tables = get_valuation_plan_mock;
	});
	
	beforeEach(inject(function (_$state_, _CalculatorServices_, _datacontext_, _testHelperService_, _moveBoardApi_,
		_RequestServices_, _moment_) {
		CalculatorServices = _CalculatorServices_;
		$state = _$state_;
		$scope = $rootScope.$new();
		moment_lib = _moment_;
		testHelper = _testHelperService_;
		moveBoardApi = _moveBoardApi_;
		RequestServices = _RequestServices_;
		frontpage = testHelper.loadJsonFile('ld-discounts-frontpage.mock');
		CalculatorServices.init(frontpage);
		$scope.$apply();
	}));
	
	describe('function checkZipValidForLD(),', () => {
		let longdistance, stateFrom, stateTo, areaCode;
		beforeEach(() => {
			longdistance = CalculatorServices.getLinkToLdSettings();
			stateFrom = 'MA';
			stateTo = 'ca';
			areaCode = '323';
		});
		it('When all fields valid should return true', () => {
			expect(CalculatorServices.checkZipValidForLD(stateFrom, stateTo, areaCode)).toBeTruthy();
		});
		it('When move to this state unchecked should return false', () => {
			longdistance.stateRates[stateFrom][stateTo].longDistance = false;
			expect(CalculatorServices.checkZipValidForLD(stateFrom, stateTo, areaCode)).toBeFalsy();
		});
		it('When state_rate <= 0 should return false', () => {
			longdistance.stateRates[stateFrom][stateTo].state_rate = 0;
			expect(CalculatorServices.checkZipValidForLD(stateFrom, stateTo, areaCode)).toBeFalsy();
		});
		it('When accept all quotes true should return true', () => {
			longdistance.acceptAllQuotes = true;
			longdistance.stateRates[stateFrom][stateTo].state_rate = 0;
			expect(CalculatorServices.checkZipValidForLD(stateFrom, stateTo, areaCode)).toBeTruthy();
		});
		it('When state_rate <= 0 but rate for area code exist should return true', () => {
			longdistance.stateRates[stateFrom][stateTo].state_rate = 0;
			longdistance.stateRates[stateFrom][stateTo].rate[areaCode] = 10;
			expect(CalculatorServices.checkZipValidForLD(stateFrom, stateTo, areaCode)).toBeTruthy();
		});
	});
	
	describe('getLongDistanceRate', () => {
		it('too small weight for discount', () => {
			let smallRequest = testHelper.loadJsonFile('ld-dicounts-too-small-request-on-creation.mock');
			expect(CalculatorServices.getLongDistanceRate(smallRequest)).toBe(10);
		});
	});
	
	describe('getLongDistanceQuote', () => {
		it('quote should be $6,660.00 (W/o inventory)', () => {
			let LD_REQUEST = testHelper.loadJsonFile('ld-commercial-without-inventory.mock');
			expect(CalculatorServices.getLongDistanceQuote(LD_REQUEST)).toBe(6660);
		});
		
		it('quote should be $4,200.00 (W/Inventory)', () => {
			let LD_REQUEST = testHelper.loadJsonFile('ld-commercial-with-inventory.mock');
			expect(CalculatorServices.getLongDistanceQuote(LD_REQUEST)).toBe(4200);
		});
	});
	
	describe('checkExperation', () => {
		let request;
		let today;
		
		beforeEach(() => {
			today = moment('2018-03-05');
			request = testHelper.loadJsonFile('check-expiration/request.mock');
			frontpage = testHelper.loadJsonFile('check-expiration/frontpage.mock');
			
			spyOn(window, 'moment').and.callFake(function (parameter) {
				if (parameter) {
					return moment_lib(parameter);
				} else {
					return today;
				}
			});
		});
		
		describe('should not expire,', () => {
			beforeEach(() => {
				frontpage.schedulesettings.ExperationReqType = 'morning';
				frontpage.schedulesettings.ExperationBeforeTime = '14';
				CalculatorServices.init(frontpage);
			});
			
			it('when morning start time request and move date before today', () => {
				//given
				let isExpire;
				
				//when
				isExpire = CalculatorServices.checkExperation(request);
				
				//then
				expect(isExpire).toBeFalsy();
			});
			
			it('when not morning start time request and today before move date', () => {
				//given
				let isExpire;
				request.date.value = '03/06/2018';
				request.start_time1.value = '11:00 AM';
				
				//when
				isExpire = CalculatorServices.checkExperation(request);
				
				//then
				expect(isExpire).toBeFalsy();
			});
			
			it('when morning start time request and today before move date and current time earlier then selected in settings', () => {
				//given
				let isExpire;
				request.date.value = '03/06/2018';
				request.start_time1.value = '11:00 AM';
				today.set('hour', 10);
				
				//when
				isExpire = CalculatorServices.checkExperation(request);
				
				//then
				expect(isExpire).toBeFalsy();
			});
			
			it('when selected all start time request and today before move date and current time earlier then selected in settings', () => {
				//given
				let isExpire;
				frontpage.schedulesettings.ExperationReqType = 'all';
				CalculatorServices.init(frontpage);
				
				request.date.value = '03/06/2018';
				request.start_time1.value = '11:00 AM';
				today.set('hour', 10);

				//when
				isExpire = CalculatorServices.checkExperation(request);

				//then
				expect(isExpire).toBeFalsy();
			});
			
			it('when selected morning start time request and today before move date and current time later then selected in settings', () => {
				//given
				let isExpire;
				request.date.value = "03/06/2018";
				request.start_time1.value = "9:00 PM";
				today.set('hour', 15);
				
				//when
				isExpire = CalculatorServices.checkExperation(request);
				
				//then
				expect(isExpire).toBeFalsy();
			});
		});
		
		describe('should expire', () => {
			beforeEach(() => {
				frontpage.schedulesettings.ExperationReqType = 'morning';
				frontpage.schedulesettings.ExperationBeforeTime = '14';
				CalculatorServices.init(frontpage);
			});
			
			describe('when expiration time morning,', () => {
				it('when morning start time request and current time same as in settings', () => {
					//given
					let isExpire;
					request.date.value = '03/06/2018';
					request.start_time1.value = '9:00 AM';
					today.set('hour', 14);
					
					//when
					isExpire = CalculatorServices.checkExperation(request);
					
					//then
					expect(isExpire).toBeTruthy();
				});
			});
			
			describe('expiration time setting all,', () => {
				it('expiration time setting all and current time same as in settings', () => {
					//given
					let isExpire;
					request.date.value = '03/06/2018';
					request.start_time1.value = '15:00 AM';
					today.set('hour', 14);
					frontpage.schedulesettings.ExperationReqType = 'all';
					frontpage.schedulesettings.ExperationBeforeTime = '14';
					CalculatorServices.init(frontpage);
					
					//when
					isExpire = CalculatorServices.checkExperation(request);
					
					//then
					expect(isExpire).toBeTruthy();
				});
			});
			
			it('when today is move date', () => {
				//given
				let isExpire;
				request.date.value = '03/05/2018';
				
				//when
				isExpire = CalculatorServices.checkExperation(request);
				
				//then
				expect(isExpire).toBeTruthy();
			});
			
			it('when today is after move date', () => {
				//given
				let isExpire;
				request.date.value = '03/01/2018';
				
				//when
				isExpire = CalculatorServices.checkExperation(request);
				
				//then
				expect(isExpire).toBeTruthy();
			});
		});
	});
	describe('getRequestWeightBasedOnMoveSize()', () => {
		let request;
		beforeEach(function () {
			request = angular.copy(testHelper.loadJsonFile('valuation/calculate-service-valuation-request-with-inventory.mock'));
		});
		it('when commercial move and field_useweighttype = 1', () => {
			let weight = CalculatorServices.getRequestWeightBasedOnMoveSize(request);
			expect(weight).toBe(420);
		});
		it('when field_useweighttype = 1 and not LD', () => {
			request.move_size.raw = 1;
			request.field_useweighttype.value = 1;
			request.service_type.raw = 1;
			let weight = CalculatorServices.getRequestWeightBasedOnMoveSize(request);
			expect(weight).toBe(175);
		});
		it('when LD and min_weight is greater then actual weight', () => {
			request.field_useweighttype.value = 2;
			request.inventory_weight.cfs = 100;
			let weight = CalculatorServices.getRequestWeightBasedOnMoveSize(request);
			expect(weight).toBe(200);
		});
	});
});
