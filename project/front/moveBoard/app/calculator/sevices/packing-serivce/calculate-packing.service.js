angular
	.module('app.calculator')
	.factory('calculatePackingService', calculatePackingService);

/* @ngInject */
function calculatePackingService(datacontext) {
	const FLAT_RATE_SERVICE_TYPE = 5;
	const LONG_DISTANCE_SERVICE_TYPE = 7;
	let service = {
		calculateTotalCharge,
		calculatePackingTotal,
		reCalculatePackingsTotal,
		updateRequestDataPackings,
	};

	return service;

	function calculateTotalCharge (quantity, rate, labor) {
		let result = 0;

		if (angular.isDefined(labor)) {
			result = Number(quantity) * (Number(rate) + Number(labor));
		} else {
			result = Number(quantity) * Number(rate);
		}

		return _.round(result, 2);
	}

	function calculatePackingTotal(packing, serviceType) {
		serviceType = +serviceType;
		let fieldData = datacontext.getFieldData();
		let settings = angular.fromJson(fieldData.basicsettings);
		let isLaborPacking = _.get(settings, 'packing_settings.packingLabor', false);
		let result = 0;

		if (serviceType !== FLAT_RATE_SERVICE_TYPE && serviceType !== LONG_DISTANCE_SERVICE_TYPE) {
			result = calculateTotalCharge(packing.quantity, packing.rate);
		} else {
			if (isLaborPacking) {
				result = calculateTotalCharge(packing.quantity, packing.LDRate, packing.laborRate);
			} else {
				result = calculateTotalCharge(packing.quantity, packing.LDRate);
			}
		}

		return result;
	}

	function reCalculatePackingsTotal(packings, serviceType) {
		angular.forEach(packings, (packing) => {
			packing.total = calculatePackingTotal(packing, serviceType);
		});
	}

	function updateRequestDataPackings(request) {
		let request_data = {};

		if (request.request_data.value.length) {
			let temp = angular.fromJson(request.request_data.value);

			if (angular.isObject(temp) && !angular.isArray(temp)) {
				request_data = temp;
			}
		} else if (_.isObject(request.request_data.value) && !_.isArray(request.request_data.value)) {
			request_data = request.request_data.value;
		}

		let packings = [];

		if (!_.isEmpty(request_data)) {
			packings = request_data.packings || [];
		}

		reCalculatePackingsTotal(packings, request.service_type.raw);

		request_data.packings = packings;
	}
}
