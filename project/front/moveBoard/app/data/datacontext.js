'use strict';

angular
	.module('app.data')
	.factory('datacontext', datacontext);

/*@ngInject*/
function datacontext($http, $rootScope, common, config, InitSettingsFactory) {
	$rootScope.settings = {};
	$rootScope.availableFlags = {};
	var defaultFlags = ['teamAssigned', 'ConfirmedbyCustomer', 'Completed'];
	var modalInstances = {};
	var $q = common.$q;

	var service = {
		// functions
		init: init,
		retreaveFieldsData: retreaveFieldsData,
		saveFieldData: saveFieldData,
		getFieldData: getFieldData,
		getSettingsData: getSettingsData,
		getSettings: getSettings,
		saveModalInstance: saveModalInstance,
		getModalInstance: getModalInstance,
		getAvailableFlags: getAvailableFlags,
		createFlags: createFlags,
		getModalInstances: getModalInstances,
		removeModalInstance: removeModalInstance,
		saveFieldDataType: saveFieldDataType
	};

	return service;

	function init() {
		var deferred = $q.defer();
		var dataPromise = retreaveFieldsData();
		var flagsPromise = getAvailableFlags();

		$q.all([dataPromise, flagsPromise])
			.then(function () {
				deferred.resolve();
			});

		return deferred.promise;
	}

	function removeModalInstance(nid) {
		delete modalInstances[nid];
	}

	function getModalInstance(nid) {
		return modalInstances[nid];
	}

	function getModalInstances() {
		return modalInstances;
	}

	function saveModalInstance(nid, modal) {
		modalInstances[nid] = modal;
	}

	function retreaveFieldsData() {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/front/frontpage')
			.success(function (data) {
				InitSettingsFactory.parseSettingsData(data);
				service.saveFieldData(data);
				deferred.resolve(data);
			})
			.error(function (data) {
				// data.success = false;
				deferred.reject(data);
			});

		return deferred.promise;
	}


	function getSettings(type) {
		var deferred = $q.defer();
		var setting = {};
		setting.name = type;

		$http.post(config.serverUrl + 'server/system/get_variable', setting)
			.success(function (data) {
				//saveSettingsData(data,setting.name);
				deferred.resolve(data);
			})
			.error(function (data) {
				// data.success = false;
				deferred.reject(data);
			});

		return deferred.promise;
	}


	function getAvailableFlags() {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/move_request/get_flags')
			.success(function (data) {
				if (_.isEmpty(data) || isNullFlags(data.company_flags)) {
					createFlags(defaultFlags)
						.then(function () {
							getAvailableFlags();
						});
				} else {
					$rootScope.availableFlags.company_flags = _.invert(data.company_flags);
				}
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function isNullFlags(data) {
		if (data.length == 0) {
			return true;
		}
		for (var key in data) {
			if (data[key] == null || data[key] == undefined) {
				return true;
			}
		}
		return false;
	}

	function createFlags(flags) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/move_request/create_flag', {
			value: flags,
			type: 'company_flags'
		})
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}


	function saveFieldData(data) {
		$rootScope.fieldData = data;
	}

	function saveFieldDataType(type, data) {
		InitSettingsFactory.correctMergeSettings($rootScope.fieldData, type, data);
	}

	function getSettingsData() {
		return $rootScope.settings;
	}

	function getFieldData() {
		return $rootScope.fieldData;
	}
}
