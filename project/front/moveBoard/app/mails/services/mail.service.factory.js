(function () {

    angular
        .module('app.mail')
        .factory('MailFactory', MailFactory);

    function MailFactory() {

        var service = {};

        service.navigation = {
            folders: [
                { title: 'Inbox', icon: 'fa fa-inbox'},
                //{ title: 'Starred', icon: 'fa fa-star-o'},
                { title: 'Sent', icon: 'fa fa-envelope-o'},
                { title: 'Drafts', icon: 'fa fa-pencil-square-o'},
                { title: 'Junk', icon: 'fa fa-bookmark-o'},
                { title: 'Trash', icon: 'fa fa-trash-o'}
            ],
            active: undefined
        };
        service.current = {
            mail: {},
            folder: {}
        };

        service.destroy = destroy;

        function destroy () {
            service = null;
        }

        return service;
    }

})();