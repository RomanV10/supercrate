(function () {

    angular
        .module('app.mail')
        .factory('MailService', MailService);

    MailService.$inject = ['$http', '$q', 'config'];

    function MailService($http,$q,config) {

        var service = {};

        service.createFolder = createFolder;
        service.removeFolder = removeFolder;

        service.sendMail = sendMail;
        service.getFolders = getFolders;
        service.getByFolder = getByFolder;
        service.getMailById = getMailById;
        service.changeReadStatus = changeReadStatus;
        service.deleteMsg = deleteMsg;

        return service;

        function createFolder(name)
        {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/mail/create_folder',
                data: {
                    name: name
                }
            }).then(function(data) {
                defer.resolve(data);
            }, function(reason) {
                defer.reject(reason);
            });
            return defer.promise;
        }

        function removeFolder(id)
        {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/mail/delete_folder',
                data: {
                    fid: id
                }
            }).then(function(data) {
                defer.resolve(data);
            }, function(reason) {
                defer.reject(reason);
            });
            return defer.promise;
        }

        function sendMail(composeMsg)
        {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/mail',
                data: {
                    data: composeMsg
                }
            }).then(function(data) {
                defer.resolve(data);
            }, function(reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function getFolders()
        {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/mail/get_folders'
            }).then(function(data) {
                defer.resolve(data);
            }, function(reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function getByFolder(filter)
        {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: config.serverUrl + 'server/mail',
                params: filter
            }).then(function(data) {
                defer.resolve(data);
            }, function(reason) {
                defer.reject(reason);
            });
            return defer.promise;
        }

        function getMailById(id)
        {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/mail/' + id
            }).then(function(data) {
                defer.resolve(data);
            }, function(reason) {
                defer.reject(reason);
            });
            return defer.promise;
        }

        function changeReadStatus(id)
        {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/mail/change_read/' + id
            }).then(function(data) {
                defer.resolve(data);
            }, function(reason) {
                defer.reject(reason);
            });
            return defer.promise;
        }

        function deleteMsg(id)
        {
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: config.serverUrl + 'server/mail/' + id
            }).then(function(data) {
                defer.resolve(data);
            }, function(reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }


    }

})();