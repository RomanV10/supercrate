(function() {

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('mails', {
                        url: '/mails',
	                    template: require('./templates/mails-page.html'),
                        title: 'Mails',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
            }
        ]
    );
})();
