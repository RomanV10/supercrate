(function () {
    'use strict';

    angular
        .module('app.mail')
        .controller('MailsController', MailsController);

    MailsController.$inject = ['$scope', 'MailService', 'MailFactory', 'SweetAlert', 'ClientsServices'];

    function MailsController ($scope, MailService, MailFactory, SweetAlert, ClientsServices) {

        var vm = this;
        vm.busy = true;
        vm.showCompose = false;
        vm.isAvailable = true;

        vm.navigation = MailFactory.navigation;
        vm.current = MailFactory.current;
        vm.composeMsg = {};

        vm.setFolder = setFolder;
        vm.deleteMsg = deleteMsg;
        vm.printMsg  = printMsg;
        vm.send  = send;
        vm.save  = save;


        //***** @Constructor
        MailService.getFolders()
            .then(function (response) {
                var inboxIndex = 0;
                if (_.isArray(response.data)) {
                    vm.isAvailable = false;
                }
                else {
                    angular.forEach(vm.navigation.folders, function (folderObj, i) {
                        angular.forEach(response.data, function (folder, id) {
                            if (folderObj.title.toLowerCase() == folder.name.toLowerCase()) {
                                vm.navigation.folders[i]['id'] = id;
                                vm.navigation.folders[i]['unread'] = folder.unread;
                            }
                        });
                        if (folderObj.title.toLowerCase() == 'inbox') {
                            vm.navigation.active = inboxIndex;
                            vm.current.folder = vm.navigation.folders[inboxIndex];
                        }
                        inboxIndex++;
                    });
                }

            })
            .finally(function () {
                vm.busy = false;
            });

        function setFolder (index, folder) {
            vm.navigation.active = index;
            vm.current.folder = folder;
            vm.current.mail = {};
        }

        function send() {

            vm.busy = true;
            if (vm.composeMsg.subject && vm.composeMsg.to) {
                MailService.sendMail(vm.composeMsg)
                    .then (function () {
                        vm.composeMsg = {};
                        vm.showCompose = false;
                        toastr.success('Success!', 'Your Email successfully sent!');
                    }, function () {
                        SweetAlert.swal('Error!', 'Your Email not sent, please check all fields and try later', 'error');
                    })
                    .finally (function () {  vm.busy = false; });
            }

        }

        function save() {
            //save in drafts
            vm.showCompose = false;
        }

        function deleteMsg (id) {
            MailService.deleteMsg(id)
                .then(function() {
                    toastr.success('Success!', 'Email successfully deleted');
                }, function() {
                    SweetAlert.swal("Error!", "Sorry, but Something going wrong. Try again later.", "error");
                });
        }

        function printMsg () {
            var prtContent = angular.element($('.view-mail-body'));
            var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.html());
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }

        $scope.$on('$destroy', function() {
            MailFactory.destroy();
            vm = null;
        });

    }

})();
    