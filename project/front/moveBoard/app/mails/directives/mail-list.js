(function () {
    'use strict';

    angular
        .module('app.mail')
        .directive('ccMailDataList', ccMailDataList);


    ccMailDataList.$inject = ['MailService', 'MailFactory'];

    function ccMailDataList () {

        return {
	        template: require('../templates/mails-list.html'),
            restrict: 'AE',
            scope: {
                active: '='
            },
            controller: MessageListCtrl
        };


        function MessageListCtrl ($scope, MailService, MailFactory) {

            var pagination = {
                page: 0,
                pagesize: 50
            };

            $scope.msgData = {};
            $scope.selectedMsgs = [];

            $scope.msgCount = 0;
            $scope.current = MailFactory.current;

            $scope.maxPagesCount = 100; //Fictional number for max page

            $scope.selectMsg = selectMsg;
            $scope.changePage = changePage;
            $scope.markAsRead = markAsRead;
            $scope.stripTags = stripTags;

            $scope.$watch('active', function(newIndex, oldIndex) {
                getMessagesByFolder(newIndex);
            });

            function selectMsg(msg) {
                $scope.current.mail = msg;
                if (msg.m_unread == 0) {
                    markAsRead([ msg.id ]);
                }
            }

            function getMessagesByFolder(newIndex) {

                if (angular.isDefined(newIndex)) {
                    var fid = MailFactory.navigation.folders[newIndex].id;

                    if (!fid) return false;

                    $scope.busy = true;

                    var filter = pagination;
                    filter.folder = fid;

                    MailService.getByFolder(filter)
                        .then(function (response) {
                            $scope.msgData = response.data;
                            $scope.msgCount = _.size(_.keys(response.data));
                        })
                        .finally(function(){
                            $scope.busy = false;
                        });
                }
            }

            function markAsRead (ids) {

                //NEED !
                if (!ids) {
                    ids = $scope.selectedMsgs;
                }
                if (!ids.length) {
                    return false;
                }

                var len = ids.length;

                $scope.current.folder.unread -= len;

                MailService.changeReadStatus(ids)
                    .then(function() {}, function(rollback) {
                        $scope.current.folder.unread += len;
                    });

            }
            function changePage(dir) {
                if (dir == 'left') {
                    pagination.page = pagination.page > 0 ?  pagination.page - 1 : 0;
                } else {
                    pagination.page = pagination.page != $scope.maxPagesCount ?  pagination.page + 1 : pagination.page;
                }
                getMessagesByFolder();
            }

            function stripTags( str ) {
                return str.replace(/<\/?[^>]+>/gi, '');
            }
        }

    }
})();
