(function () {
    'use strict';

    angular
        .module('app.mail')
        .directive('ngFreeDrag', ngFreeDrag);


    ngFreeDrag.$inject = ['$document', '$window'];

    function ngFreeDrag ($document, $window) {

        return {
            link: makeDraggable
        };

        function makeDraggable(scope, el, attr)
        {
            var startX = 0;
            var startY = 0;
            var initX, initY;
            // Start with a random pos
            var pos = el.position();

            var x = initX = pos.left;
            var y = initY = pos.top;

            var element = angular.element($('#drag-handler'));
            var closeBtn = angular.element($('.close-handler'));

            element.on('mousedown', function (event) {
                event.preventDefault();

                startX = event.pageX - x;
                startY = event.pageY - y;

                $document.on('mousemove', mousemove);
                $document.on('mouseup', mouseup);
            });

            //closeBtn.on('click', function (event) {
            //    el.css({
            //        top: initY,
            //        left: initX
            //    });
            //});

            function mousemove(event) {
                y = event.pageY - startY;
                x = event.pageX - startX;

                el.css({
                    top: y + 'px',
                    left: x + 'px'
                });
            }

            function mouseup() {
                $document.unbind('mousemove', mousemove);
                $document.unbind('mouseup', mouseup);
            }

            scope.$on('$destroy', function () {
                //$document.off('mousemove');
                //$document.off('mouseup');
                //closeBtn.off('click');
                //element.off('mousedown');
            })

        }

    }
})();