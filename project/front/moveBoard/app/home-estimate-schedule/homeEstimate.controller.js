(function () {
    'use strict';

    angular
        .module('app.schedule')
        .controller('HomeEstimateSchedule', HomeEstimateSchedule);

    HomeEstimateSchedule.$inject = ['$scope', 'logger', 'HomeEstimateService', '$rootScope', 'RequestServices', '$q'];

    function HomeEstimateSchedule($scope, logger, HomeEstimateService, $rootScope, RequestServices, $q) {
        var vm = this;

        vm.busy = false;
        vm.calendarView = 'month';
        vm.currendDate = new Date();
        vm.requests = [];
        vm.homeEstimators = {};

        vm.updateCalendar = updateCalendar;

        updateCalendar(vm.currendDate);

        function updateCalendar(date) {
            vm.busy = true;

            let fromDate = moment(date).startOf('month');
            let toDate = moment(date).endOf('month');

            let homeEstimateRequests = HomeEstimateService
                .getHomeEstimateRequests(fromDate, toDate);
            let homeEstimators = _.isEmpty(vm.homeEstimators)
                ? HomeEstimateService.getHomeEstimators()
                : {data: vm.homeEstimators};

            $q.all({requests: homeEstimateRequests, estimators: homeEstimators})
              .then(onSuccessLoadRequests, onErrorLoadRequests)
              .finally(finalLoadRequest);
        }

        function onSuccessLoadRequests(response) {
            vm.requests = response.requests.data.nodes;
            vm.homeEstimators = response.estimators.data;

            updateHomeEstimateSchedule();
        }

        function updateHomeEstimateSchedule() {
            let data = {
                homeEstimators: vm.homeEstimators,
                requests: vm.requests
            };

            $rootScope.$broadcast('home_estimate_schedule_request_was_updated', data);
        }

        function onErrorLoadRequests(error) {
            logger.error('Error', error);
        }

        function finalLoadRequest() {
            vm.busy = false;
        }

        $scope.$on('request.updated', function (event, request) {
            let updatedIndex = _.findIndex(vm.requests, {nid: request.nid});

            if (updatedIndex >= 0) {
                updateCalendar(vm.currendDate);
            }
        });

    }

})();
