(function () {

    angular.module("app")
           .config(homeEstimateConfig);

    homeEstimateConfig.$inject = ["$stateProvider"];

    function homeEstimateConfig($stateProvider) {
        $stateProvider
            .state('schedule.home-estimate-schedule', {
                url: '/home-estimate-schedule',
	            template: require('./home-estimate-schedule.template.html'),
                title: 'Schedule',
                data: {
                    permissions: {
                        except: ['anonymous', 'foreman', 'helper']
                    }
                }
            });
    }
})();
