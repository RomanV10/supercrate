import './month-cell.styl'
(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elHomeEstimateMonthCell', homeEstimateMonthCell);


    function homeEstimateMonthCell() {
        const DEFAULT_MONTH_CELL_HEIGHT_COEFFICIENT = 22;

        var directive = {
	        template: require('./month-cell.template.html'),
            restrict: 'E',
            scope: {
                monthItem: '=',
                position: '='
            },

            controller: elHomeEstimateMonthCellCtrl,
            link: linker
        };

        elHomeEstimateMonthCellCtrl.$inject = ['$scope', '$rootScope'];

        return directive;


        function elHomeEstimateMonthCellCtrl($scope, $rootScope) {
            var vm = $scope;

            vm.selectCurrentDay = selectCurrentDay;

            function selectCurrentDay() {
                $rootScope.$broadcast('homeEstimate.onClickDayItem', vm.position);
            }
        }

        function linker($scope, element) {
            init();

            function init() {
                let countHomeEstimators = _.size($scope.monthItem.homeEstimators);
                let height = countHomeEstimators * DEFAULT_MONTH_CELL_HEIGHT_COEFFICIENT;

                element.find('.home-estimate-month-cell').css('height', height);
            }

            // TODO Remove after get functional from backend for retrewe home estimators
            $scope.$watch('monthItem.homeEstimators', init);
        }
    }


})();
