import './calendar.styl'
(function () {
    'use strict';

    angular.module('move.requests')
           .directive('elMonthCalendarNavigator', monthCalendarNavigator);

    function monthCalendarNavigator() {
        return {
            restrict: 'E',
	        template: require('./calendar.template.html'),
            scope: {
                curDate: '=currentDate',
                updateCalendar: '=onUpdateCalendar'
            },
            link: linker
        };

        function linker($scope) {
            $scope.prevMonth = prevMonth;
            $scope.nextMonth = nextMonth;

            function prevMonth() {
                $scope.curDate = moment($scope.curDate).subtract(1, "month").toDate();
                updateCurrentDate();
            }

            function nextMonth() {
                $scope.curDate = moment($scope.curDate).add(1, "month").toDate();
                updateCurrentDate();
            }

            function updateCurrentDate() {
                if ($scope.updateCalendar) {
                    $scope.updateCalendar($scope.curDate);
                }
            }
        }
    }

})();
