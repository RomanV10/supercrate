import './home-estimate.styl'
(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elHomeEstimateScheduleDirective', homeEstimateScheduleDirective);


    function homeEstimateScheduleDirective() {
        var directive = {
	        template: require('./schedule.template.html'),
            restrict: 'E',
            scope: {
                requests: '=',
                view: '=',
                currentDay: '=',
                homeEstimators: '='
            },

            controller: elHomeEstimateScheduleCtrl
        };

        elHomeEstimateScheduleCtrl.$inject = ['$scope', '$timeout', '$attrs', '$locale', 'scheduleTitle'];

        return directive;


        function elHomeEstimateScheduleCtrl($scope, $timeout, $attrs, $locale, scheduleTitle) {
            var vm = $scope;

            vm.requests = vm.requests || [];

            vm.changeView = function (view, newDay) {
                vm.view = view;
                vm.currentDay = newDay;
            };
        }
    }


})();




