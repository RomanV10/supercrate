import './work-preview.styl';
'use strict';

angular
	.module('move.requests')
	.directive('elHomeEstimateWorkPreview', homeEstimateWorkCellPreview);

homeEstimateWorkCellPreview.$inject = ['common'];

function homeEstimateWorkCellPreview(common) {
	const DEFAULT_WIDTH_COEFFICIENT = 15;
	const DEFAULT_LEFT_COEFFICIENT = 4;
	const DEFAULT_LEFT_OFFSET = 50;

	var directive = {
		template: require('./work-preview.template.html'),
		restrict: 'E',
		scope: {
			work: '='
		},
		link: linker
	};

	return directive;

	function linker($scope, element) {
		init();

		function init() {
			let left = DEFAULT_LEFT_COEFFICIENT * _.round(common.convertTime($scope.work.home_estimate_actual_start.value || '12:00 AM'), 2) - DEFAULT_LEFT_OFFSET;
			let width = DEFAULT_WIDTH_COEFFICIENT * _.round(common.convertStingToIntTime($scope.work.home_estimate_time_duration.value || '00:00'), 2);

			if (left < 0) left = 0;

			element.find('.home-estimate-schedule-work-preview').css('left', left).css('width', width);
		}
	}
}
