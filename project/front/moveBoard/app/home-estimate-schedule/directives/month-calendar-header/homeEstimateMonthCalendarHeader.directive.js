(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elHomeEstimateMonthCalendarHeader', elHomeEstimateMonthCalendarHeader);


    function elHomeEstimateMonthCalendarHeader() {
        var directive = {
	        template: require('./month-calendar-header.template.html'),
            restrict: 'E',
            scope: {
            },

            controller: elHomeEstimateMonthCalendarHeaderCtrl
        };

        elHomeEstimateMonthCalendarHeaderCtrl.$inject = ['$scope', 'scheduleHelper'];

        return directive;


        function elHomeEstimateMonthCalendarHeaderCtrl($scope, scheduleHelper) {
            var vm = $scope;

            vm.weekDays = scheduleHelper.getWeekDayNames();
        }
    }


})();




