import './legend.styl';
'use strict';

angular.module('move.requests')
	.directive('elServiceTypesLegend', serviceTypesLegend);

serviceTypesLegend.$inject = ['serviceTypeService'];

function serviceTypesLegend(serviceTypeService) {
	return {
		restrict: 'E',
		template: require('./legend.template.html'),
		scope: {},
		link: linker
	};

	function linker($scope, element) {
		$scope.serviceTypes = serviceTypeService.getAllServiceTypes()
	}
}
