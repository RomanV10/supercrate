import './month-calendar.styl'
(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elHomeEstimateMonthCalendar', homeEstimateMonthCalendar);


    function homeEstimateMonthCalendar() {
        var directive = {
	        template: require('./month-calendar.template.html'),
            restrict: 'E',
            scope: {
                requests: '=',
                currentDay: '=',
                homeEstimators: '='
            },

            controller: elHomeEstimateMonthCalendarCtrl
        };

        elHomeEstimateMonthCalendarCtrl.$inject = ['$scope', 'HomeEstimateScheduleHelper'];

        return directive;


        function elHomeEstimateMonthCalendarCtrl($scope, HomeEstimateScheduleHelper) {
            var vm = $scope;

            let oldPosition = {};

            onUpdateRequests();

            $scope.$on('home_estimate_schedule_request_was_updated', onUpdateRequests);

            function onUpdateRequests(event, data) {
                if (data) {
                    vm.homeEstimators = data.homeEstimators;
                    vm.requests = data.requests;
                }

                vm.currentCalendar = HomeEstimateScheduleHelper.sortRequestByMonthCalendar(vm.currentDay, vm.requests, vm.homeEstimators);
            }

            $scope.$on('homeEstimate.onClickDayItem', onClickOnDay);

            function onClickOnDay(event, newPosition) {
                if (!_.isEmpty(oldPosition) && !_.isEqual(oldPosition, newPosition)) {
                    vm.currentCalendar[oldPosition.week][oldPosition.day].isSelected = false;
                }

                if (vm.currentCalendar[newPosition.week][newPosition.day].isCurrentMonth) {
                    if (_.isEqual(oldPosition, newPosition)) {
                        vm.currentCalendar[newPosition.week][newPosition.day].isSelected = !vm.currentCalendar[newPosition.week][newPosition.day].isSelected;
                    } else {
                        vm.currentCalendar[newPosition.week][newPosition.day].isSelected = true;
                    }
                }

                oldPosition = newPosition;
            }

            /*
            vm.isCollapsed = false;
            vm.calendarConfig = scheduleConfig;

            vm.dayClicked = function (day, index) {
                vm.openRowIndex = null;
                var dayIndex = vm.view.indexOf(day);

                if (dayIndex === vm.openDayIndex) { //the day has been clicked and is already open
                    vm.openDayIndex = null; //close the open day
                } else {
                    vm.openDayIndex = dayIndex;
                    vm.openRowIndex = Math.floor(dayIndex / 7);

                    vm.dayRequests = scheduleHelper.filterRequestsbyDay(vm.requests, day.date);
                }

                common.$timeout(function () {
                    var data = {
                        'index': index,
                        'curDate': day.date
                    };

                    $scope.$broadcast('parklot.dayClicked', data);
                });
            };

            vm.highlightEvent = function (event, shouldAddClass) {
                vm.view.forEach(function (day) {
                    delete day.highlightClass;
                    if (shouldAddClass) {
                        var dayContainsEvent = day.events.indexOf(event) > -1;
                        if (dayContainsEvent) {
                            day.highlightClass = 'day-highlight dh-event-' + event.type;
                        }
                    }
                });
            };

            vm.handleEventDrop = function (event, newDayDate) {
                var newStart = moment(event.startsAt)
                    .date(moment(newDayDate).date())
                    .month(moment(newDayDate).month());

                var newEnd = calendarHelper.adjustEndDateFromStartDiff(event.startsAt, newStart, event.endsAt);

                vm.onEventTimesChanged({
                    calendarEvent: event,
                    calendarDate: newDayDate,
                    calendarNewEventStart: newStart.toDate(),
                    calendarNewEventEnd: newEnd ? newEnd.toDate() : null
                });
            };
            */
        }
    }


})();
