import './work-cell-preview.styl'
(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elHomeEstimateWorksCellPreview', homeEstimateWorksCellPreview);


    function homeEstimateWorksCellPreview() {
        const DEFAULT_MONTH_CELL_HEIGHT_COEFFICIENT = 15;

        var directive = {
	        template: require('./work-cell-preview.template.html'),
            restrict: 'E',
            scope: {
                monthItem: '='
            },

            controller: elHomeEstimateMonthCellCtrl,
            link: linker
        };

        elHomeEstimateMonthCellCtrl.$inject = ['$scope'];

        return directive;


        function elHomeEstimateMonthCellCtrl($scope) {
            var vm = $scope;
        }

        function linker($scope, element) {
            init();

            function init() {
                let countHomeEstimators = _.size($scope.monthItem.homeEstimators);
                let height = countHomeEstimators * DEFAULT_MONTH_CELL_HEIGHT_COEFFICIENT;

                element.find('.home-estimate-works-cell-preview').css('max-height', height);
            }

            // TODO Remove after get functional from backend for retrewe home estimators
            $scope.$watch('monthItem.homeEstimators', init);
        }
    }


})();
