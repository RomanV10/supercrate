import './slide-box.styl'
(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elHomeEstimateSlideBox', homeEstimateSlideBox);


    function homeEstimateSlideBox() {
        const DEFAULT_HEIGHTS = 25;
        const THIRTY = 30;
        const FIVE = 5;

        var directive = {
	        template: require('./slide-box.template.html'),
            restrict: 'E',
            scope: {
                week: '='
            },
            link: linker
        };

        return directive;


        function linker($scope, element) {
            $scope.isOpen = isOpen;

            $scope.homeEstimateOptions = {};
            $scope.request = {};
            $scope.requests = [];
            $scope.homeEstimators = {};

            function isOpen() {
                let selectedIndex = _.findIndex($scope.week, {isSelected: true});
                let result = selectedIndex >= 0;

                if (result) {
                    let day = $scope.week[selectedIndex];
                    $scope.homeEstimators = day.homeEstimators;
                    $scope.requests = day.requests;

                    let workersCount = _.size($scope.homeEstimators);
                    let height = THIRTY + workersCount * DEFAULT_HEIGHTS + FIVE * workersCount;
                    element.find('.home-estimate-schedule-slide-box').css('height', height);
                }

                return result;
            }
        }
    }


})();




