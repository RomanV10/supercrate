(function () {

    angular
        .module('move.requests')
        .factory('HomeEstimateScheduleHelper', HomeEstimateScheduleHelper);

    function HomeEstimateScheduleHelper() {

        var service = {};
        const DAYS_OF_WEEK = 6;

        service.sortRequestByMonthCalendar = sortRequestByMonthCalendar;

        return service;


        function sortRequestByMonthCalendar(currentDate, requests, homeEstimators) {
            let result = {};
            let today = moment();
            let currentDateMoment = moment(currentDate);
            let countWeeks = getNumbersOfWeek(currentDateMoment);
            let firstMonthDay = currentDateMoment.startOf('month');
            let positionFirstDayOfWeek = firstMonthDay.weekday();
            let currentCalendarDay = firstMonthDay.clone();

            // if first day of week
            if (positionFirstDayOfWeek > 0) {
                currentCalendarDay.subtract(positionFirstDayOfWeek, 'days');
            }

            // init month calendar
            for (let week = 0; week <= countWeeks; week++) {
                result[week] = [];
            }

            // generate month matrix
            for (let week = 0; week <= countWeeks; week++) {
                for (let day = 0; day <= DAYS_OF_WEEK; day++) {
                    let currentDayRequests = searchRequestByHomeEstimateDate(currentCalendarDay, requests);
                    let currentDay = {
                        number: currentCalendarDay.date(),
                        isActualDay: today.isSame(currentCalendarDay, 'day'),
                        isCurrentMonth: firstMonthDay.isSame(currentCalendarDay, 'month'),
                        isWeekendDay: currentCalendarDay.weekday() == 6 || currentCalendarDay.weekday() == 0,
                        requests: currentDayRequests,
                        homeEstimators: homeEstimators,
                        workPlan: sortRequestByWorkers(currentDayRequests, homeEstimators)
                    };

                    result[week].push(currentDay);

                    currentCalendarDay.add(1, 'days');
                }

                if (firstMonthDay.isSame(currentCalendarDay, 'month') && countWeeks == week) {
                    countWeeks += 1;
                    result[countWeeks] = [];
                }
            }

            return result;
        }

        function getNumbersOfWeek(currentMomentDate) {
            let diffStartEndOfMonth = moment(currentMomentDate).endOf('month') - moment(currentMomentDate).startOf('month');
            let result = moment.duration(diffStartEndOfMonth).weeks();

            return result;
        }

        function searchRequestByHomeEstimateDate(date, requests) {
            let result = [];

            _.each(requests, function (request) {
                if (_.isEqual(request.home_estimate_date.value, date.format("MM/DD/YYYY"))) {
                    result.push(request);
                }
            });

            return result;
        }

        function sortRequestByWorkers(requests, workers) {
            let result = {};

            _.each(workers, function (worker, id) {
                result[id] = [];

                _.each(requests, function (request) {
                    if (request.home_estimator.value == id) {
                        result[id].push(request);
                    }
                })
            });

            return result;
        }
    }
})();