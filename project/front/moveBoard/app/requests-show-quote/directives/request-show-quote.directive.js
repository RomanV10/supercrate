'use strict';

import './request-show-quote.styl';

angular
	.module('request-show-quote')
	.directive('requestShowQuote', requestShowQuote);

/*@ngInject*/
function requestShowQuote(SweetAlert) {
	return {
		restrict: 'E',
		scope: {
			request: '=',
		},
		template: require('./request-show-quote.tpl.pug'),
		link: requestShowQuoteLink,
	};

	function requestShowQuoteLink($scope) {
		const QUOTE_STATE = {
			visible: 'Quote is visible',
			hidden: 'Quote is hidden',
			toHide: 'Hide quote?',
			toShow: 'Show quote?'
		};

		$scope.switchShowingQuote = switchShowingQuote;

		init();
		$scope.$watch('request.status.raw', isAvailableSetting);
		$scope.$on('tryToEnableShowQuoteSetting', (_, status) => isAvailableSetting(status.current, status.original));

		function init() {
			isAvailableSetting($scope.request.status.raw);
			$scope.explanationStateQuote = getCurrentExplanationStateQuote();
		}

		function isAvailableSetting(current, original) {
			if (current != original) {
				$scope.isSettingsAvailable = $scope.request.request_all_data.isDisplayQuoteEye && (current == 1 || current == 4);
			}
		}

		function switchShowingQuote() {
			let sweetAlertTitle = $scope.request.request_all_data.showQuote ? QUOTE_STATE.toHide : QUOTE_STATE.toShow;

			SweetAlert.swal({
				title: sweetAlertTitle,
				type: "warning",
				showCancelButton: true,
				cancelButtonText: "No",
				confirmButtonText: "Yes"
			}, isConfirm => {
				if (!isConfirm) return;

				$scope.request.request_all_data.showQuote = !$scope.request.request_all_data.showQuote;
				$scope.explanationStateQuote = getCurrentExplanationStateQuote();
				$scope.$emit('updateOnlyAllData');
			})
		}

		function getCurrentExplanationStateQuote() {
			return $scope.request.request_all_data.showQuote ? QUOTE_STATE.visible : QUOTE_STATE.hidden;
		}
	}
}
