describe('request-show-quote.directive.js', () => {
	let rootScope,
		element,
		$scope,
		SweetAlert;

	beforeEach(inject((_SweetAlert_) => {
		SweetAlert = _SweetAlert_;
		rootScope = $rootScope.$new();
		let request = testHelper.loadJsonFile('request-for-test.request-show-quote.directive.mock');
		rootScope.request = request;
		let directive = '<request-show-quote request="request"></request-show-quote>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	}));

	describe('on init: ', () => {
		it('$scope.switchShowingQuote have to be defined', () => {
			expect($scope.switchShowingQuote).toBeDefined();
		});

		it('$scope.isSettingsAvailable should be equals TRUE', () => {
			expect($scope.isSettingsAvailable).toBeTruthy();
		});

		it('$scope.explanationStateQuote should be "Quote is hidden"', () => {
			expect($scope.explanationStateQuote).toBe('Quote is hidden');
		});
	});

	describe('on change:', () => {
		beforeEach(() => {
			spyOn(SweetAlert, 'swal').and.callFake((params, cb) => cb(true));
			$scope.switchShowingQuote();
		});

		it('$scope.request.request_all_data.showQuote have to be TRUE', () => {
			expect($scope.request.request_all_data.showQuote).toBeTruthy();
		});

		it('$scope.explanationStateQuote have to be equals "Quote is visible"', () => {
			expect($scope.explanationStateQuote).toBe('Quote is visible');
		});
	});
});
