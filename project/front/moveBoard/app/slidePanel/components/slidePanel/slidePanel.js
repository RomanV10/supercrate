'use strict';

class SlidePanel{
    constructor(SlidePanelService){
        this.service = SlidePanelService;
    }
}

angular
    .module('slidePanel')
    .component('slidePanel', {
	    template: require('./slidePanel.html'),
        controller: SlidePanel,
        controllerAs: '$ctrl',
        bindings: {
            name: '@',
            actions: '<'
        }
    });

SlidePanel.$inject = ['SlidePanelService'];
