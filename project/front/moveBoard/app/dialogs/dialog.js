'use strict';

class Dialog {
	static get $inject() {
		return ['$scope', 'options'];
	}

    constructor($scope, options) {
        this.$scope = $scope;
        this.options = options;

        if(options && !_.isArray(options)){
            Object.keys(options).forEach((key, value) =>
                this[key] = value);
        }
    }
}

angular
    .module('dialogs')
    .controller('Dialog', Dialog);
