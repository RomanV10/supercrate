'use strict';

class NotificationModal {
    constructor(){
        this.name = 'notificationBox';
    }
    open ($uibModal, options) { 
        return $uibModal.open({
            template:  `<notification options="$ctrl.options" done="$close()"></notification>`,
            controller: 'Dialog',
            controllerAs: '$ctrl',
            windowClass: 'notification-modal',
            backdrop: false,
            resolve: {
                options
            }
        }).result
    }

    $get(){
        return this;
    }
}

angular
    .module('dialogs')
    .provider('notificationDialog', NotificationModal);
