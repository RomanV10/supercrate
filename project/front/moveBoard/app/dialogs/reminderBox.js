'use strict';

class ReminderModal {
    constructor(){
        this.name = 'reminderBox';
    }
    open ($uibModal, options) { 
        return $uibModal.open({
            template:  `<edit-reminder options="$ctrl.options" done="$close()"></edit-reminder>`,
            controller: 'Dialog',
            controllerAs: '$ctrl',
            windowClass: 'reminder-modal',
            resolve: {
                options
            }
        }).result
    }

    $get(){
        return this;
    }
}

angular
    .module('dialogs')
    .provider('reminderDialog', ReminderModal);
