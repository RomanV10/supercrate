'use strict';

class Dialogs {
	constructor() {
		this.dialogs = {};
	}

	register(options) {
		this.dialogs[options.name] = options.open;
		return this;
	}

	/* @ngInject */
	$get($uibModal) {
		_.each(this.dialogs, (val, key) =>
			this.dialogs[key] = _.bind(val, null, $uibModal));
		return this.dialogs;
	}
}

angular
	.module('dialogs')
	.provider('dialogs', Dialogs);
