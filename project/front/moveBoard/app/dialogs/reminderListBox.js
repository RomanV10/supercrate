'use strict';

class ReminderListModal {
    constructor(){
        this.name = 'reminderList';
    }
    open ($uibModal, options) {
        return $uibModal.open({
            template:  `<reminder-list options="$ctrl.options" done="$close()"></reminder-list>`,
            controller: 'Dialog',
            controllerAs: '$ctrl',
            windowClass: 'reminder-list-box',
            resolve: {
                options
            }
        }).result
    }

    $get(){
        return this;
    }
}

angular
    .module('dialogs')
    .provider('reminderListDialog', ReminderListModal);
