angular
	.module('dialogs')
	.config(/*@ngInject*/(dialogsProvider, reminderDialogProvider, notificationDialogProvider,
	         reminderListDialogProvider, reminderRequestDialogProvider) =>
		dialogsProvider
			.register(reminderDialogProvider)
			.register(notificationDialogProvider)
			.register(reminderListDialogProvider)
			.register(reminderRequestDialogProvider));
