'use strict';

class ReminderRequestModal {
    constructor(){
        this.name = 'reminderRequestBox';
    }
    open ($uibModal, options) { 
        return $uibModal.open({
            template:  `<uib-tabset active="activeReminder" justified="true">
                            <uib-tab index="0" heading="Reminder">
                                    <edit-reminder options="$ctrl.options" done="$close()"></edit-reminder>
                            </uib-tab>
                            <uib-tab index="1" heading="Reminders List">
                                    <reminder-list options="$ctrl.options" done="$close()"></reminder-list>
                            </uib-tab>
                        </uib-tabset>
                        <reminders-count collection="$ctrl.options.collection"></reminders-count>`,
            controller: 'Dialog',
            controllerAs: '$ctrl',
            windowClass: 'reminder-request-modal',
            resolve: {
                options
            }
        }).result
    }

    $get(){
        return this;
    }
}

angular
    .module('dialogs')
    .provider('reminderRequestDialog', ReminderRequestModal);
