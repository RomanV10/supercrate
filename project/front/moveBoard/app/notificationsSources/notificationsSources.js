'use strict';
(() => {

    angular
        .module('app')
        .filter('notificationsSources', function() {
            return function(text) {
                if (text == 0) {
                    return "Request";
                } else if (text == 20) {
                    return "User";
                }
            }
        }).filter('moment', function() {
            return function(input, momentFn /*, param1, param2, ...param n */ ) {
                var args = Array.prototype.slice.call(arguments, 2),
                    momentObj = moment(input);
                return momentObj[momentFn].apply(momentObj, args);
            };
        });

})();