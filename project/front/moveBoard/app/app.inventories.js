 (function () {
    'use strict';
	  angular.
	  module('inventory', [
	        'ngMaterial',
	        'newInventories',
	        'moveBoardApi',
	        'apiService',
	        'ngRoute',        
	        'ngMdIcons',

	        'elromcoCore',

	        ]).
	  config(['$locationProvider' ,'$routeProvider',
	    function config($locationProvider, $routeProvider) {
	      $locationProvider.hashPrefix('!');

	      $routeProvider.
	        when('/inventory', {
	          template: '<div inventories request="2232"></div>'
	        }).
	        otherwise('/inventory');
	    }
	  ]);
})();