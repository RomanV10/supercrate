(function () {
    'use strict';

    angular
        .module('app.account')
    
     .controller('AccountController', function ($routeParams) {

             // Inititate the promise tracker to track form submissions.

             var vm = this; 
             var uid = $routeParams.id;

             vm.hello = [];
        
           activate();
           
           function activate (){
               
                // Load account data from local storage
                var clientsPages = ClientsServices.getAllClientsStorage();

                angular.forEach(clientsPages, function(clientsPage, key) {

                        
                      if(angular.isDefined(clientsPage[uid])){
                         
                            vm.client = clientsPage[uid];
                            vm.requests = vm.client.nodes;
                            vm.hello = {
                                name : 'From Storage',
                            };
                        }


                });
                    
                    
                // IF client not loaded  from Database 
                if(angular.isUndefined(vm.client)){
                         
                        // NEED Client Service Function to load from database
                      vm.hello = {
                                name : 'From Data',
                            };
                 }    

               
           }
                            
           
        
           

    })

    


})();
    