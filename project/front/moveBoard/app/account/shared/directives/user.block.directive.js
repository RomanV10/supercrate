'use strict';

angular
	.module('app.account')
	.directive('ccUserBlock', ccUserBlock);

function ccUserBlock () {
	return {
		scope: {
			'client': '=',
			'subtitle': '=',
			'rightText': '@',
			'allowCollapse': '@'
		},
		template: require('./user-block.html'),
		restrict: 'A'
	};
}
