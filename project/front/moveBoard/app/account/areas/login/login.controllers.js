'use strict';

angular.module('app.core')
	.controller('LoginController', LoginController);

LoginController.$inject = ['$scope', '$rootScope', '$location', 'AuthenticationService', 'AUTH_EVENTS', 'SweetAlert'];

function LoginController($scope, $rootScope, $location, AuthenticationService, AUTH_EVENTS, SweetAlert) {

	$scope.email = '';
	$scope.password = '';
	$scope.busy = false;
	$scope.loading = false;
	$scope.moveBoard = true;
	var needReload = true;
	var currentLogin = localStorage.getItem('login');
	window.addEventListener('storage', function (e) {
		if (e.key === 'login') {
			if (currentLogin != localStorage.getItem('login') && needReload) {
				location.reload();
			} else {
				needReload = true;
			}
		}
	});

	$scope.login = function () {
		if (AuthenticationService.checkCORS()) {
			return;
		}
		$scope.loading = true;

		AuthenticationService.Login($scope.email, $scope.password, $scope.homeEstimate, function (response) {
			if (response.success) {
				if ($scope.homeEstimate) {
					if (_.get(response, 'user.settings.homeEstimator')) {
						
						$rootScope.currentUser.userRole.push("home-estimator");
						continueLogin('inhome.estimator-jobs');
					} else {
						$scope.loading = false;
						SweetAlert.swal("Failed!", "You could not choose home estimate option", "error");
					}
				} else {
					continueLogin('dashboard');
				}
			} else {
				$scope.error = response.message;
				$scope.loading = false;
			}
		});
	};

	function continueLogin(path) {
		needReload = false;
		localStorage.setItem('login', (new Date()).getTime());
		$scope.loading = false;
		$rootScope.nextRoute = path;
		$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
		$rootScope.login = false;
	}
}
