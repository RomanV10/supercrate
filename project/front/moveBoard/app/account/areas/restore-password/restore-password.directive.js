'use strict';

angular.module('app.core')
	.directive('erRestorePassword', erRestorePassword);

function erRestorePassword() {
	return {
		template: require('./restore.pug'),
		restrict: 'E',
		scope: {},
		controller: ERRestorePasswordController
	};
}

ERRestorePasswordController.$inject = ['$scope', '$location', 'AuthenticationService', 'SweetAlert'];

function ERRestorePasswordController($scope, $location, AuthenticationService, SweetAlert) {
	$scope.email = '';
	$scope.busy = false;
	$scope.loading = false;

	$scope.restorePassword = function () {
		$scope.busy = true;
		$scope.loading = true;
		AuthenticationService.request_new_password($scope.email, function (response) {
			if (response.success) {
				$scope.busy = false;
				$scope.loading = false;
				SweetAlert.swal("Success!", "Check your email", "success");
				$location.path('/login');
			} else {
				$scope.busy = false;
				$scope.loading = false;
				$scope.error = 'Your email address was not recognized.';
			}
		});
	};
}