(function(){
    angular.module("app")
        .config([
            "$stateProvider",
            "$urlRouterProvider",
            function ($stateProvider, $urlRouterProvider)
            {
                $stateProvider
                    .state('account', {
                        url: '/account/:id',
	                    template: require('./areas/account_page/account.html'),
                        data: {
                            permissions: {
                                only: ['authenticated user']
                            }
                        }
                    })
                    .state('inventory', {
                        url: '/inventory/:id',
	                    template: require('./areas/login/login.html'),
                        data: {
                            permissions: {
                                only: ['authenticated user']
                            }
                        }
                    })
                    .state('login', {
                        url: '/login',
	                    template: require('./areas/login/login.html'),
                        data: {
                            permissions: {
                                only: ['anonymous']
                            }
                        }
                    })
                    .state('restore', {
                        url: '/restore',
                        template: '<er-restore-password></er-restore-password>',
                        data: {
                            permissions: {
                                only: ['anonymous']
                            }
                        }
                    });
                //hack for otherwise
                $urlRouterProvider.otherwise(function($injector) {
                    var $state = $injector.get("$state");
                    $state.go('dashboard');
                });

            }
        ]
    );
})();
