'use strict';

angular.module('move.requests')
	.factory('InitRequestFactory', InitRequestFactory);

InitRequestFactory.$inject = ['sharedRequestServices', 'CalculatorServices', 'InventoriesServices', 'RequestPageServices', 'common', 'valuationService'];

function InitRequestFactory(sharedRequestServices, CalculatorServices, InventoriesServices, RequestPageServices, common, valuationService) {

	var service = {};
	var arrFields = [
		'apt_from',
		'apt_to',
		'city_from',
		'city_to',
		'date',
		'ddate',
		'delivery_date_from',
		'delivery_date_to',
		'delivery_start_time',
		'distance',
		'dstart_time1',
		'dstart_time2',
		'extraServices',
		'field_extra_dropoff',
		'field_extra_pickup',
		'field_moving_from',
		'field_moving_to',
		'field_reservation_received',
		'flat_rate_quote',
		'from_storage',
		'field_usecalculator',
		'inventory',
		'inventory_weight',
		'move_size',
		'receipts',
		'rate',
		'rooms',
		'reservation_rate',
		'service_type',
		'start_time',
		'start_time1',
		'start_time2',
		'status',
		'storage_id',
		'storage_rate',
		'travel_time',
		'request_all_data',
		'field_long_distance_rate',
		'type_to',
		'type_from',
		'field_cubic_feet',
		'request_data',
		'crew',
		'maximum_time',
		'minimum_time',
		'extraDropoff',
		'extraPickup',
		'duration',
		'work_time',
		'work_time_old',
		'work_time_int',
		'field_double_travel_time',
		'field_useweighttype',
		'custom_weight',
		'field_double_travel_time',
		'surcharge_fuel_avg',
		'surcharge_fuel_perc',
		'surcharge_fuel',
		'commercial_extra_rooms',
		'field_custom_commercial_item',
		'field_delivery_crew_size',
	];

	const FIELDS_FOR_CLEAR = [
		'extraServices',
	];
	const NAME_FULL_PACKING = 'Estimate Full Packing';
	const NAME_PARTIAL_PACKING = 'Estimate Partial Packing';

	service.initRequestFields = initRequestFields;
	service.initRequestAllData = initRequestAllData;
	service.initInvoiceRequest = initInvoiceRequest;
	service.initRequestDetails = initRequestDetails;
	service.clearFields = clearFields;

	return service;

	function clearFields (request) {
		_.forEach(FIELDS_FOR_CLEAR, field => {
			delete request[field];
		});
	}

	function initRequestFields(request, travelTimeSetting = true) {
		if (request.field_useweighttype.value == null) {
			request.field_useweighttype.value = 1;
		}

		if (_.isString(request.request_data.value) || _.isArray(request.request_data.value) || _.isEmpty(request.request_data.value)) {
			request.request_data.value = {};
		}

		request.reservation_rate.value = parseInt(request.reservation_rate.value);

		initRequestAllData(request, travelTimeSetting);

		sharedRequestServices.calculateQuote(request);

		request.total_weight = CalculatorServices.getRequestCubicFeet(request);
		request.visibleWeight = request.total_weight.weight;

		if (_.isArray(request.inventory)) {
			request.inventory = {inventory_list: []};
		}

		request.inventory_weight = InventoriesServices.calculateTotals(request.inventory.inventory_list);

		if (request.inventory_weight.cfs > 0 && request.field_useweighttype.value == '2') {
			request.visibleWeight = request.inventory_weight;
		}

		if (!_.isEmpty(request.trucks.raw)) {
			let uniqTrucks = _.uniq(request.trucks.raw);
			let uniqTrucksOld = _.uniq(request.trucks.old);
			request.trucks.raw = [...uniqTrucks];
			request.trucks.old = [...uniqTrucksOld];
		}

		request.truckCount = RequestPageServices.countTrucks(request.trucks.raw);
		var truckNeeded = CalculatorServices.getTrucks(request.total_weight);

		if (!request.truckCount) {
			request.truckCount = truckNeeded;
		}

		//CHECK IF DETAILS EXIST
		if (!isValidMoveDetails(request)) {
			request.inventory.move_details = initRequestDetails(request);
		}

		request.ld_status = +_.get(request, 'ld_flag_status.value', 0);

		removeAptSideSpace(request);
	}


	function isValidMoveDetails(request) {
		var details = request.inventory.move_details;
		var fields = ['current_door', 'new_door', 'current_permit', 'new_permit', 'addcomment', 'admincomments', 'pickup', 'delivery', 'delivery_days', 'steps_origin', 'steps_destination'];
		var result = true;

		if (_.isUndefined(details) || _.isNull(details) || _.isEmpty(details)) {
			result = false;
		}

		if (result) {
			_.each(fields, function (value) {
				if (_.isUndefined(details[value])) {
					result = false;

					return false;
				}
			});
		}

		return result;
	}

	function initRequestDetails(request, move_details, isAccount) {
		if (_.isUndefined(move_details)) {
			move_details = request.inventory.move_details;
		}

		var details = {};
		var ld_days = 0;
		var pickup = '';

		if ((request.service_type.raw == 7 || request.service_type.raw == 5)
			&& request.date.value) {

			if (request.service_type.raw == 7) {
				var linfo = CalculatorServices.getLongDistanceInfo(request);

				pickup = moment(request.date.value).valueOf();

				if (linfo) {
					ld_days = angular.isDefined(linfo.days) ? linfo.days : 0;
				}
			}
		}

		var DEFAULT_DETAILS = {
			'current_door': '1',
			'new_door': '1',
			'current_permit': 'None',
			'new_permit': 'None',
			'addcomment': '',
			'admincomments': '',
			'pickup': '',
			'delivery': '',
			'delivery_days': ld_days,
			'steps_origin': '1',
			'steps_destination': '1',
			'quoteExplanation': ''
		};

		if (isAccount) {
			DEFAULT_DETAILS['pickup'] = pickup;
		}

		if (_.isUndefined(move_details) || _.isNull(move_details)) {
			details = DEFAULT_DETAILS;
		} else {
			if (!_.isObject(move_details)) {
				move_details = {};
			}

			details = _.merge({}, DEFAULT_DETAILS, move_details);
		}

		return details;
	}

	function initRequestAllData(request, travelTimeSetting) {
		if (_.isArray(request.request_all_data) || !_.isObject(request.request_all_data)) {
			request.request_all_data = {};
		}

		if ((request.service_type.raw == 2 || request.service_type.raw == 6) && _.isUndefined(request.request_all_data.toStorage)) {
			request.request_all_data.toStorage = false;
		}

		if (_.isUndefined(request.request_all_data.surcharge_fuel_avg)) {
			request.request_all_data.surcharge_fuel_avg = 0;
		}

		if (_.isUndefined(request.request_all_data.surcharge_fuel_perc)) {
			request.request_all_data.surcharge_fuel_perc = 0;
		}

		if (_.isUndefined(request.request_all_data.surcharge_fuel)) {
			request.request_all_data.surcharge_fuel = 0;
		}

		if (_.isUndefined(request.request_all_data.showQuote)) {
			request.request_all_data.showQuote = true; // if old request then show quote
			request.request_all_data.isDisplayQuoteEye = false;
		}

		if (_.isUndefined(request.request_all_data.additionalBlock)) {
			request.request_all_data.additionalBlock = {
				'title': '',
				'text': '',
				'isVisible': false
			};
		}

		if (_.isUndefined(request.request_all_data.localdiscount)) {
			request.request_all_data.localdiscount = 0;
		}

		if (_.isUndefined(request.request_all_data.add_money_discount)) {
			request.request_all_data.add_money_discount = 0;
		}

		if (_.isUndefined(request.request_all_data.add_percent_discount)) {
			request.request_all_data.add_percent_discount = 0;
		}

		if (_.isUndefined(request.request_all_data.add_rate_discount)) {
			request.request_all_data.add_rate_discount = 0;
		}

		if (_.isUndefined(request.request_all_data.statistic)) {
			request.request_all_data.statistic = {};
		}
		if (_.isUndefined(request.request_all_data.travelTime)) {
			request.request_all_data.travelTime = travelTimeSetting;
		}
		if (_.isUndefined(request.request_all_data.valuation)) {
			request.request_all_data.valuation = {
				selected: {
					deductible_level: 'Select Valuation',
					valuation_charge: 0,
				},
				settings: {
					updated: moment().unix(),
				},
				other: {
					admonition: false,
					set_by_manager: false,
					showWarning: false,
				},
				deductible_data: {}
			};
		}

		if (!_.get(request, 'request_all_data.valuation.settings.updated')) {
			valuationUpdateToNewVersion(request);
		}

		if (!_.get(request, 'request_all_data.grandTotalRequest')) {
			fillGrandTotal(request);
		}
	}

	function fillGrandTotal(request) {
		request.request_all_data.grandTotalRequest = {
			min: 0,
			max: 0,
			longdistance: 0,
			flatrate: 0,
			invoice: 0,
		};
	}

	function valuationUpdateToNewVersion (request) {
		const POUND_COEFFICIENT = 7;
		let weight = CalculatorServices.getTotalWeight(request).weight * POUND_COEFFICIENT;

		valuationService.fillEmptyFields(request.request_all_data.valuation, weight);
	}

	function preparePackingsInvoice(invoice, request) {
		let invoicePacing = _.get(invoice, 'request_data.value.packings');

		if (!invoicePacing) invoice.request_data = angular.copy(request.request_data);

		let fullPacking = _.findIndex(invoice.request_data.value.packings, {originName: NAME_FULL_PACKING});
		let partialPacking = _.findIndex(invoice.request_data.value.packings, {originName: NAME_PARTIAL_PACKING});
		if (fullPacking >= 0) {
			invoice.request_data.value.packings.splice(fullPacking, 1);
		}
		if (partialPacking >= 0) {
			invoice.request_data.value.packings.splice(partialPacking, 1);
		}
	}

	function initInvoiceRequest(request, invoice = {}, updateInvoice = false, notResetWorkTime) {
		invoice.nid = request.nid;

		if (notResetWorkTime) {
			request.work_time_int = invoice.work_time_int;
			request.work_time = invoice.work_time_int;
		} else {
			invoice.work_time = common.decToTime(0);
			invoice.work_time_old = common.decToTime(0);
			invoice.work_time_int = 0;
		}

		if (_.isUndefined(request.request_all_data.invoice) || updateInvoice) {
			request.request_all_data.invoice = {};
			invoice.request_all_data = angular.copy(request.request_all_data);

			_.each(arrFields, function (value) {
				if (value == 'inventory') {
					invoice.inventory = angular.copy(request.inventory);
				} else if (value == 'request_data') {
					preparePackingsInvoice(invoice, request);
				} else {
					if (request[value]) {
						invoice[value] = angular.copy(request[value]);
					}
				}
			});

			if (angular.isDefined(request.work_time)) {
				invoice.work_time = angular.copy(request.work_time);
				invoice.work_time_old = angular.copy(request.work_time);
				invoice.work_time_int = _.round(common.convertStingToIntTime(request.work_time), 2);
			}

			if (angular.isUndefined(request.request_all_data.invoice.work_time)
				&& angular.isDefined(request.contract_info.laborHours)
				&& !_.isEmpty(request.contract_info)) {

				var time = request.contract_info.laborHours;

				if (_.isString(request.contract_info.laborHours)) {
					time = _.round(common.convertStingToIntTime(request.contract_info.laborHours), 2);
				}

				invoice.work_time = common.decToTime(time); // change on contract value get contract
				invoice.work_time_int = time; // change on contract value get contract
				invoice.work_time_old = common.decToTime(time); // change on contract value get contract
				request.work_time = common.decToTime(invoice.work_time);
			}

			request.request_all_data.invoice = angular.copy(invoice);
		} else {
			_.each(arrFields, function (value) {
				if (value == 'inventory') {
					invoice.inventory = angular.copy(request.inventory);
				} else if (value == 'request_data') {
					preparePackingsInvoice(invoice, request);
				} else if (request[value] || request.request_all_data.invoice[value]) {
					if (!_.isEmpty(request.request_all_data.invoice[value])) {
						invoice[value] = angular.copy(request.request_all_data.invoice[value]);
					} else {
						invoice[value] = angular.copy(request[value]);
					}
				}
			});

			if (angular.isDefined(request.contract_info)
				&& angular.isDefined(request.contract_info.laborHours)
				&& !_.isEmpty(request.contract_info)) {

				let time = request.contract_info.laborHours;

				if (_.isString(request.contract_info.laborHours)) {
					time = _.round(common.convertStingToIntTime(request.contract_info.hours), 2);
				}

				invoice.work_time_int = time; // change on contract value get contract
				invoice.work_time = common.decToTime(time); // change on contract value get contract
				invoice.work_time_old = common.decToTime(time); // change on contract value get contract
				request.work_time = common.decToTime(time);
			}

			for (let field in request.request_all_data.invoice) {
				if (field != 'date' && request.request_all_data.invoice[field]) {
					invoice[field] = angular.copy(request.request_all_data.invoice[field]);
				}
			}

			invoice.work_time_old = angular.copy(invoice.work_time);
		}

		if (_.isUndefined(invoice.request_all_data.surcharge_fuel_avg)) {
			invoice.request_all_data.surcharge_fuel_avg = request.request_all_data.surcharge_fuel_avg;
		}

		if (_.isUndefined(invoice.request_all_data.surcharge_fuel_perc)) {
			invoice.request_all_data.surcharge_fuel_perc = request.request_all_data.surcharge_fuel_perc;
		}

		if (_.isUndefined(invoice.request_all_data.surcharge_fuel)) {
			invoice.request_all_data.surcharge_fuel = request.request_all_data.surcharge_fuel;
		}

		if (_.isUndefined(invoice.request_all_data.custom_fuel_surcharge)) {
			invoice.request_all_data.custom_fuel_surcharge = request.request_all_data.custom_fuel_surcharge;
		}

		if (!_.get(invoice, 'request_all_data.valuation')) {
			_.set(invoice, 'request_all_data.valuation', request.request_all_data.valuation);
		}

		if (!_.get(invoice, 'request_all_data.valuation.settings.updated')) {
			valuationUpdateToNewVersion(invoice);
		}

		if (!_.get(request, 'request_all_data.grandTotalRequest')) {
			fillGrandTotal(request);
		}
	}

	function removeAptSideSpace(request) {
		if(_.get(request, 'apt_from.value')) {
			request.apt_from.value = request.apt_from.value.trim();
		}

		if(_.get(request, 'apt_to.value')) {
			request.apt_to.value = request.apt_to.value.trim();
		}

		if(_.get(request, 'field_extra_dropoff.premise')) {
			request.field_extra_dropoff.premise = request.field_extra_dropoff.premise.trim();
		}

		if(_.get(request, 'field_extra_pickup.premise')) {
			request.field_extra_pickup.premise = request.field_extra_pickup.premise.trim();
		}
	}
}
