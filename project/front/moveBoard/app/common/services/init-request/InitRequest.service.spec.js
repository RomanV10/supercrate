describe('InitRequest.service.js', () => {
	let InitRequestFactory, request, moment_lib;

	beforeEach(() => {
		$rootScope.fieldData = testHelper.loadJsonFile('field-data-on-init-request-service.mock');
	});

	beforeEach(inject((_InitRequestFactory_, _CalculatorServices_, _moment_) => {
		InitRequestFactory = _InitRequestFactory_;
		let fieldData = testHelper.loadJsonFile('field-data-on-init-request-service.mock');
		_CalculatorServices_.init(fieldData);
		request = testHelper.loadJsonFile('request-old-valuation-InitRequest.service.mock');
		moment_lib = _moment_;
	}));

	describe('on init', () => {
		it('InitRequestFactory.initRequestFields should be defined', () => {
			expect(InitRequestFactory.initRequestFields).toBeDefined();
		});

		it('InitRequestFactory.initRequestAllData should be defined', () => {
			expect(InitRequestFactory.initRequestAllData).toBeDefined();
		});

		it('InitRequestFactory.initInvoiceRequest should be defined', () => {
			expect(InitRequestFactory.initInvoiceRequest).toBeDefined();
		});

		it('InitRequestFactory.initRequestDetails should be defined', () => {
			expect(InitRequestFactory.initRequestDetails).toBeDefined();
		});
	});

	describe('on open request', () => {
		describe('when old version of the valuation', () => {
			beforeEach(() => {
				let today = moment('2018-03-05');

				spyOn(window, 'moment').and.callFake(function (parameter) {
					if (parameter) {
						return moment_lib(parameter);
					} else {
						return today;
					}
				});

				InitRequestFactory.initRequestAllData(request, undefined);
			});

			it('valuation should be updated', () => {
				//given
				let expectedResult = {
					selected: {
						liability_amount: 444,
						deductible_level: 250,
						valuation_charge: 44.4,
						valuation_type: 2
					},
					settings: {
						valuation_plan: 0,
						updated: undefined
					},
					other: {
						admonition: false,
						set_by_manager: false,
						showWarning: false,
					},
					deductible_data: {}
				};
				//when
				request.request_all_data.valuation.settings.updated = undefined;

				//then
				expect(request.request_all_data.valuation).toEqual(expectedResult);
			});

			it('should settings.update be defined', () => {
				expect(request.request_all_data.valuation.settings.updated).toBeDefined();
			});
		});
	});

	describe('initInvoiceRequest', () => {
		let invoice = {};
		beforeEach(() => {
			let today = moment('2018-03-05');

			spyOn(window, 'moment').and.callFake(function (parameter) {
				if (parameter) {
					return moment_lib(parameter);
				} else {
					return today;
				}
			});
			invoice = {};
			InitRequestFactory.initInvoiceRequest(request, invoice);
		});

		it('should init invoice valuation', () => {
			//given
			let expectResult = {
				selected: {
					liability_amount: 444,
					deductible_level: 250,
					valuation_charge: 44.4,
					valuation_type: 2
				},
				settings: {
					valuation_plan: 0,
					updated: undefined
				},
				other: {
					admonition: false,
					set_by_manager: false,
					showWarning: false
				},
				deductible_data: {}
			};

			//when
			invoice.request_all_data.valuation.settings.updated = undefined;

			//then
			expect(invoice.request_all_data.valuation).toEqual(expectResult);
		});

		it('should settings.update be defined', () => {
			expect(invoice.request_all_data.valuation.settings.updated).toBeDefined();
		});

		describe('fill grandTotalRequest in request all data if request has old version', () => {
			it('should init default value', () => {
				// given
				let expectedResult = {
					min: 0,
					max: 0,
					longdistance: 0,
					flatrate: 0,
					invoice: 0,
				};
				// when
				InitRequestFactory.initRequestAllData(request, undefined);
				// then
				expect(request.request_all_data.grandTotalRequest).toEqual(expectedResult);
			});
		});
	});
});
