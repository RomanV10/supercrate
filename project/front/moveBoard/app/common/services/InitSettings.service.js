'use strict';

angular.module('move.requests')
	.factory('InitSettingsFactory', InitSettingsFactory);

// DON'T Write update from front site!!!!!!!!!!!
function InitSettingsFactory() {
	const BASIC_SETTINGS_NAME = 'basicsettings';
	const LOND_DISTANCE_SETTINGS_NAME = 'longdistance';
	const CALC_SETTINGS_NAME = 'calcsettings';
	const SCHEDULE_SETTINGS_NAME = 'schedulesettings';
	const CONTRACT_PAGE_SETTINGS_NAME = 'contract_page';
	const REVIEW_SETTINGS_NAME = 'reviews_settings';
	const CUSTOMIZED_TEXT_NAME = 'customized_text';
	const UPDATER_TEXT_NAME = 'updater_settings';
	let settingsNames = [
		BASIC_SETTINGS_NAME,
		LOND_DISTANCE_SETTINGS_NAME,
		CALC_SETTINGS_NAME,
		SCHEDULE_SETTINGS_NAME,
		REVIEW_SETTINGS_NAME,
		CONTRACT_PAGE_SETTINGS_NAME,
		CUSTOMIZED_TEXT_NAME,
		UPDATER_TEXT_NAME
	];
	let service = {};

	service.parseSettingsData = parseSettingsData;
	service.correctMergeSettings = correctMergeSettings;

	return service;

	function parseSettingsData(data) {
		data[BASIC_SETTINGS_NAME] = angular.fromJson(data[BASIC_SETTINGS_NAME]);
		data[LOND_DISTANCE_SETTINGS_NAME] = angular.fromJson(data[LOND_DISTANCE_SETTINGS_NAME]);
		data[CALC_SETTINGS_NAME] = angular.fromJson(data[CALC_SETTINGS_NAME]);
		data[SCHEDULE_SETTINGS_NAME] = angular.fromJson(data[SCHEDULE_SETTINGS_NAME]);
		data[REVIEW_SETTINGS_NAME] = angular.fromJson(data[REVIEW_SETTINGS_NAME]);
		data[CONTRACT_PAGE_SETTINGS_NAME] = angular.fromJson(data[CONTRACT_PAGE_SETTINGS_NAME]);
		data[CUSTOMIZED_TEXT_NAME] = angular.fromJson(data[CUSTOMIZED_TEXT_NAME]);
		data[UPDATER_TEXT_NAME] = angular.fromJson(data[UPDATER_TEXT_NAME]);
	}

	function correctMergeSettings(data, type, settings) {
		if (~settingsNames.indexOf(type)) {
			data[type] = _.isString(settings) ? angular.fromJson(settings) : settings;
		} else {
			data[type] = settings;
		}
	}
}
