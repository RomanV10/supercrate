'use strict';

import momentTimezone from 'moment-timezone';

angular
	.module('move.requests')
	.factory('MomentWrapperService', MomentWrapperService);

/* @ngInject */
function MomentWrapperService(datacontext) {
	let TIME_ZONE = null;

	let service = {
		getZeroMoment,
		parseTimestampToJsDate,
		getDateInTimezone
	};
	return service;

	function initTimezone() {
		TIME_ZONE = datacontext.getFieldData().timeZone;

		if (_.isEmpty(TIME_ZONE)) {
			throw new Error('Time zone is empty. You need to include time zone file (project/api/.env)');
		}

		momentTimezone.tz.add(TIME_ZONE.initial);
	}

	function getDateInTimezone(date) {
		if (!TIME_ZONE) {
			initTimezone();
		}
		if (!moment.isMoment(date)) {
			date = moment(date);
		}

		return momentTimezone.tz(date, TIME_ZONE.name);
	}

	// return moment for utc 0
	function getZeroMoment(date) {
		let parsedMoment;
		if (!date) {
			parsedMoment = moment();
		} else {
			parsedMoment = moment(date);
		}

		return moment(parsedMoment).utc().add(parsedMoment.utcOffset(), 'm');
	}

	function parseTimestampToJsDate(timestamp) {
		let result = new Date(new Date(timestamp).getTime() + (new Date().getTimezoneOffset() * 60 * 1000));
		return result;
	}
}
