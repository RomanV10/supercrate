describe('Service: FuelSurchargeService', function () {
	// instantiate service
	var service;
	var testHelper;
	var request;
	var CalcRequest;

	//update the injection
	beforeEach(inject(function (_FuelSurchargeService_, _testHelperService_, _CalcRequest_) {
		service = _FuelSurchargeService_;
		testHelper = _testHelperService_;
		CalcRequest = _CalcRequest_;

		request = testHelper.loadJsonFile('calculate-fuel-surcharge-test.mock')
	}));

	/**
	 * @description
	 * Sample test case to check if the service is injected properly
	 * */
	it('should be injected and defined', function () {
		expect(service).toBeDefined();
	});

	describe('makeLog,', function () {
		it('should return false when no parameters', function () {
			// given
			var result;

			// when
			result = service.makeLog();

			//then
			expect(result).toBeFalsy();
		});

		describe('when request', function () {
			var invoice;
			var baseFuel = 0;
			var place = "admin";
			var Invoice = false;
			var result;

			it('should return correct text when admin', function () {
				//given
				var expectedResult = "Fuel Surcharge was changed automatically on Admin page";

				//when
				result = service.makeLog(request, invoice, baseFuel, place, Invoice);

				//then
				expect(expectedResult).toBe(result[0].text);
			});

			it('should return $0 from', function () {
				//given
				var expectedResult = "$0";

				//when
				result = service.makeLog(request, invoice, baseFuel, place, Invoice);

				//then
				expect(expectedResult).toBe(result[0].from);
			});

			it('should return $38.81 to', function () {
				//given
				var expectedResult = "$38.81";

				//when
				result = service.makeLog(request, invoice, baseFuel, place, Invoice);

				//then
				expect(expectedResult).toBe(result[0].to);
			})
		});

		describe('when invoice', function () {
			var invoice;
			var baseFuel = 0;
			var place = "admin";
			var Invoice = true;
			var result;

			beforeEach(function () {
				invoice = request.request_all_data.invoice;
				invoice.request_all_data.surcharge_fuel = 40.00;
			});

			it('should return correct text when admin', function () {
				//given
				var expectedResult = "Fuel Surcharge was changed automatically for closing on Admin page";

				//when
				result = service.makeLog(request, invoice, baseFuel, place, Invoice);

				//then
				expect(expectedResult).toBe(result[0].text);
			});

			it('should return $0 from', function () {
				//given
				var expectedResult = "$0";

				//when
				result = service.makeLog(request, invoice, baseFuel, place, Invoice);

				//then
				expect(expectedResult).toBe(result[0].from);
			});

			it('should return $40 to', function () {
				//given
				var expectedResult = "$40";

				//when
				result = service.makeLog(request, invoice, baseFuel, place, Invoice);

				//then
				expect(expectedResult).toBe(result[0].to);
			})
		});
	});

	describe('updateFuelSurcharge', function () {
		var vm;
		var quote = 0;
		var type = 'request';
		var place = 'admin';
		var saveLogs = false;

		beforeEach(function () {
			vm = {
				request,
				invoice: request.request_all_data.invoice,
				Invoice: false,
			}
		});

		describe('should reject,', function () {
			var result;

			it('when no request_all_data', function () {
				//given
				type = 'invoice';
				vm.invoice = {};
				var expectedResult = '';

				//when
				service.updateFuelSurcharge(vm, quote, type, place, saveLogs)
					.then(function () {
						result = 'request all data found';
					}, function () {
						result = '';
					});
				$rootScope.$apply();

				//then
				expect(expectedResult).toBe(result);
			});

			it('when auto calculate long distance and invoice', function () {
				//given
				vm.Invoice = true;
				vm.states = {invoiceState: true};
				vm.request.service_type.raw = 7;
				vm.request.request_all_data.surcharge_fuel_avg = 10;
				vm.invoice.request_all_data.surcharge_fuel_avg = 10;
				vm.request.request_all_data.isFuelSuchrargeChangeByAdmin = true;
				quote = 500;
				var expectedResult = '';

				//when
				service.updateFuelSurcharge(vm, quote, type, place, saveLogs)
					.then(function () {
						result = 'not rejected';
					}, function () {
						result = '';
					});
				$rootScope.$apply();

				//then
				expect(expectedResult).toBe(result);
			})
		});

		describe('when flat rate should return all fuel 0', function () {
			var result;

			beforeEach(function () {
				//given
				vm.request.service_type.raw = 5;
				type = 'request';

				//when
				service.updateFuelSurcharge(vm, quote, type, place, saveLogs)
					.then(function (response) {
						result = response;
					}, function () {
						result = '';
					});
				$rootScope.$apply();
			});

			it('surcharge_fuel_avg should be 0', function () {
				expect(0).toBe(result.surcharge_fuel_avg);
			});

			it('surcharge_fuel should be 0', function () {
				expect(0).toBe(result.surcharge_fuel);
			});

			it('surcharge_fuel_perc should be 0', function () {
				expect(0).toBe(result.surcharge_fuel_perc);
			});
		});

		describe('should init fields for invoice', function () {
			beforeEach(function () {
				//given
				vm.invoice = {request_all_data: {}};
				type = 'invoice';

				//when
				service.updateFuelSurcharge(vm, quote, type, place, saveLogs)
			});

			it('service_type should be copied', function () {
				expect(vm.request.service_type.raw).toBe(vm.invoice.service_type.raw);
			});

			it('field_moving_to should be copied', function () {
				expect(vm.request.field_moving_to).toBe(vm.invoice.field_moving_to);
			});

			it('field_moving_from should be copied', function () {
				expect(vm.request.field_moving_from).toBe(vm.invoice.field_moving_from);
			});
		});

		describe('update request fuel from backend', function () {
			var surcharge_fuel_avg = 111;
			var surcharge_fuel_perc = 222;
			var surcharge_fuel = 333;
			var response = {surcharge_fuel_avg: surcharge_fuel_avg, surcharge_fuel_perc: surcharge_fuel_perc, surcharge_fuel: surcharge_fuel};
			var fake = function () {
				return {
					then: function (callbackGood) {
						callbackGood(response);
					}
				}
			};

			describe('simple request', function () {
				beforeEach(function () {
					var response = {surcharge_fuel_avg: surcharge_fuel_avg, surcharge_fuel_perc: surcharge_fuel_perc, surcharge_fuel: surcharge_fuel};
					var fake = function () {
						return {
							then: function (callbackGood) {
								callbackGood(response);
							}
						}
					};

					spyOn(CalcRequest, 'calcFuelSurcharge')
						.and
						.callFake(fake);

					type = 'request';
					vm.Invoice = false;
					vm.request.request_all_data.isFuelSuchrargeChangeByAdmin = undefined;

					//when
					service.updateFuelSurcharge(vm, quote, type, place, saveLogs);
					$rootScope.$apply();
				});

				it('check update request fuel from backend', function () {
					expect(surcharge_fuel).toBe(vm.request.request_all_data.surcharge_fuel);
				});

				it('check update request surcharge_fuel_perc from backend', function () {
					expect(surcharge_fuel_perc).toBe(vm.request.request_all_data.surcharge_fuel_perc);
				});

				it('check update request surcharge_fuel_avg from backend', function () {
					expect(surcharge_fuel_avg).toBe(vm.request.request_all_data.surcharge_fuel_avg);
				});
			});

			describe('lond distance', function () {
				var expectedFuelAvg = 0;
				var expectedFuelPerc = 0;
				var expectedFuel = 0;

				beforeEach(function () {
					spyOn(CalcRequest, 'calcFuelSurcharge')
						.and
						.callFake(fake);

					type = 'request';
					vm.Invoice = true;
					vm.states = {invoiceState: true};
					vm.request.service_type.raw = 7;
					vm.request.request_all_data.isFuelSuchrargeChangeByAdmin = undefined;
					vm.invoice.request_all_data.surcharge_fuel_avg = expectedFuelAvg;
					vm.invoice.request_all_data.surcharge_fuel_perc = expectedFuelPerc;
					vm.invoice.request_all_data.surcharge_fuel = expectedFuel;
				});

				describe('and invoice state', function () {
					beforeEach(function () {
						service.updateFuelSurcharge(vm, quote, type, place, saveLogs);
						$rootScope.$apply();
					});

					it('invoice fuel not changed', function () {
						expect(expectedFuel).toBe(vm.invoice.request_all_data.surcharge_fuel);
					});

					it('invoice surcharge_fuel_avg not changed', function () {
						expect(expectedFuelAvg).toBe(vm.invoice.request_all_data.surcharge_fuel_avg);
					});

					it('invoice surcharge_fuel_perc not changed', function () {
						expect(expectedFuelPerc).toBe(vm.invoice.request_all_data.surcharge_fuel_perc);
					})
				});

				describe('not invoice state', function () {
					beforeEach(function () {
						vm.Invoice = false;
						service.updateFuelSurcharge(vm, quote, type, place, saveLogs);
						$rootScope.$apply();
					});

					it('fuel changed', function () {
						expect(surcharge_fuel).toBe(vm.request.request_all_data.surcharge_fuel);
					});

					it('surcharge_fuel_avg changed', function () {
						expect(surcharge_fuel_avg).toBe(vm.request.request_all_data.surcharge_fuel_avg);
					});

					it('surcharge_fuel_perc changed', function () {
						expect(surcharge_fuel_perc).toBe(vm.request.request_all_data.surcharge_fuel_perc);
					})
				})
			});
		});

		describe('recalculate by surcharge_fuel_avg ', function () {
			var expectedFuel = 100;

			beforeEach(function () {
				vm.request.request_all_data.surcharge_fuel_avg = 10;
				vm.request.request_all_data.surcharge_fuel = 0;
				vm.request.request_all_data.surcharge_fuel_perc = 0;
				quote = 1000;
			});

			it('selected and changed by admin should calculate fuel', function () {
				//given
				vm.request.request_all_data.isFuelSuchrargeChangeByAdmin = true;

				//when
				service.updateFuelSurcharge(vm, quote, type, place, saveLogs);
				$rootScope.$apply();

				//then
				expect(expectedFuel).toBe(vm.request.request_all_data.surcharge_fuel);
			});

			it('calculator turn off', function () {
				//given
				vm.request.field_usecalculator.value = false;

				//when
				service.updateFuelSurcharge(vm, quote, type, place, saveLogs);
				$rootScope.$apply();

				//then
				expect(expectedFuel).toBe(vm.request.request_all_data.surcharge_fuel);
			})
		})
	})
});
