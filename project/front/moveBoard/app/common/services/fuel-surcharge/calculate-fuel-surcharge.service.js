angular
	.module('move.requests')
	.factory('FuelSurchargeService', FuelSurchargeService);

/* @ngInject */
function FuelSurchargeService($q, CalcRequest, RequestServices, $rootScope) {
	let Job_Completed = 'Completed';
	let pickupJobCompleted = 'Pickup Completed';
	let deliveryJobCompleted = 'Delivery Completed';

	let service = {};

	service.updateFuelSurcharge = updateFuelSurcharge;
	service.makeLog = makeLog;

	return service;

	function updateFuelSurcharge(vm, quote, type, place, saveLogs) {
		let defer = $q.defer();
		let importedFlags = $rootScope.availableFlags;
		let isNotLdOnClosingTab = !(vm.request.service_type.raw == 7 && _.get(vm, 'states.invoiceState'));
		let isInvoice = _.get(vm, 'states.invoiceState', vm.Invoice) && isNotLdOnClosingTab;
		let result = {surcharge_fuel_avg: 0, surcharge_fuel: 0, surcharge_fuel_perc: 0};

		let fieldCompanyFlags = _.get(vm, 'request.field_company_flags.value');
		let isJobCompleted = fieldCompanyFlags.hasOwnProperty(importedFlags.company_flags[Job_Completed]);
		let isPickupCompleted = fieldCompanyFlags.hasOwnProperty(importedFlags.company_flags[pickupJobCompleted]);
		let isDeliveryCompleted = fieldCompanyFlags.hasOwnProperty(importedFlags.company_flags[deliveryJobCompleted]);
		let isDontAllowUpdateFuelSurcharge = isJobCompleted || isPickupCompleted && isDeliveryCompleted;

		if (_.isUndefined(vm[type].request_all_data) || isDontAllowUpdateFuelSurcharge) {
			defer.reject();

			return defer.promise;
		}

		initServiceType(vm, type);
		initFieldMovingFrom(vm, type);
		initFieldMovingTo(vm, type);

		// Flat Rate service type id = 5. If Flat rate don't calculate fuel surcharge.
		if (vm[type].service_type.raw == 5) {
			defer.resolve(result);

			return defer.promise;
		}

		let baseFuel = _.get(vm, `${type}.request_all_data.surcharge_fuel`, 0);

		if (vm.request.field_usecalculator.value && !vm[type].request_all_data.custom_fuel_surcharge && checkChangeFuelByAdmin(isInvoice, vm)) {
			calculateFuelSurcharge(vm, type, quote, isInvoice, saveLogs, baseFuel, place)
				.then(() => defer.resolve(), () => defer.reject());
		} else if (vm.request.request_all_data.surcharge_fuel_avg && isNotLdOnClosingTab) {
			if (!reCalculateFuelSurcharge(vm[type], quote)) {
				defer.reject();
			}

			_sendLogToRequest(saveLogs, vm, baseFuel, place, isInvoice);

			defer.resolve();
		} else {
			defer.reject();
		}

		return defer.promise;
	}

	function initServiceType(vm, type) {
		if (!vm[type].service_type) {
			vm[type].service_type = vm.request.service_type;
		}
	}

	function initFieldMovingFrom(vm, type) {
		if (!vm[type].field_moving_from) {
			vm[type].field_moving_from = vm.request.field_moving_from;
		}
	}

	function initFieldMovingTo(vm, type) {
		if (!vm[type].field_moving_to) {
			vm[type].field_moving_to = vm.request.field_moving_to;
		}
	}

	function calculateFuelSurcharge(vm, type, quote, isInvoice, saveLogs, baseFuel, place) {
		let defer = $q.defer();

		CalcRequest
			.calcFuelSurcharge(vm[type], quote, vm.request.request_all_data.request_distance)
			.then(function (data) {
				if (isInvoice && vm.invoice.request_all_data) {
					updateFuel(vm, data, "invoice");
				} else if (!isInvoice) {
					updateFuel(vm, data);
				}
				updateRequestDistance(data, vm);
				_sendLogToRequest(saveLogs, vm, baseFuel, place, isInvoice);

				defer.resolve();
			}, function () {
				defer.reject();
			});

		return defer.promise;
	}
	
	function updateFuel(vm, data, type = "request") {
		vm[type].request_all_data.surcharge_fuel_avg = data.surcharge_fuel_avg;
		vm[type].request_all_data.surcharge_fuel_perc = data.surcharge_fuel_perc;
		vm[type].request_all_data.surcharge_fuel = data.surcharge_fuel;
	}

	function checkChangeFuelByAdmin(isInvoice, vm) {
		return isInvoice
			? _.isUndefined(vm.invoice.request_all_data.isFuelSuchrargeChangeByAdmin)
			: _.isUndefined(vm.request.request_all_data.isFuelSuchrargeChangeByAdmin);
	}

	function updateRequestDistance(data, vm) {
		if (data.request_distance) {
			vm.request.request_all_data.request_distance = data.request_distance;
		}
	}

	function _sendLogToRequest(saveLogs, vm, baseFuel, place, isInvoice) {
		if (saveLogs) {
			_sendLog(vm, baseFuel, place, isInvoice);
		}
	}

	function makeLog(request, invoice, baseFuel, place, Invoice) {
		let arr = [];
		let namePage = place == 'admin' ? 'Admin' : 'Account';

		if (request && !Invoice && baseFuel != request.request_all_data.surcharge_fuel) {
			let message = 'Fuel Surcharge was changed automatically on ' + namePage + ' page';

			arr.push({
				text: message,
				from: '$' + baseFuel,
				to: '$' + request.request_all_data.surcharge_fuel
			});
		}

		if (invoice && Invoice && !_.isUndefined(invoice.request_all_data.surcharge_fuel) && baseFuel != invoice.request_all_data.surcharge_fuel) {
			let message = 'Fuel Surcharge was changed automatically for closing on ' + namePage + ' page';

			arr.push({
				text: message,
				from: '$' + baseFuel,
				to: '$' + invoice.request_all_data.surcharge_fuel
			});
		}

		return !_.isEmpty(arr) && arr;
	}

	function _sendLog(vm, baseFuel, place, isInvoice) {
		let log = makeLog(vm.request, vm.invoice, baseFuel, place, isInvoice);
		let where = place == 'admin' ? 'Request was updated' : 'Request page';

		if (log && !_.isEmpty(log)) {
			RequestServices.sendLogs(log, where, vm.request.nid, 'MOVEREQUEST');
		}
	}

	function reCalculateFuelSurcharge(request, quote) {
		let isSelectFuelAvg = !_.isString(request.request_all_data.surcharge_fuel_avg) && request.request_all_data.surcharge_fuel_avg != 0;
		let result = false;

		if (request.request_all_data && request.service_type && isSelectFuelAvg) {
			let newFuel = quote * request.request_all_data.surcharge_fuel_avg / 100;

			if (newFuel != request.request_all_data.surcharge_fuel) {
				request.request_all_data.surcharge_fuel = newFuel;
				result = true;
			}
		}

		return result;
	}
}
