(function () {
    'use strict';

    angular.module('move.requests')
           .factory('CheckRequestService', CheckRequestService);

    CheckRequestService.$inject = ['SweetAlert', 'datacontext', 'serviceTypeService'];

    function CheckRequestService(SweetAlert, datacontext, serviceTypeService) {

        var fieldData = datacontext.getFieldData();
        var basicsettings = angular.fromJson(fieldData.basicsettings);

        var service = {};

        service.isMoveDetailsChanged = isMoveDetailsChanged;
        service.isValidRequestAddress = isValidRequestAddress;
        service.isValidRequestZip = isValidRequestZip;
        service.checkHomeEstimate = checkHomeEstimate;
        service.isDeliveryFlatRate = isDeliveryFlatRate;
        service.serviceTypes = serviceTypeService.getAllServiceTypes;
        service.getServiceTypesWithValidTitles = getServiceTypesWithValidTitles;
        service.isValidHomeEstimateTime = isValidHomeEstimateTime;

        return service;

        function isMoveDetailsChanged(details, serviceType) {
            if (_.isUndefined(details) || _.isNull(details)) {
                return false;
            }

            var result = details.hasOwnProperty("current_door")
                && details.hasOwnProperty("new_door")
                && details.hasOwnProperty("current_permit")
                && details.hasOwnProperty("new_permit")
                && details.hasOwnProperty("addcomment")
                && details.hasOwnProperty("admincomments")
                && details.hasOwnProperty("pickup")
                && details.hasOwnProperty("delivery")
                && details.hasOwnProperty("delivery_days")
                && details.hasOwnProperty("steps_origin")
                && details.hasOwnProperty("steps_destination");

            if (result) {
                result = details["current_door"] != "1"
                    || details["new_door"] != "1"
                    || details["current_permit"] != "None"
                    || details["new_permit"] != "None"
                    || details["steps_origin"] != "1"
                    || details["steps_destination"] != "1"
                    || details["admincomments"] != "" && details["admincomments"] != '<p></p>';

                if (serviceType == "5" || serviceType == "7") {
                    result = result || details["delivery"] != "";
                }
            }

            return result;
        }


        function isValidRequestAddress(request, isNotConfirmed) {
			let isCheckedStatus = isNotConfirmed ? request.status.raw != 3 : request.status.raw == 3;
            let result = true;
            
            if (request.status.raw == 4) {
            	isCheckedStatus = true;
			}

            if (isCheckedStatus) {
                let isMovingAndStorage = request.service_type.raw == '2' || request.service_type.raw == '6';

                if (isMovingAndStorage) {
                    if (request.request_all_data.toStorage ) {
                        //check destination address (to)
                        if (isNotValidThoroughfare(request.field_moving_to)) {
                            result = false;
                        }
                    } else {
                        //check origin address (from)
                        if (isNotValidThoroughfare(request.field_moving_from)) {
                            result = false;
                        }
                    }
                } else {
                    let isLoadingHelp = request.service_type.raw == 3;
                    let isUploadingHelp = request.service_type.raw == 4;
	                let isPackingDay = request.service_type.raw == 8;

                    if ((isNotValidThoroughfare(request.field_moving_from) || isNotValidThoroughfare(request.field_moving_to))
                        && !(isLoadingHelp || isUploadingHelp || isPackingDay)) {

                        result = false;
                    }

                    if (isLoadingHelp && isNotValidThoroughfare(request.field_moving_from)) {
                        result = false;
                    }
	
	                if (isPackingDay && isNotValidThoroughfare(request.field_moving_from)) {
		                result = false;
	                }

                    if (isUploadingHelp && isNotValidThoroughfare(request.field_moving_to)) {
                        result = false;
                    }
                }
            }

            return result;
        }
        
        function isValidHomeEstimateTime(request) {
            let isHomeEstimate = request.status.raw == 4;
            let result = true;
            
            if (isHomeEstimate
                && (_.isNull(request.home_estimate_actual_start.value)
                || _.isNull(request.home_estimate_start_time.value))) {
                result = false;
            }
            
            return result;
		}

        function isValidRequestZip(request, isNotConfirmed) {
            let isCheckedStatus = isNotConfirmed ? request.status.raw != 3 : request.status.raw == 3;
            let result = true;

            if (isCheckedStatus) {
                let isMovingAndStorage = request.service_type.raw == '2' || request.service_type.raw == '6';

                if (isMovingAndStorage) {
                    if (request.request_all_data.toStorage ) {
                        //check destination address (to)
                        if (isNotValidZip(request.field_moving_to)) {
                            result = false;
                        }
                    } else {
                        //check origin address (from)
                        if (isNotValidZip(request.field_moving_from)) {
                            result = false;
                        }
                    }
                } else {
                    let isLoadingHelp = request.service_type.raw == 3;
                    let isUploadingHelp = request.service_type.raw == 4;
	                let isPackingDay = request.service_type.raw == 8;

                    if ((isNotValidZip(request.field_moving_from) || isNotValidZip(request.field_moving_to))
                        && !(isLoadingHelp || isUploadingHelp || isPackingDay)) {

                        result = false;
                    }

                    if (isLoadingHelp && isNotValidZip(request.field_moving_from)) {
                        result = false;
                    }
	
	                if (isPackingDay && isNotValidZip(request.field_moving_from)) {
                    	result = false;
	                }

                    if (isUploadingHelp && isNotValidZip(request.field_moving_to)) {
                        result = false;
                    }
                }
            }

            return result;
        }

        function isNotValidZip(field) {
            return _.isEmpty(field.administrative_area) || _.isNull(field.administrative_area);
        }

        function isNotValidThoroughfare(field) {
            let result = field.thoroughfare == 'Enter Address' || !field.thoroughfare;
            return result;
        }

        function checkHomeEstimate(request) {
            let result = true;

            if (request.status.raw == 4) {
                if (!request.home_estimate_date.value) {
                    SweetAlert.swal("Please Select Home Estimate Date!");
                    result = false;
                }

                if (!request.home_estimator.value) {
                    SweetAlert.swal("Please Select Home Estimator!");
                    result = false;
                }
	
				let isValidAddress = isValidRequestAddress(request);
                if (!isValidAddress) {
                	SweetAlert.swal('Please Enter Address!');
                	result = false;
				}
            }

            return result;
        }

        function isDeliveryFlatRate(currentDate, request) {
            let result = false;
            let isFlatRate = request.service_type.raw == 5;

            if (isFlatRate
                && !_.isUndefined(request.delivery_date_from)
                && request.delivery_date_from.raw != false) {

                let momentCurrentDate = moment(currentDate);
                let dateFrom = moment(request.delivery_date_from.value, 'DD-MM-YYYY');
                let dateTo = moment(request.delivery_date_to.value, 'DD-MM-YYYY');

                if (request.delivery_date_to.raw == false) {
                    dateTo = moment(request.delivery_date_from.value, 'DD-MM-YYYY');
                }

                result = momentCurrentDate.isBetween(dateFrom, dateTo)
                    || momentCurrentDate.isSame(dateFrom)
                    || momentCurrentDate.isSame(dateTo);
            }

            return result;
        }
        function getServiceTypesWithValidTitles () {
	        var services = {
		        1: {
			        title: basicsettings.services.localMoveLabel
		        },
		        2: {
			        title: basicsettings.services.localMoveStorageLabel
		        },
		        3: {
			        title: basicsettings.services.loadingHelpLabel
		        },
		        4: {
			        title: basicsettings.services.unloadingHelpLabel
		        },
		        5: {
			        title: basicsettings.services.flatRateLabel
		        },
		        6: {
			        title: basicsettings.services.overnightStorageLabel
		        },
	            7: {
			        title: basicsettings.services.longDistanceLabel
		        },
		        8: {
			        title: basicsettings.services.packingDayLabel
		        }
	        };

	        return services;
        }
    }
})();
