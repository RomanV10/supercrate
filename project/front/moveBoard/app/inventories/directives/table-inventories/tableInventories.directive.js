(function () {
    'use strict';

    angular
        .module('app.inventories')
        .directive('tableInventories', tableInventories);

    tableInventories.$inject = ['InventoriesServices', 'logger','$timeout', 'EditInventoryServices', '$http', 'config'];

    function tableInventories(InventoriesServices, logger, $timeout, EditInventoryServices, $http, config) {

        var directive = {
           link: link,
            scope: {
                rooms: '=rooms',
                listInventories:'=listinventories',
                decreaseItemsLog: '=decrease',
                increaseItemsLog: '=increase',
                removedItemsLog: '=removed'
            },
	        template: require('./tableinventories.html'),
            restrict: 'A'
        };

        function link(scope, element, attrs){

            scope.superadmin = false;
            scope.decreaseItemsLog = [];
            scope.increaseItemsLog = [];
            scope.removedItemsLog = [];

            scope.decreaseItem = decreaseItem;
            scope.increaseItem = increaseItem;
            scope.changeActiveRoom = changeActiveRoom;
            scope.changeActiveFilter = changeActiveFilter;
            scope.isRecomended = isRecomended;
            scope.addCustomItem = addCustomItem;
            scope.getRequestInventory = getRequestInventory;
            scope.calculateTotals = calculateTotals;
            scope.addInventory = addInventory;
            scope.editInventory = editInventory;

            loadInventories();
            loadFilters(function(){changeActiveRoom(scope.rooms[0])});
            scope.custom = {};


            scope.$watch('search.title', function() {

            }, true);



            //methods
            function loadInventories(callback){
                var promise = InventoriesServices.getAllInventories();
                promise.then(function(data) {
                    scope.inventories = data;
                    if(callback)
                        callback();
                }, function(reason) {
                    logger.error(reason, reason, "Error");
                });
            }

            function loadFilters(callback){
                var promise = InventoriesServices.getFilters();

                promise.then(function(data) {
                    scope.filters = data;
                    if(callback)
                        callback();
                }, function(reason) {
                    logger.error(reason, reason, "Error");
                });
            }

	        function increaseItem(item) {
		        var has = false;

		        scope.$parent.vm.busyInventory = false;

		        scope.listInventories.some(function (element, index) {
			        var oldCount = _.clone(element.count);
			        if (item.id && element.id) {
				        if (element.id == item.id && scope.activeRoom.id == element.rid) {
					        element.count++;
					        var objForLog = _.clone(element);
					        has = true;
					        if (_.isEmpty(scope.increaseItemsLog)) {
						        objForLog.oldCount = oldCount;
						        scope.increaseItemsLog.push(objForLog);
					        }
					        var count = 0;
					        _.forEach(scope.increaseItemsLog, function (el, ind) {
						        if (el.id == item.id) {
							        scope.increaseItemsLog[ind] = objForLog;
							        count++;
						        }
					        });
					        if (!count) {
						        objForLog.oldCount = oldCount;
						        scope.increaseItemsLog.push(objForLog);
					        }
					        return true;
				        } else {
					        return false;
				        }
			        } else {
				        if (customEquals(item, element)) {
					        element.count++;
					        var objForLog = _.clone(element);
					        has = true;
					        if (_.isEmpty(scope.increaseItemsLog)) {
						        objForLog.oldCount = oldCount;
						        scope.increaseItemsLog.push(objForLog);
					        }
					        var count = 0;
					        _.forEach(scope.increaseItemsLog, function (el, ind) {
						        if (el.id == item.id) {
							        scope.increaseItemsLog[ind] = objForLog;
							        count++;
						        }
					        });
					        if (!count) {
						        objForLog.oldCount = oldCount;
						        scope.increaseItemsLog.push(objForLog);
					        }
					        return true;
				        } else {
					        return false;
				        }
			        }
		        });

		        if (!has) {
			        scope.listInventories.push({
				        id: item.id,
				        rid: scope.activeRoom.id,
				        fid: scope.activeFilter.id,
				        count: 1,
				        title: item.title,
				        cf: item.cubicFeet
			        });
		        }
		        // var msg = 'Inventory "'+item.title+'" added.'
		        // var message = {
		        //     text: msg,
		        //     event_type:'request_update',
		        //     nid : 'System',
		        // };
		        // $http.post(config.serverUrl+'server/move_sales_log',message)
		        // .success(function(data, status, headers, config) {
		        //     data.success = true;
		        // });
	        }

            function decreaseItem(item){

                var delIndex = -1;

                scope.listInventories.some(function(element, index){

                    if(item.id && element.id){
                        if(element.id == item.id && scope.activeRoom.id == element.rid){
                            var oldCount = _.clone(element.count);
                            element.count --;
                            var objForLog = _.clone(element);
                            if(element.count == 0)
                                delIndex = index;
                            if(_.isEmpty(scope.decreaseItemsLog)) {
                                objForLog.oldCount = oldCount;
                                scope.decreaseItemsLog.push(objForLog);
                            }
                            var count = 0;
                            _.forEach(scope.decreaseItemsLog, function (el, ind) {
                                if(el.id == item.id) {
                                    scope.decreaseItemsLog[ind].count = objForLog.count;
                                    count++;
                                }
                            });
                            if(!count){
                                objForLog.oldCount = oldCount;
                                scope.decreaseItemsLog.push(objForLog);
                            }
                            return true;
                    } else{
                        return false;
                    }
                    } else{
                        if(customEquals(item, element)){
                            var oldCount = _.clone(element.count);
                            element.count --;
                            var objForLog = _.clone(element);
                            if(element.count == 0)
                                delIndex = index;
                            if(_.isEmpty(scope.decreaseItemsLog)) {
                                objForLog.oldCount = oldCount;
                                scope.decreaseItemsLog.push(objForLog);
                            }
                            var count = 0;
                            _.forEach(scope.decreaseItemsLog, function (el, ind) {
                                if(el.id == item.id) {
                                    scope.decreaseItemsLog[ind].count = objForLog.count;
                                    count++;
                                }
                            });
                            if(!count){
                                objForLog.oldCount = oldCount;
                                scope.decreaseItemsLog.push(objForLog);
                            }
                            return true;
                        } else{
                            return false;
                        }
                    }
                });

                if(delIndex > -1) {
                    scope.listInventories.splice(delIndex, 1);
                    scope.removedItemsLog.push(item);
                    if(!_.isEmpty(scope.decreaseItemsLog)) {
                        var ind = _.findIndex(scope.decreaseItemsLog, {id: item.id});
                        scope.decreaseItemsLog.splice(ind, 1);
                    }
                }
                // var msg = 'Inventory "'+item.title+'" deleted.'
                // var message = {
                //     text: msg,
                //     event_type:'request_update',
                //     nid : 'System',
                // };
                // $http.post(config.serverUrl+'server/move_sales_log',message)
                // .success(function(data, status, headers, config) {
                //     data.success = true;
                // });
            }

            function changeActiveRoom(room){
                angular.forEach(scope.rooms, function (roomItem) {
                    roomItem.active = false;
                });
                scope.activeRoom = room;
                scope.activeRoom.active=true;
                changeActiveFilter(getRoomMainFilter(room));
            }

            function changeActiveFilter(filter){
                scope.filters.forEach( function (filterItem) {
              filterItem.active = false;

            });
                scope.activeFilter = filter;
                scope.activeFilter.active = true;
            }

            function getRoomMainFilter(room){
                var mainFilter = {};
                scope.filters.some(function(element, index){
                if(room.main == element.id){
                    mainFilter = element;
                    return true;
                } else{
                    return false;
                }
            });
                return mainFilter;
            }

            function isRecomended(fid){
                var isRecomended = false;
                scope.activeRoom.fids.some(function(element, index){
                if(fid == element){
                    isRecomended = true;
                    return true;
                } else{
                    return false;
                }
            });
            return isRecomended;
            }

            function convertCustomItem(){
                if(scope.custom.pounds){
                    return {
                        count: parseInt(scope.custom.count, 10),
                        fid: scope.activeFilter.id,
                        rid: scope.activeRoom.id,
                        title: scope.custom.title + ' (' + scope.custom.pounds + ' cuft)',
                        cf: scope.custom.pounds
                    }
                }
                else {
                    return {
                        count: parseInt(scope.custom.count, 10),
                        fid: scope.activeFilter.id,
                        rid: scope.activeRoom.id,
                        title: scope.custom.title + '(' + scope.custom.width + 'x' + scope.custom.height + 'x' + scope.custom.length + ')',
                        cf: Math.round(parseFloat(scope.custom.width) * 0.083333 * parseFloat(scope.custom.height) * 0.083333 * parseFloat(scope.custom.length) * 0.083333)
                    }
                }
            }

            function addCustomItem(){
                var item = convertCustomItem();
                scope.listInventories.push(item);
                scope.custom = {};//clear fields

                angular.element('#custom_item_name').focus();
            }

            function getRequestInventory(id){
                var count = 0;
                if( scope.listInventories != null)
                    scope.listInventories.some(function(element, index){
                        if(element.id == id && element.rid == scope.activeRoom.id){
                            count = element.count;
                            return true;
                        } else{
                            return false;
                        }
                    });
                return count;
            }

            function customEquals(first, second){
                return (first.title == second.title
                    && first.cf == second.cf && first.rid == second.rid);
            }

            function calculateTotals(){
                var totals = {counts:0, cfs:0, boxes:0, custom:0};
                angular.forEach(scope.listInventories, function(element, index){
                    if(angular.isDefined(element.fid)) {
                        if(element.fid == 14){
                            totals.boxes +=element.count;
                        }
                        if(element.fid == 23){
                            totals.custom +=element.count;
                        }
                        totals.counts+=element.count;
                        totals.cfs = totals.cfs + element.cf * element.count;
                    }
                });

                return totals;
            }

            function editInventory(inventory){
                EditInventoryServices.openModal(inventory).then(function(result){
                    if(result.remove){
                        var index =  getInventoryIndex(result.inventory.id);
                        scope.inventories.splice(index, 1);
                    }
                });
            }

            function addInventory(){
                EditInventoryServices.openModal({ id:null, title:'', fid: 0, cubicFeet:0, imgSrc: '', largeTitle:''}).then(function(result){
                    if(result.add){
                        scope.inventories.push(result.inventory);
                    }
                });
            }


            function getInventoryIndex(id){
                var ind = -1;
                scope.inventories.some(function(element, index){
                    if(element.id == id){
                        ind = index;
                        return true;
                    } else{
                        return false;
                    }
                });
                return ind;
            }
        }

        return directive;
    }
})();
