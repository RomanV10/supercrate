'use strict';

angular
	.module('app.inventories')
	.controller('InventoriesController', InventoriesController);

InventoriesController.$inject = ['InventoriesServices', 'logger', '$scope', '$rootScope', 'RequestServices', 'requestObservableService'];

function InventoriesController(InventoriesServices, logger, $scope, $rootScope, RequestServices, requestObservableService) {
	const DECREASED_INVENTORY_ITEM_ACTIVITY = 'decreased';
	const INCREASED_INVENTORY_ITEM_ACTIVITY = 'increased';
	const REMOVED_INVENTORY_ITEM_ACTIVITY = 'removed';
	const ADDED_INVENTORY_ITEM_ACTIVITY = 'added';

	const WEIGHT_TYPE = {
		defaultType: 1,
		inventoryType: 2,
	};

	let vm = this;
	vm.busy = true;
	vm.init = init;
	vm.saveListInventories = saveListInventories;
	vm.listInventories = [];
	vm.oldListInventories = [];
	vm.busyInventory = false;
	vm.decreaseItemsLog = [];
	vm.increaseItemsLog = [];
	vm.removedItemsLog = [];
	vm.removedItems = [];
	vm.decreaseItems = [];
	vm.itemsLog = [];

	function init(rid) {
		vm.rid = rid.nid;

		loadRooms(rid).then(function () {
			if (_.isArray(rid.inventory) && _.isEmpty(rid.inventory) && (!rid.inventory.inventory_list || _.isEmpty(
				rid.inventory.inventory_list))) {
				rid.inventory = {inventory_list: []};
			}

			if (rid.inventory.inventory_list == null
				|| !_.isArray(rid.inventory.inventory_list)) {
				vm.listInventories = [];

				if ((rid.service_type.raw == 2 || rid.service_type.raw == 6) && rid.request_all_data && rid.toStorage) {

					if (rid.request_all_data.additional_inventory) {
						_.forEach(rid.request_all_data.additional_inventory, function (item) {
							item.additional = true;
							vm.listInventories.push(item);
						});
					}
				}
			} else {
				let inv = rid.inventory.inventory_list;
				vm.listInventories = inv;
				vm.oldListInventories = angular.copy(inv);
			}
			checkRoomIds(vm.listInventories);
		});
	}

	function checkRoomIds(itemsArray) {
		_.forEach(itemsArray, item => {
			if(!~_.findIndex(vm.rooms, {id: +item.rid})) {
				item.rid = vm.rooms[0].id;
			}
		});
	}

	function loadRooms(rid) {
		return new Promise(function (resolve, reject){
			InventoriesServices.getRooms(rid)
				.then(function (data) {
					vm.busy = false;
					vm.rooms = data;
					resolve();
				}, function (reason) {
					vm.busy = false;
					logger.error(reason, reason, "Error");
					resolve();
				});
		});
	}

	function saveListInventories(isCustom, list, request) {
		vm.busyInventory = true;

		var req = $scope.$parent.request || request;
		var isInventoryChange = false;
		//If storage save for storage also

		if (list) {
			vm.listInventories = list;
		}

		var details = [];

		var msg = {
			text: 'Inventory weight was changed.',
			from: $scope.request.inventory_weight.cfs + ' cfs.',
			to: InventoriesServices.calculateTotals(vm.listInventories).cfs + ' cfs.'
		};

		details.push(msg);

		if (!_.isEmpty(vm.decreaseItemsLog)) {
			_.forEach(vm.decreaseItemsLog, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was decreased.',
					from: item.oldCount,
					to: item.count
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(vm.increaseItemsLog)) {
			_.forEach(vm.increaseItemsLog, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was increased.',
					to: item.count
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(vm.removedItemsLog)) {
			_.forEach(vm.removedItemsLog, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was removed.'
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(vm.removedItems)) {
			_.forEach(vm.removedItems, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was removed.'
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(vm.decreaseItems)) {
			_.forEach(vm.decreaseItems, function (item) {
				var msg = {
					text: 'Inventory ' + item.title + ' was decreased.',
					from: item.oldCount,
					to: item.count
				};
				details.push(msg);
			});
		}

		if (!_.isEmpty(vm.listInventories)) {
			let removedItem = [];

			_.forEach(vm.listInventories, function (item) {
				let activity = getItemActivity(item);

				if (_.isEqual(activity, REMOVED_INVENTORY_ITEM_ACTIVITY)) {
					removedItem.push(angular.copy(item));
				}

				if (!_.isEmpty(activity)) {
					isInventoryChange = true;

					var msg = {
						text: 'Inventory ' + item.title + ' was ' + activity + '.',
						from: item.oldCount || '0',
						to: item.count || '0'
					};

					details.push(msg);
				}
			});

			vm.listInventories = vm.listInventories.filter(item => !_.find(removedItem, {id: item.id}));
		}

		if ((req.service_type.raw == 2 || req.service_type.raw == 6) && !_.isEqual(req.storage_id, 0)) {
			InventoriesServices.saveListInventories(req.storage_id, vm.listInventories);
			$scope.$emit('updateWeightType', {
				id: req.storage_id,
				type: vm.listInventories.length ? WEIGHT_TYPE.inventoryType : WEIGHT_TYPE.defaultType
			});
		}

		if (!isCustom && !list) {
			$scope.$parent.$parent.select($scope.$parent.$parent.tabs[0]);
		}

		if (isInventoryChange && !isCustom && !_.isEmpty(details)) {
			RequestServices.sendLogs(details, 'Request was updated', vm.rid, 'MOVEREQUEST');
		}

		var invArr = [];
		var invAddArr = [];

		_.forEach(vm.listInventories, function (item) {
			if (!item.additional)
				invArr.push(item);
			if (item.additional)
				invAddArr.push(item);
		});

		$scope.$parent.request.inventory.inventory_list = vm.listInventories;
		$scope.request.inventory_weight = InventoriesServices.calculateTotals(vm.listInventories);

		if ($scope.request.inventory_weight.cfs == 0) {
			if ($scope.$parent.request.field_useweighttype.value != 1) {
				// $scope.$parent.request.field_useweighttype.value = 1;
				// requestObservableService.setChanges('weightTypeChanged');
			}
		} else {
			if ($scope.$parent.request.field_useweighttype.value != 2) {
				// $scope.$parent.request.field_useweighttype.value = 2;
				// requestObservableService.setChanges('weightTypeChanged');
			}
		}

		$scope.$parent.request.request_all_data.additional_inventory = invAddArr;
		RequestServices.saveReqData($scope.$parent.request.nid, $scope.$parent.request.request_all_data);
		requestObservableService.setChanges('inventoryUpdate', $scope.$parent.request.visibleWeight);


		if ($scope.$parent.request.move_size.raw == 11) {
			$rootScope.$broadcast('addCommercialInventory');
		}

		let packingId = _.get(req, 'request_all_data.packing_request_id');

		if (packingId) {
			InventoriesServices.saveListInventories(packingId, vm.listInventories);
		}

		requestObservableService.setChanges('weightTypeChanged', {
			field_useweighttype: vm.listInventories.length ? WEIGHT_TYPE.inventoryType : WEIGHT_TYPE.defaultType,
		});
		RequestServices.updateRequest(req.nid, {
			'field_useweighttype': vm.listInventories.length ? WEIGHT_TYPE.inventoryType : WEIGHT_TYPE.defaultType
		});

		InventoriesServices.saveListInventories(vm.rid, invArr)
			.then(function () {
				$rootScope.$broadcast('onInventorySaved');
			});
	}

	function getItemActivity(item) {
		let result = '';

		if (checkDecrisedItem(item)) {
			result = DECREASED_INVENTORY_ITEM_ACTIVITY;
		} else if (checkIncreasedItem(item)) {
			result = INCREASED_INVENTORY_ITEM_ACTIVITY;
		} else if (checkRemovedItem(item)) {
			result = REMOVED_INVENTORY_ITEM_ACTIVITY;
		} else if (checkAddedItem(item)) {
			result = ADDED_INVENTORY_ITEM_ACTIVITY;
		}

		return result;
	}

	function checkDecrisedItem(item) {
		return item.count < item.oldCount && item.count != 0;
	}

	function checkIncreasedItem(item) {
		return item.count > item.oldCount && item.count != 0 && item.oldCount != 0;
	}

	function checkRemovedItem(item) {
		return item.count == 0 && item.oldCount > 0;
	}

	function checkAddedItem(item) {
		return (item.oldCount == 0 || _.isUndefined(item.oldCount)) && item.count > 0;
	}
}

