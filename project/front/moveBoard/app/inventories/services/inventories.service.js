(function () {
	'use strict';

	angular
		.module('app.inventories')
		.factory('InventoriesServices', InventoriesServices);

	InventoriesServices.$inject = ['$http', '$rootScope', '$q', 'config', 'datacontext', 'SweetAlert', 'apiService', 'moveBoardApi', 'newInventoriesServices'];

	function InventoriesServices($http, $rootScope, $q, config, datacontext, SweetAlert, apiService, moveBoardApi, newInventoriesServices) {
		var services = {};

		//new methods
		var datarooms = [
			{id: 1, title: 'Studio', fids: [8, 12, 14, 16, 18], main: 12, src: "/moveBoard/content/img/icons/bed.png"},
			{id: 2, title: 'Kitchen', fids: [17, 8, 14, 16, 25, 29], main: 17, src: "/moveBoard/content/img/icons/cooking.png"},
			{id: 17, title: 'Boxes', fids: [14], main: 12, src: "/moveBoard/content/img/icons/boxes.png"},
			{id: 4, title: 'Bedroom 2', fids: [8, 12, 14, 16, 18], main: 12, src: "/moveBoard/content/img/icons/sofa.png"},
			{id: 5, title: 'Bedroom 3', fids: [8, 12, 14, 16, 18], main: 12, src: "/moveBoard/content/img/icons/bed.png"},
			{id: 3, title: 'Bedroom', fids: [8, 12, 14, 16, 18], main: 12, src: "/moveBoard/content/img/icons/bed.png"},
			{id: 6, title: 'Bedroom 4', fids: [8, 12, 14, 16, 18], main: 12, src: "/moveBoard/content/img/icons/bed.png"},
			{id: 7, title: 'Living Room', fids: [8, 10, 25, 9, 17, 29, 14, 16], main: 8, src: "/moveBoard/content/img/icons/livingroom.png"},
			{id: 8, title: 'Dining Room', fids: [25, 14, 10, 8, 16], main: 25, src: "/moveBoard/content/img/icons/cutlery.png"},
			{id: 9, title: 'Office', fids: [8, 10, 25, 9, 17, 29, 14, 16], main: 21, src: "/moveBoard/content/img/icons/desk.png"},
			{id: 10, title: 'Extra Rooms', fids: [8, 18, 10, 12, 25, 9, 17, 29, 14, 16, 28, 30], main: 8, src: "/moveBoard/content/img/icons/bed.png"},
			{id: 11, title: 'Basement', fids: [8, 10, 25, 9, 17, 29, 14, 16], main: 21, src: "/moveBoard/content/img/icons/stairs.png"},
			{id: 12, title: 'Garage', fids: [27, 14, 17], main: 27, src: "/moveBoard/content/img/icons/garage.png"},
			{id: 13, title: 'Patio', fids: [26], main: 26, src: "/moveBoard/content/img/icons/grill.png"},
			{id: 14, title: 'Play Room', fids: [8, 30, 28, 14, 16, 17], main: 8, src: "/moveBoard/content/img/icons/guitar.png"},
			{id: 15, title: 'All', fids: [8, 12, 14, 16, 18], main: 12, src: "/moveBoard/content/img/icons/boxes.png"},
			{id: 16, title: 'Other', fids: [8, 12, 14, 16, 18], main: 12, src: "/moveBoard/content/img/icons/vacuum-cleaner.png"}
		];


		/*   var active_rooms = [];
		   1|Living room
		   2|Dining room
		   3|Office
		   4|Extra room
		   5|Basement/storage
		   6|Garage
		   7|Patio
		   8|Play room    */

		services.getListInventories = getListInventories;
		services.getAllInventories = getAllInventories;
		services.getFilters = getFilters;
		services.getRooms = getRooms;
		services.saveListInventories = saveListInventories;
		services.calculateTotals = calculateTotals;
		services.calculateClosingWeight = calculateClosingWeight;
		services.checkIfChangeMoveSize = checkIfChangeMoveSize;
		services.revertToPreviousInventory = revertToPreviousInventory;

		//old
		services.addInventory = addInventory;
		services.deleteInventory = deleteInventory;

		function checkIfChangeMoveSize(id, moveSize) {
			var deferred = $q.defer();

			var data = {
				"entity_id": id,
				"selected": moveSize
			};

			apiService.postData(moveBoardApi.inventory.request.checkAllowChangeMoveSize, data)
				.then(function (response) {
					deferred.resolve(response.data);
				}, function () {
					deferred.reject();
				});

			return deferred.promise;
		};


		function calculateTotals(listInventories) {
			var totals = {counts: 0, cfs: 0, boxes: 0, custom: 0};

			angular.forEach(listInventories, function (item, index) {
				if (item && angular.isDefined(item.fid)) {
					if (item.fid == 14) {
						totals.boxes += item.count;
					}

					if (item.fid == 23) {
						totals.custom += item.count;
					}

					totals.counts += item.count;

					if (angular.isDefined(item.cf)) {
						totals.cfs += item.cf * item.count;
					}
				}
			});

			return totals;
		}

		function calculateClosingWeight(request) {
			let inventory = newInventoriesServices.getSelectedInventory(request, request.contract);
			let weight = calculateTotals(inventory).cfs;

			return weight;
		}


		function getListInventories(reqId) {
			var deferred = $q.defer();


			$http.get(config.serverUrl + 'server/move_invertory/' + reqId)
				.success(function (data, status, headers, config) {
					var inventory = data.inventory_list;

					if (inventory == null) {
						inventory = [];
					}

					deferred.resolve(inventory);
				})
				.error(function (data, status, headers, config) {
					deferred.reject(data);
					SweetAlert.swal("Failed!", 'Something going wrong', "error");
				});

			return deferred.promise;
		}


		function getAllInventories() {

			var deferred = $q.defer();

			/*      var data = [
					  { id:1, title:'Chair', fid: 20, cubicFeet:5, imgSrc: 'img', largeTitle:'Chair(30x20x60)'},
					  { id:2, title:'Sofa', fid: 20, cubicFeet:10, imgSrc:'img', latgeTitle:'Sofa(60x50x40)'},
					  { id:3, title:'Box', fid:22, cubicFeet:10, imgSrc:'img', largeTitle: 'Box(70x81x66)'},
					  { id:4, title:'Guitar', fid: 21, cubicFeet:3, imgSrc:'img', largeTitle:'Guitar music'},

				  ];*/

			$http.post(config.serverUrl + 'server/move_invertory/get_invertory')
				.success(function (data, status, headers, config) {
					deferred.resolve(data);
				})
				.error(function (data, status, headers, config) {
					SweetAlert.swal("Failed!", 'Something going wrong', "error");
					deferred.reject(data);
				});

			return deferred.promise;
		}

		function addInventory(inventory) {
			var deferred = $q.defer();

			$http.post(config.serverUrl + 'server/saveInventory/add', inventory)
				.success(function (data, status, headers, config) {
					deferred.resolve(data);
				})
				.error(function (data, status, headers, config) {
					SweetAlert.swal("Failed!", 'Something going wrong', "error");
					deferred.reject(data);
				});

			return deferred.promise;
		}

		function getFilters() {
			var deferred = $q.defer();
			var fieldData = datacontext.getFieldData();
			var filters = fieldData.taxonomy.inventory_items_filter;
			var data = [];
			angular.forEach(filters, function (filter_name, id) {
				data.push({id: id, title: filter_name});
			});

			data.push({id: 23, title: 'Custom Item'});

			deferred.resolve(data);
			return deferred.promise;
		}


		function addRoom(roomName) {
			var roomId = 0;

			angular.forEach(datarooms, function (room, id) {
				if (room.title == roomName) {
					roomId = id;
				}
			});

			return datarooms[roomId];
		}

		function getRooms(request) {
			var deferred = $q.defer();
			var active_rooms = [];
			active_rooms.push(addRoom('All'));
			var requestSize = request.move_size.raw;

			switch (requestSize) {
				case '1': // Room
					active_rooms.push(addRoom('Bedroom'));
					break;
				case '2': //Studio
					active_rooms.push(addRoom('Studio'));

					break;
				case '3': // 1 Small Bedroom
					active_rooms.push(addRoom('Bedroom'));
					break;
				case '4': // 1 Large Bedroom
					active_rooms.push(addRoom('Bedroom'));
					break;
				case '5': // 2 Small Bedroom
					active_rooms.push(addRoom('Bedroom'));
					active_rooms.push(addRoom('Bedroom 2'));
					break;
				case '6': // 2 Large Bedroom
					active_rooms.push(addRoom('Bedroom'));
					active_rooms.push(addRoom('Bedroom 2'));
					break;
				case '7': //3  Bedrooms
					active_rooms.push(addRoom('Bedroom'));
					active_rooms.push(addRoom('Bedroom 2'));
					active_rooms.push(addRoom('Bedroom 3'));
					break;
				case '8': //2  Bedrooms
					active_rooms.push(addRoom('Bedroom'));
					active_rooms.push(addRoom('Bedroom 2'));
					break;
				case '9': //3  Bedrooms
					active_rooms.push(addRoom('Bedroom'));
					active_rooms.push(addRoom('Bedroom 2'));
					active_rooms.push(addRoom('Bedroom 3'));
					break;
				case '10': //4  Bedrooms
					active_rooms.push(addRoom('Bedroom'));
					active_rooms.push(addRoom('Bedroom 2'));
					active_rooms.push(addRoom('Bedroom 3'));
					active_rooms.push(addRoom('Bedroom 4'));
					break;
			}

			active_rooms.push(addRoom('Kitchen'));

			angular.forEach(request.rooms.raw, function (value, id) {
				angular.forEach(datarooms, function (room, id) {
					if (room.id == parseInt(value) + 6) {
						active_rooms.push(room);
					}
				});
			});

			active_rooms.push(addRoom('Other'));
			deferred.resolve(active_rooms);

			return deferred.promise;
		}


		function deleteInventory(inventory) {
			var deferred = $q.defer();

			$http.post(config.serverUrl + 'server/deleteInventory/delete', inventory)
				.success(function (data, status, headers, config) {
					deferred.resolve(data);
				})
				.error(function (data, status, headers, config) {
					SweetAlert.swal("Failed!", 'Something going wrong', "error");
					deferred.reject(data);
				});

			return deferred.promise;
		}

		function saveListInventories(nid, listInventories) {
			let data = {data: {items: listInventories}};
			let jsonDetails = JSON.stringify(data);

			return apiService.postData(moveBoardApi.inventoryOld.setInventory + nid, jsonDetails);
		}

		function revertToPreviousInventory(nid, items) {
			let data = {
				"entity_id": nid,
				"back_up": items
			};

			return apiService.postData(moveBoardApi.inventories.request.revertToPreviousInventory, data);
		}

		return services;
	}


	function saveStep() {
		var deferred = $q.defer();

		var data = [
			{id: 1, title: 'Chair', fid: 20, cubicFeet: 5, imgSrc: 'img', largeTitle: 'Chair(30x20x60)'},
			{id: 2, title: 'Sofa', fid: 20, cubicFeet: 10, imgSrc: 'img', latgeTitle: 'Sofa(60x50x40)'},
			{id: 3, title: 'Box', fid: 22, cubicFeet: 10, imgSrc: 'img', largeTitle: 'Box(70x81x66)'},
			{id: 4, title: 'Guitar', fid: 21, cubicFeet: 3, imgSrc: 'img', largeTitle: 'Guitar music'},
		];

		deferred.reject(data);

		return deferred.promise;
	}

})();
