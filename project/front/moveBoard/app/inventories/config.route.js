'use strict';

angular
	.module('app.inventories')
	.run(routeConfig);

/*@ngInject*/
function routeConfig(routehelper) {
	routehelper.configureRoutes(getRoutes());
}

function getRoutes() {
	return [
		{
			url: '/inventories',
			config: {
				title: 'inventories',
				template: require('./templates/inventories.html'),
			}
		}
	];
}
