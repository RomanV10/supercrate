import './seach.styl'
(function () {
    'use strict';

    angular
        .module('app.search')
        .directive('ccSearchInput', ccSearchInput);

    /* @ngInject */
    function ccSearchInput($http, config, $uibModal, $q, $location, SearchServices, logger, $rootScope, $timeout, PaymentServices, StorageService, PermissionsServices, openRequestService) {
        var directive = {
            link: link,
            template: require('./search.template.html'),
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            const MOVEREQUEST = 0;
            const STORAGEREQUEST = 1;
			var promiseArray = [];
            scope.showSearch = false;
            scope.showSearchIcon = true;
            scope.PermissionsServices = PermissionsServices;
            $(".js-custom-scrollbar").mCustomScrollbar({
                theme: 'custom',
                scrollInertia: 0
            });

            scope.searchRequests = [];

            $("#gSearchButton").bind('click', function () {
                var condition = $("#gSearch").val();
                $location.path('/search/' + condition).replace();
                //go to search page
            });
            $("#gSearchClean").bind('click', function () {
                var condition = $("#gSearch").val();
                scope.search = '';
                // $location.path('/search/'+condition).replace();
                //go to search page
            });


            scope.requestEditModal = function (request, receipt) {

                scope.busy = true;
                scope.showSearch = false;

	            openRequestService.open(request.nid)
                    .then((data) => {
	                    if (receipt && data) {
		                    PaymentServices.openPaymentModal(data.request);
		                    openReceiptModal(data.request, findById(data.request.receipts, receipt.id));
	                    }
                    })
                    .finally(() => {
	                    scope.busy = false;
	                });
            };

            function findById(arr, id) {
                return arr.filter(function (item) {
                    return item.id == id;
                })[0]
            }

            function openReceiptModal(request, receipt) {

                var modalInstance = $uibModal.open({
	                template: require('../../requests/directives/payment/controllers/receipt-modal/receiptModal.html'),
                    controller: ReceiptModalCtrl,

                    resolve: {
                        receipt: function () {
                            return receipt;
                        },
                        request: function () {
                            return request;
                        }
                    }
                });
                modalInstance.result.then(function ($scope) {
                });
            }

            function ReceiptModalCtrl($scope, $uibModalInstance, receipt, request, Session, common, $rootScope) {
                $scope.receipt = receipt;
                $scope.request = request;
                $scope.range = common.range;
                $scope.busy = true;
                var session = Session.get();
                $scope.isAdmin = (session.userRole.indexOf('administrator') > -1);
                $scope.cardImages = [];
                if ($scope.isAdmin) {
                    if (angular.isDefined($scope.receipt.id))
                        getCardImage($scope.receipt.id)
                            .then(function (data) {
                                $scope.cardImages = data;
                                $scope.busy = false;
                            });
                } else {
                    $scope.busy = false;
                }

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.openCardPhoto = function (img) {
                    var modalInstance = $uibModal.open({
	                    template: require('../../requests/directives/payment/CardPhoto.html'),
                        controller: CardPhotoModalCtrl,
                        resolve: {
                            img: function () {
                                return img;
                            }
                        }
                    });
                    modalInstance.result.then(function ($scope) {
                    });
                };
                function CardPhotoModalCtrl($scope, $uibModalInstance, img, common) {
                    $scope.img = img;

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                }

                $scope.save = function () {
                    $scope.busy = true;
                    update_receipt($scope.request.nid, $scope.receipt).then(function () {
                        $scope.busy = false;
                        $rootScope.$broadcast('custom.receipt.received');
                        $scope.cancel();
                    });
                }

                $scope.dateFormat = function (date) {
                    return _.isNaN(moment.unix(date)._i) ? moment(date).format('DD MMM, YYYY') : moment.unix(date).format('DD MMM, YYYY h:mm a');
                };

            }

            function getCardImage(nid) {
                var deferred = $q.defer();
                $http
                    .post(config.serverUrl + 'server/move_request/get_card/' + nid)
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    .error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            }

            scope.requestOpenReceiptModal = function (request) {
                var entity = {
                    nid: request.entity_id,
                    score: request.score

                }

                scope.requestEditModal(entity, request)
                //  if(request.entity_type == MOVEREQUEST){
                //      request.nid = request.entity_id;
                //      $rootScope.$broadcast('request.open_search', request);
                //  }
                //  if(request.entity_type == STORAGEREQUEST){
                //      request.nid = request.entity_id;
                //      $rootScope.$broadcast('request.open_storage_request', request);
                //  }
                scope.showSearch = false;

            };

            angular.element(document).bind('click', checkForClosing);
			angular.element(document).bind('focus', checkForClosing);
            function checkForClosing(event) {
                var isClickedElementChildOfPopup = element
                    .find(event.target)
                    .length > 0;

                if (isClickedElementChildOfPopup)
                    return;

                $timeout(function () {
                    scope.showSearch = false;
					promiseArray = [];
                });
            }

            scope.$watch('search', function (newValue, oldValue) {
                if (angular.isDefined(newValue) && newValue.length > 3) {
					if (promiseArray.length) {
						$q.all(promiseArray[promiseArray.length-1]).then(function () {
							promiseArray.push(scope.searchRequest());
						});

					} else {
						promiseArray.push(scope.searchRequest());
					}
                }

            });
            scope.searchFocus = function () {
                if (angular.isDefined(scope.search) && scope.search.length > 3) {
                    scope.searchRequest();
                }
            };

            scope.removeSearchLine = () => {
                scope.search = '';
                scope.showSearch = false;
            };

            scope.searchRequest = function () {
            	var currentPromise = $q.defer();
                var promise = SearchServices.globalSearch(scope.search);
                scope.showPreloader = true;
                scope.showSearchIcon = false;
                $q.all(promise).then(function (resolve) {
                    scope.searchRequests = [];
                    scope.searchStorageRequests = [];
                    scope.searchReceiptsRequests = [];
                    scope.showPreloader = false;
                    scope.showSearch = true;
                    $timeout(function () {
                        scope.showSearchIcon = true;
                    }, 2200);
                    if (angular.isDefined(resolve.searchResult.data[MOVEREQUEST])) {
                        scope.searchRequests = resolve.searchResult.data[MOVEREQUEST];
                    }
                    if (angular.isDefined(resolve.searchResult.data[STORAGEREQUEST])) {
                        scope.searchStorageRequests = resolve.searchResult.data[STORAGEREQUEST];
                    }
                    const RECEIPT = 4;
                    if (angular.isDefined(resolve.searchResult.data[RECEIPT])) {
                        scope.searchReceiptsRequests = resolve.searchResult.data[RECEIPT];
                    }

                    // const LDREQUEST = 2;
                    // const STORAGE = 3;
                    // const RECEIPT = 4;

					currentPromise.resolve();
                }, function (reason) {
                    logger.error(reason, reason, "Error");
					currentPromise.resolve();
                });
                return currentPromise;
            };

            scope.requestOpenModalStorage = function (request) {
                scope.busy = true;
                scope.showSearch = false;
                StorageService.getReqStorage(1).then(function (data) {
                    StorageService.getStorageReqByID(request.entity_id).then(function (req) {
                        StorageService.openModal(req.data, request.entity_id, data);
                        scope.busy = false;
                    })

                });

            }
        };


    }


})();




