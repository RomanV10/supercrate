(function () {
    'use strict';

    angular
        .module('app.search')
    
     .controller('SearchController', function ( $state,logger,SearchServices,RequestServices) {

             // Inititate the promise tracker to track form submissions.

             var vm = this;
            var condition = $state.params.id;

    
                vm.busy= true;
                vm.requests = [{id:0,name:'Searching...'}];
                vm.dtInstance = {};


                var isnum = /^\d+$/.test(condition);
                if(isnum){
                    getRequestbyNid(condition);
                }
                else {
                                 
                    activateSerach(condition);
                    
                }

                function activateSerach(string) {

                      var condition = createCondition(string);

                     var promise = SearchServices.searchRequest(condition);

                      promise.then(function(data) {

                          vm.busy = false;
                        //  RequestServices.addRequest(data);
                          vm.requests = data.nodes;
                         
                      }, function(reason) {
                            vm.requests = null;
                              vm.busy = false;
                             logger.error(reason, reason, "Error");
                          
                      });
                }
        
                function getRequestbyNid(condition) {

                     var promise = RequestServices.getRequest(condition);

                      promise.then(function(data) {

                          vm.busy = false;
                          vm.requests[0] = data;

                      }, function(reason) {
                              vm.requests = null;
                              vm.busy = false;
                             logger.error(reason, reason, "Error");
                          
                      });
                }

                function validateEmail(email) {
                var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
                }


                function validatePhone(phone) {
                var re = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
                return re.test(phone);
                }


                function createCondition(string) {

                    var condition = [];

                    //Check if EMAIL
                    if(validateEmail(string)){
                        condition = {"page":0, "pagesize":20, "condition": { "field_e_mail":string}};
                    }
                    // CHECK IF PHONE NUMBER
                    else{
                        if(validatePhone(string)){
                        condition = {"page":0, "pagesize":20, "condition": { "field_phone":string}};
                    }else{
                        condition = {"page":0, "pagesize":20, "condition": { "field_last_name":string}};
                    }
                  }


                    return condition;

                }


    })

    


})();
    