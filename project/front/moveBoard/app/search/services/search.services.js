(function () {
  
    angular
        .module('app.search')
    
    .factory('SearchServices',RequestServices);

    RequestServices.$inject = ['$rootScope', '$http','$q','config', 'PermissionsServices', 'apiService', 'moveBoardApi'];
    
    function RequestServices($rootScope, $http,$q,config, PermissionsServices, apiService, moveBoardApi) {
        
        var service = {};

        service.searchRequest = searchRequest;
        service.globalSearch = globalSearch;
        
        return service;

        function globalSearch(condition) {
            return {
                searchResult: apiService.getData(moveBoardApi.search.globalSearch + condition)
            }
        }


 function searchRequest(condition) {

        
            var deferred = $q.defer(); 


           // data.push(condition);


            $http.post(config.serverUrl+'server/move_request/search',condition)
            .success(function(data, status, headers, config) {

                                    // TODO :Save to Local Storage For future use
                                    //service.saveRequests(data);
                                    deferred.resolve(data);

            })
            .error(function(data, status, headers, config) {

                                    // data.success = false;
                                    //SHOW ALERT LOG
                                     
                                     deferred.reject(data);

             });
         
            return deferred.promise;

   }

        
        
        
 
 }})();