(() => {
    angular
        .module('newInventories', ['ngPrint', 'AngularPrint', 'ngLazyRender', 'ui.router', 'app.core'])
})();
