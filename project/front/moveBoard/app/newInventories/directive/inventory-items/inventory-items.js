'use strict';
import './inventory-items.styl';
import '../button/button.styl';
(() => {

	angular
		.module('newInventories')
		.directive('inventoryItem', (InventoriesServices, $http, config, $timeout) => {
			return {
				scope: {
					item: '=inventoryData',
					list: "=",
					calculate: "=calculate",
					deleteCustomItem: '=',
					current: "=",
					room: "=",
					deleteItem: "&"

				},
				template:  require('./inventory-items.pug'),
				restrict: 'A',
				link: ($scope, element) => {

					const defaults = {
						el: '.js-counter',
						dom: {
							input: '.js-counter-input',
							plus: '.js-counter-plus',
							minus: '.js-counter-minus'
						}
					};

                    $scope.deleteItem = $scope.deleteItem();
					$scope.options = defaults;
					$scope.$el = $(element).find($scope.options.el);

					$scope.getDom = () => {
						$scope.$input = $scope.$el.find(this.options.dom.input);
						$scope.$plus = $scope.$el.find(this.options.dom.plus);
						$scope.$minus = $scope.$el.find(this.options.dom.minus);
					}

					$scope.findById = (id, arr) => {
						return arr.filter(item =>  (item.id === id && (item.rid === $scope.current || $scope.room === item.rid)))[0];
					}

					element.on('$destroy', function () {
						$scope.$destroy();
					});

					$scope.findByRoom = (id, arr) => {
						return arr.filter(item => (item.id === id && item.rid === $scope.room))[0];
					}

					$scope.checkValue = (value, maxValue) => {
						if (value == 0 && $scope.item.id >= 10000) {
							$scope.deleteCustomItem($scope.item)
						}
						if (value < 0) {
							value = 0;
						}
						if (value > maxValue) {
							value = maxValue;
						}
						return value;
					}

					$scope.setButtonAbility = ($el, maxValue) => {
						const $input = $el.find(this.options.dom.input);
						const $plus = $el.find(this.options.dom.plus);
						const $minus = $el.find(this.options.dom.minus);
						const value = Number($input.val());

						if (maxValue === 1) {
							$minus.prop('disabled', true);
							$plus.prop('disabled', true);
							$input.css('pointer-events', 'none');
						} else if (value === 1) {
							$minus.prop('disabled', true);
							$plus.prop('disabled', false);
						} else if (value === maxValue) {
							$minus.prop('disabled', false);
							$plus.prop('disabled', true);
						} else {
							$minus.prop('disabled', false);
							$plus.prop('disabled', false);
						}
					}

					$scope.changeValue = (difference, filter) => {

						if (!filter.count) {
							filter.count = difference
						} else {
							filter.count = filter.count + difference
						}

						const item = !$scope.room ? $scope.findById(filter.id, $scope.list) : $scope.findByRoom(filter.id, $scope.list);



						if (item) {
							item.count = filter.count;
						} else {
							const newItem = angular.copy(filter);
							newItem.cf = filter.cubicFeet;
							newItem.rid = Number($scope.current);
							$scope.list.push(newItem);
						}

						if (filter.count <= 0) {
							if (filter.id >= 10000) {
								$scope.removeCustomItem(filter)
							} else {
								$scope.deleteItem(filter)
							}
						}

						$scope.calculate($scope.list);
					}

					$scope.handleChange = (e) => {
						const $target = $(e.currentTarget);
						const $el = $target.parent(this.$el);
						const $input = $el.find(this.options.dom.input);
						let value = Number($input.val());
						const maxValue = Number($input.data('max'));
						if (!isNaN(value)) {
							value = this.checkValue(value, maxValue);
							$input.val(value);
							this.setButtonAbility($el, maxValue);
						} else {
							$input.val(1);
						}
					}

					$scope.removeCustomItem = item => {
						$scope.deleteCustomItem(item)
					}

					$scope.isCustom = item => item && item.id >= 10000;
					if ($scope.item) {
						const item = !$scope.room ? $scope.findById($scope.item.id, $scope.list) : $scope.findByRoom($scope.item.id, $scope.list);
						$scope.item.count = item && item.count ? Number(item.count) : 0
					}

					$scope.showAdd = $scope.item.count > 0

					$scope.$watch(() => $scope.item.count, () => {
						if ($scope.item) {
							const item = !$scope.room ? $scope.findById($scope.item.id, $scope.list) : $scope.findByRoom($scope.item.id, $scope.list);
							$scope.item.count = item && item.count ? Number(item.count) : 0
						}

						$scope.showAdd = $scope.item.count > 0

					})

					if ($scope.item.count <= 0) {
						if ($scope.item.id >= 10000) {
							$scope.removeCustomItem($scope.item)
						} else {
							$scope.deleteItem($scope.item)
						}
					}

					$scope.render = () => {
						if (this.$el.length) {
							this.getDom();
							this.$plus.on('click', this.changeValue.bind(this, +1));
							this.$minus.on('click', this.changeValue.bind(this, -1));
							this.$input.on('change', this.handleChange.bind(this));

							this.$el.each((i, elem) => {
								const $el = $(elem);
								const $input = $el.find(this.options.dom.input);
								const maxValue = Number($input.data('max'));
								this.setButtonAbility($el, maxValue);
							});
						}
						return this;
					}
				}
			}
		});

})();
