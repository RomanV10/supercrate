
const selectors = {
	el: '.js-button'
};

const classes = {
	hover: {
		disable: 'button_hover_disable'
	}
};

const $buttons = $(selectors.el);

const onTouch = e => {
	const $target = $(e.currentTarget);
	$target.addClass(classes.hover.disable);
};


const setHandlers = () => {
	$buttons.on('touchstart', onTouch);
};

const render = () => {
	if ($buttons.length) {
		setHandlers();
	}
};
render();
