describe('Service: newInventoriesServices', function () {
	// instantiate service
	var service;
	var request;
	var testHelper;
	var contract;

	//update the injection
	beforeEach(inject(function (_newInventoriesServices_, _testHelperService_) {
		service = _newInventoriesServices_;
		testHelper = _testHelperService_;

		request = testHelper.loadJsonFile('account-inventories.services-request.mock');
		contract = testHelper.loadJsonFile('account-inventories.services-contract.mock');
	}));

	/**
	 * @description
	 * Sample test case to check if the service is injected properly
	 * */
	it('should be injected and defined', function () {
		expect(service).toBeDefined();
	});

	describe('getSelectedInventory', function () {
		describe('when no contract', function () {
			it('should get full inventory', function () {
				// given
				var result;
				var expectedResult = [ { id : 1796, count : 1, oldCount : 0, title : 'Clothing Rack', fid : 1, rid : '15', cf : '15', photo : { name : 'Clothing Rack', fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Closing-Rack.jpg?itok=r8Nq0eND' }, cubicFeet : '15' }, { id : 1792, count : 1, oldCount : 0, title : "Print, Photo 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Print, Photo 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Photo-large_0.jpg?itok=81KI9lFs' }, cubicFeet : '10' }, { id : 1793, count : 1, oldCount : 0, title : 'Fan, Floor', fid : 1, rid : '15', cf : '12', photo : { name : 'Fan, Floor', fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Fan-Standing.jpg?itok=5qkifsfX' }, cubicFeet : '12' }, { id : 1794, count : 1, oldCount : 0, title : "Painting 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Painting 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Painting-Large.jpg?itok=tFjZD8e8' }, cubicFeet : '10' } ];

				// when
				result = service.getSelectedInventory(request);

				//then
				expect(expectedResult).toEqual(result);
			});
		});

		describe('when contract', function () {
			it('should get only submitted inventory', function () {
				// given
				var result;
				var expectedResult = [ { id : 1796, count : 1, oldCount : 0, title : 'Clothing Rack', fid : 1, rid : '15', cf : '15', photo : { name : 'Clothing Rack', fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Closing-Rack.jpg?itok=r8Nq0eND' }, cubicFeet : '15' }, { id : 1792, count : 1, oldCount : 0, title : "Print, Photo 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Print, Photo 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Photo-large_0.jpg?itok=81KI9lFs' }, cubicFeet : '10' }, { id : 1794, count : 1, oldCount : 0, title : "Painting 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Painting 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Painting-Large.jpg?itok=tFjZD8e8' }, cubicFeet : '10' } ];

				// when
				result = service.getSelectedInventory(request, contract);

				//then
				expect(expectedResult).toEqual(result);
			});

			describe('moving&storage', function () {
				it('should get only submitted inventory', function () {
					// given
					request.service_type.raw = 6;
					var result;
					var expectedResult = [ { id : 1796, count : 1, oldCount : 0, title : 'Clothing Rack', fid : 1, rid : '15', cf : '15', photo : { name : 'Clothing Rack', fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Closing-Rack.jpg?itok=r8Nq0eND' }, cubicFeet : '15' }, { id : 1792, count : 1, oldCount : 0, title : "Print, Photo 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Print, Photo 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Photo-large_0.jpg?itok=81KI9lFs' }, cubicFeet : '10' }, { id : 1794, count : 1, oldCount : 0, title : "Painting 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Painting 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Painting-Large.jpg?itok=tFjZD8e8' }, cubicFeet : '10' }, { id : 1792, count : 1, oldCount : 0, title : "Print, Photo 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Print, Photo 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Photo-large_0.jpg?itok=81KI9lFs' }, cubicFeet : '10' }, { id : 1794, count : 1, oldCount : 0, title : "Painting 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Painting 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Painting-Large.jpg?itok=tFjZD8e8' }, cubicFeet : '10' } ];

					// when
					result = service.getSelectedInventory(request, contract);

					//then
					expect(expectedResult).toEqual(result);
				});

				it('should get only submitted inventory additional inventory not subbmitted', function () {
					// given
					request.service_type.raw = 6;
					contract.addInventoryMoving.isSubmitted = false;
					var result;
					var expectedResult = [ { id : 1796, count : 1, oldCount : 0, title : 'Clothing Rack', fid : 1, rid : '15', cf : '15', photo : { name : 'Clothing Rack', fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Closing-Rack.jpg?itok=r8Nq0eND' }, cubicFeet : '15' }, { id : 1792, count : 1, oldCount : 0, title : "Print, Photo 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Print, Photo 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Photo-large_0.jpg?itok=81KI9lFs' }, cubicFeet : '10' }, { id : 1794, count : 1, oldCount : 0, title : "Painting 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Painting 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Painting-Large.jpg?itok=tFjZD8e8' }, cubicFeet : '10' } ];

					// when
					result = service.getSelectedInventory(request, contract);

					//then
					expect(expectedResult).toEqual(result);
				})
			})
		});
	});

	describe('getNewStorageInventory', function () {
		it('moving&storage should get full inventory', function () {
			// given
			request.service_type.raw = 6;
			contract.addInventoryMoving.isSubmitted = false;
			var result;
			var expectedResult = [ { id : 1796, count : 1, oldCount : 0, title : 'Clothing Rack', fid : 1, rid : '15', cf : '15', photo : { name : 'Clothing Rack', fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Closing-Rack.jpg?itok=r8Nq0eND' }, cubicFeet : '15' }, { id : 1792, count : 1, oldCount : 0, title : "Print, Photo 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Print, Photo 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Photo-large_0.jpg?itok=81KI9lFs' }, cubicFeet : '10' }, { id : 1794, count : 1, oldCount : 0, title : "Painting 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Painting 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Painting-Large.jpg?itok=tFjZD8e8' }, cubicFeet : '10' }, { id : 1792, count : 1, oldCount : 0, title : "Print, Photo 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Print, Photo 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Photo-large_0.jpg?itok=81KI9lFs' }, cubicFeet : '10' }, { id : 1793, count : 1, oldCount : 0, title : 'Fan, Floor', fid : 1, rid : '15', cf : '12', photo : { name : 'Fan, Floor', fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Fan-Standing.jpg?itok=5qkifsfX' }, cubicFeet : '12' }, { id : 1794, count : 1, oldCount : 0, title : "Painting 4'x6'", fid : 1, rid : '15', cf : '10', photo : { name : "Painting 4'x6'", fid : '17', path : 'http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/Painting-Large.jpg?itok=tFjZD8e8' }, cubicFeet : '10' } ];

			// when
			result = service.getNewStorageInventory(request, contract);

			//then
			expect(expectedResult).toEqual(result);
		})
	})
});
