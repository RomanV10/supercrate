'use strict';

angular
	.module('newInventories')
	.factory('newInventoriesServices', newInventoriesServices);

function newInventoriesServices() {
	let services = {};

	services.getSelectedInventory = getSelectedInventory;
	services.getNewStorageInventory = getNewStorageInventory;

	function getSelectedInventory(request, contract) {
		let inventory = [];

		if (_.get(request, 'request_all_data.inventory') && _.get(contract, 'inventoryMoving.isSubmitted')) {
			request.request_all_data.inventory
				.forEach(function (item) {
					let copyItem = angular.copy(item);

					updateCountSelectedItem(copyItem, contract.inventoryMoving.inventory);

					if (copyItem.count) {
						inventory.push(copyItem);
					}
				});
		} else {
			inventory = inventory.concat(angular.copy(_.get(request, 'request_all_data.inventory', [])))
		}

		if (_.get(request, 'inventory.inventory_list') && _.get(contract, 'inventoryMoving.isSubmitted')) {
			request.inventory.inventory_list
				.forEach(function (item) {
					let copyItem = angular.copy(item);

					updateCountSelectedItem(copyItem, contract.inventoryMoving.inventory);

					if (copyItem.count) {
						inventory.push(copyItem);
					}
				});
		} else {
			inventory = inventory.concat(angular.copy(_.get(request, 'inventory.inventory_list', [])))
		}

		if (request.service_type.raw == 6 || request.service_type.raw == 2) {
			let additionalInventoryMoving = getAdditionalInventoryMoving(request, contract);
			inventory = inventory.concat(additionalInventoryMoving);

			let secondAdditionalInventory = getSecondAdditionalInventory(request, contract);
			inventory = inventory.concat(secondAdditionalInventory);
		}

		return inventory;
	}

	function getSecondAdditionalInventory(request, contract) {
		let inventory = [];

		if (_.get(request, 'request_all_data.second_additional_inventory') && _.get(contract, 'secondAddInventoryMoving.isSubmitted')) {
			request.request_all_data.second_additional_inventory
				.forEach(function (item) {
					let copyItem = angular.copy(item);

					updateCountSelectedItem(copyItem, contract.secondAddInventoryMoving.inventory);

					if (copyItem.count) {
						inventory.push(copyItem);
					}
				});
		}

		return inventory;
	}

	function getAdditionalInventoryMoving(request, contract) {
		let inventory = [];

		if (_.get(request, 'request_all_data.additional_inventory') && _.get(contract, 'addInventoryMoving.isSubmitted')) {
			request.request_all_data.additional_inventory
				.forEach(function (item) {
					let copyItem = angular.copy(item);

					updateCountSelectedItem(copyItem, contract.addInventoryMoving.inventory);

					if (copyItem.count) {
						inventory.push(copyItem);
					}
				});
		}

		return inventory;
	}

	function getNewStorageInventory(request, contract) {
		let inventory = getSelectedInventory(request, contract);

		if (_.get(request, 'request_all_data.additional_inventory') && !_.get(contract, 'addInventoryMoving.isSubmitted')) {
			inventory = inventory.concat(request.request_all_data.additional_inventory);
		}

		if (_.get(request, 'request_all_data.second_additional_inventory') && !_.get(contract, 'secondAddInventoryMoving.isSubmitted')) {
			inventory = inventory.concat(request.request_all_data.second_additional_inventory);
		}

		return inventory;
	}

	function updateCountSelectedItem(item, contractInventory) {
		let count = 0;
		angular.forEach(contractInventory, function (inventoryItem) {
			if (_.get(inventoryItem, 'article') == item.id) {
				count++;
			}
		});
		item.count = count;
	}

	return services;
}
