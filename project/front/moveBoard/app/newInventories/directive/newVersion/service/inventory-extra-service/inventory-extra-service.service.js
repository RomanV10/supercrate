angular
	.module('newInventories')
	.factory('inventoryExtraService', inventoryExtraService);

/* @ngInject */
function inventoryExtraService(apiService, moveBoardApi, additionalServicesFactory) {
	const DECREASED_INVENTORY_ITEM_ACTIVITY = 'decreased';
	const INCREASED_INVENTORY_ITEM_ACTIVITY = 'increased';
	const REMOVED_INVENTORY_ITEM_ACTIVITY = 'removed';
	const ADDED_INVENTORY_ITEM_ACTIVITY = 'added';

	let services = {
		isChangeInventoryExtraService,
		getRequestContract,
		makeExtraServiceLog,
	};

	return services;

	function isChangeInventoryExtraService(newExtraServices = [], oldExtraServices = []) {
		let isChangeExtraServicesLength = newExtraServices.length !== oldExtraServices.length;
		let isChangeItemCount = false;

		newExtraServices.forEach((newExtraServiceItem) => {
			let oldExtraServiceItem = _.find(oldExtraServices, (extraService) => newExtraServiceItem.inventory_item_id === extraService.inventory_item_id);

			if (newExtraServiceItem.inventory_item_id && _isChangeExtraService(newExtraServiceItem, oldExtraServiceItem)) {
				isChangeItemCount = true;
			}
		});

		return isChangeExtraServicesLength || isChangeItemCount;
	}

	function _isChangeExtraService(newExtraService, oldExtraService) {
		let newPrice = _getExtraServicePrice(newExtraService);
		let oldPrice = _getExtraServicePrice(oldExtraService);

		return newPrice !== oldPrice;
	}

	function getRequestContract(id) {
		return apiService.postData(`${moveBoardApi.requests.getRequestContract}${id}`);
	}

	function makeExtraServiceLog(newExtraServices = [], oldExtraServices = []) {
		let result = [];
		let extraServices = _mergeExtraServices(newExtraServices, oldExtraServices);

		_.forEach(extraServices, function (item) {
			let activity = _getItemActivity(item);

			if (!_.isEmpty(activity)) {
				let msg = {
					text: 'Additional Service ' + item.name + ' was ' + activity + '.',
				};

				if (item.old) {
					msg.from = '$' + item.old;
				}

				if (item.old || item.new && !item.old) {
					msg.to = '$' + item.new;
				}

				result.push(msg);
			}
		});

		if (!_.isEmpty(result)) {
			let total = additionalServicesFactory.getExtraServiceTotal(newExtraServices);

			result.unshift({
				simpleText: `Additional Services was changed on inventory tab. Total Services: $${total}`,
			});
		}

		return result;
	}

	function _mergeExtraServices(newExtraService, oldExtraService) {
		let result = [];

		newExtraService.forEach((newItem) => {
			let oldItem = _.find(oldExtraService, (extraService) => newItem.inventory_item_id === extraService.inventory_item_id);

			if (newItem.inventory_item_id) {
				result.push(_makeDifferentItemInfo(newItem, oldItem));
			}
		});

		oldExtraService.forEach((oldItem) => {
			let newItem = _.find(newExtraService, (extraService) => oldItem.inventory_item_id === extraService.inventory_item_id);

			if (!newItem && oldItem.inventory_item_id) {
				result.push(_makeDifferentItemInfo(newItem, oldItem));
			}
		});

		return result;
	}

	function _makeDifferentItemInfo(newItem, oldItem) {
		let newPrice = _getExtraServicePrice(newItem);
		let oldPrice = _getExtraServicePrice(oldItem);
		let name = _.get(newItem, 'name');
		name = _.get(oldItem, 'name', name);

		let result = {
			'old': oldPrice,
			'new': newPrice,
			name,
		};

		return result;
	}

	function _getExtraServicePrice(extraService) {
		let cost = +_.get(extraService, 'extra_services[0].services_default_value', 0);
		let count = +_.get(extraService, 'extra_services[1].services_default_value', 0);
		let price = cost * count;

		return price;
	}

	function _getItemActivity(item) {
		let result = '';

		if (_checkDecreasedItem(item)) {
			result = DECREASED_INVENTORY_ITEM_ACTIVITY;
		} else if (_checkIncreasedItem(item)) {
			result = INCREASED_INVENTORY_ITEM_ACTIVITY;
		} else if (_checkRemovedItem(item)) {
			result = REMOVED_INVENTORY_ITEM_ACTIVITY;
		} else if (_checkAddedItem(item)) {
			result = ADDED_INVENTORY_ITEM_ACTIVITY;
		}

		return result;
	}

	function _checkDecreasedItem(item) {
		return item.new < item.old && item.new !== 0;
	}

	function _checkIncreasedItem(item) {
		return item.new > item.old && item.new !== 0 && item.old !== 0;
	}

	function _checkRemovedItem(item) {
		return item.new === 0 && item.old > 0;
	}

	function _checkAddedItem(item) {
		return (item.old === 0 || _.isUndefined(item.old)) && item.new > 0;
	}
}
