describe('Unit: Inventory directive,', function () {
	function isEmptyObj(obj) {
		return obj && Object.keys(obj).length === 0;
	}
	
	let customRoom;
	let newCustomRoom;
	let $scope;
	let $roomScope;
	let element;
	let $inventoryScope;
	let SweetAlert;
	let whenDragItem;
	let whenGetRooms;
	let whenGetItems;
	let roomElement;
	let whenGetMyInventory;
	let moveInventoryRoom;
	let getInventoryRequest;
	let getItems;
	let request;
	let RequestServices;
	let additionalServicesFactory;
	let Session;
	let savedInventories;
	let inventoryExtraService;
	let contractResponse = {};

	beforeEach(inject(function (_SweetAlert_, _RequestServices_, _additionalServicesFactory_, _Session_, _inventoryExtraService_) {
		SweetAlert = _SweetAlert_;
		RequestServices = _RequestServices_;
		additionalServicesFactory = _additionalServicesFactory_;
		Session = _Session_;
		inventoryExtraService = _inventoryExtraService_;

		customRoom = testHelper.loadJsonFile('customRoom.mock');
		newCustomRoom = testHelper.loadJsonFile('newCustomRoom.mock');
		getFlags = testHelper.loadJsonFile('inventory-get-flags.mock');
		moveInventoryRoom = testHelper.loadJsonFile('move-inventory-room.mock');
		getInventoryRequest = testHelper.loadJsonFile('get-inventory-request.mock');
		getItems = testHelper.loadJsonFile('get-filter-items.mock');
		request = testHelper.loadJsonFile('request.mock');

		spyOn(additionalServicesFactory, 'saveExtraServices').and.callFake(() => {});
		spyOn(inventoryExtraService, 'getRequestContract').and.returnValue(contractResponse);

		$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.request.checkAdditionalInventory)).respond(200, [false]);
		whenGetRooms = $httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.room.rooms));
		whenGetRooms.respond(200, moveInventoryRoom);
		whenGetMyInventory = $httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.item.getMyInventory)).respond(200, getInventoryRequest);
		whenGetItems = $httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.item.getItems));
		whenGetItems.respond(200, getItems);

		$rootScope.currentUser.userRole = ["authenticated user", "administrator"];

		$scope = $rootScope.$new();

		$scope.request = request;
		$scope.listInventories = [];
		$scope.saveListInventories = (trash, newInventory, request, oldInventory) => {
			savedInventories = {};
			savedInventories.newInventory = newInventory;
			savedInventories.oldInventory = oldInventory;
		};
		$scope.isContractPage = false;
		$scope.typeInventory = false;

		makeInventoryElement();
		$timeout.flush();
	}));

	function makeInventoryElement() {
		element = $compile("<div inventories request='request' save-List-Inventories='saveListInventories' listinventories='listInventories' is-contract-page='isContractPage' type-inventory='{{typeInventory}}'></div>")($scope);
		$inventoryScope = element.isolateScope();
	}

	describe('when init,', () => {
		describe('and account,', () => {
			describe('and not admin,', () => {
				it('should be init angular.element', () => {
					//given
					spyOn(angular, 'element').and.callThrough();
					spyOn(Session, 'get').and.returnValue({userRole: ['authenticated user']});
					$scope.$apply();

					//when
					makeInventoryElement();

					//then
					expect(angular.element).toHaveBeenCalledWith('.inventory_modal .modal-content');
				});
			});
		});
	});
	
	describe('Should go to request', () => {
		it('when click Save Inventory Button', () => {
			let spy = spyOn($inventoryScope, 'closingFunction').and.callThrough();
			spyOn(RequestServices, 'sendLogs').and.callFake(() => {});
			$inventoryScope.close();
			$timeout.flush();
			expect(spy).toHaveBeenCalled();
		});
	});
	
	describe("activeRequests counter,", () => {
		it("Should add promise when call addCustomItem()", () => {
			let form = {
				$valid: true,
				$setUntouched: () => {
				},
				$setPristine: () => {
				}
			};
			$inventoryScope.newItem = {
				count: {value: 12},
				pounds: {value: 2},
				title: {value: "name"}
			};
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.item.create))
				.respond(200, {
					status_code: 200,
					data: {id: 100, item_id: 333}
				});
			$inventoryScope.addCustomItem(form);
			expect($inventoryScope.activeRequests.length).toEqual(1);
			$httpBackend.flush();
		});
	});
	
	describe('function: search,', () => {
		describe('when item found', () => {
			let searchResponse;

			beforeEach(() => {
				searchResponse = testHelper.loadJsonFile('inventory-search-inventory-item-responce.mock');
				$inventoryScope.searchText = "searchText";
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.request.search)).respond(200, searchResponse);
			});

			describe("When My inventory opened", () => {
				beforeEach(() => {
					$inventoryScope.myInventory.opened = true;
					$inventoryScope.search();
					$timeout.flush();
					$httpBackend.flush();
				});

				it("Should save item's original room_id", () => {
					expect($inventoryScope.inventoryItems[7].room_id).toEqual('15');
				});

				it("Should save item's original count", () => {
					expect($inventoryScope.inventoryItems[6].count).toEqual(2);
				});

				it("Should save item's original filter_id", () => {
					expect($inventoryScope.inventoryItems[7].filter_id).toEqual('15');
				});

			});

			describe("When room opened", () => {
				let targetRoom, targetFilter, fakeFilter;

				beforeEach(() => {
					targetRoom = $inventoryScope.rooms[0];
					targetFilter = targetRoom.filters[3];
					fakeFilter = targetRoom.filters[4];
					$inventoryScope.myInventory.opened = false;
					$inventoryScope.activeRoom = targetRoom;
					$inventoryScope.activeFilter = targetFilter;
					$inventoryScope.search();
					$timeout.flush();
					$httpBackend.flush();
				});

				describe("When item exist in room,", () => {
					beforeEach(() => {
						$inventoryScope.changeItemCount($inventoryScope.inventoryItems[1], 6);
					});

					it ("Should set item.moved to Falsy", () => {
						expect($inventoryScope.inventoryItems[1].moved).toBeFalsy();
					});

					it("Should be in his filter after adding", () => {
						expect(targetFilter.items[0].id).toEqual('77');
						expect(targetFilter.items[0].count).toEqual(6);
						expect(targetFilter.items.length).toEqual(1);
					});

					it("Should increase total_count in his filter after adding", () => {
						expect(targetFilter.total_count).toEqual(6);
					});
				});

				describe("When item NOT exist in room,", () => {
					beforeEach(() => {
						$inventoryScope.changeItemCount($inventoryScope.inventoryItems[5], 7);
					});

					it ("Should set item's room_id to current room", () => {
						expect($inventoryScope.inventoryItems[7].room_id).toEqual('15');
					});

					it("Should set item's count to 0", () => {
						expect($inventoryScope.inventoryItems[7].count).toEqual(0);
					});

					it("Should set item's filter_id to -1", () => {
						expect($inventoryScope.inventoryItems[7].filter_id).toEqual("-1");
					});

					it("Should set item.moved to 1", () => {
						expect($inventoryScope.inventoryItems[7].moved).toEqual(1);
					});

					it("Should increase total_count in -1 filter after adding", () => {
						expect(fakeFilter.total_count).toEqual(7);
					});
				});
			});
		});

		describe('when item not found', () => {
			beforeEach(() => {
				spyOn($inventoryScope, 'openRoom');
				spyOn($inventoryScope, 'setCustomItemMode').and.callThrough();
				let searchResponse = {"status_code": 200, "status_message": "OK", "data": []};
				$inventoryScope.searchText = "search text";
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.request.search)).respond(200, searchResponse);
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.request.getCustomItems)).respond(200, searchResponse);
			});

			describe('When opened my inventory', () => {
				beforeEach(() => {
					$inventoryScope.myInventory.opened = true;
					$inventoryScope.search();
					$timeout.flush();
					$httpBackend.flush();
				});

				it('should open room', () => {
					expect($inventoryScope.openRoom).toHaveBeenCalled();
				});

				it('should set custom item mode', () => {
					expect($inventoryScope.setCustomItemMode).toHaveBeenCalled();
				});

				it('should set new item title value', () => {
					//given
					let expectedData = 'search text';

					//when

					//then
					expect($inventoryScope.newItem.title.value).toEqual(expectedData);
				});
			});
		});
	});
	
	describe('function: dragulaDropFunction, ', () => {
		let $tableScope, destination;
		let droppedItem, originalItem;
		let firstRoom, secondRoom;
		
		function dragAndDropItem(item, destinationRoom) {
			let indexInOldRoom = _.findIndex(item.room.items, {id: item.id});
			item.room.items.splice(indexInOldRoom, 0);
			destinationRoom.items.push(item);
		}
		
		beforeEach(() => {
			let element = angular.element('<div inventory-table="inventory-table" table-Arguments="tableArguments" table-Permissions="tablePermissions" column-width="3" change-item-count="changeItemCount(item, value)" update-After-Drop="updateAfterDrop()" class="container inventory__table-new"></div>');
			element = $compile(element)($inventoryScope);
			$tableScope = element.isolateScope();
			
			$inventoryScope.myInventory.opened = false;
			
			whenDragItem = $httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.item.moveItem));
			
			destination = {
				attr: (a) => {
					return "5"
				}
			};
			
			firstRoom = $tableScope.roomsHolder.rooms[0];
			secondRoom = $tableScope.roomsHolder.rooms[1];
			originalItem = firstRoom.filters[0].items[0];
			$tableScope.selectedItem = originalItem;
			
		});
		
		describe('if backend allow, ', () => {
			beforeEach(() => {
				whenDragItem.respond(200, {status_code: 200});
			});
			describe('when moved from filter,', () => {
				beforeEach(() => {
					secondRoom.items = [];
					
					dragAndDropItem($tableScope.selectedItem, secondRoom);
					$tableScope.dragulaDropFunction('element', 'target', destination, 'source');
					$timeout.flush();
					
					droppedItem = secondRoom.items[0];
				});
				
				it('must write to item item.moved = truthy when dropped', () => {
					expect(droppedItem.moved).toBeTruthy();
				});
				
				it("must copy item's id", () => {
					expect(droppedItem.id).toEqual('70');
				});
				
				it("must copy item's name", () => {
					expect(droppedItem.name).toEqual('XL Tote 54 Gal 42.5x21.5x18');
				});
				
				it("must copy item's count", () => {
					expect(droppedItem.count).toEqual(1);
				});
				
				it("must copy item's total_cf and count", () => {
					expect(droppedItem.total_cf).toEqual(10);
				});
				
				it("must break link between item in destination Room and item in filter", () => {
					expect(droppedItem).not.toBe(originalItem);
				});
				
				it("must write link to new room in item", () => {
					expect(droppedItem.room).toBe(secondRoom);
				});
				
				it("must delete count in original item in filter", () => {
					expect(originalItem.count).toEqual(0);
				});
				
				it("must delete total_cf in original item in filter", () => {
					expect(originalItem.total_cf).toEqual(0);
				});
				
				it("must decrease source filter total_count", () => {
					expect(firstRoom.filters[0].total_count).toEqual(9);
				});
				
				it("must decrease source filter total_cf", () => {
					expect(firstRoom.filters[0].total_cf).toEqual(151);
				});
				
				it("must decrease source room total_count", () => {
					expect(firstRoom.total_count).toEqual(9);
				});
				
				it("must decrease source room total_cf", () => {
					expect(firstRoom.total_cf).toEqual(151);
				});
				
				it("must increase destination room total_count", () => {
					expect(secondRoom.total_count).toEqual(11);
				});
				
				it("must increase destination room total_cf", () => {
					expect(secondRoom.total_cf).toEqual(98);
				});
				
				it("must increase destination filter total_count", () => {
					expect(secondRoom.filters[0].total_count).toEqual(6);
				});
				
				it("must increase destination filter total_cf", () => {
					expect(secondRoom.filters[0].total_cf).toEqual(88);
				});
				
				it("must NOT change destination room boxes", () => {
					expect(secondRoom.boxes).toEqual(0);
				});
				
				it("must NOT change source room boxes", () => {
					expect(firstRoom.boxes).toEqual(0);
				});
				
				it("must NOT change source room custom_count", () => {
					expect(firstRoom.total_custom_count).toEqual(0);
				});
				
				it("must NOT change source room custom_cf", () => {
					expect(firstRoom.total_custom_cf).toEqual(0);
				});
				
				it("must NOT change destination room custom_count", () => {
					expect(secondRoom.total_custom_count).toEqual(0);
				});
				
				it("must NOT change destination room custom_cf", () => {
					expect(secondRoom.total_custom_cf).toEqual(0);
				});
				
				it("must NOT touch global count", () => {
					expect($inventoryScope.total.count).toEqual(29);
				});
				
				it("must NOT touch global total_cf", () => {
					expect($inventoryScope.total.total_cf).toEqual(335);
				});
			});
			
			describe('when moved from fake filter, ', () => {
				describe('if item type = 0, ', () => {
					beforeEach(() => {
						destination = {
							attr: (a) => {
								return "15"
							}
						};
						
						$tableScope.selectedItem = secondRoom.items[3];
						
						dragAndDropItem($tableScope.selectedItem, firstRoom);
						$tableScope.dragulaDropFunction('element', 'target', destination, 'source');
						$timeout.flush();
						droppedItem = firstRoom.items[4];
					});
					
					it('must write to item item.moved = truthy when dropped', () => {
						expect(droppedItem.moved).toBeTruthy();
					});
					
					it("must copy item's id", () => {
						expect(droppedItem.id).toEqual('77');
					});
					
					it("must copy item's item_id", () => {
						expect(droppedItem.item_id).toEqual('151');
					});
					
					it("must copy item's count", () => {
						expect(droppedItem.count).toEqual(1);
					});
					
					it("must copy item's total_cf", () => {
						expect(droppedItem.total_cf).toEqual(8);
					});
					
					it("must write link to new room in item", () => {
						expect(droppedItem.room).toBe(firstRoom);
					});
					
					it("must decrease source room total_count", () => {
						expect(firstRoom.total_count).toEqual(11);
					});
					
					it("must decrease source room total_cf", () => {
						expect(firstRoom.total_cf).toEqual(169);
					});
					
					it("must increase destination room total_count", () => {
						expect(secondRoom.total_count).toEqual(9);
					});
					
					it("must increase destination room total_cf", () => {
						expect(secondRoom.total_cf).toEqual(80);
					});
					
					it("must NOT change destination room boxes", () => {
						expect(secondRoom.boxes).toEqual(0);
					});
					
					it("must NOT change source room boxes", () => {
						expect(firstRoom.boxes).toEqual(0);
					});
					
					it("must NOT change source room custom_count", () => {
						expect(firstRoom.total_custom_count).toEqual(0);
					});
					
					it("must NOT change source room custom_cf", () => {
						expect(firstRoom.total_custom_cf).toEqual(0);
					});
					
					it("must NOT change destination room custom_count", () => {
						expect(secondRoom.total_custom_count).toEqual(0);
					});
					
					it("must NOT change destination room custom_cf", () => {
						expect(secondRoom.total_custom_cf).toEqual(0);
					});
					
					it("must NOT touch global count", () => {
						expect($inventoryScope.total.count).toEqual(29);
					});
					
					it("must NOT touch global total_cf", () => {
						expect($inventoryScope.total.total_cf).toEqual(335);
					});
				});
				
				describe('if item type = 1(custom), ', () => {
					describe('if my inventory opened,', () => {
						beforeEach(() => {
							let item = firstRoom.items[0];
							item.type = "1";
							delete item.filter;
							item.filter_id = undefined;
							firstRoom.custom_items.push(item);
							
							$tableScope.selectedItem = item;
							
							dragAndDropItem($tableScope.selectedItem, secondRoom);
							$tableScope.dragulaDropFunction('element', 'target', destination, 'source');
							$timeout.flush();
							droppedItem = secondRoom.items[4];
						});
						
						it('must write to item item.moved = truthy when dropped', () => {
							expect(droppedItem.moved).toBeTruthy();
						});
						
						it("must copy item's id", () => {
							expect(droppedItem.id).toEqual('70');
						});
						
						it("must copy item's item_id", () => {
							expect(droppedItem.item_id).toEqual('304');
						});
						
						it("must copy item's count", () => {
							expect(droppedItem.count).toEqual(1);
						});
						
						it("must copy item's total_cf", () => {
							expect(droppedItem.total_cf).toEqual(10);
						});
						
						it("must copy count", () => {
							expect(droppedItem.count).toEqual(1);
						});
						
						it('must remove original item from custom_items', () => {
							expect(firstRoom.custom_items.length).toBeFalsy();
						});
						
						it("must write link to new room in item", () => {
							expect(droppedItem.room).toBe(secondRoom);
						});
						
						it("must NOT decrease source room total_count", () => {
							expect(firstRoom.total_count).toEqual(10);
						});
						
						it("must NOT decrease source room total_cf", () => {
							expect(firstRoom.total_cf).toEqual(161);
						});
						
						it("must NOT increase destination room total_count", () => {
							expect(secondRoom.total_count).toEqual(10);
						});
						
						it("must NOT increase destination room total_cf", () => {
							expect(secondRoom.total_cf).toEqual(88);
						});
						
						it("must NOT increase destination room boxes", () => {
							expect(secondRoom.boxes).toEqual(0);
						});
						
						it("must NOT decrease source room boxes", () => {
							expect(firstRoom.boxes).toEqual(0);
						});
						
						it("must decrease source room custom_count", () => {
							expect(firstRoom.total_custom_count).toEqual(-1);
						});
						
						it("must decrease source room custom_cf", () => {
							expect(firstRoom.total_custom_cf).toEqual(-10);
						});
						
						it("must increase destination room custom_count", () => {
							expect(secondRoom.total_custom_count).toEqual(1);
						});
						
						it("must increase destination room custom_cf", () => {
							expect(secondRoom.total_custom_cf).toEqual(10);
						});
						
						
						it("must NOT touch global count", () => {
							expect($inventoryScope.total.count).toEqual(29);
						});
						
						it("must NOT touch global total_cf", () => {
							expect($inventoryScope.total.total_cf).toEqual(335);
						});
					});
					
					describe("if custom mode opened,", () => {
						beforeEach(() => {
							$inventoryScope.customMode = true;
							$inventoryScope.activeRoom = firstRoom;
							
							$inventoryScope.activeRoom.items[0].type = "1";
							delete $inventoryScope.activeRoom.items[0].filter;
							$inventoryScope.activeRoom.items[0].filter_id = undefined;
							$inventoryScope.activeRoom.custom_items = [$inventoryScope.activeRoom.items[0]];
							$inventoryScope.inventoryItems = $inventoryScope.activeRoom.custom_items;
							
							$tableScope.selectedItem = $inventoryScope.activeRoom.items[0];
							
							dragAndDropItem($tableScope.selectedItem, secondRoom);
							$tableScope.dragulaDropFunction('element', 'target', destination, 'source');
							$timeout.flush();
						});
						
						it('must remove original item from inventoryItems (visible items)', () => {
							expect($inventoryScope.inventoryItems.length).toBeFalsy();
						});
					});
					
				});
				
			});
			
		});
		
		describe('if backend NOT allow, ', () => {
			beforeEach(() => {
				whenDragItem.respond(200, {status_code: 404});
			});
			describe('when moved from filter, ', () => {
				beforeEach(() => {
					secondRoom.items = [];
					
					dragAndDropItem($tableScope.selectedItem, secondRoom);
					$tableScope.dragulaDropFunction('element', 'target', destination, 'source');
					$timeout.flush();
				});
				
				it('must write to item item.moved = 0 when dropped', () => {
					expect(originalItem.moved).toBeFalsy();
				});
				
				it("must NOT copy item to destinationRoom.items", () => {
					expect(isEmptyObj(secondRoom.items)).toBeTruthy();
				});
				
				it("must NOT change object link", () => {
					expect(originalItem).toBe($tableScope.selectedItem);
				});
				
				it("must NOT change item's id", () => {
					expect(originalItem.id).toEqual('70');
				});
				
				it("must NOT change item's item_id", () => {
					expect(originalItem.item_id).toEqual('304');
				});
				
				it("must NOT change item's count", () => {
					expect(originalItem.count).toEqual(1);
				});
				
				it("must NOT change item's total_cf", () => {
					expect(originalItem.total_cf).toEqual(10);
				});
				
				it("must NOT decrease source filter total_count", () => {
					expect(firstRoom.filters[0].total_count).toEqual(10);
				});
				
				it("must NOT decrease source filter total_cf", () => {
					expect(firstRoom.filters[0].total_cf).toEqual(161);
				});
				
				it("must NOT decrease source room total_count", () => {
					expect(firstRoom.total_count).toEqual(10);
				});
				
				it("must NOT decrease source room total_cf", () => {
					expect(firstRoom.total_cf).toEqual(161);
				});
				
				it("must NOT increase destination room total_count", () => {
					expect(secondRoom.total_count).toEqual(10);
				});
				
				it("must NOT increase destination room total_cf", () => {
					expect(secondRoom.total_cf).toEqual(88);
				});
				
				it("must NOT touch global count", () => {
					expect($inventoryScope.total.count).toEqual(29);
				});
				
				it("must NOT touch global total_cf", () => {
					expect($inventoryScope.total.total_cf).toEqual(335);
				});
				
			});
			
		});
	});
	
	describe('When open inventory, ', () => {
		
		describe("Filter for moved items", () => {
			it("Should be in every rooms", () => {
				_.forEach($inventoryScope.rooms, room => {
					expect(_.findIndex(room.filters, {filter_id: "-1"})).toBeGreaterThan(-1);
				});
			});
			
			it("Should contains correct total_count", () => {
				expect($inventoryScope.rooms[1].filters[4].total_count).toEqual(4);
				expect($inventoryScope.rooms[0].filters[4].total_count).toEqual(0);
			});
		});
		
		describe('If inventory room not empty,', () => {
			beforeEach(() => {
				let getItemsEmpty = testHelper.loadJsonFile('get-inventory-items-empty.mock');
				let moveInventoryRoomEmpty = testHelper.loadJsonFile('move-inventory-room-empty.mock');
				
				whenGetRooms.respond(200, moveInventoryRoomEmpty);
				whenGetItems.respond(200, getItemsEmpty);
				
				makeInventoryElement();
				$timeout.flush();
				$scope.$apply();
			});
			
			
			it('Should write selected room to activeRoom when openRoom()', () => {
				expect($inventoryScope.activeRoom).toBe($inventoryScope.rooms[0]);
			});
			
			it('Should set active.Room.opened to true when openRoom()', () => {
				expect($inventoryScope.activeRoom.opened).toBeTruthy();
			});
			
			it('Should write room items to inventory items in selected rooms when openRoom()', () => {
				expect($inventoryScope.inventoryItems).toBe($inventoryScope.activeRoom.items);
			});
			
			it('Should set myInventory.opened to false when openRoom()', () => {
				expect($inventoryScope.myInventory.opened).toBeFalsy();
			});
			
			it('It must call openRoom', () => {
				makeInventoryElement();
				let spy = spyOn($inventoryScope, 'openRoom');
				$timeout.flush();
				$scope.$apply();
				expect($inventoryScope.openRoom).toHaveBeenCalled();
			});
		});
		
		it('If inventory not empty it must call getMyInventory', () => {
			makeInventoryElement();
			let spy = spyOn($inventoryScope, 'openMyInventory');
			$timeout.flush();
			$scope.$apply();
			expect(spy).toHaveBeenCalled();
		});
		
	});
	
	describe("Operations with fake filters for moved items,", () => {
		let targetRoom, targetFilter;
		beforeEach(() => {
			targetRoom = $inventoryScope.rooms[1];
			targetFilter = targetRoom.filters[4];
		});
		it("Should show only moved items when opened", () => {
			$inventoryScope.openRoom(targetRoom);
			$inventoryScope.setFilter(targetFilter);
			expect($inventoryScope.inventoryItems.length).toEqual(1);
			expect($inventoryScope.inventoryItems[0].moved).toBeTruthy();
		});
	});
	
	describe('Operations with rooms: ', () => {
		describe('openRoom(), If inventory room empty,', () => {
			let setFilter;
			beforeEach(() => {
				let getItemsEmpty = testHelper.loadJsonFile('get-inventory-items-empty.mock');
				let moveInventoryRoomEmpty = testHelper.loadJsonFile('move-inventory-room-empty.mock');
				getItemsEmpty.data[0].filters[0].items = [];
				
				whenGetRooms.respond(200, moveInventoryRoomEmpty);
				whenGetItems.respond(200, getItemsEmpty);
				whenGetMyInventory.respond(200, {data: [], "status_code": 200});
				
				makeInventoryElement();
				setFilter = spyOn($inventoryScope, 'setFilter');
				$timeout.flush();
				$scope.$apply();
			});
			
			
			it('Should write selected room to activeRoom when openRoom()', () => {
				expect($inventoryScope.activeRoom).toBe($inventoryScope.rooms[0]);
			});
			
			it('Should set active.Room.opened to true when openRoom()', () => {
				expect($inventoryScope.activeRoom.opened).toBeTruthy();
			});
			
			it('Should write room items to inventory items in selected rooms when openRoom()', () => {
				expect($inventoryScope.inventoryItems).toEqual($inventoryScope.activeRoom.items);
			});
			
			it('Should set myInventory.opened to false when openRoom()', () => {
				expect($inventoryScope.myInventory.opened).toBeFalsy();
			});
			
			it('$scope.setFilter to have been called', () => {
				expect(setFilter).toHaveBeenCalled();
			});
			
			it('It must call openRoom', () => {
				makeInventoryElement();
				let spy = spyOn($inventoryScope, 'openRoom');
				$timeout.flush();
				$scope.$apply();
				expect($inventoryScope.openRoom).toHaveBeenCalled();
			});
		});
		
		describe('openRoom(), ', () => {
			beforeEach(() => {
				$inventoryScope.openRoom($inventoryScope.rooms[4]);
			});
			it('Should write selected room to activeRoom when openRoom()', () => {
				expect($inventoryScope.activeRoom).toBe($inventoryScope.rooms[4]);
			});
			
			it('Should set active.Room.opened to true when openRoom()', () => {
				expect($inventoryScope.activeRoom.opened).toBeTruthy();
			});
			
			it('Should write first filter in selected room to activeFilter when openRoom()', () => {
				expect($inventoryScope.activeFilter).toBe($inventoryScope.activeRoom.filters[0]);
			});
			
			it('Should set myInventory.opened to false when openRoom()', () => {
				expect($inventoryScope.myInventory.opened).toBeFalsy();
			});
			
		});
		describe('openRoom() with conditions', () => {
			it('if filters is empty', () => {
				let room = angular.copy($inventoryScope.rooms[4]);
				delete room.filters;
				let spy = spyOn($inventoryScope, 'setCustomItemMode').and.callThrough();
				$inventoryScope.openRoom(room);
				expect(spy).toHaveBeenCalled();
			});
			it('if filters not empty', () => {
				let room = angular.copy($inventoryScope.rooms[4]);
				let spy = spyOn($inventoryScope, 'setFilter');
				$inventoryScope.openRoom(room);
				expect(spy).toHaveBeenCalled();
			});
		});
		
		describe('getMyInventory,', () => {
			beforeEach(() => {
				$inventoryScope.getMyInventory();
			});
			it('Should remove room from activeRoom when getMyInventory()', () => {
				expect(isEmptyObj($inventoryScope.activeRoom)).toBeTruthy();
			});
			it('Should set myInventory.opened to true when getMyInventory()', () => {
				expect($inventoryScope.myInventory.opened).toBeTruthy();
			});
		});
		
		it('Should write selected filter to activeFilter when setFilter()', function () {
			$inventoryScope.openRoom($inventoryScope.rooms[4]);
			$inventoryScope.setFilter($inventoryScope.activeRoom.filters[1]);
			expect($inventoryScope.activeFilter.name).toEqual('Miscellaneous');
		});
	});
	
	describe('Operations with custom rooms, ', () => {
		let removeCustomRoom;
		beforeEach(() => {
			$inventoryScope.rooms.push(customRoom);
			let len = $inventoryScope.rooms.length - 1;
			
			roomElement = angular.element("<div custom-room-name room='rooms[" + len + "]' delete-custom-room='deleteCustomRoom(room, callback)'></div>");
			roomElement = $compile(roomElement)($inventoryScope);
			
			$roomScope = roomElement.isolateScope();
			removeCustomRoom = $httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.request.deleteCustomRoom, {id: 42}));
		});
		it("Should call inventory's deleteCustomRoom() when click Remove Custom Room", () => {
			removeCustomRoom.respond(200, {
				data: {status_code: 200}
			});
			let spy = spyOn($inventoryScope, 'deleteCustomRoom').and.callThrough();
			$roomScope.removeRoom();
			$timeout.flush();
			expect(spy).toHaveBeenCalled();
		});
		
		describe('function: createCustomRoom, ', () => {
			describe('room type 1 (Custom room),', () => {
				beforeEach(() => {
					spyOn(SweetAlert, "swal").and.callFake((options, callback) => {
						callback(true);
					});
				});
				
				describe('When API allow,', () => {
					let roomsCount;
					beforeEach(() => {
						let expectedData = {
							conditions: {
								type: 1,
								name: "Custom Room 2"
							},
							entity_id: "4366"
						};
						let response = {
							status_code: 200,
							data: angular.copy(newCustomRoom)
						};
						roomsCount = $inventoryScope.rooms.length;
						$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.request.createCustomRoom), expectedData).respond(200, response);
						
						$inventoryScope.createCustomRoom('1');
						$inventoryScope.$apply();
					});
					
					it('Should add new custom room to rooms and notBedrooms if API allow', () => {
						$httpBackend.flush();
						expect($inventoryScope.rooms[roomsCount].name).toEqual('newCustomRoom');
						expect($inventoryScope.notBedrooms[roomsCount - 2].name).toEqual('newCustomRoom');
					});
				});
				
				describe('When API allow,', () => {
					let roomsCount;
					beforeEach(() => {
						
						let expectedData = {
							conditions: {
								type: 1,
								name: "Custom Room 2"
							},
							entity_id: "4366"
						};
						let response = {
							status_code: 500,
							data: "some error"
						};
						roomsCount = $inventoryScope.rooms.length;
						$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.request.createCustomRoom), expectedData).respond(200, response);
						
						$inventoryScope.createCustomRoom('1');
						$inventoryScope.$apply();
					});
					it('Should NOT add new custom room to rooms if API NOT allow', () => {
						expect($inventoryScope.rooms[roomsCount]).toBeUndefined();
					});
				});
			});
		});
		
		//make for removeCustomRoom()
		
	});
	
	describe('operations with item,', function () {
		let item, countBeforeClick, weightBeforeClick;
		beforeEach(() => {
			item = $inventoryScope.rooms["0"].filters["0"].items["0"];
			countBeforeClick = item.count;
			weightBeforeClick = item.total_cf;
		});
		
		describe("When change count manually,", () => {
			beforeEach(() => {
				item.count = 70;
				$inventoryScope.changeItemCount(item, 70, true);
			});
			
			it('Item count should be setted', () => {
				expect(item.count).toEqual(70);
			});
			
			it('Item total_cf should changed', () => {
				expect(item.total_cf).toEqual(700);
			});
			
			it("Room's total_count should changed", () => {
				expect(item.room.total_count).toEqual(79);
			});
			
			it("Filter's total_count should changed", () => {
				expect(item.filter.total_count).toEqual(79);
			});
			
			it("Room's total_cf should changed", () => {
				expect(item.room.total_cf).toEqual(851);
			});
			
			it("Filter's total_cf should changed", () => {
				expect(item.filter.total_cf).toEqual(851);
			});
			
		});
		
		
		describe("When item.isSearch", () => {
			beforeEach(() => {
				item.isSearch = true;
			});
			it('Should not remove item from inventoryItems', () => {
				item.count = 1;
				$inventoryScope.changeItemCount(item, -1);
				expect($inventoryScope.inventoryItems[12]).toBe(item);
			});
		});
		
		it('Add one item to request (check count)', function () {
			$inventoryScope.changeItemCount(item, 1);
			expect(item.count).toEqual(countBeforeClick + 1);
		});
		
		it('Add one item to request (check weight)', function () {
			$inventoryScope.changeItemCount(item, 1);
			expect(item.total_cf).toEqual(weightBeforeClick + item.cf * 1);
		});
		
		it('Remove one item to request (check count)', function () {
			$inventoryScope.changeItemCount(item, -1);
			expect(item.count).toEqual(countBeforeClick - 1);
		});
		
		it('Remove one item to request (check weight)', function () {
			$inventoryScope.changeItemCount(item, -1);
			expect(item.total_cf).toEqual(weightBeforeClick - item.cf * (1));
		});
		
		it('must add item to room.items when click "ADD ITEM"', () => {
			$inventoryScope.rooms[1].filters["0"].items = [];
			$inventoryScope.rooms[1].items = [];

			let item = {
				"id": "1010",
				"item_id": "58",
				"name": "Box",
				"image_link": "http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/box.jpg?itok=QcSUqGi_",
				"price": "0",
				"cf": "20",
				"count": "0",
				"baseTotal": {},
				"room": $inventoryScope.rooms[1],
				"filter": $inventoryScope.rooms[1].filters[0],
				"status": "0",
				"handle_fee": "0",
				"packing_fee": "0",
				"type": "2",
				"extra_service": "0",
				"extra_service_name": "",
				"weight": "0",
				"room_id": "17",
				"filters": [
					"7"
				],
				"total_cf": "0",
				"uri": "public://inventory/box.jpg"
			};

			$inventoryScope.rooms[1].filters["0"].items.push(item);

			$inventoryScope.changeItemCount(item, 1);
			$inventoryScope.afterSavingItemCount({success: true, item});
			expect($inventoryScope.rooms[1].items[0]).toBe(item);
		});
		
		describe('Should add extra service,', () => {
			let item;
			beforeEach(() => {
				item = $inventoryScope.rooms["0"].filters["0"].items["0"];
			});
			
			it('When add two items to request', function () {
				$inventoryScope.changeItemCount(item, 2);
				$inventoryScope.afterSavingItemCount({success: true, item});
				expect($inventoryScope.request.extraServices.pop().extra_services[1].services_default_value).toEqual('1');
			});
		});
	});
	
	describe('operations with custom items,', () => {
		describe('when create item,', () => {
			beforeEach(() => {
				$inventoryScope.rooms[0].items = [];
				$inventoryScope.activeRoom = $inventoryScope.rooms[0];
				let form = {
					$valid: true,
					$setUntouched: () => {
					},
					$setPristine: () => {
					}
				};
				$inventoryScope.newItem = {
					count: {value: 12},
					pounds: {value: 2},
					title: {value: "name"}
				};
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.item.create))
					.respond(200, {
						status_code: 200,
						data: {id: 100, item_id: 333}
					});
				$inventoryScope.addCustomItem(form);
				$httpBackend.flush();
			});
			
			it('should add custom item to room.items', () => {
				expect($inventoryScope.rooms[0].items[0].name).toEqual("name (2 c.f.)");
			});
			
			it('should set busy to true', () => {
				$inventoryScope.close();
				expect($inventoryScope.busy).toBeTruthy();
			});
		});
	});
	
	describe('function: onDestroy', () => {
		let item;
		
		beforeEach(onDestroyBeforEach);
		
		function onDestroyBeforEach() {
			item = $inventoryScope.rooms["0"].filters["0"].items["0"];
			
			$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.request.updateItemInRequest))
				.respond(200, {status_code: 200, data: "71"});
			
			let getInventoryRequestChanged = testHelper.loadJsonFile('get-inventory-request.mock');
			getInventoryRequestChanged.data[0].count = 3;
			getInventoryRequestChanged.data[1].id = "333";
			
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.item.getMyInventory))
				.respond(200, getInventoryRequestChanged);
		}
		
		describe('contract not submited', () => {
			beforeEach(() => {
				contractResponse = {};
				spyOn(RequestServices, 'sendLogs').and.callFake(() => {});

				$inventoryScope.changeItemCount(item, 1);
				
				$inventoryScope.close();
				$timeout.flush();
				$httpBackend.flush();
			});
			
			it('must save new Item with oldCount 0', () => {
				expect(savedInventories.newInventory[1].id).toEqual(333);
				expect(savedInventories.newInventory[1].oldCount).toEqual(0);
				expect(savedInventories.newInventory[1].count).toEqual(2);
			});
			
			it('must save changed Item with new count and oldCount', () => {
				expect(savedInventories.newInventory[0].id).toEqual(70);
				expect(savedInventories.newInventory[0].oldCount).toEqual(0);
				expect(savedInventories.newInventory[0].count).toEqual(3);
			});
		});
		
		describe('additional inventory on contract page', () => {
			let saveListInventoriesSpy;
			let saveReqDataSpy;
			
			beforeEach(() => {
				$scope.isContractPage = true;
				$scope.typeInventory = 'addInventoryMoving';
				
				makeInventoryElement();
				$timeout.flush();

				contractResponse = {factory: {inventoryMoving: {isSubmitted: true}}};
				
				$httpBackend.expectPUT(testHelper.makeRequest('server/move_request/4366'))
					.respond(200, {});
				
				$httpBackend.whenPUT(testHelper.makeRequest('server/move_request/4366'))
					.respond(200, {});
				
				$inventoryScope.changeItemCount(item, 1);
				
				saveListInventoriesSpy = spyOn($scope, 'saveListInventories').and.callThrough();
				saveReqDataSpy = spyOn(RequestServices, 'saveReqData').and.callThrough();
				
				$inventoryScope.close();
				$timeout.flush();
				$httpBackend.flush();
			});
			
			it('should call saveListInventories', () => {
				expect(saveListInventoriesSpy).toHaveBeenCalled();
			});
			
			it('should call saveReqData', () => {
				expect(saveReqDataSpy).toHaveBeenCalled();
			});
		});
	});
	
	describe('extraServices on contract', function () {
		let saveListInventoriesSpy;
		let saveReqDataSpy;
		let item;
		
		beforeEach(() => {
			item = $inventoryScope.rooms["0"].items["0"];
			item.extra_service = '1';
			$inventoryScope.rooms["2"].items = [angular.copy(item)];
			$scope.isContractPage = true;
			$scope.typeInventory = 'addInventoryMoving';
			$scope.request.status.raw = 3;
			$scope.$apply();

			contractResponse = {factory: {inventoryMoving: {isSubmitted: true}}};
			
			$inventoryScope.changeItemCount(item, 1);
			$inventoryScope.afterSavingItemCount({success: true, item});
			
			saveListInventoriesSpy = spyOn($scope, 'saveListInventories').and.callThrough();
			saveReqDataSpy = spyOn(RequestServices, 'saveReqData').and.callThrough();
			
			$inventoryScope.close();
			$timeout.flush();
		});
		
		it('should call saveListInventories', () => {
			expect(saveListInventoriesSpy).toHaveBeenCalled();
		});
		
		it('should add extra service to invoice', function () {
			expect($scope.request.request_all_data.invoice.extraServices[2].name).toEqual('XL Tote 54 Gal 42.5x21.5x18');
		});
		it('should summing all services from all rooms', function () {
			expect($scope.request.request_all_data.invoice.extraServices[2].extra_services[1].services_default_value).toEqual(3);
		});
	});
});
