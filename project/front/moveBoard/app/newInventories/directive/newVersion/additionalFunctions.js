export function mainTotalCalc(rooms) {
	return {
		total_cf: calculateRoomsTotalCF(rooms) + getTotalCustomCf(rooms),
		count: calculateRoomsCount(rooms) + getTotalCustomCount(rooms),
		total_custom: getTotalCustomCount(rooms),
		boxes: getTotalBoxes(rooms)
	};
}

function calculateRoomsTotalCF(rooms) {
	return rooms
		.filter(room => room.total_cf)
		.map(room => Number(room.total_cf))
		.reduce((total_cf, sum) => total_cf + sum, 0);
}

function calculateRoomsCount(rooms) {
	return rooms
		.filter(room => room.total_count)
		.map(room => Number(room.total_count))
		.reduce((total_count, sum) => total_count + sum, 0);
}

function getTotalBoxes(rooms) {
	return rooms
		.filter(room => room.boxes)
		.map(room => Number(room.boxes))
		.reduce((boxes, sum) => boxes + sum, 0);
}

export function createCustomItems(rooms) {
	rooms
		.filter(room => !room.custom_items)
		.forEach(room => room.custom_items = []);
}

export function getTotalCustomCount(rooms) {
	return rooms
		.filter(item => item.total_custom_count)
		.map(item => Number(item.total_custom_count))
		.reduce((a, b) => a + b, 0);
}

export function getTotalCustomCf(rooms) {
	return rooms
		.filter(item => item.total_custom_cf)
		.map(item => Number(item.total_custom_cf))
		.reduce((a, b) => a + b, 0);
}

export function converInventoryToOldVersion(inventory) {
	var result = [];
	
	inventory.forEach(item => {
		if (!_.isUndefined(item.type)) {
			result.push({
				id: Number(item.id),
				count: Number(item.count),
				oldCount: Number(item.oldCount || 0),
				title: item.name,
				fid: getFidByType(item),
				rid: item.room_id,
				cf: item.cf,
				photo: {
					name: item.name, fid: item.filter_id, path: item.image_link
				},
				cubicFeet: item.cf,
				additional: item.selectedTypeInventory // is item.type_inventory
			});
		}
	});
	
	return result;
}

function getFidByType(item) {
	let result;
	switch (+item.type) {
		case 0:
			result = 1;
			break;
		case 1:
			result = 23;
			break;
		case 2:
			result = 14;
			break;
		default:
			result = 1;
	}
	return result;
}

export function changeRoomTotals(room, item, countDiff) {
	let isCustomItem = item.type == 1;
	let isBox = item.type == 2;
	let itemCF = +item.cf;
	setRoomTotalCount(countDiff);
	setRoomTotalCf(countDiff);
	
	function setRoomTotalCount(value) {
		if (isCustomItem) {
			room.total_custom_count = +room.total_custom_count || 0;
			room.total_custom_count += value;
		} else {
			room.total_count = +room.total_count || 0;
			room.total_count += value;
		}
		
		if (isBox) {
			room.boxes += value;
		}
		
		if (!room.count) {
			room.count = 0;
		}
		
		room.count += value;
		room.count = room.count || undefined;
	}
	
	function setRoomTotalCf(value) {
		let diffCF = value * itemCF;
		
		if (isCustomItem) {
			room.total_custom_cf = +room.total_custom_cf || 0;
			room.total_custom_cf += diffCF;
		} else {
			room.total_cf = +room.total_cf || 0;
			room.total_cf += diffCF;
		}
	}
}

export function changeFilterTotals(filter, item, countDiff) {
	let isCustomItem = item.type == 1;
	let itemCF = +item.cf;
	
	changeFilterTotalCount(countDiff);
	changeFilterTotalCf(countDiff);
	
	function changeFilterTotalCount(value) {
		if (isCustomItem) {
		} else {
			filter.total_count = +filter.total_count || 0;
			filter.total_count += value;
		}
	}
	
	function changeFilterTotalCf(value) {
		let diffCF = value * itemCF;
		
		if (isCustomItem) {
		} else {
			filter.total_cf = +filter.total_cf || 0;
			filter.total_cf += diffCF;
		}
	}
}

export function writeItemToRoomItems(item, room) {
	if (item.id) {
		room.items = room.items || [];
		
		let alreadyExist = _.findIndex(room.items, {id: item.id});
		
		if (alreadyExist != -1) {
			room.items[alreadyExist] = item;
		} else {
			room.items.push(item);
		}
	}
}

export function removeItemFromRoomItems(item, room) {
	if (item.id) {
		room.items = room.items || [];
		
		let alreadyExist = _.findIndex(room.items, {id: item.id});
		
		if (alreadyExist != -1) {
			room.items.splice(alreadyExist, 1);
		}
	}
}

export function updateRequestExtraService(item, rooms, request, isContractPage) {
	if (request && item.extra_service && item.extra_service != '0') {
		let extraService = getExtraService(item, request, isContractPage);
		
		extraService.extra_services[1].services_default_value = rooms.reduce(roomsLoop, 0);
		
		if (extraService.extra_services[1].services_default_value <= 0) {
			removeExtraService(item, request, isContractPage);
		}
	}
	function roomsLoop(sum, room) {
		let countInRoom = room.items.reduce(itemsLoop, 0);
		sum += countInRoom;
		return sum;
	}
	function itemsLoop(sum, itemInRoom) {
		if (itemInRoom.item_id == item.item_id) sum += itemInRoom.count;
		return sum;
	}
}

function removeExtraService(item, request, isContractPage) {
	if (request.status.raw == 3 && isContractPage) {
		if (!_.get(request, 'request_all_data.invoice.extraServices')) {
			_.set(request, 'request_all_data.invoice.extraServices', []);
		}
		
		request.request_all_data.invoice.extraServices = _.reject(request.request_all_data.invoice.extraServices, {inventory_item_id: item.item_id});
	} else {
		request.extraServices = _.reject(request.extraServices, {inventory_item_id: item.item_id});
	}
}

function getExtraService(item, request, isContractPage) {
	let extraServices;
	
	if (request.status.raw == 3 && isContractPage) {
		if (!_.get(request, 'request_all_data.invoice.extraServices')) {
			_.set(request, 'request_all_data.invoice.extraServices', []);
		}
		
		extraServices = _.get(request, 'request_all_data.invoice.extraServices');
	} else {
		extraServices = request.extraServices;
	}
	
	let result = _.find(extraServices, {inventory_item_id: item.item_id});
	
	if (!result) {
		result = {
			inventory_item_id: item.item_id,
			name: item.extra_service_name || item.name,
			edit: false,
			entity_id: request.nid,
			extra_services: [{
				services_default_value: item.extra_service,
				services_name: 'Cost',
				services_read_only: true,
				services_type: 'Amount',
			}, {
				services_default_value: 0,
				services_name: 'Count',
				services_read_only: true,
				services_type: 'Number',
			}]
		};
		
		extraServices.push(result);
	}
	
	return result;
}

export function updateRequestExtraServicesByItems(items, request) {
	request.extraServices = request.extraServices
		.filter(extraService => {
			let isUndefinedItemId = _.isUndefined(extraService.inventory_item_id);
			let isFindItem = !isUndefinedItemId && _.find(items, {item_id: extraService.inventory_item_id});
			
			return isUndefinedItemId || isFindItem;
		});

	let mergedItems = [];

	items.forEach(item => {
		let currentItem = _.find(mergedItems, (mergedItem) => item.item_id == mergedItem.item_id);

		if (currentItem) {
			currentItem.count += item.count;
		} else {
			mergedItems.push(item);
		}
	});

	mergedItems.forEach(item => updateRequestExtraServiceOnAccount(item, request));
}

export function updateRequestExtraServiceOnAccount(item, request, isContractPage) {
	if (request && item.extra_service && item.extra_service != '0') {
		let extraService = getExtraService(item, request, isContractPage);

		extraService.extra_services[1].services_default_value = item.count;

		if (extraService.extra_services[1].services_default_value <= 0) {
			removeExtraService(item, request, isContractPage);
		}
	}
}

export function mergeOldAndNewInventories(oldInventory, newInventory) {
	let mergedInventory = angular.copy(newInventory);
	
	_.forEach(oldInventory, item => {
		let indexInCurrent = _.findIndex(mergedInventory, inventoryItem => inventoryItem.id == item.id);
		
		if (indexInCurrent != -1) {
			mergedInventory[indexInCurrent].oldCount = item.count;
		} else {
			let copyItem = angular.copy(item);
			copyItem.oldCount = copyItem.count;
			copyItem.count = 0;
			mergedInventory.push(copyItem);
		}
	});
	
	return mergedInventory;
}

export function calculateTotals(listInventories = []) {
	var totals = {counts: 0, cfs: 0, boxes: 0, custom: 0};

	angular.forEach(listInventories, function (item) {
		if (item && angular.isDefined(item.fid)) {
			if (item.fid == 14) {
				totals.boxes += item.count;
			}

			if (item.fid == 23) {
				totals.custom += item.count;
			}

			totals.counts += item.count;

			if (angular.isDefined(item.cf)) {
				totals.cfs += item.cf * item.count;
			}
		}
	});

	return totals;
}