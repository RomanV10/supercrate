'use strict';
import "./inventory.styl";
import "../aside/newAside.styl";

angular
	.module('newInventories')
	.directive('inventories', inventories);

function inventories() {
	return {
		scope: {
			request: '=',
			packageId: '@',
			listInventories: '=listinventories',
			saveListInventories: '=',
			isContractPage: '=?',
			typeInventory: '@',
			closingFunction: '&'
		},
		template: require('./inventory.pug'),
		restrict: 'A',
		controller: 'inventoriesCtrl',
	};
}
