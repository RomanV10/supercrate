describe("directive: custom room name, ", () => {

	let customRoom = {
		boxes: 0,
		count: 11,
		created: "1510132045",
		custom_items: [],
		filters: [
			{
				filter_id: "47",
				items: [
					{
						cf: "5",
						count: "11",
						entity_id: "4613",
						entity_type: "0",
						extra_service: "50",
						extra_service_name: "поймать птичку",
						filter_id: "47",
						id: "16",
						image_link: "http://api.movecalc.local/sites/default/files/styles/inventory/public/inventory/avatar180_1.jpg?itok=guxxcPJ9",
						isCannotEdit: false,
						isSearch: true,
						item_id: "46",
						name: "Птичка",
						pac_rel_id: "0",
						packageId: undefined,
						package_id: "0",
						packing_fee: "0",
						price: "0",
						room_id: "42",
						selectedTypeInventory: 0,
						totals: {total_count: 11, total_cf: 55},
						type: "0",
						type_inventory: "0",
						uri: "public://inventory/avatar180_1.jpg"
					}
				],
				name: "Birds",
				rooms: ["1", "13", "15", "27", "28", "29", "30", "31", "42"],
				totals: {
					total_count: 11, total_cf: 55
				}
				,
				type: "0",
				weight: "0"
			}, {

				filter_id: "49",
				items: [],
				name: "черепаха",
				rooms: ["1", "9", "8", "5", "2", "15", "27", "28", "29", "30", "31", "39", "42"],
				type: "0",
				weight: "0",
			}, {

				filter_id: "48",
				items: [],
				name: "Cats",
				rooms: ["7", "5", "2", "15", "27", "28", "29", "30", "31", "39", "42"],
				type: "0",
				weight: "0"
			}],
		image_link: "/moveBoard/content/img/icons/boxes.png",
		items: [],
		name: "CustomRoom",
		opened: false,
		room_id: "42",
		total_cf: 55,
		total_count: 11,
		type: "1",
		uri: "/moveBoard/content/img/icons/boxes.png",
		weight: "22",
	};

	let $rootScope, $roomScope, $compile, $scope, roomElement, $templateCache;

	let whenGetInventory, whenGetItems, whenPutRoom;
	let getFlags, moveInventoryRoom, getInventoryRequest, getItems, request;

	let testFunction = function(param1, param2){

	};

	beforeEach(inject(function (_$httpBackend_, _$rootScope_, _$compile_, _$templateCache_) {
		$httpBackend = _$httpBackend_;
		$rootScope = _$rootScope_;
		$compile = _$compile_;
		$templateCache = _$templateCache_;
		
		getFlags = testHelper.loadJsonFile('inventory-get-flags.mock');
		moveInventoryRoom = testHelper.loadJsonFile('move-inventory-room.mock');
		getItems = testHelper.loadJsonFile('get-inventory-items.mock');
		
		
		$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.request.checkAdditionalInventory)).respond(200, [false]);
		$httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.item.getMyInventory)).respond(200, getInventoryRequest);
		whenPutRoom = $httpBackend.whenPUT(testHelper.makeRequest(moveBoardApi.inventory.room.update, {id: 42}));
		whenGetInventory = $httpBackend.whenGET(testHelper.makeRequest(moveBoardApi.inventory.room.index + '?entity_id=4366&entity_type=0&type_inventory=0&with_filters=1&with_items_total=1'));
		whenGetInventory.respond(200, moveInventoryRoom);
		$httpBackend.whenGET(testHelper.makeRequest(moveBoardApi.inventory.room.index + '?entity_type=0&with_filters=1&with_items_total=1')).respond(200, moveInventoryRoom);
		whenGetItems = $httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.inventory.item.getItems));
		whenGetItems.respond(200, getItems);

		$scope = $rootScope.$new();
		$scope.room = angular.copy(customRoom);
		$scope.deleteCustomItem = testFunction;

		roomElement = angular.element("<div custom-room-name room='room' delete-custom-room='deleteCustomRoom(room, callback)'></div>");
		roomElement = $compile(roomElement)($scope);

		$roomScope = roomElement.isolateScope();
		$roomScope.$apply();
	}));

	describe('function: endEdit(rename room), ', () => {
		it('shoud save new name if backend allow', () => {
			whenPutRoom.respond(200,{});
			$roomScope.newName = 'newRoomName';
			$roomScope.endEdit();
			$timeout.flush();
			expect($roomScope.room.name).toEqual("newRoomName");
		});

		it('should return old name if backend NOT allow', () => {
			whenPutRoom.respond(500,{});
			$roomScope.newName = 'newRoomName';
			$roomScope.endEdit();
			$timeout.flush();
			expect($roomScope.room.name).toEqual("CustomRoom");
		});

		it('should return old name if empty string', () => {
			whenPutRoom.respond(200,{});
			$roomScope.newName = '  ';
			$roomScope.endEdit();
			$timeout.flush();
			expect($roomScope.room.name).toEqual("CustomRoom");
		});

		it('should return old name if string contains "Bedroom"', () => {
			whenPutRoom.respond(200,{});
			$roomScope.newName = ' BEDroom 6 ';
			$roomScope.endEdit();
			$timeout.flush();
			expect($roomScope.room.name).toEqual("CustomRoom");
		});

		it('check request to backend', () => {
			$httpBackend.expectPUT(testHelper.makeRequest(moveBoardApi.inventory.room.index + '/42'), {
				data: {
					name: 'newRoomName',
					image_link: "/moveBoard/content/img/icons/boxes.png",
					type: '1'
				}
			}).respond(200, {});
			$roomScope.newName = 'newRoomName';
			$roomScope.endEdit();
			expect($roomScope.room.name).toEqual("CustomRoom");
			$httpBackend.flush();
		});
	});
});
