import './custom-room-name.styl';

angular
	.module('newInventories')
	.directive('customRoomName', customRoomName);

customRoomName.$inject = ['moveBoardApi', 'apiService'];

function customRoomName(moveBoardApi, apiService) {
	return {
		scope: {
			room: '=',
			deleteCustomRoom: '&',
			customRoomPermissions: "="
		},
		template: require('./custom-room-name.pug'),
		restrict: 'EA',
		link: customRoomNameLink
	};
	
	function customRoomNameLink($scope, $element, attrs) {
		$scope.enableEdit = enableEdit;
		$scope.endEdit = endEdit;
		$scope.clickOnInput = clickOnInput;
		$scope.inputKeyUp = inputKeyUp;
		$scope.removeRoom = removeRoom;
		$scope.isNotBedroom = $scope.room.type == 1;
		
		
		let input;
		
		init();
		
		function init() {
			$scope.newName = angular.copy($scope.room.name);
			$scope.editMode = false;
			$scope.saving = false;
			$scope.removing = false;
			input = $element.find('.title-input');
			
			input.attr('readonly', 'readonly');
			input.on('keyup', inputKeyUp);
		}
		
		function removeRoom(event) {
			if (event) {
				event.stopPropagation();
			}
			if (!$scope.removing) {
				$scope.removing = true;
				$scope.deleteCustomRoom({room: $scope.room, callback: callback});
				function callback(isSuccess) {
					if (isSuccess) {
						$scope.removing = false;
					} else {
						$scope.removing = false;
					}
				}
			}
		}
		
		function inputKeyUp(event) {
			let isEnter = event.keyCode == 13;
			if (isEnter) {
				input.blur();
				
				event.stopPropagation();
				event.preventDefault();
			}
		}
		
		function clickOnInput(event) {
			if ($scope.editMode) {
				event.stopPropagation();
			} else {
				event.preventDefault();
			}
		}
		
		function endEdit() {
			$scope.editMode = false;
			input.removeClass('editable');
			input.attr('readonly', 'readonly');
			
			if (invalidName()) {
				$scope.newName = angular.copy($scope.room.name);
			} else {
				saveRoomName();
			}
			
			function invalidName() {
				let isEmptyString = !$scope.newName.trim().length;
				let isBedroom = $scope.newName.toLowerCase().indexOf('bedroom') != -1;
				return isEmptyString || isBedroom;
			}
		}
		
		function enableEdit($event) {
			$event.stopPropagation();
			if (!$scope.editMode) {
				$scope.editMode = true;
				input.addClass('editable');
				input.removeAttr('readonly');
				input.focus();
				input.select();
			}
		}
		
		function saveRoomName() {
			if (!$scope.saving) {
				$scope.saving = true;
				sendNewNameRequest();
			}
			function sendNewNameRequest() {
				apiService.putData(moveBoardApi.inventory.room.update, {
					id: $scope.room.room_id,
					data: {
						name: $scope.newName,
						image_link: $scope.room.image_link,
						type: $scope.room.type
					}
				}).then(response => {
					let noError = response.status == 200;
					if (noError) {
						$scope.room.name = angular.copy($scope.newName);
					} else {
						$scope.newName = angular.copy($scope.room.name);
					}
					$scope.saving = false;
				}, error => {
					$scope.newName = angular.copy($scope.room.name);
					$scope.saving = false;
				});
			}
		}
	}
}