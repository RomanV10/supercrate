describe('Unit: Inventory item directive test', function () {
	var element, $scope, scope, SweetAlert;
	const ADD_ITEM_SELECTOR = '.simple-item .js-counter[data-count=1]';
	const REMOVE_ITEM_SELECTOR = '.simple-item .js-counter[data-count=-1]';

	function addItem() {
		element.find(ADD_ITEM_SELECTOR).trigger('click');
	}

	function removeItem() {
		element.find(REMOVE_ITEM_SELECTOR).trigger('click');
	}

	beforeEach(inject(function (_SweetAlert_) {
		SweetAlert = _SweetAlert_;
		$scope = $rootScope.$new();
		$scope.item = {
			cf: "22",
			extra_service: "0",
			filters: ["1"],
			handle_fee: "0",
			item_id: "2",
			name: "Loveseat",
			packing_fee: "0",
			price: "0",
			status: "0",
			total_cf: "22",
			count: "15",
			baseTotal: {
				total_cf: 0,
				total_count: 0,
			},
			type: 1,
			visible: true,
			weight: "0"
		};

		$scope.room = {
			created: "1498225189",
			filters: [
				{
					active: true,
					filter_id: "1",
					items: [],
					name: "Sofa, Chair",
					rooms: [],
					totals: {
						total_cf: "785",
						total_count: "79"
					},
					type: "0",
					weight: "0"
				},
				{
					active: true,
					filter_id: "11",
					items: [],
					name: "Bed, Mattress",
					rooms: [],
					totals: {
						total_cf: "785",
						total_count: "79"
					},
					type: "0",
					weight: "0"
				},
				{
					active: true,
					filter_id: "12",
					items: [],
					name: "Boxes, Totes",
					rooms: [],
					totals: {
						total_cf: "785",
						total_count: "79"
					},
					type: "0",
					weight: "0"
				},
				{
					active: true,
					filter_id: "13",
					items: [],
					name: "Miscellaneous",
					rooms: [],
					totals: {
						total_cf: "785",
						total_count: "79"
					},
					type: "0",
					weight: "0"
				},
				{
					active: true,
					filter_id: "14",
					items: [],
					name: "Dresser, Mirror",
					rooms: [],
					totals: {
						total_cf: "785",
						total_count: "79"
					},
					type: "0",
					weight: "0"
				},
				{
					active: true,
					filter_id: "15",
					items: [],
					name: "Soffaaaaa",
					rooms: [],
					totals: {
						total_cf: "785",
						total_count: "79"
					},
					type: "0",
					weight: "0"
				}
			],
			image_link: "/moveBoard/content/img/icons/boxes.png",
			name: "All",
			opened: true,
			room_id: "15",
			total_cf: "1207",
			total_count: "111",
			type: "0",
			weight: "0"
		};

		$scope.filter = {
			active: true,
			filter_id: "1",
			items: [],
			name: "Sofa, Chair",
			rooms: [],
			totals: {
				total_cf: "785",
				total_count: "79"
			},
			type: "0",
			weight: "0"
		};

		function debounce(callback) {
			callback();

			return function () {};
		}

		spyOn(_, 'debounce').and.callFake(debounce);

		$scope.changeItemCount = (a,b,c) => {};

		$scope.request = {};

		$scope.afterSavingItemCount = () => {};
		$scope.activeRequests = [];

		element = $compile("<div new-inventory-item request='request' filter='filter' room='room' inventory-data='item' change-Item-Count='changeItemCount(item, value, isManualUpdated)' after-saving-item-count='afterSavingItemCount(result)' active-requests='activeRequests'></div>")($scope);

		$httpBackend.whenGET(testHelper.makeRequest('server/move_inventory_room?entity_type=0&with_filters=1&with_items_total=1')).respond(200, {});
		$httpBackend.whenPOST(testHelper.makeRequest('server/move_inventory_item/add_item_to_request')).respond(200, {});
		scope = element.isolateScope();
	}));

	describe('On init', () => {
		it ("Should set item.oldCount", () => {
			expect(scope.item.oldCount).toEqual(15);
		});
	});

	describe("funciion: onClickCounter", () => {
		describe('onSuccess', () => {
			beforeEach(() => {
				spyOn($scope, 'changeItemCount').and.callFake(() => {});
				spyOn($scope, 'afterSavingItemCount').and.callFake(() => {});
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.request.updateCustomItemInRequest))
					.respond(200, {status_code: 200, data: 333});
				scope.onClickCounter(70);
				$httpBackend.flush();
			});

			it('Should set oldCount to new after function', () => {
				expect(scope.item.oldCount).toEqual(15);
			});

			it ('Should add promise to activeRequests', () => {
				expect(scope.activeRequests.length).toEqual(1);
			});

			it('Should call change item count', () => {
				expect($scope.changeItemCount).toHaveBeenCalled();
			});

			it('Should call after saving item count', () => {
				expect($scope.afterSavingItemCount).toHaveBeenCalled();
			});
		});

		describe('onError', () => {
			beforeEach(() => {
				spyOn(SweetAlert, 'swal').and.callFake(() => {});
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.request.updateCustomItemInRequest))
					.respond(400, {status_code: 400, data: 333});
				spyOn($scope, 'changeItemCount').and.callFake(() => {});
				spyOn($scope, 'afterSavingItemCount').and.callFake(() => {});

				scope.onClickCounter(70);
				$httpBackend.flush();
			});

			it('Should set oldCount to new after function', () => {
				expect(scope.item.oldCount).toEqual(15);
			});

			it ('Should add promise to activeRequests', () => {
				expect(scope.activeRequests.length).toEqual(1);
			});

			it('Should call change item count', () => {
				expect($scope.changeItemCount).toHaveBeenCalled();
			});

			it('Should call after saving item count', () => {
				expect($scope.afterSavingItemCount).toHaveBeenCalled();
			});

			it('Should call swal', () => {
				expect(SweetAlert.swal).toHaveBeenCalled();
			});
		});
	});

});
