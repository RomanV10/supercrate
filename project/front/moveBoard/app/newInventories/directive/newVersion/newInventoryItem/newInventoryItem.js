'use strict';
import './newInventoryItem.styl';

angular
	.module('newInventories')
	.directive('newInventoryItem', newInventoryItem);

/*@ngInject*/
function newInventoryItem() {
	return {
		scope: {
			item: '=inventoryData',
			request: '=',
			changeItemCount: '&',
			afterSavingItemCount: '&',
			activeRequests: '=',
		},
		template: require('./newInventoryItem.pug'),
		restrict: 'A',
		controller: 'newInventoryItemCTRL'
	};
}
