'use strict';

const UPDATE_ITEM_DEBOUNCE = 500;

angular
	.module('newInventories')
	.controller('newInventoryItemCTRL', newInventoryItemCTRL);

/*@ngInject*/
function newInventoryItemCTRL($scope, $element, moveBoardApi, apiService, SweetAlert, $q) {
	let $counterContent;
	let $addButton;
	let type = $scope.item.type || 0;
	let isCustomItem = type == 1;
	let $searchInput = angular.element('.js-inventory-search');
	let room = $scope.item.room || {};
	let filter = $scope.item.filter || {};
	let updateItemUrl = makeAddItemUrl(isCustomItem);
	let itemId = $scope.item.id;

	setUpTotals();

	$scope.onClickCounter = onClickCounter;
	$scope.manualUpdate = manualUpdate;

	$scope.isAccount = IS_ACCOUNT;
	$scope.itemName = $scope.item.name;
	$scope.item.oldCount = $scope.item.count;

	if ($scope.item.extra_service && $scope.item.extra_service != '0') {
		$element.find('.js-extraservice').css('display', 'block');
	} else {
		$element.find('.js-extraservice').css('display', 'none');
	}

	$counterContent = $element.find('.js-counter-conteiner');
	$addButton = $element.find('.js-add-button');

	if (IS_ACCOUNT && isCustomItem) {
		$scope.itemName = $scope.itemName.split('(')[0];
	}

	let requestDebounce = _.debounce(() => {
		updateItem($scope.item, room, filter, updateItemUrl)
			.then(() => {
				$scope.afterSavingItemCount({
					result: {
						success: true,
						item: $scope.item
					}
				});
			}, () => {
				$scope.afterSavingItemCount({
					result: {
						success: false,
						item: $scope.item
					}
				});
			});
	}, UPDATE_ITEM_DEBOUNCE);

	function onClickCounter(value) {
		if (value) {
			if (+$scope.item.count !== 0) {
				itemId = $scope.item.id;
			} else {
				itemId = undefined;
			}

			$scope.changeItemCount({
				item: $scope.item,
				value: value
			});
			setFocusSearchInput(value);
			$scope.item.oldCount = $scope.item.count;
		}

		requestDebounce();
	}

	this.makeAddItemUrl = makeAddItemUrl;

	function makeAddItemUrl(isCustomItem) {
		return isCustomItem
			   ? moveBoardApi.inventory.request.updateCustomItemInRequest
			   : moveBoardApi.inventory.request.updateItemInRequest;
	}

	this.updateItem = updateItem;

	function updateItem(item, room, filter, updateItemUrl) {
		let defer = $q.defer();
		let data = makeUpdateCountObject(item, room, filter);
		saveItemCount(data, item, updateItemUrl).then(res => {
			defer.resolve(res);
		}, rej => {
			defer.reject(rej);
		});
		return defer.promise;
	}

	function makeUpdateCountObject(item, room, filter) {
		let typeInventory = item.selectedTypeInventory;
		let result = {
			id: item.id || itemId,
			item_id: item.item_id,
			room_id: room.room_id,
			name: item.name,
			count: item.count,
			cf: item.cf,
			price: 0,
			entity_type: 0,
			type: item.type,
			type_inventory: typeInventory,
		};
		if (item.moved) {
			result.moved = 1;
		}
		if (filter && +filter.filter_id >= 0) {
			result.filter_id = filter.filter_id;
		}

		if ($scope.packageId) {
			result.package_id = $scope.packageId;
			result.entity_id = $scope.packageId;
		} else {
			result.entity_id = $scope.request.nid;
		}

		return result;
	}

	function saveItemCount(data, item, updateItemUrl) {
		let defer = $q.defer();
		let saveItemCountPromise = apiService.postData(updateItemUrl, {data});
		$scope.activeRequests.push(saveItemCountPromise);
		saveItemCountPromise.then(res => {
			if (res.data.status_code == 200) {
				if (data.count) {
					item.id = res.data.data;
					defer.resolve(res);
				} else {
					if (res.data.data >= 0) {
						defer.resolve(res);
					} else {
						defer.reject(res);
					}
				}
			} else {
				defer.reject(res);
				SweetAlert.swal('Error', 'Cannot save item\'s count', 'error');
			}
		}, rej => {
			defer.reject(rej);
			SweetAlert.swal('Error', 'Cannot connect to server', 'error');
		});
		return defer.promise;
	}


	function manualUpdate() {
		$scope.changeItemCount({
			item: $scope.item,
			value: $scope.item.count,
			isManualUpdated: true
		});
		$scope.item.oldCount = $scope.item.count;

		requestDebounce();
	}

	$element.find('img').on('dblclick', e => e.preventDefault());

	function setUpTotals() {
		$scope.item.count = Number($scope.item.count || 0);

		filter.total_count = Number(filter.total_count || 0);

		room.items = room.items || [];

		if (isCustomItem) {
			room.total_custom_cf = Number(room.total_custom_cf || 0);
			room.total_custom_count = Number(room.total_custom_count || 0);
		} else {
			room.total_cf = Number(room.total_cf || 0);
			room.total_count = Number(room.total_count || 0);
		}

		room.boxes = Number(room.boxes || 0);
	}

	function setFocusSearchInput() {
		if ($scope.item.isSearch && !_.isEmpty($searchInput.val())) {
			$searchInput.focus();
			$searchInput.select();
		}
	}

	$scope.$watch('item.count', showAddButton);

	function showAddButton() {
		let isShowButtong = !$scope.item.count || $scope.item.count == '0';
		$addButton.css('display', isShowButtong ? 'block' : 'none');
		$counterContent.css('display', isShowButtong ? 'none' : 'flex');
	}
}
