describe('inventory-table directive', () => {
	let Room1, Room2;
	var $scope, config, $q;
	let $tableScope, element;

	beforeEach(inject(function (_config_, _$q_) {
		$q = _$q_;
		config = _config_;
		$scope = $rootScope.$new();

		Room1 = testHelper.loadJsonFile('room1.mock');
		Room2 = testHelper.loadJsonFile('room2.mock');

		$scope.updateAfterDrop = function() {};
		$scope.changeItemCount = function(item, count) {};
		$scope.tableArguments = {};
		$scope.tableArguments.rooms = [];
		$scope.tableArguments.rooms.push(Room1);
		$scope.tableArguments.rooms.push(Room2);
		$scope.tablePermissions = {
			allowDrag: true,
			allowRemove: true
		};

		element = angular.element('<div inventory-table="inventory-table" table-Arguments="tableArguments" table-Permissions="tablePermissions" column-width="3" change-item-count="changeItemCount(item, value)" update-After-Drop="updateAfterDrop()" class="container inventory__table-new"></div>');
		element = $compile(element)($scope);
		$tableScope = element.isolateScope();
	}));

	describe("function: rememberForDrag, ", () => {
		it ("must remember item which was clicked", () => {
			let item = {id: '123'};
			$tableScope.rememberForDrag(item);
			expect($tableScope.selectedItem).toBe(item);
		});
	});

	describe('function: removeItem, ', () => {
		it('must call parent\'s changeRequestItem with value = -total_count', () => {
			spyOn($tableScope, 'changeItemCount');
			let item = {
				count: 5,
				total_cf: 10,
				room: {
					room_id: 13,
				},
			};
			$tableScope.removeItem(item);
			expect($tableScope.changeItemCount).toHaveBeenCalledWith({ item : item, value : -5 });
		});

	});

	describe('roomClass should be', () => {
		it ('default 4', () => {
			element = angular.element("<inventory-table></inventory-table>");
			$compile(element)($scope);
			let tableScope = element.isolateScope();
			expect(tableScope.roomClass).toEqual("col-md-4");
		});
		it ('set to 3', () => {
			element = angular.element("<inventory-table column-width='3'></inventory-table>");
			$compile(element)($scope);
			let tableScope = element.isolateScope();
			expect(tableScope.roomClass).toEqual("col-md-3");
		});
	});

	describe('init function', () => {
		let tableScope;
		describe('when no rooms attr', () => {
			let rooms;
			beforeEach(() => {
				rooms = {data:[
					{total_count:1, name:'filled', "room_id": "1", "weight": "1"},
					{total_custom_count:1, name:'customed', "room_id": "2", "weight": "2"},
					{total_count:0, name: 'empty', "room_id": "3", "weight": "3"}
				], status_code: 200};
				let getInventoryRequest = testHelper.loadJsonFile('get-inventory-request.mock');
				let data = {
					conditions: {
						entity_id: 1234,
						entity_type: 0,
						type_inventory: 0
					}
				};
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.room.rooms), data).respond(200, rooms);
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.inventory.item.getMyInventory)).respond(200, getInventoryRequest);
				element = angular.element("<inventory-table entity-id='1234'></inventory-table>");
				$compile(element)($scope);
				tableScope = element.isolateScope();
				$scope.$apply();
				$httpBackend.flush();
			});
			it('must do request to api when no rooms attr', () => {
				expect(tableScope.roomsHolder.rooms[0].name).toBe('filled');
			});
			it('must write true to scope.onlyViewFilledRooms if no rooms attr', () => {
				expect(tableScope.onlyViewFilledRooms).toBeTruthy();
			});
			it('must remove all empty rooms if no rooms attr', () => {
				expect(tableScope.roomsHolder.rooms.length).toEqual(2);
				expect(tableScope.roomsHolder.rooms[0].name).toEqual('filled');
				expect(tableScope.roomsHolder.rooms[1].name).toEqual('customed');
				expect(tableScope.roomsHolder.rooms[3]).toBeFalsy();
			});
		});


		it('must NOT do request to api when exist rooms attr', () => {
			$scope.tableArguments = {rooms: ['Presetted']};
			element = angular.element("<inventory-table entity-id='1234' table-Arguments='tableArguments' table-Permissions='tablePermissions'></inventory-table>");
			$compile(element)($scope);
			tableScope = element.isolateScope();
			tableScope.$apply();
			expect(tableScope.tableArguments.rooms).toBe($scope.tableArguments.rooms);
		});
		it('must write false to scope.onlyViewFilledRooms when exist rooms attr', () => {
			$scope.tableArguments = {rooms: ['Presetted']};
			element = angular.element("<inventory-table entity-id='1234' table-Arguments='tableArguments' table-Permissions='tablePermissions'></inventory-table>");
			$compile(element)($scope);
			tableScope = element.isolateScope();
			tableScope.$apply();
			expect(tableScope.onlyViewFilledRooms).toBeFalsy();
		});
	});

});
