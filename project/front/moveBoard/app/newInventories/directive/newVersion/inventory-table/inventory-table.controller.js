'use strict';

import {
	changeFilterTotals,
	changeRoomTotals,
	removeItemFromRoomItems,
	writeItemToRoomItems
} from '../additionalFunctions';

angular
	.module('newInventories')
	.controller('inventoryListCtrl', inventoryListCtrl);

/*@ngInject*/
function inventoryListCtrl($scope, $controller, $q, apiService, moveBoardApi, SweetAlert) {
	$scope.removeItem = removeItem;
	$scope.rememberForDrag = rememberForDrag;
	$scope.dragulaDropFunction = dragulaDropFunction;
	$scope.onlyViewFilledRooms = false;
	$scope.selectedItem = {};
	$scope.roomsHolder = {rooms: {}};

	if (IS_ACCOUNT) {
		$scope.$on('onReloadInventoryTable', init);
	}

	init();

	function init() {
		let columnWidth = $scope.columnWidth || 4;

		$scope.sortedRooms = [];
		$scope.roomClass = `col-md-${columnWidth}`;

		if (!$scope.tableArguments) {

			$scope.onlyViewFilledRooms = true;

			let requestForRooms = apiService.postData(moveBoardApi.inventory.room.rooms, {
				conditions: {
					entity_id: $scope.entityId || null,
					entity_type: 0,
					type_inventory: 0
				}
			});

			let data = {
				entity_id: $scope.entityId || null,
				entity_type: 0,
				conditions: {
					type_inventory: 0
				}
			};
			let requestForItems = apiService.postData(moveBoardApi.inventory.item.getMyInventory, data);

			$q.all([requestForRooms, requestForItems]).then(res => {
				let noErrors = res[0].data.status_code == 200 && res[1].data.status_code == 200;
				if (noErrors) {
					removeEmptyRooms(res[0].data.data);
					$scope.roomsHolder = {rooms: res[0].data.data};
					parseGetMyInventory(res[1].data.data);
				} else {
					let errorMsg = res[0].data.status_message + '\n' + res[1].data.status_message;
					SweetAlert.swal('Error', 'Cannot get inventory items or rooms', 'error');
				}
			}, rej => {
				SweetAlert.swal('Error', 'Cannot connect to server', 'error');
			});
		} else {

			$scope.roomsHolder = $scope.tableArguments;
			addWatchers();
		}

		function addWatchers() {
			$scope.$on('inventory-table-bag.drop-model', dragulaDropFunction);
		}

		function parseGetMyInventory(response) {
			response.forEach((item) => {
				let room = _.find($scope.roomsHolder.rooms, {room_id: item.room_id}) || {};

				room.items = room.items || [];

				checkDefaults(item);
				writeItemToRoomItems(item, room);

				function checkDefaults(item) {
					item.count = item.count || 0;
					item.total_cf = item.total_cf || 0;
					item.room = room;
				}

			});
		}
	}

	function rememberForDrag(item) {
		$scope.selectedItem = item;
	}

	function dragulaDropFunction(element, target, destination, source) {
		if (!$scope.tablePermissions.allowDrag) return;
		$scope.tableArguments.blockTable = true;
		let destinationRoomId = destination.attr('roomid');
		let destinationRoom = _.find($scope.roomsHolder.rooms, room => {
			return room.room_id == destinationRoomId;
		});
		let destinationFilter = _.find(destinationRoom.filters, {filter_id: '-1'});

		let sourceRoom = $scope.selectedItem.room;
		let sourceFilter = $scope.selectedItem.filter;

		sendReplaceRequest(sourceRoom, sourceFilter, destinationRoom, destinationFilter, $scope.selectedItem);
	}

	function sendReplaceRequest(sourceRoom, sourceFilter, destinationRoom, destinationFilter, item) {
		let data = {
			'id': item.id,
			'room_id': destinationRoom.room_id
		};
		apiService.postData(moveBoardApi.inventory.item.moveItem, data)
			.then(response => {
				let noError = response.data.status_code == 200;
				if (noError) {
					continueChanging(sourceRoom, sourceFilter, destinationRoom, destinationFilter, item);
				} else {
					revertChanges(item, sourceRoom, destinationRoom, 'Cannot replace item');
				}
			},reject => {
				revertChanges(item, sourceRoom, destinationRoom, 'Cannot connect to server');
			});
	}

	function revertChanges(item, sourceRoom, destinationRoom, reason) {
		SweetAlert.swal('Error', reason, 'error');
		writeItemToRoomItems(item, sourceRoom);
		removeItemFromRoomItems(item, destinationRoom);
		$scope.tableArguments.blockTable = false;
	}

	function continueChanging(sourceRoom, sourceFilter, destinationRoom, destinationFilter, item) {
		item = breakLinkToOriginal(item, sourceFilter, destinationRoom);
		removeOriginalCustomItem(item, sourceRoom);
		changeRoomInItem(item, sourceRoom, destinationRoom);
		changeFilterInItem(item, sourceFilter, destinationFilter);
		addItemToNewRoom(item, destinationRoom, destinationFilter);
		$scope.updateAfterDrop();

	}

	function removeOriginalCustomItem(item, sourceRoom) {
		if (item.type == 1) {
			let index = _.findIndex(sourceRoom.custom_items, {id: item.id});
			if (index != -1) {
				sourceRoom.custom_items.splice(index, 1);
			}
		}
	}

	function addItemToNewRoom(item, destinationRoom, destinationFilter) {
		item.moved = 1;
		if (destinationFilter) {
			destinationFilter.items.push(item);
		}
		if (item.type == 1) {
			destinationRoom.custom_items.push(item);
		}
		writeItemToRoomItems(item, destinationRoom);
	}

	function breakLinkToOriginal(item, sourceFilter, destinationRoom) {
		if (sourceFilter) {
			let index = _.findIndex(sourceFilter.items, {id: item.id});
			let newCopy = {};
			angular.copy(sourceFilter.items[index], newCopy);
			let sinceDropped = _.findIndex(destinationRoom.items, {id: item.id});
			destinationRoom.items[sinceDropped] = newCopy;
			cleanFilterItem(sourceFilter.items[index]);
			return newCopy;
		} else {
			return item;
		}
	}

	function cleanFilterItem(filterItem) {
		filterItem.count = 0;
		filterItem.total_cf = 0;
		delete filterItem.id;
		delete filterItem.replacing;
	}

	function changeFilterInItem(item, sourceFilter, destinationFilter) {
		if (destinationFilter) {
			item.filter = destinationFilter;
			item.filter_id = destinationFilter.filter_id;
		} else {
			delete item.filter;
			delete item.filter_id;
		}
		changeFiltersTotals(item, sourceFilter, destinationFilter);
	}

	function changeFiltersTotals(item, sourceFilter, destinationFilter) {
		if (sourceFilter) {
			changeFilterTotals(sourceFilter, item, -item.count);
		}
		if (destinationFilter) {
			changeFilterTotals(destinationFilter, item, +item.count);
		}
	}

	function changeRoomInItem(item, sourceRoom, destinationRoom) {
		item.room = destinationRoom;
		item.room_id = destinationRoom.room_id;
		changeRoomsTotals(item, sourceRoom, destinationRoom);
	}

	function changeRoomsTotals(item, sourceRoom, destinationRoom) {
		changeRoomTotals(sourceRoom, item, -item.count);
		changeRoomTotals(destinationRoom, item, +item.count);
	}

	function removeItem(item) {
		if ($scope.tablePermissions.allowRemove) {
			let fakeElement = {
				find:() => fakeElement,
				css: () => fakeElement,
				on: () => fakeElement,
			};
			let controller = $controller('newInventoryItemCTRL', {
				$scope: {
					item: item,
					request: {nid: $scope.entityId},
					changeItemCount: () => {},
					afterSavingItemCount: () => {},
					activeRequests: [],
					$watch: () => {},
				},
				$element: fakeElement
			});
			let oldCount = item.count;
			item.count = 0;
			controller.updateItem(item, item.room, item.filter, controller.makeAddItemUrl(item.type == 1));
			item.count = oldCount;
			$scope.changeItemCount({
				item: item,
				value: -item.count
			});
		}
	}

	function removeEmptyRooms(rooms) {
		let emptyAt;
		do {
			emptyAt = _.findIndex(rooms, room => {
				return !room.total_count && !room.total_custom_count;
			});
			if (emptyAt !== -1) {
				rooms.splice(emptyAt, 1);
			}
		} while (emptyAt !== -1);
	}
}

