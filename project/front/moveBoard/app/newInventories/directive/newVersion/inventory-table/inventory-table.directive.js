'use strict';

import './inventory-table.styl';

angular
	.module('newInventories')
	.directive('inventoryTable', inventoryTable);

/*@ngInject*/
function inventoryTable() {

	return {
		restrict: 'AE',
		template: require('./inventory-table.pug'),
		controller: 'inventoryListCtrl',
		scope: {
			tableArguments: '=',
			tablePermissions: '<',
			entityId: '=',
			columnWidth: '@',
			changeItemCount: '&',
			updateAfterDrop: '&'
		}
	};


}
