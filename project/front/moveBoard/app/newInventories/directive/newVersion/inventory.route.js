angular.module("newInventories")
	.config([
			"$stateProvider",
			function ($stateProvider)
			{
				$stateProvider
					.state('newinventory', {
						url: '/inventory',
						abstract: true,
						template: '<ui-view/>'
					})
					.state('newinventory.page', {
						url: '/:id',
						template: require('./inventory-page/inventory-page.pug'),
						title: 'Inventory Page',
						controller: 'InventoryPageController as inventoryPage',
						data: {
							permissions: {
								except: ['anonymous', 'foreman', 'helper']
							}
						}
					})
					.state('newinventory.back_to_request', {
						url: '/edit_request/:id',
						template: require('../../../mobile-request/mobileRequest.html'),
						title: 'Mobile Request',
						params: {initRequest: false},
						controller: 'editMobileController as vm',
						data: {
							permissions: {
								except: ['anonymous', 'foreman', 'helper']
							}
						}
					});


			}
		]
	);
