'use strict';
import {
	changeFilterTotals,
	changeRoomTotals,
	converInventoryToOldVersion,
	createCustomItems,
	mainTotalCalc,
	mergeOldAndNewInventories,
	removeItemFromRoomItems,
	updateRequestExtraService,
	writeItemToRoomItems,
	calculateTotals
} from './additionalFunctions.js';

const CUSTOM_ITEM_IMAGE_LINK = '/moveBoard/content/img/Custom.jpg';
const SEND_SEARCH_REQUEST__TIMEOUT = 300;
const CUSTOM_BEDROOM_TYPE = 2;
const DEFAULT_BEDROOM_ID = 5;
const ALL_ROOM_ID = 15;
const LARGE_CUSTOM_ITEM_CF = 150;
const SHOW_SAVE_CHANGES_ALERT_TIMEOUT = 300;
const ACCOUNT_INVENTORY_MODAL_CONTENT_SELECTOR = '.inventory_modal .modal-content';
const SAVE_INVENTORY_DELAY = 600;

angular
	.module('newInventories')
	.controller('inventoriesCtrl', inventoriesCtrl);

/*@ngInject*/
function inventoriesCtrl($scope, $element, apiService, moveBoardApi, datacontext, RequestServices, $timeout, erDeviceDetector, $window, Session, $q, SweetAlert, $rootScope, debounce, printService, additionalServicesFactory, inventoryExtraService) {
	$scope.inventoryItems = [];
	$scope.inventoryItemPlaceHolder = '<div> Loading Items! </div>';
	$scope.pageInventory = 0;
	$scope.countPages = 2;
	$scope.tablePermissions = {};
	$scope.allowCreatingCustomRooms = false;
	$scope.customRoomPermissions = {};
	$scope.inventoryItemsPermissions = {};

	switch ($scope.typeInventory) {
		case 'inventoryMoving':
		case 'addInventoryMoving':
			$scope.pageInventory = 1;
			$scope.countPages = 2;
			break;

		case 'secondAddInventoryMoving':
			$scope.pageInventory = 2;
			$scope.countPages = 3;
			break;
	}

	if (_.isUndefined($scope.listInventories) || _.isNull($scope.listInventories)) {
		$scope.listInventories = [];
	}

	$scope.rootLoadingInventory = true;
	let inventorySettings = angular.fromJson(datacontext.getFieldData().inventorySetting);
	let updateItemTimeOut;
	let isTotalWeightChanged = false;
	let session = Session.get();
	let userRole = session.userRole;
	let additionalInventoryField = getAdditionalInventoryField();
	let destroyCount = 0;
	let savedInventoryItems;
	let isShowSaveSweetAlert = true;
	let showPleaseSaveChangesModalWithDebounce = debounce(SHOW_SAVE_CHANGES_ALERT_TIMEOUT, showPleaseSaveChangesModal);
	let oldExtraService = angular.copy(_.get($scope, 'request.extraServices'));
	$scope.isContractPage = !!$scope.isContractPage;
	$scope.busy = true;
	$scope.isMobile = erDeviceDetector.isMobile;
	$scope.isSmallScreen = $window.innerWidth < 1225;
	$scope.fullscreenMode = $scope.isMobile || $scope.isSmallScreen;
	$scope.activeRoom = {};
	$scope.showMobileMenu = false;
	$scope.menuMode = inventorySettings && inventorySettings.showItemsInFilter ? 1 : 0;
	$scope.customMode = false;
	$scope.isAccount = IS_ACCOUNT;
	$scope.isAdmin = ~userRole.indexOf('administrator') || ~userRole.indexOf('foreman') || ~userRole.indexOf('manager') || ~userRole.indexOf('sales');
	$scope.isClient = userRole.length == 1 && ~userRole.indexOf('authenticated user');
	$scope.isHomeEstimator = ~userRole.indexOf('home-estimator');
	$scope.adminOnAccount = $scope.isAdmin && $scope.isAccount && !$scope.isContractPage;
	$scope.searchText = '';
	$scope.newItem = {};
	$scope.inventoryItems = [];
	$scope.isPackageSettings = $scope.packageId ? true : false;
	$scope.showAdditionalInventory = false;
	$scope.total = {
		total_cf: 0, count: 0, boxes: 0, total_custom: 0
	};
	$scope.myInventory = {
		count: '', opened: false
	};
	$scope.tableArguments = {
		blockTable: false
	};
	$scope.inventoryItemPlaceHolder = '<div> Loading Items! </div>';
	$scope.creatingCustomBedroom = false;
	$scope.creatingCustomRoom = false;
	$scope.activeRequests = [];
	$scope.mobileSavingInventory = false;
	$scope.listInventories.forEach(updateItemOldCount);

	let requestInventory = _.get($scope, 'request.inventory.inventory_list', []);
	let initInventoryTotals = calculateTotals(requestInventory || []);

	$scope.openMobileMenu = openMobileMenu;
	$scope.getMyInventory = getMyInventory;
	$scope.openRoom = openRoom;
	$scope.close = close;
	$scope.search = search;
	$scope.selectFilter = selectFilter;
	$scope.setFilter = setFilter;
	$scope.setCustomItemMode = setCustomItemMode;
	$scope.addCustomItem = addCustomItem;
	$scope.removeItem = removeItem;
	$scope.calcMyInventory = calcMyInventory;
	$scope.changeInventoryPage = changeInventoryPage;
	$scope.changeItemCount = changeItemCount;
	$scope.calculatePrint = calculatePrint;
	$scope.print = print;
	$scope.createCustomRoom = createCustomRoom;
	$scope.deleteCustomRoom = deleteCustomRoom;
	$scope.updateAfterDrop = updateAfterDrop;
	$scope.openMyInventory = openMyInventory;
	$scope.clearSearchText = clearSearchText;
	$scope.afterSavingItemCount = afterSavingItemCount;

	init();

	loadRoomsAndInventory();
	function updateItemOldCount(item) {
		item.oldCount = Number(item.count);
	}

	function init() {
		apiService.postData(moveBoardApi.inventory.request.checkAdditionalInventory, {'entity_id': $scope.request.nid})
			.then(({data}) => {
				$scope.showAdditionalInventory = data.data && !$scope.isAccount || $scope.isContractPage && $scope.isAccount;
				setTablePermissions();
				setCustomRoomPermissions();
				setInventoryItemsPermissions();
			})
			.finally(() => {
				$scope.rootLoadingInventory = false;
			});

		if ($scope.isClient) {
			angular.element(ACCOUNT_INVENTORY_MODAL_CONTENT_SELECTOR)
				.mouseleave(onMouseLeaveInventoryModal)
				.mouseenter(onMouseEnterInventoryModal);

			RequestServices.sendLogs([{
				simpleText: 'Client opened the Inventory Modal',
			}], 'Inventory', $scope.request.nid, 'MOVEREQUEST');
		}
	}

	function onMouseLeaveInventoryModal() {
		showPleaseSaveChangesModalWithDebounce();
	}

	function showPleaseSaveChangesModal() {
		let isNotSavedWeight = initInventoryTotals.cfs != $scope.total.total_cf;

		if ((isTotalWeightChanged || isNotSavedWeight) && !destroyCount && isShowSaveSweetAlert) {
			SweetAlert.swal('', 'Please save changes!', 'info');
		}
	}

	function onMouseEnterInventoryModal() {
		showPleaseSaveChangesModalWithDebounce.cancel();
	}

	function setInventoryItemsPermissions() {
		let isClientOnAccount = $scope.isClient && $scope.isAccount && !$scope.isContractPage;
		let isDefaultOnBoard = !$scope.isAccount && ($scope.pageInventory == 0);
		let isAdminOnContract = $scope.isAdmin && $scope.isAccount && $scope.isContractPage;
		let isAdditionalInventory = $scope.pageInventory > 0;
		let isMaxPage = $scope.pageInventory > 0 && ($scope.countPages - 1 == $scope.pageInventory);

		$scope.inventoryItemsPermissions.allowChange = isClientOnAccount || isDefaultOnBoard || isAdminOnContract && isAdditionalInventory && isMaxPage;
	}

	function setCustomRoomPermissions() {
		let notContractTypeInventory = $scope.pageInventory == 0 ? 0 : [1, 2];
		let typeInventory = $scope.isContractPage ? $scope.pageInventory : notContractTypeInventory;
		let isAdditional = typeInventory != 0;
		$scope.allowCreatingCustomRooms = !$scope.isAccount || $scope.isClient && !$scope.isContractPage || $scope.isAdmin && isAdditional && $scope.isContractPage;
		$scope.customRoomPermissions.allowRemove = !$scope.isAccount || $scope.isAdmin && isAdditional && $scope.isContractPage;
		$scope.customRoomPermissions.allowRename = !$scope.isAccount || $scope.isClient && !$scope.isContractPage || $scope.isAdmin && isAdditional && $scope.isContractPage;
	}

	function loadRoomsAndInventory() {
		let notContractTypeInventory = $scope.pageInventory == 0 ? 0 : [1, 2];
		let typeInventory = $scope.isContractPage ? $scope.pageInventory : notContractTypeInventory;
		let data = {
			conditions: {
				entity_id: $scope.request ? $scope.request.nid : null, entity_type: 0, type_inventory: typeInventory
			}
		};

		apiService.postData(moveBoardApi.inventory.room.rooms, data).then(res => {
			if (res.data.status_code == 200) {
				makeEmptyRooms(res.data.data);
				$scope.rooms = res.data.data;
				separateRoomsInTwoParts();
				$scope.tableArguments.rooms = $scope.rooms;
				$scope.busy = false;
				updateMyInventory().then(() => {
					if ($scope.total.count > 0) {
						$scope.openMyInventory();
					} else {
						$scope.openRoom($scope.rooms[0]);
					}
				});
			} else {
				tellAboutError('Cannot load inventory rooms');
			}
		}, rej => {
			tellAboutError('Cannot connect to server');
		});
	}

	function makeEmptyRooms(rooms) {
		rooms.forEach(room => {
			makeEmptyRoom(room);
		});
	}

	function makeEmptyRoom(room) {
		let totalCountInFilters = 0;
		if (room.filters) {
			room.filters.forEach(filter => {
				totalCountInFilters += +filter.total_count || 0;
				return filter.items = [];
			});
		} else {
			room.filters = [];
		}
		room.filters.push({
			filter_id: '-1',
			name: 'Room Inventory',
			type: '0',
			weight: '999',
			room: room,
			items: [],
			total_count: room.total_count ? room.total_count - totalCountInFilters : 0
		});
		room.custom_items = [];
		room.items = [];
		room.total_count = room.total_count || 0;
		room.total_cf = room.total_cf || 0;
		room.total_custom_count = room.total_custom_count || 0;
		room.total_custom_cf = room.total_custom_cf || 0;
		room.boxes = room.boxes || 0;
	}

	function separateRoomsInTwoParts() {

		$scope.bedRooms = getBedroomsFromRooms($scope.rooms);
		$scope.notBedrooms = getNotBedroomsFromRooms($scope.rooms);

		function getBedroomsFromRooms(rooms) {
			let onlyBedrooms = rooms.filter(room => {
				return room.room_id == ALL_ROOM_ID || room.room_id == DEFAULT_BEDROOM_ID || room.type == CUSTOM_BEDROOM_TYPE;
			});
			return onlyBedrooms;
		}

		function getNotBedroomsFromRooms(rooms) {
			let notBedrooms = rooms.filter(room => {
				return !(room.room_id == ALL_ROOM_ID || room.room_id == DEFAULT_BEDROOM_ID || room.type == CUSTOM_BEDROOM_TYPE);
			});
			return sortRoomByWeight(notBedrooms);
		}
	}

	function sortRoomByWeight (rooms) {
		const SORTING = room => +room.weight;

		return _.sortBy(rooms, [SORTING]);
	}

	function deleteCustomRoom(room, callback) {
		let isCustomRoom = room.type == 1;
		let isCustomBedroom = room.type == 2;
		if (isCustomRoom || isCustomBedroom) {
			sendDeleteRoomRequest(room);
		}

		function sendDeleteRoomRequest(room) {
			let data = {
				entity_id: $scope.request.nid, room_id: room.room_id
			};

			apiService.postData(moveBoardApi.inventory.request.deleteCustomRoom, data)
				.then(response => {
					let noError = response.data.status_code == 200;
					if (noError) {
						callback(true);
						if (room.opened) {
							if ($scope.total.count > 0) {
								$scope.getMyInventory();
							} else {
								$scope.openRoom($scope.rooms[0]);
							}
						}
						removeRoomFromRooms(room);
					} else {
						callback(false);
						tellAboutError('Please remove/move the items from this room before deleting');
					}
				}, error => {
					tellAboutError('Cannot connect to server');
					callback(false);
				});
		}

		function removeRoomFromRooms(room) {
			let roomNumber = _.findIndex($scope.rooms, roomSearch => {
				return roomSearch.room_id == room.room_id;
			});
			$scope.rooms.splice(roomNumber, 1);
			decreaseRoomsWeightsAfter(+room.weight);
			separateRoomsInTwoParts();
		}

		function decreaseRoomsWeightsAfter(weight) {
			_.forEach($scope.rooms, (room) => {
				room.weight = +room.weight;
				if (room.weight >= weight) {
					room.weight = room.weight - 1;
				}
			});
		}
	}

	function disableCreatingButton(roomType) {
		if (roomType == 1) {
			$scope.creatingCustomRoom = true;
		} else if (roomType == 2) {
			$scope.creatingCustomBedroom = true;
		}
	}

	function enableCreatingButton(roomType) {
		if (roomType == 1) {
			$scope.creatingCustomRoom = false;
		} else if (roomType == 2) {
			$scope.creatingCustomBedroom = false;
		}
	}

	function checkPendingProcess(roomType) {
		if (roomType == 1) {
			return $scope.creatingCustomRoom;
		} else if (roomType == 2) {
			return $scope.creatingCustomBedroom;
		}
	}

	function createCustomRoom(roomType) {
		roomType = +roomType;

		if (checkPendingProcess(roomType)) {
			return 0;
		}

		disableCreatingButton(roomType);
		let newRoomNames = ['Default Room', 'Custom Room', 'Bedroom'];

		showSweetAlert({
			title: `Are you sure you want to add a new ${newRoomNames[roomType]}?`,
			text: '',
			type: 'info',
			showCancelButton: true,
			confirmButtonColor: '#64C564',
			confirmButtonText: 'Confirm',
			closeOnConfirm: true
		}, continueCreatingRoom);

		function continueCreatingRoom(isConfirm) {
			if (!isConfirm) {
				enableCreatingButton(roomType);
				return;
			}

			let currentRoomNumber = countRoomsWithSameType(roomType) + 1;
			let newRoomNames = ['Default Room', 'Custom Room', 'Custom Bedroom'];
			let newRoomName = `${newRoomNames[roomType]} ${currentRoomNumber}`;

			sendCreateRoomRequest(roomType, newRoomName);

			function countRoomsWithSameType(roomType) {
				let count = 0;
				_.forEach($scope.rooms, (room) => {
					if (room.type == roomType) {
						count++;
					}
				});
				return count;
			}

			function sendCreateRoomRequest(roomType, roomName) {

				let data = {
					conditions: {
						type: roomType, name: roomName
					}, entity_id: $scope.request.nid
				};

				apiService.postData(moveBoardApi.inventory.request.createCustomRoom, data)
					.then(response => {
						let noError = response.data.status_code == 200;
						if (noError) {
							let newRoom = response.data.data;
							makeEmptyRoom(newRoom);
							insertNewRoomInRooms(newRoom);
							$scope.openRoom(newRoom);
						} else {
							tellAboutError('Cannot create custom room');
						}
						enableCreatingButton(roomType);
					}, (error) => {
						tellAboutError('Cannot connect to server');
						enableCreatingButton(roomType);
					});
			}

			function insertNewRoomInRooms(newRoom) {
				newRoom.weight = +newRoom.weight;
				increaseRoomsWeightsAfter(newRoom.weight);
				$scope.rooms.splice(newRoom.weight - 1, 0, newRoom);
				separateRoomsInTwoParts();

				function increaseRoomsWeightsAfter(insertedWeight) {
					_.forEach($scope.rooms, (room) => {
						room.weight = +room.weight;
						if (room.weight >= insertedWeight) {
							room.weight++;
						}
					});
				}
			}
		}
	}

	function tellAboutError(error) {
		isShowSaveSweetAlert = false;
		SweetAlert.swal('Error', error, 'error');
		isShowSaveSweetAlert = true;
	}

	function updateAfterDrop() {
		if ($scope.myInventory.opened) {
			updateMyInventory().then(() => {
				$scope.tableArguments.blockTable = false;
				openMyInventory();
			});
		} else {
			$scope.tableArguments.blockTable = false;
		}
	}

	function getAdditionalInventoryField() {
		let result;

		if ($scope.typeInventory == 'inventoryMoving') {
			result = 'inventory';
		}

		if ($scope.typeInventory == 'addInventoryMoving') {
			result = 'additional_inventory';
		}

		if ($scope.typeInventory == 'secondAddInventoryMoving') {
			result = 'second_additional_inventory';
		}

		return result;
	}

	function openRoom(room, event) {
		let clickedEnter = event && event.type == 'keypress' && event.keyCode == 13;

		if (room.opened || clickedEnter) {
			return;
		}

		$scope.activeRoom = room;
		$scope.searchResult = [];
		$scope.myInventory.opened = false;

		$scope.rooms.forEach(item => {
			item.opened = false;
		});

		if (!_.isEmpty(room.filters) && room.filters[0]) {
			setRoomItems(room);
		} else {
			$scope.setCustomItemMode();
		}

		room.opened = true;

		if (room.filters && room.filters.length) {
			if ($scope.isMobile) return;
			openMobileMenu();
		}
	}

	function setRoomItems(room) {
		if (_.isEmpty(room.items)) {
			$scope.setFilter(room.filters[0]);
		} else {
			$scope.inventoryItems = room.items;
			room.filters[0].active = false;
		}
	}

	function close() {
		if ($scope.isMobile) {
			$scope.mobileSavingInventory = true;
		}

		if (!$scope.isHomeEstimator) {
			$scope.fullscreenMode = false;
		}

		onDestroy()
			.then(res => {
				if ($scope.closingFunction) {
					$scope.closingFunction(false, $scope.listInventories);
				}
			});
	}

	function search() {
		$timeout.cancel(updateItemTimeOut);

		updateItemTimeOut = $timeout(() => {
			sendSearchRequest();
		}, SEND_SEARCH_REQUEST__TIMEOUT);

	}

	function sendSearchRequest() {
		if ($scope.searchText.length >= 2) {
			$scope.busy = true;
			$scope.customMode = false;
			$scope.inventoryItems = [];
			let roomId = _.get($scope, 'activeRoom.room_id', null);
			let searchData = {
				'name': $scope.searchText, 'conditions': {
					'entity_id': $scope.request.nid,
					'entity_type': '0',
					'type_inventory': $scope.pageInventory,
					'room_id': roomId
				}
			};

			apiService.postData(moveBoardApi.inventory.request.search, searchData).then(res => {
				if (res.data.status_code == 200) {
					$scope.busy = false;

					if (_.isEmpty(res.data.data)) {
						if (!roomId) {
							let allRoom = _.find($scope.rooms, room => +room.room_id === ALL_ROOM_ID);
							$scope.openRoom(allRoom);
						}

						$scope.setCustomItemMode();
					} else {
						$scope.inventoryItems = parseSearchResponse(res.data.data);
					}
				} else {
					tellAboutError('Search doesn\'t works');
				}
			}, rej => {
				tellAboutError('Cannot connect to server');
			});
		} else if (!$scope.myInventory.opened && $scope.activeFilter) {
			setFilter($scope.activeFilter);
		} else if ($scope.myInventory.opened) {
			$scope.inventoryItems = $scope.myInventory.items;
		}
	}

	function parseSearchResponse(response) {
		var result = [];
		response = response.sort(sortByRoomFunction);

		if ($scope.myInventory.opened) {

			response.forEach((item) => {
				let roomId = item.room_id;
				let room = _.find($scope.rooms, {room_id: roomId}) || {};

				item = findItemInRoom(item, room);
				item.isSearch = true;
				setItemDefaults(item);
				result.push(item);
			});

			return result;

		} else {

			let fromCurrentRoom = response
				.filter(item => {
					return item.room_id == $scope.activeRoom.room_id;
				})
				.map(item => {
					item = findItemInRoom(item, $scope.activeRoom);
					item.isSearch = true;
					setItemDefaults(item);
					return item;
				});

			let fromOtherRooms = response
				.filter(item => {
					let notInActiveRoom = _.findIndex(fromCurrentRoom, {item_id: item.item_id}) == -1;
					return item.room_id != $scope.activeRoom.room_id && notInActiveRoom;
				})
				.map(item => {
					item = makeItemForRoom(item, $scope.activeRoom);
					item.isSearch = true;
					setItemDefaults(item);
					return item;
				});


			fromCurrentRoom = [getLabelItem($scope.activeRoom.name)].concat(fromCurrentRoom);
			fromOtherRooms = [getLabelItem('In other rooms')].concat(fromOtherRooms);

			if (fromCurrentRoom.length > 1) {
				result = fromCurrentRoom.slice();
			}
			if (fromOtherRooms.length > 1) {
				result = result.concat(fromOtherRooms);
			}
			if (result.length == 0) {
				result.push(getLabelItem('Items not found'));
			}

			return result;
		}
	}

	function makeItemForRoom(item, room) {
		item.room = room;
		item.room_id = room.room_id;
		item.filter = _.find(room.filters, {filter_id: '-1'});
		item.filter_id = '-1';
		delete item.id;
		item.moved = 1;
		item.count = 0;
		item.total_cf = 0;
		return item;
	}

	function selectFilter(filter) {
		openMobileMenu();
		setFilter(filter);
	}

	function setFilter(filter) {
		$scope.rooms.forEach(room => {
			if (room.filters) {
				room.filters.forEach(item => item.active = false);
			}
		});

		$scope.customMode = false;
		filter.active = true;
		$scope.activeFilter = filter;

		$scope.busy = true;
		$scope.searchResult = [];
		$scope.inventoryItems = [];

		if ($scope.activeFilter.loaded) {
			$scope.inventoryItems = $scope.activeFilter.items;
			$scope.busy = false;
			return true;
		}
		if (+filter.filter_id >= 0) {
			apiService
				.postData(moveBoardApi.inventory.item.getItems, makeGetItemsData(filter))
				.then(res => {
					if (res.data.status_code == 200) {
						if ($scope.activeFilter === filter) {
							parseFilterResult(res.data);

							if ($scope.menuMode == 0) {
								$scope.inventoryItems = $scope.activeFilter.items;
							}

							$scope.activeFilter.loaded = true;
							calcMyInventory();
						}
					} else {
						tellAboutError('Cannot get items in filter');
					}
					$scope.busy = false;
				}, () => {
					tellAboutError('Cannot connect to server');
					$scope.busy = false;
				});
		} else {
			$scope.activeFilter.items = filter.room.items ? filter.room.items.filter(item => {
				if (item.moved) {
					item.filter = filter;
					return true;
				}
				return false;
			}) : [];

			if ($scope.menuMode == 0) {
				$scope.inventoryItems = $scope.activeFilter.items;
			}
			$scope.busy = false;
			calcMyInventory();
		}
	}

	function makeGetItemsData(filter) {
		let result = {
			conditions: {
				filter_id: $scope.menuMode == 0 ? filter.filter_id : undefined,
				room_id: $scope.activeRoom.room_id,
				entity_id: $scope.request && $scope.request.nid,
			}
		};

		if ($scope.isContractPage) {
			result.conditions.type_inventory = $scope.pageInventory;
		}

		if (!$scope.isContractPage) {
			result.conditions.type_inventory = $scope.pageInventory == 0 ? 0 : [1, 2];
		}

		return result;
	}

	function parseFilterResult(res) {
		$scope.busy = false;
		let filtersId = [];
		let withFilter = $scope.menuMode == 1;

		if (withFilter) {
			res.data = res.data.sort(sortByFilterFunction);
			$scope.activeFilter.items = [];
		}

		res.data.forEach(item => {
			if (withFilter) {
				let filterId = item.filters[0];
				let filter = _.find($scope.activeRoom.filters, {filter_id: filterId}) || {items: []};

				if (!_.find(filtersId, id => id == filterId) && filter.name) {
					$scope.inventoryItems.push(getLabelItem(filter.name));
					filtersId.push(filterId);
				}

				let alreadyInFilter = _.find(filter.items, {item_id: item.item_id});

				if (alreadyInFilter) {
					$scope.inventoryItems.push(alreadyInFilter);
				} else {
					$scope.inventoryItems.push(item);
					filter.items.push(item);
					setItemDefaults(item);
					item.filter = filter;
					item.room = $scope.activeRoom;
				}
			} else {
				let alreadyInFilter = _.find($scope.activeFilter.items, {item_id: item.item_id});
				if (alreadyInFilter) {

				} else {
					$scope.activeFilter.items.push(item);
					setItemDefaults(item);
					item.filter = $scope.activeFilter;
					item.room = $scope.activeRoom;
				}
			}
		});
	}

	function openMobileMenu() {
		$scope.showMobileMenu = !$scope.showMobileMenu;
	}

	function getMyInventory(isDestroy) {
		let defer = $q.defer();

		if (!$scope.rooms) {
			defer.reject();
		}

		openMobileMenu();

		$scope.busy = true;
		$scope.myInventory.opened = true;
		$scope.customMode = false;
		$scope.activeFilter = false;
		$scope.rooms.forEach(item => {
			item.opened = false;
		});

		$scope.inventoryItems = [];

		updateMyInventory(isDestroy).then(resolveUpdate => {
			$scope.inventoryItems = $scope.myInventory.items;
			$scope.busy = false;
			defer.resolve(resolveUpdate);
		}, rejectUpdate => {
			defer.reject();
		});

		return defer.promise;
	}

	function openMyInventory() {
		$scope.myInventory.opened = true;
		$scope.customMode = false;
		$scope.activeFilter = false;

		if ($scope.rooms) {
			$scope.rooms.forEach(item => {
				item.opened = false;
			});
		}
		
		$scope.inventoryItems = $scope.myInventory.items;
	}

	function updateMyInventory(isDestroy) {
		let defer = $q.defer();

		let data = {
			package_id: $scope.packageId, entity_id: $scope.request ? $scope.request.nid : null, entity_type: 0
		};

		if (!isDestroy) {
			let page = $scope.pageInventory == 0 ? 0 : [1, 2];
			data.conditions = {
				type_inventory: $scope.isContractPage ? $scope.pageInventory : page
			};
		}

		apiService.postData(moveBoardApi.inventory.item.getMyInventory, data).then(res => {
			if (res.data.status_code == 200) {
				$scope.myInventory.items = parseGetMyInventory(angular.copy(res.data.data));
				calcMyInventory();

				if (_.isUndefined(savedInventoryItems)) {
					savedInventoryItems = angular.copy(res.data.data);
				}

				defer.resolve(res.data.data);
			} else {
				tellAboutError('Cannot get my inventory');
				defer.reject();
			}
		}, rej => {
			tellAboutError('Cannot get my inventory');
			defer.reject();
		});

		return defer.promise;
	}

	function parseGetMyInventory(response) {
		let result = [];
		let roomIds = [];

		$scope.busy = false;

		response = response.sort(sortByRoomFunction);

		response.forEach((item) => {
			let roomId = item.room_id;
			let room = _.find($scope.rooms, {room_id: item.room_id}) || {};

			if (!_.find(roomIds, id => id == roomId) && room.name) {
				result.push(getLabelItem(room.name));
				roomIds.push(roomId);
			}

			room.items = room.items || [];
			room.custom_items = room.custom_items || [];

			item = findItemInRoom(item, room);

			item.isSearch = false;

			setItemDefaults(item);

			result.push(item);
		});

		return result;
	}

	function sortByRoomFunction(a, b) {
		return +a.room_id - +b.room_id;
	}

	function sortByFilterFunction(a, b) {
		return +a.filters[0] - +b.filters[0];
	}

	function findItemInRoom(item, room) {
		let id = item.id;
		let result = angular.copy(item);

		if (item.moved || item.filter_id === null) {
			item.moved = 1;
			item.filter = _.find(room.filters, {filter_id: '-1'});
			item.filter_id = '-1';
		}
		if (item.type == 1) {
			let currentItem = _.find(room.custom_items, {item_id: item.item_id});

			if (!currentItem) {
				room.custom_items = room.custom_items || [];
				room.custom_items.push(result);
			} else {
				result = currentItem;
			}
		} else {
			let filter = _.find(room.filters, {filter_id: item.filter_id}) || {};
			filter.items = filter.items || [];

			let searchData = {item_id: item.item_id};

			if (id) {
				searchData.id = id;
			}

			let currentItem = _.find(filter.items, searchData);

			result.filter = filter;

			if (!currentItem) {
				filter.items.push(result);
			} else {
				result = currentItem;

			}
		}
		checkDefaults(result);
		writeItemToRoomItems(result, room);

		return result;

		function checkDefaults(item) {
			item.count = item.count || 0;
			item.id = id || result.id;
			item.room = room;
		}
	}

	function setCustomItemMode() {
		_.set($scope, 'newItem.title.value', $scope.searchText);
		$scope.searchText = '';

		openMobileMenu();
		$scope.busy = true;
		$scope.customMode = true;
		$scope.activeFilter = false;
		$scope.inventoryItems = [];

		$scope.rooms.forEach(room => {
			if (room.filters) {
				room.filters.forEach(item => item.active = false);
			}
		});

		let data = {
			entity_id: $scope.request.nid, entity_type: 0, conditions: {
				room_id: $scope.activeRoom.room_id, type_inventory: $scope.pageInventory
			}
		};

		apiService.postData(moveBoardApi.inventory.request.getCustomItems, data).then(res => {
			$scope.busy = false;
			$scope.activeRoom.custom_items = res.data.data.map(item => {
				let alreadyInItems = _.find($scope.activeRoom.items, {id: item.id});
				if (alreadyInItems) {
					return alreadyInItems;
				}
				item.item_id = item.id;
				item.packageId = $scope.packageId;
				setItemDefaults(item);
				item.image_link = CUSTOM_ITEM_IMAGE_LINK;
				item.extra_service = '0';
				item.visible = true;
				item.room = $scope.activeRoom;
				return item;
			});

			$scope.inventoryItems = $scope.activeRoom.custom_items;
		});
	}

	function createCustomItem(customItem) {
		let data = {
			room_id: customItem.room.room_id,
			name: customItem.name,
			count: customItem.count,
			cf: customItem.cf,
			price: 0,
			entity_id: $scope.request.nid,
			entity_type: 0,
			type: 1,
			image_link: customItem.image_link,
			type_inventory: $scope.pageInventory
		};

		customItem.isCannotEdit = true;

		let createCustomItemPromise = apiService.postData(moveBoardApi.inventory.item.create, {data});
		$scope.activeRequests.push(createCustomItemPromise);
		createCustomItemPromise
			.then(res => {
				if (res.data.status_code == 200) {
					customItem.item_id = res.data.data.item_id;
					customItem.id = res.data.data.id;
					writeItemToRoomItems(customItem, customItem.room);
				} else {
					tellAboutError('Cannot create custom item');
				}

				customItem.isCannotEdit = false;
			});
	}

	function addCustomItem(form) {
		if (form.$valid) {
			isTotalWeightChanged = true;
			let item = makeNewCustomItem();
			if (+item.cf > LARGE_CUSTOM_ITEM_CF) {
				showSweetAlert({
					title: `This item is too big.\nDo you want to add it?`,
					text: '',
					type: 'info',
					showCancelButton: true,
					confirmButtonColor: '#64C564',
					confirmButtonText: 'Yes, I want to add the item',
					closeOnConfirm: true
				}, isConfirm => {
					if (isConfirm) {
						continueCreating(item);
					}
				});
			} else {
				continueCreating(item);
			}
		} else {
			form.$setDirty();
		}

		function continueCreating(item) {
			$scope.activeRoom.total_custom_count = Number($scope.activeRoom.total_custom_count || 0) + item.count;
			$scope.activeRoom.total_custom_cf = Number($scope.activeRoom.total_custom_cf || 0) + Number(item.total_cf);
			setItemDefaults(item);
			$scope.inventoryItems.unshift(item);
			calcMyInventory();
			$scope.activeRoom.custom_items = $scope.inventoryItems;

			createCustomItem(item);

			setDefaultNewCustomItem(form);

			angular.element('#customInventoryName').focus();
		}
	}

	function makeNewCustomItem() {
		let item = $scope.newItem;
		let cubicFeet = IS_ACCOUNT ? calculateCubicFeet(item) : item.pounds.value;

		return {
			cf: cubicFeet,
			extra_service: '0',
			handle_fee: '0',
			image_link: CUSTOM_ITEM_IMAGE_LINK,
			name: makeCustomItemName(item, cubicFeet),
			packing_fee: '0',
			price: '0',
			status: '0',
			type: '1',
			room: $scope.activeRoom,
			weight: '1',
			visible: false,
			count: +item.count.value,
			total_cf: +cubicFeet * +item.count.value
		};
	}

	function calculateCubicFeet(item) {
		let cubicInchesInCubicFoot = 1728;
		let result = Number(item.width.value) * Number(item.height.value) * Number(item.depth.value) / cubicInchesInCubicFoot;
		return Math.ceil(result);
	}

	function makeCustomItemName(item, cubicFeet) {
		let itemName = item.title.value;
		itemName += IS_ACCOUNT ? ` ${item.width.value}x${item.height.value}x${item.depth.value} ` : ` (${cubicFeet} c.f.)`;
		return itemName;
	}

	function setDefaultNewCustomItem(form) {
		$scope.newItem = {};
		form.$setUntouched();
		form.$setPristine(true);
	}

	function removeItem(item) {
		if (!($scope.activeFilter || item.isSearch)) {
			let index = _.findIndex($scope.inventoryItems, {id: item.id});

			if (index != -1) {
				$scope.inventoryItems.splice(index, 1);
				let previousElementIsLabel = isLabelItem(index - 1);
				let currentElement = $scope.inventoryItems[index];
				let currentElementIsLabel = isLabelItem(index);

				if (previousElementIsLabel && (!currentElement || currentElementIsLabel)) {
					$scope.inventoryItems.splice(index - 1, 1);
				}
			}

			if (item.filter && item.filter.items) {
				item.filter.loaded = false;
			}
		}

		if (item.type == 1) {
			item.room.total_custom_count = Number(item.room.total_custom_count) - Number(item.count);
			item.room.total_custom_cf = Number(item.room.total_custom_cf) - Number(item.total_cf);
		}

		removeItemFromRoomItems(item, item.room);
		delete item.id;
		calcMyInventory();
		$scope.tableArguments.blockTable = false;
	}

	function isLabelItem(index) {
		let item = $scope.inventoryItems[index];
		return item && _.isUndefined(item.type);
	}

	function changeInventoryPage(type) {
		$scope.busy = true;
		$scope.pageInventory = type;
		$element.find('.inventory__wraper-aside').css('display', type == 0 || $scope.isContractPage ? 'block' : 'none');

		setTablePermissions();
		setCustomRoomPermissions();
		setInventoryItemsPermissions();
		loadRoomsAndInventory();
	}

	function setTablePermissions() {
		let notContractTypeInventory = $scope.pageInventory == 0 ? 0 : [1, 2];
		let typeInventory = $scope.isContractPage ? $scope.pageInventory : notContractTypeInventory;

		let mainInventoryOnBoard = !$scope.isAccount && typeInventory == 0;
		let addInventoryOnContractPage = $scope.isContractPage && typeInventory != 0;
		$scope.tablePermissions.allowRemove = mainInventoryOnBoard || $scope.isAccount && (addInventoryOnContractPage);
		$scope.tablePermissions.allowDrag = mainInventoryOnBoard || $scope.isAccount && (addInventoryOnContractPage);
	}


	function calcMyInventory() {
		$scope.rooms.forEach(room => {
			room.count = Number(room.total_custom_count || 0) + Number(room.total_count || 0) || undefined;
		});

		let total = mainTotalCalc($scope.rooms);
		$scope.total.total_cf = total.total_cf;
		$scope.total.count = total.count;
		$scope.total.total_custom = total.total_custom;
		$scope.total.boxes = total.boxes;
		createCustomItems($scope.rooms);
	}

	function setItemDefaults(item) {
		item.isCannotEdit = !$scope.inventoryItemsPermissions.allowChange;
		item.image_link = item.type != 1 ? item.image_link : CUSTOM_ITEM_IMAGE_LINK;
		item.baseTotal = $scope.total;
		item.selectedTypeInventory = $scope.pageInventory;
		item.packageId = $scope.packageId;
	}

	function getLabelItem(name, index) {
		return {filter_id: 1, name: name, extra_service: '0'};
	}

	function calculatePrint() {
		$scope.printPages = splitInventoryByPrintPage($scope.rooms);
	}

	function splitInventoryByPrintPage(rooms) {
		let pages = [];
		let pagesClass = [];
		let pageIndex = 0;
		let itemCount = 0;
		let roomCount = 0;
		let itemOnPage = 110;

		function checkItemsAmountOnPage() {
			if (itemCount >= itemOnPage) {
				pagesClass[pageIndex] = getPrintPageColumns(itemCount);
				pageIndex++;
				itemCount = 0;
				roomCount = 0;
				let newRoom = {
					name: '', items: []
				};
				pages[pageIndex] = pages[pageIndex] || [];
				pages[pageIndex].push(newRoom);
			}
		}

		function addItemToPrintPage(item) {
			if (item.count > 0) {
				checkItemsAmountOnPage();
				itemCount++;
				pages[pageIndex][roomCount].items.push(item);
			}
		}

		rooms.forEach(room => {
			let currentRoom = {
				name: room.name, items: []
			};

			pages[pageIndex] = pages[pageIndex] || [];
			pages[pageIndex].push(currentRoom);

			if (room.items) {
				room.items.forEach(item => {
					addItemToPrintPage(item);
				});
			}
			roomCount++;
		});

		pagesClass[pageIndex] = pagesClass[pageIndex] ? pagesClass[pageIndex] : getPrintPageColumns(itemCount);

		return {
			pages: pages, pagesClass: pagesClass
		};
	}

	function getPrintPageColumns(items) {
		let printSize = calculatePrintColumnsSize(items);

		return `inventory-print__${printSize}-column-page`;
	}

	function calculatePrintColumnsSize(items) {
		let minItemOneColumn = 35;
		let maxItemTwoColumn = 70;
		let result = 'three';

		if (items < minItemOneColumn) {
			result = 'one';
		} else if (items >= minItemOneColumn && items < maxItemTwoColumn) {
			result = 'two';
		}

		return result;
	}

	function print() {
		new Promise((resolve, reject) => {
			$scope.showPrintInventory = true;
			$scope.calculatePrint();
			resolve();
		})
			.then(() => {
				const PRINT_HTML = $element.find('.inventory-print')[0].innerHTML;
				const PRINT_STYLES = require('!raw-loader!stylus-loader!./inventory-print-page.styl');
				printService.printThisLayout(PRINT_HTML, PRINT_STYLES);
			});
	}

	function afterPrint() {
		$scope.showPrintInventory = false;
		$scope.$apply();
	}

	if ($window.matchMedia) {
		let mediaQueryList = $window.matchMedia('print');
		mediaQueryList.addListener(function (mql) {
			if (!mql.matches) {
				afterPrint();
			}
		});
	}

	$window.onafterprint = afterPrint;

	function onDestroy() {
		$scope.busy = true;

		let destroyDefer = $q.defer();

		if (destroyCount >= 1) {
			destroyDefer.reject();
			return destroyDefer.promise;
		} else {
			destroyCount++;
		}

		let isNotSavedWeight = initInventoryTotals.cfs != $scope.total.total_cf;

		if (isTotalWeightChanged || isNotSavedWeight) {
			$timeout(() => {
				$q.all($scope.activeRequests)
					.then(res => {
						getInventoryAndSaveWeight()
							.finally(() => {
								destroyDefer.resolve();
							});

						if ($scope.isClient) {
							RequestServices.sendLogs([{
								simpleText: 'Client closed the Inventory Modal',
							}], 'Inventory', $scope.request.nid, 'MOVEREQUEST');
						}
					}, rej => {
						getInventoryAndSaveWeight()
							.finally(() => {
								destroyDefer.resolve();
							});

						if ($scope.isClient) {
							RequestServices.sendLogs([{
								simpleText: 'Client closed the Inventory Modal (With error)',
							}], 'Request was not updated', $scope.request.nid, 'MOVEREQUEST');
						}

						tellAboutError('Some items may be lost\n' + 'Please refresh the page to see them safety');
					});
			}, SAVE_INVENTORY_DELAY);
		} else {
			$rootScope.$broadcast('onInventorySaved');

			if ($scope.isClient) {
				RequestServices.sendLogs([{
					simpleText: 'Client closed the Inventory Modal (Inventory was not changed)',
				}], 'Request was not updated', $scope.request.nid, 'MOVEREQUEST');
			}

			destroyDefer.resolve();
		}

		return destroyDefer.promise;
	}

	function getInventoryAndSaveWeight() {
		let myInventory = getMyInventory(true);
		let isSaveExtraService = !$scope.isContractPage && $scope.request && inventoryExtraService.isChangeInventoryExtraService($scope.request.extraServices, oldExtraService);
		let savePayment = isSaveExtraService && additionalServicesFactory.saveExtraServices($scope.request.nid, $scope.request.extraServices);
		let getContract = $scope.request && inventoryExtraService.getRequestContract($scope.request.nid);
		let resultPromise = $q.all({myInventory, savePayment, getContract});

		if (isSaveExtraService) {
			let extraServiceLog = inventoryExtraService.makeExtraServiceLog($scope.request.extraServices, oldExtraService);
			RequestServices.sendLogs(extraServiceLog, 'Inventory Additional Services', $scope.request.nid, 'MOVEREQUEST');
		}

		resultPromise.then(response => {
			if ($scope.request) {
				let isNotSubmitInventory = {
					inventory: !_.get(response, 'getContract.data.factory.inventoryMoving.isSubmitted', false),
					additional_inventory: !_.get(response, 'getContract.data.factory.addInventoryMoving.isSubmitted', false),
					second_additional_inventory: !_.get(response, 'getContract.data.factory.secondAddInventoryMoving.isSubmitted', false),
				};

				let defaultInventory = response.myInventory.filter(item => item.type_inventory == 0);
				defaultInventory = converInventoryToOldVersion(defaultInventory);
				let listInventories = mergeOldAndNewInventories($scope.listInventories, defaultInventory);

				if (additionalInventoryField) {
					let pageInventory = getInventoryPageByType();
					let items = response.myInventory.filter(item => item.type_inventory == pageInventory);
					items = converInventoryToOldVersion(items);
					let oldItems = _.get($scope, `request.request_all_data.${additionalInventoryField}`, [])
						.filter(item => item.additional == pageInventory);

					listInventories = mergeOldAndNewInventories(oldItems, items);

					let saveRequestAllData = false;

					if (isNotSubmitInventory[additionalInventoryField]) {
						$scope.request.request_all_data[additionalInventoryField] = listInventories;
						$scope.request.request_all_data.all_new_inventory_list = converInventoryToOldVersion(response.myInventory);
						saveRequestAllData = true;
					}

					if ($scope.request.status.raw == 3 && $scope.isContractPage) {
						saveRequestAllData = true;
					}

					if (saveRequestAllData) {
						RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
					}
				}

				$scope.listInventories.length = 0;
				$scope.listInventories.push(...listInventories);

				if (isNotSubmitInventory.inventory || $scope.isContractPage) {
					$scope.saveListInventories(false, $scope.listInventories, $scope.request, savedInventoryItems);
				}
			}
		}, reject => {
			tellAboutError('Some items may be lost\n' + 'Please refresh the page to see them safety');
		});

		return resultPromise;
	}

	function getInventoryPageByType() {
		let result = 0;
		let type = $scope.typeInventory;

		if (type == 'addInventoryMoving' || type == 'inventoryMoving') {
			result = 1;
		}

		if (type == 'secondAddInventoryMoving') {
			result = 2;
		}

		return result;
	}

	function afterSavingItemCount(result) {
		if (result.success) {
			if (result.item.count !== 0) {
				writeItemToRoomItems(result.item, result.item.room);
			}
			updateRequestExtraService(result.item, $scope.rooms, $scope.request, $scope.isContractPage);
		} else {
			showSweetAlert({
				title: 'Error',
				text: 'Cannot save item.\nMy inventory will be opened.',
				type: 'error',
				showCancelButton: false,
				closeOnConfirm: true,
				closeOnClickOutside: false
			});
			loadRoomsAndInventory();
		}
	}

	function showSweetAlert(sweetObject, sweetCallBack) {
		isShowSaveSweetAlert = false;

		if (sweetCallBack) {
			SweetAlert.swal(sweetObject, (confirm) => {
				isShowSaveSweetAlert = true;
				sweetCallBack(confirm);
			});
		} else {
			SweetAlert.swal(sweetObject);
			isShowSaveSweetAlert = true;
		}
	}

	function changeItemCount(item, value, isManualUpdated = false) {
		value = +value;
		item.count = +item.count;
		item.cf = +item.cf;

		if (isManualUpdated) {
			value -= item.oldCount;
			item.count = +item.oldCount;
		}
		updateCounts(value, item);
	}

	function updateCounts(value, item) {
		$scope.tableArguments.blockTable = true;
		isTotalWeightChanged = true;

		item.count += value;
		item.total_cf += value * +item.cf;

		changeRoomTotals(item.room, item, value);

		if (item.filter) {
			changeFilterTotals(item.filter, item, value);
		}

		if ($scope.packageId) {
			synchronizeItem(item, item.room);
		}

		updateGlobalTotals(value, item);

		if (!item.count) {
			$scope.removeItem(item);
		}

		$scope.tableArguments.blockTable = false;
	}

	function updateGlobalTotals(value, item) {
		let isBox = item.type == 2;
		let isCustomItem = item.type == 1;

		let baseTotal = item.baseTotal;
		let diffCF = value * item.cf;

		baseTotal.total_cf += diffCF;
		baseTotal.count += value;

		if (isCustomItem) {
			baseTotal.total_custom += value;
		}

		if (isBox) {
			baseTotal.boxes += value;
		}
	}

	function synchronizeItem(item, room) {
		let currentItem = _.find(room.items, {item_id: item.item_id});

		if (currentItem) {
			currentItem.totals = item.totals;
		} else {
			room.items.push(item);
		}
	}

	function clearSearchText() {
		$scope.searchText = null;
		angular.element('.mobile-toolbar__search').focus();
	}

	$scope.$on('$destroy', onDestroy);

}
