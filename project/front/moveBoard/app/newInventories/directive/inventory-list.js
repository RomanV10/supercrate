'use strict';
import './inventory-list.styl'
import './aside/aside.styl'
(() => {

    angular
        .module('newInventories')
        .directive('listNewInventories', listNewInventories);

            listNewInventories.$inject = ['$q', '$rootScope', 'InventoriesServices', '$http', 'config', '$timeout', '$interval', '$document', 'RequestServices'];

            function listNewInventories($q, $rootScope, InventoriesServices, $http, config, $timeout, $interval, $document, RequestServices) {
            return {
                scope: {
                    'rooms': '=rooms',
                    'listInventories': '=listinventories',
                    'itemsLog': '=',
                    'saveListInventories': "=",
                    'calculateFilter': '=',
                    'saveInventoryOnDestroy': '='
                },
                template: require('./inventory-list.pug'),
                restrict: 'A',
                link: ($scope, element) => {

                    const defaults = {
                        el: '.js-aside',
                        dom: {
                            item: '.js-aside-wrap-desk',
                            nav: '.js-aside-nav',
                            items: '.js-aside-items',
                            inner: '.js-aside-inner'
                        },
                        classes: {
                            aside: {
                                state: {
                                    opened: 'aside__wrap-aside_state_opened'
                                }
                            }
                        }
                    };

                    $scope.filters = $scope.rooms || [];
                    $scope.isAccount = IS_ACCOUNT;
                    $scope.options = defaults;
                    $scope.$el = $($scope.options.el);
                    $scope.products = [];
                    $scope.invetories = [];
                    $scope.myInventory = {};
                    $scope.searchText = '';
                    $scope.inventoryLoaded = false;
                    $scope.busy = false;
                    $scope.newItem = {
                        title: {},
                        pounds: {},
                        width: {},
                        height: {},
                        depth: {}
                    };

                    if ($scope.saveInventoryOnDestroy) {
                        $scope.oldInventory = angular.copy($scope.listInventories);

                        $scope.$on('$destroy', () => {
                            if (angular.toJson($scope.oldInventory) != angular.toJson($scope.listInventories)) {
                                $scope.itemsLog = angular.copy($scope.listInventories);
                                $scope.oldInventory = angular.copy($scope.listInventories);
                                $scope.saveListInventories(true);
                            }
                        });
                    }


                    $scope.listInventories = $scope.listInventories.filter(item => item.count > 0);

                    $scope.resize = () => {
                        $scope.isMobile = document.body.clientWidth < 1024;
                    };

                    $scope.resize();

                    $(window).on('resize', e => {
                        $scope.resize();
                    });

                    _.forEach($scope.listInventories, (item) => {
                        item.oldCount = item.count;
                    });

                    $scope.checkCount = filter => {
                        const item = $scope.findById(filter.id, $scope.listInventories);
                        const result = item ? item.count : 0;
                        return result;
                    };


                    $scope.getDom = () => {
                        $scope.$nav = $scope.$el.find($scope.options.dom.nav);
                        $scope.$items = $($scope.options.dom.items);
                        $scope.$item = $($scope.options.dom.item);
                        $scope.opened = $scope.options.classes.aside.state.opened;
                    };

                    $scope.getIntitialHeight = () => {
                        $scope.$items.each((i, item) => {
                            const $item = $(item);
                            const height = $item.outerHeight(true);
                            $item.css('height', height);
                        });
                    };

                    $scope.changeMobileMenu = state => {
                        $scope.hideMobileMenu = state
                    };

                    $scope.search = (item, str) => {
                        var result = false;
                        item.finded = false;

                        if (str) {
                            if (item.title.toLowerCase().indexOf(str.toLowerCase()) > -1) {
                                result = true;
                                item.finded = true
                            }
                        }
                        return result;
                    };

                    $scope.$watch('searchText', (newV, oldV) => {
                        if (newV && newV.length) {
                            $scope.activeRoom = 15;
                            // $scope.filters.forEach(item => {
                            //  item.hasSearchItems = $scope.hasSearchItems(item, newV);
                            // })
                        }
                    });


                    $scope.onClick = (e, item) => {
                        if (item.opened) {
                            return;
                        }

                        $scope.searchText = '';

                        $scope.closeAllFilter();

                        $scope.myInventory.opened = false;

                        item.opened = !item.opened;

                        if ($scope.myInventory.opened) {
                            const $items = $($scope.options.dom.items);
                            $scope.filters.forEach(filter => {
                                filter.opened = false;
                            });

                            $items.each(index => {
                                $($items[index]).css('height', 0);
                            })
                        } else {

                            $scope.activeRoom = item.id;
                            $scope.setActiveItem(item.items[0]);
                        }
                        $timeout(() => {
                            const $target = $(e.target);
                            const $item = $target.closest($scope.$item);
                            const $inner = $item.find($scope.options.dom.inner);
                            const $content = $item.find($scope.$items);

                            if (item.opened) {
                                let height = 0;
                                $inner.each(function () {
                                    height += $(this).outerHeight(true);
                                });
                                $content.css('height', height);
                            } else {
                                $content.css('height', 0);
                            }

                        }, 10)
                    };


                    $scope.closeAllFilter = () => {
                        const $items = $($scope.options.dom.item);
                        $items.each((ind) => {
                            $scope.filters.forEach(fr => {
                                fr.opened = false;
                            });
                            const $target = $($items[ind]);
                            const $item = $target.closest($scope.$item);
                            const $inner = $item.find($scope.options.dom.inner);
                            const $content = $item.find($scope.$items);
                            $content.css('height', 0);
                        })
                    };

                    $scope.calculateTotals = calculateTotals;

                    function calculateTotals() {
                        var totals = {counts: 0, cfs: 0, boxes: 0, custom: 0};
                        angular.forEach($scope.listInventories, (element, index) => {
                            if (angular.isDefined(element.fid)) {
                                if (element.fid == 14) {
                                    totals.boxes += element.count;
                                }
                                if (element.id >= 10000) {
                                    totals.custom += element.count;
                                }
                                totals.counts += element.count;
                                totals.cfs = totals.cfs + element.cf * element.count;
                            }
                        });

                        return totals;
                    }

                    $scope.render = () => {
                        if ($scope.$el.length) {
                            $scope.getDom();
                            $scope.getIntitialHeight();
                        }
                    };

                    $scope.$watch('newItem.count.value', (newV, oldV) => {
                        if (newV) {
                            $scope.newItem.count.value = newV.replace(/[^-0-9]/gim, '');
                        }
                    });

                    $scope.$watch('newItem.pounds.value', (newV, oldV) => {
                        if (newV) {
                            $scope.newItem.pounds.value = newV.replace(/[^-0-9]/gim, '');
                        }
                    });

                    $scope.$watch('newItem.width.value', (newV, oldV) => {
                        if (newV) {
                            $scope.newItem.width.value = newV.replace(/[^-0-9]/gim, '');
                        }
                    });

                    $scope.$watch('newItem.height.value', (newV, oldV) => {
                        if (newV) {
                            $scope.newItem.height.value = newV.replace(/[^-0-9]/gim, '');
                        }
                    });

                    $scope.$watch('newItem.depth.value', (newV, oldV) => {
                        if (newV) {
                            $scope.newItem.depth.value = newV.replace(/[^-0-9]/gim, '');
                        }
                    });

                    $scope.$watch('listInventories', onUpdateListInvertories, true);

                    function onUpdateListInvertories() {
                        $scope.calcTotals = calculateTotals();
                    }

                    $timeout(() => {
                        $scope.render();
                    }, 100);

                    $scope.findById = (id, arr) => {
                        let result = -1;
                        arr.forEach(item => {
                            if (Number(item.id) == Number(id)) {
                                result = item;
                            }
                        });

                        return result;
                    };

                    $scope.getUniqId = id => {
                        let newId = $scope.findById(id, $scope.invetories);

                        if (newId != -1) {
                            Math.floor(Math.random() * (max - min + 1)) + min;
                            return $scope.getUniqId($scope.listInventories.length + 10000 + Math.floor(Math.random() * 100000))
                        } else {
                            return id
                        }

                    };

                    $scope.buildFilters = () => {
                        $scope.filters.forEach(item => {
                            item.customItem = {};
                            item.items = item.fids.map(fid => {
                                const result = $scope.findById(fid, $scope.allFilters);
                                if (result != -1) {
                                    const rs = angular.copy(result);
                                    rs.items = [];
                                    $scope.invetories.forEach(inv => {
                                        if (inv.fid == fid) {
                                            const resInv = angular.copy(inv);
                                            resInv.rid = item.id;
                                            rs.items.push(resInv);
                                        }
                                    });
                                    // rs.items = $scope.invetories.filter(item => {
                                    //  return item.fid == rs.fid
                                    // })

                                    rs.rid = item.id;
                                    return rs;
                                }
                            });

                            item.items.push({
                                id: item.id + 10000,
                                rid: item.id,
                                title: "Custom Item"
                            });

                            item.items[item.items.length - 1].items = [];

                            $scope.listInventories.forEach(lInv => {
                                if (lInv.id > 10000 && lInv.rid == item.id) {
                                    item.items[item.items.length - 1].items.push(lInv)
                                }
                            });

                            item.items[item.items.length - 1].items
                        });

                        $scope.listInventories.forEach(lInv => {
                            if (lInv.id > 10000) {
                                $scope.invetories.push(lInv)
                            }
                        })

                    };

                    $scope.deleteItem = item => {

                        $scope.listInventories = $scope.listInventories.filter(ls => {
                            return ls != item
                        });

                    };
                    $scope.calculateFilter = (list) => {

                        $scope.myInventory.count = 0;
                        $scope.filters.forEach(item => {
                            item.count = 0;
                            if (item.items) {
                                item.items.forEach(sItem => {
                                    if (sItem && sItem.count) {
                                        sItem.count = 0;
                                    }
                                })
                            }

                        });


                        list.forEach(item => {
                            const filter = $scope.findById(item.rid, $scope.filters);
                            if (filter != -1) {
                                filter.count += item.count;
                                $scope.myInventory.count += item.count;
                                if (filter.items) {
                                    filter.items.forEach((fItem) => {
                                        if (Number(fItem.id) == Number(item.fid)) {
                                            fItem.count = fItem.count ? Number(fItem.count) + Number(item.count) : Number(item.count);
                                            // var ind = _.findIndex($scope.itemsLog, { id: Number(item.id) });
                                            // if (ind >= 0)
                                            //     $scope.itemsLog[ind].count = item.count;
                                            // if (ind == -1 && $scope.itemsLog) {
                                            //     $scope.itemsLog.push(item);
                                            //     $scope.itemsLog[$scope.itemsLog.length - 1].count = item.count;
                                            //     $scope.itemsLog[$scope.itemsLog.length - 1].oldCount = 0;
                                            // }
                                        }
                                    });
                                }

                                // $scope.invetories.forEach((fItem) => {
                                //  if (Number(fItem.id) == Number(item.id)) {
                                //      fItem.count =  Number(item.count);
                                //  }
                                // })
                            }
                        });

                        if ($scope.searchText && $scope.searchText.length) {
                            var el = document.querySelector('.js-inventory-search');
                            el.focus();
                            el.select();
                        }

                    };

                    $scope.renderProduct = data => {
                        $scope.products = data
                    };

                    $scope.setActiveItem = (item, hideMobile) => {
                        if (hideMobile) {
                            $scope.changeMobileMenu(false);
                        }
                        $scope.searchText = '';
                        $scope.setUnactiveAll();
                        item.active = !item.active;
                        $scope.currentFilter = item;
                        $scope.activeRoom = item.rid;

                        if (Number(item.id) > 10000) {
                            $scope.customItemMode = true;
                        }

                    };


                    $scope.compileCustomItem = () => {

                        $scope.listInventories.forEach(item => {
                            if (item.id > 10000) {
                                $scope.invetories.push(item);
                            }
                        })
                    };

                    $scope.hasSearchItems = (room, str) => {
                        let result = 0;
                        room.items.forEach(filter => {
                            if (filter.items.join(',').indexOf(str) != -1) {
                                filter.items.forEach(item => {
                                    if ($scope.search(item, str)) {
                                        result += 1;
                                    }
                                })
                            }
                        });

                        return result;
                    };

                    $scope.addCustomItem = (form) => {

                        if (form.$valid) {

                            const cubicFeet = IS_ACCOUNT ? Number($scope.newItem.width.value) * Number($scope.newItem.height.value) * Number($scope.newItem.depth.value) : $scope.newItem.pounds.value;

                            let newItem = {
                                id: $scope.getUniqId($scope.listInventories.length + 10000 + Math.floor(Math.random() * 100000)),
                                count: Number($scope.newItem.count.value),
                                title: $scope.newItem.title.value + ' (' + cubicFeet + ' cuft)',
                                fid: $scope.currentFilter.id,
                                rid: $scope.activeRoom,
                                cf: cubicFeet,
                                photo: {
                                    name: $scope.newItem.title.value + ' (' + cubicFeet + ' cuft)',
                                    fid: $scope.currentFilter.id,
                                    path: "/moveBoard/content/img/Large_Box_0.jpg"
                                },
                                cubicFeet: cubicFeet
                            };


                            newItem = angular.copy(newItem);
                            $scope.listInventories.push(newItem);
                            $scope.invetories.push(newItem);
                            $scope.currentFilter.items = $scope.currentFilter.items ? $scope.currentFilter.items : [];
                            $scope.currentFilter.items.push(newItem);
                            $scope.calculateFilter($scope.listInventories);

                            $scope.newItem.title.value = '';
                            $scope.newItem.pounds.value = '';
                            $scope.newItem.count.value = '';
                            $scope.newItem.width.value = '';
                            $scope.newItem.height.value = '';
                            $scope.newItem.depth.value = '';

                            $('#customInventoryName').focus();


                        } else {
                            form.$setDirty();
                        }

                    };

                    $scope.deleteCustomItem = item => {
                        $scope.invetories = $scope.invetories.filter(inv => inv.id != item.id);
                        $scope.listInventories = $scope.listInventories.filter(inv => inv.id != item.id);

                        $scope.filters.forEach(filter => {
                            filter.items.forEach(mf => {
                                if (mf && mf.items) {
                                    mf.items = mf.items.filter(inv => inv.id != item.id)
                                }
                            })
                        });


                        $scope.calculateFilter($scope.listInventories);

                    };
                    $scope.setUnactiveAll = () => {

                        $scope.customItemMode = false;
                        $scope.filters.forEach(item => {
                            item.items.forEach(filter => {
                                if (filter) {
                                    filter.active = false;
                                }
                            });
                        });
                    };

                    $scope.getInventoryItem = id => {
                        let result = false;
                        $scope.listInventories.forEach(item => {
                            if (item.id == id) {
                                result = item;
                            }
                        });
                        if (!result) {
                            result = $scope.invetories.filter(item => {
                                return item.id == id
                            })[0]
                        }
                        return result;

                    };

                    InventoriesServices.getFilters().then(data => {
                        $scope.allFilters = data;
                        $scope.getAllInventories().then(() => {
                            $scope.buildFilters();
                            $scope.calculateFilter($scope.listInventories);

                            $timeout(() => {
                                $scope.closeAllFilter();

	                            $scope.inventoryLoaded = true;
	                            $scope.busy = true;
	                            $scope.hideMobileMenu = false;

                                if ($scope.myInventory.count > 0) {
                                    $scope.myInventory.opened = true;
                                    const $item = $($scope.options.dom.item).eq(0).find($scope.options.dom.nav);
                                    $item.trigger("click");
                                } else {
                                    $scope.allFilters[0].opened = true;
                                    $scope.currentFilter = $scope.allFilters[0];

                                    const $item = $($scope.options.dom.item).eq(1).find($scope.options.dom.nav);
                                    $item.trigger("click");
                                }
                            }, 50);

                            $(".js-custom-scrollbar").mCustomScrollbar(
                                {
                                    theme: "dark"
                                }
                            );
                        })

                    });

                    $scope.changeValue = (def, item) => {
                        item.count += def;
                        if (item.count <= 0) {
                            $scope.deleteCustomItem(item);
                        }
                    };

                    $scope.getAllInventories = () => {
                        return $q((resolve, reject) => {
                            InventoriesServices.getAllInventories().then(data => {
                                $scope.invetories = data;
                                $scope.compileCustomItem();
                                resolve();
                            })
                        })

                    };

                    $scope.selected = [];

                    $scope.query = {
                        order: 'name',
                        limit: 5,
                        page: 1
                    };

                    $scope.getDesserts = function () {
                    };
                }
            }
        };

})();
