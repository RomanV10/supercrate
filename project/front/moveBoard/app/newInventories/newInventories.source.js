require('ng-lazy-render');

require('./newInventory.module.js');
require('./directive/inventory-list.js');
require('./directive/newVersion/inventory.directive.js');
require('./directive/newVersion/inventory.controller.js');
require('./directive/newVersion/newInventoryItem/newInventory-item.controller');
require('./directive/newVersion/newInventoryItem/newInventoryItem.js');
require('./directive/inventory-items/inventory-items.js');
require('./directive/newVersion/inventory-table/inventory-table.controller');
require('./directive/newVersion/inventory-table/inventory-table.directive');
require('./directive/newVersion/custom-room-name/custom-room-name.directive.js');
require('./directive/newVersion/inventory.route.js');
require('./directive/newVersion/inventory-page/inventory-page.controller.js');
require('./directive/newVersion/service/inventory-service/new-inventories.service');
require('./directive/newVersion/service/inventory-extra-service/inventory-extra-service.service');
