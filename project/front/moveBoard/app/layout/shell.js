import './mobile-log-out.styl';

(function () {
	'use strict';

	angular
		.module('app')
		.controller('Shell', Shell);

	/* @ngInject */
	function Shell($rootScope, $state, $timeout, AUTH_EVENTS, datacontext, $q,
				   config, AuthenticationService, CalculatorServices, EditRequestServices, PermissionsServices,
				   TemplateBuilderService, $scope, erDeviceDetector, $window, SweetAlert, openRequestService) {

		var vm = this;
		var loadingComplete = false;
		var timer;
		var currentLogin = localStorage.getItem('currentLogin');

		vm.isDesktop = erDeviceDetector.isDesktop;
		vm.isMobile = erDeviceDetector.isMobile;

		vm.busyMessage = 'Please wait ...';
		vm.showMain = false;
		vm.showSplash = true;
		vm.needReload = true;
		vm.logoUrl = config.logoUrl;
		vm.progressLoading = 0;
		vm.PermissionsServices = PermissionsServices;
		vm.openMobileContextMenu = openMobileContextMenu;
		vm.mobileMenu = false;
		$rootScope.templateBuilder = {};
		vm.userRoleIs = {
			foreman: false,
			helper: false,
			home_estimator: false
		};

		vm.slidePanelOptions = {
			selector: 'reminder-calendar',
			panelOptions: {
				name: 'Reminders',
				components: ['notification-sound']
			}
		};

		window.addEventListener('storage', function (e) {
			if (e.key === 'currentLogin') {
				if (currentLogin != localStorage.getItem('currentLogin') && (vm.needReload)) {
					location.assign(location.protocol + '//' + location.host + location.pathname);
				} else {
					vm.needReload = true;
				}
			}
		});

		$rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
			runShell();
			vm.progressLoading = 0;
			vm.needLogin = false;
		});

		if (!vm.needLogin) {
			simulateLoading();
			runShell();
		}

		function openMobileContextMenu() {
			vm.mobileMenu = !vm.mobileMenu;
		}

		function runShell() {
			let retrieveFields = datacontext.retreaveFieldsData();
			let availableFlags = datacontext.getAvailableFlags();

			$q.all([retrieveFields, availableFlags])
				.then(function (data) {
					CalculatorServices.init(data[0]);
					//Store flags type in rootScope
					$rootScope.fieldData.flags = data[1];
					loadingComplete = true;
					vm.showSplash = false;
					vm.showMain = true;
					vm.busyMessage = 'Loading Requests ...';
					vm.progressLoading += 25;
					vm.needLogin = false;

					AuthenticationService
						.GetCurrent()
						.then(function (data) {
							var userRoles = [];
							let settings = datacontext.getFieldData().basicsettings;
							let isSuper = PermissionsServices.isSuper();
							if ($rootScope.currentUser) {
								userRoles = $rootScope.currentUser.userRole;
							} else {
								userRoles = data.userRole;
							}
							vm.userRoleIs.foreman = (userRoles.indexOf('foreman') > -1);
							vm.userRoleIs.helper = (userRoles.indexOf('helper') > -1);
							vm.userRoleIs.home_estimator = (userRoles.indexOf('home-estimator') > -1);

							if (AuthenticationService.isAuthenticated()) {
								// Load template folders only after success authorization user
								// Load template folders and emails templates store them in $rootScope
								TemplateBuilderService.initTemplateBuilder();
							}
							if(!isSuper && settings.is_disable_company){
								blockSite();
							}
						});

					$timeout(function () {
						var width = angular.element(window).width();
						if (width < 1024 && !vm.userRoleIs.home_estimator) {
							angular.element('#main-wrapper').toggleClass('sidebar-mini');
						}
					}, 1500);


				}, function (failed) {
					vm.showSplash = false;
					vm.needLogin = true;
					vm.showMain = true;
				});
		}


		function simulateLoading() {

			if (loadingComplete) {
				vm.progressLoading = 100;
				$timeout.cancel(timer);
			}

			timer = $timeout(function () {
				vm.progressLoading += 1;
				simulateLoading();
			}, 500);
		}

		vm.Logout = function () {
			vm.mobileMenu = false;
			vm.busyMessage = 'Logging Out ...';
			vm.showSplash = true;
			vm.progressLoading = 100;
			AuthenticationService.Logout()
				.then(function (data) {
					vm.needReload = false;
					localStorage.setItem('currentLogin', (new Date()).getTime());
					vm.showSplash = false;
					vm.needLogin = true;
					$rootScope.currentUser = null;

					$state.go('login');
					$rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
				});
		};

		$(document).mouseup(function (e) {
			if ($(e.target).hasClass('requestModal')) {
				EditRequestServices.setActiveModal();
			}
		});

		var TrashEvent = $rootScope.$on('request.added', handlerNewRequest);

		function handlerNewRequest(e, data) {
			if (!!data) {
				var requestsKeys = _.keys(data);

				var initial_request = {};
				if (!!requestsKeys.length) {
					var min = Math.min.apply(null, requestsKeys);
					initial_request = data[min];
				}

				$scope.busy = true;
				openRequestService.open(initial_request.nid)
					.finally(() => {
						$scope.busy = false;
					});
			}
		}

		$scope.$on('$destroy', function () {
			TrashEvent();
		});

		angular.element($window).on('click', e => {
			if (_.get(e, 'toElement.className')
				&& _.isString(e.toElement.className)
				&& !e.toElement.className.contains('mobile-login-logout-root-block')
				&& vm.mobileMenu) {
				vm.mobileMenu = false;
			}
		});

		function blockSite() {
			vm.showMain = false;
			SweetAlert.swal({
				title: "The System has been Blocked",
				text: "Please contact Elromco Sales Department to unblock it",
				type: "warning",
				showCancelButton: false,
				confirmButtonText: 'Logout',
				closeOnCancel: false
			},() => {
				vm.Logout();
				vm.showMain = true;
			});
		}
	}
})();
