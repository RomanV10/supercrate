(function () {
    'use strict';

    angular
        .module('move.requests')

        .factory('ForemanPaymentService', ForemanPaymentService);

    ForemanPaymentService.$inject = ['$http', '$q', 'config', '$uibModal', 'common', 'SweetAlert'];

    function ForemanPaymentService($http, $q, config, $uibModal, common, SweetAlert) {

        var service = {};
        service.getCustomPayroll = getCustomPayroll;
        service.createCustomPayroll = createCustomPayroll;
        service.updateCustomPayroll = updateCustomPayroll;
        service.removeCustomPayroll = removeCustomPayroll;
        service.openPaymentModal = openPaymentModal;
        service.openCustomReceipt = openCustomReceipt;

        return service;

        function getCustomPayroll(nid, uid, contractType) {
            var dataSend = {
                uid: uid,
                nid: nid,
                contract_type: contractType
            };
            var deferred = $q.defer();
            $http
                .post(config.serverUrl + 'server/payroll/get_custom_payroll', dataSend)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function createCustomPayroll(dataSend) {
            var deferred = $q.defer();
            $http
                .post(config.serverUrl + 'server/payroll/create_custom_payroll', dataSend)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function updateCustomPayroll(dataSend) {
            var deferred = $q.defer();
            $http
                .post(config.serverUrl + 'server/payroll/update_custom_payroll', dataSend)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function removeCustomPayroll(id) {
            var dataSend = {
                id: id
            };
            var deferred = $q.defer();
            $http
                .post(config.serverUrl + 'server/payroll/delete_custom_payroll', dataSend)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function openPaymentModal(nid, uid, contractType) {
            var modalInstance = $uibModal.open({
	            template: require('./foremanPaymentModal.html'),
                controller: ForemanPaymentModalCtrl,
                size: 'md',
                resolve: {
                    nid: function () {
                        return nid;
                    },
                    uid: function () {
                        return uid;
                    },
                    contractType: function () {
                        return contractType;
                    }
                }
            });
        }

        function ForemanPaymentModalCtrl($scope, $rootScope, $uibModalInstance, nid, uid, datacontext, contractType, AuthenticationService) {
            let fieldData = datacontext.getFieldData(),
                contractTypes = fieldData.enums.contract_type,
                basicsettings = angular.fromJson(fieldData.basicsettings);
            let userRole = _.get($rootScope, 'currentUser.userRole', []);
            $scope.admin = AuthenticationService.isAdmin(userRole);
            $scope.busy = false;
            $scope.step = {
                name: false,
                amount: true,
                photo: false
            };
            $scope.photo = {
                base64: ''
            };
            $scope.customPayroll = {
                uid: uid,
                nid: nid,
                amount: 0,
                name: '',
                photo: [],
                contract_type: contractType
            };
            $scope.types = basicsettings.foremanReceiptsTypes;

            $scope.removePhoto = function () {
                $scope.photo = {
                    base64: ''
                };
            };

            $scope.nextStep = function (step) {
                if (step == 'photo') {
                    if ($scope.customPayroll.name == '' || !$scope.customPayroll.name) {
                        SweetAlert.swal('Please enter the type', '', 'warning');
                        return false;
                    }
                }
                if (step == 'name') {
                    if ($scope.customPayroll.amount == 0 || !$scope.customPayroll.amount) {
                        SweetAlert.swal('Please enter the receipt amount', '', 'warning');
                        return false;
                    }
                }
                _.forEach($scope.step, function (item, key) {
                    $scope.step[key] = false;
                });
                $scope.step[step] = true;
            };

            $scope.Save = function () {
                if(!$scope.admin)
                    if ($scope.photo.base64 == '' || !$scope.photo.base64) {
                        SweetAlert.swal('Please choose the receipt photo', '', 'warning');
                        return false;
                    }

                if($scope.photo.base64)
                    $scope.customPayroll.photo.push($scope.photo.base64.compressed.dataURL);
                $scope.busy = true;
                service.createCustomPayroll($scope.customPayroll).then(function (data) {
                    let id = _.head(data);
                    $rootScope.$broadcast('custom.payroll', id, $scope.customPayroll);
                    $scope.busy = false;
					toastr.success('Receipt successfully created', 'Success');
					$scope.cancel();
                }, function (err) {
					$scope.busy = false;
					toastr.error(err, 'Error');
				});
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }

        function openCustomReceipt(usrName, receipt) {
            var modalInstance = $uibModal.open({
	            template: require('./customReceipt.html'),
                controller: CustomReceiptModalCtrl,
                size: 'lg',
                resolve: {
                    usrName: function () {
                        return usrName;
                    },
                    receipt: function () {
                        return receipt;
                    }
                }
            });
        }

        function CustomReceiptModalCtrl($scope, $uibModalInstance, usrName, receipt) {
            $scope.receipt = receipt;
            $scope.usrName = usrName;

            $scope.close = function () {
                $uibModalInstance.dismiss('cancel');
            }
        }
    }
})();
