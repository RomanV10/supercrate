import "./foreman-requests-table.styl";
import "./foreman-requests-table-responsive.styl";
(function () {
    'use strict';
    angular
        .module('app.foreman')
        .controller('ForemanController', ForemanCtrl);

    ForemanCtrl.$inject = ['$scope', '$rootScope', '$stateParams', '$window', '$timeout', 'SpecificUsersService',
		 'logger', 'ForemanPaymentService', 'datacontext', 'serviceTypeService'];

    function ForemanCtrl($scope, $rootScope, $stateParams, $window, $timeout, SpecificUsersService, logger,
								 ForemanPaymentService, datacontext, serviceTypeService) {

        let vm = this;
        let searchDellay;
        let mainContainer = angular.element("#main-wrapper");
        vm.getJobs = getJobs;
        vm.searchByNid = searchByNid;
        // Identify mobile device and close sideBar for mobile.
        vm.isIPad = document.documentElement.clientWidth < 895 && document.documentElement.clientWidth < document.documentElement.clientHeight;
        vm.isMobile = document.documentElement.clientWidth < 451 && document.documentElement.clientWidth < document.documentElement.clientHeight;
        // Copy timeout from shell.js with the added condition.
        if (vm.isIPad ) {
            $timeout(function () {
                let width = angular.element(window).width();
                if (width < 1024 && !angular.element('#main-wrapper').hasClass ('sidebar-mini')) {
                    angular.element('#main-wrapper').toggleClass('sidebar-mini');
                }
            }, 1500);
        }
        vm.busy = true;
	    let fieldData = datacontext.getFieldData(),
		    contractTypes = fieldData.enums.contract_type;
	
		 //TODO use filter from valuation here
	    vm.getCurrentServiceTypeName = request => serviceTypeService.getCurrentServiceTypeName(request);

	    let defaultSortingField = fieldData.contract_page.foremanMoveDateSorting ? "field_date_value" : "field_foreman_assign_time_value";

        vm.editReservation = editReservation;
        vm.addReceipt = addReceipt;

        vm.requests = [];
        vm.mobileData = [];
        vm.currentMobileData = [];
        vm.pageParams = {
	        uid: _.get($rootScope, 'currentUser.userId.uid'),
	        currentPage: 1,
	        perPage: 15,
	        type: $stateParams.page === 'new' ? 'new' : 'old',
	        conditions: {
	        	nid: ''
	        },
	        sorting:{
	        	orderKey: defaultSortingField,
		        orderValue: $stateParams.page === 'new' ? 'ASC' :'DESC'
	        }
        };

	    vm.getRequestsByPage = (page) => {
		    vm.pageParams.currentPage = page;
		    getJobs();
	    };

        function scrollToTop() {
            if ((vm.isMobile || vm.isIPad) && mainContainer.scrollTop() > 2000) {
               mainContainer.animate({
                    scrollTop: 0
               }, 600);
            }
        }
        function searchByNid() {
        	$timeout.cancel(searchDellay);
        	searchDellay = $timeout(function(){
		        getJobs();
	        }, 1000);
        }
        function getSortingPrams() {
        	let sorting;
	        switch (vm.pageParams.sorting.orderKey){
		        case 'field_moving_from_administrative_area':
			        sorting = {
				        field_moving_from_administrative_area: vm.pageParams.sorting.orderValue,
				        field_moving_from_locality: vm.pageParams.sorting.orderValue,
				        field_moving_from_postal_code: vm.pageParams.sorting.orderValue
			        };
			        break;
		        case 'field_moving_to_administrative_area':
			        sorting = {
				        field_moving_to_administrative_area: vm.pageParams.sorting.orderValue,
				        field_moving_to_locality: vm.pageParams.sorting.orderValue,
				        field_moving_to_postal_code: vm.pageParams.sorting.orderValue
			        };
			        break;
		        case 'field_first_name_value':
			        sorting = {
				        field_first_name_value: vm.pageParams.sorting.orderValue,
				        field_last_name_value: vm.pageParams.sorting.orderValue,
			        };
			        break;
		        default:
			        sorting = {
				        [vm.pageParams.sorting.orderKey]: vm.pageParams.sorting.orderValue
			        };
	        }

	        return sorting;
        }

        function getJobs() {
	        if ($rootScope.currentUser) {
		        scrollToTop();
		        vm.busy = true;
				let req = {
					uid: vm.pageParams.uid,
					currentPage: vm.pageParams.currentPage,
		            perPage: vm.pageParams.perPage,
			        type: vm.pageParams.type
				};
				req.sorting = getSortingPrams();
				req.conditions = vm.pageParams.conditions.nid ? vm.pageParams.conditions : undefined;
		        SpecificUsersService.getRequestsForForeman(req)
			        .then(function(requests){
				        if (!_.isEmpty(requests.data.items)) {
					        vm.requests = requests.data.items;
					        vm.mobileData = SpecificUsersService.sortReqIpad(vm.requests);
					        vm.pageParams.currentPage = requests.data.meta.currentPage;
					        vm.pageParams.totalCount = requests.data.meta.totalCount;
					        vm.pageParams.pageCount = requests.data.meta.pageCount;
				        } else {
					        vm.requests = [];
					        vm.pageParams.currentPage = 1;
					        vm.pageParams.totalCount = 0;
					        vm.pageParams.pageCount = 0;
				        }
			        }, function(reason){
				        logger.error(reason, reason, "Error");
			        })
			        .finally(function(){
				        vm.busy = false;
			        });
	        }
        }

        function editReservation(nid) {
            if (vm.clicked == nid || vm.isIPad) {
                $window.open('/account/#/request/'+nid+'/contract', '_self');
            } else {
                vm.clicked = nid;
            }
        }

        function addReceipt(nid, type) {
            var uid = _.get($rootScope, 'currentUser.userId.uid');
            let contractTypePayroll = contractTypes.PAYROLL;
            if (type == 'pickup') contractTypePayroll = contractTypes.PICKUP_PAYROLL;
            if (type == 'delivery') contractTypePayroll = contractTypes.DELIVERY_PAYROLL;
            ForemanPaymentService.openPaymentModal(nid, uid, contractTypePayroll);
        }

	    // Event in case the mobile device format was changed.
        $window.onresize = function () {
            vm.isIPad = document.documentElement.clientWidth < 895 && document.documentElement.clientWidth < document.documentElement.clientHeight;
            vm.isMobile = document.documentElement.clientWidth < 451 && document.documentElement.clientWidth < document.documentElement.clientHeight;
        };

        $scope.$on('$destroy', function(){
            $timeout.cancel(searchDellay);
            $window.onresize = null;
        });

	    getJobs();

    }
})();
