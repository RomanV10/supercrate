'use strict';
angular
	.module('app.foreman')
	.directive('mobileForeman', function () {
		return {
			replace: true,
			restrict: 'E',
			template: require('./mobile-jobs.html'),
		};
	});
