(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('foreman', {
                        url: '/foreman',
                        abstract: true,
                        template: '<ui-view/>'
                    })
                    .state('foreman.new', {
                        url: '/new-jobs',
	                    template: require('./jobs.html'),
                        title: 'New jobs',
                        params: {
                            page: 'new'
                        },
                        data: {
                            permissions: {
                                only: ['foreman']
                            }
                        }
                    })
                    .state('foreman.past', {
                        url: '/past-jobs',
	                    template: require('./jobs.html'),
                        title: 'past jobs',
                        params: {
                            page: 'past'
                        },
                        data: {
                            permissions: {
                                only: ['foreman']
                            }
                        }
                    });
            }
        ]
    );
})();
