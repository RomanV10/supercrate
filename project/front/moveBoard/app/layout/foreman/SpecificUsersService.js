(function () {
    'use strict';

    angular
        .module('app.foreman')
        .factory('SpecificUsersService', SpecificUsersService);

    SpecificUsersService.$inject = ['$q', '$http', 'config'];

    function SpecificUsersService ($q, $http, config)
    {
        return {
            getRequests: getRequests,
            getRequestsForForeman: getRequestsForForeman,
            getRequestsByFlag: getRequestsByFlag,
            sortReqIpad:sortReqIpad

        };

        function getRequests(filter) {

            var deferred = $q.defer();
            var promise = $http({
                method: "post",
                url: config.serverUrl + 'server/move_request/get_foreman_helper_requests',
                data: filter,
                timeout: deferred.promise
            });
            promise.then(function(response) {
                deferred.resolve(response);
            }, function() {
                deferred.reject(response)
            });
            promise._httpTimeout = deferred;

            return deferred.promise;
        }

        function getRequestsForForeman(filter) {

            var deferred = $q.defer();
            var promise = $http({
                method: "post",
                url: config.serverUrl + '/server/move_request/get_foreman_request',
                data: filter,
                timeout: deferred.promise
            });
            promise.then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error)
            });
            promise._httpTimeout = deferred;

            return deferred.promise;
        }

	    function getRequestsByFlag(filter) {

		    var deferred = $q.defer();
		    var promise = $http({
			    method: "post",
			    url: config.serverUrl + 'server/move_request/search',
			    data: filter,
			    timeout: deferred.promise
		    });
		    promise.then(function(response) {
			    deferred.resolve(response);
		    }, function(error) {
			    deferred.reject(error)
		    });
		    promise._httpTimeout = deferred;

		    return deferred.promise;
	    }

        function sortReqIpad (arr){
            var res = [];
            var map = {};
            var mapCount = 0;
            for(var i = 0; i < arr.length;i++){
                if (typeof map[arr[i].date.value] != "number") {
                    map[arr[i].date.value] = mapCount;
                    res[map[arr[i].date.value]]={};
                    res[map[arr[i].date.value]].title = arr[i].date.value;
                    res[map[arr[i].date.value]].data = [];
                    mapCount++;
                }
                res[map[arr[i].date.value]].data.push(arr[i]);
            }
            return res;
        }

    }

})();
