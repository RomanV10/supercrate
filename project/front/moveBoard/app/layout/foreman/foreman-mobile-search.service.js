(function () {
    'use strict';
    angular
        .module('app.foreman')
        .filter('ForemanDataMobileFilter', ForemanDataMobileFilter);

    function ForemanDataMobileFilter() {
        return function (data) {
            if (!data) {
                return data
            }
            return data.sort(function compareNumeric(a, b) {
                if (!a || !b) {
                    return 0;
                }

                if (a.data[0].field_foreman_assign_time && b.data[0].field_foreman_assign_time) {
                    if (a.data[0].field_foreman_assign_time.value > b.data[0].field_foreman_assign_time.value) {
                        return -1;
                    }

                    if (a.data[0].field_foreman_assign_time.value < b.data[0].field_foreman_assign_time.value) {
                        return 1;
                    }
                }


                return -1;
            })
        }
    }
})();
