(function () {
	"use strict";
	angular
		.module('app.layout')
		.directive('ngMiddleClick', ngMiddleClick);

	ngMiddleClick.$inject = ['$parse', '$state', '$window'];

	function ngMiddleClick($parse, $state, $window) {
		return {
			link: ngMiddleClickLink,
			restrict: 'A'
		};

		function ngMiddleClickLink(scope, element, attrs) {
			var param = $parse(attrs.ngMiddleClick)();

			element.bind('mouseup', function (event) {
				if (event.which == 2) {
					event.preventDefault();
					event.stopPropagation();

					if (!event.target.attributes['ng-right-click-layer']) {
						if (param.route) {
							$window.open($state.href(param.route[0], param.route[1]), '_blank');
						} else if (param.href) {
							$window.open(param.href, '_blank');
						}
					}
				}

			});
		}
	}
})();