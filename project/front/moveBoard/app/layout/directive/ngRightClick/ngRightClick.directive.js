import './ngRightClick.styl';
(function () {
	angular
		.module('app.layout')
		.directive('ngRightClick', ngRightClick);

	ngRightClick.$inject = ['$uibModal'];

	function ngRightClick($uibModal) {
		return {
			transclude: true,
			template: "<ng-transclude></ng-transclude>",
			link: ngRightClickLink,
			restrict: 'A',
			scope: {ngRightClick: '='}
		};

		function ngRightClickLink(scope, element) {
			var position = {};

			element.bind('contextmenu', function (event) {
				event.preventDefault();
				event.stopPropagation();

				scope.$apply(function () {
					position.posX = event.clientX - 10;
					position.posY = event.clientY - 10;
					openModal();
				});
			});

			function openModal() {
				var searchModal = $uibModal.open({
					template: require('./ngRightClick.pug'),
					controller: 'ngRightClickController',
					size: 'sm',
					backdropClass: 'fullScreenLayer',
					windowClass: 'fullScreenLayer',
					resolve: {
						position: function () {
							return position;
						},
						ngRightClick: function () {
							return scope.ngRightClick;
						}
					}
				});

				searchModal.rendered.then(function () {
					angular.element('div.fullScreenLayer').bind('mouseup', function (event) {
						if (event.which == 2) {
							closeModal(event);
						}
					});
					angular.element('div.fullScreenLayer').bind('contextmenu', closeModal);

					function closeModal(event) {

						event.preventDefault();
						event.stopPropagation();
						event.stopImmediatePropagation();
						scope.$apply(function () {
							searchModal.close();
						});

					}
				});
			}
		}
	}
})();