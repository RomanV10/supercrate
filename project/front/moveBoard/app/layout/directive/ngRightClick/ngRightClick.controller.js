(function () {
	'use strict';

	angular
		.module('app.layout')
		.controller('ngRightClickController', ngRightClickController);

	ngRightClickController.$inject = ['$scope', '$uibModalInstance', '$state', '$parse', 'position', 'ngRightClick', '$window'];

	function ngRightClickController($scope, $uibModalInstance, $state, $parse, position, ngRightClick, $window) {
		$scope.posX = position.posX;
		$scope.posY = position.posY;
		$scope.ngRightClick = ngRightClick;

		$scope.onClick = function (item) {
			if (item.route) {
				$window.open($state.href(item.route[0], $parse(item.route[1])), item.target);
			} else if (item.href) {
				$window.open(item.href, item.target);
			}

			$uibModalInstance.close();
		};
	}
})();


