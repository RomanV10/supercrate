import './logo-modal.styl'
(function () {
    'use strict';

    angular.module('app')
           .directive('branchLogo', branchLogo);

    branchLogo.$inject = ['datacontext', '$uibModal', 'config', 'PermissionsServices'];

    function branchLogo(datacontext, $uibModal, config, PermissionsServices) {
        return {
            restrict: 'EA',
            template: require('./branchLogo.template.html'),
            scope: true,
            replace: true,
            link: linker
        };

        function linker($scope, element) {
            var fieldData = datacontext.getFieldData();

            if (!fieldData) return false;

            var currentBranchId = fieldData.current_branch || 0;
            var branches = fieldData.branches;
            var branch = undefined;

            if (currentBranchId) {
                branch = _.find(branches, {id: currentBranchId});
            }

            if (!_.isEmpty(branches) && PermissionsServices.isSuper() && !branch) {
                toastr.info("Please! Select current branch.");
            }

            if (branch && branch.logo) {
                $scope.branchLogoUrl = config.serverUrl + branch.logo;
            } else {
                $scope.branchLogoUrl = 'content/img/moveboardlogo.png';
            }

            if (branch && branch.name) {
                $scope.name = branch.name;
            }

            if (PermissionsServices.ifAdmin() || PermissionsServices.isSuper()) {
                element.bind('click', onClick);
            }


            function onClick() {
                if (branch) {
                    var logoModal = $uibModal.open({
	                    template: require('./uploadLogo.template.html'),
                        controller: UploadLogoCtrl,
                        backdrop: false,
                        resolve: {
                            currentBranch: function () {
                                return branch;
                            },
                        },
                        size: 'lg'
                    });

                    logoModal.result.then(function (result) {
                        $scope.branchLogoUrl = config.serverUrl +  result;
                    });
                }
            }

            UploadLogoCtrl.$inject = ['$scope', '$uibModalInstance', 'currentBranch', 'BranchAPIService', '$q', 'Blob', '$http'];

            function UploadLogoCtrl($scope, $uibModalInstance, currentBranch, BranchAPIService, $q, Blob, $http) {
                $scope.uploader = {};
                $scope.defoultLogo = "content/img/moveboardlogo.png";
                $scope.myCroppedImage = '';

                $scope.onImageAdded = function (file) {
                    convertPhotoToBaseSixtyFour(file.file)
                        .then(function (result) {
                            $scope.myCroppedImage = '';
                            $scope.photo = result;
                        })
                };

                $scope.uploadSelectedPhoto = function (photoUrl) {
                    convertPhotoToBaseSixtyFour(photoUrl)
                        .then(function (result) {
                            $scope.uploadPhoto(result);
                        })
                };

                $scope.uploadPhoto = function (file) {
                    if (file) {
                        $scope.uploadedBusy = true;

                        BranchAPIService
                            .uploadBranchLogo(file)
                            .then(function (response) {
                                $uibModalInstance.close(response);
                                toastr.success('Logo was update successfully');
                            }, function () {
                                toastr.error('Failed to save logo');
                            })
                            .finally(function () {
                                $scope.uploadedBusy = false;
                            });
                    } else {
                        logger.error('please select a photo');
                    }

                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                function convertPhotoToBaseSixtyFour(photoUrl) {
                    var deferred = $q.defer();

                    loadPhoto(photoUrl)
                        .then(function (photoUrl) {
                            var fileReader = new FileReader();

                            fileReader.onload = (function (file) {
                                return function (e) {
                                    deferred.resolve(e.target.result);
                                };
                            })(photoUrl);

                            fileReader.readAsDataURL(photoUrl);
                        });

                    return  deferred.promise;
                }

                function loadPhoto(photoUrl) {
                    var deferred = $q.defer();

                    if (!_.isObject(photoUrl)) {
                        $http
                            .get(photoUrl, {responseType: 'arraybuffer'})
                            .then((res) => {
                                photoUrl = new Blob([res.data], {type: "image/png"});
                                deferred.resolve(photoUrl);
                            },function (photoUrl) {
                                deferred.reject(photoUrl);
                            }
                        );
                    } else {
                        deferred.resolve(photoUrl);
                    }

                    return deferred.promise;


                }
            }
        }
    }

})();
