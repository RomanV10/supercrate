(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('Sidebar', Sidebar);

    Sidebar.$inject = [
        '$rootScope','$scope', '$state','routehelper','datacontext','MessagesServices', '$window', 'tplBuilderAuthService','PermissionsServices', 'SettingServices', 'HomeEstimateService', 'config'
    ];

    function Sidebar($rootScope,$scope, $state,routehelper,datacontext,MessagesServices, $window, tplBuilderAuthService,PermissionsServices, SettingServices, HomeEstimateService, config) {
        /*jshint validthis: true */
        var vm = this;

        vm.goToPage = goToPage;
        vm.isCurrent = isCurrent;
        vm.getHrefForContextmenu = getHrefForContextmenu;

        var ua = navigator.userAgent,
            M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

	    if (M[1]==='Firefox') {
            angular.element('.sidebar.sidebar-left').css({'position': 'fixed', 'z-index': '1000'});
            angular.element('header#header').css({'position': 'fixed', 'z-index': '1010'});
        }

        vm.routes = routehelper.getRoutes();
        vm.apiUrl = config.serverUrl;
        vm.PermissionsServices = PermissionsServices;
        vm.unreadCount = 0;
        vm.services_type = [];
        vm.statuses = [];
        vm.isAdmin = PermissionsServices.ifAdmin() || PermissionsServices.isSuper();
        vm.userRoleIs = {
            foreman: getUserRole('foreman'),
            helper: getUserRole('helper'),
            sales: getUserRole('sales'),
            customer_service: getUserRole('customer service'),
            manager: getUserRole('manager'),
            home_estimator: getUserRole('home-estimator'),
        };

        function getUserRole(role) {
            return _.get($rootScope, 'currentUser.userRole', []).indexOf(role) > -1;
		}

	    $rootScope.showMenu = true;

        MessagesServices.getUnreadCount()
            .then(function(data) {
                if(data == null) {
                    vm.unreadCount = 0;
                } else {
                     vm.unreadCount = data[0];
                }
         });

        vm.fieldData = datacontext.getFieldData();
        vm.basicsettings = angular.fromJson(vm.fieldData.basicsettings);

        if(angular.isDefined(vm.basicsettings.companyFlags)){
            vm.companyColorFlags = _.clone(vm.basicsettings.companyFlags);
        }

	    //TODO I don't find any uses for vm.services_type and statuses but this pull request in master and I cannot remove this functional
	    vm.services_type = vm.fieldData.field_lists.field_move_service_type;
	    vm.statuses = vm.fieldData.field_lists.field_approve;
	    
        vm.branches = vm.fieldData.branches;
        vm.isShowBranches = vm.branches.length >= 2;
        vm.tooltipHTML = 'app/layout/branchesTooltipTemplate.html';

        vm.homeEstimateCountOfScheduleWorks = 0;

        function getCountOfHomeEstimateRequests() {
            HomeEstimateService
                .getCountOfScheduleWorks()
                .then(onGetCountOfHomeEstimateWorks);
        }

        function onGetCountOfHomeEstimateWorks(response) {
            vm.homeEstimateCountOfScheduleWorks = response.data[0];
        }

        getCountOfHomeEstimateRequests();

        // SettingServices.getFlags().then(function(data){
        //     vm.companyColorFlags = data.flags;
        // })

        $scope.$on('flag added', function(event, data){
            vm.companyColorFlags = _.clone(data);
        });

        $scope.$on('messaged.readed', function() {
            vm.unreadCount--;
        });

        function goToPage(state, param) {
            if(!param) {
                $state.transitionTo(state);
            }
            $state.go(state, param);
        }

        function isCurrent(state) {
            if ($state.current.hasOwnProperty('title')) {
                return $state.current.title.toLowerCase() == state;
            }
        }

        function getHrefForContextmenu(route, params){
			return $state.href(route, params);
        }

	    function onShowMenu(event, data) {
		    $rootScope.showMenu = data;
	    }

	    $scope.$on('show_menu', onShowMenu);
     }

})();
