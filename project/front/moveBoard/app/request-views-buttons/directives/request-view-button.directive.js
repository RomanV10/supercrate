import CONTRACT from '../src/contract.png';
import REQUEST from '../src/user.png';

import './request-view-button.styl';

'use strict';

angular.module('app.request-views').directive('requestViewButton', requestViewButton);

requestViewButton.$inject = ['$window'];

function requestViewButton($window) {
	return {
		restrict: 'AE',
		template: require('./request-view-button.tpl.pug'),
		scope: {
			buttonText: '@',
			nid: '<',
			reservation: '@?'
		},
		link: requestViewButtonLink
	};
	
	function requestViewButtonLink($scope) {
		const WEB_SITE_URL = $window.location.origin;
		
		$scope.imgUrl = $scope.reservation ? CONTRACT : REQUEST;
		
		$scope.goTo = goTo;
		
		function goTo() {
			let url = `${WEB_SITE_URL}/account/#/request/${$scope.nid}`;
			if (!!$scope.reservation) {
				url += '/reservation';
			}
			
			$window.open(url, '_blank');
		}
	}
}