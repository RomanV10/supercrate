import CLONE from '../src/copy.png';
import STORAGE from '../src/warehouse.png';
import PACKING from '../src/package.png';
import MENU from '../src/more.png';

import {
	prepareRequest,
} from '../../requests/request-helper-methods/prepare-new-request';

import './request-multiple-button.styl';

'use strict';

angular.module('app.request-views').directive('requestMultipleButton', requestMultipleButton);

/*@ngInject*/
function requestMultipleButton(SweetAlert, RequestServices, CalculatorServices, newLogService, $rootScope, StorageService, requestSettingsHelperService, openRequestService) {
	return {
		restrict: 'AE',
		template: require('./request-multiple-button.tpl.pug'),
		scope: {
			request: '=',
			fieldData: '=',
			busy: '='
		},
		link: multipleBtnLink
	};

	function multipleBtnLink($scope) {
		$scope.menu = MENU;
		$scope.availableSettings = {
			1: {
				name: 'Create Clone',
				icon: CLONE,
			},
			2: {
				name: 'Create Storage Tenant',
				icon: STORAGE,
			},
		};
		$scope.isShowAvailable = false;

		$scope.showAvailable = showAvailable;
		$scope.doAction = doAction;

		if ($scope.request.service_type.raw != 8 && !$scope.request.toStorage) {
			$scope.availableSettings['3'] = {
				name: 'Create Packing Day',
				icon: PACKING,
			};
		}

		function showAvailable() {
			$scope.isShowAvailable = !$scope.isShowAvailable;
		}

		function doAction(ActionID) {
			switch(ActionID) {
			case 0:
				cloneRequest();
				break;
			case 1:
				createTenant();
				break;
			case 2:
				createPackingDay();
				break;
			}
		}

		function cloneRequest() {
			if ($scope.request) {
				SweetAlert.swal({
					title: 'Are you sure, you want to clone request ?',
					text: '',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55', confirmButtonText: 'Clone',
					cancelButtonText: 'Cancel',
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						$scope.busy = true;
						let editRequest = prepareRequest($scope.request);
						requestSettingsHelperService.сloneRequest({
							nid: $scope.request.nid
						})
							.then(response => {
								openRequest(response, editRequest);
							})
							.catch(() => {
								toastr.error('Something going wrong');
							})
							.finally(() => {
								$scope.busy = false;
							});
					}
				}
				);
			}
		}

		function createTenant() {
			if ($scope.request.request_all_data.storage_request_id) {
				SweetAlert.swal('Failed!', 'Storage have been created for this request', 'error');
				return;
			}

			if (!$scope.request.phone || $scope.request.phone == '') {
				SweetAlert.swal('Failed!', 'Please, add client phone number', 'error');
				return false;
			}
			let weight = CalculatorServices.getTotalWeight($scope.request).weight;
			if ($scope.request.service_type.raw == 2 || $scope.request.service_type.raw == 6) {
				SweetAlert.swal({
					title: 'Do you want to create Storage Request?',
					text: '',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Yes, create it!',
					closeOnConfirm: true
				},
				function (isConfirm) {
					if (isConfirm) {
						$scope.busy = true;
						if (!$scope.request.request_all_data.toStorage) {
							RequestServices.getRequestsByNid($scope.request.storage_id)
								.then(requestStorage => {
									if (!_.isEmpty(requestStorage.nodes)) {
										StorageService.createStorageReqObj($scope.request, $scope.request.date.value, requestStorage.nodes[0].date.value, weight, requestStorage.nodes[0], $scope.request.nid, 'move')
											.finally(() => { $scope.busy = false; });
									} else {
										StorageService.createStorageReqObj($scope.request, $scope.request.date.value, '', weight, '', $scope.request.nid, 'move')
											.finally(() => { $scope.busy = false; });
									}
								});
						} else {
							RequestServices.getRequestsByNid($scope.request.storage_id)
								.then(requestStorage => {
									if (!_.isEmpty(requestStorage.nodes)) {
										StorageService.createStorageReqObj(requestStorage.nodes[0], requestStorage.nodes[0].date.value, $scope.request.date.value, weight, $scope.request, $scope.request.nid, 'move')
											.finally(() => { $scope.busy = false; });
									} else {
										StorageService.createStorageReqObj($scope.request, $scope.request.date.value, '', weight, '', $scope.request.nid, 'move')
											.finally(() => { $scope.busy = false; });
									}
								});
						}
					}
				});
			} else {
				SweetAlert.swal({
					title: 'Do you want to create Storage Request?',
					text: '',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Yes, create it!',
					closeOnConfirm: true
				},
				function (isConfirm) {
					if (isConfirm) {
						$scope.busy = true;
						RequestServices.getRequestsByNid($scope.request.storage_id)
							.then(function (requestStorage) {
								if (!_.isEmpty(requestStorage.nodes)) {
									StorageService.createStorageReqObj(requestStorage.nodes[0], requestStorage.nodes[0].date.value, $scope.request.date.value, weight, requestStorage.nodes[0], $scope.request.nid, 'move')
										.finally(() => { $scope.busy = false; });
								} else {
									StorageService.createStorageReqObj($scope.request, $scope.request.date.value, '', weight, '', $scope.request.nid, 'move')
										.finally(() => { $scope.busy = false; });
								}
							});
					}
				});
			}
		}

		function createPackingDay() {
			let isPackingRequestNotExists = !_.get($scope.request, 'request_all_data.packing_request_id');

			if (isPackingRequestNotExists) {
				SweetAlert.swal({
					title: 'Are you sure?',
					text: 'Packing day request will be created',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Create',
					cancelButtonText: 'Cancel',
					closeOnConfirm: true,
					closeOnCancel: true
				}, function (confirm) {
					if (confirm) {
						$scope.busy = true;
						let packingDay = true;
						let packingDayRequest = prepareRequest($scope.request, packingDay);

						requestSettingsHelperService.createPackingDay({
							nid: $scope.request.nid
						})
							.then(function (response) {
								openRequest(response, packingDayRequest, true);
							})
							.finally(() => {
								$scope.busy = false;
							});
					}
				});
			} else {
				SweetAlert.swal({
					title: 'You already have Packing Day request',
					text: 'Do you want open request?',
					type: 'info',
					showCancelButton: true,
					confirmButtonColor: '#3498db',
					confirmButtonText: 'Open',
					cancelButtonText: 'Cancel',
					closeOnConfirm: true,
					closeOnCancel: true
				}, function (confirm) {
					if (confirm) {
						openRequestService.open($scope.request.request_all_data.packing_request_id)
							.finally(() => {
								$scope.busy = false;
							});
					}
				});
			}
		}

		function getMessage(text, to) {
			return {
				text: text,
				to: to,
				from: $scope.request.nid
			};
		}

		function openRequest(response, editData, isPackingDay = false) {
			$scope.busy = true;
			let REQUEST_ID = response;
			let parentRequestLogTitle = 'Clone Request';
			if (isPackingDay) {
				$scope.request.request_all_data.packing_request_id = REQUEST_ID;
				parentRequestLogTitle = 'Packing Day Request';
			}
			RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
			let text = editData.data.field_move_service_type == 8 ? 'Packing request was created' : 'Request was cloned';
			var msg = getMessage(text, response);
			var arr = [];
			arr.push(msg);
			let results = angular.copy(editData.data);
			let request = results;
			request.move_size = {raw: results.field_size_of_move};
			request.rooms = {
				raw: results.field_extra_furnished_rooms,
				value: results.field_extra_furnished_rooms
			};
			request.inventory = {inventory_list: $scope.request.inventory.inventory_list};
			request.inventory_weight = $scope.request.inventory_weight;
			request.custom_weight = {value: results.field_customweight};
			request.field_useweighttype = {value: results.field_useweighttype};
			_.set(request, 'commercial_extra_rooms.value', $scope.request.commercial_extra_rooms);

			let weight = CalculatorServices.getTotalWeight(request).weight;
			let arrLogs = newLogService.createLogs('Admin Creating Request', $scope.fieldData.field_lists, editData.data, weight);
			arrLogs.push(msg);
			RequestServices.sendLogs(arrLogs, `${parentRequestLogTitle} was created`, $scope.request.nid, 'MOVEREQUEST');
			RequestServices.sendLogs(arrLogs, 'Request was created', response, 'MOVEREQUEST');

			RequestServices.getRequestsByNid(response).then(function (data) {
				let newRequest = {};

				newRequest[REQUEST_ID] = _.head(data.nodes);
				$rootScope.$broadcast('request.added', newRequest);
			}).finally(() => { $scope.busy = false; });
		}
	}
}
