describe('Request View Button', () => {
	let window;
	beforeEach(inject((_$window_) => {
		window = _$window_;
	}));
	
	describe('Request:', () => {
		let $scope, element, isolateScope;
		beforeEach(inject((_$compile_, _$rootScope_) => {
			let $compile = _$compile_;
			let $rootScope = _$rootScope_;
			$scope = $rootScope.$new();
			element = angular.element('<request-view-button button-text="Request" nid="4400"></request-view-button>');
			$compile(element)($scope);
			$scope.$apply();
			isolateScope = element.isolateScope();
		}));
		it('Button text should be contain "Request"', () => {
			expect(isolateScope.buttonText).toBe('Request');
		});
		
		it('Button text should be not contain "Confirmation"', () => {
			expect(isolateScope.buttonText).not.toBe('Confirmation');
		});
		
		it('URL should be bot contain "/reservation"', () => {
			let val;
			spyOn( window, 'open' ).and.callFake( function(value) {
				val = value;
				return true;
			} );
			isolateScope.goTo();
			expect(val).not.toContain('/reservation');
		});
	});
	
	describe('Confirmation:', () => {
		let $scope, element, isolateScope;
		beforeEach(inject((_$compile_, _$rootScope_) => {
			let $compile = _$compile_;
			let $rootScope = _$rootScope_;
			$scope = $rootScope.$new();
			element = angular.element('<request-view-button button-text="Confirmation" nid="4400" reservation="true"></request-view-button>');
			$compile(element)($scope);
			$scope.$apply();
			isolateScope = element.isolateScope();
		}));
		
		it('Button text should be contain "Confirmation"', () => {
			expect(isolateScope.buttonText).toBe('Confirmation');
		});
		
		it('Button text should be not contain "Request"', () => {
			expect(isolateScope.buttonText).not.toBe('Request');
		});
		
		it('URL should be contain "/reservation"', () => {
			let URL;
			spyOn( window, 'open' ).and.callFake( function(value) {
				URL = value;
				return true;
			} );
			isolateScope.goTo();
			expect(URL).toContain('/reservation');
		});
	});
});