'use strict';

angular
	.module('app.request-views')
	.factory('requestSettingsHelperService', requestSettingsHelperService);

requestSettingsHelperService.$inject = ['$q', 'apiService', 'moveBoardApi'];

function requestSettingsHelperService($q, apiService, moveBoardApi) {
	return {
		сloneRequest: сloneRequest,
		createPackingDay: createPackingDay
	}

	function сloneRequest(nid) {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.request.clone, nid).then(resolve => {
			if (resolve.status === 200) {
				defer.resolve(resolve.data.data);
			} else {
				defer.reject();
			}
		});

		return defer.promise;
	}

	function createPackingDay(nid) {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.request.createPackingDay, nid).then(resolve => {
			if (resolve.status === 200) {
				defer.resolve(resolve.data.data);
			} else {
				defer.reject();
			}
		});

		return defer.promise;
	}
}
