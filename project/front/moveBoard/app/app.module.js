'use strict';

import configFront from '../../shared/config.json';

import * as OfflinePluginRuntime from 'offline-plugin/runtime';
var Raven = require('raven-js');

Raven
	.config('https://a713f7dde17a4801b0a798b220263cc5@sentry.moversboard.net:9005/7', {
		"release": configFront.release
	})
	.addPlugin(require('raven-js/plugins/angular'), angular)
	.install();

angular.module('app', [
	'app.core',
	'ngRaven',
	'app.layout',
	'app.dashboard',
	'app.data',
	'ngMaterial',
	'ngMaterialDateRangePicker',
	'md.data.table',
	'move.requests',
	'app.clients',
	'app.department',
	'app.search',
	'app.schedule',
	'app.messages',
	'app.inventories',
	'newInventories',
	'app.statistics',
	'app.account',
	'app.calculator',
	'app.settings',
	'app.settings.sms',
	'app.settings.sms-template-builder',
	'app.dispatch',
	'app.foreman',
	'app.mail',
	'app.tplBuilder',
	'app.commercialMove',
	'app.scoreFilter',
	'app.request-views',
	'ui.router',
	'permission',
	'angular.filter',
	'minicolors',
	'credit-cards',
	'app.storages',
	'app.lddispatch',
	'inhome-estimate-portal',
	'app.support',
	'ngMdIcons',
	'app.payment',
	'ngImageCompress',
	'ngAnimate',
	'flow',
	'uiCropper',
	'ngFileUpload',
	'ngMessages',
	'reminders',
	'slidePanel',
	'angularMoment',
	'er-messages',
	'dialogs',
	'shared-geo-coding',
	'shared-service-types',
	'pending-info',
	'commonServices',
	'moveboard-customer-online',
	'rt.debounce',
	'shared-sockets',
	'inventory-details-settings',
	'phone-numbers',
	'er-multiple-filters',
	'labor',
	'log',
])
	.run(RunApp)
	.config(config);

config.$inject = ['$mdAriaProvider', '$compileProvider', '$httpProvider'];

function config($mdAriaProvider, $compileProvider, $httpProvider) {
	$mdAriaProvider.disableWarnings();
	$httpProvider.useApplyAsync(1000);

	if (isProduction) {
		$compileProvider.debugInfoEnabled(false);
	}
}

/*@ngInject*/
function RunApp(Permission, $urlRouter, $rootScope, $q, AUTH_EVENTS, AuthenticationService, $state, $location, NotificationService, RequestServices) {
	OfflinePluginRuntime.install({
		onUpdating: () => {
			console.log('SW Event:', 'onUpdating');
		},
		onUpdateReady: () => {
			console.log('SW Event:', 'onUpdateReady');
			// Tells to new SW to take control immediately
			OfflinePluginRuntime.applyUpdate();
		},
		onUpdated: () => {
			console.log('SW Event:', 'onUpdated');
			// Reload the webpage to load into the new version
			window.location.reload();
		},
		onUpdateFailed: () => {
			console.log('SW Event:', 'onUpdateFailed');
		}
	});
	//receive user logged or no and after this work with permissions
	var _ready = false;

	//Hack for loader, when we receive all scripts show body
	angular.element('#preloader').show();

	//*** @Constructor
	registerPermissions(['administrator', 'helper', 'foreman', 'authenticated user', 'home-estimator']);

	$rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
		var _defState = getDefaultRoute();
		Permission.defaultRoute = _defState;
		$state.go(_defState);
	});
	$rootScope.$on(AUTH_EVENTS.logoutSuccess, function (e) {});

	$rootScope.$on('$stateChangeStart', function (event) {

		if ($rootScope.currentUser && $rootScope.currentUser.userRole.length == 1) {
			event.preventDefault();
			window.location.href = '/account';
		}
		if (_ready) return false;

		event.preventDefault();

		AuthenticationService.GetCurrent().finally(function () {

			_ready = true;

			Permission.defaultRoute = getDefaultRoute();
			Permission.run();
			$urlRouter.sync();
		});

	});

	$rootScope.$on('$startReminderService', function () {
		RequestServices.getManagersFromServer()
			.then(() => NotificationService.started ? void 0 : NotificationService.run());
	});

	function registerPermissions(allNeededRoles) {

		Permission
			.defineRole('anonymous', function () {
				var deferred = $q.defer();
				!$rootScope.currentUser ? deferred.resolve() : deferred.reject();

				return deferred.promise;
			});

		Permission
			.defineManyRoles(allNeededRoles, function (stateParams, hasRole) {

				var deferred = $q.defer();

				if (!$rootScope.currentUser) {
					deferred.reject();
				} else {
					if ($rootScope.currentUser.userRole.indexOf(hasRole) > -1) {
						deferred.resolve()
					} else {

						deferred.reject();

					}
				}
				return deferred.promise;
			});
	}

	//Register default route for each type
	function getDefaultRoute() {

		var defaultRoute = 'dashboard';

		if (!!$rootScope.currentUser) {
			if ($rootScope.currentUser.userRole.indexOf('foreman') > -1) {
				defaultRoute = 'foreman.new';
			} else if ($rootScope.currentUser.userRole.indexOf('helper') > -1) {
				defaultRoute = 'helper.new'
			} else {
				defaultRoute = $rootScope.nextRoute;
			}
		} else {
			//@delete cause memory leak infinity loop router
			defaultRoute = 'login';
		}

		return defaultRoute;
	}
}
