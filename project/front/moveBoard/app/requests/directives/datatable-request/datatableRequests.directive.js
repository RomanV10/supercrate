import momentTimezone from 'moment-timezone';
import './datatable-requests.styl';

'use strict';

angular
	.module('move.requests')
	.directive('ccRequestsDatatable', ccRequestsDatatable);

/*@ngInject*/
function ccRequestsDatatable(DTOptionsBuilder, RequestServices, logger, datacontext, requestModals,
	MomentWrapperService, erDeviceDetector, serviceTypeService, openRequestService) {
	return {
		scope: {
			'requests': '=requests',
			'displayLength': '@',
			'showheading': '=showheading',
			'allowCollapse': '@',
			'tbtitle': '=tbtitle',
			'payrollstatus': '=payrollstatus',
			'currentstatus': '=currentstatus',
			'totalentries': '=totalentries',
			'flags': '=flags',
			'moveDate': '=moveDate',
			'createDate': '=createDate',
			'company_flags': '=companyFlags',
			'sortField': '=',
			'isEnableSorting': '=',
			'leadScoreFilter': '=?',
			'reservationReceived': '=?',
			'isReservedTab': '=?',
			'clientHistory': '=?',
			'datesFilter': '=',

			// only for opened jobs in local dispatch
			'openedJobs': '@',
			'dates': '='
		},
		controller: requestsController,
		replace: true,
		compile: compile,
		template: require('./requestsdatatable.html'),
		restrict: 'A',
	};

	function compile(tElement, tAttrs, transclude) {
		return {
			pre: function preLink(scope, iElement, iAttrs, controller) {
				let tableSelector = scope.pagination ? '#datatable' : '#datatable_without_pagination';

				let showBooked = scope.showBooked;
				scope.pagination = iAttrs.customPagination;
				scope.bookedDate = iAttrs.bookeddate;

				scope.default_sorting = iAttrs.issorting;
				scope.sorting_desc = true;
				scope.sorting = 'DESC';

				if (scope.$parent.vm) {
					scope.$parent.vm.sorting = scope.sorting;
					scope.$parent.vm.sorting_fields = scope.default_sorting;
				}

				scope.sorting_asc = false;
				scope.sorting_fields = {
					nid: false,
					field_date: false,
					created: false,
					updated: false,
					field_date_confirmed: false,
					field_parser_provider_name: false,
					field_request_manager: false
				};
				//Choose Default Sorting
				angular.forEach(scope.sorting_fields, function (value, field) {
					if (scope.default_sorting == field) {
						scope.sorting_fields[field] = true;
						scope.sort_field = field;

						if (scope.$parent.vm) {
							scope.$parent.vm.sort_field = scope.sort_field;
						}
					}
				});

				let defaultSort = 0;

				if (showBooked) {
					defaultSort = 3;
				}

				if (scope.pagination) {
					scope.dtOptions = DTOptionsBuilder.newOptions()
						.withOption('aaSorting', [defaultSort, 'desc'])
						.withOption('bInfo', false)
						.withDisplayLength(25)
						.withOption('bPaginate', false);
				} else {
					scope.dtOptions = DTOptionsBuilder.newOptions()
						.withPaginationType('simple')
						.withDisplayLength(25)
						.withOption('aaSorting', [defaultSort, 'desc']);
				}
				if (_.isObject(scope.leadScoreFilter) && scope.leadScoreFilter.isChanged) {
					scope.getRequestsByLeadScoreFilter();
				}

				scope.appendToEl = angular.element(tableSelector);
			}
		};
	}

	/* @ngInject */
	function requestsController($scope, $attrs, common, $timeout, $rootScope, SweetAlert, PermissionsServices, $state, pollSourcesService) {
		const NOT_SELECTED_STATUS = 0;
		const COMPLETED_STATUS_ID = 20;
		const COMPLETED_STATUS_ID_DUPLICATE = 8;
		const COMPLETED_STATUS_NAME = ' Complete';
		const COMPLETED_COMPANY_FLAG_ID = '3';

		let tableSelector = $scope.pagination ? '#datatable' : '#datatable_without_pagination';

		$scope.appendToEl = angular.element(tableSelector);
		$scope.isDropUp = false;
		$scope.toggleDropUp = toggleDropUp;
		$scope.receiptSearch = false;
		$scope.receipt = {};
		$scope.receipt.customer_name = '';
		$scope.receipt.customer_phone = '';
		$scope.receipt.credit_card = '';
		$scope.receipt.amount = '';
		$scope.receipt.cart_type = '';
		$scope.receipt.phone = '';
		$scope.pagination = true;
		$scope.receipt.date = new Date();

		$scope.isMobile = erDeviceDetector.isMobile;

		$scope.currentTab = $scope.isReservedTab ? 4 : undefined;
		let firstSetUp = true;

		$scope.isMobile = erDeviceDetector.isMobile;
		if (_.isUndefined($scope.requests)) {
			$scope.requests = [];
		}

		$scope.cardType = {
			creditcard: 'Credit Card'
		};

		$scope.sorting_fields = {
			nid: false,
			field_date: false,
			created: false,
			updated: false,
			field_date_confirmed: false,
			field_parser_provider_name: false,
			field_request_manager: false
		};
		$scope.sorting_desc = true;
		$scope.sorting_asc = false;
		$scope.default_sorting = 'nid';
		$scope.managersList = PermissionsServices.hasPermission('canSignedSales') || PermissionsServices.hasPermission(
			'canAssignUnassignedLeads') ? common.objToArray(RequestServices.getManagers()) : [];
		$scope.currentPage = 0;
		$scope.canSeeOtherLeads = PermissionsServices.hasPermission('canSeeOtherLeads');
		//STATUSES
		$scope.statuses = [
			{
				id: '0',
				text: 'All Requests'
			},
			{
				id: '1',
				text: 'Pending'
			},
			{
				id: '2',
				text: 'Not confirmed'
			},
			{
				id: '3',
				text: 'Confirmed'
			},
			{
				id: '7',
				text: 'Date Pending'
			},
			{
				id: '17',
				text: 'Pending-info'
			},
			{
				id: '12',
				text: 'Expired'
			},
			{
				id: '5',
				text: 'Cancelled'
			},
			{
				id: '9',
				text: 'LD Step 1'
			},
			{
				id: '18',
				text: 'Not interested'
			}
		];
		$scope.menuOptions = [
			[
				'Select', $itemScope => {}
			],
			null, // Dividier
			[
				'Remove', $itemScope => {}
			]
		];

		$scope.selectInputs = {
			status: '0',
			serviceTypeOption: '0',
			managerIdSort: '0',
			sourceSort: '0'
		};

		if ($scope.currentstatus != undefined) {
			$scope.selectInputs.status = $scope.currentstatus;
		}
		$scope.status = ($scope.selectInputs.status == undefined) ? 0 : $scope.selectInputs.status;
		$scope.serviceTypeOption = 0;
		$scope.managerIdSort = 0;
		$scope.sourceSort = 0;
		$scope.busy = false;
		$scope.dateFields = {
			selected: '1'
		};

		//HEADER DATEPICKER SETTINGS
		$scope.clicked = 0;
		$scope.dateFrom = new Date();
		$scope.maxDate = new Date();

		$scope.dateFrom.setMonth($scope.dateFrom.getMonth() - 1);
		$scope.dateTo = new Date();

		$scope.dateFromOptions = {
			maxDate: $scope.dateTo,
			formatYear: 'yy',
			startingDay: 0
		};
		$scope.dateToOptions = {
			minDate: $scope.dateFrom,
			formatYear: 'yy',
			startingDay: 0
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'MMM d, y'];
		$scope.format = $scope.formats[3];
		$scope.receiptSearchHide = true;

		$scope.importedFlags = $rootScope.availableFlags || $rootScope.fieldData.field_lists.field_company_flags;
		$scope.showBooked = angular.isDefined($attrs.showbooked);
		$scope.small = angular.isDefined($attrs.small);
		$scope.fieldData = datacontext.getFieldData();

		const TIME_ZONE = $scope.fieldData.timeZone;

		if (_.isEmpty($scope.fieldData.timeZone)) {
			throw new Error('Time zone is empty. You need to include time zone file (project/api/.env)');
		}

		momentTimezone.tz.add(TIME_ZONE.initial);

		$scope.basicsettings = angular.fromJson($scope.fieldData.basicsettings);
		$scope.leadscoringsettings = angular.fromJson($scope.fieldData.leadscoringsettings);

		$scope.sources = pollSourcesService.getAllPollSources();

		$scope.companyFlags = {};
		$scope.currentFlag = {};

		$scope.PermissionsServices = PermissionsServices;
		$scope.getLeadScoreStyle = getLeadScoreStyle;
		$scope.previousPage = previousPage;
		$scope.nextPage = nextPage;
		$scope.changeSorting = changeSorting;
		$scope.showStatus = showStatus;
		$scope.createCurrentFlag = createCurrentFlag;
		$scope.receiptChange = receiptChange;
		$scope.requestJobStatus = requestJobStatus;
		$scope.requestPayrollStatus = requestPayrollStatus;
		$scope.clearDate = clearDate;
		$scope.globalReceiptSearch = globalReceiptSearch;
		$scope.requestEditModal = requestEditModal;
		$scope.openQuickMessage = openQuickMessage;
		$scope.GetMonthStats = getMonthStats;
		$scope.dateFieldChange = dateFieldChange;
		$scope.changeFlag = changeFlag;
		$scope.removeFlag = removeFlag;
		$scope.setManager = setManager;
		$scope.shortName = shortName;
		$scope.showManagerDropdown = showManagerDropdown;
		$scope.getRequestsByLeadScoreFilter = getRequestsByLeadScoreFilter;
		$scope.getCurrentServiceTypeName = serviceTypeService.getCurrentServiceTypeName;

		// TODO It is only quick improve performance. If you can. Please do it better.
		if ($rootScope.managersList) {
			$scope.managersList = angular.copy($rootScope.managersList);
		} else {
			if (_.isEmpty($scope.managersList)) {
				if (PermissionsServices.hasPermission('canSignedSales') || PermissionsServices.hasPermission(
						'canAssignUnassignedLeads')) {
					RequestServices.getManagersFromServer().then(function (data) {
						$scope.managersList = common.objToArray(data);
						$rootScope.managersList = angular.copy($scope.managersList);
					});
				}
			}
		}

		if (angular.isDefined($scope.basicsettings.companyFlags)) {
			$scope.companyFlags = angular.copy($scope.basicsettings.companyFlags);
		}

		if (angular.isDefined($scope.fieldData.field_lists.field_move_service_type)) {
			$scope.serviceType = serviceTypeService.getAllServiceTypes();
		}

		if (angular.isDefined($scope.fieldData.field_lists.field_approve)) {
			$scope.statusFields = angular.copy($scope.fieldData.field_lists.field_approve);

			let statusName = $scope.statusFields[COMPLETED_STATUS_ID_DUPLICATE];
			let isFoundCompletedStatus = statusName === COMPLETED_STATUS_NAME;

			if (isFoundCompletedStatus) {
				delete $scope.statusFields[COMPLETED_STATUS_ID_DUPLICATE];
			}
		}

		function showManagerDropdown(managerName) {
			return PermissionsServices.hasPermission('canSignedSales') ||
				!managerName && PermissionsServices.hasPermission('canAssignUnassignedLeads');
		}

		function changeSorting(sort_by, sorting) {
			if ($scope.sortField || $scope.sortField === null) {
				$scope.sortField = sort_by;
			}

			$scope.default_sorting = sort_by;
			let moveDate, createDate = null;
			if (angular.isDefined($scope.$parent.vm) && $scope.$parent.vm.isRequestsPage) {
				$scope.serviceTypeOption = parseInt($scope.selectInputs.serviceTypeOption);
				$scope.status = $scope.selectInputs.status;
				$scope.managerIdSort = $scope.selectInputs.managerIdSort;
				$scope.sourceSort = $scope.selectInputs.sourceSort;
				let dateFrom = $('#dateFrom').val();
				let dateTo = $('#dateTo').val();

				dateFrom = $.datepicker.formatDate('yy-mm-dd', new Date(dateFrom), {});
				dateTo = $.datepicker.formatDate('yy-mm-dd', new Date(dateTo), {});

				if ($scope.dateFields.selected == '1') { //create date
					createDate = {
						from: dateFrom,
						to: dateTo
					};
				} else {
					moveDate = {
						to: dateTo,
						from: dateFrom
					};
				}
				if ($scope.status != 0) {
					$scope.currentstatus = [$scope.status];
				}
			}

			$scope.sort_field = sort_by;

			if ($scope.$parent.vm) {
				$scope.$parent.vm.sort_field = $scope.sort_field;
			}

			angular.forEach($scope.sorting_fields, function (value, field) {
				$scope.sorting_fields[field] = false;
			});
			$scope.sorting_fields[sort_by] = true;

			if ($scope.sorting_fields[sort_by]) {
				if (!sorting) {
					$scope.sorting_desc = $scope.sorting_asc;
					$scope.sorting_asc = !$scope.sorting_desc;
				}
				if ($scope.sorting_desc) {
					$scope.sorting = 'DESC';
				}
				else {
					$scope.sorting = 'ASC';
				}

				if ($scope.$parent.vm) {
					$scope.$parent.vm.sorting = $scope.sorting;
				}

				if (_.get($scope, '$parent.vm.searchParams')) {
					$scope.$parent.vm.searchParams.sorting = $scope.sorting;
				}
			}

			$scope.busy = false;
			let reverse = ($scope.sorting == 'ASC');
			let field_name = sort_by;
			if ($scope.requests[0]) {
				_.forEach($scope.requests[0], function (item, key) {
					if (_.isObject(item)) {
						if (item.field == sort_by) {
							if (item.raw) {
								field_name = key + '.raw';
							} else if (item.value) {
								field_name = key + '.value';
							}
						}
					}
				});
			}
			if ($state.current.name != 'dashboard') {
				if (!_.isEmpty($scope.selectInputs.leadScoringInput)) {
					$scope.leadScoreFilter = {
						enabled: true,
						key: $scope.selectInputs.leadScoringInput,
						value: getLeadScoreParams($scope.selectInputs.leadScoringInput)
					};
				} else {
					$scope.leadScoreFilter = {
						false: false,
						key: '',
						value: {}
					};
				}
			}

			if (!firstSetUp) {
				let params = {
					status: $scope.currentstatus,
					page: $scope.currentPage,
					booked: $scope.bookedDate,
					sorting: $scope.sorting,
					sort_field: sort_by,
					leadScoreFilter: $scope.leadScoreFilter,
					flags: undefined,
					company_flags: undefined,
					moveDate: moveDate,
					createDate: createDate,
					service_type: $scope.serviceTypeOption,
					reservation_received: $scope.reservationReceived,
					currTab: $scope.currentTab,
					managerId: $scope.managerIdSort,
					statusSort: $scope.sourceSort,
					dates_filter: $scope.datesFilter
				};

				RequestServices.getStatusRequestsByPage(params)
					.then(function (response) {
						$scope.busy = false;
						$scope.requests = response.data.nodes;
					});
			}

			firstSetUp = false;
		}

		function getLeadScoreStyle(score) {
			let style;
			switch (true) {
			case score < $scope.leadscoringsettings.scoring.warm:
				style = 'cold';
				break;
			case (score >= $scope.leadscoringsettings.scoring.warm && score < $scope.leadscoringsettings.scoring.hot):
				style = 'warm';
				break;
			case score >= $scope.leadscoringsettings.scoring.hot:
				style = 'hot';
				break;
			}
			return style;
		}

		function getLeadScoreParams(key) {
			let result;
			switch (key) {
			case 'hot':
				result = {
					from: $scope.leadscoringsettings.scoring.hot,
					to: 100000
				};
				break;
			case 'warm':
				result = {
					from: $scope.leadscoringsettings.scoring.warm,
					to: $scope.leadscoringsettings.scoring.hot - 1
				};
				break;
			case 'cold':
				result = {
					from: 0,
					to: $scope.leadscoringsettings.scoring.warm - 1
				};
				break;
			default:
				result = {};
			}
			return result;
		}


		function getRequestsByLeadScoreFilter() {
			$scope.leadScoreFilter.isChanged = true;

			if (($scope.leadScoreFilter.enabled && !_.isEmpty(
					$scope.leadScoreFilter.key)) || !$scope.leadScoreFilter.enabled) {

				let sort_by = _.findKey($scope.sorting_fields, function (value) {
					return value == true;
				});
				/*if (!sort_by) {
				 sort_by = 'nid';
				 $scope.sorting = "DESC";
				 }*/
				let moveDate, createDate = null;
				if (angular.isDefined($scope.$parent.vm) && $scope.$parent.vm.isRequestsPage) {
					$scope.serviceTypeOption = parseInt($scope.selectInputs.serviceTypeOption);
					$scope.status = $scope.selectInputs.status;
					$scope.managerIdSort = $scope.selectInputs.managerIdSort;
					$scope.sourceSort = $scope.selectInputs.sourceSort;
					let dateFrom = $('#dateFrom').val();
					let dateTo = $('#dateTo').val();

					dateFrom = $.datepicker.formatDate('yy-mm-dd', new Date(dateFrom), {});
					dateTo = $.datepicker.formatDate('yy-mm-dd', new Date(dateTo), {});

					if ($scope.dateFields.selected == '1') { //create date
						createDate = {
							from: dateFrom,
							to: dateTo
						};
					} else {
						moveDate = {
							to: dateTo,
							from: dateFrom
						};
					}
					if ($scope.status != 0) {
						$scope.currentstatus = [$scope.status];
					}
				}

				if ($scope.leadScoreFilter.enabled) {
					$scope.leadScoreFilter.value = getLeadScoreParams($scope.leadScoreFilter.key);
				} else {
					$scope.leadScoreFilter.key = '';
					$scope.leadScoreFilter.value = {};
				}

				$scope.busy = true;
				let params = {
					status: $scope.currentstatus,
					page: $scope.currentPage,
					booked: $scope.bookedDate,
					sorting: $scope.sorting,
					sort_field: sort_by,
					leadScoreFilter: $scope.leadScoreFilter,
					flags: undefined,
					company_flags: undefined,
					moveDate: moveDate,
					createDate: createDate,
					service_type: $scope.serviceTypeOption,
					reservation_received: $scope.reservationReceived,
					currTab: $scope.currentTab,
					managerId: $scope.managerIdSort,
					statusSort: $scope.sourceSort,
					dates_filter: $scope.datesFilter
				};
				RequestServices.getStatusRequestsByPage(params)
					.then(function (response) {
						$scope.busy = false;
						$scope.requests = response.data.nodes;
						$scope.totalentries = response.data.count;
					});
			}
		}

		function nextPage() {
			$scope.currentPage += 1;
			if ($scope.$parent.vm && $scope.$parent.vm.currentPage != null && $scope.$parent.vm.currentPage != undefined) {
				$scope.$parent.vm.currentPage = $scope.currentPage;
			}

			if (_.get($scope, '$parent.vm.searchParams')) {
				$scope.$parent.vm.searchParams.currentPage = $scope.currentPage;
			}

			$scope.serviceTypeOption = $scope.selectInputs.serviceTypeOption;
			$scope.status = $scope.selectInputs.status;
			$scope.managerIdSort = $scope.selectInputs.managerIdSort;
			$scope.sourceSort = $scope.selectInputs.sourceSort;
			let flags = undefined;
			let company_flags = undefined;
			let service_type = undefined;
			let currentstatus = $scope.currentstatus;
			let manager_id_sort = undefined;
			let source_sort = undefined;
			$scope.busy = true;
			if ($scope.showheading) {
				let dateFrom = $('#dateFrom').val();
				let dateTo = $('#dateTo').val();
				if ($scope.dateFields.selected == 1) {
					$scope.createDate = {
						from: MomentWrapperService.getZeroMoment(new Date(dateFrom)).format('YYYY-MM-DD'),
						to: MomentWrapperService.getZeroMoment(new Date(dateTo)).format('YYYY-MM-DD')
					};
				} else if ($scope.dateFields.selected == 2) {
					$scope.createDate = void 0;
					$scope.moveDate = {
						from: MomentWrapperService.getZeroMoment(new Date(dateFrom)).format('YYYY-MM-DD'),
						to: MomentWrapperService.getZeroMoment(new Date(dateTo)).format('YYYY-MM-DD')
					};
				}
				if ($scope.serviceTypeOption != 0) {
					service_type = $scope.serviceTypeOption;
				}
				if ($scope.status != 0) {
					currentstatus = $scope.status;
				}
				if ($scope.managerIdSort != 0) {
					manager_id_sort = $scope.managerIdSort;
				}
				if ($scope.sourceSort != 0) {
					source_sort = $scope.sourceSort;
				}
			}
			if ($scope.flags) {
				if ($scope.flags != 'requests') {
					if (!!$scope.flags) {
						flags = $scope.flags;
					}
				}
			}
			if ($scope.company_flags) {
				company_flags = $scope.company_flags;
			}

			if ($state.current.name != 'dashboard') {
				if (!_.isEmpty($scope.selectInputs.leadScoringInput)) {
					$scope.leadScoreFilter = {
						enabled: true,
						key: $scope.selectInputs.leadScoringInput,
						value: getLeadScoreParams($scope.selectInputs.leadScoringInput)
					};
				} else {
					$scope.leadScoreFilter = {
						false: false,
						key: '',
						value: {}
					};
				}
			}
			if (!_.isEmpty($scope.receipt) && angular.isDefined($scope.receiptRequests)) {
				let arr = _.clone($scope.receiptRequests);
				let page = $scope.currentPage == 1 ? 25 : ((($scope.currentPage) * 25) - 1);
				if ((arr.length - page + 25) < 25) {
					arr = arr.slice(page, arr.length);
				} else {
					arr = arr.slice(page, page + 25);
				}
				let getRequests = {
					'page': 0,
					'pagesize': 25,
					'condition': {
						'nid': arr
					}
				};
				let promise = RequestServices.getRequestsArr(getRequests);
				promise.then(function (response) {
					$scope.busy = false;
					$scope.requests = response.nodes;
					$timeout(function () {
						_.forEach(response.nodes, function (request) {
							createCurrentFlag(request.nid);
						});
					});
				});
			} else {
				if ($scope.openedJobs) {
					getOpenedJobs();
				} else {
					let params = {
						status: currentstatus,
						page: $scope.currentPage,
						booked: $scope.bookedDate,
						sorting: $scope.sorting,
						sort_field: $scope.sort_field,
						leadScoreFilter: $scope.leadScoreFilter,
						flags: flags,
						company_flags: company_flags,
						moveDate: $scope.moveDate,
						createDate: $scope.createDate,
						service_type: service_type,
						reservation_received: $scope.reservationReceived,
						currTab: $scope.currentTab,
						managerId: manager_id_sort,
						statusSort: source_sort,
						dates_filter: $scope.datesFilter
					};
					RequestServices.getStatusRequestsByPage(params)
						.then(function (response) {
							$scope.busy = false;
							$scope.requests = response.data.nodes;
							$timeout(function () {
								_.forEach(response.data.nodes, function (request) {
									createCurrentFlag(request.nid);
								});
							});
						});
				}
			}
		}

		function previousPage() {
			$scope.currentPage -= 1;

			if ($scope.$parent.vm && $scope.$parent.vm.currentPage != null && $scope.$parent.vm.currentPage != undefined) {
				$scope.$parent.vm.currentPage = $scope.currentPage;
			}

			if (_.get($scope, '$parent.vm.searchParams')) {
				$scope.$parent.vm.searchParams.currentPage = $scope.currentPage;
			}

			$scope.serviceTypeOption = $scope.selectInputs.serviceTypeOption;
			$scope.status = $scope.selectInputs.status;
			$scope.managerIdSort = $scope.selectInputs.managerIdSort;
			$scope.sourceSort = $scope.selectInputs.sourceSort;
			let flags = undefined;
			let company_flags = undefined;
			let service_type = undefined;
			let currentstatus = $scope.currentstatus;
			let manager_id_sort = undefined;
			let sourse_sort = undefined;
			$scope.busy = true;
			if ($scope.showheading) {
				let dateFrom = $('#dateFrom').val();
				let dateTo = $('#dateTo').val();
				if ($scope.dateFields.selected == 1) {
					$scope.createDate = {
						from: MomentWrapperService.getZeroMoment(new Date(dateFrom)).format('YYYY-MM-DD'),
						to: MomentWrapperService.getZeroMoment(new Date(dateTo)).format('YYYY-MM-DD')
					};
				} else if ($scope.dateFields.selected == 2) {
					$scope.createDate = void 0;
					$scope.moveDate = {
						from: MomentWrapperService.getZeroMoment(new Date(dateFrom)).format('YYYY-MM-DD'),
						to: MomentWrapperService.getZeroMoment(new Date(dateTo)).format('YYYY-MM-DD'),
					};
				}
				if ($scope.serviceTypeOption != 0) {
					service_type = $scope.serviceTypeOption;
				}
				if ($scope.status != 0) {
					currentstatus = $scope.status;
				}
				if ($scope.managerIdSort != 0) {
					manager_id_sort = $scope.managerIdSort;
				}
				if ($scope.sourceSort != 0) {
					sourse_sort = $scope.sourceSort;
				}
			}
			if ($scope.flags) {
				if ($scope.flags != 'requests') {
					if (!!$scope.flags) {
						flags = $scope.flags;
					}
				}
			}
			if ($scope.company_flags) {
				company_flags = $scope.company_flags;
			}

			if ($state.current.name != 'dashboard') {
				if (!_.isEmpty($scope.selectInputs.leadScoringInput)) {
					$scope.leadScoreFilter = {
						enabled: true,
						key: $scope.selectInputs.leadScoringInput,
						value: getLeadScoreParams($scope.selectInputs.leadScoringInput)
					};
				} else {
					$scope.leadScoreFilter = {
						false: false,
						key: '',
						value: {}
					};
				}
			}
			if (!_.isEmpty($scope.receipt) && angular.isDefined($scope.receiptRequests)) {
				let arr = _.clone($scope.receiptRequests);
				let page = $scope.currentPage == 0 ? 25 : ((($scope.currentPage) * 25) - 1);
				arr = arr.slice(page, page + 25);
				let getRequests = {
					'page': 0,
					'pagesize': 25,
					'condition': {
						'nid': arr
					}
				};
				let promise = RequestServices.getRequestsArr(getRequests);
				promise.then(function (response) {
					$scope.busy = false;
					$scope.requests = response.nodes;
					$timeout(function () {
						_.forEach(response.nodes, function (request) {
							createCurrentFlag(request.nid);
						});
					});
				});
			} else {
				if ($scope.openedJobs) {
					getOpenedJobs();
				} else {
					let params = {
						status: currentstatus,
						page: $scope.currentPage,
						booked: $scope.bookedDate,
						sorting: $scope.sorting,
						sort_field: $scope.sort_field,
						leadScoreFilter: $scope.leadScoreFilter,
						flags: flags,
						company_flags: company_flags,
						moveDate: $scope.moveDate,
						createDate: $scope.createDate,
						service_type: service_type,
						reservation_received: $scope.reservationReceived,
						currTab: $scope.currentTab,
						managerId: manager_id_sort,
						statusSort: sourse_sort,
						dates_filter: $scope.datesFilter
					};
					RequestServices.getStatusRequestsByPage(params)
						.then(function (response) {
							$scope.busy = false;
							$scope.requests = response.data.nodes;
							$timeout(function () {
								_.forEach(response.data.nodes, function (request) {
									createCurrentFlag(request.nid);
								});
							});
						});
				}
			}
		}

		function getOpenedJobs() {
			let data = {
				from: moment(new Date($scope.dates.from)).format('YYYY-MM-DD'),
				to: moment(new Date($scope.dates.to)).format('YYYY-MM-DD')
			};
			RequestServices.getOpenedJobs(data, $scope.currentPage).then(function (data) {
				$scope.requests.length = 0;
				$scope.totalCount = data.count;

				$scope.requests = data.nodes;

				$scope.busy = false;
			});
		}

		$scope.$on('request_flag', function (event, data, flag) {
			if (flag == 'remove') {
				removeFlag(data.nid, true);
			} else {
				changeFlag(data.nid, flag, true);
			}
		});

		$scope.$on('request.updated', handlerUpdateRequest);

		function handlerUpdateRequest(event, data) {
			if (!_.isUndefined(data.request_all_data)) {
				let index = _.findIndex($scope.requests, {nid: data.nid});

				if (index > -1) {
					let request = $scope.requests[index];
					request.request_all_data = data.request_all_data;
					changeSorting($scope.default_sorting || 'nid', true);
				}
			}
		}

		function toggleDropUp($event) {
			let dropdownContainer = $event.currentTarget;
			let position = dropdownContainer.getBoundingClientRect().top;
			let buttonHeight = dropdownContainer.getBoundingClientRect().height;

			let dropdownMenu = $(dropdownContainer).find('.dropdown-menu');
			let menuHeight = dropdownMenu.outerHeight();

			let $win = $(window);
			if (position > menuHeight && $win.height() - position < buttonHeight + menuHeight) {
				$scope.isDropUp = true;
			} else {
				$scope.isDropUp = false;
			}
		}

		function showStatus() {
			let el = $('#fstatus option:selected');
			let statusId = el.val();
			let status = 'status_' + statusId;

			if (statusId == 0) {
				status = '';
			}

			$(tableSelector).DataTable().row(3).search(status).draw();
		}

		function createCurrentFlag(nid) {
			let indReq = _.findIndex($scope.requests, req => req && req.nid == nid);
			if (angular.isDefined(nid) && !_.isArray($scope.requests)) {
				if (angular.isDefined($scope.requests[indReq].field_flags) && angular.isDefined(
						$scope.requests[indReq].field_flags.value)) {
					for (let id in $scope.requests[indReq].field_flags.value) {
						for (let j in $scope.companyFlags) {
							if (id == $scope.companyFlags[j].id) {
								$scope.currentFlag[nid] = $scope.companyFlags[j];
							}
						}
					}
				}
			} else if (angular.isDefined(nid) && _.isArray($scope.requests)) {
				for (let i in $scope.requests) {
					if (angular.isDefined($scope.requests[i].field_flags.value)) {
						for (let id in $scope.requests[i].field_flags.value) {
							for (let j in $scope.companyFlags) {
								if (id == $scope.companyFlags[j].id) {
									$scope.currentFlag[$scope.requests[i].nid] = $scope.companyFlags[j];
								}
							}
						}
					}
				}
			}
		}

		function receiptChange() {
			$scope.receiptSearchHide = !$scope.receiptSearchHide;
			common.$timeout(function () {
				$scope.receiptSearch = !$scope.receiptSearch;
				if (!$scope.receiptSearch) {
					$scope.receipt = {};
				}
			}, 100);
		}

		function requestJobStatus(nid) {
			let indReq = _.findIndex($scope.requests, req => req && req.nid == nid);
			if ($scope.payrollstatus && $scope.importedFlags.company_flags) {
				let Job_Completed = 'Completed';
				return $scope.requests[indReq].field_company_flags.value.hasOwnProperty(
					$scope.importedFlags.company_flags[Job_Completed]);
			}
		}

		function requestPayrollStatus(nid) {
			let indReq = _.findIndex($scope.requests, {nid: nid});
			if ($scope.payrollstatus && $scope.importedFlags.company_flags) {
				let Payroll_Done = 'Payroll Done';

				return $scope.requests[indReq].field_company_flags.value.hasOwnProperty(
					$scope.importedFlags.company_flags[Payroll_Done]);
			}
		}

		function clearDate() {
			$scope.receipt.date = '';
		}

		function globalReceiptSearch() {
			$scope.busy = true;
			$scope.currentPage = 0;

			let reg = /^\d+$/;
			let searchValues = {};

			if ($scope.receipt.customer_name != '' && $scope.receipt.customer_name != null && angular.isDefined(
					$scope.receipt.customer_name)) {
				searchValues.customer_name = $scope.receipt.customer_name;
			}

			if ($scope.receipt.customer_phone != '' && $scope.receipt.customer_phone != null && angular.isDefined(
					$scope.receipt.customer_phone)) {
				searchValues.phone = $scope.receipt.customer_phone;
			}

			if ($scope.receipt.credit_card != '' && $scope.receipt.credit_card != null && angular.isDefined(
					$scope.receipt.credit_card)) {
				if (reg.test($scope.receipt.credit_card)) {
					searchValues.credit_card = $scope.receipt.credit_card;
				}
			}

			if ($scope.receipt.amount != '' && $scope.receipt.amount != null && angular.isDefined(
					$scope.receipt.amount)) {
				searchValues.amount = $scope.receipt.amount;
			}

			if ($scope.receipt.card_type != '' && $scope.receipt.card_type != null && angular.isDefined(
					$scope.receipt.card_type)) {
				searchValues.cart_type = $scope.receipt.card_type;
			}

			if ($scope.receipt.date != '' && $scope.receipt.date != null && angular.isDefined($scope.receipt.date)) {
				searchValues.date = MomentWrapperService.getZeroMoment(new Date($scope.receipt.date)).format(
					'YYYY-MM-DD');
			}

			if (!_.isEmpty(searchValues)) {
				RequestServices.receiptSearch(searchValues).then(function (data) {
					let arr = data;
					arr = arr.slice(0, 25);
					let getRequests = {
						'page': 0,
						'pagesize': 25,
						'condition': {
							'nid': arr
						}
					};
					$scope.receiptRequests = data;
					$scope.totalentries = data.length;
					if (!_.isEmpty(data)) {
						RequestServices.getRequestsArr(getRequests).then(function (requests) {
							$scope.requests = _.clone(requests.nodes);
							$scope.busy = false;
						});
					} else {
						$scope.requests = [];
						$scope.totalentries = 0;
						$scope.busy = false;
					}
				});
			}
		}

		function requestEditModal(request) {

			if (request.nid == $scope.clicked) {
				//GET SAME DATE REQUESTS 07/31/2015
				if ($scope.isMobile) {
					$state.go('mobile.openEditRequest', {id: request.nid});
				} else {
					$scope.busy = true;

					openRequestService.open(request.nid)
						.finally(() => {
							$scope.busy = false;
						});
				}
			} else {
				$scope.clicked = request.nid;
			}
		}

		function openQuickMessage(nid) {
			requestModals.openCommentModal(nid);
			$scope.clicked = nid;
		}

		function getMonthStats() {
			//$scope.sorting_fields['field_date'] = false;
			$scope.receipt = {};
			$scope.receiptSearchHide = true;
			$scope.receiptSearch = false;
			$scope.serviceTypeOption = $scope.selectInputs.serviceTypeOption;
			$scope.status = $scope.selectInputs.status;
			$scope.managerIdSort = $scope.selectInputs.managerIdSort;
			$scope.sourceSort = $scope.selectInputs.sourceSort;
			let dateFrom = $('#dateFrom').val();
			let dateTo = $('#dateTo').val();
			let data = {
				pagesize: 25,
				page: 0,
				filtering: {},
				condition: {},
			};
			let sortBy = _.findKey($scope.sorting_fields, function (value) {
				return value == true;
			});
			if (sortBy) {
				data.sorting = {
					field: sortBy,
					direction: $scope.sorting
				};
			}

			let from = moment(dateFrom).format('YYYY-MM-DD');
			let to = moment(dateTo).format('YYYY-MM-DD');

			if ($scope.dateFields.selected == '1') { //create date
				data.filtering.date_from = momentTimezone.tz(from, TIME_ZONE.name).startOf('day').unix();
				data.filtering.date_to = momentTimezone.tz(to, TIME_ZONE.name).endOf('day').unix();
			} else {
				data.filtering.field_date_from = momentTimezone.tz(from, TIME_ZONE.name).startOf('day').unix();
				data.filtering.field_date_to = momentTimezone.tz(to, TIME_ZONE.name).endOf('day').unix();
			}

			if ($scope.serviceTypeOption != 0) {
				data.condition.field_move_service_type = $scope.serviceTypeOption;
			}

			if (+$scope.status !== NOT_SELECTED_STATUS) {
				if (+$scope.status === COMPLETED_STATUS_ID) {
					data.condition.field_company_flags = [COMPLETED_COMPANY_FLAG_ID];
				} else {
					data.condition.field_approve = [$scope.status];
				}
			}

			if ($scope.managerIdSort != 0) {
				data.condition.field_request_manager = $scope.managerIdSort;
			}

			if ($scope.sourceSort != 0) {
				data.condition.field_parser_provider_name = $scope.sourceSort;
			}

			if ($scope.flags != 'requests') {
				if (!!$scope.flags) {
					data.condition.field_flags = $scope.flags;
				}
			}

			if (!_.isEmpty($scope.selectInputs.leadScoringInput)) {
				data.condition.field_total_score = getLeadScoreParams($scope.selectInputs.leadScoringInput);
			}

			$scope.busy = true;
			$scope.bDateChange = false;

			if (_.isEmpty(data.condition)) {
				data.condition = undefined;
			}

			RequestServices.getAllRequestsDateRange(data).then(function (response) {
				if (response.data.nodes) {
					$scope.requests = response.data.nodes;
					$scope.totalentries = response.data.count;
				} else {
					$scope.requests = [];
					$scope.totalentries = 0;
				}

				$scope.busy = false;

			}, function (reason) {
				$scope.busy = false;
				logger.error(reason, reason, 'Error');
			});
		}

		function dateFieldChange() {
			$scope.bDateChange = true;
			if ($scope.dateFields.selected == '1') {
				$scope.maxDate = new Date();
			} else {
				$scope.maxDate = '';
			}
		}

		function changeFlag(nid, flag, withoutLog) {

			if (_.isEmpty($scope.requests)) {
				return;
			}

			let arr = [];
			arr.push(Number(flag.id));
			let indReq = 0;
			$scope.busy = true;

			_.forEach($scope.requests, function (request, ind) {
				if (request.nid == nid) {
					indReq = ind;
				}
			});

			RequestServices.addFlagToRequest(arr, nid).then(function (data) {
				$scope.busy = false;
				if (angular.isDefined($scope.requests[indReq].field_flags)) {
					if (angular.isDefined($scope.requests[indReq].field_flags.value)) {
						for (let i in $scope.requests[indReq].field_flags.value) {
							delete $scope.requests[indReq].field_flags.value[i];
						}
						$scope.requests[indReq].field_flags.value[flag.id] = flag.name;
					} else {
						$scope.requests[indReq].field_flags.value = {};
						$scope.requests[indReq].field_flags.value[flag.id] = flag.name;
					}
				} else {
					$scope.requests[indReq].field_flags = {};
					$scope.requests[indReq].field_flags.value = {};
					$scope.requests[indReq].field_flags.value[flag.id] = flag.name;
				}

				if (angular.isUndefined($scope.currentFlag[nid])) {
					$scope.currentFlag[nid] = {};
				}

				if (!_.isEqual($scope.currentFlag[nid].name, flag.name)) {
					$scope.message = 'Flag was ';
					if ($scope.currentFlag[nid].name) {
						$scope.message += ' changed from ' + $scope.currentFlag[nid].name + ' ';
					} else {
						$scope.message += ' set ';
					}
					$scope.message += 'to ' + flag.name;
				}

				if (_.get($scope, 'message.length')) {
					let details = [];
					let msg = {
						simpleText: $scope.message
					};

					details.push(msg);

					if (!withoutLog) {
						RequestServices.sendLogs(details, 'Request update', nid, 'MOVEREQUEST');
					}

					$scope.message = '';
				}

				$scope.currentFlag[nid] = flag;
			});
		}

		function removeFlag(nid, withoutLog) {
			let arr = [];
			$scope.busy = true;
			let indReq = 0;
			_.forEach($scope.requests, function (request, ind) {
				if (request.nid == nid) {
					indReq = ind;
				}
			});

			RequestServices.addFlagToRequest(arr, nid).then(function (data) {
				$scope.busy = false;
				if (angular.isDefined($scope.requests[indReq].field_flags)) {
					if (angular.isDefined($scope.requests[indReq].field_flags.value)) {
						for (let i in $scope.requests[indReq].field_flags.value) {
							delete $scope.requests[indReq].field_flags.value[i];
						}
					} else {
						$scope.requests[indReq].field_flags.value = {};
					}
				} else {
					$scope.requests[indReq].field_flags = {};
					$scope.requests[indReq].field_flags.value = {};
				}
				if ($scope.currentFlag[nid]) {
					$scope.message = 'Flag was removed from ' + $scope.currentFlag[nid].name;

					if (_.get($scope, 'message.length')) {
						let details = [];
						let msg = {
							simpleText: $scope.message
						};
						details.push(msg);

						if (!withoutLog) {
							RequestServices.sendLogs(details, 'Request update', nid, 'MOVEREQUEST');
						}

						$scope.message = '';
					}

					delete $scope.currentFlag[nid];
				}
			});
		}

		function setManager(manager, request) {
			SweetAlert.swal({
				title: 'Set new manager',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#64C564',
				confirmButtonText: 'Confirm',
				closeOnConfirm: true
			}, function (isConfirm) {
				if (isConfirm) {
					setRequestManager(manager, request);
				}
			});
		}

		function setRequestManager(manager, request) {
			let promise = RequestServices.setManager(manager.uid, request.nid);
			promise.then(function () {
				toastr.success('Success!', 'New salesperson was assigned to the request');
				let msg = {
					text: 'New manager was apply to request',
					to: manager.field_user_first_name + ' ' + manager.field_user_last_name,
					from: ''
				};
				if ((request.manager) && (request.manager.first_name)) {
					msg.from = request.manager.first_name + ' ' + request.manager.last_name;
				}
				let arr = [];
				arr.push(msg);
				RequestServices.sendLogs(arr, 'Manager was changed', request.nid, 'MOVEREQUEST');
				let data = {
					first_name: manager.field_user_first_name,
					last_name: manager.field_user_last_name
				};
				$rootScope.$broadcast('manager.assigned', data, request.nid);
			}, function (reason) {
				toastr.error('Error', '');
			});
		}

		function shortName(name) {
			if (name.length > 16) {
				let sliced = name.slice(0, 12);
				if (sliced.length < name.length) {
					sliced += '...';
				}
				return sliced;
			}
			return name;
		}

		$scope.$watch('selectInputs.status', updateDashbordRequest);

		$scope.$watch('selectInputs.serviceTypeOption', updateDashbordRequest);

		$scope.$watch('dateFields.selected', updateDashbordRequest);

		$scope.$watch('selectInputs.managerIdSort', updateDashbordRequest);

		$scope.$watch('selectInputs.sourceSort', updateDashbordRequest);

		if ($scope.leadscoringsettings.enabled) {
			$scope.$watch('selectInputs.leadScoringInput', updateDashbordRequest);
		}

		function updateDashbordRequest(newValue, oldValue) {
			if (newValue != oldValue) {
				$scope.currentPage = 0;
				getMonthStats();
			}
		}

		$scope.$on('dashboard.refresh', function (event, data) {
			if ($scope.$parent.vm) {
				$scope.$parent.vm.sorting = $scope.sorting;
			}
			$scope.sorting_fields = {
				nid: false,
				field_date: false,
				created: false,
				updated: false,
				field_date_confirmed: false,
				field_parser_provider_name: false,
				field_request_manager: false
			};
			//Choose Default Sorting
			angular.forEach($scope.sorting_fields, function (value, field) {
				if ($scope.default_sorting == field) {
					$scope.sorting_fields[field] = true;
					$scope.sort_field = field;

					if ($scope.$parent.vm) {
						$scope.$parent.vm.sort_field = $scope.sort_field;
					}
				}
			});
			changeSorting($scope.default_sorting, true);
		});

		$scope.$watch('totalentries', function (newValue, oldValue) {
			$scope.totalentries = newValue;
		});

		$scope.$on('total_entries', function (data) {
			$scope.total_entries = data;
		});

		$scope.$on('job_completed', function (event, data) {
			let indReq = _.findIndex($scope.requests, req => req && req.nid == data.nid);
			let Job_Completed = 'Completed';
			if ($scope.requests[indReq]) {
				if (data.job_completed) {
					$scope.requests[indReq].field_company_flags.value[$scope.importedFlags.company_flags[Job_Completed]] = Job_Completed;
				} else {
					delete $scope.requests[indReq].field_company_flags.value[$scope.importedFlags.company_flags[Job_Completed]];
				}
			}
		});

		$scope.$on('request.open_search', function (event, data) {
			$scope.clicked = data.nid;
			$scope.requestEditModal(data);
		});

		$scope.$on('manager.assigned', function (data, manager, nid) {
			let index = _.findIndex($scope.requests, o => o && o.nid == nid);

			if (index != -1) {
				$scope.requests[index].manager = manager;
			}
		});

		// fix bug with automatically load request on requests menu
		if ($scope.isEnableSorting) {
			changeSorting($attrs['issorting'], true);
		} else {
			firstSetUp = false;
		}


	}
}
