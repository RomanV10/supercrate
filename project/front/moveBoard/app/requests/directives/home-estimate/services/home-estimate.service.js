(function () {

    angular
        .module('move.requests')
        .factory('HomeEstimateService', HomeEstimateService);

    HomeEstimateService.$inject = ['moveBoardApi', 'apiService', 'MomentWrapperService'];

    function HomeEstimateService(moveBoardApi, apiService, MomentWrapperService) {

        var service = {};

        service.getHomeEstimateRequests = getHomeEstimateRequests;
        service.getHomeEstimators = getHomeEstimators;
        service.getCountOfScheduleWorks = getCountOfScheduleWorks;

        return service;


        function getHomeEstimateRequests(fromDate, toDate) {
            if (_.isUndefined(toDate)) {
                toDate = fromDate;
            }

            fromDate = MomentWrapperService.getZeroMoment(fromDate).startOf('day').unix();
            toDate = MomentWrapperService.getZeroMoment(toDate).endOf('day').unix();

            let data = {
                "pagesize": 500,
                "filtering": {
                    "home_estimate_date_from": fromDate,
                    "home_estimate_date_to": toDate
                }
            };

            return apiService.postData(moveBoardApi.homeEstimate.getRequests, data);
        }

        function getHomeEstimators() {
            return apiService.postData(moveBoardApi.homeEstimate.getHomeEstimators);
        }

        function getCountOfScheduleWorks() {
            return apiService.postData(moveBoardApi.homeEstimate.getCountSheduleRequests);
        }
    }
})();