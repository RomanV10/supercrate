import './home-estimate.styl'
(function () {
    'use strict';

    angular.module('move.requests')
           .directive('homeEstimate', homeEstimate);

    homeEstimate.$inject = ['HomeEstimateService', '$state', 'erDeviceDetector'];

    function homeEstimate(HomeEstimateService, $state, erDeviceDetector) {
        return {
            restrict: 'E',
            template: require('./home-estimate.template.html'),
            scope: {
                requests: '=',
                request: '=',
                homeEstimators: '=',
                options: '=',
                updateMessage: '=onUpdateMessage',
                removeMessage: '=onRemoveMessage',
            },
            link: linker
        };

        function linker($scope, element) {
			$scope.isMobile = erDeviceDetector.isMobile;
            
            $scope.busy = false;
	
			$scope.isShowHomeEstimate = !_.isEqual($state.current.name, "inhome.estimator-request");

            if ($scope.options) {
                $scope.isActiveActions = $scope.options.isActiveActions || false;
            }

            if ($scope.isActiveActions) {
                if ($scope.request.home_estimate_date.value) {
                    $scope.curDate = moment($scope.request.home_estimate_date.value).format('dddd, MMMM DD, YYYY')
                } else {
                    $scope.curDate = moment().format('dddd, MMMM DD, YYYY');
                }
            }

            $scope.$watch('curDate', onUpdateCurDate);

            function onUpdateCurDate(newVal) {
				$scope.busy = true;
	
				HomeEstimateService
					.getHomeEstimateRequests(newVal)
					.then(function (response) {
						$scope.requests = response.data.nodes;
					})
					.finally(function () {
						$scope.busy = false;
					})
            }
        }
    }

})();
