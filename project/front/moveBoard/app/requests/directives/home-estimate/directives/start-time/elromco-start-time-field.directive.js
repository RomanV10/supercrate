(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elromcoStartTimeField', elromcoStartTimeField);

    elromcoStartTimeField.$inject = ['datacontext'];

    function elromcoStartTimeField(datacontext) {
        return {
            restrict: 'AE',
            scope: {
                field: '=',
                updateMessage: '=onUpdateMessage',
                removeMessage: '=onRemoveMessage'
            },
            link: linker
        };

        function linker(scope, element, attrs, ngModel) {
            element.timepicker({
                'disableTouchKeyboard': true,
                'minTime': '00:00am',
                'maxTime': '11:30pm',
                'timeFormat': 'g:i A'
            });

            element.bind('change', function () {
                var value = element.val();
                var oldVal = scope.field.old;
                scope.field.value = value;

                if (scope.updateMessage && value != oldVal) {
	                updateMessage(scope.field);
                }

                if (scope.removeMessage && value == oldVal) {
                    scope.removeMessage(scope.field);
                }

                scope.$apply();
            });

	        function updateMessage(field) {
		        if (!field) return;

		        scope.updateMessage({
			        field: field,
		        });
	        }
        }
    }

})();
