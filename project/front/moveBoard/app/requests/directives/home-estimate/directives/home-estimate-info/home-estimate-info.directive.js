import './home-estimate-info.styl'

(function () {
    'use strict';

    angular.module('move.requests')
           .directive('elHomeEstimateInfo', homeEstimateInfo);

    homeEstimateInfo.$inject = ['$rootScope'];

    function homeEstimateInfo($rootScope) {
        return {
            restrict: 'E',
            template: require('./home-estimate-info.template.html'),
            scope: {
                requests: '=',
                request: '=',
                curDate: '=',
                options: '=',
                updateMessage: '=onUpdateMessage',
                removeMessage: '=onRemoveMessage',
            },
            link: linker
        };

        function linker($scope, element) {
            if ($scope.options) {
                $scope.isActiveActions = $scope.options.isActiveActions || false;
            }

            if ($scope.isActiveActions) {
                if ($scope.request.home_estimate_assigned.value) {
                    $scope.assignedUserInfo = $scope.request.home_estimate_assigned.value;
                } else {
                    $scope.assignedUserInfo = $rootScope.currentUser.userId.field_user_first_name + " " + $rootScope.currentUser.userId.field_user_last_name;
                }


                if ($scope.request.home_estimate_date_created.value) {
                    $scope.createdDateInfo = moment($scope.request.home_estimate_date_created.value).format('MMMM DD, YYYY');
                } else {
                    $scope.createdDateInfo = moment().format('MMMM DD, YYYY');
                }
            }
        }
    }

})();