(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('homeEstimateStatus', homeEstimateStatus);

    function homeEstimateStatus() {
        return {
            restrict: 'AE',
            template: require('./status.template.html'),
            scope: {
                field: '=',
                updateMessage: '=onUpdateMessage',
                removeMessage: '=onRemoveMessage'
            },
            link: linker
        };

        function linker(scope, element, attrs, ngModel) {
            scope.types = [
                {id: "1", name: "Schedule"},
                {id: "2", name: "Done"},
                {id: "3", name: "Canceled"}
            ];

            scope.updateStatusField = updateStatusField;

            function updateStatusField() {
                if (scope.updateMessage && scope.field.value != scope.field.old) {
	                updateMessage(scope.field);
                }

                if (scope.removeMessage && scope.field.value == scope.field.old) {
                    scope.removeMessage(scope.field);
                }
            }

	        function updateMessage(field) {
		        if (!field) return;

		        scope.updateMessage({
			        field: field,
		        });
	        }
        }
    }

})();
