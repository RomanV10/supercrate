import './worker-item.styl'

(function () {
    'use strict';

    angular.module('move.requests')
           .directive('elHomeEstimateWorkerItem', workItem);

    function workItem() {
        return {
            restrict: 'E',
            template: require('./worker-item.template.html'),
            scope: {
                item: '=',
                options: '='
            },
            link: linker
        };

        function linker($scope, element) {
            if ($scope.options) {
                $scope.isActiveActions = $scope.options.isActiveActions || false;
            }
        }
    }

})();