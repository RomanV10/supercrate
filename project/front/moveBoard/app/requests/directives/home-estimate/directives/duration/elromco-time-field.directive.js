(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elromcoTimeDurationField', elromcoTimeDurationField);

    elromcoTimeDurationField.$inject = ['datacontext'];

    function elromcoTimeDurationField(datacontext) {
        return {
            restrict: 'AE',
            scope: {
                field: '=',
                updateMessage: '=onUpdateMessage',
                removeMessage: '=onRemoveMessage'
            },
            link: linker
        };

        function linker(scope, element, attrs, ngModel) {
            element.timepicker({
                'disableTouchKeyboard': true,
                'minTime': '00:30',
                'maxTime': '24:00',
                'timeFormat': 'H:i',
                'step': 15,
            });

            element.bind('change', function () {
                var value = element.val();
                var oldVal = scope.field.old;
                scope.field.value = value;

                if (scope.updateMessage && value != oldVal) {
	                updateMessage(scope.field);
                }

                if (scope.removeMessage && value == oldVal) {
                    scope.removeMessage(scope.field);
                }

                scope.$apply();
            });

	        function updateMessage(field) {
		        if (!field) return;

		        scope.updateMessage({
			        field: field,
		        });
	        }
        }
    }

})();
