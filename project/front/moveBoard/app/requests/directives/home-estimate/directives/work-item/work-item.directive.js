import './work-item.styl'
import './work-item-tooltip.styl'

(function () {
    'use strict';

    angular.module('move.requests')
           .directive('elHomeEstimateWorkItem', workItem);

    workItem.$inject = ['common', '$rootScope', 'tripRequest', '$timeout'];

    function workItem(common, $rootScope, tripRequest, $timeout) {
        const DEFAULT_LEFT_COEFFICIENT = 25;
        const DEFAULT_WIDTH_COEFFICIENT = 50;

        return {
            restrict: 'E',
            template: require('./work-item.template.html'),
            scope: {
                item: '=',
                options: '='
            },
            link: linker
        };

        function linker($scope, element) {
            $scope.serviceTypes = $rootScope.fieldData.field_lists.field_move_service_type;
            $scope.openRequest = tripRequest.openRequest;
            init();

            function init() {
                let left = DEFAULT_LEFT_COEFFICIENT * _.round(common.convertTime($scope.item.home_estimate_actual_start.value || '12:00 AM'), 2);
                let width = DEFAULT_WIDTH_COEFFICIENT * _.round(common.convertStingToIntTime($scope.item.home_estimate_time_duration.value || '00:00'), 2);

                element.find('.home-estimate-work-item').css('left', left).css('width', width);

                if ($scope.item.home_estimate_date.value) {
                    $scope.item.homeEstimateDate = moment($scope.item.home_estimate_date.value).format('MMMM DD, YYYY');
                }
            }

            $scope.$watch('item', onUpdateWorkItem, true);

            function onUpdateWorkItem() {
                init();
            }

            // use in tooltip
            $scope.closeTooltip = (e) => {
            	// fix bug with apply already in progress
            	$timeout(() => {
		            element.find('.home-estimate-work-item').click();
	            });
	        };
        }
    }

})();
