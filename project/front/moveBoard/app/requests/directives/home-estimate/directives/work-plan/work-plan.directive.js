import './work-plan.styl'

(function () {
    'use strict';

    angular.module('move.requests')
           .directive('elHomeEstimateWorkPlan', workPlan);

    workPlan.$inject = ['common', 'SweetAlert'];

    function workPlan(common, SweetAlert) {
        const DEFAULT_HEIGHTS = 25;
        const THIRTY = 30;
        const FIVE = 5;

        return {
            restrict: 'E',
            template: require('./work-plan.template.html'),
            scope: {
                requests: '=',
                request: '=',
                options: '=',
                homeEstimators: '=',
                curDate: '=',
                updateMessage: '=onUpdateMessage',
                removeMessage: '=onRemoveMessage',
            },
            link: linker
        };

        function linker($scope, element) {
            if ($scope.options) {
                $scope.isActiveActions = $scope.options.isActiveActions || false;
            }

            let requestNid = $scope.request && $scope.request.nid ? $scope.request.nid : 0;

            var times = common.getTimeArray();
            $scope.times = angular.copy(times);
            $scope.times.length = $scope.times.length * 2;
            let firstInit = true;
            let homeEstimatorIds = _.keys($scope.homeEstimators);

            $scope.selectWorker = selectWorker;

            $scope.$watch('requests', init, true);

            init();

            function init() {
                if (firstInit) {
                    let workersCount = _.size($scope.homeEstimators);
                    let height = THIRTY + workersCount * DEFAULT_HEIGHTS + FIVE * workersCount;
                    element.find('.home-estimate-work-plan-container').css('height', height);
                    element.find('.dhx_matrix_line .tblines').css('height', height);

                    if ($scope.isActiveActions) {
                        var dragableElement = element.find('.home-estimate-day-plan');
                        dragableElement.css('left', -245);
                        dragableElement.draggable(
                            {axis: "x"},
                            {containment: [120, 200, 570, 200]},
                            {handle: ".dhx_cal_header"},
                            {drag: function () {}},
                            {stop: function () {}}
                        );
                    }

                    firstInit = false;
                }

                initHomeEstimators();
                initRequestItems();
            }

            function initHomeEstimators() {
                if ($scope.isActiveActions) {
                    _.each($scope.homeEstimators, function (estimator, id) {
                        estimator.active = false;
                    });

                    let isEnableHomeEstimator = $scope.request.home_estimator.value && $scope.homeEstimators[$scope.request.home_estimator.value];

                    if (!isEnableHomeEstimator && $scope.request.home_estimator.value) {
                        let homeEstimatorName = `${$scope.request.home_estimator.first_name} ${$scope.request.home_estimator.last_name}`;
                        let text = `Home estimator ${homeEstimatorName} was disabled from department`;
                        toastr.info(text, 'Home estimator disabled');
                    }


                    if (isEnableHomeEstimator && isCurrentMomentSameToRequestMomemnt()) {
                        $scope.homeEstimators[$scope.request.home_estimator.value].active = true;
                    }
                }
            }

            function isCurrentMomentSameToRequestMomemnt() {
                let result = false;

                if ($scope.request && $scope.request.home_estimate_date.value) {
                    let currentMoment = moment($scope.curDate);
                    let requestMoment = moment($scope.request.home_estimate_date.value);

                    result = currentMoment.isSame(requestMoment)
                }

                return result;
            }

            function initRequestItems() {
                // TODO Reinit ids for home estimate schedule
                homeEstimatorIds = _.keys($scope.homeEstimators);
                $scope.requestItems = {};
                checkCurrentRequestForDisplay();

                _.each(homeEstimatorIds, function (estimatorId) {
                    if (!$scope.requestItems[estimatorId]) {
                        $scope.requestItems[estimatorId] = [];
                    }

                    _.each($scope.requests, function (request) {
                        if (request.home_estimator && request.home_estimator.value == estimatorId) {
                            request.isCurrent = request.nid == requestNid;

                            if (request.isCurrent && $scope.request) {
                                $scope.request.isCurrent = true;
                                $scope.requestItems[estimatorId].push($scope.request);
                            } else {
                                $scope.requestItems[estimatorId].push(request);
                            }
                        }
                    });
                })
            }

            function checkCurrentRequestForDisplay() {
                if ($scope.isActiveActions && $scope.request && $scope.request.home_estimate_date.value) {
                    let isSameCurrentMomentToRequest = isCurrentMomentSameToRequestMomemnt();

                    if (_.isArray($scope.requests)) {
                        let requestIndex = _.findIndex($scope.requests, {nid: requestNid});

                        if (isSameCurrentMomentToRequest && requestIndex == -1) {
                            $scope.requests.push($scope.request);
                        }

                        if (!isSameCurrentMomentToRequest && requestIndex >= 0) {
                            $scope.requests.splice(requestIndex, 1);
                        }
                    } else {
                        if (isSameCurrentMomentToRequest && !$scope.requests[requestNid]) {
                            $scope.requests[requestNid] = $scope.request;
                        }

                        if (!isSameCurrentMomentToRequest && $scope.requests[requestNid]) {
                            delete $scope.requests[requestNid]
                        }
                    }
                }
            }

            function selectWorker(id) {
                if ($scope.isActiveActions && cheskIsEmptyHomeEstimateDate()) {
                    if (isCurrentMomentSameToRequestMomemnt()) {
                        let isUncheck = false;

                        removeCurrentRequest();

                        if ($scope.request.home_estimator.value != id) {
                            $scope.request.home_estimator.value = id;
                            $scope.request.isCurrent = true;
                            $scope.requestItems[id].push($scope.request);
                        } else {
                            $scope.request.home_estimator.value = null;
                            isUncheck = true;
                        }

                        if ($scope.request.home_estimator.old != id || isUncheck) {
	                        updateMessage($scope.request.home_estimator);
                        }

                        if ($scope.request.home_estimator.old == id && !isUncheck) {
                            $scope.removeMessage($scope.request.home_estimator);
                        }

                        initHomeEstimators();
                    } else {
                        SweetAlert
                            .swal({
                                    title: getChangeMoveDateTitle(),
                                    type: "warning",
                                    showCancelButton: true,
                                    cancelButtonText: "No",
                                    confirmButtonText: "Yes"
                                },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.request.home_estimate_date.value = moment($scope.curDate).format('MMMM D, YYYY');
	                                    updateMessage($scope.request.home_estimate_date);

                                        if ($scope.request.home_estimator.value != id) {
                                            $scope.request.home_estimator.value = id;
	                                        updateMessage($scope.request.home_estimator);
                                        }

                                        init();
                                    }
                                });
                    }
                }
            }

            function cheskIsEmptyHomeEstimateDate() {
                let result = true;

                if ($scope.request && !$scope.request.home_estimate_date.value) {
                    toastr.warning('Please select home estimate date');
                    result = false;
                }

                return result;
            }

            function removeCurrentRequest() {
                _.each(homeEstimatorIds, function (id) {
                    if ($scope.requestItems[id]) {
                        let index = _.findIndex($scope.requestItems[id], {nid: requestNid});

                        if (index >= 0) {
                            $scope.requestItems[id].splice(index, 1);
                        }
                    }
                });
            }

            function getChangeMoveDateTitle() {
                let currentDateText = moment($scope.curDate).format('MMMM D, YYYY');
                var result = `Do you wanna choose ${currentDateText} home estimate date?`;

                return result;
            }

	        function updateMessage(field) {
		        if (!field) return;

		        $scope.updateMessage({
			        field: field,
		        });
	        }
        }
    }

})();
