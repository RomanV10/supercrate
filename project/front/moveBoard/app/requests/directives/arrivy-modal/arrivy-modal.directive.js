import "./arrivy-modal.styl"

'use strict';

angular.module('move.requests').directive('arrivyModal', arrivyModal);

arrivyModal.$inject = ['$uibModal', '$sce'];

function arrivyModal($uibModal, $sce) {
	return {
		restrict: 'A',
		scope: {
			arrivyLink: '<'
		},
		link: arrivyModalLink
	};
	
	function arrivyModalLink($scope, $element) {
		$scope.arrivyLink = $sce.trustAsResourceUrl($scope.arrivyLink);
		
		$element.bind('click', (e) => {$scope.openModal($scope.arrivyLink)});
		$scope.openModal = openModal;
		
		function openModal(url) {
			let modalInstance = $uibModal.open({
				template: require('./arrivy-modal.tpl.pug'),
				controller: arrivyModalController,
				size: 'lg',
				windowClass: 'arrivyModal_windowClass',
				backdrop: false,
				keyboard: false,
				resolve: {
					url: function () {
						return url;
					}
				},
			});
		}
		
		function arrivyModalController($scope, url) {
			$scope.url = url;
		}
	}
}