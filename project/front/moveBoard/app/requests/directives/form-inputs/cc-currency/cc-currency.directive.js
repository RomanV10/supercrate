'use strict';

angular
	.module('move.requests')
	.directive('ccCurrency', ccCurrency);

function ccCurrency($filter) {
	return {
		restrict: 'A',
		require: '^ngModel',
		scope: true,
		link: function (scope, el, attrs, ngModelCtrl) {

			var reservedVal = 0;

			function formatter(value) {
				value = value ? parseFloat(value.toString().replace(/[^0-9._-]/g, '')) || 0 : 0;
				var formattedValue = $filter('currency')(value);
				el.val(formattedValue);

				ngModelCtrl.$setViewValue(value);

				return formattedValue;
			}

			ngModelCtrl.$formatters.push(formatter);
			
			function onFocus() {
				if (attrs.readonly) {
					return false;
				}
				reservedVal = el.val();
				el.val('');
			}
			
			function onBlur() {
				var val = (el.val() != '' && parseFloat(el.val()) != NaN && _.isNumber(parseFloat(el.val())))
					? parseFloat(el.val())
					: reservedVal;

				formatter(val);
			}
			
			el.bind('focus', onFocus);
			
			el.bind('blur', onBlur);
		}

	};

}
