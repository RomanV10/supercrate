'use strict';

angular
	.module('move.requests')
	.directive('ccInputInvoice', ccInputInvoice);

	ccInputInvoice.$inject = ['browserService'];

function ccInputInvoice(browserService) {
	return {
		restrict: 'A',
		scope: {
			'message': '=',
			'field': '=',
			'editrequest': '=',

		},
		link: function (scope, element) {

			let allowedEvent = browserService.isIEorEDGE() ? 'blur' : 'change';
			element.bind(allowedEvent, onChange);

			function onChange() {

				//BIND CHANGE FUNCTION
				if (scope.editrequest.invoice) {
					if (!_.isEmpty(scope.$parent.invoice)) {
						if (scope.field[1] == 'work_time') {
							scope.message[scope.field[1]] = {
								label: "Actual Work Time",
								oldValue: scope.editrequest.invoice[scope.field[1]],
								newValue: element.val()
							};

							scope.editrequest.invoice[scope.field[1]] = element.val();
						} else {
							switch (scope.field[0].field) {
								case 'field_long_distance_rate':
									scope.message[scope.field[0].field] = {
										label: 'Long Distance Rate',
										oldValue: `$${scope.field[0].old}`,
										newValue: `$${element.val()}`
									};
									break;
								case 'field_price_per_hour':
									scope.message[scope.field[0].field] = {
										label: scope.field[0].label,
										oldValue: `$${scope.field[0].old}`,
										newValue: `$${element.val()}`
									};
									break;
								case 'field_estimated_prise':
									scope.message[scope.field[0].field] = {
										label: scope.field[0].label,
										newValue: `$${element.val()}`
									};
									break;
								default:
									scope.message[scope.field[0].field] = {
										label: scope.field[0].label,
										oldValue: scope.field[0].old,
										newValue: element.val()
									};
							}

							scope.editrequest.invoice[scope.field[1]] = scope.field[0];
							scope.editrequest.invoice[scope.field[1]].value = element.val();

							if (scope.field[0].field == 'field_actual_start_time' || scope.field[0].field == "field_start_time_max") {
								scope.$parent.parklotControl.updateStartTime(scope.field[0].field, element.val());
							}

							if (scope.field[0].field == 'field_price_per_hour' || scope.field[0].field == "field_long_distance_rate") {
								scope.$parent.updateQuote();
							}
						}
					}
				} else {
					scope.editrequest[scope.field.field] = element.val();

					switch (scope.field.field) {
						case 'field_long_distance_rate':
							scope.message[scope.field.field] = {
								label: 'Long Distance Rate',
								oldValue: `$${scope.field.old}`,
								newValue: `$${element.val()}`
							};
							break;
						case 'field_price_per_hour':
							scope.message[scope.field.field] = {
								label: scope.field.label,
								oldValue: `$${scope.field.old}`,
								newValue: `$${element.val()}`
							};
							break;
						case 'field_estimated_prise':
							scope.message[scope.field.field] = {
								label: scope.field.label,
								newValue: `$${element.val()}`
							};
							break;
						default:
							scope.message[scope.field.field] = {
								label: scope.field.label,
								oldValue: scope.field.old,
								newValue: element.val()
							};
					}

					scope.field.value = element.val();

					if (scope.field.field == 'field_actual_start_time' || scope.field.field == "field_start_time_max") {
						scope.$parent.parklotControl.updateStartTime(scope.field.field, element.val());
					}

					if (scope.field.field == 'field_price_per_hour') {
						scope.$parent.updateQuote();
					}

					scope.$parent.messageInvoice[scope.field.field] = angular.copy(scope.message[scope.field.field]);
				}
			}

		}
	};

}
