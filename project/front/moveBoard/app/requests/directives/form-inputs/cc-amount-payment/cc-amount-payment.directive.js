'use strict';

angular
	.module('move.requests')
	.directive('ccAmountPayment', ccAmountPayment);

function ccAmountPayment() {
	return {
		restrict: 'A',
		require: '^ngModel',
		scope: {
			'ngModel': '='
		},
		link: function (scope, el, attrs, ngModelCtrl) {
			var baseVal = 0;
			
			function formatter(value) {
				if (value == undefined) {
					value = 0;
				}
				
				var formattedValue = null;
				formattedValue = '$ ' + Number(value).toFixed(2);
				el.val(formattedValue);
				ngModelCtrl.$setViewValue(_.round(value, 2));
			}
			
			formatter(scope.ngModel);
			
			function onFocus() {
				baseVal = el.val();
				el.val('');
			}
			
			function onBlur() {
				formatter(scope.ngModel);
			}
			
			function onChange() {
				var val = el.val() != '' ? el.val() : baseVal;
				formatter(parseFloat(val));
			}
			
			el.bind('focus', onFocus);
			
			el.bind('focusout', onBlur);
			
			el.bind('change', onChange);
		}
	};
}