'use strict';

angular
	.module('move.requests')
	.directive('ccInput', ccInput);

function ccInput() {
	return {
		restrict: 'A',
		scope: {
			'message': '=',
			'field': '=',
			'editrequest': '=',
			
		},
		link: function (scope, element) {
			
			function onChange() {
				
				scope.editrequest[scope.field.field] = element.val();
				scope.message[scope.field.field] = {
					label: scope.field.label,
					oldValue: scope.field.old,
					newValue: element.val(),
					
				};
				scope.field.value = element.val();
				
				if (scope.field.field == 'field_actual_start_time' || scope.field.field == "field_start_time_max") {
					scope.$parent.parklotControl.updateStartTime(scope.field.field, element.val());
				}
				
				if (scope.field.field == 'field_price_per_hour') {
					scope.$parent.updateQuote();
					scope.$parent.request.reservation_rate = element.val();
					$("#reserv_price").val(element.val());
				}
			}
			
			element.bind('change', onChange);
		}
	};
	
}