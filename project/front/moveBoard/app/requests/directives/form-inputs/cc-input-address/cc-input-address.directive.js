'use strict';

angular
	.module('move.requests')
	.directive('ccInputAddress', ccInputAddress);

ccInputAddress.$inject = ['CalculatorServices', 'SweetAlert', 'geoCodingService'];

function ccInputAddress(CalculatorServices, SweetAlert, geoCodingService) {
	return {
		restrict: 'A',
		scope: {
			'message': '=',
			'field': '=',
			'subfield': "@",
			'type': '@',
			'editrequest': '='
		},
		link: function (scope, element) {
			function functionToBind() {
				scope.editrequest[scope.type] = scope.field;
				scope.editrequest[scope.type][scope.subfield] = element.val();

				if (scope.subfield == "postal_code") {
					var postal_code = element.val();
					element.removeClass('error');

					if (postal_code.length == 5) {
						geoCodingService.geoCode(element.val())
							.then(function (result) {
								scope.field.locality = result.city;
								scope.field.administrative_area = result.state;
								
								if (scope.type == "field_extra_pickup" || scope.type == "field_extra_dropoff") {
									if (scope.field.organisation_name == null) {
										scope.field.organisation_name = 1;
									}
								}
								
								scope.editrequest[scope.type] = scope.field;
								
								return result;
							}, function () {
								SweetAlert.swal("You entered the wrong zip code.", '', 'error');
								element.addClass('error');
								element.val('');
							});
					} else {
						element.addClass('error');
						element.val('');
					}
				}
			}

			element.bind('change', functionToBind);

		}
	};

}
