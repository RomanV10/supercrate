export function GET_ADDRESS_MSG (field, message, typeOfEntrance) {
	if (!_.isNull(field.thoroughfare)) {
		message += `${field.thoroughfare}, `;
	}
	if (!_.isNull(field.premise)) {
		message += `${field.premise}, `;
	}
	if (!_.isNull(field.locality)) {
		message += `${field.locality}, `;
	}
	if (!_.isNull(field.administrative_area)) {
		message += field.administrative_area;
	}

	if (!_.isNull(field.postal_code)) {
		message += `, ${field.postal_code}`;
	} else {
		message = 'None'
	}

	if (_.get(field, 'organisation_name') && !_.isNull(field.organisation_name) && typeOfEntrance) {
		message += `, ${typeOfEntrance[field.organisation_name]}`;
	}

	return message;
}
