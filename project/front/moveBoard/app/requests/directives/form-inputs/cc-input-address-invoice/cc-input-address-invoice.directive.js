'use strict';

import {
	GET_ADDRESS_MSG
} from '../commonFunctions';

angular
	.module('move.requests')
	.directive('ccInputAddressInvoice', ccInputAddressInvoice);

/*@ngInject*/
function ccInputAddressInvoice(CalculatorServices, $rootScope, $q, SweetAlert, datacontext, geoCodingService) {
	return {
		restrict: 'A',
		scope: {
			'message': '=',
			'field': '=',
			'subfield': '@',
			'type': '@',
			'editrequest': '=',
			'validateZip': '&',
			'ccChange': '&'
		},
		link: function (scope, element) {
			let previousValue;
			let oldValue;
			let fieldData = datacontext.getFieldData();
			const WRONG_ZIP_MSG = {
				title: 'You entered the wrong zip code.',
				text: 'Zip code was not changed'
			};
			let reverted = false;

			function getAddressString(field) {
				let message = '';

				if (field) {
					if (scope.type == 'field_extra_pickup' || scope.type == 'field_extra_dropoff') {
						message = GET_ADDRESS_MSG(field, message, fieldData.field_lists.field_type_of_entrance_from);
					} else if (scope.type == 'field_moving_from') {
						message = GET_ADDRESS_MSG(field, message);
					} else if (scope.type == 'field_moving_to') {
						message = GET_ADDRESS_MSG(field, message);
					}
				}

				return message;
			}

			function msgLogRequest(label) {
				var message = getAddressString(scope.field);
				if (getAddressString(oldValue) != message) {
					if (!scope.message[scope.type]) {
						scope.message[scope.type] = {};
					}
					scope.message[scope.type].label = label;

					if (message != '') {
						scope.message[scope.type].newValue = message;
					}
					scope.message[scope.type].previousValueString = getAddressString(previousValue);
					scope.message[scope.type].oldValueString = getAddressString(oldValue);
					scope.message[scope.type].previousValue = previousValue;
					scope.message[scope.type].oldValue = oldValue;
					scope.message[scope.type].previousPostalCode = previousValue.postal_code;
					scope.message[scope.type].lastChangeTime = moment().unix();
				}
			}

			function msgLogInvoice(label) {
				var message = getAddressString(scope.field[0]);
				if (getAddressString(oldValue) != message) {
					if (!scope.message[scope.type]) {
						scope.message[scope.type] = {};
					}
					scope.message[scope.type].label = label;

					if (message != '') {
						scope.message[scope.type].newValue = message;
					}
					scope.message[scope.type].previousValueString = getAddressString(previousValue);
					scope.message[scope.type].oldValueString = getAddressString(oldValue);
					scope.message[scope.type].previousValue = previousValue;
					scope.message[scope.type].oldValue = oldValue;
					scope.message[scope.type].previousPostalCode = previousValue.postal_code;
					scope.message[scope.type].lastChangeTime = moment().unix();
				}
			}

			function getMessageLabel() {
				var label = '';
				var words = _.words(scope.type.replace(/_/g, ' '));
				_.forEach(words, function (word, i) {
					if (i > 0) {
						word = word.replace(/\w\S*/g, function (txt) {
							return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
						});
						label += word + ' ';
					}
				});
				return label;
			}

			element.ready(function () {
				oldValue = scope.field[0] ? angular.copy(scope.field[0]) : angular.copy(scope.field);
			}).bind('focus', function () {
				previousValue = scope.field[0] ? angular.copy(scope.field[0]) : angular.copy(scope.field);
			}).bind('change input', function () {
				var label = getMessageLabel();
				if (scope.field[0]) {
					if (!_.isEmpty(scope.$parent.invoice)) {
						scope.editrequest.invoice[scope.type] = scope.field[0];
						scope.editrequest.invoice[scope.type][scope.subfield] = element.val();

						if (scope.subfield == 'thoroughfare'
							|| scope.subfield == 'premise'
							|| scope.subfield == 'organisation_name') {
							scope.field[0][scope.subfield] = element.val();
							scope.editrequest.invoice[scope.type] = scope.field[0];
							scope.ccChange({fieldName: scope.type});
						}

						msgLogInvoice(label);

						if (scope.subfield == 'postal_code' && element.val().length == 5) {
							element.removeClass('error');
							let promiseGeoCode = geoCodingService.geoCode(element.val());
							let promiseAreaCode = CalculatorServices.getLongDistanceCode(scope.field[0].postal_code);
							$q.all([promiseGeoCode, promiseAreaCode])
								.then(function (result) {
									if (result[0].isWrong) {
										badResponse(previousValue[0]);
									} else {
										if (reverted) {
											getOptimisticResponseForInvoice(label, previousValue, result);
										} else {
											scope.validateZip({
												from: scope.type === 'field_moving_from' ? element.val() : scope.$parent.invoice.field_moving_from.postal_code,
												to: scope.type === 'field_moving_to' ? element.val() : scope.$parent.invoice.field_moving_to.postal_code,
												stateFrom: scope.type === 'field_moving_from' ? result[0].state : scope.$parent.invoice.field_moving_from.administrative_area,
												stateTo: scope.type === 'field_moving_to' ? result[0].state : scope.$parent.invoice.field_moving_to.administrative_area,
												areaCode: scope.type === 'field_moving_to' ? result[1] : scope.$parent.invoice.field_moving_to.premise,
											}).then(() => {
												getOptimisticResponseForInvoice(label, previousValue, result);
											}, rejectMsg => {
												badResponse(previousValue, rejectMsg);
											});
										}
									}
								});
						}
					}
				} else {

					if (scope.subfield == 'thoroughfare'
						|| scope.subfield == 'premise'
						|| scope.subfield == 'organisation_name') {
						scope.field[scope.subfield] = element.val();
						scope.editrequest[scope.type] = scope.field;
						scope.ccChange({fieldName: scope.type});
					}
					msgLogRequest(label);

					if (scope.subfield == 'postal_code' && element.val().length == 5) {
						element.removeClass('error');
						let promiseGeoCode = geoCodingService.geoCode(element.val());
						let promiseAreaCode = CalculatorServices.getLongDistanceCode(element.val());

						$q.all([promiseGeoCode, promiseAreaCode]).then(function (result) {
							if (result[0].isWrong) {
								badResponse(previousValue);
							} else {
								if (reverted) {
									getOptimisticResponse(result, label);
								} else {
									scope.validateZip({
										from: scope.type === 'field_moving_from' ? element.val() : scope.$parent.request.field_moving_from.postal_code,
										to: scope.type === 'field_moving_to' ? element.val() : scope.$parent.request.field_moving_to.postal_code,
										stateFrom: scope.type === 'field_moving_from' ? result[0].state : scope.$parent.request.field_moving_from.administrative_area,
										stateTo: scope.type === 'field_moving_to' ? result[0].state : scope.$parent.request.field_moving_to.administrative_area,
										areaCode: scope.type === 'field_moving_to' ? result[1] : scope.$parent.request.field_moving_to.premise,
									}).then(() => {
										getOptimisticResponse(result, label);
									}, rejectMsg => {
										badResponse(previousValue, rejectMsg);
									});
								}
							}
						}, () => {
							badResponse(previousValue);
						});
					}
				}
			})
				.bind('change', () => {
					const ZIP_LENGTH = 5;
					let revertData = _.get(previousValue, '[0]', previousValue);
					if (scope.subfield == 'postal_code'
						&& element.val().length !== ZIP_LENGTH) {
						badResponse(revertData);
					}
					if (scope.subfield == 'locality' && !element.val().length) {
						element.val(revertData.locality);
					}
				});

			function getOptimisticResponseForInvoice(label, previousValue, result) {
				scope.field[0].locality = result[0].city;
				scope.field[0].administrative_area = result[0].state;
				msgLogInvoice(label, previousValue);
				if (scope.type == 'field_extra_pickup' || scope.type == 'field_extra_dropoff') {
					if (scope.field[0].organisation_name == null) {
						scope.field[0].organisation_name = 1;
					}
				} else {
					scope.field[0].premise = result[1];
				}
				msgLogInvoice(label);
				scope.editrequest.invoice[scope.type] = scope.field[0];
				$rootScope.$broadcast('postal.code.updated');
				reverted = false;
				scope.ccChange({fieldName: scope.type});
			}

			function getOptimisticResponse(result, label) {
				scope.editrequest[scope.type] = scope.field;
				scope.editrequest[scope.type][scope.subfield] = element.val();

				scope.field.locality = result[0].city;
				scope.field.administrative_area = result[0].state;

				if (scope.type == 'field_extra_pickup' || scope.type == 'field_extra_dropoff') {
					if (scope.field.organisation_name == null) {
						scope.field.organisation_name = 1;
					}
				} else {
					scope.field.premise = result[1];
				}

				msgLogRequest(label);
				scope.editrequest[scope.type] = scope.field;
				$rootScope.$broadcast('postal.code.updated');
				reverted = false;
				scope.ccChange({fieldName: scope.type});
			}

			function badResponse(previous, errorMessage = WRONG_ZIP_MSG) {
				element.val(previous.postal_code);
				scope.field = angular.copy(previous);
				SweetAlert.swal(errorMessage.title, errorMessage.text, 'error');
				scope.editrequest[scope.type] = scope.field;
				if (scope.$parent.request.status.raw == 3) {
					scope.$parent.invoice[scope.type].postal_code = angular.copy(previous.postal_code);
				}
				element.triggerHandler('input');
				reverted = true;
			}

		}
	};

}
