'use strict';

angular
	.module('move.requests')
	.directive('ccDatePickerInvoice', ccDatePickerInvoice);

function ccDatePickerInvoice(datacontext, SweetAlert, $timeout) {
	return {
		restrict: 'A',
		require: '?ngModel',
		scope: {
			'message': '=',
			'field': '=',
			'editrequest': '=',
			'calendar': '='
		},
		link: function (scope, element, attrs, ngModel) {
			var calendar = datacontext.getFieldData().calendar;
			var calendartype = datacontext.getFieldData().calendartype;

			var refresh = (attrs.refresh === "true");
			element.datepicker({
				disableTouchKeyboard: true,
				dateFormat: 'MM d, yy',
				numberOfMonths: 2,
				changeMonth: false,
				changeYear: false,
				showButtonPanel: true,
				minDate: new Date(),
				onSelect: onSelect,
				beforeShowDay: checkDate
			});
			
			function onSelect(date, obj) {
				// var time = date.getTime(); //"06/16/2015",
				if (obj.settings.beforeShowDay(new Date(moment(date, 'MMMM D, YYYY')))[1] == "Block this day") {
					SweetAlert.swal({
						title: "Error!",
						text: "This day is blocked",
						type: "error",
						timer: 2000,
					});
					while (obj.settings.beforeShowDay(new Date(moment(date, 'MMMM D, YYYY')))[1] == "Block this day") {
						date = moment(date, 'MMMM D, YYYY').add(1, 'day').format('MMMM D, YYYY');
					}
					obj.selectedDay = (new Date(date)).getDate();
					obj.selectedMonth = (new Date(date)).getMonth();
					obj.selectedYear = (new Date(date)).getFullYear();
					element[0].value = date;
				}
				if (scope.$parent.request.inventory.move_details.delivery && moment(date).isAfter(scope.$parent.request.inventory.move_details.delivery)) {
					showChangeDeliveryDayConfirm(date)
				} else {
					scope.date = date;
					angular.element('.delivery-days-calendar').datepicker("option", "minDate", scope.date);
					initNewDate();
					scope.$apply()
				}
				function showChangeDeliveryDayConfirm(date) {
					SweetAlert.swal({
							title: "Delivery date can't be greater than first available delivery date",
							text: "Do you want to change the first available delivery date?",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, let's do it!",
							cancelButtonText: "No, cancel pls!",
							closeOnConfirm: true,
							closeOnCancel: true
						},
						function (isConfirm) {
							if (isConfirm) {
								scope.date = date;
								angular.element('.delivery-days-calendar').datepicker("option", "minDate", scope.date);
								var oldDDay = angular.copy(scope.$parent.request.inventory.move_details.delivery);
								scope.$parent.request.inventory.move_details.delivery = scope.date;
								scope.$parent.checkDeliveryDay(oldDDay);
								initNewDate()
							} else {
								scope.date = moment(new Date(scope.field.old)).format('MMMM D, YYYY');
								ngModel.$setViewValue(scope.date);
								ngModel.$render();
							}
						});
					
				}
				function initNewDate() {
					scope.date = date;
					var dprv = (new Date(scope.field.value)).getTime();
					var formated_date = $.datepicker.formatDate('yy-mm-dd', new Date(date), {});
					if (scope.editrequest.invoice) {
						if (!_.isEmpty(scope.$parent.invoice) && scope.$parent.Invoice) {
							scope.editrequest.invoice[scope.field[1]] = {date: formated_date, time: '15:10:00'};
							scope.editrequest.invoice[scope.field[1]].value = date;
							scope.editrequest.invoice[scope.field[1]].raw = dprv;
							scope.field[0].value = formated_date;
							var oldValue = angular.isDefined(scope.field[0].old) ? moment(new Date(scope.field[0].old)).format('MMM D, YYYY') : "";
							var newValue = moment(date, 'MMM D, YYYY').format('MMMM D, YYYY');
							if (!_.isEqual(oldValue, newValue)) {
								scope.message[scope.field[0].field] = {
									label: scope.field[0].label,
									oldValue: oldValue,
									newValue: newValue
								};
							}
							scope.$parent.parklotControl.reinitParklot(formated_date);
							scope.$apply();
							scope.$parent.updateRate();
						}
					} else {
						scope.editrequest[scope.field.field] = {date: formated_date, time: '15:10:00'};
						scope.field.value = formated_date;
						
						var oldValue = angular.isDefined(scope.field.old) ? moment(new Date(scope.field.old)).format('MMMM D, YYYY') : "";
						var newValue = moment(date, 'MMM D, YYYY').format('MMMM D, YYYY');
						
						if (!_.isEqual(oldValue, newValue)) {
							scope.message[scope.field.field] = {
								label: scope.field.label,
								oldValue: oldValue,
								newValue: newValue
							};
						}
						if (scope.$parent.Invoice) {
							//add new value in invoice
							var msg = angular.copy(scope.message[scope.field.field]) || {};
							msg.oldValue = scope.$parent.invoice.date.old;
							scope.$parent.messageInvoice[scope.field.field] = msg;
						}
						if (scope.field.field == "field_delivery_date") {
							if (scope.$parent.Invoice) {
								scope.$parent.invoice.ddate = angular.copy(scope.field);
								scope.$parent.invoice.ddate.raw = (new Date(scope.field.value)).getTime();
								scope.$parent.deliveryDateInputInvoice = date;
								scope.$parent.editRequestinvoice.invoice.ddate = angular.copy(scope.field);
								$('#deliveryDateInputInvoice').datepicker();
								$('#deliveryDateInputInvoice').datepicker('setDate', new Date(date));
								scope.$parent.changeRequestField('ddate');
							}
							if (scope.$parent.request.delivery_date_from.value != false) {
								if (scope.$parent.request.delivery_date_to.value == false) {
									scope.$parent.request.delivery_date_to.value = scope.$parent.request.delivery_date_from.value;
								}
								
								let momentCurrentDDate = moment(scope.$parent.request.ddate.value);
								let dateFrom = moment(scope.$parent.request.delivery_date_from.value, 'DD-MM-YYYY');
								let dateTo = moment(scope.$parent.request.delivery_date_to.value, 'DD-MM-YYYY');
								let isCorrectDate = momentCurrentDDate.isBetween(dateFrom, dateTo)
									|| momentCurrentDDate.isSame(dateFrom)
									|| momentCurrentDDate.isSame(dateTo);
								
								if (!isCorrectDate) {
									toastr.warning('Delivery Date must be the same or between in Delivery Dates range', 'Warning!')
								}
							}
						}
						if (scope.field.field == "field_date") {
							var firstFormatDate = scope.field.value.indexOf("/") != -1 ? "MM/DD/YYYY" : "YYYY-MM-DD";
							var movedateFirst = moment(scope.field.value, firstFormatDate);
							scope.field.raw = movedateFirst.unix();
							scope.$parent.request.updateRequestDate = true; // make for correct remove truck
							if (scope.$parent.Invoice) {
								scope.$parent.invoice.date = angular.copy(scope.field);
								scope.$parent.invoice.date.raw = (new Date(scope.field.value)).getTime();
								scope.$parent.moveDateInputInvoice = date;
								scope.$parent.editRequestinvoice.invoice.date = angular.copy(scope.field);
								$('#moveDateInputInvoice').datepicker();
								$('#moveDateInputInvoice').datepicker('setDate', new Date(date));
							}
							scope.$parent.changeRequestField('date');
							scope.$parent.parklotControl.reinitParklot(formated_date);
							
						}
						var newDigestCycle = $timeout(() => {
							scope.$apply()
						});
						scope.$on("$destroy", () => {
								$timeout.cancel(newDigestCycle);
							}
						);
						scope.$parent.updateRate();
					}
					
					//INIT SHEDULE FOR THIS DAY
					var curdate = $.datepicker.formatDate('MM d, yy', new Date(dprv), {});
					if (refresh) {
						scope.$parent.parklotControl.reinitParklot(formated_date);
						$(".curdate").text(curdate);
					}
				}
			}
			
			function checkDate(date) {
				var day = moment(date).format('DD');
				var month = moment(date).format('MM');
				var year = date.getFullYear();
				var date = year + '-' + month + '-' + day;

				if (calendar[year][date] == 'block_date') {
					return [false, 'not-available', 'This date is NOT available'];
				} else {
					return [true, calendartype[calendar[year][date]], calendartype[calendar[year][date]]];
				}
			}
		}
	};
}

