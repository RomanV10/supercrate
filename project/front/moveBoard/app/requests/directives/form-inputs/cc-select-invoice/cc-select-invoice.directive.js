'use strict';

angular
	.module('move.requests')

	.directive('ccSelectInvoice', ccSelectInvoice);

/*@ngInject*/
function ccSelectInvoice($rootScope, InventoriesServices, Raven, SweetAlert, serviceTypeService) {
	return {
		restrict: 'A',
		scope: {
			'editrequest': '=editrequest',
			'message': '=message',
			'field': '=field',
			'ngModel': '=ngModel'

		},
		link: function (scope, element) {
			element.bind('change', onChange);
			element.bind('click', onClick);

			function onChange() {
				if (scope.field[1] == 'move_size') {
					let checkIfChangeMoveSize = InventoriesServices.checkIfChangeMoveSize(scope.$parent.request.nid, element.val());
					checkIfChangeMoveSize
						.then(result => {
							let hasError = result.status_code != 200;
							if (hasError) {
								element.val(scope.field[0].raw);
								let rooms = '';
								result.data.forEach(room => {
									rooms += `${room.name}\n`
								});
							}
						})
						.catch(() => {
							SweetAlert.swal('Move size was not updated', 'Try Again', 'error');
							Raven.captureException('Move size was not updated');
						});
				}

				continueProcessChange();

				function continueProcessChange() {
					let serviceNames = serviceTypeService.getAllServiceTypesAsSimpleObject();
					if (scope.editrequest.invoice) {
						if (!_.isEmpty(scope.$parent.invoice)) {
							let dataField = scope.$root.fieldData.field_lists[scope.field[0].field];

							if (angular.isUndefined(dataField)) {
								dataField = scope.$root.fieldData.field_lists['field_type_of_entrance_to_'];
							}

							if (scope.field[0].field == "field_move_service_type") {
								dataField = serviceNames;
							}

							scope.field[0].raw = element.val();
							scope.$parent.invoice[scope.field[1]].raw = element.val();
							scope.$parent.invoice[scope.field[1]].value = dataField[element.val()];
							scope.message[scope.field[0].field] = {
								label: scope.field[0].label,
								oldValue: dataField[scope.field[0].old],
								newValue: dataField[element.val()]
							};
							scope.editrequest.invoice[scope.field[1]] = scope.field[0];
							scope.editrequest.invoice[scope.field[1]].raw = element.val();
							scope.editrequest.invoice[scope.field[1]].value = dataField[element.val()];
							scope.$parent.changeTypeOfServiceInvoice();

							if (element.val() == '5' && scope.field[0].field == "service_type") {
								scope.$parent.flatrateInvoice = true;
							}

							if (element.val() == '7' && scope.field[0].field == "service_type") {
								scope.$parent.longDistanceInvoice = true;
							}

							scope.$parent.changeRequestField(scope.field[0].field);

							if (element.val() == '7' && scope.field[1] == "service_type") {
								$rootScope.$broadcast('update.LDQuote');
							}
						}
					} else {
						let dataField = scope.$root.fieldData.field_lists[scope.field[0].field];

						if (angular.isUndefined(dataField)) {
							dataField = scope.$root.fieldData.field_lists['field_type_of_entrance_to_'];
						}

						if (scope.field[0].field == "field_move_service_type") {
							dataField = serviceNames;
						}

						scope.editrequest[scope.field[0].field] = element.val();
						scope.field[0].raw = element.val();
						scope.field[0].value = dataField[element.val()];

						if (element.val() == '7' && scope.field[1] == "service_type") {
							$rootScope.$broadcast('update.LDQuote');
						}

						scope.message[scope.field[0].field] = {
							label: scope.field[0].label,
							oldValue: dataField[scope.field[0].old],
							newValue: dataField[element.val()]
						};

						scope.$parent.changeRequestField(scope.field[0].field);
					}

					scope.$parent.edit = true;
				}
			}

			function onClick() {
				if (scope.$parent.isInvalidParsingRequest) {
					if (_.isUndefined(scope.$parent.editrequest[scope.field[0].field])) {
						scope.$parent.editrequest[scope.field[0].field] = element.val();
						scope.$apply();
					}
				}
			}
		}
	};
}
