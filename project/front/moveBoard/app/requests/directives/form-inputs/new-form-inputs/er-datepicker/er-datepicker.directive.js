'use strict';
import './er-datepicker.styl';

angular
	.module('move.requests')
	.directive('erDatepicker', erDatepicker);

function erDatepicker() {
	return {
		restrict: 'E',
		template: require('./er-datepicker.tpl.pug'),
		scope: {
			'erModel': '=',
			'monthCount': '<',
			'getDayClass': '&',
			'cbOnSelect': '&',
		},
		controller: erDatepickerCtrl
	};
	
	function erDatepickerCtrl($scope) {
		if (!$scope.monthCount) $scope.monthCount = 1;
		let now = moment();
		$scope.initDate = moment($scope.erModel);
		if (!$scope.initDate.isValid()) {
			let tryToFixInitDate = moment($scope.initDate.toDate());
			if (tryToFixInitDate.isValid()) $scope.initDate = tryToFixInitDate;
			else $scope.initDate = moment();
		}
		$scope.$watch('erModel', onChangeModel);
		
		$scope.select = select;
		$scope.nextMonth = nextMonth;
		$scope.prevMonth = prevMonth;
		
		init();
		
		function onChangeModel() {
			$scope.initDate = moment($scope.erModel);
			if (!$scope.initDate.isValid()) $scope.initDate = moment();
			init();
		}
		
		function init() {
			$scope.monthes = [];
			$scope.daysOfWeek = [];
			let firstOfMonth = angular.copy($scope.initDate).startOf('month');
			for (let i = 0; i < +$scope.monthCount; i++) {
				$scope.monthes.push(makeMonth(firstOfMonth));
				firstOfMonth.add(1, 'month');
			}
			
			let firstOfWeek = angular.copy($scope.initDate).startOf('week');
			for (let i = 0; i < 7; i++) {
				$scope.daysOfWeek.push(firstOfWeek.format('dd'));
				firstOfWeek.add(1, 'day');
			}
		}
		
		function makeMonth(firstOfMonth) {
			let weeks = [];
			let label = firstOfMonth.format('MMMM, YYYY');
			let firstOfWeek = angular.copy(firstOfMonth).startOf('week');
			for (let i = 0; i < 5; i++) {
				weeks.push(makeWeek(firstOfWeek, firstOfMonth));
				firstOfWeek.add(1, 'weeks');
			}
			return {weeks, label};
		}
		
		function makeWeek(firstOfWeek, firstOfMonth) {
			let week = [];
			let currentDate = angular.copy(firstOfWeek);
			for (let i = 0; i < 7; i++) {
				week.push(makeDay(currentDate, firstOfMonth));
				currentDate.add(1, 'days');
			}
			return week;
		}
		
		function makeDay(currentDate, firstOfMonth) {
			let dayObj = {
				moment: moment(currentDate),
				date: currentDate.date(),
				timestamp: currentDate.unix(),
				className: $scope.getDayClass({date: currentDate}),
				attributeString: moment(currentDate).format('YYYY-MM-DD'),
			};
			
			if (currentDate.isBefore(now, 'day')) {
				dayObj.className += ' past';
			}
			
			if (currentDate.isSame(firstOfMonth, 'month')) {
				if (currentDate.isSame($scope.erModel, 'day')) {
					dayObj.className += ' active';
				}
			} else {
				dayObj.className += ' differentMonth';
			}
			return dayObj;
		}
		
		function select(date) {
			$scope.erModel.set('year', date.year());
			$scope.erModel.set('month', date.month());
			$scope.erModel.set('date', date.date());
			$scope.cbOnSelect({date});
		}
		
		function nextMonth() {
			$scope.initDate.add(1, 'month');
			init();
		}
		
		function prevMonth() {
			$scope.initDate.subtract(1, 'month');
			init();
		}
		
	}
}