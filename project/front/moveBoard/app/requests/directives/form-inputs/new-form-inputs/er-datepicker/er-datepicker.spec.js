describe('Unit: er-datepicker.directive,', () => {
	let $localScope, element, $scope;
	let currentMoment, etalonMoment;
	const MONTH_FORMAT = 'MMMM, YYYY';

	beforeEach(() => {
		$scope = $rootScope.$new();
		currentMoment = moment();
		etalonMoment = currentMoment.clone();

		$scope.selectedDate = currentMoment;
		$scope.getDayClass = () => 'testClass';
		$scope.selectDay = () => true;

		spyOn($scope, 'getDayClass').and.callThrough();
		spyOn($scope, 'selectDay').and.callThrough();

		element = $compile(`<er-datepicker
			er-model="selectedDate"
			month-count="2"
			get-day-class="getDayClass(date)"
			cb-on-select="selectDay(date)">`)($scope);

		$localScope = element.isolateScope();
	});

	describe('After initializing,', () => {
		it('Should show selectedDate in first month', () => {
			expect(etalonMoment.format(MONTH_FORMAT)).toEqual($localScope.monthes[0].label);
		});
		it('Should show 2 month', () => {
			expect($localScope.monthes.length).toEqual(2);
		});
		it('2nd month should be proper', () => {
			let nextMonth = moment(etalonMoment).add(1, 'month').format(MONTH_FORMAT);
			expect($localScope.monthes[1].label).toEqual(nextMonth);
		});
		it('Days must have proper classes', () => {
			expect(~$localScope.monthes[0].weeks[0][0].className.indexOf('testClass')).toBeTruthy();
		});
		it('Should call get-day-class', () => {
			expect($scope.getDayClass).toHaveBeenCalled();
		});
	});

	describe('function: select,', () => {
		let clickedDay;
		beforeEach(() => {
			clickedDay = $localScope.monthes[1].weeks[3][4];
			$localScope.select(clickedDay.moment);
		});

		it('Should call cb-on-select', () => {
			expect($scope.selectDay).toHaveBeenCalled();
		});
		it('Should update parent\'s model', () => {
			expect($localScope.erModel.isSame(clickedDay.moment, 'day')).toBeTruthy();
		});
		it('Should update $scope.erModel model', () => {
			expect($scope.selectedDate.isSame(clickedDay.moment, 'day')).toBeTruthy();
		});
	});

	describe('function: nextMonth,', () => {
		beforeEach(() => {
			$localScope.nextMonth();
		});

		it('Should show month after selectedDate in first month', () => {
			expect(moment(etalonMoment).add(1, 'month').format(MONTH_FORMAT)).toEqual($localScope.monthes[0].label);
		});
		it('Should show 2 month', () => {
			expect($localScope.monthes.length).toEqual(2);
		});
		it('2nd month should be proper', () => {
			let nextMonth = moment(etalonMoment).add(2, 'month').format(MONTH_FORMAT);
			expect($localScope.monthes[1].label).toEqual(nextMonth);
		});
		it('Days must have proper classes', () => {
			expect(~$localScope.monthes[0].weeks[0][0].className.indexOf('testClass')).toBeTruthy();
		});
		it('Should call get-day-class', () => {
			expect($scope.getDayClass).toHaveBeenCalled();
		});
	});

	describe('function: prevMonth,', () => {
		beforeEach(() => {
			$localScope.prevMonth();
		});

		it('Should show month after selectedDate in first month', () => {
			expect(moment(etalonMoment).subtract(1, 'month').format(MONTH_FORMAT)).toEqual($localScope.monthes[0].label);
		});
		it('Should show 2 month', () => {
			expect($localScope.monthes.length).toEqual(2);
		});
		it('2nd month should be proper', () => {
			let nextMonth = etalonMoment.format(MONTH_FORMAT);
			expect($localScope.monthes[1].label).toEqual(nextMonth);
		});
		it('Days must have proper classes', () => {
			expect(~$localScope.monthes[0].weeks[0][0].className.indexOf('testClass')).toBeTruthy();
		});
		it('Should call get-day-class', () => {
			expect($scope.getDayClass).toHaveBeenCalled();
		});
	});

});
