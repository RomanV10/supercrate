'use strict';
import './er-datepicker-input.styl';

angular
	.module('move.requests')
	.directive('erDatepickerInput', erDatepickerInput);

function erDatepickerInput() {
	
	return {
		restrict: 'E',
		template: require('./er-datepicker-input.tpl.pug'),
		scope: {
			'validatorAsync': '&',
			'erValue': '<',
			'erChange': '&',
			'allowPast': '@',
			'format': '@'
		},
		controller: erDatepickerInputCtrl
	};
	
	function erDatepickerInputCtrl($scope, $element, formInputsServices) {
		const DATE_FORMAT_DEFAULT = $scope.format ? $scope.format : 'MMMM D, YYYY';
		const MOMENT_IS_SAME_LIMIT = 'day';
		const allowPast = $scope.allowPast == 'allow';
		$scope.isCalendarVisible = false;
		$scope.inputValue = getStringValue($scope.erValue);
		$scope.selectedDate = getMomentValue($scope.inputValue);
		
		$scope.getDayClass = getDayClass;
		$scope.openCalendar = openCalendar;
		
		$scope.$watch('erValue', onChangeValue);
		$scope.$watch('selectedDate', selectedDateChanged, true);
		$scope.$watch('inputValue', onChangeInput);
		
		function onChangeInput(newVal, oldVal) {
			let typedDate = moment(newVal, DATE_FORMAT_DEFAULT);
			if (typedDate.isValid()) {
				$scope.selectedDate = typedDate;
			}
		}
		
		function getDayClass(date) {
			let preClass = formInputsServices.getDayClass(date);
			if (!checkForPast(date))
				preClass += ' disabled';
			return preClass;
		}
		
		function onChangeValue(newVal, oldVal) {
			$scope.inputValue = getStringValue(newVal);
			let oldValMoment = getMomentValue(oldVal);
			let newValMoment = getMomentValue(newVal);
			if (!newValMoment.isSame(oldValMoment, MOMENT_IS_SAME_LIMIT)) {
				$scope.selectedDate = newValMoment;
			}
		}
		
		function getMomentValue(value) {
			if (moment.isMoment(value)) return value;
			else if (_.isDate(value)) return moment(value);
			else return moment(value, DATE_FORMAT_DEFAULT);
		}
		
		function getStringValue(value) {
			if (moment.isMoment(value)) return value.format(DATE_FORMAT_DEFAULT);
			else if (_.isDate(value)) return moment(value).format(DATE_FORMAT_DEFAULT);
			else return value;
		}
		
		function openCalendar() {
			$scope.isCalendarVisible = true;
			angular.element('body').bind('click', checkForClose);
		}
		
		function checkForClose(event) {
			if (!$element.find(event.target).length) {
				closeDatepicker();
			}
		}
		
		function closeDatepicker() {
			$scope.isCalendarVisible = false;
			angular.element('body').unbind('click', checkForClose);
		}
		
		function selectedDateChanged(newVal, oldVal) {
			let currentDate = moment($scope.erValue, DATE_FORMAT_DEFAULT);
			if (newVal.isSame(currentDate, MOMENT_IS_SAME_LIMIT)) return;
			
			closeDatepicker();
			validateDate(newVal);
		}
		
		function validateDate(dateObj) {
			let oldDateString = getStringValue($scope.erValue);
			let oldDateObj = moment(oldDateString, DATE_FORMAT_DEFAULT);
			if (dateObj.isValid()) {
				let firstFreeDay = formInputsServices.findFirstUnblockedDay(dateObj);
				if (!firstFreeDay.isSame(dateObj, MOMENT_IS_SAME_LIMIT)) {
					selectDayManual(firstFreeDay);
				} else {
					if (dateObj.isSame(oldDateObj, MOMENT_IS_SAME_LIMIT)) {
						$scope.selectedDate = oldDateObj;
					} else {
						callValidator(dateObj, oldDateString, oldDateObj);
					}
				}
			} else {
				let tryToFixMoment = moment($scope.selectedDate.toDate());
				if (tryToFixMoment.isValid()) $scope.selectedDate = tryToFixMoment;
			}
		}
		
		function callValidator(dateObj, oldDateString, oldDateObj) {
			let validatorPromise = $scope.validatorAsync({date: dateObj});
			let isReallyPromise = validatorPromise && validatorPromise.then;
			if (isReallyPromise) {
				validatorPromise.then(() => {
					$scope.erChange({
						newVal: dateObj,
						oldVal: $scope.erValue,
					});
				}, () => {
					$scope.selectedDate = oldDateObj;
				});
			} else {
				$scope.erChange({
					newVal: dateObj,
					oldVal: oldDateString,
				});
			}
		}
		
		function checkForPast(date) {
			return allowPast || !date.isBefore(moment(), 'day');
		}
		
		function selectDayManual(date) {
			$scope.selectedDate = date;
		}
	}
}