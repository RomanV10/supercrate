describe('Unit: er-datepicker-input.directive,', () => {
	let $localScope, element, $scope, formInputsServices, moment_lib;
	let currentMoment, etalonMoment;
	
	const DATE_FORMAT_DEFAULT = 'MMMM D, YYYY';
	const DATE_COMPARE_PRECISION = 'day';
	
	function goodValidator(date) {
		return {
			then: (res, rej) => res()
		};
	}
	
	beforeEach(inject((_formInputsServices_, _moment_) => {
		moment_lib = _moment_;
		$scope = $rootScope.$new();
		formInputsServices = _formInputsServices_;
		currentMoment = moment_lib('2018-03-05');
		spyOn(window, 'moment').and.callFake(function(parameter) {
			if (parameter) {
				return moment_lib(parameter);
			} else {
				return currentMoment;
			}
		});
		etalonMoment = moment(currentMoment);
		
		$scope.valueInParent = etalonMoment.toDate();
		$scope.validator = goodValidator;
		$scope.callback = (newVal) => {
			dateInCallback = newVal;
			return true;
		};
		
		element = $compile(`<er-datepicker-input
				er-value="valueInParent"
				validator-async="validator(date)"
				er-change="callback(newVal, oldVal)"/>`)($scope);
		
		$localScope = element.isolateScope();
		$localScope.$apply();
	}));
	
	describe('After initializing,', () => {
		it('Calendar should be closed', () => {
			expect($localScope.isCalendarVisible).toBeFalsy();
		});
		it('Should copy erValue to inputValue', () => {
			expect($localScope.inputValue).toEqual(moment($localScope.erValue).format(DATE_FORMAT_DEFAULT));
		});
	});
	
	describe('function: getDayClass,', () => {
		beforeEach(() => {
			spyOn(formInputsServices, 'getDayClass').and.callFake(() => {
				return 'testClass';
			});
			$localScope.getDayClass(currentMoment);
		});
		
		it('Should call formInputsServices.getDayClass', () => {
			expect(formInputsServices.getDayClass).toHaveBeenCalled();
		});
	});
	
	describe('function: openCalendar,', () => {
		beforeEach(() => {
			$localScope.openCalendar();
		});
		
		it('Should set isCalendarVisible to true', () => {
			expect($localScope.isCalendarVisible).toBeTruthy();
		});
	});
	
	describe('When changed $localScope.selectedDate', () => {
		describe('When date is after current,', () => {
			let newSelectedDate;
			beforeEach(() => {
				newSelectedDate = moment(currentMoment).add(6, 'days');
				$localScope.selectedDate = angular.copy(newSelectedDate);
				$localScope.$apply();
			});
			
			it('Should set new selectedDate', () => {
				expect($localScope.selectedDate.isSame(newSelectedDate, DATE_COMPARE_PRECISION)).toBeTruthy();
			});
		});
	});
	
	describe('when input changed manually,', () => {
		describe('When date is after current,', () => {
			beforeEach(() => {
				$localScope.inputValue = moment(currentMoment).add(6, 'days').format(DATE_FORMAT_DEFAULT);
				$localScope.$apply();
			});
			
			it('Should set new selectedDate', () => {
				let selectedDate = $localScope.selectedDate;
				let inputValue = moment($localScope.inputValue, DATE_FORMAT_DEFAULT, true);
				
				expect(selectedDate.isSame(inputValue, DATE_COMPARE_PRECISION)).toBeTruthy();
			});
		});
	});
});