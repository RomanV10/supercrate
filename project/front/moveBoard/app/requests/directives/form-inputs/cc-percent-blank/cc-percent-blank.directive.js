'use strict';

angular
	.module('move.requests')
	.directive('ccPercentBlank', ccPercentBlank);

function ccPercentBlank($filter) {
	return {
		restrict: 'A',
		require: '^ngModel',
		scope: true,
		link: function (scope, el, attrs, ngModelCtrl) {
			var reservedValue;

			function formatter(value) {
				value = value ? parseFloat(value.toString().replace(/[^0-9._-]/g, '')) || '' : value;
				var formattedValue = $filter('number')(parseFloat(value), 0) + '%';
				el.val(formattedValue);

				ngModelCtrl.$setViewValue(value);

				return formattedValue;
			}

			ngModelCtrl.$formatters.push(formatter);

			el.bind('focus', function () {
				reservedValue = el.val();
				el.val('');
			});

			el.bind('blur', function () {
				var value = el.val();
				formatter(value);
			});
		}

	};

}
