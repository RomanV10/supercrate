'use strict';

angular
	.module('move.requests')
	.directive('ccStartTimeInvoice', ccStartTimeInvoice);

function ccStartTimeInvoice($rootScope) {
	return {
		restrict: 'A',
		scope: {
			'message': '=message',
			'field': '=field',
			'editrequest': '=editrequest'

		},
		link: function (scope, element, attrs) {
			element.timepicker({
				'disableTouchKeyboard': true,
				'minTime': '00:00am',
				'maxTime': '11:30pm',
				'timeFormat': 'g:i A'
			}).keydown(false);

			element.bind('change', onChange);

			function onChange() {
				let elementValue = element.val();
				let previousValue;

				if (scope.editrequest.invoice) {
					previousValue = scope.field[0].old;

					if (!_.isEmpty(scope.$parent.invoice)) {
						scope.message[scope.field[0].field] = {
							label: scope.field[0].label,
							oldValue: scope.field[0].old,
							newValue: elementValue,
							previousValue: previousValue,
							lastChangeTime: moment().unix()
						};

						if (scope.$parent.previousTemp && scope.$parent.previousTemp[scope.field[1]]) {
							scope.$parent.previousTemp[scope.field[1]] = previousValue;
						}

						scope.editrequest.invoice[scope.field[1]] = scope.field[0];
						scope.editrequest.invoice[scope.field[1]].value = elementValue;
						scope.$parent.parklotControl.updateStartTime(scope.field.field, elementValue);

						if (previousValue != elementValue) {
							$rootScope.$broadcast('check.overbooking');
						}
					}
				} else {
					previousValue = scope.editrequest[scope.field.field] || scope.field.old;
					scope.editrequest[scope.field.field] = angular.copy(elementValue);
					scope.message[scope.field.field] = {
						label: scope.field.label,
						oldValue: scope.field.old,
						newValue: elementValue,
						previousValue: previousValue,
						lastChangeTime: moment().unix()
					};

					if (scope.$parent.previousTemp && scope.$parent.previousTemp[scope.field.field]) {
						scope.$parent.previousTemp[scope.field.field] = previousValue;
					}

					//add new value in invoice
					scope.$parent.messageInvoice[scope.field.field] = angular.copy(scope.message[scope.field.field]);
					scope.$parent.parklotControl.updateStartTime(scope.field.field, elementValue);

					if (previousValue != elementValue) {
						$rootScope.$broadcast('check.overbooking');
					}
				}
			}
		}
	};
}
