'use strict';

angular
	.module('move.requests')
	.directive('ccRoomsInvoice', ccRoomsInvoice);

function ccRoomsInvoice() {
	return {
		restrict: 'A',
		scope: {
			'message': '=message',
			'field': '=field',
			'editrequest': '=editrequest',
		},
		link: function (scope, element) {
			scope.rooms = ['living room', 'dining room', 'office', 'extra room', 'basement', 'garage', 'patio', 'play room'];
			$("#edit-furnished-rooms-invoice").chosen({width: "89% !important"});

			if (!_.isEmpty(scope.$parent.invoice)) {
				if (scope.$parent.invoice.rooms && angular.isArray(scope.field)) {
					jQuery.each(scope.field[0].value, function (index, item) {
						element.find("option[value=" + item + "]").attr('selected', 'selected');
						element.trigger("chosen:updated");
					});

					element.chosen({disable_search_threshold: 10});

					element.bind('change', function () {
						//BIND CHANGE FUNCTION
						scope.editrequest.invoice = scope.field[0];
						scope.message[scope.field[0].field] = {
							label: scope.field[0].label,
							oldValue: [],
							newValue: []
						};
						var count = 0;

						for (var i in scope.field.old) {
							count++;
							scope.message[scope.field[0].field].oldValue += scope.rooms[scope.field[0].old[i] - 1];
							if (scope.field[0].old.length > 1 && scope.field[0].old.length != count) {
								scope.message[scope.field[0].field].oldValue += ', ';
							}
						}

						var newVal = element.val();
						count = 0;

						for (var j in newVal) {
							count++;
							scope.message[scope.field[0].field].newValue += scope.rooms[newVal[j] - 1];
							if (newVal.length > 1 && newVal.length != count) {
								scope.message[scope.field[0].field].newValue += ', ';
							}
						}
					});
				} else {
					if (!angular.isArray(scope.field) && angular.isDefined(scope.field)) {
						jQuery.each(scope.field.raw, function (index, item) {
							element.find("option[value=" + item + "]").attr('selected', 'selected');
						});

						element.chosen({disable_search_threshold: 10});

						element.bind('change', function () {
							//BIND CHANGE FUNCTION
							scope.editrequest[scope.field.field] = element.val() || [];
							scope.message[scope.field.field] = {
								label: scope.field.label,
								oldValue: '',
								newValue: ''
							};
							var count = 0;

							for (var i in scope.field.old) {
								count++;
								scope.message[scope.field.field].oldValue += scope.rooms[scope.field.old[i] - 1];
								if (scope.field.old && scope.field.old.length > 1 && scope.field.old.length != count) {
									scope.message[scope.field.field].oldValue += ', ';
								}
							}

							var newVal = element.val() || [];
							count = 0;

							for (var j in newVal) {
								count++;
								scope.message[scope.field.field].newValue += scope.rooms[newVal[j] - 1];
								if (newVal && newVal.length > 1 && newVal.length != count) {
									scope.message[scope.field.field].newValue += ', ';
								}
							}

							$("#edit-furnished-rooms-invoice").chosen({width: "89% !important"});
							jQuery.each(scope.editrequest[scope.field.field], function (index, item) {
								$('#edit-furnished-rooms-invoice').find("option[value=" + item + "]").attr('selected', 'selected');
								$('#edit-furnished-rooms-invoice').trigger("chosen:updated");
							});
							//add new value in invoice
							scope.$parent.messageInvoice[scope.field.field] = angular.copy(scope.message[scope.field.field]);
						});
					}
				}
			} else {
				//Selecte Rooms
				if (angular.isDefined(scope.field)) {
					jQuery.each(scope.field.raw, function (index, item) {
						element.find("option[value=" + item + "]").attr('selected', 'selected');
					});

					element.chosen({disable_search_threshold: 10});

					element.bind('change', function () {
						//BIND CHANGE FUNCTION
						scope.editrequest[scope.field.field] = element.val();
						scope.message[scope.field.field] = {
							label: scope.field.label,
							oldValue: [],
							newValue: []
						};
						var count = 0;

						for (var i in scope.field.old) {
							count++;
							scope.message[scope.field.field].oldValue += scope.rooms[scope.field.old[i] - 1];
							if (scope.field.old && scope.field.old.length > 1 && scope.field.old.length != count) {
								scope.message[scope.field.field].oldValue += ', ';
							}
						}

						var newVal = element.val();
						count = 0;

						for (var j in newVal) {
							count++;
							scope.message[scope.field.field].newValue += scope.rooms[newVal[j] - 1];
							if (newVal && newVal.length > 1 && newVal.length != count) {
								scope.message[scope.field.field].newValue += ', ';
							}
						}

						jQuery.each(scope.editrequest[scope.field.field], function (index, item) {
							$('#edit-furnished-rooms-invoice').find("option[value=" + item + "]").attr('selected', 'selected');
							$('#edit-furnished-rooms-invoice').trigger("chosen:updated");
						});
						//add new value in invoice
						scope.$parent.messageInvoice[scope.field.field] = angular.copy(scope.message[scope.field.field]);
					});
				}
			}
		}
	};
}
