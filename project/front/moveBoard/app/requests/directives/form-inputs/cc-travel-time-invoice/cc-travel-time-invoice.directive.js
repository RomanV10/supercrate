'use strict';

angular
	.module('move.requests')
	.directive('ccTravelTimeInvoice', ccTravelTimeInvoice);

function ccTravelTimeInvoice(common, $rootScope) {
	return {
		restrict: 'A',
		scope: {
			'message': '=message',
			'field': '=field',
			'editrequest': '=editrequest',

		},
		link: function (scope, element) {
			element.timepicker({
				'disableTouchKeyboard': true,
				'minTime': '00:00',
				'maxTime': '99:00',
				'timeFormat': 'H:i',
				'step': 15,
			}).keydown(false);

			element.bind('change', onChange);

			function onChange() {
				if (scope.editrequest.invoice) {
					if (!_.isEmpty(scope.$parent.invoice)) {
						if (_.isUndefined(scope.editrequest.invoice[scope.field[1]])) {
							scope.editrequest.invoice[scope.field[1]] = {};
						}

						if (_.isUndefined(scope.editrequest.invoice[scope.field[1]]).value) {
							scope.editrequest.invoice[scope.field[1]].value = {};
						}

						let previousValue;

						if (_.isNumber(scope.editrequest.invoice[scope.field[1]].value)) {
							previousValue = common.decToTime(scope.editrequest.invoice[scope.field[1]].value);
						} else {
							previousValue = scope.editrequest.invoice[scope.field[1]].value || scope.field[1].old;
						}

						scope.editrequest.invoice[scope.field[1]].value = element.val();
						scope.editrequest.invoice[scope.field[1]].raw = _.round(common.convertStingToIntTime(element.val()), 2);
						scope.message[scope.field[0].field] = {
							label: scope.field[0].label,
							oldValue: scope.field[0].old,
							newValue: element.val(),
							previousValue: previousValue,
							lastChangeTime: moment().unix()
						};

						if (scope.$parent.previousTemp && scope.$parent.previousTemp[scope.field[1]]) {
							scope.$parent.previousTemp[scope.field[1]] = previousValue;
						}

						//add new value in invoice
						if (_.isUndefined(scope.field[0].value)) {
							scope.field[0].value = {};
						}

						if (_.isUndefined(scope.field[0].raw)) {
							scope.field[0].raw = {};
						}

						scope.field[0].value = element.val();
						scope.field[0].raw = _.round(common.convertStingToIntTime(element.val()), 2);
						scope.$parent.parklotControl.updateWorkTime(scope.field[0].field, element.val());
						scope.$parent.updateQuote();

						if (previousValue != element.val()) {
							$rootScope.$broadcast('check.overbooking');
						}
					}
				} else {
					let previousValue;

					if (_.isNumber(scope.editrequest[scope.field.field])) {
						previousValue = common.decToTime(scope.editrequest[scope.field.field]);
					} else {
						previousValue = scope.editrequest[scope.field.field] || scope.field.old;
					}

					scope.editrequest[scope.field.field] = _.round(common.convertStingToIntTime(element.val()), 2);
					scope.message[scope.field.field] = {
						label: scope.field.label,
						oldValue: _.isNumber(scope.field.old) ? common.decToTime(scope.field.old) : scope.field.old,
						newValue: element.val(),
						previousValue: previousValue,
						lastChangeTime: moment().unix()
					};

					if (scope.$parent.previousTemp && scope.$parent.previousTemp[scope.field.field]) {
						scope.$parent.previousTemp[scope.field.field] = previousValue;
					}

					//add new value in invoice
					scope.$parent.messageInvoice[scope.field.field] = angular.copy(scope.message[scope.field.field]);
					scope.field.value = element.val();
					scope.field.raw = _.round(common.convertStingToIntTime(element.val()), 2);
					scope.$parent.parklotControl.updateWorkTime(scope.field.field, element.val());
					scope.$parent.updateQuote();

					if (previousValue != element.val()) {
						$rootScope.$broadcast('check.overbooking');
					}
				}
			}
		}
	};
}
