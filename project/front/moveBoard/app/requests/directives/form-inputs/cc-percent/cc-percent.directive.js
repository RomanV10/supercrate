'use strict';

angular
	.module('move.requests')
	.directive('ccPercent', ccPercent);

function ccPercent($filter) {
	return {
		restrict: 'A',
		require: '^ngModel',
		scope: true,
		link: function (scope, el, attrs, ngModelCtrl) {
			var reservedValue;

			function formatter(value) {
				value = value ? parseFloat(value.toString().replace(/[^0-9._-]/g, '')) || 0 : 0;
				var formattedValue = $filter('number')(parseFloat(value)) + '%';
				el.val(formattedValue);

				ngModelCtrl.$setViewValue(value);

				return formattedValue;
			}

			ngModelCtrl.$formatters.push(formatter);

			el.bind('focus', function () {
				reservedValue = el.val();
				el.val('');
			});

			el.bind('blur', function () {
				var value = el.val() || 0;

				formatter(value);
			});
		}

	};

}
