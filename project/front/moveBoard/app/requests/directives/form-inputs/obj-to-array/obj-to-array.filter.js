'use strict';

angular
	.module('move.requests')
	.filter('objToArray', objToArray);

function objToArray() {
	return function (obj) {
		if (!(obj instanceof Object)) {
			return obj;
		}

		return _.map(obj, function (val, key) {
			return Object.defineProperty(val, '$key', {__proto__: null, value: key});
		});
	}
}
