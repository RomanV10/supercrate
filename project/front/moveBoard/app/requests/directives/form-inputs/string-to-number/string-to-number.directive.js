'use strict';

angular
	.module('move.requests')
	.directive("stringToNumber", stringToNumber);

function stringToNumber() {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function (value) {
				let parsedValue = parseFloat(value, 10);
				if (_.isNaN(parsedValue)) parsedValue = 0;
				ngModel.$setViewValue(parsedValue);
				ngModel.$render();
				return '' + parsedValue;
			});

			ngModel.$formatters.push(function (value) {
				let formattedValue = parseFloat(value, 10);
				if (_.isNaN(formattedValue)) formattedValue = 0;
				return formattedValue;
			});
		}
	};
}
