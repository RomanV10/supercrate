'use strict';

angular
	.module('move.requests')
	.directive('ccPercentAndCurrency', ccPercentAndCurrency);

function ccPercentAndCurrency($filter) {
	return {
		restrict: 'A',
		require: '^ngModel',
		scope: true,
		link: function (scope, el, attrs, ngModelCtrl) {
			
			function formatter(value) {
				value = value ? parseFloat(value.toString().replace(/[^0-9._-]/g, '')) || 0 : 0;
				
				var formattedValue;
				// var percent = ['Additional Service', 'Commission from total', 'Extras Commission', 'Insurance', 'Packing Commission'];
				var percent = ['Extras Commission', 'Commission from total', 'Packing Commission'];
				scope.$watch('rateCommission[$index].option', rateCommissionChanged);
				function rateCommissionChanged(newValue, oldValue) {
					if (newValue) {
						if (percent.indexOf(newValue) > -1) {
							formattedValue = $filter('number')(parseFloat(value), 2) + '%';
							el.val(formattedValue);
							ngModelCtrl.$setViewValue(value);
							return formattedValue;
						} else {
							formattedValue = $filter('currency')(value);
							el.val(formattedValue);
							ngModelCtrl.$setViewValue(value);
							return formattedValue;
						}
					}
				}
			}
			
			ngModelCtrl.$formatters.push(formatter);
			
			el.bind('focus', function () {
				el.val('');
			});
			
			el.bind('blur', function () {
				formatter(el.val());
			});
		}
		
	};
	
}