'use strict';

angular
	.module('move.requests')
	.directive('positiveDecimalNumber', positiveDecimalNumber);

function positiveDecimalNumber() {
	return {
		restrict: "A",
		require: '?ngModel',
		link: function (scope, element, attrs, ngModelCtrl) {
			if (!ngModelCtrl) {
				return;
			}

			ngModelCtrl.$parsers.push(function (val) {
				if (val === null)
					return;

				var regex = /^[0-9]+(\.[0-9]{1,2})?/;

				if (regex.test(val)) {
					ngModelCtrl.$setViewValue(val);
					ngModelCtrl.$render();
					return val;
				} else {
					ngModelCtrl.$setViewValue('');
					ngModelCtrl.$render()
				}
			});
		}
	}
}
