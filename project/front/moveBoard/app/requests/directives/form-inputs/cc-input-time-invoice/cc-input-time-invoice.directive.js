'use strict';

angular
	.module('move.requests')
	.directive('ccInputTimeInvoice', ccInputTimeInvoice);

/* @ngInject */
function ccInputTimeInvoice(common, $rootScope, $q, SweetAlert) {
	return {
		restrict: 'A',
		scope: {
			'message': '=',
			'field': '=',
			'editrequest': '=',
			'request': '=request',
		},
		link: function (scope, element, attrs) {
			let $super = scope.$parent;
			element.timepicker({
				'disableTouchKeyboard': true,
				'minTime': '00:30',
				'maxTime': '24:00',
				'timeFormat': 'H:i',
				'step': 15,
			}).keydown(false);
			
			element.bind('change', () => {
				changeMaximumWorkTime()
					.then(onChange);
			});
			
			function onChange() {
				let elementValue = element.val();
				let previousValue;

				if (scope.editrequest.invoice) {
					if (!_.isEmpty($super.invoice)) {
						if (scope.field[1] == 'work_time') {
							scope.message[scope.field[1]] = {
								label: "Actual Work Time",
								oldValue: $super.invoice.work_time_old,
								newValue: elementValue,
							};
							scope.editrequest.invoice[scope.field[1]] = scope.field[0];
							// scope.field[0].value  = elementValue;
							$super.invoice.work_time_int = _.round(common.convertStingToIntTime(element.val()), 2);
							$super.parklotControl.updateWorkTime(scope.field[0].field, element.val());
							$super.updateQuote();
						} else {
							if (isChangeMinimumOrMaximumTime()) {
								if (!_.isNaN(Number(scope.field[0].old))) {
									scope.field[0].old = common.decToTime(Number(scope.field[0].old));
								}
							}

							if (_.isNumber(scope.editrequest[scope.field.field])) {
								previousValue = common.decToTime(scope.editrequest[scope.field.field]);
							} else {
								previousValue = scope.editrequest[scope.field.field] || scope.field.old;
							}

							scope.message[scope.field[0].field] = {
								label: scope.field[0].label,
								oldValue: scope.field[0].old,
								newValue: elementValue,
								previousValue: previousValue,
								lastChangeTime: moment().unix()
							};

							if ($super.previousTemp && $super.previousTemp[scope.field[0]]) {
								$super.previousTemp[scope.field[0]] = previousValue;
							}

							if (!_.isEmpty(scope.editrequest.invoice)
								&& !_.isUndefined(scope.editrequest.invoice[scope.field[1]])) {
								scope.message[scope.field[0].field].previousValue = scope.editrequest.invoice[scope.field[1]].value || scope.field[1].old;
							}
							
							scope.field[0].value = elementValue;
							
							if (angular.isUndefined(scope.editrequest.invoice[scope.field[1]])) {
								scope.editrequest.invoice[scope.field[1]] = {};
							}
							
							scope.editrequest.invoice[scope.field[1]] = scope.field[0];
							scope.editrequest.invoice[scope.field[1]].raw = _.round(common.convertStingToIntTime(element.val()), 2);
							scope.editrequest.invoice[scope.field[1]].value = elementValue;
							
							$super.parklotControl.updateWorkTime(scope.field[0].field, element.val());
							$super.updateQuote();
							if (elementValue != previousValue) {
								$rootScope.$broadcast('check.overbooking');
							}
						}
					}
				} else {
					if (angular.isDefined(scope.field)) {
						if (isChangeMinimumOrMaximumTime()) {
							if (!_.isNaN(Number(scope.field.old))) {
								scope.field.old = common.decToTime(Number(scope.field.old));
							}
							previousValue = scope.editrequest[scope.field.field] || scope.field.old;
							scope.editrequest[scope.field.field] = _.round(common.convertStingToIntTime(element.val()), 2);
							scope.message[scope.field.field] = {
								label: scope.field.label,
								oldValue: scope.field.old,
								newValue: elementValue,
								previousValue: previousValue,
								lastChangeTime: moment().unix()
							};
							scope.field.value = elementValue;
							$super.messageInvoice[scope.field.field] = angular.copy(scope.message[scope.field.field]);
							$super.parklotControl.updateWorkTime(scope.field.field, element.val());
							$super.updateQuote();
							if (previousValue != element.val()) {
								$rootScope.$broadcast('check.overbooking');
							}
						}
					}
				}
				
				
				if (attrs.id && attrs.id == 'edit-min-time') {
					angular.element('#edit-max-time').timepicker('option', {
						'minTime': scope.field.value ? scope.field.value : '00:30'
					}).keydown(false);
				}
				if (attrs.id && attrs.id == 'edit-max-time') {
					angular.element('#edit-min-time').timepicker('option', {
						'maxTime': scope.field.value ? scope.field.value : '24:00'
					}).keydown(false);
				}
			}

			function changeMaximumWorkTime() {
				let defer = $q.defer();

				if ($super.request.field_usecalculator.value && $super.request.timeChangeManually && isChangeMinimumOrMaximumTime()) {
					SweetAlert.swal({
						title: 'Are you sure you want to set time manualy?',
						text: 'Calculator will be turned off.',
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, let\'s do it!',
						cancelButtonText: 'No, cancel pls!',
						closeOnConfirm: true,
						closeOnCancel: true,
					}, setTimeManually);
				} else {
					defer.resolve();
				}

				function setTimeManually (confirm) {
					if (confirm) {
						$super.request.field_usecalculator.value = false;
						$super.updateMessage($super.request.field_usecalculator);
						defer.resolve();
					} else {
						$super.request.maximum_time.value = $super.request.maximum_time.old;
						$super.request.minimum_time.value = $super.request.minimum_time.old;
						$super.request.timeChangeManually = false;
						$super.removeMessage($super.request.maximum_time);
						defer.reject();
					}
				}

				return defer.promise;
			}

			function isChangeMinimumOrMaximumTime() {
				let field = _.get(scope, 'field[0].field', scope.field.field);

				return !_.get(scope, 'field[1]') && (field == 'field_maximum_move_time' || field == 'field_minimum_move_time');
			}
		}
	};
	
}