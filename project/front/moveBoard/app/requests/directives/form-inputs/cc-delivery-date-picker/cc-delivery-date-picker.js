'use strict';

angular
	.module('move.requests')
	.directive('ccDeliveryDatePicker', ccDeliveryDatePicker);

function ccDeliveryDatePicker() {
	return {
		restrict: 'A',
		scope: {
			'ngModel' : '=ngModel',
			updateRequest: '='
		},
		link: function (scope, element, attrs) {
			element.datepicker({
				dateFormat: 'MM d, yy',
				numberOfMonths: attrs.numberOfMonths ? parseInt(attrs.numberOfMonths) : 2,
				changeMonth: false,
				changeYear: !!attrs.changeYear || false,
				setDate : new Date(),
				minDate: attrs.minDate ? new Date(attrs.minDate) : new Date(),
				showButtonPanel: true,
				onSelect: onSelect
			});
			
			function onSelect(date, obj) {
				var oldV = scope.ngModel;
				scope.date = date;
				var dprv = (new Date(date)).getTime();
				
				var curdate = $.datepicker.formatDate( 'MM d, yy', new Date(dprv), {} );
				scope.ngModel = curdate;
				scope.$apply();
				scope.updateRequest(oldV)
			}
			
		}
	};
}