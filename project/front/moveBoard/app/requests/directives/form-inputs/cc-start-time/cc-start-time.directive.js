'use strict';

angular
	.module('move.requests')
	.directive('ccStartTime', ccStartTime);

function ccStartTime() {
	return {
		restrict: 'A',
		scope: {
			'message': '=message',
			'field': '=field',
			'editrequest': '=editrequest',

		},
		link: function (scope, element) {
			element.timepicker({
				'disableTouchKeyboard': true,
				'minTime': '6:00am',
				'maxTime': '11:00pm',
				'timeFormat': 'g:i A'
			});

			element.bind('change', onChange);

			function onChange() {
				scope.editrequest[scope.field.field] = element.val();
				scope.message[scope.field.field] = {
					label: scope.field.label,
					oldValue: scope.field.old,
					newValue: element.val(),
				};
				scope.$parent.parklotControl.updateStartTime(scope.field.field, element.val());
			}
		}
	};

}
