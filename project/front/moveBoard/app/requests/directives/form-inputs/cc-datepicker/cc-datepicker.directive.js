'use strict';

angular
	.module('move.requests')
	.directive('ccDatePicker', ccDatePicker);

function ccDatePicker(datacontext) {
	return {
		restrict: 'A',
		scope: {
			'message': '=',
			'field': '=',
			'editrequest': '=',
			'calendar': '='
			
		},
		link: function (scope, element, attrs) {
			var calendar = datacontext.getFieldData().calendar;
			var calendartype = datacontext.getFieldData().calendartype;
			
			var refresh = (attrs.refresh === "true");
			element.datepicker({
				disableTouchKeyboard: true,
				dateFormat: 'MM d, yy',
				numberOfMonths: 2,
				changeMonth: false,
				changeYear: false,
				showButtonPanel: true,
				minDate: new Date(),
				onSelect: onSelect,
				beforeShowDay: checkDate
			});
			
			function onSelect(date, obj) {
				scope.date = date;
				var dprv = (new Date(date)).getTime();
				var formated_date = $.datepicker.formatDate('yy-mm-dd', new Date(dprv), {});
				
				scope.editrequest[scope.field.field] = {date: formated_date, time: '15:10:00'};
				scope.field.value = formated_date;
				scope.message[scope.field.field] = {
					label: scope.field.label,
					oldValue: scope.field.old,
					newValue: element.val()
					
				};
				scope.$parent.parklotControl.reinitParklot(formated_date);
				scope.$apply();
				scope.$parent.updateRate();
				
				
				//INIT CHEDULE FOR THIS DAY
				var curdate = $.datepicker.formatDate('MM d, yy', new Date(dprv), {});
				
				if (refresh) {
					scope.$parent.parklotControl.reinitParklot(formated_date);
					$(".curdate").text(curdate);
				}
			}
			
			function checkDate(date) {
				var day = moment(date).format('DD');
				var month = moment(date).format('MM');
				var year = date.getFullYear();
				var date = year + '-' + month + '-' + day;
				
				
				if (calendar[year][date] == 'block_date') {
					return [false, 'not-available', 'This date is NOT available'];
				} else {
					return [true, calendartype[calendar[year][date]], calendartype[calendar[year][date]]];
				}
			}
		}
	};
}