'use strict';

angular
	.module('move.requests')
	.directive('ccReadMore', ccReadMore);

function ccReadMore() {
	return {
		restrict: 'A',
		scope: {
			text: "=TextTruncate",
		},
		link: function (scope, element, attrs) {
			var default_height = 50;

			if (angular.isDefined(attrs.height)) {
				default_height = parseInt(attrs.height);
			}

			scope.$watch("text", function () {
				element.readmore({
					collapsedHeight: default_height,
					moreLink: '<a href="#" style="float: right;margin-top: -10px;font-size: 10px;" class="comment_more">More</a>',
					lessLink: '<a href="#" style="float: right;margin-top: -10px;font-size: 10px;" class="comment_more">Less</a>',
				});
			});
		}
	};
}
