'use strict';

angular
	.module('move.requests')
	.directive('ccPayrollRateType', ccPayrollRateType);

function ccPayrollRateType() {
	return {
		restrict: 'A',
		require: '^ngModel',
		scope: {
			'typeValue': '=',
			'ngModel': '=',
			'commission': '=',
			'worker': '=',
			'message': '='
		},
		link: function (scope, el, attrs, ngModelCtrl) {
			var baseVal = 0;
			var msg = {};

			el.bind('focus', onFocus);
			el.bind('focusout', onBlur);
			el.bind('change', onChange);

			scope.$watch('typeValue', function (newValue, oldValue) {
				formatter(scope.ngModel);
			});

			function formatter(value) {
				var formattedValue;

				if (scope.typeValue == 'Hrs' || scope.typeValue == '%') {
					formattedValue = Number(value || 0).toFixed(2) + ' ' + scope.typeValue;
				} else {
					formattedValue = scope.typeValue + ' ' + Number(value || 0).toFixed(2);
				}

				el.val(formattedValue);
				ngModelCtrl.$setViewValue(_.round(value, 2));
			}

			formatter(scope.ngModel);

			function onFocus() {
				msg = {
					text: scope.worker + ' rate commission "' + scope.commission + '" was changed',
					from: el.val()
				};

				if (!scope.message[scope.commission]) {
					scope.message[scope.commission] = msg;
				}

				baseVal = el.val();
				el.val('');
			}

			function onBlur() {
				formatter(scope.ngModel);
				var formattedValue;

				if (scope.typeValue == 'Hrs' || scope.typeValue == '%') {
					formattedValue = scope.ngModel + ' ' + scope.typeValue;
				} else {
					formattedValue = scope.typeValue + ' ' + scope.ngModel;
				}

				scope.message[scope.commission].to = formattedValue;
			}

			function onChange() {
				var val = el.val() != '' ? el.val() : baseVal;
				formatter(parseFloat(val));
			}
		}
	};
}
