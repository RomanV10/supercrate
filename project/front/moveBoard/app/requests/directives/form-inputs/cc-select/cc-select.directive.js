'use strict';

angular
	.module('move.requests')
	.directive('ccSelect', ccSelect);

function ccSelect() {
	return {
		restrict: 'A',
		scope: {
			'editrequest': '=editrequest',
			'message': '=message',
			'field': '=field',

		},
		link: function (scope, element) {
			element.bind('change', onChange);

			function onChange() {
				var dataField = scope.$root.fieldData.field_lists[scope.field.field];

				if (angular.isUndefined(dataField)) {
					var dataField = scope.$root.fieldData.field_lists['field_type_of_entrance_to_'];
				}

				scope.editrequest[scope.field.field] = element.val();

				if (scope.field.field != "field_approve") {
					scope.field.raw = element.val();
				}

				scope.message[scope.field.field] = {
					label: scope.field.label,
					oldValue: scope.field.value,
					newValue: dataField[element.val()],

				};

				scope.$parent.edit = true;
			}
		}
	};
}
