'use strict';

angular
	.module('move.requests')
	.directive('ccTravelTime', ccTravelTime);

function ccTravelTime(common) {
	return {
		restrict: 'A',
		scope: {
			'message': '=message',
			'field': '=field',
			'editrequest': '=editrequest',

		},
		link: function (scope, element) {
			element.timepicker({
				'disableTouchKeyboard': true,
				'minTime': '00:30',
				'maxTime': '03:00',
				'timeFormat': 'H:i',
				'step': 15,
			});

			element.bind('change', onChange);

			function onChange() {
				scope.editrequest[scope.field.field] = _.round(common.convertStingToIntTime(element.val()), 2);
				scope.message[scope.field.field] = {
					label: scope.field.label,
					oldValue: scope.field.old,
					newValue: element.val(),

				};
				scope.field.value = element.val();
				scope.field.raw = _.round(common.convertStingToIntTime(element.val()), 2);
				scope.$parent.parklotControl.updateWorkTime(scope.field.field, element.val());
				scope.$parent.updateQuote();
			}

		}
	};

}
