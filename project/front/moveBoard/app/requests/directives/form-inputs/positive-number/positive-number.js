'use strict';

angular
	.module('move.requests')
	.directive('positiveNumber', positiveNumber);

function positiveNumber() {
	return {
		restrict: "A",
		require: '?ngModel',
		link: function (scope, element, attrs, ngModelCtrl) {
			if (!ngModelCtrl) {
				return;
			}

			ngModelCtrl.$parsers.push(function (val) {
				if (val === null)
					return;

				var regex =  /^[1-9]\d*$/g;

				if (regex.test(val)) {
					ngModelCtrl.$setViewValue(val);
					ngModelCtrl.$render();
					return val;
				} else {
					ngModelCtrl.$setViewValue('');
					ngModelCtrl.$render()
				}
			});
		}
	}
}
