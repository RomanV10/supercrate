'use strict';

angular
	.module('move.requests')
	.directive('ccFaPicker', ccFaPicker);

function ccFaPicker() {
	return {
		restrict: 'A',
		scope: {
			'calendar': '=calendar'
		},
		link: function (scope, element, attrs, ngModel) {
			element.datepicker({
				disableTouchKeyboard: true,
				dateFormat: 'DD,MM d, yy',
				numberOfMonths: 2,
				changeMonth: false,
				changeYear: false,
				showOn: 'button',
				buttonImageOnly: true,
				setDate: attrs.date,
				showButtonPanel: true,
				onSelect: onSelect,
				beforeShowDay: checkDate
			});
			
			function onSelect(date, obj) {
				scope.date = date;
				var dprv = (new Date(date)).getTime();
				var date = $.datepicker.formatDate('mm/dd/yy', new Date(dprv), {});
				var curdate = $.datepicker.formatDate('DD,MM d, yy', new Date(dprv), {});
				
				//REINIT SCHEDULE
				scope.$parent.parklotControl.reinitParklot(date);
				$(".curdate").text(curdate);
			}
			
			function checkDate(date) {
				var day = (date.getDate().toString()).slice(-2);
				var month = ((date.getMonth() + 1).toString()).slice(-2);
				var year = date.getFullYear();
				var date = year + '-' + month + '-' + day;
				
				if (scope.calendar[year][date] == 'block_date') {
					return [false, 'not-available', 'This date is NOT available'];
				} else {
					return [true, scope.calendar[year][date], scope.calendar[year][date]];
				}
			}
		}
	};
}