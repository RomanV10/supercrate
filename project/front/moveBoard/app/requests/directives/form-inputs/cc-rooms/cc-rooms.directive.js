'use strict';

angular
	.module('move.requests')
	.directive('ccRooms', ccRooms);

function ccRooms() {
	return {
		restrict: 'A',
		scope: {
			'message': '=message',
			'field': '=field',
			'editrequest': '=editrequest',

		},
		link: function (scope, element) {
			//Selecte Rooms
			jQuery.each(scope.field.raw, function (index, item) {
				element.find("option[value=" + item + "]").attr('selected', 'selected');
			});

			element.chosen({disable_search_threshold: 10});
			element.bind('change', onChange);

			function onChange() {
				scope.editrequest[scope.field.field] = element.val();
				scope.message[scope.field.field] = {
					lable: scope.field.label,
					oldValue: scope.field.old,
					newValue: element.val(),
				}
			}
		}
	};
}
