'use strict';

angular
	.module('move.requests')
	.directive('ccInvoiceTime', ccInvoiceTime);

function ccInvoiceTime() {
	return {
		restrict: 'A',
		link: function (scope, element) {
			element.timepicker({
				'disableTouchKeyboard': true,
				'minTime': '12:00am',
				'maxTime': '11:45pm',
				'timeFormat': 'g:i A',
				'step': 15
			});

			element.bind('change', function () {});
		}
	};
}
