describe('Speedy inventory icon', () => {
	let element;
	let scope;
	let isolateScope;

	function compileElement(requestField) {
		element = angular.element('<speedy-inventory-icon speedy-field="requestField"></speedy-inventory-icon>');

		scope.requestField = requestField;
		$compile(element)(scope);
		scope.$apply();
		isolateScope = element.isolateScope();
	}

	beforeEach(() => {
		scope = $rootScope.$new();
	});

	it('should hide icon when empty field value', () => {
		//given
		let expectedResult = false;

		//when
		compileElement({});

		//then
		expect(isolateScope.showIcon).toBe(expectedResult);
	});

	it('should hide icon when field raw equal null', () => {
		//given
		let expectedResult = false;

		//when
		compileElement({raw: null});

		//then
		expect(isolateScope.showIcon).toBe(expectedResult);
	});

	it('should show icon when field raw equal number', () => {
		//given
		let expectedResult = true;

		//when
		compileElement({raw: 1111});

		//then
		expect(isolateScope.showIcon).toBe(expectedResult);
	});
});