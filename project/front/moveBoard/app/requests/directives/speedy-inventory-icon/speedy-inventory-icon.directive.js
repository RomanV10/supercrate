'use strict';

import './speedy-inventory-icon.styl';

angular.module('app.settings')
	.directive('speedyInventoryIcon', speedyInventoryIcon);

function speedyInventoryIcon () {
	return {
		restrict: 'E',
		scope: {
			speedyField: '<',
		},
		template: require('./speedy-inventory-icon.template.html'),
		link: speedyInventoryLink
	};

	function speedyInventoryLink($scope) {
		$scope.showIcon = !!_.get($scope, 'speedyField.raw', false);
	}
}