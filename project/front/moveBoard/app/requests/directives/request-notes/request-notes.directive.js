'use strict';

import './request-notes.styl';

angular
	.module('move.requests')
	.directive('requestNotes', requestNotes);

/* @ngInject */
function requestNotes($q, EditRequestServices, RequestServices, $rootScope) {
	return {
		restrict: 'E',
		template: require('./request-notes.tpl.pug'),
		scope: {
			request: '='
		},
		link: linkNotes
	};
	
	function linkNotes($scope) {
		const SALES = 'sales_notes';
		const FOREMAN = 'foreman_notes';
		const CLIENT = 'client_notes';
		const LOCAL_DISPATCH = 'dispatch_notes';
		const WYSIWYG = [['bold','italics', 'underline', 'strikeThrough'],
			['ul', 'ol'], ['justifyLeft', 'justifyCenter', 'justifyRight'],
			['fontColor', 'fontName', 'fontSize'],
			['insertLink', 'insertImage'],
			['undo', 'redo', 'clear', 'html']];
		
		$scope.tabs = [
			{name: 'Sales notes', selected: true, show: true},
			{name: 'Foreman notes', selected: false, show: true},
			{name: 'Client notes', selected: false, show: true},
			{name: 'Dispatch notes', selected: false, show: true},
		];
		
		$scope.notes = [
			{text: '', type: SALES, selected: true, toobarSettings: []},
			{text: '', type: FOREMAN, selected: false, toobarSettings: []},
			{text: '', type: CLIENT, selected: false, toobarSettings: WYSIWYG},
			{text: '', type: LOCAL_DISPATCH, selected: false, toobarSettings: []},
		];
		
		let originalNotes;
		let logs = [];
		let isUpdated = false;
		
		$scope.updateNote = updateNote;
		$scope.select = select;
		$scope.noteEdited = noteEdited;
		
		function noteEdited() {
			isUpdated = JSON.stringify($scope.notes) !== JSON.stringify(originalNotes);
			
			$rootScope.$broadcast('notes.updated', isUpdated);
		}
		
		function getNotes() {
			var data = {
				role_note: [
					SALES,
					FOREMAN,
					CLIENT,
					LOCAL_DISPATCH,
				]
			};
			
			var promise = EditRequestServices.getNotes($scope.request.nid, data);
			
			$q.all(promise).then(({requestNotes: {data: {sales_notes, foreman_notes, client_notes, dispatch_notes}}}) => {
				$scope.notes[0].text = sales_notes;
				$scope.notes[1].text = foreman_notes;
				$scope.notes[2].text = client_notes;
				$scope.notes[3].text = dispatch_notes;
				
				originalNotes = angular.copy($scope.notes);
			});
		}
		
		function updateNote() {
			let data = {
				notes: []
			};

			let isChangeDispatchNote = false;

			angular.forEach($scope.notes, function (note, key) {
				data.notes.push(note.text);
				prepareLogs(note, key);

				if (!_.isEqual(originalNotes[key].text, note.text) && (key == 3 || key == 0)) {
					isChangeDispatchNote = true;
				}

				originalNotes[key].text = angular.copy(note.text);
			});
			
			var promise = EditRequestServices.setNotes($scope.request.nid, data);
			
			$q.all(promise)
				.then(() => {
					RequestServices.sendLogs(logs, 'Notes were updated', $scope.request.nid, 'MOVEREQUEST');
					logs = [];
					$rootScope.$broadcast('notes.updated', false);

					if (isChangeDispatchNote) {
						$rootScope.$broadcast('onUpdateDispatchNote', {
							nid: $scope.request.nid,
							notes: {
								sales_notes: originalNotes[0].text,
								dispatch_notes: originalNotes[3].text
							}
						});
					}

					toastr.success('Notes were saved');
				});
		}
		
		function prepareLogs(currentNote, index) {
			if (currentNote.text != originalNotes[index].text) {
				let message = {};
				if (!_.isEqual(currentNote.type, CLIENT)) {
					message = {
						text: currentNote.type.toString().replace(/_/g, ' '),
						to: currentNote.text.replace(/<\/?[^>]+(>|$)/g, '')
					};
					
					if (!_.isNull(originalNotes[index].text)) {
						message.from = originalNotes[index].text.replace(/<\/?[^>]+(>|$)/g, '');
					}
				} else {
					message = {
						text: currentNote.type.toString().replace(/_/g, ' '),
						to: currentNote.text
					};
					if (!_.isNull(originalNotes[index].text)) {
						message.from = originalNotes[index].text;
					}
				}
				logs.push(message);
			}
		}
		
		function select(tab) {
			angular.forEach($scope.tabs, function (item, key) {
				item.selected = false;
				$scope.notes[key].selected = false;
			});
			
			$scope.tabs[tab].selected = true;
			$scope.notes[tab].selected = true;
		}
		
		getNotes();
		$scope.$on('notes.save', updateNote);
	}
}
