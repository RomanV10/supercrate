(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccRequestBtnDate', ccRequestBtnDate);

    ccRequestBtnDate.$inject = ['datacontext', 'SweetAlert'];

    function ccRequestBtnDate(datacontext, SweetAlert) {
        return {
            restrict: 'A',
            scope: {
                'calendar': '=',
                'editrequest': '=',
                'minDate': '='
            },
            link: function (scope, element, attrs) {

                var minDate = scope.minDate ? new Date(scope.minDate) : new Date();

                scope.$watch('minDate', () => {
                    if (scope.minDate) {
                        var minDate = scope.minDate ? new Date(scope.minDate) : new Date();
                        element.datepicker( "option", "minDate", minDate );
                    }
                })

                element.datepicker({
                    dateFormat: 'M d, yy',
                    numberOfMonths: 2,
                    changeMonth: false,
                    changeYear: false,
                    showButtonPanel: true,
                    onSelect: function (date, obj) {
                        if(obj.settings.beforeShowDay(new Date(moment(date, 'MMM D, YYYY')))[1] != "Block this day"){
                            scope.editrequest.data[attrs.field] = date;
                            scope.date = date;
                        }else{
                            SweetAlert.swal({title: "Error!", text: "This day is blocked", type: "error", timer: 2000,});
                            while(obj.settings.beforeShowDay(new Date(moment(date, 'MMM D, YYYY')))[1] == "Block this day"){
                                date = moment(date, 'MMM D, YYYY').add(1, 'day').format('MMM D, YYYY');
                            }
                            scope.editrequest.data[attrs.field] = date;
                            scope.date = date;
                        }
                    },
                    beforeShow: function () {
                        beforeShowCal();
                    },
                    beforeShowDay: function (date) {
                        return checkDate(date, scope.calendar);
                    }
                });

                function checkDate(date, calendar) {
                    var settings = datacontext.getFieldData();

                    var day = (date.getDate().toString()).slice(-2);
                    if (day < 10) day = '0' + day;

                    var month = ((date.getMonth() + 1).toString()).slice(-2);
                    if (month < 10) month = '0' + month;

                    var year = date.getFullYear();
                    var dates = year + '-' + month + '-' + day;

                    if (calendar[year][dates] == 'block_date')
                        return [false, 'not-available', 'This date is NOT available'];
                    else
                        return [true, settings.calendartype[calendar[year][dates]], settings.calendartype[calendar[year][dates]]];
                }

                function beforeShowCal() {
                    setTimeout(function () {

                        var buttonPane = element
                            .datepicker("widget")
                            .find(".ui-datepicker-buttonpane");

                        buttonPane.find(".ui-datepicker-current").hide();
                        buttonPane.find(".ui-datepicker-close").hide();
                        buttonPane.find(".pricehelp").remove();

                        var helps = "<span class='pricehelp'><li class='type dicount'>Discount</li><li class='type regular'>Regular</li><li class='type subpeak'>Sub-peak</li><li class='type peak'>Peak</li><li class='type highpeak'>Hi-peak</li></span>";
                        buttonPane.append(helps);
                    }, 1);
                }
            }
        }
    }

})();
