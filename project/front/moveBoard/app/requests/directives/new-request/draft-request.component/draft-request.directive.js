'use strict';

import './draft-request.styl';

angular
	.module('move.requests')
	.directive('draftRequest', draftRequest);

/* @ngInject */
function draftRequest(apiService, moveBoardApi, RequestServices, logger, openRequestService) {
	return {
		restrict: 'AE',
		scope: {},
		template: require('./draft-request.tpl.pug'),
		link: draftRequestLink
	};

	function draftRequestLink($scope) {
		$scope.busy = false;
		$scope.createDraft = createDraft;

		function createDraft() {
			$scope.busy = true;
			apiService.postData(moveBoardApi.draftRequest.createDraftRequest).then((resolve) => {
				let requestNid = resolve.data[0];
				let msg = {
					text: `Request #${requestNid} was created as draft`
				};
				RequestServices.sendLogs([msg], 'Admin created Draft Request', requestNid, 'MOVEREQUEST');

				openRequestService.open(requestNid)
					.finally(() => {
						$scope.busy = false;
					});
			}, (reject) => {
				logger.error(reject, reject, "Error");
			}).finally(() => {$scope.busy = false});

		}
	}
}