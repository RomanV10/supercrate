(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccRequestBtnRooms', ccRequestBtnRooms);

    function ccRequestBtnRooms() {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                //Selected Rooms
                element.chosen({ disable_search_threshold: 10, width: "100%" });
                element.bind('change', function () {
                    scope.editrequest.data.field_extra_furnished_rooms = element.val();
                });
                scope.$watch('editrequest.data.field_size_of_move', function (moveSize, oldV) {
                    let rooms = _.get(scope.editrequest, 'data.field_extra_furnished_rooms', []) || [];

                    if (moveSize == 3 || moveSize == 4 || moveSize == 5 || moveSize == 6 || moveSize == 7 || moveSize == 8 || moveSize == 9 || moveSize == 10) {
                        if (rooms.indexOf('1') < 0) {
                            rooms.push('1');
                        }
                    }

                    if (moveSize == 8 || moveSize == 9 || moveSize == 10) {
                        if (rooms.indexOf('2') < 0) {
                            rooms.push('2');
                        }
                    }
                    
                    if (rooms.length) {
                        jQuery.each(rooms, function (index, item) {
                            element.find("option[value=" + item + "]").attr('selected', 'selected');
                            element.trigger("chosen:updated");
                        });
                        scope.editrequest.data.field_extra_furnished_rooms = rooms;
                    }

                });

                scope.editrequest.data.field_size_of_move = 1;
            }
        };
    }

})();
