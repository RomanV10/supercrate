(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccRequestBtn', ccRequestBtn);

/*@ngInject*/
	function ccRequestBtn($uibModal, $rootScope, datacontext, modalNewRequestService, SweetAlert, AuthenticationService,
		SendEmailsService, RequestServices, CalcRequest, $timeout, common, CommercialCalc, additionalServicesFactory) {
            return {
                controller: EditFormController,
                controllerAs: 'vm',
                template: '<a href="javascript:void(0);"  ng-click="vm.openEditModal()" class="btn btn-primary" style="margin:15px 0 0 22px">Create Request</a>',
                scope: {},
                restrict: 'A'
            };

            function EditFormController() {
                var vm = this;
                vm.openEditModal = function () {
                    var modalInstance = $uibModal.open({
                        template: require('./new-request-form.html'),
                        backdrop: false,
                        windowClass: 'visible-overflow',
                        controller: ModalInstanceCtrl
                    });

                    modalInstance.result.then(function (newRequests) {
                    });
                };

                ModalInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'SweetAlert', 'modalNewRequestService', 'CalculatorServices', '$q', 'newLogService', 'CheckRequestService', 'geoCodingService', 'serviceTypeService'];

                function ModalInstanceCtrl($scope, $uibModalInstance, SweetAlert, modalNewRequestService, CalculatorServices, $q, newLogService, CheckRequestService, geoCodingService, serviceTypeService) {
                    const ONE_DAY = 86401;
                    $scope.fieldData = datacontext.getFieldData();
                    $scope.basicsettings = angular.fromJson($scope.fieldData.basicsettings);
                    $scope.longdistance = angular.fromJson($scope.fieldData.longdistance);
                    $scope.schedulesettigns = angular.fromJson($scope.fieldData.schedulesettings);
                    $scope.calcSettings = angular.fromJson($scope.fieldData.calcsettings);
                    $scope.min_hours = parseFloat($scope.calcSettings.min_hours);
                    $scope.services = serviceTypeService.getAllServiceTypes();

                    let commercialSetting = angular.fromJson($rootScope.fieldData['commercial_move_size_setting']);

                    var serviceTypeId = 1;
                    if ($scope.basicsettings.islong_distance_miles) {
                        serviceTypeId = 7;
                    }
                    if ($scope.basicsettings.services.overnightStorageOn) {
                        serviceTypeId = 6;
                    }
                    if ($scope.basicsettings.isflat_rate_miles) {
                        serviceTypeId = 5;
                    }
                    if ($scope.basicsettings.services.unloadingHelpOn) {
                        serviceTypeId = 4;
                    }
                    if ($scope.basicsettings.services.loadingHelpOn) {
                        serviceTypeId = 3;
                    }
                    if ($scope.basicsettings.services.localMoveStorageOn) {
                        serviceTypeId = 2;
                    }
                    if ($scope.basicsettings.services.localMoveOn) {
                        serviceTypeId = 1;
                    }

                    $scope.request = {
						field_commercial_extra_rooms: {},
					};
                    $scope.message = {};
                    $scope.emailsForSend = [];
                    $scope.emailParameters = {
                    	isShowPreviewTabs: false
                    };
                    $scope.editrequest = {
                        data: {
                            field_moving_from: {
                                postal_code: ''
                            },
                            field_moving_to: {
                                postal_code: ''
                            },
                            field_move_service_type: serviceTypeId,
                            field_type_of_entrance_to_: '1',
                            field_type_of_entrance_from: '1',
                            field_extra_furnished_rooms: [],
	                        field_commercial_extra_rooms: {},
	                        field_custom_commercial_item: {
		                        name: null,
		                        cubic_feet: null,
		                        is_checked: false,
		                        id: -1
	                        },
                            field_size_of_move: 1,
                            field_actual_start_time: '8:00 AM',
                            field_start_time_max: '8:00 PM',
	                        field_commercial_company_name: ''
                        },
	                    request_all_data: {},
                        account: {}
                    };

                    $scope.defaults = {};

	                $scope.defaults.sizeOfMove = [];
	                angular.forEach($rootScope.fieldData.field_lists.field_size_of_move, function (title, value) {
		                $scope.defaults.sizeOfMove.push({value: Number(value), title: title});
	                });

					if (!_.isUndefined(commercialSetting) && !_.get(commercialSetting, 'isEnabled')) {
						let commercialIndex = _.findIndex($scope.defaults.sizeOfMove, {
							title: _.get(commercialSetting, 'name', '')
						});
						$scope.defaults.sizeOfMove.splice(commercialIndex, 1);
					}

                    $scope.step1 = true;
                    $scope.step2 = false;
                    $scope.step3 = false;
                    $scope.isDoubleDriveTime = $scope.calcSettings.doubleDriveTime;
                    var localServices = [1, 2, 3, 4, 6, 8];

                    $scope.moveToState = true;
                    var flatRateDistance = 0;
                    var serviceTypes = $rootScope.fieldData.enums.request_service_types;

                    if ($scope.basicsettings.isflat_rate_miles) {
                        flatRateDistance = $scope.basicsettings.flat_rate_miles;
                    }
                    else {
                        flatRateDistance = 0;
                    }

                    var digits = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];

                    function onlyDigits(text) {
                        if (angular.isDefined(text)) {
                            var len = text.length;
                            var clearText = '';
                            for (var i = 0; i < len; i++) {
                                if (text[i] in digits) {
                                    clearText = clearText + text[i];
                                }
                            }
                            return clearText;
                        } else {
                            return "";
                        }
                    }

                    function checkMoveToState(stateFrom, stateTo) {
                        let stateCodeTo = stateTo.administrative_area.toLowerCase();
                        let stateCodeFrom = stateFrom.administrative_area;
                        let areaCode = stateTo.premise;

                        let LDEnabled = $scope.longdistance.stateRates[stateCodeFrom][stateCodeTo].longDistance;
                        let statePriceExist = $scope.longdistance.stateRates[stateCodeFrom][stateCodeTo].state_rate > 0;
						let areaPriceExist = $scope.longdistance.stateRates[stateCodeFrom][stateCodeTo].rate
							&& $scope.longdistance.stateRates[stateCodeFrom][stateCodeTo].rate[areaCode]> 0;

                        return LDEnabled && (statePriceExist || areaPriceExist);
                    }

                    function CheckServiceType(newVal, oldVal, ZipFromChange, ZipToChange) {
                        if (angular.isDefined(newVal)
                            && angular.isDefined(oldVal)
                            && !_.isEqual(newVal.premise, oldVal.premise)) {

                            return false;
                        }

                        if (angular.isDefined($scope.editrequest.data.field_moving_from)) {
                            var from = $scope.editrequest.data.field_moving_from.postal_code;
                            var to = $scope.editrequest.data.field_moving_to.postal_code;

                            if (angular.isDefined(from)
                                && from.length == 5
                                && angular.isDefined(to)
                                && to.length == 5
                                && $scope.editrequest.data.field_move_service_type != 4) {

                                $scope.dotsLoader = true;
                                var serviceTypePromise = CalculatorServices.getRequestServiceType(from, to); // get full distance
                                serviceTypePromise.then(function (data) {
                                    var serviceType = data[0]; // get service type
                                    $scope.moveToState = true;
                                    switch (serviceType) {
                                        case serviceTypes.LONG_DISTANCE: {
											let allowMoveToState = checkMoveToState($scope.editrequest.data.field_moving_from,
												$scope.editrequest.data.field_moving_to);
											if (($scope.longdistance.acceptAllQuotes || allowMoveToState)
												&& $scope.basicsettings.islong_distance_miles) {
                                                $scope.editrequest.data.field_move_service_type = serviceTypes.LONG_DISTANCE;
                                                $scope.longDis = true;
                                            }
                                            else {
                                                $scope.moveToState = false;
                                                SweetAlert.swal("We don't do moves to this area!");
                                                if (ZipFromChange) {
                                                    angular.element('#edit-zip-code-from').addClass('error');
                                                }
                                                if (ZipToChange) {
                                                    angular.element('#edit-zip-code-to').addClass('error');
                                                }
                                                if ($scope.editrequest.data.field_move_service_type == serviceTypes.FLAT_RATE
                                                    || $scope.editrequest.data.field_move_service_type == serviceTypes.LONG_DISTANCE) {
                                                    $scope.editrequest.data.field_move_service_type = serviceTypes.MOVING;
                                                }
                                            }
                                            break;
                                        }
                                        case serviceTypes.FLAT_RATE: {
                                            if($scope.basicsettings.isflat_rate_miles) {
                                                $scope.request.status = 9; // Flat Rate Step 1    //??
                                                $scope.editrequest.data.field_move_service_type = serviceTypes.FLAT_RATE;
                                            }
                                            break;
                                        }
                                        case serviceTypes.MOVING: {
                                            if (($scope.editrequest.data.field_move_service_type == serviceTypes.FLAT_RATE
                                                || $scope.editrequest.data.field_move_service_type == serviceTypes.LONG_DISTANCE) && $scope.basicsettings.services.localMoveOn) {
                                                $scope.editrequest.data.field_move_service_type = serviceTypes.MOVING;
                                            }
                                            $scope.longDis = false;
                                            $scope.request.status = 1;
                                            break;
                                        }
                                        default : {
                                            $scope.editrequest.data.field_move_service_type = serviceType;
                                        }
                                    }
                                })
                                .finally(function () {
                                    $scope.dotsLoader = false;
                                });
                            }
                        }
                    }

                    $scope.$watch('editrequest.data', onChangeEditRequest, true);

                    function onChangeEditRequest(newVal, oldVal) {
                        if (!_.isUndefined($scope.editrequest.data.field_delivery_date)
                            && $scope.editrequest.data.field_move_service_type == 2 && newVal.field_move_service_type == oldVal.field_move_service_type) {

                            var deliveryDate = moment($scope.editrequest.data.field_delivery_date).unix();
                            var improveDeliveryDate = moment($scope.editrequest.data.field_date).unix() + ONE_DAY;

                            if (deliveryDate < improveDeliveryDate) {
                                $scope.editrequest.data.field_move_service_type = 6;
                            } else {
                                $scope.editrequest.data.field_move_service_type = 2;
                            }
                        }

                        var isZipFromChange = newVal.field_moving_from.postal_code != oldVal.field_moving_from.postal_code;
                        var isZipToChange = newVal.field_moving_to.postal_code != oldVal.field_moving_to.postal_code;

                        if (isZipFromChange) {
                            var zipCodeForm = angular.element('#edit-zip-code-from');
                            if (zipCodeForm[0] !== undefined) {
                                $scope.editrequest.data.field_moving_from.postal_code = onlyDigits($scope.editrequest.data.field_moving_from.postal_code);
                                zipCodeForm.removeClass('error');
                                if ($scope.editrequest.data.field_moving_from.postal_code.length == 5) {
									geoCodingService.geoCode($scope.editrequest.data.field_moving_from.postal_code)
                                    .then(function (data) {
                                        $scope.editrequest.data.field_moving_from.locality = data.city;
                                        if (angular.isDefined(data.state)) {
                                            $scope.editrequest.data.field_moving_from.administrative_area = data.state;
                                            CheckServiceType(newVal, oldVal, isZipFromChange, isZipToChange);
                                        } else {
                                            zipCodeForm.addClass('error');
                                            SweetAlert.swal("We don't do moves to this area!");
                                        }
                                        return data;
                                    }, function () {
                                        zipCodeForm.addClass('error');
                                        SweetAlert.swal("We don't do moves to this area!");
                                    });
                                }
                                else {
                                    zipCodeForm.addClass('error');
                                }
                            }
                        }
                        if (isZipToChange) {
                            var zipCodeTo = angular.element('#edit-zip-code-to');
                            if (zipCodeTo[0] !== undefined) {
                                $scope.editrequest.data.field_moving_to.postal_code = onlyDigits($scope.editrequest.data.field_moving_to.postal_code);
                                zipCodeTo.removeClass('error');
                                if ($scope.editrequest.data.field_moving_to.postal_code.length == 5) {
                                	let promiseGeoCode = geoCodingService.geoCode($scope.editrequest.data.field_moving_to.postal_code);
                                	let promiseAreaCode =  CalculatorServices.getLongDistanceCode($scope.editrequest.data.field_moving_to.postal_code);
                                    $q.all([promiseGeoCode, promiseAreaCode]).then(function (data) {
										$scope.editrequest.data.field_moving_to.premise = data[1];

										$scope.editrequest.data.field_moving_to.locality = data[0].city;
										if (angular.isDefined(data[0].state)) {
											$scope.editrequest.data.field_moving_to.administrative_area = data[0].state;
											CheckServiceType(newVal, oldVal, isZipFromChange, isZipToChange);
										} else {
											zipCodeTo.addClass('error');
											SweetAlert.swal("We don't do moves to this area!");
										}
										return data[0].result;
                                    }, function () {
                                        zipCodeTo.addClass('error');
                                        SweetAlert.swal("We don't do moves to this area!");
                                    });
                                }
                                else {
                                    zipCodeTo.addClass('error');
                                }
                            }
                        }

                        if (newVal.field_move_service_type != oldVal.field_move_service_type) {
                            var serviceType = $scope.editrequest.data.field_move_service_type;
                            $scope.isDoubleDriveTime = $scope.calcSettings.doubleDriveTime && localServices.indexOf(parseInt(serviceType)) >= 0;
                        }
                    }

                    $scope.create = function () {
                        $scope.busy = true;
                        $scope.error = {};

                        $scope.editrequest.data.field_start_time = 1;
                        var moveSize = $scope.editrequest.data.field_size_of_move;

                        if (moveSize == 8 || moveSize == 9 || moveSize == 10) {
                            $scope.editrequest.data.field_type_of_entrance_to_ = '7'
                            $scope.editrequest.data.field_type_of_entrance_from = '7';
                        }

                        $scope.editrequest.calculate = false;

                        var log = {
                            'request': $scope.editrequest,
                            'browser': AuthenticationService.get_browser(),
                            'message_type': "Creating Request",
                            'message': "Admin Creating Request",
                            'type': 'Admin Panel'

                        };
                        // AuthenticationService.sendLog("Pending",log);
                        modalNewRequestService.registerNewRequest($scope.editrequest, callback);

                        function callback(newRequests) {
                            $scope.busy = false;

                            if (!!newRequests && !_.isEmpty(newRequests.nodes)) {
                                _.each(newRequests.nodes, function (node, key) {
                                    var fieldData = $scope.fieldData.field_lists;
                                    let weight = ($scope.weight.min + $scope.weight.max)/2;
                                    let arr = newLogService.createLogs("Admin Creating Request", fieldData, $scope.editrequest.data, parseFloat(weight));
                                    RequestServices.sendLogs(arr, 'Request was created', node.nid, 'MOVEREQUEST');
                                });
                                $timeout(function () {
                                    SendEmailsService.sendEmails(newRequests.nodes[0].nid, $scope.emailsForSend);
                                }, 100);
                                AuthenticationService.sendLog("Success", log);
                                $rootScope.$broadcast('request.added', newRequests.nodes);
                                $uibModalInstance.dismiss('cancel');
                            } else {
                                SweetAlert.swal("Error!!", "Something going wrong", "error");
                                AuthenticationService.sendLog("Error", log);
                                $uibModalInstance.dismiss('cancel');

                            }

                        }

                    };

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.calculateTimeQuote = function (time, duration, travel) {
                        if ($scope.results || ($scope.results1 && $scope.results1)) {
                            return Number(time) + Number(travel);
                        }
                    };

                    $scope.newRequestPhoneInvalid = false;

                    $scope.checkInput = function (inputName) {
                        switch (inputName) {
                            case 'newRequestPhone':
                                var elem = angular.element('#newRequestPhone');
                                $scope.newRequestPhoneInvalid = (elem.hasClass('ng-dirty')
                                && elem.hasClass('ng-invalid'));
                                break;
                        }
                    };

                    $scope.isValid = function () {
                        var service = $scope.editrequest.data.field_move_service_type,
                            needle;

                        switch (service) {
                            case 1:
                                needle = angular.isDefined($scope.editrequest.data.field_moving_from)
                                    && angular.isDefined($scope.editrequest.data.field_moving_to)
                                    && !!($scope.editrequest.data.field_moving_to.postal_code && $scope.editrequest.data.field_moving_from.postal_code);

                                break;
                            case 2:
                                needle = !!($scope.editrequest.data.field_delivery_date
                                    && $scope.editrequest.data.field_date)
                                    && !!$scope.editrequest.data.field_moving_to
                                    && !!$scope.editrequest.data.field_moving_to.postal_code
                                    && !!$scope.editrequest.data.field_moving_to.postal_code.length;

                                break;
                            case 3:
                            case 8:
                                needle = angular.isDefined($scope.editrequest.data.field_moving_from)
                                    && !!$scope.editrequest.data.field_moving_from.postal_code;

                                break;
                            case 4:
                            case 5:
                            case 7:
                                needle = angular.isDefined($scope.editrequest.data.field_moving_to)
                                    && !!$scope.editrequest.data.field_moving_to.postal_code
                                    && !!$scope.editrequest.data.field_moving_to.postal_code.length;
                                break;
                            case 6:
                                needle = angular.isDefined($scope.editrequest.data.field_moving_to)
                                    && angular.isDefined($scope.editrequest.data.field_moving_from)
                                    && !!($scope.editrequest.data.field_moving_to.postal_code
                                    && $scope.editrequest.data.field_moving_from.postal_code);

                                break;
                        }

                        function classCheck() {
                            var hasError = false;
                            var ZipFrom = angular.element('#edit-zip-code-from');
                            var ZipTo = angular.element('#edit-zip-code-to');
                            hasError = ((ZipFrom[0] !== undefined) && ZipFrom.hasClass('error')) ||
                                ((ZipTo[0] !== undefined) && ZipTo.hasClass('error'));
                            return hasError;
                        }

                        return needle && !!$scope.editrequest.data.field_date && !classCheck();
                    };


                    $scope.Calculate = function () {
                        $scope.storage = false;
                        $scope.busy = true;
                        $scope.error = {};
                        $scope.editrequest.data.field_start_time = 1;
                        var moveSize = $scope.editrequest.data.field_size_of_move;

                        if (moveSize == 8 || moveSize == 9 || moveSize == 10) {
                            $scope.editrequest.data.field_type_of_entrance_to_ = '7';
                            $scope.editrequest.data.field_type_of_entrance_from = '7';
                        }

                        $scope.editrequest.calculate = true;

                        if ($scope.editrequest.data.field_move_service_type == 2) {
                            $scope.storage = true;
                        }

                        if ($scope.editrequest.data.field_move_service_type == 6) {
                            $scope.overnight = true;
                            $scope.editrequest.data.field_delivery_date = moment($scope.editrequest.data.field_date).add('days', 1).format('YYYY-MM-DD');
                            $scope.storage = true;
                        }

                        modalNewRequestService.registerNewRequest($scope.editrequest, callback);

	                    function callback(RequestsResult) {

		                    if (!!RequestsResult) {
			                    var request = [];
			                    request.move_size = [];
			                    request.rooms = [];
			                    request.field_commercial_extra_rooms = [];

			                    if (angular.isDefined(RequestsResult.storage1)) {
				                    $scope.storage = true;
				                    $scope.results1 = RequestsResult.storage1.data;
				                    $scope.results2 = RequestsResult.storage2.data;
				                    request.move_size.raw = $scope.results1.field_size_of_move;

				                    if ($scope.results1.field_size_of_move != 11) {
					                    request.rooms.raw = $scope.results1.field_extra_furnished_rooms;
					                    request.rooms.value = $scope.results1.field_extra_furnished_rooms;
					                    $scope.weight = CalculatorServices.getCubicFeet(request);
                                    } else {
					                    request.field_commercial_extra_rooms.raw = $scope.results1.field_commercial_extra_rooms;
					                    request.field_commercial_extra_rooms.value = $scope.results1.field_commercial_extra_rooms;
					                    $scope.weight = CommercialCalc.getCommercialCubicFeet(request.field_commercial_extra_rooms.value, $scope.calcSettings, $scope.results1.field_custom_commercial_item);
                                    }

				                    if ($scope.results2.field_size_of_move != 11) {
					                    request.rooms.raw = $scope.results2.field_extra_furnished_rooms;
					                    request.rooms.value = $scope.results2.field_extra_furnished_rooms;
					                    $scope.weight = CalculatorServices.getCubicFeet(request);
                                    } else {
					                    request.field_commercial_extra_rooms.raw = $scope.results2.field_commercial_extra_rooms;
					                    request.field_commercial_extra_rooms.value = $scope.results2.field_commercial_extra_rooms;
					                    $scope.weight = CommercialCalc.getCommercialCubicFeet(request.field_commercial_extra_rooms.value, $scope.calcSettings, $scope.results2.field_custom_commercial_item);
                                    }

				                    $scope.trucks = CalculatorServices.getTrucks($scope.weight);
				                    $scope.equipmentFeeMovingTo = additionalServicesFactory.getEquipmentFeeForCalcForm($scope.results1.request_all_data.request_distance.max_distance);
				                    $scope.equipmentFeeMovingFrom = additionalServicesFactory.getEquipmentFeeForCalcForm($scope.results2.request_all_data.request_distance.max_distance);

				                    calculateStorageRequests()
					                    .then(function () {
						                    selectSecondStep();
					                    });
			                    } else {
				                    $scope.results = RequestsResult;
				                    $scope.editrequest.data.field_date = RequestsResult.field_date.date;
				                    request.move_size.raw = $scope.results.field_size_of_move;
				                    if (RequestsResult.field_size_of_move != 11) {
					                    request.rooms.raw = $scope.results.field_extra_furnished_rooms;
					                    request.rooms.value = $scope.results.field_extra_furnished_rooms;
					                    $scope.weight = CalculatorServices.getCubicFeet(request);
                                    } else {
					                    request.field_commercial_extra_rooms.raw = $scope.results.field_commercial_extra_rooms;
					                    request.field_commercial_extra_rooms.value = $scope.results.field_commercial_extra_rooms;
					                    $scope.weight = CommercialCalc.getCommercialCubicFeet(request.field_commercial_extra_rooms.value, $scope.calcSettings, $scope.results.field_custom_commercial_item);
                                    }

				                    $scope.trucks = CalculatorServices.getTrucks($scope.weight);


				                    $scope.equipmentFee = additionalServicesFactory.getEquipmentFeeForCalcForm($scope.results.request_all_data.request_distance.max_distance);

				                    if ($scope.results.field_move_service_type == 7) {
					                    $scope.longDistanceRate = $scope.results.field_long_distance_rate;
					                    $scope.request.service_type = {
						                    raw: $scope.results.field_move_service_type
					                    };
					                    $scope.request.move_size = [];
					                    $scope.request.field_cubic_feet = [];
					                    $scope.request.field_long_distance_rate = [];
					                    $scope.request.rooms = [];
					                    $scope.request.field_cubic_feet.value = 1;
					                    $scope.request.field_moving_to = $scope.results.field_moving_to;
					                    $scope.request.field_moving_from = $scope.results.field_moving_from;
					                    $scope.request.move_size.raw = $scope.results.field_size_of_move;
					                    $scope.request.rooms.value = $scope.results.field_extra_furnished_rooms;
										$scope.request.field_commercial_extra_rooms.raw = $scope.results.field_commercial_extra_rooms;
										$scope.request.field_commercial_extra_rooms.value = $scope.results.field_commercial_extra_rooms;
										$scope.request.field_custom_commercial_item = $scope.results.field_custom_commercial_item;
					                    if ($scope.request.move_size.raw != 11) {
						                    $scope.request.inventory_weight = CalculatorServices.getRequestCubicFeet($scope.request);
                                        } else {
						                    $scope.request.inventory_weight = CommercialCalc.getCommercialCubicFeet($scope.results.field_commercial_extra_rooms, $scope.calcSettings, $scope.results.field_custom_commercial_item);
                                        }


					                    var linfo = CalculatorServices.getLongDistanceInfo($scope.request);
					                    var min_weight = angular.copy(linfo.min_weight || 0);

					                    if (min_weight > $scope.request.inventory_weight.weight) {
						                    $scope.request.inventory_weight.cfs = min_weight;
						                    $scope.request.inventory_weight.weight = min_weight;
					                    } else {
						                    $scope.request.inventory_weight.cfs = $scope.request.inventory_weight.weight;
					                    }

					                    $scope.longDistanceQuote = CalculatorServices.getLongDistanceQuote($scope.request);
					                    $scope.request.typeTo = $scope.results.field_type_of_entrance_to_;
					                    $scope.request.type_to = {raw: $scope.results.field_type_of_entrance_to_};
					                    $scope.request.typeFrom = $scope.results.field_type_of_entrance_from;
										$scope.request.type_from = {raw: $scope.results.field_type_of_entrance_from};
					                    $scope.request.extraServices = additionalServicesFactory.getLongDistanceExtra($scope.request, $scope.request.inventory_weight.weight);

					                    calculateRequest()
						                    .then(function () {
							                    selectSecondStep();
						                    });
				                    } else {
					                    calculateRequest()
						                    .then(function () {
							                    selectSecondStep();
						                    });
				                    }
			                    }

			                    initKeyNamesForSendEmail();
		                    } else {
			                    SweetAlert.swal("Error!!", "Something going wrong", "error");
			                    $uibModalInstance.dismiss('cancel');
			                    $scope.busy = false;
		                    }
	                    }

                        function selectSecondStep() {
                            $scope.busy = false;
                            $scope.step1 = false;
                            $scope.step2 = true;
                            $scope.step3 = false;
                        }

                        function initKeyNamesForSendEmail() {
                            var keyNames = [];

                        // automatically send from backend
                        keyNames.push('new_request_new_user');
                        keyNames.push('admin_new_request');
                        switch ($scope.editrequest.data.field_move_service_type) {
                            case 1:
                                keyNames.push('new_request_local');
                                break;
                            case 3:
                                keyNames.push('new_request_loading_help');
                                break;
                            case 4:
                                keyNames.push('new_request_unloading_help');
                                break;
                            case 2:
                                keyNames.push('new_request_storage');
                                break;
                            case 5:
                                keyNames.push('new_request_flat_rate');
                                break;
                            case 7:
                                keyNames.push('new_request_ld');
                                break;
                            case 8:
	                            keyNames.push('packing_day_pending');
                                break;
                        }

                            $scope.emailParameters.keyNames = keyNames;
                        }
                    };

                    function initStorageCalcResult() {
                        $scope.storageCalcResult = {
                            to: {},
                            from: {}
                        };
                    }

                    function initRequestCalcResult() {
                        $scope.requestCalcResult = {};
                    }

                    function calculateRequest() {
                        var defer = $q.defer();
                        initRequestCalcResult();
                        var result = {};
                        var request = angular.copy($scope.results);
                        var isTravelTime = $scope.calcSettings.travelTime;
                        var travelTime = isTravelTime ? request.field_travel_time : 0;
                        var isDoubleDriveTime = $scope.isDoubleDriveTime;
                        var min_hours = Number($scope.min_hours);
                        var equipmentFee = +$scope.equipmentFee.amount;
                        var localdiscount = request.request_all_data.localdiscount;
                        var rate = request.field_price_per_hour;
                        var service = request.field_move_service_type;

                        result.discount = 1;
                        result.localdiscount = localdiscount;
                        result.equipmentFee = $scope.equipmentFee;
                        result.distance = request.request_all_data.request_distance.distances.AB.distance;

                        if (localdiscount) {
                            result.discount = (1 - localdiscount / 100);
                            result.discountRate = rate / result.discount;
                        }

                        if (service == 7) {
                            result.extraServiceTotal = additionalServicesFactory.getExtraServiceTotal($scope.request.extraServices);
                            result.quote = $scope.longDistanceQuote;
                            result.travelTime = travelTime;

                            var fuelData = {
                                serviceType: service,
                                zipFrom: request.field_moving_from.postal_code,
                                zipTo: request.field_moving_to.postal_code,
                                quote: result.quote,
                                request_distance: request.request_all_data.request_distance
                            };

                            CalcRequest.getFuelSurcharge(fuelData)
                            .then(function (response) {
                                result.surcharge_fuel = response.data.surcharge_fuel;
                                $scope.editrequest.request_all_data.surcharge_fuel = response.data.surcharge_fuel;
                                $scope.editrequest.request_all_data.surcharge_fuel_perc = response.data.surcharge_fuel_perc;
                                $scope.editrequest.request_all_data.surcharge_fuel_avg = response.data.surcharge_fuel_avg;
                                result.quote += response.data.surcharge_fuel + equipmentFee + result.extraServiceTotal;

                                if (localdiscount) {
                                    result.discountQuote = result.quote / result.discount;
                                }

                                $scope.requestCalcResult = result;
                            })
                            .finally(function () {
                                defer.resolve();
                            });
                        } else {
                            if (isDoubleDriveTime) {
                                travelTime = request.field_double_travel_time;
                            }

                            result.travelTime = travelTime;
                            result.minTime = request.field_minimum_move_time + travelTime;
                            result.maxTime = request.field_maximum_move_time + travelTime;
                            if (result.minTime < min_hours) {
                                result.minTime = min_hours;
                            }
                            if (result.maxTime < min_hours) {
                                result.maxTime = min_hours;
                            }
                            if (result.minTime == result.maxTime) {
                                result.small_job = true;
                            }
                            result.isMinHourLessThenMaxTime = min_hours < result.maxTime;
                            result.isMinHourMoreMaxTime = min_hours >= result.maxTime;
                            result.isMinHourLessThenMinTime = min_hours < result.minTime;
                            result.isMinHourMoreMinTime = min_hours >= result.minTime;
                            var maxTime = result.isMinHourMoreMaxTime ? min_hours : result.maxTime;
                            var minTime = result.isMinHourMoreMinTime ? min_hours : result.minTime;

                            result.minQuote = minTime * rate;
                            result.maxQuote = maxTime * rate;

                            // For Flat rate quote 0 when create request
                            var fuelQuote = service != 5 ? (result.minQuote + result.maxQuote) / 2 : 0;

                            var fuelData = {
                                serviceType: service,
                                zipFrom: request.field_moving_from.postal_code,
                                zipTo: request.field_moving_to.postal_code,
                                quote: fuelQuote,
                                request_distance: request.request_all_data.request_distance
                            };

                            CalcRequest.getFuelSurcharge(fuelData)
                            .then(function (response) {
                                result.surcharge_fuel = response.data.surcharge_fuel;
                                $scope.editrequest.request_all_data.surcharge_fuel = response.data.surcharge_fuel;
                                $scope.editrequest.request_all_data.surcharge_fuel_perc = response.data.surcharge_fuel_perc;
                                $scope.editrequest.request_all_data.surcharge_fuel_avg = response.data.surcharge_fuel_avg;
                                result.minQuote += response.data.surcharge_fuel + equipmentFee;
                                result.maxQuote += response.data.surcharge_fuel + equipmentFee;

                                if (localdiscount) {
                                    result.discountMinQuote = result.minQuote / result.discount;
                                    result.discountMaxQuote = result.maxQuote / result.discount;
                                }

                                //We don't need FUEL SURCHAGE IF IT'S FLAT RATE REQUEST
                                if (service == 5) {

                                    $scope.editrequest.request_all_data.surcharge_fuel = 0;
                                    $scope.editrequest.request_all_data.surcharge_fuel_perc = 0;
                                    $scope.editrequest.request_all_data.surcharge_fuel_avg = 0;
                                    result.surcharge_fuel = 0;

                                }


                                $scope.requestCalcResult = result;
                            })
                            .finally(function () {
                                defer.resolve();
                            });
                        }

                        return defer.promise;
                    }

                    function calculateStorageRequests() {
                        var defer = $q.defer();
                        initStorageCalcResult();
                        var isTravelTime = $scope.calcSettings.travelTime;
                        var travelTimeTo = isTravelTime ? $scope.results1.field_travel_time : 0;
                        var travelTimeFrom = isTravelTime ? $scope.results2.field_travel_time : 0;
                        var isDoubleDriveTime = $scope.isDoubleDriveTime;
                        var min_hours = $scope.min_hours;
                        var equipmentFeeTo = +$scope.equipmentFeeMovingTo.amount;
                        var equipmentFeeFrom = +$scope.equipmentFeeMovingFrom.amount;

                        var localdiscount = $scope.results1.request_all_data.localdiscount;
                        var service = $scope.results1.field_move_service_type;

                        var result = {to: {}, from: {}};
                        result.to.equipmentFee = $scope.equipmentFeeMovingTo;
                        result.from.equipmentFee = $scope.equipmentFeeMovingFrom;
                        result.localdiscount = localdiscount;
                        result.serviceType = service;
                        result.rate = $scope.results1.field_price_per_hour;

                        if (localdiscount) {
                            result.discount = (1 - localdiscount / 100);
                            result.discountRate = result.rate / result.discount;
                        }

                        if (service == 6) {
                            result.overnightRate = parseFloat($scope.basicsettings.overnight.rate); // Overnight Storage
                        } else {
                            result.overnightRate = 0;
                        }

                        if (isDoubleDriveTime) {
                            travelTimeTo = $scope.results1.field_double_travel_time;
                            travelTimeFrom = $scope.results2.field_double_travel_time;
                        }

                        result.to.travelTime = travelTimeTo;
                        result.from.travelTime = travelTimeFrom;

                        result.to.minTime = $scope.results1.field_minimum_move_time + travelTimeTo;
                        result.to.maxTime = $scope.results1.field_maximum_move_time + travelTimeTo;
                        result.from.minTime = $scope.results2.field_minimum_move_time + travelTimeFrom;
                        result.from.maxTime = $scope.results2.field_maximum_move_time + travelTimeFrom;
                        if (result.to.minTime < min_hours) {
                            result.to.minTime = min_hours;
                        }
                        if (result.to.maxTime < min_hours) {
                            result.to.maxTime = min_hours;
                        }
                        if (result.to.minTime == result.to.maxTime) {
                            result.to.small_job = true;
                        }
                        if (result.from.minTime < min_hours) {
                            result.from.minTime = min_hours;
                        }
                        if (result.from.maxTime < min_hours) {
                            result.from.maxTime = min_hours;
                        }
                        if (result.from.minTime == result.from.maxTime) {
                            result.from.small_job = true;
                        }

                        result.to.isMinHourLessThenMaxTime = min_hours < result.to.maxTime;
                        result.to.isMinHourMoreMaxTime = min_hours >= result.to.maxTime;
                        result.to.isMinHourLessThenMinTime = min_hours < result.to.minTime;
                        result.to.isMinHourMoreMinTime = min_hours >= result.to.minTime;

                        result.from.isMinHourLessThenMinTime = min_hours < result.from.minTime;
                        result.from.isMinHourMoreMinTime = min_hours >= result.from.minTime;
                        result.from.isMinHourLessThenMaxTime = min_hours < result.from.maxTime;
                        result.from.isMinHourMoreMaxTime = min_hours >= result.from.maxTime;

                        var minTimeTo = result.to.isMinHourMoreMinTime ? min_hours : result.to.minTime;
                        var maxTimeTo = result.to.isMinHourMoreMaxTime ? min_hours : result.to.maxTime;

                        var minTimeFrom = result.from.isMinHourMoreMinTime ? min_hours : result.from.minTime;
                        var maxTimeFrom = result.from.isMinHourMoreMaxTime ? min_hours : result.from.maxTime;

                        result.to.minQuote = minTimeTo * result.rate;
                        result.to.maxQuote = maxTimeTo * result.rate;
                        result.from.minQuote = minTimeFrom * result.rate;
                        result.from.maxQuote = maxTimeFrom * result.rate;

                        var fuelDataTo = {
                            serviceType: service,
                            zipFrom: $scope.results1.field_moving_from.postal_code,
                            zipTo: $scope.results1.field_moving_to.postal_code,
                            quote: (result.to.minQuote + result.to.maxQuote) / 2,
                            request_distance: $scope.results1.request_all_data.request_distance
                        };

                        var fuelDataFrom = {
                            serviceType: service,
                            zipFrom: $scope.results2.field_moving_from.postal_code,
                            zipTo: $scope.results2.field_moving_to.postal_code,
                            quote: (result.from.minQuote + result.from.maxQuote) / 2,
                            request_distance: $scope.results2.request_all_data.request_distance
                        };

                        var fuelPromiseTo = CalcRequest.getFuelSurcharge(fuelDataTo);
                        var fuelPromiseFrom = CalcRequest.getFuelSurcharge(fuelDataFrom);
                        var fuelPromise = {
                            to: fuelPromiseTo,
                            from: fuelPromiseFrom
                        };

                        $q.all(fuelPromise)
                        .then(function (data) {
                            var fuel = data.to.data;
                            result.to.surcharge_fuel = fuel.surcharge_fuel;
                            $scope.editrequest.to_request_all_data.surcharge_fuel = fuel.surcharge_fuel;
                            $scope.editrequest.to_request_all_data.surcharge_fuel_avg = fuel.surcharge_fuel_avg;
                            $scope.editrequest.to_request_all_data.surcharge_fuel_perc = fuel.surcharge_fuel_perc;
                            result.to.minQuote += fuel.surcharge_fuel + equipmentFeeTo + result.overnightRate;
                            result.to.maxQuote += fuel.surcharge_fuel + equipmentFeeTo + result.overnightRate;

                          fuel = data.from.data;
                          result.from.surcharge_fuel = fuel.surcharge_fuel;
                          $scope.editrequest.from_request_all_data.surcharge_fuel = fuel.surcharge_fuel;
                          $scope.editrequest.from_request_all_data.surcharge_fuel_avg = fuel.surcharge_fuel_avg;
                          $scope.editrequest.from_request_all_data.surcharge_fuel_perc = fuel.surcharge_fuel_perc;
                          result.from.minQuote += fuel.surcharge_fuel + equipmentFeeFrom;
                          result.from.maxQuote += fuel.surcharge_fuel + equipmentFeeFrom;

                            $scope.storageCalcResult = result;
                        })
                        .finally(function () {
                            defer.resolve();
                        });

                        return defer.promise;
                    }

                }
            }
        }
})();
