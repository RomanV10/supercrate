(function () {

    angular
        .module('move.requests')

        .factory('InvoiceServices', InvoiceServices);

    InvoiceServices.$inject = ['$uibModal', '$q', '$http', 'config', 'datacontext', 'SettingServices', '$rootScope'];

    function InvoiceServices($uibModal, $q, $http, config, datacontext, SettingServices, $rootScope) {
        var service = {};

        service.openInvoiceDialog = openInvoiceDialog;
        service.createInvoice = createInvoice;
        service.getInvoices = getInvoices;
        service.editInvoice = editInvoice;
        service.deleteInvoice = deleteInvoice;
        service.hashInvoice = hashInvoice;
        service.showTemplatesOfItemsModal = showTemplatesOfItemsModal;

        return service;

        function getInvoices(data) {

            var deferred = $q.defer();
            $http
                .post(config.serverUrl + 'server/move_invoice/get_invoice_for_entity', data)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function createInvoice(data) {
            var deferred = $q.defer();
            $http
                .post(config.serverUrl + 'server/move_invoice', data)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function hashInvoice(hash) {
            var data = {};
            data.hash = hash;
            var deferred = $q.defer();
            $http
                .post(config.serverUrl + 'server/move_invoice/hash_invoice', data)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function editInvoice(id, data) {
            var deferred = $q.defer();
            $http
                .put(config.serverUrl + 'server/move_invoice/' + id, data)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function deleteInvoice(id) {
            var deferred = $q.defer();
            $http
                .delete(config.serverUrl + 'server/move_invoice/' + id)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }


        //******** PACKING
        function openInvoiceDialog(request, entity_type, edit, prorate, terms) {

            var modalInstance = $uibModal.open({
                template: require('./templates/invoiceModal.html'),
                controller: InvoiceModalInstanceCtrl,
                size: 'lg',
                backdrop: false,
                resolve: {
                    request: function () {
                        return request;
                    },
                    entity_type: function () {
                        return entity_type;
                    },
                    edit: function () {
                        return edit;
                    },
                    prorate: function () {
                        return prorate;
                    },
                    terms: function () {
                        return terms;
                    }
                }
            });

            modalInstance.result.then(function ($scope) {

            });

        }

        function InvoiceModalInstanceCtrl($scope, $uibModalInstance, request, entity_type, edit, prorate, terms) {
            //  $scope.settings =  datacontext.getFieldData().basicsettings;
            //  $scope.settings =  angular.fromJson($scope.settings);
            $scope.request = request;
            $scope.edit = edit;
            $scope.entity_type = entity_type;
            $scope.prorate = prorate;
            $scope.terms = terms;

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.save = function () {

            }
            $scope.$on('close.parent.modal', function () {
                $scope.cancel();
            });
        }

        function showTemplatesOfItemsModal() {
            var modalInstance = $uibModal.open({
                template: require('./templates/invoiceTemplatesOfItemsModal.html'),
                controller: InvoiceTemplatesSettingsModalInstanceCtrl,
                size: 'lg',
                backdrop: false
            });

            modalInstance.result.then(function ($scope) {

            });
        }

        function InvoiceTemplatesSettingsModalInstanceCtrl($scope, $uibModalInstance, datacontext, SettingServices, $rootScope) {
            var basicsettings = datacontext.getFieldData().basicsettings;
            basicsettings = angular.fromJson(basicsettings);

            if (_.isUndefined(basicsettings.invoice)) {
                basicsettings.invoice = {};
            }

            if (_.isUndefined(basicsettings.invoice.templatesOfItem)) {
                $scope.templatesOfItem = [];
            } else {
                $scope.templatesOfItem = basicsettings.invoice.templatesOfItem;
            }

            var defaultTemplateItem = {
                name: "",
                description: "",
                cost: 0,
                qty: 0,
                tax: 0
            };

            $scope.templateItem = angular.copy(defaultTemplateItem);
            var editInvoiceItems = [];

            $scope.isEditInvoceItem = function (index) {
                return editInvoiceItems.indexOf(index) != -1;
            };

            $scope.editInvoiceItem = function (index) {
                editInvoiceItems.push(index);
            };

            $scope.applyEditInvoice = function (index) {
                var indexOfItem = editInvoiceItems.indexOf(index);

                if (indexOfItem != -1) {
                    editInvoiceItems.splice(indexOfItem, 1);
                }
            };

            $scope.addItemTemplate = function () {
                $scope.templatesOfItem.push($scope.templateItem);
                $scope.templateItem = angular.copy(defaultTemplateItem);
            };

            $scope.deleteInvoiceItem = function (index) {
                $scope.templatesOfItem.splice(index, 1);
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.save = function () {
                if (!_.isEqual(defaultTemplateItem, $scope.templateItem)) {
                    $scope.addItemTemplate();
                }

                basicsettings.invoice.templatesOfItem = $scope.templatesOfItem;
                datacontext.getFieldData().basicsettings = basicsettings;
                $rootScope.$broadcast('invoiceItemsTemplate.update', $scope.templatesOfItem);

                SettingServices.saveSettings(basicsettings, 'basicsettings')
                    .then(function () {
                    })
                    .finally(function () {
                        $uibModalInstance.close();
                    });
            };

            $scope.$on('close.parent.modal', function () {
                $scope.cancel();
            });
        }

    }
})();
