import "./templates/invoice.directive.styl"

'use strict';

angular
	.module('move.requests')
	.directive('ccInvoice', ccInvoice)

ccInvoice.$inject = ['InvoiceServices', 'logger', 'datacontext', 'SweetAlert', 'SendEmailsService', '$q', '$rootScope',
	'CalculatorServices', 'RequestServices', 'SettingServices', 'moveBoardApi', 'apiService'];

function ccInvoice(InvoiceServices, logger, datacontext, SweetAlert, SendEmailsService, $q, $rootScope,
						 CalculatorServices, RequestServices, SettingServices, moveBoardApi, apiService) {
	return {
		scope: {
			'request': '=request',
			'entitytype': '=entitytype',
			'entityid': '@',
			'edit': '=edit',
			'prorate': '=prorate',
			'terms': '=terms'
		},
		controller: invoiceController,
		template: require('./templates/invoice.directive.html'),
		restrict: 'E'
	};
	
	function invoiceController($scope) {
		
		$scope.addItem = addItem;
		$scope.applyInvoiceItemForCharge = applyInvoiceItemForCharge;
		$scope.removeCharge = removeCharge;
		$scope.changeField = changeField;
		$scope.invoiceSubTotal = invoiceSubTotal;
		$scope.showTemplatesOfItemsModal = showTemplatesOfItemsModal;
		$scope.calculateDiscount = calculateDiscount;
		$scope.calculateInvoiceTax = calculateInvoiceTax;
		$scope.calculateGrandTotal = calculateGrandTotal;
		$scope.saveAsDraft = saveAsDraft;
		$scope.updateInvoice = updateInvoice;
		$scope.sendInvoice = sendInvoice;
		$scope.saveTaxName = saveTaxName;
		
		$scope.invoice = {
			tax: 0,
			discount: 0
		};
		
		getTaxName();
		
		function getTaxName() {
			SettingServices.getVariable('taxName').then(
				res => {
					$scope.invoice.taxName = res;
				},
				rej => {
					$scope.invoice.taxName = 'Tax';
					saveTaxName();
				}
			);
		}
		
		function saveTaxName() {
			SettingServices.setVariable($scope.invoice.taxName, 'taxName').then(
				res => {
					changeField('taxName', $scope.invoice.taxName)
				},
				rej => {
					logger.error('Can not save new tax name');
				}
			);
		}
		
		$scope.fieldData = datacontext.getFieldData();
		
		if (_.isUndefined($scope.request.nid)
			&& !_.isUndefined($scope.request.id)) {
			
			$scope.request.nid = $scope.request.id;
		}
		
		if (_.isUndefined($scope.entityid)) {
			$scope.entityid = $scope.request.nid;
		}
		
		var entity_id = $scope.entityid;
		$scope.invoiceFlags = $scope.fieldData.enums.invoice_flags;
		$scope.invoicesFlagsArray = [];
		$scope.msgLog = {};
		$scope.basicData = {};
		
		angular.forEach($scope.invoiceFlags, function (id, flagname) {
			if (flagname == "DRAFT") {
				flagname = 'Not Sent';
			}
			
			$scope.invoicesFlagsArray[id] = flagname;
		});
		
		
		$scope.invoice.id = entity_id;
		$scope.request.nid = entity_id;
		
		var basicsettings = angular.fromJson($scope.fieldData.basicsettings);
		
		if (_.isUndefined(basicsettings.invoice)) {
			basicsettings.invoice = {};
		}
		
		if (_.isUndefined(basicsettings.invoice.templatesOfItem)) {
			$scope.templatesOfItem = [];
		} else {
			$scope.templatesOfItem = basicsettings.invoice.templatesOfItem;
		}
		
		if (!$scope.edit) {
			$scope.invoice.charges = [];
			// Init First Element
			$scope.addItem();
			// IF REQUEST INVOICE
			if ($scope.entitytype == $scope.fieldData.enums.entities.MOVEREQUEST) {
				$scope.invoice.request_invoice = true;
			}
			// IF STORAGE INVOICE
			if ($scope.entitytype == $scope.fieldData.enums.entities.STORAGEREQUEST) {
				$scope.invoice.storage_invoice = true;
				entity_id = $scope.request.id || $scope.request.nid;
				if ($scope.request.id && !$scope.request.nid)
					$scope.request.nid = $scope.request.id;
			}
			// IF LONG DISTANCE INVOICE
			if ($scope.entitytype == $scope.fieldData.enums.entities.LDREQUEST) {
				$scope.invoice.storage_invoice = true;
				entity_id = $scope.request.id;
				$scope.request.nid = $scope.request.id;
			}
			
			// Init Basic Data User Info and Addresses
			$scope.invoice.field_moving_from = {};
			$scope.invoice.date = moment().format("MMMM DD, YYYY");
			
			if (angular.isDefined($scope.request) && !$scope.request.newInvoice) {
				if ($scope.entitytype == $scope.fieldData.enums.entities.MOVEREQUEST && !$scope.prorate) {
					$scope.invoice.client_name = $scope.request.first_name + ' ' + $scope.request.last_name;
					$scope.invoice.email = $scope.request.email;
					$scope.invoice.phone = $scope.request.phone;
					$scope.invoice.id = $scope.request.nid;
					$scope.invoice.uid = $scope.request.uid.uid;
					$scope.invoice.field_moving_from = {};
					
					if ($scope.request.field_moving_from) {
						$scope.invoice.field_moving_from.locality = $scope.request.field_moving_from.locality;
						$scope.invoice.field_moving_from.administrative_area = $scope.request.field_moving_from.administrative_area;
						$scope.invoice.field_moving_from.postal_code = $scope.request.field_moving_from.postal_code;
						$scope.invoice.field_moving_from.thoroughfare = $scope.request.field_moving_from.thoroughfare;
					}
				}
				
				if ($scope.entitytype == $scope.fieldData.enums.entities.STORAGEREQUEST && !$scope.prorate) {
					$scope.invoice.client_name = $scope.request.data.user_info.name;
					$scope.invoice.email = $scope.request.data.user_info.email;
					$scope.invoice.phone = $scope.request.data.user_info.phone1;
					$scope.invoice.id = $scope.request.id;
					$scope.invoice.field_moving_from.postal_code = $scope.request.data.user_info.zip;
					$scope.invoice.field_moving_from.thoroughfare = $scope.request.data.user_info.address;
					$scope.invoice.field_moving_from.locality = $scope.request.data.user_info.city;
					$scope.invoice.field_moving_from.administrative_area = $scope.request.data.user_info.state;
					$scope.invoice.uid = $scope.request.data.user_info.uid;
				}
			} else if ($scope.request.newInvoice) {
				$scope.invoice = $scope.request;
				$scope.invoice.id = entity_id;
			}
		} else {
			$scope.invoice = angular.copy($scope.edit.data);
			$scope.invoice.id = $scope.edit.id;
			
			if (_.isUndefined($scope.invoice.tax)) {
				$scope.invoice.tax = 0;
			}
			if (_.isUndefined($scope.invoice.discount)) {
				$scope.invoice.discount = 0;
			}
			
			$scope.invoice.flag = Number($scope.invoice.flag);
		}
		
		var invoiceTemplate = [''];
		$scope.invoiceTitle = '';
		if ($scope.entitytype == $scope.fieldData.enums.entities.MOVEREQUEST) {
			$scope.invoiceTitle = 'Invoice for Request';
			invoiceTemplate = ['send_to_user_invoice'];
		}
		if ($scope.entitytype == $scope.fieldData.enums.entities.STORAGEREQUEST) {
			$scope.invoiceTitle = 'Invoice for Storage Services';
			invoiceTemplate = ['storage_invoice_template'];
		}
		
		if ($scope.prorate) {
			$scope.invoice = angular.copy($scope.prorate.data);
		}
		if ($scope.terms)
			$scope.invoice.terms = $scope.terms;
		_.forEach($scope.invoice, function (item, key) {
			if (key == 'field_moving_from') {
				_.forEach(item, function (val, i) {
					$scope.basicData[i] = angular.copy(val);
				});
			} else {
				$scope.basicData[key] = angular.copy(item);
			}
		});
		
		function changeField(field, value, subfield) {
			var from = $scope.basicData[field] || '';
			var to = value;
			if (field == 'date') {
				if (from && $scope.edit)
					from = moment(from).format('MMM DD, YYYY');
				if (to)
					to = moment(to).format('MMM DD, YYYY');
				return false;
			}
			if (field == 'tax' || field == 'discount') {
				if (from)
					from += '%';
				if (to)
					to += '%';
				return false;
			}
			if (field == 'field_moving_from') {
				var label = field;
				if (!$scope.msgLog[subfield])
					$scope.msgLog[subfield] = {};
				$scope.msgLog[subfield].from = $scope.basicData[subfield] || '';
				$scope.msgLog[subfield].to = to || '';
				label = label.replace(/field_/g, "");
				var sub = subfield.replace(/_/g, " ");
				$scope.msgLog[subfield].text = label.replace(/_/g, " ").charAt(0).toUpperCase() + label.replace(/_/g, " ").slice(1) + ' ' + sub + ' was changed';
				return false;
			}
			if (from || to)
				$scope.msgLog[field] = {
					from: from,
					to: to,
					text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' was changed'
				};
			if (field == 'charges') {
				var textFrom = '';
				var textTo = '';
				if (from && $scope.edit)
					_.forEach(from, function (obj) {
						textFrom += 'Name: ' + obj.name + ', Description: ' + obj.description + ', Cost: ' + obj.cost + ', Qty: ' + obj.qty + '.<br>';
					});
				if (to)
					_.forEach(to, function (obj) {
						textTo += 'Name: ' + obj.name + ', Description: ' + obj.description + ', Cost: ' + obj.cost + ', Qty: ' + obj.qty + '.<br>';
					});
				$scope.msgLog[field] = {
					from: textFrom,
					to: textTo,
					text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' was changed'
				};
			}
		};
		
		// Calculates the sub total of the invoice
		function invoiceSubTotal() {
			var total = 0.00;

			angular.forEach($scope.invoice.charges, function (charge, key) {
				total += (charge.qty * charge.cost);
			});

			return _.round(total, 2);
		}
		
		function showTemplatesOfItemsModal() {
			InvoiceServices.showTemplatesOfItemsModal();
		}
		
		function calculateDiscount() {
			if ($scope.invoice.discount == 0) {
				return 0;
			} else {
				return invoiceSubTotal() * +$scope.invoice.discount / 100;
			}
		}
		
		
		function calculateInvoiceTax() {
			if ($scope.invoice.tax == 0) {
				return 0;
			} else {
				return (invoiceSubTotal() - calculateDiscount()) * +$scope.invoice.tax / 100;
			}
		};
		
		// Calculates the grand total of the invoice
		function calculateGrandTotal() {
			//  saveInvoice();
			return _.round(invoiceSubTotal() - calculateDiscount() + calculateInvoiceTax(), 2);
		};
		
		
		function saveAsDraft () {
			$scope.busy = true;
			
			$scope.invoice.flag = $scope.invoiceFlags.DRAFT;
			$scope.invoice.totalInvoice = calculateGrandTotal();
			$scope.invoice.total = calculateGrandTotal();
			
			createInvoice()
				.then(resolve => {
					$scope.busy = false;
					let arr = [];
					_.forEach($scope.msgLog, function (item) {
						arr.push(item);
					});
					if (!_.isEmpty(arr) && $scope.edit)
						RequestServices.sendLogs(arr, 'Invoice updated', $scope.request.nid, 'STORAGEREQUEST');
					if (!$scope.edit) {
						sendNewInvoiceToLog(_.head(resolve));
						let invoiceData = {
							data: $scope.invoice,
							id: _.head(resolve)
						};
						$rootScope.$broadcast('move_invoice.created', invoiceData);
					}
					$rootScope.$broadcast('close.parent.modal');
					toastr.success("Invoice was save", "Success");
				})
			
		};
		
		
		function updateInvoice(flag) {
			if ($scope.edit) {
				$scope.busy = true;
				$scope.invoice.totalInvoice = calculateGrandTotal();
				$scope.invoice.total = calculateGrandTotal();
				
				// Show template preview then save and send letter
				
				if (_.isEqual(flag, $scope.invoiceFlags.SENT)) {
					var emailParameters = {
						keyNames: invoiceTemplate
					};
					
					SendEmailsService
						.prepareForEditEmails(emailParameters)
						.then(function (data) {
							var emails = data.emails;
							
							if (_.isEmpty(emails)) {
								emails.push(SendEmailsService.createEmptyEmailObject());
							}
							
							var isSendEmailPreview = true;
							var templateOptions = {
								rtype: 2,
								rid: $scope.edit.id,
								isShowPreviewTabs: true
							};
							var openMailPreviewPromise = SendEmailsService.showEmailPreview(emails[0], data.blocksMenu, isSendEmailPreview, templateOptions);
							var editInvoicePromise = editInvoice();
							
							$q.all([openMailPreviewPromise, editInvoicePromise])
								.then(function (response) {
									var email = response[0];
									if (_.isObject(email)
										&& !_.isEmpty(email.blocks)) {
										
										SendEmailsService
											.sendEmailsInvoice($scope.edit.id, [email])
											.then(function () {
												toastr.success('Email was sent', 'Success')
											}, function () {
												toastr.success("Email wasn't sent", 'Error')
											});
									} else {
										$scope.invoice.flag = $scope.invoiceFlags.DRAFT;
										editInvoice();
										$rootScope.$broadcast('close.parent.modal');
									}
									
									toastr.success("Invoice was updated", "Success");
									$rootScope.$broadcast('close.parent.modal');
								}, function (reason) {
								})
						});
				} else {
					editInvoice()
						.then(function (response) {
							toastr.success("Invoice was updated", "Success");
						}, function (error) {
							toastr.error("Invoice wasn't updated", "Error");
						});
				}
			}
		};
		
		function editInvoice(idInvoice) {
			var defer = $q.defer();
			var data = {};
			data.data = $scope.invoice;
			
			var id = $scope.edit.id;
			
			if (!_.isUndefined(idInvoice)) {
				id = idInvoice;
			}
			
			//Update INVOICE
			InvoiceServices
				.editInvoice(id, data)
				.then(function (response) {
					$scope.busy = false;
					var arr = [];
					_.forEach($scope.msgLog, function (item) {
						arr.push(item);
					});
					if (!_.isEmpty(arr) && $scope.edit)
						RequestServices.sendLogs(arr, 'Invoice updated', $scope.request.nid, 'STORAGEREQUEST');
					if (_.isUndefined(idInvoice)) {
						$scope.edit.data = $scope.invoice;
					}
					$rootScope.$broadcast('move_invoice.changed');
					defer.resolve();
				}, function (reason) {
					$scope.busy = false;
					
					logger.error(reason, reason, "Error");
					defer.reject(reason);
				});
			
			return defer.promise;
		}
		
		function ValidateInvoice() {
			
			
			if (angular.isUndefined($scope.invoice.client_name)) {
				SweetAlert.swal("Fill out Client Name", "", "error");
				return false;
				
			}
			if (angular.isUndefined($scope.invoice.field_moving_from.locality) ||
				angular.isUndefined($scope.invoice.field_moving_from.thoroughfare) ||
				angular.isUndefined($scope.invoice.field_moving_from.administrative_area) ||
				angular.isUndefined($scope.invoice.field_moving_from.postal_code)
			) {
				SweetAlert.swal("Fill out  Address", "", "error");
				return false;
			}
			
			if (!$scope.invoice.charges[0].name.length) {
				SweetAlert.swal("Add at least one item", "", "error");
				return false;
				
			}
			
			return true;
		}
		
		//Send Invoice Function
		
		function sendInvoice() {
			if (ValidateInvoice()) {
				$scope.busy = true;
				
				$scope.invoice.flag = $scope.invoiceFlags.SENT;
				
				
				$scope.invoice.totalInvoice = calculateGrandTotal();
				$scope.invoice.total = calculateGrandTotal();
				
				var emailParameters = {
					keyNames: invoiceTemplate
				};
				
				SendEmailsService
					.prepareForEditEmails(emailParameters)
					.then(function (data) {
						var emails = data.emails;
						
						if (_.isEmpty(emails)) {
							emails.push(SendEmailsService.createEmptyEmailObject());
						}
						
						var isSendEmailPreview = true;
						var openMailPreviewPromise = SendEmailsService.showEmailPreview(emails[0], data.blocksMenu, isSendEmailPreview);
						var createInvoicePromise = createInvoice();
						
						$q.all([openMailPreviewPromise, createInvoicePromise])
							.then(function (response) {
								var email = response[0];
								var idInvoice = response[1][0];

								if (_.isObject(email)
									&& !_.isEmpty(email.blocks)) {
									
									SendEmailsService
										.sendEmailsInvoice(idInvoice, [email])
										.then(function () {
											toastr.success('Email was sent', 'Success');
											if ($scope.invoice.flag != $scope.invoiceFlags.PAID)
												$scope.invoice.flag = $scope.invoiceFlags.SENT;
											
											editInvoice(idInvoice);
											$rootScope.$broadcast('close.parent.modal');
											toastr.success("Invoice was sent", "Success");
										}, function () {
											toastr.success("Email wasn't sent", 'Error');
										});
								} else {
									$scope.invoice.flag = $scope.invoiceFlags.DRAFT;
									editInvoice(idInvoice);
									$rootScope.$broadcast('close.parent.modal');
								}
								
								$rootScope.$broadcast('close.parent.modal');
							}, function (reason) {
							})
					});
			}
		};
		
		function sendNewInvoiceToLog(id) {
			var msgLog = {};
			_.forEach($scope.invoice, function (value, field) {
				if (field == 'field_moving_from') {
					if (!_.isEmpty(value))
						_.forEach(value, function (item, subfield) {
							var label = field;
							if (!msgLog[subfield])
								msgLog[subfield] = {};
							msgLog[subfield].to = item || '';
							label = label.replace(/field_/g, "");
							var sub = subfield.replace(/_/g, " ");
							msgLog[subfield].text = label.replace(/_/g, " ").charAt(0).toUpperCase() + label.replace(/_/g, " ").slice(1) + ' ' + sub + ' changed';
						});
				}
				var to = value;
				if (field == 'charges') {
					var textTo = '';
					if (value) {
						_.forEach(value, function (obj) {
							textTo += 'Name: ' + obj.name + ', Description: ' + obj.description + ', Cost: ' + obj.cost + ', Qty: ' + obj.qty + '.<br>';
						});
						msgLog[field] = {
							to: textTo,
							text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' was changed'
						};
					}
				}
				if (field == 'date') {
					if (to) {
						to = moment(to).format('MMM DD, YYYY');
						msgLog[field] = {
							to: to,
							text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' changed'
						};
					}
				}
				if (field == 'tax' || field == 'discount') {
					if (to) {
						to += '%';
						msgLog[field] = {
							to: to,
							text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' changed'
						};
					}
				}
				if (field == 'phone' || field == 'client_name')
					if (to) {
						msgLog[field] = {
							to: to,
							text: field.replace(/_/g, " ").charAt(0).toUpperCase() + field.replace(/_/g, " ").slice(1) + ' changed'
						};
					}
			});
			var arr = [];
			_.forEach(msgLog, function (item) {
				arr.push(item);
			});
			var msg = {
				text: 'Total charge',
				to: '$' + $scope.invoice.totalInvoice.toFixed(2)
			};
			arr.push(msg);
			var title = 'Invoice #' + id + ' created';
			if ($scope.prorate)
				title = 'Invoice Storage Pro-Rate #' + id + ' created';
			RequestServices.sendLogs(arr, title, $scope.request.nid, 'STORAGEREQUEST');
		}
		
		function createInvoice() {
			var defer = $q.defer();
			var data = {};
			var entity_id = $scope.entityid;
			$scope.invoice.total = calculateGrandTotal();
			$scope.invoice.totalInvoice = calculateGrandTotal();
			data.data = $scope.invoice;
			data.data.entity_type = $scope.entitytype;
			data.data.entity_id = entity_id;
			data.entity_type = $scope.entitytype;
			data.entity_id = entity_id;
			//Save INVOICE
			InvoiceServices
				.createInvoice(data)
				.then(function (response) {
					if ($scope.prorate) {
						$rootScope.$broadcast('move_invoice.prorate', {
							invoice: $scope.invoice,
							id: _.head(response)
						});
					}
					$scope.busy = false;
					sendNewInvoiceToLog(_.head(response));
					var invoiceData = {
						data: $scope.invoice,
						id: _.head(response)
					};
					if (!$scope.prorate)
						$rootScope.$broadcast('move_invoice.created', invoiceData);
					defer.resolve(response);
				}, function (reason) {
					$scope.busy = false;
					logger.error(reason, reason, "Error");
					defer.reject(reason);
				});
			
			return defer.promise;
		}
		
		$scope.$on('invoiceItemsTemplate.update', function (event, data) {
			$scope.templatesOfItem = data;
		});
		
		
		function addItem() {
			$scope.invoice.charges.push({qty: 0, cost: 0, description: "", name: ""});
		}
		
		function applyInvoiceItemForCharge(item, index) {
			$scope.invoice.charges[index] = angular.copy(item);
		}
		
		
		function removeCharge(index) {
			$scope.invoice.charges.splice(index, 1);
			
			if ($scope.invoice.charges.length == 0) {
				addItem();
			}
		};
		
		
	}
}
