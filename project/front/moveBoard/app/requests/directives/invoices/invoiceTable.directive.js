(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccInvoiceTable', ccInvoiceTable);

    ccInvoiceTable.$inject = ['InvoiceServices', 'logger', 'datacontext', 'SweetAlert', 'RequestServices', '$rootScope'];
    // INVOICE TABLE //

    function ccInvoiceTable(InvoiceServices, logger, datacontext, SweetAlert, RequestServices, $rootScope) {
        return {
            scope: {
                'nid': '=nid',
                'entitytype': '=entitytype',
                'request': '=request',
                'invoices': '=invoices'
            },
            controller: invoiceTableController,
            template: require('./templates/invoice.table.html'),
            restrict: 'E'
        };

        function invoiceTableController($scope) {

            // $scope.invoices = [];
            var data = {};
            $scope.fieldData = datacontext.getFieldData();
            $scope.basicsettings = angular.fromJson($scope.fieldData.basicsettings);

            $scope.invoiceFlags = $scope.fieldData.enums.invoice_flags;
            $scope.invoicesFlagsArray = [];

            angular.forEach($scope.invoiceFlags, function (id, flagname) {
                if (flagname == "DRAFT") {
                    flagname = 'Not Sent';
                }

                $scope.invoicesFlagsArray[id] = flagname;
            });

            data.entity_type = $scope.entitytype;
            data.entity_id = $scope.nid;

            //if StorgeReuquest
            // получить инвойсы и получить реситы.
            // показывать баланс
            function activate() {
                InvoiceServices.getInvoices(data).then(function (response) {
                    $scope.busy = false;
                    $scope.invoices = response;
                }, function (reason) {
                    $scope.busy = false;
                    logger.error(reason, reason, "Error");
                });
            }

            if ($scope.nid)
                activate();

            $scope.$on('move_invoice.created', function () {
                activate();
            });
            $scope.$on('move_invoice.changed', function () {
                activate();
            });

            $scope.$on('request.payment_receipts', function () {
                activate();
            });

            $scope.dateFormat = function (date, type) {
                return _.isNaN(moment.unix(date)._i) ? moment(date).format('DD MMM, YYYY') : moment.unix(date).format('DD MMM, YYYY h:mm a');
            };

            $scope.viewInvoice = function (invoice) {


            };

            //Delete Invoice
            $scope.deleteInvoice = function (invoice) {

                SweetAlert.swal({
                        title: "Are you sure you want to delete invoice ?",
                        text: "It will be deleted permanently",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Delete!",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {

                            //Delete Action
                            InvoiceServices.deleteInvoice(invoice.id).then(function (response) {
                                $scope.busy = false;
                                var message = "Invoice in the amount  $" + invoice.data.totalInvoice + ' was removed.';
                                var obj = {
                                    simpleText: message
                                };
                                var arr = [obj];
                                RequestServices.sendLogs(arr, 'Invoice removed', $scope.request.nid, 'STORAGEREQUEST');
                                var index = _.findIndex($scope.invoices, function (o) {
                                    return o.id == invoice.id;
                                });
                                $scope.invoices.splice(index, 1);
                                $rootScope.$broadcast('move_invoice.removed', $scope.invoices);
                            }, function (reason) {
                                $scope.busy = false;
                                logger.error(reason, reason, "Error");
                            });

                        }
                    });


            };

            $scope.editInvoice = function (invoice) {
                InvoiceServices.openInvoiceDialog($scope.request, $scope.entitytype, invoice);
            }

        }


    }

})();
