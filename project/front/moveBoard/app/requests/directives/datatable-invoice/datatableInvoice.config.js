'use strict';

angular
	.module('move.requests')
	.config(function ($mdDateLocaleProvider) {
		$mdDateLocaleProvider.formatDate = (date) => date ? moment(date).format('MMM DD, YYYY') : '';

		$mdDateLocaleProvider.parseDate = function (dateString) {
			var parsedDate = moment(dateString, 'MMM DD, YYYY', true);
			return parsedDate.isValid() ? parsedDate.toDate() : new Date(NaN);
		};
	});
