describe("directive datatableInvoice:", () => {
	let apiService, moveBoardApi, SweetAlert, $q;
	let $scope, $localScope, element;
	let invoicesFromApi, invoicesRequest;

	beforeEach(inject((_apiService_, _moveBoardApi_, _SweetAlert_, _$q_) => {
		$q = _$q_;
		apiService = _apiService_;
		moveBoardApi = _moveBoardApi_;
		SweetAlert = _SweetAlert_;
		let responseTimezone = testHelper.loadJsonFile('timezone.mock');
		$rootScope.fieldData.timeZone = responseTimezone;
		invoicesFromApi = testHelper.loadJsonFile('get-move-request-invoices.mock');
		invoicesRequest = $httpBackend.whenPOST(testHelper.makeRequest(moveBoardApi.invoice.getInvoicesForDashboard));
		invoicesRequest.respond(200, invoicesFromApi);

		$scope = $rootScope.$new();
		$scope.totalsTop = {};
		element = $compile('<div datatable-invoice totals-Top="totalsTop"></div>')($scope);
		$localScope = element.isolateScope();
		$timeout.flush();
	}));

	describe('initializing,', () => {
		it("Should set days to current month", () => {
			let today = new Date();
			let firstOfMonth = angular.copy(today);
			firstOfMonth.setDate(1);
			function f(date) {
				return moment(date).format("YYYYMMDD")
			}
			expect(f($localScope.startDate)).toEqual(f(firstOfMonth));
			expect(f($localScope.endDate)).toEqual(f(today));
		});

		it("Should choose 'id' as sorting field", () => {
			expect($localScope.sortingFields.id.sortBy).toBeTruthy();
			expect($localScope.sortingFields.client_name.sortBy).toBeFalsy();
			expect($localScope.sortingFields.description.sortBy).toBeFalsy();
			expect($localScope.sortingFields.invoice_date.sortBy).toBeFalsy();
			expect($localScope.sortingFields.total.sortBy).toBeFalsy();
			expect($localScope.sortingFields.status.sortBy).toBeFalsy();
			expect($localScope.sortingFields.storage_request_id.sortBy).toBeFalsy();
		});

		it("Should send request for invoives", () => {
			invoicesRequest = $httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.invoice.getInvoicesForDashboard));
			element = $compile('<div datatable-invoice totals-Top="totalsTop"></div>')($scope);
			$localScope = element.isolateScope();
		});

		it("Should set filterFlag to -1(all)", () => {
			expect($localScope.filterFlag).toEqual("-1");
		});

		it("Should set totalPages from first responce", () => {
			expect($localScope.totalPages).toEqual(1);
		});

		it("Should fill table from first responce", () => {
			expect($localScope.invoices.length).toEqual(10);
		});

		it("Should set totalInvoices from first responce", () => {
			expect($localScope.totalInvoices).toEqual(10);
		});

		it("Should set table info from first responce", () => {
			expect($localScope.pageStartsFrom).toEqual(1);
			expect($localScope.pageEndsOn).toEqual(10);
		});

		it("Should set sort direction to true", () => {
			expect($localScope.sortingDirection).toBeTruthy();
		});

	});

	describe('When fake dates,', () => {
		beforeEach(() => {
			$localScope.startDate = moment(1516789705387)._d;
			$localScope.endDate = moment(1515000000000)._d;
		});

		describe("function clickOnField,", () => {
			describe("When click on active field,", () => {
				beforeEach(() => {
					let etalon = {
						"sort": "id",
						"page": 0,
						"direction": "ASC",
						"field": "id",
						"filter": {"date_from": 1516770000, "date_to": 1515041999, "flag": null}
					};
					$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.invoice.getInvoicesForDashboard), etalon)
						.respond(200, invoicesFromApi);
					$localScope.clickOnField("id");
					$httpBackend.flush();
				});

				it("Should set sort direction to false", () => {
					expect($localScope.sortingDirection).toBeFalsy();
				});

				it("Should leave sort field as is", () => {
					expect($localScope.sortingFields.id.sortBy).toBeTruthy();
					expect($localScope.sortingFields.client_name.sortBy).toBeFalsy();
					expect($localScope.sortingFields.description.sortBy).toBeFalsy();
					expect($localScope.sortingFields.invoice_date.sortBy).toBeFalsy();
					expect($localScope.sortingFields.total.sortBy).toBeFalsy();
					expect($localScope.sortingFields.status.sortBy).toBeFalsy();
					expect($localScope.sortingFields.storage_request_id.sortBy).toBeFalsy();
				});
			});

			describe("When click on passive field,", () => {
				beforeEach(() => {
					let etalon = {
						"sort": "description",
						"page": 0,
						"direction": "DESC",
						"field": "description",
						"filter": {"date_from": 1516770000, "date_to": 1515041999, "flag": null}
					};
					$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.invoice.getInvoicesForDashboard), etalon)
						.respond(200, invoicesFromApi);
					$localScope.clickOnField("description");
					$httpBackend.flush();
				});

				it("Should leave sort direction as is", () => {
					expect($localScope.sortingDirection).toBeTruthy();
				});

				it("Should set sort field to description", () => {
					expect($localScope.sortingFields.id.sortBy).toBeFalsy();
					expect($localScope.sortingFields.client_name.sortBy).toBeFalsy();
					expect($localScope.sortingFields.description.sortBy).toBeTruthy();
					expect($localScope.sortingFields.invoice_date.sortBy).toBeFalsy();
					expect($localScope.sortingFields.total.sortBy).toBeFalsy();
					expect($localScope.sortingFields.status.sortBy).toBeFalsy();
					expect($localScope.sortingFields.storage_request_id.sortBy).toBeFalsy();
				});
			});

		});

		describe("function setPage,", () => {
			it("Should send new page in request", () => {
				let etalon = {
					"sort": "id",
					"page": 100500,
					"direction": "DESC",
					"field": "id",
					"filter": {"date_from": 1516770000, "date_to": 1515041999, "flag": null}
				};
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.invoice.getInvoicesForDashboard), etalon)
					.respond(200, invoicesFromApi);
				$localScope.setPage(100500);
				$httpBackend.flush();
			});
		});

		describe("function openRequest,", () => {
			it("Should send get_super_request with request Id", () => {
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.request.getSuperRequest + "12345"))
					.respond(404, {});
				$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.requests.getRequestContract + "12345"))
					.respond(404, {});
				$localScope.openRequest(12345);
				$httpBackend.flush();
			});
		});

	});

});
