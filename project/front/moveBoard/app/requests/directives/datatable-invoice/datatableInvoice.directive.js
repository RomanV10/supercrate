import './datatableInvoice.styl';
import MOMENT_TIMEZONE from 'moment-timezone';
import sortingFields from './sortingFields.json';
const PAGESIZE = 25;


'use strict';

angular
	.module('move.requests')
	.directive('datatableInvoice', datatableInvoice);

datatableInvoice.$inject = ['$q', 'RequestServices', 'apiService', 'moveBoardApi', 'SweetAlert'];

function datatableInvoice($q, RequestServices, apiService, moveBoardApi, SweetAlert) {
	return {
		scope: {
			totalsTop: '='
		},
		controller: datatableInvoiceController,
		replace: true,
		template: require('./datatableInvoice.html'),
		restrict: 'A',
	};
	
	function datatableInvoiceController($scope, $q, RequestServices, EditRequestServices, datacontext) {
		$scope.busy = true;
		$scope.sortingDirection = true;
		
		const TIME_ZONE = datacontext.getFieldData().timeZone;
		MOMENT_TIMEZONE.tz.add(TIME_ZONE.initial);
		
		
		$scope.sortingFields = angular.copy(sortingFields);
		$scope.filterFlag = '-1';
		$scope.startDate = '';
		$scope.endDate = '';
		$scope.currentPage = 0;
		$scope.pageStartsFrom = 0;
		$scope.pageEndsOn = 0;
		$scope.totalPages = 0;
		$scope.totalInvoices = 0;
		$scope.invoices = [];
		
		$scope.statusNames = ['draft', 'sent', 'viewed', 'paid', 'autopaid', 'failed'];
		$scope.statusesInSelect = [{name: 'paid', code: 3}, {name: 'unpaid', code: [0, 1, 2, 4, 5]}];
		
		$scope.clickOnField = clickOnField;
		$scope.clickCurrMonth = clickCurrMonth;
		$scope.clickPrevMonth = clickPrevMonth;
		$scope.setPage = setPage;
		$scope.openRequest = openRequest;
		$scope.previousPage = previousPage;
		$scope.nextPage = nextPage;
		$scope.getInvoicesFromApi = getInvoicesFromApi;
		$scope.openInvoice = openInvoice;
		
		$scope.$on('updateInvoices', $scope.getInvoicesFromApi);
		$scope.$watch('filterFlag', filterChanged);
		
		init();
		
		function init() {
			setDatesToCurrMonth();
			
			chooseDefaultSorting('id');
			
			getInvoicesFromApi();
			
			function chooseDefaultSorting(sortingName) {
				if ($scope.sortingFields[sortingName] == undefined) {
					sortingName = 'id';
				}
				$scope.sortingFields[sortingName].sortBy = true;
			}
		}
		
		function filterChanged(oldVal, newVal) {
			if (oldVal != newVal) {
				$scope.getInvoicesFromApi();
			}
		}
		
		function clickPrevMonth() {
			setDatesToPrevMonth();
			$scope.getInvoicesFromApi();
		}
		
		function clickCurrMonth() {
			setDatesToCurrMonth();
			$scope.getInvoicesFromApi();
		}
		
		function setDatesToCurrMonth() {
			let today = new Date();
			let firstOfMonth = angular.copy(today);
			firstOfMonth.setDate(1);
			
			$scope.startDate = firstOfMonth;
			$scope.endDate = today;
		}
		
		function setDatesToPrevMonth() {
			let today = new Date();
			today.setDate(0);
			let firstOfMonth = angular.copy(today);
			firstOfMonth.setDate(1);
			
			$scope.startDate = firstOfMonth;
			$scope.endDate = today;
		}
		
		function getInvoicesFromApi() {
			$scope.busy = true;
			let defer = $q.defer();
			
			let fieldName = 'id';
			
			for (let field in $scope.sortingFields) {
				if ($scope.sortingFields[field].sortBy) {
					fieldName = field;
				}
			}
			
			let data = {
				sort: fieldName,
				page: $scope.currentPage,
				direction: $scope.sortingDirection ? 'DESC' : 'ASC',
				field: fieldName,
				filter: {
					date_from: MOMENT_TIMEZONE.tz($scope.startDate, TIME_ZONE.name).startOf('day').unix(),
					date_to: MOMENT_TIMEZONE.tz($scope.endDate, TIME_ZONE.name).endOf('day').unix(),
					flag: $scope.filterFlag == '-1' ? null : $scope.statusesInSelect[$scope.filterFlag].code
				}
			};
			
			apiService.postData(moveBoardApi.invoice.getInvoicesForDashboard, data).then(res => {
				if (res.data.status_code && res.data.status_code != 200) {
					onBadResponce(res.data.status_message);
				} else {
					parseResponce(res.data);
				}
				$scope.busy = false;
			}, rej => {
				onBadResponce('Cannot connect to server');
				$scope.busy = false;
			});
			
			function onBadResponce(reason) {
				SweetAlert.swal('Error', 'Cannot get invoices\n' + reason, 'error');
			}
			
			return defer.promise;
		}
		
		function parseResponce(data) {
			if (data.items) {
				$scope.invoices = data.items.map(invoice => {
					invoice.invoice_date = getDate(invoice.invoice_date);
					return invoice;
				});
			} else {
				$scope.invoices = [];
			}
			$scope.totalPages = data._meta.pageCount;
			$scope.totalInvoices = data._meta.totalCount;
			$scope.currentPage = data._meta.currentPage;
			$scope.pageStartsFrom = 0;
			$scope.pageEndsOn = 0;
			if ($scope.totalInvoices) {
				$scope.pageStartsFrom = $scope.currentPage * PAGESIZE + 1;
				let lastOnPage = ($scope.currentPage + 1) * PAGESIZE;
				$scope.pageEndsOn = (lastOnPage > $scope.totalInvoices) ? $scope.totalInvoices : lastOnPage;
			}
			
			$scope.totalsTop.all = data.count.all;
			$scope.totalsTop.paid = data.count.paid;
			$scope.totalsTop.unpaid = data.count.unpaid;
			
			$scope.totalsTop.unpaidMoney = data.sum.unpaid;
			$scope.totalsTop.allMoney = data.sum.all;
			$scope.totalsTop.paidMoney = data.sum.paid;
			
		}
		
		function getDate(date) {
			if (_.isDate(date)) {
				return moment(date).format('MMM DD, YYYY');
			} else {
				return moment.unix(+date).format('MMM DD, YYYY');
			}
		}
		
		function clickOnField(fieldName) {
			if (fieldName == 'hash') fieldName = 'id';
			
			if ($scope.sortingFields[fieldName].sortBy) {
				$scope.sortingDirection = !$scope.sortingDirection;
			} else {
				for (let field in $scope.sortingFields) {
					$scope.sortingFields[field].sortBy = false;
				}
				$scope.sortingFields[fieldName].sortBy = true;
			}
			
			getInvoicesFromApi();
		}
		
		function previousPage() {
			setPage($scope.currentPage - 1);
		}
		
		function nextPage() {
			setPage($scope.currentPage + 1);
		}
		
		function setPage(newPage) {
			if (newPage != $scope.currentPage) {
				$scope.currentPage = newPage;
				getInvoicesFromApi();
			}
		}
		
		function openRequest(requestId) {
			$scope.busy = true;
			let superPromise = RequestServices.getSuperRequest(requestId);
			superPromise.then(function (data) {
				$scope.busy = false;
				EditRequestServices.requestEditModal(data.request, data.requests_day, data);
			});
		}
		
		function openInvoice(hash) {
			open('/account/#/invoice/' + hash, '_blank');
		}
		
	}
}