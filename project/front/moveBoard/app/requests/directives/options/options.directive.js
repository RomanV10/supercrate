(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccOptions', ccOptions);

    function ccOptions ($http, config, logger, RequestServices, SweetAlert, CalculatorServices, common, apiService, moveBoardApi, SendEmailsService, $rootScope) {
        return {
            link: link,
            scope: {
                'request': '='
            },
            template: require('./options.template.html'),
            restrict: 'A'
        };

        function link (scope, element, attrs) {

            var nid = scope.request.nid;
            scope.busy = true;
            scope.loadOptions = loadOptions;
            scope.active = 0;
            scope.editMode = false;
            scope.added = false;

            let pickup = moment(scope.request.date.value).valueOf();
            let linfo = CalculatorServices.getLongDistanceInfo(scope.request);
            let ld_days = angular.isDefined(linfo.days) ? linfo.days : '';
	        $rootScope.$broadcast('loading-request-is-stopped');

            const DEFAULT_DETAILS = {
                "current_door": "1",
                "new_door": "1",
                "current_permit": "None",
                "new_permit": "None",
                "addcomment": "",
                "admincomments": "",
                "pickup": "",
                "delivery": "",
                "delivery_days": ld_days,
                "steps_origin": "1",
                "steps_destination": "1"
            };

            scope.options = [];
            var optionsWasChosen = false;

            initOption();
            scope.loadOptions(nid);

            scope.$on('request.details_updated', function () {
                common.$timeout(function () {
                    scope.loadOptions(nid);
                }, 1000);
            });

            ////////////////////////////////////
            function loadOptions (nid) {
                $http.get(config.serverUrl + 'server/move_invertory/' + nid)
                    .success(function (data, status, headers, config) {
                        scope.busy = false;
                        scope.details = data.move_details;

                        if (scope.details != null) {
                            if (angular.isDefined(scope.details.options)
                                && (_.isUndefined(scope.options) || _.isEmpty(scope.options))
                                && scope.details.options) {

                                scope.options = scope.details.options;
                            }
                        }

                        if (angular.isUndefined(scope.details)
                            || _.isNull(scope.details)) {

                            scope.details = angular.copy(DEFAULT_DETAILS);
                            scope.details['options'] = [];

                            if ((_.isUndefined(scope.options) || _.isEmpty(scope.options))
                                && scope.details.options) {

                                scope.options = scope.details.options;
                            }
                        }

                        if (angular.isUndefined(scope.details)) {
                            if (!_.isEmpty(scope.details.pickup)
                                && _.isString(scope.details.pickup)
                                && scope.details.pickup.indexOf('-') == -1) {

                                let dates = scope.details.pickup;

                                var deli = [];
                                var isNumberDates = angular.isNumber(dates);
                                if (!isNumberDates) {
                                    deli = dates.split(",");
                                    var firstDate = parseInt(deli[0]);
                                    if (!(angular.isNumber(firstDate) && !isNaN(firstDate)) || !angular.isNumber(parseInt(deli[1]))) {
                                        deli[0] *= moment(dates).unix() * 1000;
                                        deli[1] *= moment(dates).unix() * 1000;
                                    }
                                } else {
                                    deli[0] = dates;
                                    deli[1] = dates;
                                }

                                if (deli[0] != deli[1]) {
                                    pickup = moment(Math.min(parseInt(deli[0]), parseInt(deli[1]))).format("YYYY-MM-DD");
                                    scope.details.pickup = Math.min(parseInt(deli[0]), parseInt(deli[1]));
                                    scope.secondPickUp = moment(Math.max(parseInt(deli[0]), parseInt(deli[1]))).format("MMM D, YYYY")
                                } else {
                                    pickup = moment(parseInt(deli[0])).format("YYYY-MM-DD");
                                    scope.details.pickup = parseInt(deli[0]);
                                }
                            }
                        }
                    });
            }

            scope.addOption = function (option) {
                // validate
                scope.added = true;
                var option = scope.option;

                if (option.picktime1 == 0 || option.deltime1 == 0 || !option.pickup.length || !option.delivery.length || !option.rate.length) {
                    return false;
                }

                scope.options.push(option);
                scope.added = false;

                initOption();
            };

            scope.updateOption = function (option, index) {
                // validate
                scope.options[index] = option;
                scope.editMode = false;
                scope.active = index;

                initOption();
            };

            scope.deleteOption = function (index) {
                scope.options.splice(index, 1);
                scope.editMode = false;

                initOption();
            };

            scope.editOption = function (option, index) {
                // validate
                scope.option = option;
                scope.editMode = true;
                scope.active = index;
            };

            scope.chooseOption = function (option) {
                SweetAlert.swal({
                        title: "Are you sure you want to choose option?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Choose",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            // validate
                            var nid = scope.request.nid;
                            var data = {
                                "options": option,
                                "requestId": nid
                            };

                            var promise = RequestServices.sendOption(data);

                            promise.then(function (data) {
                                scope.$broadcast('option.choosed', scope.option);
                                optionsWasChosen = true;
                            }, function (reason) {
                                logger.error(reason, reason, "Error");
                            });
                        }
                        else {

                        }
                    });
            };

            function initOption () {
                scope.option = {
                    "pickup": moment(pickup).format("MMMM DD, YYYY"),
                    "delivery": '',
                    "picktime1": 0,
                    "picktime2": 0,
                    "deltime1": 0,
                    "deltime2": 0,
                    "rate": '',
                    "text": scope.request.option_text
                }
            }

            scope.saveOptions = function () {
                var nid = scope.request.nid;
                var data = {};
                data.data = {};
                data.data.details = scope.details;
                data.data.details['options'] = scope.options;
                var jsonDetails = JSON.stringify(data);

                scope.busy = true;

                $http.post(config.serverUrl + 'server/move_invertory/' + nid, jsonDetails)
                    .success(function (data, status, headers, config) {
                        scope.busy = false;
                        scope.logs = data;
                        scope.changed = false;
                        logger.success("Details were updated", data, "Success");

                        if (_.isUndefined(scope.request.inventory.move_details)) {
                            scope.request.inventory.move_details = {};
                        }

                        scope.request.inventory.move_details.options = _.clone(scope.options);

                        var emailParameters = {
                            keyNames: ['send_option_flat_rate']
                        };

                        SendEmailsService
                            .prepareForEditEmails(emailParameters)
                            .then(function (data) {
                                var emails = data.emails;

                                if (_.isEmpty(emails)) {
                                    SweetAlert.swal("Warning!", "Email was not send", "warning");
                                } else {
                                    var email = _.head(emails);

                                    SendEmailsService
                                        .sendEmails(scope.request.nid, [email]);
                                }
                            });
                    });

                var data = {
                    field_approve: 11
                };

                RequestServices.updateRequest(nid, data);
                toastr.success("Updated!", "Options was send");
            };


            element.find(".delivery_time_select_from").on("change", function () {
                var time = $(this).val();
                element.find(".delivery_time_select_to option").prop("disabled", false).show();

                for (var i = 0; i <= time; i++) {
                    element.find(".delivery_time_select_to option[value='" + i + "']").prop("disabled", true).hide();
                }
            });

            element.find(".pickup_time_select_from").on("change", function () {
                var time = $(this).val();
                element.find(".pickup_time_select_to option").prop("disabled", false).show();

                for (var i = 0; i <= time; i++) {
                    element.find(".pickup_time_select_to option[value='" + i + "']").prop("disabled", true).hide();
                }
            });

            scope.isOptionsWasSend = function () {
                return angular.isDefined(scope.request.inventory.move_details)
                    && !_.isNull(scope.request.inventory.move_details)
                    && (!_.isEmpty(scope.request.inventory) ? scope.request.inventory.move_details.hasOwnProperty('options') : false)
                    && !scope.isOptionsChosen();
            };

            scope.isOptionsChosen = function () {
                return !_.isUndefined(scope.request.date.value) && !_.isNull(scope.request.date.value)
                    && !_.isUndefined(scope.request.ddate.value) && !_.isNull(scope.request.ddate.value) && !_.isEmpty(scope.request.ddate.value)
                    && !_.isUndefined(scope.request.dstart_time1.value) && !_.isNull(scope.request.dstart_time1.value)
                    && !_.isUndefined(scope.request.rate.value) && !_.isNull(scope.request.rate.value)
                    && !_.isUndefined(scope.request.reservation_rate.value) && !_.isNull(scope.request.reservation_rate.value)
                    && !_.isUndefined(scope.request.start_time1.value) && !_.isNull(scope.request.start_time1.value)
                    && !_.isUndefined(scope.request.start_time2.value) && !_.isNull(scope.request.start_time2.value)
                    && (!_.isEmpty(scope.request.inventory) ? scope.request.inventory.move_details.hasOwnProperty('options') : false)
                    || optionsWasChosen;
            }
        }
    }
})();
