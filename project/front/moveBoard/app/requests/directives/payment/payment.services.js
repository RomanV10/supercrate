'use strict';

import './payment-modal.styl';

angular
	.module('move.requests')
	.factory('PaymentServices', PaymentServices);

/* @ngInject */
function PaymentServices($http, $rootScope, $q, config, $uibModal, RequestServices, SweetAlert, AuthenticationService, apiService, moveBoardApi, Raven) {
	var authModalInstance = [];

	return {
		getPaymentFingerPrint: getPaymentFingerPrint,
		openContractPaymentModal: openContractPaymentModal,
		openPaymentModal: openPaymentModal,
		getPaymentArray: getPaymentArray,
		calcPayment: calcPayment,
		calcInvoices: calcInvoices,
		openReceiptModal: openReceiptModal,
		openAuthPaymentModal: openAuthPaymentModal,
		closeAuthModal: closeAuthModal,
		get_receipt: get_receipt,
		set_receipt: set_receipt,
		update_receipt: update_receipt,
		delete_receipt: delete_receipt,
		openCustomPaymentModal: openCustomPaymentModal,
		getPackingTotal: getPackingTotal,
		openAddPackingModal: openAddPackingModal,
		openAuthRedirectModal: openAuthRedirectModal,
		setAuthorizeNet: setAuthorizeNet,
		getAuthorizeNet: getAuthorizeNet,
		applyPayment: applyPayment,
		applySitPayment: applySitPayment,
		saveCardImage: saveCardImage,
		getCardImage: getCardImage,
		refundReceipt: refundReceipt,
		openRefundModal: openRefundModal,
		sendMsgLog: sendMsgLog
	};

	// Requests to API

	function refundReceipt(payment_info) {
		var data = payment_info;
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payment/refund', data).success(function (data) {
			deferred.resolve(data);
		}).error(function (data) {
			deferred.reject(data);
		});

		return deferred.promise;
	}

	function saveCardImage(nid, data) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/move_request/set_card/' + nid, {
			data: data
		}).success(function (data) {
			deferred.resolve(data);
		}).error(function (data) {
			deferred.reject(data);
		});

		return deferred.promise;
	}

	function applyPayment(payment_info, nid, entity_type) {
		var data = payment_info;
		data.entity_id = nid;
		data.entity_type = entity_type;
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payment', data).success(function (data) {
			deferred.resolve(data);
		}).error(function (error) {
			deferred.reject(error);
		});

		return deferred.promise;
	}

	function applySitPayment(payment_info, nid, entity_type) {
		let data = payment_info;
		data.entity_id = nid;
		data.entity_type = entity_type;
		data.data.type = "Authorized Credit Card";

		return $http.post(config.serverUrl + 'server/long_distance_payments/set_receipt', data);
	}

	function setAuthorizeNet(loginid, transactionkey) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payment/set_authorizenet', {
			loginid: loginid,
			transactionkey: transactionkey
		}).success(function (data) {
			deferred.resolve(data);
		}).error(function (data) {
			deferred.reject(data);
		});

		return deferred.promise;
	}

	function getAuthorizeNet() {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payment/get_authorizenet').success(data => {
			deferred.resolve(data);
		}).error(data => {
			deferred.reject(data);
		});

		return deferred.promise;
	}

	function delete_receipt(id, isTrip = false) {
		if (isTrip) {
			return deleteSITReceipt(id);
		} else {
			return deleteRegularReceipt(id);
		}
	}

	function deleteRegularReceipt (id) {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.receipt.deleteReceipt, {
			rid: id
		})
			.then(resolve => {
				defer.resolve(resolve.data);
			}, reject => {
				defer.reject(reject.data);
			});

		return defer.promise;
	}

	function deleteSITReceipt (id) {
		let defer = $q.defer();

		apiService.postData(`${moveBoardApi.ldReceipt.deleteReceipt}/${id}`, {})
			.then(resolve => {
				defer.resolve(resolve.data);
			}, reject => {
				defer.reject(reject.data);
			});

		return defer.promise;
	}

	function set_receipt(nid, value, entity_type) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/move_request/set_receipt', {
			entity_id: nid,
			entity_type: entity_type,
			data: value
		}).success(function (data) {
			deferred.resolve(data);
		}).error(function (data) {
			deferred.reject(data);
		});

		return deferred.promise;
	}

	function update_receipt(nid, value, isTrip = false) {
		let defer = $q.defer();

		let method = moveBoardApi.receipt.updateReceipt;

		if (isTrip) {
			method = moveBoardApi.ldReceipt.updateReceipt;
		}

		apiService.postData(method, {
			id: value.id || nid,
			data: value
		})
			.then(resolve => {
				defer.resolve(resolve.data);
			})
			.catch(err => {
				Raven.captureException(`Update receipt: ${err}`);
				defer.reject(err);
			});

		return defer.promise;
	}

	function get_receipt(nid, entity_type) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/move_request/get_receipt', {
			entity_id: nid,
			entity_type: entity_type
		}).success(function (data) {
			deferred.resolve(data);
		}).error(function (data) {
			deferred.reject(data);
		});

		return deferred.promise;
	}

	function getPaymentFingerPrint(value) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/move_request/fingerprint', {
			amount: value
		}).success(function (data) {
			deferred.resolve(data);
		}).error(function (data) {
			deferred.reject(data);
		});

		return deferred.promise;
	}

	function getCardImage(receiptID) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/move_request/get_card/' + receiptID).success(data => {
			deferred.resolve(data);
		}).error(function (data) {
			deferred.reject(data);
		});

		return deferred.promise;
	}

	// Common logic

	function sendMsgLog(receipt, id, entitytype) {
		if (receipt.invoice_id != 'custom') {
			var arrLog = [];
			var msg = {
				simpleText: receipt.name + ' made a payment in the amount  $' + receipt.amount
			};
			arrLog.push(msg);
			msg = {
				simpleText: 'Card type: ' + receipt.card_type + '. Card Number ****' + receipt.credit_card.card_number.substr(-4)
			};
			arrLog.push(msg);
		} else {
			return false;
		}

		var currUser = $rootScope.currentUser;

		if (angular.isUndefined(currUser)) {
			currUser = [];
			currUser.userRole = ['anonymous'];
			currUser.userId = [];
			currUser.userId.field_user_first_name = 'anonymous';
			currUser.userId.field_user_last_name = '';
		}

		var userRole = '';

		if (currUser.userRole.indexOf('administrator') >= 0) {
			userRole = 'administrator';
		} else if (currUser.userRole.indexOf('manager') >= 0) {
			userRole = 'manager';
		} else if (currUser.userRole.indexOf('sales') >= 0) {
			userRole = 'sales';
		} else if (currUser.userRole.indexOf('foreman') >= 0) {
			userRole = 'foreman';
		}

		var msg = {
			entity_id: id,
			entity_type: entitytype,
			data: [{
				details: [{
					title: 'Online payment',
					text: [],
					activity: userRole,
					date: moment().unix(),
					info: {
						browser: AuthenticationService.get_browser(),
					}
				}],
				source: currUser.userId.field_user_first_name + ' ' + currUser.userId.field_user_last_name
			}]
		};

		msg.data[0].details[0].text = arrLog;
		RequestServices.postLogs(msg);
	}

	function calcPayment(payments) {
		var total = 0;
		angular.forEach(payments, function (payment) {
			if (payment.refunds) {
				total -= _.round(payment.amount, 2);
			} else if (!payment.pending && angular.isDefined(payment.pending)) {
				total += _.round(payment.amount, 2);
			} else if (payment.credit_card) {
				total += _.round(payment.credit_card.amount, 2);
			}
		});

		return total;
	}

	function calcInvoices(invoices) {
		var total = 0;
		_.forEach(invoices, function (invoice) {
			total += Number(invoice.data.totalInvoice);
		});
		total = _.round(total, 2);

		return total;
	}

	function getPaymentArray(amount) {
		var receipt = {
			account_number: "XXXX1111",
			amount: _.round(amount, 2),
			auth_code: "V0T6XO",
			card_type: "Visa",
			date: "December 20, 2015, 10:28 pm",
			description: "Move Reservation #61992",
			email: "roma4ke@gmail.com",
			first_name: "Roman",
			invoice_id: "61992",
			last_name: "Halavach",
			payment_method: "CC",
			phone: "(201) 680-8055",
			transaction_id: "2247203984",
			pending: false,

		};

		return receipt;
	}

	function getPackingTotal(packings) {
		let add_extra_charges = [];
		let total_services = 0;

		if (packings && packings != '') {
			add_extra_charges = (angular.fromJson(packings)).packings;
		}

		_.forEach(add_extra_charges, (charge) => {
			total_services += parseFloat(charge.total || 0);
		});

		return _.round(total_services, 2);
	}

	// MODALS
	function saveAuthModal(modalInstance) {
		authModalInstance = modalInstance;
	}

	function closeAuthModal() {
		authModalInstance.dismiss('cancel');
	}

	function openAuthPaymentModal(request, step, data_receipt, entity_type, $scope) {
		var currUser = $rootScope.currentUser;

		if (angular.isUndefined(currUser)) {
			currUser = {
				userRole: ['anonymous'],
				userId: {
					field_user_first_name: 'anonymous',
					field_user_last_name: ''
				}
			};
		}

		var userRole = '';

		if (currUser.userRole.indexOf('administrator') >= 0) {
			userRole = 'administrator';
		} else if (currUser.userRole.indexOf('manager') >= 0) {
			userRole = 'manager';
		} else if (currUser.userRole.indexOf('sales') >= 0) {
			userRole = 'sales';
		} else if (currUser.userRole.indexOf('foreman') >= 0) {
			userRole = 'foreman';
		}

		var serverError;
		var msg = {
			entity_id: request.nid,
			entity_type: entity_type,
			data: [{
				details: [{
					title: "Payment window opened on " + ((request.agreement || (request.request_all_data && request.request_all_data.agreementSave)) ? "rental agreement" : "contract") + " page",
					text: [],
					activity: userRole,
					date: moment().unix()
				}],
				source: currUser.userId.field_user_first_name + ' ' + currUser.userId.field_user_last_name
			}]
		};

		var obj = {
			text: angular.isDefined(step) ? 'Automatically' : 'Foreman click Pay button.'
		};

		msg.data[0].details[0].text.push(obj);

		if (request.payment_flag != 'contract' && !request.agreement) {
			serverError = false;
			var modalInstance = $uibModal.open({
				template: require('./authModal.html'),
				controller: 'AuthModalCtrl',
				backdrop: false,
				keyboard: false,
				size: 'lg',
				windowClass: 'payment-modal',
				resolve: {
					request: function () {
						return request;
					},
					serverError: function () {
						return serverError;
					},
					step: function () {
						return step;
					},
					data_receipt: function () {
						return data_receipt;
					},
					entity_type: function () {
						return entity_type;
					},
					page_id: getPageId
				}
			});
			saveAuthModal(modalInstance);
			return false;
		}

		RequestServices.postLogs(msg).then(function () {
			serverError = false;
			var modalInstance = $uibModal.open({
				template: require('./authModal.html'),
				controller: 'AuthModalCtrl',
				backdrop: false,
				keyboard: false,
				size: 'lg',
				windowClass: 'payment-modal',
				resolve: {
					request: function () {
						return request;
					},
					serverError: function () {
						return serverError;
					},
					step: function () {
						return step;
					},
					data_receipt: function () {
						return data_receipt;
					},
					entity_type: function () {
						return entity_type;
					},
					page_id: getPageId
				}
			});
			saveAuthModal(modalInstance);
			modalInstance.opened.then(function () {

				if (angular.isDefined($scope)) {
					if (angular.isDefined($scope.openPay)) $scope.openPay = false;
					if (angular.isDefined($scope.payClicked)) $scope.payClicked = false;
				}
			});
		}, () => {
			serverError = true;
			var modalInstance = $uibModal.open({
				template: require('./authModal.html'),
				controller: 'AuthModalCtrl',
				backdrop: false,
				keyboard: false,
				size: 'lg',
				windowClass: 'payment-modal',
				resolve: {
					request: function () {
						return request;
					},
					serverError: function () {
						return serverError;
					},
					step: function () {
						return step;
					},
					data_receipt: function () {
						return data_receipt;
					},
					entity_type: function () {
						return entity_type;
					},
					page_id: getPageId
				}
			});

			SweetAlert.swal("Internet connection problem. Please contact your manager.", "", "error");
			saveAuthModal(modalInstance);
			modalInstance.opened.then(function () {
				$scope.openPay = false;
			});
		});

		function getPageId() {
			return _.get($scope, 'contractPage.page_id');
		}
	}

	function openCustomPaymentModal(request, entitytype) {
		var modalInstance = $uibModal.open({
			template: require('./customPaymentModal.html'),
			controller: 'CustomModalCtrl',
			resolve: {
				request: function () {
					return request;
				},
				entitytype: function () {
					return entitytype;
				}
			}
		});

		modalInstance.result.then();
	}

	function openAddPackingModal(request, invoice, type, grandTotal, grandTotalInvoice) {

		let modalInstance = $uibModal.open({
			template: require('./addPackingModal.html'),
			controller: 'PackingModalInstanceCtrl',
			size: 'lg',
			resolve: {
				request: () => request,
				invoice: () => invoice,
				type: () => type,
				grandTotal: () => grandTotal,
				grandTotalInvoice: () => grandTotalInvoice
			}
		});

		modalInstance.result.then();
	}

	function openReceiptModal(request, receipt, isAccount, entity) {
		var modalInstance = $uibModal.open({
			template: require('./controllers/receipt-modal/receiptModal.html'),
			controller: 'ReceiptModalCtrl',
			resolve: {
				receipt: function () {
					return receipt;
				},
				request: function () {
					return request;
				},
				isAccount: function () {
					return isAccount;
				},
				entity: function() {
					return entity;
				}
			}
		});

		modalInstance.result.then();
	}

	function openAuthRedirectModal(request) {
		var modalInstance = $uibModal.open({
			template: require('./authredirectModal.html'),
			controller: 'AuthRedirectModalCtrl',
			resolve: {
				request: function () {
					return request;
				}
			}
		});

		saveAuthModal(modalInstance);
		modalInstance.result.then();
	}

	function openPaymentModal(request, entity) {
		var modalInstance = $uibModal.open({
			template: require('./paymentModal.html'),
			controller: 'PaymentModalCtrl',
			size: 'lg',
			resolve: {
				request: function () {
					return request;
				},
				entity: function() {
					return entity
				}
			},
		});

		modalInstance.result.then(() => {
			$rootScope.$broadcast('paymentModalClosed');
		});
	}

	function openRefundModal(request, uid, entity_type, receiptData) {
		var modalInstance = $uibModal.open({
			template: require('./refundModal.html'),
			controller: 'refundModalInstanceCtrl',
			size: 'lg',
			backdrop: false,
			resolve: {
				request: function () {
					return request;
				},
				uid: function () {
					return uid;
				},
				entity_type: function () {
					return entity_type;
				},
				receiptData: function () {
					return receiptData;
				}
			}
		});

		modalInstance.result.then();
	}

	function openContractPaymentModal(request) {
		var modalInstance = $uibModal.open({
			template: require('./paymentContractModal.html'),
			controller: 'ContractPaymentModalCtrl',
			size: 'lg',
			resolve: {
				request: function () {
					return request;
				},
			}
		});
		modalInstance.result.then();
	}
}
