'use strict';

angular
	.module('move.requests')
	.controller('refundModalInstanceCtrl', refundModalInstanceCtrl);

refundModalInstanceCtrl.$inject = ['$scope', 'request', 'uid', 'entity_type', 'receiptData', 'common', '$uibModalInstance', 'datacontext', '$rootScope', 'SweetAlert', 'RequestServices', 'PaymentServices'];

function refundModalInstanceCtrl ($scope, request, uid, entity_type, receiptData, common, $uibModalInstance, datacontext, $rootScope, SweetAlert, RequestServices, PaymentServices) {
	$scope.request = request;
	let entityType = 'MOVEREQUEST';
	$scope.range = common.range;
	$scope.currentYear = +moment().format('YYYY');
	
	if (!$scope.request.zip) {
		if ($scope.request.field_moving_from) {
			$scope.request.zip = $scope.request.field_moving_from.postal_code;
		}
	}
	
	$scope.data = {
		"uid": $scope.request.uid.uid,
		"entity_id": $scope.request.nid,
		"entity_type": entity_type || 0,
		"name": receiptData.name ? receiptData.name : $scope.request.name,
		"email": receiptData.email ? receiptData.email : $scope.request.email,
		"phone": receiptData.phone ? receiptData.phone : $scope.request.phone,
		"zip_code": receiptData.zip_code ? receiptData.zip_code : $scope.request.zip,
		"credit_card": {
			"card_number": '',
			"exp_date": '',
			"card_code": ''
		},
		"amount": receiptData.amount || 0,
		"description": '',
		"trans_id": receiptData.transaction_id || '',
		"receipt_id": receiptData.id,
	};
	
	$scope.fieldData = datacontext.getFieldData();
	$scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);
	$scope.entitytype = $rootScope.fieldData.enums.entities.MOVEREQUEST;
	
	if (angular.isDefined(entity_type)) {
		$scope.entitytype = entity_type;
		entityType = 'STORAGEREQUEST';
	}
	
	$scope.applyRefund = function () {
		$scope.data.credit_card.exp_date = moment().set('month', Number($scope.expmonth) - 1).set('year', $scope.expyear).format('MM/YY');
		$scope.busy = true;
		PaymentServices.refundReceipt($scope.data).then(function (data) {
			if (_.head(data) == false) {
				SweetAlert.swal("Error!", "Payment refund rejected!", "error");
				$scope.busy = false;
				return false;
			}
			
			if (!data.status) {
				SweetAlert.swal("Payment refund rejected!", data.text, "error");
				$scope.busy = false;
				return false;
			}
			
			$rootScope.$broadcast('receipt_sent');
			$scope.busy = false;
			var message = "Payment in the amount  $" + $scope.data.amount + ' was refund.';
			var obj = {
				simpleText: message
			};
			var arr = [obj];
			RequestServices.sendLogs(arr, 'Refund Payment', $scope.request.nid, entityType);
			toastr.success("Success!", "Payment refunded!");
			$rootScope.$broadcast('initGetReceipts');
			$uibModalInstance.dismiss('cancel');
		}, function (err) {
			$scope.busy = false;
			SweetAlert.swal("Failed!", err, "error")
		});
	};
	
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}
