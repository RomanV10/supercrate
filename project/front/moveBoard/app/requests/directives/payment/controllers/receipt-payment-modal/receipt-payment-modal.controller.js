'use strict';

angular
	.module('move.requests')
	.controller('ReceiptPaymentModalCtrl', ReceiptPaymentModalCtrl);

ReceiptPaymentModalCtrl.$inject = ['$scope', '$uibModalInstance', 'receipt', 'PaymentServices', '$uibModal'];

function ReceiptPaymentModalCtrl($scope, $uibModalInstance, receipt, PaymentServices, $uibModal) {
	$scope.customType = customType;
	$scope.cancel = cancel;
	$scope.openCardPhoto = openCardPhoto;
	$scope.dateFormat = dateFormat;
	
	function init() {
		$scope.receipt = receipt;
		if (_.get($scope.receipt, 'id', false)) {
			$scope.busy = true;
			PaymentServices.getCardImage($scope.receipt.id).then(data => {
				$scope.cards = data;
			}).finally(() => {
				$scope.busy = false;
			});
		}
	}
	
	function customType(receipt) {
		let customTypeReceipt;
		switch (receipt.type) {
			case "cash":
				customTypeReceipt = '(cash)';
				break;
			case 'check':
				customTypeReceipt = '(check)';
				break;
			case 'creditcard':
				customTypeReceipt = '(card)';
				break;
			case "customrefund":
				customTypeReceipt = '(custom refund)';
				break;
			default:
				customTypeReceipt = 'Payment';
				break;
		}
		return customTypeReceipt;
	}
	
	function cancel() {
		$uibModalInstance.dismiss('cancel');
	}
	
	function openCardPhoto(img) {
		var modalInstance = $uibModal.open({
			template: require('../../../../../../../account/app/contractpage/templates/CardPhoto.html'),
			controller: 'CardPhotoModalCtrl',
			resolve: {
				img: function () {
					return img;
				}
			},
		});
		modalInstance.result.then(function ($scope) {
		});
	}
	
	function dateFormat(date) {
		return _.isNaN(moment.unix(date)._i) ? moment(date).format('DD MMM, YYYY') : moment.unix(date).format('DD MMM, YYYY h:mm a');
	}
	
	init();
}
