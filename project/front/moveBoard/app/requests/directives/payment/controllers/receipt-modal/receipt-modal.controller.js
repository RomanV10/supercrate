'use strict';

import './receipt-modal.styl';

angular
	.module('move.requests')
	.controller('ReceiptModalCtrl', ReceiptModalCtrl);

/*@ngInject*/
function ReceiptModalCtrl ($scope, $uibModalInstance, receipt, request, Session, common, $rootScope, isAccount, entity,
	EditRequestServices, PaymentServices, $uibModal) {
	$scope.isNotAccount = !isAccount;
	$scope.isAccount = isAccount;
	$scope.entity = entity;
	$scope.receipt = receipt;
	$scope.request = request;
	$scope.range = common.range;
	let session = Session.get();
	$scope.isAdmin = (session.userRole.indexOf('administrator') > -1);
	$scope.cardImages = [];

	function init() {
		if ($scope.isAdmin && _.get($scope.receipt, 'id', false)) {
			$scope.busy = true;
			PaymentServices.getCardImage($scope.receipt.id).then(data => {
				$scope.cardImages = data;
			}).finally(() => {
				$scope.busy = false;
			});
		}
	}

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.openCardPhoto = function (img) {
		var modalInstance = $uibModal.open({
			template: require('../../CardPhoto.html'),
			controller: 'CardPhotoModalCtrl',
			resolve: {
				img: function () {
					return img;
				}
			}
		});
		modalInstance.result.then(() => {});
	};

	function validateFields(){
		if (angular.element('.payment-receipt-modal .validate.ng-invalid').length) {
			toastr.error('Cannot save receipt', 'There are invalid fields');
			return false;
		}
		return true;
	}

	$scope.save = function () {
		if (!validateFields()) return;
		if ($scope.receipt.amount <= 0 && $scope.receipt.type != 'customrefund') {
			toastr.error('Error', 'Amount must be greater than zero.');
			return false;
		}
		let isTrip = $scope.receipt.payment_flag.includes('Trip') || $scope.receipt.payment_flag.includes('T/P');

		changeReceipt();
		$scope.busy = true;
		$scope.receipt.entity_id = $scope.entity.entity_id;
		$scope.receipt.entity_type = $scope.entity.entity_type;

		PaymentServices.update_receipt($scope.request.nid, $scope.receipt, isTrip)
			.then(res => {
				EditRequestServices.getQuickBookFlag($scope.request);
				$rootScope.$broadcast('custom.receipt.received', $scope.receipt);
				$scope.cancel();
			})
			.catch(err => {
				toastr.error('Cannot save receipt', err.status_message);
			})
			.finally(() => $scope.busy = false);
	};

	$scope.dateFormat = function (date) {
		let tryParseDate = moment.unix(date)._i;
		let result = _.isNaN(tryParseDate) ? moment(date, 'MM-DD-YYYY').format('DD MMM, YYYY') : moment.unix(date).format('DD MMM, YYYY h:mm a');
		return result;
	};

	function changeReceipt() {
		let indexReceipt;
		if (!_.isUndefined($scope.request.receipts)) {
			// MOVE REQUEST
			indexReceipt = _.findIndex($scope.request.receipts, {id: $scope.receipt.id});
			$scope.request.receipts[indexReceipt] = $scope.receipt;
			$rootScope.$broadcast('request.payment_receipts', $scope.request.receipts, $scope.request.nid);
		} else {
			//STORAGE REQUEST
			let payments = _.get($scope.request, 'data.payments', $scope.request.payments);
			indexReceipt = _.findIndex(payments, {id: $scope.receipt.id});
			payments[indexReceipt] = $scope.receipt;
			$rootScope.$broadcast('request.payment_receipts', payments, $scope.request.nid);
		}
	}

	init();
}
