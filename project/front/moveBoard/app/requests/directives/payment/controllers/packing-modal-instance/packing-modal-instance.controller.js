'use strict';

import {
	PACKING_MODAL
} from '../payment-common/payment-common'

angular
	.module('move.requests')
	.controller('PackingModalInstanceCtrl', PackingModalInstanceCtrl);

/* @ngInject */
function PackingModalInstanceCtrl ($scope, $uibModalInstance, request, invoice, type, grandTotal, grandTotalInvoice, datacontext, SettingServices, RequestServices, $rootScope, CalculatorServices, CommercialCalc, SweetAlert, calculatePackingService) {
	let settings = datacontext.getFieldData();
	$scope.settings = angular.fromJson(settings.basicsettings);
	$scope.calcSettings = angular.fromJson(settings.calcsettings);
	$scope.request = request;
	$scope.invoice = invoice;
	$scope.type = type;
	$scope.grandTotal = grandTotal;
	$scope.grandTotalInvoice = grandTotalInvoice;
	$scope.total_services = 0;
	$scope.add_extra_charges = [];
	$scope.addPacking = [];
	$scope.editPacking = [];
	$scope.removePacking = [];

	var importedFlags = $rootScope.availableFlags;
	var Job_Completed = 'Completed';
	$scope.Job_Completed = $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[Job_Completed]);

	var custom_extra = {
		name: 'Custom Packing',
		rate: 0,
		quantity: 1,
		custom: true,
		LDRate: 0,
		laborRate: 0,
	};

	var request_data = {};

	$scope.packing = {};
	$scope.packing.account = false;
	$scope.packing.labor = false;
	$scope.packingService = {};

	if (angular.isUndefined($scope.settings.packing_settings)) {
		$scope.settings.packing_settings = angular.copy(PACKING_MODAL.packingSettings);
		var setting = $scope.settings;
		var setting_name = 'basicsettings';
		SettingServices.saveSettings(setting, setting_name);
	} else {
		$scope.packingService = angular.copy(PACKING_MODAL.packingService);
		if (angular.isDefined($scope.settings.packing_settings.packingAccount)) {
			$scope.packing.account = $scope.settings.packing_settings.packingAccount;
		}

		if (angular.isDefined($scope.settings.packing_settings.packingLabor)) {
			$scope.packing.labor = $scope.settings.packing_settings.packingLabor;
		}

		if (angular.isDefined($scope.settings.packing_settings.packing)) {
			if (angular.isDefined($scope.settings.packing_settings.packing.LM)) {
				$scope.packingService.LM = $scope.settings.packing_settings.packing.LM;
			}
		}

		if (angular.isDefined($scope.settings.packing_settings.packing)) {
			if (angular.isDefined($scope.settings.packing_settings.packing.LD)) {
				$scope.packingService.LD = $scope.settings.packing_settings.packing.LD;
			}
		}
	}


	if ($scope.type == 'invoice') {
		if ($scope.invoice.request_data.value.length) {
			var temp = angular.fromJson($scope.invoice.request_data.value);

			if (angular.isObject(temp)
				&& !angular.isArray(temp)) {

				request_data = temp;
			}
		} else if (_.isObject($scope.invoice.request_data.value)
			&& !_.isArray($scope.invoice.request_data.value)) {
			request_data = $scope.invoice.request_data.value;
		}
	} else {
		if ($scope.request.request_data.value.length) {
			var temp = angular.fromJson($scope.request.request_data.value);

			if (angular.isObject(temp)
				&& !angular.isArray(temp)) {

				request_data = temp;
			}
		} else if (_.isObject($scope.request.request_data.value)
			&& !_.isArray($scope.request.request_data.value)) {
			request_data = $scope.request.request_data.value;
		}
	}

	if (!_.isEmpty(request_data)) {
		$scope.add_extra_charges = request_data.packings || [];
	}

	for (var service in $scope.add_extra_charges) {
		if ($scope.add_extra_charges[service].name == 'Estimate Partial Packing') {
			$scope.packing_service = 1;
		}

		if ($scope.add_extra_charges[service].name == 'Estimate Full Packing') {
			$scope.packing_service = 2;
		}
	}

	$scope.requestType = ($scope.request.service_type.raw == '5' || $scope.request.service_type.raw == '7');

	$scope.addExtraCharges = addExtraCharges;
	$scope.removeCharge = removeCharge;
	$scope.calculateTotals = calculateTotals;
	$scope.calculateRowTotal = calculateRowTotal;
	$scope.extra_charges = {};
	$scope.calculateTotalCharge = calculatePackingService.calculateTotalCharge;
	$scope.calculateLaborRowTotal = calculateLaborRowTotal;
	$scope.choosePackingService = choosePackingService;
	$scope.addExtraChargesPacking = addExtraChargesPacking;
	$scope.removeChargePacking = removeChargePacking;

	SettingServices.getSettings('packing_settings').then(function (data) {
		$scope.extra_charges = JSON.parse(data);
		$scope.extra_charges.unshift(custom_extra);
	});

	function calculateRowTotal (charge) {
		var one = 0;
		one += (charge.rate * charge.quantity);
		return _.round(one, 2);
	}

	function calculateLaborRowTotal (charge) {
		var one = 0;
		let requestType = ($scope.request.service_type.raw == '5' || $scope.request.service_type.raw == '7');
		if ($scope.packing.labor) {
			one += (requestType ? (parseFloat(charge.LDRate) + parseFloat(charge.laborRate)) : parseFloat(charge.rate)) * charge.quantity;
		} else {
			one += (requestType ? parseFloat(charge.LDRate) : parseFloat(charge.rate)) * charge.quantity;
		}
		return _.round(one, 2);
	}

	function fetchWeight(request) {
		let weight = {};
		if (request.move_size.raw != 11 || request.field_useweighttype.value == 2) {
			weight = CalculatorServices.getTotalWeight(request);
		} else {
			weight = CommercialCalc.getCommercialCubicFeet(request.commercial_extra_rooms.value[0], $scope.calcSettings, request.field_custom_commercial_item);
		}
		return weight;
	}

	function choosePackingService (value) {
		var weight = {};
		weight.weight = 0;
		let requestType = $scope.type;

		$scope[requestType].request_all_data.requiredPackingService = value;
		RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);

		if ($scope.request.status.raw == 3) {
			weight = fetchWeight($scope.invoice);
		} else {
			weight = fetchWeight($scope.request);
		}

		if (value == 0) {
			for (var i in $scope.add_extra_charges) {
				if ($scope.add_extra_charges[i].name == 'Estimate Full Packing' || $scope.add_extra_charges[i].name == 'Estimate Partial Packing') {
					removeChargePacking(i);
					return;
				}
			}
		} else if (value == 1 || value == 2) {
			let type = value == 1 ? 'Estimate Partial Packing' : 'Estimate Full Packing';
			let packingType = value == 1 ? 'partial' : 'full';
			let serviceType = ($scope.request.service_type.raw == '5' || $scope.request.service_type.raw == '7') ? 'LD' : 'LM';
			let rateValue = {};
			rateValue.LD = Number($scope.packingService.LD.full);
			rateValue.LM = Number($scope.packingService.LM.full);

			if (packingType == 'partial') {
				rateValue.LD = rateValue.LD * parseFloat($scope.packingService.LD[packingType] / 100);
				rateValue.LM = rateValue.LM * parseFloat($scope.packingService.LM[packingType] / 100);
			}


			for (var i in $scope.add_extra_charges) {
				if ($scope.add_extra_charges[i].name == 'Estimate Partial Packing' || $scope.add_extra_charges[i].name == 'Estimate Full Packing') {
					$scope.add_extra_charges[i].rate = rateValue.LM;
					$scope.add_extra_charges[i].LDRate = rateValue.LD;
					$scope.add_extra_charges[i].quantity = weight.weight;
					$scope.add_extra_charges[i].name = type;
					$scope.add_extra_charges[i].custom = true;
					$scope.add_extra_charges[i].total = _.round((serviceType == "LD" ? rateValue.LD : rateValue.LM) * weight.weight, 2);
					return;
				}
			}

			addExtraChargesPacking(type, rateValue, weight.weight);
		}
	}

	function addExtraChargesPacking (name, value, weight) {
		let LDServiceType = $scope.request.service_type.raw == '5' || $scope.request.service_type.raw == '7';
		$scope.request.request_all_data.removedPacking = 0;
		let customExtraTotal = (LDServiceType ? value.LD : value.LM) * weight;
		var custom_extra = {
			name: name,
			rate: value.LM,
			quantity: weight,
			LDRate: value.LD,
			laborRate: 0,
			showInContract: true,
			custom: true,
			originName: name,
			total: _.round(customExtraTotal, 2)
		};

		$scope.addPacking.push({
			text: `${custom_extra.name} was added.`
		});
		$scope.add_extra_charges.push(custom_extra);
	}

	function removeChargePacking (index) {
		$scope.removePacking.push({
			text: `Service ${$scope.add_extra_charges[index].name} was removed.'`
		});
		$scope.add_extra_charges.splice(index, 1);
	}

	function addPackingFlag(full, partial) {
		if (full) {
			$scope.request.request_all_data.removedPacking = 2;
		} else if (partial) {
			$scope.request.request_all_data.removedPacking = 1;
		} else {
			$scope.request.request_all_data.removedPacking = 0;
		}
	}

	function addExtraCharges (extra_charge) {
		let charge = _.find($scope.add_extra_charges, {
			name: extra_charge.name
		});

		if (_.isUndefined(charge)) {
			extra_charge.quantity = 1;
			$scope.add_extra_charges.push(extra_charge);
		} else {
			charge.quantity = Number(charge.quantity)+1;
			extra_charge = charge;
		}


		extra_charge.total = calculatePackingService.calculatePackingTotal(extra_charge, $scope.request.service_type.raw);

		$scope.addPacking.push({
			text: `Packing ${extra_charge.name} was added. Quantity: ${extra_charge.quantity}. Total: $${extra_charge.total}.`
		});

		calculateTotals();
	}

	function removeCharge (index) {
		let fullPacking = $scope.add_extra_charges[index].originName == 'Estimate Full Packing';
		let partialPacking = $scope.add_extra_charges[index].originName == 'Estimate Partial Packing';
		addPackingFlag(fullPacking, partialPacking);

		$scope.removePacking.push({
			text: `Packing ${$scope.add_extra_charges[index].name} was removed.`
		});
		$scope.add_extra_charges.splice(index, 1);
		calculateTotals();
	}

	function calculateTotals () {
		$scope.total_services = 0;
		angular.forEach($scope.add_extra_charges, function (charge) {
			$scope.total_services += parseFloat(charge.total);
		});

		return parseFloat($scope.total_services).toFixed(2);
	}

	$scope.editPackingLog = function (title, extra_charge, val) {
		extra_charge.total = calculatePackingService.calculatePackingTotal(extra_charge, $scope.request.service_type.raw);

		$scope.editPacking.push({
			text: `${title} on ${extra_charge.name} was changed.`,
			to: val
		});
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	function updateLogsArray(newLogs = [], details) {
		if (!details) return;

		_.forEach(newLogs, log => {
			details.text.push(log);
		});

		return details;
	}

	$scope.save = function () {
		$scope.busy = true;
		var details = {
			title: 'Packing service',
			text: [],
			date: moment().unix()
		};

		if ($scope.request.request_all_data.needPartialPacking
			|| $scope.request.request_all_data.needFullPacking) {
			$scope.request.request_all_data.requestHasPackingPrice = true;
		}
		details = updateLogsArray($scope.addPacking, details);
		details = updateLogsArray($scope.editPacking, details);
		details = updateLogsArray($scope.removePacking, details);

		if (!_.isEmpty($scope.add_extra_charges)) {
			request_data.packings = {};
			request_data.packings = $scope.add_extra_charges;
		}

		if ($scope.type == 'invoice') {
			$scope.invoice.request_data.value = angular.copy(request_data);
			$scope.request.request_all_data.invoice = angular.copy($scope.invoice);
			var message = "Packing was changed on closing tab. Total Packings: $" + $scope.calculateTotals($scope.add_extra_charges);
			var obj = {
				simpleText: message
			};
			details.text.push(obj);
			$scope.calculateTotals($scope.add_extra_charges);

			$scope.busy = false;
			$rootScope.$broadcast('request.packing_update', '');
			$rootScope.$broadcast('grandTotal.closing', {
				grand: $scope.grandTotal,
				logs: details.text
			}, $scope.request.nid);
			$uibModalInstance.dismiss('cancel');
		} else {
			var message = "Packing was changed on sales tab. Total Packings: $" + $scope.calculateTotals($scope.add_extra_charges);
			var obj = {
				simpleText: message
			};
			details.text.push(obj);

			RequestServices.updateRequest($scope.request.nid, {
				request_data: {
					value: request_data
				}
			}).then(() => {
				if ($scope.request.status.raw == 3) {
					if (_.isUndefined($scope.invoice.request_data)) {
						$scope.invoice.request_data = {};
					}
					$scope.invoice.request_data.value = angular.copy(request_data);
					$scope.request.request_all_data.invoice = angular.copy($scope.invoice);

					let fullPacking = _.findIndex($scope.invoice.request_data.value.packings, {originName: 'Estimate Full Packing'});
					let partialPacking = _.findIndex($scope.invoice.request_data.value.packings, {originName: 'Estimate Partial Packing'});
					if (fullPacking >= 0) {
						$scope.invoice.request_data.value.packings.splice(fullPacking, 1);
					}
					if (partialPacking >= 0) {
						$scope.invoice.request_data.value.packings.splice(partialPacking, 1);
					}

					RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data).then(() => {
						$rootScope.$broadcast('grandTotal.closing', {
							grand: $scope.grandTotalInvoice,
							logs: []
						}, $scope.request.nid);
					});
				}

				$scope.request.request_data.value = request_data;
				$rootScope.$broadcast('request.packing_update', $scope.add_extra_charges);
				$rootScope.$broadcast('grandTotal.sales', {
					grand: $scope.grandTotal,
					logs: details.text
				}, $scope.request.nid);
				$uibModalInstance.dismiss('cancel');
			}, () => {
				SweetAlert.swal('Packing has not saved', 'Try again', 'error');
			}).finally(() => {
				$scope.busy = false;
			});
			RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
		}
	};
}
