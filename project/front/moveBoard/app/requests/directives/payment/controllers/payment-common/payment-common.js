export const AUTH_MODAL = {
	editForm: {
		tipsAmount: {
			text: "Tips' amount",
			edited: false
		},
		tipsPercent: {
			text: "Tips' percent",
			edited: false
		},
		amount: {
			text: "Amount",
			edited: false
		},
		cardNum: {
			text: "Card number",
			edited: false
		},
		expMonth: {
			text: "Exp month",
			edited: false
		},
		expYear: {
			text: "Exp year",
			edited: false
		},
		secureCode: {
			text: "CVС",
			edited: false
		},
		fullName: {
			text: "Name",
			edited: false
		},
		firstName: {
			text: "First name",
			edited: false
		},
		lastName: {
			text: "Last name",
			edited: false
		},
		zipCode: {
			text: "Zip code",
			edited: false
		},
		flag: {
			text: "Flag of payment",
			edited: false
		},
		checkNum: {
			text: "Check number",
			edited: false
		},
		phoneNumber: {
			text: "Phone number",
			edited: false
		},
		userEmail: {
			text: "Email",
			edited: false
		}
	},
	statuses: {
		'cashPay': false,
		'creditPay': false,
		'checkPay': false,
		'customRefundPay': false
	},
	cardErrorMessages: {
		card_number: 'The card number entered is invalid.',
		cvc: 'Card type verification error. Please check CVV on your card.',
		exp_month: 'The expiration month entered for the credit card is invalid.',
		exp_year: 'The expiration year entered for the credit card is invalid.'
	}
};

export const PACKING_MODAL = {
	packingSettings: {
		packingAccount: false,
		packing: {
			LM: {
				full: 0.5,
				partial: 50
			},
			LD: {
				full: 0.7,
				partial: 44
			}
		}
	},
	packingService: {
		LM: {
			full: 0.5,
			partial: 50
		},
		LD: {
			full: 0.7,
			partial: 44
		}
	},
}
