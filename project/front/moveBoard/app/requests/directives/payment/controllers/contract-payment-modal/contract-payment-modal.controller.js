'use strict';

angular
	.module('move.requests')
	.controller('ContractPaymentModalCtrl', ContractPaymentModalCtrl);

ContractPaymentModalCtrl.$inject = ['$scope', '$uibModalInstance', 'request', 'datacontext', 'SweetAlert', 'RequestServices', 'PaymentServices', '$uibModal'];

function ContractPaymentModalCtrl ($scope, $uibModalInstance, request, datacontext, SweetAlert, RequestServices, PaymentServices, $uibModal) {
	$scope.request = request;
	$scope.invoices = [];
	var receipts = angular.fromJson(request.receipts);
	receipts = receipts.filter(receipt => receipt.payment_flag == "contract");
	
	$scope.receipts = receipts;
	$scope.activeRow = -1;
	$scope.fieldData = datacontext.getFieldData();
	$scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);
	$scope.contract_page = angular.fromJson($scope.fieldData.contract_page);
	
	$scope.showReceiptModal = function (receipt, index) {
		if ($scope.activeRow == index) {
			var modalInstance = $uibModal.open({
				template: require('../../receiptContractModal.html?4'),
				controller: 'ReceiptPaymentModalCtrl',
				size: 'lg',
				resolve: {
					receipt: function () {
						return receipt;
					}
				}
			});
			modalInstance.result.then(function ($scope) {
			});
		} else {
			$scope.activeRow = index;
		}
	};
	
	$scope.prepareToDelete = function (index) {
		$scope.activeRow = index;
	};
	
	$scope.removeReceipt = function () {
		if ($scope.receipts[$scope.activeRow].transaction_id != 'Custom Payment') {
			SweetAlert.swal("Error!", "You can't remove payment!\n In this case, please use button 'Refund'", 'error');
			return false;
		}
		
		SweetAlert.swal({
				title: "Are you sure you want to delete receipt ?",
				text: "It will be deleted permanently",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55", confirmButtonText: "Delete!",
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			}, isConfirm => {
				if (isConfirm) {
					var id = $scope.receipts[$scope.activeRow].id;
					PaymentServices.delete_receipt(id).then(function () {
						var message = "Payment in the amount  $" + $scope.receipts[$scope.activeRow].amount + ' was removed.';
						var obj = {
							simpleText: message
						};
						var arr = [obj];
						RequestServices.sendLogs(arr, 'Payment removed', $scope.request.nid, 'MOVEREQUEST');
						$scope.receipts.splice($scope.activeRow, 1);
					}, function (reason) {
						SweetAlert.swal("Failed!", reason, "error");
					});
					
					$rootScope.$broadcast('request.payment_receipts', $scope.receipts, $scope.request.nid);
				}
			});
	};
	
	$scope.calculatePayments = function () {
		return PaymentServices.calcPayment($scope.receipts);
	};
	
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.dateFormat = function (date) {
		return _.isNaN(moment.unix(date)._i) ? moment(date).format('DD MMM, YYYY') : moment.unix(date).format('DD MMM, YYYY h:mm a');
	};
}

