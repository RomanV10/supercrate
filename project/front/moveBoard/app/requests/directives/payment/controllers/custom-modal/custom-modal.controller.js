'use strict';

angular
	.module('move.requests')
	.controller('CustomModalCtrl', CustomModalCtrl);

CustomModalCtrl.$inject  = ['$scope', '$uibModalInstance', 'request', 'common', 'entitytype', '$rootScope', 'PaymentServices', 'RequestServices'];

function CustomModalCtrl ($scope, $uibModalInstance, request, common, entitytype, $rootScope, PaymentServices, RequestServices) {
	$scope.request = angular.copy(request);
	var MOVEREQUEST = 0;
	var STORAGEREQUEST = 1;
	$scope.entitytype = entitytype || 0;
	$scope.entityType = 'MOVEREQUEST';
	$scope.range = common.range;
	$scope.currYear = (new Date()).getFullYear();

	$scope.receipt = [];
	$scope.payment_flag = 'regular';

	if (request.payment_flag == 'contract') {
		$scope.payment_flag = 'contract';
	}

	$scope.cardsTypes = {
		visa: 'Visa',
		master: 'Master Card',
		americanexpress: 'American Express',
		discover: 'Discover'
	};

	$scope.receipt = {
		account_number: "",
		amount: '',
		auth_code: "",
		card_type: "",
		date: moment().format('MM/DD/YYYY'),
		created: moment().unix(),
		description: "",
		email: request.email,
		name: '',
		invoice_id: "custom",
		payment_method: "",
		phone: '',
		transaction_id: "Custom Payment",
		pending: false,
		type: "cash",
		checkN: '',
		payment_flag: $scope.payment_flag

	};

	if (entitytype == STORAGEREQUEST) {
		$scope.entityType = 'STORAGEREQUEST';
		$scope.receipt.name = request.user_info.name;
		$scope.receipt.phone = request.user_info.phone1;
	} else if (entitytype == MOVEREQUEST) {
		$scope.receipt.name = request.first_name + ' ' + request.last_name;
		$scope.receipt.phone = request.phone;
	}


	$scope.Save = function () {
		var receiptData = {};

		if ($scope.receipt.amount <= 0) {
			toastr.error('Error', 'Amount must be greater than zero.');
			return false;
		}

		let userName = _.get(request, 'user_info.name', request.name);
		let userEmail = _.get(request, 'user_info.email', request.email);
		let userPhone = _.get(request, 'user_info.phone1', request.phone);

		if ($scope.receipt.type == 'cash') {
			receiptData = {
				account_number: "",
				amount: _.round($scope.receipt.amount, 2),
				auth_code: $scope.receipt.auth_code,
				date: moment($scope.receipt.date).format('MM/DD/YYYY'),
				created: moment().unix(),
				description: $scope.receipt.description,
				email: userEmail,
				name: userName,
				invoice_id: "custom",
				payment_method: $scope.receipt.type,
				phone: userPhone,
				transaction_id: "Custom Payment",
				pending: $scope.receipt.pending,
				type: $scope.receipt.type,
				payment_flag: $scope.payment_flag
			};
		}

		if ($scope.receipt.type == 'customrefund') {
			receiptData = {
				account_number: "",
				amount: _.round(-Math.abs($scope.receipt.amount), 2),
				auth_code: $scope.receipt.auth_code,
				date: moment().format('MM/DD/YYYY'),
				created: moment().unix(),
				description: $scope.receipt.description,
				email: userEmail,
				name: userName,
				invoice_id: "custom",
				payment_method: $scope.receipt.type,
				phone: userPhone,
				transaction_id: "Custom Payment",
				pending: $scope.receipt.pending,
				type: $scope.receipt.type,
				payment_flag: $scope.payment_flag
			};
		}

		if($scope.receipt.type == 'customrefund' && parseFloat($scope.receipt.amount) >  PaymentServices.calcPayment($scope.request.receipts)){
			toastr.error('Error', `Custom refund can't be heigher then total.`);
			return false;
		}

		if ($scope.receipt.type == 'check') {
			receiptData = {
				account_number: "",
				amount: _.round($scope.receipt.amount, 2),
				auth_code: '',
				date: moment($scope.receipt.date).format('MM/DD/YYYY'),
				created: moment().unix(),
				description: $scope.receipt.description,
				email: userEmail,
				name: userName,
				invoice_id: "custom",
				payment_method: $scope.receipt.type,
				phone: userPhone,
				transaction_id: "Custom Payment",
				pending: $scope.receipt.pending,
				type: $scope.receipt.type,
				payment_flag: $scope.payment_flag,
				checkN: $scope.receipt.checkN
			}
		}

		if ($scope.receipt.type == 'creditcard' || $scope.receipt.type == 'Reservation') {
			receiptData = {
				account_number: "",
				amount: _.round($scope.receipt.amount, 2),
				auth_code: $scope.receipt.auth_code,
				date: moment($scope.receipt.date).format('MM/DD/YYYY'),
				created: moment().unix(),
				description: $scope.receipt.description,
				email: userEmail,
				name: userName,
				invoice_id: "custom",
				payment_method: $scope.receipt.type,
				phone: userPhone,
				transaction_id: "Custom Payment",
				pending: $scope.receipt.pending,
				type: $scope.receipt.type,
				payment_flag: $scope.receipt.type == 'creditcard' ? $scope.payment_flag : "Reservation",
				cctype: $scope.receipt.cctype,
				ccardN: $scope.receipt.ccardN,
				cvv: $scope.receipt.cvv,
				expmonth: $scope.receipt.expmonth,
				expyear: $scope.receipt.expyear
			}
		}
		if (entitytype == STORAGEREQUEST) {
			receiptData.name = userName;
			receiptData.phone = userPhone;
		} else if (entitytype == MOVEREQUEST) {
			receiptData.name = request.first_name + ' ' + request.last_name;
			receiptData.phone = request.phone;
		}

		$scope.receipt = receiptData;

		if (angular.isDefined($scope.receipt.ccardN)) {
			var telNb = $scope.receipt.ccardN;
			$scope.receipt.ccardN = telNb.substring(telNb.length - 4, telNb.length);
		}

		var receipts = $scope.request.receipts ? $scope.request.receipts : [];

		if (!receipts.length) {

		}

		if (request.payment_flag == 'contract') {
			receipts.push($scope.receipt);
		}

		PaymentServices.set_receipt($scope.request.nid, receiptData, $scope.entitytype)
			.then(function (data) {
				var message = "Custom payment in the amount  $" + receiptData.amount + ' (type: ' + $scope.receipt.type + ')' + ' was added.';
				var obj = {
					simpleText: message
				};
				var arr = [obj];
				$rootScope.$broadcast('receipt_sent');
				RequestServices.sendLogs(arr, 'Custom payment', $scope.request.nid, $scope.entityType);
				$scope.receipt.id = _.head(data);

				if ($scope.receipt.type == 'Reservation') {
					$scope.request.request_all_data.reservation_receipt = $scope.receipt.id;
					RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
					let editrequest = {field_reservation_price: "0"};
					$scope.request.reservation_rate.value = "0";
					RequestServices.updateRequest($scope.request.nid, editrequest);
				} else {
					$rootScope.$broadcast('custom.receipt.received', $scope.receipt);
				}

				$rootScope.$broadcast('initGetReceipts');
			});

		toastr.success('success', 'Save Receipt');
		$uibModalInstance.dismiss('cancel');
	};

	$scope.Cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

}
