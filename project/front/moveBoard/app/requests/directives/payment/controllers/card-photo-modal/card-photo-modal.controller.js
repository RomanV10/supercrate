'use strict';

angular
	.module('move.requests')
	.controller('CardPhotoModalCtrl', CardPhotoModalCtrl);

CardPhotoModalCtrl.$inject = ['$scope', '$uibModalInstance', 'img'];

function CardPhotoModalCtrl ($scope, $uibModalInstance, img) {
	$scope.img = img;
	
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	}
}
