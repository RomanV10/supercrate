'use strict';

angular
	.module('move.requests')
	.controller('AuthRedirectModalCtrl', AuthRedirectModalCtrl);

AuthRedirectModalCtrl.$inject = ['$scope', '$uibModalInstance', 'request'];

function AuthRedirectModalCtrl($scope, $uibModalInstance, request) {
	$scope.request = request;
	$scope.chargeAmount = $scope.request.reservation_rate.value;
	
	if ($scope.request.reservation) {
		$scope.chargeAmount = $scope.request.reservation_rate.value;
	}
	
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}
