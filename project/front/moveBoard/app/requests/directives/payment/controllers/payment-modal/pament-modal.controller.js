'use strict';

angular
	.module('move.requests')
	.controller('PaymentModalCtrl', PaymentModalCtrl);

PaymentModalCtrl.$inject = ['$scope', '$uibModalInstance', 'request', 'entity', 'datacontext', '$rootScope', 'apiService', 'moveBoardApi', 'InvoiceServices', 'SweetAlert', 'PaymentServices', 'common', 'RequestServices', 'EditRequestServices'];

function PaymentModalCtrl ($scope, $uibModalInstance, request, entity, datacontext, $rootScope, apiService, moveBoardApi, InvoiceServices, SweetAlert, PaymentServices, common, RequestServices, EditRequestServices) {
	$scope.request = request;
	$scope.receipts = [];
	$scope.activeRow = -1;
	$scope.activeReceiptId = '';
	$scope.busy = true;
	$scope.receiptLoader = false;
	$scope.entity = entity;

	const ENTITY_TYPE = 0;
	const CREDIT_CARD = 'creditcard';
	const RESERVATION_BY_CLIENT = 'Reservation by client';

	var importedFlags = $rootScope.availableFlags;
	var Job_Completed = 'Completed';
	$scope.Job_Completed = $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[Job_Completed]);

	function sortRefundReceipts () {
		var refundsArr = [];
		_.forEachRight($scope.receipts, function (receipt, ind) {
			if (receipt.refunds) {
				refundsArr.push(receipt);
				$scope.receipts.splice(ind, 1);
			}
		});
		_.forEach(refundsArr, function (refund) {
			var ind = _.findIndex($scope.receipts, {id: refund.receipt_id.toString()});
			$scope.receipts.splice(ind + 1, 0, refund);
		});
		$scope.busy = false;
		$scope.receiptLoader = false;
	}

	sortRefundReceipts();

	var searchPayment = false;
	$scope.fieldData = datacontext.getFieldData();
	$scope.basicSettings = angular.fromJson($scope.fieldData.basicsettings);
	$scope.entitytype = $rootScope.fieldData.enums.entities.MOVEREQUEST;

	$scope.customType = function (receipt) {
		switch (receipt.type) {
		case 'cash':
			return '(cash)';
			break;
		case 'check':
			return '(check)';
			break;
		case 'creditcard':
			return '(card)';
			break;
		case 'customrefund':
			return '(custom refund)';
			break;
		default:
			return 'Payment';
		}
	};

	$scope.createInvoice = function () {
		InvoiceServices.openInvoiceDialog(request, $scope.entitytype, false);
	};

	$scope.editInvoice = function () {
		InvoiceServices.openInvoiceDialog(request, $scope.entitytype, true);
	};

	$scope.dateFormat = function (date) {
		return _.isNaN(moment.unix(date)._i) ? moment(date).format('DD MMM, YYYY') : moment.unix(date).format('DD MMM, YYYY h:mm a');
	};

	$scope.prepareToDelete = function (index, id) {
		$scope.activeRow = index;
		$scope.activeReceiptId = id;
	};

	$scope.removeReceipt = function () {
		let ind = _.findIndex($scope.receipts, {id: $scope.activeReceiptId});
		if (~ind && ($scope.receipts[ind].payment_method == CREDIT_CARD || $scope.receipts[ind].payment_flag == RESERVATION_BY_CLIENT)) {
			SweetAlert.swal('Error!', 'You can\'t remove payment!\n In this case, please use button \'Refund\'', 'error');
			return false;
		}

		SweetAlert.swal({
			title: 'Are you sure you want to delete receipt ?',
			text: 'It will be deleted permanently',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55', confirmButtonText: 'Delete!',
			cancelButtonText: 'No',
			closeOnConfirm: true,
			closeOnCancel: true
		},
		function (isConfirm) {
			if (isConfirm) {

				var id = $scope.receipts[ind].id;
				let isTrip = $scope.receipts[ind].payment_flag.includes('Trip') || $scope.receipts[ind].payment_flag.includes('T/P');

				PaymentServices.delete_receipt(id, isTrip).then(function () {
					var message = 'Payment in the amount  $' + $scope.receipts[ind].amount + ' was removed.';
					var obj = {
						simpleText: message
					};
					var arr = [obj];
					RequestServices.sendLogs(arr, 'Payment removed', $scope.request.nid, 'MOVEREQUEST');
					$scope.receipts.splice(ind, 1);
					$scope.activeRow = -1;
					$scope.activeReceiptId = '';
					activate();
				}, function (reason) {
					SweetAlert.swal('Failed!', reason, 'error');
				});
				$rootScope.$broadcast('request.payment_receipts', $scope.receipts, $scope.request.nid);
			}
		});
	};

	$scope.showReceipt = function (receipt_id) {
		var idx = _.findIndex($scope.receipts, {id: receipt_id});
		PaymentServices.openReceiptModal(request, $scope.receipts[idx], undefined, $scope.entity);
	};

	$scope.addAuthOldPayment = function () {
		request.reservation = false;
		PaymentServices.openAuthRedirectModal(request);
	};

	$scope.addAuthPayment = function () {
		request.reservation = false;
		PaymentServices.openAuthPaymentModal(request, '', '', $scope.entitytype);
	};

	$scope.addReservationPayment = function () {
		request.reservation = true;
		PaymentServices.openAuthPaymentModal(request, '', '', $scope.entitytype);
	};

	$scope.addCustomPayment = function () {
		PaymentServices.openCustomPaymentModal(request, $scope.entitytype);
	};

	$scope.addPayment = function () {
		PaymentServices.openAuthPaymentModal(request, '', '', $scope.entitytype);
	};

	$scope.changePending = function (receipt) {
		let isTrip = receipt.payment_flag.includes('Trip') || receipt.payment_flag.includes('T/P');
		receipt.entity_id = $scope.request.nid;
		receipt.entity_type = $scope.entitytype;

		PaymentServices.update_receipt($scope.request.nid, receipt, isTrip).then(function () {
			var message = 'Payment in the amount  $' + receipt.amount + '(was changed pending option).';
			var obj = {simpleText: message};
			var arr = [obj];
			RequestServices.sendLogs(arr, 'Payment pending option', $scope.request.nid, 'MOVEREQUEST');
		});

		$rootScope.$broadcast('request.payment_receipts', $scope.receipts, $scope.request.nid);
	};

	$scope.calculatePayments = function () {
		return PaymentServices.calcPayment($scope.receipts);
	};

	$scope.openRefund = function () {
		var receiptData = '';
		var idx = _.findIndex($scope.receipts, {id: $scope.activeReceiptId});
		if ($scope.activeRow != -1 && idx > -1) {
			receiptData = $scope.receipts[idx];
		}

		let uid = _.get($rootScope, 'currentUser.userId.uid');

		PaymentServices.openRefundModal($scope.request, uid, $scope.entitytype, receiptData);
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
		searchPayment = true;
	};

	$scope.save = function () {
		$scope.cancel();
	};

	$uibModalInstance.result.catch(() => {
		searchPayment = true;
	});
	let receiptLength = 0;

	async function preActivate() {
		let receipts = await PaymentServices.get_receipt(request.nid, ENTITY_TYPE);
		receiptLength = receipts.length;
		activate();
	}

	preActivate();

	function activate () {
		PaymentServices.get_receipt(request.nid, ENTITY_TYPE)
			.then(resolve => {
				$scope.receipts = resolve;
				if (receiptLength !== resolve.length) {
					receiptLength = resolve.length;
					EditRequestServices.getQuickBookFlag($scope.request);

					searchPayment = false;
					sortRefundReceipts();

					$rootScope.$broadcast('request.payment_receipts', $scope.receipts, request.nid);
				}
			});
	}

	$scope.$on('receipt_sent', function () {
		$scope.receiptLoader = true;
	});

	$scope.$on('initGetReceipts', activate);
}
