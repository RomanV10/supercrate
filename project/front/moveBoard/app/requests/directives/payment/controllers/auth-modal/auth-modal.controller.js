'use strict';

import {
	AUTH_MODAL
} from '../payment-common/payment-common';

angular
	.module('move.requests')
	.controller('AuthModalCtrl', AuthModalCtrl);

/*@ngInject*/
function AuthModalCtrl ($scope, $location, $uibModalInstance, request, serverError,
						step, data_receipt, entity_type, datacontext,
						AuthenticationService, $rootScope, $window, SendEmailsService, apiService,
						moveBoardApi, Session, PaymentServices, RequestServices, EditRequestServices,
						SweetAlert, geoCodingService, $q, page_id, contractDispatchLogs) {
	let session = Session.get();
	let userRole = _.get(session, 'userRole', []);
	const isAdmin = userRole.indexOf('administrator') > -1;
	const reader = new FileReader();
	let errorMessages = angular.copy(AUTH_MODAL.cardErrorMessages);
	$scope.uploadFile = uploadFile;
	let entityType = 'MOVEREQUEST';
	$scope.payment = {};
	$scope.payment.canvasInit = canvasInit;
	$scope.payment.setPaymentBlockHeight = setPaymentBlockHeight;
	$scope.step = step;
	$scope.data_receipt = data_receipt;
	$scope.contractPageId = page_id;
	$scope.receipt = {};
	$scope.fieldData = datacontext.getFieldData();
	$scope.settings = angular.fromJson($scope.fieldData.basicsettings);
	$scope.contract_page = angular.fromJson($scope.fieldData.contract_page);
	$scope.enumsNotification = $scope.fieldData.enums.notification_types;
	$scope.enums = $scope.fieldData.enums.entities;
	$scope.serverError = serverError;
	$scope.cardMin = 17;
	$scope.cardMax = 19;
	$scope.paymentCheck = {
		check_num: ''
	};
	$scope.cvv = {};
	$scope.cvv.Amex = false;
	$scope.secure = {
		cvc: '',
		cvcAmex: ''
	};
	$scope.request = _.clone(request);
	$scope.busy = false;
	$scope.editrequest = {};
	$scope.payment.payment_flag = 'regular';
	if (request.payment_flag == 'contract') {
		$scope.payment.payment_flag = 'contract';
	}
	$scope.card_photo = {
		front: {
			base64: null
		},
		back: {
			base64: null
		}
	};
	$scope.outputImage = {};
	$scope.outputImage.front = null;
	$scope.outputImage.back = null;
	$scope.charge_value = {
		value: 0
	};
	$scope.editForm = angular.copy(AUTH_MODAL.editForm);

	$scope.checkout_title = "Payment";
	$scope.take_address = "Destination";
	if (request.field_moving_from) {
		$scope.payment.billing_city = request.field_moving_from.locality;
		$scope.payment.billing_state = request.field_moving_from.administrative_area;
		$scope.payment.billing_zip = request.field_moving_from.postal_code;
		$scope.payment.billing_address = request.field_moving_from.thoroughfare;
	}

	$scope.clientPay = ($scope.request.clientPay);
	//TYPE
	$scope.statuses = angular.copy(AUTH_MODAL.statuses);

	$scope.reciept_to_moveRequest = {};

	let currentUserRole = _.get($rootScope, 'currentUser.userRole', []);
	const IS_HOME_ESTIMATOR = currentUserRole.indexOf('home-estimator') > -1;

	let creditChargeTax = 0;
	let cashDiscount= 0;

	$scope.goStepTwo = goStepTwo;
	$scope.setStep = setStep;
	$scope.logClickButtons = logClickButtons;
	$scope.changeChargeValue = changeChargeValue;
	$scope.setCustomValue = setCustomValue;
	$scope.editField = editField;

	$scope.$watch('payment.billing_zip', function (newValue, oldValue) {
		if (request.field_moving_from) {
			let postalCodeTo = !_.isUndefined(request.field_moving_to) ? request.field_moving_to.postal_code : null;
			if (newValue != oldValue && newValue != postalCodeTo) {
				var postal_code = newValue;
				if (postal_code.length == 5) {
					geoCodingService.geoCode(postal_code)
						.then(function (result) {
							$scope.payment.billing_city = result.city;
							$scope.payment.billing_state = result.state;
						}, function () {
							SweetAlert.swal("You entered the wrong zip code.", '', 'error');
							$scope.payment.billing_city = '';
							$scope.payment.billing_state = '';
						});
				}
			}
		} else {
			if (newValue != oldValue) {
				var postal_code = newValue;
				if (postal_code.length == 5) {
					geoCodingService.geoCode(postal_code)
						.then(function (result) {
							$scope.payment.billing_city = result.city;
							$scope.payment.billing_state = result.state;
						}, function () {
							SweetAlert.swal("You entered the wrong zip code.", '', 'error');
							$scope.payment.billing_city = '';
							$scope.payment.billing_state = '';
						});
				}
			}
		}
	});

	function setPaymentBlockHeight (type) {
		var addVal = 150;
		let selectedCustomPaymentOptions = _.find($scope.contract_page.paymentOptions, {
			alias: 'custom',
			selected: true
		});

		if ($scope.payment.payment_flag == 'contract' && selectedCustomPaymentOptions) {
			setTimeout(function () {
				var paymentHeight = $('.pay_options').height();
				if (angular.element('.step.credit_form ').height() < paymentHeight) {
					angular.element(type).height(paymentHeight - addVal);
				}
				var height = angular.element('.step.credit_form ').height();
				angular.element('.step.credit_form ').height(paymentHeight > height ? paymentHeight : height);
			}, 100);
		}
	}

	function editField(fieldName){
		$scope.editForm[fieldName].edited = true;
	}

	function setEditedFields(){
		let editedFields = _.filter($scope.editForm, {edited: true});
		let logsData = [];
		if(!_.isEmpty(editedFields)){
			_.forEach(editedFields, function(val){
				if (_.isUndefined(val.text)) return;

				let msg = {
					simpleText:  val.text + ' has changed.'
				};
				logsData.push(msg);
			})
		}
		return logsData;
	}

	function setCustomValue() {
		let totalCharge = basicTotal;
		if($scope.changePaymentVal) {
			totalCharge = angular.copy($scope.changePaymentVal);
		}
		$scope.charge_value.value = totalCharge
	}

	function changeChargeValue(val) {
		$scope.changePaymentVal = val;
	}

	function logClickButtons(txt){
		if($scope.payment.payment_flag == 'Reservation' || $scope.payment.payment_flag == 'Reservation by client')
			saveToLogMsg( txt, 'User activity', $scope.request.nid);
	}

	function setStep(stepNumNew, stepNumOld){
		let stepNameNew = 'step' + stepNumNew;
		let stepNameOld = 'step' + stepNumOld;
		$scope[stepNameNew] = true;
		$scope[stepNameOld] = false;
	}

	$scope.choosePayment = function (type) {
		if(!_.isNumber($scope.selectedTips)) $scope.selectedTips = 0;
		_.forEach($scope.statuses, function (item, key) {
			$scope.statuses[key] = false;
		});
		$scope.statuses[type] = true;

		if (basicTotal == 0) {
			basicTotal = $scope.charge_value.value;
		}

		$scope.charge_value.value = basicTotal;
		let totalCharge = $scope.charge_value.value;

		let isContractPaymentOrCreditFee = $scope.payment.payment_flag == 'contract' || $scope.payment.creditCardFee;
		let requestTips = $scope.request.requestTipsPaid === false ? $scope.request.requestTips : 0;
		if($scope.changePaymentVal)
			totalCharge = angular.copy($scope.changePaymentVal);
		if (type == 'creditPay') {
			if ($scope.statuses.creditPay && isContractPaymentOrCreditFee && !$scope.request.agreement) {
				$scope.charge_value.value = _.round(parseFloat(totalCharge + requestTips) * (creditChargeTax / 100 + 1), 2);
			}else{
				$scope.charge_value.value = parseFloat(totalCharge);
			}
		} else if(type == 'cashPay') {
			if($scope.statuses.cashPay && $scope.payment.payment_flag == 'contract' && !$scope.request.agreement) {
				totalCharge = $scope.request.total_amount;
				if($scope.changePaymentVal)
					totalCharge = $scope.changePaymentVal - $scope.selectedTips;
				$scope.charge_value.value = _.round(parseFloat(totalCharge) * (1 - cashDiscount / 100), 2) + $scope.selectedTips + requestTips;
			}
		} else if(type == 'checkPay') {
			$scope.charge_value.value = totalCharge + requestTips;
		} else {
			if (_.get($scope, `contract_page.paymentTax[${type}].value`, 0) !== 0 && isContractPaymentOrCreditFee && !$scope.request.agreement) {
				$scope.charge_value.value = _.round(parseFloat(totalCharge + requestTips) * ($scope.contract_page.paymentTax[type].value / 100 + 1), 2);
			} else {
				$scope.charge_value.value = totalCharge + requestTips;
			}
		}
	};

	if ($scope.payment.payment_flag == 'contract') {
		$scope.settings = angular.fromJson((datacontext.getFieldData()).basicsettings);
		var pushTips = false;
		if (angular.isDefined($scope.contract_page.pushTips)) {
			pushTips = $scope.contract_page.pushTips;
		}
	} else {
		$scope.settings = angular.fromJson((datacontext.getFieldData()).basicsettings);
		if (!$scope.contract_page.paymentOptions) {
			$scope.contract_page.paymentOptions = [];
			var obj = {
				selected: true
			};
			for (var i = 0; i < 3; i++) {
				$scope.contract_page.paymentOptions.push(obj);
			}
		}
	}

	_.forEach($scope.contract_page.paymentOptions, function (item, ind) {
		if (item.alias == 'custom') {
			$scope.contract_page.paymentOptions[ind].type = (item.name.toLowerCase()).replace(/\s/g, '_');
			$scope.statuses[(item.name.toLowerCase()).replace(/\s/g, '_')] = false;
		}
	});

	var choosenPaymentType = _.findIndex($scope.contract_page.paymentOptions, {
		alias: 'creditPay',
		selected: true
	});

	if (choosenPaymentType == -1) {
		choosenPaymentType = _.findIndex($scope.contract_page.paymentOptions, {
			alias: 'creditPayment',
			selected: true
		});
	}

	if (choosenPaymentType > -1) {
		$scope.statuses[($scope.contract_page.paymentOptions[choosenPaymentType].alias).replace('Payment', 'Pay')] = true;
	} else {
		choosenPaymentType = _.findIndex($scope.contract_page.paymentOptions, {selected: true});

		if ( choosenPaymentType > -1 && $scope.contract_page.paymentOptions[choosenPaymentType]) {
			if($scope.contract_page.paymentOptions[choosenPaymentType].alias == 'custom')
				$scope.statuses[$scope.contract_page.paymentOptions[choosenPaymentType].type] = true;
			else
				$scope.statuses[($scope.contract_page.paymentOptions[choosenPaymentType].alias).replace('Payment', 'Pay')] = true;
		} else {
			choosenPaymentType = _.findIndex($scope.contract_page.paymentOptions, {
				alias: 'creditPayment'
			});

			if($scope.contract_page.paymentOptions[choosenPaymentType].alias)
				$scope.statuses[($scope.contract_page.paymentOptions[choosenPaymentType].alias).replace('Payment', 'Pay')] = true;
			else $scope.statuses.creditPay = true;
		}
	}

	if($scope.contract_page.paymentTax) {
		if($scope.contract_page.paymentTax.creditCharge.state)
			creditChargeTax = $scope.contract_page.paymentTax.creditCharge.value;
		if($scope.contract_page.paymentTax.cashCharge.state)
			cashDiscount = $scope.contract_page.paymentTax.cashCharge.value;
	}

	if ($scope.request.reservation) {
		$scope.charge_value.value = $scope.request.reservation_rate.value;
		$scope.checkout_title = "Reservation";
		$scope.payment.payment_flag = 'Reservation';
		if ($scope.clientPay) {
			$scope.payment.payment_flag = 'Reservation by client';
		}
	}
	else {
		$scope.charge_value.value = 0;
	}

	if ($scope.request.request_invoice) {
		$scope.charge_value.value = $scope.request.total;
		$scope.checkout_title = "Invoice secure payment";
		$scope.request.name = $scope.request.client_name;
		$scope.payment.payment_flag = `Payment for Invoice by ${isAdmin ? 'admin'  : 'client'}`;
		$scope.clientPay = true;
	}

	if ($scope.request.storage_invoice) {
		$scope.checkout_title = "Invoice secure payment";
		$scope.request.name = $scope.request.client_name;
		$scope.payment.payment_flag = `Payment for Invoice by ${isAdmin ? 'admin'  : 'client'}`;
		$scope.clientPay = true;
	}

	if (angular.isDefined($scope.request.total_amount)) {
		$scope.charge_value.value = $scope.request.total_amount;
	}

	if (angular.isDefined($scope.request.total_for_tip)) {
		$scope.total_for_tip = $scope.request.total_for_tip;
	}


	$scope.step0 = true;
	$scope.tips = {
		perc: 0,
		amount: 0
	};
	$scope.step1 = false;
	$scope.step2 = false;
	$scope.step3 = false;
	$scope.step4 = false;
	$scope.step5 = false;
	$scope.payment.name = $scope.request.name;

	var basicTotal = angular.copy($scope.charge_value.value);

	$scope.charge_invalide = false;

	if (!pushTips && $scope.payment.payment_flag == 'contract') {
		$scope.step0 = false;
		$scope.step1 = true;
	}

	if ($scope.payment.payment_flag != 'contract') {
		$scope.step0 = false;
		$scope.step1 = true;
	}

	if ($scope.clientPay) {
		$scope.step1 = false;
		if($scope.payment.payment_flag != 'Reservation by client')
			$scope.step2 = true;

		if ($scope.request.receiptDescription) {
			$scope.payment.payment_flag = 'contract';
		}
	}

	if ($scope.serverError) {
		$scope.step1 = false;
		$scope.step2 = false;
		$scope.step0 = true;
	}

	var storageId = '';
	var requestId = '';

	if (angular.isDefined($scope.step) && $scope.step != '') {
		$scope.step0 = false;
		setTimeout(function () {
			$scope.step1 = false;
			$scope.step2 = false;
			$scope.step3 = false;
			$scope[$scope.step] = true;
			$scope.step0 = false;
			$scope.$apply();
			if($scope.request.request_all_data && $scope.request.request_all_data.agreementSave && $scope.request.request_all_data.agreementSave.storageId &&
				$scope.request.request_all_data.agreementSave.requestId){
				storageId = $scope.request.request_all_data.agreementSave.storageId;
				requestId = $scope.request.request_all_data.agreementSave.requestId;
			}
		}, 300);
	}

	if ($scope.request.payment_flag == 'Reservation' && $scope.request.openReservSign) {
		$scope.receipt = $scope.request.reservReceipt;
		$scope.step0 = false;
		$scope.step1 = false;
		$scope.step2 = false;
		$scope.step3 = false;
		$scope.step4 = false;
		$scope.step5 = true;
	}

	$scope.entity_type = entity_type || 0;

	if ($scope.entity_type == $scope.fieldData.enums.entities.STORAGEREQUEST) {
		entityType = 'STORAGEREQUEST';

		if ($scope.request.client_name) {
			$scope.payment.name = $scope.request.client_name;
			$scope.request.uid = {
				uid: $scope.request.uid
			};
			$scope.charge_value.value = _.round($scope.request.total, 2);
		} else if ($scope.request.user_info) {
			if($scope.request.total)
				$scope.charge_value.value = _.round($scope.request.total, 2);
			$scope.payment.name = $scope.request.user_info.name;
			$scope.payment.billing_zip = $scope.request.user_info.zip;
			$scope.request.phone = $scope.request.user_info.phone1;
			$scope.request.email = $scope.request.user_info.email;
			$scope.request.uid = {
				uid: $scope.request.user_info.uid
			};

			var postal_code = $scope.payment.billing_zip;

			if (postal_code.length == 5) {
				geoCodingService.geoCode(postal_code)
					.then(function (result) {
						$scope.payment.billing_city = result.city;
						$scope.payment.billing_state = result.state;
					}, function () {
						SweetAlert.swal("You entered the wrong zip code.", '', 'error');
						$scope.payment.billing_city = '';
						$scope.payment.billing_state = '';
					});
			}
		}
		if($scope.request.payForStorage){
			$scope.payment.payment_flag = 'storage';
			$scope.checkout_title = "Payment for storage";

		}
	}

	if ($scope.request.agreement) {
		$scope.charge_value.value = $scope.request.total || 0;
		$scope.payment.name = $scope.request.user_info.name;
		$scope.payment.billing_zip = $scope.request.user_info.zip;
		$scope.request.phone = $scope.request.user_info.phone1;
		$scope.request.email = $scope.request.user_info.email;
		$scope.request.uid = {
			uid: $scope.request.user_info.uid
		};
		var postal_code = $scope.payment.billing_zip;

		if (postal_code.length == 5) {
			geoCodingService.geoCode(postal_code)
				.then(function (result) {
					$scope.payment.billing_city = result.city;
					$scope.payment.billing_state = result.state;
				}, function () {
					SweetAlert.swal("You entered the wrong zip code.", '', 'error');
					$scope.payment.billing_city = '';
					$scope.payment.billing_state = '';
				});
		}
	}

	if ($scope.entity_type == $scope.fieldData.enums.entities.COUPON) {
		$scope.step0 = false;
		$scope.step1 = false;
		$scope.step2 = true;
		$scope.step3 = false;
		$scope.step4 = false;
		$scope.charge_value.value = $scope.request.total || 0;
		$scope.payment.name = $scope.request.name;
		$scope.payment.billing_zip = $scope.request.zip;
		$scope.request.uid = {
			uid: $scope.request.uid
		};
		$scope.payment.payment_flag = $scope.request.payment_flag;
		var obj = {
			selected: true
		};
		$scope.contract_page = {};
		$scope.contract_page.paymentOptions = [];

		for (var i = 0; i < 3; i++) {
			if (i == 0 || i == 2) {
				obj.selected = false;
			}
			$scope.contract_page.paymentOptions.push(obj);
		}
	}

	if (angular.isDefined($scope.data_receipt)) {
		if (_.isArray($scope.data_receipt)) {
			$scope.receipt = _.clone($scope.data_receipt[0]);
		} else {
			$scope.receipt = _.clone($scope.data_receipt.receipt);
		}
	}

	$uibModalInstance.close($scope);

	if($scope.payment.name.split(' ').length < 2) {
		$scope.payment.firstName = $scope.payment.name.split(' ')[0];
		$scope.payment.lastName = '';
		$scope.payment.name = '';
	}

	if($scope.request.accountInvoice) {
		$scope.payment.firstName = '';
		$scope.payment.lastName = '';
		$scope.payment.name = '';
		$scope.payment.billing_zip = '';
		$scope.payment.billing_city = '';
		$scope.payment.billing_state = '';
	}

	$scope.$watch('card_photo.front.base64', function (newValue) {
		if (angular.isUndefined(newValue) || newValue == null) {
			return false;
		}

		if ($scope.card_photo.front.base64) {
			if ($scope.card_photo.front.base64.compressed) {
				if ($scope.card_photo.front.base64.compressed.dataURL && $scope.card_photo.front.base64.compressed.dataURL != '') {
					$scope.busy = false;
				}
			}
		}
	});

	$scope.$watch('card_photo.back.base64', function (newValue) {
		if (angular.isUndefined(newValue)) {
			return false;
		}

		if ($scope.card_photo.back.base64) {
			if ($scope.card_photo.back.base64.compressed) {
				if ($scope.card_photo.back.base64.compressed.dataURL && $scope.card_photo.back.base64.compressed.dataURL != '') {
					$scope.busy = false;
				}
			}
		}
	});

	$scope.$watch('step2', function (newValue) {
		if (newValue) {
			if(!_.isNumber($scope.selectedTips)) $scope.selectedTips = 0;
			let totalCharge = basicTotal;
			let requestTips = $scope.request.requestTipsPaid === false ? $scope.request.requestTips : 0;
			if($scope.changePaymentVal)
				totalCharge = angular.copy($scope.changePaymentVal);
			if($scope.statuses.creditPay && $scope.payment.payment_flag == 'contract'  && !$scope.request.agreement){
				$scope.charge_value.value = _.round(parseFloat(totalCharge + requestTips) * (creditChargeTax / 100 + 1), 2);
			}else
			if($scope.statuses.cashPay && $scope.payment.payment_flag == 'contract'  && !$scope.request.agreement) {
				totalCharge = $scope.request.total_amount;

				if($scope.changePaymentVal)
					totalCharge = angular.copy($scope.changePaymentVal);

				$scope.charge_value.value = _.round(parseFloat(totalCharge) * (1 - cashDiscount/100), 2) + $scope.selectedTips + requestTips;
			}
		}
	});

	function canvasInit (id) {
		$scope.canvas = document.getElementById(id);
		$scope.signaturePad = new SignaturePad($scope.canvas);
	}

	function saveAgreementPayment () {
		if ($scope.request.agreement && !$scope.reciept_to_moveRequest.id) {
			if (localStorage['save_agreement']) {
				if (angular.fromJson(localStorage['save_agreement']).request.move_request != $scope.request.move_request) {
					var obj = {
						request: $scope.request,
						receipt: $scope.reciept_to_moveRequest
					};
					localStorage['save_agreement'] = angular.toJson(obj);
				}
			} else {
				var obj = {
					request: $scope.request,
					receipt: $scope.reciept_to_moveRequest
				};
				localStorage['save_agreement'] = angular.toJson(obj);
			}
		}
	}

	function uploadFile (type) {
		$scope.busy = true;
		$scope.selectedPhoto = type;
		$scope.$apply();
	}

	function saveReceiptToLocalStorage () {
		var data = {
			receipt: $scope.receipt,
			nid: $scope.request.nid,
			images: [],
			sign: []
		};

		if (!$scope.receipt.id) {
			data.tips = $scope.selectedTips;
		}

		if ($scope.request.request_all_data) {
			localStorage['save_receipt'] = angular.toJson(data);
			$scope.request.request_all_data.save_receipt = angular.toJson(data);
			RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
		}
	}

	$scope.uploadFrontFile = function (files) {
		var fd = new FormData();
		fd.append("file", files[0]);

		var file = files[0];
		reader.onload = function (evt) {
			$scope.$apply(function ($scope) {
				$scope.card_photo.front.base64 = evt.target.result;
			});
		};
		reader.readAsDataURL(file);
	};

	$scope.uploadBackFile = function (files) {
		var fd = new FormData();
		fd.append("file", files[0]);

		var file = files[0];
		reader.onload = function (evt) {
			$scope.$apply(function ($scope) {
				$scope.card_photo.back.base64 = evt.target.result;
			});
		};
		reader.readAsDataURL(file);
	};

	$scope.tipsSelected = function () {
		$scope.step1 = true;
		$scope.step0 = false;

		if ($scope.request.reservation) {
			$scope.charge_value.value = $scope.request.reservation_rate.value;
		} else {
			$scope.charge_value.value = '';
		}

		if (angular.isDefined($scope.request.total_amount)) {
			$scope.charge_value.value = $scope.request.total_amount;
		}

		var amount = _.clone($scope.charge_value.value);
		if ($scope.tips.amount == 0) {
			amount += _.round($scope.total_for_tip * $scope.tips.perc / 100, 2);
			$scope.selectedTips = _.round($scope.total_for_tip * $scope.tips.perc / 100, 2);
		}

		if ($scope.tips.perc == 0) {
			amount += _.round($scope.tips.amount, 2);
			$scope.selectedTips = $scope.tips.amount;
		}

		$scope.charge_value.value = _.round(amount, 2);
		basicTotal = angular.copy($scope.charge_value.value);
	};

	$scope.tipsPercChange = function (perc) {
		$scope.tips.perc = parseFloat(perc);
		$scope.tips.amount = 0;
	};

	$scope.tipsChange = function (type) {
		if (type == 'perc') {
			$scope.tips.amount = 0;
		}
		if (type == 'amount') {
			$scope.tips.perc = 0;
		}
	};

	$scope.allPhotos = function () {
		if ($scope.request.agreement || (storageId && requestId)) {
			if($scope.statuses.creditPay) {
				return !(_.get($scope.card_photo.front, 'base64.dataURL', '') != ''
					   || _.get($scope.card_photo.back, 'base64.dataURL', '') != '');
			} else {
				return !(_.get($scope.card_photo.back, 'base64.dataURL', '') != '');
			}
		}

		return !(_.get($scope.card_photo.front, 'base64.dataURL', '') != '');
	};

	$scope.switchAddress = function () {
		if (request.field_moving_to && request.field_moving_from) {
			if ($scope.take_address == "Destination") {
				$scope.take_address = "Origin";
				$scope.payment.billing_city = request.field_moving_to.locality;
				$scope.payment.billing_state = request.field_moving_to.administrative_area;
				$scope.payment.billing_zip = request.field_moving_to.postal_code;
				$scope.payment.billing_address = request.field_moving_to.thoroughfare;
			} else {
				$scope.take_address = "Destination";
				$scope.payment.billing_city = request.field_moving_from.locality;
				$scope.payment.billing_state = request.field_moving_from.administrative_area;
				$scope.payment.billing_zip = request.field_moving_from.postal_code;
				$scope.payment.billing_address = request.field_moving_from.thoroughfare;
			}
		}

		if ($scope.entity_type == 1) {
			if ($scope.take_address == "Destination") {
				$scope.take_address = "Origin";

				if ($scope.request.user_info.zip_from) {
					$scope.payment.billing_zip = $scope.request.user_info.zip_from;
				}
			} else {
				$scope.take_address = "Destination";
				$scope.payment.billing_zip = $scope.request.user_info.zip;
			}
		}
	};


	function goStepTwo () {
		$scope.charge_value.value = _.round($scope.charge_value.value, 2);
		if($scope.changePaymentVal)
			$scope.changePaymentVal = _.round($scope.changePaymentVal, 2);
		if (!$scope.serverError) {
			if ($scope.charge_value.value && $scope.charge_value.value >= ($scope.selectedTips || 0)) {
				$scope.step1 = false;
				$scope.step2 = true;
				$scope.charge_invalide = false;
				if ($scope.request.receiptDescription) {
					$scope.payment.payment_flag = 'contract';
				}
			} else if(!$scope.charge_value.value || $scope.charge_value.value < ($scope.selectedTips || 0)){
				$scope.charge_invalide = true;
				if($scope.charge_value.value < $scope.selectedTips) {
					$scope.charge_value.value = angular.copy($scope.selectedTips);
					$scope.changePaymentVal = angular.copy($scope.selectedTips);
				}
			}
		} else {
			$scope.step1 = false;
			$scope.step3 = true;
		}
	}

	function receiptLogMsg(receipt, error, success, text) {
		let data = {
			amount: receipt.amount ? ('Amount: $' + receipt.amount + '. <br>') : '',
			cardType: (receipt.card_type ? ('Card type: ' + receipt.card_type + '. <br>') : ''),
			cardNum: (receipt.credit_card ? ('Card Number: ****' + receipt.credit_card.card_number.substr(-4) + '. <br>') : ''),
			flag: receipt.payment_flag ? ('Payment flag: ' + receipt.payment_flag + '. <br>') : '',
			id: 'Receipt id: ' + receipt.id + '. <br>',
			text: angular.isDefined(text) ? (text + '. <br>') : '',
			error: angular.isDefined(error) ? ('Error: '+ error + '. ') : '',
			success: angular.isDefined(success) ? success + '. ' : ''
		};
		let title = data.error ? data.error : data.success;
		let msg = data.text + data.amount + data.cardType + data.cardNum + data.flag + data.id + data.error + data.success;
		let log = {
			title: title,
			msg: msg
		};
		return log;
	}

	$scope.clearSignature = function () {
		$scope.signaturePad.clear();
	};

	$scope.saveSignature = function () {
		const STORAGE_TYPE = 1;
		const RENTAL_AGREEMENT_TYPE = 0;
		var blank = $scope.isCanvasBlank('signatureCanvasPayment');

		if (blank) {
			SweetAlert.swal("Failed!", 'Sign please', "error")
		} else {
			if (!$scope.serverError) {
				$scope.busy = true;
				var signatureValue = getCropedSignatureValue();
				var SIGNATURE_PREFIX = 'data:image/gif;base64,';
				var signature = {
					value: SIGNATURE_PREFIX + signatureValue,
					date: moment().format('x')
				};

				var data = [];
				data.push(signature.value);
				if ($scope.request.request_all_data) {
					if (localStorage['save_receipt'] && $scope.payment.payment_flag == 'contract') {
						var im = angular.fromJson(localStorage['save_receipt']);
						im.sign = data;
						im.signature = true;
						localStorage['save_receipt'] = angular.toJson(im);
						if ($scope.request.request_all_data) {
							$scope.request.request_all_data.save_receipt = angular.toJson(im);
							RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
						}
					}
				}

				if(storageId && requestId){
					var promise1 = PaymentServices.saveCardImage(storageId, data);
					var promise2 = PaymentServices.saveCardImage(requestId, data);
					$q.all([promise1, promise2]).then(function () {
						$rootScope.$broadcast('save.agreement.signature');
						$scope.busy = false;
						var title = "Payment" + ($scope.request.payment_flag ? ("(" + $scope.request.payment_flag + ")") : '');
						saveToLogMsg('Signature was successfully saved.', title, $scope.request.request_all_data.storage_request_id, $scope.enums.STORAGEREQUEST);
						saveToLogMsg('Signature was successfully saved.', title, $scope.request.nid, $scope.enums.MOVEREQUEST);
						$scope.step3 = false;
						$scope.signatureSuccess = true;
						skipStepWithPhoto(STORAGE_TYPE);
					});
					return false;
				}

				PaymentServices.saveCardImage($scope.receipt.id, data).then(function () {
					if ($scope.entity_type == $scope.fieldData.enums.entities.COUPON) {
						$scope.busy = false;
						SweetAlert.swal({
							title: "Thank you for purchasing the coupon!",
							text: $scope.couponMsg,
							html: true,
							type: "success"
						}, function () {
							var websiteUrl = $window.location.origin;
							if ($scope.request.uid.uid) {
								$window.history.back();
							} else {
								$window.open(websiteUrl, '_self');
							}
						});
						$uibModalInstance.dismiss('cancel');
						return false;
					}

					if ($scope.request.agreement && $scope.reciept_to_moveRequest && $scope.reciept_to_moveRequest.id) {
						$rootScope.$broadcast('save.agreement.signature');
						if (localStorage['save_agreement']) {
							if (angular.fromJson(localStorage['save_agreement']).request.move_request != $scope.request.move_request) {
								PaymentServices.saveCardImage($scope.reciept_to_moveRequest.id, data);
							}
						} else {
							PaymentServices.saveCardImage($scope.reciept_to_moveRequest.id, data);
						}
					}

					if ($scope.request.request_all_data) {
						if ($scope.request.request_all_data.save_receipt) {
							var im = angular.fromJson($scope.request.request_all_data.save_receipt);
							im.sign = data;
							im.signature = true;

							if ($scope.request.request_all_data) {
								$scope.request.request_all_data.save_receipt = angular.toJson(im);
								RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
							}
						}
					}

					$scope.busy = false;
					var title = "Payment" + ($scope.request.payment_flag ? ("(" + $scope.request.payment_flag + ")") : '');
					saveToLogMsg('Signature was successfully saved.', title, $scope.request.nid);
					$scope.step3 = false;
					$scope.signatureSuccess = true;
					let isStorageRequest = _.get($scope.request, 'rentals.storage');
					let type = isStorageRequest ? STORAGE_TYPE : RENTAL_AGREEMENT_TYPE;
					skipStepWithPhoto(type);
				}, function (reason) {
					if (localStorage['save_receipt'] && $scope.payment.payment_flag == 'contract') {
						var im = angular.fromJson(localStorage['save_receipt']);
						im.sign = data;
						im.signature = true;
						localStorage['save_receipt'] = angular.toJson(im);
					} else {
						if ($scope.payment.payment_flag == 'contract') {
							saveReceiptToLocalStorage();
							var im = angular.fromJson(localStorage['save_receipt']);
							im.sign = data;
							im.signature = true;
							localStorage['save_receipt'] = angular.toJson(im);
						}
					}

					$scope.busy = false;
					$scope.step3 = false;
					$scope.step4 = true;
					let logMsg = receiptLogMsg( $scope.receipt, reason);
					saveToLogMsg( logMsg.msg, 'File saving error', $scope.request.nid);
					SweetAlert.swal("Failed!", reason, "error");
				});

				$rootScope.$broadcast('images_upload', data);
			} else {
				if (localStorage['save_receipt'] && $scope.payment.payment_flag == 'contract') {
					var signatureValue = getCropedSignatureValue();
					var SIGNATURE_PREFIX = 'data:image/gif;base64,';
					var signature = {
						value: SIGNATURE_PREFIX + signatureValue,
						date: moment().format('x')
					};
					var data = [];
					data.push(signature.value);
					var im = angular.fromJson(localStorage['save_receipt']);
					im.signature = true;
					im.sign = data;
					localStorage['save_receipt'] = angular.toJson(im);
				}

				$scope.step3 = false;
				$scope.step4 = true;
			}
		}
	};

	function skipStepWithPhoto(type) {
		let isNotToSkip = _.get($scope, `contract_page.skippingPhoto[${type}].value`, true);
		if (isNotToSkip) {
			$scope.step4 = true;
			return;
		}

		const SAVE_AGREEMENT = angular.fromJson(localStorage['save_agreement']);
		const SAVE_RECEIPT = localStorage['save_receipt'];
		let title = `Payment ${_.get($scope.request, 'payment_flag', '')}`;

		if (SAVE_RECEIPT && $scope.payment.payment_flag == 'contract') {
			delete localStorage['save_receipt'];
		}

		if (_.get($scope.request, 'request_all_data.save_receipt')) {
			delete $scope.request.request_all_data.save_receipt;
			RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
		}

		if ($scope.request.agreement && $scope.reciept_to_moveRequest) {
			$rootScope.$broadcast('remove.agreement.data');
			if (!$scope.reciept_to_moveRequest.id) $rootScope.$broadcast('save.agreement.payment');
			$rootScope.$broadcast('payment.rental', $scope.receipt);
		}

		saveToLogMsg('Signature was successfully saved.', title, $scope.request.nid);

		if ($scope.request.agreement_repeat
			&& SAVE_AGREEMENT
			&& _.get(SAVE_AGREEMENT, 'request.move_request') == $scope.request.move_request) {
			delete localStorage['save_agreement'];
		}

		if ($scope.request.agreement
			&& !$scope.reciept_to_moveRequest.id
			&& !$scope.request.agreement_repeat) {
			localStorage['save_agreement'] = angular.toJson({
				request: $scope.request,
				receipt: $scope.reciept_to_moveRequest
			});
			$rootScope.$broadcast('save.agreement.payment');
		}
		$scope.busy = false;
		$uibModalInstance.dismiss('cancel');
	}

	var getCropedSignatureValue = function () {
		var url;
		var originImgData;
		var xi, yi;
		var imageData, nCanvas;

		var ctx = $scope.signaturePad._ctx;
		var canvasWidth = $scope.canvas.width;
		var canvasHeight = $scope.canvas.height;
		var minX = canvasWidth;
		var minY = canvasHeight;
		var maxX = 0;
		var maxY = 0;
		var padding = 10;

		originImgData = ctx.getImageData(0, 0, canvasWidth, canvasHeight).data;

		for (var i = 0; i < originImgData.length; i += 4) {
			if (originImgData[i] + originImgData[i + 1] + originImgData[i + 2] + originImgData[i + 3] > 0) {

				yi = Math.floor(i / (4 * canvasWidth));
				xi = Math.floor((i - (yi * 4 * canvasWidth)) / 4);

				if (xi < minX) {
					minX = xi;
				}

				if (xi > maxX) {
					maxX = xi;
				}

				if (yi < minY) {
					minY = yi;
				}

				if (yi > maxY) {
					maxY = yi;
				}
			}
		}

		imageData = ctx.getImageData(minX, minY, maxX - minX, maxY - minY);

		nCanvas = document.createElement('canvas');
		nCanvas.setAttribute('id', 'nc');
		nCanvas.width = 2 * padding + maxX - minX;
		nCanvas.height = 2 * padding + maxY - minY;
		nCanvas.getContext('2d').putImageData(imageData, padding, padding);
		url = nCanvas.toDataURL().split(',')[1];
		$("canvas#nc").remove();

		return url;
	};

	function saveToLogMsg (message, title, nid, type) {
		var MOVEREQUEST = 0;
		var currUser = $rootScope.currentUser;

		if (angular.isUndefined(currUser)) {
			currUser = [];
			currUser.userRole = ['anonymous'];
			currUser.userId = [];
			currUser.userId.field_user_first_name = 'anonymous';
			currUser.userId.field_user_last_name = '';
		}

		var userRole = '';

		if (currUser.userRole.indexOf('administrator') >= 0) {
			userRole = 'administrator';
		} else if (currUser.userRole.indexOf('manager') >= 0) {
			userRole = 'manager';
		} else if (currUser.userRole.indexOf('sales') >= 0) {
			userRole = 'sales';
		} else if (currUser.userRole.indexOf('foreman') >= 0) {
			userRole = 'foreman';
		} else if (currUser.userRole.indexOf('authenticated user') >= 0) {
			userRole = 'client';
		}

		var msg = {
			entity_id: nid,
			entity_type: type || $scope.entity_type || MOVEREQUEST,
			data: [{
				details: [{
					title: title,
					text: [],
					activity: userRole,
					date: moment().unix(),
					info: {
						browser: AuthenticationService.get_browser(),
					}
				}],
				source: userRole
			}]
		};

		var obj = {
			text: message
		};
		msg.data[0].details[0].text.push(obj);
		RequestServices.postLogs(msg);
	}

	function errorCreditCardData(ccObj) {
		if (ccObj.card_number.$invalid) return errorMessages.card_number;
		else if ((ccObj.cvcAmex.$invalid && $scope.cvv.Amex) || (!$scope.cvv.Amex && ccObj.ccCvc.$invalid)) return errorMessages.cvc;
		else if (ccObj.exp_month.$invalid) return errorMessages.exp_month;
		else if (ccObj.exp_year.$invalid) return errorMessages.exp_year;
		else return 'Please check all fields';
	}

	function sendAlertMsgToLog(text) {
		var obj = {
			simpleText: text
		};
		var arr = setEditedFields();
		arr.push(obj);
		RequestServices.sendLogs(arr, 'Online payment validation', $scope.request.nid, entityType);

		_.forEach($scope.editForm, function(editFormObject){editFormObject.edited = false;});
	}

	function GetCardType (number) {
		// visa
		var re = new RegExp("^4");

		if (number.match(re) != null) {
			return "Visa";
		}

		// Mastercard
		re = new RegExp("^5[1-5]");

		if (number.match(re) != null) {
			return "Mastercard";
		}

		// AMEX
		re = new RegExp("^3[47]");

		if (number.match(re) != null) {
			return "AMEX";
		}

		// Discover
		re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");

		if (number.match(re) != null) {
			return "Discover";
		}

		// Visa Electron
		re = new RegExp("^(4026|417500|4508|4844|491(3|7))");

		if (number.match(re) != null) {
			return "Visa Electron";
		}

		return "";
	}


	$scope.applyPayment = function () {

		if ($scope.statuses.creditPay) {
			var inputCard = document.getElementById('card_number'),
				inputMonth = document.getElementById('exp_month'),
				inputYear = document.getElementById('exp_year'),
				inputCVV = document.getElementById('cvc');
			inputCard.addEventListener('invalid', function (event) {
				event.preventDefault();
			});
			inputMonth.addEventListener('invalid', function (event) {
				event.preventDefault();
			});
			inputYear.addEventListener('invalid', function (event) {
				event.preventDefault();
			});
			inputCVV.addEventListener('invalid', function (event) {
				event.preventDefault();
			});
			let fullName;
			if ($scope.payment.firstName && $scope.payment.lastName) {
				fullName = $scope.payment.firstName + ' ' + $scope.payment.lastName;
			} else {
				fullName = $scope.payment.name;
			}

			$scope.payment.name = fullName ? fullName : '';
			if($scope.payment.name && $scope.payment.name.length < 2) {
				let msgErr = "Please, add your last name";
				SweetAlert.swal("Failed!", msgErr, "error");
				sendAlertMsgToLog(msgErr);
				return false;
			}

			if (_.isEmpty($scope.payment.name)) {
				if (_.isEmpty($scope.payment.firstName) || _.isEmpty($scope.payment.lastName)) {
					let msgErr = "Please, add your name";
					SweetAlert.swal("Failed!", msgErr, "error");
					sendAlertMsgToLog(msgErr);
					return false;
				}else{
					let msgErr = "Please, add your name";
					SweetAlert.swal("Failed!", msgErr, "error");
					sendAlertMsgToLog(msgErr);
					return false;
				}
			}

			if (!$scope.payment.billing_zip || $scope.payment.billing_zip == '') {
				let msgErr = "Please, add postal code";
				SweetAlert.swal("Failed!", msgErr, "error");
				sendAlertMsgToLog(msgErr);
				return false;
			}

			if ($scope.payment.phoneNumber) {
				$scope.request.phone = $scope.payment.phoneNumber;
			}

			if ((!$scope.request.phone || $scope.request.phone == '') && !$scope.request.request_invoice) {
				let msgErr = "Please, add your phone number";
				SweetAlert.swal("Failed!", msgErr, "error");
				sendAlertMsgToLog(msgErr);
				return false;
			}

			if ($scope.payment.userEmail) {
				$scope.request.email = $scope.payment.userEmail;
			}

			if (!$scope.request.email || $scope.request.email == '') {
				let msgErr = "Please, add your email";
				SweetAlert.swal("Failed!", msgErr, "error");
				sendAlertMsgToLog(msgErr);

				return false;
			}

			if ($scope.cvv.Amex) {
				if ($scope.checkout_form.card_number.$invalid || $scope.checkout_form.cvcAmex.$invalid || $scope.checkout_form.exp_month.$invalid || $scope.checkout_form.exp_year.$invalid) {
					let errorMsg = errorCreditCardData($scope.checkout_form);
					SweetAlert.swal("Failed!", errorMsg, "error");
					sendAlertMsgToLog(errorMsg);
					return false;
				}
			} else {
				if ($scope.checkout_form.card_number.$invalid || $scope.checkout_form.ccCvc.$invalid || $scope.checkout_form.exp_month.$invalid || $scope.checkout_form.exp_year.$invalid) {
					let errorMsg = errorCreditCardData($scope.checkout_form);
					SweetAlert.swal("Failed!", errorMsg, "error");
					sendAlertMsgToLog(errorMsg);
					return false;
				}
			}
		}

		if ($scope.statuses.checkPay) {
			if ($scope.paymentCheck.check_num == null || $scope.paymentCheck.check_num == 'null' || $scope.paymentCheck.check_num == '') {
				SweetAlert.swal("Failed!", "Please fill all fields", "error");
				sendAlertMsgToLog('Check numder is empty');
				return false;
			}
		}

		$scope.busy = true;
		if ($scope.statuses.cashPay) {
			if ($scope.request.receiptDescription) {
				$scope.payment.payment_flag = 'contract';
			}

			var payMethod = '';
			_.forEach($scope.contract_page.paymentOptions, function (item) {
				if (item.alias == 'custom' && $scope.statuses[item.type]) {
					payMethod = item.name;
				}
			});

			$scope.receipt = {
				account_number: "",
				amount: _.round($scope.charge_value.value, 2),
				auth_code: "",
				card_type: "",
				date: moment().format('MM/DD/YYYY'),
				description: "",
				email: $scope.request.email,
				name: $scope.payment.name,
				invoice_id: "custom",
				payment_method: payMethod || "cash",
				phone: $scope.request.phone,
				transaction_id: payMethod || "Custom Payment",
				pending: false,
				payment_flag: $scope.payment.payment_flag,
				checkN: '',
				created: moment().unix()
			};

			if ($scope.request.receiptDescription) {
				$scope.receipt.description = $scope.request.receiptDescription;
			}

			PaymentServices.set_receipt($scope.request.nid, $scope.receipt, $scope.entity_type).then(function (data) {
				if (data[0] == false) {
					$scope.busy = false;
					var msg = '';

					if (data.text) {
						msg = data.text;
					}

					SweetAlert.swal({
						title: "Failed!",
						text: msg,
						type: 'error',
						html: true
					});
					let logMsg = receiptLogMsg( $scope.receipt, msg, '');
					saveToLogMsg(logMsg.msg, 'Receipt error', $scope.request.nid);

					return false;
				}

				if ($scope.request.agreement) {
					if (localStorage['save_agreement']) {
						if (angular.fromJson(localStorage['save_agreement']).request.move_request != $scope.request.move_request) {
							$scope.reciept_to_moveRequest = angular.copy($scope.receipt);
							PaymentServices.set_receipt($scope.request.move_request, $scope.receipt, $scope.enums.MOVEREQUEST).then(function (id) {
								$rootScope.$broadcast('save.agreement', _.head(data),_.head(id), $scope.receipt);
								$scope.reciept_to_moveRequest.id = _.head(id);
								$rootScope.$broadcast('receipt.rental', $scope.reciept_to_moveRequest);
							});
						}
					} else {
						$scope.reciept_to_moveRequest = angular.copy($scope.receipt);
						PaymentServices.set_receipt($scope.request.move_request, $scope.receipt, $scope.enums.MOVEREQUEST).then(function (id) {
							$rootScope.$broadcast('save.agreement', _.head(data),_.head(id), $scope.receipt);
							$scope.reciept_to_moveRequest.id = _.head(id);
							$rootScope.$broadcast('receipt.rental', $scope.reciept_to_moveRequest);
						});
					}
				}

				$rootScope.$broadcast('receipt_sent');
				$scope.busy = false;
				var message = "Custom payment in the amount  $" + $scope.receipt.amount + ' (method: ' + $scope.receipt.payment_method + ', type: ' + $scope.payment.payment_flag + ')' + ' was added.';
				var obj = {
					simpleText: message
				};
				var arr = setEditedFields();
				arr.push(obj);

				if ($scope.request.service_type) {
					RequestServices.sendLogs(arr, 'Custom payment', $scope.request.nid, entityType);
				}
				if($scope.request.move_request && $scope.request.agreement)
					RequestServices.sendLogs(arr, 'Custom payment', $scope.request.move_request, 'MOVEREQUEST');

				$scope.receipt.id = _.head(data);

				if ($scope.payment.payment_flag == 'contract') {
					if ($scope.request.receipts) {
						$scope.request.receipts.push($scope.receipt);
					}

					contractDispatchLogs.assignLogs($scope.request);
				}

				var receipt = {};
				receipt.charge_value = $scope.charge_value.value;

				if ($scope.payment.payment_flag == 'Reservation' || $scope.payment.payment_flag == 'Reservation by client') {
					$rootScope.$broadcast('payment.reservation', $scope.request);
					$scope.request.request_all_data.reservation_receipt = _.head(data);
					RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
					let editrequest = {field_reservation_price: "0"};
					$scope.request.field_reservation_received.value = true;
					$scope.request.reservation_rate.value = "0";
					RequestServices.updateRequest($scope.request.nid, editrequest);

					if ($scope.receipt.id && $scope.payment.payment_flag == 'Reservation by client') {
						saveReservationSignature($scope.receipt.id, $scope.reservSignature);
					}

					if($scope.payment.payment_flag == 'Reservation') {
						var key_name = ['please_confirm'];
						var emails = $rootScope.templateBuilder.emailTemplates;
						var retreaveEamils = SendEmailsService.initTemplatesByKeyName(key_name, emails);

						if (!_.isEmpty(retreaveEamils)) {
							SendEmailsService.sendEmails($scope.request.nid, [retreaveEamils[0]]);
						}
					}
				} else {
					let contractCashDiscount = 0;
					let contractCreditTax = 0;
					let requestTips = 0;
					if($scope.request.requestTipsPaid === false)
						requestTips = $scope.request.requestTips;

					if ($scope.statuses.cashPay && $scope.payment.payment_flag == 'contract' && !$scope.request.agreement) {
						let totalCharge = basicTotal;
						if($scope.changePaymentVal)
							totalCharge = angular.copy($scope.changePaymentVal);
						contractCashDiscount = _.round(totalCharge + requestTips - $scope.charge_value.value, 2);
					}

					$rootScope.$broadcast('payment.received', receipt, $scope.request, $scope.selectedTips, contractCreditTax, contractCashDiscount, requestTips, undefined, $scope.contractPageId);
				}
				let logMsg = receiptLogMsg( $scope.receipt, undefined, 'Receipt successfully completed');
				saveToLogMsg(logMsg.msg, 'Receipt', $scope.request.nid);
				sendPaymentNotification();

				if (!$scope.request.agreement) {
					EditRequestServices.getQuickBookFlag($scope.request);
					toastr.success("Payment was received!");
				}

				if ($scope.request.agreement) {
					$scope.step2 = false;
					$scope.step4 = true;
					return false;
				}

				$rootScope.$broadcast('initGetReceipts');
				$uibModalInstance.dismiss('cancel');
			}, function (reason) {
				let logMsg = receiptLogMsg( $scope.receipt, reason);
				saveToLogMsg(logMsg.msg, 'Receipt error', $scope.request.nid);
				$scope.busy = false;
				saveReceiptToLocalStorage();
				$uibModalInstance.dismiss('cancel');
				SweetAlert.swal("Failed!", reason, "error");
			});
		}

		if ($scope.statuses.checkPay) {
			if ($scope.request.receiptDescription) {
				$scope.payment.payment_flag = 'contract';
			}
			$scope.receipt = {
				account_number: "",
				amount: _.round($scope.charge_value.value, 2),
				auth_code: "",
				card_type: "",
				date: moment().format('MM/DD/YYYY'),
				description: "",
				email: $scope.request.email,
				invoice_id: "custom",
				name: $scope.payment.name,
				payment_method: "check",
				phone: $scope.request.phone,
				transaction_id: "Custom Payment",
				pending: false,
				payment_flag: $scope.payment.payment_flag,
				checkN: $scope.paymentCheck.check_num,
				created: moment().unix()
			};

			if ($scope.request.receiptDescription) {
				$scope.receipt.description = $scope.request.receiptDescription;
			}

			PaymentServices.set_receipt($scope.request.nid, $scope.receipt, $scope.entity_type).then(function (data) {
				$scope.busy = false;
				if (data[0] == false) {
					$scope.busy = false;
					var msg = '';
					if (data.text) {
						msg = data.text;
					}
					SweetAlert.swal({
						title: "Failed!",
						text: msg,
						type: "error",
						html: true
					});
					let logMsg = receiptLogMsg( $scope.receipt, reason);
					saveToLogMsg(logMsg.msg, msg, $scope.request.nid);
					return false;
				}

				if ($scope.request.agreement) {
					if (localStorage['save_agreement']) {
						if (angular.fromJson(localStorage['save_agreement']).request.move_request != $scope.request.move_request) {
							$scope.reciept_to_moveRequest = angular.copy($scope.receipt);
							PaymentServices.set_receipt($scope.request.move_request, $scope.receipt, $scope.enums.MOVEREQUEST).then(function (id) {
								$rootScope.$broadcast('save.agreement', _.head(data),_.head(id), $scope.receipt);
								$scope.reciept_to_moveRequest.id = _.head(id);
								$rootScope.$broadcast('receipt.rental', $scope.reciept_to_moveRequest);
							});
						}
					} else {
						$scope.reciept_to_moveRequest = angular.copy($scope.receipt);
						PaymentServices.set_receipt($scope.request.move_request, $scope.receipt, $scope.enums.MOVEREQUEST).then(function (id) {
							$rootScope.$broadcast('save.agreement', _.head(data),_.head(id), $scope.receipt);
							$scope.reciept_to_moveRequest.id = _.head(id);
							$rootScope.$broadcast('receipt.rental', $scope.reciept_to_moveRequest);
						});
					}
				}
				$rootScope.$broadcast('receipt_sent');
				var message = "Custom payment in the amount  $" + $scope.receipt.amount + ' (method: ' + $scope.receipt.payment_method + ', type: ' + $scope.payment.payment_flag + ')' + ' was added.';
				var obj = {
					simpleText: message
				};
				var arr = setEditedFields();
				arr.push(obj);

				if ($scope.request.service_type) {
					RequestServices.sendLogs(arr, 'Custom payment', $scope.request.nid, entityType);
				}
				if($scope.request.move_request && $scope.request.agreement)
					RequestServices.sendLogs(arr, 'Custom payment', $scope.request.move_request, 'MOVEREQUEST');

				$scope.receipt.id = _.head(data);

				if ($scope.payment.payment_flag == 'contract') {
					if ($scope.request.receipts) {
						$scope.request.receipts.push($scope.receipt);
					}
					contractDispatchLogs.assignLogs($scope.request);
				}
				var receipt = {};
				receipt.charge_value = $scope.charge_value.value;

				if ($scope.payment.payment_flag == 'Reservation' || $scope.payment.payment_flag == 'Reservation by client') {
					$rootScope.$broadcast('payment.reservation', $scope.request);
					$scope.request.request_all_data.reservation_receipt = $scope.receipt.id;
					RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
					let editrequest = {field_reservation_price: "0"};
					$scope.request.reservation_rate.value = "0";
					$scope.request.field_reservation_received.value = true;

					if ($scope.payment.payment_flag == 'Reservation') {
						var key_name = ['please_confirm'];
						var emails = $rootScope.templateBuilder.emailTemplates;
						var retreaveEamils = SendEmailsService.initTemplatesByKeyName(key_name, emails);

						if (!_.isEmpty(retreaveEamils)) {
							SendEmailsService.sendEmails($scope.request.nid, [retreaveEamils[0]]);
						}
					}
					RequestServices.updateRequest($scope.request.nid, editrequest);
					if($scope.receipt.id && $scope.payment.payment_flag == 'Reservation by client'){
						saveReservationSignature($scope.receipt.id, $scope.reservSignature);
					}
				} else {
					let requestTips = 0;
					let contractCreditTax = 0;
					let contractCashDiscount = 0;
					if($scope.request.requestTipsPaid === false)
						requestTips = $scope.request.requestTips;
					$rootScope.$broadcast('payment.received', receipt, $scope.request, $scope.selectedTips, contractCreditTax, contractCashDiscount, requestTips, undefined, $scope.contractPageId);
				}
				let logMsg = receiptLogMsg( $scope.receipt, undefined, 'Receipt successfully completed');
				saveToLogMsg(logMsg.msg, 'Receipt', $scope.request.nid);
				sendPaymentNotification();

				if (!$scope.request.agreement) {
					EditRequestServices.getQuickBookFlag($scope.request);
					toastr.success("Payment was received!");
				}

				if ($scope.request.agreement) {
					$scope.step2 = false;
					$scope.step4 = true;
					return false;
				}
				$rootScope.$broadcast('initGetReceipts');
				$uibModalInstance.dismiss('cancel');
			}, function (reason) {
				let logMsg = receiptLogMsg( $scope.receipt, reason);
				saveToLogMsg(logMsg.msg, 'Receipt error', $scope.request.nid);
				saveReceiptToLocalStorage();
				$scope.busy = false;
				$uibModalInstance.dismiss('cancel');
				SweetAlert.swal("Failed!", reason, "error");
			});
		}

		if ($scope.statuses.creditPay) {

			if ($scope.request.receiptDescription) {
				$scope.payment.payment_flag = 'contract';
			}

			var numb = $scope.payment.card_num.replace(/ /g, '');

			if ($scope.request.request_invoice || $scope.request.storage_invoice) {
				var payment_info = {
					"name": $scope.payment.name,
					"phone": $scope.request.phone,
					"is_invoice": $scope.request.request_invoice || $scope.request.storage_invoice,
					"invoice_id": $scope.request.id,
					"email": $scope.request.email,
					"zip_code": $scope.payment.billing_zip,
					"card_type": GetCardType(numb),
					"description": `Payment for Invoice #${$scope.request.nid}-${$scope.request.id}`,
					"credit_card": {
						"card_number": $scope.payment.card_num.replace(/ /g, ''),
						"exp_date": $scope.payment.exp_month + "/" + $scope.checkout_form.exp_year.$viewValue,
						"card_code": $scope.secure.cvc
					},
					"payment_method": 'creditcard',
					"payment_flag": $scope.payment.payment_flag,
					"date": moment().format('MM/DD/YYYY'),
					"created": moment().unix(),
					"amount": $scope.charge_value.value
				};
			} else {
				var payment_info = {
					"uid": $scope.request.uid.uid,
					"name": $scope.payment.name,
					"zip_code": $scope.payment.billing_zip,
					"phone": $scope.request.phone,
					"email": $scope.request.email,
					"card_type": GetCardType(numb),
					"description": $scope.payment.payment_flag + " Request #" + ($scope.request.storage_invoice ? $scope.request.entity_id : $scope.request.nid),
					"credit_card": {
						"card_number": $scope.payment.card_num.replace(/ /g, ''),
						"exp_date": $scope.payment.exp_month + "/" + $scope.checkout_form.exp_year.$viewValue,
						"card_code": $scope.secure.cvc
					},
					"payment_method": 'creditcard',
					"payment_flag": $scope.payment.payment_flag,
					"amount": $scope.charge_value.value,
					"created": moment().unix()
				};
			}

			if ($scope.cvv.Amex) {
				payment_info.credit_card.card_code = $scope.secure.cvcAmex;
			}

			var reciept_to_request = {
				"amount": $scope.charge_value.value,
				"type": $scope.payment.payment_flag,
				"ccardN": $scope.payment.card_num.replace(/ /g, ''),
				"name": $scope.payment.name,
				"zip_code": $scope.payment.billing_zip,
				"phone": $scope.request.phone,
				"email": $scope.request.email,
				"description": $scope.payment.payment_flag + "Payment for Request #" + $scope.request.nid,
				"date": moment().format('MM/DD/YYYY'),
				"created": moment().unix(),
				"card_type": GetCardType(numb),
				"pending": false,
				"payment_flag": $scope.payment.payment_flag,
				"payment_method": 'creditcard'
			};

			if ($scope.request.receiptDescription) {
				payment_info.description = $scope.request.receiptDescription;
				reciept_to_request.description = $scope.request.receiptDescription;
			}
			if ($scope.request.payForStorage) {
				payment_info.description = $scope.checkout_title;
				reciept_to_request.description = $scope.checkout_title;
			}

			$scope.receipt = payment_info;
			delete payment_info.uid;

			if ($scope.entity_type == $scope.fieldData.enums.entities.COUPON) {
				payment_info.coupon = {};
				payment_info.coupon.discount = $scope.request.discount;
				payment_info.coupon.coupon_id = $scope.request.nid;
				payment_info.coupon.hash = $scope.request.hash;

				if ($scope.request.uid.uid) {
					payment_info.uid = $scope.request.uid.uid;
				}
			}

			if ($scope.request.agreement_repeat) {
				var paymentPromise = PaymentServices.applyPayment(payment_info, $scope.request.move_request, $scope.enums.MOVEREQUEST);
			} else if($scope.request.sit && $scope.request.request_invoice) {
				payment_info.jobs = angular.copy($scope.request.jobs_id);
				payment_info.transaction_type = 2;
				payment_info.entity_id = $scope.request.nid;
				payment_info.entity_type = $scope.entity_type;
				payment_info = {
					data: payment_info
				};
				var paymentPromise = PaymentServices.applySitPayment(payment_info, $scope.request.nid, $scope.entity_type);
			} else {
				var paymentPromise = PaymentServices.applyPayment(payment_info, $scope.request.nid, $scope.entity_type);
			}

			paymentPromise.then(function (data) {
				if (_.isObject(data)) {
					data = data.data || data;
				}

				$scope.busy = false;
				let transactionError = !_.isEmpty(data) && !data.receipt_id && !data.transaction_id;
				let receiptNotCreated = !_.isEmpty(data) && !data.receipt_id && data.transaction_id;

				if (transactionError || !data.status) {
					let msg = data.text ? data.text : '"Authorize.net" has response failed. Try again in a couple of minutes.';
					SweetAlert.swal({
						title: 'Transaction failed!',
						text: msg,
						type: "error",
						html: true
					});
					return false;
				} else if (receiptNotCreated) {
					let email = getReceiptErrorEmail();
					const COMPANY_EMAIL = 'dev@elromco.com';
					if (!_.isEmpty(email)) {
						SendEmailsService.sendEmailsTo($scope.request.nid, email, COMPANY_EMAIL);
					}
					getSuccessfullyAuthorizeAnswer(data, reciept_to_request, payment_info);
				} else {
					getSuccessfullyAuthorizeAnswer(data, reciept_to_request, payment_info);
				}
			}, function (reason) {
				$scope.busy = false;

				if ($scope.request.agreement) {
					return false;
					SweetAlert.swal("Failed!", reason, "error");
				}

				if ($scope.payment.payment_flag == 'contract') {
					saveReceiptToLocalStorage();
				}

				if ($scope.payment.payment_flag == 'contract' && $scope.request.receipts) {
					$scope.request.receipts.push(reciept_to_request);
				}

				if ($scope.payment.payment_flag == 'contract') {
					$scope.step2 = false;
					$scope.step3 = true;
				} else if ($scope.payment.payment_flag == 'Reservation by client') {
					return false;
				} else {
					$scope.step2 = false;
					$uibModalInstance.dismiss('cancel');
				}

				$scope.busy = false;

				SweetAlert.swal("Failed!", reason, "error");
			});
		}

		if (!$scope.statuses.cashPay && !$scope.statuses.checkPay && !$scope.statuses.creditPay) {
			$scope.busy = true;
			let paymentMethod;

			for(let status in $scope.statuses) {
				if ($scope.statuses[status]) {
					paymentMethod = status;
				}
			}

			if ($scope.request.receiptDescription) {
				$scope.payment.payment_flag = 'contract';
			}

			$scope.receipt = {
				account_number: "",
				amount: _.round($scope.charge_value.value, 2),
				auth_code: "",
				card_type: "",
				date: moment().format('MM/DD/YYYY'),
				description: $scope.request.receiptDescription || "",
				email: $scope.request.email,
				name: $scope.payment.name,
				invoice_id: "custom",
				payment_method: paymentMethod,
				phone: $scope.request.phone,
				transaction_id: paymentMethod,
				pending: false,
				payment_flag: $scope.payment.payment_flag,
				checkN: '',
				created: moment().unix()
			};

			PaymentServices.set_receipt($scope.request.nid, $scope.receipt, $scope.entity_type)
				.then((data) => {
					if (data[0] == false) {
						$scope.busy = false;
						let msg = data.text || '';

						SweetAlert.swal({title: "Failed!", text: msg, type: "Error", html: true});

						saveToLogMsg(receiptLogMsg($scope.receipt, msg, ''),
							'Receipt error',
							$scope.request.nid);

						return false;
					}

					$scope.reciept_to_moveRequest = angular.copy($scope.receipt);

					if ($scope.request.agreement) {
						PaymentServices.set_receipt($scope.request.move_request, $scope.receipt, $scope.enums.MOVEREQUEST)
							.then((id) => {
								$rootScope.$broadcast('save.agreement', _.head(data),_.head(id), $scope.receipt);
								$scope.reciept_to_moveRequest.id = _.head(id);
								$rootScope.$broadcast('receipt.rental', $scope.reciept_to_moveRequest);
							})
							.catch(() => SweetAlert.swal({title: "Failed!", text: "Error", type: "Error", html: true}));
					}

					$rootScope.$broadcast('receipt_sent');
					$scope.busy = false;

					let arr = setEditedFields();

					arr.push({
						simpleText: `Custom payment in the amount $${$scope.receipt.amount}
                                                (method: ${$scope.receipt.payment_method} , type: ${$scope.payment.payment_flag} ) was added.`
					});

					if ($scope.request.service_type) {
						RequestServices.sendLogs(arr, 'Custom payment', $scope.request.nid, entityType);
					}
					if($scope.request.move_request && $scope.request.agreement) {
						RequestServices.sendLogs(arr, 'Custom payment', $scope.request.move_request, 'MOVEREQUEST');
					}

					$scope.receipt.id = _.head(data);

					if ($scope.payment.payment_flag == 'contract') {
						if ($scope.request.receipts) {
							$scope.request.receipts.push($scope.receipt);
						}
						contractDispatchLogs.assignLogs($scope.request);
					}

					let receipt = {};
					receipt.charge_value = $scope.charge_value.value;

					if ($scope.payment.payment_flag == 'Reservation' || $scope.payment.payment_flag == 'Reservation by client') {
						$rootScope.$broadcast('payment.reservation', $scope.request);
						$scope.request.request_all_data.reservation_receipt = _.head(data);
						RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
						let editrequest = {field_reservation_price: "0"};
						$scope.request.field_reservation_received.value = true;
						$scope.request.reservation_rate.value = "0";
						RequestServices.updateRequest($scope.request.nid, editrequest);

						if ($scope.receipt.id && $scope.payment.payment_flag == 'Reservation by client') {
							saveReservationSignature($scope.receipt.id, $scope.reservSignature);
						} else if($scope.payment.payment_flag == 'Reservation') {
							let retreaveEmails = SendEmailsService.initTemplatesByKeyName(['please_confirm'], $rootScope.templateBuilder.emailTemplates);

							if (!_.isEmpty(retreaveEmails)) {
								SendEmailsService.sendEmails($scope.request.nid, [retreaveEmails[0]]);
							}
						}
					} else {
						let totalCharge = $scope.changePaymentVal ? $scope.changePaymentVal : basicTotal,
							contractCashDiscount = 0,
							contractCreditTax = 0,
							requestTips = $scope.request.requestTips || 0,
							customPayment = {
								name: paymentMethod,
								value: _.round($scope.charge_value.value - totalCharge - requestTips, 2)
							};

						$rootScope.$broadcast('payment.received', receipt, $scope.request, $scope.selectedTips, contractCreditTax, contractCashDiscount, requestTips, customPayment, $scope.contractPageId);
					}

					saveToLogMsg(receiptLogMsg($scope.receipt, undefined, 'Receipt successfully completed'),
						'Receipt',
						$scope.request.nid);

					sendPaymentNotification();

					if (!$scope.request.agreement) {
						EditRequestServices.getQuickBookFlag($scope.request);
						toastr.success("Payment was received!");
					} else {
						$scope.step2 = false;
						$scope.step4 = true;
						return false;
					}

					$scope.busy = false;
					$rootScope.$broadcast('initGetReceipts');
					$uibModalInstance.dismiss('cancel');
				})
				.catch((err) => {
					saveToLogMsg(receiptLogMsg($scope.receipt, err),
						'Receipt error',
						$scope.request.nid);

					saveReceiptToLocalStorage();
					$scope.busy = false;
					$uibModalInstance.dismiss('cancel');
					SweetAlert.swal("Failed!", err, "error");
				});
		}
	};

	function getSuccessfullyAuthorizeAnswer(data, reciept_to_request, payment_info) {
		$scope.receipt.id = data.receipt_id;
		reciept_to_request.id = data.receipt_id;

		var arr = setEditedFields();
		RequestServices.sendLogs(arr, 'Online payment', $scope.request.nid, entityType);

		if ($scope.entity_type == $scope.fieldData.enums.entities.COUPON) {
			$scope.couponMsg = `<h3>We have sent you an email with coupon details.</h3><h3> But just in case your redeem code is:<b>${data.promo}</b></h3>`;
			$rootScope.$broadcast('coupon.purchased');
			$scope.step2 = false;
			$scope.step3 = true;

			sendPaymentNotification();
			return false;
		}

		if ($scope.request.agreement_repeat) {
			if (localStorage['save_agreement']) {
				if (angular.fromJson(localStorage['save_agreement']).request.move_request == $scope.request.move_request) {
					delete localStorage['save_agreement'];
				}
			}
		}
		var receiptData = {
			amount: _.round($scope.charge_value.value, 2),
			date: moment().format('MM/DD/YYYY'),
			created: moment().unix(),
			description: $scope.request.receiptDescription,
			email: $scope.request.email,
			name: $scope.payment.name,
			invoice_id: "custom",
			payment_method: 'creditcard',
			phone: $scope.request.phone,
			transaction_id: "Custom Payment",
			pending: false,
			type: 'creditcard',
			payment_flag: $scope.payment.payment_flag,
			cctype: GetCardType($scope.payment.card_num),
			ccardN: $scope.payment.card_num.replace(/ /g, ''),
			cvv: $scope.secure.cvc,
			expmonth: $scope.payment.exp_month,
			expyear: $scope.checkout_form.exp_year.$viewValue
		};

		if ($scope.request.agreement && !$scope.request.agreement_repeat) {
			if (localStorage['save_agreement']) {
				if (angular.fromJson(localStorage['save_agreement']).request.move_request != $scope.request.move_request) {
					$scope.reciept_to_moveRequest = angular.copy(receiptData);
					PaymentServices.set_receipt($scope.request.move_request, $scope.reciept_to_moveRequest, $scope.enums.MOVEREQUEST).then(function (id) {
						if (_.isEmpty(data) || !data.receipt_id || data.status == false) {
							$scope.reciept_to_moveRequest.id = data.receipt_id;
							saveAgreementPayment();
							return false;
						}
						$rootScope.$broadcast('save.agreement', data.receipt_id,_.head(id), payment_info);
						$scope.reciept_to_moveRequest.id = _.head(id);
						$rootScope.$broadcast('receipt.rental', $scope.reciept_to_moveRequest);
					});
				}
			} else {
				$scope.reciept_to_moveRequest = angular.copy(receiptData);
				PaymentServices.set_receipt($scope.request.move_request, $scope.reciept_to_moveRequest, $scope.enums.MOVEREQUEST).then(function (id) {
					if (_.isEmpty(data) || !data.receipt_id || data.status == false) {
						$scope.reciept_to_moveRequest.id = data.receipt_id;
						saveAgreementPayment();
						return false;
					}
					$rootScope.$broadcast('save.agreement', data.receipt_id,_.head(id), payment_info);
					$scope.reciept_to_moveRequest.id = _.head(id);
					$rootScope.$broadcast('receipt.rental', $scope.reciept_to_moveRequest);
				});
			}
		}
		sendPaymentNotification();
		$rootScope.$broadcast('receipt_sent', reciept_to_request);

		if ($scope.payment.payment_flag == 'contract') {
			if ($scope.request.receipts) {
				$scope.request.receipts.push(reciept_to_request);
			}
			contractDispatchLogs.assignLogs($scope.request);
		}

		var receipt = {};
		receipt.charge_value = $scope.charge_value.value;

		if ($scope.payment.payment_flag == 'Reservation' || $scope.payment.payment_flag == 'Reservation by client') {
			$scope.request.request_all_data.reservation_receipt = data.receipt_id;
			if ($scope.payment.payment_flag == 'Reservation by client'){
				$scope.request.receipts.push(reciept_to_request);
				$scope.request.request_all_data.invoice.rate = angular.copy($scope.request.rate);

				let isRequestHasPackingDay = $scope.request.request_all_data.packing_request_id
					&& $scope.request.service_type.raw != 8;
				if (isRequestHasPackingDay) {
					RequestServices.UpdatePackingRequest($scope.request.request_all_data.packing_request_id, {
						serviceType: $scope.request.service_type.value,
						status: 'Confirmed',
						moveDate: moment($scope.request.date.value).format('MMM DD, YYYY')
					});
				}

			}
			$scope.request.field_reservation_received.value = true;
			$rootScope.$broadcast('payment.reservation', $scope.request);
			if($scope.payment.payment_flag == 'Reservation') {
				var key_name = ['please_confirm'];
				var emails = $rootScope.templateBuilder.emailTemplates;
				var retreaveEamils = SendEmailsService.initTemplatesByKeyName(key_name, emails);

				if (!_.isEmpty(retreaveEamils)) {
					SendEmailsService.sendEmails($scope.request.nid, [retreaveEamils[0]]);
				}
			}
			RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data)
			if($scope.receipt.id && $scope.payment.payment_flag == 'Reservation by client'){
				saveReservationSignature($scope.receipt.id, $scope.reservSignature);
				return false;
			}
		} else {
			let creditTax = 0;
			let requestTips = 0;
			let contractCashDiscount = 0;
			if($scope.request.requestTipsPaid === false)
				requestTips = $scope.request.requestTips;
			if($scope.statuses.creditPay && $scope.payment.payment_flag == 'contract' && creditChargeTax && !$scope.request.agreement) {
				let totalCharge = basicTotal;
				if($scope.changePaymentVal)
					totalCharge = angular.copy($scope.changePaymentVal);
				creditTax = _.round($scope.charge_value.value - totalCharge - requestTips, 2);
			}
			$rootScope.$broadcast('payment.received', receipt, $scope.request, $scope.selectedTips, creditTax, contractCashDiscount, requestTips, undefined, $scope.contractPageId);
		}

		if (!$scope.request.agreement) {
			EditRequestServices.getQuickBookFlag($scope.request);
			toastr.success("Payment was received!");
		}
		if ($scope.payment.payment_flag == 'contract') {
			$scope.step2 = false;
			$scope.step3 = true;
			saveReceiptToLocalStorage();
		} else if ($scope.payment.payment_flag == 'Reservation by client') {
			$scope.step2 = false;
			$scope.step5 = true;
		} else {
			$scope.step2 = false;
			$uibModalInstance.dismiss('cancel');
		}
		$rootScope.$broadcast('initGetReceipts');
	}

	function getReceiptErrorEmail() {
		let templates = _.clone($rootScope.templateBuilder.emailTemplates);
		let receiptErrorKeyName = ["receipt_failed"];
		let result = SendEmailsService.initTemplatesByKeyName(receiptErrorKeyName, templates);

		return result;
	}


	$scope.clearReservSignature = function () {
		$scope.signaturePad.clear();
	};

	$scope.isCanvasBlank = function (elementID) {
		var canvas = document.getElementById(elementID);
		var blank = document.createElement('canvas');
		blank.width = canvas.width;
		blank.height = canvas.height;
		var context = canvas.getContext("2d");
		var perc = 0, area = canvas.width * canvas.height;

		if (canvas.toDataURL() == blank.toDataURL()) {
			return canvas.toDataURL() == blank.toDataURL();
		} else {
			var data = context.getImageData(0, 0, canvas.width, canvas.height).data;

			for (var ct = 0, i = 3, len = data.length; i < len; i += 4) {
				if (data[i] > 50) {
					ct++;
				}
			}

			perc = _.round(100 * ct / area, 2);

			return (perc < 0.1);
		}
	};

	$scope.saveReservSignature = function () {
		var blank = $scope.isCanvasBlank('signatureCanvasReserv');

		if (blank) {
			SweetAlert.swal("Failed!", 'Sign please', "error")
		} else {
			var signatureValue = getCropedSignatureValue();
			var SIGNATURE_PREFIX = 'data:image/gif;base64,';

			var signature = {
				value: SIGNATURE_PREFIX + signatureValue,
				date: moment().format('x')
			};
			var data = [];
			data.push(signature.value);
			$scope.reservSignature = data;
			var id = $scope.request.request_all_data.reservation_receipt;
			if(id){
				saveReservationSignature(id, data);
				return false;
			}

			$scope.step5 = false;
			$scope.step2 = true;
		}
	};

	function saveReservationSignature(id, data){
		$scope.busy = true;
		PaymentServices.saveCardImage(id, data).then(function () {
			var data = {
				field_approve: 3
			};
			$scope.busy = true;
			RequestServices.updateRequest($scope.request.nid, data).then(function () {
				$scope.request.status.raw = 3;
				$scope.busy = false;
				$uibModalInstance.dismiss('cancel');
				redirectToPortal();

			});
			var title = "Payment" + ($scope.request.payment_flag ?( "(" + $scope.request.payment_flag + ")" ) : '');
			saveToLogMsg('Signature was successfully saved.', title, $scope.request.nid);
		}, function (reason) {
			$scope.busy = false;
			saveToLogMsg('Error: ' + reason + ' Receipt id:' + id, 'File saving error', $scope.request.nid);
			SweetAlert.swal("Failed!", reason, "error");
		});
	}

	$scope.$on('storage.request.created', function (ev, data) {
		$scope.request.request_all_data.storage_request_id = data;
		RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
	});

	function saveReceiptToStorage (images, ID) {
		var name = 'receipt_' + ID;
		localStorage[name] = angular.toJson({
			images: images,
			ID: ID,
			nid: $scope.request.nid
		});
	}

	$scope.saveDataByReceiptID = function () {
		$scope.busy = true;

		var signatureValue = getCropedSignatureValue();
		var SIGNATURE_PREFIX = 'data:image/gif;base64,';

		var signature = {
			value: SIGNATURE_PREFIX + signatureValue,
			date: moment().format('x')
		};

		var data = [];
		data.push(signature.value);
		data.push($scope.card_photo.front.base64.compressed.dataURL);

		if ($scope.card_photo.back.base64) {
			if ($scope.card_photo.back.base64.dataURL) {
				data.push($scope.card_photo.back.base64.compressed.dataURL);
			}
		}

		let creditTax = 0;
		if($scope.statuses.creditPay && $scope.payment.payment_flag == 'contract' && !$scope.request.agreement)
			creditTax = _.round($scope.charge_value.value - basicTotal, 2);

		$rootScope.$broadcast('sever_error.payment.received', $scope.charge_value.value, $scope.selectedTips, creditTax);
		saveReceiptToStorage(data, $scope.receipt.id, $scope.selectedTips);
		$uibModalInstance.dismiss('cancel');
		$scope.busy = false;
	};

	$scope.saveFile = function () {
		if (!$scope.serverError) {
			var data = [];
			if ($scope.card_photo.front.base64) {
				if ($scope.card_photo.front.base64.dataURL) {
					data.push($scope.card_photo.front.base64.compressed.dataURL);
				}
			}

			if ($scope.card_photo.back.base64) {
				if ($scope.card_photo.back.base64.dataURL) {
					data.push($scope.card_photo.back.base64.compressed.dataURL);
				}
			}

			$rootScope.$broadcast('images_upload', data);
			$scope.busy = true;

			if(storageId && requestId){
				var promise1 = PaymentServices.saveCardImage(storageId, data);
				var promise2 = PaymentServices.saveCardImage(requestId, data);
				$q.all([promise1, promise2]).then(function () {
					$scope.busy = false;

					$rootScope.$broadcast('payment.rental');
					$rootScope.$broadcast('remove.agreement.data');

					var title = "Payment" + ($scope.request.payment_flag ? ("(" + $scope.request.payment_flag + ")" ) : '');
					saveToLogMsg('Signature was successfully saved.', title, $scope.request.request_all_data.storage_request_id, $scope.enums.STORAGEREQUEST);
					saveToLogMsg('Signature was successfully saved.', title, $scope.request.nid, $scope.enums.MOVEREQUEST);

					$uibModalInstance.dismiss('cancel');
				});
				return false;
			}

			PaymentServices.saveCardImage($scope.receipt.id, data).then(function () {
				$scope.busy = false;
				if (localStorage['save_receipt'] && $scope.payment.payment_flag == 'contract') {
					delete localStorage['save_receipt'];
				}

				if ($scope.request.request_all_data) {
					if ($scope.request.request_all_data.save_receipt) {
						delete $scope.request.request_all_data.save_receipt;
						RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
					}
				}

				if ($scope.request.agreement && $scope.reciept_to_moveRequest) {
					$rootScope.$broadcast('remove.agreement.data');
					if ($scope.reciept_to_moveRequest.id) {
						PaymentServices.saveCardImage($scope.reciept_to_moveRequest.id, data);
						$rootScope.$broadcast('payment.rental', $scope.receipt);
					} else {
						$rootScope.$broadcast('payment.rental', $scope.receipt);
						$rootScope.$broadcast('save.agreement.payment');
					}
				}

				var title = "Payment" + ($scope.request.payment_flag ? ("(" + $scope.request.payment_flag + ")" ) : '');
				saveToLogMsg('Signature was successfully saved.', title, $scope.request.nid);

				if ($scope.request.agreement_repeat) {
					if (localStorage['save_agreement']) {
						if (angular.fromJson(localStorage['save_agreement']).request.move_request == $scope.request.move_request) {
							delete localStorage['save_agreement'];
						}
					}
				}

				if ($scope.request.agreement && !$scope.reciept_to_moveRequest.id && !$scope.request.agreement_repeat) {
					if (localStorage['save_agreement']) {
						if (angular.fromJson(localStorage['save_agreement']).request.move_request != $scope.request.move_request) {
							var obj = {
								request: $scope.request,
								receipt: $scope.reciept_to_moveRequest
							};
							localStorage['save_agreement'] = angular.toJson(obj);
							$rootScope.$broadcast('save.agreement.payment');
						}
					} else {
						var obj = {
							request: $scope.request,
							receipt: $scope.reciept_to_moveRequest
						};
						localStorage['save_agreement'] = angular.toJson(obj);
						$rootScope.$broadcast('save.agreement.payment');
					}
				}

				$uibModalInstance.dismiss('cancel');
			}, function (reason) {
				$scope.busy = false;
				if ($scope.request.agreement) {
					return false;
				}

				var data = [];

				if ($scope.card_photo.front.base64) {
					if ($scope.card_photo.front.base64.dataURL) {
						data.push($scope.card_photo.front.base64.compressed.dataURL);
					}
				}

				if ($scope.card_photo.back.base64) {
					if ($scope.card_photo.back.base64.dataURL) {
						data.push($scope.card_photo.back.base64.compressed.dataURL);
					}
				}

				if (localStorage['save_receipt'] && $scope.payment.payment_flag == 'contract') {
					var data = [];
					data.push($scope.card_photo.front.base64.compressed.dataURL);

					if ($scope.card_photo.back.base64) {
						if ($scope.card_photo.back.base64.dataURL) {
							data.push($scope.card_photo.back.base64.compressed.dataURL);
						}
					}

					var im = angular.fromJson(localStorage['save_receipt']);
					im.signature = true;
					im.images = data;
					localStorage['save_receipt'] = angular.toJson(im);
				} else {
					if ($scope.payment.payment_flag == 'contract') {
						saveReceiptToLocalStorage();
						var im = angular.fromJson(localStorage['save_receipt']);
						im.signature = true;
						im.images = data;
						localStorage['save_receipt'] = angular.toJson(im);
					}
				}

				$uibModalInstance.dismiss('cancel');

				let logMsg = receiptLogMsg( $scope.receipt, reason);
				saveToLogMsg( logMsg.msg, 'File saving error', $scope.request.nid);
				SweetAlert.swal("Failed!", reason, "error");
			});
		} else {
			if (localStorage['save_receipt'] && $scope.payment.payment_flag == 'contract') {
				var data = [];
				if ($scope.card_photo.front.base64) {
					if ($scope.card_photo.front.base64.dataURL) {
						data.push($scope.card_photo.front.base64.compressed.dataURL);
					}
				}

				if ($scope.card_photo.back.base64) {
					if ($scope.card_photo.back.base64.dataURL) {
						data.push($scope.card_photo.back.base64.compressed.dataURL);
					}
				}

				var im = angular.fromJson(localStorage['save_receipt']);
				im.signature = true;
				im.images = data;
				localStorage['save_receipt'] = angular.toJson(im);
				$uibModalInstance.dismiss('cancel');
			}

			$scope.step4 = false;
			$scope.step6 = true;
		}
	};

	$scope.removeFile = function (type) {
		if (type == 'all') {
			if ($scope.card_photo.front.base64) {
				$scope.card_photo.front.base64.dataURL = '';
				$scope.card_photo.back.base64.dataURL = '';
				if ($scope.card_photo.front.base64.compressed) {
					$scope.card_photo.front.base64.compressed.dataURL = '';
					$scope.card_photo.back.base64.compressed.dataURL = '';
				}
			}
		}

		if (type == 'back') {
			$scope.card_photo.back.base64.dataURL = '';
			$scope.card_photo.back.base64.compressed.dataURL = '';
		}

		if (type == 'front') {
			$scope.card_photo.front.base64.dataURL = '';
			$scope.card_photo.front.base64.compressed.dataURL = '';
		}
	};

	function sendPaymentNotification(){
		let data = {
			notification: {
				text: ''
			},
			nid: $scope.request.nid
		};
		let typeEnum = $scope.enumsNotification.CUSTOMER_PAY_CASH_FROM_CONTRACT_ANY_PAYMENT;
		let dataNotify = {};
		if($scope.entity_type == $scope.fieldData.enums.entities.COUPON){
			delete data.nid;
			data.notification.text = 'Customer has bought coupon';
			typeEnum = $scope.enumsNotification.CUSTOMER_BUY_COUPON;
			dataNotify = {
				type: typeEnum,
				data: data,
				entity_id: $scope.request.requestNid,
				entity_type: $scope.fieldData.enums.entities.MOVEREQUEST
			};
			apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
			return false;
		}
		if ($scope.payment.payment_flag == 'contract' && !$scope.request.agreement) {
			if ($scope.receipt.description) {
				data.notification.text = $scope.receipt.description;
			}
			if ($scope.statuses.checkPay) {
				typeEnum = $scope.enumsNotification.CUSTOMER_PAY_CHECK_FROM_CONTRACT;
				data.notification.text += ' .Check payment was received on the contract';
			}
			if ($scope.statuses.cashPay || $scope.statuses.customPay) {
				typeEnum = $scope.enumsNotification.CUSTOMER_PAY_CASH_FROM_CONTRACT;
				data.notification.text += ' .Cash payment was received on the contract';
			}
			if ($scope.statuses.creditPay) {
				typeEnum = $scope.enumsNotification.CUSTOMER_PAY_CREDIT_CARD_FROM_CONTRACT;
				data.notification.text += ' .Credit card payment was received on the contract';
			}
			dataNotify = {
				type: typeEnum,
				data: data,
				entity_id: $scope.request.nid,
				entity_type: $scope.fieldData.enums.entities.MOVEREQUEST
			};
			apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
		}else if($scope.request.agreement){
			data = {
				node: { nid: $scope.request.move_request },
				notification: {
					text:  $scope.receipt.description
				}
			};
			if($scope.statuses.checkPay){
				typeEnum = $scope.enumsNotification.CUSTOMER_PAY_CHECK_FROM_STORAGE_AGREEMENT;
				data.notification.text += '. Check payment.';
			}
			if($scope.statuses.cashPay  || $scope.statuses.customPay){
				typeEnum = $scope.enumsNotification.CUSTOMER_PAY_CASH_FROM_STORAGE_AGREEMENT;
				data.notification.text += '. Cash payment.';
			}
			if($scope.statuses.creditPay){
				typeEnum = $scope.enumsNotification.CUSTOMER_PAY_CREDIT_CARD_FROM_STORAGE_AGREEMENT;
				data.notification.text += '. Credit card payment.';
			}
			dataNotify = {
				type: typeEnum,
				data: data,
				entity_id: $scope.request.move_request,
				entity_type: $scope.fieldData.enums.entities.MOVEREQUEST
			};
			apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
		}else if($scope.payment.payment_flag == 'Reservation by client'){
			data.notification.text = 'Request is confirmed';
			typeEnum = $scope.enumsNotification.CUSTOMER_BOOK_JOB;
			dataNotify = {
				type: typeEnum,
				data: data,
				entity_id: $scope.request.nid,
				entity_type: $scope.fieldData.enums.entities.MOVEREQUEST
			};
			apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
		}
	}

	$scope.cancel = function () {
		var message = 'User close payment modal.';
		var obj = {
			simpleText: message
		};
		var arr = [obj];
		if($scope.payment.payment_flag != 'regular' && !$scope.editForm.flag.edited)
			RequestServices.sendLogs(arr, 'Payment', $scope.request.nid, entityType);
		$uibModalInstance.dismiss('cancel');
	};

	$scope.$on('$destroy', function () {
		$rootScope.$broadcast('close.payment.modal');
	});

	function redirectToPortal () {
		if (IS_HOME_ESTIMATOR) {
			$rootScope.$broadcast('redirect-to-in-home-estimate-request');
		} else {
			$location.path('/request/' + $scope.request.nid);
		}
	}

}
