(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('ccPaymentBlock', ccPaymentBlock)
        .directive('ccCreditCard', ccCreditCard)
        .directive('ccPaymentLink', ccPaymentLink)
        .directive('ccPaymentNumber', ccPaymentNumber)
        .directive('ccPaymentNextDiv', ccPaymentNextDiv)
        .directive('ccPaymentNext', ['$parse', function($parse) {
            return {
                restrict: 'A',
                require: ['ngModel'],
                link: function(scope, element, attrs, ctrls) {
                    var model=ctrls[0], form=ctrls[1];

                    scope.next = function(){
                        return model.$valid
                    }

                    scope.$watch(scope.next, function(newValue, oldValue){
                        if (newValue && model.$dirty)
                        {
                            var nextinput = element.next('input');
                            if (nextinput.length === 1)
                            {
                                nextinput[0].focus();
                            }
                        }
                    })
                }
            }
        }])


    function ccPaymentNextDiv($parse) {
        return {
            restrict: 'A',
            require: ['ngModel'],
            link: function (scope, element, attrs, ctrls) {

                var model=ctrls[0], form=ctrls[1];
                var amex = false,
                    re,
                    cardId = 'card_number',
                    expMonth = 'exp_month';

                    scope.next = function(){
                        return model.$valid
                    }
                    

                    scope.$watch(scope.next, function(newValue, oldValue){
                        if(element.attr('id') == cardId){
                            re = new RegExp("^3[47]");
                            if(angular.isDefined(model.$modelValue))
                                amex = model.$modelValue.match(re) != null;

                            if(amex == true){
                                if (newValue && model.$dirty && model.$valid)
                                {
                                    var nextinput = element.closest('div').next('div').find('input');
                                    if (nextinput.length === 1)
                                    {
                                        nextinput[0].focus();
                                    }
                                }
                            }else{
                               if (newValue && model.$dirty && model.$valid)
                                {
                                    var nextinput = element.closest('div').next('div').find('input');
                                    if (nextinput.length === 1)
                                    {
                                        nextinput[0].focus();
                                    }
                                }
                            }
                        } else{
                            if (newValue && model.$dirty)
                                {
                                    var nextinput = element.closest('div').next('div').find('input');
                                    if (nextinput.length === 1)
                                    {
                                        nextinput[0].focus();
                                    }
                                }
                        }
                    })
            }
        }
    }

    function ccPaymentNumber() {
        return {
            restrict: 'A',
            link: function (element) {

                element.bind('change', function () {
                    if(_.isNaN(Number(element.val())))
                        element.val('');
                });
            }
        }
    };

    function ccCreditCard() {
        return {
            restrict: 'A',
            scope: {},
            link: function (scope, element) {

                element.bind('change paste keyup', function () {

                    var credit_number = element.val();

                     var type = GetCardType(credit_number);
                        if(angular.isDefined(scope.$parent.cardClass))
                            element.removeClass(scope.$parent.cardClass);
                        scope.$parent.cardClass = type;
                        element.addClass(type);
                        if(type == 'AMEX'){
                            scope.$parent.cvv.Amex = true;
                            scope.$parent.cardMax = 17;
                            $('#cvc').attr('maxlength', 4);
                        }else{
                            scope.$parent.cvv.Amex = false;
                            $('#cvc').attr('maxlength', 3);
                            scope.$parent.cardMax = 19;
                        }


                });

                function GetCardType(number) {
                    // visa
                    var re = new RegExp("^4");
                    if (number.match(re) != null)
                        return "Visa";

                    // Mastercard
                    re = new RegExp("^5[1-5]");
                    if (number.match(re) != null)
                        return "Mastercard";

                    // AMEX
                    re = new RegExp("^3[47]");
                    if (number.match(re) != null)
                        return "AMEX";

                    // Discover
                    re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
                    if (number.match(re) != null)
                        return "Discover";

                    // Visa Electron
                    re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
                    if (number.match(re) != null)
                        return "Visa Electron";

                    return "";
                }
            }

        }
    }


        function ccPaymentLink (PaymentServices) {
        var directive = {
           link: link,
            scope: {
                'request': '=',
            },
            template: require('./paymentLink.html'),
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {

            //load payments

            scope.button_name = "PROCEED TO SECURE PAYMENT";
            if(angular.isDefined(attrs.name)){
                scope.button_name =attrs.name;
            }

            scope.test = false;
            if(scope.request.test) {
                scope.test = true;
            }

            scope.company = "";
            if(angular.isDefined(attrs.company)){
                scope.company ='BFRM';
            }

            scope.addRefund = function(){
                var receipts = scope.request.receipts;
                receipts.push(PaymentServices.getPaymentArray(scope.value));
                PaymentServices.set_receipt(scope.request.nid,receipts);
            }

            scope.busy = true;
            scope.value = 0;
            scope.active = false;
            scope.refund = false;
            attrs.$observe('amount',function (nval) {

                if(nval.length)
                    nval = parseFloat(nval);
                else
                    return;

                if(angular.isDefined(nval) && angular.isNumber(nval)){
                    scope.busy = true;

                    if(nval < 0){
                        scope.refund = true;
                        scope.value = nval;
                        scope.busy = false;
                    }
                    else {
                        scope.refund = false;
                        PaymentServices.getPaymentFingerPrint(nval).then(function (data) {
                            scope.busy = false;
                            scope.value = nval;
                            scope.active = true;
                            scope.button_name = "PROCEED TO SECURE PAYMENT";
                            scope.fingerPrint = data.fingerPrint;
                            scope.api_login_id = data.api_login_id;
                            scope.fp_timestamp = data.fp_timestamp;
                            scope.fp_sequence = data.fp_sequence;
                        });
                    }

                }


            });


        }





    }


    function ccPaymentBlock (PaymentServices,SweetAlert) {
        var directive = {
            link: link,
            scope: {
                'request': '=',
                'payment_type':'='
            },
            template: require('./paymentBlock.html'),
            restrict: 'A'
        };
        return directive;

        function link(scope) {

            scope.step1 = true;
            scope.step2 = false;
            scope.name = scope.request.name;
            scope.checkout_title = "Payment";

            if(angular.isDefined(scope.payment_type)){
                if(scope.payment_type == 'reservation') {
                    scope.charge_value = scope.request.reservation_rate.value;
                    scope.checkout_title = "Reservation"
                }
            }

            //PAYMENT FUNCTION
            scope.applyPayment  = function(){

                if (scope.checkout_form.$valid) {

                    var payment_info = {
                        "uid":  scope.request.uid.uid,
                        "nid": scope.request.nid,
                        "name": scope.name,
                        "phone": request.phone,
                        "email": request.email,
                        "zip_code": scope.billing_zip,
                        "credit_card": {
                            "card_number": scope.card_num,
                            "exp_date": scope.exp_month + "/" + scope.exp_year,
                            "card_code": scope.cvc
                        },
                            "amount": scope.charge_value,
                            "type": scope.payment_type
                    };

                    var paymentPromise = PaymentServices.applyPayment(payment_info);

                    paymentPromise.then(function(data) {

                        toastr.success("Success!", "You clicked the button!");


                    }, function(reason) {
                        SweetAlert.swal("Failed!", reason, "error");
                    });

                }


            }



        }
    }

})();
