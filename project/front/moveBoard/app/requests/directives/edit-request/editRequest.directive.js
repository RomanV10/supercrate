import './edit-request.styl';
import './mobile-timing-templates/mobile-request-timing.styl';

import {TYPES} from './edit-request-types.constants';

(function () {
	'use strict';

	angular
		.module('move.requests')
		.directive('ccEditRequestForm', ccEditRequestForm)
		.controller('erEditFormController', erEditFormController);

	function ccEditRequestForm() {
		var directive = {
			scope: {
				'request': '=request',
				'requests': '=requests',
				'homeestimates': '=',
				'userForHomeEstimate': '=',
				'editrequest': '=',
				'message': '=',
				'updateMessage': '&',
			},
			controller: 'erEditFormController',
			template: require('./editRequestForm.html'),
			restrict: 'A',
			link: ($scope) => {
				$scope.$emit('editRequestLoaded');
			}
		};

		return directive;
	}

	/* @ngInject */
	function erEditFormController($rootScope, requestModals, common, datacontext, EditRequestServices,
		CalculatorServices, InventoriesServices, SweetAlert, RequestServices, PaymentServices, CalcRequest,
		sharedRequestServices, PermissionsServices, $location, logger, StorageService, RequestHelperService,
		ParserServices, InitRequestFactory, FuelSurchargeService, CheckRequestService, $timeout, $state,
		apiService, moveBoardApi, CommercialCalc, Reminder, $filter, dialogs, $scope, erDeviceDetector, $q,
		serviceTypeService, EmailBuilderRequestService, requestObservableService, Raven, formInputsServices,
		valuationService, editRequestCalculatorService, grandTotalService, additionalServicesFactory,
	    laborService, debounce, openRequestService, erDateFormatParser, calculatePackingService) {

		if ($rootScope.inventoryRequestData) {
			$scope.request = $rootScope.inventoryRequestData.request;
			$scope.requests = $rootScope.inventoryRequestData.requests_day;
			$scope.homeestimates = $rootScope.inventoryRequestData.home_estimate;
			$scope.userForHomeEstimate = $rootScope.inventoryRequestData.user_for_estimate;

			delete $rootScope.inventoryRequestData;
		}

		if (!$scope.message) {
			$scope.message = {};
		}

		if (!$scope.editrequest) {
			$scope.editrequest = {};
		}

		$scope.valuationTypes = valuationService.VALUATION_TYPES;
		$scope.isMobile = erDeviceDetector.isMobile;
		$scope.isOpenInPortal = $location.path().includes('inhome-estimator');

		let isNotRecalculateRequestData = true;

		if (angular.isDefined($scope.request.initRequest)) {
			isNotRecalculateRequestData = $scope.request.initRequest;
		}

		let unLoadTimer;
		const TIME_ZONE_OFFSET = -new Date().getTimezoneOffset() * 60;
		const NAME_FULL_PACKING = 'Estimate Full Packing';
		const NAME_PARTIAL_PACKING = 'Estimate Partial Packing';

		const DATE_FORMAT_SHORT = 'MMM DD, YYYY'; // => 'Mar 13, 2018'
		const FULL_DATE_FORMAT = 'MMMM DD, YYYY'; // => 'March 13, 2018'
		const DATE_FORMAT_SLASH = 'MM/DD/YYYY'; // => '03/13/2018'
		const DATE_FORMAT_DEFIS = 'DD-MM-YYYY';// => '13-03-2018'
		const MOMENT_IS_SAME_LIMIT = 'day';
		const UPDATE_FUEL_SURCHARGE_TIMEOUT = 1000;

		const COMMERCIAL_QUOTE_INDEX = 9;
		const CONFIRMED_REQUEST = '3';

		$scope.checkMoveToThisState = checkMoveToThisState;

		$scope.newRequests = $scope.requests;
		$scope.invoice = {};
		$scope.Invoice = $scope.request.status.raw == 3;
		$scope.Math = window.Math;
		$scope.fieldData = datacontext.getFieldData();
		let commercialSetting = angular.fromJson($scope.fieldData['commercial_move_size_setting']);
		$scope.basicsettings = angular.fromJson($scope.fieldData.basicsettings);
		$scope.contract_page = angular.fromJson($scope.fieldData.contract_page);
		$scope.flatsettings = angular.fromJson($scope.fieldData.longdistance);
		$scope.calcSettings = angular.fromJson($scope.fieldData.calcsettings);
		$scope.quoteSettings = angular.fromJson($scope.fieldData.customized_text);
		var scheduleSettings = angular.fromJson($scope.fieldData.schedulesettings);
		var isUpdateReservation = false;
		var contractTypes = $scope.fieldData.enums.contract_type;
		let updateFuelSurchargeWithDebounce = debounce(UPDATE_FUEL_SURCHARGE_TIMEOUT, updateFuelSurcharge);
		const SERVICE_TYPES = $scope.fieldData.enums.request_service_types;
		$scope.isEmpty = _.isEmpty;
		$scope.showWarningBeforeSendEmail = showWarningBeforeSendEmail;

		$scope.truckCount = 0;
		$scope.services = serviceTypeService.getAllServiceTypes();

		const HUNDRED_PERCENT = 100,
			PICK_UP_PERCENT = $scope.contract_page.pickUpPercent,
			MIN_PAYROLL_HOURS = $scope.contract_page.minPayrollHours;

		InitRequestFactory.initRequestFields($scope.request, $scope.calcSettings.travelTime);

		$scope.serviceTypesOfSIT = {
			1: 'Move in',
			2: 'Move out'
		};

		$('.modal-dialog').draggable({handle: '.modal-header'});
		$scope.busy = false;
		$scope.tips = {
			value: 0
		};

		$scope.emails = [];

		setUpEmailParameters();

		$scope.tripStatusArr = [
			'',
			'',
			'Pending Trip',
			'In Route',
			'Delivered'
		];

		$scope.currentDay = moment(new Date($scope.request.date.value)).format('MM/DD/YYYY');
		$scope.dtlRequests = [];
		$scope.conflictRequests = [];
		$scope.parklotControl = {};
		$scope.min_hour_rate = parseFloat($scope.calcSettings.min_hours);
		var importedFlags = $rootScope.availableFlags;
		var withTravelTime = $scope.basicsettings.withTravelTime;
		$scope.travelTime = angular.isDefined($scope.request.request_all_data.travelTime) ? $scope.request.request_all_data.travelTime : $scope.calcSettings.travelTime;
		$scope.min_hours = parseFloat($scope.calcSettings.min_hours);
		$scope.getSettings = datacontext.getSettingsData();
		$scope.schedulesettings = angular.fromJson($scope.fieldData.schedulesettings);
		$scope.leadscoringsettings = angular.fromJson($scope.fieldData.leadscoringsettings);
		$scope.allowedStatuses = PermissionsServices.allowedStatuses();
		$scope.allowedServiceTypes = $scope.fieldData.field_lists.field_move_service_type;
		$scope.allowedMoveSize = angular.copy($scope.fieldData.field_lists.field_size_of_move);
		$scope.request.option_text = $scope.flatsettings.default_flat_option;
		$scope.editinvoice = $scope.$parent.$parent.editinvoice || {};
		$scope.request.extraDropoff = false;
		$scope.request.extraPickup = false;
		$scope.request.timeChangeManually = false;
		$scope.payrollDone = false;
		$scope.pickupPayrollDone = false;
		$scope.deliveryPayrollDone = false;
		$scope.range = common.range;
		$scope.request.extraServices = angular.fromJson($scope.request.extraServices);
		$scope.request.visibleWeight;
		$scope.editRequestinvoice = {
			invoice: {}
		};
		$scope.fullPacking = false;
		$scope.partialPacking = false;
		$scope.myselfPacking = true;
		$scope.oldTrucks = angular.copy($scope.request.trucks.old);

		const services = [1, 2, 3, 4, 6, 8];
		var minCATravelTime = $scope.basicsettings.minCATavelTime ? CalculatorServices.getRoundedTime($scope.basicsettings.minCATavelTime / 60) : 0;

		var baseRequestFuel = angular.copy($scope.request.request_all_data.surcharge_fuel);
		var baseInvoiceFuel = 0;
		let distancesABDuration = 0;

		var requestPacking = [];
		if (!_.isEmpty($scope.request.request_data)) {
			if (angular.isDefined($scope.request.request_data.value)) {
				if ($scope.request.request_data.value != '') {
					requestPacking = angular.fromJson($scope.request.request_data.value).packings || [];
				}
			}
		}

		function setisDoubleDriveTime() {
			$scope.isDoubleDriveTime = $scope.calcSettings.doubleDriveTime
				&& $scope.request.field_double_travel_time
				&& services.indexOf(parseInt($scope.request.service_type.raw)) >= 0;
		}

		checkCommercialSetting();
		setisDoubleDriveTime();

		var indexService = '';

		function setUpPackingServices() {
			if (!_.isEmpty($scope.request.request_data)) {
				if (angular.isDefined($scope.request.request_data.value)) {
					if ($scope.request.request_data.value != '') {
						requestPacking = angular.fromJson($scope.request.request_data.value).packings || [];
					}
				}
			}
			var partialPackingIndex = _.findIndex(requestPacking, {name: NAME_PARTIAL_PACKING});
			var fullPackingIndex = _.findIndex(requestPacking, {name: NAME_FULL_PACKING});
			$scope.myselfPacking = true;
			$scope.partialPacking = false;
			$scope.fullPacking = false;

			if (partialPackingIndex >= 0) {
				$scope.partialPacking = true;
				$scope.myselfPacking = false;
				$scope.fullPacking = false;
				indexService = partialPackingIndex;
			}

			if (fullPackingIndex >= 0) {
				$scope.fullPacking = true;
				$scope.myselfPacking = false;
				$scope.partialPacking = false;
				indexService = fullPackingIndex;
			}
		}

		function setUpEmailParameters() {
			$scope.emailParameters = EmailBuilderRequestService.getEmailParameters($scope.request);
			$scope.request.emailParameters = $scope.emailParameters;
		}

		setUpPackingServices();

		$scope.messageInvoice = {};
		$scope.request.sendingTemplates = [];
		$scope.packing = {};
		$scope.packing.labor = false;

		if (angular.isUndefined($scope.basicsettings.packing_settings)) {
			let packing_settings = {
				packingAccount: false,
				packingLabor: false,
				packing: {
					LM: {
						full: 0.5,
						partial: 50
					},
					LD: {
						full: 0.7,
						partial: 44
					}
				}
			};
			$scope.basicsettings.packing_settings = packing_settings;
		} else {
			if (angular.isDefined($scope.basicsettings.packing_settings.packingLabor)) {
				$scope.packing.labor = $scope.basicsettings.packing_settings.packingLabor;
			}
		}

		$scope.weightTypeChange = weightTypeChange;
		$scope.removeExtra = removeExtra;
		$scope.removeInvoiceExtra = removeInvoiceExtra;
		$scope.getTrucksCount = getTrucksCount;
		$scope.updateRate = updateRate;
		$scope.validateTrucks = validateTrucks;
		$scope.UpdateRequestInvoice = UpdateRequestInvoice;
		$scope.changeActualTime = changeActualTime;
		$scope.changeTypeOfServiceInvoice = changeTypeOfServiceInvoice;
		$scope.changeRequestField = changeRequestField;
		$scope.grandTotalFunc = grandTotalFunc;
		$scope.longDistanceQuoteInvoiceChange = longDistanceQuoteInvoiceChange;
		$scope.longDistanceQuoteRequestChange = longDistanceQuoteRequestChange;
		$scope.flatrateQuoteInvoiceChange = CalcRequest.flatrateQuoteInvoiceChange;
		$scope.updateQuote = updateQuote;
		$scope.updateAddPackingService = updateAddPackingService;
		$scope.saveRequestAllData = saveRequestAllData;
		$scope.changeFlatRateLocalMove = changeFlatRateLocalMove;
		$scope.changeSalesClosingTab = changeSalesClosingTab;
		$scope.openTrip = openTrip;
		$scope.openBindingRequest = openBindingRequest;
		$scope.collectRemindersOptions = collectRemindersOptions;
		$scope.openReminderBox = openReminderBox;
		$scope.sendNotificationAfterSaveChanges = sendNotificationAfterSaveChanges;
		$scope.updateLDTooltip = updateLDTooltip;
		$scope.changeReservationManually = changeReservationManually;
		$scope.getBaseGrandTotalLocalMove = getBaseGrandTotalLocalMove;

		var Payroll_Done = 'Payroll Done';
		var pickupPayrollDone = 'Pickup Payroll Done';
		var deliveryPayrollDone = 'Delivery Payroll Done';

		if ($scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[Payroll_Done])) {
			$scope.payrollDone = true;
		}
		if ($scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[pickupPayrollDone])) {
			$scope.pickupPayrollDone = true;
		}
		if ($scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[deliveryPayrollDone])) {
			$scope.deliveryPayrollDone = true;
		}

		if ($scope.request.field_moving_from.thoroughfare == 'Enter Address' || !$scope.request.field_moving_from.thoroughfare) {
			$scope.request.field_moving_from.thoroughfare = '';
		}
		if ($scope.request.field_moving_to.thoroughfare == 'Enter Address' || !$scope.request.field_moving_to.thoroughfare) {
			$scope.request.field_moving_to.thoroughfare = '';
		}

		//Check if Job is Done
		var Job_Completed = 'Completed';
		var pickupJobCompleted = 'Pickup Completed';
		var deliveryJobCompleted = 'Delivery Completed';
		var TeamAssigned = 'teamAssigned';
		$scope.states = {
			salesState: false,
			invoiceState: true
		};

		$rootScope.$broadcast('isClosing', $scope.states.invoiceState);

		$scope.contractDiscountType = {
			'yelp': 'Yelp Coupon',
			'website': 'Website Coupon',
			'manager': 'Manager'
		};
		$scope.ifStorage = $scope.request.service_type.raw == 2 || $scope.request.service_type.raw == 6;

		$scope.invoceState = true;
		$scope.Job_Completed = $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[Job_Completed]);
		$scope.pickupCompleted = $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[pickupJobCompleted]);
		$scope.deliveryCompleted = $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[deliveryJobCompleted]);
		$scope.TeamAssigned = $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[TeamAssigned]);

		if ($scope.request.ld_job.trip_id) {
			changeSalesClosingTab('closing');
		} else {
			if ($scope.Invoice) {
				changeSalesClosingTab('sales');
			}
		}

		initInvoice();
		collectRemindersOptions();

		//Check CLOSING ARRAY
		updateChange($scope.request);

		var date = new Date($scope.request.date.value);
		var calendar = $scope.fieldData.calendar;
		var day = date.getDate().toString().slice(-2);
		if (day < 10) {
			day = '0' + day;
		}

		var month = (date.getMonth() + 1).toString().slice(-2);
		if (month < 10) {
			month = '0' + month;
		}

		var year = date.getFullYear();
		var dates = year + '-' + month + '-' + day;
		if (calendar[year] && $scope.fieldData.calendartype[calendar[year][dates]] == 'Block this day') {
			SweetAlert.swal({title: 'Error!', text: 'This day is blocked', type: 'error'});
		}

		function initInvoice(updateInvoice) {
			if ($scope.Invoice || $scope.request.status.raw == 2 && $scope.recervation_received) {
				InitRequestFactory.initInvoiceRequest($scope.request, $scope.invoice, updateInvoice);
				baseInvoiceFuel = angular.copy($scope.invoice.request_all_data.surcharge_fuel);

				if ($scope.request.request_all_data.invoice.discount) {
					$scope.contract_discount = $scope.request.request_all_data.invoice.discount;
					$scope.contract_discountType = $scope.request.request_all_data.invoice.discountType;
				}

				if (angular.isDefined($scope.request.request_all_data.req_tips)) {
					$scope.tips.value = $scope.request.request_all_data.req_tips;
				}

				if (_.isUndefined($scope.request.request_all_data.invoice.work_time)
					&& angular.isDefined($scope.request.contract_info.laborHours)
					&& !_.isEmpty($scope.request.contract_info)) {

					$scope.tips.value = $scope.request.contract_info.tips_total;
					$scope.contract_discount = $scope.request.contract_info.discount;
					$scope.contract_discountType = $scope.request.contract_info.discountType;
				}

				if (angular.isDefined($scope.invoice.service_type)) {
					$scope.serviceTypeInvoice = $scope.invoice.service_type.raw == '5' || $scope.invoice.service_type.raw == '7';
				}
				if ($scope.invoice.maximum_time) {
					$scope.total_time = common.decToTime(Number($scope.invoice.maximum_time.raw) + Number($scope.travelTime ? $scope.invoice.travel_time.raw : 0));
				}
				if (!$scope.invoice.closing_weight || updateInvoice) {
					initInvoiceClosingWeight(!updateInvoice);
				}

				if ($scope.request.service_type.raw == 7) {
					longDistanceQuoteInvoiceChange();
				}

				if (_.get($scope, 'invoice.ddate.value')) {
					let formatDdate = erDateFormatParser.getDeliveryDateFormat($scope.invoice.ddate.value);
					$scope.deliveryDateInputInvoice = $.datepicker.formatDate('MM d, yy', new Date(moment($scope.invoice.ddate.value, formatDdate)), {});
				} else {
					let formatDdate = erDateFormatParser.getDeliveryDateFormat($scope.request.ddate.value);
					$scope.deliveryDateInputInvoice = $.datepicker.formatDate('MM d, yy', new Date(moment($scope.request.ddate.value, formatDdate)), {});
				}

				if (updateInvoice) {
					changeTypeOfServiceInvoice();
				} else {
					$scope.flatrateInvoice = $scope.invoice.service_type.raw == 5;
					$scope.longDistanceInvoice = $scope.invoice.service_type.raw == 7;
					$scope.ifStorage = $scope.invoice.service_type.raw == 2 || $scope.invoice.service_type.raw == 6;
				}
			} else {
				changeSalesClosingTab('sales');
			}
		}

		if ($scope.request.request_all_data && (angular.isUndefined($scope.request.contract_info) || _.isEmpty($scope.request.contract_info))) {
			if ($scope.request.request_all_data.couponData) {
				$scope.contract_discount = $scope.request.request_all_data.couponData.discount;
				$scope.contract_discountType = $scope.request.request_all_data.couponData.discountType;
			}
		}

		var dispatchStr = 'dispatch';
		var urlName = $location.path();
		var res = urlName.search(dispatchStr);

		if (res >= 0 && $scope.Invoice) {
			changeSalesClosingTab('closing');
		}

		if (angular.isDefined($scope.invoice.service_type)) {
			$scope.invoice_service_type = serviceTypeService.getCurrentServiceTypeName($scope.invoice);

			if ($scope.invoice.service_type.raw == 2) {
				$scope.ifStorage = true;
			}
		}

		//Check Discounts
		if ($scope.request.status.raw != 3 && angular.isDefined($scope.request.request_all_data.add_rate_discount_expiration)) {
			var now = moment().unix();
			var then = moment($scope.request.request_all_data.add_rate_discount_expiration).unix();

			if (then < now) {
				$scope.extra_discount_expiration = true;
				$scope.request.request_all_data.add_rate_discount = 0;
			}
		}

		function calculateRatediscount() {
			$scope.rateDiscount = $scope.request.request_all_data.add_rate_discount;

			if (!_.isUndefined($scope.rateDiscount)
				&& !_.isNull($scope.rateDiscount)
				&& $scope.rateDiscount > 0) {

				$scope.request.rateDiscount = $scope.rateDiscount;

				if ($scope.Invoice
					&& !_.isEmpty($scope.invoice)
					|| _.isUndefined($scope.invoice.rateDiscount)) {

					$scope.invoice.rateDiscount = $scope.rateDiscount;
				}
			} else {
				$scope.request.rateDiscount = 0;
				$scope.invoice.rateDiscount = 0;
			}
		}

		calculateRatediscount();

		//DISCOUNT SETTINGS
		$scope.localdiscount = parseInt($scope.request.request_all_data.localdiscount);

		if (angular.isUndefined($scope.invoice.request_all_data)) {
			$scope.invoice.request_all_data = {};
			$scope.invoice.request_all_data.localdiscount = 0;
			$scope.localdiscountInvoice = 0;
		} else {
			if (angular.isDefined($scope.invoice.request_all_data.localdiscount)) {
				$scope.localdiscountInvoice = parseInt($scope.invoice.request_all_data.localdiscount);
			} else {
				$scope.invoice.request_all_data.localdiscount = 0;
				$scope.localdiscountInvoice = 0;
			}
		}

		if ($scope.request.field_cubic_feet.value == '0') {
			$scope.request.pounds = true;
		}

		$scope.recervation_received = $scope.request.field_reservation_received.value;
		$scope.request.test = $scope.basicsettings.sandboxPayment;

		$scope.parklotControl.updateWorkTime = updateWorkTime;


		function isFlatRateReq() {
			var pickupDate = moment($scope.request.date.value).format('MM/DD/YYYY');
			var today = moment($scope.currentDay).format('MM/DD/YYYY');

			return $scope.request.service_type.raw == 5 && pickupDate != today; // diff time
		}


		$scope.cancelReservation = function () {
			SweetAlert.swal({
				title: 'Are you sure you want to cancel the reservation?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, let\'s do it!',
				cancelButtonText: 'No, cancel pls!',
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					var editrequest = {field_reservation_received: '0'};
					RequestServices.updateRequest($scope.request.nid, editrequest);
					$scope.recervation_received = false;
				}
			});
		};

		$scope.removeMessage = removeMessage;

		function removeMessage(field) {
			if (angular.isDefined($scope.editrequest[field.field])) {
				delete $scope.editrequest[field.field];
				delete $scope.message[field.field];
			}
		}

		$scope.confirmedStats = 0;
		$scope.unconfirmedStats = 0;

		function initRequestWeight(initRequest) {
			let inventoryNotEmpty = _.get($scope.request, 'inventory.inventory_list.length', false);
			let weightTypeIsEmpty = $scope.request.field_useweighttype.old == null && !$scope.request.field_useweighttype.value;

			if (weightTypeIsEmpty) {
				if (inventoryNotEmpty) {
					$scope.request.field_useweighttype.value = 2;
				} else {
					$scope.request.field_useweighttype.value = 1;
				}
			}

			let weightType = parseInt($scope.request.field_useweighttype.value);

			if ($scope.request.move_size.raw != 11) {
				$scope.request.total_weight = CalculatorServices.getRequestCubicFeet($scope.request);
			} else {
				calculateCommercial($scope.request, $scope.calcSettings, initRequest);
			}

			switch (weightType) {
			case 1:
				$scope.request.visibleWeight = $scope.request.total_weight.weight;
				break;
			case 2:
				if (_.get($scope, 'request.inventory.inventory_list.length')) {
					$scope.request.visibleWeight = $scope.request.inventory_weight.cfs;
				}

				break;
			case 3:
				$scope.request.visibleWeight = $scope.request.custom_weight.value;
				break;
			}

			if ($scope.request.move_size.raw == 11 && $scope.request.status.raw != 3) {
				calculateTime();
			} else {
				calculateTime(initRequest);
			}

			let customWeight = $scope.request.visibleWeight;

			if (!$scope.request.closing_weight) {
				_.set($scope, 'request.closing_weight.old', customWeight);
			}

			$scope.request.closing_weight.value = customWeight;

			if ($scope.Invoice) {
				initInvoiceClosingWeight(initRequest);

				$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
				$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));

				if (!_.isUndefined(_.get($scope.invoice, 'request_all_data.valuation.selected.valuation_charge')) && !initRequest) {
					valuationService.reCalculateValuation($scope.request, $scope.invoice, 'invoice');
				}
			} else if (_.get($scope, 'request.request_all_data.valuation.selected.valuation_charge', false) && !initRequest) {
				valuationService.reCalculateValuation($scope.request, $scope.invoice, 'request');
			}

			if ($scope.longDistance) {
				updateLDTooltip();
			}
		}

		function initInvoiceClosingWeight(initRequest = false) {
			if ($scope.request.service_type.raw != 7 && _.get($scope, 'invoice.field_useweighttype.value', 1) == 2) {
				let closingWeight = InventoriesServices.calculateClosingWeight($scope.request);
				_.set($scope, 'invoice.closing_weight.value', closingWeight);
			} else {
				if (initRequest) {
					$scope.invoice.closing_weight = $scope.invoice.closing_weight || angular.copy($scope.request.closing_weight);
				} else {
					$scope.invoice.closing_weight = angular.copy($scope.request.closing_weight);
				}
			}
		}


		function updateLDTooltip() {
			$rootScope.$broadcast('updateTooltip');
		}

		function checkForMinWeightLdOnly(weight, useOldMinWeight) {
			weight = +weight;
			let result = weight;
			let isOldMinWeight = useOldMinWeight && angular.isDefined($scope.request.request_all_data.min_weight_old);
			let minWeight = isOldMinWeight ? +$scope.request.request_all_data.min_weight_old : +$scope.request.request_all_data.min_weight;

			if ($scope.longDistance && minWeight > weight) {
				result = minWeight;
			}

			return parseFloat(result);
		}

		function updateInvoiceClosingWeight() {
			if ($scope.Invoice) {
				$scope.invoice.closing_weight.value = $scope.request.closing_weight.value;
			}
		}

		function updateRequestWeight(minWeightNotChanged, _oldWeight) {
			let oldWeight = _oldWeight || $scope.request.visibleWeight;
			initRequestWeight(minWeightNotChanged);
			let weightChanged = checkForMinWeightLdOnly(oldWeight, true) != checkForMinWeightLdOnly($scope.request.visibleWeight);
			if (weightChanged) {
				updateInvoiceClosingWeight();
				if ($scope.longDistance) {
					updateLongDistanceRate();
					$scope.longDistanceQuote = CalculatorServices.getLongDistanceQuote($scope.request);

					if ($scope.Invoice) {
						$scope.invoice.inventory.inventory_list = angular.copy($scope.request.inventory.inventory_list);
						$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
						$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));
						if ($scope.invoice.request_all_data.valuation && !_.isUndefined($scope.invoice.request_all_data.valuation.selected.valuation_charge)) {
							valuationService.reCalculateValuation($scope.request, $scope.invoice, 'invoice');
						}
					} else {
						if ( $scope.request.request_all_data.valuation && !_.isUndefined($scope.request.request_all_data.valuation.selected.valuation_charge)) {
							valuationService.reCalculateValuation($scope.request, $scope.invoice, 'request');
						}
					}
					caclulateLongDistanceQuoteUp();
				}
				if ($scope.Invoice) {
					if ($scope.invoice.request_all_data.valuation && !_.isUndefined($scope.invoice.request_all_data.valuation.selected.valuation_charge)) {
						valuationService.reCalculateValuation($scope.request, $scope.invoice, 'invoice');
					}
				} else {
					if ( $scope.request.request_all_data.valuation && !_.isUndefined($scope.request.request_all_data.valuation.selected.valuation_charge)) {
						valuationService.reCalculateValuation($scope.request, $scope.invoice, 'request');
					}
				}

				updateFuelSurchargeRelatedToQuote();
			}
		}

		$scope.$on('updateWeight', updateRequestWeight);


		// Only init data for view. Not update.
		function initData(isFirstTime) {
			initServiceType();

			if ($scope.request.move_size.raw != 11) {
				$scope.request.total_weight = CalculatorServices.getRequestCubicFeet($scope.request);
			} else {
				calculateCommercial($scope.request, $scope.calcSettings, isFirstTime);
			}

			$scope.request.inventory_weight = InventoriesServices.calculateTotals($scope.request.inventory.inventory_list);

			//Minimum Weight
			if ($scope.longDistance) {
				CalculatorServices.updateLdMinWeightSettings($scope.request);
			}

			initRequestWeight(isNotRecalculateRequestData);

			if ($scope.request.schedule_delivery_date) {
				if ($scope.request.schedule_delivery_date.value) {
					var formatDate = $scope.request.schedule_delivery_date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
					$scope.scheduleDeliveryDate = moment($scope.request.schedule_delivery_date.value, formatDate).format(FULL_DATE_FORMAT);
				}
			} else {
				$scope.scheduleDeliveryDate = $.datepicker.formatDate('MM d, yy', new Date($scope.request.schedule_delivery_date.value), {});
			}

			if ($scope.request.date) {
				var formatDate = $scope.request.date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
				$scope.moveDateInput = moment($scope.request.date.value, formatDate).format(FULL_DATE_FORMAT);
			} else {
				$scope.moveDateInput = $.datepicker.formatDate('MM d, yy', new Date($scope.request.date.value), {});
			}

			if (!$scope.request.start_time1.old) {
				$scope.request.start_time1.old = angular.copy($scope.request.start_time1.value);
			}

			if (!$scope.request.start_time2.old) {
				$scope.request.start_time2.old = angular.copy($scope.request.start_time2.value);
			}

			if ($scope.request.date) {
				var formatDate = $scope.request.date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
				$scope.moveDateInput = moment($scope.request.date.value, formatDate).format(FULL_DATE_FORMAT);
			} else {
				$scope.moveDateInput = $.datepicker.formatDate('MM d, yy', new Date($scope.request.date.value), {});
			}

			if ($scope.invoice.date) {
				let formatDate = $scope.invoice.date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
				$scope.moveDateInputInvoice = angular.copy(moment($scope.invoice.date.value, formatDate).format(DATE_FORMAT_SHORT));
			} else {
				let formatDate = $scope.request.date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
				$scope.moveDateInputInvoice = $.datepicker.formatDate('MM d, yy', new Date(moment($scope.request.date.value, formatDate)), {});
			}

			$scope.moveDate = $.datepicker.formatDate('MM d, yy', new Date($scope.request.date.value), {});
			$scope.curDate = moment($scope.request.date.value, $scope.request.date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD').format('dddd, MMMM DD, YYYY');
			let date = new Date($scope.request.date.value);
			reinitRarklot(date);

			if ($scope.request.ddate) {
				if (!$scope.request.ddate.value.length) {
					$scope.deliveryDateInput = '';
				} else {
					let formatDate = $scope.request.ddate.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
					$scope.deliveryDateInput = angular.copy(moment($scope.request.ddate.value, formatDate).format(DATE_FORMAT_SHORT));
				}
			} else {
				let formatDate = $scope.request.ddate.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
				$scope.deliveryDateInput = $.datepicker.formatDate('MM d, yy', new Date(moment($scope.request.ddate.value, formatDate)), {});
			}

			if ($scope.request.ddate_second) {
				if (!$scope.request.ddate_second.value.length) {
					$scope.deliveryDateSecondInput = $scope.request.ddate_second.value;
				} else {
					$scope.deliveryDateSecondInput = angular.copy(moment($scope.request.ddate_second.value, 'MM/DD/YYYY').format(DATE_FORMAT_SHORT));
				}
			} else {
				$scope.deliveryDateSecondInput = $.datepicker.formatDate('MM d, yy', new Date(moment($scope.request.ddate_second.value, 'MM/DD/YYYY')), {});
			}

			if ($scope.invoice.ddate) {
				let formatDate = erDateFormatParser.getDeliveryDateFormat($scope.invoice.ddate.value);
				$scope.deliveryDateInputInvoice = $.datepicker.formatDate('MM d, yy', new Date(moment($scope.invoice.ddate.value, formatDate)), {});
			} else {
				let formatDate = erDateFormatParser.getDeliveryDateFormat($scope.request.ddate.value);
				$scope.deliveryDateInputInvoice = $.datepicker.formatDate('MM d, yy', new Date(moment($scope.request.ddate.value, formatDate)), {});
			}

			if (angular.isDefined($scope.request.ddate)) {
				let formatDate = $scope.request.ddate.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
				$scope.moveDeliveryDate = $.datepicker.formatDate('M d, yy', new Date(moment($scope.request.ddate.value, formatDate)), {});
			}
			//EXTRA DROP OF OR PICKUP INIT
			if ($scope.request.field_extra_dropoff.postal_code != null) {
				if ($scope.request.field_extra_dropoff.postal_code.length == 5
					&& _.isUndefined($scope.editrequest.field_extra_dropoff)) {
					$scope.request.extraDropoff = true;
				}
			}

			if ($scope.request.field_extra_pickup.postal_code != null) {
				if ($scope.request.field_extra_pickup.postal_code.length == 5
					&& _.isUndefined($scope.editrequest.field_extra_pickup)) {
					$scope.request.extraPickup = true;
				}
			}

			//Moving and Storage and Overnight
			$scope.ifStorage = $scope.request.service_type.raw == 2 || $scope.request.service_type.raw == 6;

			if ($scope.request.service_type.raw == 5) {
				if (angular.isDefined($scope.request.flat_rate_quote)
					&& angular.isDefined($scope.request.flat_rate_quote.value)
					&& $scope.request.flat_rate_quote.value == null) {
					$scope.request.flat_rate_quote.value = 0;
				}
			}

			if ($scope.request.service_type.raw == 7) {
				if ($scope.request.field_moving_to.premise == null || $scope.request.field_moving_to.premise == '') {
					GetAreaCode();
				}

				//default Weight Type
				if ($scope.request.field_cubic_feet.value == null) {
					if ($scope.flatsettings.default_weight == 1) {
						$scope.request.field_cubic_feet.value = '1';

						if (_.isNull($scope.request.field_long_distance_rate.value)
							|| _.isUndefined($scope.request.field_long_distance_rate.value)) {

							$scope.request.field_long_distance_rate.value = $scope.longDistanceRate || 0;
							updateMessage($scope.request.field_long_distance_rate);
						}

						updateMessage($scope.request.field_cubic_feet);
					} else {
						$scope.request.field_cubic_feet.value = '0';

						if (_.isNull($scope.request.field_long_distance_rate.value)
							|| _.isUndefined($scope.request.field_long_distance_rate.value)) {

							$scope.request.field_long_distance_rate.value = $scope.longDistanceRate || 0;
							updateMessage($scope.request.field_long_distance_rate);
						}

						updateMessage($scope.request.field_cubic_feet);
					}
				}

				$scope.longDistanceRate = CalculatorServices.getLongDistanceRate($scope.request);

				if (_.isNull($scope.request.field_long_distance_rate.value)
					|| _.isUndefined($scope.request.field_long_distance_rate.value)) {

					$scope.request.field_long_distance_rate.value = $scope.longDistanceRate || 0;
				}

				$scope.longDistanceQuote = CalculatorServices.getLongDistanceQuote($scope.request);

				let linfo = CalculatorServices.getLongDistanceInfo($scope.invoice);

				caclulateLongDistanceQuoteUp();

				if (!isFirstTime) {
					recalculateLongDistanceExtraServices();
				}

				//Delivery Window
				if (angular.isDefined($scope.request.inventory.move_details) && $scope.request.inventory.move_details != null) {
					setLastDeliveryDay();
					//delivery days
					if (angular.isUndefined($scope.request.inventory.move_details.delivery_days)) {
						$scope.request.inventory.move_details.delivery_days = linfo.days;
						$scope.oldDeliveryDaysValue = $scope.request.inventory.move_details.delivery_days;
					}
				}
				else {
					if (angular.isUndefined($scope.request.inventory.move_details)) {
						$scope.request.inventory.move_details = {};
						$scope.request.inventory.move_details.delivery_days = linfo.days;
					}
				}

			}

			if (angular.isUndefined($scope.request.ddate.old)) {
				$scope.request.ddate.old = angular.copy($scope.request.ddate.value);
			}


			if ($scope.ifStorage) {
				prepareMovingStorageRate();
			}

			if ($scope.request.service_type.raw == 7) {
				$scope.addMainPage = !!_.find($scope.contract_page.additionalContract, {mainContract: true});
			}
		}

		function recalculateLongDistanceExtraServices() {
			if ($scope.request.service_type.raw != 7 || !_.get($scope.request, 'field_usecalculator.value'))
				return;

			let weight = checkForMinWeightLdOnly($scope.request.visibleWeight);

			if (angular.isUndefined($scope.request.extraServices)) {
				$scope.request.extraServices = additionalServicesFactory.getLongDistanceExtra($scope.request, weight);
			}
			else {
				if (_.isString($scope.request.extraServices)) {
					$scope.request.extraServices = angular.fromJson($scope.request.extraServices);
				}

				$scope.request.extraServices = $scope.request.extraServices
					.filter(service => !service.autoService || service.fee_all_services)
					.concat(additionalServicesFactory.getLongDistanceExtra($scope.request, weight));
			}

			if ($scope.Invoice) {
				$scope.invoice.extraServices = angular.copy($scope.request.extraServices);
			}

			additionalServicesFactory.saveExtraServices($scope.request.nid, $scope.request.extraServices);
			saveOnlyRequestAllData();
		}

		if ($scope.ifStorage
			&& (!_.isUndefined($scope.request.storage_id) && !_.isEqual($scope.request.storage_id, 0)
				|| $scope.request.request_all_data.baseRequestNid)
			&& _.isUndefined($scope.pairedRequest)) {

			loadPairedRequest();
		}

		var isInValidRequest = !ParserServices.validateParserRequest($scope.request, $scope.editrequest);

		if (isInValidRequest) {
			$scope.isInvalidParsingRequest = true;
			$scope.isInvalidRequestDate = ParserServices.isInvalidFieldDate($scope.request);
			$scope.isInvalidStartTime = ParserServices.isInvalidFieldStartTime($scope.request);
			$scope.isInvalidMoveSize = ParserServices.isInvalidFieldMoveSize($scope.request);

			$scope.isInvalidFieldMovingFrom = ParserServices.isInvalidFieldMovingFrom($scope.request);

			if ($scope.isInvalidFieldMovingFrom) {
				$scope.request.type_from.raw = '';
			}

			$scope.isInvalidFieldMovingTo = ParserServices.isInvalidFieldMovingTo($scope.request);

			if ($scope.isInvalidFieldMovingTo) {
				$scope.request.type_to.raw = '';
			}

			$scope.isInvalidTypeTo = ParserServices.isInvalidTypeTo($scope.request);
			$scope.isInvalidTypeFrom = ParserServices.isInvalidTypeFrom($scope.request);

			$scope.$watchCollection('editrequest', function (newVal, oldVal) {
				if ($scope.editrequest.field_date) {
					$scope.isInvalidRequestDate = ParserServices.isInvalidEditFieldDate($scope.editrequest);
				}

				if ($scope.editrequest.field_moving_from) {
					$scope.isInvalidFieldMovingFrom = ParserServices.isInvalidFieldMovingFrom($scope.editrequest);
				}

				if ($scope.editrequest.field_moving_to) {
					$scope.isInvalidFieldMovingTo = ParserServices.isInvalidFieldMovingTo($scope.editrequest);
				}

				if (!_.isUndefined($scope.editrequest.field_type_of_entrance_to_)) {
					$scope.isInvalidTypeTo = ParserServices.isInvalidFieldTypeTo($scope.editrequest);
				}

				if (!_.isUndefined($scope.editrequest.field_type_of_entrance_from)) {
					$scope.isInvalidTypeFrom = ParserServices.isInvalidFieldTypeFrom($scope.editrequest);
				}

				if (!_.isUndefined($scope.editrequest.field_start_time)) {
					$scope.isInvalidStartTime = false;
				}

				if (!_.isEqual(newVal.field_size_of_move, oldVal.field_size_of_move)) {
					$scope.isInvalidMoveSize = false;
				}

				initData(true);
				calculateTime();
			});
		}

		function caclulateLongDistanceQuoteUp() { // QuoteUp == Promotion
			var longDistanceGrandTotal = calculateGrandTotalBalance('longdistance');
			var total = 0;
			total += $scope.longDistanceQuote;
			total += parseFloat(calculatePackingServices()) + parseFloat(calculateAddServices());

			if ($scope.request.request_all_data.valuation && $scope.request.request_all_data.valuation.selected.valuation_charge) {
				total += Number($scope.request.request_all_data.valuation.selected.valuation_charge);
			}

			if ($scope.request.request_all_data.surcharge_fuel) {
				total += $scope.request.request_all_data.surcharge_fuel;
			}

			var discount = $scope.request.request_all_data.add_money_discount + total * $scope.request.request_all_data.add_percent_discount / 100;
			longDistanceGrandTotal += discount;

			$scope.longDistanceQuoteUp = Math.round(longDistanceGrandTotal / (1 - $scope.localdiscount / 100));
		}

		function initServiceType() {
			$scope.flatrate = $scope.request.service_type.raw == 5;
			$scope.longDistance = $scope.request.service_type.raw == 7;

			$scope.service_type = serviceTypeService.getCurrentServiceTypeName($scope.request);
			if ($scope.request.service_type.raw == 2) {
				$scope.ifStorage = true;
			}
		}

		initData(true);
		if ($scope.request.move_size.raw == 11) {
			calculateTime(true);
		}

		$scope.toggleOpen = function (accordin) {
			accordin.open = !accordin.open;
		};

		$scope.DeliveryDay = function () {
			var date = $scope.request.ddate.value;
			$scope.parklotControl.reinitParklot(date);
		};

		$scope.PickupDay = function () {
			var date = $scope.request.date.value;
			$scope.parklotControl.reinitParklot(date);
		};

		$scope.parklotControl.reinitParklot = reinitRarklot;

		function reinitRarklot(date) {
			$scope.busy = true;
			$scope.curDate = moment(date).format('dddd, MMMM DD, YYYY');
			$scope.currentDay = moment(date).format('MM/DD/YYYY');

			RequestServices
				.getRequestParklot(date)
				.then(onSuccessGetParklot, onErrorResponse);
		}

		function onSuccessGetParklot({data}) {
			$scope.busy = false;
			$scope.newRequests = data;

			let currentRequestIndex = _.findKey(data, request => request.nid == $scope.request.nid);
			$scope.newRequests[currentRequestIndex] = $scope.request;

			$scope.$emit('setVarInBaseScope', {
				name: 'requests',
				value: angular.copy($scope.newRequests)
			});
			includeCurrentRequestInParklot();
			$scope.$broadcast('parklot.init', $scope.newRequests);
		}

		function includeCurrentRequestInParklot() {
			let requests = $scope.newRequests;
			let deliveryDate = moment($scope.curDate).format('YYYY-MM-DD');
			let pickUpDate = $scope.request.date.value.indexOf('-') != -1 ? $scope.request.date.value : moment($scope.request.date.value).format('YYYY-MM-DD');
			let isSamePickupDate = deliveryDate == pickUpDate;
			let requestIndex = _.findIndex(requests, {nid: $scope.request.nid});
			let isNotFoundCurrentRequest = requestIndex == -1;
			let isDeliveryFlatRate = CheckRequestService.isDeliveryFlatRate($scope.curDate, $scope.request);
			let isValidCurrentRequest = isNotFoundCurrentRequest && (isSamePickupDate || isDeliveryFlatRate);

			if (isValidCurrentRequest) {
				requests.push($scope.request);
			}

			// remove current request if not acceptable
			if (!isNotFoundCurrentRequest && !(isSamePickupDate || isDeliveryFlatRate)) {
				requests.splice(requestIndex, 1);
			}
		}

		function onErrorResponse(reason) {
			logger.error(reason, reason, 'Error');
		}

		$scope.parklotControl.updateStartTime = function (field, val) {
			if (field == 'field_actual_start_time') {
				$scope.request.start_time1.value = val;
			} else if (field == 'field_start_time_max') {
				$scope.request.start_time2.value = val;
			}

			if ($scope.updateTruck) {
				$scope.updateTruck();
			}
		};

		function updateWorkTime(field, val) {
			var fieldName = angular.copy(field);

			if (_.isObject(fieldName)) {
				fieldName = fieldName.field;
			}

			if (fieldName == 'field_minimum_move_time') {
				if ($scope.request.minimum_time.old != val) {
					$scope.request.minimum_time.raw = common.convertStingToIntTime(val);
					$scope.request.minimum_time.value = val;
					updateMessage($scope.request.minimum_time);
				} else {
					$scope.request.minimum_time.raw = common.convertStingToIntTime(val);
					$scope.request.minimum_time.value = val;
					removeMessage($scope.request.minimum_time);
				}
			} else if (fieldName == 'field_maximum_move_time') {
				if ($scope.request.maximum_time.old != val) {
					$scope.request.maximum_time.raw = common.convertStingToIntTime(val);
					$scope.request.maximum_time.value = val;
					updateMessage($scope.request.maximum_time);
				} else {
					$scope.request.maximum_time.raw = common.convertStingToIntTime(val);
					$scope.request.maximum_time.value = val;
					removeMessage($scope.request.maximum_time);
				}
			}

			if ($scope.updateTruck) {
				$scope.updateTruck();
			}
		}

		//UPDATE REQUEST
		$scope.UpdateRequest = _updateRequest;

		function _updateRequest() {
			//Validate Update
			var count = common.count($scope.editrequest);

			if (!CheckRequestService.isValidRequestZip($scope.request)) {
				SweetAlert.swal('Please enter the correct zip code');

				return false;
			}

			if (!count) {
				SweetAlert.swal('Nothing to Update!');

				return false;
			}

			if (!CheckRequestService.isValidRequestAddress($scope.request)) {
				SweetAlert.swal('Please Enter Address!');

				return false;
			}

			if (!CheckRequestService.isValidHomeEstimateTime($scope.request)) {
				SweetAlert.swal('Add Start Time Window For Home Estimate');
				return false;
			}

			if (!CheckRequestService.checkHomeEstimate($scope.request)) {
				return false;
			}

			var status = false;

			if ($scope.request.status.raw == 2 || $scope.Invoice) {
				status = true;

				if (angular.isDefined($scope.editrequest.field_approve)) {
					if ($scope.editrequest.field_approve != 2 || $scope.editrequest.field_approve != 3) {
						status = false;
					}
				}
			}

			if ($scope.editrequest.field_approve == 1 || $scope.editrequest.field_approve == 2 || $scope.editrequest.field_approve == 3) {
				status = true;
			}

			var today = moment($scope.curDate).format('MM/DD/YYYY');
			var flatRate = $scope.request.service_type.raw == 5 && $scope.request.date.value != today;

			if ($scope.request.trucks.raw.length == 0 && status) {
				SweetAlert.swal('Please Choose Truck!');
				return false;
			}

			if ($scope.request.trucks.raw.length && status && $scope.request.field_usecalculator.value) {
				//check if we have truck
				var trucks = getTrucksCount();

				if (_.isUndefined($scope.calcResults)) {
					calculateTime();
				}

				var calcTrucks = $scope.calcResults.trucks;
				if (trucks < calcTrucks) {
					//show errow based on calculator you can not choose more trucks
					SweetAlert.swal('You need more trucks for this job!');
					return false;
				}
			}

			//Check if confirmed and if it overlay blue
			if ($scope.Invoice) {
				checkConflictRequestOnUpdate();
			}

			var messageCount = 0;

			if (!_.isArray($scope.message)
				&& _.isObject($scope.message)) {

				_.each($scope.message, function (item, prop) {

					if (!_.isUndefined(item.oldValue)
						&& !_.isUndefined(item.newValue)
						&& item.oldValue == item.newValue) {
						messageCount += 1;

						delete $scope.message[prop];
					}
				});
			}

			if (_.isEmpty($scope.message)
				&& messageCount == count) {

				angular.forEach($scope.editrequest, (value, key) => {
					delete $scope.editrequest[key];
				});

				SweetAlert.swal('Nothing to Update!');
				return false;
			}

			requestModals.confirmUpdate($scope.request, $scope.message, $scope.editrequest, $scope.invoice);

			return true;
		}

		function checkConflictRequestOnUpdate() {
			if ($scope.editrequest.field_list_truck) {
				getConflictRequest($scope.editrequest.field_list_truck);
			} else {
				getConflictRequest($scope.request.trucks.raw);
			}

			showContlictRequestMessage();
		}

		function checkConflictRequestWithoutUpdate() {
			getConflictRequest($scope.request.trucks.raw);

			showContlictRequestMessage();
		}

		function showContlictRequestMessage() {
			//check if we have conflict request
			if ($scope.conflictRequests.length) {
				var jobString = '';

				_.each($scope.conflictRequests, function (request, id) {
					if (id != $scope.conflictRequests.length - 1) {
						jobString += request.nid + ',';
					} else {
						jobString += request.nid;
					}
				});

				SweetAlert.swal('Attention! Request ' + jobString + ' will be sent to pending status!', '', 'error');
			}
		}


		function getConflictRequest(field_list_truck) {
			$scope.conflictRequests = [];

			if (angular.isDefined(field_list_truck)) {
				_.each(field_list_truck, function (tid, id) {
					var conflict = false;
					if (withTravelTime) {
						var work_time = common.convertStingToIntTime($scope.request.maximum_time.value) * 2 + $scope.request.travel_time.raw * 2;
					} else {
						var work_time = common.convertStingToIntTime($scope.request.maximum_time.value) * 2;
					}

					var nsT1 = common.convertTime($scope.request.start_time1.value);

					if ($scope.unconparklot) {
						_.each($scope.unconparklot[tid], function (request, id2) {
							var conflict = false;
							for (var i = nsT1; i < nsT1 + work_time; i++) {
								// FIND JOB OF THE SAME TRUCK IF THEY INTERCROS PUT UNCINFIRMED TO CONFLICT.
								var maximum_time = request.maximum_time.raw * 2;
								var sT1 = common.convertTime(request.start_time1.value);
								if (withTravelTime) {
									maximum_time = maximum_time + ($scope.travelTime ? request.travel_time * 2 : 0);
								}

								for (var k = sT1; k <= sT1 + maximum_time; k++) {
									if (k == i) {
										conflict = true;
									}
								}
							}


							if (conflict && _.findIndex($scope.conflictRequests, {nid: request.nid}) == -1) {
								$scope.conflictRequests.push(request);
							}

						});
					}
				});
			}
		}

		function setLastDeliveryDay() {
			if (angular.isDefined($scope.request.inventory.move_details.delivery)
				&& $scope.request.inventory.move_details.delivery.length) {

				$scope.lastDeliveryDay = CalculatorServices.getLongDistanceWindow($scope.request, $scope.request.inventory.move_details.delivery, $scope.request.inventory.move_details.delivery_days);

				if ($scope.lastDeliveryDay) {
					$scope.lastDeliveryDay = moment($scope.lastDeliveryDay).format(FULL_DATE_FORMAT);
				}
			} else {
				$scope.lastDeliveryDay = 0;
			}
		}

		$scope.objectKeys = function (obj) {
			return Object.keys(obj);
		};

		//Caclulate Quote Functionality
		function updateQuote(initRequest) {
			sharedRequestServices.calculateQuote($scope.request);

			if (!$scope.request.sendingTemplates) {
				$scope.request.sendingTemplates = [];
			}

			$scope.smallJob = $scope.request.quote.min == $scope.request.quote.max;

			updateFuelSurchargeRelatedToQuote(initRequest);
		}

		function increaseReservationPrice() {
			editRequestCalculatorService.increaseReservationPrice($scope.request)
				.then(() => {
					let time = scheduleSettings.whenReservationRateIncrease;

					SweetAlert.swal(`Due to less than ${time} hours move date rule, the reservation price was increased`, '', 'info');

					saveOnlyRequestAllData();
				});
		}

		increaseReservationPrice();

		$scope.addTruck = function (tid, flatRate) {
			if (!flatRate) {
				$scope.request.trucks.raw.push(tid);
				if (!$scope.request.field_usecalculator.value) {
					$scope.request.truckCount = $scope.request.trucks.raw.length || 1;
				}
			} else {
				$scope.request.delivery_trucks.raw.push(tid);
			}

			validateTrucks(flatRate);
		};

		$scope.removeTruck = function (tid) {
			if (!$scope.flatrate || !isFlatRateReq()) {
				var trucks = $scope.request.trucks.raw;
				var index = trucks.indexOf(tid);
				$scope.request.trucks.raw.splice(index, 1);
				if (!$scope.request.field_usecalculator.value) {
					$scope.request.truckCount = $scope.request.trucks.raw.length || 1;
				}
			} else {
				var trucks = angular.copy($scope.request.delivery_trucks.raw);
				$scope.request.delivery_trucks.old = trucks;
				var index = trucks.indexOf(tid);
				$scope.request.delivery_trucks.raw.splice(index, 1);
			}

			validateTrucks(isFlatRateReq());
		};

		function validateTrucks(flatrate) {
			if (!$scope.flatrate) {
				updateMessage($scope.request.trucks);
			} else {
				if (!flatrate) {
					$scope.editrequest[$scope.request.trucks.field] = angular.copy($scope.request.trucks.raw);
					$scope.message[$scope.request.trucks.field] = {
						label: 'custom',
						oldValue: '',
						newValue: 'Pickup Truck was set.',
					};
				} else {
					$scope.editrequest[$scope.request.delivery_trucks.field] = angular.copy($scope.request.delivery_trucks.raw);
					$scope.message[$scope.request.delivery_trucks.field] = {
						label: 'custom',
						oldValue: '',
						newValue: 'Delivery Truck was set.',
					};
				}
			}
		}

		$scope.showPopup = function (request) {
			$scope.parklotControl.initPopup(request);
		};

		$scope.openCalendar = function () {
			angular.element('.parklot-calendar .ui-datepicker-trigger').click();
		};

		$scope.OpenModal = function (nid) {
			$scope.busy = true;

			EditRequestServices
				.requestEditModal(nid)
				.then(function () {

				})
				.finally(function () {
					$scope.busy = false;
				});
		};

		$scope.openGroupModal = function () {
			EditRequestServices
				.groupRequestModal($scope.request, $scope.pairedRequest);
		};

		$scope.unbindPackingDay = function () {
			let unbindData;

			if ($scope.request.service_type.raw == 8) {
				unbindData = {
					parent_nid: $scope.request.request_all_data.packing_request_id,
					child_nid: $scope.request.nid,
				};
			} else {
				unbindData = {
					parent_nid: $scope.request.nid,
					child_nid: $scope.request.request_all_data.packing_request_id,
				};
			}

			EditRequestServices.unbindPackingDay(unbindData).then(() => {
				let msg = [{
					text: `Packing day request was successfully unbind. Parent request: ${unbindData.parent_nid}`,
				}];

				RequestServices.sendLogs(msg, 'Request was updated', unbindData.parent_nid, 'MOVEREQUEST');
				RequestServices.sendLogs(msg, 'Request was updated', unbindData.child_nid, 'MOVEREQUEST');

				$scope.request.request_all_data.packing_request_id = null;
				toastr.success('Request was successfully unbind');
			}, () => {
				toastr.error('Please, check your API', 'Request was not unbinded');
			});
		};

		$scope.openRelinkModal = function () {
			EditRequestServices
				.groupRequestModal($scope.request, $scope.pairedRequest);
		};

		function updateRequestWorkData(minWorkTime, maxWorkTime, moversCount) {
			if ($scope.request.minimum_time.old != common.decToTime(minWorkTime)) {
				$scope.request.minimum_time.raw = minWorkTime;
				$scope.request.minimum_time.value = common.decToTime(minWorkTime);
				updateMessage($scope.request.minimum_time);
				$scope.request.minimum_time.value = common.decToTime(minWorkTime);
			} else {
				// Get back old value for correct work calculator
				$scope.request.minimum_time.raw = minWorkTime;
				$scope.request.minimum_time.value = common.decToTime(minWorkTime);
				removeMessage($scope.request.minimum_time);
			}

			if ($scope.request.maximum_time.old != common.decToTime(maxWorkTime)) {
				$scope.request.maximum_time.raw = maxWorkTime;
				$scope.request.maximum_time.value = common.decToTime(maxWorkTime);
				updateMessage($scope.request.maximum_time);
				$scope.request.maximum_time.value = common.decToTime(maxWorkTime);
			} else {
				// Get back old value for correct work calculator
				$scope.request.maximum_time.raw = maxWorkTime;
				$scope.request.maximum_time.value = common.decToTime(maxWorkTime);
				removeMessage($scope.request.maximum_time);
			}

			if ($scope.request.crew.old != moversCount) {
				$scope.request.crew.value = moversCount;
				updateMessage($scope.request.crew);
			} else {
				// Get back old value for correct work calculator
				$scope.request.crew.value = moversCount;
				removeMessage($scope.request.crew);
			}
		}

		function calculateTime(initRequest) {
			if ($scope.request.field_usecalculator.value == null) {
				$scope.request.field_usecalculator.value = false;
			}

			let allowRecalculateTime = $scope.request.field_usecalculator.value
				&& (!$scope.Job_Completed || !$scope.pickupCompleted && !$scope.deliveryCompleted);

			if ($scope.request.field_usecalculator.value) {
				let total_cf = [];
				total_cf.weight = checkForMinWeightLdOnly($scope.request.visibleWeight);
				$scope.request.typeFrom = $scope.request.type_from.raw || '1';
				$scope.request.typeTo = $scope.request.type_to.raw || '1';
				$scope.request.serviceType = $scope.request.service_type.raw;
				$scope.calcResults = CalculatorServices.calculateTime($scope.request, total_cf);
			}

			if (allowRecalculateTime) {
				let flatRate = $scope.request.service_type.raw == 5 || $scope.request.service_type.raw == 7;
				let storageRequest = $scope.request.service_type.raw == 2 || $scope.request.service_type.raw == 6;
				let duration = common.filterFloat($scope.request.duration.value);
				let distance = parseFloat($scope.request.distance.value);
				let settings = angular.fromJson($scope.fieldData.basicsettings);

				if ($scope.request.duration.value == null) {
					$scope.request.duration.value = 0;
				}

				if (!!settings.local_flat_miles && distance > settings.local_flat_miles && !flatRate && !storageRequest) {
					duration = 0;
					$scope.localFlatMilesFlag = true;
				} else {
					$scope.localFlatMilesFlag = false;
				}

				if (flatRate) {
					duration = 0;
				}

				let calcResults = $scope.calcResults;
				let moversCount = calcResults.movers_count;
				let trucksCount = calcResults.trucks;
				let minWorkTime = calcResults.work_time.min;
				let maxWorkTime = calcResults.work_time.max;
				$scope.truckCount = trucksCount;

				if (!$scope.calcSettings.doubleDriveTime && !$scope.calcSettings.isTravelTimeSameAsDuration) {
					minWorkTime += duration;
					maxWorkTime += duration;
				}

				minWorkTime = CalculatorServices.getRoundedTime(minWorkTime);
				maxWorkTime = CalculatorServices.getRoundedTime(maxWorkTime);

				let allowedStorage = ($scope.request.service_type.raw == 2
					|| $scope.request.service_type.raw == 6)
					&& $scope.request.status.raw != 3;
				let isAllowUpdate = !initRequest || allowedStorage;

				if (isAllowUpdate) {
					updateRequestWorkData(minWorkTime, maxWorkTime, moversCount);
				}

				updateRate(trucksCount, !isAllowUpdate);
				updateQuote(!isAllowUpdate);
			}
		}

		$scope.checkDeliveryDay = function (oldValue, isRemove) {
			if (!isRemove) {
				let lastLdDeliveryDay;
				if ($scope.request.inventory.move_details.delivery.length) {
					let lastLdDeliveryDayTemp = CalculatorServices.getLongDistanceWindow($scope.request, $scope.request.inventory.move_details.delivery, $scope.request.inventory.move_details.delivery_days);

					lastLdDeliveryDay = $scope.request.inventory.move_details.delivery_days == 0 ? undefined : moment(lastLdDeliveryDayTemp).unix() + TIME_ZONE_OFFSET;
				}

				$scope.changeDDay(oldValue, lastLdDeliveryDay);
			} else {
				SweetAlert.swal({
					title: 'Do you want to delete first delivery dates?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes',
					cancelButtonText: 'No',
					closeOnConfirm: true,
					closeOnCancel: true
				}, function (isConfirm) {
					if (isConfirm) {
						$scope.request.inventory.move_details.delivery = '';
						$scope.changeDDay(oldValue);
					}
				});
			}
		};

		$scope.changeDDay = function (oldValue, lastLdDeliveryDay) {
			$scope.busy = true;

			RequestServices.saveDetails($scope.request.nid, $scope.request.inventory.move_details).then(function (data) {
				RequestServices.updateRequest($scope.request.nid, {
					field_move_service_type: $scope.request.service_type.raw,
					field_ld_sit_delivery_dates: {
						value: $scope.request.inventory.move_details.delivery.length ? moment($scope.request.inventory.move_details.delivery).unix() + TIME_ZONE_OFFSET : undefined,
						value2: lastLdDeliveryDay
					}
				});

				var obj = {
					text: 'Delivery Date changed',
					from: oldValue || 'Don\'t Know Yet',
					to: $scope.request.inventory.move_details.delivery || 'Don\'t Know Yet'
				};
				var arr = [];
				arr.push(obj);

				_.forEach(data.logs, function (log) {
					arr.push(log);
				});

				if (!_.isEmpty(arr)) {
					sendNotificationAfterSaveChanges(arr, $scope.request.nid, 'MOVEREQUEST');
					RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
				}

				if ($scope.request.status.raw == '3') {
					saveOnlyRequestAllData()
						.then(function (data) {
							toastr.success('Delivery day saved');
							$scope.request.request_all_data.invoice = angular.copy($scope.invoice);
							$scope.busy = false;
						});
				} else {
					toastr.success('Delivery day saved');
					$scope.busy = false;
				}
			});

			initData();
		};

		$scope.changeDDays = function () {
			$scope.busy = true;
			let lastLdDeliveryDayTemp = CalculatorServices.getLongDistanceWindow($scope.request, $scope.request.inventory.move_details.delivery, $scope.request.inventory.move_details.delivery_days);
			if ($scope.request.inventory.move_details.delivery.length) {
				var lastLdDeliveryDay = $scope.request.inventory.move_details.delivery_days == 0 ? undefined : moment(lastLdDeliveryDayTemp).unix() + TIME_ZONE_OFFSET;
			}
			RequestServices.saveDetails($scope.request.nid, $scope.request.inventory.move_details).then(function (data) {
				RequestServices.updateRequest($scope.request.nid, {
					field_move_service_type: $scope.request.service_type.raw,
					field_ld_sit_delivery_dates: {
						value: $scope.request.inventory.move_details.delivery.length ? moment($scope.request.inventory.move_details.delivery).unix() + TIME_ZONE_OFFSET : undefined,
						value2: lastLdDeliveryDay
					}
				});

				var obj = {
					text: 'Delivery Dates changed',
					from: $scope.oldDeliveryDaysValue,
					to: $scope.request.inventory.move_details.delivery_days
				};
				var arr = [];
				arr.push(obj);
				_.forEach(data.logs, function (log) {
					arr.push(log);
				});
				if (!_.isEmpty(arr)) {
					sendNotificationAfterSaveChanges(arr, $scope.request.nid, 'MOVEREQUEST');
					RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
				}
				$scope.oldDeliveryDaysValue = $scope.request.inventory.move_details.delivery_days;
				if ($scope.request.status.raw == '3') {
					saveOnlyRequestAllData()
						.then(function (data) {
							toastr.success('Delivery days saved');
							$scope.request.request_all_data.invoice = angular.copy($scope.invoice);
							$scope.busy = false;
						});
				} else {
					toastr.success('Delivery days saved');
					$scope.busy = false;
				}
			});

			initData();
		};

		$scope.changeDDaysInvoice = function () {
			if ($scope.request.status.raw == '3') {
				$scope.busy = true;

				saveOnlyRequestAllData().then(() => {
					toastr.success('Delivery days saved');
					$rootScope.$broadcast('request.updated', $scope.request);

					$scope.busy = false;
				});

				initData();
			}
		};

		$scope.changeLDStatus = function () {
			$scope.request.ld_status = $scope.request.ld_status || null;
			RequestServices.updateRequest($scope.request.nid, {
				field_ld_flag_status: $scope.request.ld_status
			}).then(() => {
				toastr.success('Long Distance status was updated!');
				var arr = [];
				if ($scope.request.ld_status) {
					var obj = {
						text: 'Long distance status was changed',
						to: $scope.flatsettings.ldStatus.filter(item => item.id == $scope.request.ld_status)[0].name
					};
				} else {
					var obj = {
						text: 'Long distance status was removed'
					};
				}

				arr.push(obj);
				RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST')
			})
				.catch(err => {
					Raven.captureException(`Saving field_ld_flag_status from moveBoard: ${err}`);
					toastr.error('Cannot connect to server!', 'Long Distance status was NOT updated!');
				});
		};
		$scope.changeScheduleDeliveryDate = function () {

			RequestServices.updateRequest($scope.request.nid, {
				field_schedule_delivery_date: $scope.request.schedule_delivery_date
			}).then(() => {
				RequestServices.sendLogs([{
					text: 'Schedule delivery date was changed',
					from: $scope.request.schedule_delivery_date.old,
					to: $scope.request.schedule_delivery_date.value
				}], 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
			});
		};

		$scope.openSendRequestToSITModal = function () {
			EditRequestServices.openSendRequestToSITModal($scope.request);
		};

		$scope.redirectToMap = function () {
			var from = getAddressOfField($scope.request.field_moving_from);
			var to = getAddressOfField($scope.request.field_moving_to);
			//from A to B
			window.open('https://maps.google.com/maps/dir/' + from + '/' + to, '_blank');
		};

		function getAddressOfField(field) {
			var result = getFieldValue(field, 'locality') + ', ' + getFieldValue(field, 'administrative_area') + ', ' + getFieldValue(field, 'postal_code');

			if (!_.isEqual(field.thoroughfare, 'Enter Address') && !_.isEmpty(field.thoroughfare)) {
				result = getFieldValue(field, 'thoroughfare') + ', ' + result;
			}

			return result;
		}

		function getFieldValue(field, property) {
			return _.isUndefined(field[property]) || _.isNull(field[property]) ? '' : field[property];
		}

		$scope.showPathFromCompanyToPoint = function (field) {
			var to = getAddressOfField(field);
			var parking = $scope.basicsettings.company_address;

			window.open('https://maps.google.com/maps/dir/' + parking + '/' + to, '_blank');
		};

		function GetAreaCode() {
			var promise = CalculatorServices.getLongDistanceCode($scope.request.field_moving_to.postal_code);
			promise.then(function (data) {
				if (data) {
					$scope.request.field_moving_to.premise = data;
					$scope.editrequest['field_moving_to'] = $scope.request.field_moving_to;
					$scope.editrequest['field_moving_to']['premise'] = data;
					$scope.message['field_moving_to'] = {
						label: 'custom',
						oldValue: '',
						newValue: 'Area Code was Added',

					};
					initData();
				}
			});
		}

		function getTrucksCount() {
			return $scope.request.trucks.raw.length;
		}

		function _ld_calcExtraPickup() {
			var zip_from = $scope.request.field_moving_from.postal_code;
			var pickup_zip_from = $scope.request.field_extra_pickup.postal_code;
			var serviceType = $scope.request.service_type.raw;

			var promise = CalculatorServices.getDuration(zip_from, pickup_zip_from, serviceType);

			promise.then(function (data) {
				if ($scope.request.travel_time.old != common.decToTime(data.duration)) {
					$scope.request.travel_time.value = common.decToTime(data.duration);
					$scope.request.travel_time.raw = data.duration;
					updateMessage($scope.request.travel_time);
				} else {
					$scope.request.travel_time.value = common.decToTime(data.duration);
					$scope.request.travel_time.raw = data.duration;
					removeMessage($scope.request.travel_time);
				}

				if ($scope.request.duration.old != common.decToTime(data.distances.AB.duration)) {
					$scope.request.duration.value = data.distances.AB.duration;
					updateMessage($scope.request.duration);
				} else {
					$scope.request.duration.value = data.distances.AB.duration;
					removeMessage($scope.request.duration);
				}


				$scope.request.distance.value = data.distances.AB.distance;
				updateMessage($scope.request.distance);

				var lextrapickup = additionalServicesFactory.getLongDistanceExtraPickup($scope.request.distance.value);
				//1.Show sweet alert and cancel extra pickup
				if (lextrapickup == -1) {
					SweetAlert.swal('Extra pickup is not available for ' + $scope.request.distance.value + 'miles from Origin');
					removeExtra('field_extra_pickup');
					$scope.request.extraPickup = false;
				}

				initData();
				calculateTime();
			});
		}

		function _ld_calcExtraDropof() {
			var zip_from = $scope.request.field_moving_to.postal_code;
			var drop_zip_from = $scope.request.field_extra_dropoff.postal_code;
			var serviceType = $scope.request.service_type.raw;

			var promise = CalculatorServices.getDuration(zip_from, drop_zip_from, serviceType);

			promise.then(function (data) {
				var lextradelivery = additionalServicesFactory.getLongDistanceExtraDelivery(data.distances.AB.distance);
				//1.Show sweet alert and cancel extra pickup
				if (lextradelivery == -1) {
					SweetAlert.swal('Extra delivery is not available for more than ' + $scope.request.distance.value + 'miles from Destination');
					removeExtra('field_extra_dropoff');
					$scope.request.extraPickup = false;
				}

				initData();
				calculateTime();
			});
		}

		function calcDistance() {
			// if long Distance
			if ($scope.request.service_type.raw == 7) {
				CalculatorServices.calcDistance($scope.request)
					.then(function ({duration, distance}) {
						$scope.request.duration.value = duration;
						$scope.request.distance.value = distance;

						updateMessage($scope.request.distance);
						updateMessage($scope.request.duration);
						initData();
						calculateTime();
					});
			} else {
				let zipCodes = [$scope.request.field_moving_from.postal_code, $scope.request.field_moving_to.postal_code];

				if ($scope.request.service_type.raw == 3) {
					zipCodes = [$scope.request.field_moving_from.postal_code, $scope.request.field_moving_from.postal_code]
				}

				if ($scope.request.service_type.raw == 4) {
					zipCodes = [$scope.request.field_moving_to.postal_code, $scope.request.field_moving_to.postal_code]
				}

				CalculatorServices.getFullDistance(...zipCodes)
					.then(function ({duration, distance}) {
						$scope.request.duration.value = duration;
						$scope.request.distance.value = distance;

						updateMessage($scope.request.distance);
						updateMessage($scope.request.duration);
						initData();
						calculateTime();
					});
			}
		}

		function removeInvoiceExtra(field) {
			var initialExtra = {
				'administrative_area': '',
				'locality': '',
				'organisation_name': '1',
				'postal_code': '',
				'premise': '',
				'thoroughfare': ''
			};

			$scope.editRequestinvoice.invoice[field] = initialExtra;
			$scope.invoice[field] = initialExtra;

			var text = _.isEqual(field, 'field_extra_dropoff') ? 'Extra Drop-off was Removed' : 'Extra Pickup was Removed';

			$scope.messageInvoice[field] = {
				label: 'custom',
				oldValue: '',
				newValue: text
			};

			initData();
		}

		function removeExtra(field) {
			var initialExtra = {
				'administrative_area': '',
				'locality': '',
				'organisation_name': '1',
				'postal_code': '',
				'premise': '',
				'thoroughfare': ''
			};

			$scope.editrequest[field] = initialExtra;
			$scope.request[field] = initialExtra;

			var text = _.isEqual(field, 'field_extra_dropoff') ? 'Extra Drop-off was Removed' : 'Extra Pickup was Removed';

			$scope.message[field] = {
				label: 'custom',
				oldValue: '',
				newValue: text
			};
			if ($scope.Invoice) {
				removeInvoiceExtra(field);
				if (_.isEqual(field, 'field_extra_dropoff')) {
					$scope.invoice.extraDropoff = false;
				} else {
					$scope.invoice.extraPickup = false;
				}
			}
			initData();
		}

		function updateChange(request) {
			var reload = request.status.raw != $scope.request.status.raw;

			//Moving and Storage or Overnight
			$scope.ifStorage = request.service_type.raw == 2 || request.service_type.raw == 6;
			var inventory = InventoriesServices.calculateTotals(request.inventory.inventory_list);

			if (inventory.cfs != 0 && $scope.request.inventory_weight.cfs != inventory.cfs && request.field_useweighttype.value != 3) {
				request.inventory_weight = inventory;
				request.field_useweighttype.value = 2;
				SweetAlert.swal('Inventory was changed by Customer');
				reload = true;
			}

			// if not custom weight
			if (inventory.cfs == 0 && request.field_useweighttype.value != 3) {
				request.field_useweighttype.value = 1;
			}

			if ($scope.request.service_type.raw != request.service_type.raw) {
				request.request_all_data.reservationIncreased = false;
				request.request_all_data.reservationChangedManually = false;
				reload = true;
			}

			var currFlags = Object.keys($scope.request.field_company_flags.value);
			var newFlags = Object.keys(request.field_company_flags.value);
			var diffFlags = false;
			_.forEach(currFlags, function (i) {
				if (_.indexOf(newFlags, i) == -1) {
					diffFlags = true;
				}
			});
			if (diffFlags) {
				reload = true;
			}

			for (var i in $scope.request.contract_info) {
				if ($scope.request.contract_info[i] != request.contract_info[i]) {
					reload = true;
				}
			}

			if (reload) {
				$scope.request = request;

				if (!$scope.request.sendingTemplates) {
					$scope.request.sendingTemplates = [];
				}

				initData();

				changeTypeOfServiceInvoice();
				$rootScope.$broadcast('request.updated', request);
			}
		}


		if ($scope.longDistance) {
			$scope.$watch('states.invoiceState', () => {
				calculateGrandTotalBalance('longdistance');
			});
			let customWeight = $scope.request.visibleWeight;
			if (!$scope.request.closing_weight) {
				$scope.request.closing_weight = {value: customWeight, old: customWeight};

				if (!$scope.invoice.closing_weight) {
					$scope.invoice.closing_weight = $scope.request.closing_weight;
				}
			}

			updateLDTooltip();
		}

		$scope.showQuoteTooltip = (val) => {
			$scope.isShowQuoteTooltip = val;
		};

		$scope.changeClosingWeightValue = () => {
			$scope.invoice.closing_weight.value = parseFloat($scope.invoice.closing_weight.value);
			$scope.longDistanceClosingQuote = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
			$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
			calculateGrandTotalBalance('longdistance');
			$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));
			$scope.editRequestinvoice.invoice.closing_weight = $scope.invoice.closing_weight.value;
			$scope.messageInvoice.closing_weight = {
				label: 'Closing weight',
				newValue: $scope.invoice.closing_weight.value + '',
				oldValue: ($scope.invoice.closing_weight.old || $scope.request.closing_weight.old) + ''
			};

			updateLDTooltip();
			if ($scope.Invoice) {
				$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
				$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));
			}
		};

		function weightTypeChange(state) {
			if (state) {
				$scope.request.field_cubic_feet.value = '1';
				$scope.request.field_long_distance_rate.value = parseFloat($scope.request.field_long_distance_rate.value * 7).toFixed(2);
			} else {
				$scope.request.pounds = true;
				$scope.request.field_cubic_feet.value = '0';
				$scope.request.field_long_distance_rate.value = parseFloat($scope.request.field_long_distance_rate.value / 7).toFixed(2);

			}

			updateMessage($scope.request.field_cubic_feet);
			updateMessage($scope.request.field_long_distance_rate);
		}

		$scope.OpenPaymentModal = function () {
			let entity = {
				entity_id: $scope.request.nid,
				entity_type: $scope.fieldData.enums.entities.MOVEREQUEST
			};

			PaymentServices.openPaymentModal($scope.request, entity);
		};

		$scope.OpenAuthModal = function () {
			$scope.request.reservation = true;
			PaymentServices.openAuthPaymentModal($scope.request, 'step0', $scope.request.receipts, 0);
		};

		$scope.additional_services = 0;

		$scope.calculatePackingServicesInvoice = calculatePackingServicesInvoice;

		function calculatePackingServicesInvoice() {
			if (angular.isDefined($scope.invoice.service_type)) {
				var serviceType = $scope.invoice.service_type.raw == '5' || $scope.invoice.service_type.raw == '7';
			}

			if (angular.isDefined($scope.invoice.request_data)) {
				return PaymentServices.getPackingTotal($scope.invoice.request_data.value, $scope.packing.labor, serviceType);
			}
		}

		$scope.calculateAddServicesInvoice = calculateAddServicesInvoice;

		function calculateAddServicesInvoice() {
			var result = 0;

			if (angular.isDefined($scope.invoice.extraServices)) {
				result = additionalServicesFactory.getExtraServiceTotal($scope.invoice.extraServices);
			}

			return parseFloat(result);
		}

		$scope.calculateAddServices = calculateAddServices;

		function calculateAddServices() {
			return parseFloat(additionalServicesFactory.getExtraServiceTotal($scope.request.extraServices));
		}

		$scope.calculatePackingServices = calculatePackingServices;

		function calculatePackingServices() {
			var serviceType = $scope.request.service_type.raw == '5' || $scope.request.service_type.raw == '7';
			return PaymentServices.getPackingTotal($scope.request.request_data.value, $scope.packing.labor, serviceType);
		}

		$scope.calculatePayments = function () {
			return PaymentServices.calcPayment($scope.request.receipts);
		};

		$scope.PermissionsServices = PermissionsServices;

		$scope.calculateTotalBalance = calculateTotalBalance;

		function calculateTotalBalance(type) {
			var total = 0;

			if (type == 'min') {
				total = $scope.request.quote.min;
			} else if (type == 'max') {
				total = $scope.request.quote.max;
			} else if (type == 'longdistance') {
				total = $scope.longDistanceQuote;
			} else if (type == 'flatrate') {
				total = parseFloat($scope.request.flat_rate_quote.value);
			} else if (type == 'invoice') {
				if (angular.isDefined($scope.invoice.rate)) {
					total = $scope.invoice.work_time_int * parseFloat($scope.invoice.rate.value);
				}

				if (angular.isUndefined($scope.invoice.rate)) {
					total = $scope.invoice.work_time_int * parseFloat($scope.request.rate.value);
				}
			}

			var discount = $scope.request.request_all_data.add_money_discount + total * $scope.request.request_all_data.add_percent_discount / 100;

			return _.round(total - discount, 2);
		}

		$scope.calculateGrandTotalBalance = calculateGrandTotalBalance;

		function calculateGrandTotalBalance(type) {
			let result = 0;
			if (type === 'invoice') {
				result = grandTotalService.getGrandTotal(type, $scope.invoice);
			} else {
				result = grandTotalService.getGrandTotal(type, $scope.request);
			}
			$scope.request.request_all_data.grandTotalRequest[type] = result;
			return result;
		}

		$scope.openAddServicesInvoiceModal = function () {
			var grandTotal = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			additionalServicesFactory.openAddServicesModal($scope.request, $scope.invoice, 'invoice', grandTotal);
		};

		$scope.openAddPackingInvoiceModal = function () {
			var grandTotal = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			PaymentServices.openAddPackingModal($scope.request, $scope.invoice, 'invoice', grandTotal);
		};

		$scope.OpenSurchargeInvoiceModal = function () {
			var grandTotal = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);

			if ($scope.request.service_type.raw == 5) {
				var quote = CalcRequest.flatrateQuoteInvoiceChange($scope.invoice, $scope.request);
				requestModals.surchargeModal(quote, $scope.request, $scope.invoice, 'invoice', grandTotal);
			} else if ($scope.request.service_type.raw == 7) {
				var quote = $scope.longDistanceQuoteInvoice;
				requestModals.surchargeModal(quote, $scope.request, $scope.invoice, 'invoice', grandTotal);
			} else {
				var quote = (calculateTotalBalance('min') + calculateTotalBalance('max')) / 2;
				requestModals.surchargeModal(quote, $scope.request, $scope.invoice, 'invoice', grandTotal);
			}
		};

		$scope.OpenDiscountInvoiceModal = function () {
			var grandTotal = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			requestModals.additionalDiscountModal($scope.request, $scope.invoice, 'invoice', grandTotal);
		};

		$scope.openValuationInvoiceModal = function () {
			let grandTotal = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			valuationService.openValuationModal($scope.request, $scope.invoice, 'invoice', grandTotal);
		};

		//sales
		$scope.openAddServicesModal = function () {
			if ($scope.Invoice) {
				var grandTotalInvoice = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			}
			var grandTotal = 0;
			if ($scope.longDistance) {
				grandTotal = calculateGrandTotalBalance('longdistance');
			}
			if ($scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('flatrate');
			}
			if (!$scope.longDistance && !$scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('min') + ' - $' + calculateGrandTotalBalance('max');
			}
			additionalServicesFactory.openAddServicesModal($scope.request, $scope.invoice, 'request', grandTotal, grandTotalInvoice);
		};

		$scope.openAddPackingModal = function () {
			if ($scope.Invoice) {
				var grandTotalInvoice = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			}
			var grandTotal = 0;
			if ($scope.longDistance) {
				grandTotal = calculateGrandTotalBalance('longdistance');
			}
			if ($scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('flatrate');
			}
			if (!$scope.longDistance && !$scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('min') + ' - $' + calculateGrandTotalBalance('max');
			}
			PaymentServices.openAddPackingModal($scope.request, $scope.invoice, 'request', grandTotal, grandTotalInvoice);
		};

		$scope.openValuationModal = function () {
			let grandTotal = 0;
			if ($scope.longDistance) {
				grandTotal = calculateGrandTotalBalance('longdistance');
			}
			if ($scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('flatrate');
			}
			if (!$scope.longDistance && !$scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('min') + ' - $' + calculateGrandTotalBalance('max');
			}
			valuationService.openValuationModal($scope.request, $scope.invoice, 'request', grandTotal, false);

		};

		$scope.OpenSurchargeModal = function () {
			if ($scope.Invoice) {
				var grandTotalInvoice = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			}
			var grandTotal = 0;
			if ($scope.longDistance) {
				grandTotal = calculateGrandTotalBalance('longdistance');
			}
			if ($scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('flatrate');
			}
			if (!$scope.longDistance && !$scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('min') + ' - $' + calculateGrandTotalBalance('max');
			}

			if ($scope.request.service_type.raw == 5) {
				var quote = CalcRequest.flatrateQuoteRequestChange($scope.request);
				requestModals.surchargeModal(quote, $scope.request, $scope.invoice, 'request', grandTotal, grandTotalInvoice);
			} else if ($scope.request.service_type.raw == 7) {
				var quote = $scope.longDistanceQuote;
				requestModals.surchargeModal(quote, $scope.request, $scope.invoice, 'request', grandTotal, grandTotalInvoice);
			} else {
				var quote = (calculateTotalBalance('min') + calculateTotalBalance('max')) / 2;
				requestModals.surchargeModal(quote, $scope.request, $scope.invoice, 'request', grandTotal, grandTotalInvoice);
			}

		};

		$scope.OpenDiscountModal = function () {
			if ($scope.Invoice) {
				var grandTotalInvoice = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			}
			var grandTotal = 0;
			if ($scope.longDistance) {
				grandTotal = calculateGrandTotalBalance('longdistance');
			}
			if ($scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('flatrate');
			}
			if (!$scope.longDistance && !$scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('min') + ' - $' + calculateGrandTotalBalance('max');
			}
			requestModals.additionalDiscountModal($scope.request, $scope.invoice, 'request', grandTotal, grandTotalInvoice);
		};

		$scope.OpenDiscountRateModal = function () {
			requestModals.RateDiscountModal($scope.request, $scope.message, $scope.editrequest);
		};

		$scope.openSalaryCommisionModal = function (type) {

			let canEditPayroll = PermissionsServices.hasPermission('canEditPayroll');

			if (!canEditPayroll) {
				toastr.error('You cannot have permission to edit payroll');
				return false;
			}

			if (!$scope.Job_Completed && ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value)) {
				SweetAlert.swal('Please close the job before opening payroll window', '', 'info');
				return false;
			} else if ($scope.request.service_type.raw == 5) {
				if (type == 'pickedUpCrew' && !$scope.pickupCompleted) {
					SweetAlert.swal('Please close the pickup before opening payroll window', '', 'info');
					return false;
				}
				if (type == 'deliveryCrew' && !$scope.deliveryCompleted) {
					SweetAlert.swal('Please close the delivery before opening payroll window', '', 'info');
					return false;
				}
			}
			let contractType;
			if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
				contractType = contractTypes.PAYROLL;
			} else {
				if (type == 'pickedUpCrew') {
					contractType = contractTypes.PICKUP_PAYROLL;
				}
				if (type == 'deliveryCrew') {
					contractType = contractTypes.DELIVERY_PAYROLL;
				}
			}
			$scope.busy = true;

			let payrollPromise = RequestServices.getPayroll($scope.request, contractType);
			let payrollInfoPromise = RequestServices.getPayrollInfo();

			$q.all({payrollPromise, payrollInfoPromise})
				.then(resolve => {
					let grandTotal = grandTotalFunc($scope.invoice, 'invoice', $scope.tips) - Number($scope.tips.value);
					requestModals.salaryCommisionModal(resolve.payrollPromise, $scope.request, resolve.payrollInfoPromise, $scope.invoice, grandTotal, type);
				})
				.finally(() => {
					$scope.busy = false;
				});
		};

		$scope.openMinWeight = function () {
			requestModals.minWeightModal($scope.request, $scope.invoice, $scope.request.request_all_data.min_weight, $scope.request.request_all_data.min_price, $scope.request.request_all_data.min_price_enabled);
		};

		$scope.openJob = function (type) {
			let flag;
			if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
				flag = Job_Completed;
			} else {
				if (type == 'Pickup Done') {
					if ($scope.deliveryCompleted) {
						SweetAlert.swal('Delivery isn\'t opened', '', 'error');
						return false;
					}
					flag = pickupJobCompleted;
				} else if (type == 'Delivery Done') {
					flag = deliveryJobCompleted;
				}
			}

			delete $scope.request.field_company_flags.value[importedFlags.company_flags[flag]];
			if (type == 'Pickup Done') {
				delete $scope.request.field_company_flags.value[importedFlags.company_flags[Job_Completed]];
			}
			var field_flags = _.keys($scope.request.field_company_flags.value) || [];
			$scope.busy = true;

			RequestServices.updateRequest($scope.request.nid, {
				field_company_flags: field_flags
			}).then(() => {
				toastr.success('Job is open.');
				$scope.$broadcast('request.updated', $scope.request);
				var message = 'Job Opened';
				if (flag == Job_Completed) {
					$scope.Job_Completed = false;
					$rootScope.$broadcast('job_completed', {
						job_completed: false,
						isFlatRate: false,
						nid: $scope.request.nid
					});
				}
				if (flag == pickupJobCompleted) {
					$scope.pickupCompleted = false;
					message = 'Pickup opened';
					$rootScope.$broadcast('job_completed', {
						job_pickup_open: true,
						isFlatRate: true,
						nid: $scope.request.nid
					});
				}
				if (flag == deliveryJobCompleted) {
					$scope.deliveryCompleted = false;
					message = 'Delivery opened';
					$rootScope.$broadcast('job_completed', {
						job_delivery_open: true,
						isFlatRate: true,
						nid: $scope.request.nid
					});
				}
				if (!$scope.pickupCompleted && !$scope.deliveryCompleted) {
					$scope.Job_Completed = false;
				}
				var obj = {
					simpleText: message
				};
				var arr = [obj];
				RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
				$scope.busy = false;
			});
		};

		$scope.closeJob = function (type) {
			//validate check if actual time is no 0
			if ($scope.invoice.work_time_int == 0 && $scope.invoice.service_type.raw != 5 && $scope.invoice.service_type.raw != 7) {
				SweetAlert.swal('Work time is not set', '', 'error');
				return false;
			}
			if ($scope.request.service_type.raw == 5 && !$scope.request.field_flat_rate_local_move.value) {
				if (type == 'Delivery Done' && !$scope.pickupCompleted) {
					SweetAlert.swal('Pickup isn\'t closed', '', 'error');
					return false;
				}
			}

			$scope.busy = true;
			var payrollReq = {};
			$scope.grandTotal = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			let total = calculateGrandTotalWithoutContractTaxDiscount($scope.invoice, 'invoice', $scope.tips);
			let managerTotal = total;
			var multiplier = 1;
			if (type == 'Pickup Done' && $scope.request.service_type.raw == 5 && !$scope.request.field_flat_rate_local_move.alue) {
				multiplier = (HUNDRED_PERCENT - PICK_UP_PERCENT) / HUNDRED_PERCENT;
				total = _.round(total * PICK_UP_PERCENT / HUNDRED_PERCENT, 2);
			}
			if (type == 'Delivery Done') {
				multiplier = (HUNDRED_PERCENT - PICK_UP_PERCENT) / HUNDRED_PERCENT;
				total = _.round(total * (HUNDRED_PERCENT - PICK_UP_PERCENT) / HUNDRED_PERCENT, 2);
			}
			var discount = $scope.invoice.request_all_data.add_money_discount + total * ($scope.invoice.request_all_data.add_percent_discount / 100);
			if (type != 'Delivery Done')
				payrollReq.manager_total = managerTotal;
			payrollReq.materials = (parseFloat(calculatePackingServicesInvoice()) * multiplier).toFixed(2);
			payrollReq.extra_service_total = (parseFloat(calculateAddServicesInvoice()) * multiplier).toFixed(2);
			payrollReq.tips_total = $scope.tips.value;
			payrollReq.moneyDiscount = (discount * multiplier).toFixed(2);
			payrollReq.discount = 0;
			payrollReq.grand_total = total;
			payrollReq.fuel = ($scope.invoice.request_all_data.surcharge_fuel * multiplier).toFixed(2);
			payrollReq.valuation = 0;
			payrollReq.min_hour_rate = false;

			if ($scope.request.service_type.raw == 7) {
				payrollReq.estimate_total = calculateGrandTotalBalance('longdistance');
			} else if($scope.request.service_type.raw == 5) {
				payrollReq.estimate_total = calculateGrandTotalBalance('flatrate');
			} else {
				payrollReq.estimate_total = (calculateGrandTotalBalance('min') + calculateGrandTotalBalance('max')) / 2;
			}

			if ($scope.invoice.request_all_data.valuation && $scope.invoice.request_all_data.valuation.selected.valuation_charge) {
				payrollReq.valuation = ($scope.invoice.request_all_data.valuation.selected.valuation_charge * multiplier).toFixed(2);
			}

			if ($scope.request.service_type.raw == 5 || $scope.request.service_type.raw == 7) {
				payrollReq.hours = Number($scope.invoice.maximum_time.raw) + Number($scope.travelTime ? $scope.invoice.travel_time.raw : 0);
				payrollReq.laborHours = Number($scope.invoice.maximum_time.raw);
				$scope.invoice.work_time_int = angular.copy(payrollReq.laborHours);
			} else {
				payrollReq.hours = Number($scope.invoice.work_time_int);
				if ($scope.isDoubleDriveTime) {
					payrollReq.hours += $scope.invoice.field_double_travel_time.raw;
				} else {
					if ($scope.travelTime) {
						payrollReq.hours += Number($scope.travelTime ? $scope.invoice.travel_time.raw : 0);
					}
				}
				if (parseFloat(payrollReq.hours) < $scope.min_hour_rate) {
					payrollReq.hours = $scope.min_hour_rate;
					payrollReq.min_hour_rate = true;
				}
				payrollReq.laborHours = Number($scope.invoice.work_time_int);
				if ($scope.request.request_data.value && $scope.request.request_data.value.paymentType && $scope.request.request_data.value.paymentType == 'LaborAndTravel') {
					if ($scope.isDoubleDriveTime) {
						payrollReq.laborHours += $scope.invoice.field_double_travel_time.raw;
					} else {
						payrollReq.laborHours += $scope.invoice.travel_time.raw;
					}
				}
			}

			//create payroll for workers
			if (!_.isEmpty($scope.request.request_data) && !_.isEmpty($scope.request.request_data.value)) {
				let request_data;
				if (_.isString($scope.request.request_data.value)) {
					request_data = angular.fromJson($scope.request.request_data.value);
				} else {
					request_data = $scope.request.request_data.value;
				}

				let switchers = request_data.switchers || null;
				let	workTimesForSwitchers = '';
				let travelTime = getTravelTime();
				let	workDuration = getPayrollWorkersHours(payrollReq.hours, travelTime.raw);
				let	jobType = '';

				let dividedFee = 0;
				if (request_data.crews && request_data.crews.baseCrew) {
					if (!!request_data.crews.baseCrew.unpaidHelpers
						&& request_data.crews.baseCrew.unpaidHelpers.length) {
						dividedFee = _.round(workDuration * (request_data.crews.baseCrew.helpers.length - (request_data.crews.baseCrew.unpaidHelpers || []).length + 1), 2);
						dividedFee = _.round(dividedFee / (request_data.crews.baseCrew.helpers.length + 1), 2);
						dividedFee = MIN_PAYROLL_HOURS > dividedFee ? MIN_PAYROLL_HOURS : dividedFee;
					}
				}
				workDuration = MIN_PAYROLL_HOURS > workDuration ? MIN_PAYROLL_HOURS : workDuration;

				if (type == 'Pickup Done' || $scope.request.service_type.raw == 5 && $scope.request.field_flat_rate_local_move.value) {
					jobType = 'pickup';
				}
				if (type == 'Delivery Done') {
					jobType = 'delivery';
				}
				let payroll = CalculatorServices.generateWorkerPayroll(request_data, switchers, workTimesForSwitchers, workDuration, dividedFee, payrollReq.grand_total, travelTime.raw, jobType, '');
				payrollReq.foreman = payroll.foreman;
				payrollReq.helper = payroll.helper;
			}

			if ($scope.isDoubleDriveTime) {
				let workPlusTravelTime = $scope.invoice.work_time_int + $scope.invoice.field_double_travel_time.raw;
				let totalMinTime = $scope.calcSettings.min_hours > workPlusTravelTime ? $scope.calcSettings.min_hours : workPlusTravelTime;
				$scope.invoiceLabor = totalMinTime * ($scope.request.request_all_data.add_rate_discount || $scope.invoice.rate.value);
				$scope.request.request_all_data.totalHourlyCharge = totalMinTime * ($scope.request.request_all_data.add_rate_discount || $scope.invoice.rate.value);
			} else {
				let travelTime = $scope.invoice.work_time_int + ($scope.travelTime ? $scope.invoice.travel_time.raw : 0);
				let totalMinTime = $scope.calcSettings.min_hours > travelTime ? $scope.calcSettings.min_hours : travelTime;
				$scope.invoiceLabor = totalMinTime * ($scope.request.request_all_data.add_rate_discount || $scope.invoice.rate.value);
				$scope.request.request_all_data.totalHourlyCharge = totalMinTime * ($scope.request.request_all_data.add_rate_discount || $scope.invoice.rate.value);
			}

			if ($scope.serviceTypeInvoice) {
				$scope.request.request_all_data.totalHourlyCharge = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
			}

			$scope.request.request_all_data.req_tips = $scope.tips.value;

			payrollReq.progress = 0;
			// Submit contract (payroll).
			EditRequestServices.updateInvoiceFields($scope.invoice, $scope.editRequestinvoice.invoice);

			let invoiceLogs = copyInvoiceToAllData();
			let allData = $scope.request.request_all_data;
			if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
				closeRequest(payrollReq, Job_Completed, contractTypes.PAYROLL, allData, invoiceLogs);
			} else if (type == 'Pickup Done') {
				closeRequest(payrollReq, pickupJobCompleted, contractTypes.PICKUP_PAYROLL, allData, invoiceLogs);
			} else if (type == 'Delivery Done') {
				closeRequest(payrollReq, deliveryJobCompleted, contractTypes.DELIVERY_PAYROLL, allData, invoiceLogs);
			}
		};

		function getPayrollWorkersHours(payrollHours, travelTime) {
			let laborOnly = !!_.find($scope.contract_page.paymentTypes, (type) => type.name === 'Labor only.' && type.selected);

			if (laborOnly) {
				payrollHours -= travelTime;
			}

			return payrollHours;
		}

		function getTravelTime() {
			let travelTime = 0;

			if ($scope.isDoubleDriveTime) {
				if ($scope.request.request_all_data.invoice.field_double_travel_time
					&& !_.isNull($scope.request.request_all_data.invoice.field_double_travel_time.raw)) {
					travelTime = angular.copy($scope.request.request_all_data.invoice.field_double_travel_time);
				} else {
					travelTime = angular.copy($scope.request.field_double_travel_time);
				}
			} else {
				if ($scope.request.request_all_data.invoice.travel_time) {
					travelTime = angular.copy($scope.request.request_all_data.invoice.travel_time);
				} else {
					travelTime = angular.copy($scope.request.travel_time);
				}
			}

			return travelTime;
		}

		function closeRequest(payrollReq, flag, contractType, allData, invoiceLogs) {
			var new_field = importedFlags.company_flags[flag];

			if (_.isArray($scope.request.field_company_flags.value)) {
				$scope.request.field_company_flags.value = {};
			}

			$scope.request.field_company_flags.value[new_field] = flag;

			let updateFields = {
				field_company_flags: _.keys($scope.request.field_company_flags.value) || [],
				all_data: allData
			};
			RequestServices.updateRequest($scope.request.nid, updateFields)
				.then(res => {
					if (!_.head(res)) return;

					RequestServices.createPayroll($scope.request.nid, payrollReq, contractType)
						.then(res => {
							if (!_.head(res)) return;
							afterSuccessClosing();
						})
						.catch(err => {
							toastr.error('Cannot create payroll!', 'Please contact maintance');
						})
						.finally(() => {
							$scope.busy = false;
						});
				})
				.catch(() => {
					toastr.error('Cannot save changes before closing', 'Job was not closed!');
					$scope.busy = false;
				});

			function afterSuccessClosing() {
				toastr.success('Job is closed.');
				$scope.$broadcast('request.updated', $scope.request);
				$scope.editRequestinvoice.invoice = {};
				$scope.messageInvoice = {};
				var message = 'Job Closed';
				if (flag == Job_Completed) {
					$scope.Job_Completed = true;
					$rootScope.$broadcast('job_completed', {
						job_completed: true,
						isFlatRate: false,
						nid: $scope.request.nid
					});
				}
				if (flag == pickupJobCompleted) {
					$scope.pickupCompleted = true;
					message = pickupJobCompleted;
					$rootScope.$broadcast('job_completed', {
						job_pickup_close: true,
						isFlatRate: true,
						nid: $scope.request.nid
					});
				}
				if (flag == deliveryJobCompleted) {
					$scope.deliveryCompleted = true;
					message = deliveryJobCompleted;
					$rootScope.$broadcast('job_completed', {
						job_delivery_close: true,
						isFlatRate: true,
						nid: $scope.request.nid
					});
				}
				if (invoiceLogs) {
					RequestServices.sendLogs(invoiceLogs, 'Invoice was updated', $scope.request.nid, 'MOVEREQUEST');
				}
				RequestServices.sendLogs([{
					simpleText: message
				}], 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
				EditRequestServices.getQuickBookFlag($scope.request);
			}
		}

		function updateRate(trucksCount, initRequest) {
			if (angular.isUndefined(trucksCount)) {
				if ($scope.request.field_usecalculator.value) {
					trucksCount = $scope.truckCount || 1;
				} else {
					trucksCount = 1;
				}
			}

			let fieldRate = $scope.states.invoiceState ? 'invoice' : 'request';

			let rate = CalculatorServices.getRate($scope[fieldRate].date.value, $scope[fieldRate].crew.value, trucksCount);

			if (rate != $scope[fieldRate].rate.value && !initRequest) {
				$scope[fieldRate].rate.value = rate;
				if (!$scope.states.invoiceState) {
					_.set($scope.invoice, 'rate.value', rate);
					_.set($scope.invoice, 'crew.value', $scope.request.crew.value);
				}
				updateMessage($scope[fieldRate].rate);
			}

			if ($scope[fieldRate].move_size.raw == 11
				&& rate == $scope[fieldRate].rate.value
				&& !_.isUndefined($scope.editrequest.field_price_per_hour)) {
				removeMessage($scope[fieldRate].rate);
			}

			recalculateReservation();
		}


		function UpdateRequestInvoice() {
			var countInvoice = common.count($scope.editRequestinvoice.invoice);

			if (!CheckRequestService.isValidRequestZip($scope.invoice)) {
				SweetAlert.swal('Please enter the correct zip code');
				return false;
			}

			if (!countInvoice) {
				SweetAlert.swal('Nothing to Update!');
				return false;
			} else {
				EditRequestServices.updateInvoiceFields($scope.invoice, $scope.editRequestinvoice.invoice);

				var info = {
					invoice: $scope.invoice
				};

				requestModals.confirmUpdateInvoice($scope.request, $scope.messageInvoice, info)
					.then(result => {
						if (_.isObject(result) && result.type == 'update') {
							$scope.messageInvoice = {};
							$scope.editRequestinvoice.invoice = {};
						}
					});
			}
		}

		$scope.$on('localMove.closing.saved', () => {
			$scope.messageInvoice = {};
			$scope.editRequestinvoice.invoice = {};
			$scope.request.field_flat_rate_local_move.old = angular.copy($scope.request.field_flat_rate_local_move.value);
		});

		function changeActualTime(workTime) {
			$scope.editRequestinvoice.invoice.work_time = workTime;
			$scope.invoice.work_time_int = _.round(common.convertStingToIntTime(workTime), 2);
			updateFuelSurchargeRelatedToQuote();
		}

		function enableExtra(field, setting) {
			let extraPostal = _.get($scope.invoice, `${[field]}.postal_code`);
			let isShoudEnableExtra = !!extraPostal && extraPostal.length == 5;
			if (!isShoudEnableExtra) return;
			$scope.invoice[setting] = true;
		}

		function prepareFlatRateInvoice() {
			$scope.flatrateInvoice = true;
			if (!$scope.invoice.ddate.value) {
				$scope.invoice.ddate.value = moment().format(FULL_DATE_FORMAT);
				$scope.invoice.delivery_date_to = angular.copy($scope.request.delivery_date_to);
				$scope.invoice.delivery_date_from = angular.copy($scope.request.delivery_date_from);
				$scope.deliveryDateInputInvoice = moment().format(FULL_DATE_FORMAT);
				$scope.invoice.dstart_time1 = angular.copy($scope.request.dstart_time1);
				$scope.invoice.dstart_time2 = angular.copy($scope.request.dstart_time2);
				$scope.invoice.delivery_start_time = angular.copy($scope.request.delivery_start_time);
			}
		}

		function prepareLongDistanceInvoice() {
			$scope.longDistanceInvoice = true;

			if (!$scope.invoice.field_moving_to.premise) {
				GetAreaCode();
			}

			let longDistanceInfo = CalculatorServices.getLongDistanceInfo($scope.invoice);

			$scope.longDistanceRate = CalculatorServices.getLongDistanceRate($scope.invoice);
			//default Weight Tipe
			if (angular.isDefined($scope.invoice.field_cubic_feet) && !$scope.invoice.field_cubic_feet.value) {
				if ($scope.flatsettings.default_weight == 1) {
					$scope.invoice.field_cubic_feet.value = '1';
				} else {
					$scope.invoice.field_cubic_feet.value = '0';
				}
				updateMessage($scope.invoice.field_cubic_feet);
			}

			if (!$scope.invoice.field_long_distance_rate.value) {
				$scope.invoice.field_long_distance_rate.value = $scope.longDistanceRate || 0;
				updateMessage($scope.invoice.field_long_distance_rate);
			}

			$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
			$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));

			if (_.isUndefined($scope.invoice.extraServices)) {
				$scope.invoice.extraServices = additionalServicesFactory.getLongDistanceExtra($scope.invoice, $scope.request.request_all_data.min_weight);
			} else {
				if (_.isString($scope.invoice.extraServices)) {
					$scope.invoice.extraServices = angular.fromJson($scope.invoice.extraServices);
				}

				angular.forEach(additionalServicesFactory.getLongDistanceExtra($scope.invoice, $scope.request.request_all_data.min_weight), function (service) {
					if (!_.find($scope.invoice.extraServices, {'name': service.name})) {
						$scope.invoice.extraServices.push(service);
					}
				});

				additionalServicesFactory.saveExtraServices($scope.invoice.nid, $scope.invoice.extraServices);
			}

			if ($scope.invoice.inventory.move_details) {
				setLastDeliveryDay();
			}

			if (!_.get($scope.invoice, 'inventory.move_details.delivery_days')) {
				$scope.invoice.inventory.move_details.delivery_days = longDistanceInfo.days;
			}
		}

		function changeTypeOfServiceInvoice() {
			if (!$scope.Invoice) return;

			$scope.invoice.service_type = angular.copy($scope.request.service_type);

			$scope.flatrateInvoice = false;
			$scope.longDistanceInvoice = false;
			$scope.ifStorage = false;
			$scope.invoice_service_type = serviceTypeService.getCurrentServiceTypeName($scope.invoice);

			if (_.isUndefined($scope.invoice.request_all_data)) {
				initInvoice();
			}
			if ($scope.request.ld_job) {
				changeSalesClosingTab('closing');
			} else if ($scope.states.invoiceState && !$scope.states.salesState) {
				changeSalesClosingTab('sales');
			}

			switch (Number($scope.invoice.service_type.raw)) {
			case 5:
				prepareFlatRateInvoice();
				break;
			case 7:
				prepareLongDistanceInvoice();
				break;
			case 2:
			case 6:
				$scope.ifStorage = true;
				break;
			}

			calculateTime();
			enableExtra('field_extra_dropoff', 'extraDropoff');
			enableExtra('field_extra_pickup', 'extraPickup');
		}

		function changeRequestFieldServiceType(field, type) {
			let isStorageRequest = $scope.request.service_type.raw == SERVICE_TYPES.MOVING_AND_STORAGE
				|| $scope.request.service_type.raw == SERVICE_TYPES.OVERNIGHT;
			let isPackingDay = $scope.request.service_type.raw == SERVICE_TYPES.PACKING_DAY;
			let isInvoice = type === 'invoice';

			if (!_.get($scope.request, 'field_long_distance_rate.value')) {
				$scope.request.field_long_distance_rate.value = 0;
			}

			valuationService.reCalculateValuation($scope.request, $scope.invoice, type);

			if (!isInvoice) {
				if ($scope.request.service_type.raw == '5') {
					$scope.request.flat_rate_quote.value = 0;
				}
				if ($scope.request.service_type.raw == '7') {
					updateLongDistanceBySettings();
				}

				calculatePackingService.updateRequestDataPackings($scope.request);
				RequestServices.updateRequest($scope.request.nid, {
					request_data: {
						value: $scope.request.request_data.value
					}
				});
			}

			if (isInvoice) {
				changeTypeOfServiceInvoice();
				$scope.flatrateInvoice = $scope.invoice.service_type.raw == '5';
				$scope.longDistanceInvoice = $scope.invoice.service_type.raw == '7';

				if ($scope.flatrateInvoice && !$scope.request.ddate.value) {
					$scope.request.ddate.value = moment().format(DATE_FORMAT_SHORT);
				}
			}

			if (isStorageRequest) {
				let storageReqTo = false;
				let storageReqFrom = false;

				if (($scope.message[field].oldValue == ' Moving & Storage' || $scope.message[field].oldValue == ' Overnight') && $scope.request.storage_id) {
					storageReqTo = !$scope.request.request_all_data.toStorage;
					storageReqFrom = $scope.request.request_all_data.toStorage;
				}
				$scope.busy = true;
				CalcRequest.initStorageRequest($scope.request, storageReqTo, storageReqFrom)
					.finally(() => {
						$scope.busy = false;
					});
			}

			if(isPackingDay) {
				$scope.request.request_all_data.toStorage = false;
			}
		}

		function changeRequestFieldDDateSecond () {
			if ($scope.request.service_type.raw == 5) {
				$scope.request.ddate_second.value = angular.copy($scope.deliveryDateSecondInput);
				updateMessage($scope.request.ddate_second);
			}
		}

		function changeRequestFieldLongDistanceRate() {
			if ($scope.request.field_usecalculator.value) {
				$scope.request.field_usecalculator.value = false;

				if ($scope.Invoice) {
					$scope.invoice.field_usecalculator.value = false;
				}

				SweetAlert.swal({title: 'Warning!', text: 'Calculator was turned off', type: 'warning'});
			}

			if ($scope.Invoice) {
				longDistanceQuoteInvoiceChange();
				longDistanceQuoteRequestChange();
			}
		}

		function changeRequestFieldScheduleDeliveryDate (field) {
			if ($scope.request.status.raw != 3) return;

			$scope.request.schedule_delivery_date.value = angular.copy($scope.scheduleDeliveryDate);
			$scope.messageInvoice[field].oldValue = moment($scope.messageInvoice[field].oldValue).format(DATE_FORMAT_SHORT);
			$scope.$apply();
		}

		function changeRequestFieldDate (field, requestType) {
			if ($scope.request.status.raw != 3) return;

			$scope.moveDateInput = moment($scope.request.date.value, 'YYYY-MM-DD').format('MMMM D, YYYY');
			$scope.messageInvoice[field].oldValue = moment($scope.messageInvoice[field].oldValue).format('MMMM D, YYYY');
			let formatDate = ~$scope[requestType].date.value.indexOf('/') ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
			$scope.moveDateInputInvoice = moment($scope[requestType].date.value, formatDate).format('MMMM D, YYYY');
		}

		function changeRequestFieldTravelTime(field) {
			if ($scope.request.status.raw != 3) return;
			$scope.invoice[field].raw = _.round(common.convertStingToIntTime($scope.request[field].value), 2);

			if (field === 'field_double_travel_time') {
				$scope.invoice[field].value = angular.copy($scope.request[field].value);
				updateQuote();
			}
		}

		function changeFieldMoving () {
			if ($scope.request.status.raw != 3) return;

			CalculatorServices.updateLdMinWeightSettings($scope.request, true);
			$scope.invoice.field_moving_to = angular.copy($scope.request.field_moving_to);
			$scope.invoice.field_moving_from = angular.copy($scope.request.field_moving_from);
			CalculatorServices.updateLdMinWeightSettings($scope.invoice, true);
			updateFuelSurchargeRelatedToQuote();
		}

		function changeRequestFieldDDate (field) {
			if ($scope.request.status.raw != 3) return;

			$scope.deliveryDateInput = moment($scope.request.ddate.value).format('MMM DD, YYYY');
			if ($scope.invoice.ddate) {
				let formatDate = erDateFormatParser.getDeliveryDateFormat($scope.invoice.ddate.value);
				$scope.deliveryDateInputInvoice = $.datepicker.formatDate('MM d, yy', new Date(moment($scope.invoice.ddate.value, formatDate)), {});
			} else {
				$scope.deliveryDateInputInvoice = angular.copy($scope.deliveryDateInput);
			}
			$scope.messageInvoice[field].oldValue = moment($scope.messageInvoice[field].oldValue).format('MMMM D, YYYY');
			$scope.$apply();
		}

		function changeRequestFieldInventoryDetailsDelivery() {
			if ($scope.request.status.raw != 3) return;

			if (!_.get($scope.invoice, 'inventory.move_details.delivery')) {
				$scope.invoice.inventory.move_details = {};
			}
			$scope.invoice.inventory.move_details.delivery = angular.copy($scope.request.inventory.move_details.delivery);
		}

		function initInvoiceDate (typeRequest) {
			$scope.invoice.date = {};
			$scope.invoice.date = angular.copy($scope.request.date);
			$scope.invoice.ddate = angular.copy($scope.request.ddate);
			let formatDate = ~$scope.request.date.value.indexOf('/') ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
			let formatDDate = erDateFormatParser.getDeliveryDateFormat($scope.invoice.ddate.value);
			$scope.moveDateInputInvoice = $.datepicker.formatDate('MM d, yy', new Date(moment($scope.request.date.value, formatDate)), {});

			if (_.get($scope, `[${typeRequest}].ddate.value`)) {
				$scope.deliveryDateInputInvoice = $.datepicker.formatDate('MM d, yy', new Date(moment($scope[typeRequest].ddate.value, formatDDate)), {});
			} else {
				$scope.deliveryDateInputInvoice = $scope.moveDateInputInvoice;
			}
		}

		function changeRequestField(field) {
			let requestType = $scope.request.status.raw == 3 ? 'invoice' : 'request';
			if (field === 'field_size_of_move') field = TYPES.moveSize;
			if (requestType === 'invoice') {

				$scope.invoice[field] = angular.copy($scope.request[field]);
				$scope.editRequestinvoice.invoice[field] = angular.copy($scope.request[field]);

				if (field != TYPES.rate
					&& field != TYPES.crew
					&& field != TYPES.field_delivery_crew_size
					&& field != TYPES.rooms
					&& $scope.message[field]) {
					$scope.messageInvoice[field] = $scope.message[field];
				}

				if (!$scope.invoice.date) {
					initInvoiceDate(requestType);
				}
			}

			switch (field) {
			case TYPES.typeFrom:
			case TYPES.typeTo:
				reCalculateTimeAndRate();
				break;
			case TYPES.serviceType:
				changeRequestFieldServiceType(field, requestType);
				break;
			case TYPES.secondDeliveryDate:
				changeRequestFieldDDateSecond();
				break;
			case TYPES.ldRate:
				changeRequestFieldLongDistanceRate();
				break;
			case TYPES.travelTime:
			case TYPES.doubleDriveTime:
				changeRequestFieldTravelTime(field);
				break;
			case TYPES.scheduleDeliveryDate:
				changeRequestFieldScheduleDeliveryDate(field);
				break;
			case TYPES.date:
				changeRequestFieldDate(field, requestType);
				break;
			case TYPES.movingTo:
			case TYPES.movingFrom:
				changeFieldMoving();
				break;
			case TYPES.deliveryDate:
				changeRequestFieldDDate(field);
				break;
			case TYPES.delivery:
			case TYPES.deliveryDays:
				changeRequestFieldInventoryDetailsDelivery();
				break;
			case TYPES.moveSize:
				changeRequestAllDataOnMoveSize();
				break;
			}
		}

		function changeRequestAllDataOnMoveSize() {
			if ($scope.request.status.raw == CONFIRMED_REQUEST) return;
			let currentServiceType = _.get($scope.request, 'service_type.raw');
			let soughtValue = $scope.request.move_size.raw === '11' ? COMMERCIAL_QUOTE_INDEX : currentServiceType;
			let currentShowQuote =  _.get($scope.basicsettings, `showQuote[${soughtValue}]`, true);
			$scope.request.request_all_data.isDisplayQuoteEye = !currentShowQuote;
			$scope.request.request_all_data.showQuote = currentShowQuote;
			$scope.$broadcast('tryToEnableShowQuoteSetting', {
				current: $scope.request.status.raw,
				original: $scope.request.status.old,
			})
		}

		function calculateGrandTotalWithoutContractTaxDiscount(invoice, type) {
			var total = 0;

			if (angular.isDefined(invoice.rate)) {
				var totalTime = $scope.travelTime ? invoice.travel_time.raw : 0;
				if ($scope.isDoubleDriveTime) {
					totalTime = invoice.field_double_travel_time.raw;
				}
				totalTime += invoice.work_time_int;
				if (type == 'invoice') {
					if ($scope.request.request_all_data.totalHourlyCharge && $scope.Job_Completed && !$scope.serviceTypeInvoice) {
						total += $scope.request.request_all_data.totalHourlyCharge;
						$scope.invoiceLabor = $scope.request.request_all_data.totalHourlyCharge;
					} else {
						if (angular.isDefined(invoice.service_type)) {
							if (invoice.service_type.raw == '5') {
								total = CalcRequest.flatrateQuoteInvoiceChange($scope.invoice, $scope.request);
							} else if (invoice.service_type.raw == '7') {
								longDistanceQuoteInvoiceChange();
								total = $scope.longDistanceQuoteInvoice;
							} else {
								if (invoice.work_time_int) {
									total += getBaseGrandTotalLocalMove(invoice.work_time_int);
								}
							}
						}
					}
					total = total + parseFloat(calculateAddServicesInvoice()) + parseFloat(calculatePackingServicesInvoice());
					if (angular.isDefined($scope.request.request_all_data.contractInputs)) {
						total += _.sum($scope.request.request_all_data.contractInputs);
					}
				} else if (type == 'request') {
					if (angular.isDefined(invoice.service_type)) {
						if (invoice.service_type.raw == '5') {
							total = CalcRequest.flatrateQuoteInvoiceChange($scope.invoice, $scope.request);
						} else if (invoice.service_type.raw == '7') {
							longDistanceQuoteRequestChange(true);
							total = $scope.longDistanceQuote;
						} else {
							total = ($scope.calcSettings.min_hours > totalTime ? $scope.calcSettings.min_hours : totalTime) * parseFloat($scope.request.request_all_data.add_rate_discount || invoice.rate.value);
						}
					}
					total = total + parseFloat(calculateAddServicesInvoice()) + parseFloat(calculatePackingServicesInvoice());
				}
			}

			if (angular.isUndefined(invoice.request_all_data.add_money_discount)) {
				invoice.request_all_data.add_money_discount = 0;
			}

			if (angular.isUndefined(invoice.request_all_data.add_percent_discount)) {
				invoice.request_all_data.add_percent_discount = 0;
			}

			if (_.get(invoice.request_all_data, 'valuation.selected.valuation_charge')) {
				total = total + invoice.request_all_data.valuation.selected.valuation_charge;
			}

			if (invoice.request_all_data.surcharge_fuel) {
				total += invoice.request_all_data.surcharge_fuel;
			}

			if ($scope.contract_discount) {
				total = total - Number($scope.contract_discount);
			}

			let discount = invoice.request_all_data.add_money_discount + total * (invoice.request_all_data.add_percent_discount / 100);

			total = _.round(total - discount, 2);

			return total;
		}

		function grandTotalFunc(invoice, type, tips) {
			var total = calculateGrandTotalWithoutContractTaxDiscount(invoice, type);
			if (type == 'invoice') {
				let contractIsSubmit = _.get($scope.request, 'contract.isSubmitted') || _.get($scope.request, 'contract.isFirstSubmittedPage');
				//custom tax
				if ($scope.request.request_all_data.customTax && _.get($scope.request, 'contract.isSubmitted')) {
					total += $scope.request.request_all_data.customTax;
				} else if (_.get($scope.contract_page.paymentTax, 'customTax.state') && contractIsSubmit) {
					total = total * ($scope.contract_page.paymentTax.customTax.value / 100 + 1);
				}
				//credit tax
				total += $scope.request.request_all_data.creditTax ? $scope.request.request_all_data.creditTax : 0;
				//cash discount
				total -= $scope.request.request_all_data.cashDiscount ? $scope.request.request_all_data.cashDiscount : 0;
				// custom payment tax
				if ($scope.request.request_all_data.customPayments && Object.keys($scope.request.request_all_data.customPayments).length) {
					let payment;
					for (payment in $scope.request.request_all_data.customPayments) {
						total += $scope.request.request_all_data.customPayments[payment].value;
					}
				}
			}

			if (angular.isDefined(tips)) {
				total = total + Number(tips.value);
			}

			return _.round(total, 2);
		}

		function longDistanceQuoteInvoiceChange() {
			if (common.count($scope.invoice) > 1 && $scope.request.service_type.raw == 7) {
				if (angular.isDefined($scope.invoice.field_moving_to)) {
					$scope.longDistanceRate = CalculatorServices.getLongDistanceRate($scope.invoice);

					if (_.isUndefined($scope.invoice.field_long_distance_rate)
						|| _.isUndefined($scope.invoice.field_long_distance_rate.value)
						|| _.isNull($scope.invoice.field_long_distance_rate)
						|| _.isNull($scope.invoice.field_long_distance_rate.value)
					) {
						$scope.invoice.field_long_distance_rate = $scope.request.field_long_distance_rate;
						$scope.invoice.field_long_distance_rate.value = $scope.longDistanceRate || 0;
					}

					$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
					$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));
				}
			}
		}

		function longDistanceQuoteRequestChange(isAutoFunction) {
			if (angular.isDefined($scope.request.field_moving_to)) {
				$scope.longDistanceRate = CalculatorServices.getLongDistanceRate($scope.request);

				if (_.isUndefined($scope.request.field_long_distance_rate)
					|| _.isUndefined($scope.request.field_long_distance_rate.value)
					|| _.isNull($scope.request.field_long_distance_rate)
					|| _.isNull($scope.request.field_long_distance_rate.value)
				) {
					$scope.request.field_long_distance_rate.value = $scope.longDistanceRate || 0;
				}

				if (angular.isDefined($scope.request.field_useweighttype) && $scope.request.field_useweighttype.value) {
					$scope.longDistanceQuote = CalculatorServices.getLongDistanceQuote($scope.request);
					caclulateLongDistanceQuoteUp();
				}

				if (!isAutoFunction) {
					updateFuelSurchargeRelatedToQuote();
				}
			}
		}

		$scope.removeCompanyFlagPayrollDone = removeCompanyFlagPayrollDone;

		function removeCompanyFlagPayrollDone() {
			delete $scope.request.field_company_flags.value[importedFlags.company_flags[Payroll_Done]];
			var field_flags = _.keys($scope.request.field_company_flags.value);

			RequestServices.updateRequest($scope.request.nid, {
				field_company_flags: field_flags
			}).then(function (res) {
			});
		}

		$scope.addCompanyFlagPayrollDone = addCompanyFlagPayrollDone;

		function addCompanyFlagPayrollDone() {
			if (!$scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[Payroll_Done])) {
				var field_flags = _.keys($scope.request.field_company_flags.value) || [];
				field_flags.push(importedFlags.company_flags[Payroll_Done]);

				RequestServices.updateRequest($scope.request.nid, {
					field_company_flags: field_flags
				}).then(() => {
					var new_field = importedFlags.company_flags[Payroll_Done];

					if (_.isArray($scope.request.field_company_flags.value)) {
						$scope.request.field_company_flags.value = {};
					}
					$scope.request.field_company_flags.value[new_field] = Payroll_Done;
				});
			}
		}

		if (angular.isDefined($scope.invoice.request_all_data) && $scope.Invoice) {
			if (angular.isUndefined($scope.invoice.request_all_data.surcharge_fuel) || $scope.invoice.request_all_data.surcharge_fuel == 0) {
				$scope.invoice.request_all_data.surcharge_fuel_avg = $scope.request.request_all_data.surcharge_fuel_avg;
				$scope.invoice.request_all_data.surcharge_fuel_perc = $scope.request.request_all_data.surcharge_fuel_perc;
				$scope.invoice.request_all_data.surcharge_fuel = $scope.request.request_all_data.surcharge_fuel;
				$scope.request.request_all_data.invoice = angular.copy($scope.invoice);
			}
		}


		function updateAddPackingService(saveJustInvoice) {
			setUpPackingServices();
			let needToSave = false;

			function recalculateTotals(packings, rateName) {
				let laborOn = $scope.packing.labor && rateName == 'LDRate' ? 1 : 0;
				_.each(packings, function (packing) {
					packing.laborRate = packing.laborRate || 0;
					let newTotal = parseFloat(packing.quantity * (parseFloat(packing[rateName]) + laborOn * packing.laborRate)).toFixed(2);
					if (packing.total != newTotal) {
						packing.total = newTotal;
						needToSave = true;
					}
				});
			}

			if (!saveJustInvoice) {
				let rateName = $scope.request.service_type.raw == '5' || $scope.request.service_type.raw == '7' ?
					'LDRate' : 'rate';

				recalculateTotals(requestPacking, rateName);
				if ($scope.partialPacking || $scope.fullPacking) {
					var weight = CalculatorServices.getTotalWeight($scope.request);

					requestPacking[indexService].quantity = weight.weight;

					let newPrice = _.round(requestPacking[indexService].quantity * requestPacking[indexService][rateName], 2);
					if (requestPacking[indexService].total != newPrice) {
						requestPacking[indexService].total = newPrice;
						if ($scope.request.request_data.value == '') {
							$scope.request.request_data.value = {};
						}
						$scope.request.request_data.value.packings = requestPacking;

						if ($scope.Invoice) {
							if ($scope.invoice.request_data.value == '') {
								$scope.invoice.request_data.value = {};
							}
							let packingsForInvoice = angular.copy(requestPacking);
							$scope.invoice.request_data.value.packings = packingsForInvoice.filter(packing => {
								return packing.originName != NAME_PARTIAL_PACKING
									&& packing.originName != NAME_FULL_PACKING;
							});
							needToSave = true;
						}

						if (needToSave) {
							recalculateReservation();
							saveOnlyRequestAllData();
						}
						RequestServices.updateRequest($scope.request.nid, {
							request_data: {
								value: $scope.request.request_data.value
							}
						});
					}
				}
			} else {
				let invoicePacking = [];
				if (!_.isEmpty($scope.invoice.request_data)) {
					if (angular.isDefined($scope.invoice.request_data.value)) {
						if ($scope.invoice.request_data.value != '') {
							invoicePacking = angular.fromJson($scope.invoice.request_data.value).packings || [];
						}
					}
				}

				let partialInvoicePackingIndex = _.findIndex(invoicePacking, {name: NAME_PARTIAL_PACKING});
				let fullInvoicePackingIndex = _.findIndex(invoicePacking, {name: NAME_FULL_PACKING});
				let packingIndex;
				if (partialInvoicePackingIndex >= 0) {
					packingIndex = partialInvoicePackingIndex;
				}
				if (fullInvoicePackingIndex >= 0) {
					packingIndex = fullInvoicePackingIndex;
				}
				weight = CalculatorServices.getTotalWeight($scope.invoice);

				if (!_.isUndefined(invoicePacking[indexService])) {
					invoicePacking[indexService].quantity = weight.weight;//суём в сервис новый вес
				}

				let rateName = $scope.invoice.service_type.raw == '5' || $scope.invoice.service_type.raw == '7' ?
					'LDRate' : 'rate';

				recalculateTotals(invoicePacking, rateName);

				if (packingIndex >= 0) {
					invoicePacking[packingIndex].quantity = weight.weight;
					let newPrice = _.round(invoicePacking[packingIndex].quantity * invoicePacking[packingIndex].rate, 2);
					if (invoicePacking[packingIndex].total != newPrice) {
						invoicePacking[packingIndex].total = newPrice;
						var invData = $scope.invoice.request_data.value != '' ? angular.fromJson($scope.invoice.request_data.value) : {};
						$scope.invoice.request_data.value = invData;
						needToSave = true;
					}
					if (needToSave) {
						recalculateReservation();
						saveOnlyRequestAllData();
					}
				}
			}
		}

		function saveRequestAllData() {
			$scope.request.request_all_data.req_tips = Number($scope.tips.value);
			recalculateReservation();
			saveOnlyRequestAllData();
		}

		$scope.goToDay = function (type) {
			var newDate = '';

			if (type == 'prev') {
				newDate = moment($scope.curDate).subtract(1, 'days').format('MM/DD/YYYY');
			} else {
				newDate = moment($scope.curDate).add(1, 'days').format('MM/DD/YYYY');
			}

			$scope.parklotControl.reinitParklot(newDate);
		};

		function loadPairedRequest() {
			if (_.isEqual($scope.request.storage_id, 0) && !$scope.request.request_all_data.baseRequestNid) {
				$scope.pairedRequest = undefined;
			} else {
				var storageNid = $scope.request.storage_id ? $scope.request.storage_id : $scope.request.request_all_data.baseRequestNid;
				var promise = RequestServices.getSuperRequest(storageNid);
				$scope.busy = true;

				promise.then(function (data) {
					var request = data.request;
					$scope.busy = false;
					$scope.pairedRequest = request;
					$scope.homeEstimatePermission = $location.path().includes('inhome-estimator')
						&& !_.isEqual($scope.pairedRequest.home_estimator.value, $scope.request.home_estimator.value);
					initMoveDateOfPairedRequest();
					prepareMovingStorageRate();
				}, function (reason) {
					logger.error(reason, reason, 'Error');
					$scope.busy = false;
				});
			}
		}

		function initMoveDateOfPairedRequest() {
			$scope.pairedRequestDate = moment($scope.pairedRequest.date.value, 'MM/DD/YYYY').format(FULL_DATE_FORMAT);
		}

		$scope.changeToFromMoveStorage = function () {
			if ($scope.ifStorage) {
				var field_moving_to = angular.copy($scope.request.field_moving_to);
				$scope.request.field_moving_to = angular.copy($scope.request.field_moving_from);
				$scope.request.field_moving_from = field_moving_to;

				$scope.editrequest.field_moving_to = $scope.request.field_moving_to;
				$scope.editrequest.field_moving_from = $scope.request.field_moving_from;

				if ($scope.request.request_all_data.toStorage) {
					$scope.editrequest.field_to_storage_move = 0;
					$scope.editrequest.field_from_storage_move = $scope.request.storage_id;
				} else {
					$scope.editrequest.field_to_storage_move = $scope.request.storage_id;
					$scope.editrequest.field_from_storage_move = 0;
				}

				$scope.editrequest.field_to_storage_temp = !$scope.request.request_all_data.toStorage;
				$scope.request.request_all_data.toStorage = !$scope.request.request_all_data.toStorage;

				initServiceType();
				prepareMessageForMoveStorage($scope.editrequest);
			}
		};

		function prepareMessageForMoveStorage(editRequest) {

			for (var type_field in editRequest) {
				if (_.isEqual(type_field, 'field_extra_pickup')
					|| _.isEqual(type_field, 'field_extra_dropoff')
					|| _.isEqual(type_field, 'field_moving_from')
					|| _.isEqual(type_field, 'field_moving_to')) {

					$scope.message[type_field] = {};
					var label = '';
					var words = _.words(type_field.replace(/_/g, ' '));

					_.forEach(words, function (word, i) {
						if (i > 0) {
							word = word.replace(/\w\S*/g, function (txt) {
								return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
							});
							label += word + ' ';
						}
					});

					$scope.message[type_field].label = label;

					var message = '';
					if (type_field == 'field_extra_pickup'
						|| type_field == 'field_extra_dropoff') {

						message = makeMessageForMoveField(editRequest[type_field]);
					} else if (type_field == 'field_moving_from') {
						message = makeMessageForMoveField(editRequest[type_field]);
					} else if (type_field == 'field_moving_to') {
						message = makeMessageForMoveField(editRequest[type_field]);
					}

					$scope.message[type_field].newValue = message;
				}
			}
		}

		function makeMessageForMoveField(editrequest) {
			var message = '';
			var fields = ['thoroughfare', 'premise', 'locality', 'administrative_area', 'postal_code'];

			for (var i = 0; i < fields.length; i++) {
				if (editrequest[fields[i]]
					&& editrequest[fields[i]] != null
					&& editrequest[fields[i]] != '') {

					switch (fields[i]) {
					case 'thoroughfare':
					case 'premise':
					case 'locality':
						message += editrequest[fields[i]] + ' ';
						break;
					case 'administrative_area':
						message += editrequest.administrative_area;
						break;
					case 'postal_code':
						message += ', ' + editrequest.postal_code;
						break;
					}
				}
			}

			return message;
		}

		$scope.grandTotal = 0;

		if ($scope.longDistance) {
			$scope.grandTotal = calculateGrandTotalBalance('longdistance');
		}

		if ($scope.flatrate) {
			$scope.grandTotal = calculateGrandTotalBalance('flatrate');
		}

		if (!$scope.longDistance && !$scope.flatrate) {
			$scope.grandTotal = calculateGrandTotalBalance('min') + ' - $' + calculateGrandTotalBalance('max');
		}

		$scope.grandTotalInvoice = 0;

		if ($scope.Invoice) {
			$scope.grandTotalInvoice = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
		}

		$scope.openStorageRequest = function (id) {
			$scope.busy = true;
			StorageService.getStorageReqByID(id).then(function ({data}) {
				$scope.busy = false;
				StorageService.openModal(data, id);
			});
		};

		function prepareMovingStorageRate() {
			let now = $scope.request.date.raw;

			if (angular.isDefined($scope.pairedRequest)
				&& ($scope.request.storage_id != 0 || $scope.request.request_all_data.baseRequestNid)) {
				var storageNid = $scope.request.storage_id ? $scope.request.storage_id : $scope.request.request_all_data.baseRequestNid;
				if ($scope.pairedRequest.nid == storageNid) {
					setStorageRate();
				} else {
					RequestServices.getRequestsByNid(storageNid).then(data => {
						$scope.pairedRequest = data.nodes[0];

						setStorageRate();
					});
				}
			}

			function setStorageRate() {
				if ($scope.request.service_type.raw == 2) {
					let then = $scope.pairedRequest.date.raw;

					if (!$scope.request.request_all_data.toStorage) {
						$scope.interval = (then - now) / 86400;
					} else {
						$scope.interval = (now - then) / 86400;
					}

					$scope.storage_rate = CalculatorServices.getStorageRate($scope.request, $scope.interval);
					$scope.interval = CalculatorServices.convertDateIntoMonths($scope.interval);
				} else if ($scope.pairedRequest) {
					$scope.overnightRate = CalculatorServices.getOvernightRate($scope.pairedRequest);
				}
			}
		}

		function revertChangeFlatRateLocalMoveMessage(type) {
			if (type == 'request') {
				delete $scope.editrequest.dontShowDelirery;
				delete $scope.message.dontShowDelirery;
				delete $scope.editrequest.field_flat_rate_local_move;
			} else {
				delete $scope.messageInvoice.dontShowDelirery;
				delete $scope.editRequestinvoice.invoice.dontShowDelirery;
				delete $scope.editRequestinvoice.invoice.field_flat_rate_local_move;
			}
		}

		function setFlatRateLocalMoveMessage(type) {
			var newMsg = {
				label: 'Flat Rate Local Move',
				newValue: $scope.request.field_flat_rate_local_move.value ? 'On' : 'Off',
				oldValue: $scope.request.field_flat_rate_local_move.old ? 'On' : 'Off'
			};
			if (type == 'request') {
				$scope.editrequest.dontShowDelirery = {};
				$scope.message.dontShowDelirery = newMsg;
				$scope.editrequest.field_flat_rate_local_move = $scope.request.field_flat_rate_local_move.value;
			} else {
				$scope.messageInvoice.dontShowDelirery = newMsg;
				$scope.editRequestinvoice.invoice.dontShowDelirery = {};
				$scope.invoice.field_flat_rate_local_move = $scope.request.field_flat_rate_local_move.value;
				$scope.editRequestinvoice.invoice.field_flat_rate_local_move = $scope.request.field_flat_rate_local_move.value;
			}
		}

		function changeFlatRateLocalMove(type) {
			if ($scope.request.field_flat_rate_local_move.value != $scope.request.field_flat_rate_local_move.old) {
				setFlatRateLocalMoveMessage(type);
			} else {
				revertChangeFlatRateLocalMoveMessage(type);
			}
		}

		function changeSalesClosingTab(type) {
			if (type == 'sales') {
				$scope.states.invoiceState = false;
				$scope.states.salesState = true;
				$rootScope.$broadcast('sales.closing.tab', type);
			}
			if (type == 'closing') {
				let hasUpates;
				if (!_.isEmpty($scope.editrequest) && $scope.request.status.old != 3) {
					hasUpates = _updateRequest();
				}
				if (angular.isUndefined(hasUpates)) {
					$scope.states.invoiceState = true;
					$scope.states.salesState = false;
					$rootScope.$broadcast('sales.closing.tab', type);
				}
			}
			$rootScope.$broadcast('isClosing', $scope.states.invoiceState);
		}

		$scope.$watch('state.invoiceState', () => {
			$rootScope.$broadcast('isClosing', $scope.states.invoiceState);
		});

		function openTrip(id) {
			var tripPage = '/moveBoard/#/lddispatch/trip-planner/';
			var tab = '/0';
			var link = tripPage + id + tab;
			window.open(link, '_blank');
		}

		function openBindingRequest(nid) {
			$scope.busy = true;

			openRequestService.open(nid)
				.finally(() => {
					$scope.busy = false;
				});
		}

		function updateOneWayRequestArea(updateData) {
			$scope.request[updateData.current] = angular.copy(updateData.originalArea[updateData.original]);
			$scope.editrequest[updateData.current] = $scope.request[updateData.current];
			$scope.message[updateData.current] = {
				label: updateData.label,
				newValue: makeMessageForMoveField($scope.editrequest[updateData.current])
			};
		}

		function changeServiceTypeToLD() {
			SweetAlert.swal({
				title: 'Seems that the distance is more suitable for Long Distance',
				text: 'Do you want to change the service type for this request?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: 'No',
				closeOnConfirm: true,
				closeOnCancel: true
			}, (confirm) => {
				if (!confirm) return;
				$scope.request.service_type.raw = 7;
				$scope.request.service_type.value = _.find($scope.services, service => service.id == $scope.request.service_type.raw).title;
				updateMessage($scope.request.service_type);
				changeRequestField('field_move_service_type');
				changeTypeOfServiceInvoice();
			});
		}

		$scope.$on('weightTypeChanged', onChangeWeightType);

		function onChangeWeightType() {
			updateMessage($scope.request.field_useweighttype);
			$scope.message[$scope.request.field_useweighttype.field] = {
				label: 'custom',
				oldValue: '',
				newValue: 'Weight Type was changed',
			};

			//Recaclulate All DATA
			if ($scope.request.field_useweighttype.value == 1) {

				if ($scope.request.move_size.raw != 11) {
					$scope.request.total_weight = CalculatorServices.getRequestCubicFeet($scope.request);
					$scope.request.visibleWeight = $scope.request.total_weight.weight;
				} else {
					calculateCommercial($scope.request, $scope.calcSettings);
				}

			} else if ($scope.request.field_useweighttype.value == 2) {
				$scope.request.inventory_weight = InventoriesServices.calculateTotals($scope.request.inventory.inventory_list);
				$scope.request.visibleWeight = $scope.request.inventory_weight.cfs;
			} else {
				$scope.request.visibleWeight = $scope.request.custom_weight.value;
			}

			if ($scope.request.field_usecalculator.value) {
				calculateTime();
			}

			if ($scope.Invoice) {
				if (!$scope.invoice.field_useweighttype) {
					$scope.invoice.field_useweighttype = {};
				}
				$scope.invoice.field_useweighttype.value = $scope.request.field_useweighttype.value;

				if ($scope.invoice.field_useweighttype.value == 2) {
					$scope.invoice.closing_weight.value = InventoriesServices.calculateClosingWeight($scope.request);
				} else {
					$scope.invoice.closing_weight.value = $scope.request.visibleWeight;
				}

				if ($scope.request.service_type.raw == 7) {
					$scope.invoice.field_long_distance_rate.value = $scope.request.field_long_distance_rate.value;
					$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
					$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));
				}
			}

			updateLongDistanceRate();
			updateAddPackingService();
			recalculateLongDistanceExtraServices();
		}

		function onChangeUseWeightTypeInvoice() {
			if ($scope.Invoice) {
				if ($scope.invoice.field_useweighttype.value == 2) {
					$scope.invoice.closing_weight.value = InventoriesServices.calculateClosingWeight($scope.request);
				} else {
					$scope.invoice.closing_weight.value = $scope.request.visibleWeight;
				}

				if ($scope.request.service_type.raw == 7) {
					$scope.invoice.field_long_distance_rate.value = $scope.request.field_long_distance_rate.value;
					$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
					$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));
				}
			}

			updateLongDistanceRate();
			updateAddPackingService();
			recalculateLongDistanceExtraServices();
		}


		$scope.$watch('request', function (newVal, oldVal) {

			//Reservation Price
			if ((newVal.reservation_rate.value != oldVal.reservation_rate.value
					|| $scope.request.reservation_rate.old == '-1.00')
				&& !$scope.request.field_reservation_received.value
				&& !$scope.request.request_all_data.reservationIncreased
				&& !isUpdateReservation) {

				if ($scope.request.reservation_rate.old == '-1.00') {
					if (newVal.reservation_rate.value != -1) {
						// do not remove 'isUpdating' else get 'out of memory' error
						isUpdateReservation = true;
						RequestServices.updateRequest($scope.request.nid, {
							field_reservation_price: newVal.reservation_rate.value
						}).then(function () {
							$scope.request.reservation_rate.old = parseInt(newVal.reservation_rate.value).toString();
							isUpdateReservation = false;
						});
					} else if ($scope.request.reservation_rate.value == '-1.00') {
						grandTotalService.getReservationRate($scope.request);
					}
				} else {
					updateMessage($scope.request.reservation_rate);
				}
			}

			// Invoice work time change
			if (angular.isDefined(newVal.work_time)
				&& newVal.work_time != oldVal.work_time) {

				$scope.invoice.work_time = _.round(common.convertStingToIntTime(newVal.work_time), 2);
			}

			if ($scope.invoice.maximum_time) {
				$scope.total_time = common.decToTime(Number($scope.invoice.maximum_time.raw) + Number($scope.travelTime ? $scope.invoice.travel_time.raw : 0));
			}

			if (newVal.service_type.raw != oldVal.service_type.raw) {
				updateAddPackingService();
				setisDoubleDriveTime();
				calcDistance();
				initData();

				$scope.request.request_all_data.reservationIncreased = false;
				$scope.request.request_all_data.reservationChangedManually = false;

				let isRequestHasPackingDay = $scope.request.request_all_data.packing_request_id
					&& $scope.request.service_type.raw != 8;

				if (isRequestHasPackingDay) {
					RequestServices.UpdatePackingRequest($scope.request.request_all_data.packing_request_id, {
						serviceType: newVal.service_type.value,
						status: $scope.request.status.value,
						moveDate: moment($scope.request.date.value).format('MMM DD, YYYY')
					});
				}

				if (newVal.service_type.raw == 3 && oldVal.service_type.raw == 4) {
					$scope.request.apt_from.value = angular.copy(oldVal.apt_to.value);
					updateMessage($scope.request.apt_from);
					removeMessage($scope.request.apt_to);
					updateOneWayRequestArea({
						originalArea: oldVal,
						original: 'field_moving_to',
						current: 'field_moving_from',
						label: 'Moving From'
					});
				}

				if (newVal.service_type.raw == 4 && oldVal.service_type.raw == 3) {
					$scope.request.apt_to.value = angular.copy(oldVal.apt_from.value);
					updateMessage($scope.request.apt_to);
					removeMessage($scope.request.apt_from);
					updateOneWayRequestArea({
						originalArea: oldVal,
						original: 'field_moving_from',
						current: 'field_moving_to',
						label: 'Moving To'
					});
				}

				$scope.request.field_quote_explanationn = angular.copy($scope.quoteSettings.quoteExplanation[newVal.service_type.raw]);

				if (newVal.service_type.raw == 6) {
					RequestHelperService.addOvernightExtraService($scope.request);
				} else {
					RequestHelperService.removeOvernightExtraService($scope.request);

					if (!_.isUndefined($scope.pairedRequest)) {
						RequestHelperService.removeOvernightExtraService($scope.pairedRequest);
					}
				}

				if (newVal.service_type.raw == 7) {
					getNewTravelTime();
				}

				grandTotalService.getReservationRate($scope.request);
			}

			if (newVal.field_long_distance_rate.value != oldVal.field_long_distance_rate.value || newVal.flat_rate_quote.value != oldVal.flat_rate_quote.value) {
				if ($scope.request.field_long_distance_rate.value == $scope.request.field_long_distance_rate.old) {
					removeMessage($scope.request.field_long_distance_rate);
				} else {
					updateMessage($scope.request.field_long_distance_rate);
				}

				updateFuelSurchargeRelatedToQuote();
				recalculateReservation();
				updateLDTooltip();
			}

			if (!_.isEqual(newVal.move_size.raw, oldVal.move_size.raw)
				|| !_.isEqual(newVal.rooms.value, oldVal.rooms.value)
				|| !_.isEqual(newVal.commercial_extra_rooms.value, oldVal.commercial_extra_rooms.value)) {

				let oldWeight = $scope.request.visibleWeight;
				initRequestWeight();
				let weightChanged = $scope.request.visibleWeight != oldWeight;
				if ($scope.longDistance) {
					recalculateLongDistanceExtraServices();
					if (weightChanged) {
						updateLongDistanceRate();
					}
				}
				if ($scope.ifStorage) {
					prepareMovingStorageRate();
				}

				updateAddPackingService();
				updateFuelSurchargeRelatedToQuote();
			}

			if (newVal.field_useweighttype.value != oldVal.field_useweighttype.value) {
				onChangeWeightType();
			}

			//CUSTOM WEIGHT
			if (newVal.custom_weight.value != oldVal.custom_weight.value) {
				if (newVal.field_useweighttype.value == 2) return;
				$scope.request.visibleWeight = $scope.request.custom_weight.value;
				updateMessage($scope.request.custom_weight);

				if ($scope.Invoice) {
					if ($scope.invoice.field_useweighttype.value == 2) {
						$scope.invoice.closing_weight.value = InventoriesServices.calculateClosingWeight($scope.request);
					} else {
						$scope.invoice.closing_weight.value = $scope.request.visibleWeight;
					}

					$scope.invoice.field_long_distance_rate.value = $scope.request.field_long_distance_rate.value;
					$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
					$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));
				}

				if ($scope.request.field_usecalculator.value) {
					calculateTime();
				}
				updateAddPackingService();
				recalculateLongDistanceExtraServices();
			}

			//TURN ON OR OFF AUTO CALCULATOR
			if (newVal.field_usecalculator.value != oldVal.field_usecalculator.value) {
				onChangeUseCalculator();
			}

			//STATUS
			if (newVal.status.raw != oldVal.status.raw) {
				if (+$scope.request.reservation_rate.old != +$scope.request.reservation_rate.value
					&& !$scope.request.field_reservation_received.value
					&& !$scope.request.request_all_data.reservationIncreased
					&& !isUpdateReservation) {
					updateMessage($scope.request.reservation_rate);
				}

				let isRequestHasPackingDay = $scope.request.request_all_data.packing_request_id
					&& $scope.request.service_type.raw != 8;

				increaseReservationPrice();

				if (isRequestHasPackingDay) {
					RequestServices.UpdatePackingRequest($scope.request.request_all_data.packing_request_id, {
						serviceType: $scope.request.service_type.value,
						status: newVal.status.value,
						moveDate: moment($scope.request.date.value).format('MMM DD, YYYY')
					});
				}

				//uppdate status
				var newStatus = `status_${newVal.status.raw}`;
				var oldStatus = `status_${oldVal.status.raw}`;
				angular.element('.modal')
					.removeClass(oldStatus)
					.addClass(newStatus);
				angular.element('#event_' + oldVal.nid)
					.removeClass(oldStatus)
					.addClass(newStatus);

				$scope.Invoice = $scope.request.status.raw == 3;

				if (newVal.status.raw == 3) {
					initInvoice(true);
					setCustomConfirmationText();
				}

				if (newVal.status.raw == 4) {
					if (!$scope.request.home_estimate_date_created.value) {
						$scope.request.home_estimate_date_created.value = moment().format(FULL_DATE_FORMAT);
						updateMessage($scope.request.home_estimate_date_created);
					}

					if (!$scope.request.home_estimate_assigned.value) {
						let uid = _.get($rootScope, 'currentUser.userId.uid');
						$scope.request.home_estimate_assigned.value = uid;
						updateMessage($scope.request.home_estimate_assigned);
					}
				} else {
					if (!$scope.request.home_estimate_date_created.old) {
						removeMessage($scope.request.home_estimate_date_created);
					}

					if (!$scope.request.home_estimate_assigned.old) {
						removeMessage($scope.request.home_estimate_assigned);
					}
				}

				if (!$scope.flatrate && !$scope.longDistance) {
					recalculateReservation();
				}
			}

			//ZIP CHANGE
			if (newVal.field_extra_pickup.postal_code != oldVal.field_extra_pickup.postal_code) {
				$scope.request.extraPickup = false;

				$scope.request.extraPickup = !_.isNull($scope.request.field_extra_pickup.postal_code)
					&& $scope.request.field_extra_pickup.postal_code.length == 5;

				if ($scope.longDistance) {
					_ld_calcExtraPickup();
				} else {
					calcDistance();
				}
			}

			if (newVal.field_extra_dropoff.postal_code != oldVal.field_extra_dropoff.postal_code) {
				//EXTRA DROP OF OR PICKUP INIT
				$scope.request.extraDropoff = !_.isNull($scope.request.field_extra_dropoff.postal_code)
					&& $scope.request.field_extra_dropoff.postal_code.length == 5;

				if ($scope.longDistance) {
					_ld_calcExtraDropof();
				} else {
					calcDistance();
				}
			}
			//ZIP CHANGE
			if (newVal.field_moving_from.postal_code != oldVal.field_moving_from.postal_code
				|| newVal.field_moving_to.postal_code != oldVal.field_moving_to.postal_code) {
				getNewTravelTime();

				//3. Change WorkTime -> Change Quote
				calcDistance();

				if ($scope.request.service_type.raw == 7) {
					GetAreaCode();
				}
			}

			// ENtarance CHANGE
			if (newVal.type_from.raw != oldVal.type_from.raw || newVal.type_to.raw != oldVal.type_to.raw) {
				recalculateLongDistanceExtraServices();
			}

			//RATE, MAXTIME CHANGE
			if (newVal.rate.value != oldVal.rate.value) {
				updateQuote();
				recalculateReservation();
			}

			if (newVal.maximum_time.raw != oldVal.maximum_time.raw
				|| newVal.minimum_time.raw != oldVal.minimum_time.raw) {
				changeMaximumWorkTime(newVal, oldVal);
			}

			let trucksChange = newVal.truckCount != oldVal.truckCount;
			if (trucksChange) {

				let trucks = newVal.truckCount;
				if (trucks < 1) {
					trucks = 1;
				}

				if (!$scope.request.field_usecalculator.value) {
					let addTruckRate = CalculatorServices.getAdditionalTruckRate($scope.request.date.value, $scope.request.crew.value, trucks);
					$scope.request.rate.value = +$scope.request.rate.value;
					$scope.request.rate.value += addTruckRate * (newVal.truckCount - oldVal.truckCount);
					if ($scope.request.rate.value < 0) $scope.request.rate.value = addTruckRate;
					$scope.request.rate.highlighted = true;
					updateMessage($scope.request.rate);
					$scope.invoice.rate = angular.copy($scope.request.rate);
					updateMessage($scope.invoice.rate);
				}
			}

			//CREW CHANGE -> CHANGE RATE
			let crewChange = newVal.crew.value != oldVal.crew.value;
			if (crewChange) {
				//minimum 1 truck , if parklot has 2 trucks add rate
				let {trucks} = CalculatorServices.calculateTime($scope.request, $scope.request.total_weight);

				if (trucks < 1) {
					trucks = 1;
				}

				if (!$scope.Invoice) {
					updateRate(trucks, newVal);
				}

				if (!$scope.request.field_usecalculator.value) {
					trucks = newVal.truckCount || 1;
					$scope.request.rate.value = CalculatorServices.getRate($scope.request.date.value, $scope.request.crew.value, trucks);
				}

				$scope.request.rate.highlighted = true;

				updateMessage($scope.request.rate);
				updateMessage($scope.request.crew);
				$scope.invoice.rate = angular.copy($scope.request.rate);
				$scope.invoice.crew = angular.copy($scope.request.crew);
				$scope.messageInvoice['field_movers_count'] = {
					label: $scope.invoice.crew.label,
					oldValue: oldVal.crew.value,
					newValue: newVal.crew.value,
				};
				if ($scope.request.service_type.raw != 7) {
					$scope.messageInvoice['field_price_per_hour"'] = {
						label: $scope.invoice.rate.label,
						oldValue: $scope.invoice.rate.old,
						newValue: $scope.invoice.rate.value,
					};
				}

				updateMessage($scope.invoice.rate);
				updateMessage($scope.invoice.crew);
			}

			if (newVal.storage_id != oldVal.storage_id) {
				if (_.isUndefined($scope.pairedRequest)
					|| _.isEqual(newVal.storage_id, 0)) {
					loadPairedRequest();
				} else {
					initMoveDateOfPairedRequest();
				}
			}

			if (!_.isEqual(newVal.date, oldVal.date)
				&& ($scope.request.status.raw == 1
					|| $scope.request.status.raw == 2
					|| $scope.Invoice)
				&& $scope.request.updateRequestDate) {
				let isRequestHasPackingDay = $scope.request.request_all_data.packing_request_id
					&& $scope.request.service_type.raw != 8;

				if (isRequestHasPackingDay) {
					RequestServices.UpdatePackingRequest($scope.request.request_all_data.packing_request_id, {
						serviceType: $scope.request.service_type.value,
						status: $scope.request.status.value,
						moveDate: moment(newVal.date.value).format('MMM DD, YYYY')
					});
				}

				$scope.request.updateRequestDate = false;

				_.each($scope.request.trucks.raw, function (truck) {
					$scope.chooseTruck(truck, true);
				});

				$scope.request.trucks.raw.length = 0;

				calculateTime();
			}

			if (!_.isEqual(newVal.schedule_delivery_date, oldVal.schedule_delivery_date)) {
				if (newVal.schedule_delivery_date) {
					if (newVal.schedule_delivery_date.value) {
						var formatDate = newVal.schedule_delivery_date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
						$scope.scheduleDeliveryDate = moment(newVal.schedule_delivery_date.value, formatDate).format(FULL_DATE_FORMAT);
					}
				} else {
					$scope.scheduleDeliveryDate = $.datepicker.formatDate('MM d, yy', new Date(newVal.schedule_delivery_date.value), {});
				}
				$scope.changeScheduleDeliveryDate();
			}

			if (!_.isEqual(newVal.inventory.move_details, oldVal.inventory.move_details)) {
				//Delivery Window

				let linfo = CalculatorServices.getLongDistanceInfo($scope.request);

				if (angular.isDefined($scope.request.inventory.move_details) && $scope.request.inventory.move_details != null) {
					setLastDeliveryDay();
					//delivery days
					if (angular.isUndefined($scope.request.inventory.move_details.delivery_days)) {
						$scope.request.inventory.move_details.delivery_days = linfo.days;
					}
				} else {
					if (angular.isUndefined($scope.request.inventory.move_details)) {
						$scope.request.inventory.move_details = {};
						$scope.request.inventory.move_details.delivery_days = linfo.days;
					}
				}
			}

			if (!_.isEqual(newVal.request_all_data, oldVal.request_all_data)
				&& !_.isUndefined(newVal.request_all_data.add_rate_discount)) {
				calculateRatediscount();
			}


			if (!_.isEqual(newVal.travel_time, oldVal.travel_time)
				|| !_.isEqual(newVal.field_double_travel_time, oldVal.field_double_travel_time)) {
				if ($scope.updateTruck) {
					$scope.updateTruck();
				}
			}


			if (newVal.ldStatus !== oldVal.ldStatus) {
				$scope.message['ldStatus'] = {
					label: 'Ld status',
					oldValue: oldVal.ldStatus,
					newValue: newVal.ldStatus,
				};
			}
			if (!_.isEqual(newVal.storage_id, oldVal.storage_id)) {
				loadPairedRequest();

				if ($scope.ifStorage) {
					prepareMovingStorageRate();
				}
			}

			if (!_.isEqual(newVal.email, oldVal.email)) {
				setUpEmailParameters();
			}
		}, true);

		function changeMaximumWorkTime(current, original) {
			if ($scope.request.field_usecalculator.value && $scope.request.timeChangeManually) {
				SweetAlert.swal({
					title: 'Are you sure you want to set time manualy?',
					text: 'Calculator will be turned off.',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, let\'s do it!',
					cancelButtonText: 'No, cancel pls!',
					closeOnConfirm: true,
					closeOnCancel: true
				}, setTimeManually);
			} else {
				if (current.maximum_time.raw != original.maximum_time.raw) {
					$scope.parklotControl.updateWorkTime($scope.request.maximum_time, current.maximum_time.value);
				}
				if (current.minimum_time.raw != original.minimum_time.raw) {
					$scope.parklotControl.updateWorkTime($scope.request.minimum_time, current.minimum_time.value);
				}
			}
			updateQuote();

			function setTimeManually (confirm) {
				if (confirm) {
					$scope.request.field_usecalculator.value = false;
					updateMessage($scope.request.field_usecalculator);
				} else {
					$scope.request.maximum_time.value = original.maximum_time.old;
					$scope.request.minimum_time.value = original.minimum_time.old;
					$scope.request.timeChangeManually = false;
					removeMessage($scope.request.maximum_time);
					$scope.$broadcast('re-render-parklot');
				}
			}
		}

		function reCalculateTimeAndRate () {
			calculateTime();

			if (_.get($scope, 'calcResults.truckCount') != getTrucksCount()) {
				updateRate();
				$scope.choosen_truck = [];
				validateTrucks();
				$scope.parklotControl.reinitParklot($scope.request.date.value);
			}
		}

		function onChangeUseCalculator() {
			if ($scope.request.field_usecalculator.value) {
				$scope.request.timeChangeManually = true;
				reCalculateTimeAndRate();
			}
			$scope.request.timeChangeManually = false;

			if ($scope.request.field_usecalculator.value) {
				$scope.editrequest['field_usecalculator'] = '1';
				$scope.message['field_usecalculator'] = {
					label: "custom",
					oldValue: '',
					newValue: "Calculator was turned on",
				};
			} else {
				$scope.request.truckCount = $scope.request.trucks.raw.length || 1;
				$scope.editrequest['field_usecalculator'] = '0';
				$scope.message['field_usecalculator'] = {
					label: "custom",
					oldValue: '',
					newValue: "Calculator was turned off",
				};
			}
		}

		function getNewTravelTime() {
			CalculatorServices.getDuration($scope.request.field_moving_from.postal_code, $scope.request.field_moving_to.postal_code, $scope.request.service_type.raw)
				.then(function (data) {
					let duration = data.distances.AB.duration;
					distancesABDuration = data.distances.AB.duration;
					let travelTime = data.duration;
					let parseTavelTime = common.decToTime(travelTime);
					$scope.request.travel_time.value = parseTavelTime;
					$scope.request.travel_time.raw = travelTime;

					if ($scope.request.travel_time.old != parseTavelTime) {
						updateMessage($scope.request.travel_time);
						$rootScope.$broadcast('check.overbooking');
					} else {
						removeMessage($scope.request.travel_time);
					}

					if ($scope.request.distance.old != data.distances.AB.distance) {
						$scope.request.distance.value = data.distances.AB.distance;
						recalculateEquipmentFee();
						updateMessage($scope.request.distance);
					} else {
						$scope.request.distance.value = data.distances.AB.distance;
						removeMessage($scope.request.distance);
					}

					if ($scope.isDoubleDriveTime) {
						let maxTravelTime = Math.max(minCATravelTime, duration * 2);
						maxTravelTime = CalculatorServices.getRoundedTime(maxTravelTime);
						if ($scope.request.field_double_travel_time.old != common.decToTime(maxTravelTime)) {
							$scope.request.field_double_travel_time.raw = maxTravelTime;
							$scope.request.field_double_travel_time.value = common.decToTime(maxTravelTime);
							updateMessage($scope.request.field_double_travel_time);
						} else {
							$scope.request.field_double_travel_time.raw = maxTravelTime;
							$scope.request.field_double_travel_time.value = common.decToTime(maxTravelTime);
							removeMessage($scope.request.field_double_travel_time);
						}
					}

					if ($scope.request.duration.old != duration) {
						$scope.request.duration.value = duration;
						updateMessage($scope.request.duration);
					} else {
						$scope.request.duration.value = duration;
						removeMessage($scope.request.duration);
					}

					// ParserFLoat Gluk
					let flatRate = $scope.request.service_type.raw == 5 || $scope.request.service_type.raw == 7;
					let requestStorage = $scope.request.service_type.raw == 2 || $scope.request.service_type.raw == 6;
					let distance = parseFloat($scope.request.distance.value);
					let settings = $scope.basicsettings;
					let addToTravelTime = 0;

					// add duration to travel time
					if (!!settings.local_flat_miles
						&& distance > settings.local_flat_miles
						&& !flatRate
						&& !requestStorage) {
						addToTravelTime = common.filterFloat($scope.request.duration.value);
						$scope.request.request_all_data.localFlatMilesFlag = true;
					} else {
						$scope.request.request_all_data.localFlatMilesFlag = false;
					}

					$scope.localFlatMilesFlag = $scope.request.request_all_data.localFlatMilesFlag;

					if (!$scope.calcSettings.doubleDriveTime) {
						$scope.request.travel_time.raw += addToTravelTime;
						$scope.request.travel_time.value = common.decToTime($scope.request.travel_time.raw);
						updateMessage($scope.request.duration);
						updateMessage($scope.request.travel_time);
					}

					updateFuelSurchargeRelatedToQuote();
					calculateTime();
					updateInvoiceOnChangeTravelTime();
				});
		}

		function updateInvoiceOnChangeTravelTime () {
			if ($scope.request.status.raw != 3) return;

			$scope.invoice.travel_time = angular.copy($scope.request.travel_time);
			$scope.invoice.field_moving_from = angular.copy($scope.request.field_moving_from);
			$scope.invoice.field_moving_to = angular.copy($scope.request.field_moving_to);
		}

		function checkMoveToThisState(from, to, stateFrom, stateTo, areaCode) {
			let defer = $q.defer();
			CalculatorServices.getFullDistance(from, to)
				.then(distance => {
					let isLongDistanceZip = $scope.basicsettings.islong_distance_miles
						&& distance.distance > +$scope.basicsettings.long_distance_miles;
					if (isLongDistanceZip) {
						if (CalculatorServices.checkZipValidForLD(stateFrom, stateTo, areaCode)) {
							if (!$scope.longDistance) {
								changeServiceTypeToLD();
							}
							defer.resolve();
						} else {
							defer.reject({
								title: 'We don\'t do moves to this state!',
								text: 'Zip code was not changed'
							});
						}
					} else {
						defer.resolve();
					}
				})
				.catch(err => {
					defer.reject({
						title: 'Cannot retrieve distance!',
						text: 'Zip code was not changed'
					});
				});
			return defer.promise;
		}

		$scope.$watch('total_weight.weight', function () {
			if ($scope.request.service_type.raw == '7') {
				$scope.longDistanceQuote = CalculatorServices.getLongDistanceQuote($scope.request);
			}

			if ($scope.request.status.raw == '3'
				&& angular.isDefined($scope.invoice.service_type)
				&& $scope.invoice.service_type.raw == '7') {

				$scope.invoice.field_useweighttype = angular.copy($scope.request.field_useweighttype);
				$scope.invoice.inventory_weight = angular.copy($scope.request.inventory_weight);
				$scope.invoice.custom_weight = angular.copy($scope.request.custom_weight);
				longDistanceQuoteInvoiceChange();

			}
		}, true);

		$scope.$on('min_weight', function (event, data) {
			updateRequestWeight(!data.weightChanged);
			recalculateLongDistanceExtraServices();

			if ($scope.Invoice) {
				if ($scope.invoice.field_useweighttype.value == 2) {
					$scope.invoice.closing_weight.value = InventoriesServices.calculateClosingWeight($scope.request);
				} else {
					$scope.invoice.closing_weight.value = $scope.request.visibleWeight;
				}
				$scope.invoice.request_all_data.min_weight = angular.copy($scope.request.request_all_data.min_weight);
				longDistanceQuoteInvoiceChange();
			}
			longDistanceQuoteRequestChange();

			if ($scope.request.service_type.raw == 6) {
				RequestHelperService.recalculateCountOvernightExtraService($scope.request);

				if (!_.isUndefined($scope.pairedRequest)) {
					RequestHelperService.recalculateCountOvernightExtraService($scope.pairedRequest);
				}
			}
		});

		$scope.$on('apply.coupon', function (evt, data) {
			if (data.id) {
				$scope.request.request_all_data.couponData = {};
				$scope.request.request_all_data.couponData.discount = data.discount;
				$scope.request.request_all_data.couponData.discountType = data.type;
				$scope.request.request_all_data.couponData.discountId = data.id;
				$scope.contract_discount = data.discount;
				$scope.contract_discountType = data.type;
			} else {
				delete $scope.request.request_all_data.couponData;
				$scope.contract_discount = '';
				$scope.contract_discountType = '';
			}
		});

		$scope.$on('request.sendLog', function (event, data, nid) {
			if (nid != $scope.request.nid) {
				return false;
			}
			var arr = [];
			var grandTotal = 0;
			var grandTotalInvoice = 0;
			if ($scope.longDistance) {
				grandTotal = calculateGrandTotalBalance('longdistance');
			}
			if ($scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('flatrate');
			}
			if (!$scope.longDistance && !$scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('min') + ' - $' + calculateGrandTotalBalance('max');
			}
			if ($scope.grandTotal != grandTotal) {
				var obj = {
					text: 'Grand total was changed(sales tab)',
					from: '$' + $scope.grandTotal,
					to: '$' + grandTotal
				};
				arr.push(obj);
			}
			if ($scope.Invoice) {
				grandTotalInvoice = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
				if ($scope.grandTotalInvoice != grandTotalInvoice) {
					var obj1 = {
						text: 'Grand total was changed(closing tab)',
						from: '$' + $scope.grandTotalInvoice,
						to: '$' + grandTotalInvoice
					};
					if (!_.isNaN(grandTotalInvoice)) {
						$scope.grandTotalInvoice = angular.copy(grandTotalInvoice);
						arr.push(obj1);
					}

					var saveReqAllData = false;

					if (angular.isUndefined($scope.request.request_all_data.grand_total) && !_.isNaN(grandTotalInvoice)) {
						$scope.request.request_all_data.grand_total = _.round(grandTotalInvoice, 2);
						saveReqAllData = true;
					}

					if ($scope.request.request_all_data.grand_total != _.round(grandTotalInvoice, 2) && !_.isNaN(grandTotalInvoice)) {
						$scope.request.request_all_data.grand_total = _.round(grandTotalInvoice, 2);
						saveReqAllData = true;
					}

					if (saveReqAllData) {
						recalculateReservation();
						saveOnlyRequestAllData();
					}
				}
			}
			_.forEach(data.logs, function (log) {
				arr.push(log);
			});

			let fuel = $scope.Invoice ? baseInvoiceFuel : baseRequestFuel;
			let fuelMessage = FuelSurchargeService.makeLog($scope.request, $scope.invoice, fuel, 'admin', $scope.Invoice);

			baseRequestFuel = angular.copy($scope.request.request_all_data.surcharge_fuel);
			if ($scope.Invoice) {
				baseInvoiceFuel = angular.copy($scope.invoice.request_all_data.surcharge_fuel);
			}

			if (fuelMessage) {
				arr = arr.concat(fuelMessage);
			}

			if (!_.isEmpty(arr)) {
				RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST').then(function () {
					sendNotificationAfterSaveChanges(arr, $scope.request.nid, 'MOVEREQUEST');
				});
			}
		});

		$scope.$on('request.sendLog.closing', function (event, data, nid) {
			if (nid != $scope.request.nid) {
				return false;
			}
			var arr = [];
			var grandTotalInvoice = 0;
			var saveReqAllData = false;
			if ($scope.Invoice) {
				grandTotalInvoice = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
				if ($scope.grandTotalInvoice != grandTotalInvoice) {
					var obj = {
						text: 'Grand total was changed(closing tab)',
						from: '$' + $scope.grandTotalInvoice,
						to: '$' + grandTotalInvoice
					};
					if (!_.isNaN(grandTotalInvoice)) {
						$scope.grandTotalInvoice = angular.copy(grandTotalInvoice);
						arr.push(obj);
					}

					if (angular.isUndefined($scope.request.request_all_data.grand_total) && !_.isNaN(grandTotalInvoice)) {
						$scope.request.request_all_data.grand_total = _.round(grandTotalInvoice, 2);
						saveReqAllData = true;
					}

					if ($scope.request.request_all_data.grand_total != _.round(grandTotalInvoice, 2) && !_.isNaN(grandTotalInvoice)) {
						$scope.request.request_all_data.grand_total = _.round(grandTotalInvoice, 2);
						saveReqAllData = true;
					}
				}
				_.forEach(data.logs, function (log) {
					arr.push(log);
				});

				let fuelMessage = FuelSurchargeService.makeLog('', $scope.invoice, baseInvoiceFuel, 'admin', true);
				baseInvoiceFuel = angular.copy($scope.invoice.request_all_data.surcharge_fuel);
				if (fuelMessage) {
					arr = arr.concat(fuelMessage);
				}

				if (!_.isEmpty(arr)) {
					saveReqAllData = true;
					RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST').then(function () {
						sendNotificationAfterSaveChanges(arr, $scope.request.nid, 'MOVEREQUEST');
					});
				}
				if (saveReqAllData) {
					recalculateReservation();
					saveOnlyRequestAllData();
				}
			}
		});

		$scope.$on('request.packing_update', () => {
			setUpPackingServices();
			saveOnlyRequestAllData();
		});

		$scope.$on('grandTotal.sales', function (event, data, nid) {
			if (nid != $scope.request.nid) {
				return false;
			}

			var grandTotal = 0;

			if ($scope.longDistance) {
				grandTotal = calculateGrandTotalBalance('longdistance');
				caclulateLongDistanceQuoteUp();
			}

			if ($scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('flatrate');
			}

			if (!$scope.longDistance && !$scope.flatrate) {
				grandTotal = calculateGrandTotalBalance('min') + ' - $' + calculateGrandTotalBalance('max');
			}

			let obj = {
				text: 'Grand total was changed(sales tab)',
				from: '$' + data.grand,
				to: '$' + grandTotal
			};
			let arr = [];
			arr.push(obj);

			_.forEach(data.logs, function (log) {
				arr.push(log);
			});

			if (!_.isEmpty(arr)) {
				sendNotificationAfterSaveChanges(arr, $scope.request.nid, 'MOVEREQUEST');
				RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');

				recalculateReservation();
				saveOnlyRequestAllData();
			}
		});

		$scope.$on('grandTotal.closing', function (event, data, nid) {
			if (nid != $scope.request.nid) {
				return false;
			}
			if ($scope.Invoice) {
				var arr = [];
				var grandTotal = grandTotalFunc($scope.invoice, 'invoice', $scope.tips);
				if (data.grand != grandTotal) {
					var obj = {
						text: 'Grand total was changed(closing tab)',
						from: '$' + data.grand,
						to: '$' + grandTotal
					};
					if (!_.isNaN(grandTotal) && !_.isNaN(data.grand)) {
						$scope.grandTotalInvoice = angular.copy(grandTotal);
						arr.push(obj);
					}

					var saveReqAllData = false;

					if (angular.isUndefined($scope.request.request_all_data.grand_total) && !_.isNaN(grandTotal)) {
						$scope.request.request_all_data.grand_total = _.round(grandTotal, 2);
						saveReqAllData = true;
					}
					if ($scope.request.request_all_data.grand_total != _.round(grandTotal, 2) && !_.isNaN(grandTotal)) {
						$scope.request.request_all_data.grand_total = _.round(grandTotal, 2);
						saveReqAllData = true;
					}

					if (saveReqAllData) {
						recalculateReservation();
						saveOnlyRequestAllData();
					}
				}
				_.forEach(data.logs, function (log) {
					arr.push(log);
				});
				if (!_.isEmpty(arr)) {
					sendNotificationAfterSaveChanges(arr, $scope.request.nid, 'MOVEREQUEST');
					RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
				}
			}
		});

		$scope.$on('request.updated', function (event, data) {
			if ($scope.request.nid != data.nid) {
				return false;
			}
			_.forEach($scope.editrequest, function (item, field) {
				if (_.isEqual(field, 'field_flat_rate_local_move')) {
					$scope.request.field_flat_rate_local_move.old = angular.copy($scope.request.field_flat_rate_local_move.value);
				}
			});
			_.forEach($scope.editrequest, function (item, key) {
				if (_.find($scope.request, {'field': key})) {
					if (_.find($scope.request, {'field': key}).old && !_.isObject(item) && !_.isArray(item)) {
						_.find($scope.request, {'field': key}).old = angular.copy(item);
					} else if (item && item.date) {
						if (_.find($scope.request, {'field': key}).old) {
							_.find($scope.request, {'field': key}).old = angular.copy(item.date);
						}
					} else if (_.isArray(item) && key == 'field_list_truck') {
						_.find($scope.request, {'field': key}).old = angular.copy(item);
						$scope.oldTrucks = angular.copy(item);
					}
				}
			});

			angular.forEach($scope.editrequest, function (value, field) {
				if (_.isEqual(field, 'field_approve')) {
					$scope.request.status.raw = value;

					if (angular.isDefined($scope.newRequests)) {
						var index = _.findIndex($scope.newRequests, {nid: $scope.request.nid});

						if (index == -1) {
							$scope.newRequests.push($scope.request);
						} else {
							$scope.newRequests[index] = $scope.request;
						}

						$scope.$broadcast('parklot.init', $scope.newRequests);
					} else {
						$scope.$broadcast('parklot.init');
					}

					if ($scope.Invoice) {
						_.forEach($scope.invoice, function (item) {
							if (item && item.old) {
								item.old = angular.copy(item.value);
							}
						});

						if ($scope.invoice.request_all_data) {
							if ($scope.invoice.field_double_travel_time) {
								$scope.invoice.field_double_travel_time.old = angular.copy($scope.invoice.field_double_travel_time.value);
							}
						}

						if ($scope.request.field_double_travel_time) {
							$scope.request.field_double_travel_time.old = angular.copy($scope.request.field_double_travel_time.value);
						}
						longDistanceQuoteInvoiceChange();
					}

					longDistanceQuoteRequestChange();
					updateFuelSurchargeRelatedToQuote();
					recalculateReservation();
					saveOnlyRequestAllData();
				}

				if ($scope.editrequest.field_double_travel_time && !$scope.editrequest.field_approve) {
					if ($scope.request.field_double_travel_time) {
						$scope.request.field_double_travel_time.old = angular.copy($scope.request.field_double_travel_time.value);
						recalculateReservation();
						saveOnlyRequestAllData();
					}
				}

				if ($scope.ifStorage
					&& _.isEqual(field, 'field_date')
					&& (!_.isUndefined($scope.request.storage_id) && !_.isEqual($scope.request.storage_id, 0)
						|| $scope.request.request_all_data.baseRequestNid)
					&& !_.isUndefined($scope.pairedRequest)) {

					var firstFormatDate = $scope.request.date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
					var movedateFirst = moment($scope.request.date.value, firstFormatDate);
					var secondFormatDate = $scope.pairedRequest.date.value.indexOf('/') != -1 ? 'MM/DD/YYYY' : 'YYYY-MM-DD';
					var movedateSecond = moment($scope.pairedRequest.date.value, secondFormatDate);

					var diffDays = Math.abs(movedateFirst.diff(movedateSecond, 'days'));

					$scope.request.service_type.raw = diffDays == 1 ? '6' : '2';

					if ($scope.request.service_type.raw == '6') {
						RequestHelperService.addOvernightExtraService($scope.request);
					}

					prepareMovingStorageRate();
				}

				if (_.isEqual(field, 'field_date')) {
					common.$timeout(function () {
						var date = $scope.request.date.value;
						$scope.parklotControl.reinitParklot(date);
					}, 1000);
				}

				if (_.isEqual(field, 'field_delivery_date_from')) {
					$scope.request.delivery_date_from.value = moment(value.date).format('DD-MM-YYYY');
					$scope.request.delivery_date_from.raw = moment(value.date).unix();
				}

				if (_.isEqual(field, 'field_delivery_date_to')) {
					$scope.request.delivery_date_to.value = moment(value.date).format('DD-MM-YYYY');
					$scope.request.delivery_date_to.raw = moment(value.date).unix();
				}
			});

			if (!_.isEqual($scope.request.ddate.old, $scope.request.ddate.value)) {
				$scope.request.ddate.old = angular.copy($scope.request.ddate.value);
			}

			if ($scope.conflictRequests.length) {
				$timeout(function () {
					$scope.parklotControl.reinitParklot($scope.currentDay);
				}, 1000);
			}

			if ($scope.request.service_type.raw == 6) {
				RequestHelperService.recalculateCountOvernightExtraService($scope.request);

				if (!_.isUndefined($scope.pairedRequest)) {
					RequestHelperService.recalculateCountOvernightExtraService($scope.pairedRequest);
				}
			}
		});

		$scope.$on('updateQuote', function () {
			updateQuote();
		});

		$scope.$on('request.payment_receipts', function (event, receipts, nid = $scope.request.nid) {
			if (nid != $scope.request.nid) return;

			$scope.request.receipts = receipts;
			$scope.recervation_received = false;

			angular.forEach(receipts, function (receipt) {
				if (receipt.payment_flag == 'Reservation' && !receipt.pending) {
					$scope.recervation_received = true;

					checkConflictRequestWithoutUpdate();

					$scope.parklotControl.reinitParklot($scope.currentDay);
				}
			});
		});

		$scope.$on('payment.reservation', function (event, request) {
			angular.forEach(request.receipts, function (receipt, id) {
				if (receipt.payment_flag == 'Reservation' && !receipt.pending) {
					$scope.recervation_received = true;
					$scope.parklotControl.reinitParklot($scope.currentDay);
				}
			});
		});

		$scope.$on('request.details_updated', function () {
			setLastDeliveryDay();
		});

		$scope.$on('update.LDQuote', updateLDQuote);

		function updateLDQuote() {
			$scope.longDistanceQuote = CalculatorServices.getLongDistanceQuote($scope.request);

			if (angular.isDefined($scope.invoice.field_useweighttype)) {
				$scope.invoice.field_long_distance_rate.value = $scope.request.field_long_distance_rate.value;
				$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
				$scope.longDistanceQuoteUpInvoice = Math.round($scope.longDistanceQuoteInvoice / (1 - $scope.localdiscountInvoice / 100));
			}

			CalculatorServices.updateLdMinWeightSettings($scope.request);

			if ($scope.Invoice) {
				longDistanceQuoteInvoiceChange();
			}

			longDistanceQuoteRequestChange();
			updateFuelSurchargeRelatedToQuote();
		}

		$scope.$on('inventory_update', onInventoryUpdate);

		function onInventoryUpdate(oldWeight) {
			updateRequestWeight(null, oldWeight);
			recalculateLongDistanceExtraServices();
			updateAddPackingService();

			let haveToRecalculateValuationForRequest = $scope.request.request_all_data.valuation
				&& !$scope.request.request_all_data.valuation.lability_amount
				&& !_.isUndefined($scope.request.request_all_data.valuation.selected.valuation_charge)
				&& $scope.request.field_useweighttype.value == 2;

			let haveToRecalculateValuationForInvoice = $scope.Invoice
				&& $scope.invoice.request_all_data.valuation
				&& !$scope.invoice.request_all_data.valuation.lability_amount
				&& !_.isUndefined($scope.invoice.request_all_data.valuation.selected.valuation_charge)
				&& $scope.invoice.field_useweighttype.value == 2;

			if (haveToRecalculateValuationForRequest) {
				valuationService.reCalculateValuation($scope.request, $scope.invoice, 'request');
			}

			if (haveToRecalculateValuationForInvoice) {
				valuationService.reCalculateValuation($scope.request, $scope.invoice, 'invoice');
			}

			if ($scope.request.service_type.raw == 6) {
				RequestHelperService.recalculateCountOvernightExtraService($scope.request);

				if (!_.isUndefined($scope.pairedRequest)) {
					RequestHelperService.recalculateCountOvernightExtraService($scope.pairedRequest);
				}
			}

			recalculateReservation();
		}

		function updateLongDistanceRate() {
			if ($scope.request.field_usecalculator.value) {
				let weightForCalcs = checkForMinWeightLdOnly($scope.request.visibleWeight);
				$scope.longDistanceRate = CalculatorServices.recalculateLongDistanceRate($scope.request, weightForCalcs);
				$scope.request.field_long_distance_rate.value = $scope.longDistanceRate;
				if ($scope.Invoice && $scope.longDistance) {
					$scope.invoice.field_long_distance_rate.value = $scope.longDistanceRate || 0;
				}
				if ($scope.request.field_long_distance_rate.value == $scope.request.field_long_distance_rate.old) {
					removeMessage($scope.request.field_long_distance_rate);
				} else {
					updateMessage($scope.request.field_long_distance_rate);
				}
			}
		}

		$scope.$on('postal.code.updated', onChangePostalCode);

		function onChangePostalCode() {
			if ($scope.request.service_type.raw == 7) {
				updateLongDistanceBySettings();
			}
		}

		function recalculateEquipmentFee() {
			additionalServicesFactory.recalculateEquipmentFee($scope.request, $scope.invoice, wasChanged => {
				if (wasChanged) {
					additionalServicesFactory.saveExtraServices($scope.request.nid, $scope.request.extraServices);
					saveOnlyRequestAllData();
				}
			});
		}

		function updateLongDistanceBySettings() {
			CalculatorServices.updateLdMinWeightSettings($scope.request, true);
			updateLongDistanceRate();

			if ($scope.Invoice) {
				$scope.invoice.field_moving_to = angular.copy($scope.request.field_moving_to);
				$scope.invoice.field_moving_from = angular.copy($scope.request.field_moving_from);
				CalculatorServices.updateLdMinWeightSettings($scope.invoice, true);
			}

			updateFuelSurchargeRelatedToQuote();
		}

		$scope.$on('$destroy', function () {
			common.$timeout.cancel(unLoadTimer);
		});

		function updateFuelSurchargeRelatedToQuote(initRequest) {
			if ($scope.Job_Completed) {
				return false;
			}

			var typeCalculate = $scope.states.invoiceState ? 'invoice' : 'request';

			if ($scope[typeCalculate] && _.isUndefined($scope[typeCalculate].request_all_data)) {
				return;
			}

			var quote = 0;

			if (typeCalculate == 'request') {
				if ($scope[typeCalculate].service_type.raw == 5) {
					quote = CalcRequest.flatrateQuoteRequestChange($scope.request);
				} else {
					if ($scope[typeCalculate].service_type.raw == 7) {
						$scope.longDistanceQuote = CalculatorServices.getLongDistanceQuote($scope.request);

						quote = $scope.longDistanceQuote;
					} else {
						quote = _.round(($scope.request.quote.min + $scope.request.quote.max) / 2, 2);
					}
				}
			} else if (typeCalculate == 'invoice') {
				if ($scope[typeCalculate].service_type.raw == 5) {
					quote = CalcRequest.flatrateQuoteInvoiceChange($scope.invoice, $scope.request);
				} else {
					if ($scope[typeCalculate].service_type.raw == 7) {
						if (!$scope.longDistanceQuoteInvoice) {
							$scope.longDistanceQuoteInvoice = CalculatorServices.getLongDistanceQuote($scope.invoice, true);
						}

						quote = $scope.longDistanceQuoteInvoice;
					} else {
						quote = _.round(($scope.request.quote.min + $scope.request.quote.max) / 2, 2);
					}
				}
			}

			$scope.disableByFuel = true;

			updateFuelSurchargeWithDebounce(quote, typeCalculate, 'admin', initRequest);
		}

		function updateFuelSurcharge(quote, typeCalculate, place, initRequest) {
			let oldFuel = _.get($scope, `request.request_all_data.surcharge_fuel`, 0);
			let oldFuelInvoice = _.get($scope, `invoice.request_all_data.surcharge_fuel`, 0);

			FuelSurchargeService.updateFuelSurcharge($scope, quote, typeCalculate, 'admin')
				.then(() => {
					if (typeCalculate != 'invoice' && $scope.Invoice && $scope.request.service_type.raw == 7 && !initRequest) {
						$scope.invoice.request_all_data.surcharge_fuel = $scope.request.request_all_data.surcharge_fuel;
					}

					let newFuel = _.get($scope, `request.request_all_data.surcharge_fuel`, 0);
					let newFuelInvoice = _.get($scope, `invoice.request_all_data.surcharge_fuel`, 0);

					let isChangeFuel = oldFuel != newFuel || oldFuelInvoice != newFuelInvoice;

					if (isChangeFuel) {
						recalculateReservation();
						saveOnlyRequestAllData();
					}
				})
				.finally(() => {
					$scope.disableByFuel = false;
				});
		}


		function saveOnlyRequestAllData() {
			let logs = copyInvoiceToAllData();

			if (logs) {
				RequestServices.sendLogs(logs, 'Invoice was updated', $scope.request.nid, 'MOVEREQUEST');
			}

			return RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
		}

		function copyInvoiceToAllData() {
			let logs = false;

			if ($scope.Invoice) {
				let oldInvoiceRate = _.get($scope, 'request.request_all_data.invoice.rate.value');
				let newInvoiceRate = $scope.invoice.rate.value;

				if (!_.isUndefined(oldInvoiceRate) && oldInvoiceRate != newInvoiceRate) {
					logs = [{
						simpleText: 'Closing rate:',
						html: `Was changed automaticaly from $${oldInvoiceRate} to $${newInvoiceRate}`
					}];
				}

				$scope.request.request_all_data.invoice = angular.copy($scope.invoice);

				let grandTotalInvoice = +grandTotalFunc($scope.invoice, 'invoice', $scope.tips);

				if ($scope.request.request_all_data.grand_total != grandTotalInvoice && !_.isNaN(grandTotalInvoice)) {
					$scope.request.request_all_data.grand_total = _.round(grandTotalInvoice, 2);
				}
			}

			return logs;
		}

		$scope.$on('updateOnlyAllData', saveOnlyRequestAllData);

		let isDispatch = $state.current.name == 'dispatch.local' || $state.current.name == 'dispatch.payroll';

		if (($scope.Job_Completed || isDispatch && !$scope.recervation_received) && $scope.request.status.raw == 3) {
			changeSalesClosingTab('closing');
		}

		function showWarningBeforeSendEmail() {
			if (!_.isEmpty($scope.editrequest)) {
				SweetAlert.swal({title: 'Warning!', text: 'Please save changes!', type: 'warning'});
			}
		}

		function sendNotificationAfterSaveChanges(arr, nid, entity_type) {
			let entityType = $scope.fieldData.enums.entities[entity_type];
			let enumType = $scope.fieldData.enums.notification_types.MANAGER_DO_CHANGE_WITH_REQUEST;
			let data = {
				node: {nid: nid},
				notification: {
					text: ''
				}
			};
			let notificationMsg = '';

			_.forEach(arr, function (msg) {
				notificationMsg += msg.text || msg.simpleText;

				if (notificationMsg.substr(notificationMsg.length - 1) == '.') {
					notificationMsg = notificationMsg.slice(0, -1);
				}

				if (msg.from && msg.to) {
					notificationMsg += ' from: ' + msg.from + ', to: ' + msg.to;
				} else if (msg.to) {
					notificationMsg += ' to: ' + msg.to;
				}

				notificationMsg += '\n';
			});

			data.notification.text = notificationMsg;
			let dataNotify = {
				type: enumType,
				data: data,
				entity_id: nid,
				entity_type: entityType
			};

			apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
		}

		function openReminderBox () {
			dialogs.reminderRequestBox($scope.reminderOptions)
				.then(collectRemindersOptions.bind($scope));
		}

		function collectRemindersOptions () {
			Reminder.getByRequest({node_id: $scope.request.nid})
				.then((reminders) => {
					$scope.reminders = reminders;
					$scope.hasUpcomingReminders = !!$filter('upcoming')($scope.reminders, true).length;
					$scope.reminderOptions = {
						node_id: $scope.request.nid,
						client: $scope.request.name,
						request: true,
						collection: $scope.reminders
					};
				});
		}

		$scope.clearSecDeliveryDate = function () {
			//Clear second delivery date input
			angular.element('#deliveryDateSecondInput').datepicker('setDate', null);
			angular.element('#delivery-date-second-input').datepicker('setDate', null);
			changeRequestField('ddate_second');
		};

		let isFlatRateLocal = $scope.request.field_flat_rate_local_move ? $scope.request.field_flat_rate_local_move.value : true;

		if ($scope.request.isDelivery && !isFlatRateLocal) {
			$scope.DeliveryDay();
		}

		function calculateCommercial(request, calcSettings, isFirstTime) {
			let isCustomName = !_.isUndefined(request.request_all_data.commercialCustomName)
				&& request.request_all_data.commercialCustomName.length;
			let commercialExtra = request.commercial_extra_rooms;

			let weight = CommercialCalc.getCommercialCubicFeet(commercialExtra.value, calcSettings, request.field_custom_commercial_item);

			if (request.field_custom_commercial_item.is_checked) {
				$scope.request.custom_weight.value = weight.weight;
				$scope.editrequest.field_custom_commercial_item = $scope.request.field_custom_commercial_item;
			} else {
				$scope.request.total_weight = weight;
			}

			if (isCustomName) {
				$scope.commercialTitle = request.request_all_data.commercialCustomName;
			} else {
				$scope.commercialTitle = request.move_size.value;
			}

			calculateTime(isFirstTime);
		}

		function checkCommercialSetting() {
			let isCommercial = $scope.request.move_size.raw == 11;

			if (commercialSetting && !commercialSetting.isEnabled && !isCommercial) {
				let commercial = Object.keys($scope.allowedMoveSize).find(key => {
					return $scope.allowedMoveSize[key] === commercialSetting.name;
				});

				delete $scope.allowedMoveSize[commercial];
			}
		}

		$scope.$on('notes.updated', function(ev, isUpdated) {
			if (isUpdated) {
				$scope.editrequest.notes = isUpdated;
				$scope.message.notes = {
					label: 'notes',
					oldValue: '',
					newValue: 'Notes were changed'
				};
			} else {
				delete $scope.editrequest.notes;
				delete $scope.message.notes;
			}
		});

		$scope.$watchCollection('calcResults', function(current, original) {
			if (!_.isUndefined(current) && !_.isUndefined(original)) {
				if (current.trucks != original.trucks) {
					$scope.message.trucksCount = {
						label: 'custom',
						oldValue: '',
						newValue: `Trucks count was changed to ${current.trucks}`
					};
				}

				if (current.movers_count != original.movers_count) {
					$scope.editrequest.moversCount = current.movers_count;
					$scope.message.moversCount = {
						label: 'custom',
						oldValue: '',
						newValue: `Crew count was changed to ${current.movers_count}`
					};
				}
			}
		});

		$scope.$on('save-edit-request', _updateRequest);

		function updateMessage(field) {
			if (!field || !$scope.updateMessage) return;

			$scope.updateMessage({
				field: field,
			});
		}

		function detailsUpdated() {
			setLastDeliveryDay();
			recalculateLongDistanceExtraServices();
		}


		//=======================================REFACTOR FORM INPUTS============================================

		$scope.askForChangeDeliveryDate = (pickupDate) => formInputsServices.askForChangeDeliveryDate(pickupDate, $scope.request);
		$scope.onChangeMoveDateLocalMove = onChangeMoveDateLocalMove;
		$scope.onChangeMoveDateLocalMoveClosing = onChangeMoveDateLocalMoveClosing;
		$scope.onChangeFieldDateLongDistance = onChangeFieldDateLongDistance;
		$scope.onChangeFieldDateLongDistanceClosing = onChangeFieldDateLongDistanceClosing;
		$scope.onChangeFieldScheduleDeliveryDateLongDistance = onChangeFieldScheduleDeliveryDateLongDistance;
		$scope.onChangeFieldDeliveryDateFlatRate = onChangeFieldDeliveryDateFlatRate;
		$scope.onChangeFieldDeliveryDateFlatRateClosing = onChangeFieldDeliveryDateFlatRateClosing;
		$scope.onChangeFieldDeliveryDateSecondFlatRate = onChangeFieldDeliveryDateSecondFlatRate;

		function onChangeMoveDateLocalMove(_newVal) {
			let newVal = _newVal.format(FULL_DATE_FORMAT);
			$scope.moveDateInput = newVal;
			$scope.moveDateInputInvoice = newVal;

			formInputsServices.changeAndSaveSimpleDateFieldSales($scope, newVal, 'field_date');
			updateParklot(newVal);
			checkDeliveryDate(newVal);
			updatePackingDayAndTrucksAndTime(newVal);
		}

		function onChangeMoveDateLocalMoveClosing(_newVal) {
			let newVal = _newVal.format(FULL_DATE_FORMAT);
			$scope.moveDateInputInvoice = newVal;

			formInputsServices.changeAndSaveSimpleDateFieldClosing($scope, newVal, 'field_date');
			updateParklot(newVal);
			checkDeliveryDate(newVal);
		}

		function onChangeFieldDateLongDistance(_newVal) {
			let newVal = _newVal.format(FULL_DATE_FORMAT);
			$scope.moveDateInput = newVal;
			$scope.moveDateInputInvoice = newVal;

			formInputsServices.changeAndSaveSimpleDateFieldSales($scope, newVal, 'field_date');
			updateParklot(newVal);
			checkDeliveryDate(newVal);
			updatePackingDayAndTrucksAndTime(newVal);
		}

		function onChangeFieldDateLongDistanceClosing(_newVal) {
			let newVal = _newVal.format(DATE_FORMAT_SHORT);
			$scope.moveDateInputInvoice = newVal;

			formInputsServices.changeAndSaveSimpleDateFieldClosing($scope, newVal, 'field_date');
			updateParklot(newVal);
			checkDeliveryDate(newVal);
		}

		function onChangeFieldScheduleDeliveryDateLongDistance(_newVal) {
			let newVal = _newVal.format(FULL_DATE_FORMAT);
			$scope.scheduleDeliveryDate = newVal;

			formInputsServices.changeAndSaveSimpleDateFieldSales($scope, newVal, 'field_schedule_delivery_date');
			$scope.changeScheduleDeliveryDate(newVal);
		}

		function onChangeFieldDeliveryDateFlatRate(_newVal) {
			let newVal = _newVal.format(FULL_DATE_FORMAT);
			$scope.deliveryDateInput = newVal;
			$scope.deliveryDateInputInvoice = newVal;

			formInputsServices.changeAndSaveSimpleDateFieldSales($scope, newVal, 'field_delivery_date');
			checkDeliveryDateRange();
		}

		function onChangeFieldDeliveryDateFlatRateClosing(_newVal) {
			let newVal = _newVal.format(FULL_DATE_FORMAT);
			$scope.deliveryDateInputInvoice = newVal;

			formInputsServices.changeAndSaveSimpleDateFieldClosing($scope, newVal, 'field_delivery_date');
			checkDeliveryDateRange();
		}

		function onChangeFieldDeliveryDateSecondFlatRate(_newVal) {
			let newVal = _newVal.format(FULL_DATE_FORMAT);
			$scope.deliveryDateSecondInput = newVal;

			formInputsServices.changeAndSaveSimpleDateFieldSales($scope, newVal, 'field_delivery_date_second');
			checkDeliveryDateRange();
		}

		function updateParklot(newVal) {
			$scope.request.updateRequestDate = true; // make for correct remove truck
			$scope.parklotControl.reinitParklot(newVal);
			$scope.updateRate();
		}


		function checkDeliveryDate(newVal) {
			let newMoveDateObj = moment(newVal, formInputsServices.DATE_FORMAT_DEFAULT);
			let needToChangeDeliveryDate = $scope.request.inventory.move_details.delivery
				&& newMoveDateObj.isAfter($scope.request.inventory.move_details.delivery);

			if (needToChangeDeliveryDate) {
				setNewDeliveryDate(newVal);
			}
		}

		function checkDeliveryDateRange() {
			if ($scope.request.delivery_date_from.value != false) {
				if ($scope.request.delivery_date_to.value == false) {
					$scope.request.delivery_date_to.value = $scope.request.delivery_date_from.value;
				}

				let momentCurrentDDate = moment($scope.request.ddate.value, DATE_FORMAT_SLASH);
				let dateFrom = moment($scope.request.delivery_date_from.value, DATE_FORMAT_DEFIS);
				let dateTo = moment($scope.request.delivery_date_to.value, DATE_FORMAT_DEFIS);
				let isCorrectDate = momentCurrentDDate.isBetween(dateFrom, dateTo)
					|| momentCurrentDDate.isSame(dateFrom, MOMENT_IS_SAME_LIMIT)
					|| momentCurrentDDate.isSame(dateTo, MOMENT_IS_SAME_LIMIT);

				if (!isCorrectDate) {
					toastr.warning('Delivery Date must be the same or between in Delivery Dates range', 'Warning!')
				}
			}
		}

		function updatePackingDayAndTrucksAndTime(newVal) {
			if ($scope.request.status.raw == 1
				|| $scope.request.status.raw == 2
				|| $scope.Invoice) {
				let isRequestHasPackingDay = $scope.request.request_all_data.packing_request_id
					&& $scope.request.service_type.raw != 8;

				if (isRequestHasPackingDay) {
					RequestServices.UpdatePackingRequest($scope.request.request_all_data.packing_request_id, {
						serviceType: $scope.request.service_type.value,
						status: $scope.request.status.value,
						moveDate: moment(newVal.date.value).format(DATE_FORMAT_SHORT)
					})
						.catch(err => Raven.captureException('Cannot update packing day request'));
				}

				_.forEach($scope.request.trucks.raw, function (truck) {
					$scope.chooseTruck(truck, true);
				});

				$scope.request.trucks.raw.length = 0;
				calculateTime();
			}
		}

		function setNewDeliveryDate(newDeliveryDate) { //remake later with delivery dates
			angular.element('.delivery-days-calendar').datepicker('option', 'minDate', newDeliveryDate);
			let oldDDay = angular.copy($scope.request.inventory.move_details.delivery);
			$scope.request.inventory.move_details.delivery = newDeliveryDate;
			$scope.checkDeliveryDay(oldDDay);
		}

		function setCustomConfirmationText() {
			$scope.request.request_all_data.isCustomConfirmationText = !!$scope.contract_page.useCustomConfirmationText;
		}

		requestObservableService.getChanges('weightTypeChanged', onChangeWeightType, $scope.request.field_useweighttype);
		requestObservableService.getChanges('weightTypeInvoiceChanged', onChangeUseWeightTypeInvoice, $scope.request.field_useweighttype);
		requestObservableService.getChanges('inventoryUpdate', onInventoryUpdate);
		requestObservableService.getChanges('detailsUpdated', detailsUpdated);
		requestObservableService.getChanges('useCalculatorChanged', onChangeUseCalculator);

		function changeReservationManually() {
			let isChangeReservationPrice = _.get($scope.request, 'reservation_rate.value') != _.get($scope.request, 'reservation_rate.old');
			let isCanChangeReservationPrice = !_.get($scope.request, 'field_reservation_received.value') && !isUpdateReservation;

			if (isChangeReservationPrice && isCanChangeReservationPrice) {
				updateMessage($scope.request.reservation_rate);

				if ($scope.request.service_type.raw != 5) {
					$scope.request.request_all_data.reservationChangedManually = true;

					saveOnlyRequestAllData();
				}
			} else {
				removeMessage($scope.request.reservation_rate);
			}
		}

		function getBaseGrandTotalLocalMove(work_time_int) {
			let total = 0;
			let rate = 0;
			let time = 0;
			let crewDuration = 0;

			_.forEach($scope.request.contract.crews, (crew, index) => {
				rate = laborService.getCrewRate(index, $scope.request.contract.crews, _.get($scope.request.request_data, 'value.crews.additionalCrews'));
				crewDuration = crew.workDuration;

				time += crewDuration;
				total += rate * crewDuration;
			});

			if (!_.get($scope.request, 'contract.crews[0].workDuration')) {
				rate = $scope.request.request_all_data.add_rate_discount || $scope.invoice.rate.value;
				time += work_time_int;
				total = work_time_int * rate;
			}

			if ($scope.calcSettings.doubleDriveTime) {
				let maxTravelTime = Math.max(minCATravelTime, distancesABDuration * 2);
				maxTravelTime = CalculatorServices.getRoundedTime(maxTravelTime);

				if (!maxTravelTime || _.get($scope, 'invoice.field_double_travel_time.raw')) {
					time += $scope.invoice.field_double_travel_time.raw;
					total += rate * $scope.invoice.field_double_travel_time.raw;
				} else {
					time += maxTravelTime;
					total += rate * maxTravelTime;
				}
			} else if ($scope.travelTime) {
				time += $scope.invoice.travel_time.raw;
				total += rate * $scope.invoice.travel_time.raw;
			}

			if (time < $scope.calcSettings.min_hours) {
				total += rate * ($scope.calcSettings.min_hours - time);
			}

			return total;
		}

		function recalculateReservation() {
			let reservationPaidChangedManually = !$scope.recervation_received && !$scope.request.request_all_data.reservationChangedManually;
			let updateReservationLocalMoves = !$scope.flatrate && !$scope.longDistance && $scope.request.status.raw != 3;

			if (reservationPaidChangedManually && ($scope.request.service_type.raw == 7 || updateReservationLocalMoves)) {
				grandTotalService.getReservationRate($scope.request);
			}
		}
	}
})();
