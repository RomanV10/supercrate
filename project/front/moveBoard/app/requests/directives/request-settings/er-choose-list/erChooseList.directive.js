'use strict';

angular.module('move.requests')
	.directive('erChooseList', erChooseList);

/* @ngInject */
function erChooseList($rootScope, RequestServices, requestObservableService) {
	var directive = {
		scope: {
			request: '=',
			editrequest: '=',
			message: '=',
		},
		template: require('./choose-list.template.html'),
		restrict: 'E',
		link: link,
	};

	return directive;

	function link(scope) {
		scope.selectedType = scope.request.field_useweighttype.value;
		scope.clickEdit = false;
		const COMMERCIAL = 11;
		
		scope.selectList = function (id) {
			scope.selectedType = id;
			scope.request.field_useweighttype.value = id;
			
			if (id == 3) {
				scope.clickEdit = true;
			}
			
			var details = [];
			
			var msg = {
				text: 'Weight type was changed.',
				to: ['Default','Inventory','Custom'][id - 1]
			};
			
			details.push(msg);
			requestObservableService.setChanges('weightTypeChanged');
			updateMessage(scope.request.field_useweighttype);
		};
		
		scope.$on('selectList', function (ev, id) {
			scope.selectList(id);

			let editPairedRequest = {
				field_useweighttype: id
			};
			
			if (scope.request.storage_id) {
				RequestServices.updateRequest(scope.request.storage_id, editPairedRequest);
			} else {
				RequestServices.updateRequest(scope.request.nid, editPairedRequest);
			}
		});
		
		scope.setCustom = function () {
			if (scope.request.custom_weight.value.length) {
				if (scope.request.move_size.raw == COMMERCIAL) {
					scope.request.field_custom_commercial_item.cubic_feet = angular.copy(scope.request.custom_weight.value);
				}
				scope.clickEdit = false;
				scope.selectedType = 3;
				scope.request.field_useweighttype.value = 3;
				requestObservableService.setChanges('inventoryUpdate');
				updateMessage(scope.request.field_useweighttype);
				updateMessage(scope.request.custom_weight);
			}
		};
		
		function updateMessage(field) {
			if (!field) return;
			
			scope.editrequest[field.field] = field.value;
			switch (field.field) {
			case 'field_useweighttype':
				scope.message[field.field] = {
					label: 'custom',
					oldValue: '',
					newValue: 'Weight Type was changed',
				};
				break;
			default:
				scope.message[field.field] = {
					label: field.label,
					oldValue: '',
					newValue: field.value,
				};
			}
			
		}
	}
}
