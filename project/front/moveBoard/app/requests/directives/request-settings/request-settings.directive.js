'use strict';

import { prepareRequest } from '../../request-helper-methods/prepare-new-request';

angular
    .module('move.requests')
    .directive('erRequestSettings', erRequestSettings);

erRequestSettings.$inject = ['$q', '$rootScope', '$http', 'RequestServices', 'common', 'logger', 'SweetAlert', 'config', '$window', 'datacontext',
    'InventoriesServices', 'PermissionsServices', 'StorageService', 'BranchAPIService', 'newLogService', 'CalculatorServices', 'PaymentServices', 'apiService', 'moveBoardApi', 'DispatchService', '$location'];

function erRequestSettings($q, $rootScope, $http, RequestServices, common, logger, SweetAlert, config, $window, datacontext,
                           InventoriesServices, PermissionsServices, StorageService, BranchAPIService, newLogService, CalculatorServices, PaymentServices, apiService, moveBoardApi, DispatchService, $location) {
    return {
        link: link,
        scope: {
            request: '=',
			editrequest: '=',
			message: '=',
	        editrequestInvoice: '=',
	        messageInvoice: '='
        },
        template: require('./request-settings.template.html'),
        restrict: 'E'
    };

    function link(scope) {
        var nid = scope.request.nid;
        scope.managersList = [];
        scope.currentManager = 1;
        var websiteUrl = $window.location.origin;
        scope.PermissionsServices = PermissionsServices;
        scope.fieldData = datacontext.getFieldData();

        scope.managersList = common.objToArray(RequestServices.getManagers());
        scope.manager = RequestServices.getManagers();
        scope.currentManager = scope.request.manager;

        scope.migrateToBranch = migrateToBranch;
        scope.branches = angular.copy(datacontext.getFieldData().branches);
        scope.isActiveBranching = scope.branches.length >= 2 && (PermissionsServices.hasPermission('administrator') || PermissionsServices.hasPermission('canMoveRequestToOtherBranch'));
        scope.selectedBranchId = "";
        scope.hideSettings = false;

        scope.isShowClosingWeightChooseList = checkIsShowClosingUseWeightType();

        scope.showManagerDropdown = showManagerDropdown;

        function showManagerDropdown(managerName) {
            return PermissionsServices.hasPermission('canSignedSales') ||
                   !managerName && PermissionsServices.hasPermission('canAssignUnassignedLeads');
        }

        function filterBranches() {
            var index = _.findIndex(scope.branches, function (branch) {
                return config.serverUrl.indexOf(branch.api_url) >= 0;
            });

            if (index >= 0) {
              scope.branches.splice(index, 1);
            }
        }

        filterBranches();

        function migrateToBranch() {
            var branch = _.find(scope.branches, {id: scope.selectedBranchId});
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Request will be copy to branch " + branch.name,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#64C564",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    scope.busy = true;

                    BranchAPIService
                        .migrateRequest(scope.request.nid, scope.selectedBranchId)
                        .then(function () {
                            toastr.success('Request was successfully copy to branch ' + branch.name)
                        }, function () {
                            toastr.error('Request was not copy to branch ' + branch.name)
                        })
                        .finally(function () {
                            scope.busy = false;
                        })
                }
            });
        }

        scope.setManager = function (uid) {
            SweetAlert.swal({
                title: "Set new manager",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#64C564",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    setManager(uid);
                }
            });
        };

         function setManager(uid){

             var nid = scope.request.nid;
              // SweetAlert.swal("Success!", "New manager was apply to request", "success");
               var promise = RequestServices.setManager(uid,nid);
                promise.then(function() {
                    //broadCaset
                    toastr.success("Success!", "New salesperson was assigned to the request");
                    var msg = {
                        text: 'New manager was apply to request',
                        to: scope.manager[uid].field_user_first_name + ' '+scope.manager[uid].field_user_last_name,
                        from: ''
                    };
                    if(scope.currentManager)
                        if(scope.currentManager.first_name)
                            msg.from = scope.currentManager.first_name+ ' '+scope.currentManager.last_name
                    var arr = [];
                    arr.push(msg);
	                RequestServices.sendLogs(arr, 'Manager was changed', scope.request.nid, 'MOVEREQUEST');
                    var data = {
                        first_name: scope.manager[uid].field_user_first_name,
                        last_name: scope.manager[uid].field_user_last_name
                    };
                    scope.currentManager.first_name = data.first_name;
                    scope.currentManager.last_name = data.last_name;
                    $rootScope.$broadcast('manager.assigned', data, nid);

            }, function (reason) {
                logger.error("Connection Error", "Connection Error", "Error");
            });
        }

        scope.showCouponChanged = function () {
            RequestServices.saveReqData(scope.request.nid, scope.request.request_all_data);
        };

        scope.goToRequest = function () {
            var url = websiteUrl + '/account/#/request/' + scope.request.nid;
            $window.open(url, '_blank');
        };
        scope.goToConfirmation = function () {
            var url = websiteUrl + '/account/#/request/' + scope.request.nid + '/reservation';
            $window.open(url, '_blank');
        };

	        scope.cloneRequest = function () {
		        if (!!scope.request) {
			        SweetAlert.swal({
					        title: "Are you sure, you want to clone request?",
					        text: '',
					        type: "warning",
					        showCancelButton: true,
					        confirmButtonColor: "#DD6B55", confirmButtonText: "Clone",
					        cancelButtonText: "Cancel",
					        closeOnConfirm: true,
					        closeOnCancel: true
				        },
				        function (isConfirm) {
					        if (isConfirm) {
						        scope.busy = true;
						        let editRequest = prepareRequest(scope.request);

						        apiService.postData(moveBoardApi.request.clone, {nid: scope.request.nid})
							        .then(function (response) {
								        let responseData = response.data;

								        if (responseData.status == 200) {
									        let nid = responseData.data;

									        var msg = {
										        text: 'Request was cloned',
										        to: nid,
										        from: scope.request.nid
									        };

									        var arr = [];
									        arr.push(msg);
									        let results = angular.copy(editRequest.data);
									        let request = results;
									        request.move_size = {raw: results.field_size_of_move};
									        request.rooms = {
										        raw: results.field_extra_furnished_rooms,
										        value: results.field_extra_furnished_rooms
									        };
									        request.inventory = {inventory_list: scope.request.inventory.inventory_list};
									        request.inventory_weight = scope.request.inventory_weight;
									        request.custom_weight = {value: results.field_customweight};
									        request.field_useweighttype = {value: results.field_useweighttype};
									        _.set(request, 'commercial_extra_rooms.value', scope.request.commercial_extra_rooms);

									        let weight = CalculatorServices.getTotalWeight(request).weight;

									        RequestServices.sendLogs(arr, 'Request was cloned', scope.request.nid, 'MOVEREQUEST');
									        let arrLogs = newLogService.createLogs("Admin Creating Request", scope.fieldData.field_lists, editRequest.data, weight);
									        arrLogs.push(msg);
									        RequestServices.sendLogs(arrLogs, 'Request was created', nid, 'MOVEREQUEST');

									        RequestServices.getRequestsByNid(nid).then(function (data) {
										        let newRequest = {};
										        newRequest[nid] = _.head(data.nodes);
										        $rootScope.$broadcast('request.added', newRequest);
										        scope.busy = false;
									        });
								        }
							        });
					        }
				        }
			        );
		        }
	        };

        scope.createStorageReq = function () {
			if (scope.request.request_all_data.storage_request_id) {
				SweetAlert.swal('Failed!', 'Storage have been created for this request', 'error');
				return;
			}
        	
            if (!scope.request.phone || scope.request.phone == '') {
                SweetAlert.swal("Failed!", "Please, add client phone number", "error");
                return false;
            }
            DispatchService.getRequestContract(scope.request.nid)
                .then((response) => {
                    if(_.get(response, 'factory.agreement.address')) {
                        scope.request.address = response.factory.agreement.address;
                    }

                    let weight = CalculatorServices.getTotalWeight(scope.request).weight;
                    if (scope.request.service_type.raw == 2 || scope.request.service_type.raw == 6) {
                        SweetAlert.swal({
                                title: "Do you want to create Storage Request?",
                                text: "",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, create it!",
                                closeOnConfirm: true
                            },
                            function (isConfirm) {
                                if (isConfirm) {
        
                                    if (!scope.request.request_all_data.toStorage) {
                                        RequestServices.getRequestsByNid(scope.request.storage_id).then(function (requestStorage) {
                                            if (!_.isEmpty(requestStorage.nodes)) {
                                                StorageService.createStorageReqObj(scope.request, scope.request.date.value, requestStorage.nodes[0].date.value, weight, requestStorage.nodes[0], scope.request.nid, 'move');
                                            } else {
                                                StorageService.createStorageReqObj(scope.request, scope.request.date.value, '', weight, '', scope.request.nid, 'move');
                                            }
                                        });
                                    } else {
                                        RequestServices.getRequestsByNid(scope.request.storage_id).then(function (requestStorage) {
                                            if (!_.isEmpty(requestStorage.nodes)) {
                                                StorageService.createStorageReqObj(requestStorage.nodes[0], requestStorage.nodes[0].date.value, scope.request.date.value, weight, scope.request, scope.request.nid, 'move');
                                            } else {
                                                StorageService.createStorageReqObj(scope.request, scope.request.date.value, '', weight, '', scope.request.nid, 'move');
                                            }
                                        });
                                    }
                                }
                            });
                    } else {
                        SweetAlert.swal({
                                title: "Do you want to create Storage Request?",
                                text: "",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Yes, create it!",
                                closeOnConfirm: true
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    RequestServices.getRequestsByNid(scope.request.storage_id).then(function (requestStorage) {
                                        if (!_.isEmpty(requestStorage.nodes)) {
                                            StorageService.createStorageReqObj(requestStorage.nodes[0], requestStorage.nodes[0].date.value, scope.request.date.value, weight, requestStorage.nodes[0], scope.request.nid, 'move');
                                        } else {
                                            StorageService.createStorageReqObj(scope.request, scope.request.date.value, '', weight, '', scope.request.nid, 'move');
                                        }
                                    });
                                }
                            });
                    }
                })
                .catch((err) => SweetAlert.swal("Failed!", 'Error of creating storage request', "error"));
        };

        scope.$on('storage.request.created', function (ev, data) {
            scope.request.request_all_data.storage_request_id = data;
            RequestServices.saveReqData(scope.request.nid, scope.request.request_all_data);
        });

        scope.$on('isClosing', function (event, data) {
            scope.hideSettings = data;
        })
		
		scope.copy = (type) => {
			let data = '';

        	if (type === 'mainLink') {
		        data = angular.element('#main-page');
	        } else if (type === 'additionalLink') {
		        data = angular.element('#additional-pages');
	        } else {
        		data = angular.element('#main-page-pdf');
	        }

			data.select();
			
			let result = document.execCommand('copy');
			if(result) {
				SweetAlert.swal('Success!', 'Successfully copy data to clipboard', 'success');
			} else {
				SweetAlert.swal('Failed!', 'Error of copy link to clipboard', 'error');
			}
		};
	
		let additionalContract = _.get(scope.fieldData, 'contract_page.additionalContract', [])
			.some(item => !!item.proposalPage);
  
		scope.clientRoute = {
			mainPage: `http://${$location.host()}:${$location.port()}/account/#/request/${scope.request.nid}/contract_main_page_read`,
			get additionalPages() {
				return additionalContract
					? `http://${$location.host()}:${$location.port()}/account/#/request/${scope.request.nid}/contract_proposal_additional_pages_read`
					: void 0;
			},
			get mainPagePDF() {
				return _.get(scope, 'request.contract.mainContractPagePdf.url', 0);
			}
		};

		function checkIsShowClosingUseWeightType() {
			let inventoryList = _.get(scope.request, 'inventory.inventory_list');
			let isEmptyDefaultInventory = _.isEmpty(inventoryList);
			let additionalInventoryList = _.get(scope.request, 'request_all_data.inventory');
			let isNotEmptyAdditionalInventory = !_.isEmpty(additionalInventoryList);
			let firstAdditionalInventoryList = _.get(scope.request, 'request_all_data.second_additional_inventory');
			let isNotEmptyFirstAdditonalInventory = !_.isEmpty(firstAdditionalInventoryList);
			let secondAdditionalInventoryList = _.get(scope.request, 'request_all_data.second_additional_inventory');
			let isNotEmptySecondAdditionalInventory = !_.isEmpty(secondAdditionalInventoryList);
			let isExistAdditioanlInventory = isNotEmptyAdditionalInventory || isNotEmptyFirstAdditonalInventory || isNotEmptySecondAdditionalInventory;

			return +scope.request.status.raw === 3 && isEmptyDefaultInventory && isExistAdditioanlInventory;
		}
    }
}
