'use strict';

angular
	.module('move.requests')
	.directive('reqAccountPage', reqAccountPage);

reqAccountPage.$inject = ['$http', 'config', 'logger', 'Lightbox', 'RequestServices', 'common', 'datacontext', 'CalculatorServices', '$uibModal', '$rootScope', 'SettingServices', 'LongDistanceServices', 'InitRequestFactory', 'apiService', 'moveBoardApi'];

function reqAccountPage($http, config, logger, Lightbox, RequestServices, common, datacontext, CalculatorServices, $uibModal, $rootScope, SettingServices, LongDistanceServices, InitRequestFactory, apiService, moveBoardApi) {
	return {
		link: linkFn,
		scope: {
			request: '=',
			editrequest: '=',
			message: '='
		},
		template: require('./request-account-page-settings.html'),
		restrict: 'E'
	};
	
	function linkFn(scope) {
		var nid = scope.request.nid;
		
		
		if (_.isUndefined(scope.request.request_all_data.additionalBlock)) {
			scope.request.request_all_data.additionalBlock = [{
				title: '',
				body: '',
				isVisible: false,
				confirmationPage: false
			}];
		}
		if (!_.isArray(scope.request.request_all_data.additionalBlock)) {
			scope.request.request_all_data.additionalBlock = [scope.request.request_all_data.additionalBlock];
		}
		scope.additionalBlock = angular.copy(scope.request.request_all_data.additionalBlock);
		
		
		scope.changed = false;
		var fieldData = datacontext.getFieldData();
		scope.longdistanceSettings = angular.fromJson(fieldData.longdistance);
		scope.quoteSettings = angular.fromJson(fieldData.customized_text);
		
		if (_.isUndefined(scope.request.request_all_data.arrivalTimeWindow)
			|| _.isEmpty(scope.request.request_all_data.arrivalTimeWindow)) {
			scope.arrivalTime = {
				text: '',
				showInAccount: false
			};
		} else {
			scope.arrivalTime = angular.copy(scope.request.request_all_data.arrivalTimeWindow);
		}
		
		scope.logMsg = {};
		scope.loadDetails = loadDetails;
		scope.details_change = details_change;
		scope.save = save;
		scope.addNewAdditionalBlock = addNewAdditionalBlock;
		scope.removeNewAdditionalBlock = removeNewAdditionalBlock;
		scope.switchAdditionalBlock = switchAdditionalBlock;
		scope.showArrivalTimeWindow = showArrivalTimeWindow;
		
		loadDetails(nid);
		
		function showArrivalTimeWindow() {
			scope.request.request_all_data.arrivalTimeWindow = angular.copy(scope.arrivalTime);
		}
		
		function loadDetails(nid) {
			$http.get(config.serverUrl + 'server/move_invertory/' + nid)
				.success(function (data, status, headers, config) {
					scope.details = InitRequestFactory.initRequestDetails(scope.request, data.move_details);
					scope.request.inventory.move_details = scope.details;
					setUpQuoteExplanation();
				});
		}
		
		function setUpQuoteExplanation() {
			if (scope.request.service_type.raw == 7) {
				scope.details.deliveryExplanation = scope.details.deliveryExplanation || scope.longdistanceSettings.delivery_info_text;
			}
			
			if (_.isNull(scope.request.field_quote_explanationn)) {
				let serviceType = scope.request.service_type.raw;
				let isCommercial = scope.request.move_size.raw == 11;
				if (serviceType > 7) {
					serviceType--;
				}
				if (isCommercial) {
					scope.request.field_quote_explanationn = angular.copy(scope.quoteSettings.quoteExplanation['8']);
				} else {
					scope.request.field_quote_explanationn = angular.copy(scope.quoteSettings.quoteExplanation[serviceType]);
				}
			}
		}
		
		function addNewAdditionalBlock() {
			var newAdditionalBlock = {
				title: '',
				text: '',
				isVisible: false,
				confirmationPage: false
			};
			scope.additionalBlock.push(newAdditionalBlock);
			save();
		}
		
		function switchAdditionalBlock() {
			angular.forEach(scope.additionalBlock, function (item) {
				if (scope.isVisible) {
					item.isVisible = true;
					item.confirmationPage = true;
				} else {
					item.isVisible = false;
					item.confirmationPage = false;
				}
			});
		}
		
		function removeNewAdditionalBlock() {
			if (scope.additionalBlock.length > 0) {
				scope.additionalBlock.pop();
			}
			save();
		}
		
		function save() {
			
			var arr = [];
			_.forEach(scope.additionalBlock, function (item, ind) {
				var msg = {
					simpleText: '',
					html: ''
				};
				if (item.title == '' && item.text == '') {
					msg.simpleText = 'Additional Block #' + (ind + 1) + ' was successfully added';
				} else if (item.title != scope.request.request_all_data.additionalBlock[ind].title || item.text != scope.request.request_all_data.additionalBlock[ind].text) {
					msg.simpleText = 'Additional Block #' + (ind + 1) + ' was successfully edited';
					msg.html = '<h3>' + item.title + '</h3>' + item.text;
				}
				if (msg.simpleText)
					arr.push(msg);
			});
			switchAdditionalBlock();
			scope.request.request_all_data.additionalBlock = angular.copy(scope.additionalBlock);
			RequestServices.saveReqData(scope.request.nid, scope.request.request_all_data).then(function () {
				if (!_.isEmpty(arr))
					RequestServices.sendLogs(arr, 'Request was updated', scope.request.nid, 'MOVEREQUEST');
			});
		}
		
		scope.saveComments = () => {
			$rootScope.$broadcast('updateCommercialName');
			save();
			showArrivalTimeWindow();
			RequestServices.updateRequest(scope.request.nid, {field_quote_explanationn: scope.request.field_quote_explanationn});
			
			var nid = scope.request.nid;
			var data = {
				data: {
					details: scope.details
				}
			};
			var jsonDetails = JSON.stringify(data);
			var logArr = [];
			_.forEach(scope.logMsg, function (item) {
				logArr.push(item);
			});
			
			//If storage save for storage also
			if ((scope.request.service_type.raw == 2
					|| scope.request.service_type.raw == 6)
				&& !_.isEqual(scope.request.storage_id, 0)) {
				
				apiService.postData(moveBoardApi.inventoryOld.setInventory + scope.request.storage_id, jsonDetails)
					.then(() => {
						scope.logMsg = {};
						if (!_.isEmpty(logArr)) {
							RequestServices.sendLogs(logArr, 'Request was updated', scope.request.storage_id, 'MOVEREQUEST');
						}
					});
			}
			
			scope.busy = true;
			
			scope.$parent.$parent.request.inventory.move_details = scope.details;
			scope.$parent.$parent.originalDetails = _.clone(scope.details);
			$rootScope.$broadcast('request.details_updated', angular.copy(scope.$parent.$parent.request));
			
			apiService.postData(moveBoardApi.inventoryOld.setInventory + nid, jsonDetails)
				.then(resolve => {
					scope.busy = false;
					scope.logs = resolve.data;
					scope.changed = false;
					if (!_.isEmpty(logArr)) {
						RequestServices.sendLogs(logArr, 'Request was updated', scope.request.nid, 'MOVEREQUEST');
					}
					scope.logMsg = {};
					logger.success('Account settings were updated', resolve.data, 'Success');
				});
		};
		
		function details_change(title, value, field) {
			var to = '';
			var html = '';
			if (title.toLowerCase().search('comments') >= 0) {
				html = value;
			}
			var msg = {
				text: title,
				to: to,
				html: html
			};
			scope.logMsg[field] = msg;
		}
	}
}
