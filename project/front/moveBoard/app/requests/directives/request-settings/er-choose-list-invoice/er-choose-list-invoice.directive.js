'use strict';

angular.module('move.requests')
	.directive('erChooseListInvoice', erChooseListInvoice);

/* @ngInject */
function erChooseListInvoice(requestObservableService, InventoriesServices, CalculatorServices) {
	var directive = {
		scope: {
			request: '=',
		},
		template: require('./choose-list-invoice.template.html'),
		restrict: 'E',
		link: link,
	};

	return directive;

	function link($scope) {
		$scope.invoice = $scope.request.request_all_data.invoice;
		let invoice = $scope.request.request_all_data.invoice;
		$scope.selectedType = invoice.field_useweighttype.value;
		$scope.clickEdit = false;
		const COMMERCIAL = 11;

		$scope.closingWeightInventory = InventoriesServices.calculateClosingWeight($scope.request);
		$scope.closingSizeOfMoveWeight = CalculatorServices.getTotalWeight($scope.request).weight;

		$scope.selectList = function (id) {
			$scope.selectedType = id;
			invoice.field_useweighttype.value = id;

			if (id == 1) {
				invoice.closing_weight.value = $scope.closingSizeOfMoveWeight;
			}

			if (id == 3) {
				$scope.clickEdit = true;
			}

			requestObservableService.setChanges('weightTypeInvoiceChanged');
		};

		$scope.$on('selectList', function (ev, id) {
			$scope.selectList(id);
		});

		$scope.setCustom = function () {
			if (invoice.custom_weight.value.length) {
				if (invoice.move_size.raw == COMMERCIAL) {
					invoice.field_custom_commercial_item.cubic_feet = invoice.custom_weight.value;
				}

				$scope.clickEdit = false;
				$scope.selectedType = 3;
				invoice.field_useweighttype.value = 3;
				requestObservableService.setChanges('inventoryUpdateInvoice');
			}
		};
	}
}
