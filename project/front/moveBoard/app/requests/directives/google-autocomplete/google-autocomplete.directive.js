'use strict';

angular
	.module('move.requests')
	.directive('addressAutocomplete', addressAutocomplete);

addressAutocomplete.$inject = ['Raven', '$location', '$timeout'];

function addressAutocomplete(Raven, $location, $timeout) {
	const HTTPS_PROTOCOL = 'https';
	const GOOGLE_STREET_NUMBER = 'street_number';
	const GOOGLE_ROUTE = 'route';
	const GOOGLE_POSTAL_CODE = 'postal_code';
	const GOOGLE_ADMINISTRATIVE_AREA = 'administrative_area_level_1';
	const GOOGLE_ADMINISTRATIVE_SUBAREA = 'administrative_area_level_2';
	const GOOGLE_LOCALITY = 'locality';
	const GOOGLE_REQUEST_TIMEOUT = 500;

	let directive = {
		restrict: 'A',
		link: linkFunction
	};

	return directive;

	function linkFunction($scope, element, $attrs) {
		let autocomplete;
		let timer;
		let movingFields = _.get($scope, `$parent.${$attrs.field}`);

		element.on('focus', inputFocus);
		element.on('keydown', throttleListener);
		element.on('blur', revokeTimer);

		function revokeTimer () {
			$timeout.cancel(timer);
		}

		function throttleListener(event) {
			event.stopImmediatePropagation();
			revokeTimer();
			timer = $timeout(() => {
				google.maps.event.trigger(element[0], 'focus', {});
			}, GOOGLE_REQUEST_TIMEOUT);
		}

		function inputFocus() {
			let isSecureProtocol = _.isEqual($location.$$protocol, HTTPS_PROTOCOL);

			if (navigator.geolocation && isSecureProtocol) {
				navigator.geolocation.getCurrentPosition(position => {
					let geolocation = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					let circle = new google.maps.Circle({
						center: geolocation,
						radius: position.coords.accuracy
					});
					autocomplete.setBounds(circle.getBounds());
				}, () => {});
			}
		}

		function initAutocomplete() {
			if (!('google' in window)) return;

			let input = element[0];
			let options = {
				types: ['address'],
				componentRestrictions: {country: 'usa'}
			};
			autocomplete = new google.maps.places.Autocomplete(input, options);
			autocomplete.addListener('place_changed', fillInAddress);
		}

		function fillInAddress() {
			let place = autocomplete.getPlace();
			let streetNumber = '';
			let route = '';
			let localityFound = false;

			place.address_components.forEach((field) => {
				let fieldType = _.get(field, 'types[0]');

				if (fieldType === GOOGLE_POSTAL_CODE) {
					movingFields.postal_code = field.short_name;
				}

				if (fieldType === GOOGLE_ADMINISTRATIVE_AREA) {
					movingFields.administrative_area = field.short_name;
				}

				if (fieldType === GOOGLE_LOCALITY) {
					movingFields.locality = field.short_name;
					localityFound = true;
				}
				if (fieldType === GOOGLE_STREET_NUMBER) {
					streetNumber = field.long_name;
				}
				if (fieldType === GOOGLE_ROUTE) {
					route = field.long_name;
				}
				
				if (fieldType === GOOGLE_ADMINISTRATIVE_SUBAREA && !localityFound) {
					movingFields.locality = field.short_name;
				}
			});

			element[0].value = `${streetNumber} ${route}`;
			element.triggerHandler('change');

		}

		initAutocomplete();
	}
}

