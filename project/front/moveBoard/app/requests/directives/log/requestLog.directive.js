'use strict';

angular
	.module('move.requests')
	.directive('ccRequestLog', ccRequestLog);

/* @ngInject */
function ccRequestLog($http, $sce, config) {
	var directive = {
		link: link,
		scope: {
			'nid': '=nid'
		},
		template: require('./saleslog.template.html'),
		restrict: 'A'
	};

	return directive;

	function link(scope, element, attrs) {
		scope.open = false;
		scope.admin_name = attrs.adminName;
		scope.busy = true;
		scope.clientLogs = [];
		scope.systemLogs = [];
		scope.foremanLogs = [];
		scope.tabs = [
			{name: 'System', selected: true},
			{name: 'Foreman', selected: false},
			{name: 'Client', selected: false}
		];

		scope.$on('job_completed', function (event, data) {
			if (data.job_completed) {
				scope.message = 'Job Closed'
			} else {
				scope.message = 'Job Opened'
			}

			scope.message = '';
		});

		scope.$on('request_flag', function (event, data, flag) {
			if (flag == 'remove') {
				scope.message = 'Flag was removed from ' + data.old.name;
			} else {
				scope.message = 'Flag was ';

				if (data.old.name) {
					scope.message += ' changed from ' + data.old.name + ' ';
				} else {
					scope.message += ' set ';
				}

				scope.message += 'to ' + flag.name;
			}

			scope.message = '';
		});

		scope.select = function (tab) {
			_.forEach(scope.tabs, function (tab) {
				tab.selected = false;
			});

			tab.selected = true;
		};

		scope.loadLog = function (nid) {
			$http.get(config.serverUrl + 'server/move_sales_log/' + nid)
				.success(function (data, status, headers, config) {
					scope.logs = _.clone(data);
					_.forEach(data, function (log, key) {
						if (log.event_type == 'client_activity') {
							scope.clientLogs.push(log);
						} else {

							if (log.user_name == 'System' || log.event_type == "parse_log") {
								scope.systemLogs.push(log);
							} else {
								scope.foremanLogs.push(log);
							}
						}
					});
					scope.busy = false;
				});
		};

		scope.loadLog(scope.nid);

		scope.logOpen = function () {
			scope.open = !scope.open;
		};

		scope.toTrustedHTML = function (html) {
			if (angular.isString(html)) {
				return $sce.trustAsHtml(html);
			}
		};

		scope.addMessage = function () {
			if (_.get(scope, 'message.length')) {
				var d = new Date();
				var n = d.getTime();
				var message = {
					text: scope.message,
					date: Math.round(n / 1000),
					event_type: 'custom_log',
					user_name: scope.admin_name ? scope.admin_name : scope.$root.currentUser.userId.name,
				};
				scope.systemLogs.unshift(message);
				// add to DB
				var message = {
					text: scope.message,
					event_type: 'request_update',
					entity_id: scope.nid,
					entity_type: $rootScope.fieldData.enums.entities.MOVEREQUEST
				};
				$http.post(config.serverUrl + 'server/move_sales_log', message).success(function (data, status, headers, config) {
					data.success = true;
				});
				scope.message = '';
			}
		};


		scope.getIconType = function (type) {
			switch (type) {
				case 'mail':
					return 'fa-envelope-o';
					break;
				case 'request':
					return 'fa-folder-o';
					break;
				case 'custom_log':
					return 'fa-comment-o';
					break;
				default:
					return 'fa-bookmark';
			}
		}
	}
}
