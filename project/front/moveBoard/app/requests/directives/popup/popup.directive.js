'use strict';

angular
	.module('move.requests')
	.directive('requestPopup', requestPopup);

requestPopup.$inject = ['$state', '$timeout', 'common', 'EditRequestServices', 'RequestServices', 'datacontext', '$window', 'CalculatorServices', 'serviceTypeService', 'sharedRequestServices'];

function requestPopup($state, $timeout, common, EditRequestServices, RequestServices, datacontext, $window, CalculatorServices, serviceTypeService, sharedRequestServices) {
	const MAX_REQUEST_WIDTH = 12;
	const TARGET_NAME = '_blank';
	const OPEN_REQUEST_PATH = '/moveBoard/#/dashboard/open_request/';
	const PARENT_SCROLL_ELEMENT_SELECTOR = '.mCustomScrollBox';
	const CONTENT_BLOCK_SELECTOR = '.box_info';
	const PARENT_TRACK_LINE_SELECTOR = 'tr';
	const CHECK_CURRENT_POSITION_TIMEOUT = 200;
	const BOTTOM_ELEMENT_OFFSET = 100;
	const POPUP_WIDTH = 350;

	let directive = {
		link: link,
		template: require('./requestinfopopup.html'),
		restrict: 'A',
		scope: {
			'request': '=',
			'overlay': '=',
			'currentModalRequestId': '=?'
		},
	};

	return directive;

	function link(scope, element, attrs) {
		let fieldData = datacontext.getFieldData();
		let calcSettings = angular.fromJson(fieldData.calcsettings);
		scope.calcSettings = calcSettings;
		let localServices = [1, 2, 3, 4, 6, 8];
		scope.isDoubleDriveTime = calcSettings.doubleDriveTime
			&& scope.request.field_double_travel_time
			&& !_.isNull(scope.request.field_double_travel_time.raw)
			&& localServices.indexOf(parseInt(scope.request.service_type.raw)) >= 0;

		scope.$on('request.updated', onRequestUpdated);
		scope.ifright = false;
		scope.width = scope.request.maximum_time.raw > MAX_REQUEST_WIDTH ? MAX_REQUEST_WIDTH : scope.request.maximum_time.raw ;

		if (scope.request.ld_trip) {
			scope.width = 6;
		}

		let currentScript = element.closest('.dhx_cal_event_wrapper');
		let parkLot = element.closest('.park_lot');
		let trucksList = parkLot.find('.trucks');
		let scriptWidth = currentScript.innerWidth() + 10;
		let leftBorderPosition = currentScript.offset().left;
		let rightBorderPosition = leftBorderPosition + scriptWidth;
		let offsetLeft = -leftBorderPosition + trucksList.offset().left + trucksList.innerWidth();
		let offsetRight = parkLot.offset().left + parkLot.innerWidth() - rightBorderPosition;

		if (offsetRight > -offsetLeft) {
			scope.ifright = true;
			scope.left = scriptWidth;
			if (POPUP_WIDTH > offsetRight) {
				scope.left -= POPUP_WIDTH - offsetRight;
			}
		} else {
			scope.ifright = false;
			scope.left = -POPUP_WIDTH;
			if (scope.left < offsetLeft) {
				scope.left = offsetLeft;
			}
		}

		let boxInfo = element.find('.box_info');

		scope.width = attrs.width;
		scope.currentNid = scope.request.nid;
		scope.popup_request = scope.request.source;
		scope.serviceTypeName = serviceTypeService.getCurrentServiceTypeName(scope.popup_request);
		sharedRequestServices.calculateQuote(scope.popup_request);
		scope.currentRate = scope.popup_request.request_all_data.add_rate_discount || scope.popup_request.rate.value;

		if(!scope.popup_request.ld_trip){
			scope.requestWeight = CalculatorServices.getTotalWeight(scope.popup_request).weight;
		}

		if (scope.popup_request.ld_trip && _.get(scope, 'popup_request.trip_info.jobs.items')) {
			scope.popup_request.trip_info.jobs.items.forEach(job => {
				if (job.is_total == 1) {
					scope.popup_request.trip_info.totals = job;
				}
			});

			scope.popup_request.trip_info.jobs_count = 0;

			if (!_.isEmpty(scope.popup_request.trip_info.jobs.items)) {
				scope.popup_request.trip_info.jobs_count = scope.popup_request.trip_info.jobs.items.length - 1;
			}
		}

		whenInitTrySetCurrentPosition();

		scope.switchCurrent = switchCurrent;
		scope.goToTrip = goToTrip;
		scope.closePopup = closePopup;
		scope.openRequest = openRequest;
		scope.openRequestInAnotherBranch = openRequestInAnotherBranch;

		function switchCurrent(req) {
			scope.popup_request = req.source;
			scope.serviceTypeName = serviceTypeService.getCurrentServiceTypeName(scope.popup_request);
			sharedRequestServices.calculateQuote(scope.popup_request);
			scope.currentNid = req.nid;
		}

		function goToTrip(id) {
			scope.closePopup();
			let tab = 0;
			let index = 0;
			$state.go("lddispatch.tripDetails", { id, tab });
			var modals = angular.element(document).find('.hide-request-modal-arrow');

			angular.forEach(modals, item => {
				if (angular.element(item).hasClass('fa-chevron-down')) {
					hideOpenedModal(item, index);
					index++;
				}
			});
		}

		function hideOpenedModal(modal, i) {
			var hideModalTimer = $timeout(function(){
				angular.element(modal).trigger('click');
			}, i * 300);

			scope.$on('destroy', () => {
				$timeout.cancel( hideModalTimer );
			});
		}

		function closePopup() {
			scope.request.showpopup = false;
		}

		function openRequest(request) {
			if (!request) return;

			scope.busy = true;

			RequestServices.getSuperRequest(request.nid)
				.then(function (data) {
					scope.busy = false;
					EditRequestServices.requestEditModal(data.request, data.requests_day, data);
				});
		}

		function openRequestInAnotherBranch(request) {
			let href = makeOpenRequestPath(request);

			$window.open(href, TARGET_NAME);
		}

		function makeOpenRequestPath(request) {
			return request.branch.url + OPEN_REQUEST_PATH + request.nid;
		}

		function onRequestUpdated(event, request) {
			scope.popup_request = request.source;
		}

		function whenInitTrySetCurrentPosition() {
			boxInfo = element.find(CONTENT_BLOCK_SELECTOR);

			if (boxInfo[0]) {
				boxInfo = angular.element(boxInfo[0]);
				setCorrectPosition();
			} else {
				$timeout(() => whenInitTrySetCurrentPosition(), CHECK_CURRENT_POSITION_TIMEOUT);
			}
		}

		function setCorrectPosition() {
			let scrollBox = element.closest(PARENT_SCROLL_ELEMENT_SELECTOR);

			if (scrollBox[0]) {
				let scrollBoxHeight = scrollBox.innerHeight();
				let elementHeight = boxInfo.innerHeight();
				let trackLineElement = element.closest(PARENT_TRACK_LINE_SELECTOR);
				let elementPosition = trackLineElement.position();
				let elementBottom = elementPosition.top + elementHeight;

				let isElementUnderScroll = elementBottom > scrollBoxHeight;

				if (isElementUnderScroll) {
					let newBoxInfoPosition = BOTTOM_ELEMENT_OFFSET - elementHeight;
					boxInfo.css('top', newBoxInfoPosition);
				}
			}
		}
	}
}
