'use strict';
import './email-is-read-tooltip.styl';

angular
	.module('move.requests')
	.directive('ccRequestNewLog', ccRequestNewLog);

/* @ngInject */
function ccRequestNewLog(logger, RequestServices, $sce, AuthenticationService, $timeout, PermissionsServices) {
	var directive = {
		link: link,
		scope: {
			'nid': '=nid',
			'request': '=request',
			'entitytype': '@'
		},
		template: require('./newLog.template.html'),
		restrict: 'A'
	};

	return directive;

	function link(scope, element, attrs) {
		scope.busy = true;
		scope.clientLogs = [];
		scope.systemLogs = [];
		scope.allLogs = [];
		scope.salesLogs = [];
		scope.foremanLogs = [];
		scope.clientLogsShow = {};
		scope.systemLogsShow = {};
		scope.allLogsShow = {};
		scope.salesLogsShow = {};
		scope.foremanLogsShow = {};
		scope.logs = [];
		scope.logsOld = [];
		scope.tabs = [
			{name: 'All', selected: true},
			{name: 'System', selected: false},
			{name: 'Foreman', selected: false},
			{name: 'Sales', selected: false},
			{name: 'Client', selected: false}
		];
		scope.EntityTypes = {
			MOVEREQUEST: 0,
			STORAGEREQUEST: 1,
			LDREQUEST: 2,
			SYSTEM: 3
		};
		const DEPRECATED_USER_BY_SOURCE = {
			superUser: 'Roman Halavach',
			anonymous: 'anonymous user'
		};
		var SYSTEM = 3;
		var needReload = false;
		var currUser = scope.$root.currentUser;
		var userRole = '';

		if (currUser.userRole.indexOf('administrator') >= 0) {
			userRole = 'administrator';
		} else if (currUser.userRole.indexOf('manager') >= 0) {
			userRole = 'manager';
		} else if (currUser.userRole.indexOf('sales') >= 0) {
			userRole = 'sales';
		} else if (currUser.userRole.indexOf('foreman') >= 0) {
			userRole = 'foreman';
		}

		RequestServices.getLogs(scope.nid, scope.EntityTypes[scope.entitytype]).then(function (response) {
			scope.busy = false;
			scope.logs = response;

			_.forEach(scope.logs, function (log) {
				if (_.isArray(log.details)) {
					let sourceLowerCase = log.source.toLowerCase();
					let isDeprecatedSource = _.isEqual(sourceLowerCase, DEPRECATED_USER_BY_SOURCE.anonymous)
						|| _.isEqual(log.source, DEPRECATED_USER_BY_SOURCE.superUser);

					if (isDeprecatedSource) {
						log.source = 'System';
					}

					_.forEach(log.details, function (data) {
						if (!_.isNull(data.date_viewed) && !_.isUndefined(data.date_viewed)) {
							data.date_viewed = moment.unix(data.date_viewed).format('MMMM DD, YYYY h:mm A');
						}

						if (data.title === 'Overbooking Log For Super User' && !PermissionsServices.isSuper()) {
							return;
						}

						if (data.activity.toLowerCase() == 'client') {
							if (log.source == 'client') {
								log.source = 'Client';
							}

							scope.clientLogs.push(log);
						} else if (data.activity.toLowerCase() == 'foreman') {
							scope.foremanLogs.push(log);
						} else if (data.activity.toLowerCase() == 'sales' || data.activity.toLowerCase() == 'manager' || data.activity.toLowerCase() == 'administrator') {
							scope.salesLogs.push(log);
						} else if (data.activity.toLowerCase() == 'system' || log.entity_type == SYSTEM) {
							scope.systemLogs.push(log);
						}

						scope.allLogs.push(log);
					});
				}

				if (typeof log.details == 'string') {
					if (log.details.match(/<(\w+)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/)) {
						var systemLog = {
							details: [{
								title: 'Request event',
								text: [
									{html: $sce.trustAsHtml(log.details)}
								]
							}],
							date: log.date,
							source: _.head(log.source).toLowerCase() == 'cron' ? 'System' : _.head(log.source).charAt(0).toUpperCase() + _.head(log.source).slice(1),
							id: log.id
						};
					} else {
						var systemLog = {
							details: [{
								title: 'Request event',
								text: [
									{text: log.details}
								]
							}],
							date: log.date,
							source: _.head(log.source).toLowerCase() == 'cron' ? 'System' : _.head(log.source).charAt(0).toUpperCase() + _.head(log.source).slice(1),
							id: log.id
						};
					}
					scope.allLogs.push(systemLog);
					scope.systemLogs.push(systemLog);
				}
			});

			$timeout(() => {
				element.find('a').on('click', e => {
					if (!needReload && $(e.target).text() == 'View Request Page') {
						AuthenticationService.Logout()
							.then(function (data) {
								localStorage.setItem('currentLogin', (new Date()).getTime());
								needReload = true;
								location.reload();
								return true;
							});
					}
				});
			}, 50);

		}, function (err) {
			scope.busy = false;
			logger.error(err, err, 'Error');
		});

		scope.dateFormatter = dateFormatter;
		scope.select = select;
		scope.addMessage = addMessage;
		scope.toTrustedHTML = toTrustedHTML;
		scope.getIconType = getIconType;
		scope.getIconLogType = getIconLogType;
		scope.isItArray = isItArray;

		function dateFormatter(date) {
			return moment.unix(date).format('l') + ' at ' + moment.unix(date).format('LTS');
		}

		function select(tab) {
			_.forEach(scope.tabs, function (tab) {
				tab.selected = false;
			});

			tab.selected = true;
		}

		function addMessage() {
			if (!_.get(scope, 'message.length')) {
				return false;
			}

			scope.busy = true;
			var msg = {
				entity_id: scope.nid,
				entity_type: scope.EntityTypes[scope.entitytype],
				data: [{
					details: [{
						title: 'Message was successfully sent',
						text: [],
						activity: userRole,
						date: moment().unix()
					}],
					source: currUser.userId.field_user_first_name + ' ' + currUser.userId.field_user_last_name
				}]
			};
			var obj = {
				simpleText: scope.message
			};
			var info = {
				browser: AuthenticationService.get_browser(),
			};
			msg.data[0].details[0].info = {};
			msg.data[0].details[0].info = info;
			msg.data[0].details[0].text.push(obj);

			RequestServices.postLogs(msg).then(function (data) {
				scope.busy = false;
				msg.data[0].date = msg.data[0].details[0].date.toString();

				if (msg.data[0].details[0].activity.toLowerCase() == 'client') {
					scope.clientLogs.push(msg.data[0]);
				} else if (msg.data[0].details[0].activity.toLowerCase() == 'foreman') {
					scope.foremanLogs.push(msg.data[0]);
				} else if (msg.data[0].details[0].activity.toLowerCase() == 'sales' || msg.data[0].details[0].activity.toLowerCase() == 'manager' || msg.data[0].details[0].activity.toLowerCase() == 'administrator') {
					scope.salesLogs.push(msg.data[0]);
				} else if (msg.data[0].details[0].activity.toLowerCase() == 'system' || log.entity_type == SYSTEM) {
					scope.systemLogs.push(msg.data[0]);
				}

				scope.allLogs.push(msg.data[0]);
				scope.message = [];
			}, function (reason) {
				scope.busy = false;
				logger.error(reason, reason, 'Error');
			});
		}

		function toTrustedHTML(html) {
			if (angular.isString(html)) {
				return $sce.trustAsHtml(html);
			}
		}

		function getIconLogType(title, arr) {
			var types = ['payment', 'request', 'message', 'contract', 'surcharge', 'valuation', 'packing', 'discount', 'additional', 'mail'];

			if (!title) {
				return false;
			}

			var head = title.toLowerCase();
			var type = '';

			_.forEach(types, function (t) {
				if (head.search(t) >= 0) {
					type = t;
				}
			});

			if (arr) {
				_.forEach(types, function (t) {
					_.forEach(arr.text, function (item) {
						if (item.simpleText) {
							var simpleText = item.simpleText.toLowerCase();
							if (simpleText.search(t) >= 0) {
								type = t;
							}
						}
					});
				});
			}

			switch (type) {
				case 'valuation':
					return 'icon-wallet';
					break;
				case 'surcharge':
					return 'fa-tint';
					break;
				case 'payment':
					return 'fa-credit-card';
					break;
				case 'request':
					return 'fa-cog';
					break;
				case 'message':
					return 'fa-comment-o';
					break;
				case 'contract':
					return 'fa-pencil-square-o';
					break;
				case 'packing':
					return 'fa-archive';
					break;
				case 'discount':
					return 'fa-tags';
					break;
				case 'additional':
					return 'fa-list-alt';
					break;
				case 'mail':
					return 'fa-envelope-o';
					break;
				default:
					return 'fa-bookmark';
			}
		}

		function getIconType(type) {
			switch (type) {
				case 'mail':
					return 'fa-envelope-o';
					break;
				case 'request':
					return 'fa-folder-o';
					break;
				case 'custom_log':
					return 'fa-comment-o';
					break;
				default:
					return 'fa-bookmark';
			}
		}

		function isItArray(detail) {
			return _.isArray(detail.text);
		}
	}
}
