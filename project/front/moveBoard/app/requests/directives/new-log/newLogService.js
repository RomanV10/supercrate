/**
 * Settings service
 */
angular.module('move.requests')
    .factory('newLogService', newLogService);

newLogService.$inject = ['RequestServices'];

function newLogService(RequestServices) {
    let service = {};

    service.createLogs = createLogs;
    service.sendOverbookingLogs = sendOverbookingLogs;

    return service;

    function createLogs(createdFrom, fieldData, editrequest, weight){
        let msg = {
            simpleText: createdFrom
        };
        let arr = [msg];
        let notUseFields = ['status', 'field_actual_start_time', 'field_start_time_max', 'inventory', 'field_useweighttype', 'uid', 'field_custom_commercial_item'];

        _.forEach(editrequest, function (item, key) {
            if(!item) return;
            var msg = '';
            if (_.indexOf(notUseFields , key) == -1 && item) {
                var fieldName = key.replace(/field_/g, "").replace(/_/g, " ").charAt(0).toUpperCase() + key.replace(/field_/g, "").replace(/_/g, " ").slice(1);
                if(key == 'field_moving_from' || key == 'field_moving_to'){
                    if (!item.postal_code.length) return;
                    msg = addressLog(item, fieldName);
                    let m = {
                        simpleText: msg
                    };
                    arr.push(m);
                    return;
                }
                if(fieldName == 'Date') {
                    fieldName = 'Move Date';
                    let date = item;
                    if(item.date)
                        date = item.date;
                    msg = fieldName + ': '+ moment(date).format('MMM DD, YYYY');
                    let m = {
                        simpleText: msg
                    };
                    arr.push(m);
                    return;
                }
                msg = fieldName + ': ';
                if (_.isObject(item) && !_.isArray(item)) {
                    _.forEach(item, function (el, i) {
                        if (fieldData[key]) {
                            i = fieldData[key][i];
                        }

                        msg += i.toString().replace(/_/g, " ").charAt(0).toUpperCase() + i.toString().replace(/_/g, " ").slice(1) + ' - ' + el;

                        if (!_.last(item)) {
                            msg += ', ';
                        }

                        if (_.last(item)) {
                            msg += '.';
                        }
                    });
                } else if (_.isArray(item) && !_.isEmpty(item)) {
                    _.forEach(item, function (el) {
                        var room = '';
                        if (fieldData[key]) {
                            room = fieldData[key][el];
                        }

                        msg += room.toString().replace(/_/g, " ").charAt(0).toUpperCase() + room.toString().replace(/_/g, " ").slice(1);

                        if (_.last(item) != el) {
                            msg += ', ';
                        }

                        if (_.last(item) == el) {
                            msg += '.';
                        }
                    });
                } else {
                    if (fieldData[key]) {
                        item = fieldData[key][item];
                    }

                    if (item) {
                        msg += item.toString().replace(/_/g, " ").charAt(0).toUpperCase() + item.toString().replace(/_/g, " ").slice(1);
                    }
                }
                var m = {
                    simpleText: msg
                };
                arr.push(m);
            }
        });
        if(weight){
            let msg = {
                simpleText: "Weight: " + weight.toFixed(2) + " c.f."
            };
            arr.push(msg);
        }
        return arr;
    }

    function addressLog(obj, field){
        return (field + ': ' + obj.locality + ' ' + obj.administrative_area + ', ' + obj.postal_code);
    }

	function sendOverbookingLogs(data, response, nid) {
		let logs = [{text: `Overbooking status: ${response}`}];

		_.forEach(data, ((item, key) => {
			if (_.isEqual(key, 'nid')) {
				return;
			}
			if (_.isArray(item)) {
				item = item[0];
			}
			logs.push({
				text: `${key.replace(/_/g, ' ')
					.replace('field ', '')
					.replace(/\b\w/g, value => (value ? value.toUpperCase() : ''))}: ${(item ? item.toString() : '')}`
			});
		}));

		RequestServices.sendLogs(logs, 'Overbooking Log For Super User', nid, 'MOVEREQUEST');
	}

}
