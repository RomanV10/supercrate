import './navigate-calendar.styl'

(function () {
    'use strict';

    angular.module('move.requests')
           .directive('navigateCalendar', navigateCalendar);

    function navigateCalendar() {
        return {
            restrict: 'E',
            template: require('./calendar.template.html'),
            scope: {
                curDate: '='
            },
            link: linker
        };

        function linker($scope, element) {
            $scope.openCalendar = openCalendar;
            $scope.goToPrevDay = goToPrevDay;
            $scope.goToNextDay = goToNextDay;

            function openCalendar() {
                element.find('.ui-datepicker-trigger').click();
            }

            function goToPrevDay() {
                $scope.curDate = moment($scope.curDate).subtract(1, 'days').format('dddd, MMMM DD, YYYY');
            }

            function goToNextDay() {
                $scope.curDate = moment($scope.curDate).add(1, 'days').format('dddd, MMMM DD, YYYY');
            }
        }
    }

})();