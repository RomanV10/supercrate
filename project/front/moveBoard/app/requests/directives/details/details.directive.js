import './details.styl';

(function () {
	'use strict';

	angular
		.module('move.requests')
		.directive('ccDetails', ccDetails);

	ccDetails.$inject = ['$http','config','logger','Lightbox', 'RequestServices','common','datacontext','CalculatorServices',
		'$uibModal', '$rootScope', 'SettingServices', 'LongDistanceServices', 'InitRequestFactory', 'apiService', 'moveBoardApi', 'SweetAlert', 'requestObservableService'];

	function ccDetails ($http,config,logger, Lightbox, RequestServices,common,datacontext,CalculatorServices,
		$uibModal,$rootScope, SettingServices, LongDistanceServices, InitRequestFactory, apiService, moveBoardApi, SweetAlert, requestObservableService) {
		return {
			link: linkFn,
			scope: {
				'request': '='
			},
			template: require('./details.template.html'),
			restrict: 'A'
		};

		function linkFn(scope) {
			const TIME_ZONE_OFFSET = - new Date().getTimezoneOffset() * 60;
			var nid = scope.request.nid;
			var init_pic_quantity = scope.request.field_move_images.length;
			var imgReq = {
				field_move_images: {
					add: [],
					del: []
				}
			};

			scope.additionalBlock = angular.copy(scope.request.request_all_data.additionalBlock || [{}]);

			scope.changed = false;
			scope.busy = true;

			scope.range = common.range;
			var fieldData = datacontext.getFieldData();
			scope.longdistanceSettings = angular.fromJson(fieldData.longdistance);
			scope.quoteSettings = angular.fromJson(fieldData.customized_text);

			scope.service = scope.request.service_type.raw;
			scope.status = scope.request.status.raw;
			scope.from = scope.request.from;
			scope.to = scope.request.to;
			scope.entarence_origin = scope.request.type_from.value;
			scope.entarence_dest = scope.request.type_to.value;
			var originDetails = {};

			//field_move_images[]
			//{fid, title, url}
			scope.logMsg = {};

			scope.loadDetails = loadDetails;
			scope.details_change = details_change;
			loadDetails(nid);

			scope.$watchGroup([
				'details.pickup',
				'details.delivery'
			], function() {
				scope.changed = true;
			}
			);

			function loadDetails(nid){
				$http.get(config.serverUrl + 'server/move_invertory/' + nid)
					.success(function(data, status, headers, config) {
						scope.busy = false;
						scope.details = InitRequestFactory.initRequestDetails(scope.request, data.move_details);
						scope.request.inventory.move_details = scope.details;
						scope.deliveryDateOldValue = scope.details.delivery;
                      	setUpQuoteExplanation();
						initDefaults();
					});
			}

			function initDefaults() {
	            scope.disableDeliveryDatesCheckbox = {};
				if (scope.details.delivery == '') {
	              scope.disableDeliveryDatesCheckbox.disableInput = true;
	              scope.disableDeliveryDatesCheckbox.checkboxValue = true;
				}
			}

			function setUpQuoteExplanation() {
				scope.moving_explanation = scope.request.field_quote_explanationn || scope.quoteSettings.quoteExplanation[scope.request.service_type.raw];
			}
			function setDateLogs() {
				var obj = {
					text: 'Delivery Date changed',
					from: scope.deliveryDateOldValue,
	                to: scope.delivery_disable ? 'Don\'t Know Yet' : scope.request.inventory.move_details.delivery
				};
				var arr = [];
				arr.push(obj);
				_.forEach(scope.logMsg, function (log) {
					arr.push(log);
				});
				if (!_.isEmpty(arr)) {
					sendNotificationAfterSaveChanges(arr, scope.request.nid, 'MOVEREQUEST');
					RequestServices.sendLogs(arr, 'Request was updated', scope.request.nid, 'MOVEREQUEST');
				}
				scope.deliveryDateOldValue = scope.details.delivery;
			}
			scope.saveDetails = () => {
				let lastLdDeliveryDayTemp = CalculatorServices.getLongDistanceWindow(scope.request, scope.request.inventory.move_details.delivery, scope.request.inventory.move_details.delivery_days);
				if (scope.request.inventory.move_details.delivery.length) {
					var lastLdDeliveryDay = scope.request.inventory.move_details.delivery_days == 0 ? undefined : moment(lastLdDeliveryDayTemp).unix() + TIME_ZONE_OFFSET;
				}
				var nid = scope.request.nid;
				var data = {
					data: {
						details: scope.details,
						field_ld_sit_delivery_dates: {
							value: scope.request.inventory.move_details.delivery.length ? moment(scope.request.inventory.move_details.delivery).unix() + TIME_ZONE_OFFSET : undefined,
							value2: lastLdDeliveryDay
						}
					}
				};
				var jsonDetails = JSON.stringify(data);
				var logArr = [];
				_.forEach(scope.logMsg, function (item) {
					logArr.push(item);
				});
				if (scope.deliveryDateOldValue != scope.details.delivery) {
					setDateLogs();
				}
				//If storage save for storage also
				if ((scope.request.service_type.raw == 2
                        || scope.request.service_type.raw == 6)
                    && !_.isEqual(scope.request.storage_id, 0)) {

					$http.post(config.serverUrl + 'server/move_invertory/' + scope.request.storage_id,jsonDetails)
						.success(function(data, status, headers, config) {
							scope.logMsg = {};
							if(!_.isEmpty(logArr))
								RequestServices.sendLogs(logArr, 'Request was updated', scope.request.storage_id, 'MOVEREQUEST');
						});
				}

				scope.busy = true;

				scope.$parent.$parent.select(scope.$parent.$parent.tabs[0]);
				scope.$parent.$parent.request.inventory.move_details = scope.details;
				scope.$parent.$parent.originalDetails = _.clone(scope.details);
				requestObservableService.setChanges('detailsUpdated');

				$http.post(config.serverUrl + 'server/move_invertory/' + nid,jsonDetails)
					.success(function(data, status, headers, config) {
						scope.busy = false;
						scope.logs = data;
						scope.changed = false;
						if(!_.isEmpty(logArr))
							RequestServices.sendLogs(logArr, 'Request was updated', scope.request.nid, 'MOVEREQUEST');
						scope.logMsg = {};
						logger.success('Details were updated', data, 'Success');
					});
				if (init_pic_quantity !== scope.request.field_move_images.length) {
					init_pic_quantity = scope.request.field_move_images.length;
					angular.forEach(scope.request.field_move_images, function(img, i){
						imgReq.field_move_images.add.push({
							uri: img.url,
							title: img.title
						});
						imgReq.field_move_images.del.push({
							fid: img.fid
						});
					});
					RequestServices.updateRequest(nid, imgReq)
						.then(function (response) {
							console.log(response);
						}
						);
					console.log('Save images');
				}
			};

			scope.showDetailImagesModal = function () {
				$uibModal.open({
					template: require('./preview-images-modal.html'),
					controller: previewImageModalCtrl,
					size: 'lg',
					backdrop: false,
					resolve: {
						images: function () {
							return scope.request.field_move_images;
						},
						imgReq: function () {
							return imgReq;
						}
					}
				});
			};

            function previewImageModalCtrl($scope, $uibModalInstance, images, imgReq, Lightbox) {
                $scope.images = images;
                $scope.openLightboxModal = function(index) {
                    Lightbox.openModal(images, index);
                };

				$scope.removeImg = function(fid, index) {
					$scope.images.splice(index,1);
					imgReq.field_move_images.del.push({
						fid: fid
					});
				};

                $scope.close = function() {
						 $uibModalInstance.dismiss('cancel');
                }
            }

	        scope.disableDeliveryDate = function (disable) {
		        if (disable && scope.details.delivery) {
			        SweetAlert.swal({
                        title: "Do you want to delete first delivery date?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            scope.details.delivery = '';
                            scope.disableDeliveryDatesCheckbox.disableInput = true;
                        } else {
                            scope.disableDeliveryDatesCheckbox.disableInput = false;
                            scope.disableDeliveryDatesCheckbox.checkboxValue = false;
                        }
                    });
                } else {
                    scope.disableDeliveryDatesCheckbox.disableInput = disable;
                }
            };

			scope.showEditAdditionalBlockModal = function () {
				$uibModal.open({
					template: require('./edit-additional-block-modal.html'),
					controller: editAdditionalBlockModalCtrl,
					size: 'lg',
					backdrop: false,
					resolve: {
						request: function () {
							return scope.request;
						}
					}
				});
			};

            function editAdditionalBlockModalCtrl($scope, $uibModalInstance, request, RequestServices) {
                $scope.additionalBlock = angular.copy(request.request_all_data.additionalBlock);

                $scope.close = function() {
						 $uibModalInstance.dismiss('cancel');
                };

                $scope.save = function () {
                    var msg = {
                        simpleText: ''
                    };
                    if(request.request_all_data.additionalBlock.title == '' && request.request_all_data.additionalBlock.text == '')
                        msg.simpleText = 'Additional Block was successfully added';
                    else
                        msg.simpleText = 'Additional Block was successfully edited';
                    var arr = [];
                    arr.push(msg);
                    request.request_all_data.additionalBlock = $scope.additionalBlock;
                    RequestServices.saveReqData(request.nid,request.request_all_data).then(function () {
                        if(!_.isEmpty(arr))
                            RequestServices.sendLogs(arr, 'Request was updated', request.nid, 'MOVEREQUEST');
                    });
						 $uibModalInstance.dismiss('cancel');
                }
            }
            function sendNotificationAfterSaveChanges(arr, nid, entity_type) {
                let entityType = fieldData.enums.entities[entity_type];
                let enumType = fieldData.enums.notification_types.MANAGER_DO_CHANGE_WITH_REQUEST;
                let data = {
                    node: {nid: nid},
                    notification: {
                        text: ''
                    }
                };
                let notificationMsg = '';
                _.forEach(arr, function (msg) {
                    notificationMsg += msg.text || msg.simpleText;
                    if (notificationMsg.substr(notificationMsg.length - 1) == ".") {
                        notificationMsg = notificationMsg.slice(0, -1);
                    }

					if (msg.from && msg.to) {
						notificationMsg += ' from: ' + msg.from + ', to: ' + msg.to;
					} else if (msg.to) {
						notificationMsg += ' to: ' + msg.to;
					}
					notificationMsg += '\n';
				});
				data.notification.text = notificationMsg;
				let dataNotify = {
					type: enumType,
					data: data,
					entity_id: nid,
					entity_type: entityType
				};
				apiService.postData(moveBoardApi.notifications.createNotification, dataNotify);
			}
			function details_change(title, value, field) {
				var parking = {
					None: 'Please Select',
					PR: 'Street Parking',
					PM: 'Loading Dock',
					LZA: 'Commercial Loading Zone',
					PDW: 'Private Driveway',
					DK: 'Don\'t Know'
				};
				var to = '';
				var html = '';
				if(title.toLowerCase().search('distance') >= 0){
					to = value + ' feet';
					if(value == 1){
						to = 'Select approximate distances';
					}
					if(value == 2){
						to = 'Don\'t Know';
					}
					if(value == 301){
						to = '>250 feet';
					}
				}else if(title.toLowerCase().search('stairs') >= 0){
					to = value + '  steps';
					if(value == 1){
						to = 'Select number of steps';
					}
					if(value == 2){
						to = 'Don\'t Know';
					}
				}else if(title.toLowerCase().search('parking') >= 0){
					to = parking[value];
				}else if(title.toLowerCase().search('comments') >= 0){
					html = value;
				}
				var msg = {
					text: title,
					to: to,
					html: html
				};
				scope.logMsg[field] = msg;
			}
		}
	}
})();
