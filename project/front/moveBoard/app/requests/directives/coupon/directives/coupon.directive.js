/* coupon.directive.js */

/**
 * @desc coupon directive for user
 * @example <div cc-move-coupons></div>
 */
(function () {
  'use strict';

  angular
    .module('move.requests')
    .directive('ccMoveCoupons', ccMoveCoupons);

  ccMoveCoupons.$inject = ['SweetAlert', 'MoveCouponService', 'datacontext', 'Session'];


  function ccMoveCoupons(SweetAlert, MoveCouponService, datacontext, Session) {
    var directive = {
      template: require('../templates/coupons.html'),
      scope:{},
      restrict: 'AE',
      link: linkFn
    };
    return directive;


    function linkFn(scope, element, attrs) {
      scope.busy = false;
      scope.currCoupons = [];
      scope.fieldData = datacontext.getFieldData();
      scope.settings = angular.fromJson(scope.fieldData.basicsettings);

      scope.setRequestNid = setRequestNid;

      function setRequestNid(){
          Session.addRequestNid(scope.$parent.request.nid);
      }

      MoveCouponService.getCouponsByAdmin().then(function (data) {
        _.forEach(data, function (item) {
          if(item.value.active && item.value.coupons_left != 0){
            scope.currCoupons.push(item);
          }
        })
      });

    }
  }
})();
