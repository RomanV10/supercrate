(function () {

  angular
    .module('move.requests')
    .factory('MoveCouponService', MoveCouponService);

  MoveCouponService.$inject = ['$http', '$rootScope', '$q', 'config', 'AuthenticationService', 'datacontext'];

  function MoveCouponService($http, $rootScope, $q, config, AuthenticationService, datacontext) {

    var service = {};

    service.createCoupon = createCoupon;
    service.getCouponById = getCouponById;
    service.updateCouponById = updateCouponById;
    service.deleteCouponById = deleteCouponById;
    service.getCouponsByAdmin = getCouponsByAdmin;
    /**
     * requests for users' coupons
     */
    service.getPurchased = getPurchased;
    service.getUserCoupon = getUserCoupon;
    service.getUserCoupons = getUserCoupons;
    service.getUnusedUserCouponsAdmin = getUnusedUserCouponsAdmin;
    service.getUnusedUserCoupons = getUnusedUserCoupons;
    service.updateUserCoupon = updateUserCoupon;
    service.deleteUserCoupon = deleteUserCoupon;

    service.getCouponByHash = getCouponByHash;
    service.getCouponByPromo = getCouponByPromo;


    function createCoupon(info){
      var data = {
        data: info
      };
      var deferred = $q.defer();
      $http.post(config.serverUrl+'server/move_coupon',data)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function getCouponById(id){
      var deferred = $q.defer();
      $http.get(config.serverUrl+'server/move_coupon/'+id)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function updateCouponById(info, id){
      var data = {
        data: info
      };
      var deferred = $q.defer();
      $http.put(config.serverUrl+'server/move_coupon/'+id,data)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function deleteCouponById(id){
      var deferred = $q.defer();
      $http.delete(config.serverUrl+'server/move_coupon/'+id)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function getCouponsByAdmin(){
      var deferred = $q.defer();
      $http.get(config.serverUrl+'server/move_coupon')
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function getPurchased(){
      var deferred = $q.defer();
      $http.post(config.serverUrl+'server/move_coupon_user/get_purchased_coupons')
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function getUnusedUserCouponsAdmin(uid){
        var data = {
            uid: uid
        };
      var deferred = $q.defer();
      $http.post(config.serverUrl+'server/move_coupon_user/get_not_used_coupons_for_admin', data)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function getUnusedUserCoupons(){
      var deferred = $q.defer();
      $http.post(config.serverUrl+'server/move_coupon_user/get_not_used_coupons_for_user')
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function getUserCoupons(uid){
      var deferred = $q.defer();
      $http.get(config.serverUrl+'server/move_coupon_user')
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function getUserCoupon(uid){
      var deferred = $q.defer();
      $http.get(config.serverUrl+'server/move_coupon_user/'+uid)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function updateUserCoupon(info, id){
      var data = {
        data: info
      };
      var deferred = $q.defer();
      $http.put(config.serverUrl+'server/move_coupon_user/'+id,data)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function deleteUserCoupon(id){
      var deferred = $q.defer();
      $http.delete(config.serverUrl+'server/move_coupon_user/'+id)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function getCouponByHash(hash){
      var deferred = $q.defer();
      var data = {
        hash: hash
      };
      $http.post(config.serverUrl+'server/move_coupon/hash_coupon',data)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }
    function getCouponByPromo(promo){
      var deferred = $q.defer();
      var data = {
        promo: promo
      };
      $http.post(config.serverUrl+'server/move_coupon/promo_coupon',data)
        .success(function(data) {
          deferred.resolve(data);
        })
        .error(function(data) {
          deferred.reject(data);
        });
      return deferred.promise;
    }

    return service;

  }
})();
