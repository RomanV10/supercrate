import './parser-provider-name.styl';

angular.module('move.requests')
	.directive('parserProviderName', parserProviderName);

/* @ngInject */
function parserProviderName(RequestServices, pollSourcesService) {
	return {
		restrict: 'E',
		scope: {
			request: '='
		},
		template: require('./parser-provider-name.html'),
		link,
	};

	function link($scope, $element) {
		$scope.sources = pollSourcesService.getRequestPollSources();

		$scope.onSelectParserProvider = onSelectParserProvider;
		$scope.isPresentSource = $scope.sources.find(source => source == $scope.request.field_parser_provider_name) || _.isEmpty($scope.request.field_parser_provider_name);

		function onSelectParserProvider() {
			RequestServices.updateRequest($scope.request.nid, {
				field_parser_provider_name: $scope.request.field_parser_provider_name
			}).then(() => {
				toastr.success('Source was applied!');
			});
		}
	}
}
