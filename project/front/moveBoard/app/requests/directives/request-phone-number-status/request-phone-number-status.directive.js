import './request-phone-number-status.styl';

angular.module('move.requests')
	.directive('requestPhoneNumberStatus', requestPhoneNumberStatus);

/* @ngInject */
function requestPhoneNumberStatus(datacontext) {
	const PHONE_NUMBER_IS_NOT_CHECKED = 1;
	const PHONE_NUMBER_IS_OK = 2;
	const PHONE_NUMBER_IS_WRONG = 3;
	const PHONE_NUMBER_IS_UNSUBSCRIBED = 4;
	const PHONE_STATUSES = {
		'queued': PHONE_NUMBER_IS_OK,
		'failed': PHONE_NUMBER_IS_WRONG,
		'invalid phone': PHONE_NUMBER_IS_WRONG,
		'sent': PHONE_NUMBER_IS_OK,
		'delivered': PHONE_NUMBER_IS_OK,
		'undelivered': PHONE_NUMBER_IS_OK,
		'blacklist': PHONE_NUMBER_IS_UNSUBSCRIBED,
	};

	return {
		restrict: 'E',
		scope: {
			request: '<',
		},
		template: require('./request-phone-number-status.html'),
		link,
	};

	function link($scope) {
		let {smsEnable = 0} = datacontext.getFieldData();
		$scope.smsEnable = !!Number(smsEnable);
		onChangeList($scope.request.messagesList);

		$scope.$watchCollection('request.messagesList', onChangeList);

		$scope.phoneStatus = 1;

		function onChangeList(newMessages) {
			if (!_.isEmpty(newMessages)) {
				let sms = _.find(newMessages, (sms) => sms.sms == 1);

				if (sms) {
					$scope.phoneStatus = PHONE_STATUSES[sms.sms_status] || PHONE_NUMBER_IS_WRONG;
				}
			}
		}
	}
}
