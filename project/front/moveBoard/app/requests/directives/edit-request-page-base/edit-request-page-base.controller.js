'use strict';

import {
	TABS
} from '../../../inhome-estimator-portal/constants/request-tabs';

angular
	.module('app')
	.controller('EditRequestPageBaseController', EditRequestPageBaseController);

/*@ngInject*/

function EditRequestPageBaseController($scope, datacontext, RequestServices, SweetAlert, CalculatorServices,
	InventoriesServices, CheckRequestService, SendEmailsService, EditRequestServices, $rootScope, AuthenticationService,
	common, $uibModal, erDeviceDetector, ParserServices, logger, $interval, MessagesServices, serviceTypeService,
	requestObservableService) {
	let initial_request = $scope.initialRequest;
	let superResponse = $scope.superResponse;
	let requests = $scope.requests;
	let isDelivery = $scope.isDelivery;
	let $uibModalInstance = $scope.modalInstance;
	const DESKTOP_INTERVAL = 15000;
	const DEVICES_INTERVAL = 100000;
	let updateMessageTime = erDeviceDetector.isDesktop ? DESKTOP_INTERVAL : DEVICES_INTERVAL;

	const HOME_ESTIMATE_STATUSES = {
		'1': 'Schedule',
		'2': 'Done',
		'3': 'Canceled',
	};

	$scope.requestCustomEventWatcher = {
		inventory_update: false,
		editRequestLoaded: false,
		detailsUpdated: false,
		weightTypeChanged: false,
	};

	$scope.isDesktop = erDeviceDetector.isDesktop;
	$scope.fieldData = datacontext.getFieldData();
	$scope.basicsettings = angular.fromJson($scope.fieldData.basicsettings);
	$scope.request = angular.copy(initial_request);

	$scope.tabs = angular.copy(TABS);
	$scope.tabs[0].name += $scope.request.nid;
	$scope.template = $scope.tabs[0];

	isDraft($scope.request.uid);

	if ($scope.request.service_type.raw == 5) {
		$scope.flatsettings = angular.fromJson($scope.fieldData.longdistance);
		$scope.request.option_text = $scope.flatsettings.default_flat_option;
	}

	$scope.requests = requests;
	$scope.homeestimates = superResponse.home_estimate;
	$scope.userForHomeEstimate = superResponse.user_for_estimate;
	$scope.editrequest = {};
	$scope.editinvoice = {};
	$scope.message = {};
	$scope.fieldData = datacontext.getFieldData();
	$scope.closed = false;
	$scope.canedit = true;
	let userRole = _.get($rootScope, 'currentUser.userRole', []);
	$scope.admin = AuthenticationService.isAdmin(userRole);
	$scope.disableTabs = false;
	$scope.services = serviceTypeService.getAllServiceTypes();

	$scope.isInventory = false;
	if (isDelivery) {
		$scope.request.isDelivery = isDelivery;
	}

	$scope.companyFlags = [];
	$scope.ifStorage = $scope.request.service_type.raw == 2 || $scope.request.service_type.raw == 6;
	var inv = [];
	if ($scope.request.inventory) {
		if ($scope.request.inventory.inventory_list) {
			inv = angular.copy($scope.request.inventory.inventory_list);
		}
	}
	if (($scope.request.service_type.raw == 2 || $scope.request.service_type.raw == 6) && $scope.request.request_all_data && $scope.request.request_all_data.toStorage) {
		if ($scope.request.request_all_data.additional_inventory) {
			_.forEach($scope.request.request_all_data.additional_inventory, function (item) {
				item.additional = true;
				inv.push(item);
			});
		}
	}

	$scope.request.inventory.inventory_list = angular.copy(inv);

	if (angular.isDefined($scope.basicsettings.companyFlags)) {
		$scope.companyFlags = angular.copy($scope.basicsettings.companyFlags);

		for (var i in $scope.companyFlags) {
			for (var id in $scope.request.field_flags.value) {
				if (id == $scope.companyFlags[i].id) {
					$scope.currentFlag = $scope.companyFlags[i];
				}
			}
		}
	}

	EditRequestServices.getQuickBookFlag($scope.request);

	$scope.saveChanges = saveChanges;
	$scope.updateMessage = updateMessage;

	function saveChanges() {
		$scope.$broadcast('save-edit-request');
	}

	let loadMessageInterval = $interval(() => {
		getRequestMessages();
	}, updateMessageTime);

	getRequestMessages();

	$scope.removeFlag = function (nid) {
		var arr = [];
		$scope.busy = true;

		RequestServices.addFlagToRequest(arr, nid).then(function (data) {
			$scope.busy = false;

			if (angular.isDefined($scope.request.field_flags)) {
				if (angular.isDefined($scope.request.field_flags.value)) {
					for (var i in $scope.request.field_flags.value) {
						delete $scope.request.field_flags.value[i];
					}
				} else {
					$scope.request.field_flags.value = {};
				}
			} else {
				$scope.request.field_flags = {};
				$scope.request.field_flags.value = {};
			}
			// SweetAlert.swal("Success", "Flag has been removed.", "success");
			$rootScope.$broadcast('request_flag', {
				nid: $scope.request.nid,
				old: $scope.currentFlag
			}, 'remove');
			$rootScope.$broadcast('request_flag_table', {
				nid: $scope.request.nid,
				old: $scope.currentFlag
			}, 'remove');
			if ($scope.currentFlag) {
				var message = 'Flag was removed from ' + $scope.currentFlag.name;
				var obj = {
					simpleText: message
				};
				var arr = [obj];
			}
			if ($scope.currentFlag) {
				RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
			}
			$scope.currentFlag = '';
		});
	};

	$scope.changeFlag = function (flag) {
		var currFlag = flag;
		var arr = [];
		arr.push(flag.id);
		$scope.busy = true;

		RequestServices.addFlagToRequest(arr, $scope.request.nid).then(function (data) {
			$scope.busy = false;

			if (angular.isDefined($scope.request.field_flags)) {
				if (angular.isDefined($scope.request.field_flags.value)) {
					for (var i in $scope.request.field_flags.value) {
						delete $scope.request.field_flags.value[i];
					}

					$scope.request.field_flags.value[currFlag.id] = currFlag.name;
				} else {
					$scope.request.field_flags.value = {};
					$scope.request.field_flags.value[currFlag.id] = currFlag.name;
				}
			} else {
				$scope.request.field_flags = {};
				$scope.request.field_flags.value = {};
				$scope.request.field_flags.value[currFlag.id] = currFlag.name;
			}
			// SweetAlert.swal("Success", "Flag was updated.", "success");
			$rootScope.$broadcast('request_flag', {
				nid: $scope.request.nid,
				old: $scope.currentFlag
			}, currFlag);
			$rootScope.$broadcast('request_flag_table', {
				nid: $scope.request.nid,
				old: $scope.currentFlag
			}, currFlag);
			var message = 'Flag was ';
			if ($scope.currentFlag && $scope.currentFlag.name) {
				message += ' changed from ' + $scope.currentFlag.name + ' ';
			} else {
				message += ' set ';
			}
			message += 'to ' + currFlag.name;

			var obj = {
				simpleText: message
			};
			var arr = [obj];
			RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');

			$scope.currentFlag = currFlag;
		});
	};

	EditRequestServices.addModalInstance(initial_request);

	$scope.select = function (tab) {
		angular.forEach($scope.tabs, function (tab) {
			tab.selected = false;
		});
		tab.selected = true;
		$scope.template = tab;
		$scope.isInventory = tab.name == 'Inventory';
		$scope.requestCustomEventWatcher.editRequestLoaded = false;

		if ($scope.checkInventoryTab) {
			$scope.checkInventoryTab();
		}
	};

	if ($scope.request.service_type.raw == 5 && $scope.request.status.raw == 10) {
		$scope.select($scope.tabs[6]);
	}

	$scope.switchCalc = function () {
		if ($scope.request.status.raw == 3 && !$scope.request.field_usecalculator.value) {
			SweetAlert.swal({
				title: 'Are you sure you want to continue?',
				text: 'The turning on calculator may lead to recalculating and auto-saving of some values in the closing tab.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#64C564',
				confirmButtonText: 'Confirm',
				closeOnConfirm: true
			}, continueSwithing);
		} else {
			continueSwithing();
		}
	};

	function continueSwithing(confirm = true) {
		if (confirm) {
			$scope.request.field_usecalculator.value = !$scope.request.field_usecalculator.value;
			requestObservableService.setChanges('useCalculatorChanged');
		}
	}

	function cancelModal() {
		$uibModal.open({
			template: require('../../modals/templates/close-request-modal.html'),
			controller: canelModalInstanceCtrl
		});
	}

	function canelModalInstanceCtrl($scope, $uibModalInstance) {
		$scope.Apply = function () {
			updateAndClose();
			$uibModalInstance.dismiss('cancel');
		};
		$scope.quit = function () {
			$uibModalInstance.dismiss('cancel');
			closeDialogWindow();
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};

	}

	$scope.cancel = function () {
		if (!CheckRequestService.isValidRequestZip($scope.request)) {
			SweetAlert.swal('Please enter the correct zip code');

			return false;
		}

		//check if there is ane change editRequest is not Empty
		if ($scope.canedit) {
			var changes = common.count($scope.editrequest);
			if (changes != 0 && !_.isEmpty($scope.message)) {
				cancelModal();
			} else {
				$uibModalInstance.dismiss('cancel');
				EditRequestServices.removeModalInstance($scope.request.nid);
				EditRequestServices.repositionModals();
				$rootScope.$broadcast('request.closed');

				if ($scope.canedit) {
					RequestServices.removeEditable($scope.request.nid);
				}

				$scope.closed = true;
			}
		} else {
			$uibModalInstance.dismiss('cancel');
			EditRequestServices.removeModalInstance($scope.request.nid);
			EditRequestServices.repositionModals();
			$rootScope.$broadcast('request.closed');

			$scope.closed = true;
		}
		requestObservableService.clearChanges();
	};

	$scope.$on('onRequestUserDelete', onRequestUserDelete);

	function onRequestUserDelete() {
		$uibModalInstance.dismiss('cancel');
		EditRequestServices.removeModalInstance($scope.request.nid);
		EditRequestServices.repositionModals();
		$rootScope.$broadcast('request.closed');
		$rootScope.$broadcast('request.addNewRequest');
	}

	function closeDialogWindow() {
		$uibModalInstance.dismiss('cancel');
		EditRequestServices.removeModalInstance($scope.request.nid);
		EditRequestServices.repositionModals();
		$rootScope.$broadcast('request.closed');

		if ($scope.canedit) {
			RequestServices.removeEditable($scope.request.nid);
		}

		$scope.closed = true;
	}

	function updateAndClose() {
		var status = $scope.request.status.raw == 1 || $scope.request.status.raw == 2 || $scope.request.status.raw == 3;
		var today = new Date();
		// var flatRate = $scope.request.service_type.raw == 5 && $scope.request.date.value != today;

		if ($scope.request.trucks.raw.length == 0 && status) {
			SweetAlert.swal('Please Choose Truck!');

			return false;
		}

		if ($scope.request.trucks.raw.length
			&& status
			&& $scope.request.field_usecalculator.value) {
			//check if we have truck
			var trucks = getTrucksCount();
			var total_cf = {
				weight: 0
			};

			$scope.total_weight = {
				weight: 0
			};

			if ($scope.request.inventory.inventory_list == null
				|| $scope.request.field_useweighttype.value == 1) {

				$scope.total_weight = CalculatorServices.getRequestCubicFeet($scope.request);
			}

			if ($scope.request.field_useweighttype.value == 2) {
				$scope.total_weight.weight = $scope.request.inventory_weight.cfs;
			}
			if ($scope.total_weight.weight) {
				total_cf.weight = $scope.total_weight.weight;
			}
			$scope.calcResults = CalculatorServices.calculateTime($scope.request, total_cf);
			var calcTrucks = $scope.calcResults.trucks;

			if (trucks < calcTrucks) {
				//show errow based on calculator you can not choose more trucks
				SweetAlert.swal('You need more trucks for this job!');
				return false;
			}
		}

		if (!CheckRequestService.isValidRequestAddress($scope.request)) {
			SweetAlert.swal('Please Enter Address!');

			return false;
		}

		if (!CheckRequestService.checkHomeEstimate($scope.request)) {
			return false;
		}

		if (!_.isUndefined($scope.editrequest.field_approve)
			&& $scope.editrequest.field_approve == 3) {

			EditRequestServices.saveBookedStatistics($scope.request);
		}

		if (!_.isUndefined($scope.editrequest.notes)) {
			$rootScope.$broadcast('notes.save');
			delete $scope.editrequest.notes;
		}

		var editRequest = angular.copy($scope.editrequest);

		if (!_.isUndefined(editRequest.field_minimum_move_time) && !_.isNumber(editRequest.field_minimum_move_time)) {
			editRequest.field_minimum_move_time = _.round(common.convertStingToIntTime(editRequest.field_minimum_move_time), 2);
		}

		if (!_.isUndefined(editRequest.field_maximum_move_time) && !_.isNumber(editRequest.field_maximum_move_time)) {
			editRequest.field_maximum_move_time = _.round(common.convertStingToIntTime(editRequest.field_maximum_move_time), 2);
		}

		if (!_.isUndefined(editRequest.field_travel_time) && !_.isNumber(editRequest.field_travel_time)) {
			editRequest.field_travel_time = _.round(common.convertStingToIntTime(editRequest.field_travel_time), 2);
		}

		if (!_.isUndefined(editRequest.moversCount)) {
			delete editRequest.moversCount;
		}

		if (!_.isUndefined(editRequest.dontShowDelirery)) {
			delete editRequest.dontShowDelirery;
		}

		//save then close
		$scope.busy = true;
		if (!_.isEmpty(editRequest)) {
			var promise = RequestServices.updateRequest($scope.request.nid, editRequest);

			promise.then(function (data) {
				$scope.busy = true;

				ParserServices.saveEntrance($scope.request, editRequest);
				let messages = [];
				angular.forEach($scope.message, function (value) {
					messages.push(value);
				});
				EditRequestServices.sendLogsAfterSaveChanges(messages, $scope.request);
				// Update Original Request
				if (editRequest.field_approve) {
					var serviceType = editRequest.field_move_service_type || $scope.request.service_type.raw;
					var status = editRequest.field_approve;
					var emailParameters = {
						status: status,
						serviceType: serviceType
					};

					SendEmailsService
						.prepareForEditEmails(emailParameters)
						.then(function (data) {
							var emails = data.emails;

							if (!_.isEmpty(emails)) {
								SendEmailsService
									.sendEmails($scope.request.nid, emails)
									.then(function () {
										toastr.success('Email was sent', 'Success');
									}, function () {
										toastr.success('Email wasn\'t sent', 'Error');
									});
							}
						});
				}

				$rootScope.$broadcast('request.updated', $scope.request);
				$scope.fields = [];
				logger.success('Request was updated', data, 'Success');
				EditRequestServices.removeModalInstance($scope.request.nid);
				EditRequestServices.repositionModals();
				$uibModalInstance.dismiss('cancel');
				$rootScope.$broadcast('request.closed');

				if ($scope.canedit) {
					RequestServices.removeEditable($scope.request.nid);
				}

				toastr.success('Success', 'Request was updated.');
			}, function (reason) {
				let errorLogs = [];
				let error = '<b>Has some errors:</b>';
				if (_.get(reason, 'validation_errors')) {
					_.each(reason.validation_errors, function (validError) {
						let msg = {
							text: validError
						};

						error += `<br/>${validError}`;
						errorLogs.push(msg);
					});
				}

				SweetAlert.swal({
					title: 'Request was not saved.',
					text: error,
					type: 'error',
					html: true
				});
				RequestServices.sendLogs(errorLogs, 'Request was not updated', $scope.request.nid, 'MOVEREQUEST');
				logger.error(reason, reason, 'Error');
				EditRequestServices.removeModalInstance($scope.request.nid);
				EditRequestServices.repositionModals();
				$scope.closed = true;
			});
		} else {
			$rootScope.$broadcast('request.updated', $scope.request);
			$scope.fields = [];
			EditRequestServices.removeModalInstance($scope.request.nid);
			EditRequestServices.repositionModals();
			$uibModalInstance.dismiss('cancel');
			$rootScope.$broadcast('request.closed');
			toastr.success('Success', 'Request was updated.');
		}
	}

	function getTrucksCount() {
		return $scope.request.trucks.raw.length;
	}

	if ($scope.request.inventory.inventory_list != null) {
		$scope.request.inventory_list_old = $scope.request.inventory.inventory_list;
	} else {
		$scope.request.inventory_list_old = [];
	}

	$scope.request.inventory_weight = InventoriesServices.calculateTotals($scope.request.inventory.inventory_list);

	$scope.$on('sales.closing.tab', (event, type) => {
		$scope.disableTabs = type === 'closing';
	});

	$scope.$on('inventory.updated', function (event, inventory_list) {
		$scope.select($scope.tabs[0], true);
		initial_request.inventory.inventory_list = inventory_list;
		if (inventory_list.length) {
			inventory_list.forEach(item => {
				let title = _.get(item, 'title', '');
				if (title.toLowerCase().indexOf('piano') > -1) {
					$scope.isHaveRedPiano = true;
				}
			});
		}

	});

	$scope.$watch(() => {
		return $scope.request.inventory_weight.cfs;
	}, () => {
		$scope.isHaveRedPiano = false;
		if ($scope.request.inventory && $scope.request.inventory.inventory_list && _.isFunction($scope.request.inventory.inventory_list.forEach)) {
			$scope.request.inventory.inventory_list.forEach(item => {
				let title = _.get(item, 'title', '');
				if (title.toLowerCase().indexOf('piano') > -1) {
					$scope.isHaveRedPiano = true;
				}
			});
		}
	});

	$scope.$on('field_move_images.updated', function (event, field_move_images) {
		$scope.select($scope.tabs[0], true);
		$scope.request.field_move_images[0].url = url;
		if (field_move_images.length) {
			field_move_images.forEach(item => {
				if (item != 0) {
					$scope.isHavePhoto = true;
				}
			});
		}

	});

	// TODO Need refactor this watcher
	$scope.$watch(() => {
		$scope.isHavePhoto = false;
		if ($scope.request.field_move_images[0] == null) {
			return false;
		} else {
			if ($scope.request.field_move_images && $scope.request.field_move_images[0].url) {
				$scope.request.field_move_images.forEach(item => {
					if (item != 0) {
						$scope.isHavePhoto = true;
					}
				});
			}
		}
	});

	$scope.$on('request.editRequest', function (event, request, edit) {
		if ($scope.request.nid == request.nid) {
			_.forEach(edit, function (item, field) {
				let newRequestItem = _.findKey(request, {field: field});
				if (request[newRequestItem] && request[newRequestItem].old) {
					if (field.search('time') >= 0) {
						request[newRequestItem].old = common.decToTime(item);
					} else {
						request[newRequestItem].old = item;
					}
				}
				if ($scope.editrequest[field]) {
					delete $scope.editrequest[field];
				}
				if ($scope.message[field]) {
					delete $scope.message[field];
				}
			});
		}
	});

	$scope.$on('request.payment_update', function (event, data) {
		if ($scope.request.nid == data.nid) {
			$scope.request.extraServices = data.charges;
		}
	});

	$scope.$on('manager.assigned', function (data, manager, nid) {
		$scope.request.manager = manager;
		initial_request.manager = manager;
	});

	$scope.$on('request.payment_receipts', function (event, receipts) {
		$scope.request.receiepts = receipts;
	});

	$scope.$on('request.packing_update', function (event, add_packing_data) {
		if ($scope.request.request_data && _.isArray(add_packing_data)) {
			if (!_.isEmpty($scope.request.request_data.value)) {
				var data = angular.fromJson($scope.request.request_data.value);

				data.packings = add_packing_data;
			}
		}

		if (_.isString($scope.request.request_data.value)
			&& !_.isEmpty($scope.request.request_data.value)) {

			$scope.request.request_data.value = JSON.parse($scope.request.request_data.value);
		}
	});

	$scope.$on('client_info.updated', function (event, info) {
		if ($scope.request.uid.uid != info.uid) return;
		$scope.request.email = info.mail;
		$scope.request.name = `${info.field_user_first_name} ${info.field_user_last_name}`;
		$scope.request.phone = info.field_primary_phone;
		isDraft(info);
	});

	function isDraft(info) {
		const DRAFT_NAME = 'Draft User';
		let currentName = `${info.field_user_first_name} ${info.field_user_last_name}`;
		$scope.isDraftRequest = _.isEqual(currentName, DRAFT_NAME)
			|| !!info.mail.match(/(\d+)@draft.www/);
	}

	$scope.$on('request.updated', function (event, request) {
		initial_request = EditRequestServices.updateRequestLive(initial_request, $scope.editrequest);
		initial_request.field_extra_dropoff = request.field_extra_dropoff;
		$scope.request.inventory_weight = request.inventory_weight;
		initial_request.field_extra_pickup = request.field_extra_pickup;
		initial_request.field_moving_from = request.field_moving_from;
		initial_request.field_moving_to = request.field_moving_to;
		//Cherry
		if (angular.isDefined($scope.editrequest.field_approve)) {
			if ($scope.editrequest.field_approve == 2) {
				common.showAnimation(request.nid);
			}
		}

		$scope.editrequest = {};
		$scope.message = {};
		$scope.select($scope.tabs[0]);
	});

	$scope.isDetailsChanged = () => {
		var result = CheckRequestService.isMoveDetailsChanged($scope.request.inventory.move_details, $scope.request.service_type.raw);

		return result;
	};

	$scope.isOptionsChanged = function () {
		if (angular.isUndefined($scope.request.inventory.move_details)
			|| _.isNull($scope.request.inventory.move_details)) {
			return false;
		}

		var details = _.clone($scope.request.inventory.move_details);
		var result = details.hasOwnProperty('options');

		if (result) {
			result = details.options.length != 0;
		}

		return result;
	};

	function getRequestMessages() {
		MessagesServices.getMessagesThread($scope.request.nid)
			.then(function (data) {
				let newMessages = 0;

				angular.forEach(data, function (m, cid) {
					if (!m.read && !m.admin) {
						newMessages++;
					}
				});

				$scope.request.messages = newMessages;
				$scope.request.messagesList = Object.values(data)
					.sort((firstMessage, lastMessage) => lastMessage.cid - firstMessage.cid);
			});
	}

	$scope.$on('$destroy', () => {
		$interval.cancel(loadMessageInterval);
	});

	function updateMessage(field) {
		//Array for updating request
		switch (field.field) {
		case 'field_travel_time':
		case 'field_double_travel_time':
		case 'field_move_service_type':
		case 'field_minimum_move_time':
		case 'field_maximum_move_time':
			$scope.editrequest[field.field] = field.raw;

			break;

		case 'field_home_estimate_date':
		case 'field_home_estimate_date_created':
			var formatedDate = moment(field.value).format('YYYY-MM-DD');
			$scope.editrequest[field.field] = formatedDate;

			break;

		case 'field_home_estimate_time_duratio':
			$scope.editrequest[field.field] = _.round(common.convertStingToIntTime(field.value), 2);
			break;

		case 'field_list_truck':
			$scope.editrequest[field.field] = field.raw;
			$scope.message[field.field] = getTruckMessage(field);
			break;

		default :
			$scope.editrequest[field.field] = field.value;
		}

		//Message which field will be updated
		if (!_.isEqual(field.old, field.value) && !_.isEqual(field.field, 'field_list_truck')) {
			switch (field.field) {
			case 'field_move_service_type':
				$scope.message[field.field] = {
					label: field.label,
					oldValue: $scope.request.service_type.value,
					newValue: _.find($scope.services, (service) => service.id == $scope.request.service_type.raw).title
				};
				break;
			case 'field_minimum_move_time':
			case 'field_maximum_move_time':
				$scope.message[field.field] = {
					label: field.label,
					oldValue: field.old,
					newValue: !_.isString(field.value) ? common.decToTime(field.value) : field.value,
					lastChangeTime: moment().unix(),
				};
				break;
			case 'field_travel_time':
			case 'field_double_travel_time':
				$scope.message[field.field] = {
					label: field.label,
					oldValue: field.old,
					newValue: field.value,
					lastChangeTime: moment().unix()
				};

				break;
			case 'field_price_per_hour':
				if ($scope.request.service_type.raw == 7) {
					return;
				}
				$scope.message[field.field] = {
					label: field.label,
					oldValue: `$${field.old}`,
					newValue: `$${field.value}`
				};

				break;

			case 'field_home_estimate_status':
				$scope.message[field.field] = {
					label: field.label,
					oldValue: HOME_ESTIMATE_STATUSES[field.old],
					newValue: HOME_ESTIMATE_STATUSES[field.value],
				};

				break;

			case 'field_home_estimator':
				let estimatorOld = $scope.userForHomeEstimate[field.old];
				let estimator = $scope.userForHomeEstimate[field.value];
				let estimatorNameOld = estimatorOld ? estimatorOld.first_name + ' ' + estimatorOld.last_name : 'None';
				let estimatorName = estimator ? estimator.first_name + ' ' + estimator.last_name : 'None';

				$scope.message[field.field] = {
					label: field.label,
					oldValue: estimatorNameOld,
					newValue: estimatorName,
				};

				break;

			case 'field_home_estimate_assignedn':
				let currentUser = $rootScope.currentUser.userId;
				$scope.message[field.field] = {
					label: field.label,
					oldValue: '',
					newValue: `${currentUser.field_user_first_name} ${currentUser.field_user_last_name}`,
				};

				break;
			case 'field_long_distance_rate':
				$scope.message[field.field] = {
					label: 'Long Distance Rate',
					oldValue: '$' + _.round(field.old, 2),
					newValue: '$' + _.round(field.value, 2)
				};

				break;
			case 'field_reservation_price':
				$scope.message[field.field] = {
					label: 'Reservation Price',
					oldValue: '$' + _.round(field.old, 2),
					newValue: '$' + _.round(field.value, 2)
				};

				break;
			default:
				if (_.isUndefined($scope.message[field.field])) {
					$scope.message[field.field] = {
						label: field.label,
						oldValue: field.old,
						newValue: field.value,
					};
				} else {
					$scope.message[field.field].label = field.label;
					$scope.message[field.field].oldValue = field.old;
					$scope.message[field.field].newValue = field.value;
				}
			}
		}
	}

	function getTruckMessage(field) {
		let trucks = $scope.fieldData.taxonomy.trucks;
		let truckText = '';
		let oldTruckText = '';
		let trucksQuantity = field.raw.length - 1;
		_.forEach(field.raw, (tid, index) => {
			truckText += _.isEqual(trucksQuantity, index) ? trucks[tid] : `${trucks[tid]}, `;
		});
		_.forEach(field.old, function (tid, i) {
			oldTruckText = oldTruckText + trucks[tid];
			oldTruckText += field.old.length - 1 != i ? ', ' : '';
		});

		return {
			label: 'Truck',
			oldValue: oldTruckText,
			newValue: truckText,
		};
	}


	$scope.$on('inventoryUpdate', () => {
		$scope.requestCustomEventWatcher.inventory_update = true;
	});

	$scope.$on('editRequestLoaded', () => {
		$scope.requestCustomEventWatcher.editRequestLoaded = true;
	});

	$scope.$on('detailsUpdated', () => {
		$scope.requestCustomEventWatcher.detailsUpdated = true;
	});

	$scope.$on('weightTypeChanged', () => {
		$scope.requestCustomEventWatcher.weightTypeChanged = true;
	});

	$scope.$on('updateWeightType', updateWeightType);

	function updateWeightType(_, updatedData) {
		requestObservableService.setChanges('useWeightType');
		RequestServices.updateRequest(updatedData.id, {
			'field_useweighttype': updatedData.type
		});
	}

	$scope.$watch('requestCustomEventWatcher.editRequestLoaded', current => {
		if (current && $scope.requestCustomEventWatcher.inventory_update) {
			$scope.$broadcast('inventory_update');
			$scope.requestCustomEventWatcher.inventory_update = false;
		}

		if (current && $scope.requestCustomEventWatcher.detailsUpdated) {
			$scope.$broadcast('request.details_updated');
			$scope.requestCustomEventWatcher.detailsUpdated = false;
		}

		if (current && $scope.requestCustomEventWatcher.weightTypeChanged) {
			$scope.$broadcast('weightTypeChanged');
			$scope.requestCustomEventWatcher.weightTypeChanged = false;
		}
	});

	$scope.$on('storage.request.created', function (ev, data) {
		$scope.request.request_all_data.storage_request_id = data;
		RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
	});

	$scope.$on('setVarInBaseScope', (event, params) => {
		$scope[params.name] = params.value;
	});
}
