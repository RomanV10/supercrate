(function () {
	'use strict';

	angular
		.module('move.requests')
		.directive('ccFlatRateRangePicker', ccFlatRateRangePicker);

	function ccFlatRateRangePicker() {
		return {
			restrict: 'A',
			template: ' <input><div></div>',
			scope: {
				'request': '=',
				'message': '=',
				'editrequest': '=',
				ngModel: '=ngModel',
			},
			link: function (scope, element, attrs, ngModel) {
				var rangepicker = element.find('div');
				var rangeinput = element.find('input');
				var cur = -1, prv = -1;
				var pickupDates = '';
				var dateFormat = 'd M, yy';
				var momentFormat = 'DD-MM-YYYY';

				var dateFrom = scope.request.delivery_date_from.value
					? moment(scope.request.delivery_date_from.value, 'DD-MM-YYYY').toDate().getTime() : '';
				var dateTo = scope.request.delivery_date_to.value
					? moment(scope.request.delivery_date_to.value, 'DD-MM-YYYY').toDate().getTime() : '';

				if (scope.request.delivery_date_to.raw == false) {
					dateTo = scope.request.delivery_date_from.value
						? moment(scope.request.delivery_date_from.value, 'DD-MM-YYYY').toDate().getTime() : '';
				}
				var previousDateFrom = scope.request.delivery_date_from.value ? moment(
					scope.request.delivery_date_from.value, 'DD-MM-YYYY').format('YYYY-MM-DD') : '';
				var previousDateTo = scope.request.delivery_date_to.value
					? moment(scope.request.delivery_date_to.value, 'DD-MM-YYYY').format('YYYY-MM-DD') : '';
				if (dateFrom != dateTo) {
					initPossibleDates(dateFrom + ',' + dateTo);
				} else {
					initPossibleDates(dateFrom);
				}

				scope.$watch('ngModel', function (val) {
					initPossibleDates(scope.ngModel);
				});

				scope.$on('request.updated', function (event, request) {
					previousDateFrom = request.delivery_date_from.value
						? moment(request.delivery_date_from.value, 'DD-MM-YYYY').format('YYYY-MM-DD') : '';
					previousDateTo = request.delivery_date_to.value
						? moment(request.delivery_date_to.value, 'DD-MM-YYYY').format('YYYY-MM-DD') : '';
				});

				rangepicker
					.datepicker({
						dateFormat: dateFormat,
						numberOfMonths: 2,
						changeMonth: false,
						changeYear: false,
						showButtonPanel: true,
						minDate: moment(scope.request.date.value).unix(),
						beforeShowDay: function (date) {
							if (date.getTime() == Math.min(prv, cur) || date.getTime() == Math.max(prv, cur)) {
								return [
									true,
									( (date.getTime() == Math.min(prv, cur) || date.getTime() == Math.max(prv, cur))
										? 'date-range-selected range'
										: '')
								];
							} else {
								return [
									true,
									( (date.getTime() > Math.min(prv, cur) && date.getTime() < Math.max(prv, cur))
										? 'date-range-selected'
										: '')
								];
							}

						},

						onSelect: function (dateText, inst) {
							var d1, d2;
							var ddd1, ddd2;

							if (prv != -1 && cur != -1 && prv != cur) {
								cur = -1;
								prv = -1;
							}


							cur = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();

							if (angular.isUndefined(scope.$parent.invoice.delivery_date_from)) {
								scope.$parent.invoice.delivery_date_from = {};
							}

							if (angular.isUndefined(scope.$parent.invoice.delivery_date_to)) {
								scope.$parent.invoice.delivery_date_to = {};
							}

							if (prv == -1 || prv == cur) {
								prv = cur;
								pickupDates = cur;
								rangeinput.val(dateText);
								scope.request.delivery_date_from.value = moment(Math.min(prv, cur))
									.format(momentFormat);
								scope.request.delivery_date_from.raw = moment(Math.min(prv, cur)).unix();
								_.set(scope.$parent, 'invoice.delivery_date_from.value',
									scope.request.delivery_date_from.value);
								_.set(scope.$parent, 'invoice.delivery_date_from.raw',
									scope.request.delivery_date_from.raw);

								scope.request.delivery_date_to.value = moment(Math.max(prv, cur)).format(momentFormat);
								scope.request.delivery_date_to.raw = moment(Math.max(prv, cur)).unix();
								_.set(scope.$parent, 'invoice.delivery_date_to.value',
									scope.request.delivery_date_to.value);
								_.set(scope.$parent, 'invoice.delivery_date_to.raw',
									scope.request.delivery_date_to.raw);

								checkDeliveryDates();

								updateModel(pickupDates);
								//  rangepicker.datepicker( "option", "minDate",d1);

								if (attrs.delivery) {
									updateMessage(pickupDates, true, previousDateFrom);
									updateMessage(pickupDates, false, previousDateTo);
								}
							} else {
								d1 = $.datepicker.formatDate(dateFormat, new Date(Math.min(prv, cur)), {});
								//  element.datepicker( "option", "minDate",d1);
								d2 = $.datepicker.formatDate(dateFormat, new Date(Math.max(prv, cur)), {});
								scope.request.delivery_date_from.value = moment(Math.min(prv, cur))
									.format(momentFormat);
								scope.request.delivery_date_from.raw = moment(Math.min(prv, cur)).unix();
								_.set(scope.$parent, 'invoice.delivery_date_from.value', scope.request.delivery_date_from.value);
								_.set(scope.$parent, 'invoice.delivery_date_from.raw', scope.request.delivery_date_from.raw);

								scope.request.delivery_date_to.value = moment(Math.max(prv, cur)).format(momentFormat);
								scope.request.delivery_date_to.raw = moment(Math.max(prv, cur)).unix();
								_.set(scope.$parent, 'invoice.delivery_date_to.value', scope.request.delivery_date_to.value);
								_.set(scope.$parent, 'invoice.delivery_date_to.raw', scope.request.delivery_date_to.raw);

								checkDeliveryDates();

								rangeinput.val(d1 + ' - ' + d2);
								ddd1 = cur;
								ddd2 = prv;
								pickupDates = ddd1 + ',' + ddd2;
								updateModel(pickupDates);

								if (attrs.delivery) {
									updateMessage(d1, true, previousDateFrom);
									updateMessage(d2, false, previousDateTo);
								}
							}
						},

						onChangeMonthYear: function (year, month, inst) {
							//prv = cur = -1;
						},

						onAfterUpdate: function (inst) {
							$('<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" data-handler="hide" data-event="click">Done</button>')
								.appendTo(rangepicker.find('.ui-datepicker-buttonpane'))
								.on('click', function () { rangepicker.hide(); });
						}
					})
					.position({
						my: 'left top',
						at: 'left bottom',
						of: rangepicker
					})
					.hide();

				function checkDeliveryDates() {
					if (scope.request.ddate.value != false) {
						let momentCurrentDDate = moment(scope.request.ddate.value);
						let dateFrom = moment(scope.request.delivery_date_from.value, 'DD-MM-YYYY');
						let dateTo = moment(scope.request.delivery_date_to.value, 'DD-MM-YYYY');
						let isCorrectDate = momentCurrentDDate.isBetween(dateFrom, dateTo)
							|| momentCurrentDDate.isSame(dateFrom)
							|| momentCurrentDDate.isSame(dateTo);

						if (!isCorrectDate) {
							toastr.warning('Delivery Dates must be the same or contain Delivery Date', 'Warning!');
						}
					}
				}

				rangeinput.on('focus', function (e) {
					var v = this.value,
						d;

					try {
						if (v.indexOf(' - ') > -1) {
							d = v.split(' - ');

							prv = $.datepicker.parseDate(dateFormat, d[0]).getTime();
							cur = $.datepicker.parseDate(dateFormat, d[1]).getTime();

						} else if (v.length > 0) {
							prv = cur = $.datepicker.parseDate(dateFormat, v).getTime();
						}
					} catch (e) {
						cur = prv = -1;
					}

					if (cur > -1) {
						rangepicker.datepicker('setDate', new Date(cur));
					}

					rangepicker.datepicker('refresh').show();
				});

				element.on('mouseout', '.ui-datepicker-calendar td', function (event) {
					$('.ui-datepicker-calendar td').removeClass('highlight');
				})
					.on('mouseover', '.ui-datepicker-calendar td', function (event) {

						var year = $(this).attr('data-year');
						var month = $(this).attr('data-month');
						var day = $(this).children().text();
						var tocurtime = (new Date(year, month, day)).getTime();
						// compare with prv date

						if (tocurtime > cur && cur != -1 && prv == cur) {
							// higlight if true
							$('.ui-datepicker-calendar td').each(function () {

								var year = $(this).attr('data-year');
								var month = $(this).attr('data-month');
								var day = $(this).children().text();
								var curtime = (new Date(year, month, day)).getTime();
								if (curtime > cur && curtime < tocurtime && cur != -1) {
									$(this).addClass('highlight');
								}

							});

						}
						if (tocurtime < cur && cur != -1 && prv == cur) {
							// higlight if true
							$('.ui-datepicker-calendar td').each(function () {
								var year = $(this).attr('data-year');
								var month = $(this).attr('data-month');
								var day = $(this).children().text();
								var curtime = (new Date(year, month, day)).getTime();
								if (curtime < cur && curtime > tocurtime && cur != -1) {
									$(this).addClass('highlight');
								}

							});
						}
					});

				$(document).mouseup(function (e) {
					var container = rangepicker;
					var target = e.target;

					if (!container.is(e.target) // if the target of the click isn't the container...
						&& container.has(e.target).length === 0 && e.target['localName'] != 'input') // ... nor a descendant
						// of the container
					{
						container.hide();
					}
				});

				function updateModel(dates) {
					if (angular.isDefined(scope.ngModel)) {
						scope.ngModel = dates;
					}
				}

				function updateMessage(date, from, previous) {

					if (angular.isDefined(scope.message)) {
						var date = moment(date).format('YYYY-MM-DD');

						if (from) {
							scope.field = scope.request.delivery_date_from;
							scope.lastVal = previous;
						} else {
							scope.field = scope.request.delivery_date_to;
							scope.lastVal = previous;
						}

						if (scope.field.field == 'field_delivery_date_from') {
							scope.editrequest[scope.field.field] = {
								date: date,
								time: '00:00:00'
							};
							scope.$parent.editRequestinvoice[scope.field.field] = {
								date: date,
								time: '00:00:00'
							};
						} else {
							scope.editrequest[scope.field.field] = {
								date: date,
								time: '00:00:00'
							};
							scope.$parent.editRequestinvoice[scope.field.field] = {
								date: date,
								time: '00:00:00'
							};
						}

						if (!_.isEqual(scope.lastVal, date)) {
							scope.message[scope.field.field] = {
								label: scope.field.label,
								oldValue: scope.lastVal,
								newValue: date,
							};
						}

						if (from) {
							scope.field = scope.$parent.invoice.delivery_date_from;
							scope.lastVal = scope.$parent.invoice.delivery_date_from.value;
						} else {
							scope.field = scope.$parent.invoice.delivery_date_to;
							scope.lastVal = scope.$parent.invoice.delivery_date_to.value;
						}

						scope.$parent.messageInvoice[scope.field.field] = {
							label: scope.field.label,
							// oldValue : scope.lastVal,
							newValue: date
						};

					}

				}

				function initPossibleDates(dates) {

					if (dates) {
						var deli = [];
						var isStringDate = false;

						if (!angular.isNumber(dates)) {
							deli = dates.split(',');
							var firstDate = parseInt(deli[0]);

							if (isNaN(firstDate) || !angular.isNumber(parseInt(deli[1]))) {
								rangeinput.val(dates);

								isStringDate = true;
							}
						} else {
							deli[0] = dates;
							deli[1] = dates;
						}

						if (!isStringDate) {
							var c1 = $.datepicker.formatDate(dateFormat, new Date(Math.min(deli[0], deli[1])), {});
							var c2 = $.datepicker.formatDate(dateFormat, new Date(Math.max(deli[0], deli[1])), {});

							if (c1 != c2) {
								rangeinput.val(c1 + ' - ' + c2);
							} else {
								rangeinput.val(c1);
							}
						}
					}
				}
			}
		};

	};
})();
