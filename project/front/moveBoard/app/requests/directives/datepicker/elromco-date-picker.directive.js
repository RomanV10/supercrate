(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elromcoDatePicker', elromcoDatePicker);

    elromcoDatePicker.$inject = ['datacontext'];

    function elromcoDatePicker(datacontext) {
        var calendartype = datacontext.getFieldData().calendartype;

        return {
            restrict: 'A',
            scope: {
                currentDate: '=',
                changeCurrentDate: '=onChangeCurrentDate'
            },
            link: linker
        };

        function linker(scope, element, attrs, ngModel) {
            var calendar = datacontext.getFieldData().calendar;

            element.datepicker({
                disableTouchKeyboard: true,
                dateFormat: 'DD,MM d, yy',
                numberOfMonths: 2,
                changeMonth: false,
                changeYear: false,
                showOn: 'button',
                buttonImageOnly: true,
                setDate: attrs.date,
                showButtonPanel: true,
                onSelect: function (date, obj) {
                    scope.currentDate = moment(date).format('dddd, MMMM DD, YYYY');
                    scope.$apply();
                },
                beforeShowDay: function (date) {
                    return checkDate(date, calendar);
                }
            });
        }

        function checkDate(date, calendar) {
            var day = moment(date).format('DD');
            var month = moment(date).format('MM');
            var year = date.getFullYear();
            var date = year + '-' + month + '-' + day;

            if (calendar[year][date] == 'block_date') {
                return [false, 'not-available', 'This date is NOT available'];
            } else {
                return [true, calendartype[calendar[year][date]], calendartype[calendar[year][date]]];
            }
        }
    }

})();