angular
	.module('move.requests')
	.directive('ccDaPicker', ccDaPicker)
	.directive('ccRatePicker', ccRatePicker);

/* @ngInject */
function ccDaPicker() {
	return {
		restrict: 'A',
		scope: {
			'ngModel': '=',
			'createdDate': '=?'
		},
		link: function (scope, element, attrs) {
			element.datepicker({
				dateFormat: 'MM d, yy',
				numberOfMonths: attrs.numberOfMonths ? parseInt(attrs.numberOfMonths) : 2,
				changeMonth: false,
				changeYear: !!attrs.changeYear || false,
				yearRange: attrs.minDate ? new Date(attrs.minDate).getFullYear() + ':2030' : '2000:2030',
				setDate: new Date(),
				minDate: attrs.minDate ? new Date(attrs.minDate) : new Date(),
				showButtonPanel: true,
				onSelect: function (date) {
					scope.date = date;

					let dprv = (new Date(date)).getTime();
					let curdate = $.datepicker.formatDate('MM d, yy', new Date(dprv), {});

					scope.ngModel = curdate;

					if (scope.createdDate) {
						scope.createdDate = moment(curdate, 'MMMM D, YYYY').unix();
					}
				}
			});
		}
	};
}

/* @ngInject */
function ccRatePicker($rootScope, datacontext) {
	return {
		restrict: 'A',
		scope: {
			'ngModel': '='
		},
		link: function (scope, element) {
			let calendar = datacontext.getFieldData().calendar;
			let calendartype = datacontext.getFieldData().calendartype;

			element.datepicker({
				dateFormat: 'M d, yy',
				numberOfMonths: 2,
				changeMonth: false,
				changeYear: false,
				setDate: new Date(),
				showButtonPanel: true,
				onSelect: function (date, obj) {
					let dprv = (new Date(date)).getTime();
					let curdate = $.datepicker.formatDate('M d, yy', new Date(dprv), {});
					scope.ngModel = curdate;
					$rootScope.$broadcast('isDate', curdate);
				},
				beforeShowDay: function (date) {
					return checkDate(date, calendar);
				}
			});

			function checkDate(date, calendar) {
				let day = moment(date).format('DD');
				let month = moment(date).format('MM');
				let year = date.getFullYear();
				date = year + '-' + month + '-' + day;

				if (calendar[year][date] == 'block_date') {
					return [false, 'not-available', 'This date is NOT available'];
				} else {
					return [true, calendartype[calendar[year][date]], calendartype[calendar[year][date]]];
				}
			}
		}
	};
}