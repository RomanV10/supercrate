(function () {
    'use strict';

    angular
        .module('move.requests')
        .directive('elromcoFieldDatePicker', elromcoFieldDatePicker);

    elromcoFieldDatePicker.$inject = ['datacontext'];

    function elromcoFieldDatePicker(datacontext) {
        var calendartype = datacontext.getFieldData().calendartype;
        const DATE_FORMAT = 'MMMM DD, YYYY';
        const CURRENT_DATE_FORMAT = 'dddd, MMMM DD, YYYY';

        return {
            restrict: 'AE',
            scope: {
                field: '=',
                updateMessage: '=onUpdateMessage',
                removeMessage: '=onRemoveMessage',
                curDate: '='
            },
            template: require('./elromco-field-datepicker.template.html'),
            link: linker
        };

        function linker(scope, element, attrs, ngModel) {
            var calendar = datacontext.getFieldData().calendar;

            var currentDate = moment().format(DATE_FORMAT);

            if (scope.field.value) {
                currentDate = moment(scope.field.value).format(DATE_FORMAT);
                scope.field.value = currentDate;
            }

            var input = element.find('.date-input');

            input.datepicker({
                disableTouchKeyboard: true,
                dateFormat: 'MM d, yy',
                numberOfMonths: 2,
                changeMonth: false,
                changeYear: false,
                showOn: 'button',
                buttonImageOnly: true,
                setDate: currentDate,
                showButtonPanel: true,
                onSelect: function (date, obj) {
                    var value = moment(date).format(DATE_FORMAT);
                    scope.field.value = value;
                    var oldValue = moment(scope.field.old).format(DATE_FORMAT);

                    if (scope.updateMessage && value != oldValue) {
	                    updateMessage(scope.field);
                    }

                    if (scope.removeMessage && value == oldValue) {
                        scope.removeMessage(scope.field);
                    }

                    if (scope.curDate) {
                        scope.curDate = moment(date).format(CURRENT_DATE_FORMAT);
                    }

                    scope.$apply();
                },
                beforeShowDay: function (date) {
                    return checkDate(date, calendar);
                }
            });

            scope.opentDatePicker = opentDatePicker;

            function opentDatePicker() {
                element.find('.ui-datepicker-trigger').click();
            }

	        function updateMessage(field) {
		        if (!field) return;

		        scope.updateMessage({
			        field: field,
		        });
	        }
        }

        function checkDate(date, calendar) {
            var day = moment(date).format('DD');
            var month = moment(date).format('MM');
            var year = date.getFullYear();
            var date = year + '-' + month + '-' + day;

            if (calendar[year][date] == 'block_date') {
                return [false, 'not-available', 'This date is NOT available'];
            } else {
                return [true, calendartype[calendar[year][date]], calendartype[calendar[year][date]]];
            }
        }
    }

})();
