'use strict';

import './edit-request-page.styl'

angular.module('app')
	.directive('erEditRequestPage', erEditRequestPage);

function erEditRequestPage() {
	return {
		restrict: 'E',
		template: require('./edit-request-page.html'),
		scope: {
			initialRequest: '=',
			requests: '=',
			superResponse: '=',
			isDelivery: '=',
			modalInstance: '='
		},
		controller: 'EditRequestPageController',
		transclude: true
	};
}
