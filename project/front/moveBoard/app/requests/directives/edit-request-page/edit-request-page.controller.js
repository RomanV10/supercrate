'use strict';

import {
	REQUEST_STATUS_BORDER
} from '../../../inhome-estimator-portal/constants/request-status-border';

angular
	.module('app')
	.controller('EditRequestPageController', EditRequestPageController);

EditRequestPageController.$inject = ['$scope', 'RequestServices', '$rootScope', 'PermissionsServices', '$controller', '$interval'];

function EditRequestPageController($scope, RequestServices, $rootScope, PermissionsServices, $controller, $interval) {
	angular.extend(this, $controller('EditRequestPageBaseController', {$scope: $scope}));

	$scope.canedit = false;
	$scope.permisionedit = PermissionsServices.canEditRequest($scope.request);
	$scope.checkInventoryTab = checkInventoryTab;

	$scope.ReleaseEdit = function () {
		$scope.canedit = true;
		RequestServices.removeEditable($scope.request.nid);
	};

	function checkEditable() {
		var user_editable = RequestServices.checkEditable($scope.request.nid);
		var uid = _.get($rootScope, 'currentUser.userId.uid');

		if (user_editable.uid.uid == uid) {
			$scope.canedit = true;
		}

		$scope.isCurrentUserHaveEditPermission = user_editable.status;
		$scope.editUser = user_editable.uid.name;
	}

	checkEditable();

	$(window).on('resize', e => {
		checkInventoryTab();
	});

	function checkInventoryTab() {
		var elementModal = angular.element(".modalId_" + $scope.request.nid + " .modal-dialog");

		$interval.cancel($scope.inventoryInterval);

		$scope.inventoryInterval = $interval(() => {
			if (!_.isUndefined(elementModal.attr('style'))) {
				elementModal.attr('style', elementModal.attr('style').replace(/width:[^;]+/g, ''));
			}

			if ($scope.isInventory && document.body.clientWidth > 900) {
				elementModal.addClass("wideDialog");
			} else {
				elementModal.removeClass("wideDialog");
			}

			$interval.cancel($scope.inventoryInterval);
		}, 1000);
	}


	$scope.$watch('request.status.raw', () => {
		if (!$scope.isDesktop) {
			changeRequestBorder();
		}
	});

	function changeRequestBorder() {
		angular.element('.mobile-request-root-wrapper').css({
			border: `10px solid ${REQUEST_STATUS_BORDER[$scope.request.status.raw]}`,
		});
	}
}
