import moment from 'moment';

export function prepareRequest(request, packingDay) {
	const PACKING_DAY_SERVICE_TYPE = 8;
	const ONE_DAY_UNIX = 86400;
	let packingDate = {
		date: moment.unix(request.date.raw - ONE_DAY_UNIX).utc().startOf('day').format('YYYY-MM-DD'),
		time: '15:10:00'
	};
	
	var new_request = {};
	var user = request.uid || {};
	
	new_request.data = {
		status: 1,
		title: 'Move Request',
		uid: user.uid,
		
		field_approve : 1,
		field_move_service_type : packingDay ? PACKING_DAY_SERVICE_TYPE : request.service_type.raw, //Move & storage
		field_date : {date: moment.unix(request.date.raw).utc().startOf('day').format('YYYY-MM-DD'), time: '15:10:00'},
		field_size_of_move : request.move_size.raw,
		field_type_of_entrance_from : request.type_from.raw,
		field_type_of_entrance_to_ : request.type_to.raw,
		
		field_start_time: request.start_time.raw,
		field_price_per_hour: request.rate.value,
		field_movers_count: request.crew.value,
		
		field_distance: request.distance.value,
		field_travel_time: request.travel_time.raw,
		field_minimum_move_time: request.minimum_time.raw,
		field_maximum_move_time: request.maximum_time.raw,
		field_duration: request.duration.value,
		field_custom_commercial_item: request.field_custom_commercial_item,
		field_extra_furnished_rooms: request.rooms.raw, //?
		field_moving_to: {
			country: "US",
			administrative_area: request.field_moving_to.administrative_area,
			locality: request.field_moving_to.locality,
			postal_code: request.field_moving_to.postal_code,
			thoroughfare: request.field_moving_to.thoroughfare,
			premise: ""
		},
		field_apt_from: request.apt_from.value,
		field_apt_to: request.apt_to.value,
		field_moving_from: {
			country: "US",
			administrative_area: request.field_moving_from.administrative_area,
			locality: request.field_moving_from.locality,
			postal_code: request.field_moving_from.postal_code,
			thoroughfare: request.field_moving_from.thoroughfare,
			premise: ""
		},
		
		field_storage_price : request.storage_rate.value,
		field_reservation_price : request.reservation_rate.value,
		field_useweighttype: request.field_useweighttype.value,
		field_usecalculator: request.field_usecalculator.value ? "1" : "0",
		field_customweight: request.custom_weight.value || "0",
		field_double_travel_time: request.field_double_travel_time.raw,
		field_request_settings: request.field_request_settings,
	};
	
	new_request.data.field_first_name = user.field_user_first_name || "";
	new_request.data.field_last_name =  user.field_user_last_name || "";
	new_request.data.field_e_mail = user.mail || "";
	new_request.data.field_phone = user.field_primary_phone || "";
	new_request.data.field_additional_phone = user.field_user_additional_phone || "";
	
	new_request.account = {
		name: angular.copy(user.name),
		mail: angular.copy(user.mail)
	};
	
	new_request.all_data = angular.copy(request.request_all_data);
	
	new_request.data.inventory = {};
	
	if (request.inventory && request.inventory.inventory_list) {
		new_request.data.inventory.items = angular.copy(request.inventory.inventory_list);
	}
	
	if (request.inventory && request.inventory.move_details) {
		new_request.data.inventory.details = angular.copy(request.inventory.move_details);
	}
	
	if (request.move_size.raw == 11) {
		new_request.data.field_commercial_extra_rooms = request.commercial_extra_rooms.value;
	}
	
	if (packingDay) {
		new_request.data.field_date = packingDate;
		new_request.all_data.packing_request_id = request.nid;
		new_request.all_data.additionalInfo = {
			serviceType: request.service_type.value,
			status: request.status.value,
			moveDate: moment(request.date.value).format('MMM DD, YYYY')
		};

		if (!_.isUndefined(new_request.all_data.toStorage)) {
			delete new_request.all_data.toStorage;
		}
	}
	
	return new_request;
}
// fix console warning
moment.createFromInputFallback = function (config) {
	config._d = new Date(config._i);
};
