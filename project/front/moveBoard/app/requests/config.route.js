(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
              $stateProvider
                    .state('requests', {
                        url: '/requests',
                        abstract: true,
                        template: '<ui-view/>'
                    })
                    .state('requests.child', {
                        url: '/:id',
                        template: require('../../app/requests/areas/allrequests.html'),
                        title: 'Requests',
                        controller: 'RequestsController as vm',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('requests.status', {
                        url: '/status/:statusReq',
                        template: require('../../app/requests/areas/allrequests.html'),
                        title: 'Requests',
                        controller: 'RequestsController as vm',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });


            }
        ]
    );
})();
