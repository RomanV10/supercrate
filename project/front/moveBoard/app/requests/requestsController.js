'use strict';

angular
	.module('move.requests')
	.controller('RequestsController', RequestsController);

RequestsController.$inject = ['$scope', '$stateParams', 'RequestServices', 'logger'];

function RequestsController($scope, $stateParams, RequestServices, logger) {

	// Inititate the promise tracker to track form submissions.
	var vm = this;
	$scope.flagID = $stateParams.id;
	$scope.statusCode = $stateParams.statusReq;
	var lastRequest; //Set for cancel pending request for each reason
	vm.isRequestsPage = true;
	vm.date = {};
	vm.date.to = new Date();
	vm.date.from = new Date();
	vm.date.from.setMonth(vm.date.to.getMonth() - 1);
	vm.date.to = $.datepicker.formatDate('yy-mm-dd', vm.date.to, {});
	vm.date.from = $.datepicker.formatDate('yy-mm-dd', vm.date.from, {});
	vm.date.to = moment(vm.date.to).format('YYYY-MM-DD');
	vm.date.from = moment(vm.date.from).format('YYYY-MM-DD');
	vm.busy = true;
	vm.dtAllRequests = [];

	activate($scope.flagID);

	vm.filterFunction = function (element) {
		return !!element.service_type_id.match(1);
	};

	function activate() {
		//Default Loading
		var data = {
			pagesize: 25,
			page: 0,
			filtering: {
				date_from: moment(vm.date.from).startOf('day').unix(),
				date_to: moment(vm.date.to).endOf('day').unix(),
			},
			sorting: {
				direction: "DESC"
			}
		};
		// Load requests only for set serviceType
		if ($scope.flagID != 'requests')
			if (!!$scope.flagID) {
				data.condition = {
					field_flags: $scope.flagID
				}
			}


		if (!!$scope.statusCode) {
			data.condition = {
				field_approve: [$scope.statusCode]
			}
		}

		lastRequest = RequestServices.getAllRequestsDateRange(data);
		lastRequest.then(function (response) {
			vm.busy = false;
			vm.requests = response.data.nodes;
			vm.totalCount = response.data.count;
		}, function (reason) {
			vm.busy = false;
			logger.error(reason, reason, "Error");
		});
	}

	$scope.$on('request.updated', handlerUpdateRequest);

	function handlerUpdateRequest(event, data) {
		var nid = data.nid;
		var indReq = _.findIndex(vm.requests, req => req && req.nid == nid);
		vm.requests[indReq] = angular.copy(data);
	}

	$scope.$on('$destroy', function () {
		//Clear all requests
		RequestServices.cancel(lastRequest);
	});
}
