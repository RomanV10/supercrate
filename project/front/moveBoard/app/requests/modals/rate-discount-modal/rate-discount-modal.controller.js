'use strict';

angular
	.module('move.requests')
	.controller('RateDiscountModalInstanceCtrl', RateDiscountModalInstanceCtrl);

RateDiscountModalInstanceCtrl.$inject = ['$scope', '$uibModalInstance', 'request', 'message', 'editrequest', '$rootScope', 'RequestServices', 'SweetAlert'];

function RateDiscountModalInstanceCtrl($scope, $uibModalInstance, request, message, editrequest, $rootScope, RequestServices, SweetAlert) {
	const DEFAULT_DISCOUNT = 0;
	$scope.request = angular.copy(request);
	$scope.discount = DEFAULT_DISCOUNT;

	if (angular.isDefined($scope.request.request_all_data)) {
		if (angular.isArray($scope.request.request_all_data)) {
			$scope.request.request_all_data = {};
		}
	}

	if (angular.isUndefined(request.request_all_data.add_rate_discount)) {
		$scope.request.request_all_data.add_rate_discount = $scope.request.rate.value;
	}

	$scope.rateDiscount = $scope.request.request_all_data.add_rate_discount;

	if (angular.isUndefined($scope.request.request_all_data.localdiscount)) {
		$scope.request.request_all_data.localdiscount = DEFAULT_DISCOUNT;
		$scope.localdiscount = DEFAULT_DISCOUNT;
	} else {
		$scope.localdiscount = parseInt($scope.request.request_all_data.localdiscount);
		$scope.discount = $scope.localdiscount;
	}

	if ($scope.localdiscount) {
		$scope.uprate = Math.round($scope.request.rate.value / (1 - $scope.localdiscount / 100));

		if ($scope.rateDiscount != $scope.request.rate.value) {
			var rate_discount = (1 - $scope.request.request_all_data.add_rate_discount / $scope.uprate) * 100;
			$scope.localdiscount = rate_discount;
			$scope.discount = $scope.localdiscount;
		}
	} else {
		$scope.discount = (1 - $scope.request.request_all_data.add_rate_discount / $scope.request.rate.value ) * 100;
	}

	$scope.calcDiscount = function () {
		if ($scope.localdiscount) {
			var rate_discount = (1 - $scope.request.request_all_data.add_rate_discount / $scope.uprate) * 100;
			$scope.discount = rate_discount;
		} else {
			$scope.discount = (1 - $scope.request.request_all_data.add_rate_discount / $scope.request.rate.value) * 100;

			if ($scope.discount < DEFAULT_DISCOUNT) {
				SweetAlert.swal("You can not set negative discount!");
				$scope.discount = 0;
				$scope.request.request_all_data.add_rate_discount = DEFAULT_DISCOUNT;
			}
		}

		if ($scope.discount == DEFAULT_DISCOUNT) {
			$scope.request.request_all_data.add_rate_discount = DEFAULT_DISCOUNT;
		}
	};

	$scope.removeDiscount = function () {
		$scope.discount = DEFAULT_DISCOUNT;
		$scope.request.request_all_data.add_rate_discount = DEFAULT_DISCOUNT;
		RequestServices.sendLogs([{
			text: 'Rate discount was removed'
		}], 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
	};

	$scope.Apply = function () {
		SweetAlert.swal({
			title: "Are you sure you want to apply discount ?",
			text: "",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, let's do it!",
			cancelButtonText: "No, cancel pls!",
			closeOnConfirm: true,
			closeOnCancel: true
		}, applyNewDiscount);
	};

	function applyNewDiscount (confirm) {
		if (!confirm) {
			$scope.request.request_all_data.add_rate_discount = DEFAULT_DISCOUNT;
			return DEFAULT_DISCOUNT;
		}

		if (!!$scope.request.request_all_data.add_rate_discount) {
			let msg = {
				text: `New rate discount was applied`,
				to: `$${$scope.request.request_all_data.add_rate_discount}`
			};
			RequestServices.sendLogs([msg], 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
		}

		request.request_all_data = angular.copy($scope.request.request_all_data);
		RequestServices.saveReqData(request.nid, request.request_all_data);
		$rootScope.$broadcast('updateQuote');
		$uibModalInstance.dismiss('cancel');
	}

	$scope.changeDiscount = function (type) {
		if (type == 'money') {
			$scope.request.request_all_data.rate_discount = DEFAULT_DISCOUNT;
		} else {
			$scope.request.request_all_data.add_money_discount = DEFAULT_DISCOUNT;
		}
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}
