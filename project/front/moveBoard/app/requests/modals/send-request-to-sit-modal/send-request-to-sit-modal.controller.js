'use strict';

import tripPlannerConstants from '../../../lddispatch/trip-planner-constants.json';

angular
	.module('move.requests')
	.controller('sendRequestToSITModal', sendRequestToSITModal);

sendRequestToSITModal.$inject = ['$scope', '$uibModalInstance', 'request', 'StorageService', '$q', 'DispatchService', '$rootScope', 'apiService', 'moveBoardApi', 'RequestServices'];

function sendRequestToSITModal($scope, $uibModalInstance, request, StorageService, $q, DispatchService, $rootScope, apiService, moveBoardApi, RequestServices) {
	var request_all_data = _.isUndefined(request.request_all_data) ? {} : request.request_all_data;
	var initial_sit = tripPlannerConstants.initialSit;
	$scope.sit = _.isUndefined(request_all_data.sit) ? angular.copy(initial_sit) : angular.copy(request_all_data.sit);
	$scope.moveInDate = !$scope.sit.date ? moment.unix(request.date.raw).toDate() : moment.unix($scope.sit.date).toDate();
	
	$scope.basicSit = angular.copy($scope.sit);
	$scope.msgLog = {};
	$scope.requestId = request.nid;
	$scope.clientName = request.name;
	$scope.serviceTypes = tripPlannerConstants.sitServiceTypes;
	$scope.jobTypes = tripPlannerConstants.sitJobTypes;
	$scope.storages = [];
	$scope.workers = {};
	$scope.busy = true;
	$scope.selectedTab = 0;
	$scope.foremanSelectDisabled = false;
	$scope.setTab = (tab) => {
		$scope.selectedTab = tab;
	};
	
	apiService.getData(moveBoardApi.ldStorages.list, {short: 1, pageSize: -1, page: 1}).then(data => {
		$scope.storages = data.data.items;
		$scope.busy = false;
	});
	
	apiService.postData(moveBoardApi.foreman.list, {active: 1, role: ['foreman']}).then(data => {
		$scope.workers = $scope.getArrayFromObject(data.data.foreman);
		
		if (request.field_foreman.value && !Array.isArray(request.field_foreman.value)) {
			$scope.sit.foreman = request.field_foreman.value;
			$scope.foremanSelectDisabled = true;
		}
	}).catch(error => {
		console.log(error);
	});
	$scope.getArrayFromObject = (obj) => {
		var array = [];
		for (var key in obj) {
			obj[key].allName = obj[key].field_user_first_name + ' ' + obj[key].field_user_last_name;
			array.push(obj[key]);
		}
		return array;
	};
	
	$scope.save = function () {
		$scope.sit.date = moment($scope.moveInDate).unix();
		request_all_data.sit = $scope.sit;
		request.request_all_data = request_all_data;
		
		apiService.putData(moveBoardApi.requestSIT.create, {id: request.nid, entity_type: 0, data: $scope.sit});
		
		RequestServices.saveReqData(request.nid, request_all_data)
			.then(function () {
				$rootScope.$broadcast('request.updated', request);
				toastr.success('Job was saved in SIT', 'Success');
			});
		
		$uibModalInstance.close();
	};
	
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	//lot of numbers info
	$scope.addRow = function () {
		$scope.sit.lots_info.push({
			'create': '',
			'line': '',
			'lot_num': ''
		});
	};
	
	$scope.removeRow = function (index) {
		$scope.sit.lots_info.splice(index, 1);
	};
	$scope.changeField = function (field, value) {
		if (!_.isUndefined($scope.msgLog)) {
			var from = $scope.basicSit[field];
			var to = value;
			var textFrom = '';
			var textTo = '';
			
			if (field == 'lots_info') {
				textFrom = prepareMSGLogText(from);
				textTo = prepareMSGLogText(to);
			}
			
			if (field == 'status_flag') {
				if (from) {
					textFrom = $scope.serviceTypes[from];
				}
				
				if (to) {
					textTo = $scope.serviceTypes[to];
				}
			}
			
			if (field == 'foreman') {
				if (from) {
					textFrom = initial_sit.foreman[from];
				}
				
				if (to) {
					textTo = initial_sit.foreman[to];
				}
			}
			
			if (field == 'date') {
				if (from) {
					textFrom = moment.unix(from).format('MMM DD, YYYY');
				}
				
				if (to) {
					textTo = moment(to).format('MMM DD, YYYY');
				}
			}
			
			if (field == 'status') {
				if (from) {
					textFrom = $scope.serviceTypes[from];
				}
				
				if (to) {
					textTo = $scope.serviceTypes[to];
				}
			}
			
			if (textFrom != textTo) {
				$scope.msgLog[field] = {
					from: textFrom,
					to: textTo,
					text: field.replace(/_/g, ' ').charAt(0).toUpperCase() + field.replace(/_/g, ' ').slice(1) + ' was changed'
				};
			}
		}
	};
	
	function prepareMSGLogText(fieldValue) {
		var result = '';
		
		if (fieldValue) {
			_.forEach(fieldValue, function (obj) {
				_.forEach(obj, function (item, key) {
					if (angular.isUndefined(item)) {
						return false;
					}
					
					result += key.charAt(0).toUpperCase() + key.slice(1) + ': ' + item;
					
					if (key != _.findLastKey(obj)) {
						result += ', ';
					} else {
						result += '.<br>';
					}
				});
			});
		}
		
		return result;
	}
	
	$scope.isInvalidForm = function () {
		var result = $scope.storageInTransit.$invalid;
		
		if (result && $scope.storageInTransit.$error.date) {
			result = _.size($scope.storageInTransit.$error) > 1;
		}
		
		return result;
	};
	
	if ($scope.sit.lot_number.length == 0) {
		$scope.sit.lot_number.push({
			'number': '',
			'color': '',
			'from': '',
			'to': ''
		});
	}
	
	if ($scope.sit.lots_info.length == 0) {
		$scope.addRow();
	}
	
}
