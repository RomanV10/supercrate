'use strict';

// for salaryCommisionModal in project/front/moveBoard/app/requests/modals/modals.services.js

angular
	.module('move.requests')
	.controller('salaryCommissionInstanceCtrl', salaryCommissionInstanceCtrl);

/*@ngInject*/
function salaryCommissionInstanceCtrl($scope, $uibModalInstance, request, type, RequestServices, $q, $rootScope, PayrollConfigs, datacontext, ForemanPaymentService, SweetAlert) {
	$scope.request = angular.copy(request[0]);
	if (type) {
		$scope.type = type;
	}
	let crews = {};
	let switchers;
	if ($scope.request.request_data) {
		if ($scope.request.request_data.value != '') {
			crews = angular.fromJson($scope.request.request_data.value).crews;
			switchers = angular.fromJson($scope.request.request_data.value).switchers;
		}
	}
	
	$scope.payroll = angular.copy(request[1].data);
	$scope.workers = angular.copy(request[2]);
	$scope.invoice = angular.copy(request[3]);
	$scope.grandTotal = angular.copy(request[4]);
	let importedFlags = $rootScope.availableFlags;
	let Payroll_Done = 'Payroll Done';
	let pickupPayrollDone = 'Pickup Payroll Done';
	let deliveryPayrollDone = 'Delivery Payroll Done';
	let fieldData = datacontext.getFieldData(),
		contract_page = angular.fromJson(fieldData.contract_page),
		calcSettings = angular.fromJson(fieldData.calcsettings),
		contractTypes = fieldData.enums.contract_type;
	
	let travelTimeSetting = angular.isDefined($scope.request.request_all_data.travelTime)
		? $scope.request.request_all_data.travelTime
		: calcSettings.travelTime;
	if ($scope.request.request_data.value && $scope.request.request_data.value.paymentType && $scope.request.request_data.value.paymentType == 'LaborAndTravel')
		travelTimeSetting = true;
	
	$scope.selected = {
		salesPerson: [], 
		foreman: [], 
		driver: [], 
		helper: [], 
		foremanAsHelper: []
	};
	$scope.selectedCommissions = {};
	$scope.foreman = [];
	$scope.foremanAsHelper = [];
	$scope.driver = [];
	$scope.helper = [];
	$scope.salesPerson = [];
	$scope.msgLog = [];
	$scope.msgLogCommission = {};
	$scope.msgLogRate = {};
	$scope.busy = true;
	$scope.baseForemanInd = 1;
	$scope.roles = ['salesPerson', 'foreman', 'driver', 'helper', 'foremanAsHelper'];
	$scope.workersTotal = {};
	$scope.workersTotal = {
		foreman: 0, 
		helper: 0, 
		salesPerson: 0, 
		driver: 0
	};
	$scope.commissionType = ['$', '%', 'Hrs'];
	$scope.commissionHelperForemanList = [
		{name: '', type: 1}, // { name: 'Additional Service', type: 1},
		{name: 'Commission from total', type: 1}, 
		{name: 'Extras Commission', type: 1}, 
		{name: 'Daily Rate', type: 1},
		{name: 'Hourly Rate', type: 0}, 
		{name: '', type: 1}, // { name: 'Insurance', type: 1},
		{name: '', type: 0}, // { name: 'Loading per ou.ft', type: 0},
		{name: 'Packing Commission', type: 1}, 
		{name: '', type: 0}, // { name: 'Unloading per ou.ft', type: 0},
		{name: 'Hourly rate helper', type: 0}, 
		{name: 'Tips', type: 0}, 
		{name: 'Bonus', type: 0}
	];
	$scope.commissionManagerList = [
		{name: 'Office Commission', type: 1}, 
		{name: 'Office With visual', type: 1},
		{name: 'Visual Estimate', type: 1}
	];
	$scope.tabs = [
		{name: 'Sales Person', selected: true}, 
		{name: 'Foreman', selected: false}, 
		{name: 'Helpers', selected: false}, 
		{name: 'Driver', selected: false}
	];
	$scope.calcEmployeesTotal = calcEmployeesTotal;
	$scope.calcWorkerTotal = calcWorkerTotal;
	$scope.addReceipt = addReceipt;
	let reqForeman = [];
	let localServices = [1, 2, 3, 4, 6, 8];
	let isDoubleDriveTime = calcSettings.doubleDriveTime && $scope.request.field_double_travel_time && !_.isNull($scope.request.field_double_travel_time.raw) && localServices.indexOf(parseInt($scope.request.service_type.raw)) >= 0;

	$scope.submitPayroll = submitPayroll;
	$scope.foremanInfo = foremanInfo;
	$scope.openCustomReceipt = openCustomReceipt;
	$scope.removeCustomCommission = removeCustomCommission;
	$scope.saveCustomCommission = saveCustomCommission;
	$scope.enumerationTypesHelperForeman = enumerationTypesHelperForeman;
	$scope.enumerationRateType = enumerationRateType;
	$scope.enumerationCommissionsType = enumerationCommissionsType;
	$scope.enumerationTypesManager = enumerationTypesManager;
	$scope.select = select;
	$scope.cancel = cancel;
	$scope.changeForemanHelper = changeForemanHelper;
	$scope.changeCommission = changeCommission;
	$scope.isSelected = isSelected;
	$scope.addWorker = addWorker;
	$scope.reSubmitPayroll = reSubmitPayroll;
	$scope.sendRequestPayroll = sendRequestPayroll;
	$scope.removeWorker = removeWorker;
	$scope.isSelectedCommission = isSelectedCommission;
	$scope.getWorker = getWorker;
	$scope.changeForemanHelperCommissions = changeForemanHelperCommissions;
	$scope.addNewCommission = addNewCommission;
	$scope.removeCommission = removeCommission;
	$scope.checkCommissions = checkCommissions;
	$scope.revokePayroll = revokePayroll;
	$scope.disableRate = disableRate;
	
	function getTravelTime() {
		let travelTime = travelTimeSetting ? +$scope.invoice.travel_time.raw : 0;
		return isDoubleDriveTime ? $scope.invoice.field_double_travel_time.raw : travelTime;
	}
	
	let laborTime = 0;
	let contractInfoHours = _.get($scope.request, 'contract_info.hours');
	if (!_.isUndefined(contractInfoHours)) {
		laborTime = (contract_page.minPayrollHours > $scope.request.contract_info.hours) ? contract_page.minPayrollHours : $scope.request.contract_info.hours;
	} else {
		let time = getTravelTime();
		if (angular.isDefined($scope.invoice.work_time_int)) {
			laborTime = $scope.invoice.work_time_int + time;
		} else {
			laborTime = time;
		}
	}
	laborTime = contract_page.minPayrollHours > laborTime ? contract_page.minPayrollHours : laborTime;
	
	if (_.isArray($scope.request.field_foreman.value)) {
		reqForeman = $scope.request.field_foreman.value;
	} else if (_.isString($scope.request.field_foreman.value)) {
		reqForeman.push($scope.request.field_foreman.value);
	}
	
	_.forEach($scope.payroll, function (payroll, i) {
		_.forEach(payroll, function (worker, j) {
			if (!_.isEmpty(worker)) {
				$scope.payroll[i][j].uid = j;
				if (angular.isUndefined($scope.payroll[i][j].commissions)) {
					createCommissions(i, j);
				}
				
				$scope.selectedCommissions[j] = {};
				for (let k in $scope.payroll[i][j].commissions) {
					$scope.selectedCommissions[j][k] = k;
				}
				if (i == 'manager' || i == 'sales') {
					$scope.salesPerson.push($scope.payroll[i][j]);
					$scope.selected.salesPerson.push(j);
					$scope.calcEmployeesTotal();
					$scope.calcWorkerTotal('salesPerson');
				} else {
					//set foremen, foremen as helpers, helpers
					if ($scope.type) {
						//if Flat Rate
						if (!_.isEmpty(crews)) {
							if (crews.deliveryCrew && crews.deliveryCrew.foreman && $scope.type == 'deliveryCrew' && _.findIndex($scope[i], {uid: j}) < 0 && i == 'foreman') {
								if (_.indexOf(reqForeman, j) >= 0 && j == crews.deliveryCrew.foreman) {
									$scope.foreman.push($scope.payroll[i][j]);
									$scope.selected[i].push(j);
								}
							}
							if (crews.pickedUpCrew && crews.pickedUpCrew.foreman && $scope.type == 'pickedUpCrew' && _.findIndex($scope[i], {uid: j}) < 0 && i == 'foreman') {
								if (_.indexOf(reqForeman, j) >= 0 && j == crews.pickedUpCrew.foreman) {
									$scope.foreman.push($scope.payroll[i][j]);
									$scope.selected[i].push(j);
								}
							}
							if (crews.deliveryCrew && $scope.type == 'deliveryCrew') {
								_.forEach(crews.deliveryCrew.helpers, function (worker) {
									if (worker != '') {
										if (j == worker && _.indexOf(reqForeman, worker) < 0 && i == 'foreman' && !_.invert(switchers)[worker]) {
											$scope.foremanAsHelper.push($scope.payroll[i][j]);
											$scope.selected.foremanAsHelper.push(j);
										} else if (j == worker && (i == 'helper' || i == 'driver')) {
											$scope[i].push($scope.payroll[i][j]);
											$scope.selected[i].push(j);
										}
									}
								});
							}
							if (crews.pickedUpCrew && $scope.type == 'pickedUpCrew') {
								_.forEach(crews.pickedUpCrew.helpers, function (worker) {
									if (worker != '') {
										if (j == worker && _.indexOf(reqForeman, worker) < 0 && i == 'foreman' && !_.invert(switchers)[worker]) {
											$scope.foremanAsHelper.push($scope.payroll[i][j]);
											$scope.selected.foremanAsHelper.push(j);
										} else if (j == worker && (i == 'helper' || i == 'driver')) {
											$scope[i].push($scope.payroll[i][j]);
											$scope.selected[i].push(j);
										}
									}
								});
							}
						} else {
							setCustomWorkers(i, j);
						}
					} else {
						// if local Move
						if (!_.isEmpty(crews)) {
							if (crews.foreman) {
								if (_.indexOf(reqForeman, j) >= 0 && j == crews.foreman && _.findIndex($scope[i], {uid: j}) < 0 && i == 'foreman') {
									$scope.foreman.push($scope.payroll[i][j]);
									$scope.selected[i].push(j);
								}
							}
							if (crews.baseCrew) {
								_.forEach(crews.baseCrew.helpers, function (worker) {
									if (j == worker && _.indexOf(reqForeman, worker) < 0 && i == 'foreman') {
										$scope.foremanAsHelper.push($scope.payroll[i][j]);
										$scope.selected.foremanAsHelper.push(j);
									} else if (j == worker && (i == 'helper' || i == 'driver')) {
										$scope[i].push($scope.payroll[i][j]);
										$scope.selected[i].push(j);
									}
								});
							}
							if (crews.additionalCrews) {
								_.forEach(crews.additionalCrews, function (addCrews) {
									_.forEach(addCrews.helpers, function (worker) {
										if (j == worker && _.indexOf(reqForeman, worker) < 0 && i == 'foreman') {
											$scope.foremanAsHelper.push($scope.payroll[i][j]);
											$scope.selected.foremanAsHelper.push(j);
										} else if (j == worker && (i == 'helper' || i == 'driver')) {
											$scope[i].push($scope.payroll[i][j]);
											$scope.selected[i].push(j);
										}
									});
								});
							}
						} else {
							setCustomWorkers(i, j);
						}
					}
					setCustomWorkers(i, j);
				}
			}
		});
	});
	if (!_.isEmpty(switchers)) {
		_.forEach(switchers, function (switchUid, mainUid) {
			//check foremens' switchers
			if (crews.foreman) {
				if (switchers[crews.foreman] && mainUid == crews.foreman && _.findIndex($scope.foreman, {uid: switchUid}) == -1) {
					$scope.foreman.push($scope.payroll.foreman[switchUid]);
					$scope.selected.foreman.push(switchUid);
				} else if ($scope.payroll.foreman[switchUid] && _.findIndex($scope.foremanAsHelper, {uid: switchUid}) == -1 && _.findIndex($scope.foreman, {uid: switchUid}) == -1) {
					$scope.foremanAsHelper.push($scope.payroll.foreman[switchUid]);
					$scope.selected.foremanAsHelper.push(switchUid);
				}
			} else {
				if (switchers[crews.deliveryCrew.foreman] && mainUid == crews.deliveryCrew.foreman && $scope.type == 'deliveryCrew' && _.findIndex($scope.foreman, {uid: switchUid}) == -1) {
					$scope.foreman.push($scope.payroll.foreman[switchUid]);
					$scope.selected.foreman.push(switchUid);
				} else if (switchers[crews.pickedUpCrew.foreman] && mainUid == crews.pickedUpCrew.foreman && _.findIndex($scope.foreman, {uid: switchUid}) == -1) {
					$scope.foreman.push($scope.payroll.foreman[switchUid]);
					$scope.selected.foreman.push(switchUid);
				}
			}
			//check helpers' switchers
			if (_.findIndex($scope.helper, {uid: mainUid}) >= 0 && _.findIndex($scope.helper, {uid: switchUid}) == -1 && $scope.payroll.helper[switchUid]) {
				$scope.helper.push($scope.payroll.helper[switchUid]);
				$scope.selected.helper.push(switchUid);
			} else if (_.findIndex($scope.driver, {uid: mainUid}) >= 0 && _.findIndex($scope.driver, {uid: mainUid}) == -1 && $scope.payroll.driver[switchUid]) {
				$scope.driver.push($scope.payroll.driver[switchUid]);
				$scope.selected.driver.push(switchUid);
			}
		});
	}
	_.forEach($scope.payroll, function (payroll, i) {
		if (i == 'manager' || i == 'sales') {
			_.forEach($scope.salesPerson, function (worker, uid) {
				let tmpCommissions = [];
				_.forEach(worker.commissions, function (commission, j) {
					commission.id = j;
					tmpCommissions.push(commission);
				});
				$scope.salesPerson[uid].commissions = [];
				$scope.salesPerson[uid].commissions = tmpCommissions;
			});
		} else {
			_.forEach($scope[i], function (worker, uid) {
				let tmpCommissions = [];
				_.forEach(worker.commissions, function (commission, j) {
					commission.id = j;
					tmpCommissions.push(commission);
				});
				$scope[i][uid].commissions = [];
				$scope[i][uid].commissions = tmpCommissions;
			});
		}
	});
	_.forEach($scope.foremanAsHelper, function (worker, uid) {
		let tmpCommissions = [];
		_.forEach(worker.commissions, function (commission, j) {
			commission.id = j;
			tmpCommissions.push(commission);
		});
		$scope.foremanAsHelper[uid].commissions = [];
		$scope.foremanAsHelper[uid].commissions = tmpCommissions;
	});
	
	function setCustomWorkers(i, j) {
		if (_.findIndex($scope[i], {uid: j}) != -1) {
			return false;
		}
		if (i == 'foreman' && _.indexOf(reqForeman, j) >= 0 && _.findIndex($scope[i], {uid: j}) == -1) {
			$scope[i].push($scope.payroll[i][j]);
			$scope.selected[i].push(j);
		} else if (i == 'manager' || i == 'sales') {
			$scope.salesPerson.push($scope.payroll[i][j]);
			$scope.selected.salesPerson.push(j);
			$scope.calcEmployeesTotal();
			$scope.calcWorkerTotal('salesPerson');
			
		} else {
			if (i == 'foreman' && _.indexOf(reqForeman, j) < 0 && !_.invert(switchers)[j] && _.findIndex($scope[i], {uid: j}) == -1 && _.findIndex($scope.foremanAsHelper, {uid: j}) == -1) {
				$scope.foremanAsHelper.push($scope.payroll[i][j]);
				$scope.calcEmployeesTotal();
				$scope.calcWorkerTotal('foremanAsHelper');
				$scope.selected.foremanAsHelper.push(j);
			} else if (_.findIndex($scope[i], {uid: j}) == -1 && _.findIndex($scope.foremanAsHelper, {uid: j}) == -1) {
				$scope[i].push($scope.payroll[i][j]);
				$scope.calcEmployeesTotal();
				$scope.calcWorkerTotal(i);
				$scope.selected[i].push(j);
			}
		}
	}
	
	function createCommissions(i, j) {
		if (_.isEmpty($scope.payroll[i][j])) {
			return false;
		}
		$scope.payroll[i][j].commissions = {};
		let payrollWorker = angular.copy($scope.payroll[i][j]);
		let settingsWorker = angular.copy($scope.workers[i][j]);
		_.forEach(settingsWorker.rates, function (rates, n) {
			payrollWorker.commissions[n] = {};
			payrollWorker.commissions[n].for_commission = 0;
			payrollWorker.commissions[n].total = 0;
			_.forEach(rates, function (rate, c) {
				payrollWorker.commissions[n][c] = angular.copy(settingsWorker.rates[n][c]);
				if (n == '3') { //daily rate
					payrollWorker.commissions[n].rate = 100;
					payrollWorker.commissions[n].for_commission = angular.copy(settingsWorker.rates[n].rate);
					payrollWorker.commissions[n].total = payrollWorker.commissions[n].for_commission;
				}
				if (n == '4' || n == '11') { //hourly rate, bonus
					payrollWorker.commissions[n].for_commission = laborTime;
				}
				if (typeof payrollWorker.commissions[n].rate == 'string' && payrollWorker.commissions[n].rate.indexOf(',') > -1) {
					payrollWorker.commissions[n].rate = payrollWorker.commissions[n].rate.replace(/,/g, '.');
				}
				if (typeof payrollWorker.commissions[n].for_commission == 'string' && payrollWorker.commissions[n].for_commission.indexOf(',') > -1) {
					if (!payrollWorker.commissions[n].for_commission && !payrollWorker.sumFromTotal) {
						getTotalForWorker(payrollWorker.uid).then(function (data) {
							payrollWorker.sumFromTotal = _.head(data);
							payrollWorker.commissions[n].for_commission = getSumFromTotal(payrollWorker);
						});
					} else {
						payrollWorker.commissions[n].for_commission = getSumFromTotal(payrollWorker);
					}
				}
				if (typeof payrollWorker.commissions[n].total == 'string' && payrollWorker.commissions[n].total.indexOf(',') > -1) {
					payrollWorker.commissions[n].total = payrollWorker.commissions[n].total.replace(/,/g, '.');
				}
				if (payrollWorker.commissions[n].type == 0) {
					payrollWorker.commissions[n].total = Number(payrollWorker.commissions[n].for_commission) * Number(payrollWorker.commissions[n].rate);
					
				}
				if (payrollWorker.commissions[n].type == 1) {
					payrollWorker.commissions[n].total = Number(payrollWorker.commissions[n].for_commission) * Number(payrollWorker.commissions[n].rate) / 100;
				}
				payrollWorker.commissions[n].total = Number(_.round(payrollWorker.commissions[n].total, 2));
				if (n == '10') { //tips
					payrollWorker.commissions[n].total = Number(payrollWorker.commissions[n].for_commission) * Number(payrollWorker.commissions[n].rate) / 100;
					payrollWorker.commissions[n].total = Number(_.round(payrollWorker.commissions[n].total, 2));
				}
			});
		});
		$scope.payroll[i][j] = payrollWorker;
		$scope.workers[i][j] = settingsWorker;
	}
	
	function getTotalForWorker(uid) {
		let deferred = $q.defer();
		
		let contractType;
		if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
			contractType = contractTypes.PAYROLL;
		} else if ($scope.request.service_type.raw == 5) {
			if ($scope.type == 'pickedUpCrew') {
				contractType = contractTypes.PICKUP_PAYROLL;
			}
			if ($scope.type == 'deliveryCrew') {
				contractType = contractTypes.DELIVERY_PAYROLL;
			}
		}
		RequestServices.getSumCommissionFromTotal(uid, $scope.request.nid, contractType).then(function (data) {
			deferred.resolve(data);
		});
		
		return deferred.promise;
	}
	
	function addReceipt(uid) {
		let contractType;
		if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
			contractType = contractTypes.PAYROLL;
		} else if ($scope.request.service_type.raw == 5) {
			if ($scope.type == 'pickedUpCrew') {
				contractType = contractTypes.PICKUP_PAYROLL;
			}
			
			if ($scope.type == 'deliveryCrew') {
				contractType = contractTypes.DELIVERY_PAYROLL;
			}
		}
		ForemanPaymentService.openPaymentModal($scope.request.nid, uid, contractType);
	}
	
	function addFlag(flag) {
		if (!$scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[flag])) {
			return false; //Submit
		} else if ($scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[flag])) {
			return true; //Re-submit
		}
	}
	
	function submitPayroll () {
		if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
			return addFlag(Payroll_Done);
		} else if ($scope.request.service_type.raw == 5) {
			if ($scope.type == 'pickedUpCrew') {
				return addFlag(pickupPayrollDone);
			}
			if ($scope.type == 'deliveryCrew') {
				return addFlag(deliveryPayrollDone);
			}
		}
	};
	
	function foremanInfo(worker) {
		let name = worker.info.field_user_first_name + ' ' + worker.info.field_user_last_name;
		return name;
	};
	$scope.workers.salesPerson = {};
	$scope.workers.foremanAsHelper = {};
	$scope.workers.foremanAsHelper = $scope.workers.foreman;
	for (let j in $scope.workers) {
		for (let i in $scope.workers[j]) {
			if (j == 'sales' || j == 'manager') {
				$scope.workers.salesPerson[i] = {};
				$scope.workers.salesPerson[i] = $scope.workers[j][i];
				if ($scope.workers.salesPerson[i] && $scope.workers.salesPerson[i].info && $scope.workers.salesPerson[i].info.name) {
					$scope.workers.salesPerson[i].info.name = $scope.foremanInfo($scope.workers[j][i]);
				}
			} else {
				if ($scope.workers[j][i] && $scope.workers[j][i].info && $scope.workers[j][i].info.name) {
					$scope.workers[j][i].info.name = $scope.foremanInfo($scope.workers[j][i]);
				}
			}
		}
	}
	for (let i in $scope.workers) {
		for (let j in $scope.workers[i]) {
			for (let k in $scope.workers[i][j].rates) {
				if (i == 'salesPerson' && k < 3) {
					$scope.workers[i][j].rates[k].field = $scope.commissionManagerList[k].name;
				} else if (i == 'foreman' || i == 'helper' || i == 'driver') {
					$scope.workers[i][j].rates[k].field = $scope.commissionHelperForemanList[k].name;
				}
			}
		}
	}
	let arrPromise = [];
	
	let contractTypePayroll;
	if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
		contractTypePayroll = contractTypes.PAYROLL;
	} else if ($scope.request.service_type.raw == 5) {
		if ($scope.type == 'pickedUpCrew') {
			contractTypePayroll = contractTypes.PICKUP_PAYROLL;
		}
		
		if ($scope.type == 'deliveryCrew') {
			contractTypePayroll = contractTypes.DELIVERY_PAYROLL;
		}
	}
	_.forEach($scope.payroll, function (payroll, role) {
		_.forEach(payroll, function (worker, i) {
			arrPromise.push(ForemanPaymentService.getCustomPayroll($scope.request.nid, i, contractTypePayroll));
		});
	});
	
	$q.all(arrPromise).then(function (data) {
		$scope.busy = false;
		_.forEach(data, function (workerPayrolls) {
			_.forEach(workerPayrolls, function (custom_payroll, id) {
				custom_payroll.id = id;
				_.forEach($scope.roles, function (val) {
					let ind = _.findIndex($scope[val], {uid: custom_payroll.uid});
					if (ind > -1) {
						if (!$scope[val][ind].custom_commission) {
							$scope[val][ind].custom_commission = [];
						}
						$scope[val][ind].custom_commission.push(custom_payroll);
					}
					$scope.calcEmployeesTotal();
					$scope.calcWorkerTotal(val);
				});
			});
		});
	});
	
	function openCustomReceipt(role, ind, receipt) {
		let usrName = $scope[role][ind].name;
		ForemanPaymentService.openCustomReceipt(usrName, receipt);
	}
	
	function removeCustomCommission(role, userInd, customInd, id) {
		SweeetAlert.swal({
			title: 'Are you sure?',
			text: 'Remove this payment from payroll?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes!',
			closeOnConfirm: true
		}, function () {
			$scope.busy = true;
			ForemanPaymentService.removeCustomPayroll(id).then(function () {
				$scope[role][userInd].custom_commission.splice(customInd, 1);
				$scope.busy = false;
			});
		});
	}
	
	function saveCustomCommission(role, userInd, customInd, customId) {
		let data = {
			'id': customId,
			'name': $scope[role][userInd].custom_commission[customInd].name,
			'amount': $scope[role][userInd].custom_commission[customInd].amount,
			'photo': []
		};
		$scope.busy = true;
		ForemanPaymentService.updateCustomPayroll(data).then(function () {
			$scope.busy = false;
		});
	}
	
	function enumerationTypesHelperForeman(key) {
		if (_.isUndefined(key) || _.isEqual(key, 'new')) {
			
			return false;
		}
		
		let i = Number(key);
		return $scope.commissionHelperForemanList[i].name;
	}
	
	function enumerationRateType(key, index, role) {
		let uid = $scope[role][index].uid;
		let i = 0;
		if (key == '11') {
			return $scope.commissionType[0];
		}
		if (key == '10' || key == 'new') {
			i = 1;
			return $scope.commissionType[i];
		} else if ($scope.workers[role][uid].rates[key].type == 0) {
			i = 0;
			return $scope.commissionType[i];
		} else if ($scope.workers[role][uid].rates[key].type == 1) {
			i = 1;
			return $scope.commissionType[i];
		}
	}
	function enumerationCommissionsType(key, index, role) {
		let uid = $scope[role][index].uid;
		let i = 0;
		if (key == '11') {
			return $scope.commissionType[2];
		}
		if (key == '10' || key == 'new') {
			i = 0;
			return $scope.commissionType[i];
		} else {
			if (angular.isUndefined($scope.workers[role][uid].rates[key])) {
				if (role != 'salesPerson') {
					$scope.workers[role][uid].rates[key] = {};
					$scope.workers[role][uid].rates[key].type = $scope.commissionHelperForemanList[key].type;
				} else {
					$scope.workers[role][uid].rates[key].type = $scope.commissionManagerList[key].type;
				}
			}
			if ($scope.workers[role][uid].rates[key].type == 0) {
				i = 2;
				return $scope.commissionType[i];
			} else if ($scope.workers[role][uid].rates[key].type == 1) {
				i = 0;
				return $scope.commissionType[i];
			}
			i = $scope.workers[role][uid].rates[key].type;
			return $scope.commissionType[i];
		}
	}
	function enumerationTypesManager(key, uid, role) {
		let i = Number(key);
		return $scope.commissionManagerList[i].name;
	}
	
	function select(tab) {
		angular.forEach($scope.tabs, function (tab) {
			tab.selected = false;
		});
		tab.selected = true;
	}
	
	function cancel() {
		if ($scope.editChanges) {
			SweetAlert.swal({
				title: 'Are you sure?',
				text: 'Close this payroll?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes!',
				closeOnConfirm: true
			}, function () {
				$uibModalInstance.dismiss('cancel');
			});
		} else {
			$uibModalInstance.dismiss('cancel');
		}
	}
	
	function calcEmployeesTotal() {
		$scope.calcTotal = 0;
		let roles = ['helper', 'foreman', 'driver', 'salesPerson', 'foremanAsHelper'];
		
		_.each(roles, function (role, roleIndex) {
			_.each($scope[role], function (worker, workerIndex) {
				_.each(worker.commissions, function (commission, commisionIndex) {
					if (_.isString(commission.total) && commission.total > -1) {
						
						commission.total = commission.total.replace(/,/g, '.');
					}
					
					$scope.calcTotal += Number(commission.total);
				});
				
				
				if (!_.isUndefined(worker.custom_commission)) {
					_.forEach(worker.custom_commission, function (commission, n) {
						$scope.calcTotal += Number(commission.amount);
					});
				}
			});
		});
		
		$scope.calcTotal = _.round($scope.calcTotal, 2);
	}
	
	function checkForemanHelperHasCustomPayment(action, role, index) {
		let currentWorker = $scope[role][index];
		let isForeman = _.isEqual(role, 'foreman');
		let isHelper = _.isEqual(role, 'helper');
		let isChange = _.isEqual(action, 'change');
		
		if ((isForeman || isHelper) && _.isArray(currentWorker.custom_commission) && currentWorker.custom_commission.length) {
			let roleFirstCharacterUppercase = role.charAt(0).toUpperCase() + role.substr(1);
			let alertTitle = `${roleFirstCharacterUppercase} has custom payment`;
			let alertText = `You can't ${action} a ${role} with custom payment`;
			let alertType = 'warning';
			if (isChange) {
				if (isForeman) {
					$scope.selected.foreman[index] = currentWorker.uid;
				}
				if (isHelper) {
					$scope.selected.helper[index] = currentWorker.uid;
				}
			}
			
			SweetAlert.swal({
				title: alertTitle, 
				text: alertText, 
				type: alertType,
			});
			return true;
		}
	}
	
	function changeForemanHelper(index, newWorker, role) {
		let currentWorker = $scope[role][index];
		let isCanChange = !checkForemanHelperHasCustomPayment('change', role, index);
		
		if (isCanChange) {
			$scope.editChanges = true;
			let key = currentWorker.uid;
			let obj = {};
			
			if (currentWorker) {
				obj = angular.copy(currentWorker);
			}
			
			if (!obj.new) {
				let msg = {
					text: 'Worker was changed',
					to: $scope.foremanInfo($scope.workers[role][newWorker]),
					from: $scope.foremanInfo($scope.workers[role][key])
				};
				$scope.msgLog.push(msg);
				currentWorker.old_uid = obj.uid;
				
				delete currentWorker.commissions;
			} else {
				let msg = {
					text: 'New worker was added', to: $scope.foremanInfo($scope.workers[role][newWorker])
				};
				$scope.msgLog.push(msg);
				currentWorker.new = obj.new;
			}
			
			currentWorker.name = $scope.foremanInfo($scope.workers[role][newWorker]);
			currentWorker.total = 0;
			currentWorker.uid = newWorker;
			
			if (obj.additional || obj.switcher) {
				currentWorker.additional = obj.additional;
				currentWorker.switcher = obj.switcher;
			} else {
				currentWorker.additional = 0;
				currentWorker.switcher = 0;
			}
			
			if (!currentWorker.sumFromTotal) {
				$scope.busy = true;
				getTotalForWorker(currentWorker.uid).then(function (data) {
					$scope.busy = false;
					currentWorker.sumFromTotal = _.head(data);
					return setNewWorkerCommissions(role, index, newWorker);
				});
			} else {
				return setNewWorkerCommissions(role, index, newWorker);
			}
		}
	}
	
	function changeCommission(index, id, rate, role, type) {
		$scope.editChanges = true;
		let workerCommission = angular.copy($scope[role][index].commissions);
		workerCommission[rate].id = id;
		let commissionType = $scope.commissionHelperForemanList[id].type;
		
		if (role == 'salesPerson' && $scope.commissionManagerList[id]) {
			commissionType = $scope.commissionManagerList[id].type;
		}
		if (typeof workerCommission[rate].rate == 'string' && workerCommission[rate].rate.indexOf(',') > -1) {
			workerCommission[rate].rate = workerCommission[rate].rate.replace(/,/g, '.');
		}
		if (typeof workerCommission[rate].for_commission == 'string' && workerCommission[rate].for_commission.indexOf(',') > -1) {
			workerCommission[rate].for_commission = workerCommission[rate].for_commission.replace(/,/g, '.');
		}
		if (typeof workerCommission[rate].total == 'string' && workerCommission[rate].total.indexOf(',') > -1) {
			workerCommission[rate].total = workerCommission[rate].total.replace(/,/g, '.');
		}
		if (id == '10') {
			workerCommission[rate].total = Number(workerCommission[rate].for_commission) * Number(workerCommission[rate].rate) / 100;
			workerCommission[rate].total = Number(_.round(workerCommission[rate].total, 2));
			$scope.calcEmployeesTotal();
			$scope.calcWorkerTotal(role);
			$scope[role][index].commissions = angular.copy(workerCommission);
			return false;
		}
		if (commissionType == 0) {
			workerCommission[rate].total = Number(workerCommission[rate].for_commission) * Number(workerCommission[rate].rate);
			
		}
		if (commissionType == 1) {
			workerCommission[rate].total = Number(workerCommission[rate].for_commission) * Number(workerCommission[rate].rate) / 100;
		}
		workerCommission[rate].total = Number(_.round(workerCommission[rate].total, 2));
		
		$scope[role][index].commissions = angular.copy(workerCommission);
		
		$scope.calcEmployeesTotal();
		$scope.calcWorkerTotal(role);
	}
	
	function setNewWorkerCommissions(role, index, newWorker) {
		let commissions = [];
		let payrollWorker = $scope[role][index];
		
		_.forEach($scope.workers[role][newWorker].rates, function (rate, i) {
			let rateObj = {};
			rateObj.id = i;
			rateObj.for_commission = 0;
			rateObj.rate = 0;
			
			if (rate.rate) {
				rateObj.rate = rate.rate;
			}
			
			rateObj.total = 0;
			rateObj.type = rate.type;
			
			if (role == 'salesPerson') {
				if (i >= 1) {
					return;
				}
				if (i != 0) {
					rateObj.for_commission = 0;
					rateObj.rate = 0;
					rateObj.total = 0;
				}
				if (payrollWorker.sumFromTotal) {
					rateObj.for_commission = getSumFromTotal(payrollWorker);
				} else {
					rateObj.for_commission = 0;
				}
				// rateObj.for_commission = angular.copy(angular.isDefined($scope.grandTotal) ? $scope.grandTotal : 0);
			} else {
				if (i == '3') {
					rateObj.rate = 100;
					rateObj.for_commission = angular.copy(angular.isDefined(rate.rate) ? rate.rate : 0);
					rateObj.total = rateObj.for_commission;
					rateObj.type = angular.copy($scope.commissionHelperForemanList[i].type);
				}
				
				if (i == '4' || i == '11') {
					rateObj.for_commission = laborTime;
				}
			}
			
			commissions.push(rateObj);
		});
		
		
		payrollWorker.commissions = [];
		payrollWorker.commissions = commissions;
		
		for (let j in payrollWorker.commissions) {
			changeCommission(index, payrollWorker.commissions[j].id, j, role, payrollWorker.commissions[j].type);
		}
		
		$scope.addNewWorker = false;
		$scope.calcEmployeesTotal();
		$scope.calcWorkerTotal(role);
		
		return payrollWorker;
	}
	
	function isSelected(index, uid, role) {
		if (role == 'foreman' || role == 'foremanAsHelper') {
			for (let i in $scope.foreman) {
				if ($scope.foreman[i].uid == uid) {
					return true;
				}
			}
			for (let i in $scope.foremanAsHelper) {
				if ($scope.foremanAsHelper[i].uid == uid) {
					return true;
				}
			}
		} else {
			for (let i in $scope[role]) {
				if ($scope[role][i].uid == uid) {
					return true;
				}
			}
		}
	}

	function addWorker(role) {
		let obj = {
			new: 1, commissions: [], name: '', total: 0
		};
		$scope[role].push(obj);
		$scope.addNewWorker = true;
	}
	
	function reSubmitPayroll() {
		$scope.sendRequestPayroll();
	}
	
	function sendRequestPayroll() {
		SweetAlert.swal({
			title: 'Are you sure?',
			text: 'Resubmit this payroll?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes!',
			closeOnConfirm: true
		}, function (isConfirm) {
			if (isConfirm) {
				$scope.busy = true;
				$scope.updateUserPayrollPromise = [];
				$scope.setPayrollInfoPromise = [];
				let nid = $scope.request.nid;
				let roles = ['helper', 'foreman', 'salesPerson', 'foremanAsHelper', 'driver'];
				let contract_type = contractTypes.PAYROLL;
				
				if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
					contract_type = contractTypes.PAYROLL;
				} else {
					if ($scope.type == 'pickedUpCrew') {
						contract_type = contractTypes.PICKUP_PAYROLL;
					} else if ($scope.type == 'deliveryCrew') {
						contract_type = contractTypes.DELIVERY_PAYROLL;
					}
				}
				
				_.each(roles, function (role, index) {
					_.each($scope[role], function (s_role, index) {
						if (s_role.old_uid) {
							let update = {
								nid: nid, old_uid: s_role.old_uid, new_uid: s_role.uid, contract_type: contract_type
							};
							
							let funcUdate = function () {
								return RequestServices.updateUserPayroll(update);
							};
							
							$scope.updateUserPayrollPromise.push(funcUdate());
						}
					});
				});
				
				$q.all($scope.updateUserPayrollPromise).then(function (data) {
					let obj = {
						nid: nid, data: []
					};
					
					_.each(roles, function (role, index) {
						let workersByRole = $scope[role];
						
						if (role == 'foreman' && !_.isEmpty(workersByRole)) {
							let foremanUids = [];
							
							_.each(workersByRole, function (value) {
								foremanUids.push(value.uid);
							});
							
							request[0].field_foreman.value = foremanUids;
						}
						
						_.each(workersByRole, function (s_role, index) {
							let totalCommission = 0;
							
							if (index != '$$hashKey') {
								let worker = {
									uid: s_role.uid, value: {
										additional: 0, switcher: 0, commissions: {}, total: totalCommission
									}
								};
								
								if (s_role.new) {
									worker.new = 1;
									s_role.new = false;
								} else {
									worker.new = 0;
								}
								
								_.forEach(s_role.commissions, function (commission, i) {
									let k = commission.id;
									worker.value.commissions[k] = {};
									s_role.commissions[i].for_commission = Number(s_role.commissions[i].for_commission);
									s_role.commissions[i].rate = Number(s_role.commissions[i].rate);
									s_role.commissions[i].total = Number(s_role.commissions[i].total);
									totalCommission += Number(s_role.commissions[i].total);
									worker.value.commissions[k].for_commission = Number(s_role.commissions[i].for_commission);
									worker.value.commissions[k].rate = Number(s_role.commissions[i].rate);
									worker.value.commissions[k].total = Number(s_role.commissions[i].total);
									worker.value.total = angular.copy(totalCommission);
								});
								
								obj.data.push(worker);
							}
						});
					});
					
					obj.contract_type = contract_type;
					
					$rootScope.$broadcast(PayrollConfigs.events.UPDATE_PAYROLL_OF_REQUEST, obj);
					
					let funcSet = function () {
						return RequestServices.setPayrollInfo(obj);
					};
					
					$scope.setPayrollInfoPromise.push(funcSet());
					
					$q.all($scope.setPayrollInfoPromise).then(function (data) {
						$scope.busy = false;
						SweetAlert.swal('Payroll has been resubmitted', ' ', 'success');
						data = {
							payroll_done: true
						};
						let payrollFlag;
						let message = '';
						
						if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
							payrollFlag = Payroll_Done;
							message = 'Payroll was submited';
						} else if ($scope.request.service_type.raw == 5) {
							if ($scope.type == 'pickedUpCrew') {
								payrollFlag = pickupPayrollDone;
								message = 'Pickup payroll was submited';
							}
							if ($scope.type == 'deliveryCrew') {
								payrollFlag = deliveryPayrollDone;
								message = 'Delivery payroll was submited';
							}
						}
						
						let obj = {
							simpleText: message
						};
						
						_.forEach($scope.msgLogCommission, function (commission) {
							$scope.msgLog.push(commission);
						});
						
						_.forEach($scope.msgLogRate, function (commission) {
							$scope.msgLog.push(commission);
						});
						
						RequestServices.sendLogs($scope.msgLog, message, $scope.request.nid, 'MOVEREQUEST');
						
						$scope.editChanges = false;
						
						if (!$scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[payrollFlag])) {
							let new_field = importedFlags.company_flags[payrollFlag];
							
							if (_.isArray($scope.request.field_company_flags.value)) {
								$scope.request.field_company_flags.value = {};
							}
							
							$scope.request.field_company_flags.value[new_field] = payrollFlag;
							let field_flags = _.keys($scope.request.field_company_flags.value) || [];
							
							RequestServices.updateRequest($scope.request.nid, {
								field_company_flags: field_flags
							});
						}
					});
				});
				
				
			}
		}); //close  swal
	}
	
	function calcWorkerTotal(role) {
		$scope.workersTotal[role] = 0;
		
		_.forEach($scope[role], function (worker, k) {
			if (angular.isUndefined(worker.commissions)) {
				worker.commissions = [];
				
				_.forEach($scope.workers[role][$scope[role][k].uid].rates, function (rates, i) {
					let obj = {};
					obj.type = rates.type;
					obj.rate = rates.rate;
					obj.for_commission = 0;
					obj.total = 0;
					obj.id = i;
					worker.commissions.push(obj);
				});
			} else {
				_.forEach(worker.commissions, function (commission, n) {
					if (typeof commission.for_commission == 'object') {
						commission.for_commission = 0;
						commission.total = 0;
					}
					
					if (n == '10') {
						commission.rate = 100;
						commission.type = 1;
					}
					
					$scope.workersTotal[role] += commission.total;
				});
			}
			
			if (angular.isDefined(worker.custom_commission)) {
				_.forEach(worker.custom_commission, function (commission, n) {
					$scope.workersTotal[role] += Number(commission.amount);
				});
			}
		});
		
		$scope.workersTotal[role] = Number(_.round($scope.workersTotal[role], 2));
	}
	
	function removeWorker(index, role) {
		let currentWorker = $scope[role][index];
		let isCanRemove = !checkForemanHelperHasCustomPayment('remove', role, index);
		
		if (isCanRemove) {
			SweetAlert.swal({
				title: 'Are you sure?',
				text: 'This worker will be removed from request',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes, delete it!',
				closeOnConfirm: true
			}, function (isConfirm) {
				if (isConfirm) {
					if (currentWorker.new) {
						$scope.busy = true;
						
						unselectWorker(role, index);
						$scope[role].splice(index, 1);
						$scope.calcWorkerTotal(role);
						$scope.calcEmployeesTotal();
						SweetAlert.swal('Success!', 'Worker was removed from request', 'success');
						
						$scope.busy = false;
						$scope.addNewWorker = false;
					} else {
						let contract_type = contractTypes.PAYROLL;
						
						if ($scope.request.service_type.raw == 7) {
							contract_type = contractTypes.LONGDISTANCE;
						} else if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
							contract_type = contractTypes.PAYROLL;
						} else if ($scope.request.service_type.raw == 5) {
							if ($scope.type == 'pickedUpCrew') {
								contract_type = contractTypes.PICKUP_PAYROLL;
							} else if ($scope.type == 'deliveryCrew') {
								contract_type = contractTypes.DELIVERY_PAYROLL;
							}
						}
						
						$scope.busy = true;
						let data = {
							nid: $scope.request.nid, uid: [currentWorker.uid], contract_type: contract_type
						};
						
						RequestServices.removeUserPayroll(data).then(function (data) {
							if (_.isEqual(data[currentWorker.uid], 0)) {
								$scope.busy = false;
								toastr.error('Worker didn\'t removed from payroll', 'Error');
								return;
							}
							let msg = {
								simpleText: 'Worker ' + currentWorker.name + ' was removed from payroll of request'
							};
							
							let arr = [];
							arr.push(msg);
							RequestServices.sendLogs(arr, 'Request was cloned', $scope.request.nid, 'MOVEREQUEST');
							unselectWorker(role, index);
							
							$rootScope.$broadcast(PayrollConfigs.events.REMOVE_USER_FROM_PAYROLL_OF_REQUEST, {
								nid: $scope.request.nid, uid: currentWorker.uid
							});
							
							$scope[role].splice(index, 1);
							$scope.calcWorkerTotal(role);
							$scope.calcEmployeesTotal();
							$scope.busy = false;
							toastr.success('Worker was removed from request', 'Success');
						});
					}
				}
			});
		}
	}
	
	function unselectWorker(role, index) {
		let workerIndex = _.findIndex($scope.selected[role], function (o) {
			return o == $scope[role][index].uid;
		});
		
		if (workerIndex != -1) {
			$scope.selected[role].splice(workerIndex, 1);
		}
	}
	
	function isSelectedCommission(index, commission, role) {
		for (let i in $scope[role][index].commissions) {
			if ($scope[role][index].commissions[i].id == commission) {
				return true;
			}
		}
	}
	
	function getWorker(role, index) {
		if ($scope[role][index]) {
			return $scope[role][index].name;
		}
	}
	
	function changeForemanHelperCommissions(index, id, value, role, uid) {
		$scope.editChanges = true;
		let workerData = $scope[role][index];
		let workerCommission = angular.copy(workerData.commissions);
		
		if (workerCommission[id].id == 'new') {
			let msg = {
				text: 'Commission "' + $scope.enumerationTypesHelperForeman(value) + '" was added for ' + workerData.name
			};
			$scope.msgLog.push(msg);
		} else {
			let msg = {
				text: 'Commission for ' + workerData.name + ' was changed',
				from: $scope.enumerationTypesHelperForeman(workerCommission[id].old_id),
				to: $scope.enumerationTypesHelperForeman(value)
			};
			$scope.msgLog.push(msg);
		}
		workerCommission[id].id = value;
		workerCommission[id].old_id = value;
		if (value != '10') {
			workerCommission[id].for_commission = 0;
			workerCommission[id].total = 0;
			
			if (!workerData.sumFromTotal) {
				getTotalForWorker(workerData.uid).then(function (data) {
					workerData.sumFromTotal = _.head(data);
					workerCommission[id].for_commission = getSumFromTotal(workerData);
				});
			} else {
				workerCommission[id].for_commission = getSumFromTotal(workerData);
			}
			
			if (angular.isDefined($scope.workers[role][uid].rates[value])) {
				workerCommission[id].type = $scope.workers[role][uid].rates[value].type || 0;
				workerCommission[id].rate = $scope.workers[role][uid].rates[value].rate || 0;
			} else {
				workerCommission[id].rate = 0;
				workerCommission[id].type = $scope.commissionHelperForemanList[value].type;
			}
			
			if (role != 'salesPerson') {
				workerCommission[id].type = $scope.commissionHelperForemanList[value].type;
				if (value == '3') {
					workerCommission[id].rate = 100;
				}
				
				if (value == '4' || value == '11') {
					workerCommission[id].for_commission = laborTime;
					workerCommission[id].total = _.round((workerCommission[id].for_commission * workerCommission[id].rate), 2);
				}
				
			}
			if (role == 'salesPerson') {
				workerCommission[id].type = $scope.commissionManagerList[value].type;
			}
			
		} else {
			workerCommission[id].for_commission = 0;
			
			if (role != 'salesPerson') {
				workerCommission[id].type = $scope.commissionHelperForemanList[value].type;
			}
			
			if (role == 'salesPerson') {
				if (!workerData.sumFromTotal) {
					getTotalForWorker(workerData.uid).then(function (data) {
						workerData.sumFromTotal = _.head(data);
						workerCommission[id].for_commission = getSumFromTotal(workerData);
					});
				} else {
					workerCommission[id].for_commission = getSumFromTotal(workerData);
				}
				
				workerCommission[id].type = $scope.commissionManagerList[value].type;
			}
			
			workerCommission[id].rate = 100;
			workerCommission[id].total = 0;
		}
		
		changeCommission(index, value, id, role, workerCommission[id].type);
		$scope[role][index].commissions = angular.copy(workerCommission);
	}
	
	function getSumFromTotal(workerData) {
		let result = 0;
		
		if (_.isArray(workerData.sumFromTotal)) {
			workerData.sumFromTotal = _.head(workerData.sumFromTotal);
		}
		
		if (_.isString(workerData.sumFromTotal)) {
			result = workerData.sumFromTotal.replace(/,/g, '.');
		} else {
			result = workerData.sumFromTotal > 0 ? workerData.sumFromTotal : 0;
		}
		
		return result;
	}
	
	function addNewCommission(role, index, uid) {
		if ($scope[role][index].commissions.length > 0) {
			if (role != 'salesPerson') {
				if ($scope[role][index].commissions.length < $scope.commissionHelperForemanList.length) {
					let a = {};
					a.id = 'new';
					a.type = 0;
					a.rate = 0;
					a.for_commission = 0;
					a.total = 0;
					$scope[role][index].commissions.push(a);
					$scope.addNewCommissionItem = true;
				}
			} else {
				if ($scope[role][index].commissions.length < $scope.commissionManagerList.length) {
					let a = {};
					a.id = 'new';
					a.type = 0;
					a.rate = 0;
					a.for_commission = 0;
					a.total = 0;
					$scope[role][index].commissions.push(a);
					$scope.addNewCommissionItem = true;
				}
			}
		} else {
			$scope[role][index].commissions = [];
			let a = {};
			a.id = 'new';
			a.type = 0;
			a.rate = 0;
			a.for_commission = 0;
			a.total = 0;
			$scope[role][index].commissions.push(a);
			$scope[role][index].commissions[a] = {};
			$scope[role][index].commissions[a].type = 0;
			$scope[role][index].commissions[a].rate = 0;
			$scope[role][index].commissions[a].for_commission = 0;
			$scope[role][index].commissions[a].total = 0;
			$scope.addNewCommissionItem = true;
		}
	}
	
	function removeCommission(role, index, commission) {
		let obj = {
			text: 'Commission "' + $scope.enumerationTypesHelperForeman($scope[role][index].commissions[commission].id) + '" was removed for ' + $scope[role][index].name
		};
		$scope.msgLog.push(obj);
		$scope.editChanges = true;
		$scope[role][index].commissions.splice(commission, 1);
		
		$scope.calcEmployeesTotal();
		$scope.calcWorkerTotal(role);
	}
	
	function checkCommissions() {
		let roles = ['helper', 'foreman', 'salesPerson'];
		for (let i in roles) {
			let role = roles[i];
			for (let j in $scope[role]) {
				for (let k in $scope[role][j].commissions) {
					if (k == 'new') {
						return true;
					}
				}
			}
			if (roles[i].name == '' && roles[i].new == 1) {
				return true;
			}
		}
		
		return !!$scope.addNewWorker;
	}
	
	function revokePayroll() {
		SweetAlert.swal({
			title: 'Are you sure?',
			text: 'This payroll will be revoked',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes, delete it!',
			closeOnConfirm: false
		}, function (isConfirm) {
			let roles = ['helper', 'foreman', 'salesPerson'];
			let revokePromise = [];
			let revokeArr = [];
			let data = {
				nid: $scope.request.nid, uid: []
			};
			
			for (let role in roles) {
				for (let index in $scope[roles[role]]) {
					$scope.busy = true;
					data.uid.push($scope[roles[role]][index].uid);
					revokeArr.push({index: index, role: roles[role], uid: $scope[roles[role]][index].uid});
				}
			}
			
			RequestServices.removeUserPayroll(data).then(function (data) {
				for (let role in roles) {
					$scope[roles[role]] = _.drop($scope[roles[role]], $scope[roles[role]].length);
				}
				$scope.calcEmployeesTotal();
				$scope.busy = false;
				let message = 'Payroll revoked';
				let obj = {
					simpleText: message
				};
				let arr = [obj];
				RequestServices.sendLogs(arr, 'Request was updated', $scope.request.nid, 'MOVEREQUEST');
				let payrollFlag;
				if ($scope.request.service_type.raw != 5 || $scope.request.field_flat_rate_local_move.value) {
					payrollFlag = Payroll_Done;
				} else if ($scope.request.service_type.raw == 5) {
					if ($scope.type == 'pickedUpCrew') {
						payrollFlag = pickupPayrollDone;
					}
					if ($scope.type == 'deliveryCrew') {
						payrollFlag = deliveryPayrollDone;
					}
				}
				
				delete $scope.request.field_company_flags.value[importedFlags.company_flags[payrollFlag]];
				SweetAlert.swal('Deleted!', 'Payroll has been revoked', 'success');
			});
		});
	}
	
	function disableRate(role, i, j) {
		if (j == 'new') {
			return true;
		}
	}
	
	_.forEach($scope.roles, function (role) {
		_.forEach($scope[role], function (worker, index) {
			if(worker.id && _.isNumber(worker.id)) {
				worker.id = worker.id.toString();
			}

			worker.old_id = worker.id;

			_.forEach(worker.commissions, function (commission, j) {
				changeCommission(index, commission.id, j, role, commission.type);
			});
		});
		$scope.calcEmployeesTotal();
		$scope.calcWorkerTotal(role);
	});
	
	$scope.$on('custom.payroll', getCustomPayroll);

	function getCustomPayroll(_, id, payroll) {
		let customPayroll = payroll;
		customPayroll.id = id;
		let photo = {
			id: id,
			value: _.head(customPayroll.photo)
		};
		customPayroll.photo = [];
		customPayroll.photo.push(photo);
		_.forEach($scope.roles, function (name) {
			let ind = _.findIndex($scope[name], { uid: customPayroll.uid });
			if (ind != -1) {
				if (!$scope[name][ind].custom_commission) {
					$scope[name][ind].custom_commission = [];
				}
				$scope[name][ind].custom_commission.push(customPayroll);
				return false;
			}
		});
	}

	$uibModalInstance.rendered.then(() => {
		$scope.editChanges = false;
	});
}