import './templates/min-weight-modal.styl';

angular
	.module('move.requests')

	.factory('requestModals', requestModals)
	.directive('modalToggle', modalToggle);

function modalToggle(RequestServices, EditRequestServices, ParserServices) {
	return {
		restrict: 'A',
		scope: {
			'nid': '@nid',
		},
		link: function (scope, element) {

			let content = element.parent().parent().parent().find('.tab-wrappe');
			let backdrop = element.parent().parent().parent().parent().parent().parent();

			element.click(function () {
				content.slideToggle('fast');
				element.toggleClass('fa-chevron-down fa-chevron-up');
				backdrop.addClass('toggled');

				if ($(this).hasClass('fa-chevron-up')) {
					backdrop.addClass('toggled');
					$('body').removeClass('modal-open');
					EditRequestServices.repositionModals();
					$('.modalId_' + scope.nid + ' .reqheader').show();
				} else {
					backdrop.removeClass('toggled');
					$('body').addClass('modal-open');
					EditRequestServices.repositionModals();
					EditRequestServices.removeModalInstance(scope.nid);

					$('.modalId_' + scope.nid + ' .reqheader').hide();
				}


			});


		}
	};


}
requestModals.$inject = ['RequestServices','$uibModal','logger','config','SweetAlert', 'SendEmailsService'];

function requestModals(RequestServices, $uibModal, logger, config, SweetAlert, SendEmailsService) {

	var service = {};

	service.confirmUpdate = confirmUpdate;
	service.confirmUpdateInvoice = confirmUpdateInvoice;
	service.alertModal = alertModal;
	service.chooseOption = chooseOption;
	service.openCommentModal = openCommentModal;
	service.additionalDiscountModal = additionalDiscountModal;
	service.RateDiscountModal = RateDiscountModal;
	service.salaryCommisionModal = salaryCommisionModal;

	service.surchargeModal = surchargeModal;
	service.minWeightModal = minWeightModal;

	return service;

	// additionalDiscountModal
	function RateDiscountModal(request, message, editrequest) {

		$uibModal.open({
			template: require('./templates/rate-discount-modal.html'),
			controller: 'RateDiscountModalInstanceCtrl',
			resolve: {
				request: () => request,
				message: () => message,
				editrequest: () => editrequest,
			}
		});
	}


	// additionalDiscountModal
	function additionalDiscountModal(request, invoice, type, grandTotal, grandTotalInvoice) {

		var commentInstance = $uibModal.open({
			template: require('./templates/discount-modal.html'),
			controller: additionalDiscountModalInstanceCtrl,
			resolve: {
				request: function () {
					return request;
				},
				invoice: function () {
					return invoice;
				},
				type: function () {
					return type;
				},
				grandTotal: function () {
					return grandTotal;
				},
				grandTotalInvoice: function () {
					return grandTotalInvoice;
				}
			}
		});


	}

	function additionalDiscountModalInstanceCtrl($scope, $uibModalInstance, request, invoice, type, grandTotal, grandTotalInvoice, $rootScope, MoveCouponService, DispatchService) {

		$scope.request = request;
		if ($scope.request.request_all_data.couponData) {
			$scope.discountId = $scope.request.request_all_data.couponData.discountId;
		}
		$scope.invoice = invoice;
		$scope.type = type;
		$scope.grandTotal = grandTotal;
		$scope.grandTotalInvoice = grandTotalInvoice;
		$scope.change = false;

		var baseRequest = angular.copy(request);
		var baseInvoice = angular.copy(invoice);

		var importedFlags = $rootScope.availableFlags;
		var Job_Completed = 'Completed';
		$scope.Job_Completed = $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[Job_Completed]);

		$scope.openCouponModal = openCouponModal;
		$scope.removeCoupon = removeCoupon;

		$scope.Apply = function () {
			SweetAlert.swal({
					title: 'Are you sure you want to apply discount ?',
					text: '',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, let\'s do it!',
					cancelButtonText: 'No, cancel pls!',
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						if ($scope.type == 'invoice') {
							$scope.request.request_all_data.invoice = angular.copy($scope.invoice);
							var promise = RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
							promise.then(function (data) {
								var message = 'Discount was changed on closing tab. ';
								if ($scope.invoice.request_all_data.add_percent_discount != 0) {
									message += '% discount: ' + $scope.invoice.request_all_data.add_percent_discount + '%.';
								} else {
									message += 'Money discount: $' + $scope.invoice.request_all_data.add_money_discount + '.';
								}
								var obj = {
									simpleText: message
								};
								var arr = [obj];
								$rootScope.$broadcast('grandTotal.closing', {
									grand: $scope.grandTotal,
									logs: arr
								}, request.nid);
								toastr.success('Discount was applied!');
								$uibModalInstance.dismiss('cancel');
							});
						} else {
							if (angular.isDefined($scope.invoice)) {
								if (angular.isDefined($scope.invoice.request_all_data)) {
									$scope.invoice.request_all_data.add_percent_discount = $scope.request.request_all_data.add_percent_discount;
									$scope.invoice.request_all_data.add_money_discount = $scope.request.request_all_data.add_money_discount;
									$scope.request.request_all_data.invoice = angular.copy($scope.invoice);
									$rootScope.$broadcast('grandTotal.closing', {
										grand: $scope.grandTotalInvoice,
										logs: []
									}, request.nid);
								}
							}
							RequestServices.saveReqData(request.nid, request.request_all_data).then(function () {
								var message = 'Discount was changed on sales tab. ';
								if ($scope.request.request_all_data.add_percent_discount != 0) {
									message += '% discount: ' + $scope.request.request_all_data.add_percent_discount + '%.';
								} else {
									message += 'Money discount: $' + $scope.request.request_all_data.add_money_discount + '.';
								}
								var obj = {
									simpleText: message
								};
								var arr = [obj];
								$rootScope.$broadcast('grandTotal.sales', {
									grand: $scope.grandTotal,
									logs: arr
								}, $scope.request.nid);
								toastr.success('Discount was applied!');
								$uibModalInstance.dismiss('cancel');
								SweetAlert.close();
							});

						}
					}
					else {
						$scope.request.request_all_data.add_percent_discount = 0;
						$scope.request.request_all_data.add_money_discount = 0;
					}
				});


		};


		$scope.changeDiscount = function (type) {
			$scope.change = true;

			if (_.isEqual($scope.type, 'invoice')) {
				if (type == 'money') {
					$scope.invoice.request_all_data.add_percent_discount = 0;
				} else {
					$scope.invoice.request_all_data.add_money_discount = 0;
				}
			} else {
				if (type == 'money') {
					$scope.request.request_all_data.add_percent_discount = 0;
				} else {
					$scope.request.request_all_data.add_money_discount = 0;
				}
			}
		};

		$scope.cancel = function () {
			$scope.request.request_all_data = angular.copy(baseRequest.request_all_data);
			$scope.invoice.request_all_data = angular.copy(baseInvoice.request_all_data);
			$uibModalInstance.dismiss('cancel');
		};

		function saveCouponToContract() {
			DispatchService.getRequestContract($scope.request.nid).then(function (contract) {
				var data = contract;
				if (contract.factory && contract.finance) {
					data.factory.discountType = 'website';
					data.factory.discountUsed = true;
					data.finance.discount = parseFloat($scope.coupon.discount);
					DispatchService.setRequestContract(data, $scope.request.nid).then(function () {
					}, function (error) {
						SweetAlert.swal('Error', error, 'error');
					});
				}
				$scope.request.request_all_data.couponData = {};
				$scope.request.request_all_data.couponData.discount = $scope.coupon.discount;
				$scope.request.request_all_data.couponData.discountType = 'website';
				$scope.request.request_all_data.couponData.discountId = $scope.coupon.id;
				RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data)
					.then(() => {
						request = $scope.request;
						invoice = $scope.invoice;
						SweetAlert.swal('Coupon has been successfully used', '', 'success');
						$rootScope.$broadcast('apply.coupon', {
							discount: $scope.coupon.discount,
							type: 'website',
							id: $scope.coupon.id
						});
					})
					.catch(() => {
						SweetAlert.swal(`Coupon hasn't been used`, '', 'error');
						removeCoupon();
					});
				$uibModalInstance.dismiss('cancel');
			}, function (error) {
				SweetAlert.swal('Error', error, 'error');
			});
		}

		function removeCouponFromContract() {
			DispatchService.getRequestContract($scope.request.nid).then(function (contract) {
				$scope.busy = false;
				var data = contract;
				if (contract.factory && contract.finance) {
					data.factory.discountType = '';
					data.factory.discountUsed = false;
					data.finance.discount = 0;
					DispatchService.setRequestContract(data, $scope.request.nid).then(function () {
						delete $scope.request.request_all_data.couponData;
						RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
						$rootScope.$broadcast('apply.coupon', {discount: '', type: '', id: ''});
						toastr.success('Success', 'Coupon has been successfully removed');
						request = $scope.request;
						invoice = $scope.invoice;
						$uibModalInstance.dismiss('cancel');
					}, function (error) {
						SweetAlert.swal('Error', error, 'error');
					});
				} else {
					delete $scope.request.request_all_data.couponData;
					RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
					$rootScope.$broadcast('apply.coupon', {discount: '', type: '', id: ''});
					toastr.success('Success', 'Coupon has been successfully removed');
					request = $scope.request;
					invoice = $scope.invoice;
					$uibModalInstance.dismiss('cancel');
				}
			});
		}

		function openCouponModal() {
			SweetAlert.swal({
					title: 'Enter promo code of your coupon',
					text: '',
					type: 'input',
					showCancelButton: true,
					closeOnConfirm: false,
					confirmButtonText: 'Use coupon',
					animation: 'slide-from-top',
					inputPlaceholder: 'Promo code',
					inputType: 'text',
					showLoaderOnConfirm: true
				},
				function (inputValue) {
					if (inputValue === false) {
						return false;
					}
					if (inputValue === '') {
						swal.showInputError('You need to write something!');
						return false;
					}
					$scope.promoCode = inputValue;
					MoveCouponService.getCouponByPromo($scope.promoCode).then(
						function (data) {
							if (_.isEmpty(data)) {
								swal.showInputError('Error', 'This promo code doesn\'t exist', 'error');
								return false;
							}
							$scope.coupon = _.head(data);
							if (!data || !_.head(data)) {
								swal.showInputError('Error', 'Coupon was already used', 'error');
								return false;
							}
							if ($scope.coupon.used == 1) {
								swal.showInputError('Error', 'Coupon was already used', 'error');
								return false;
							} else {
								$scope.coupon.used = 1;
								MoveCouponService.updateUserCoupon($scope.coupon, $scope.coupon.id).then(
									function () {
										saveCouponToContract();
									},
									function (error) {
										SweetAlert.swal('Error', error, 'error');
									}
								);
							}
						},
						function (error) {
							swal.showInputError('Error', 'This promo code doesn\'t exist', 'error');
						}
					);
				}
			);
		}

		function removeCoupon() {
			$scope.busy = true;
			MoveCouponService.getUserCoupon($scope.request.request_all_data.couponData.discountId).then(
				function (data) {
					if (!data) {
						SweetAlert.swal('Error', '', 'error');
						return false;
					}
					var coupon = data;
					coupon.used = 0;
					MoveCouponService.updateUserCoupon(coupon, coupon.id).then(
						function () {
							removeCouponFromContract();
						},
						function (error) {
							$scope.busy = false;
							SweetAlert.swal('Error', error, 'error');
						}
					);
				}
			);

		}

	}


	//Comment MODAL
	function openCommentModal(nid) {

		var commentInstance = $uibModal.open({
			template: require('./templates/comment-modal.html'),
			controller: CommentInstanceCtrl,
			windowClass: 'commentQuickModal',
			resolve: {
				nid: function () {
					return nid;
				}
			},
		});


	}

	function CommentInstanceCtrl($scope, $uibModalInstance, nid) {
		$scope.nid = nid;
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};

	}

	//ALERT MODAL
	function alertModal(reason) {

		var alertInstance = $uibModal.open({
			template: require('./templates/alert-modal.html'),
			controller: AlertInstanceCtrl,
			resolve: {
				reason: function () {
					return reason;
				}
			},
		});


	}

	function AlertInstanceCtrl($scope, $uibModalInstance, reason) {

		$scope.reason = reason;


		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};


	}

	//CONFIRM MODAL INVOICE
	function confirmUpdateInvoice(request,message,fields) {
		var modalInstance = $uibModal.open({
			template: require('./templates/update-confirm.html'),
			controller: ModalInstanceInvoiceCtrl,
			windowClass: 'visible-overflow',
			resolve: {
				request: function () {
					return request;
				},
				fields: function () {
					return fields;
				},
				message: function () {
					return message;
				}
			}
		});

		return modalInstance.result;
	}

	function ModalInstanceInvoiceCtrl($scope, $uibModalInstance, fields, request, message, $rootScope) {
		$scope.request = request;

		$scope.messages = [];
		$scope.fields = fields;
		angular.forEach(message, function (value, key) {
			$scope.messages.push(value);
		});

		$scope.update = function () {
			$scope.busy = true;
			var nid = request.nid;
			//  if(angular.isDefined($scope.fields.invoice.field_move_service_type)){
			//     if($scope.field_move_service_type == 1 && request.storage_id){
			//         var edit= {field_approve: 12}
			//         RequestServices.updateRequest(request.storage_id,edit);
			//     }
			// }
			_.forEach($scope.fields.invoice, function (item) {
				if (item) {
					if (item.old) {
						item.old = angular.copy(item.value);
					}
				}
			});
			if ($scope.fields.invoice.field_double_travel_time) {
				$scope.fields.invoice.field_double_travel_time.old = angular.copy($scope.fields.invoice.field_double_travel_time.value);
			}

			request.request_all_data.invoice = $scope.fields.invoice;
			$scope.busy = true;
			if (angular.isDefined($scope.fields.invoice.field_approve)) {
				if ($scope.fields.invoice.field_approve != request.status.raw) {
					request.status.raw = $scope.fields.invoice.field_approve;
				}
			}
			if ($scope.fields.invoice) {
				request.request_all_data.invoice = angular.copy($scope.fields.invoice);
			}
			var arr = [];
			_.forEach($scope.messages, function (message) {
				var msg = {
					from: '',
					to: '',
					text: ''
				};
				if (angular.isUndefined(message.oldValue) && message.label != 'custom') {
					msg.text = message.label;
					msg.to = message.newValue;
				}
				if (message.oldValue) {
					if (!message.oldValue.length && message.label != 'custom') {
						msg.text = message.label;
						msg.to = message.newValue;
					} else if (message.oldValue.length && message.label != 'custom') {
						msg.text = message.label;
						msg.to = message.newValue;
						msg.from = message.oldValue;
					}
				}
				if (message.label == 'custom') {
					msg.text = message.newValue;
				}
				arr.push(msg);
			});

			if (_.has($scope.fields.invoice, 'field_flat_rate_local_move')) {
				let fieldLocal = {
					field_flat_rate_local_move: $scope.fields.invoice.field_flat_rate_local_move
				};
				var promise = RequestServices.updateRequest(nid, fieldLocal);

				promise.then(function (data) {
					$rootScope.$broadcast('localMove.closing.saved');
				}, function (reason) {
					$scope.busy = false;
					let errorLogs = [];
					let error = '<b>Has some errors:</b>';
					_.each(reason.validation_errors, function (validError) {
						let msg = {
							text: validError
						};
						error += `<br/>${validError}`;
						errorLogs.push(msg);
					});
					SweetAlert.swal({
						title: 'Request was not saved.',
						text: error,
						type: 'error',
						html: true
					});
					RequestServices.sendLogs(errorLogs, 'Request was not updated', $scope.request.nid, 'MOVEREQUEST');
				});
			}
			// $rootScope.$broadcast('request.updated',request);
			$rootScope.$broadcast('request.sendLog.closing', {logs: arr}, $scope.request.nid);
			$scope.fields = {};
			logger.success('Request was updated', 'Success');
			$uibModalInstance.close({type: 'update'});
		};


		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};


	}

	function confirmUpdate(request, message, fields, invoice) {
		if (request.emailParameters) {
			request.emailParameters.status = fields.field_approve;
			request.emailParameters.serviceType = request.service_type.raw;
			request.emailParameters.hideSelectEmail = true;
		}

		if (_.isUndefined(request.sendingTemplates)) {
			request.sendingTemplates = [];
		}

		var modalInstance = $uibModal.open({
			template: require('./templates/update-confirm.html'),
			controller: ModalInstanceCtrl,
			windowClass: 'visible-overflow',
			resolve: {
				request: function () {
					return request;
				},
				fields: function () {
					return fields;
				},
				message: function () {
					return message;
				},
				invoice: function () {
					return invoice;
				}
			}
		});

		modalInstance.result.finally(() => {
			request.emailParameters.hideSelectEmail = false;
		});
	}



	function ModalInstanceCtrl($scope, $uibModalInstance, fields, request, message, invoice, $rootScope, RequestServices, StorageService, EditRequestServices, ParserServices, FuelSurchargeService, common, requestObservableService, AuthenticationService) {
		$scope.request = request;
		$scope.messages = [];
		$scope.fields = fields;
		$scope.invoice = invoice;
		angular.forEach(message, function (value, key) {
			$scope.messages.push(value);
		});

		function prepareRequestAllData() {
			_.forEach($scope.invoice, function (item) {
				if (item) {
					if (item.old) {
						item.old = angular.copy(item.value);
					}
				}
			});
			if ($scope.invoice.field_double_travel_time) {
				$scope.invoice.field_double_travel_time.old = angular.copy($scope.invoice.field_double_travel_time.value);
			}

			$scope.request.request_all_data.invoice = angular.copy($scope.invoice);
		}

		function saveRequest(data) {
			ParserServices.saveEntrance($scope.request, $scope.fields);
			$scope.busy = true;
			if (angular.isDefined($scope.fields.field_approve)) {
				if ($scope.fields.field_approve != request.status.raw) {
					request.status.raw = $scope.fields.field_approve;
				}
			}
			// Update Original Request
			$rootScope.$broadcast('request.updated', request);
			if (request.sendingTemplates.length) {
				SendEmailsService.sendEmails(request.nid, request.sendingTemplates);
			}
			$scope.fields = [];
			logger.success('Request was updated', data, 'Success');
		}

		$scope.update = function () {

			//update Request
			//Busy
			$scope.busy = true;
			var nid = request.nid;

			//Check if service type change. and if it's moving and storage to moving , make anther requerst expiered
			if (angular.isDefined($scope.fields.field_move_service_type)) {
				if ($scope.field_move_service_type == 1 && request.storage_id) {
					var edit = {field_approve: 12};
					RequestServices.updateRequest(request.storage_id, edit);
				}
			}

			if (!_.isUndefined($scope.fields.field_approve)
				&& $scope.fields.field_approve == 3) {

				EditRequestServices.saveBookedStatistics($scope.request);
			}

			if (!_.isUndefined($scope.fields.notes)) {
				$rootScope.$broadcast('notes.save');
				delete $scope.fields.notes;
			}

			if (!_.isUndefined($scope.fields.trucksCount)) {
				delete $scope.fields.trucksCount;
			}

			if (!_.isUndefined($scope.fields.moversCount)) {
				delete $scope.fields.moversCount;
			}

			_.forEachRight($scope.fields, function (item, key) {
				if (_.isObject(item)
					&& _.isEmpty(item)
					&& !_.isArray(item)) {

					delete $scope.fields[key];
				}
			});

			if (!_.isEmpty($scope.fields)) {
				if (!_.isUndefined($scope.fields.field_minimum_move_time) && !_.isNumber($scope.fields.field_minimum_move_time)) {
					$scope.fields.field_minimum_move_time = _.round(common.convertStingToIntTime($scope.fields.field_minimum_move_time), 2);
				}

				if (!_.isUndefined($scope.fields.field_maximum_move_time) && !_.isNumber($scope.fields.field_maximum_move_time)) {
					$scope.fields.field_maximum_move_time = _.round(common.convertStingToIntTime($scope.fields.field_maximum_move_time), 2);
				}

				if (!_.isUndefined($scope.fields.field_travel_time) && !_.isNumber($scope.fields.field_travel_time)) {
					$scope.fields.field_travel_time = _.round(common.convertStingToIntTime($scope.fields.field_travel_time), 2);
				}
				prepareRequestAllData();
				$scope.fields.all_data = $scope.request.request_all_data;
				AuthenticationService.checkSession()
					.then(() => {
						updateRequestByAuthorizedUser(nid);
					}, () => {
						$scope.busy = false;
					});

			} else {
				saveRequest();
				$uibModalInstance.dismiss('cancel');
			}
			//busy false
		};

		function updateRequestByAuthorizedUser (nid) {
			RequestServices.updateRequest(nid, $scope.fields)
				.then(data => {
					saveRequest(data);
					EditRequestServices.sendLogsAfterSaveChanges($scope.messages, $scope.request);
					if (!_.isUndefined(fields.field_to_storage_temp)) {
						delete fields.field_to_storage_temp;
					}
					requestObservableService.clearChanges();
					$uibModalInstance.dismiss('cancel');
				}, reason => {
					$scope.busy = false;
					let errorLogs = [];
					let error = '<b>Has some errors:</b>';
					_.forEach(_.get(reason, 'validation_errors', []), validError => {
						let msg = {
							text: validError
						};
						error += `<br/>${validError}`;
						errorLogs.push(msg);
					});
					SweetAlert.swal({
						title: 'Request was not saved.',
						text: error,
						type: 'error',
						html: true
					});
					RequestServices.sendLogs(errorLogs, 'Request was not updated', $scope.request.nid, 'MOVEREQUEST');
				});
		}

		$scope.$on('storage.request.created', function (ev, data) {
			$scope.request.request_all_data.storage_request_id = data;
			RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
		});

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};

	}


	//CONFIRM MODAL
	function chooseOption(request, option) {

		var modalOptionInstance = $uibModal.open({
			template: require('./templates/update-confirm.html'),
			controller: modalOptionInstance,
			windowClass: 'visible-overflow',
			resolve: {
				request: function () {
					return request;
				},
				option: function () {
					return option;
				}
			},
		});

		modalOptionInstance.result.then(function ($scope) {

			//


		});


	}

	function modalOptionInstance($scope, $uibModalInstance, option, request, $rootScope) {

		$scope.messages = [];
		$scope.request = request;
		$scope.option = option;
		$scope.update = function () {
			$scope.busy = true;
			var nid = $scope.request.nid;
			var data = {
				'options': option,
				'requestId': nid
			};

			var promise = RequestServices.sendOption(data);
			promise.then(function (data) {
				$scope.busy = false;
				$rootScope.$broadcast('option.choosed', $scope.option);
				$uibModalInstance.dismiss('cancel');

			}, function (reason) {

				logger.error(reason, reason, 'Error');

			});

		};
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};


	}

	// Salary&Commission
	function salaryCommisionModal(data, request, info, invoice, grandTotal, type) {
		var salaryCommisionInstance = $uibModal.open({
			template: require('./templates/salaryCommission.html'),
			controller: 'salaryCommissionInstanceCtrl',
			size: 'lg',
			backdrop: 'static',
			keyboard: false,
			resolve: {
				request: function () {
					return [request, data, info, invoice, grandTotal];
				},
				type: function() {
					return type;
				}
			}
		});
	}

	function surchargeModal(quote, request, invoice, type, grandTotal, grandTotalInvoice) {
		$uibModal.open({
			template: require('./templates/surcharge-modal.html'),
			controller: surchargeModalInstanceCtrl,
			resolve: {
				request: function () {
					return request;
				},
				invoice: function () {
					return invoice;
				},
				quote: function () {
					return quote;
				},
				type: function () {
					return type;
				},
				grandTotal: function () {
					return grandTotal;
				},
				grandTotalInvoice: function () {
					return grandTotalInvoice;
				}
			}
		});
	}

	function surchargeModalInstanceCtrl($scope, $uibModalInstance, quote, request, invoice, type, grandTotal, grandTotalInvoice, RequestServices, $rootScope) {
		var TYPE_INVOICE = 'invoice';
		var isInvoice = _.isEqual(type, TYPE_INVOICE);
		$scope.request = _.clone(request);
		$scope.invoice = invoice;
		$scope.quote = quote;
		$scope.type = type;
		$scope.grandTotal = grandTotal;
		$scope.grandTotalInvoice = grandTotalInvoice;
		$scope.baseFuel = {};

		var importedFlags = $rootScope.availableFlags;
		var Job_Completed = 'Completed';
		$scope.Job_Completed = $scope.request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[Job_Completed]);

		var isNotSetFuelSurchargeByAdmin = _.isEqual(type, TYPE_INVOICE)
			? _.isUndefined($scope.invoice.request_all_data.isFuelSuchrargeChangeByAdmin)
			: _.isUndefined($scope.request.request_all_data.isFuelSuchrargeChangeByAdmin);

		if (isInvoice) {
			$scope.baseFuel.surcharge_fuel_avg = $scope.invoice.request_all_data.surcharge_fuel_avg;
			$scope.baseFuel.surcharge_fuel = $scope.invoice.request_all_data.surcharge_fuel;
			$scope.baseFuel.surcharge_fuel_perc = $scope.invoice.request_all_data.surcharge_fuel_perc;
		} else {
			$scope.baseFuel.surcharge_fuel_avg = $scope.request.request_all_data.surcharge_fuel_avg;
			$scope.baseFuel.surcharge_fuel = $scope.request.request_all_data.surcharge_fuel;
			$scope.baseFuel.surcharge_fuel_perc = $scope.request.request_all_data.surcharge_fuel_perc;
		}

		$scope.changeSurcharge = function (type, input) {
			$scope.change = true;
			if (isInvoice) {
				if (input == 'perc') {
					$scope.invoice.request_all_data.surcharge_fuel_avg = 0;
					$scope.invoice.request_all_data.surcharge_fuel = $scope.invoice.request_all_data.surcharge_fuel_perc;
				}
				else if (input == 'avg') {
					$scope.invoice.request_all_data.surcharge_fuel_perc = 0;
					$scope.invoice.request_all_data.surcharge_fuel = $scope.quote * ($scope.invoice.request_all_data.surcharge_fuel_avg / 100);
				} else if (input == 'total') {
					$scope.invoice.request_all_data.surcharge_fuel_perc = '';
					$scope.invoice.request_all_data.surcharge_fuel_avg = '';
				}
			} else if (type == 'request') {
				if (input == 'perc') {
					$scope.request.request_all_data.surcharge_fuel_avg = 0;
					$scope.request.request_all_data.surcharge_fuel = $scope.request.request_all_data.surcharge_fuel_perc;
				}
				else if (input == 'avg') {
					$scope.request.request_all_data.surcharge_fuel_perc = 0;
					$scope.request.request_all_data.surcharge_fuel = $scope.request.request_all_data.surcharge_fuel_perc;
					$scope.request.request_all_data.surcharge_fuel = $scope.quote * ($scope.request.request_all_data.surcharge_fuel_avg / 100);
				} else if (input == 'total') {
					$scope.request.request_all_data.surcharge_fuel_perc = '';
					$scope.request.request_all_data.surcharge_fuel_avg = '';
				}
			}
		};

		$scope.Apply = function () {
			if (isInvoice) {
				$scope.invoice.request_all_data.surcharge_fuel = _.round($scope.invoice.request_all_data.surcharge_fuel, 2);
				$scope.invoice.request_all_data.custom_fuel_surcharge = true;
				$scope.invoice.request_all_data.isFuelSuchrargeChangeByAdmin = true;
				$scope.request.request_all_data.invoice = angular.copy($scope.invoice);
				var promise = RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data);
				promise.then(function (data) {
					var message = 'Fuel Surcharge was changed on closing tab';
					var obj = {
						text: message,
						from: '$' + $scope.baseFuel.surcharge_fuel,
						to: '$' + $scope.invoice.request_all_data.surcharge_fuel
					};
					var arr = [obj];
					$rootScope.$broadcast('grandTotal.closing', {grand: $scope.grandTotal, logs: arr}, request.nid);
					toastr.success('Fuel Surcharge was applied!');
					$uibModalInstance.dismiss('cancel');
				});
			} else {
				if (angular.isDefined($scope.request.request_all_data.invoice)
					&& angular.isDefined($scope.invoice)) {

					if (angular.isDefined($scope.request.request_all_data.invoice.request_all_data)) {
						$scope.invoice.request_all_data.isFuelSuchrargeChangeByAdmin = true;
						$scope.invoice.request_all_data.surcharge_fuel_avg = $scope.request.request_all_data.surcharge_fuel_avg;
						$scope.invoice.request_all_data.surcharge_fuel = $scope.request.request_all_data.surcharge_fuel;
						$scope.invoice.request_all_data.surcharge_fuel_perc = $scope.request.request_all_data.surcharge_fuel_perc;
						$scope.invoice.request_all_data.surcharge_fuel = _.round($scope.invoice.request_all_data.surcharge_fuel, 2);
						$scope.request.request_all_data.surcharge_fuel = _.round($scope.request.request_all_data.surcharge_fuel, 2);
						$scope.request.request_all_data.invoice = angular.copy($scope.invoice);
						$rootScope.$broadcast('grandTotal.closing', {
							grand: $scope.grandTotalInvoice,
							logs: []
						}, request.nid);
					}
				}

				$scope.request.request_all_data.isFuelSuchrargeChangeByAdmin = true;
				$scope.request.request_all_data.custom_fuel_surcharge = !!$scope.change;
				RequestServices.saveReqData($scope.request.nid, $scope.request.request_all_data).then(function (data) {
					var message = 'Fuel Surcharge was changed on sales tab';
					let from = '$' + $scope.baseFuel.surcharge_fuel;
					let to = '';
					if ($scope.request.request_all_data.surcharge_fuel_avg == 0) {
						to += '$' + $scope.request.request_all_data.surcharge_fuel;
					} else {
						to += ' (' + $scope.request.request_all_data.surcharge_fuel_avg + '%; total: $' + $scope.request.request_all_data.surcharge_fuel + ').';
					}
					var obj = {
						text: message,
						from: from,
						to: to
					};
					var arr = [obj];
					$rootScope.$broadcast('grandTotal.sales', {
						grand: $scope.grandTotal,
						logs: arr
					}, $scope.request.nid);
					toastr.success('Fuel Surcharge was applied!');
					$uibModalInstance.dismiss('cancel');
				});
			}
		};

		$scope.cancel = function () {
			if (isNotSetFuelSurchargeByAdmin) {
				if (isInvoice) {
					delete $scope.invoice.request_all_data.isFuelSuchrargeChangeByAdmin;
				} else {
					delete $scope.request.request_all_data.isFuelSuchrargeChangeByAdmin;
				}
			}

			if (isInvoice) {
				$scope.invoice.request_all_data.surcharge_fuel_avg = $scope.baseFuel.surcharge_fuel_avg;
				$scope.invoice.request_all_data.surcharge_fuel = $scope.baseFuel.surcharge_fuel;
				$scope.invoice.request_all_data.surcharge_fuel_perc = $scope.baseFuel.surcharge_fuel_perc;
			} else {
				$scope.request.request_all_data.surcharge_fuel_avg = $scope.baseFuel.surcharge_fuel_avg;
				$scope.request.request_all_data.surcharge_fuel = $scope.baseFuel.surcharge_fuel;
				$scope.request.request_all_data.surcharge_fuel_perc = $scope.baseFuel.surcharge_fuel_perc;
			}

			$uibModalInstance.dismiss('cancel');
		};

	}

	function minWeightModal(request, invoice, min_weight, min_price, min_price_enabled) {
		$uibModal.open({
			template: require('./templates/min-weight-modal.html'),
			controller: minWeightModalInstanceCtrl,
			resolve: {
				request: function () {
					return request;
				},
				invoice: function () {
					return invoice;
				},
				min_weight: function () {
					return min_weight;
				},
				min_price: function () {
					return min_price;
				},
				min_price_enabled: function () {
					return min_price_enabled;
				}
			}
		});

	}

	function minWeightModalInstanceCtrl($scope, $rootScope, $uibModalInstance, request, invoice, min_weight, min_price, min_price_enabled, RequestServices) {
		$scope.min_weight = _.clone(min_weight);
		$scope.min_price = _.clone(min_price);
		$scope.min_price_enabled = _.clone(min_price_enabled);


		$scope.Apply = function () {
			var arr = [];
			if (request.request_all_data.min_weight != $scope.min_weight) {
				arr.push({
					text: 'Minimum Volume was changed',
					from: (request.request_all_data.min_weight || 0) + 'c.f.',
					to: $scope.min_weight + 'c.f.'
				});
			}
			if (request.request_all_data.min_price != $scope.min_price) {
				arr.push({
					text: 'Minimum Price was changed',
					from: '$' + (request.request_all_data.min_price || 0),
					to: '$' + $scope.min_price
				});
			}
			if (request.request_all_data.min_price_enabled != $scope.min_price_enabled) {
				arr.push({
					text: 'Minimum Price was turned ' + ($scope.min_price_enabled ? 'on' : 'off')
				});
			}
			RequestServices.sendLogs(arr, 'Request was updated', request.nid, 'MOVEREQUEST');
			let data = {
				min_weight: $scope.min_weight,
				weightChanged: request.request_all_data.min_weight != $scope.min_weight
			};
			request.request_all_data.min_weight_old = request.request_all_data.min_weight;
			request.request_all_data.min_weight = $scope.min_weight;
			request.request_all_data.min_price = $scope.min_price;
			request.request_all_data.min_price_enabled = $scope.min_price_enabled;
			if (invoice) {
				invoice.request_all_data.min_weight = $scope.min_weight;
				invoice.request_all_data.min_price = $scope.min_price;
				invoice.request_all_data.min_price_enabled = $scope.min_price_enabled;

			}
			RequestServices.saveReqData(request.nid, request.request_all_data);

			$rootScope.$broadcast('min_weight', data);
			$uibModalInstance.dismiss('cancel');
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};

	}


}
