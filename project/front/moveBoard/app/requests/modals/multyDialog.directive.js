(function () {
  
    angular
        .module('move.requests')
        .directive('modalToggle', modalToggle)
    
  
    function modalToggle($rootScope,EditRequestServices) {
    return {
        restrict: 'A',
        scope: {
            'nid': '=nid',            
        },
        link: function(scope, element) {
            
    
        element.click(function() {
                
                
                var content =  $(this).parent().parent().parent().find('.tab-wrappe');
                content.slideToggle("fast"), $(this).toggleClass("fa-chevron-down fa-chevron-up");
                var backdrop = $(this).parent().parent().parent().parent().parent().addClass("toggled");
                var modal = $(this).parent().parent().parent().parent().parent().addClass("toggled");
              
    
                
                if( $(this).hasClass("fa-chevron-up")){

                        backdrop.addClass("toggled");
                        $("body").removeClass("modal-open");
                        EditRequestServices.repositionModals();
               
                }
                else {

                        backdrop.removeClass("toggled");
                        $("body").addClass("modal-open");
                        EditRequestServices.repositionModals();
                }

            
            
            });
            
        }
    }

    
    };
    
});
    