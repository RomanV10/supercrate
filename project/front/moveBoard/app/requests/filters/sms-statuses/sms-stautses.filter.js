angular.module('move.requests')
	.filter('smsStatusesFilter', smsStatusesFilter);

/* @ngInject */
function smsStatusesFilter() {
	return function (status) {
		const statusNames = {
			queued: 'in process',
		};

		let result = statusNames[status] || status;

		return result;
	};
}