'use strict';

angular
	.module('move.requests')
	.directive('customScroll', customScroll);

function customScroll() {
	return {
		restrict: 'A',
		link: function postLink(scope, element, attr) {
			const MIN_WINDOW_SIZE = 1024;

			if (window.innerWidth <= MIN_WINDOW_SIZE) {
				let asix = attr.asix || 'x';
				
				element.mCustomScrollbar({
					asix,
					theme: 'light',
					setWidth: '100%',
					advanced: {
						autoExpandHorizontalScroll: true
					}
				});
			}
		}
	};
}
