'use strict';

angular
	.module('move.requests',
		[
			'bootstrapLightbox',
			'app.tplBuilder',
			'app.calculator',
			'AngularPrint',
			'leadScore',
			'request-show-quote',
			'grand-total',
			'browser',
			'additionalServices'
		]);
