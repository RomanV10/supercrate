'use strict';

import {
	LABELS_FOR_MESSAGES,
	LABELS_FOR_MESSAGES_INVOICE,
	VARIABLE_NAMES
} from './form-inputs.services.commons';

angular
	.module('move.requests')
	.factory('formInputsServices', formInputsServices);

function formInputsServices(datacontext, $q, SweetAlert) {
	const fieldData = datacontext.getFieldData();
	const calendartype = fieldData.calendartype;
	const calendar = fieldData.calendar;
	const DATE_FORMAT_CALENDAR = 'YYYY-MM-DD';
	const DATE_FORMAT_DEFAULT = 'MMMM D, YYYY';
	const DATE_FORMAT_EDITREQUEST = 'YYYY-MM-DD';
	const DATE_FORMAT_FIELD = 'MM/DD/YYYY';
	const CONFIRMED_STATUS = 3;
	
	let formInputsServices = {
		DATE_FORMAT_CALENDAR: DATE_FORMAT_CALENDAR,
		DATE_FORMAT_DEFAULT: DATE_FORMAT_DEFAULT,
		DATE_FORMAT_EDITREQUEST: DATE_FORMAT_EDITREQUEST,
		DATE_FORMAT_FIELD: DATE_FORMAT_FIELD,
		getDayClass: getDayClass,
		findFirstUnblockedDay: findFirstUnblockedDay,
		askForChangeDeliveryDate: askForChangeDeliveryDate,
		changeAndSaveSimpleDateFieldClosing: changeAndSaveSimpleDateFieldClosing,
		changeAndSaveSimpleDateFieldSales: changeAndSaveSimpleDateFieldSales,
	};
	
	return formInputsServices;
	
	function askForChangeDeliveryDate(pickupDate, request) {
		let defer = $q.defer();
		let needToChangeDeliveryDate = request.inventory.move_details.delivery
			&& pickupDate.isAfter(request.inventory.move_details.delivery);
		if (needToChangeDeliveryDate) {
			showChangeDeliveryDayConfirm();
		} else {
			defer.resolve();
		}
		return defer.promise;
		
		function showChangeDeliveryDayConfirm() {
			SweetAlert.swal(
				{
					title: 'Delivery date cannot be greater than first available delivery date',
					text: 'Do you want to change the first available delivery date?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55', confirmButtonText: 'Yes, let\'s do it!',
					cancelButtonText: 'No, cancel pls!',
					closeOnConfirm: true,
					closeOnCancel: true
				},
				isConfirm => {
					if (isConfirm) {
						defer.resolve();
					} else {
						defer.reject();
					}
				});
		}
	}
	
	function changeAndSaveSimpleDateFieldSales($scope, newVal, FIELD_NAME, callback) {
		const REQUEST_FIELD_NAME = VARIABLE_NAMES[FIELD_NAME];
		let momentNewValue = moment(newVal, DATE_FORMAT_DEFAULT);
		
		writeChangesToRequest(REQUEST_FIELD_NAME, $scope.request, momentNewValue);
		
		changeFieldInEditrequest($scope.editrequest, FIELD_NAME, {
			date: momentNewValue.format(DATE_FORMAT_EDITREQUEST),
			time: '15:10:00',
		});
		
		let momentOldValueOnField = moment(_.get($scope.request, REQUEST_FIELD_NAME + '.old', ''));
		addFieldToMessage($scope.message, FIELD_NAME, {
			label: LABELS_FOR_MESSAGES[FIELD_NAME],
			oldValue: momentOldValueOnField.isValid() ? momentOldValueOnField.format(DATE_FORMAT_DEFAULT) : '',
			newValue: newVal
		});
		
		formInputsServices.changeAndSaveSimpleDateFieldClosing($scope, newVal, FIELD_NAME, callback);
	}
	
	function changeAndSaveSimpleDateFieldClosing($scope, newVal, FIELD_NAME, callback) {
		let isConfirmed = $scope.request.status.raw == CONFIRMED_STATUS;
		if (isConfirmed) {
			const REQUEST_FIELD_NAME = VARIABLE_NAMES[FIELD_NAME];
			let momentNewValue = moment(newVal, DATE_FORMAT_DEFAULT);
			
			writeChangesToInvoice(REQUEST_FIELD_NAME, $scope.request, $scope.invoice, momentNewValue);
			
			changeFieldInEditrequestInvoice($scope.editRequestinvoice, FIELD_NAME, {
				date: momentNewValue.format(DATE_FORMAT_EDITREQUEST),
				time: '15:10:00',
			});
			
			let momentOldValueInFieldInvoice = moment(_.get($scope.invoice, REQUEST_FIELD_NAME + '.old', ''));
			addFieldToMessageInvoice($scope.messageInvoice, FIELD_NAME, {
				label: LABELS_FOR_MESSAGES_INVOICE[FIELD_NAME],
				oldValue: momentOldValueInFieldInvoice.isValid() ? momentOldValueInFieldInvoice.format(DATE_FORMAT_DEFAULT) : '',
				newValue: newVal
			});
		}
		
		if (callback) callback();
	}
	
	function writeChangesToRequest(VAR_NAME, request, momentNewValue) {
		request[VAR_NAME].value = momentNewValue.format(DATE_FORMAT_FIELD);
		request[VAR_NAME].raw = momentNewValue.unix();
	}
	
	function writeChangesToInvoice(VAR_NAME, request, invoice, momentNewValue) {
		invoice[VAR_NAME] = invoice[VAR_NAME] || angular.copy(request[VAR_NAME]);
		invoice[VAR_NAME].raw = momentNewValue.unix();
		invoice[VAR_NAME].value = momentNewValue.format(DATE_FORMAT_FIELD);
	}
	
	function changeFieldInEditrequest(editrequest, fieldName, value) {
		editrequest[fieldName] = value;
	}
	
	function addFieldToMessage(message, fieldName, value) {
		message[fieldName] = value;
	}
	
	function changeFieldInEditrequestInvoice(editRequestinvoice, fieldName, value) {
		editRequestinvoice.invoice[fieldName] = value;
	}
	
	function addFieldToMessageInvoice(messageInvoice, fieldName, value) {
		messageInvoice[fieldName] = value;
	}
	
	function getDayClass(date) {
		date = moment(date);
		let dateString = date.format(DATE_FORMAT_CALENDAR);
		let year = date.get('year');
		if (calendar[year]
			&& calendar[year][dateString]
			&& calendartype[calendar[year][dateString]] != 'Block this day') {
			return calendartype[calendar[year][dateString]];
		}
		else {
			return 'block_date';
		}
	}
	
	function findFirstUnblockedDay(dateObj) { //dateObj is moment()
		let newDay = moment(dateObj);
		const ONE_DAY = 1;
		if (isBlocked(newDay)) {
			SweetAlert.swal({
				title: 'Error!',
				text: 'This day is blocked',
				type: 'error',
				timer: 2000,
			});
			while (isBlocked(newDay)) {
				newDay.add(ONE_DAY, 'd');
			}
		}
		return newDay;
	}
	
	function isBlocked(dateObj) {
		return getDayClass(dateObj) == 'block_date';
	}
}