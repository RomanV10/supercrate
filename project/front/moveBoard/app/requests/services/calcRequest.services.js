angular
	.module('move.requests')
	.factory('CalcRequest', CalcRequest);

CalcRequest.$inject = ['$http', '$q', '$rootScope', 'common', 'config', 'CalculatorServices', 'RequestServices', 'EditRequestServices', 'InventoriesServices', 'SweetAlert', 'RequestHelperService', 'datacontext', 'moveBoardApi', 'apiService', 'newLogService', 'CommercialCalc'];

function CalcRequest($http, $q, $rootScope, common, config, CalculatorServices, RequestServices, EditRequestServices, InventoriesServices, SweetAlert, RequestHelperService, datacontext, moveBoardApi, apiService, newLogService, CommercialCalc) {

	let basicsettings = angular.fromJson($rootScope.fieldData.basicsettings);

	let service = {};
	service.getDrivingTime = getDrivingTime;
	service.initStorageRequest = initStorageRequest;
	service.calcFuelSurcharge = calcFuelSurcharge;
	service.getFuelSurcharge = getFuelSurcharge;
	service.flatrateQuoteRequestChange = flatrateQuoteRequestChange;
	service.flatrateQuoteInvoiceChange = flatrateQuoteInvoiceChange;

	let tempRequest = [];
	let requestFromStorage = {},
		requestToStorage = {},
		storageReq;

	return service;

	function flatrateQuoteRequestChange(request) {
		var total = 0;

		if (_.get(request, 'flat_rate_quote.value')) {
			total += Number(request.flat_rate_quote.value);
		}

		return total;
	}

	function flatrateQuoteInvoiceChange(invoice, request) {
		var total = 0;

		if (_.get(invoice, 'flat_rate_quote.value')) {
			total += Number(invoice.flat_rate_quote.value);
		} else {
			invoice.flat_rate_quote = request.flat_rate_quote;
			total += Number(invoice.flat_rate_quote.value);
		}

		return total;
	}

	function SaveTemp(request) {
		tempRequest = request;
	}

	function getTemp() {
		return tempRequest;
	}

	function IsStorage(reqTo, reqFrom) {
		storageReq = {
			to: reqTo,
			from: reqFrom
		};
	}

	function getIsStorage() {
		return storageReq;
	}

	function initStorageRequest(request, toSt, fromSt) {
		let defer = $q.defer();
		SweetAlert.swal({
			title: 'Are you sure you want to create a storage request?',
			text: 'Storage Request will be created.',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Create',
			cancelButtonText: 'Don\'t create',
			closeOnConfirm: true,
			closeOnCancel: true
		}, function (isConfirm) {
			if (isConfirm) {
				createStorageRequest(request, toSt, fromSt)
					.finally(() => {
						defer.resolve();
					});
			} else {
				let promises = [];
				// only for request to storage
				if (request.service_type.raw == 6 && !request.request_all_data.toStorage) {
					promises.push(RequestHelperService.addOvernightExtraService(request));
				} else {
					promises.push(RequestHelperService.removeOvernightExtraService(request));
				}

				if (request.service_type.raw == 2) {
					request.request_all_data.withoutStorageRequest = true;
					promises.push(RequestServices.saveReqData(request.nid, request.request_all_data));
				}
				$q.all().finally(() => {
					defer.resolve();
				});
			}
		});
		return defer.promise;
	}

	function createStorageRequest(initial_request, toSt, fromSt) {
		SaveTemp(initial_request);
		IsStorage(toSt, fromSt);
		let defer = $q.defer();
		calculateStorageRequest(afterCalculating);
		return defer.promise;

		function afterCalculating(calculatedData) {
			let newRequest = _prepareRequest(calculatedData.From.request);
			if (fromSt) {
				newRequest = _prepareRequest(calculatedData.To.request, fromSt);
			}

			if (toSt) {
				newRequest = _prepareRequest(calculatedData.From.request);
			}
			newRequest.all_data = angular.copy(initial_request.request_all_data);

			if (_.isUndefined(newRequest.all_data.toStorage)) {
				newRequest.all_data.toStorage = true;
				initial_request.request_all_data.toStorage = false;
				RequestServices.saveReqData(initial_request.nid, initial_request.request_all_data);
			} else {
				newRequest.all_data.toStorage = !initial_request.request_all_data.toStorage;
			}

			_createRequest(newRequest)
				.then(function (response) {
					if (!!response) {
						let newId = response.nid;

						//create log message for update new storage request
						let fieldData = $rootScope.fieldData.field_lists;
						let results = newRequest.data;
						let request = {
							move_size: {raw: results.field_size_of_move},
							rooms: {
								raw: results.field_extra_furnished_rooms,
								value: results.field_extra_furnished_rooms
							}
						};
						let weight = CalculatorServices.getCubicFeet(request);
						let weightToLog = (weight.min + weight.max) / 2;
						let arr = newLogService.createLogs('Admin Creating Request', fieldData, newRequest.data,
							parseFloat(weightToLog));

						if (!_.isEmpty(arr)) {
							RequestServices.sendLogs(arr, 'Request was created', newId, 'MOVEREQUEST');
						}

						calculatedData.To.editrequest.field_from_storage_move = newId;
						//Update original requests
						if (!fromSt && !toSt) {
							calculatedData.To.editrequest.field_moving_to = calculatedData.To.request.field_moving_to;
							initial_request.field_moving_to = calculatedData.To.request.field_moving_to;
						}

						let promise1 = RequestServices.updateRequest(initial_request.nid,
							calculatedData.To.editrequest);
						let promise2 = RequestServices.getRequestsByNid(newId);

						//create log message for update initial request
						let msgLog = newLogService.createLogs('Request service type changed', fieldData,
							calculatedData.To.editrequest, parseFloat(weightToLog));

						if (!_.isEmpty(msgLog)) {
							RequestServices.sendLogs(msgLog, 'Request was updated', initial_request.nid, 'MOVEREQUEST');
						}

					let saveInventoryList = {};

					if (_.get(initial_request, 'inventory.inventory_list')) {
						saveInventoryList = InventoriesServices.saveListInventories(newId, initial_request.inventory.inventory_list);
					}

					$q.all([promise1, promise2, saveInventoryList]).then(function (responseArr) {
						initial_request = EditRequestServices.updateRequestLive(initial_request, calculatedData.To.editrequest);
						initial_request.storage_id = newId;

								if (initial_request.service_type.raw == 6) {
									// only for request to storage
									if (!initial_request.request_all_data.toStorage) {
										RequestHelperService.addOvernightExtraService(initial_request);
									}

									let secondRequest = _.first(responseArr[1].nodes);

									if (secondRequest && !secondRequest.request_all_data.toStorage) {
										RequestHelperService.addOvernightExtraService(secondRequest);
									}
								}

								$rootScope.$broadcast('request.editRequest', initial_request,
									calculatedData.To.editrequest);
								$rootScope.$broadcast('request.addNewRequest');

								EditRequestServices.requestEditModal(newId);

								toastr.success('Updated!');
							})
							.finally(() => {
								defer.resolve();
							});
					} else {
						defer.resolve();
					}
				})
				.catch(() => {
					defer.resolve();
				});
		}
	}

	function calculateStorageRequest(callBack = false) {
		var initial_request = getTemp();
		var is_storage = getIsStorage();
		angular.copy(initial_request, requestFromStorage);
		angular.copy(initial_request, requestToStorage);

		var basicSettings = angular.fromJson($rootScope.fieldData.basicsettings);
		var calcSettings = angular.fromJson($rootScope.fieldData.calcsettings);
		var minCATravelTime = basicSettings.minCATavelTime ? CalculatorServices.getRoundedTime(basicSettings.minCATavelTime / 60) : 0;
		var services = [1, 2, 3, 4, 6, 8];
		var isDoubleDriveTime = calcSettings.doubleDriveTime && services.indexOf(parseInt(requestToStorage.service_type.raw)) >= 0;

		requestToStorage.zipFrom = initial_request.field_moving_from.postal_code;
		requestToStorage.typeFrom = initial_request.type_from.raw;
		requestToStorage.typeTo = 1;
		requestToStorage.type_to.raw = 1;
		requestToStorage.zipTo = basicSettings.parking_address;

		requestToStorage.field_moving_to.postal_code = basicSettings.parking_address;
		requestToStorage.field_moving_to.locality = basicSettings.storage_city;
		requestToStorage.field_moving_to.thoroughfare = basicSettings.company_name;
		requestToStorage.field_moving_to.administrative_area = basicSettings.main_state;

		requestFromStorage.field_moving_from.postal_code = basicSettings.parking_address;
		requestFromStorage.field_moving_from.locality = basicSettings.storage_city;
		requestFromStorage.field_moving_from.thoroughfare = basicSettings.company_name;
		requestFromStorage.field_moving_from.administrative_area = basicSettings.main_state;

		requestFromStorage.zipTo = initial_request.field_moving_to.postal_code;
		requestFromStorage.typeTo = initial_request.type_to.raw;
		requestFromStorage.typeFrom = 1;
		requestToStorage.type_from.raw = 1;
		requestFromStorage.zipFrom = basicSettings.parking_address;

		requestFromStorage.duration.value = 0;

		if (requestFromStorage.service_type.raw == 6) {
			requestFromStorage.date.raw = moment.unix(requestFromStorage.date.raw)
				.add(1, 'd')
				.unix();
		} else {
			requestFromStorage.date.raw = moment.unix(requestFromStorage.date.raw)
				.add(2, 'd')
				.unix();
		}

		requestToStorage.duration.value = 0;

		var promise1 = CalculatorServices.getDuration(requestToStorage.zipFrom, requestToStorage.zipTo, requestToStorage.service_type.raw); //FROM ADDRESS TO STORAGE
		var promise2 = CalculatorServices.getDuration(requestFromStorage.zipFrom, requestFromStorage.zipTo, requestFromStorage.service_type.raw); // FROM STORAGE TO ADDRESS

		var tasks = [promise1, promise2];

		if (is_storage.to || is_storage.from) {
			var promise5 = RequestServices.getRequestsByNid(initial_request.storage_id);
			tasks.push(promise5);
		}

		$q.all(tasks)
			.then(function (data) {
				var editrequestTo = {};
				var editrequestFrom = {};

				if (requestToStorage.field_extra_dropoff.postal_code != null) {
					if (requestToStorage.field_extra_dropoff.postal_code.length) {
						editrequestFrom['field_extra_dropoff'] = requestToStorage.field_extra_dropoff;
						editrequestTo['field_extra_dropoff'] = requestToStorage.field_extra_dropoff;
						editrequestTo['field_extra_dropoff']['postal_code'] = '';
					}
				}

				editrequestTo['field_travel_time'] = data[0]['duration'];
				editrequestTo['field_distance'] = data[0]['distance'];
				editrequestTo['field_duration'] = data[0].distances.AB.duration;

				editrequestFrom['field_travel_time'] = data[1]['duration'];
				editrequestFrom['field_distance'] = data[1]['distance'];
				editrequestFrom['field_duration'] = data[1].distances.AB.duration;

				requestToStorage.distance.value = data[0]['distance'];
				requestToStorage.duration.value = data[0].distances.AB.duration;
				requestToStorage.travel_time.raw = data[0]['duration'];

				requestFromStorage.travel_time.raw = data[1]['duration'];
				requestFromStorage.duration.value = data[1].distances.AB.duration;
				requestFromStorage.distance.value = data[1]['distance'];

				if (isDoubleDriveTime) {
					requestToStorage.field_double_travel_time = Math.max(minCATravelTime, data[0].distances.AB.duration * 2);
					requestFromStorage.field_double_travel_time = Math.max(minCATravelTime, data[1].distances.AB.duration * 2);
					requestToStorage.field_double_travel_time = CalculatorServices.getRoundedTime(requestToStorage.field_double_travel_time);
					requestFromStorage.field_double_travel_time = CalculatorServices.getRoundedTime(requestFromStorage.field_double_travel_time);
				}

				var datas = {
					To: {},
					From: {}
				};

				datas.To = calculateStorage(requestToStorage, editrequestTo);
				datas.From = calculateStorage(requestFromStorage, editrequestFrom);

				if (data[2]) {
					if (is_storage.to) {
						datas.From.request.type_to = data[2].nodes[0].type_to;
						datas.From.request.field_moving_to = data[2].nodes[0].field_moving_to;
						datas.From.request.apt_to = data[2].nodes[0].apt_to;
					} else if (is_storage.from) {
						datas.To.request.type_from = data[2].nodes[0].type_from;
						datas.To.request.field_moving_from = data[2].nodes[0].field_moving_from;
						datas.To.request.apt_from = data[2].nodes[0].apt_from;
					}
				}

				if (callBack) {
					callBack(datas);
				}
			});
	}

	function calculateStorage(request, editrequest) {
		let calcSettings = angular.fromJson(datacontext.getFieldData().calcsettings);

		var total_weight;
		if (request.move_size.raw != 11) {
			total_weight = CalculatorServices.getRequestCubicFeet(request);
		} else {
			total_weight = CommercialCalc.getCommercialCubicFeet(request.commercial_extra_rooms.value, calcSettings, request.field_custom_commercial_item);
		}

		var inventory_weight = InventoriesServices.calculateTotals(request.inventory.inventory_list);

		if (inventory_weight.cfs > 0) {
			total_weight.weight = inventory_weight.cfs;
		}

		var results = calculateTime(request, total_weight.weight);

		request.rate.value = results.rate.value;
		request.crew.value = results.crew.value;
		request.maximum_time.raw = results.maximum_time.raw;
		request.minimum_time.raw = results.minimum_time.raw;

		editrequest['field_move_service_type'] = request.service_type.raw;
		editrequest['field_price_per_hour'] = results.rate.value;
		editrequest['field_movers_count'] = results.crew.value;
		editrequest['field_maximum_move_time'] = results.maximum_time.raw;
		editrequest['field_minimum_move_time'] = results.minimum_time.raw;

		return {
			request: request,
			editrequest: editrequest
		};
	}


	function _prepareRequest(request, fromSt) {
		const DEFAULT_SOURCE = 'Moveboard';
		var new_request = {};
		var user = request.uid;
		var userField_user_additional_phone;

		if (user.field_user_additional_phone != null) {
			userField_user_additional_phone = user.field_user_additional_phone;
		}

		new_request.data = {
			status: 1,
			title: 'Move Request',
			uid: user.uid,
			field_first_name: user.field_user_first_name,
			field_last_name: user.field_user_last_name,
			field_e_mail: user.mail || '',
			field_phone: user.field_primary_phone || '',
			field_additional_phone: userField_user_additional_phone || '',
			field_approve: 1,
			field_move_service_type: request.service_type.raw, //Move & storage
			field_date: {
				date: moment.unix(request.date.raw).utc().startOf('day').format('YYYY-MM-DD'),
				time: '15:10:00',
			},
			field_size_of_move: request.move_size.raw,
			field_type_of_entrance_from: request.type_from.raw,
			field_type_of_entrance_to_: request.type_to.raw,
			field_start_time: request.start_time.raw,
			field_price_per_hour: request.rate.value,
			field_movers_count: request.crew.value,
			field_distance: request.distance.value,
			field_travel_time: request.travel_time.raw,
			field_minimum_move_time: request.minimum_time.raw,
			field_maximum_move_time: request.maximum_time.raw,
			field_duration: request.duration.value,
			field_commercial_extra_rooms: request.commercial_extra_rooms.raw,
			field_extra_furnished_rooms: request.rooms.raw, //?
			field_moving_to: {
				country: 'US',
				administrative_area: request.field_moving_to.administrative_area,
				locality: request.field_moving_to.locality,
				postal_code: request.field_moving_to.postal_code,
				thoroughfare: request.field_moving_to.thoroughfare,
				premise: ''
			},
			field_apt_from: request.apt_from.value,
			field_apt_to: request.apt_to.value,
			field_moving_from: {
				country: 'US',
				administrative_area: request.field_moving_from.administrative_area,
				locality: request.field_moving_from.locality,
				postal_code: request.field_moving_from.postal_code,
				thoroughfare: request.field_moving_from.thoroughfare,
				premise: ''
			},
			field_storage_price: request.storage_rate.value,
			field_reservation_price: request.reservation_rate.value,
			field_request_settings: request.field_request_settings || {storage_rate: basicsettings.storage_rate},
			field_parser_provider_name: request.field_parser_provider_name || DEFAULT_SOURCE,
			field_useweighttype: request.field_useweighttype.value,
		};

		if (request.move_size.raw == 11) {
			new_request.data.field_commercial_extra_rooms = request.commercial_extra_rooms.value;
		}

		if (fromSt) {
			new_request.data.field_from_storage_move = request.nid;
		} else {
			new_request.data.field_to_storage_move = request.nid;
		}

		return new_request;
	}

	function _createRequest(request) {
		var deferred = $q.defer();

		$http
			.post(config.serverUrl + 'server/move_request', request)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}


	function getDrivingTime(request) {
		var result = [];
		var serviceType = request.service_type.raw;

		if (!request.extraPickup && !request.extraDropoff) {
			var promise = CalculatorServices.getDuration(request.zip_from.value, request.zip_to.value, serviceType);
		} else if (request.extraPickup) {
			var promise = CalculatorServices.getDuration(request.zip_from.value, request.pickup_zip_from.value, serviceType);
			var promise2 = CalculatorServices.getDuration(request.pickup_zip_from.value, request.zip_to.value, serviceType);
		} else if (request.extraDropoff) {
			var promise = CalculatorServices.getDuration(request.zip_from.value, request.drop_zip_from.value, serviceType);
			var promise2 = CalculatorServices.getDuration(request.drop_zip_from.value, request.zip_to.value, serviceType);
		} else {
			var promise = CalculatorServices.getDuration(request.zip_from.value, request.pickup_zip_from.value, serviceType);
			var promise2 = CalculatorServices.getDuration(request.pickup_zip_from.value, request.drop_zip_from.value, serviceType);
			var promise3 = CalculatorServices.getDuration(request.drop_zip_from.value, request.zip_to.value, serviceType);

			$q.all([promise, promise2, promise3])
				.then(function (data) {
					result.duration = data[0].distances.AB.duration + data[1].distances.AB.duration + data[2].distances.AB.duration;
					result.distance = data[0].distances.AB.distance + data[1].distances.AB.distance + data[2].distances.AB.distance;
					return result;
				});
		}

		$q.all([promise, promise2, promise3])
			.then(function (data) {
				request.duration.value = data.duration;
				request.distance.value = data.distance;
				deferred.resolve();
			});
	}

	function calculateTime(request, weight) {
		var total_cf = [];
		var calcSettings = angular.fromJson($rootScope.fieldData.calcsettings);

		total_cf.weight = weight;
		request.typeFrom = request.type_from.raw;
		request.typeTo = request.type_to.raw;
		request.serviceType = request.service_type.raw;

		var calcResults = CalculatorServices.calculateTime(request, total_cf);
		var moversCount = calcResults.movers_count;
		var minWorkTime = calcResults.work_time.min;
		var maxWorkTime = calcResults.work_time.max;

		if (!calcSettings.doubleDriveTime) {
			minWorkTime += parseFloat(request.duration.value);
			maxWorkTime += parseFloat(request.duration.value);
		}

		request.minimum_time.raw = minWorkTime;
		request.minimum_time.value = common.decToTime(minWorkTime);

		request.maximum_time.raw = maxWorkTime;
		request.maximum_time.value = common.decToTime(maxWorkTime);

		request.crew.value = moversCount;

		var truckNeeded = CalculatorServices.getTrucks(weight);

		request.truckCount = truckNeeded;
		var date = '';

		if (typeof request.date.value != 'object') {
			date = request.date.value;
		} else {
			date = request.date.value.date;
		}

		request.rate.value = CalculatorServices.getRate(date, moversCount, truckNeeded);

		return request;
	}

	function getFuelSurcharge(data) {
		var data = {
			data: data
		};

		return apiService.postData(moveBoardApi.calcService.calcFuelSurcharge, data);
	}

	function calcFuelSurcharge(request, quote, request_distance) {
		let deferred = $q.defer();

		let serviceType = request.service_type.raw;
		let zipFrom = request.field_moving_from.postal_code;
		let zipTo = request.field_moving_to.postal_code;
		let data = {
			serviceType,
			zipFrom,
			zipTo,
			quote
		};

		if (!_.isUndefined(request_distance) && request_distance.zipFrom == zipFrom && request_distance.zipTo == zipTo) {
			data.request_distance = request_distance;
		}

		let result = {
			surcharge_fuel_avg: 0, surcharge_fuel: 0, surcharge_fuel_perc: 0
		};

		getFuelSurcharge(data)
			.then(function (response) {
				result.surcharge_fuel = response.data.surcharge_fuel;
				result.surcharge_fuel_perc = response.data.surcharge_fuel_perc;
				result.surcharge_fuel_avg = response.data.surcharge_fuel_avg;

				if (response.data.request_distance) {
					result.request_distance = response.data.request_distance;
				}

				deferred.resolve(result);
			}, function () {
				deferred.resolve(result);
			});

		return deferred.promise;
	}
}
