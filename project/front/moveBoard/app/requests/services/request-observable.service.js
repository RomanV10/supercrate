'use strict';

angular
	.module('app')
	.factory('requestObservableService', requestObservableService);

requestObservableService.$inject = [];

function requestObservableService () {
	
	const BASE_VARIABLE_BODY = {
		changed: false,
		store: {},
	};
	
	let watchesVariables = {
		weightTypeChanged: angular.copy(BASE_VARIABLE_BODY),
		weightTypeInvoiceChanged: angular.copy(BASE_VARIABLE_BODY),
		inventoryUpdate: angular.copy(BASE_VARIABLE_BODY),
		detailsUpdated: angular.copy(BASE_VARIABLE_BODY),
		updateWeightType: angular.copy(BASE_VARIABLE_BODY),
		useCalculatorChanged: angular.copy(BASE_VARIABLE_BODY),
	};
	
	return {
		setChanges,
		getChanges,
		clearChanges,
	};
	
	function setChanges(props, storedData) {
		if (!watchesVariables[props]) return;
		
		watchesVariables[props].changed = true;
		
		if (storedData) {
			watchesVariables[props].store = storedData;
		}
	}
	
	function getChanges(props, callback, field) {
		if (watchesVariables[props].changed) {
			let dataForCallback = angular.copy(watchesVariables[props].store);
			setToRequest(field, watchesVariables[props].store);
			callback(dataForCallback);
			watchesVariables[props].changed = false;
		}
	}
	
	function setToRequest (field, store) {
		if (field && !_.isEmpty(store)) {
			field.value = store[field.field];
			delete store[field.field];
		}
	}
	
	function clearChanges () {
		for (let prop in watchesVariables) {
			watchesVariables[prop].changed = false;
		}
	}
}
