(function () {
    'use strict';

    angular
        .module('move.requests')

        .factory('ParserServices', ParserServices);

    ParserServices.$inject = ['RequestServices'];

    function ParserServices(RequestServices) {

        var service = {};

        service.isInvalidEntarance = isInvalidEntarance;
        service.saveEntrance = saveEntrance;
        service.validateParserRequest = validateParserRequest;
        service.isInvalidFieldMovingTo = isInvalidFieldMovingTo;
        service.isInvalidFieldMovingFrom = isInvalidFieldMovingFrom;
        service.isInvalidFieldStartTime = isInvalidFieldStartTime;
        service.isInvalidFieldMoveSize = isInvalidFieldMoveSize;
        service.isInvalidFieldDate = isInvalidFieldDate;
        service.isInvalidEditFieldDate = isInvalidEditFieldDate;
        service.isInvalidTypeFrom = isInvalidTypeFrom;
        service.isInvalidFieldTypeFrom = isInvalidFieldTypeFrom;
        service.isInvalidTypeTo = isInvalidTypeTo;
        service.isInvalidFieldTypeTo = isInvalidFieldTypeTo;
        service.isParserRequest = isParserRequest;
        service.isValidateFrom = isValidateFrom;
        service.isValidateTo = isValidateTo;

        return service;


        function isInvalidEntarance(request) {
            if (_.isUndefined(request.request_all_data.entrance)) {
                return false;
            } else {
                return request.request_all_data.entrance == false;
            }
        }

        function saveEntrance(request, editrequest) {
            if (isInvalidEntarance(request)
                && (!_.isUndefined(editrequest.field_type_of_entrance_to_) && isValidateTo(request) || !_.isUndefined(editrequest.field_type_of_entrance_from) && isValidateFrom(request))) {

                request.request_all_data.entrance = true;
                RequestServices.saveReqData(request.nid, request.request_all_data);
            }
        }

        //TODO Refactor this file

        function isValidateFrom(request) {
            let isAlwaysValid = request.service_type.raw == 1 || request.service_type.raw == 5 || request.service_type.raw == 7;
            let isMoveAndStorageToStorage = (request.service_type.raw == 2 || request.service_type.raw == 6) && !request.request_all_data.toStorage;
            let isLoadingHelp = request.service_type.raw == 3;
            let isPackingDay = request.service_type.raw == 8;

            return isAlwaysValid || isMoveAndStorageToStorage || isLoadingHelp || isPackingDay;
        }

        function isValidateTo(request) {
            let isAlwaysValid = request.service_type.raw == 1 || request.service_type.raw == 5 || request.service_type.raw == 7;
            let isMoveAndStorageFromStorage = (request.service_type.raw == 2 || request.service_type.raw == 6) && request.request_all_data.toStorage;
            let isUnloadingHelp = request.service_type.raw == 4;

            return isAlwaysValid || isMoveAndStorageFromStorage || isUnloadingHelp;
        }

        // validate parser request
        function validateParserRequest(request, editrequest) {
            let result = true;

            // service_type
            // 1 moving
            // 2 move & storage
            // 3 loading help
            // 4 uploading help
            // 5 flat rate
            // 6 overnight
            // 7 long distance
            if ((request.status.raw == 1
                || request.status.raw == 2)
                && isParserRequest(request)) { // only for email parsing request

                if (request.service_type.raw == 2
                    || request.service_type.raw == 3
                    || request.service_type.raw == 4
                    || request.service_type.raw == 6
	                || request.service_type.raw == 8) {

                    if ((!_.isUndefined(request.request_all_data)
                        && !_.isUndefined(request.request_all_data.toStorage)
                        && request.request_all_data.toStorage == true)
                        || request.service_type.raw == 4) {

                        result = result && (!isInvalidFieldMovingTo(request) || (editrequest && editrequest.field_moving_to && !isInvalidFieldMovingTo(editrequest) && isValidateTo(request)));

                        result = result && (!isInvalidTypeTo(request) || (!_.isUndefined(editrequest) && !_.isUndefined(editrequest.field_type_of_entrance_to_) && !isInvalidFieldTypeTo(editrequest) && isValidateTo(request)));
                    }

                    if ((!_.isUndefined(request.request_all_data)
                        && !_.isUndefined(request.request_all_data.toStorage)
                        && request.request_all_data.toStorage == false)
                        || request.service_type.raw == 3
                        || request.service_type.raw == 8) {

                        result = result && (!isInvalidFieldMovingFrom(request) || (editrequest && editrequest.field_moving_from && !isInvalidFieldMovingFrom(editrequest) && isValidateFrom(request)));

                        result = result && (!isInvalidTypeFrom(request) || (!_.isUndefined(editrequest) && !_.isUndefined(editrequest.field_type_of_entrance_from) && !isInvalidFieldTypeFrom(editrequest) && isValidateFrom(request)));
                    }
                } else {
                    result = result && (!isInvalidFieldMovingFrom(request) || (editrequest && editrequest.field_moving_from && !isInvalidFieldMovingFrom(editrequest) && isValidateFrom(request)));

                    result = result && (!isInvalidFieldMovingTo(request) || (editrequest && editrequest.field_moving_to && !isInvalidFieldMovingTo(editrequest) && isValidateTo(request)));

                    result = result && (!isInvalidTypeFrom(request) || (!_.isUndefined(editrequest) && !_.isUndefined(editrequest.field_type_of_entrance_from) && !isInvalidFieldTypeFrom(editrequest) && isValidateFrom(request)));
                    result = result && (!isInvalidTypeTo(request) || (!_.isUndefined(editrequest) && !_.isUndefined(editrequest.field_type_of_entrance_to_) && !isInvalidFieldTypeTo(editrequest) && isValidateTo(request)));
                }


                result = result && (!isInvalidFieldDate(request) || (editrequest && editrequest.field_date && !isInvalidEditFieldDate(editrequest)));
                result = result && (!isInvalidFieldMoveSize(request) || (editrequest && editrequest.field_size_of_move));


            }

            return result;
        }

        function isParserRequest(request) {
            return _.get(request, 'request_all_data.parser');
        }

        function isInvalidFieldMovingTo(request) {
            if (_.isUndefined(request)
                || _.isUndefined(request.field_moving_to)) {

                return true;
            } else {
                return _.isEmpty(request.field_moving_to.postal_code)
                    || _.isEmpty(request.field_moving_to.administrative_area)
                    || _.isNull(request.field_moving_to.administrative_area)
                    || _.isEmpty(request.field_moving_to.locality);
            }
        }

        function isInvalidFieldMovingFrom(request) {
            if (_.isUndefined(request)
                || _.isUndefined(request.field_moving_from)) {

                return true;
            } else {
                return _.isEmpty(request.field_moving_from.postal_code)
                    || _.isEmpty(request.field_moving_from.administrative_area)
                    || _.isNull(request.field_moving_from.administrative_area)
                    || _.isEmpty(request.field_moving_from.locality);
            }

        }

        function isInvalidFieldStartTime (request) {
            return _.isNull(request.start_time.raw) || _.isUndefined(request.start_time.raw);
        }

        function isInvalidFieldMoveSize (request) {
            return _.isNull(request.move_size.raw) || _.isUndefined(request.move_size.raw);
        }

        function isInvalidFieldDate(request) {
            return _.isEmpty(request.date.value) || request.date.raw == 0;
        }

        function isInvalidEditFieldDate(editrequest) {
            return _.isUndefined(editrequest)
            || _.isUndefined(editrequest.field_date)
                ? false
                : _.isEmpty(editrequest.field_date.date);
        }

        function isInvalidTypeFrom(request) {
            if (_.isUndefined(request.request_all_data.entrance)) {
                return false;
            } else {
                return request.request_all_data.entrance == false;
            }
        }

        function isInvalidFieldTypeFrom(editrequest) {
            if (_.isUndefined(editrequest)) {
                return false;
            } else {
                return _.isUndefined(editrequest.field_type_of_entrance_from) || !editrequest.field_type_of_entrance_from;
            }
        }

        function isInvalidTypeTo(request) {
            if (_.isUndefined(request.request_all_data.entrance)) {
                return false;
            } else {
                return request.request_all_data.entrance == false;
            }
        }

        function isInvalidFieldTypeTo(editrequest) {
            if (_.isUndefined(editrequest)) {
                return false;
            } else {
                return _.isUndefined(editrequest.field_type_of_entrance_to_) || !editrequest.field_type_of_entrance_to_;
            }
        }
    }
})();
