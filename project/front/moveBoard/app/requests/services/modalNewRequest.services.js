(function () {

    angular
        .module('move.requests')
        .factory('modalNewRequestService', NewRequestService);

    NewRequestService.$inject = ['config', '$q', 'CalculatorServices', 'datacontext', '$http', 'SweetAlert', 'AuthenticationService','common', '$rootScope', 'CommercialCalc'];

    function NewRequestService(config, $q, CalculatorServices, datacontext, $http, SweetAlert, AuthenticationService, common, $rootScope, CommercialCalc) {

        function InitService(requestData, callback) {
            var fieldData = datacontext.getFieldData();
            var basicsettings = angular.fromJson(fieldData.basicsettings);
            var longdistance = angular.fromJson(fieldData.longdistance);
            var calcSettings = angular.fromJson(fieldData.calcsettings);

            run();

            function run() {
                var request = angular.copy(requestData.data);
                var user = requestData.account;
                var calculator = requestData.calculate;
                var isSelectFlatRate = request.field_move_service_type == 5;
                var flatRateDistance = basicsettings.isflat_rate_miles ? basicsettings.flat_rate_miles || 0 : 0;
                var longDistanceMiles = basicsettings.islong_distance_miles ? basicsettings.long_distance_miles || 0 : 0;
	            let allData = requestData.request_all_data;
                user.name = requestData.account.mail;

                if (!calculator) {
                    request.field_e_mail = user.mail;
                    request.field_first_name = user.fields.field_user_first_name;
                    request.field_last_name = user.fields.field_user_last_name;
                    request.field_phone = user.fields.field_primary_phone;
                }

                request.status = 1;
                request.title = 'Move Request';
                request.field_approve = 1;
                request.field_useweighttype = '1';

	            if (request.field_custom_commercial_item.is_checked) {
		            request.field_useweighttype = '3';
                }

                if (!request.hasOwnProperty('field_moving_from')) {
                    request.field_moving_from = {
                        country: 'US'
                    };
                }

                if (!request.hasOwnProperty('field_moving_to')) {
                    request.field_moving_to = {
                        country: 'US'
                    }
                }

                if (angular.isDefined(request.field_date.date)) {
                    var date = moment(request.field_date.date).format('YYYY-MM-DD');

                }
                else {
                    var date = moment(request.field_date).format('YYYY-MM-DD');
                }

                request.field_date = {date: date, time: '15:10:00'};

                //request default settings
                request.field_request_settings = {
	                storage_rate: basicsettings.storage_rate,
                    inventoryVersion: 2
                };

                // This date must be only for flat rate
                if (!!request.field_delivery_date) {
                    var ddate = moment(request.field_delivery_date).format('YYYY-MM-DD');

                    if (angular.isDefined(request.field_delivery_date.date)) {
                        ddate = moment(request.field_delivery_date.date).format('YYYY-MM-DD');
                    }

                    request.field_delivery_date = {date: ddate, time: '15:10:00'};
                }

                var serviceType = request.field_move_service_type;
                var services = [1, 2, 3, 4, 6, 8];
                var isDoubleDriveTime = calcSettings.doubleDriveTime && services.indexOf(parseInt(serviceType)) >= 0;
                var minCATravelTime = basicsettings.minCATavelTime ? CalculatorServices.getRoundedTime(basicsettings.minCATavelTime / 60) : 0;
                let all_data = {};

                if (requestData.request_all_data) {
                    all_data = angular.copy(requestData.request_all_data);
                }

                if (serviceType == 5 || serviceType == 7) {
                    if (basicsettings.longDistanceDistountOn) {
                        all_data.localdiscount = basicsettings.longDistanceDistount;
                    }
                } else {
                    if (basicsettings.localDistountOn) {
                        all_data.localdiscount = basicsettings.localDistount;
                    }
                }

	            if (!_.isEqual(request.field_size_of_move, 11)) {
		            all_data.isDisplayQuoteEye = !basicsettings.showQuote[serviceType];
		            all_data.showQuote = basicsettings.showQuote[serviceType];
                } else {
                    // Commercial Move
		            all_data.isDisplayQuoteEye = !basicsettings.showQuote['9'];
		            all_data.showQuote = basicsettings.showQuote['9'];
                }

                let adminUid = _.get($rootScope, 'currentUser.userId.uid');

                all_data.admin = adminUid;
                all_data.travelTime = calcSettings.travelTime;
                request.field_parser_provider_name = 'Moveboard';

                if (serviceType == 2 || serviceType == 6) { //Move and storage
                    var requestToStorage = angular.copy(request);
                    var requestFromStorage = angular.copy(request);
                    let allDataToStorage = angular.copy(all_data);
                    let allDataFromStorage = angular.copy(all_data);

                    if (requestData.to_request_all_data) {
                        allDataToStorage = angular.copy(requestData.to_request_all_data);
                    }

                    if (requestData.from_request_all_data) {
                        allDataFromStorage = angular.copy(requestData.from_request_all_data);
                    }

                    allDataToStorage.toStorage = false;
                    allDataFromStorage.toStorage = true;

                    requestToStorage.field_moving_from.postal_code = request.field_moving_from.postal_code;
                    requestToStorage.field_type_of_entrance_from = request.field_type_of_entrance_from;
                    requestToStorage.field_type_of_entrance_to_ = 1;
                    requestToStorage.field_moving_to.postal_code = basicsettings.parking_address;
                    requestToStorage.field_moving_to.locality = basicsettings.storage_city;
                    requestToStorage.field_moving_to.thoroughfare = basicsettings.company_name;
                    requestToStorage.field_moving_to.administrative_area = basicsettings.main_state;

                    requestFromStorage.field_moving_to.postal_code = request.field_moving_to.postal_code;
                    requestFromStorage.field_type_of_entrance_to_ = request.field_type_of_entrance_to_;
                    requestFromStorage.field_type_of_entrance_from = 1;
                    requestFromStorage.field_moving_from.postal_code = basicsettings.parking_address;
                    requestFromStorage.field_moving_from.locality = basicsettings.storage_city;
                    requestFromStorage.field_moving_from.thoroughfare = basicsettings.company_name;
                    requestFromStorage.field_moving_from.administrative_area = basicsettings.main_state;
                    requestFromStorage.field_date = requestFromStorage.field_delivery_date;

                    if (calculator) {
                        var p1 = CalculatorServices.getDuration(requestToStorage.field_moving_from.postal_code, requestToStorage.field_moving_to.postal_code, serviceType); //FROM ADDRESS TO STORAGE
                        var p2 = CalculatorServices.getDuration(requestFromStorage.field_moving_from.postal_code, requestFromStorage.field_moving_to.postal_code, serviceType); // FROM STORAGE TO ADDRESS
                    } else {
                        var p1 = requestData.request_distance_to;
                        var p2 = requestData.request_distance_from;
                    }

                    $q.all([p1, p2]).then(function (data) {
                        requestToStorage.field_travel_time = data[0]['duration'];
                        requestFromStorage.field_travel_time = data[1]['duration'];
                        var toDuration = data[0].distances.AB.duration;
                        requestToStorage.field_duration = toDuration;
                        var fromDuration = data[1].distances.AB.duration;
                        requestFromStorage.field_duration = fromDuration;
                        requestToStorage.field_distance = data[0].distances.AB.distance;
                        requestFromStorage.field_distance = data[1].distances.AB.distance;

                        allDataToStorage.request_distance = data[0];
                        allDataFromStorage.request_distance = data[1];
                        requestData.request_distance_to = data[0];
                        requestData.request_distance_from = data[1];

                        if (isDoubleDriveTime) {
                            requestToStorage.field_double_travel_time = Math.max(minCATravelTime, toDuration * 2);
                            requestFromStorage.field_double_travel_time = Math.max(minCATravelTime, fromDuration * 2);
                            requestToStorage.field_double_travel_time = CalculatorServices.getRoundedTime(requestToStorage.field_double_travel_time);
                            requestFromStorage.field_double_travel_time = CalculatorServices.getRoundedTime(requestFromStorage.field_double_travel_time);
                        }

                        if (angular.isUndefined(requestFromStorage.field_date)) {
                            requestFromStorage.field_date = _.clone(requestToStorage.field_date)
                        }

                        requestToStorage = _calculate(requestToStorage);
                        requestFromStorage = _calculate(requestFromStorage);

                        var storageRequest = {
                            storage1: {data: requestToStorage, account: user, all_data: allDataToStorage},
                            storage2: {data: requestFromStorage, account: user, all_data: allDataFromStorage}
                        };

                        if (calculator) {
                            storageRequest.storage1.data.request_all_data = allDataToStorage;
                            storageRequest.storage2.data.request_all_data = allDataFromStorage;

                            requestData.to_request_all_data = allDataToStorage;
                            requestData.from_request_all_data = allDataFromStorage;


                            callback(storageRequest);
                        } else {
	                        delete requestToStorage.inventory_weight;
	                        delete requestFromStorage.inventory_weight;
                            // This date must be only for flat rate
                            delete storageRequest.storage1.data.field_delivery_date;
                            delete storageRequest.storage2.data.field_delivery_date;

                            _createStorageRequest(storageRequest).then(function (newRequests) {
                                if (!!newRequests[0]) {
                                    requestToStorage.nid = newRequests[0];
                                    requestFromStorage.nid = newRequests[1];
                                    var nids = [];
                                    nids.push(requestToStorage.nid.nid);
                                    nids.push(requestFromStorage.nid.nid);

                                    if (!requestToStorage.nid.nid || !requestFromStorage.nid.nid) {
                                        return false;
                                    }


                                    getRequestsByNid(nids).then(function (requests) {
                                        !!callback && callback(requests);
                                    });
                                }
                            });
                        }
                    });
                } else {
                    if (serviceType == 3 || serviceType == 8) { // Loading HElP FROM ZIP ONLY
                        request.field_moving_to['postal_code'] = request.field_moving_from.postal_code;
                        request['field_type_of_entrance_to_'] = request.field_type_of_entrance_from;
                    }

                    if (serviceType == 4) { // UNLOADING HELP TO ZIP ONLY
                        request.field_moving_from['postal_code'] = request.field_moving_to.postal_code;
                        request['field_type_of_entrance_from'] = request.field_type_of_entrance_to_;
                    }

                    //check if it's long distnace or flat rate
                    var fr = false;
                    var if_main_state = false;

                    var basicdistance = 0;

                    if (requestData.request_distance) {
                        basicdistance = requestData.request_distance.distances.AB.distance;
                    }

                    if (basicdistance > flatRateDistance && flatRateDistance || isSelectFlatRate) {
                        request.field_move_service_type = 5; // Flat Rate Step 1
                        fr = true;
                    }

                    if ((longDistanceMiles && basicdistance > longDistanceMiles) || request.field_move_service_type == 7) {
                        fr = true;

                        var state = request.field_moving_to.administrative_area.toLowerCase();
                        var baseState = request.field_moving_from.administrative_area.toUpperCase();
                        var main_state = basicsettings.main_state.toLowerCase();

                        if (baseState.toLowerCase() == main_state) {
                            if_main_state = true;
                        }

                        var isWeMoveToThisState = longdistance.stateRates[baseState][state]['longDistance'];

                        if (isWeMoveToThisState) {
                            request.field_move_service_type = 7;
                        } else {
                            if (longdistance.acceptAllQuotes) {
                                request.field_move_service_type = 7;

                                all_data.isDisplayQuoteEye = true;
                                all_data.showQuote = false;
                            }
                        }
                    }

                    if (calculator) {
                        if (fr) {
                            var p5 = CalculatorServices.getDuration(request.field_moving_from.postal_code, request.field_moving_to.postal_code, serviceType); //FROM ADDRESS TO STORAGE
                            var p6 = CalculatorServices.getLongDistanceCode(request.field_moving_from.postal_code);
                        } else {
                            var p5 = CalculatorServices.getDuration(request.field_moving_from.postal_code, request.field_moving_to.postal_code, serviceType);
                            var p6 = {};
                        }
                    } else {
                        if (fr) {
                            var p5 = requestData.request_distance;
                            var p6 = CalculatorServices.getLongDistanceCode(request.field_moving_from.postal_code);
                        } else {
                            var p5 = requestData.request_distance;
                            var p6 = {};
                        }
                    }

                    $q.all([p5, p6]).then(function (data) {
                        request.field_travel_time = data[0]['duration'];
                        var duration = data[0].distances.AB.duration;
                        request.field_duration = duration;
                        request.field_distance = data[0].distances.AB.distance;
                        requestData.request_distance = data[0];
                        all_data.request_distance = requestData.request_distance;

                        if (isDoubleDriveTime) {
                            request.field_double_travel_time = Math.max(minCATravelTime, duration * 2);
                            request.field_double_travel_time = CalculatorServices.getRoundedTime(request.field_double_travel_time);
                        }

                        var flatRate = (serviceType == 5 || serviceType == 7);
                        var requestStorage = (serviceType == 2 || serviceType == 6);
                        var distance = parseFloat(request.field_distance);

                        if (!!basicsettings.local_flat_miles && distance > basicsettings.local_flat_miles && !flatRate && !requestStorage) {
                            request.field_travel_time += parseFloat(duration);
                            all_data.localFlatMilesFlag = true;
                        } else {
                            all_data.localFlatMilesFlag = false;
                        }

                        request = _calculate(request);

	                    if (serviceType == 7) {
		                    var minWeight = CalculatorServices.getLongDistanceWeight(request);
		                    all_data.min_weight = minWeight.minWeight;
		                    all_data.min_price = minWeight.minPrice;
		                    all_data.min_price_enabled = minWeight.minPriceEnabled;
	                    }

                        if (calculator) {
                            request.request_all_data = all_data;
                            requestData.request_all_data = all_data;
                            callback(request);
                        }

                        if (!calculator) {
	                        delete request['inventory_weight'];

                            if (request.request_all_data) {
                                delete request.request_all_data;
                            }

                            // This field field_delivery_date must be only for flat rate
                            if (serviceType != 5 && request.field_delivery_date) {
                                delete request.field_delivery_date;
                            }

                            _createRequest({data: request, account: user, admin: true, all_data: all_data}).then(function (response) {
                                if (!!response) {
                                    var nid = response['nid'];
                                    var uid = response['uid'];

                                    if (angular.isUndefined(nid)) {
                                        var log = {
                                            'request': request,
                                            'browser': AuthenticationService.get_browser(),
                                            'message_type': "Admin Creating Request",
                                            'message': response[0],
                                            'type': 'Admin Panel',

                                        };
                                        AuthenticationService.sendLog("Error", log);
                                    }
                                    if (!nid || angular.isUndefined(nid)) {
                                        callback();
                                        return false;
                                    }

                                    getRequestsByNid([nid]).then(function (requests) {
                                        if (!_.isEmpty(requests)) {
                                            !!callback && callback(requests);
                                        }
                                    });

                                }
                            });
                        }
                    });


                }
            }

            var _calculate = function (request) {
                var calcSettings = angular.fromJson(fieldData.calcsettings);
                var basicSettings = angular.fromJson(fieldData.basicsettings);
                var total_weight;

	            if (!_.isEqual(request.field_size_of_move, 11)) {
		            total_weight = getRequestCubicFeet();
                } else {
		            total_weight = CommercialCalc.getCommercialCubicFeet(request.field_commercial_extra_rooms, calcSettings, request.field_custom_commercial_item);
                }

                var results = getTime(total_weight);
                let long_distance_rate = CalculatorServices.recalculateLongDistanceRate(request, total_weight.weight);

                request['field_price_per_hour'] = results.rate;
                request['field_movers_count'] = results.crew;
                request['field_delivery_crew_size'] = results.crew;
                request['field_maximum_move_time'] = results.maximum_time;
                request['field_minimum_move_time'] = results.minimum_time;
                request['field_reservation_price'] = -1;
                request['inventory_weight'] = total_weight;
                request['field_long_distance_rate'] = long_distance_rate;

                return request;

                function getRequestCubicFeet() {
                    // Get default size of apartment
                    var move_size = parseInt(calcSettings.size[request.field_size_of_move]);
                    // Get rooms if exist
                    var total_room_weight = 0;
                    angular.forEach(request.field_extra_furnished_rooms, function (rid, key) {
                        total_room_weight += parseInt(calcSettings.room_size[rid]);
                    });
                    //Get Total Weight
                    var total = [];
                    total.weight = total_room_weight + move_size + parseInt(calcSettings.room_kitchen);


                    if (request.field_move_service_type == 7) {
                        var copyRequest = angular.copy(request);
                        copyRequest.service_type = {raw: 7};

                        var linfo = CalculatorServices.getLongDistanceInfo(copyRequest);
                        var min_weight = angular.copy(linfo.min_weight || 0);

                        if (min_weight > total.weight) {
                            total.weight = min_weight;
                        }
                    }

                    total.min = total.weight - 100;
                    total.max = total.weight + 100;

                    return total;
                }

                function getTime(total_cf) {
                    var result = {};


                    var tmpRequest = angular.copy(request);
                    tmpRequest.typeFrom = tmpRequest.field_type_of_entrance_from;
                    tmpRequest.typeTo = tmpRequest.field_type_of_entrance_to_;
                    tmpRequest.serviceType = tmpRequest.field_move_service_type;
                    var duration = CalculatorServices.getRoundedTime(parseFloat(tmpRequest.field_duration));
                    var distance = parseFloat(tmpRequest.field_distance);
                    var isLongDistance = tmpRequest.field_move_service_type == 5 || tmpRequest.field_move_service_type == 7;
                    var isStorage = tmpRequest.field_move_service_type == 2 || tmpRequest.field_move_service_type == 6;

                    if (!!basicsettings.local_flat_miles && distance > basicSettings.local_flat_miles && !isLongDistance && !isStorage) {
                        duration = 0;
                    }

                    if (isLongDistance) {
                        duration = 0;
                    }

                    var calcResults = CalculatorServices.calculateTime(tmpRequest, total_cf);
                    var moversCount = calcResults.movers_count;
                    var minWorkTime = calcResults.work_time.min;
                    var maxWorkTime = calcResults.work_time.max;

                    if (!calcSettings.doubleDriveTime && !calcSettings.isTravelTimeSameAsDuration) {
                        minWorkTime += duration;
                        maxWorkTime += duration;
                    }

                    result.minimum_time = CalculatorServices.getRoundedTime(minWorkTime);
                    result.maximum_time = CalculatorServices.getRoundedTime(maxWorkTime);
                    result.crew = moversCount;

                    var truckNeeded = CalculatorServices.getTrucks(total_cf);

                    result.truckCount = truckNeeded;
                    result.rate = CalculatorServices.getRate(tmpRequest.field_date.date, moversCount, truckNeeded);
                    return result;
                }
            };

            function _createStorageRequest(request) {
                var deferred = $q.defer();
                $http
                    .post(config.serverUrl + 'server/move_request/storage_request', request)
                    .success(function (data, status, headers, config) {
                        //data.success = true;
                        deferred.resolve(data);
                    })
                    .error(function (data, status, headers, config) {
                        // data.success = false;
                        deferred.reject(data);

                    });
                return deferred.promise;
            }

            function _createRequest(request) {

                var deferred = $q.defer();
                $http
                    .post(config.serverUrl + 'server/move_request', request)
                    .success(function (data, status, headers, config) {
                        deferred.resolve(data);
                    })
                    .error(function (data, status, headers, config) {
                        deferred.reject(data);
                    });

                return deferred.promise;
            }

            //@NOTE: TEMP
            function getRequestsByNid(nids, callback) {
                var deferred = $q.defer();
                var data = {
                    pagesize: 500,
                    condition: {
                        nid: nids
                    }
                };
                $http.post(config.serverUrl + 'server/move_request/search', data)
                     .success(function (data, status, headers, config) {
                         deferred.resolve(data);

                     })
                     .error(function (data, status, headers, config) {
                         deferred.reject(data);

                     });
                return deferred.promise;
            }
        }


        function prepareRequest() {
            return {
                data: {
                    status: 1,
                    title: 'Move Request',
                    uid: '',
                    field_first_name: '',
                    field_last_name: '',
                    field_e_mail: '',
                    field_phone: '',
                    field_additional_phone: '',
                    field_approve: '',
                    field_move_service_type: '',
                    field_date: {date: '', time: '15:10:00'}, //"06/16/2015",
                    field_size_of_move: '',
                    field_type_of_entrance_from: '',
                    field_type_of_entrance_to_: '',

                    field_start_time: '',
                    field_price_per_hour: '',
                    field_movers_count: '',
	                field_delivery_crew_size: '',
                    field_suggested_truck: '',

                    field_flat_rate: '',
                    field_estimated_total_time: '',
                    field_estimated_prise: '',

                    field_distance: '',
                    field_travel_time: '',
                    field_minimum_move_time: '',
                    field_maximum_move_time: '',
                    field_duration: '',

                    field_extra_furnished_rooms: '',
                    field_moving_to: {
                        country: "US",
                        administrative_area: '',
                        locality: '',
                        postal_code: '',
                        thoroughfare: ''
                    },
                    field_apt_from: '',
                    field_apt_to: '',
                    field_moving_from: {
                        country: "US",
                        administrative_area: '',
                        locality: '',
                        postal_code: '',
                        thoroughfare: ''
                    },
                    field_storage_price: '',
                    field_reservation_price: ''
                },
                account: {
                    name: '',
                    mail: '',
                    fields: {
                        field_user_first_name: '',
                        field_user_last_name: '',
                        field_user_additional_phone: '',
                        field_primary_phone: ''
                    }
                },
                admin: {
                    name: 'true'
                }
            };

        }

        function registerNewRequest(requestData, cb) {
            if (typeof requestData != 'object') {
                return false;
            }

            return new InitService(requestData, cb);
        }


        return {
            registerNewRequest: registerNewRequest,
            prepareRequest: prepareRequest,
        }
    }
})();
