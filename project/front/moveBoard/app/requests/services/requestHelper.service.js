'use strict';

angular
	.module('move.requests')
	.factory('RequestHelperService', RequestHelperService);

/*@ngInject*/
function RequestHelperService($rootScope, additionalServicesFactory, CalculatorServices, $q, CommercialCalc, datacontext) {

	let fieldData = datacontext.getFieldData();
	let calcSettings = angular.fromJson(fieldData.calcsettings);

	return {
		addOvernightExtraService: addOvernightExtraService,
		removeOvernightExtraService: removeOvernightExtraService,
		recalculateCountOvernightExtraService: recalculateCountOvernightExtraService,
		fetchTotalWeight: fetchTotalWeight,
	};

	function fetchTotalWeight (request) {
		let weight = {};

		if (request.move_size.raw != 11) {
			weight = CalculatorServices.getRequestCubicFeet(request);
		} else {
			weight = CommercialCalc.getCommercialCubicFeet(request.commercial_extra_rooms.value, calcSettings, request.field_custom_commercial_item);
		}

		return weight;
	}

	function addOvernightExtraService(request) {
		let defer = $q.defer();
		let add_extra_charges = [];

		if (angular.isDefined(request.extraServices)) {
			if (_.isString(request.extraServices)) {
				add_extra_charges = angular.fromJson(request.extraServices);
			} else {
				add_extra_charges = angular.copy(request.extraServices);
			}
		}

		let overnightExtra = makeOvernightExtraCharge(request);
		let overnightCharge = _.find(add_extra_charges, {index: 1});

		if (_.isUndefined(overnightCharge)) {
			add_extra_charges.push(overnightExtra);
			return saveExtraCharges(request, add_extra_charges);
		} else {
			defer.resolve();
		}
		return defer.promise;
	}

	function removeOvernightExtraService(request) {
		let defer = $q.defer();
		let add_extra_charges = [];

		if (angular.isDefined(request.extraServices)) {
			if (_.isString(request.extraServices)) {
				add_extra_charges = angular.fromJson(request.extraServices);
			} else {
				add_extra_charges = angular.copy(request.extraServices);
			}
		}

		let index = _.findIndex(add_extra_charges, {index: 1});

		if (index != -1) {
			add_extra_charges.splice(index, 1);
			return saveExtraCharges(request, add_extra_charges);
		} else {
			defer.resolve();
		}
		return defer.promise;
	}

	function recalculateCountOvernightExtraService(request) {
		let defer = $q.defer();
		let add_extra_charges = [];

		if (angular.isDefined(request.extraServices)) {
			if (_.isString(request.extraServices)) {
				add_extra_charges = angular.fromJson(request.extraServices);
			} else {
				add_extra_charges = angular.copy(request.extraServices);
			}
		}

		let index = _.findIndex(add_extra_charges, {index: 1});

		if (index != -1) {
			let truckCount = calculateTruckCount(request);
			add_extra_charges[index].extra_services[1].services_default_value = truckCount;
			saveExtraCharges(request, add_extra_charges)
				.then(
					res => {
						defer.resolve();
					},
					rej => {
						defer.reject();
					}
				);
		} else {
			defer.resolve();
		}

		return defer.promise;
	}

	function makeOvernightExtraCharge(request) {
		let basicSettings = angular.fromJson($rootScope.fieldData.basicsettings);

		if (_.isUndefined(basicSettings.overnight)) {
			basicSettings.overnight = {};
		}

		if (_.isUndefined(basicSettings.overnight.rate)) {
			basicSettings.overnight.rate = 0;
		}

		let truckCount = calculateTruckCount(request);

		let result = {
			name: 'Overnight Storage',
			index: 1,
			edit: true,
			extra_services: [
				{
					services_default_value: basicSettings.overnight.rate,
					services_name: "Cost",
					services_read_only: false,
					services_type: "Amount"
				},
				{
					services_default_value: truckCount,
					services_name: "Value",
					services_read_only: false,
					services_type: "Number"
				}
			]
		};

		return result;
	}

	function calculateTruckCount(request) {
		let weight = {weight: 0};

		if (angular.isDefined(request.inventory_weight) && angular.isDefined(request.inventory_weight.cfs) && request.inventory_weight.cfs != 0) {
			weight.weight = _.clone(request.inventory_weight.cfs);
		} else {
			weight = CalculatorServices.getCubicFeet(request);
		}

		let result = CalculatorServices.getTrucks(weight);

		return result;
	}

	function saveExtraCharges(request, add_extra_charges) {
		let defer = $q.defer();
		let data = {
			nid: request.nid,
			charges: add_extra_charges
		};

		request.extraServices = add_extra_charges;

		additionalServicesFactory
			.saveExtraServices(request.nid, add_extra_charges)
			.then(
				res => {
					$rootScope.$broadcast('request.payment_update', data);
					defer.resolve();
				},
				rej => {
					defer.reject();
				}
			);

		return defer.promise;
	}
}
