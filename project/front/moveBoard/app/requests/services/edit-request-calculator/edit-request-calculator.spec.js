describe('Edit request calculator service', () => {
	let $scope, service, $q, increaseReservationPrice, request, RequestServices, moment_lib, scheduleSettings, datacontext;

	beforeEach(inject((_editRequestCalculatorService_, _$q_, _CalculatorServices_, _RequestServices_, _moment_, _datacontext_) => {
		scheduleSettings = testHelper.loadJsonFile('edit-request-calculator__schedule-settings.mock');
		datacontext = _datacontext_;
		spyOn(datacontext, 'getFieldData').and.returnValue({schedulesettings: scheduleSettings});
		service = _editRequestCalculatorService_;
		$q = _$q_;
		$scope = $rootScope.$new();
		request = testHelper.loadJsonFile('edit-request-calculator__request-for-increase-reservation-price.mock');
		RequestServices = _RequestServices_;
		moment_lib = _moment_;
		increaseReservationPrice = {result: 10};
	}));

	it('service to be defined', () => {
		//given

		//when

		//then
		expect(service).toBeDefined();
	});

	describe('increaseReservationPrice should,', () => {
		beforeEach(() => {
			spyOn(service, 'calculateReservationPrice')
				.and
				.callFake(() => { return increaseReservationPrice.result});
		});

		it('be defined', () => {
			//given
			//when
			//then
			expect(service.increaseReservationPrice).toBeDefined();
		});

		describe('reject when', () => {
			it('status not equal to not confirmed', () => {
				//given
				let result;
				let expectedResult = 'rejected';
				request.status.raw = 3;

				//when
				service.increaseReservationPrice(request)
					.then(() => {result = 'resolved'}, () => {result = 'rejected'});
				$scope.$apply();

				//then
				expect(result).toBe(expectedResult);
			});

			it('reservation price already increased', () => {
				//given
				let result;
				let expectedResult = 'rejected';
				request.request_all_data.reservationIncreased = true;

				//when
				service.increaseReservationPrice(request)
					.then(() => {result = 'resolved'}, () => {result = 'rejected'});
				$scope.$apply();

				//then
				expect(result).toBe(expectedResult);
			});

			it('increase reservation price is equal to zero', () => {
				//given
				let result;
				let expectedResult = 'rejected';
				increaseReservationPrice.result = 0;

				//when
				service.increaseReservationPrice(request)
					.then(() => {result = 'resolved'}, () => {result = 'rejected'});
				$scope.$apply();

				//then
				expect(result).toBe(expectedResult);
			});

			it('reservation price was received', () => {
				//given
				let result;
				let expectedResult = 'rejected';
				request.field_reservation_received.value = true;

				//when
				service.increaseReservationPrice(request)
					.then(() => {result = 'resolved'}, () => {result = 'rejected'});
				$scope.$apply();

				//then
				expect(result).toBe(expectedResult);
			});

			it('calculator turned off', () => {
				//given
				let result;
				let expectedResult = 'rejected';
				request.field_usecalculator.value = false;

				//when
				service.increaseReservationPrice(request)
					.then(() => {result = 'resolved'}, () => {result = 'rejected'});
				$scope.$apply();

				//then
				expect(result).toBe(expectedResult);
			});

			it('when not save field_reservation_price', () => {
				//given
				let result;
				let expectedResult = 'rejected';
				spyOn(RequestServices, 'updateRequest').and
					.callFake(() => {return {then: () => {return {catch: (errorCallback) => {errorCallback()}}}}});

				//when
				service.increaseReservationPrice(request)
					.then(() => {result = 'resolved'}, () => {result = 'rejected'});
				$scope.$apply();

				//then
				expect(result).toBe(expectedResult);
			});
		});

		describe('when resolve should be', () => {
			beforeEach(() => {
				increaseReservationPrice.result = 10;
			});

			it('increased reservation rate on 10', () => {
				//given
				let result;
				let expectedResult = 110;

				//when
				service.increaseReservationPrice(request);
				result = request.reservation_rate.value;

				//then
				expect(result).toBe(expectedResult);
			});

			it('called updateRequest with two parameters', () => {
				//given
				spyOn(RequestServices, 'updateRequest').and.callThrough();
				let nid = 1;
				let fields = { field_reservation_price : 110 };

				//when
				service.increaseReservationPrice(request);

				//then
				expect(RequestServices.updateRequest).toHaveBeenCalledWith(nid, fields);
			});

			describe('when update request', () => {
				beforeEach(() => {
					spyOn(RequestServices, 'updateRequest').and.callFake(() => {
						return {
							then: (successCallBack) => {
								successCallBack();
								return {catch: () => {}}
							}
						}
					});
					spyOn(RequestServices, 'sendLogs').and.callFake(() => {});
				});

				it('called sendLogs with parameters', () => {
					//given
					let nid = 1;
					let logs = [ { simpleText : 'Reservation price:', html : 'Was changed automatically from $100.00 to $110' } ];
					let logTitle = 'Request was updated';
					let logType = 'MOVEREQUEST';

					//when
					service.increaseReservationPrice(request);
					$scope.$apply();

					//then
					expect(RequestServices.sendLogs).toHaveBeenCalledWith(logs, logTitle, nid, logType);
				});

				it('reservation rate old should be 110', () => {
					//given
					let expectedResult = '110';

					//when
					service.increaseReservationPrice(request);
					$scope.$apply();

					//then
					expect(request.reservation_rate.old).toBe(expectedResult);
				});

				it('reservation Increased should be true', () => {
					//given
					let expectedResult = true;

					//when
					service.increaseReservationPrice(request);

					//then
					expect(request.request_all_data.reservationIncreased).toBe(expectedResult);
				});

				it('when reservation rate increased should be resolve', () => {
					//given
					let expectedResult = 'resolved';
					let result;

					//when
					service.increaseReservationPrice(request)
						.then(() => {result = 'resolved'}, () => {result = 'rejected'});
					$scope.$apply();

					//then
					expect(result).toBe(expectedResult);
				})
			});
		});
	});

	describe('calculateReservationPrice should return', () => {
		let today;
		let result;
		let expectedResult;

		beforeEach(() => {
			today = moment('03/05/2018');

			spyOn(window, 'moment').and.callFake(function(parameter) {
				if (parameter) {
					return moment_lib(parameter);
				} else {
					return today;
				}
			});

			expectedResult = 0;
		});

		describe('0', () => {
			it('when flat rate', () => {
				//given
				request.service_type.raw = 5;

				//when
				result = service.calculateReservationPrice(request);

				//then
				expect(result).toBe(expectedResult)
			});

			it('when long distance', () => {
				//given
				request.service_type.raw = 5;

				//when
				result = service.calculateReservationPrice(request);

				//then
				expect(result).toBe(expectedResult)
			});

			it('when time earlier than in settings', () => {
				//given

				//when
				result = service.calculateReservationPrice(request);

				//then
				expect(result).toBe(expectedResult)
			});

			it('when request quote is undefined', () => {
				//given
				request.date.value = '03/06/2018';
				request.quote.max = undefined;
				service.init();

				//when
				result = service.calculateReservationPrice(request);

				//then
				expect(result).toBe(expectedResult);
			});
		});

		it('75 when choosed percent of max quote', () => {
			//given
			request.date.value = '03/07/2018';
			expectedResult = 75;
			service.init();

			//when
			result = service.calculateReservationPrice(request);

			//then
			expect(result).toBe(expectedResult);
		});

		it('100 when selected increased rate', () => {
			//given
			request.date.value = '03/07/2018';
			expectedResult = 100;
			scheduleSettings.ReservationRateIncreaseRate = 100;
			scheduleSettings.ReservationRateIncreaseRatePercent = '0';
			service.init();

			//when
			let result = service.calculateReservationPrice(request);

			//then
			expect(result).toBe(expectedResult);
		});

		it('31 when increase round reservation based on quota', () => {
			//given
			request.date.value = '03/07/2018';
			expectedResult = 31;
			request.quote.max = 123;
			scheduleSettings.ReservationRateIncreaseRate = 0;
			scheduleSettings.ReservationRateIncreaseRatePercent = '25';
			service.init();

			//when
			let result = service.calculateReservationPrice(request);

			//then
			expect(result).toBe(expectedResult);
		});
	});
});