angular
	.module('move.requests')
	.factory('editRequestCalculatorService', editRequestCalculatorService);

/*@ngInject*/
function editRequestCalculatorService($q, RequestServices, datacontext, CalculatorServices) {
	let scheduleData = {};
	let service = {};
	init();

	service.init = init;
	service.increaseReservationPrice = increaseReservationPrice;
	service.calculateReservationPrice = calculateReservationPrice;

	return service;

	function init() {
		scheduleData = angular.fromJson(datacontext.getFieldData().schedulesettings);
	}

	function increaseReservationPrice(request, reservationIncreased) {
		let defer = $q.defer();

		let isReservationPriceNotIncreased = !_.get(request, 'request_all_data.reservationIncreased') || reservationIncreased;
		let isNotConfirmed = request.status.raw == 2 || reservationIncreased;
		let increasedRequestReservation = service.calculateReservationPrice(request);
		let isNotReceivedReservation = !request.field_reservation_received.value;
		let isTurnOnCalculator = request.field_usecalculator.value;

		if (isNotConfirmed && isTurnOnCalculator && isReservationPriceNotIncreased && isNotReceivedReservation) {
			request.reservation_rate.value += increasedRequestReservation;
			request.request_all_data.reservationIncreased = !!increasedRequestReservation;

			if (request.reservation_rate.value != request.reservation_rate.old && request.request_all_data.reservationIncreased) {
				RequestServices.updateRequest(request.nid, {field_reservation_price: request.reservation_rate.value})
					.then(function () {
						let logs = _makeIncreaseReservationLogs(request.reservation_rate);
						RequestServices.sendLogs(logs, 'Request was updated', request.nid, 'MOVEREQUEST');

						request.reservation_rate.old = parseInt(request.reservation_rate.value).toString();
						defer.resolve();
					})
					.catch(() => {
						request.request_all_data.reservationIncreased = !request.request_all_data.reservationIncreased;
						defer.reject()
					});

			} else {
				defer.reject();
			}

		} else {
			defer.reject();
		}

		return defer.promise;
	}

	function _makeIncreaseReservationLogs(reservation_rate) {
		let simpleText = 'Reservation price:';
		let html = 'Was changed automatically from $' + reservation_rate.old + ' to $' + reservation_rate.value;
		let log = {simpleText, html};

		return [log];
	}

	function calculateReservationPrice(request) {
		let reservation = 0;

		let now = moment().unix();
		let then = moment(request.date.value).unix();
		let interval = (then - now) / 3600; // in hours
		let beforeHours = scheduleData.whenReservationRateIncrease;
		let incrRate = scheduleData.ReservationRateIncreaseRate;
		let incrQuote = scheduleData.ReservationRateIncreaseRatePercent;

		if (beforeHours >= interval) {
			reservation = incrRate;

			if (!reservation) {
				if (request.service_type.raw == 5) {
					reservation = _.get(request, 'flat_rate_quote.value', 0) * parseFloat(incrQuote) / 100;
				} else if (request.service_type.raw == 7) {
					reservation = CalculatorServices.getLongDistanceQuote(request) * parseFloat(incrQuote) / 100;
				} else {
					reservation = _.get(request, 'quote.max', 0) * parseFloat(incrQuote) / 100;
				}
			}
		}

		return _.round(reservation, 0);
	}
}
