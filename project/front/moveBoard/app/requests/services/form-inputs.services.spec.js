describe('Service: form-inputs.services,', () => {
	let $scope, formInputsServices, SweetAlert;
	const MOMENT_IS_SAME_LIMIT = 'day';
	beforeEach(inject((_formInputsServices_, _SweetAlert_) => {
		formInputsServices = _formInputsServices_;
		SweetAlert = _SweetAlert_;
	}));
	
	describe('After initialization,', () => {
		it('Should have function getDayClass', () => {
			expect(formInputsServices.getDayClass).toBeDefined();
		});
		it('Should have function askForChangeDeliveryDate', () => {
			expect(formInputsServices.askForChangeDeliveryDate).toBeDefined();
		});
		it('Should have function changeAndSaveSimpleDateFieldClosing', () => {
			expect(formInputsServices.changeAndSaveSimpleDateFieldClosing).toBeDefined();
		});
		it('Should have function changeAndSaveSimpleDateFieldSales', () => {
			expect(formInputsServices.changeAndSaveSimpleDateFieldSales).toBeDefined();
		});
		it('Should have function findFirstUnblockedDay', () => {
			expect(formInputsServices.findFirstUnblockedDay).toBeDefined();
		});
	});
	
	describe('function getDayClass,', () => {
		it('Should return \'testClass\'', () => {
			let className = formInputsServices.getDayClass(moment('12/10/2018'));
			expect(className).toEqual('Regular');
		});
	});
	
	describe('function askForChangeDeliveryDate,', () => {
		it('Should ask if Pickup date is greater than delivery date', () => {
			spyOn(SweetAlert, 'swal').and.callFake((params, cb) => {
				cb();
			});
			let today = moment('12/10/2018');
			let yesterday = today.clone().subtract(1, 'days');
			let request = {inventory: {move_details: {delivery: yesterday}}};
			formInputsServices.askForChangeDeliveryDate(today, request);
			expect(SweetAlert.swal).toHaveBeenCalled();
		});
		it('Should NOT ask if Pickup date is greater than delivery date', () => {
			spyOn(SweetAlert, 'swal').and.callFake((params, cb) => {
				cb();
			});
			let today = moment('12/10/2018');
			let yesterday = today.clone().add(1, 'days');
			let request = {inventory: {move_details: {delivery: yesterday}}};
			formInputsServices.askForChangeDeliveryDate(today, request);
			expect(SweetAlert.swal).not.toHaveBeenCalled();
		});
	});
	
	describe('function: changeAndSaveSimpleDateFieldClosing,', () => {
		let $scope, newVal, momentNewVal;
		
		beforeEach(() => {
			$scope = {};
			$scope.editRequestinvoice = {};
			$scope.editRequestinvoice.invoice = {};
			$scope.messageInvoice = {};
			$scope.request = {};
			$scope.request.date = {};
			$scope.request.status = {};
			$scope.request.status.raw = 3;
			$scope.invoice = {};
			$scope.invoice.date = {};
			$scope.invoice.status = {};
			$scope.invoice.status.raw = 3;
			
			momentNewVal = moment('12/10/2018');
			newVal = momentNewVal.format('MMMM D, YYYY');
			formInputsServices.changeAndSaveSimpleDateFieldClosing($scope, newVal, 'field_date');
		});
		
		it('Should write info in invoice', () => {
			expect($scope.invoice.date.raw).toEqual(momentNewVal.clone().startOf(MOMENT_IS_SAME_LIMIT).unix());
			expect($scope.invoice.date.value).toEqual(momentNewVal.format('MM/DD/YYYY'));
		});
		
		it('Should write info in editRequestInvoice', () => {
			expect($scope.editRequestinvoice.invoice.field_date.time).toEqual('15:10:00');
			expect($scope.editRequestinvoice.invoice.field_date.date).toEqual(momentNewVal.format('YYYY-MM-DD'));
		});
		
		it('Should write info in messageInvoice', () => {
			expect($scope.messageInvoice.field_date.label).toEqual('Invoice Date');
			expect($scope.messageInvoice.field_date.oldValue).toEqual('');
			expect($scope.messageInvoice.field_date.newValue).toEqual(newVal);
		});
	});
	
	describe('function: changeAndSaveSimpleDateFieldSales,', () => {
		let $scope, newVal, momentNewVal;
		
		beforeEach(() => {
			$scope = {};
			$scope.editrequest = {};
			$scope.editRequestinvoice = {};
			$scope.editRequestinvoice.invoice = {};
			$scope.message = {};
			$scope.messageInvoice = {};
			$scope.request = {};
			$scope.request.date = {};
			$scope.request.status = {};
			$scope.request.status.raw = 3;
			$scope.invoice = {};
			$scope.invoice.date = {};
			$scope.invoice.status = {};
			$scope.invoice.status.raw = 3;
			$scope.callback = () => {};
			
			momentNewVal = moment('12/10/2018');
			newVal = momentNewVal.format('MMMM D, YYYY');
			spyOn(formInputsServices, 'changeAndSaveSimpleDateFieldClosing');
			formInputsServices.changeAndSaveSimpleDateFieldSales($scope, newVal, 'field_date', $scope.callback);
		});
		
		it('Should call changeAndSaveSimpleDateFieldSales if confirmed', () => {
			expect(formInputsServices.changeAndSaveSimpleDateFieldClosing).toHaveBeenCalledWith($scope, newVal, 'field_date', $scope.callback);
		});
		
		it('Should write info in invoice', () => {
			expect($scope.request.date.raw).toEqual(momentNewVal.clone().startOf(MOMENT_IS_SAME_LIMIT).unix());
			expect($scope.request.date.value).toEqual(momentNewVal.format('MM/DD/YYYY'));
		});
		
		it('Should write info in editRequestInvoice', () => {
			expect($scope.editrequest.field_date.time).toEqual('15:10:00');
			expect($scope.editrequest.field_date.date).toEqual(momentNewVal.format('YYYY-MM-DD'));
		});
		
		it('Should write info in messageInvoice', () => {
			expect($scope.message.field_date.label).toEqual('Date');
			expect($scope.message.field_date.oldValue).toEqual('');
			expect($scope.message.field_date.newValue).toEqual(newVal);
		});
	});
});