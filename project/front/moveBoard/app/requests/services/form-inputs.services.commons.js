export const LABELS_FOR_MESSAGES = {
	'field_date': 'Date',
	'field_delivery_date': 'Delivery date',
	'field_delivery_date_second': 'Delivery date second',
	'field_schedule_delivery_date': 'Schedule delivery date'
};
export const LABELS_FOR_MESSAGES_INVOICE = {
	'field_date': 'Invoice Date',
	'field_delivery_date': 'Invoice delivery date',
	'field_delivery_date_second': 'Invoice delivery date second',
	'field_schedule_delivery_date': 'Invoice Schedule delivery date'
};

export const VARIABLE_NAMES = {
	'field_date': 'date',
	'field_delivery_date': 'ddate',
	'field_delivery_date_second': 'ddate_second',
	'field_schedule_delivery_date': 'schedule_delivery_date',
};