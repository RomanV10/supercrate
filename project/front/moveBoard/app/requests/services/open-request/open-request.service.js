'use strict';

angular
	.module('move.requests')
	.factory('openRequestService', openRequestService);

/*@ngInject*/
function openRequestService(erDeviceDetector, EditRequestServices, RequestServices, $state, logger, $q) {
	let service = {
		open,
	};

	return service;

	function open(nid) {
		let defer = $q.defer();

		if (erDeviceDetector.isMobile) {
			$state.go('mobile.openEditRequest', {id: nid});
			defer.resolve();
		} else {
			RequestServices.getSuperRequest(nid)
				.then(function (data) {
					let requests = data.requests_day;
					let open_request = data.request;

					EditRequestServices.openModal(open_request, requests, data);
					defer.resolve(data);
				}, function (reason) {
					logger.error(reason, reason, 'Error');
					defer.reject();
				});
		}

		return defer.promise;
	}
}
