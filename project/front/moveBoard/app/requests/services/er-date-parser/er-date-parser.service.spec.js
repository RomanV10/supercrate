describe('er date parser: ', () => {
	let erDateFormatParser;
	let expectedResult;
	let result;
	let deliveryDate;

	beforeEach(inject((_erDateFormatParser_) => {
		erDateFormatParser = _erDateFormatParser_;
	}));

	describe('Should parse ', () => {
		it('MMMM D, YYYY', () => {
			// given
			deliveryDate = 'November 22, 2018';
			expectedResult = 'MMMM D, YYYY';
			result = '';

			// when
			result = erDateFormatParser.getDeliveryDateFormat(deliveryDate);

			// then
			expect(result).toEqual(expectedResult);
		});

		it('MM/DD/YYYY', () => {
			// given
			deliveryDate = '11/22/2018';
			expectedResult = 'MM/DD/YYYY';
			result = '';

			// when
			result = erDateFormatParser.getDeliveryDateFormat(deliveryDate);

			// then
			expect(result).toEqual(expectedResult);
		});

		it('YYYY-MM-DD', () => {
			// given
			deliveryDate = '2018-11-22';
			expectedResult = 'YYYY-MM-DD';
			result = '';

			// when
			result = erDateFormatParser.getDeliveryDateFormat(deliveryDate);

			// then
			expect(result).toEqual(expectedResult);
		});
	});
});