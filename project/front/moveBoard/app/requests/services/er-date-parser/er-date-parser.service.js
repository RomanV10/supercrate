angular
	.module('move.requests')
	.factory('erDateFormatParser', erDateFormatParser);

/*@ngInject*/
function erDateFormatParser() {
	let service = {
		getDeliveryDateFormat,
	};

	return service;

	function getDeliveryDateFormat(value) {
		let result = '';

		if (~value.indexOf(',')) {
			result = 'MMMM D, YYYY';
		}

		if (~value.indexOf('/')) {
			result = 'MM/DD/YYYY';
		}

		if (~value.indexOf('-')) {
			result = 'YYYY-MM-DD';
		}

		return result;
	}
}
