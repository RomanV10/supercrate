angular
	.module('move.requests')
	.factory('EditRequestServices', EditRequestServices);

EditRequestServices.$inject = ['$rootScope', '$uibModal', 'common', 'logger', 'RequestServices', 'datacontext', 'InventoriesServices', '$q', 'apiService', 'moveBoardApi'];

function EditRequestServices($rootScope, $uibModal, common, logger, RequestServices, datacontext, InventoriesServices, $q, apiService, moveBoardApi) {
	let service = {};
	const NOT_FOUND_STATUS_CODE = 406;

	service.openModal = openModal;
	service.sendLogsAfterSaveChanges = sendLogsAfterSaveChanges;
	service.checkAddresses = checkAddresses;
	service.updateRequestLive = updateRequestLive;
	service.requestEditModal = requestEditModal;
	service.groupRequestModal = groupRequestModal;
	service.addModalInstance = addModalInstance;
	service.removeModalInstance = removeModalInstance;
	service.getModalInstances = getModalInstances;
	service.getModalInstance = getModalInstance;
	service.repositionModals = repositionModals;
	service.setActiveModal = setActiveModal;
	service.openSendRequestToSITModal = openSendRequestToSITModal;
	service.saveBookedStatistics = saveBookedStatistics;
	service.getNotes = getNotes;
	service.getBranchNotes = getBranchNotes;
	service.setNotes = setNotes;
	service.getQuickBookFlag = getQuickBookFlag;
	service.unbindPackingDay = unbindPackingDay;
	service.updateInvoiceFields = updateInvoiceFields;

	return service;

	function updateInvoiceFields(invoice, editInvoice) {
		for (var invoiceField in invoice) {
			if (editInvoice[invoiceField]) {
				if (angular.isDefined(invoice[invoiceField].value) && angular.isDefined(editInvoice[invoiceField].value)) {
					invoice[invoiceField].value = editInvoice[invoiceField].value;
				}

				if (angular.isDefined(invoice[invoiceField].raw) && angular.isDefined(editInvoice[invoiceField].raw)) {
					invoice[invoiceField].raw = editInvoice[invoiceField].raw;
				}
			}

			if (angular.isDefined(invoice[invoiceField]) && invoice[invoiceField] != null) {
				if (angular.isDefined(invoice[invoiceField].old) && invoice[invoiceField].value) {
					invoice[invoiceField].old = angular.copy(invoice[invoiceField].value);
				}

				if (invoiceField == 'move_size'
					|| invoiceField == 'service_type'
					|| invoiceField == 'status'
					|| invoiceField == 'type_from'
					|| invoiceField == 'type_to') {
					invoice[invoiceField].old = angular.copy(invoice[invoiceField].raw);
				}

				if (invoiceField == 'work_time_old') {
					invoice.work_time_old = angular.copy(invoice.work_time);
				}
			}
		}
	}

	function unbindPackingDay(unbindData) {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.request.unbindPackingDay, unbindData)
			.then(res => {
				if (res.data.status === 200) {
					defer.resolve();
				} else {
					defer.reject();
				}
			});

		return defer.promise;
	}

	function sendLogsAfterSaveChanges(messages, request) {
		var arr = [];

		_.forEach(messages, function (message) {
			if (message.label == 'notes') {
				return false;
			}

			var msg = {
				from: '',
				to: '',
				text: ''
			};

			if ( (angular.isUndefined(message.oldValue) || message.oldValue == null || message.oldValue == '')
				&& message.label != 'custom') {
				msg.text = message.label;
				msg.to = message.newValue;
			}

			if (message.oldValue) {
				if (!message.oldValue.length && message.label != 'custom') {
					msg.text = message.label;
					msg.to = message.newValue;
				} else if (message.oldValue.length && message.label != 'custom') {
					msg.text = message.label;
					msg.to = message.newValue;
					msg.from = message.oldValue;
				}
			}

			if (msg.to != msg.from) {
				arr.push(msg);
			}

			if (message.label == 'custom') {
				msg.text = message.newValue;
				arr.push(msg);
			}
		});

		$rootScope.$broadcast('request.sendLog', {logs: arr}, request.nid);
	}

	function checkAddresses(req) {
		if (req.service_type.raw == 3 && (_.isEmpty(req.field_moving_from.administrative_area) || _.isEmpty(req.field_moving_from.postal_code))) {
			return true;
		} else if (req.service_type.raw == 4 && (_.isEmpty(req.field_moving_to.administrative_area) || _.isEmpty(req.field_moving_to.postal_code))) {
			return true;
		} else if (req.service_type.raw != 4 && req.service_type.raw != 3 && (_.isEmpty(req.field_moving_from.administrative_area) || _.isEmpty(req.field_moving_from.postal_code) || _.isEmpty(req.field_moving_to.administrative_area) || _.isEmpty(req.field_moving_to.postal_code))) {
			return true;
		}
		return false;
	}

	function addModalInstance(request) {
		var modal = {};
		var nid = request.nid;
		var count = 0;
		modal.nid = nid;
		modal.modal = '.modalId_' + nid;

		datacontext.saveModalInstance(nid, modal);

		var modals = service.getModalInstances();

		angular.forEach(modals, function (modal) {
			modal.active = false;
			count++;
		});

		if (count > 1) {
			common.$timeout(function () {
				$(modal.modal + ' .modal-dialog').css('top', '30px');
				$(modal.modal + ' .modal-dialog').css('right', '-35px');
			}, 500);

		}

		modal.active = true;
	}

	function removeModalInstance(nid) {
		let modals = service.getModalInstances();
		let count = 0;
		let i = 0;

		angular.forEach(modals, () => count++);

		// delete old styles
		angular.forEach(modals, function (modal, nid) {
			i++;
			let style = Math.round(100 / count);

			for (let k = 1; k <= count; k++) {
				$(modal.modal).removeClass('toggled' + style + ' pos' + k);
			}
		});

		datacontext.removeModalInstance(nid);
	}

	function getModalInstance(nid) {
		return datacontext.getModalInstance(nid);
	}

	function getModalInstances() {
		return datacontext.getModalInstances();
	}

	function setActiveModal() {
		var modals = service.getModalInstances();
		var count = 0;

		angular.forEach(modals, () => count++);

		if (count >= 2) {
			angular.forEach(modals, function (modal) {
				if (modal.active) {
					$(modal.modal).css('z-index', 1050 - 2);
					modal.active = false;
				} else {
					$(modal.modal).css('z-index', 1050 + 2);
					modal.active = true;
				}
			});
		}
	}

	function repositionModals() {
		var modals = service.getModalInstances();
		var count = 0;
		var i = 0;

		angular.forEach(modals, function (modal, i) {
			count++;
		});
		// delete old styles
		angular.forEach(modals, function (modal, nid) {
			i++;
			var cc = count - 2;
			var style = Math.round(100 / cc);

			$(modal.modal).removeClass('toggled' + style + ' pos' + i);
		});

		i = 0;

		// add new styles
		angular.forEach(modals, function (modal, nid) {
			i++;
			var style = Math.round(100 / count);
			$(modal.modal).addClass('toggled' + style + ' pos' + i);
			$(modal.modal + ' .modal-dialog').css('top', '0px');
		});
	}

	function updateRequestLive(OldRequest, NewRequest) {
		var fieldData = datacontext.getFieldData();
		angular.forEach(NewRequest, function (value, field_name) {
			angular.forEach(OldRequest, function (request_field, request_field_name) {
				if (request_field != null) {
					if (angular.isDefined(request_field.field) && request_field.field == field_name) {
						if (request_field.field == 'field_maximum_move_time' ||
							request_field.field == 'field_travel_time' ||
							request_field.field == 'field_double_travel_time' ||
							request_field.field == 'field_minimum_move_time') {
							OldRequest[request_field_name].raw = value;
							OldRequest[request_field_name].value = common.decToTime(value);
						} else if (request_field.field == 'field_date') {
							OldRequest[request_field_name].raw = moment(value.date).unix();
							OldRequest[request_field_name].value = moment(value.date).format("MM/DD/YYYY");
						} else {
							OldRequest[request_field_name].raw = value;

							if (angular.isDefined(fieldData.field_lists[field_name])) {
								OldRequest[request_field_name].value = fieldData.field_lists[field_name][value];
							} else {
								OldRequest[request_field_name].value = value;
							}
						}
					}
				}
			});
		});

		return OldRequest;
	}

	function requestEditModal(request) {
		var defer = $q.defer();
		var nid = angular.isDefined(request.nid) ? request.nid : request;
		var superPromise = RequestServices.getSuperRequest(nid);

		superPromise
			.then(function (data) {
				defer.resolve();
				service.openModal(data.request, data.requests_day, data);
			}, function (reason) {
				defer.reject();
				logger.error(reason, reason, 'Error');
			});

		return defer.promise;
	}

	function groupRequestModal(request, pairedRequest) {
		$uibModal.open({
			template: require('./templates/groupRequestsTemplate.html'),
			controller: ['$scope', '$uibModalInstance', 'initial_request', 'paired_request', 'RequestServices', 'datacontext', 'RequestHelperService', GroupRequestCtrl],
			size: 'md',
			backdrop: false,
			resolve: {
				initial_request: function () {
					return request;
				},
				paired_request: function () {
					return pairedRequest;
				}
			}
		});
	}

	/*@ngInject*/
	function GroupRequestCtrl($scope, $uibModalInstance, initial_request, paired_request, RequestServices, datacontext, RequestHelperService) {
		var fieldData = datacontext.getFieldData();
		var basicsettings = angular.fromJson(fieldData.basicsettings);
		var isPackingRequest = initial_request.service_type.raw == 8;

		$scope.secondRequestNid = _.isUndefined(paired_request) ? '' : paired_request.nid;

		$scope.update = function () {
			$scope.busy = true;

			var noStorageEditRequest = {
				field_from_storage_move: 0,
				field_to_storage_move: 0
			};

			if ((_.isEmpty($scope.secondRequestNid) || $scope.secondRequestNid == 0) && !_.isEqual(initial_request.storage_id, 0)) {
				RequestServices.updateRequest(initial_request.storage_id, noStorageEditRequest);
				RequestServices.updateRequest(initial_request.nid, noStorageEditRequest);
				initial_request.storage_id = 0;
				$uibModalInstance.close();
				$scope.busy = false;
			} else if (isPackingRequest) {
				getRequestByNID($scope.secondRequestNid).then(function (resolve) {
					if (resolve.request_all_data.toStorage) {
						$scope.busy = false;
						toastr.error('Try another request', 'You can not bind this request');
						return false;
					} else if (initial_request.uid.mail.localeCompare(resolve.uid.mail) != 0) {
						$scope.busy = false;
						toastr.info('You cannot link requests of different users');
						return false;
					} else {
						bindingRequestToPackingDay(initial_request, $scope.secondRequestNid, resolve);
					}
				})
					.catch(() => {
						$scope.busy = false;
					});
			} else {
				getRequestByNID($scope.secondRequestNid)
					.then(function (data) {
						var storageId = data.storage_id;

						if (storageId && initial_request.nid != storageId) {
							RequestServices.updateRequest(storageId, noStorageEditRequest);
						}

						let isStorageRequest = data.service_type.raw == 2 || data.service_type.raw == 6;

						if (!_.isUndefined(paired_request) && (paired_request.service_type.raw == 2 || paired_request.service_type.raw == 6)) {

							RequestServices.updateRequest(paired_request.nid, noStorageEditRequest);
						}

						var editrequest1 = {};
						var editrequest2 = {};
						var companyAddress = {
							postal_code: basicsettings.parking_address,
							locality: basicsettings.storage_city,
							thoroughfare: basicsettings.company_name
						};
						var movedateFirst = moment(initial_request.date.value, 'MM/DD/YYYY');
						var movedateSecond = moment(data.date.value, 'MM/DD/YYYY');

						var diffDays = Math.abs(movedateFirst.diff(movedateSecond, 'days'));

						if (_.isEqual(initial_request.request_all_data.toStorage, true)) {
							editrequest1.field_to_storage_move = $scope.secondRequestNid;
							editrequest1.field_from_storage_move = 0;
							editrequest1.field_moving_from = _.clone(companyAddress);

							initial_request.field_moving_from = _.clone(companyAddress);
						} else {
							editrequest1.field_from_storage_move = $scope.secondRequestNid;
							editrequest1.field_to_storage_move = 0;
							editrequest1.field_moving_to = _.clone(companyAddress);

							initial_request.field_moving_to = _.clone(companyAddress);
						}

						if (isStorageRequest) {
							if (!_.isEqual(initial_request.request_all_data.toStorage, true)) {
								editrequest2.field_to_storage_move = initial_request.nid;
								editrequest2.field_from_storage_move = 0;
								editrequest2.field_moving_from = _.clone(companyAddress);
							} else {
								editrequest2.field_from_storage_move = initial_request.nid;
								editrequest2.field_to_storage_move = 0;
								editrequest2.field_moving_to = _.clone(companyAddress);
							}
						}

						data.request_all_data.toStorage = !initial_request.request_all_data.toStorage;

						if (diffDays == 1) {
							editrequest1.field_move_service_type = '6';
							initial_request.service_type.raw = '6';

							RequestHelperService.addOvernightExtraService(initial_request);

							if (isStorageRequest) {
								editrequest2.field_move_service_type = '6';
								RequestHelperService.addOvernightExtraService(data);
							}
						} else {
							editrequest1.field_move_service_type = '2';

							initial_request.service_type.raw = '2';
							RequestHelperService.removeOvernightExtraService(initial_request);

							if (isStorageRequest) {
								editrequest2.field_move_service_type = '2';
								RequestHelperService.removeOvernightExtraService(data);
							}
						}

						if (_.isUndefined(paired_request)) {
							paired_request = {};
						}

						angular.copy(data, paired_request);
						initial_request.storage_id = $scope.secondRequestNid;

						var listInventories = initial_request.inventory.inventory_list;
						InventoriesServices.saveListInventories(initial_request.storage_id, listInventories);

						var details = initial_request.inventory.move_details;
						RequestServices.saveDetails(initial_request.storage_id, details);

						if (isStorageRequest) {
							RequestServices.saveReqData(data.nid, data.request_all_data);
						}

						var updatePromise1 = RequestServices.updateRequest(initial_request.nid, editrequest1);
						let promises = [updatePromise1];

						if (!_.isEmpty(editrequest2)) {
							var updatePromise2 = RequestServices.updateRequest($scope.secondRequestNid, editrequest2);
							promises.push(updatePromise2);
						}

						$q.all(promises)
							.then(function () {
								$uibModalInstance.close();
								$scope.busy = false;
							}, function () {
								$scope.busy = false;
							});
					}, function () {
						$scope.busy = false;
					});
			}
		};

		function bindingRequestToPackingDay(initialRequest, bindingNid, resolve) {
			let bindingRequest = resolve;

			if (!_.isEmpty(bindingRequest.request_all_data.packing_request_id)) {
				toastr.info('Request #' + bindingNid + ' already has Packing Day request');
				$scope.busy = false;
				return false;
			}

			if (bindingRequest.service_type.raw == 8) {
				toastr.error('You can not link two Packing Day requests');
				$scope.busy = false;
				return false;
			}

			initialRequest.request_all_data.packing_request_id = bindingRequest.nid;
			bindingRequest.request_all_data.packing_request_id = initialRequest.nid;

			if (!_.isEmpty(initialRequest.inventory.inventory_list)) {
				let listInventories = initialRequest.inventory.inventory_list;
				InventoriesServices.saveListInventories(initialRequest.request_all_data.packing_request_id, listInventories);
			}

			if (!_.isEmpty(bindingRequest.inventory.inventory_list)) {
				let listInventories = bindingRequest.inventory.inventory_list;
				InventoriesServices.saveListInventories(bindingRequest.request_all_data.packing_request_id, listInventories);
			}

			RequestServices.saveReqData(bindingNid, bindingRequest.request_all_data);
			RequestServices.saveReqData(initialRequest.nid, initialRequest.request_all_data);
			toastr.success('Packing Day request was binding to ' + bindingNid);
			$scope.busy = false;
			$uibModalInstance.close();
		}

		$scope.close = function () {
			$uibModalInstance.dismiss('cancel');
		};

		$scope.isUpdateDisable = function () {
			return $scope.groupRequestId.requestuid.$invalid
				|| (!_.isUndefined(paired_request)
				&& _.isEqual(paired_request.nid, $scope.secondRequestNid))
				|| (_.isEmpty($scope.secondRequestNid)
				&& !_.isUndefined(paired_request)
				&& _.isEqual(initial_request.storage_id, 0))
				|| (_.isEmpty($scope.secondRequestNid)
				&& _.isUndefined(paired_request)
				&& _.isEqual(initial_request.storage_id, 0));
		};

		function getRequestByNID(nid) {
			let defer = $q.defer();
			let promise = RequestServices.getSuperRequest(nid);

			promise
				.then(function ({request}) {
					defer.resolve(request);
				},
				function (reason = {}) {
					if (reason.status_code === NOT_FOUND_STATUS_CODE) {
						logger.error(`Request #${nid} not found!`, '', 'Error');
					} else {
						logger.error(reason, reason, 'Error');
					}

					defer.reject();
				});

			return defer.promise;
		}
	}

	function openModal(request, requests, response, isDelivery) {
		let ch = service.getModalInstance(request.nid);

		if (angular.isUndefined(ch)) {
			$uibModal.open({
				template: require('./templates/editRequestTemplate.html'),
				controller: modalEditController,
				size: 'lg',
				backdrop: false,
				keyboard: false,
				windowClass: 'requestModal status_' + request.status.raw + ' modalId_' + request.nid,
				resolve: {
					initialRequest: function () {
						return request;
					},
					requests: function () {
						return requests;
					},
					superResponse: function () { // from get super request
						return response;
					},
					isDelivery: function () {
						return isDelivery;
					}
				},
			});
		} else {
			service.setActiveModal();
		}
	}

	function modalEditController($scope, initialRequest, requests, superResponse, isDelivery, $uibModalInstance) {
		$scope.initialRequest = initialRequest;
		$scope.requests = requests;
		$scope.superResponse = superResponse;
		$scope.isDelivery = isDelivery;
		$scope.$uibModalInstance = $uibModalInstance;
	}

	function saveBookedStatistics(request) {
		if (_.isUndefined(request.request_all_data.statistic.booked_by)) {
			request.request_all_data.statistic.booked_by = 'manager';

			if (_.isUndefined(request.request_all_data.statistic.booked_date)) {
				request.request_all_data.statistic.booked_date = moment().unix();
			}

			RequestServices.saveReqData(request.nid, request.request_all_data);
		}
	}

	function getNotes(request, roles) {
		return {
			requestNotes: apiService.postData(moveBoardApi.requestNotes.getNotes + request, roles)
		};
	}

	function getBranchNotes(request, roles, branchApi) {
		return apiService.postData(moveBoardApi.requestNotes.getNotes + request, roles, branchApi + '/');
	}

	function setNotes(request, data) {
		return {
			note: apiService.postData(moveBoardApi.requestNotes.setNotes + request, data)
		};
	}

	function getQuickBookFlag(request) {
		var showQBFlag = apiService.postData(moveBoardApi.quickbooks.checkTokenDisconnectButton);
		var qbFlag = apiService.getData(moveBoardApi.quickbooks.moveQuickbooks + request.nid);

		$q.all({showQBFlag, qbFlag})
			.then(function (response) {
				request.showQuickBooksStatus = _.head(response.showQBFlag.data);
				let quickBooksFlag = _.head(response.qbFlag.data);

				if (quickBooksFlag == 0) {
					request.quickBookFlag = 'not in';
				} else if (quickBooksFlag == 1) {
					request.quickBookFlag = 'in';
				} else {
					request.quickBookFlag = 'partly in';
				}
			});
	}

	function openSendRequestToSITModal(request) {
		$uibModal.open({
			template: require('./templates/sendRequestToSITModalTemplate.html'),
			controller: 'sendRequestToSITModal',
			size: 'md',
			backdrop: false,
			resolve: {
				request: function () {
					return request;
				}
			}
		});
	}
}
