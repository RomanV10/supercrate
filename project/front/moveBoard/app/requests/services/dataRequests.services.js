angular
	.module('move.requests')
	.factory('RequestServices', RequestServices);

/* @ngInject */
function RequestServices($http, $rootScope, $q, config, AuthenticationService, MomentWrapperService,
	apiService, moveBoardApi, TrucksServices, common, PermissionsServices, Raven) {

	var service = {};

	$rootScope.userEditable = [];
	$rootScope.managers = [];

	service.EntityTypes = {
		MOVEREQUEST: 0,
		STORAGEREQUEST: 1,
		LDREQUEST: 2,
		SYSTEM: 3
	};

	service.getDashboard = getDashboard;
	service.getAllRequests = getAllRequests;
	service.getAllRequestsDateRange = getAllRequestsDateRange;
	service.getRequest = getRequest;
	service.getRequestParklot = getRequestParklot;

	service.getSuperRequest = getSuperRequest;

	service.updateRequest = updateRequest;
	service.getDateRequests = getDateRequests;
	service.saveDateRequests = saveDateRequests;
	service.storageDateRequests = storageDateRequests;
	service.getRequestsByNid = getRequestsByNid;
	service.saveRequests = saveRequests;
	service.getRequests = getRequests;
	service.addRequest = addRequest;
	service.getStatusRequestsByPage = getStatusRequestsByPage;
	service.getStatusRequests = getStatusRequests;
	service.getAllRequestsMonthSchedule = getAllRequestsMonthSchedule;
	service.saveDetails = saveDetails;
	service.getRequestsByUid = getRequestsByUid;

	service.sendOption = sendOption;

	service.saveManagers = saveManagers;
	service.getManagers = getManagers;
	service.getManagersFromServer = getManagersFromServer;
	service.setManager = setManager;
	service.getManager = getManager;
	service.cancel = cancel;

	service.checkEditable = checkEditable;
	service.saveEditable = saveEditable;

	service.setEditable = setEditable;
	service.removeEditable = removeEditable;

	service.saveReqData = saveReqData;

	service.getPayroll = getPayroll;
	service.getPayrollInfo = getPayrollInfo;
	service.getPayrollEnumerationTypes = getPayrollEnumerationTypes;
	service.updateUserPayroll = updateUserPayroll;
	service.setPayrollInfo = setPayrollInfo;
	service.createPayroll = createPayroll;
	service.removeUserPayroll = removeUserPayroll;
	service.getRequestsByStatusDate = getRequestsByStatusDate;
	service.getClosedJobs = getClosedJobs;
	service.getOpenedJobs = getOpenedJobs;
	service.sendMail = sendMail;
	service.addFlagToRequest = addFlagToRequest;
	service.createCompanyFlag = createCompanyFlag;
	service.receiptSearch = receiptSearch;
	service.getRequestsArr = getRequestsArr;
	service.changeFlagOnTable = changeFlagOnTable;
	service.getNewComments = getNewComments;
	service.getLogs = getLogs;
	service.postLogs = postLogs;
	service.sendLogs = sendLogs;
	service.getEntities = getEntities;
	service.sendServiceLogs = sendServiceLogs;
	service.getSumCommissionFromTotal = getSumCommissionFromTotal;

	service.createRequest = createRequest;
	service.UpdatePackingRequest = UpdatePackingRequest;

	service.getRequestByStatusDateWithTimeZone = getRequestByStatusDateWithTimeZone;

	return service;

	function createRequest(request) {

		var deferred = $q.defer();
		$http
			.post(config.serverUrl + 'server/move_request', request)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function saveManagers(data) {
		$rootScope.managers = data;
	}

	function changeFlagOnTable(data) {
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/move_sales_log', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;

	}


	function getSuperRequest(nid) {
		let deferred = $q.defer();

		if (nid) {
			let getSuperRequest = apiService.postData(`${moveBoardApi.request.getSuperRequest}${nid}`);
			let getContract = apiService.postData(`${moveBoardApi.requests.getRequestContract}${nid}`);

			$q.all({getSuperRequest, getContract})
				.then(function (response) {
					let data = response.getSuperRequest.data;
					filterDeliveryFlatRateRequest(data.requests_day, data.request.date.value);
					saveEditable(nid, data.check_editable);
					saveManagers(data.managers);
					addArrivyLinkToRequest(data.arrivy_request_url, data.request);
					TrucksServices.saveTrucks(nid, data.trucks);
					data.request.contract = _.get(response, 'getContract.data.factory', {});
					data.request.messagesList = [];

					deferred.resolve(data);
				}, function (data) {
					deferred.reject(data);
				});
		} else {
			deferred.reject('Request nid is undefined');
		}

		return deferred.promise;
	}

	function addArrivyLinkToRequest(link, request) {
		request.arrivyRequestUrl = link;
	}

	function filterDeliveryFlatRateRequest(requests, deliveryDate) {
		var requestDay = MomentWrapperService.getZeroMoment(deliveryDate);

		angular.forEach(requests, function (request, d) {
			if (request.trucks && !_.isEmpty(request.trucks.raw)) {
				let uniqTrucks = _.uniq(request.trucks.raw);
				if (!_.get(request, 'trucks.old')) request.trucks.old = [];
				request.trucks.raw.length = 0;
				request.trucks.old.length = 0;
				request.trucks.raw.push(...uniqTrucks);
				request.trucks.old.push(...uniqTrucks);
			}

			if (request.service_type.raw == 5 && request.delivery_date_from.value && request.delivery_date_to.value ) {
				let dateFrom = moment.unix(request.delivery_date_from.raw);
				let dateTo = moment.unix(request.delivery_date_to.raw);
				let isDeliveryFlatRateRequest = requestDay.isBetween(dateFrom, dateTo) || requestDay.isSame(dateFrom, 'day') || requestDay.isSame(dateTo, 'day');

				request.flatRate = isDeliveryFlatRateRequest;
			}
		});
	}

	function getDashboard() {

		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/move_request/get_dashboard')
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;

	}

	function getRequestsArr(data) {
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/move_request/search', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;

	}

	function receiptSearch(data) {
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/move_request/search_receipt', {data:data, entity_type:0})
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;

	}

	function createCompanyFlag(flag) {
		var deferred = $q.defer();
		var data = {};
		data.value = [];
		for (var i in flag) {
			data.value.push(flag[i].name);
		}
		data.type = 'company_flags';
		$http.post(config.serverUrl + 'server/move_request/create_flag', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;

	}

	function addFlagToRequest(flag, nid) {
		var defer = $q.defer();
		var data = {
			data: {
				field_flags: flag
			}
		};
		$http
			.put(config.serverUrl + 'server/move_request/' + nid, data)
			.success(function (data) {
				defer.resolve(data);
			})
			.error(function (reason) {
				defer.reject(reason);
			});
		return defer.promise;
	}

	function removeUserPayroll(data) {
		var defer = $q.defer();
		$http
			.post(config.serverUrl + 'server/payroll/remove_user_payroll', data)
			.success(function (data) {
				defer.resolve(data);
			})
			.error(function (reason) {
				defer.reject(reason);
			});
		return defer.promise;
	}

	function sendMail(nid, tmp_data) {
		var defer = $q.defer();
		$http({
			method: 'POST',
			url: config.serverUrl + 'server/template_builder/send_email_template/' + nid,
			data: {
				'data': tmp_data,
			}
		}).then(function (data) {
			defer.resolve(data);
		}, function (reason) {
			defer.reject(reason);
		});

		return defer.promise;
	}

	function createPayroll(nid, data, contractType) {
		var defer = $q.defer();
		var request = {
			nid: nid,
			value: data,
			contract_type: contractType
		};
		$http
			.post(config.serverUrl + 'server/payroll/set_contract_info', request)
			.success(function (data) {
				defer.resolve(data);
			})
			.error(function (reason) {
				defer.reject(reason);
			});
		return defer.promise;
	}

	function setPayrollInfo(data) {
		var defer = $q.defer();
		$http
			.post(config.serverUrl + 'server/payroll/set_payroll_info', data)
			.success(function (data) {
				defer.resolve(data);
			})
			.error(function (reason) {
				defer.reject(reason);
			});
		return defer.promise;
	}

	function updateUserPayroll(data) {
		var defer = $q.defer();
		$http
			.post(config.serverUrl + 'server/payroll/update_user_payroll', data)
			.success(function (data) {
				defer.resolve(data);
			})
			.error(function (reason) {
				defer.reject(reason);
			});
		return defer.promise;
	}

	function getPayrollEnumerationTypes() {
		var defer = $q.defer();
		var data = {};
		$http
			.post(config.serverUrl + 'server/payroll/enumeration_types', data)
			.success(function (data) {
				defer.resolve(data);
			})
			.error(function (reason) {
				defer.reject(reason);
			});
		return defer.promise;
	}

	function getPayrollInfo() {
		var defer = $q.defer();
		var data = {};
		$http
			.post(config.serverUrl + 'server/payroll/get_workers_info', data)
			.success(function (info) {
				defer.resolve(info);
			})
			.error(function (info) {
				defer.reject(info);
			});
		return defer.promise;
	}

	function getPayroll(request, contractType) {
		var defer = $q.defer();
		$http({
			method: 'POST',
			url: config.serverUrl + 'server/payroll/get_all_payroll_info',
			data: {
				nid: request.nid,
				contract_type: contractType
			}
		}).then(function (data) {
			defer.resolve(data);
		}, function (reason) {
			defer.reject(reason);
		});
		return defer.promise;
	}


	function saveReqData(nid, info) {
		var deferred = $q.defer();
		var data = {};
		data.data = {
			'all_data': info
		};
		$http
			.put(config.serverUrl + 'server/move_request/' + nid, data)
			.success(function (data) {
				deferred.resolve(data);
				var data_info = {
					'all_data': info,
					'nid': nid,
				};
				$rootScope.$broadcast('request.all_data', data_info);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function saveEditable(nid, user) {
		$rootScope.userEditable[nid] = user;
	}

	function checkEditable(nid) {

		return $rootScope.userEditable[nid];
		/*
		 var deferred = $q.defer();
		 var data = {
		 "nid": nid
		 }
		 $http
		 .post(config.serverUrl+'server/move_request/check_editable',data)
		 .success(function(data) {
		 deferred.resolve(data);
		 })
		 .error(function(data) {
		 deferred.reject(data);
		 });
		 return deferred.promise;
		 */

	}


	function setEditable(nid, uid) {
		var deferred = $q.defer();
		var data = {
			'nid': nid,
			'uid': uid
		};
		$http
			.post(config.serverUrl + 'server/move_request/create_editable', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function removeEditable(nid) {
		var deferred = $q.defer();
		var data = {
			'nid': nid
		};
		$http
			.post(config.serverUrl + 'server/move_request/delete_editable', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;
	}


	function sendOption(data) {
		var deferred = $q.defer();
		$http
			.post(config.serverUrl + 'server/move_request/get_options_for_pickup', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});
		return deferred.promise;
	}


	function saveDetails(nid, details) {

		var data = {};
		data.data = {};
		data.data.details = details;
		var jsonDetails = JSON.stringify(data);
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/move_invertory/' + nid, jsonDetails)
			.success(function (data, status, headers, config) {

				deferred.resolve(data);

			})
			.error(function (data, status, headers, config) {

				// data.success = false;
				deferred.reject(data);

			});
		return deferred.promise;

	}

	function getManager(nid, callback) {

		var data = {};

		var deferred = $q.defer();

		$http
			.post(config.serverUrl + 'server/move_request/request_manager/' + nid)
			.success(function (data, status, headers, config) {

				//data.success = true;
				deferred.resolve(data);

			})
			.error(function (data, status, headers, config) {

				// data.success = false;
				deferred.reject(data);

			});

		return deferred.promise;

	}

	function getManagers() {
		return $rootScope.managers;
		/*
		 var data = {};
		 var deferred = $q.defer();
		 $http
		 .post(config.serverUrl+'server/move_request/managers')
		 .success(function(data, status, headers, config) {
		 deferred.resolve(data);
		 })
		 .error(function(data, status, headers, config) {

		 deferred.reject(data);
		 });

		 return deferred.promise;
		 */

	}

	function getManagersFromServer() {
		var deferred = $q.defer();
		$http
			.post(config.serverUrl + 'server/move_request/managers')
			.success(function (data, status, headers, config) {
				let uid = _.get($rootScope, 'currentUser.userId.uid');

				if (uid == 1) {
					if(!_.find($rootScope.managersList, manager => manager.uid == uid)) {
						data[uid] = angular.copy($rootScope.currentUser.userId);
					}
				}

				$rootScope.managersList = common.objToArray(data);
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {

				deferred.reject(data);
			});

		return deferred.promise;
	}

	function setManager(uid, nid, callback) {

		var data = {nid: nid, uid: uid};


		var deferred = $q.defer();

		$http
			.post(config.serverUrl + 'server/move_request/assign_manager', data)
			.success(function (data, status, headers, config) {

				//data.success = true;
				deferred.resolve(data);

			})
			.error(function (data, status, headers, config) {

				// data.success = false;
				deferred.reject(data);

			});

		return deferred.promise;

	}

	function updateRequest(nid, data) {
		let deferred = $q.defer();

		apiService.putData(`${moveBoardApi.request.unique}${nid}`, {
			data
		})
			.then(resolve => {
				let response = _.head(resolve.data);
				if (!response) {
					deferred.reject();
					return;
				}
				deferred.resolve(resolve.data);
			}, reject => {
				let rejected = reject.data ? reject.data.response : reject;
				deferred.reject(rejected);
			});

		return deferred.promise;

	}

	function getRequestsByNid(nids, callback) {


		var deferred = $q.defer();

		var data = {
			pagesize: 500,
			condition: {
				nid: nids
			}
		};


		$http.post(config.serverUrl + 'server/move_request/search', data)
			.success(function (data, status, headers, config) {

				//data.success = true;
				// service.saveRequests(data);
				deferred.resolve(data);

			})
			.error(function (data, status, headers, config) {

				// data.success = false;
				deferred.reject(data);

			});

		return deferred.promise;

	}

	function getRequestsByUid(uid) {
		var deferred = $q.defer();
		var data = {
			pagesize: 500,
			condition: {
				'author:uid': parseInt(uid),
			},
		};
		$http.post(config.serverUrl + 'server/move_request/search', data)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;

	}

	function getStatusRequestsByPage(params) {
		var search_data = {
			pagesize: 25,
			page: params.page
		};
		const COMPLETED_STATUS_ID = 20;
		const COMPLETED_COMPANY_FLAG_ID = '3';

		if (params.status) {
			search_data.condition = {};

			if (+params.status === COMPLETED_STATUS_ID) {
				search_data.condition.field_company_flags = [COMPLETED_COMPANY_FLAG_ID];
			} else {
				search_data.condition.field_approve = params.status;
			}


		}
		if(params.managerId != 0){
			if (angular.isUndefined(search_data.condition)) {
				search_data.condition = {};
			}
			search_data.condition.field_request_manager = params.managerId;
		}
		if(params.statusSort != 0){
			if (angular.isUndefined(search_data.condition)) {
				search_data.condition = {};
			}
			search_data.condition.field_parser_provider_name = params.statusSort;
		}
		if (angular.isDefined(params.flags)) {
			if (params.flags != 'requests') {
				if (params.flags) {
					if (angular.isUndefined(search_data.condition)) {
						search_data.condition = {};
					}
					search_data.condition.field_flags = params.flags;
				}
			}
		}
		if (angular.isDefined(params.booked)) {
			search_data.confirmed = +params.booked;
		}
		if (angular.isDefined(params.sorting)) {
			search_data.sorting = {
				direction: params.sorting,
				field: params.sort_field
			};
		}
		if (params.createDate) { //create date
			search_data.filtering = {
				date_from: MomentWrapperService.getZeroMoment(params.createDate.from).startOf('day').unix(),
				date_to: MomentWrapperService.getZeroMoment(params.createDate.to).endOf('day').unix()
			};
		} else if (params.moveDate) {
			search_data.filtering = {
				field_date_to: MomentWrapperService.getZeroMoment(params.moveDate.to).endOf('day').unix(),
				field_date_from: MomentWrapperService.getZeroMoment(params.moveDate.from).startOf('day').unix()
			};
		}
		if (params.company_flags) {
			if (angular.isUndefined(search_data.condition)) {
				search_data.condition = {};
			}
			search_data.condition.field_company_flags = [params.company_flags];
		}
		if (params.service_type != 0) {
			if (angular.isUndefined(search_data.condition)) {
				search_data.condition = {};
			}
			search_data.condition.field_move_service_type = params.service_type;
		}
		if (params.reservation_received) {
			if (angular.isUndefined(search_data.condition)) {
				search_data.condition = {};
			}
			search_data.condition.field_reservation_received = 1;
		}

		if (_.isArray(params.status) && params.status[0] == 2) {
			search_data.condition.field_reservation_received = params.currTab == 4 ? 1 : 0;
		}

		if (_.isObject(params.leadScoreFilter)) {
			if (params.leadScoreFilter.enabled) {
				search_data.condition = search_data.condition || {};
				search_data.condition.field_total_score = params.leadScoreFilter.value;
			}
		}

		if (!params.dates_filter || params.dates_filter == 0) {
			search_data.all_dates = {
				first: MomentWrapperService.getDateInTimezone(moment().startOf('month')).unix(),
				last: MomentWrapperService.getDateInTimezone(moment().endOf('month')).unix(),
			};
		} else if (params.dates_filter == 1) {
			search_data.all_dates = {
				first: 0,
				last: moment().add(100, 'years').unix()
			};
		} else if (params.dates_filter == 2) {
			search_data.all_dates = {
				first: MomentWrapperService.getDateInTimezone(moment().startOf('day')).unix(),
				last: moment().add(100, 'years').unix()
			};
		}

		var timeout = $q.defer();
		var promise = $http({
			method: 'post',
			url: config.serverUrl + 'server/move_request/search',

			data: search_data,

			timeout: timeout.promise
		});
		promise._httpTimeout = timeout;

		return promise;

	}

	function getStatusRequests(status) {

		var timeout = $q.defer();
		var promise = $http({
			method: 'post',
			url: config.serverUrl + 'server/move_request/search',
			data: {
				pagesize: 500,
				condition: {
					field_approve: status
				}
			},
			timeout: timeout.promise
		});
		promise._httpTimeout = timeout;

		return promise;

	}

	function getDateRequests(date) {


		var deferred = $q.defer();

		var data = {
			pagesize: 1000,
			condition: {
				field_approve: [2, 3],
			},
			filtering: {
				field_date: MomentWrapperService.getZeroMoment(date).startOf('day').unix(),
				// created :date
			},

		};


		$http.post(config.serverUrl + 'server/move_request/search', data)
			.success(function (data, status, headers, config) {

				//data.success = true;
				service.saveDateRequests(date, data);
				deferred.resolve(data);

			})
			.error(function (data, status, headers, config) {

				// data.success = false;
				deferred.reject(data);

			});

		return deferred.promise;

	}

	function saveDateRequests(date, data) {

		if (angular.isUndefined($rootScope.dateRequests)) {

			$rootScope.dateRequests = [];

		}

		$rootScope.dateRequests[date] = data;


	}

	function storageDateRequests(date) {

		return $rootScope.dateRequests[date];
	}

	function getAllRequests(callback) {


		var deferred = $q.defer();

		$http.get(config.serverUrl + 'server/move_request?pagesize=100')
			.success(function (data, status, headers, config) {
				service.saveRequests(data);
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function getAllRequestsMonthSchedule(satuses, from, to, callback) {


		var deferred = $q.defer();

		var data = {
			pagesize: 1000,
			condition: {
				field_approve: satuses,

			},
			filtering: {
				field_date_from: MomentWrapperService.getZeroMoment(from).startOf('day').unix(),
				field_date_to: MomentWrapperService.getZeroMoment(to).endOf('day').unix(),
			}
		};

		$http.post(config.serverUrl + 'server/move_request/search', data)
			.success(function (data, status, headers, config) {

				//data.success = true;
				service.saveRequests(data);
				deferred.resolve(data);

			})
			.error(function (data, status, headers, config) {

				// data.success = false;
				deferred.reject(data);

			});

		return deferred.promise;

	}

	function getAllRequestsDateRange(filtering, callback) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/move_request/search', filtering)
			.then(function (data) {
				if (data.status == 200) {
					service.saveRequests(data.data);
				}

				deferred.resolve(data);
			}, function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function getRequest(nid) {


		var deferred = $q.defer();


		$http.get(config.serverUrl + 'server/move_request/' + nid)
			.success(function (data, status, headers, config) {
				//data.success = true;
				var request = data;
				service.addRequest(data.nid, data);
				deferred.resolve(request);
			})
			.error(function (data, status, headers, config) {
				// data.success = false
				deferred.reject(data);
			});

		return deferred.promise;

	}

	function getRequestParklot(date) {
		var deferred = $q.defer();

		var data = {
			date: MomentWrapperService.getZeroMoment(date).unix(),
			condition: {
				field_approve: [2, 3]
			}
		};

		let method = $rootScope.fieldData.is_common_branch_truck ? moveBoardApi.requests.getAllBranchParklotByDay : moveBoardApi.requests.getParklotOfRequestDate;

		apiService.postData(method, data).then(function (data) {
			filterDeliveryFlatRateRequest(data.data, date);
			deferred.resolve(data);
		}, function (error) {
			deferred.reject(error);
		});

		return deferred.promise;

	}

	function saveRequests(data) {

		$rootScope.requests = data;
		$rootScope.dateRequests = [];

	}

	function addRequest(nid, data) {

		var requests = service.getRequests();

		if (angular.isUndefined(requests)) {

			$rootScope.requests = data;

		}
		else {
			requests[nid] = data;
		}
		service.saveRequests(requests);
	}

	function getRequests() {
		return $rootScope.requests;
	}

	function cancel(promises) {

		if (_.isArray(promises) && promises.length) {
			angular.forEach(promises, function (promise, i) {
				clearTimeout(promise);

			});
		} else {
			// If the promise does not contain a hook into the deferred timeout,
			// the simply ignore the cancel request.
			clearTimeout(promises);
		}

		function clearTimeout(promise) {
			if (
				promise &&
				promise._httpTimeout &&
				promise._httpTimeout.resolve
			) {
				promise._httpTimeout.resolve();
			}
		}
	}

	function getRequestsByStatusDate(data) {
		var deferred = $q.defer();
		var data = {
			pagesize: 999999,
			condition: {
				field_approve: ['3'],
			},
			filtering: {
				field_date_from: MomentWrapperService.getZeroMoment(data.from).startOf('day').unix(),
				field_date_to: MomentWrapperService.getZeroMoment(data.to).endOf('day').unix(),
			}
		};
		$http.post(config.serverUrl + 'server/move_request/search', data)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function getRequestByStatusDateWithTimeZone(date) {
		let defer = $q.defer();

		let data = {
			pagesize: 999999,
			condition: {
				field_approve: ['3'],
			},
			filtering: {
				field_date_from: date.from,
				field_date_to: date.to,
			}
		};

		apiService.postData(moveBoardApi.search.searchRequest, data)
			.then(resolve => {
				defer.resolve(resolve.data);
			}, reject => {
				defer.reject(reject.data);
			});

		return defer.promise;
	}

	function getClosedJobs(date, flag) {
		var deferred = $q.defer();
		var data = {
			pagesize: 25,
			page: 0,
			condition: {
				field_company_flags: [flag]
			},
			filtering: {
				field_date_from: MomentWrapperService.getZeroMoment(date.from).startOf('day').unix(),
				field_date_to: MomentWrapperService.getZeroMoment(date.to).endOf('day').unix(),
			}
		};

		$http.post(config.serverUrl + 'server/move_request/search', data)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function getOpenedJobs(date, page) {
		var deferred = $q.defer();
		var data = {
			pagesize: 25,
			page: 0,
			filtering: {
				date_from: MomentWrapperService.getZeroMoment(date.from).startOf('day').unix(),
				date_to: MomentWrapperService.getZeroMoment(date.to).endOf('day').unix(),
			}
		};

		if (!_.isUndefined(page)) {
			data.page = page;
		}

		apiService.postData(moveBoardApi.request.getOpenedJobs, data)
			.then(function (response) {
				deferred.resolve(response.data);
			}, function (error) {
				deferred.reject(error.data);
			});

		return deferred.promise;
	}

	function getNewComments() {
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/move_request_comments/get_new_comments')
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function getLogs(nid, type) {
		var entity_type = type;
		var deferred = $q.defer();
		$http.get(config.serverUrl + 'server/new_log?entity_id=' + nid + '&entity_type=' + entity_type)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function postLogs(data) {
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/new_log', data)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function sendLogs(data, title, nid, type, info) {
		var currUser = $rootScope.currentUser;
		const USER_NAME = `${currUser.userId.field_user_first_name} ${currUser.userId.field_user_last_name}`;
		const DEPRECATED_USER = PermissionsServices.isSuper();
		var userRole = '';
		if (angular.isUndefined(currUser)) {
			currUser = [];
			currUser.userRole = ['anonymous'];
			currUser.userId = [];
			currUser.userId.field_user_first_name = 'anonymous';
			currUser.userId.field_user_last_name = '';
		}
		if (currUser.userRole.indexOf('administrator') >= 0) {
			userRole = 'administrator';
		} else if (currUser.userRole.indexOf('manager') >= 0) {
			userRole = 'manager';
		} else if (currUser.userRole.indexOf('sales') >= 0) {
			userRole = 'sales';
		} else if (currUser.userRole.indexOf('foreman') >= 0) {
			userRole = 'foreman';
		}
		var entity_type = service.EntityTypes[type];
		var msg = {
			entity_id: nid,
			entity_type: entity_type,
			data: [{
				details: [{
					activity: userRole,
					title: title,
					text: [],
					date: moment().unix()
				}],
				source: DEPRECATED_USER ? 'System' : USER_NAME,
			}]
		};
		var info = {
			browser: AuthenticationService.get_browser(),
		};
		msg.data[0].details[0].info = {};
		msg.data[0].details[0].info = info;
		_.forEach(data, function (obj) {
			msg.data[0].details[0].text.push(obj);
		});
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/new_log', msg)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function sendServiceLogs(data, nid, type) {
		var currUser = $rootScope.currentUser;
		var MOVEREQUEST = 0;
		var STORAGEREQUEST = 1;
		var LDREQUEST = 2;
		var SYSTEM = 3;
		var userRole = '';
		if (currUser.userRole.indexOf('administrator') >= 0) {
			userRole = 'administrator';
		} else if (currUser.userRole.indexOf('manager') >= 0) {
			userRole = 'manager';
		} else if (currUser.userRole.indexOf('sales') >= 0) {
			userRole = 'sales';
		} else if (currUser.userRole.indexOf('foreman') >= 0) {
			userRole = 'foreman';
		}
		var entity_type = SYSTEM;
		if (type == 6 || type == 2) {
			entity_type = STORAGEREQUEST;
		} else if (type == 5 || type == 7) {
			entity_type = LDREQUEST;
		} else {
			entity_type = MOVEREQUEST;
		}
		var info = {
			browser: AuthenticationService.get_browser(),
		};
		var msg = {
			entity_id: nid,
			entity_type: entity_type,
			data: []
		};
		_.forEach(data, function (obj) {
			_.forEach(obj, function (details) {
				_.forEach(details, function (detail) {
					detail.activity = userRole;
					detail.info = info;
				});
			});
			obj.source = currUser.userId.field_user_first_name + ' ' + currUser.userId.field_user_last_name;
		});
		msg.data = data;
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/new_log', msg)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function getEntities() {
		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/settings/get_entities')
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;
	}
	function getSumCommissionFromTotal(uid, nid, contractType) {
		var deferred = $q.defer();
		var data = {
			uid: Number(uid),
			nid: Number(nid),
			contract_type: Number(contractType),
		};
		$http.post(config.serverUrl + 'server/payroll/sum_commission_from_total', data)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function UpdatePackingRequest(packingId, additionalInfo) {
		//get packing request
		getRequestsByNid(packingId).then(resolve => {
			//updateFields
			let allData = _.head(resolve.nodes).request_all_data;
			if (!_.isUndefined(allData.additionalInfo)) {
				allData.additionalInfo = additionalInfo;
				saveReqData(packingId, allData);
			}
		});
	}

}
