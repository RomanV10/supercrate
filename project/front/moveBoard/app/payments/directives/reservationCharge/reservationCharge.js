'use strict';
import './reservationCharge.styl'
(() => {

    angular
        .module('app.payment')
        .directive('paymentsReservationCharge', (apiService, moveBoardApi, $mdDialog, common) => {
            return {
                scope: {
                    openReservationCharge: '='
                },
	            template: require('./reservationCharge.pug'),
                restrict: 'A',
                link: ($scope, element) => {

                    $scope.range = common.range;
                    $scope.currentStep = 1;

                     $scope.openReservationCharge = (ev, nid) => {
                        $scope.payment = {
                            date: '',
                            type: '',
                            amount: '',
                            description: '',
                            pending: false,
                            nid: nid
                        };
                        $mdDialog.show({
                            controller: DialogController,
                            contentElement: '#addReservationCharge',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: true
                        });

                    };

                    function DialogController($scope, $mdDialog) {
                        $scope.hide = function () {
                            $mdDialog.hide();
                        };

                        $scope.cancel = function () {
                            $mdDialog.cancel();
                        };

                        $scope.answer = function (answer) {
                            $mdDialog.hide(answer);
                        };
                    }

                }
            }
        });

})();
