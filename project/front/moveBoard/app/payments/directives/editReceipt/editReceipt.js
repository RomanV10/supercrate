'use strict';
import './editReceipt.styl'
(() => {

    angular
        .module('app.payment')
        .directive('editReceipt', (apiService, moveBoardApi, $mdDialog) => {
            return {
                scope: {
                    openReceipt: '=',
                    paySuccess: '='
                },
	            template: require('./editReceipt.pug'),
                restrict: 'A',
                link: ($scope, element) => {

                    $scope.openReceipt = (ev, payment) => {
                        $scope.payment = payment;
                        $mdDialog.show({
                            controller: DialogController,
                            contentElement: '#editReceipt',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: true
                        });

                    };

                    $scope.hide = function () {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.answer = function (answer) {
                        $mdDialog.hide(answer);
                    };
                    function DialogController($scope, $mdDialog) {

                    }

                }
            }
        });

})();
