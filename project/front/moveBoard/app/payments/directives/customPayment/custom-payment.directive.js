import './custom-payment.styl'
'use strict';

angular.module('app.payment')
	.directive('paymentsCustomPayment', customPayment);

customPayment.$inject = ['apiService', 'moveBoardApi', '$mdDialog', 'common', 'OnlinePaymentService', 'CalculatorServices', '$timeout', 'SweetAlert', 'geoCodingService'];

function customPayment(apiService, moveBoardApi, $mdDialog, common, OnlinePaymentService, CalculatorServices, $timeout, SweetAlert, geoCodingService) {
	return {
		scope: {
			openCustomPayment: '=',
			userInfo: '<',
			tripId: '<?',
			paySuccess: '&',
			entityType: '@',
			entityId: '@',
			isTpDelivery: '<?',
			balance: '=?',
			nid: '=?'
		},
		template: require('./custom-payment.pug'),
		restrict: 'A',
		link: linker
	};

	function linker($scope, element) {
		$scope.editMode = false;
		$scope.receiptID = 0;
		$scope.showLoader = false;
		$scope.thisYear = moment().year();
		$scope.paymentTypes = [
			'Cash',
			'Check',
			'Credit Card',
			'Reservation'
		];
		$scope.creditCardTypes = [
			"Visa",
			"Mastercard",
			"AMEX",
			"Discover",
			"Visa Electron"
		];
		$scope.range = common.range;

		$scope.openCustomPayment = openCustomPayment;
		$scope.setType = setType;
		$scope.getBalance = getBalance;
		$scope.cancel = cancel;
		$scope.getGeoCode = getGeoCode;
		$scope.getCardType = getCardType;
		$scope.checkCardExpireDateValidation = checkCardExpireDateValidation;
		$scope.save = save;
		function getReceiptData(amount, jobs, type, receipt, invoice) {
			let payment;
			if (!receipt) {
				payment = {
					amount: amount + '',
					auth_code: "",
					card_type: "",
					transaction_type: type,
					jobs: jobs,
					date: new Date(),
					description: "",
					type: $scope.paymentTypes[0],
					payment_method: $scope.paymentTypes[0],
					transaction_id: "Custom Payment",
					pending: false,
					checkN: '',
					total_amount: angular.copy(amount)
				};
				$scope.balanceCopy = angular.copy($scope.balance);

				if ($scope.tripId) {
					payment.payment_flag = 'Trip #' + $scope.tripId;
					payment.trip_id = $scope.tripId
				}

				if ($scope.nid != 0) {
					payment.nid = $scope.nid
				}
				if ($scope.isTpDelivery) {
					payment.tp_delivery_transaction = 1
				}
				$scope.editMode = false;
			} else {
				let key = receipt.value ? 'value' : "data";
				payment = angular.copy(receipt[key]);
				payment.receipt_id = receipt.receipt_id || receipt.id;
				$scope.receiptID = payment.receipt_id;
				payment.date = new Date(receipt.created * 1000);
				$scope.balanceCopy = $scope.balance ? parseFloat($scope.balance) - parseFloat(payment.amount) : undefined;
				$scope.editMode = true;
				if (payment.type === 'Custom Refund') {
					$scope.paymentTypes.push('Custom Refund')
				}
			}
			if (invoice) {
				payment.invoice_id = $scope.invoiceId ? $scope.invoiceId : "custom";
				payment.name = invoice.data.customer_name || invoice.data.client_name;
				payment.phone = invoice.data.phone;
				payment.email = invoice.data.email
			} else if ($scope.userInfo) {
				payment.phone = $scope.userInfo.phones[0];
				payment.name = $scope.userInfo.name;
				payment.contact_person_phone = $scope.userInfo.contact_person_phone;
				payment.email = $scope.userInfo.email
			}


			return payment
		}

		function setPaymentTypes (invoice) {
			if (invoice) {
				if ($scope.paymentTypes.indexOf('Authorized Credit Card') === -1) {
					$scope.paymentTypes.push('Authorized Credit Card');
				}
				if ($scope.paymentTypes.indexOf('Custom Refund') === -1 && !_.isEmpty(invoice.receipts) && checkAllReceiptForInvoice(invoice.receipts)) {
					$scope.paymentTypes.push('Custom Refund')
				}
				$scope.invoiceId = invoice.id || invoice.invoice_id;
				$scope.invoice = invoice
			} else {
				if ($scope.paymentTypes.indexOf('Authorized Credit Card') !== -1) {
					$scope.paymentTypes.splice($scope.paymentTypes.indexOf('Authorized Credit Card'), 1)
				}
				if ($scope.paymentTypes.indexOf('Custom Refund') !== -1) {
					$scope.paymentTypes.splice($scope.paymentTypes.indexOf('Custom Refund'), 1)
				}
				delete $scope.invoiceId
			}
		}

		function openCustomPayment(ev, amount, maxAmount, jobs, type, receipt, invoice) {
			$scope.maxAmount = maxAmount;
			$scope.cardNumberInputFocused = false;
			element.find('#customPaymentCardNo').attr({
				'placeholder': '4111 1111 1111 1111',
				'ng-minlength': '19',
				'maxlength': '19'
			});

			$scope.amountForm.$setUntouched();
			$scope.cardForm.$setUntouched();
			$scope.checkForm.$setUntouched();

			setPaymentTypes(invoice);

			$scope.payment = getReceiptData(amount, jobs, type, receipt, invoice);

			$mdDialog.show({
				contentElement: '#addCustomPayment',
				parent: angular.element(document.body),
				targetEvent: ev
			});
		}

		function setType() {
			if ($scope.payment.type === 'Custom Refund') {
				$scope.payment.pending = false;
				$scope.payment.amount = $scope.invoice.data.partially_paid;
			} else {
				$scope.payment.amount = $scope.payment.total_amount;
			}

			if ($scope.maxAmount) {
				$scope.maxAmount = $scope.payment.amount;
			}
		}

		function getBalance () {
			let resultBalance = 0;
			if ($scope.balanceCopy) {
				if ($scope.payment.amount && !$scope.payment.pending) {
					resultBalance = parseFloat($scope.payment.amount) + parseFloat($scope.balanceCopy)
				} else {
					resultBalance = parseFloat($scope.balanceCopy)
				}
			}
			return resultBalance
		}

		function cancel() {
			$scope.balanceCopy = angular.copy($scope.balance);
			$scope.payment = {};
			$scope.showLoader = false;
			$mdDialog.cancel();
		}


		function checkAllReceiptForInvoice(receipts) {
			let isPaymentsExists = false;
			receipts.forEach(item => {
				if(!item.value.refunds && !item.value.pending) {
					isPaymentsExists = true;
				}
			});
			return isPaymentsExists
		}

		function checkCardAndCheckValidation() {
			let valid = true;
			if ($scope.payment.type === 'Check' && !$scope.checkForm.$valid) {
				valid = false;
				$scope.checkForm.$setSubmitted()
			}
			if (($scope.payment.type === 'Credit Card' || $scope.payment.type === 'Authorized Credit Card') && !$scope.cardForm.$valid) {
				valid = false;
				$scope.cardForm.$setSubmitted()
			}
			if ($scope.payment.type !== 'Check' && $scope.payment.type !== 'Credit Card' && $scope.payment.type !== 'Authorized Credit Card') {
				valid = true
			}
			if (($scope.payment.type === 'Credit Card' || $scope.payment.type === 'Authorized Credit Card') && valid) {
				$scope.payment.name = $scope.payment.firstName + ' ' + $scope.payment.lastName
			}
			return valid
		}

		function getGeoCode() {
			$scope.payment.zip_code = $scope.payment.zip_code || '';
			$scope.cardForm.zip_code.$setValidity('invalidZip', true);
			if ($scope.payment.zip_code.length === 5) {
				geoCodingService.geoCode($scope.payment.zip_code)
					.then(function (response) {
						$scope.payment.city = response.city;
						$scope.payment.state = response.state;
						if (!response.state) {
							$scope.cardForm.zip_code.$setValidity('invalidZip', false)
						}
					}, function () {
						$scope.payment.city = '';
						$scope.payment.state = '';
						$scope.cardForm.zip_code.$setValidity('invalidZip', false)
					});
			}
		}

		function getCardType() {
			if ($scope.payment.ccardN) {
				if ($scope.payment.ccardN.length > 2) {
					let cardNumber = $scope.payment.ccardN.replace(/ /g, '');
					$scope.payment.cctype = OnlinePaymentService.getCardType(cardNumber);
					if (!$scope.payment.cctype) {
						$scope.cardForm.cardNumber.$setValidity("InvalidCardType", false);
						$scope.cardForm.cardNumber.$setTouched()
					} else {
						$scope.cardForm.cardNumber.$setValidity("InvalidCardType", true);
						if ($scope.payment.cctype !== "AMEX") {
							element.find('#customPaymentCardNo').trigger('focus');
							delete $scope.payment.cvcAmex
						} else {
							element.find('#customPaymentAmexCardNo').trigger('focus');
							delete $scope.payment.cvc
						}
					}
				} else {
					delete $scope.payment.cctype;
					$scope.cardForm.cardNumber.$setValidity("InvalidCardType", true)
				}
			}
		}

		function checkCardExpireDateValidation() {
			if ($scope.payment.expmonth && $scope.payment.expyear) {
				let expDate = moment({
					year: $scope.payment.expyear,
					month: $scope.payment.expmonth - 1
				}).endOf('month');
				let thisDate = moment().startOf('month');
				if (expDate.isBefore(thisDate)) {
					$scope.cardForm.month.$setValidity("InvalidDate", false);
					$scope.cardForm.month.$setTouched();
					$scope.cardForm.year.$setValidity("InvalidDate", false);
					$scope.cardForm.year.$setTouched()
				} else {
					$scope.cardForm.month.$setValidity("InvalidDate", true);
					$scope.cardForm.year.$setValidity("InvalidDate", true)
				}
			}
		}

		function checkAmountValidation() {
			let amountValid = true;
			if (parseFloat($scope.payment.amount) <= 0 || !$scope.payment.amount) {
				$scope.amountForm.amount.$setValidity("required", false);
				$scope.amountForm.$setSubmitted();
				amountValid = false
			} else {
				$scope.amountForm.amount.$setValidity("required", true);
				$scope.amountForm.amount.$setValidity("maxAmount", true)
			}
			return amountValid
		}


		function setReceipt() {
			if ($scope.nid != 0) {
				$scope.payment.nid = $scope.nid
			}
			apiService.postData(moveBoardApi.ldReceipt.setReceipt, {
				entity_id: $scope.entityId,
				entity_type: $scope.entityType,
				data: $scope.payment
			}).then(data => {
				$scope.showLoader = false;
				$scope.payment.id = data.data[0];
				$scope.paySuccess();
				$mdDialog.cancel();
			})
		}

		function updateReceipt() {
			$scope.payment.entity_id = $scope.entityId;
			$scope.payment.entity_type = $scope.entityType;

			apiService.postData(moveBoardApi.ldReceipt.updateReceipt, {
				id: $scope.receiptID,
				data: $scope.payment
			}).then(data => {
				$scope.showLoader = false;
				$scope.paySuccess();
				$mdDialog.cancel();
			})
		}

		function setRefund() {
			$scope.payment.refunds = true;
			$scope.invoice_id = $scope.invoiceId;
			let refund = {
				amount: $scope.payment.amount,
				phone: $scope.payment.phone,
				contact_person_phone: $scope.payment.contact_person_phone,
				name: $scope.payment.name,
				email: $scope.payment.email,
				description: $scope.payment.description,
				entity_id: $scope.entityId,
				entity_type: $scope.entityType,
				jobs: $scope.payment.jobs,
				date: new Date(),
				invoice_id: $scope.invoiceId,
				type: 'Custom Refund',
				transaction_type: 2,
				refunds: true,
				refund_type: 0,
				payment_method: 'cash'
			};

			apiService.postData(moveBoardApi.ldReceipt.setReceipt, {
				entity_id: $scope.entityId,
				entity_type: $scope.entityType,
				data: refund
			}).then(res => {
				if (_.head(res.data) == false) {
					SweetAlert.swal("Error!", "Payment refund rejected!", "error");
					$scope.showLoader = false;
					return false;
				} else if (!res.data.status) {
					SweetAlert.swal("Payment refund rejected!", res.data.text, "error");
					$scope.showLoader = false;
					return false;
				} else {
					$mdDialog.hide();
					$scope.showLoader = false;
					$scope.paySuccess();
				}
			}, err => {
				$scope.showLoader = false;
				SweetAlert.swal("Failed!", err.data, "error")
			})
		}
		function setCustomInvoicePayment() {
			apiService.postData(moveBoardApi.ldReceipt.setReceipt, {
				entity_id: $scope.entityId,
				entity_type: $scope.entityType,
				data: $scope.payment
			}).then(data => {
				$scope.showLoader = false;
				toastr.success("Invoice was paid", "Success");
				$scope.paySuccess();
				$mdDialog.cancel();
			})
		}

		function setAutorizedInvoicePayment() {
			let onlinePayment = {
				amount: $scope.payment.amount,
				phone: $scope.payment.phone,
				contact_person_phone: $scope.payment.contact_person_phone,
				email: $scope.payment.email,
				pending: $scope.payment.pending,
				jobs: $scope.payment.jobs,
				description: $scope.payment.description,
				card_type: OnlinePaymentService.getCardType($scope.payment.ccardN),
				entity_id: $scope.entityId,
				entity_type: $scope.entityType,
				name: $scope.payment.firstName + ' ' + $scope.payment.lastName,
				date: new Date(),
				type: $scope.payment.type,
				transaction_type: 2,
				payment_method: 'creditcard',
				zip_code: $scope.payment.zip_code,
				invoice_id: $scope.invoiceId,
				credit_card: {
					"card_number": $scope.payment.ccardN.replace(/ /g, ''),
					"exp_date": $scope.payment.expmonth + "/" + $scope.payment.expyear,
					"card_code": $scope.payment.cvc || $scope.payment.cvcAmex
				}
			};
			onlinePayment.credit_card.card_code = $scope.payment.cctype === "AMEX" ? $scope.payment.cvcAmex : $scope.payment.cvc;

			apiService.postData(moveBoardApi.ldReceipt.setReceipt, {
				entity_id:$scope.entityId,
				entity_type:$scope.entityType,
				data:onlinePayment
			}).then(res => {
				if (_.isEmpty(res.data) || !res.data.receipt_id || !res.data.status) {
					SweetAlert.swal({
						title: "Failed!",
						text: res.data.text,
						type: "error",
						html: true
					});
					$scope.showLoader = false;
				} else {
					$scope.paySuccess();
					toastr.success("Invoice was paid", "Success");
					$scope.showLoader = false;
					$mdDialog.cancel();
				}
			}).catch(error => {
				toastr.error("Invoice wasn't paid", "Error");
			})
		}

		function save() {
			if ($scope.balance) {
				$scope.balance = getBalance()
			}
			if (checkCardAndCheckValidation() && checkAmountValidation()) {
				$scope.showLoader = true;
				if ($scope.editMode) {
					updateReceipt()
				} else {
					if ($scope.invoiceId) {
						if ($scope.payment.type === 'Authorized Credit Card') {
							setAutorizedInvoicePayment()
						} else if ($scope.payment.type === 'Custom Refund') {
							setRefund()
						} else {
							setCustomInvoicePayment()
						}
					} else {
						setReceipt()
					}
				}

			}
		}

	}
}
