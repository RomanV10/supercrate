'use strict';

angular
	.module('app.payment')
	.directive('validateAmount', validateAmount);

validateAmount.$inject = ['$timeout'];

function validateAmount($timeout) {
	return {
		restrict: 'A',
		require: ['^form','ngModel'],
		link: linker
	};

	function linker($scope, element, attrs, ctrls) {
		let  amountTimeout;
		let formController = ctrls[0];
		let modelController = ctrls[1];
		if (!modelController) {
			return;
		}


		function amountTimer() {
			amountTimeout = $timeout(function () {
				formController.amount.$setValidity("maxAmount", true)
			}, 2000)
		}

		function triggerError() {
			element.trigger('blur');
			formController.amount.$setValidity("maxAmount", false);
			amountTimer()
		}

		modelController.$parsers.push(function (inputValue) {
			if (attrs.validateAmount) {
				let maxAmount = angular.copy(attrs.validateAmount);
				let result = parseFloat(maxAmount) - parseFloat(inputValue);

				if (result < 0) {
					inputValue = maxAmount;
					modelController.$setViewValue(maxAmount);
					modelController.$render();

					triggerError()
				} else {
					formController.amount.$setValidity("maxAmount", true)
				}
			}
			return inputValue
		});

	}
}

