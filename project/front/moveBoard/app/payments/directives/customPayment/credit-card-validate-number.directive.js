'use strict';

angular
	.module('app.payment')
	.directive('creditCardValidateNumber', cardValidateNumber);

function cardValidateNumber() {
	return {
		restrict: 'A',
		require: '?ngModel',
		scope: {
			cardType: '='
		},
		link: linker
	};

	function linker($scope, elem, attrs, ngModel) {

		var caretPos = 0;
		var tempValue = '';

		if (!ngModel) {
			return;
		}

		function formatCardNumber(value) {
			var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
			var matches = v.match(/\d{4,16}/g);
			var match = matches && matches[0] || '';
			var parts = [];

			for (var i = 0, len = match.length; i < len; i += 4) {
				parts.push(match.substring(i, i + 4))
			}

			if (value.length >= tempValue.length) {
				if (caretPos == 5 || caretPos == 10 || caretPos == 15) {
					caretPos++
				}
			} else {
				if (caretPos == 5 || caretPos == 10 || caretPos == 15) {
					caretPos--
				}
			}

			if (parts.length) {
				return parts.join(' ')
			} else {
				return value
			}
		}

		function formatAmexCardNumber(value) {
			var v = value.replace(/[^0-9]/gi, '');
			var parts = [];

			if (v.length > 4) {
				parts.push(v.substring(0, 4));
				parts.push(v.substring(4, 10))
			}
			if (v.length > 10) {
				parts.push(v.substring(10, 15))
			}
			if (value.length >= tempValue.length) {
				if (caretPos == 5 || caretPos == 12) {
					caretPos++
				}
			} else {
				if (caretPos == 5 || caretPos == 12) {
					caretPos--
				}
			}


			if (caretPos == 5) {
			}

			if (parts.length) {
				return parts.join(' ')
			} else {
				return value
			}
		}

		ngModel.$parsers.push(function (inputValue) {
			caretPos = $(elem).caret();
			var placeholders = ['4111 1111 1111 1111', '3411 111111 11111'];
			var placeholder = ($scope.cardType == 'AMEX') ? placeholders[1] : placeholders[0];
			var minLength = ($scope.cardType == 'AMEX') ? 17 : 19;
			var cardNr = ($scope.cardType == 'AMEX') ? formatAmexCardNumber(inputValue) : formatCardNumber(inputValue);
			elem.attr({
				'placeholder': placeholder,
				'ng-minlength': minLength,
				'maxlength': minLength
			});
			if (cardNr != inputValue) {
				ngModel.$setViewValue(cardNr);
				ngModel.$render();
			}
			tempValue = inputValue;
			$(elem).caret(caretPos);

			return cardNr;
		});

	}
}

