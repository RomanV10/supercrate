'use strict';
import './invoice.styl'

angular
	.module('app.payment')
	.directive('paymentsInvoice', paymentsInvoice);

/* @ngInject */
function paymentsInvoice(apiService, moveBoardApi, $mdDialog, $rootScope, SendEmailsService, InvoiceServices, $q, logger) {
	return {
		scope: {
			openInvoice: '=',
			entityType: '@',
			entityId: '@',
			paySuccess: '&',
			userInfo: '=',
			onPaid: '&',
			sit: '<'
		},
		template: require('./invoice.pug'),
		restrict: 'A',
		link: ($scope, element) => {
			$scope.invoice = {
				name: "Jack Test",
				charges: []
			};
			$scope.invoiceTotal = 0;

			$scope.showLoader = true;

			$scope.openInvoice = (ev, charge, edit, jobs, amount, id, isInvoiceList, descriptionAndNotes) => {
				$scope.invoice_id = id
				$scope.isInvoiceList = isInvoiceList
				if (!edit) {
					$scope.invoice = {
						newInvoice: true,
						client_name: $scope.userInfo.name,
						email: $scope.userInfo.email,
						request_invoice: true,
						entity_id: $scope.entityId,
						entity_type: $scope.entityType,
						phone: $scope.userInfo.phones[0],
						zip_code: $scope.userInfo.zip_code,
						is_invoice: 1,
						carrier_id: $scope.entityId,
						amount: amount,
						jobs_id: jobs,
						discount: 0,
						tax: 0,
						field_moving_from: {
							administrative_area: $scope.userInfo.state,
							postal_code: $scope.userInfo.zip_code,
							locality: $scope.userInfo.city,
							thoroughfare: $scope.userInfo.address,
						},
						date: new Date(),
						charges: [
						]
					};
					if (_.isArray(charge)) {
						$scope.invoice.charges = charge;
					} else if (charge) {
						$scope.invoice.charges.unshift(charge);
					}
					if (descriptionAndNotes) {
						$scope.invoice.description = descriptionAndNotes.description
						$scope.invoice.notes = descriptionAndNotes.notes
					}
				} else {
					$scope.edit = charge;
					$scope.invoice = charge;
				}
				if ($scope.sit) {
					$scope.invoice.sit = true;
				}

				if(amount){
					$scope.invoiceTotal = amount;
				}

				$scope.showLoader = false;
				$mdDialog.show({
					controller: DialogController,
					contentElement: '#invoice',
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose: true
				});



			};

			$scope.getGrandTotal = () => {
				const subTotal = $scope.getSubTotal();
				return _.round((subTotal - subTotal * $scope.invoice.discount / 100 + (subTotal - subTotal * $scope.invoice.discount / 100) * $scope.invoice.tax / 100), 2);
			};

			$scope.getSubTotal = () => {
				return $scope.invoice.charges.reduce((sum, current) => sum + (current.cost * current.qty), 0);
			};

			$scope.hide = function () {
				$mdDialog.hide();
				$scope.showLoader = true;

			};

			$scope.cancel = function () {
				$mdDialog.cancel();
				$scope.showLoader = true;

			};

			$scope.answer = function (answer) {
				$mdDialog.hide(answer);
			};


			$scope.getSubTotal = () => {
				return $scope.invoice.charges.reduce((sum, current) => sum + (current.cost * current.qty), 0)
			}

			$scope.saveAsDraft = () => {
				$scope.invoice.flag = 0
				if (!$scope.invoice_id) {
					apiService.postData(moveBoardApi.invoice.create, { entity_id: $scope.entityId, entity_type: $scope.entityType, data: $scope.invoice }).then(data => {
						if ($scope.isInvoiceList) {
							$scope.onPaid()
						} else {
							$scope.paySuccess()
						}
						$mdDialog.cancel();
					});
				} else {
					apiService.putData(moveBoardApi.invoice.update, {id: $scope.invoice_id, entity_id: $scope.entityId, entity_type: $scope.entityType, data: $scope.invoice }).then(data => {
						if ($scope.isInvoiceList) {
							$scope.onPaid()
						} else {
							$scope.paySuccess()
						}
						$mdDialog.cancel();
					});
				}
			};

			function saveAgentFolioInvoice(email) {
				createInvoice().then(function (response) {
					var idInvoice = $scope.invoice_id ? $scope.invoice_id: response[0];

					if (_.isObject(email) && !_.isEmpty(email.blocks)) {
						SendEmailsService
							.sendEmailsInvoice(idInvoice, [email])
							.then(function () {
								toastr.success('Email was sent', 'Success');

								if ($scope.text == 'invoiceList') {
									$scope.onPaid()
								} else {
									$scope.paySuccess()
								}
							}, function () {
								toastr.success("Email wasn't send", 'Error');
							});
					}

					toastr.success("Invoice was send", "Success");
					$rootScope.$broadcast('close.parent.modal');

					if ($scope.text == 'invoiceList') {
						$scope.onPaid()
					} else {
						$scope.paySuccess()
					}
				}, function (reason) {
					logger.error(reason, reason, "Error");
				});
			}

			$scope.sendInvoice = () => {
				$scope.invoice.flag = 0;
				var emailParameters = {
					keyNames: ['agent_folio_invoice_template']
				};

				$mdDialog.cancel();
				SendEmailsService
					.prepareForEditEmails(emailParameters)
					.then(function (data) {
						var emails = data.emails;

						if (_.isEmpty(emails)) {
							emails.push(SendEmailsService.createEmptyEmailObject());
						}

						var isSendEmailPreview = true;

						SendEmailsService.showEmailPreview(emails[0], data.blocksMenu, isSendEmailPreview).then(function (response) {
							if (response !== "cancel") {
								saveAgentFolioInvoice(response);
							} else {
								$rootScope.$broadcast('close.parent.modal');
							}
						}, function (reason) {
							logger.error(reason, reason, "Error");
						});
					});
			};

			function invoiceSubTotal () {
				var total = 0.00;

				angular.forEach($scope.invoice.charges, function (charge, key) {
					total += charge.qty * charge.cost;
				});

				return _.round(total, 2);
			}

			function calculateDiscount () {
				if ($scope.invoice.discount == 0) {
					return false;
				} else {
					return invoiceSubTotal() * $scope.invoice.discount / 100;
				}
			}


			function calculateInvoiceTax () {
				if ($scope.invoice.tax == 0) {
					return false;
				} else {
					return (invoiceSubTotal() - calculateDiscount()) * $scope.invoice.tax / 100;
				}
			}

			// Calculates the grand total of the invoice
			function calculateGrandTotal () {
				//  saveInvoice();
				return Number(invoiceSubTotal() - calculateDiscount() + calculateInvoiceTax());
			}


			function createInvoice() {
				//console.log('$scope.entityid', $scope.entityId)
				var defer = $q.defer();
				var data = {};
				var entity_id = $scope.entityId;
				data.data = $scope.invoice;
				data.data.entity_type = $scope.entityType;
				data.data.entity_id = entity_id;
				data.data.totalInvoice = calculateGrandTotal();
				data.data.total = calculateGrandTotal();
				data.entity_type = $scope.entityType;
				data.entity_id = entity_id;
				//Save INVOICE
				if (!$scope.invoice_id) {
					InvoiceServices
						.createInvoice(data)
						.then(function (response) {
							if ($scope.prorate) {
								$rootScope.$broadcast('move_invoice.prorate', {
									invoice: $scope.invoice,
									id: _.head(response)
								});
							}
							$scope.busy = false;
							// sendNewInvoiceToLog(_.head(response));
							var invoiceData = {
								data: $scope.invoice,
								id: _.head(response)
							};
							if (!$scope.prorate)
								$rootScope.$broadcast('move_invoice.created', invoiceData);
							defer.resolve(response);
						}, function (reason) {
							$scope.busy = false;
							logger.error(reason, reason, "Error");
							defer.reject(reason);
						});

					return defer.promise;
				} else {
					InvoiceServices
						.editInvoice($scope.invoice_id, data)
						.then(function (response) {
							if ($scope.prorate) {
								$rootScope.$broadcast('move_invoice.prorate', {
									invoice: $scope.invoice,
									id: $scope.invoice_id
								});
							}
							$scope.busy = false;
							// sendNewInvoiceToLog(_.head(response));

							if (!$scope.prorate)
								var invoiceData = {
									data: $scope.invoice,
									id: $scope.invoice_id
								};
							$rootScope.$broadcast('move_invoice.changed', invoiceData);
							defer.resolve(response);
						}, function (reason) {
							$scope.busy = false;
							logger.error(reason, reason, "Error");
							defer.reject(reason);
						});

					return defer.promise;
				}

			}

			$scope.addItemToCharges = () => {

				$scope.invoice.charges.push({
					name: "",
					description: "",
					cost: "",
					qty: ""
				});

			};

			$scope.removeItemForCharges = index => {
				$scope.invoice.charges.splice(index, 1);
			};

			$rootScope.$on('move_invoice.created', () => $mdDialog.cancel());

			/* @ngInject */
			function DialogController($scope, $mdDialog) {}
		}
	};
}
