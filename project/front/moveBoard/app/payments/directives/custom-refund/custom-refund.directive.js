'use strict';
angular.module('app.payment')
	.directive('customRefund', customRefund);

customRefund.$inject = ['apiService', 'moveBoardApi', '$mdDialog', 'common', 'OnlinePaymentService', 'CalculatorServices', '$timeout', 'SweetAlert', 'geoCodingService'];

function customRefund(apiService, moveBoardApi, $mdDialog, common, OnlinePaymentService, CalculatorServices, $timeout, SweetAlert, geoCodingService) {
	return {
		scope: {
			userInfo: '=',
			tripId: '=?',
			paySuccess: '&',
			entityType: '@',
			entityId: '@',
			balance: '=',
			receipt: '=?',
			invoice: '=?',
			overpaid: '='
		},
		template: require('./custom-refund.pug'),
		restrict: 'A',
		link: linker
	};

	function linker($scope, element) {

		$scope.openCustomRefund = (ev) => {
			$mdDialog.show({
				controller: DialogController,
				template: require('./../custom-refund/custom-refund.template.pug'),
				parent: angular.element(document.body),
				targetEvent: ev,
				locals: {
					receipt: $scope.receipt,
					invoice: $scope.invoice,
					userInfo: $scope.userInfo,
					tripId: $scope.tripId,
					paySuccess: $scope.paySuccess,
					entityType: $scope.entityType,
					entityId: $scope.entityId,
					balance: $scope.balance,
				}
			});
		};

		function DialogController($scope, $mdDialog, receipt, invoice, userInfo, tripId, paySuccess, entityType, entityId, balance) {
			$scope.receipt = receipt;
			$scope.invoice = invoice;
			$scope.userInfo = userInfo;
			$scope.tripId = tripId;
			$scope.paySuccess = paySuccess;
			$scope.entityType = entityType;
			$scope.entityId = entityId;
			$scope.balance = balance;

			element.find('#customRefundCardNo').attr({
				'placeholder': '4111 1111 1111 1111',
				'ng-minlength': '19',
				'maxlength': '19'
			});

			$scope.balanceCopy = angular.copy($scope.balance);

			if (!_.isUndefined($scope.receipt)) {
				$scope.payment = {
					account_number: $scope.receipt.value.phone,
					phone: $scope.receipt.value.phone,
					email: $scope.receipt.value.email,
					amount: $scope.receipt.value.amount + '',
					auth_code: "",
					card_type: "",
					jobs: $scope.receipt.value.jobs,
					description: "",
					transaction_id: "Custom Payment",
					pending: false,
					payment_flag: 'Trip #' + $scope.tripId,
					checkN: '',
					total_amount: $scope.receipt.value.amount,
				};
				$scope.autorized = $scope.receipt.value.transaction_id !== "Custom Payment";
			} else {
				$scope.invoice.data = $scope.invoice.data || $scope.invoice.value;
				$scope.payment = {
					account_number: $scope.invoice.data.phone,
					phone: $scope.invoice.data.phone,
					email: $scope.invoice.data.email,
					amount: $scope.invoice.data.overpay + '',
					auth_code: "",
					card_type: "",
					jobs: $scope.invoice.data.jobs_id || $scope.invoice.data.jobs,
					description: "",
					pending: false,
					checkN: '',
					total_amount: $scope.invoice.data.amount,
					invoice_id: $scope.invoice.invoice_id || $scope.invoice.id,
					type: 'Custom Refund',
					transaction_id: "Custom Refund",
					transaction_type: 2,
					refunds: true,
					refund_type: 0,
					payment_method: 'cash'
				};
				$scope.autorized = false;
			}

			$scope.showLoader = false;
			$scope.creditCardTypes = [
				"Visa",
				"Mastercard",
				"AMEX",
				"Discover",
				"Visa Electron"
			];
			$scope.range = common.range;
			$scope.getBalance = () => {
				let resultBalance = 0;
				if ($scope.balanceCopy) {
					if ($scope.payment.amount) {
						resultBalance = parseFloat($scope.payment.amount) + parseFloat($scope.balanceCopy)
					} else {
						resultBalance = parseFloat($scope.balanceCopy)
					}
				}
				return resultBalance
			};
			$scope.hide = function () {
				$scope.balanceCopy = angular.copy($scope.balance);
				$mdDialog.hide();
			};

			$scope.cancel = function () {
				$scope.balanceCopy = angular.copy($scope.balance);
				$scope.payment = {};
				$scope.showLoader = false;
				$mdDialog.cancel();
			};
			function checkCardValidation() {
				let valid = true;
				if (!$scope.cardRefundForm.$valid) {
					valid = false;
					$scope.cardRefundForm.$setSubmitted()
				}
				return valid
			}

			$scope.getGeoCode = () => {
				$scope.payment.zip_code = $scope.payment.zip_code || '';
				$scope.cardRefundForm.zip_code.$setValidity('invalidZip', true);
				if ($scope.payment.zip_code.length == 5) {
					geoCodingService.geoCode($scope.payment.zip_code)
						.then(function (response) {
							$scope.payment.city = response.city;
							$scope.payment.state = response.state;
							if (!response.state) {
								$scope.cardRefundForm.zip_code.$setValidity('invalidZip', false)
							}
						}, function () {
							$scope.payment.city = '';
							$scope.payment.state = '';
							$scope.cardRefundForm.zip_code.$setValidity('invalidZip', false)
						});
				}
			};
			$scope.getCardType = () => {
				if ($scope.payment.ccardN) {
					if ($scope.payment.ccardN.length > 2) {
						let cardNumber = $scope.payment.ccardN.replace(/ /g, '');
						$scope.payment.cctype = OnlinePaymentService.getCardType(cardNumber);
						if (!$scope.payment.cctype) {
							$scope.cardRefundForm.cardNumber.$setValidity("InvalidCardType", false);
							$scope.cardRefundForm.cardNumber.$setTouched()
						} else {
							$scope.cardRefundForm.cardNumber.$setValidity("InvalidCardType", true);
							if ($scope.payment.cctype != "AMEX") {
								element.find('#customRefundCardNo').trigger('focus');
								delete $scope.payment.cvcAmex
							} else {
								element.find('#customRefundCardNo').trigger('focus');
								delete $scope.payment.cvc
							}
						}
					} else {
						delete $scope.payment.cctype;
						$scope.cardRefundForm.cardNumber.$setValidity("InvalidCardType", true)
					}
				}
			};
			$scope.checkCardExpireDateValidation = () => {
				if ($scope.payment.expmonth, $scope.payment.expyear) {
					var expDate = moment({
						year: $scope.payment.expyear,
						month: $scope.payment.expmonth - 1
					}).endOf('month');
					var thisDate = moment().startOf('month');
					if (expDate.isBefore(thisDate)) {
						$scope.cardRefundForm.month.$setValidity("InvalidDate", false);
						$scope.cardRefundForm.month.$setTouched();
						$scope.cardRefundForm.year.$setValidity("InvalidDate", false);
						$scope.cardRefundForm.year.$setTouched()
					} else {
						$scope.cardRefundForm.month.$setValidity("InvalidDate", true);
						$scope.cardRefundForm.year.$setValidity("InvalidDate", true)
					}
				}
			};
			function customRefund() {
				var refund = {
					amount: $scope.payment.amount,
					pending: false,
					phone: $scope.userInfo.phones[0],
					email: $scope.userInfo.email,
					entity_id: $scope.entityId,
					name: $scope.userInfo.name,
					entity_type: $scope.entityType,
					jobs: $scope.payment.jobs,
					date: new Date(),
					refund_notes: $scope.payment.description,
					type: 'Custom Refund',
					transaction_type: 2,
					refunds: true,
					trans_id: 'Custom Payment',
					payment_method: 'cash'
				};
				if (!_.isUndefined($scope.receipt)) {
					refund.receipt_id = $scope.receipt.receipt_id || $scope.receipt.id;
					refund.refund_type = 1;
					refund.invoice_id = $scope.receipt.value.invoice_id
				} else {
					refund.invoice_id = $scope.payment.invoice_id;
					refund.refund_type = 0
				}

				apiService.postData(moveBoardApi.ldReceipt.setReceipt, {
					entity_id: $scope.entityId,
					entity_type: $scope.entityType,
					data: refund
				}).then(res => {
					$mdDialog.hide();
					$scope.showLoader = false;
					$scope.paySuccess()
				}, err => {
					$scope.showLoader = false;
					SweetAlert.swal("Failed!", err.data, "error")
				})
			}

			function refundReceipt() {
				var refund = {
					amount: $scope.payment.amount,
					pending: false,
					phone: $scope.userInfo.phones[0],
					email: $scope.userInfo.email,
					entity_id: $scope.entityId,
					name: $scope.userInfo.name,
					entity_type: $scope.entityType,
					jobs: $scope.payment.jobs,
					date: new Date(),
					refund_notes: $scope.payment.description,
					type: 'Custom Refund',
					transaction_type: 2,
					refunds: true,
					trans_id: 'Custom Payment'
				};
				if (!_.isUndefined($scope.receipt)) {
					refund.receipt_id = $scope.receipt.receipt_id || $scope.receipt.id;
					refund.refund_type = 1;
					refund.invoice_id = $scope.receipt.value.invoice_id
					//refund.trans_id = $scope.receipt.value.transaction_id  // REMOVED FOR TESTS
				} else {
					refund.invoice_id = $scope.payment.invoice_id;
					refund.refund_type = 0
				}

				refund.name = $scope.payment.firstName + ' ' + $scope.payment.lastName;
				refund.card_type = OnlinePaymentService.getCardType($scope.payment.ccardN);
				refund.payment_method = 'creditcard';
				refund.zip_code = $scope.payment.zip_code;
				refund.credit_card = {
					"card_number": $scope.payment.ccardN.replace(/ /g, ''),
					"exp_date": $scope.payment.expmonth + "/" + $scope.payment.expyear.substring(2, $scope.payment.expyear.length),
					"card_code": $scope.payment.cvc || $scope.payment.cvcAmex
				};
				refund.credit_card.card_code = $scope.payment.cctype == "AMEX" ? $scope.payment.cvcAmex : $scope.payment.cvc;

				apiService.postData(moveBoardApi.receipt.refund, {value: refund}).then(res => {
					if (_.head(res.data) == false) {
						SweetAlert.swal("Error!", "Payment refund rejected!", "error");
						$scope.showLoader = false;
						return false;
					} else if (!res.data.status) {
						SweetAlert.swal("Payment refund rejected!", res.data.text, "error");
						$scope.showLoader = false;
						return false;
					} else {
						$mdDialog.hide();
						$scope.showLoader = false;
						$scope.paySuccess()
					}
				}, err => {
					$scope.showLoader = false;
					SweetAlert.swal("Failed!", err.data, "error")
				})
			}

			$scope.save = () => {
				if ($scope.balance) {
					$scope.balance = parseFloat($scope.payment.amount) + parseFloat($scope.balance)
				}
				if ($scope.autorized) {
					if (checkCardValidation()) {
						refundReceipt()
					}
				} else {
					customRefund()
				}
			}
		}
	}
}
