'use strict';
import './onlinePayment.styl'
(() => {

    angular
        .module('app.payment')
        .directive('paymentsOnlinePayment', (apiService, moveBoardApi, $mdDialog, common) => {
            return {
                scope: {
                    openOnlinePayment: '=',
                    userInfo: '=',
                    paySuccess: '=',
                    entityType: '@',
                    entityId: '@'
                },
	            template: require('./onlinePayment.pug'),
                restrict: 'A',
                link: ($scope, element) => {

                    $scope.range = common.range;
                    $scope.currentStep = 1;
                    $scope.showLoader = false;
                    $scope.currentPaymentType = 1;
                    $scope.payment = {};
                    $scope.userInfo = $scope.userInfo || {};

                    $scope.openOnlinePayment = (ev, nid) => {
                        $scope.payment = {
                            nid: nid
                        };
                        $mdDialog.show({
                            controller: DialogController,
                            contentElement: '#addOnlinePayment',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: true
                        });

                    };

                    function GetCardType(number) {
                        // visa
                        var re = new RegExp("^4");
                        if (number.match(re) != null)
                            return "Visa";

                        // Mastercard
                        re = new RegExp("^5[1-5]");
                        if (number.match(re) != null)
                            return "Mastercard";

                        // AMEX
                        re = new RegExp("^3[47]");
                        if (number.match(re) != null)
                            return "AMEX";

                        // Discover
                        re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
                        if (number.match(re) != null)
                            return "Discover";

                        // Visa Electron
                        re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
                        if (number.match(re) != null)
                            return "Visa Electron";

                        return "";
                    }

                    $scope.hide = function () {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.answer = function (answer) {
                        $mdDialog.hide(answer);
                    };

                    function DialogController($scope, $mdDialog) {

                    }

                    $scope.save = () => {

                        $scope.showLoader = true;
                        $scope.payment.phone = $scope.userInfo.phone_number_1,
                        $scope.payment.email = $scope.userInfo.email,
                        $scope.payment.card_type = GetCardType($scope.payment.card_num);
                        $scope.payment.entity_id = $scope.entityId
                        $scope.payment.entity_type = $scope.entityType
                        $scope.payment.date = new Date();
                        $scope.payment.description = $scope.payment.payment_flag + " Payment #" + $scope.entityId
                        $scope.payment.credit_card = {
                            "card_number": $scope.payment.card_num.replace(/ /g, ''),
                            "exp_date": $scope.payment.exp_month + "/" + $scope.checkout_form.cardYear.$viewValue,
                            "card_code": $scope.payment.cvc

                        };
                        $scope.payment.payment_method = 'creditcard';
                        apiService.postData(moveBoardApi.receipt.setOnlineReceipt, $scope.payment).then(data => {
                            $scope.showLoader = false;
                            $scope.payment.id = data.data[0];
                            $scope.updateTable($scope.payment);
                            $mdDialog.cancel();
                            if ($scope.paySuccess) {
                                $scope.paySuccess();
                            }
                        })

                        // $scope.payment = {
                        //     "name": $scope.userInfo.name || '',
                        //     "phone": $scope.userInfo.phone || '',
                        //     "is_invoice": $scope.request.request_invoice,
                        //     "invoice_id": $scope.request.id,
                        //     "email": $scope.userInfo.email || '',
                        //     "zip_code": $scope.userInfo.billing_zip || '',
                        //     "card_type": GetCardType(numb),
                        //     "description": $scope.payment.payment_flag + " Payment #" + $scope.entityId,
                        //     "credit_card": {
                        //         "card_number": $scope.payment.card_num.replace(/ /g, ''),
                        //         "exp_date": $scope.payment.exp_month + "/" + $scope.checkout_form.exp_year.$viewValue,
                        //         "card_code": $scope.secure.cvc
                        //         // "amount": $scope.charge_value.value
                        //     },
                        //     "payment_method": 'creditcard',
                        //     "payment_flag": $scope.payment.payment_flag,
                        //     "date": moment().format('MM/DD/YYYY'),
                        //     "amount": $scope.charge_value.value
                        // };


                    }
                }
            }
        });

})();
