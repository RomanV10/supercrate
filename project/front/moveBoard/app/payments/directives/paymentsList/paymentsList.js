'use strict';
import './paymentsList.styl'
(() => {

    angular
        .module('app.payment')
        .directive('paymentsList', (apiService, moveBoardApi, $mdDialog, $rootScope) => {
            return {
                scope: {
                    userInfo: '=',
                    openPaymentList: '=',
                    openCustomPayment: '=',
                    openOnlinePayment: '=',
                    openReservationCharge: '=',
                    payments: '=',
                    openInvoice: '=',
                    entityType: '@',
                    entityId: '@'
                },
	            template: require('./paymentsList.pug'),
                restrict: 'A',
                link: ($scope, element) => {

                    $scope.showLoader = true;
                    $scope.invoices = [];

                    $scope.updatePayments = (params) => {
                        let loadedCount = 0;
                        $scope.filtered = false
                        $scope.showLoader = true;

                        apiService.postData(moveBoardApi.receipt.getReceipt, { entity_id: $scope.entityId, entity_type: $scope.entityType }).then(data => {
                            if (params && params.requestId) {
                                $scope.filtered = true
                                $scope.requestId = params.requestId
                            }
                                $scope.payments = data.data

                            loadedCount += 1;
                            if (loadedCount == 2) $scope.showLoader = false;
                        })

                        apiService.postData(moveBoardApi.invoice.getInvoices, { entity_id: $scope.entityId, entity_type: $scope.entityType }).then(data => {
                            $scope.invoices = data.data;
                            loadedCount += 1;
                            if (loadedCount == 2) $scope.showLoader = false;
                        })

                    }


                    $scope.openPaymentList = (ev, params) => {
                        $scope.updatePayments(params)
                        $mdDialog.show({
                            contentElement: '#paymentList',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: true
                        });

                    };

                    $scope.hide = function () {
                        $mdDialog.hide();
                    };

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                    $scope.answer = function (answer) {
                        $mdDialog.hide(answer);
                    };

                    $rootScope.$on('move_invoice.created', (ev, data) => {
                        $scope.invoices.push(data);
                    });

                    $scope.isHaveCharge = (invoice, requestId) => {
                        return invoice.data.charges.filter(item => item.nid == requestId).length
                    }

                    $scope.updatePayments();

                    $scope.fab = {
                        isOpen: false,
                        selectedMode: 'md-scale',
                        selectedDirection: 'right'
                    }
                }
            }
        });

})();
