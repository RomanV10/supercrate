'use strict';

angular
	.module('app.lddispatch')
	.factory('OnlinePaymentService', OnlinePaymentService);

OnlinePaymentService.$inject = [];

function OnlinePaymentService() {
	var service = {};
	service.getCardType = getCardType;

	return service;

	function getCardType(number) {
		// visa
		var re = new RegExp("^4");
		if (number.match(re) != null) {
			return "Visa";
		}

		// Mastercard
		re = new RegExp("^5[1-5]");
		if (number.match(re) != null) {
			return "Mastercard";
		}

		// AMEX
		re = new RegExp("^3[47]");
		if (number.match(re) != null) {
			return "AMEX";
		}

		// Discover
		re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
		if (number.match(re) != null) {
			return "Discover";
		}

		// Visa Electron
		re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
		if (number.match(re) != null) {
			return "Visa Electron";
		}

		return "";
	}
}

