(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('payment', {
                        url: '/payment',
                        template: '<div payments-list entity-type="2" entity-id="1"> </div>',
                        title: 'Payments',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });

            }
        ]
    );
})();
