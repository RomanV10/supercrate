'use strict';

import './mobile-styles/mobile-request.styl';
import './mobile-styles/request-tabs.styl';

angular
	.module('app.dashboard')
	.controller('editMobileController', editMobileController);

editMobileController.$inject = ['$state', 'logger', 'RequestServices', '$rootScope', '$stateParams', '$scope'];

function editMobileController($state, logger, RequestServices, $rootScope, $stateParams, $scope) {
	let vm = this;
	vm.busy = true;
	vm.modalInstance = {dismiss: closeEditRequest};
	vm.globallLoading = true;
	vm.redirectLoading = false;
	let requestId = $state.params.id;
	$rootScope.$broadcast('show_menu', false);

	RequestServices
		.getSuperRequest(requestId)
		.then(function (data) {
			$rootScope.$broadcast('show_menu', false);
			vm.requests = data.requests_day;
			vm.initialRequest = data.request;
			vm.initialRequest.initRequest = $stateParams.initRequest;
			vm.listInventories = _.get(vm, 'initialRequest.inventory.inventory_list', []);
			vm.superResponse = data;
			vm.busy = false;
		}, function (reason) {
			vm.busy = false;
			logger.error(reason, reason, "Error");
		});

	function closeEditRequest() {
		$state.go('dashboard');
		$rootScope.$broadcast('show_menu', true);
	}

	$scope.$on('loading-request-is-stopped', () => {
		vm.globallLoading = false;
	});

	$scope.$on('redirect-user-to-confirmation-page', () => {
		vm.redirectLoading = true;
	})
}
