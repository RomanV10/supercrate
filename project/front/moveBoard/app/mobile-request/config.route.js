angular.module("app")
	.config([
			"$stateProvider",
			function ($stateProvider)
			{
				$stateProvider
					.state('mobile', {
						url: '/mobile',
						abstract: true,
						template: '<ui-view/>',
						title: 'Mobile Request',
						data: {
							permissions: {
								except: ['anonymous','foreman','helper']
							}
						}
					})
					.state('mobile.openEditRequest', {
						url: '/edit_request/:id',
						template: require('./mobileRequest.html'),
						title: 'Mobile Request',
						controller: 'editMobileController as vm',
						data: {
							permissions: {
								except: ['anonymous', 'foreman', 'helper']
							}
						}
					});
			}
		]
	);
