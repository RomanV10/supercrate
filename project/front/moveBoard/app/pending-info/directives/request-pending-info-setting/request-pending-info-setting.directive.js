'use strict';

import './request-pending-info-setting.styl';

angular
	.module('pending-info')
	.directive('requestPendingInfoSetting', requestPendingInfoSetting);

requestPendingInfoSetting.$inject = [];

function requestPendingInfoSetting () {
	return {
		restrict: 'E',
		scope: {
			field: '=',
			editRequest: '=',
			message: '=',
		},
		template: require('./request-pending-info-setting.tpl.pug'),
		link: requestPendingInfoSettingLink,
	};
	
	function requestPendingInfoSettingLink($scope) {
		const FIELD = 'field_allow_to_pending_info';
		$scope.switchSetting = switchSetting;
		
		function switchSetting () {
			$scope.editRequest[FIELD] = Number($scope.field);
			$scope.message[FIELD] = {
				label: 'custom',
				oldValue: '',
				newValue: `Pending-info status is ${$scope.field ? 'allowed' : 'disable'}`,
			};
		}
	}
}
