'use strict';

import './pending-info-settings.styl';

angular
	.module('pending-info')
	.directive('pendingInfoSettings', pendingInfoSettings);

pendingInfoSettings.$inject = ['AccountPageService', 'SweetAlert', 'Raven'];

function pendingInfoSettings(AccountPageService, SweetAlert, Raven) {
	return {
		restrict: 'E',
		scope: {
			setting: '='
		},
		template: require('./pending-info-settings.tpl.pug'),
		link: pendingInfoSettingsLink,
	};
	
	function pendingInfoSettingsLink($scope) {
		const SETTING_NAME = 'allowToPendingInfo';
		
		$scope.updateSetting = updateSetting;
		
		function updateSetting() {
			AccountPageService.updateSetting(SETTING_NAME, Number($scope.setting))
				.then(() => {
					toastr.success('Setting was updated');
				})
				.catch(() => {
					SweetAlert.swal('Setting was not updated', 'Please, try again', 'error');
					Raven.captureException('Setting was not updated');
				});
		}
	}
}
