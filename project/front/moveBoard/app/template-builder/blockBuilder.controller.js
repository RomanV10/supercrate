(function () {
    'use strict';

    angular
        .module('app.tplBuilder')
        .controller('BlockBuilderController', BlockBuilderController);

	/*@ngInject*/
    function BlockBuilderController($scope, EmailBuilderRequestService, TemplateBuilderService, $log,
                                    logger, EditElementService, tplBuilderAuthService, $uibModal, FileSaver, Blob, config,
                                    $rootScope, MainBlockService, SweetAlert, $q, $timeout) {
        var vm = this;

        vm.getBlockTypeName = TemplateBuilderService.getBlockTypeName;
        vm.styles = TemplateBuilderService.getStyles();

        vm.blockTemplateIsEmpty = blockTemplateIsEmpty;
        vm.blockTemplateIsNotEmpty = blockTemplateIsNotEmpty;
        vm.createBaseBlock = createBaseBlock;
        vm.resetBaseBlock = resetBaseBlock;
        vm.setBaseBlockForEdit = setBaseBlockForEdit;
        vm.updateBaseBlock = updateBlock;
        vm.copyBaseBlock = copyBaseBlock;
        vm.deleteBaseBlock = deleteBaseBlock;
        vm.showImportDialog = showImportDialog;
		vm.showExportDialog = showExportDialog;
		vm.showTemplateBuilder = showTemplateBuilder;

		vm.baseBlock = TemplateBuilderService.initBlock();
		vm.isAdmin = tplBuilderAuthService.isAdmin;
		vm.blocksMenu = null;
		$scope.styleFilter = {'data': {'style': 0}};
        vm.spinner = 0;
	    const BLOCK_BUILDER_TEMPLATE_SELECTOR = '.blockBuilder';
        var exportBlocks = {};
        var startTemplateTable = "<table>\n    <tbody>\n    <tr>\n        <td id=\"block\" style=\"width:640px; background: white\">\n            <div class=\"row-edit\" data-type=\"large-text\">\n                <!-- Please insert your template bellow this comment-->\n\n";
        var endTemplateTable = "\n\n                <!-- Please insert your template higher this comment-->\n            </div>\n        </td>\n    </tr>\n    </tbody>\n</table>";
        vm.isShowPreviewFrame = false;
        vm.companyLogoUrl = config.logoUrl;
        vm.blockTypes = $rootScope.fieldData.enums.template_block_type;

        loadBlocks();

        $scope.$watch('vm.spinner', function (newValue) {
            vm.busy = newValue > 0;
        });

        function showSpinner() {
            vm.spinner += 1;
        }

        function hideSpinner() {
            vm.spinner -= 1;
        }

        $scope.$watch('vm.baseBlock.template', function(newTemplate, oldTemplate) {
            if (angular.isDefined(newTemplate) && blockTemplateIsNotEmpty() && !newTemplate.includes("row-edit")) {
                var result = "";
                result += startTemplateTable;
                result += newTemplate;
                result += endTemplateTable;
                vm.baseBlock.template = result;
            }
        });

        function blockTemplateIsEmpty() {
            return EditElementService.isEmptyString(vm.baseBlock.template);
        }

        function blockTemplateIsNotEmpty() {
            return !blockTemplateIsEmpty();
        }

        function resetBaseBlock() {
            vm.baseBlock = TemplateBuilderService.initBlock();
        }

        function setBaseBlockForEdit(block) {
            vm.baseBlock = angular.copy(block);
            setDefaultBlockData();
        }

        function setDefaultBlockData() {
            if (vm.baseBlock.data == null) {
                vm.baseBlock.data = {'style': 0};
            } else {
                if (!angular.isDefined(vm.baseBlock.data.templateType)) {
                    vm.baseBlock.data['templateType'] = 0;
                }
            }
        }

        function updateBlock() {
			if(vm.baseBlock.data.isMainBlock) {
				confirmUpdateBaseBlockCopies();
			} else {
				updateBaseBlock();
			}
		}

        function updateBaseBlock() {
            showSpinner();
			if(vm.baseBlock.data.isMainBlock && !vm.baseBlock.data.mainId){
				vm.baseBlock.data.mainId = vm.baseBlock.id;
			}
            EmailBuilderRequestService
                .updateBaseBlock(vm.baseBlock)
                .then(function (response) {
                	let isMainBlockTriggerChanged = isChangeMainBlockTrigger();
					if(vm.baseBlock.data.isMainBlock || isMainBlockTriggerChanged){
						updateBaseBlockCopies(vm.baseBlock);
					}else{
						hideSpinner();
					}
                    logger.success('Block was successfully updated!', response.status, 'Success');
                    resetBaseBlock();
                    loadBlocks();
                }, function (error) {
                    hideSpinner();
                    logger.error('Error updating the block!', JSON.stringify(error), 'Error');
                });
        }

        function updateBaseBlockCopies(block) {
        	if(!block.data.mainId){
        		block.data.mainId = block.id;
			}
			let isMainBlockTriggerChanged = isChangeMainBlockTrigger();
			let tempsWithCurrentBlock = MainBlockService.getTemplatesWithCurrentBlock(block);
			let tempsWithUpdatedBlock = MainBlockService.changeTemplatesBlocks(tempsWithCurrentBlock , block, isMainBlockTriggerChanged);
			let preparedTemplatesPromises = updateTemplate(tempsWithUpdatedBlock);
			$q.all(preparedTemplatesPromises).then(templates => {
				saveUpdatedTemplate(templates);
			});
		}

		function saveUpdatedTemplate(templates) {
			let promises = MainBlockService.updateTemplates(templates);

			if(promises.length){
				$q.all(promises)
					.then(() => {
						hideSpinner();
						SweetAlert.swal("Templates were successfully updated", "", "success");
					}, () => {
						hideSpinner();
						SweetAlert.swal("Error updating templates", "", "error");
					});
			} else {
				hideSpinner();
			}
		}

	    function updateTemplate(temps) {
		    let prevPromise;

		    let result = temps.map(item => {
			    let defer = $q.defer();
			    let updateTemplateItem = () => {
				    let copyTemplate = (item) => {
					    $timeout(() => {
						    $scope.$apply(() => {
							    item.template = TemplateBuilderService.getCurrentHTMLTemplate(BLOCK_BUILDER_TEMPLATE_SELECTOR);
							    defer.resolve(item);
						    });
					    });
				    };

				    let setUpTemplate = (item) => {
					    $timeout(() => {
						    $scope.$apply(() => {
							    vm.template = item;
							    vm.showTemplate = true;
							    copyTemplate(item);
						    });
					    });
				    };

				    let removeTemplate = (item) => {
					    $timeout(() => {
						    $scope.$apply(() => {
							    vm.showTemplate = false;
							    setUpTemplate(item);
						    });
					    });
				    };

				    removeTemplate(item);
			    };

			    if (prevPromise) {
				    prevPromise.then(() => {
					    updateTemplateItem(item);
				    });
			    } else {
				    updateTemplateItem(item);
			    }

			    prevPromise = defer.promise;
			    return defer.promise;
		    });

		    return result;
	    }

		function isChangeMainBlockTrigger() {
        	let oldBlock = _.find(exportBlocks, {'id': vm.baseBlock.id});
			let oldMainBlockStatus = _.get(oldBlock, 'data.isMainBlock');
			let modifiedMainBlockStatus = _.get(vm.baseBlock, 'data.isMainBlock');

			return oldMainBlockStatus !== modifiedMainBlockStatus;
		}

        function confirmUpdateBaseBlockCopies() {
			SweetAlert
				.swal({
						title: "Are you sure?",
						text: "This is a Main Block and it will update all its copies. All copies will be overwritten. It can take a few minutes.",
						type: "warning",
						showCancelButton: true,
						cancelButtonText: "No",
						confirmButtonText: "Yes"
					},
					isConfirm => {
						if (isConfirm) {
							updateBaseBlock();
						}
					});
		}

        function copyBaseBlock(block) {
            vm.baseBlock = angular.copy(block);
            vm.baseBlock.id = null;
	        vm.baseBlock.data.isMainBlock = false;

	        if (vm.baseBlock.data.mainId) {
		        delete vm.baseBlock.data.mainId;
	        }

            setDefaultBlockData();
        }

        function createBaseBlock() {
            showSpinner();
            EmailBuilderRequestService
                .createBaseBlock(vm.baseBlock)
                .then(function (response) {
                    hideSpinner();
                    logger.success('Create block successful!', response.status, 'Success');
                    resetBaseBlock();
                    loadBlocks();
                }, function (error) {
                    hideSpinner();
                    logger.error('Error create block!', JSON.stringify(error), 'Error');
                });
        }

        function deleteBaseBlock() {
        	let isMainBlockTriggerChanged = isChangeMainBlockTrigger();
        	if(vm.baseBlock.data.isMainBlock || isMainBlockTriggerChanged){
				SweetAlert
					.swal({
						title: "You can't remove the Main Block",
						text: "If you want to remove this block you should uncheck the Main Block checkbox and update it before",
						type: "warning"
					});
			}else{
				showSpinner();
				EmailBuilderRequestService
					.deleteBaseBlock(vm.baseBlock)
					.then(function (response) {
						hideSpinner();
						logger.success('Delete base block successful!', response.status, 'Success');
						resetBaseBlock();
						loadBlocks();
					}, function (error) {
						hideSpinner();
						logger.error('Error delete base block!', JSON.stringify(error), 'Error');
					});
			}

        }

        function loadBlocks() {
            showSpinner();
            EmailBuilderRequestService
                .getBaseBlocks()
                .then(function (response) {
                    hideSpinner();
                    exportBlocks = angular.copy(response.data);
                    vm.blocksMenu = TemplateBuilderService.createBlockMenuObject(response.data);
                }, function (error) {
                    hideSpinner();
                    $log.error(JSON.stringify(error));
                });
        }

        function showImportDialog() {
            $uibModal.open({
	            template: require('./templates/import-dialog.html'),
                controller: ImportBlocksModalCtrl,
                size: 'lg',
                backdrop: false
            });
        }

        function ImportBlocksModalCtrl($scope, $uibModalInstance) {
            $scope.busyModal = false;
            $scope.importHeader = 'Import block or blocks';
            $scope.importButton = 'Import';
            $scope.content = '';
            $scope.close = close;
            $scope.importData = importData;
            $scope.spinner = 0;
            $scope.errors = 0;
            $scope.isImportDataEmpty = isImportDataEmpty;

            $scope.$watch('spinner', function (spinner) {
                $scope.busyModal = spinner > 0;
            });

            function showSpinner() {
                $scope.spinner += 1;
            }

            function hideSpinner(status) {
                $scope.spinner -= 1;

                if (!status) {
                    $scope.errors += 1;
                }

                if ($scope.spinner == 0) {
                    if ($scope.errors > 0) {
                        logger.error('Error when create blocks!', '', 'Error');
                    } else {
                        logger.success('All blocks create successful!', '', 'Success');
                        loadBlocks();
                    }
                    $scope.errors = 0;
                }
            }

            function close() {
                $uibModalInstance.dismiss('cancel');
            }

            function importData() {
                var blocks = JSON.parse($scope.content);
                angular.forEach(blocks, function(block, key) {
                    showSpinner();
                    EmailBuilderRequestService
                        .createBaseBlock(block)
                        .then(function (respose) {
                            hideSpinner(true);
                        }, function (error) {
                            hideSpinner(false);
                        });
                });
                $scope.content = '';
            }

            function isImportDataEmpty() {
                return EditElementService.isEmptyString($scope.content);
            }
        }

        function showExportDialog() {
            $uibModal.open({
	            template: require('./templates/export-dialog.html'),
                controller: ExportBlocksModalCtrl,
                size: 'lg',
                backdrop: false,
                resolve: {
                    blocks: function () {
                        return exportBlocks;
                    }
                }
            });
        }

        function ExportBlocksModalCtrl($scope, $uibModalInstance, blocks) {
            $scope.exportHeader = 'Export blocks';
            $scope.exportButton = 'Export blocks in file';
            $scope.close = close;
            $scope.exportData = exportData;

            function close() {
                $uibModalInstance.dismiss('cancel');
            }

            function exportData() {
                var textBlocks = JSON.stringify(blocks, null, '  ');
                var data = new Blob([textBlocks], { type: 'text/plain;charset=utf-8' });
                FileSaver.saveAs(data, 'blocks.json');
            }
        }

        function showTemplateBuilder() {
            $rootScope.$broadcast('template_builder.select-first-tab');
        }
    }

})();
