(function () {
    'use strict';

    angular
        .module('app.tplBuilder')
        .controller('TplBuilderController', TplBuilderController);

    TplBuilderController.$inject = ['$scope', 'EmailBuilderRequestService', 'TemplateBuilderService', '$log', 'dragulaService', 'SweetAlert', 'logger', 'tplBuilderAuthService', '$uibModal', 'EditElementService', 'FileSaver', 'Blob', '$q', 'templateFolderService', '$rootScope', 'config'];

    function TplBuilderController($scope, EmailBuilderRequestService, TemplateBuilderService, $log, dragulaService, SweetAlert, logger, tplBuilderAuthService, $uibModal, EditElementService, FileSaver, Blob, $q, templateFolderService, $rootScope, config) {
        var vm = this;

        vm.getBlockTypeName = TemplateBuilderService.getBlockTypeName;
        vm.styles = TemplateBuilderService.getStyles();
        vm.addBlockToTemplate = addBlockToTemplate;
        vm.templateIsEmpty = templateIsEmpty;
        vm.templateIsNotEmpty = templateIsNotEmpty;
        vm.createTemplate = createTemplate;
        vm.createNewTemplate = createNewTemplate;
        vm.updateTemplate = updateTemplate;
        vm.clearTemplate = clearTemplate;
        vm.editTemplate = editTemplate;
        vm.copyTemplate = copyTemplate;
        vm.deleteTemplate = deleteTemplate;
        vm.showImportDialog = showImportDialog;
        vm.showExportDialog = showExportDialog;
        vm.addFolder = addFolder;
        vm.removeFolder = removeFolder;
        vm.setFolderForEdit = setFolderForEdit;
        vm.editFolder = editFolder;
        vm.cancelEditFolder = cancelEditFolder;
	    vm.isChooseChildTemplate = isChooseChildTemplate;
		vm.modalOptions = {isTemplateBuilderTab: true};
	    vm.isAdmin = tplBuilderAuthService.isAdmin;
        vm.isDefaultFolder = templateFolderService.isDefaultFolder;
        vm.templatesMenu = null;
        vm.blocksMenu = null;
        vm.template = TemplateBuilderService.initTemplate();
        var standardTemplate = TemplateBuilderService.initTemplate();
        vm.templatesVisibility = true;
        $scope.styleFilter = {'data': {'style': 0}};
        $scope.filterByKeyName = {"key_name": ""};
        $scope.comparatorFunction = (actual, expected) => {
            return _.isEmpty(expected) || _.isEqual(actual, expected);
        };
        vm.spinner = 0;
        var exportTemplates = {};
        vm.templateKeyNames = TemplateBuilderService.getTemplateKeyNames();
        vm.scopeTemplate = false; // true edit style on all template
        vm.templateFolders = {"lastIndex": 0, "folders":[]};
        vm.newFolder = "";
        vm.companyLogoUrl = config.logoUrl;
        vm.isShowPreviewFrame = false;
        var updateTemplateMenu = false;
        var orderTemplateObject = null;
	    const TEMPLATE_BUILDER_TEMPLATE_SELECTOR = '.tplBuilder .row .frame';


        initTemplateBuilder();

        function initTemplateBuilder() {
            showSpinner();
            var enumType = $rootScope.fieldData.enums;
            var templateFolders = EmailBuilderRequestService.getTemplateFolders();
            var templates = EmailBuilderRequestService.getTemplates();
            var baseBlocks = EmailBuilderRequestService.getBaseBlocks();
            var orderTemplateObjectPromise = EmailBuilderRequestService.getOrderTemplateSettings();

            $q.all([enumType, templateFolders, templates, baseBlocks, orderTemplateObjectPromise])
                .then(function (result) {
                    // block types
                    vm.blockTypes = result[0].template_block_type;
                    // template folders
                    vm.templateFolders = result[1];
                    $rootScope.templateBuilder.templateFolders = result[1];
                    // templates
                    $rootScope.templateBuilder.emailTemplates = result[2].data;
                    exportTemplates = angular.copy(result[2].data);
                    updateTemplateMenuObject();
                    // blocks
                    vm.blocksMenu = TemplateBuilderService.createBlockMenuObject(result[3].data);
                    // order object
                    if (Object.keys(result[4]).length == 0) {
                        $rootScope.templateBuilder.orderObject = result[4];
                        initOrderTemplateObject();
                    } else {
                        $rootScope.templateBuilder.orderObject = result[4];
                        orderTemplateObject = result[4];
                    }
                    hideSpinner();
                });
        }

        function initOrderTemplateObject() {
            angular.forEach($rootScope.templateBuilder.emailTemplates, function (value, key) {
                if(value.data != null) {
                    if (angular.isUndefined($rootScope.templateBuilder.orderObject[value.id])) {
                        $rootScope.templateBuilder.orderObject[value.id] = value.id;
                    }
                }
            });
        }

        $scope.$watch('vm.spinner', function (spinner) {
            vm.busy = spinner > 0;
        });

        function showSpinner() {
            vm.spinner += 1;
        }

        function hideSpinner() {
            vm.spinner -= 1;
            if (vm.spinner < 0) {
                vm.spinner = 0;
            }
        }

        function initTemplates() {
            vm.template = TemplateBuilderService.initTemplate();
            standardTemplate = TemplateBuilderService.initTemplate();
            vm.blockChosen = false;
        }

        function addBlockToTemplate(block) {
            var copyBlock = {};
            copyBlock = angular.copy(block);
            vm.template.blocks.push(copyBlock);
        }

        $scope.$watch('vm.template.blocks', function() {
            $scope.$broadcast('blockTemplateChanged');
        }, true);

        function templateIsEmpty() {
            return vm.template.blocks.length == 0;
        }

        function templateIsNotEmpty() {
            return !templateIsEmpty();
        }

        function createTemplate() {
            showSpinner();
            vm.template.template = TemplateBuilderService.getCurrentHTMLTemplate(TEMPLATE_BUILDER_TEMPLATE_SELECTOR);
            EmailBuilderRequestService
                .createTemplate(vm.template)
                .then(function (response) {
                    hideSpinner();
                    logger.success('Template was created successfully!', response.status, 'Success');
                    loadTemplates();
                    updateStandardTemplate();
                }, function (error) {
                    hideSpinner();
                    logger.error('Error creating a template!', JSON.stringify(error), 'Error');
                });
        }

        function updateTemplate() {
            showSpinner();
	        vm.template.template = TemplateBuilderService.getCurrentHTMLTemplate(TEMPLATE_BUILDER_TEMPLATE_SELECTOR);
            EmailBuilderRequestService
                .updateTemplate(vm.template)
                .then(function (response) {
                    hideSpinner();
                    logger.success('Template was updated successfully!', response.status, 'Success');
                    loadTemplates();
                    updateStandardTemplate();
                }, function (error) {
                    hideSpinner();
                    logger.error('Error updating a template!', JSON.stringify(error), 'Error');
                });
        }

        function updateStandardTemplate() {
            standardTemplate = angular.copy(vm.template);
        }

        function clearTemplate() {
            if (isChangeTemplate()) {
                var confirmButtonText = 'Create';

                if (vm.template.id != null) {
                    confirmButtonText = 'Save';
                }

                SweetAlert.swal({
                        title: "You haven't saved the changes!",
                        text: "You have unsaved changes! Do you want to cancel them?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: confirmButtonText,
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            //save then close
                            if (vm.template.id != null) {
                                updateTemplate();
                            } else {
                                createTemplate();
                            }
                        }
                        initTemplates();
                    });
            } else {
                initTemplates();
            }
        }

        function isChangeTemplate() {
            return !angular.equals(vm.template, standardTemplate);
        }

        function loadTemplates() {
            showSpinner();
            EmailBuilderRequestService
                .getTemplates()
                .then(function (response) {
                    hideSpinner();
                    $rootScope.templateBuilder.emailTemplates = response.data;
                    exportTemplates = angular.copy(response.data);
                    showSpinner();
                    TemplateBuilderService
                        .createTemplateMenuObject(response.data, vm.templateFolders)
                        .then(function(menu) {
                            vm.templatesMenu = menu;
                            hideSpinner();
                        }, function() {
                            hideSpinner();
                        });
                }, function (error) {
                    hideSpinner();
                    logger.error('Error when loading a template!', JSON.stringify(error), 'Error');
                });
        }

        function editTemplate(template) {
            prepareTemplate(template);
        }

        function copyTemplate(template) {
            var newTemplate = angular.copy(template);
            newTemplate.id = null;
            prepareTemplate(newTemplate);
        }

        function prepareTemplate(template) {
            if (template.data == null) {
                template.data = {
                    'description': '',
                    'templateType': 0
                }
            } else {
                if (!angular.isDefined(template.data.templateType)) {
                    template.data['templateType'] = 0;
                }
            }
            if (isChangeTemplate()) {
                var confirmButtonText = 'Create';

                if (vm.template.id != null) {
                    confirmButtonText = 'Save';
                }

                SweetAlert.swal({
                        title: "You haven't saved the changes!",
                        text: "You have unsaved changes! Do you want to cancel them?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: confirmButtonText,
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            //save then close
                            if (vm.template.id != null) {
                                updateTemplate();
                            } else {
                                createTemplate();
                            }
                        }

                        initByTemplate(template);
                    });
            } else {
                initByTemplate(template);
            }
        }

        function initByTemplate(template) {
            vm.blockChosen = false;
            standardTemplate = angular.copy(template);
            vm.template = angular.copy(template);
        }


        function deleteTemplate(id) {
            SweetAlert.swal({
                    title: "You are removing the template!",
                    text: "The template will be deleted! Do you want to delete it?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55", confirmButtonText: 'Delete',
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        showSpinner();
                        EmailBuilderRequestService
                            .deleteTemplate(id)
                            .then(function (response) {
                                hideSpinner();
                                logger.success('Template was deleted successfully!', response.status, 'Success');
                                loadTemplates();
                            }, function (error) {
                                hideSpinner();
                                logger.error('Error deleting a template!', JSON.stringify(error), 'Error');
                            });
                    }
                });
        }

        dragulaService.options($scope, 'preview-template-bag', {
            moves: function (el, container, handle) {
                var wrapHandle = angular.element(handle);
                if (!angular.isUndefined(wrapHandle)) {
                    updateTemplateMenu = wrapHandle.hasClass('drag-templates');
                }
                return wrapHandle.hasClass('drag-templates');
            }
        });

        $scope.$watch('vm.templatesMenu', function(newMenu, oldMenu) {
            if (updateTemplateMenu) {
                showSpinner();
                var copyNewMenu = {};
                var copyOldMenu = {};
                copyNewMenu = angular.copy(newMenu);
                copyOldMenu = angular.copy(oldMenu);
                for (var index in copyNewMenu) {
                    var isChangedMenu = !angular.equals(copyNewMenu[index], copyOldMenu[index]);

                    if (isChangedMenu) {
                        var newTemplates = copyNewMenu[index].templates;
                        var oldTemplates = copyOldMenu[index].templates;
                        for (var i = 0; i < newTemplates.length; i++) {
                            var newTemplate = newTemplates[i];
                            var oldTemplate = oldTemplates[i];
                            var isChangedTemplate = !angular.equals(newTemplate, oldTemplate);
                            if (isChangedTemplate) {
                                orderTemplateObject[newTemplate.id] = oldTemplate.orderId;
                                newTemplate.orderId = oldTemplate.orderId;
                                newTemplates[i] = newTemplate;
                            }
                        }
                        copyNewMenu[index].templates = newTemplates;
                        updateTemplateOrderObject();
                    }
                }

                updateTemplateMenu = false;
                vm.templatesMenu = copyNewMenu;
                hideSpinner();
            }
        }, true);

        function loadBlocks() {
            showSpinner();
            EmailBuilderRequestService
                .getBaseBlocks()
                .then(function (response) {
                    hideSpinner();
                    vm.blocksMenu = TemplateBuilderService.createBlockMenuObject(response.data);
                }, function (error) {
                    hideSpinner();
                    logger.error('Error when loading blocks!', JSON.stringify(error), 'Error');
                });
        }

        function createNewTemplate() {
            selectTabByName(blocksTabName);
            prepareTemplate(TemplateBuilderService.initTemplate());
        }

        // Menu tabs
        vm.select = select;
        vm.isSelectBlockBuilderTab = false;
        var blocksTabName = 'Blocks';
        var blockBuilderTabName = 'Block builder';
        vm.tabs =
            [
                {name: 'Templates', url: 'app/template-builder/templates/templates-menu.html', selected: true, icon: ''},
                {name: blocksTabName, url: 'app/template-builder/templates/blocks-menu.html', selected: false, icon: ''},
                //{name: 'Style', url: 'app/template-builder/templates/style-menu.html', selected: false, icon: ''},
                {name: 'Template folders', url: 'app/template-builder/templates/template-folders-panel.html', selected: false, icon: ''},
                {name: blockBuilderTabName, url: 'app/template-builder/templates/block-builder.html', selected: false, icon: ''}
            ];


        function select(tab) {
        	if(tab.name === blockBuilderTabName){
        		clearTemplate();
			}
            angular.forEach(vm.tabs, function (tab) {
                tab.selected = false;
            });

            vm.isSelectBlockBuilderTab = _.isEqual(blockBuilderTabName, tab.name);
            tab.selected = true;
        }

        function selectTabByName(name) {
            angular.forEach(vm.tabs, function (tab) {
                tab.selected = tab.name == name;
            });
        }

	    function showImportDialog() {
		    $uibModal.open({
			    template: require('./templates/import-dialog.html'),
			    controller: ImportTemplatesModalCtrl,
			    size: 'lg',
			    backdrop: false
		    });
	    }

        function ImportTemplatesModalCtrl($scope, $uibModalInstance) {
            $scope.busyModal = false;
            $scope.importHeader = 'Import template or templates';
            $scope.importButton = 'Import';
            $scope.content = '';
            $scope.close = close;
            $scope.importData = importData;
            $scope.spinner = 0;
            $scope.errors = 0;
            $scope.isImportDataEmpty = isImportDataEmpty;

            $scope.$watch('spinner', function (spinner) {
                $scope.busyModal = spinner > 0;
            });

            function showSpinner() {
                $scope.spinner += 1;
            }

            function hideSpinner(status) {
                $scope.spinner -= 1;

                if (!status) {
                    $scope.errors += 1;
                }

                if ($scope.spinner == 0) {
                    if ($scope.errors > 0) {
                        logger.error('Error when creating templates!', '', 'Error');
                    } else {
                        logger.success('All templates were created successfully!', '', 'Success');
                        loadTemplates();
                    }
                    $scope.errors = 0;
                }
            }

            function close() {
                $uibModalInstance.dismiss('cancel');
            }

            function importData() {
                var templates = JSON.parse($scope.content);
                angular.forEach(templates, function (template, key) {
                    showSpinner();
                    EmailBuilderRequestService
                        .createTemplate(template)
                        .then(function (response) {
                            hideSpinner(true);
                        }, function (error) {
                            hideSpinner(false);
                        });
                });
                $scope.content = '';
            }

            function isImportDataEmpty() {
                return EditElementService.isEmptyString($scope.content);
            }
        }

        function showExportDialog() {
          $uibModal.open({
	            template: require('./templates/export-dialog.html'),
                controller: ExportTemplatesModalCtrl,
                size: 'lg',
                backdrop: false,
                resolve: {
                    templates: function () {
                        return exportTemplates;
                    }
                }
            });
        }

        function ExportTemplatesModalCtrl($scope, $uibModalInstance, templates) {
            $scope.exportHeader = 'Export templates';
            $scope.exportButton = 'Export templates in file';
            $scope.close = close;
            $scope.exportData = exportData;

            function close() {
                $uibModalInstance.dismiss('cancel');
            }

            function exportData() {
                var textTemplates = JSON.stringify(templates, null, '  ');
                var data = new Blob([textTemplates], {type: 'text/plain;charset=utf-8'});
                FileSaver.saveAs(data, 'templates.json');
            }
        }

        function addFolder() {
            if (vm.newFolder != ""
                && vm.newFolder != "Default") {
                vm.templateFolders.folders.push({"index": vm.templateFolders.lastIndex.toString(), "name": vm.newFolder});
                vm.templateFolders.lastIndex += 1;
                vm.newFolder = "";
                updateTemplateFolders();
            }
        }

        function setFolderForEdit (folder) {
            vm.indexEditFolder = folder.index;
            vm.newFolder = folder.name;
        }

        function editFolder () {
            if (vm.newFolder != ""
                && vm.newFolder != "Default") {
                var index = _.findIndex(vm.templateFolders.folders, {"index": vm.indexEditFolder});

                if (index > -1) {
                    vm.templateFolders.folders[index].name = vm.newFolder;
                }

                cancelEditFolder();
                updateTemplateFolders();
            }
        }

        function cancelEditFolder () {
            vm.indexEditFolder = undefined;
            vm.newFolder = "";
        }

        function removeFolder(index) {
            vm.templateFolders.folders.splice(index, 1);
            updateTemplateFolders();
        }

        function updateTemplateFolders() {
            EmailBuilderRequestService
                .saveTemplateFolders(vm.templateFolders)
                .then(function () {
                    updateTemplateMenuObject();
                }, function () {
                });
        }

        function updateTemplateMenuObject() {
            $rootScope.templateBuilder.templateFolders = vm.templateFolders;
            showSpinner();
            TemplateBuilderService
                .createTemplateMenuObject(exportTemplates, vm.templateFolders)
                .then(function(menu) {
                    vm.templatesMenu = menu;
                    hideSpinner();
                }, function() {
                    hideSpinner();
                });
        }

        function updateTemplateOrderObject() {
            showSpinner();
            EmailBuilderRequestService
                .saveOrderTemplateSettings(orderTemplateObject)
                .then(function() {
                    hideSpinner();
                }, function() {
                    hideSpinner();
                    $log.error("Error when saving order template object")
                });
        }

        $scope.$on('template_builder.select-first-tab', onSelectFirstTab);

        function onSelectFirstTab() {
            select(vm.tabs[0]);
            initTemplateBuilder();
        }

        function isChooseChildTemplate (templates) {
            return _.find(templates, {"id": vm.template.id});
        }
    }

})();
