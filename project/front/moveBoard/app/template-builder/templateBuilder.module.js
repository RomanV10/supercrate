'use strict';

let angularDragula = require('angularjs-dragula');

angular
	.module('app.tplBuilder', [angularDragula(angular), 'textAngular', 'colorpicker.module', 'blocks.logger', 'ngFileSaver', 'app.settings']);