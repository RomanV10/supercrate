(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('onFinishRender', finishRender);

    finishRender.$inject = ['$timeout', '$rootScope'];

    function finishRender($timeout, $rootScope) {
        var directive = {
            restrict: 'A',
            link: linker
        };

        return directive;

        function linker(scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    $rootScope.$broadcast('renderFinished');
                });
            }
        }
    };
})();