(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('previewBlock', previewBlock);

    function previewBlock() {
        var directive = {
            restrict: 'E',
            scope: {
                content: "="
            },
            link: linker,
        };
        return directive;

        function linker($scope, element) {
            var iframe = document.createElement('iframe');
            var element0 = element[0];
            element0.appendChild(iframe);
            var body = iframe.contentDocument.body;
            var startContainer = "<table align=\"center\" style='margin:auto;'>\n    <tbody>\n    <tr>\n <td> \n";
            var endContainder = "\n </td>\n    </tr>\n    </tbody>\n</table>\n";

            $scope.$watch('content', function () {
                var content = "";
                content += startContainer;
                content += $scope.content;
                content += endContainder;
                body.innerHTML = content;
            });
        }
    }
})();