(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('ngEnter', ngEnter);

    function ngEnter() {
        var directive = {
            restrict: 'A',
            link: linker
        };
        return directive;

        function linker($scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    $scope.$apply(function(){
                        $scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        }
    }
})();