(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('templatePreview', templateDirective);

    function templateDirective() {
        var directive = {
            restrict: 'EA',
            scope: {
                template: '=',
                deleteButton: '=',
                editorButton: '=',
                dragdropButton: '=',
                editChildElements: '=',
                companyLogoUrl: '=',
                editBlockOptions: '='
            },
            link: linker,
            controller: ['$scope', 'dragulaService', controller],
	        template: require('../templates/template.html'),
        };

        return directive;

        function linker($scope, element) {
            $scope.deleteBlockFromTemplate = deleteBlockFromTemplate;

            function deleteBlockFromTemplate(index) {
                $scope.template.blocks.splice(index, 1);
            }
        }

        function controller($scope, dragulaService) {
            dragulaService.options($scope, 'template-bag', {
                moves: function (el, container, handle) {
                    return handle.className === 'dragdrop-button';
                },
                direction: 'vertical'
            });
        }
    }
})();
