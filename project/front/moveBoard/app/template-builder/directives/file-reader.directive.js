(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('fileReader', fileReader);

    fileReader.$inject = ['$window'];

    function fileReader($window) {
        if (!$window.File || !$window.FileReader) {
            throw new Error('Browser not supported');
        }

        var directive = {
            scope: {
                fileModel: '='
            },
            link: linker
        };
        return directive;

        function linker(scope, element, attrs) {
            if (attrs.type !== 'file') {
                return;
            }

            var method = attrs.method || 'readAsText';
            var fileReader = new $window.FileReader();

            fileReader.onloadend = function (evt) {
                scope.$apply(function () {
                    scope.fileModel = evt.target.result;
                });
            };

            element.on('change', function (e) {
                if (!e.target.files || !e.target.files.length || !e.target.files[0]) {
                    return true;
                }
                var files = e.target.files;
                fileReader[method](files[0]);
            });
        }
    }
})();