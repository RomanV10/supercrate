(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('erStylExecutorFrame', styleExecutor);

    styleExecutor.$inject = ['StyleExecutorService', 'EditElementService'];

    function styleExecutor(StyleExecutorService, EditElementService) {
        var directive = {
            restrict: 'E',
            scope: {
                styleContainer: '=',
                scopeTemplate: '=',
                blockChosen: '='
            },
            link: linker
        };

        return directive;

        function linker($scope, element) {
            var template = null;
            var isInitStyleContainer = true;
            var currentIndex = 0;
            var idPartsOfTemplate = StyleExecutorService.getIdPartsOfTemplate();

            $scope.$on('renderFinished', function () {
                prepareBlocks();
            });

            $scope.$on('blockTemplateChanged', function () {
                prepareBlocks();
            });

            function prepareBlocks() {
                var blocks = element.find('#block');
                template = blocks;
                var index = 0;
                angular.forEach(blocks, function (value) {
                    var block = angular.element(value);
                    block.on('click', {"index": index}, setUpStyleContainer);
                    index += 1;
                });
            }

            function setUpStyleContainer(event) {
                currentIndex = event.data.index;
                isInitStyleContainer = true;
                $scope.styleContainer = {};
                $scope.blockChosen = true;
                var block = angular.element(this);
                angular.forEach(idPartsOfTemplate, function (id) {
                    var child;
                    if (block.attr('id') == id) {
                        child = block;
                    } else {
                        var children = block.find(wrapId(id));
                        child = angular.element(children[0]);
                    }
                    isInitStyleContainer = true;
                    var childStyle = child.attr('style');
                    var childStyleObject = StyleExecutorService.parseStyleString(childStyle);
                    if (!angular.isUndefined(childStyleObject.backgroundColor) && !EditElementService.isEmptyString(childStyleObject.backgroundColor)) {
                        $scope.styleContainer[id] = {"backgroundColor": childStyleObject.backgroundColor};
                    }
                });

            }

            function wrapId(id) {
                var result = '#' + id;
                return result;
            }

            $scope.$watch('styleContainer', function (newValue, oldValue) {
                if (template != null && !isInitStyleContainer) {
                    applyStyle(newValue, oldValue);
                }
                isInitStyleContainer = false;
            }, true);

            function applyStyle(newValue, oldValue) {
                var diffStyle = findDiffStyle(newValue, oldValue);
                if ($scope.scopeTemplate == true) {
                    angular.forEach(template, function (block) {
                        var wrapBlock = angular.element(block);
                        applyStyleToBlock(diffStyle, wrapBlock);
                    });
                } else {
                    var wrapBlock = angular.element(template[currentIndex]);
                    applyStyleToBlock(diffStyle, wrapBlock);
                }
            }

            function findDiffStyle(newValue, oldValue) {
                var result = {};
                for (var keyName in newValue) {
                    if (!angular.equals(newValue[keyName], oldValue[keyName])) {
                        result[keyName] = newValue[keyName];
                        break;
                    }
                }
                return result;
            }

            function applyStyleToBlock(style, block) {
                angular.forEach(style, function (valueStyle, keyName) {
                    if (block.attr('id') == keyName) {
                        angular.forEach(valueStyle, function (styleValue, styleName) {
                            block.css(styleName, styleValue);
                        });
                    } else {
                        var children = block.find(wrapId(keyName));
                        angular.forEach(children, function (child) {
                            var wrapChild = angular.element(child);
                            angular.forEach(valueStyle, function (styleValue, styleName) {
                                wrapChild.css(styleName, styleValue);
                            });
                        });
                    }
                });
            }
        }
    }
})();

