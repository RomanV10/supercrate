(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('emailTemplatePreview', emailTemplatePreview);

    emailTemplatePreview.$inject = ['EmailBuilderRequestService', 'TemplateBuilderService'];

    function emailTemplatePreview(EmailBuilderRequestService, TemplateBuilderService) {
        var directive = {
            restrict: 'EA',
            scope: {
                template: '=',
                blocksMenu: '=',
                cancel: '&onCancel',
                save: '&onSave',
                isSendPreview: '=',
                templateOptions: '='
            },
            link: linker,
	        template: require('../templates/template-preview.html'),
        };

        return directive;

        function linker($scope, element) {
            $scope.closeButtonText = $scope.isSendPreview ? 'Close' : 'Cancel';
            $scope.saveButtonText = $scope.isSendPreview ? 'Send & Close' : 'Save';
            $scope.templatePreview = '';
            $scope.tabs = [
                    {name: 'Edit Template', url: 'app/template-builder/templates/edit-template-preview.html', selected: true},
                    {name: 'Preview Template', url: 'app/template-builder/templates/check-template-preview.html', selected: false, preview: true},
                ];
            $scope.busy = false;

            init($scope.templateOptions);
            initEvents();

            $scope.addBlockToTemplate = addBlockToTemplate;
            $scope.select = select;
            $scope.enableStyle = TemplateBuilderService.enableStyle;
            $scope.initEvents = initEvents;

            function init(templateOptions) {
                $scope.templateTitle = templateOptions && templateOptions.previewTitle ? templateOptions.previewTitle : "Template preview";
                $scope.isShowCloseButton = templateOptions && !_.isUndefined(templateOptions.isShowCloseButton) ? templateOptions.isShowCloseButton : true;
                $scope.isShowSaveButton = templateOptions && !_.isUndefined(templateOptions.isShowSaveButton) ? templateOptions.isShowSaveButton : true;
                $scope.isEditTemplateName = templateOptions && !_.isUndefined(templateOptions.isEditTemplateName) ? templateOptions.isEditTemplateName : false;
                $scope.editBlockOptions = templateOptions ? templateOptions.editBlockOptions : undefined;
                $scope.isShowPreviewTabs = templateOptions && !_.isUndefined(templateOptions.isShowPreviewTabs) ? templateOptions.isShowPreviewTabs : false;
            }

            function addBlockToTemplate(block) {
                $scope.template.blocks.push(block);
            }

            function select(tab) {
                angular.forEach($scope.tabs, function (tab) {
                    tab.selected = false;
                });

                tab.selected = true;

                if (tab.preview) {
                    loadTemplate();
                }
            }

            function loadTemplate() {
                if ($scope.templateOptions && $scope.isShowPreviewTabs) {
                    if (!$scope.templateOptions.rid) {
                        console.error('You try to load template preview without request! Please create request and check email.');
                        return false;
                    }

                    $scope.busy = true;

                    let template = angular.copy($scope.template);
                    template.template = TemplateBuilderService.getCurrentHTMLTemplate();

                    EmailBuilderRequestService
                        .getTemplatePreview($scope.templateOptions.rid, $scope.templateOptions.rtype, template)
                        .then(function (html) {
                            $scope.templatePreview = _.head(html.data);
                        }, function () {
                            toastr.error('Problem with load template', 'Error!');
                        })
                        .finally(function () {
                            $scope.busy = false;
                        });
                }
            }

            function initEvents() {
                var headerTab = angular.element(element.find('#header-tab')[0]);
                var headerContainer = angular.element(element.find('#header-container')[0]);
                var footerTab = angular.element(element.find('#footer-tab')[0]);
                var footerContainer = angular.element(element.find('#footer-container')[0]);
                var bodyTab = angular.element(element.find('#body-tab')[0]);
                var bodyContainer = angular.element(element.find('#body-container')[0]);

                headerTab
                    .hover(function () {
                            headerContainer.show();
                            footerContainer.hide();
                            bodyContainer.hide();
                        },
                        function () {
                            headerContainer.hide();
                        });

                footerTab
                    .hover(function () {
                        footerContainer.show();
                        headerContainer.hide();
                        bodyContainer.hide();
                    }, function () {
                        footerContainer.hide();
                    });

                bodyTab
                    .hover(function () {
                            bodyContainer.show();
                            footerContainer.hide();
                            headerContainer.hide();
                        },
                        function () {
                            bodyContainer.hide();
                        });

                headerContainer
                    .hover(function () {
                        headerContainer.show()
                    }, function () {
                        headerContainer.hide();
                    });

                footerContainer
                    .hover(function () {
                        footerContainer.show();
                    }, function () {
                        footerContainer.hide();
                    });

                bodyContainer
                    .hover(function () {
                        bodyContainer.show();
                    }, function () {
                        bodyContainer.hide();
                    });
            }

        }
    }
})();
