(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('blockDirective', blockDirective);

    blockDirective.$inject = ['TemplateBuilderService', 'EditElementService', '$log', '$rootScope'];
    function blockDirective(TemplateBuilderService, EditElementService, $log, $rootScope) {
        var directive = {
            restrict: 'EA',
            scope: {
                block: '=',
                deleteBlock: '&',
                deleteButton: '=',
                editorButton: '=',
                dragdropButton: '=',
                editChildElements: '=',
                logoUrl: '=companyLogoUrl',
                modalOptions: '=editBlockOptions'
            },
            link: linker,
	        template: require('../templates/block.html'),
        };
        return directive;

        function linker($scope, element) {
            $scope.isVisibleEditor = false;

            $scope.setEditorVisibility = function () {
                $scope.isVisibleEditor = !$scope.isVisibleEditor;
            };

            $scope.enableStyle = TemplateBuilderService.enableStyle;

            $scope.$on('renderFinished', function () {
                enableEditChildElements();
            });

            $scope.$on('saveBlockChanges', function () {
                saveChanges();
            });

            function saveChanges() {
                var currentBlock = element.find('#block-template');
                var wrapBlock = angular.element(currentBlock[0]);
                $scope.block.template = wrapBlock.html();
            }

            $scope.$watch('block.template', function () {
                enableEditChildElements();
                changeCompanyLogoUrl();
                $rootScope.$broadcast('blockTemplateChanged');
            });

            function enableEditChildElements() {
                if ($scope.editChildElements) {
                    EditElementService.prepareElementsForEdit(element, $scope.block, $scope.modalOptions);
                }
            }

            function changeCompanyLogoUrl() {
                if (angular.isDefined($scope.logoUrl)) {
                    var currentBlock = element.find('#block-template');
                    var wrapBlock = angular.element(currentBlock[0]);
                    var logos = wrapBlock.find("img");
                    var changed = false;
                    for (var i = 0; i < logos.length; i++) {
                        var logo = angular.element(logos[i]);
                        if (logo.prop("id") != "company-logo") {
                            continue;
                        }
                        var src = logo.attr('src').trim();

                        if (!angular.equals(src, $scope.logoUrl)) {
                            logo.attr('src', $scope.logoUrl);
                            changed = true;
                        }
                    }
                    if (changed) {
                        saveChanges();
                    }
                }
            }
        }
    }
})();
