(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('erStylEditorFrame', styleEditor);

    styleEditor.$inject = ['$rootScope'];
    function styleEditor($rootScope) {
        var directive = {
            restrict: 'E',
            scope: {
                styleContainer: '=',
                scopeTemplate: '='
            },
	        template: require('../templates/template-style-editor.html'),
            link: linker
        };

        return directive;

        function linker($scope, element) {
            $scope.switchEditorScope = function() {
                $scope.scopeTemplate = !$scope.scopeTemplate;
            };

            element.on("mouseleave", function() {
                $rootScope.$broadcast('saveBlockChanges');
            });
        }
    }
})();
