(function () {

    angular.module("app")
        .config([
                "$stateProvider",
                function ($stateProvider) {
                    $stateProvider
                        .state('settings.templatebuilder', {
                            url: '/template-builder',
	                        template: require('./templates/template-builder.html'),
                            title: 'Email template builder',
                            data: {
                                permissions: {
                                    except: ['anonymous', 'foreman', 'helper']
                                }
                            }
                        })
                }
            ]
        );
})();
