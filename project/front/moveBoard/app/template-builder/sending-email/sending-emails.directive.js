(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .directive('sendingEmails', sendingEmails);

    sendingEmails.$inject = ['SendEmailsService'];

    function sendingEmails(SendEmailsService) {
        var directive = {
            restrict: 'E',
            template: require('./sending-emails.html'),
            scope: {
                emails: '=',
                emailParameters: '=',
                sendEmailButton: '='
            },
            link: linker
        };

        return directive;

        function linker($scope, element, attr) {
            $scope.openMailDialog = openMailDialog;
            $scope.deleteEmail = deleteEmail;
            $scope.isEmptyEmails = isEmptyEmails;
            $scope.smallBusyModal = false;
            $scope.spinner = 0;
            $scope.emailsMenu = {};
            $scope.blocksMenu = {};
            $scope.busyModal = true;

            SendEmailsService.prepareForEditEmails($scope.emailParameters)
                .then(function (data) {
                    $scope.emails = data.emails;
                    $scope.emailsMenu = data.emailsMenu;
                    $scope.blocksMenu = data.blocksMenu;
                    $scope.busyModal = false;
                }, function () {
                    $scope.busyModal = false;
                });

            $scope.$watch('emailParameters', function (newValue) {
                if (angular.isDefined(newValue)
                    && angular.isDefined(newValue.keyNames)
                    && newValue.keyNames.length > 0) {

                    var emailParameters = {keyNames: newValue.keyNames};

                    SendEmailsService
                        .prepareForEditEmails(emailParameters)
                        .then(function (result) {
                            $scope.emails = result.emails;
                        });
                }
            }, true);

            function deleteEmail(index) {
                $scope.emails.splice(index, 1);
            }

            function isEmptyEmails() {
                return $scope.emails.length == 0;
            }

            function openMailDialog() {
                SendEmailsService.openMailDialog($scope);
            }
        }
    }
})();