(function () {
	'use strict';

	angular
		.module('app.tplBuilder')
		.factory('SendEmailsService', SendEmailsService);

	SendEmailsService.$inject = ['$q', '$http', 'config', 'EmailBuilderRequestService', '$rootScope', '$uibModal',
		'logger', 'SweetAlert', 'RequestServices', 'TemplateBuilderService'];

	function SendEmailsService($q, $http, config, EmailBuilderRequestService, $rootScope, $uibModal,
							   logger, SweetAlert, RequestServices, TemplateBuilderService) {
		var service = {};
		service.initTemplatesByStatus = initTemplatesByStatus;
		service.initTemplatesByKeyName = initTemplatesByKeyName;
		service.initTemplateByStatusAndServiceType = initTemplateByStatusAndServiceType;
		service.sendEmails = sendEmails;
		service.sendEmailsTo = sendEmailsTo;
		service.sendEmailsInvoice = sendEmailsInvoice;
		service.createEmailObject = createEmailObject;
		service.createEmptyEmailObject = createEmptyEmailObject;
		service.openMailDialog = openMailDialog;
		service.loadBlocks = loadBlocks;
		service.loadTemplateFolders = loadTemplateFolders;
		service.loadEmailTemplates = loadEmailTemplates;
		service.prepareForEditEmails = prepareForEditEmails;
		service.showEmailPreview = showEmailPreview;
		service.getKeyNames = getKeyNames;

		return service;

		function initTemplatesByStatus(status, emails) {
			var statusCode = parseInt(status);
			var keyNames = [];

			switch (statusCode) {
				case 1: // Pending
					break;
				case 2: // Not confirmed
					keyNames.push('status_not_confirmed');
					break;
				case 3: // Confirmed
					keyNames.push('status_confirmed');
					break;
				case 4: // Inhome Estimate
					keyNames.push('inhome_estimate');
					break;
				case 12: // Expired
					keyNames.push('status_expired');
					break;
				case 13: // We are not available
					break;
				case 14: // Spam
					break;
			}

			var result = initTemplatesByKeyName(keyNames, emails);
			return result;
		}

		function initTemplateByStatusAndServiceType(status, serviceType, emails) {
			var keyNames = getKeyNames(status, serviceType);
			var result = initTemplatesByKeyName(keyNames, emails);

			return result;
		}

		function getKeyNames(status, serviceType) {
			var statusCode = parseInt(status);
			var serviceTypeCode = parseInt(serviceType);
			var keyNames = [];

			switch (statusCode) {
				case 1: // Pending
					break;
				case 2: // Not confirmed
					if (serviceTypeCode == 7) { //long distance
						keyNames.push('status_not_confirmed_ld');
					} else if (serviceTypeCode == 5) { // flat rate
						keyNames.push('status_not_confirmed_fr');
					} else if (serviceTypeCode == 2 || serviceTypeCode == 6) {
						keyNames.push('status_not_confirmed_ms');
					} else if (serviceType == 3) {
						keyNames.push('status_not_confirmed_loading_help');
					} else if (serviceType == 4) {
						keyNames.push('status_not_confirmed_unloading_help');
					} else if (serviceType == 8) {
						keyNames.push('packing_day_not_confirmed');
					} else {
						keyNames.push('status_not_confirmed');
					}

					break;
				case 3: // Confirmed
					if (serviceTypeCode == 7) { // long distance
						keyNames.push('status_confirmed_ld');
					} else if (serviceTypeCode == 5) { // flat rate
						keyNames.push('status_confirmed_fr');
					} else if (serviceTypeCode == 2 || serviceTypeCode == 6) {
						keyNames.push('status_confirmed_ms');
					} else if (serviceType == 3) {
						keyNames.push('status_confirmed_loading_help');
					} else if (serviceType == 4) {
						keyNames.push('status_confirmed_unloading_help');
					} else if (serviceType == 8) {
						keyNames.push('packing_day_confirmed');
					} else {
						keyNames.push('status_confirmed');
					}

					break;
				case 4: // Inhome Estimate
					keyNames.push('inhome_estimate');
					break;
				case 5:
					keyNames.push('status_cancelled'); //Cancelled
					break;
				case 12: // Expired
					keyNames.push('status_expired');
					break;
				case 13: // We are not available
					keyNames.push('status_we_are_not_available');
					break;
				case 14: // Spam
					break;
			}

			return keyNames;
		}

		function initTemplatesByKeyName(keyNames, emails) {
			var result = [];

			angular.forEach(keyNames, function (keyName) {
				angular.forEach(emails, function (email) {
					if (email.key_name == keyName) {
						result.push(createEmailObject(email));
					}
				});
			});

			return result;
		}

		function sendEmails(nid, emails) {
			var defer = $q.defer();

			var isContainNewRequestNewUser = _.findIndex(emails, {key_name: 'new_request_new_user'});

			if (isContainNewRequestNewUser > 0) {
				emails.splice(0, 0, emails.splice(isContainNewRequestNewUser, 1)[0]);
			}

			$http({
				method: 'POST',
				url: config.serverUrl + 'server/template_builder/send_email_template/' + nid,
				data: {
					'data': emails,
				}
			}).then(function (data) {
				defer.resolve(data);
			}, function (reason) {
				defer.reject(reason);
			});

			return defer.promise;
		}

		function sendEmailsTo(nid, emails, to) {
			var defer = $q.defer();
			var data = {
				"to": to,
				"templates": emails
			};

			$http({
				method: 'POST',
				url: config.serverUrl + 'server/template_builder/send_email_template_to/' + nid,
				data: {
					'data': data
				}
			}).then(function (data) {
				defer.resolve(data);
			}, function (reason) {
				defer.reject(reason);
			});

			return defer.promise;
		}

		function sendEmailsInvoice(id_invoice, emails, storage_id) {
			var defer = $q.defer();
			var data = {
				"templates": emails,
				'storage_id': storage_id
			};

			$http({
				method: 'POST',
				url: config.serverUrl + 'server/template_builder/send_email_template_invoice/' + id_invoice,
				data: {
					'data': data
				}
			}).then(function (data) {
				defer.resolve(data);
			}, function (reason) {
				defer.reject(reason);
			});

			return defer.promise;
		}

		function createEmailObject(email) {
			var result = {};

			angular.copy(email, result);
			result['templateId'] = email.id;
			result['subject'] = email.data.subject;

			return result;
		}

		function createEmptyEmailObject() {
			return createEmailObject(TemplateBuilderService.initTemplate());
		}

		function openMailDialog($scope) {
			var defer = $q.defer();

			var templateOptions = {};

			if ($scope.emailParameters) {
				if ($scope.emailParameters.service_type && $scope.emailParameters.service_type == "STORAGEREQUEST") {
					templateOptions.rtype = 1;
				}

				if ($scope.emailParameters.service_type && ($scope.emailParameters.service_type == "MOVEREQUEST")) {
					templateOptions.rtype = 0;
				}

				templateOptions.rid = $scope.emailParameters.nid;
				templateOptions.isShowPreviewTabs = _.get($scope.emailParameters, 'isShowPreviewTabs', true);
				templateOptions.previewTitle = 'Edit Template';
			}

			var modalInstance = $uibModal.open({
				template: require('../sending-email/emails-modal.html'),
				controller: ShowEmailsModalCtrl,
				size: 'lg',
				backdrop: false,
				resolve: {
					emails: function () {
						return $scope.emails
					},
					emailsMenu: function () {
						return $scope.emailsMenu
					},
					blocksMenu: function () {
						return $scope.blocksMenu
					},
					isSendEmails: function () {
						return $scope.sendEmailButton
					},
					templateOptions: function () {
						return templateOptions;
					},
					emailParameters: function () {
						return $scope.emailParameters
					}
				}
			});

			modalInstance
				.result
				.then(function (emails) {
					$scope.emails = emails;

					if ($scope.sendEmailButton) {
						$scope.smallBusyModal = true;

						var afterSendingSuccess = function (data) {
							if (data.data.send) {
								defer.resolve($scope.emails);
								toastr.success("Emails were successfully sent!");
							} else {
								defer.reject();
								SweetAlert.swal("Error sending emails", "", "error");
							}

							$scope.smallBusyModal = false;
						};
						var afterSendingFail = function (reason) {
							$scope.smallBusyModal = false;
							defer.reject();
							SweetAlert.swal("Error", "", "error");
						};
						if ($scope.emailParameters.service_type == "STORAGEREQUEST") {
							sendEmailsInvoice($scope.emailParameters.nid, $scope.emails, $scope.emailParameters.nid)
								.then(afterSendingSuccess, afterSendingFail);
						} else if ($scope.emailParameters.emailReceivers.length > 0) {
							sendEmailsTo($scope.emailParameters.nid, $scope.emails, $scope.emailParameters.emailReceivers)
								.then(afterSendingSuccess, afterSendingFail);
						}
					} else {
						defer.resolve($scope.emails);
					}
				}, function () {
					defer.reject();
				});

			return defer.promise;
		}

		function ShowEmailsModalCtrl($scope, $uibModalInstance, emails, emailsMenu, blocksMenu, isSendEmails, emailParameters, templateOptions) {
			$scope.emailHeader = 'Select email for sending';
			$scope.emails = angular.copy(emails);
			$scope.emailsMenu = emailsMenu;
			$scope.isSendEmails = isSendEmails;
			$scope.emailParameters = emailParameters;
			$scope.cancel = cancel;
			$scope.save = save;
			$scope.addEmail = addEmail;
			$scope.removeEmail = removeEmail;
			$scope.sendEmailsAndClose = sendEmailsAndClose;
			$scope.showPreview = showPreview;
			$scope.checkEmails = checkEmails;
			$scope.emailChecked = {};

			var emailReceivers = [{
				mail: $scope.emailParameters.email,
				name: $scope.emailParameters.mainClientName,
				selected: true
			}];

			$scope.filteredReceivers = [];
			$scope.selectedReceivers = [];
			$scope.enabledSelector = (emailParameters.emailReceivers != undefined) && !emailParameters.hideSelectEmail;

			$scope.selectedReceivers.push({
				mail: emailReceivers[0].mail,
				name: emailReceivers[0].name
			});

			let isEnableAdditionalContacts = _.get(emailParameters, 'additionalContact.mail');

			if(isEnableAdditionalContacts) {
				$scope.selectedReceivers.push({
					mail: emailParameters.additionalContact.mail,
					name: emailParameters.additionalContact.name
				});
			}

			markSelected();

			getAllDepartment()
				.then(function (data) {
					emailReceivers = emailReceivers.concat(data);
					angular.forEach(emailReceivers, function (receiver) {
						$scope.filteredReceivers.push(receiver);
					});
				});

			$scope.addReceiver = function () {
				var waitForFreeSelected = $q.defer();
				var defer = $q.defer();

				var waitForFreeSelectedFunc = function () {
					if ($scope.currentSelected == undefined) {
						waitForFreeSelected.resolve();
					} else {
						setTimeout(waitForFreeSelectedFunc, 50);
					}
				};
				setTimeout(waitForFreeSelectedFunc, 50);
				waitForFreeSelected.promise.then(function () {
					var currLen = $scope.selectedReceivers.length;
					var waitForNewReceiver = function () {
						if (angular.element('.emailSelectorReceiver').length > currLen) {
							defer.resolve();
						} else {
							setTimeout(waitForNewReceiver, 50);
						}
					};
					$scope.selectedReceivers.push({name: "", mail: '', justMaked: true});
					setTimeout(waitForNewReceiver, 50);
					defer.promise.then(function () {
						angular.element('.emailSelectorReceiverInput:last').focus();
					});
				});
			};

			$scope.removeReceiverByIndex = function (index) {
				$scope.selectedReceivers.splice(index, 1);
			};

			var clickListener = function (e) {
				e.stopPropagation();
				if (e.target.classList.contains('R' + $scope.currentSelected) && (e.target.classList.contains('emailSelectorReceiver')
					|| e.target.classList.contains('emailSelectorReceiverInput')
					|| e.target.classList.contains('icon-people')
					|| e.target.classList.contains('emailSelectorReceiverFilterIcon')
					|| e.target.classList.contains('emailSelectorReceiverFilter')
					|| e.target.classList.contains('emailSelectorReceiverFilterItem')
					|| e.target.classList.contains('emailSelectorAdd'))) {
				} else {
					$scope.outOfSelect($scope.currentSelected);
				}
			};

			$scope.selectReceiver = function (inFilter, inSelect) {
				if (!$scope.filteredReceivers[inFilter].selected) {
					$scope.selectedReceivers[inSelect].name = $scope.filteredReceivers[inFilter].name;
					$scope.selectedReceivers[inSelect].mail = $scope.filteredReceivers[inFilter].mail;
					$scope.outOfSelect(inSelect);
				}
			};

			function checkEmails() {
				if($scope.emailChecked.main){
					if(!$scope.selectedReceivers.find((reciver) => reciver.mail === emailParameters.email)){
						$scope.selectedReceivers.push({
							mail: emailParameters.email,
							name: emailParameters.mainClientName
						});
					}
				} else {
					_.remove($scope.selectedReceivers, (reciver) => reciver.mail === emailParameters.email);
				}

				if($scope.emailChecked.additional){
					if(!$scope.selectedReceivers.find((reciver) => reciver.mail == emailParameters.additionalContact.mail)){
						$scope.selectedReceivers.push({
							mail: emailParameters.additionalContact.mail,
							name: emailParameters.additionalContact.name
						});
					}
				} else {
					_.remove($scope.selectedReceivers, (reciver) => reciver.mail === emailParameters.additionalContact.mail);
				}
			}

			function markSelected() {
				angular.forEach(emailReceivers, function (available) {
					available.selected = false;
				});

				angular.forEach(emailReceivers, function (available) {
					angular.forEach($scope.selectedReceivers, function (selected) {
						available.selected = available.mail && selected.mail && available.mail.toLowerCase().localeCompare(selected.mail.toLowerCase()) == 0;
					});
				});
			}

			function checkSelected(email) {
				var count = 0;
				angular.forEach($scope.selectedReceivers, function (receiver) {
					if (receiver.mail && receiver.mail.toLowerCase().localeCompare(email.toLowerCase()) == 0) {
						count++;
					}
				});
				return count > 1;
			}

			$scope.enterToSelect = function (inSelect) {
				$scope.currentSelected = inSelect;
				$scope.selectedReceivers[inSelect].justMaked = false;
				//$scope.selectedReceivers[inSelect].mail = undefined;
				$scope.filteredReceivers = emailReceivers.slice();
				//$scope.doFilter(inSelect);
				document.getElementsByTagName('body')[0].addEventListener('click', clickListener);
			};

			$scope.getSize = function (index) {
				var len = Math.round($scope.selectedReceivers[index].mail.length * 0.8);
				if (len == 0) {
					return 10;
				} else {
					return len;
				}
			};

			$scope.doFilter = function (index) {
				$scope.selectedReceivers[index].showFilter = true;//auto switching filter whet typing
				if ($scope.selectedReceivers[index].showFilter) {
					if ($scope.selectedReceivers[index].mail == "") {
						$scope.filteredReceivers = emailReceivers.slice();
					} else {
						$scope.filteredReceivers = [];
						angular.forEach(emailReceivers, function (person) {
							if ((person.mail.indexOf($scope.selectedReceivers[index].mail) != -1)
								|| (person.name.indexOf($scope.selectedReceivers[index].mail) != -1)) {
								$scope.filteredReceivers.push(person);
							}
						});
					}
				}
			};

			$scope.outOfSelect = function (indexx) {
				for (var index = $scope.selectedReceivers.length - 1; index >= 0; index--) {
					if (!$scope.selectedReceivers[index].justMaked) {
						document.getElementsByTagName('body')[0].removeEventListener('click', clickListener);
						angular.element('.emailSelectorReceiverInput:eq(' + index + ')').removeClass('error');
						$scope.switchFilter(index, false);
						if (($scope.selectedReceivers[index].mail < 1) || checkSelected($scope.selectedReceivers[index].mail)) {
							$scope.removeReceiverByIndex(index);
							//index++;
						} else if (validMail($scope.selectedReceivers[index].mail)) {
							//$scope.selectedReceivers[index].mail = str;
						} else {
							angular.element('.emailSelectorReceiverInput:eq(' + index + ')').addClass('error');
						}
					}
				}
				markSelected();
				$scope.currentSelected = undefined;
			};

			function validMail(mail) {
				var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,6}\.)?[a-z]{2,6}$/i;
				if (pattern.test(mail)) {
					return true;
				} else {
					return false;
				}
			}

			$scope.switchFilter = function (inSelect, toDo) {
				for (var a in $scope.selectedReceivers) {
					if (a != inSelect) {
						$scope.selectedReceivers[a].showFilter = false;
					}
				}
				if (toDo == undefined) {
					$scope.selectedReceivers[inSelect].showFilter = !$scope.selectedReceivers[inSelect].showFilter;
					if ($scope.selectedReceivers[inSelect].showFilter) {
						angular.element('.emailSelectorReceiverInput:eq(' + inSelect + ')').focus();
					}
				} else {
					$scope.selectedReceivers[inSelect].showFilter = toDo;
				}
			};

			function cancel() {
				$uibModalInstance.dismiss('cancel');
			}

			function save() {
				$uibModalInstance.close($scope.emails);
			}

			function addEmail(email) {
				if (!angular.isDefined($scope.emails)) {
					$scope.emails = [];
				}
				var newEmail = createEmailObject(email);
				$scope.emails.push(newEmail);
			}

			function removeEmail(index) {
				$scope.emails.splice(index, 1);
			}

			function sendEmailsAndClose() { // We send emails after close modal window
				$scope.emailParameters.emailReceivers = [];
				angular.forEach($scope.selectedReceivers, function (receiver) {
					$scope.emailParameters.emailReceivers.push(receiver.mail);
				});
				$uibModalInstance.close($scope.emails);
			}

			function showPreview(index) {
				var isSendEmailPreview = false;

				showEmailPreview($scope.emails[index], blocksMenu, isSendEmailPreview, templateOptions)
					.then(function (template) {
						if (_.isObject(template)) {
							$scope.emails[index] = template;
						}
					}, function () {
					});
			}
		}

		function loadBlocks() {
			var defer = $q.defer();

			if (!angular.isUndefined($rootScope.templateBuilder.emailBlocks)) {
				defer.resolve($rootScope.templateBuilder.emailBlocks);
			} else {
				EmailBuilderRequestService
					.getBaseBlocks()
					.then(function (response) {
						$rootScope.templateBuilder.emailBlocks = response.data;
						defer.resolve($rootScope.templateBuilder.emailBlocks);
					}, function (error) {
						defer.reject(error);
						logger.error('Error when load blocks!', JSON.stringify(error), 'Error');
					});
			}

			return defer.promise;
		}

		function loadTemplateFolders() {
			var defer = $q.defer();

			if (angular.isDefined($rootScope.templateBuilder.templateFolders)) {
				defer.resolve($rootScope.templateBuilder.templateFolders);
			} else {
				EmailBuilderRequestService
					.getTemplateFolders()
					.then(function (folders) {
						$rootScope.templateBuilder.templateFolders = folders;
						defer.resolve($rootScope.templateBuilder.templateFolders);
					}, function (error) {
						defer.reject(error);
					});
			}

			return defer.promise;
		}

		function loadEmailTemplates() {
			var defer = $q.defer();

			if (angular.isDefined($rootScope.templateBuilder.emailTemplates)) {
				defer.resolve($rootScope.templateBuilder.emailTemplates);
			} else {
				EmailBuilderRequestService
					.getTemplates()
					.then(function (response) {
						$rootScope.templateBuilder.emailTemplates = response.data;
						defer.resolve($rootScope.templateBuilder.emailTemplates);
					}, function (error) {
						logger.error('Error when load email templates!', JSON.stringify(error), 'Error');
						defer.reject(error);
					});
			}

			return defer.promise;
		}

		function prepareForEditEmails(emailParameters) {
			var defer = $q.defer();
			var templateFoldersPromise = loadTemplateFolders();
			var emailTemplatesPromise = loadEmailTemplates();
			var emailBlocksPromise = loadBlocks();

			$q.all([templateFoldersPromise, emailTemplatesPromise, emailBlocksPromise])
				.then(function (response) {
					var templateFolders = response[0];
					var emailsResponse = response[1];
					var emailsPromise = initEmails(emailsResponse);
					var emailsMenuPromise = TemplateBuilderService.createTemplateMenuObject(emailsResponse, templateFolders);
					var blocksMenu = TemplateBuilderService.createBlockMenuObject(response[2]);

					$q.all([emailsPromise, emailsMenuPromise])
						.then(function (response) {
							defer.resolve({
								emails: response[0],
								emailsMenu: response[1],
								blocksMenu: blocksMenu
							});
						}, function () {
							defer.reject();
						});

				}, function () {
					defer.reject();
				});

			function initEmails(emails) { // Return init emails
				var defer = $q.defer();

				var result = [];

				if (emails != null && angular.isDefined(emailParameters)) {
					if (angular.isDefined(emailParameters.keyNames)) {
						var keyNames = emailParameters.keyNames;
						result = initTemplatesByKeyName(keyNames, emails);
					} else if (angular.isDefined(emailParameters.status)) {
						var status = emailParameters.status;


						if (angular.isDefined(emailParameters.serviceType)) {
							var serviceType = emailParameters.serviceType;
							result = initTemplateByStatusAndServiceType(status, serviceType, emails);
						} else {
							result = initTemplatesByStatus(status, emails);
						}
					}

					defer.resolve(result);
				} else {
					defer.resolve(result);
				}

				return defer.promise;
			}

			return defer.promise;
		}

		function showEmailPreview(email, blocksMenu, isSendEmailPreview, templateOptions) {
			var modalInstance = $uibModal.open({
				template: require('../sending-email/edit-template-modal.html'),
				controller: ShowTemplateModalCtrl,
				size: 'lg',
				backdrop: false,
				windowClass: 'visible-overflow auto-x-overflow',
				resolve: {
					template: function () {
						return email
					},
					blocksMenu: function () {
						return blocksMenu;
					},
					isSendEmailPreview: function () {
						return isSendEmailPreview;
					},
					templateOptions: function () {
						return templateOptions;
					}
				}
			});

			return modalInstance.result;

			function ShowTemplateModalCtrl($scope, $rootScope, $uibModalInstance, template, blocksMenu, isSendEmailPreview, templateOptions) {
				$scope.template = {};
				angular.copy(template, $scope.template);
				$scope.blocksMenu = blocksMenu;
				$scope.cancel = cancel;
				$scope.save = save;
				$scope.isSendEmailPreview = isSendEmailPreview;

				$scope.templateOptions = templateOptions || {};

				$scope.templateOptions.editBlockOptions = {
					isShowVariableToolTip: true,
					isShowOnlyInvoiceVariable: false
				};

				if (isSendEmailPreview) {
					$scope.templateOptions.editBlockOptions.isShowOnlyInvoiceVariable = true;
				}


				function cancel() {
					$uibModalInstance.close('cancel');
					$rootScope.$broadcast('close.parent.modal');
				}

				function save() {
					$scope.template.template = TemplateBuilderService.getCurrentHTMLTemplate();
					$uibModalInstance.close($scope.template);
					$rootScope.$broadcast('close.parent.modal');
				}
			}
		}

		function getAllDepartment() {
			var request = {
				'role': ['Administrator', 'Foreman', 'Sales', 'Manager', 'Customer service'],
				'active': 1
			};
			var deferred = $q.defer();
			$http.post(config.serverUrl + 'server/move_users_resource/get_user_by_role', request)
				.success(function (data, status, headers, config) {
					var justMans = [];
					angular.forEach(data, function (profession) {
						angular.forEach(profession, function (person) {
							justMans.push({
								name: person.field_user_first_name +
								' ' + person.field_user_last_name,
								mail: person.mail
							});
						});
					});
					deferred.resolve(justMans);
				})
				.error(function (data, status, headers, config) {
					deferred.reject(data);
				});

			return deferred.promise;
		}
	}
})();
