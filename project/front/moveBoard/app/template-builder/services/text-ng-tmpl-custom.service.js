'use strict';

angular
	.module('app.tplBuilder')
	.config(provideFunction);

provideFunction.$inject = ['$provide'];

function provideFunction($provide){
	$provide.decorator('taOptions', options);

	options.$inject = ['taRegisterTool', '$delegate', 'taSelection'];

	function options(taRegisterTool, taOptions, taSelection){
		taRegisterTool('insertCustom', {
			display: require('../templates/text-ng-custom-template-veriable-button.html'),
			tooltiptext: "Insert custom variable",
			action: function(item) {
				if(item.value){
					this.$editor().$parent.insertTextInInput( item.value);
				}
				angular.element('.bar-btn-dropdown.dropdown').removeClass('text-angular-custom-variable-dropdown-menu');
			},
			activateSearch: function(e){
				angular.element('.dropdown-menu .search-bar').focus();
				e.stopPropagation();
			},
			open: function(){
				angular.element('.bar-btn-dropdown.dropdown.dropdown-custom-variable').addClass('text-angular-custom-variable-dropdown-menu');
			},
			options: function(){
				if(this.$editor() && this.$editor().$parent){
					return this.$editor().$parent.tooltipButtons;
				}else{
					return [];
				}
			},
			active: '[custom:]',
		});

		taOptions.toolbar[1].push('insertCustom');
		return taOptions;
	}
}
