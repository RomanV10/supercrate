describe('emailBuilderRequest.service.rest unit-test:', () => {
	let EmailBuilderRequestService,
		testHelper;
	
	beforeEach(inject((_EmailBuilderRequestService_, _testHelperService_) => {
		EmailBuilderRequestService = _EmailBuilderRequestService_;
		testHelper = _testHelperService_;
	}));
	
	it('EmailBuilderRequestService should be defined: ', () => {
		expect(EmailBuilderRequestService).toBeDefined();
	})
	
	describe('getEmailParameters method:', () => {
		let validReuest;
		beforeEach(() => {
			validReuest = testHelper.loadJsonFile('valid-request-for-get-email-params.mock');
		});
		
		it('return valid object: ', () => {
			expect(EmailBuilderRequestService.getEmailParameters(validReuest)).toEqual({
				nid:"4847",
				email:"89user_1520434884@draft.www",
				service_type:"MOVEREQUEST",
				mainClientName:"Draft User",
				additionalContact:{},
				emailReceivers:[
					"89user_1520434884@draft.www",
					undefined
				]
			});
		});
	})
});
