'use strict';

import templateBuilderButtons from '../tooltip-data/templateBuilderButtons.json';
import invoiceButtons from '../tooltip-data/invoiceButtons.json';
import tooltipButtons from '../tooltip-data/tooltipButtons.json';

angular
	.module('app.tplBuilder')
	.factory('ModalEditElementService', ModalEditElementService);

ModalEditElementService.$inject = ['$uibModal'];

function ModalEditElementService($uibModal) {
	var service = {};
	service.openModal = openModal;
	return service;

	function openModal(elementValue, typeElement, modalOptions) {
		var modalInstance = $uibModal.open({
			template: require('../templates/edit-elements-template.html'),
			controller: ModalInstanceCtrl,
			size: 'lg',
			backdrop: false,
			windowClass: 'visible-overflow',
			resolve: {
				elementValue: () => elementValue,
				typeElement: () => typeElement,
				modalOptions: () => modalOptions,
			}
		});

		return modalInstance;
	}

	//ModalController
	function ModalInstanceCtrl($scope, $uibModalInstance, elementValue, typeElement, modalOptions) {
		copyElementValue();
		$scope.typeElement = typeElement;
		$scope.cancel = cancel;
		$scope.save = save;
		$scope.delete = deleteContent;
		$scope.isImage = isImage;
		$scope.isText = isText;
		$scope.isLargeText = isLargeText;
		$scope.isLinkImage = isLinkImage;
		$scope.isLink = isLink;
		$scope.insertTextInInput = insertTextInInput;
		$scope.closeTooltip = closeTooltip;
		$scope.setTooltipTrigger = setTooltipTrigger;
		$scope.tooltipTriggers = {};
		$scope.tooltipHTML = 'app/template-builder/templates/tooltip-template.html';

		var isSetTextAngularHandlers = false;
		$scope.textAngularFocussed = false;
		$scope.isShowVariableToolTip = _.get(modalOptions, 'isShowVariableToolTip', true);
		$scope.tooltipButtons = _.union(tooltipButtons, invoiceButtons, templateBuilderButtons);

		if (!_.isEmpty(modalOptions)) {
			if (modalOptions.isShowOnlyInvoiceVariable) {
				$scope.tooltipButtons = _.union(tooltipButtons, invoiceButtons);
			}
		}

		function copyElementValue() {
			$scope.elementTextAngularValue = '';
			if (typeof elementValue === 'object') {
				$scope.elementValue = {};
				angular.copy(elementValue, $scope.elementValue);
			} else {
				$scope.elementTextAngularValue = elementValue;
				$scope.elementValue = elementValue;
			}
		}

		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		function save() {
			if (isLargeText()) {
				$uibModalInstance.close($scope.elementTextAngularValue);
			} else {
				$uibModalInstance.close($scope.elementValue);
			}
		}

		function deleteContent() {
			var result;
			if (isLinkImage() || isLink()) {
				result = {};
				angular.copy($scope.elementValue, result);

				if (isLinkImage()) {
					result.src = '';
					result.href = '';
				}

				if (isLink()) {
					result.text = '';
					result.href = '';
				}
			} else {
				result = '';
			}

			$uibModalInstance.close(result);
		}

		function isImage() {
			return typeElement.toLowerCase() == 'image';
		}

		function isText() {
			return typeElement.toLowerCase() == 'text';
		}

		function isLargeText() {
			if (!isSetTextAngularHandlers) {
				setTextAngularHandler();
			}
			return typeElement.toLowerCase() == 'large-text';
		}

		function isLinkImage() {
			return typeElement.toLowerCase() == 'link-image';
		}

		function isLink() {
			return typeElement.toLowerCase() == 'link';
		}

		function insertTextInInput(text) {
			var modalWindow = angular.element(document.querySelector("#edit-element-modal"));
			if (isLargeText()) {
				var textEditor = findChildChildElementByPartId(modalWindow, 'DIV', 'taTextElement');
				textEditor.focus();
				insertTextAtCursor(text);
			} else {
				var toolTipButton = angular.element(modalWindow.find(':focus')[0]);
				var toolTipParent = toolTipButton.closest('div.edit-field-wrapper');
				var inputElement = angular.element(toolTipParent.find('input')[0]);
				inputElement.caret(text);
				var inputVaule = inputElement.val();
				var ngModel = inputElement.attr('ng-model');
				var pointPosition = ngModel.indexOf('.');
				if (pointPosition < 0) {
					$scope.elementValue = inputVaule;
				} else {
					var parameter = ngModel.substr(pointPosition + 1);
					$scope.elementValue[parameter] = inputVaule;
				}

			}
		}

		function insertTextAtCursor(text) {
			setTimeout(function () {
				var sel, range;
				if (window.getSelection) {
					sel = window.getSelection();
					if (sel.getRangeAt && sel.rangeCount) {
						range = sel.getRangeAt(0);
						range.deleteContents();
						range.insertNode(document.createTextNode(text));
					}
				} else if (document.selection && document.selection.createRange) {
					document.selection.createRange().text = text;
				}
			}, 10);
		}

		function setTextAngularHandler() {
			isSetTextAngularHandlers = true;
			var modalWindow = angular.element(document.querySelector("#edit-element-modal"));
			var textEditor = findChildChildElementByPartId(modalWindow, 'DIV', 'taTextElement');
			SetupEditor();
		}

		function findChildChildElementByPartId(baseElement, tag, partId) {
			var elementsByTag = baseElement.find(tag);
			for (var i = 0; i < elementsByTag.length; i++) {
				var wrapElement = angular.element(elementsByTag[i]);
				var id = wrapElement.attr('id');
				var tagName = wrapElement.prop("tagName");
				if (angular.isDefined(id) && angular.equals(tag, tagName) && id.indexOf(partId) >= 0) {
					return wrapElement;
				}
			}
		}

		function closeTooltip() {
			$scope.tooltipTriggers = {};
		}

		function setTooltipTrigger(triggerName) {
			closeTooltip();
			$scope.tooltipTriggers[triggerName] = true;
		}

		function SetupEditor() {
			var modalWindow = angular.element(document.querySelector("#edit-element-modal"));
			var editable = findChildChildElementByPartId(modalWindow, 'DIV', 'taTextElement');
			var selection;
			var range;
			var wrapEditable = angular.element(editable);

			// Populates selection and range variables
			function captureSelection(e) {
				// Don't capture selection outside editable region
				var isOrContainsAnchor = false,
					isOrContainsFocus = false,
					sel = window.getSelection(),
					parentAnchor = sel.anchorNode,
					parentFocus = sel.focusNode;

				while (parentAnchor && parentAnchor != document.documentElement) {
					if (parentAnchor == editable) {
						isOrContainsAnchor = true;
					}
					parentAnchor = parentAnchor.parentNode;
				}

				while (parentFocus && parentFocus != document.documentElement) {
					if (parentFocus == editable) {
						isOrContainsFocus = true;
					}
					parentFocus = parentFocus.parentNode;
				}

				if (!isOrContainsAnchor || !isOrContainsFocus) {
					return;
				}

				selection = window.getSelection();

				// Get range (standards)
				if (selection.getRangeAt !== undefined) {
					range = selection.getRangeAt(0);

					// Get range (Safari 2)
				} else if (
					document.createRange &&
					selection.anchorNode &&
					selection.anchorOffset &&
					selection.focusNode &&
					selection.focusOffset
				) {
					range = document.createRange();
					range.setStart(selection.anchorNode, selection.anchorOffset);
					range.setEnd(selection.focusNode, selection.focusOffset);
				} else {
					// Failure here, not handled by the rest of the script.
					// Probably IE or some older browser
				}
			}

			// Recalculate selection while typing

			wrapEditable.keyup(captureSelection);
			wrapEditable.click(captureSelection);

			wrapEditable.focusout(function () {
				var cursorStart = document.createElement('span');
				var collapsed = false;

				wrapEditable.find('#cursorStart').remove();

				if (angular.isDefined(range)) {
					collapsed = !!range.collapsed;

					cursorStart.id = 'cursorStart';

					// Insert beginning cursor marker
					range.insertNode(cursorStart);
				}

				// Insert end cursor marker if any text is selected
				if (angular.isDefined(range) && !collapsed) {
					var cursorEnd = document.createElement('span');
					cursorEnd.id = 'cursorEnd';
					wrapEditable.find('#cursorEnd').remove();
					range.collapse();
					range.insertNode(cursorEnd);
				}
			});

			wrapEditable.focusin(function () {
				var cursorStart = wrapEditable.find('#cursorStart')[0];
				var cursorEnd = wrapEditable.find('#cursorEnd')[0];

				// Don't do anything if user is creating a new selection
				if (cursorStart) {
					captureSelection();
					var range = document.createRange();

					if (cursorEnd) {
						range.setStartAfter(cursorStart);
						range.setEndBefore(cursorEnd);

						// Delete cursor markers
						wrapEditable.find('#cursorStart').remove();
						wrapEditable.find('#cursorEnd').remove();

						// Select range
						selection.removeAllRanges();
						selection.addRange(range);
					} else {
						range.selectNode(cursorStart);
						wrapEditable.find('#cursorStart').remove();

						// Select range
						if (angular.isDefined(selection)) {
							selection.removeAllRanges();
							selection.addRange(range);
						}
					}
				}

				// Register selection again
				captureSelection();
			});
		}
	}
}
