'use strict';

angular.module('app.tplBuilder')
		.factory('MainBlockService', mainBlock);

mainBlock.$inject = ['$rootScope', 'EmailBuilderRequestService', 'logger', '$q'];

function mainBlock($rootScope, EmailBuilderRequestService, logger, $q){
	let services = {};

	services.getTemplatesWithCurrentBlock = getTemplatesWithCurrentBlock;
	services.changeTemplatesBlocks = changeTemplatesBlocks;
	services.updateScopeTemplates = updateScopeTemplates;
	services.updateTemplates = updateTemplates;



	function changeTemplatesBlocks(temps, block, mainBlockTrigger) {
		for(let i in temps){
			for(let k in temps[i].blocks){
				if(temps[i].blocks[k].data.mainId === block.data.mainId){
					if(block.data.isMainBlock){
						temps[i].blocks[k].template = block.template;
					}
					if(mainBlockTrigger){
						temps[i].blocks[k].data.isMainBlock = block.data.isMainBlock;
					}
				}
			}
		}
		return temps;
	}

	function getTemplatesWithCurrentBlock(block) {
		let result = [];
		let templates = $rootScope.templateBuilder.emailTemplates;
		for(let i in templates){
			let template = templates[i];
			for(let key in template.blocks){
				if( _.get(template, `blocks[${key}].data.mainId`) === block.data.mainId){
					result.push(template);
				}
			}
		}
		return result;
	}

	function updateTemplates(templates) {
		let result = [];
		updateScopeTemplates(templates);
		result = templates.map((tmpl) => {
			return updateTemplate(tmpl);
		});
		return result;
	}

	function updateScopeTemplates(templates) {
		templates.forEach(template => {
			$rootScope.templateBuilder.emailTemplates[template.id] = template;
		});
	}

	function updateTemplate(template) {
		let defer = $q.defer();
		EmailBuilderRequestService
			.updateTemplate(template)
			.then(function (response) {
				defer.resolve(response);
				logger.success('Templates were successfully updated!', response.status, 'Success');
			}, function (error) {
				defer.reject();
				logger.error('Error updating a template!', JSON.stringify(error), 'Error');
			});
		return defer.promise;
	}

	return services;
}
