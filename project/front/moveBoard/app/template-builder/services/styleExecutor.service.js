(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .factory('StyleExecutorService', styleExecutor);

    function styleExecutor() {
        var service = {};
        var idPartsOfTemplate = ['block',
            'largeButton', 'smallButton', 'button', 'linkButton',
            'titleBox', 'contentBox',
            'innerBox', 'outerBox',
            'leftBox', 'centerBox', 'rightBox'];
        service.parseStyleString = parseStyleString;
        service.getIdPartsOfTemplate = getIdPartsOfTemplate;
        return service;

        function parseStyleString(styleString) {
            var result = {};

            if (!angular.isDefined(styleString)) {
                return result;
            }

            var styles = styleString.split(';');
            var countStyles = styles.length;
            var currentStyle, styleName, styleValue;

            while (countStyles--) {
                currentStyle = styles[countStyles].split(':');
                if (currentStyle.length != 2) {
                    continue;
                }
                styleName = currentStyle[0].trim();
                styleValue = currentStyle[1].trim();
                if (styleName.length > 0 && styleValue.length > 0) {
                    result[styleName] = styleValue;
                }
            }

            result = convertInJSONStyleObject(result);
            return result;
        }

        function convertInJSONStyleObject(htmlStyleObject) {
            var result = {};

            angular.forEach(htmlStyleObject, function (value, key) {
                key = key.toLowerCase();
                switch (key) {
                    case 'background-color':
                        result['backgroundColor'] = value;
                        break;
                    default:
                        result[key] = value;
                }
            });

            if (!angular.isDefined(result['backgroundColor'])) {
                result['backgroundColor'] = '';
            }

            return result;
        }

        function getIdPartsOfTemplate() {
            return idPartsOfTemplate;
        }
    }
})();