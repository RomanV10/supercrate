(function () {
    'use strict';

    angular.module('app.tplBuilder')
        .factory('TemplateBuilderService', TemplateBuilderService);

    TemplateBuilderService.$inject = ['$sce', 'EmailBuilderRequestService', '$q', '$log', '$filter', '$rootScope'];
    function TemplateBuilderService($sce, EmailBuilderRequestService, $q, $log, $filter, $rootScope) {
        var service = {};
        var blockTypeName = ['Header', 'Content', 'Footer'];
        var styles = ['Standard', 'Metro', 'Modern'];

        // Also change documentation on http://develnk.dlinkddns.com:10083/documents/40
        var templateKeyNames = [ // When rename this array, Please find usages for edited key names
            {id: 'new_request_new_user', value: "Greeting Email with Login and Password.", type: "New Client"},

            {id: 'new_request_local', value: "Local Move", type: "New Request"},
            {id: 'new_request_ld', value: "Long Distance", type: "New Request"},
            {id: 'new_request_storage', value: "Moving and Storage", type: "New Request"},
            {id: 'new_request_flat_rate', value: "Flat Rate", type: "New Request"},
            {id: 'new_request_loading_help', value: "Loading Help", type: "New Request"},
            {id: 'new_request_unloading_help', value: "Unloading Help", type: "New Request"},
	        {id: 'packing_day_pending', value: "Packing Day", type: "New Request"},
            {id: 'no_quote_email', value: "Request without quote", type: "New Request"},

            {id: 'reminder_notconfirmed', value: "No Activity on an Unconfirmed Request.", type: 'Reminder'},
            {id: 'move_reminder_1', value: "Your Move is Tomorrow.", type: 'Reminder'},
            {id: 'move_reminder_5', value: "Your Move is in 5 days.", type: 'Reminder'},
            {id: 'reminder_finish_reservation', value: "Reminder Reservation paid/not confirmed", type: 'Reminder'},
            {id: 'storage_reminder1', value: "Storage Reminder 1", type: 'Reminder'},
            {id: 'storage_reminder2', value: "Storage Reminder 2", type: 'Reminder'},
            {id: 'storage_reminder3', value: "Storage Reminder 3", type: 'Reminder'},
            {id: 'send_option_flat_rate', value: "Send Options Flat Rate", type: 'Reminder'},
            {id: 'review_reminder_send', value: 'Move Review Reminder', type: 'Reminder'},
            {id: 'review_success_reminder', value: 'Send if Review was a Success', type: 'Reminder'},
            {id: 'review_fail_reminder', value: 'Send if Review was a Failure', type: 'Reminder'},
            {id: 'send_when_contract_submit', value: 'Send when contract was submitted', type: 'Reminder'},
            {id: 'inhome_estimate_reminder', value: 'Your In-home Estimate Job is Tomorrow', type: 'Reminder'},

            {id: 'status_not_confirmed', value: "Not Confirmed Local", type: 'Status Change'},
            {id: 'status_confirmed_loading_help', value: "Confirmed Loading Help", type: 'Status Change'},
            {id: 'status_not_confirmed_loading_help', value: "Not Confirmed Loading Help", type: 'Status Change'},
            {id: 'status_confirmed_unloading_help', value: "Confirmed Unloading Help", type: 'Status Change'},
            {id: 'status_not_confirmed_unloading_help', value: "Not Confirmed Unloading Help", type: 'Status Change'},
            {id: 'status_confirmed', value: "Confirmed Local", type: 'Status Change'},
            {id: 'status_not_confirmed_ms', value: "Not Confirmed Moving with Storage", type: 'Status Change'},
            {id: 'status_confirmed_ms', value: "Confirmed Moving with Storage", type: 'Status Change'},
            {id: 'status_not_confirmed_ld', value: "Not Confirmed Long Distance", type: 'Status Change'},
            {id: 'status_confirmed_ld', value: "Confirmed Long Distance", type: 'Status Change'},
            {id: 'status_not_confirmed_fr', value: "Not Confirmed Flat Rate", type: 'Status Change'},
            {id: 'status_confirmed_fr', value: "Confirmed Flat Rate", type: 'Status Change'},
            {id: 'status_expired', value: "Expired", type: 'Status Change'},
            {id: 'status_we_are_not_available', value: "We are not available", type: 'Status Change'},
            {id: 'status_cancelled', value: "Canceled", type: 'Status Change'},
	        {id: 'packing_day_not_confirmed', value: "Not Confirmed Packing Day", type: 'Status Change'},
	        {id: 'packing_day_confirmed', value: "Confirmed Packing Day", type: 'Status Change'},
            {id: 'inhome_estimate', value: "In-home Estimate", type: 'Status Change'},

            {id: 'restore_password', value: "Restore Password Email", type: "Misc"},
            {id: 'new_message', value: "New Message was sent", type: 'Misc'},
            {id: 'please_confirm', value: "Complete the confirmation process.", type: 'Misc'},
            {id: 'receipt_failed', value: "Receipt not created", type: 'Misc'},

            {id: 'admin_new_request', value: "Send to Admin when New Request was created", type: 'Admin'},
            {id: 'admin_confirmed_request', value: "Send to Admin when Request was confirmed", type: 'Admin'},
            {id: 'admin_assigned_work_foreman', value: 'Send to Foreman when Admin assigned work', type: 'Admin'},
            {id: 'admin_assigned_work_helper', value: 'Send to Helper when Admin assigned work', type: 'Admin'},
            {id: 'admin_assigned_work_driver', value: 'Send to Driver when Admin assigned work', type: 'Admin'},

            {id: 'send_to_user_invoice', value: 'Moving invoice template', type: 'Invoice'},
            {id: 'storage_invoice_template', value: 'Storage invoice template', type: 'Invoice'},
            {id: 'fee_invoice_template', value: 'Storage fee invoice template', type: 'Invoice'},
            {id: 'storage_statement_template', value: 'Storage statement URL', type: 'Invoice'},
            {id: 'notification_send', value: 'Notification send mail', type: 'Misc'},
            {id: 'agent_folio_invoice_template', value: 'Agent folio invoice template', type: 'Invoice'}
        ];

        service.enableStyle = enableStyle;
        service.createTemplateMenuObject = createTemplateMenuObject;
        service.createBlockMenuObject = createBlockMenuObject;
        service.getBlockTypeName = getBlockTypeName;
        service.getStyle = getStyle;
        service.getStyles = getStyles;
        service.getCurrentHTMLTemplate = getCurrentHTMLTemplate;
        service.getTemplateKeyNames = getTemplateKeyNames;
        service.initTemplate = initTemplate;
        service.initBlock = initBlock;
        service.initTemplateBuilder = initTemplateBuilder;
        service.updateGlobalEmailTemplates = updateGlobalEmailTemplates;

        return service;

        function enableStyle(htmlContent) {
            return $sce.trustAsHtml(htmlContent);
        }

        function createBlockMenuObject(data) {
            var result = {};

            for (var i = 0; i < blockTypeName.length; i++) {
                result[i] = {
                    'header': blockTypeName[i],
                    'blocks': []
                };
            }

            angular.forEach(data, function (value, key) {
                var type = value.block_type;
                var data = value.data;

                if (isUndefinedBlockType(type)) {
                    type = 0;
                    value.block_type = 0;
                }

                if (typeof data == 'string') {
                    if (data.indexOf('{') > -1 && data.indexOf('}') > -1) {
                        value.data = JSON.parse(data);
                    }
                }

                if (data == null || data == 'null') {
                    value.data = {
                        "style": 0
                    };
                }
                result[type].blocks.push(value);
            });

            return result;
        }

        function isUndefinedBlockType(type) {
            return !angular.isDefined(type) || type > blockTypeName.length - 1;
        }

        function createTemplateMenuObject(data, templateFolders) {
            var defer = $q.defer();

            if (angular.isDefined($rootScope.templateBuilder.orderObject)) {
                createMenuObject(data, templateFolders, $rootScope.templateBuilder.orderObject)
                    .then(function (result) {
                        defer.resolve(result);
                    }, function () {
                        defer.reject();
                    });
            } else {
                EmailBuilderRequestService
                    .getOrderTemplateSettings()
                    .then(function (orderTemplate) {
                        $rootScope.templateBuilder.orderObject = orderTemplate;

                        createMenuObject(data, templateFolders, orderTemplate)
                            .then(function (result) {
                                defer.resolve(result);
                            }, function () {
                                defer.reject();
                            });
                    }, function () {
                        $log.error('Error when load order template settings');
                        defer.reject();
                    });
            }


            return defer.promise;
        }

        function createMenuObject(data, templateFolders, orderTemplate) {
            var defer = $q.defer();
            var order = orderTemplate;
            var isUpdateOrder = false;
            var result = {};
            var folders = templateFolders.folders;

            angular.forEach(folders, function (folder) {
                result[folder.index] = {
                    'header': folder.name,
                    'templates': []
                };
            });

            var templateArray = [];

            angular.forEach(data, function (value, key) {
                if (value.data != null) {
                    if (angular.isUndefined(order[value.id])) {
                        // Set default order id for new template
                        // or init orderId
                        order[value.id] = value.id;
                        isUpdateOrder = true;
                    }
                    value["orderId"] = parseInt(order[value.id]);
                    templateArray.push(value);
                }
            });

            var orderBy = $filter('orderBy');
            var templateSortedByOrderId = orderBy(templateArray, 'orderId');

            angular.forEach(templateSortedByOrderId, function (value, key) {
                if (value.data != null) {
                    var indexFolder = value.data.templateFolder;
                    if (isUndefinedTemplateFolder(indexFolder, folders)) {
                        var defaultFolder = findFolderByField(folders, 'name', 'Default');
                        value.data['templateFolder'] = defaultFolder.index;
                        indexFolder = defaultFolder.index;
                    }
                    if (!angular.isDefined(value.data['subject'])) {
                        value.data['subject'] = value.data.description;
                    }
                    result[indexFolder].templates.push(value);
                }
            });

            if (isUpdateOrder) {
                EmailBuilderRequestService
                    .saveOrderTemplateSettings(order)
                    .then(function () {
                            defer.resolve(result);
                        },
                        function () {
                            $log.error('Error when save order template settings');
                            defer.reject();
                        });
            } else {
                defer.resolve(result);
            }

            return defer.promise;
        }

        function isUndefinedTemplateFolder(index, folders) {
            return !angular.isDefined(index) || angular.equals(findFolderByField(folders, 'index', index), null);
        }

        function findFolderByField(folders, field, value) {
            var result = null;

            if (angular.isDefined(folders)) {
                for (var i = 0; i < folders.length; i++) {
                    var folder = folders[i];
                    if (folder[field] == value) {
                        result = folder;
                        break;
                    }
                }
            }

            return result;
        }

        function getBlockTypeName(index) {
            return blockTypeName[index];
        }

        function getStyle(index) {
            return styles[index];
        }

        function getStyles() {
            return styles;
        }

        function getCurrentHTMLTemplate(additionalSelector) {
            var currentTemplate = getCurrentTemplate(additionalSelector);
            var templateCopy = removeNotTemplateElements(currentTemplate.clone());
            removeAttrFromTemplate(templateCopy, 'id');
            removeAttrFromTemplate(templateCopy, 'class');
            removeAttrFromTemplate(templateCopy, 'data-type');
            return templateCopy[0].outerHTML;
        }

        function getCurrentTemplate(additionalSelector) {
            var selector = '#template-container';

            if (!_.isUndefined(additionalSelector)) {
                selector = additionalSelector + " " + selector;
            }

            return angular.element(selector).find('#template');
        }

        function removeNotTemplateElements(template) {
            var result = template;
            result.find('#edit-button').remove();
            result.find('#delete-button').remove();
            result.find('#dragdrop-button').remove();
            result.find('#block-editor').remove();
            return result;
        }

        function removeAttrFromTemplate(template, attribute) {
            if (template.attr(attribute) != undefined) {
                template.removeAttr(attribute);
            }

            var children = template.children();

            if (children.length != 0) {
                angular.forEach(children, function (child) {
                    var wrapChild = angular.element(child);
                    removeAttrFromTemplate(wrapChild, attribute);
                });
            }
        }

        function getTemplateKeyNames() {
            return templateKeyNames;
        }

        //Now template_type initialize on front-end by field templateType
        function initTemplate() {
            return {
                'id': null,
                'template_type': 0,
                'template': '',
                'name': '',
                'blocks': [],
                'data': {
                    'description': '',
                    'subject': '',
                    'templateType': 0,
                    'templateFolder': '0'
                },
                'key_name': ''
            }
        }

        // Some explanation initTemplate
        //{
        //    'id': null,
        //    'template_type': 0,  not used anywhere
        //    'template': '',
        //    'name': '',
        //    'blocks': [],
        //    'data': {
        //        'description': '',
        //        'subject': '',
        //        'templateType': 0,    not used anywhere
        //        'templateFolder': '0'
        //    },
        //    'key_name': ''
        //}

        function initBlock() {
            return {
                'id': null,
                'block_type': 0,
                'template_type': 0,
                'template': '',
                'data': {
                    'style': 0,
                    'templateType': 0
                }
            };
        }

        // Some explanation initBlock
        //{
        //    'id': null,
        //    'block_type': 0,
        //    'template_type': 0,   not used anywhere
        //    'template': '',
        //    'data': {
        //        'style': 0,
        //        'templateType': 0 not used anywhere
        //    }
        //};

        function initTemplateBuilder() {
            var templateFolders = EmailBuilderRequestService.getTemplateFolders();
            var templates = EmailBuilderRequestService.getTemplates();
            var baseBlocks = EmailBuilderRequestService.getBaseBlocks();
            var orderTemplateObjectPromise = EmailBuilderRequestService.getOrderTemplateSettings();

            $q.all([templateFolders, templates, baseBlocks, orderTemplateObjectPromise])
                .then(function (result) {
                    // template folders
                    $rootScope.templateBuilder.templateFolders = result[0];
                    // templates
                    $rootScope.templateBuilder.emailTemplates = result[1].data;
                    // blocks
                    $rootScope.templateBuilder.emailBlocks = result[2].data;
                    // order object
                    $rootScope.templateBuilder.orderObject = result[3];
                });
        }

        function updateGlobalEmailTemplates(template) {
            if (_.isObject(template)
                && !_.isNull(template.id)
                && !_.isUndefined(template.id)) {

                $rootScope.templateBuilder.emailTemplates[template.id] = angular.copy(template);
            }
        }
    }
})();
