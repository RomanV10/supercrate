(function () {
    'use strict';

    angular
        .module('app.tplBuilder')
        .factory('tplBuilderAuthService', AuthService);

    AuthService.$inject = ['$rootScope'];

    function AuthService($rootScope) {
        var service = {};
        service.isAdmin = isAdmin;
        return service;

        function isAdmin() {
            var user_name = $rootScope.currentUser.userId.name;

            return user_name == 'roma4ke';
        }
    }
})();