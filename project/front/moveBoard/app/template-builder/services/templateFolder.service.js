(function () {
    'use strict';

    angular
        .module('app.tplBuilder')
        .factory('templateFolderService', TemplateFolders);

    function TemplateFolders() {
        var service = {};
        var NAME_DEFAULT_FOLDER = "Default";

        service.initTemplateFolders = initTemplateFolders;
        service.isDefaultFolder = isDefaultFolder;

        return service;

        function initTemplateFolders(templateFolder) {
            if(angular.isUndefined(templateFolder.lastIndex)) {
                templateFolder = {"lastIndex": 0, "folders":[]};
            }
            templateFolder.folders = removeNullValueIfExists(templateFolder.folders);
            if (indexOfFolderByName(templateFolder.folders, NAME_DEFAULT_FOLDER) < 0) {
                templateFolder.folders.push({"index": templateFolder.lastIndex.toString(), "name": NAME_DEFAULT_FOLDER});
                templateFolder.lastIndex += 1;
            }
            templateFolder.folders = convertIndexToString(templateFolder.folders);
            return templateFolder;
        }

        function removeNullValueIfExists(folders) {
            var index = folders.indexOf(null);
            if (index >= 0) {
                folders.splice(index, 1);
                return removeNullValueIfExists(folders);
            } else {
                return folders;
            }
        }

        function indexOfFolderByName(folders, name) {
            var result = -1;
            if (folders.length == 0) {
                return result;
            }
            for (var i = 0; i < folders.length; i++) {
                if (folders[i].name == name) {
                    return i;
                }
            }
            return result;
        }

        function convertIndexToString(folders) {
            var result = folders;
            angular.forEach(result, function (folder) {
                folder.index = folder.index.toString();
            });
            return result;
        }

        function isDefaultFolder(folder) {
            return folder.name == NAME_DEFAULT_FOLDER;
        }
    }
})();