(function () {
    'use strict';

    angular
        .module('app.tplBuilder')
        .factory('EditElementService', EditElementService);

    /* @ngInject */
    function EditElementService(ModalEditElementService, $rootScope, SweetAlert) {
        var service = {
	        isEmptyHTMLString,
	        isEmptyString,
	        prepareElementsForEdit,
        };

        return service;

        function isEmptyHTMLString(text) {
	        text = text || '';

	        return text.replace(/\s|&nbsp;/g, '')
			        .replace(/<(?:.|\n)*?>/gm, '').length == 0;
        }

        function isEmptyString(text) {
            return text.replace(/\s|&nbsp;/g, '').length == 0;
        }

        function prepareElementsForEdit(element, block, modalOptions = {}) {
            var TAG_IMAGE = 'img';
            var TAG_LINK = 'a';
            var TAGS_TEXT = ['p', 'span', 'div'];
            var TAGS_CONTENT_NAME = ['p', 'span', 'div', 'td', 'tr', 'tbody', 'table'];

            // data-type elements
            var TYPE_IMAGE = 'image';
            var TYPE_LINK_IMAGE = 'link-image';
            var TYPE_TEXT = 'text';
            var TYPE_LINK = 'link';
            var TYPE_LARGE_TEXT = 'large-text';
            var PARENT_CLASS_NAME = 'row-edit';

            var editRows = element.find('.row-edit');
            for (var i = 0; i < editRows.length; i++) {
                var editRow = wrapElement(editRows[i]);
                editRow.on('mouseleave', onMouseLeaveElement);
                if (angular.equals(getDataTypeAttribute(editRow), TYPE_LINK_IMAGE)) {
                    editRow.on('mouseenter', onMouseOverLinkWithImage);
                    continue;
                }
                if (angular.equals(getDataTypeAttribute(editRow), TYPE_IMAGE)) {
                    editRow.on('mouseenter', onMouseOverImage);
                    continue;
                }
                if (angular.equals(getDataTypeAttribute(editRow), TYPE_TEXT)) {
                    editRow.on('mouseenter', onMouseOverText);
                    continue;
                }
                if (angular.equals(getDataTypeAttribute(editRow), TYPE_LARGE_TEXT)) {
                    editRow.on('mouseenter', onMouseOverLargeText);
                    continue;
                }
                if (angular.equals(getDataTypeAttribute(editRow), TYPE_LINK)) {
                    editRow.on('mouseenter', onMouseOverLink);
                }
            }

            function wrapElement(element) {
                return angular.element(element);
            }

            function onMouseLeaveElement() {
                var element = wrapElement(this);
                removeEditHoverBlock(element);
            }

            function removeEditHoverBlock(element) {
                element.find('.row-edit-hover').remove();
            }

            function getDataTypeAttribute(element) {
                return element.attr('data-type');
            }

            function onMouseOverLinkWithImage() {
                var linkWrapper = wrapElement(this);
                appendRowEditBlock(linkWrapper);
                bindOnClickToButton(linkWrapper, onEditLinkWithImage);
            }

            function onEditLinkWithImage() {
                var parent = getParentWrapper(wrapElement(this));
                var link = findChildElement(parent, TAG_LINK);
                var href = link.attr('href').trim();
                var image = findChildElement(link, TAG_IMAGE);
                var src = image.attr('src').trim();
                var linkImageObject = {'href': href, 'src': src};
                ModalEditElementService.openModal(linkImageObject, TYPE_LINK_IMAGE, modalOptions)
                    .result
                    .then(function (value) {
                        if (isEmptyString(value.href) && isEmptyString(value.src)) {
                            parent.remove();
                            removeEmptyChildIfPresent();
                        }

                        if (isEmptyString(value.href) && !isEmptyString(value.src)) {
                            image.attr('src', value.src);
                            parent.append(image);
                            parent.attr('data-type', TYPE_IMAGE);
                            link.remove();
                        }

                        if (!isEmptyString(value.href) && isEmptyString(value.src)) {
                            link.attr('href', value.href);
                            link.text('Content link');
                            parent.append(link);
                            parent.attr('data-type', TYPE_LINK);
                            image.remove();
                        }

                        if (!isEmptyString(value.href) && !isEmptyString(value.src)) {
                            link.attr('href', value.href);
                            image.attr('src', value.src);
                        }

                        sendSaveBlockSignal();
                    }, function () {
                    });
            }

            function sendSaveBlockSignal() {
                $rootScope.$broadcast('saveBlockChanges');
            }

            function onMouseOverImage() {
                var imageWrapper = wrapElement(this);
                appendRowEditBlock(imageWrapper);
                bindOnClickToButton(imageWrapper, onEditImage);
            }

            function appendRowEditBlock(element) {
                removeEditHoverBlock(element);
                element.append('<div class="row-edit-hover">'
                    + '<i class="fa fa-pencil" style="line-height: 30px"></i>'
                    + '</div>');
            }

            function bindOnClickToButton(wrapper, func) {
                var buttons = wrapper.find('.fa-pencil');
                for (var i = 0; i < buttons.length; i++) {
                    var button = wrapElement(buttons[i]);
                    button.on('click', func);
                }
            }

            function onEditImage() {
                var parent = getParentWrapper(wrapElement(this));
                var image = findChildElement(parent, TAG_IMAGE);
                var src = image.attr('src').trim();
                openModalWindow(src, TYPE_IMAGE, image, saveImage);
            }

            function getParentWrapper(child) {
                var result = child;
                if (result.hasClass(PARENT_CLASS_NAME)) {
                    return result;
                } else {
                    return getParentWrapper(result.parent());
                }

            }

            function findChildElement(parent, tagName) {
                var children = parent.children();
                for (var i = 0; i < children.length; i++) {
                    var child = wrapElement(children[i]);
                    if (angular.equals(child.prop("tagName").toLowerCase(), tagName)) {
                        return child;
                    }
                    if (isPresentChildrenElements(child)) {
                        return findChildElement(child, tagName);
                    }
                }
                return null;
            }

            function saveImage(image, value) {
                if (isEmptyString(value)) {
                    removeParentWrapper(image);
                } else {
                    image.attr('src', value);
                }
            }

            function removeParentWrapper(element) {
                var parent = getParentWrapper(element);
                parent.remove();
                removeEmptyChildIfPresent();
            }

            function isPresentChildrenElements(child) {
                return child.children().length > 0;
            }

            function onMouseOverText() {
                var textWrapper = wrapElement(this);
                appendRowEditBlock(textWrapper);
                bindOnClickToButton(textWrapper, onEditText);
            }

            function onEditText() {
                var parent = getParentWrapper(wrapElement(this));
                var textElement = findChildTextElement(parent);
                var textValue = textElement.text().trim();
                openModalWindow(textValue, TYPE_TEXT, textElement, saveTextElement);
            }

            function findChildTextElement(parent) {
                var result;
                for (var i = 0; i < TAGS_TEXT.length; i++) {
                    result = findChildElement(parent, TAGS_TEXT[i]);
                    if (result != null && result.attr('class') != 'row-edit-hover') {
                        return result;
                    }
                }
                return result;
            }

            function saveTextElement(element, value) {
                if (isEmptyString(value)) {
                    removeParentWrapper(element);
                } else {
                    element.text(value);
                }
            }

            function onMouseOverLargeText() {
                var textWrapper = wrapElement(this);
                appendRowEditBlock(textWrapper);
                bindOnClickToButton(textWrapper, onEditLargeText);
            }

            function onEditLargeText() {
                var textElement = getParentWrapper(wrapElement(this));
                removeEditHoverBlock(textElement);
                var textValue = textElement.html();
                openModalWindow(textValue, TYPE_LARGE_TEXT, textElement, saveLargeTextElement);
            }

            function saveLargeTextElement(element, value) {
                if (isEmptyString(value)) {
                    removeParentWrapper(element);
                } else {
                    element.html(value);
                }
            }

            function onMouseOverLink() {
                var linkWrapper = wrapElement(this);
                appendRowEditBlock(linkWrapper);
                bindOnClickToButton(linkWrapper, onEditLink);
            }

            function onEditLink() {
                var parent = getParentWrapper(wrapElement(this));
                var currentLink = findChildElement(parent, TAG_LINK);
                var text = currentLink.text().trim();
                var href = currentLink.attr('href').trim();
                var linkObject = {'text': text, 'href': href};
                openModalWindow(linkObject, TYPE_LINK, currentLink, saveLink)
            }

            function saveLink(link, value) {
                if (isEmptyString(value.text)) {
                    removeParentWrapper(link);
                } else {
                    link.text(value.text);
                    link.attr('href', value.href);
                }
            }

            function openModalWindow(value, type, element, saveFunction) {
            	if (modalOptions.isTemplateBuilderTab && block.data.isMainBlock) {
            		SweetAlert.swal("You can't edit the copy of the Main Block", "", "warning");

				} else {
					ModalEditElementService.openModal(value, type, modalOptions)
						.result
						.then(function (value) {
							saveFunction(element, value);
							sendSaveBlockSignal();
						}, function () {
						});
				}
            }

            function removeEmptyChildIfPresent() {
                var elementWasRemoved = false;
                angular.forEach(TAGS_CONTENT_NAME, function (tagName) {
                    var tags = element.find(tagName);
                    angular.forEach(tags, function (tag) {
                        var wrapTag = wrapElement(tag);
                        if (isEmptyString(wrapTag.html())
                                && !wrapTag.hasClass('dragdrop-button')
                                && !wrapTag.hasClass('delete-button')
                                && !wrapTag.hasClass('code-button')
                                && !wrapTag.hasClass('save-code-button')) {
                            wrapTag.remove();
                            elementWasRemoved = true;
                        }
                    });
                });
                if (elementWasRemoved) {
                    removeEmptyChildIfPresent();
                }
            }
        }
    }
})();