(function () {

    angular
        .module('app.tplBuilder')
        .factory('EmailBuilderRequestService', EmailBuilderRequestService);

    EmailBuilderRequestService.$inject = ['$http', '$q', 'config', 'SettingServices', 'templateFolderService', 'apiService', 'moveBoardApi', 'Raven'];

    function EmailBuilderRequestService($http, $q, config, SettingServices, templateFolderService, apiService, moveBoardApi, Raven) {
        var service = {};
        var TEMPLATE_SETTINGS_TYPE = 'templateBuilderFolders';
        var ORDER_TEMPLATE_SETTINGS_TYPE = 'orderTemplatesBuilder';
        service.createBaseBlock = createBaseBlock;
        service.updateBaseBlock = updateBaseBlock;
        service.deleteBaseBlock = deleteBaseBlock;
        service.getBaseBlocks = getBaseBlocks;
        service.createTemplate = createTemplate;
        service.updateTemplate = updateTemplate;
        service.deleteTemplate = deleteTemplate;
        service.getTemplates = getTemplates;
        service.getTemplate = getTemplate;
        service.saveTemplateFolders = saveTemplateFolders;
        service.getTemplateFolders = getTemplateFolders;
        service.saveOrderTemplateSettings = saveOrderTemplateSettings;
        service.getOrderTemplateSettings = getOrderTemplateSettings;
        service.getTemplatePreview = getTemplatePreview;
        service.getEmailParameters = getEmailParameters;

        return service;

        function createBaseBlock(block) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/template_builder/create_block_base',
                data: {
                    'template_type': block.template_type,
                    'block_type': block.block_type,
                    'value': block.template,
                    'data': block.data
                }
            }).then(function (data) {
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function updateBaseBlock(block) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/template_builder/update_blocks_base/' + block.id,
                data: {
                    'template_type': block.template_type,
                    'block_type': block.block_type,
                    'value': block.template,
                    'data': block.data
                }
            }).then(function (data) {
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function deleteBaseBlock(block) {
            var defer = $q.defer();
            var id = block.id;
            if (typeof id == 'string') {
                id = parseInt(id);
            }
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/template_builder/delete_base_block',
                data: {
                    'id': id
                }
            }).then(function (data) {
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function getBaseBlocks() {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/template_builder/retrieve_blocks_base'
            }).then(function (data) {
                parseData(data);
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function createTemplate(template) {
            var defer = $q.defer();
            $http({
                method: 'POST',
                url: config.serverUrl + 'server/template_builder',
                data: {
                    'template_type': template.template_type,
                    'value': template.template,
                    'name': template.name,
                    'blocks': template.blocks,
                    'key_name': template.key_name,
                    'data': template.data
                }
            }).then(function (data) {
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function updateTemplate(template) {
            var defer = $q.defer();
            $http({
                method: 'PUT',
                url: config.serverUrl + 'server/template_builder/' + template.id,
                data: {
                    'template_type': template.template_type,
                    'value': template.template,
                    'name': template.name,
                    'blocks': template.blocks,
                    'key_name': template.key_name,
                    'data': template.data
                }
            }).then(function (data) {
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function deleteTemplate(idTemplate) {
            var defer = $q.defer();
            $http({
                method: 'DELETE',
                url: config.serverUrl + 'server/template_builder/' + idTemplate
            }).then(function (data) {
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function getTemplates() {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: config.serverUrl + 'server/template_builder'
            }).then(function (data) {
                parseData(data);
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function parseData(data) {
            var result = data;

            if (result.data[0] == false) {
                result.data = {};
            }

            return result;
        }

        function getTemplate(idTemplate) {
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: config.serverUrl + 'server/template_builder/' + idTemplate
            }).then(function (data) {
                parseData(data);
                defer.resolve(data);
            }, function (reason) {
                defer.reject(reason);
            });

            return defer.promise;
        }

        function saveTemplateFolders(folders) {
            return SettingServices.saveSettings(folders, TEMPLATE_SETTINGS_TYPE);
        }

        function getTemplateFolders() {
            var defer = $q.defer();
            SettingServices
                .getSettings(TEMPLATE_SETTINGS_TYPE)
                .then(function (folders) {
                    var result = parseSettingsData(folders);
                    result = templateFolderService.initTemplateFolders(result);
                    defer.resolve(result);
                }, function (error) {
                    defer.reject(error);
                });
            return defer.promise;
        }

        function parseSettingsData(data) {
            var result = {};
            if (isTextSettings(data)) {
                result = JSON.parse(data[0]);
            }
            if (result == null || data == null) {
                result = {};
            }
            return result;
        }

        function isTextSettings(settings) {
            return settings != null && typeof settings[0] == 'string'
        }

        function saveOrderTemplateSettings(order) {
            return SettingServices.saveSettings(order, ORDER_TEMPLATE_SETTINGS_TYPE);
        }

        function getOrderTemplateSettings() {
            var defer = $q.defer();
            SettingServices
                .getSettings(ORDER_TEMPLATE_SETTINGS_TYPE)
                .then(function (orders) {
                    var result = parseSettingsData(orders);
                    defer.resolve(result);
                }, function (error) {
                    defer.reject(error);
                });
            return defer.promise;
        }

        function getTemplatePreview(rid, type, template) {
            var data = {
                "request_id": rid,
                "request_type": type,
                "template": template
            };

            return apiService.postData(moveBoardApi.templateBuilder.getTemplatePreview, data);
        }
	
		function getEmailParameters(request = {}) {
			if (_.isEmpty(request)) {
				Raven.captureException('getEmailParameters empty request');
			}
			
			let additionalContact = {};
		
			Object.defineProperty(additionalContact, "name", {
				get: () => `${_.get(request, 'field_additional_user.first_name')} ${_.get(request, 'field_additional_user.last_name')}`
			});
		
			Object.defineProperty(additionalContact, "mail", {
				get: () => _.get(request, 'field_additional_user.mail')
			});
		
			let emailParameters = {
				"nid": request.nid,
				email: request.email,
				service_type: 'MOVEREQUEST',
				mainClientName: request.name,
				additionalContact,
				emailReceivers: [
					request.email,
					additionalContact.mail
				]
			};
		
			return emailParameters;
		}
    }

})();
