(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('messages', {
                        url: '/messages',
	                    template: require('./templates/messages-page.html'),
                        title: 'Messages',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
            }
        ]
    );
})();
