import MOMENT_TIMEZONE from 'moment-timezone';

angular
	.module('app.messages')
	.factory('MessagesServices', MessagesServices);

MessagesServices.$inject = ['$q', 'apiService', 'moveBoardApi'];

function MessagesServices($q, apiService, moveBoardApi) {
	const CURRENT_TIME_ZONE = MOMENT_TIMEZONE.tz.guess();

	return {
		getMessagesThread,
		addMessage,
		getUnreadCount,
		getUnreadComments,
		setCommentReaded,
		getAllMessages,
		updateMessage,
		sendSms,
	};

	function setCommentReaded(nid) {
		let defer = $q.defer();
		apiService.postData(`${moveBoardApi.comments.setRead}${nid}`)
			.then(resolve => {
				defer.resolve(resolve.data);
			})
			.catch(error => {
				defer.reject(error);
			});

		return defer.promise;
	}

	function getMessagesThread(nid) {
		let defer = $q.defer();

		apiService.getData(`${moveBoardApi.comments.moveRequestComments}/${nid}`)
			.then(resolve => {
				defer.resolve(resolve.data);
			})
			.catch(error => {
				defer.reject(error);
			});

		return defer.promise;
	}


	function addMessage(nid, message, format) {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.comments.moveRequestComments, {
			data: {
				message,
				nid,
				format,
				time_zone: CURRENT_TIME_ZONE,
			}
		})
			.then(resolve => {
				defer.resolve(resolve.data);
			})
			.catch(error => {
				defer.reject(error);
			});
		return defer.promise;
	}

	function sendSms(sms) {
		return apiService.postData(moveBoardApi.smsSettings.sendSMS, sms)
			.then(({data}) => data);
	}

	function updateMessage(nid, message, format) {
		let defer = $q.defer();

		apiService.putData(`${moveBoardApi.comments.moveRequestComments}/${nid}`, {
			data: {
				message,
				format
			}
		})
			.then(resolve => {
				defer.resolve(resolve.data);
			})
			.catch(error => {
				defer.reject(error);
			});

		return defer.promise;
	}

	function getUnreadCount() {
		let deferred = $q.defer();

		apiService.postData(moveBoardApi.comments.unReadComments)
			.then(resolve => {
				deferred.resolve(resolve.data);
			})
			.catch(error => {
				deferred.reject(error);
			});

		return deferred.promise;
	}

	function getUnreadComments() {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.comments.getNewComments)
			.then(resolve => {
				defer.resolve(resolve.data);
			})
			.catch(error => {
				defer.reject(error);
			});

		return defer.promise;
	}

	function getAllMessages() {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.comments.adminAllComments)
			.then(resolve => {
				defer.resolve(resolve.data);
			})
			.catch(error => {
				defer.reject(error);
			});

		return defer.promise;
	}
}
