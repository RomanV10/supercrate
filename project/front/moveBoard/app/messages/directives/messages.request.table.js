(function () {
    'use strict';

    angular
        .module('app.messages')
        .directive('ccMessagesDatatable', ccMessagesDatatable);

    /* @ngInject */
    function ccMessagesDatatable(common, DTOptionsBuilder, MessagesServices, $rootScope, openRequestService) {
        var directive = {
            scope: {
                'requests': '=requests',
				'request': '=request',
                'active': '=active',
            },
            controller: requestsController,
            replace: true,
            compile: compile,
	        template: require('../templates/messagesTable.html'),
            restrict: 'A'
        };

        return directive;

        function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) {
                    scope.dtinstance = {};

                    scope.dtOptions = DTOptionsBuilder.newOptions()
                        .withPaginationType('simple')
                        .withDisplayLength(100)
                        .withOption('aaSorting', [0, 'desc']);
                }
            }
        }

        /* @ngInject */
        function requestsController($scope) {
            $scope.nid = 0;
            $scope.$watch('requests', function () {
                //$scope.requests = common.objToArray($scope.requests);
                if (Object.keys($scope.requests).length) {
                    //$scope.nid = _.last($scope.requests).nid;
                    $scope.nid = common.objToArray($scope.requests)[0].nid;
                    $scope.active = $scope.nid;
                }
                //   $scope.showComments(request);
            });

            $scope.status = 0;
            $scope.busy = false;
            //HEADER DATEPICKER SETTINGS

            $scope.requestEditModal = requestEditModal;

            function requestEditModal(request) {
                $scope.busy = true;

	            openRequestService.open(request.nid)
                    .finally(() => {
	                    $scope.busy = false;
                    });
            }

            $scope.showComments = function (request) {
            	$scope.request = request;
                $scope.nid = request.nid;
                $scope.active = request.nid;
            };

            $scope.markAllMessagesAsRead = function () {
                for (var index in $scope.requests) {
                    var nid = $scope.requests[index].nid;
                    $rootScope.$broadcast('read_all_request_messages', {nid: nid});

                    var promise = MessagesServices.getMessagesThread(nid);
                    promise.then(function (messages) {
                        for (var id in messages) {
                            var msg = messages[id];
                            if (!msg.read) {
                                MessagesServices.setCommentReaded(msg.cid);
                                msg.read = 1;
                            }
                        }
                    })
                }
            };
        }
    }
})();
