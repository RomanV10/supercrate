import './sms-message-header.styl';

angular
	.module('app.messages')
	.directive('smsMessageHeader', smsMessageHeader);

/* @ngInject */
function smsMessageHeader() {
	return {
		restrict: 'E',
		template: require('./sms-message-header.html'),
	};
}
