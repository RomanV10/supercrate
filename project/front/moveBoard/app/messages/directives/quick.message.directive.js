(function () {
    'use strict';

    angular
        .module('app.messages')
        .directive('quickMessage', quickMessage);

    function quickMessage () {
        var directive = {
            link: link,
            scope: {
                'nid': '=nid',
            },
            template: require('../templates/quickMessage.template.html'),
            restrict: 'A'
        };
        return directive;


        function link(scope) {

            scope.active = scope.nid;
        }

    }
})();
