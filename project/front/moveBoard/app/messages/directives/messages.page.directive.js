'use strict';

angular
	.module('app.messages')
	.directive('messagesPage', messagesPage);

messagesPage.$inject = [];


function messagesPage() {
	return {
		link: link,
		scope: {
			requests: '=',
		},
		template: require('./messages.page.template.html'),
		restrict: 'A'
	};


	function link(scope) {
		const ZERO = 0;
		scope.$watch('requests', () => {
			if (!_.isEmpty(scope.requests)) {
				_.forEach(scope.requests, date => {
					date.date.value = moment(date.date.value).format('MMM DD, YYYY');
				});
				let firstNode = _.head(Object.keys(scope.requests));
				scope.active = _.get(scope.requests, `${[firstNode]}.nid`);
				scope.request = _.get(scope.requests, [firstNode]);
			} else {
				scope.active = ZERO;
				scope.request = {};
			}
		});
	}
}
