'use strict';

import './messages.styl';

angular
	.module('app.messages')
	.directive('messageThread', messageThread);

/* @ngInject */
function messageThread($interval, MessagesServices, Session, common, logger, $rootScope, AuthenticationService, $uibModal, erDeviceDetector, datacontext, EditElementService) {
	const DESKTOP_INTERVAL = 15000;
	const DEVICES_INTERVAL = 100000;
	const FORMAT_MESSAGE = 'full_html';
	return {
		link: link,
		scope: {
			'nid': '=nid',
			'request': '=request',
			'type': '@'
		},
		template: require('./messages_thread.html'),
		restrict: 'A'
	};

	function link($scope) {
		let fieldData = datacontext.getFieldData();
		$scope.isEnableSms = !!Number(fieldData.smsEnable);

		$scope.busy = true;
		$scope.empty = false;
		$scope.isEmptyString = EditElementService.isEmptyHTMLString;
		$scope.loadMessages = loadMessages;
		$scope.openEditCommentModal = openEditCommentModal;
		$scope.messages = [];
		$scope.messagesObj = {};
		let updateMessageTime = erDeviceDetector.isDesktop ? DESKTOP_INTERVAL : DEVICES_INTERVAL;
		let loadMessageInterval;
		let cancel = false;
		let commentsFirst = {};
		let session = Session.get();
		let userRole = _.get(session, 'userRole', []);

		$scope.authorize = AuthenticationService.isAuthorized(userRole);

		$scope.$watch('nid', function () {
			$scope.busy = true;
			if ($scope.nid) {
				loadMessages($scope.nid);
				$interval.cancel(loadMessageInterval);
				loadMessageInterval = $interval(() => {
					loadMessages($scope.nid);
				}, updateMessageTime);
			}
		});

		$scope.$on('request.closed', function () {
			cancel = true;
		});

		$scope.$on('read_all_request_messages', onReadRequestMessage);

		function onReadRequestMessage(event, data) {
			var nid = data.nid;

			if (nid == $scope.nid) {
				$scope.messages.forEach(function (item) {
					if (!item.read) {
						item.read = 1;
					}
				});
			}
		}

		function loadMessages(nid) {
			if (angular.isUndefined($scope.messagesObj[nid])) {
				$scope.messagesObj[nid] = {};
				commentsFirst[nid] = true;
			}

			if (!cancel) {
				MessagesServices.getMessagesThread(nid)
					.then(function (data) {
						$scope.messages = common.objToArray($scope.messagesObj[nid]);
						$scope.busy = false;

						let diff = common.diffArray(data, $scope.messagesObj[nid]);

						if (commentsFirst[nid]) {
							$scope.messages = common.objToArray(data);
							$scope.messagesObj[nid] = data;
							commentsFirst[nid] = false;
						} else if (!commentsFirst[nid] && diff.length) {
							angular.forEach(diff, function (diff) {
								$scope.messages.push(data[diff]);
								$scope.messagesObj[nid][diff] = data[diff];
							});
						}

						let newmessages = 0;

						if (angular.isDefined($scope.request)) {
							angular.forEach(data, function (m, cid) {
								if (!m.read && !m.admin) {
									newmessages++;
								}
							});
						}

						if (angular.isDefined($scope.request)) {
							$scope.request.messages = newmessages;
						}

						angular.forEach($scope.messages, (message) => {
							message.sms = Number(message.sms || 0);
						});

						$scope.empty = !$scope.messages.length;

					}, function (reason) {
						$scope.busy = false;
						logger.error(reason, reason, "Error");
					});
			}
		}

		$scope.addMessage = function () {
			if (_.isEmpty($scope.message)) return;

			$scope.busy = true;
			let session = Session.get();
			let message = {
				comment_body: $scope.message,
				admin: 1,
				user_first_name: session.userId.field_user_first_name,
				user_last_name: session.userId.field_user_last_name,
				created: moment().unix()
			};

			$scope.messages.push(message);
			$scope.empty = false;

			MessagesServices.addMessage($scope.nid, $scope.message, FORMAT_MESSAGE)
				.then(data => {
					$scope.message = null;
					let cid = data[0];
					$scope.messagesObj[cid] = {};
				}, reason => {
					logger.error(reason, reason, "Error");
				})
				.finally(() => {
					$scope.busy = false;
				});
		};

		$scope.sendSMS = () => {
			if (_.isEmpty($scope.message)) return;

			$scope.busy = true;

			let sms = {
				nid: $scope.nid,
				body: $scope.message,
			};

			$scope.empty = false;

			MessagesServices.sendSms(sms)
				.then(() => {
					$scope.message = null;
					loadMessages($scope.nid);
				}, reason => {
					logger.error(reason, reason, "Error");
				})
				.finally(() => {
					$scope.busy = false;
				});
		};

		$scope.readMessage = function (message) {
			if (!message.read) {
				MessagesServices.setCommentReaded(message.cid);
				$scope.request.messages--;
				$rootScope.$broadcast('messaged.readed');
			}
		};


		function openEditCommentModal(comment, nid) {
			$uibModal.open({
				template: require('../templates/edit_comment.html'),
				controller: EditCommentModalCtrl,
				backdrop: false,
				resolve: {
					comment: function () {
						return comment;
					},
				},
			});
		}

		function EditCommentModalCtrl($scope, $uibModalInstance, comment) {
			$scope.comment = comment;

			$scope.Save = function () {
				MessagesServices.updateMessage($scope.comment.cid, $scope.comment.comment_body, FORMAT_MESSAGE);
				$uibModalInstance.dismiss('cancel');
			};

			$scope.Cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};
		}

		$scope.$on('$destroy', () => {
			$interval.cancel(loadMessageInterval);
		});
	}

}
