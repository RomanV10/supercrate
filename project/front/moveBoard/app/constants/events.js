angular.module('app')
	.constant(
		'events', {
			'trip': {
				'tpCollectedRemoved': 'tpcollectedremoved',
				'updateTripDate': 'updatetripdate'
			}
		}
	);
