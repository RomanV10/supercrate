const DISPATCH_INITIAL_SETTINGS = {
	'isCrewAlwaysAvailable': false
};

angular
	.module('app')
	.constant('DISPATCH_INITIAL_SETTINGS', DISPATCH_INITIAL_SETTINGS)
