'use strict';

import './customer-online-modal.styl';

angular
	.module('moveboard-customer-online')
	.controller('CustomerOnlineModalController', CustomerOnlineModalController);

CustomerOnlineModalController.$inject = ['$scope', '$uibModalInstance', 'requests', '$controller'];

function CustomerOnlineModalController($scope, $uibModalInstance, requests, $controller) {
	$controller('SharedCustomerOnline', {$scope});
	const DEPRECATED_USER = 'ADMIN';

	$scope.requests = requests;
	setOnlineCustomersTotalCount();

	$scope.onEvent('message', onConnectUser);
	$scope.onEvent('leave', onLeaveUser);
	$scope.closeCustomersOnlineModal = closeCustomersOnlineModal;

	function onConnectUser(onlineUsers) {
		let request = _.get(onlineUsers, 'data.data.request');
		let uid = _.get(onlineUsers, 'data.data.uid');
		if (_.isEqual(uid, DEPRECATED_USER)) return;

		if (request && uid) {
			$scope.requests[uid] = request;
			setOnlineCustomersTotalCount();
		}
	}

	function onLeaveUser(onlineUsers) {
		let uid = onlineUsers.data.user;
		if (_.isEqual(uid, DEPRECATED_USER)) return;

		delete requests[uid];
		setOnlineCustomersTotalCount();
	}

	function setOnlineCustomersTotalCount() {
		$scope.onlineCustomers = Object.keys($scope.requests).length;
	}

	function closeCustomersOnlineModal() {
		$uibModalInstance.dismiss();
	}
}
