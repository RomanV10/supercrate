'use strict';

import './customer-online-panel.styl';

angular
	.module('moveboard-customer-online')
	.directive('customerOnlinePanel', customerOnlinePanel);

customerOnlinePanel.$inject = ['$rootScope', '$uibModal', 'PermissionsServices'];

function customerOnlinePanel($rootScope,  $uibModal, PermissionsServices) {
	return {
		restrict: 'E',
		template: require('./customer-online-panel.tpl.html'),
		scope: {},
		link: customerOnlinePanelLink,
		controller: 'SharedCustomerOnline',
		controllerAs: 'vm',
	};

	function customerOnlinePanelLink($scope, element, attr, vm) {
		const DEPRECATED_USER = 'ADMIN';
		const CAN_SEE_OTHER_LEADS = PermissionsServices.hasPermission('canSeeOtherLeads');
		const CAN_SEE_UNASSIGNED_LEADS = PermissionsServices.hasPermission('canSeeUnsignedLeads');
		const CURRENT_USER_ID = _.get($rootScope, 'currentUser.userId.uid');
		let requests = {};
		let wasPresence = true;

		$scope.onlineCustomers = 0;

		vm.getOnline();

		function setOnlineCustomersTotalCount() {
			$scope.onlineCustomers = Object.keys(requests).length;
		}

		$scope.onEvent('leave', onlineUsers => {
			let uid = _.get(onlineUsers, 'data.user');
			if (!uid || _.isEqual(uid, DEPRECATED_USER) || !wasPresence) return;

			delete requests[uid];
			setOnlineCustomersTotalCount();
		});

		function filterUser(onlineUser){
			let managerId = _.get(onlineUser, 'data.data.manager');
			let isNotMine = CURRENT_USER_ID != managerId;
			let hideBecauseNotMine = isNotMine && !CAN_SEE_OTHER_LEADS;
			let isUnasigned = managerId == -1;
			let hideBecauseUnassigned = isUnasigned && !CAN_SEE_UNASSIGNED_LEADS;
			return hideBecauseNotMine || hideBecauseUnassigned;
		}

		$scope.onEvent('message', onlineUser => {
			if (filterUser(onlineUser)) return;
			let request = _.get(onlineUser, 'data.data.request');
			let uid = _.get(onlineUser, 'data.data.uid');
			if (request && uid) {
				requests[uid] = request;
				setOnlineCustomersTotalCount();
			}
		});

		$scope.openCustomerOnlineModal = openCustomerOnlineModal;

		function openCustomerOnlineModal() {
			$uibModal.open({
				template: require('../customer-online-modal/customer-online-modal.tpl.html'),
				controller: 'CustomerOnlineModalController',
				size: 'lg',
				backdrop: false,
				resolve: {
					requests: () => requests,
				},
			});
		}
	}
}
