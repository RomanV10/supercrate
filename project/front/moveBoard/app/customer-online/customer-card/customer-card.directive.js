'use strict';

import './customer-card.styl';

angular
	.module('moveboard-customer-online')
	.directive('customerCard', customerCard);

/* @ngInject */
function customerCard (customerOnlineService, openRequestService) {
	return {
		restrict: 'E',
		template: require('./customer-card.tpl.html'),
		scope: {
			request: '<',
			user: '<',
		},
		link: customerCardLink
	};

	function customerCardLink($scope) {
		$scope.$watch('request', () => {
			$scope.userCard = customerOnlineService.prepareUserCardInfo($scope.request, $scope.user);
		});
		$scope.userCard = customerOnlineService.prepareUserCardInfo($scope.request, $scope.user);
		$scope.hideFrom = $scope.request.service_type.raw == '4';
		$scope.hideTo = $scope.request.service_type.raw == '3'
			|| $scope.request.service_type.raw == '8';

		$scope.openRequestModal = openRequestModal;

		function openRequestModal() {
			$scope.busy = true;

			openRequestService.open($scope.request.nid)
				.finally(() => {
					$scope.busy = false;
				});
		}
	}
}
