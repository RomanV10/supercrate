describe('customer-card.directive.js:', () => {
	let rootScope,
		element,
		$scope,
		customerOnlineService,
		RequestServices;

	beforeEach(inject((_customerOnlineService_, _RequestServices_) => {
		customerOnlineService = _customerOnlineService_;
		RequestServices = _RequestServices_;
		rootScope = $rootScope.$new();
		rootScope.request = testHelper.loadJsonFile('request-customer-card.directive.mock');
		rootScope.user = testHelper.loadJsonFile('user-customer-card.directive.mock');
		let directive = '<customer-card request="request" user="user"></customer-card>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	}));

	describe('on init', () => {
		it('$scope.userCard should be defined', () => {
			expect($scope.userCard).toBeDefined();
		});
	});

	describe('on click openRequestModal', () => {
		it('RequestServices.getSuperRequest have to be triggered', () => {
			// given
			let getSuperRequestSpy = spyOn(RequestServices, 'getSuperRequest').and.callThrough();
			// when
			$scope.openRequestModal();
			// then
			expect(getSuperRequestSpy).toHaveBeenCalled();
		});
	});
});
