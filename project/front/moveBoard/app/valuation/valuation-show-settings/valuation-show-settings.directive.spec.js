describe('valuation-show-settings.directive.js:', () => {
	let $scope,
		rootScope,
		element;
	beforeEach(() => {
		rootScope = $rootScope.$new();
		let directive = '<valuation-show-settings></valuation-show-settings>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	});
	
	describe('on init directive', () => {
		it('$scope.directivePresets should be defined', () => {
			expect($scope.directivePresets).toBeDefined();
		});
	});
});
