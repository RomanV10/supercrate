'use strict';

import './valuation-show-settings.styl';
import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.directive('valuationShowSettings', valuationShowSettings);

valuationShowSettings.$inject = [];

function valuationShowSettings () {
	return {
		restrict: 'E',
		template: require('./valuation-show-settings.tpl.html'),
		scope: {},
		link: valuationShowSettingsLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'vm',
	};
	
	function valuationShowSettingsLink ($scope, element, attr, vm) {
		$scope.directivePresets = vm.init({
			setting_name: 'service_types',
			optimisticText: 'Settings for service type were successfully updated',
			errorText: 'Settings for service type were not updated',
			changes: false,
			setting: {},
			all: false,
			index: 2
		});
		
		$scope.$on('$destroy', () => {
			if ($scope.directivePresets.changes) $scope.$emit(ACTIONS.valuationUnsavedChanges, $scope.directivePresets.index, $scope.directivePresets.changes);
		});
	}
}
