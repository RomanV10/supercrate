describe('valuation-plan-setting.directive.js:', () => {
	
	let $scope, $localScope, element, valuationService;
	let get_valuation_plan_mock, settings;

	beforeEach(inject((_valuationService_) => {
		valuationService = _valuationService_;
		$scope = $rootScope.$new();
		settings = testHelper.loadJsonFile('default_valuation_settings.mock');
		get_valuation_plan_mock = testHelper.loadJsonFile('valuation-plan-settings_get_valuation_plan.mock');
		spyOn(valuationService, 'getValuationPlan').and.callFake(() => {
			let fakePromise = {
				then: callback => {
					callback(get_valuation_plan_mock);
					return fakePromise;
				},
				catch: () => {
					return fakePromise;
				},
				finally: callback => {
					callback();
					return fakePromise;
				}
			};
			return fakePromise;
		});
		spyOn(valuationService, 'getSetting').and.callFake(name => settings[name]);
		element = $compile(angular.element('<valuation-plan-settings></valuation-plan-settings>'))($scope);
		$localScope = element.isolateScope();
	}));

	describe('After init,', () => {
		it('selectedItem should be equal 0', () => {
			expect($localScope.selectedItem).toEqual(1);
		});

		it('first item should be truthy', () => {
			expect($localScope.valuationPlan[$localScope.selectedItem]).toBeTruthy();
		});
	});

	describe('changeCurrentPlan (method)', () => {
		beforeEach(() => {
			$localScope.selectedItem = '2';
			$localScope.changeCurrentPlan($localScope.selectedItem);
		});

		it('should select last plan', () => {
			expect($localScope.valuationPlan[0].selected).toBeFalsy();
			expect($localScope.valuationPlan[2].selected).toBeTruthy();
		})
	});

	describe('saveChanges (method)', () => {
		beforeEach(() => {
			spyOn(valuationService, 'updateSetting');
			$localScope.saveChanges();
		});

		it('it should call apiService.postData', () => {
			expect(valuationService.updateSetting).toHaveBeenCalled();
		});
	});

	describe('watchChanges', () => {
		beforeEach(() => {
			let current = {
				header: [200, 300, 400],
				body: [
					{
						name: 100,
						value: [1, 2, 3]
					},
					{
						name: 200,
						value: [2, 3, 4]
					},
					{
						name: 300,
						value: [3, 4, 5]
					}
				]
			};

			let original = {
				header: [200, 300, 400],
				body: [
					{
						name: 100,
						value: [1, 777, 3]
					},
					{
						name: 200,
						value: [2, 3, 4]
					},
					{
						name: 300,
						value: [3, 4, 5]
					}
				]
			};
			$localScope.watchTableChanges(current, original);
		});

		it('button "Save Changes" should be unblocked', () => {
			expect($localScope.changeDetected).toBeTruthy();
		});
	});
});
