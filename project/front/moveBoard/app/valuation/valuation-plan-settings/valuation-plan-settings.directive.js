'use strict';

import './valuation-plan-settings.styl';

angular
	.module('moveboard-valuation')
	.directive('valuationPlanSettings', valuationPlanSettings);

valuationPlanSettings.$inject = [];

function valuationPlanSettings () {
	return {
		restrict: 'E',
		template: require('./valuation-plan-settings.tpl.html'),
		scope: {},
		controller: 'ValuationPlanSettingsController',
	};
}
