'use strict';

import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.controller('ValuationPlanSettingsController', ValuationPlanSettingsController);

ValuationPlanSettingsController.$inject = ['$scope', 'valuationService', 'Raven', 'SweetAlert', '$q'];

function ValuationPlanSettingsController($scope, valuationService, Raven, SweetAlert, $q) {
	let TABLE_NAMES = valuationService.TABLE_NAMES;
	let vm = this;
	const BY_RATE_TABLE_INDEX = 2;
	const UPDATE_MSG = 'Saving...';
	vm.saveChanges = saveChanges;
	$scope.valuationPlan = valuationService.getSetting('valuation_plan_settings');
	$scope.selectedItem = _.findIndex($scope.valuationPlan, plan => plan.selected);
	$scope.changeDetected = false;
	$scope.changeCurrentPlan = changeCurrentPlan;
	$scope.saveChanges = saveChanges;
	$scope.watchTableChanges = watchTableChanges;
	$scope.loading = {
		text: 'Loading...', action: true
	};
	vm.idRateTable = $scope.selectedItem === BY_RATE_TABLE_INDEX;
	
	init();
	
	function init() {
		$scope.tables = valuationService.getValuationPlan();
		vm.selectedTable = $scope.tables[TABLE_NAMES[$scope.selectedItem]];
		vm.percent = !$scope.selectedItem;
		$scope.loading.action = false;
		$scope.$watch('tables', watchTableChanges, true);
	}
	
	function changeCurrentPlan(index) {
		if (TABLE_NAMES[index]) vm.selectedTable = $scope.tables[TABLE_NAMES[index]];
		vm.percent = !index;
		vm.idRateTable = index === BY_RATE_TABLE_INDEX;
		for (let key in $scope.valuationPlan) {
			$scope.valuationPlan[key].selected = false;
		}
		$scope.valuationPlan[$scope.selectedItem].selected = true;
		valuationService.setSetting('valuation_plan_settings', $scope.valuationPlan);
		$scope.changeDetected = true;
	}
	
	function watchTableChanges(current, original) {
		if (current !== original) {
			$scope.changeDetected = true;
		}
	}
	
	function saveChanges() {
		$scope.loading.action = true;
		$scope.loading.text = UPDATE_MSG;
		let planPromise = valuationService.saveValuationPlan($scope.tables);
		let settingPromise = valuationService.updateSetting();
		$q.all({planPromise, settingPromise})
			.then(() => {
				toastr.success('Valuation Settings were saved');
				$scope.changeDetected = false;
			})
			.catch(error => {
				Raven.captureException(`Save valuation settings throw error: ${error}`);
				SweetAlert.swal('Error', 'Valuation settings were not saved', 'error');
			})
			.finally(() => $scope.loading.action = false);
	}

	$scope.$on(ACTIONS.coefficientWasChanged, () => {
		$scope.changeDetected = true;
	});
}
