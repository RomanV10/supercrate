describe('valuation-proposal-to-contact.directive.js:', () => {
	let rootScope, element, $scope;
	
	beforeEach(() => {
		rootScope = $rootScope.$new();
		let directive = '<valuation-proposal-to-contact></valuation-proposal-to-contact>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	});
	
	describe('on init', () => {
		it('$scope.directivePresets have to be defined', () => {
			expect($scope.directivePresets).toBeDefined();
		});
	});
});
