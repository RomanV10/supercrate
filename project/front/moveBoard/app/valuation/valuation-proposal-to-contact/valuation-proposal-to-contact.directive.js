'use strict';

import './valuation-proposal-to-contact.styl';
import {
	ACTIONS,
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.directive('valuationProposalToContact', valuationProposalToContact);

valuationProposalToContact.$inject = [];

function valuationProposalToContact() {
	return {
		restrict: 'E',
		template: require('./valuation-proposal-to-contact.tpl.html'),
		scope: {
			state: '=',
		},
		link: valuationProposalToContactLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'vm',
	};
	
	function valuationProposalToContactLink($scope, element, attr, vm) {
		$scope.directivePresets = vm.init({
			setting_name: 'valuation_proposal_to_contact',
			optimisticText: 'Setting of the proposal to contact was successfully updated',
			errorText: 'Setting of the proposal to contact was not updated',
			changes: false,
			setting: {},
			index: 3,
		});
		
		$scope.$watch('directivePresets.changes', (current) => {
			if (current) $scope.$emit(ACTIONS.childrenHasChangesValuationAccountShowSettings);
		});
		
	}
}
