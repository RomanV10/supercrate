'use strcit';

import './valuation-coefficient.styl';
import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.directive('valuationCoefficient', valuationCoefficient);

valuationCoefficient.$inject = [];

function valuationCoefficient () {
	return {
		restrict: 'E',
		template: require('./valuation-coefficient.tpl.html'),
		scope: {},
		link: valuationCoefficientLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'vm',
	};
	
	function valuationCoefficientLink($scope, element, attr, vm) {
		$scope.directivePresets = vm.init({
			setting_name: 'valuation_cent_per_pound',
			optimisticText: 'Cent per pound was successfully updated',
			errorText: 'Cent per pound feet was not updated',
			changes: false,
			setting: {},
			index: 4,
		});
		
		$scope.$on('$destroy', () => {
			if ($scope.directivePresets.changes) $scope.$emit(ACTIONS.valuationUnsavedChanges, $scope.directivePresets.index, $scope.directivePresets.changes);
		});
	}
}
