describe('valuation-coefficient.directive.js', () => {
	let rootScope, element, $scope;
	
	beforeEach(() => {
		rootScope = $rootScope.$new();
		let directive = '<valuation-coefficient></valuation-coefficient>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	});
	
	describe('on init', () => {
		it('$scope.directivePresets should be defined', () => {
			expect($scope.directivePresets).toBeDefined();
		});
		
		it('$scope.directivePresets should be equal expectedResult', () => {
			// given
			let expectedResult = {
				setting_name: 'valuation_cent_per_pound',
				optimisticText: 'Cent per pound was successfully updated',
				errorText: 'Cent per pound feet was not updated',
				changes: false,
				setting: 0.6,
				index: 4
			};
			//then
			expect($scope.directivePresets).toEqual(expectedResult);
		});
	});
});
