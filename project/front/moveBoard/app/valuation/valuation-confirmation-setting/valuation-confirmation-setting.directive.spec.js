describe('valuation-confirmation-setting.directive.js, ', () => {
	let rootScope,
		element,
		$scope;
	
	beforeEach(() => {
		rootScope = $rootScope.$new();
		let directive = '<valuation-confirmation-setting></valuation-confirmation-setting>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	});
	
	describe('on init', () => {
		it('$scope.directivePresets should be defined', () => {
			expect($scope.directivePresets).toBeDefined();
		});
		
		it('$scope.directivePresets have to be equals expectedResult', () => {
			// given
			let expectedResult = {
				setting_name: 'show_valuation_confirmation_page_setting',
				optimisticText: 'Valuation settings for confirmation page were successfully updated',
				errorText: 'Valuation settings for confirmation page were not updated',
				changes: false,
				setting: {
					1: true,
					2: true,
					3: true,
					4: true,
					5: true,
					6: true,
					7: true,
					8: true,
				},
				all: true,
				index: 2,
			};
			// then
			expect($scope.directivePresets).toEqual(expectedResult);
		});
	});
});
