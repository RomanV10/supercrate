'use strict';

import './valuation-confirmation-setting.styl';
import {
	ACTIONS,
} from '../../../../shared/valuation-shared/helper/events'

angular
	.module('moveboard-valuation')
	.directive('valuationConfirmationSetting', valuationConfirmationSetting);

valuationConfirmationSetting.$inject = [];

function valuationConfirmationSetting () {
	return {
		restrict: 'E',
		template: require('./valuation-confirmation-setting.tpl.html'),
		scope: {},
		link: valuationConfirmationSettingLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'vm',
	};
	
	function valuationConfirmationSettingLink ($scope, element, attr, vm) {
		$scope.directivePresets = vm.init({
			setting_name: 'show_valuation_confirmation_page_setting',
			optimisticText: 'Valuation settings for confirmation page were successfully updated',
			errorText: 'Valuation settings for confirmation page were not updated',
			changes: false,
			setting: {},
			all: false,
			index: 2,
		});
		
		$scope.$on('$destroy', () => {
			if ($scope.directivePresets.changes) $scope.$emit(ACTIONS.valuationUnsavedChanges, $scope.directivePresets.index, $scope.directivePresets.changes);
		});
	}
}
