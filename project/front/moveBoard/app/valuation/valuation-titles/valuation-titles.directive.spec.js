describe('valuation-title.directive.js:', () => {
	let rootScope, element, $scope;
	
	beforeEach(() => {
		rootScope = $rootScope.$new();
		let directive = '<valuation-titles></valuation-titles>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	});
	
	describe('After init,', () => {
		it('$scope.directivePresets should be defined', () => {
			expect($scope.directivePresets).toBeDefined();
		});
	});
});
