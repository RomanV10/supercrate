'use strict';
import './valuation-titles.styl';
import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.directive('valuationTitles', valuationTitles);

valuationTitles.$inject = ['valuationService'];

function valuationTitles(valuationService) {
	return {
		restrict: 'E',
		template: require('./valuation-titles.tpl.html'),
		scope: {},
		link: valuationTitlesLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'vm',
	};
	
	function valuationTitlesLink($scope, element, attr, vm) {
		$scope.tooltipButtons = [
			{'name': 'Client Reservation Signature', 'value': '[estimate-signature:client-reservation-signature]'},
			{'name': 'Sales Signature', 'value': '[estimate-signature:sales-signature]'},
		];

		$scope.valuationTypes = valuationService.VALUATION_TYPES;
		$scope.directivePresets = vm.init({
			setting_name: 'valuation_titles',
			optimisticText: 'Setting was successfully updated',
			errorText: 'Setting was not updated',
			changes: false,
			setting: {},
			index: 1,
		});

		$scope.insertTextInInput = insertTextInInput;
		
		$scope.$on('$destroy', () => {
			if ($scope.directivePresets.changes) $scope.$emit(ACTIONS.valuationUnsavedChanges, $scope.directivePresets.index, $scope.directivePresets.changes);
		});

		function insertTextInInput(text) {
			let textEditor = findChildChildElementByPartId(element, 'DIV', 'taTextElement');
			textEditor.focus();
			insertTextAtCursor(text);
		}

		function findChildChildElementByPartId(baseElement, tag, partId) {
			let elementsByTag = baseElement.find(tag);

			for (let i = 0; i < elementsByTag.length; i++) {
				let wrapElement = angular.element(elementsByTag[i]);
				let id = wrapElement.attr('id');
				let tagName = wrapElement.prop('tagName');

				if (angular.isDefined(id) && angular.equals(tag, tagName) && id.indexOf(partId) >= 0) {
					return wrapElement;
				}
			}
		}

		function insertTextAtCursor(text) {
			setTimeout(function () {
				let sel, range;

				if (window.getSelection) {
					sel = window.getSelection();
					if (sel.getRangeAt && sel.rangeCount) {
						range = sel.getRangeAt(0);
						range.deleteContents();
						range.insertNode(document.createTextNode(text));
					}
				} else if (document.selection && document.selection.createRange) {
					document.selection.createRange().text = text;
				}
			}, 10);
		}
	}
}
