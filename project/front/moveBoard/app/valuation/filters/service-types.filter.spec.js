describe('service-types.filter.js:', () => {
	
	describe('filter should return name of first service type,', () => {
		it('Local Moving returns', () => {
			// given
			let expectedResult = 'Local Moving';
			let result;
			//when
			result = $filter('serviceTypeFilter')(1);
			
			//then
			expect(result).toBe(expectedResult);
		});
	});
	
	describe('filter should return service type name if service type is disabled', () => {
		it('returns "Moving"', () => {
			// given
			let expectedResult = 'Moving';
			let result;
			$rootScope.fieldData.basicsettings = angular.fromJson($rootScope.fieldData.basicsettings);
			$rootScope.fieldData.basicsettings.services.localMoveOn = false;
			
			//when
			result = $filter('serviceTypeFilter')(1);
			
			//then
			expect(result).toBe(expectedResult);
		});
	});
});
