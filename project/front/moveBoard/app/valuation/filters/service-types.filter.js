'use strict';

angular
	.module('moveboard-valuation')
	.filter('serviceTypeFilter', serviceTypeFilter);

serviceTypeFilter.$inject = ['serviceTypeService', 'datacontext'];

function serviceTypeFilter (serviceTypeService, datacontext) {
	
	let fieldData = datacontext.getFieldData();
	let allowedServiceTypes = fieldData.field_lists.field_move_service_type;
	
	return (id) => {
		let all = serviceTypeService.getAllServiceTypes();
		let currentService = _.find(all, item => item.id == id);
		return _.get(currentService, 'title', allowedServiceTypes[id]);
	};
}
