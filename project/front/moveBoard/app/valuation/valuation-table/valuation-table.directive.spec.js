describe('valuation-table.directive.js:', () => {
	let rootScope,
		element,
		$scope;

	beforeEach(() => {
		rootScope = $rootScope.$new();
		element = $compile('<valuation-plan-settings><valuation-table/></valuation-plan-settings>')(rootScope);
		$scope = element.find('valuation-table').isolateScope();
	});

	describe('on init,', () => {
		it('vm.selectedTable should be defined', () => {
			expect($scope.vm.selectedTable).toBeDefined();
		});

		it('changeDeductibleLevel should be defined', () => {
			expect($scope.changeDeductibleLevel).toBeDefined();
		});

		it('changeAmountOfValuation should be defined', () => {
			expect($scope.changeAmountOfValuation).toBeDefined();
		});

		it('removeAmountOfValuation should be defined', () => {
			expect($scope.removeAmountOfValuation).toBeDefined();
		});
	});

	describe('changeDeductibleLevel,', () => {
		let index = 1;
		beforeEach(() => {
			$scope.changeDeductibleLevel(index, 255);
		});

		it('method should update model', () => {
			expect($scope.vm.selectedTable.header[index]).toEqual(255);
		});
	});

	describe('removeRow,', () => {
		beforeEach(() => {
			$scope.removeRow(true, 0);
		});

		it('method should remove item', () => {
			expect($scope.vm.selectedTable.body.length).toEqual(2);
			expect($scope.vm.selectedTable.body[0].name).not.toEqual(100);
			expect($scope.vm.selectedTable.body[0].name).toEqual('200');
		});
	});

	describe('removeColumn,', () => {
		beforeEach(() => {
			$scope.removeColumn(true, 0);
		});

		it('method should remove item and remove all items in body with the same index', () => {
			expect($scope.vm.selectedTable.header.length).toEqual(2);
			expect($scope.vm.selectedTable.body[0].value.length).toEqual(2);
			expect($scope.vm.selectedTable.body[1].value.length).toEqual(2);
			expect($scope.vm.selectedTable.body[2].value.length).toEqual(2);
		});
	});

	describe('addRow,', () => {
		describe('If there is not any rows yet,', () => {
			beforeEach(() => {
				spyOn($scope, 'sortByRows');
				$scope.vm.selectedTable.body = [];
				$scope.addRow();
			});

			it('method should add new row to body by preset', () => {
				expect($scope.vm.selectedTable.body.length).toEqual(1);
				expect($scope.vm.selectedTable.body[0]).toEqual({
					name: 0,
					value: [0, 0, 0],
				});
			});
			it('should call sortByRows()', () => {
				expect($scope.sortByRows).toHaveBeenCalled();
			});
		});

		describe('If there is some rows already,', () => {
			beforeEach(() => {
				spyOn($scope, 'sortByRows');
				$scope.addRow();
			});

			it('method should copy the last row', () => {
				expect($scope.vm.selectedTable.body.length).toEqual(4);
				expect($scope.vm.selectedTable.body[3].name).toEqual($scope.vm.selectedTable.body[2].name);
				expect($scope.vm.selectedTable.body[3].value).toEqual($scope.vm.selectedTable.body[2].value);
			});
			it('should call sortByRows()', () => {
				expect($scope.sortByRows).toHaveBeenCalled();
			});
		});
	});

	describe('addColumn,', () => {
		beforeEach(() => {
			$scope.addColumn();
		});

		it('method should add new column', () => {
			expect($scope.vm.selectedTable.header.length).toEqual(4);
			expect($scope.vm.selectedTable.body[0].value.length).toEqual(4);
			expect($scope.vm.selectedTable.body[1].value.length).toEqual(4);
			expect($scope.vm.selectedTable.body[2].value.length).toEqual(4);
		});
	});

	describe('changeAmountOfValuation,', () => {
		beforeEach(() => {
			let index = 1;
			let amount = 1234;
			spyOn($scope, 'sortByRows');
			$scope.changeAmountOfValuation($scope.vm.selectedTable.body[2].value, index, amount);
		});

		it('method should update cell', () => {
			expect($scope.vm.selectedTable.body[2].value[1]).toBe(1234);
		});
		it('should call sortByRows()', () => {
			expect($scope.sortByRows).toHaveBeenCalled();
		});
	});
});
