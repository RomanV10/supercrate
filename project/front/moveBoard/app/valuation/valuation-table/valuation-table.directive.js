'use strict';

import './valuation-table.styl';

angular
	.module('moveboard-valuation')
	.directive('valuationTable', valuationTable);

valuationTable.$inject = ['SweetAlert'];

function valuationTable (SweetAlert) {
	return {
		restrict: 'E',
		template: require('./valuation-table.tpl.html'),
		scope: {},
		require: '^valuationPlanSettings',
		link: valuationTableLink
	};

	function valuationTableLink ($scope, element, attr, vm) {
		const NEW_ROW_PRESET = {
			name: 0,
			value: [],
		};

		$scope.vm = vm;

		$scope.changeDeductibleLevel = changeDeductibleLevel;
		$scope.changeAmountOfValuation = changeAmountOfValuation;
		$scope.removeAmountOfValuation = removeAmountOfValuation;
		$scope.removeDeductibleLevel = removeDeductibleLevel;
		$scope.sortByRows = sortByRows;
		$scope.addRow = addRow;
		$scope.addColumn = addColumn;
		$scope.removeColumn = removeColumn;
		$scope.removeRow = removeRow;
		$scope.trackValuationTable = trackValuationTable;

		function addRow() {
			let newRow = {};
			let rowsCount = vm.selectedTable.body.length;
			if (vm.selectedTable.body.length) {
				newRow.name = vm.selectedTable.body[rowsCount - 1].name;
				newRow.value = angular.copy(vm.selectedTable.body[rowsCount - 1].value);
			} else {
				newRow = angular.copy(NEW_ROW_PRESET);
				let columnsCount = vm.selectedTable.header.length;
				for (let i = 0; i < columnsCount; i++) {
					newRow.value.push(0);
				}
			}
			vm.selectedTable.body.push(newRow);
			$scope.sortByRows();
		}

		function addColumn() {
			let columnCount = vm.selectedTable.header.length - 1;
			let headItem = _.get(vm.selectedTable.header, columnCount, 0);
			vm.selectedTable.header.push(headItem);
			_.forEach(vm.selectedTable.body, item => {
				let arritem = _.get(item.value, columnCount, 0);
				item.value.push(arritem);
			});
		}

		function changeAmountOfValuation(row, index, amount) {
			if (row[index] == amount) return;
			row[index] = amount;
			$scope.sortByRows();
		}

		function changeDeductibleLevel(index, deductible) {
			if (vm.selectedTable.header[index] == deductible) return;

			vm.selectedTable.header[index] = deductible;
		}

		function removeDeductibleLevel(index) {
			SweetAlert.swal({
				title: 'Are you sure want to remove this deductible level?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: `Yes, let's do it!`,
				cancelButtonText: 'No, cancel please!',
				closeOnConfirm: true,
				closeOnCancel: true
			}, confirm => removeColumn(confirm, index));
		}

		function removeColumn (confirm, index) {
			if (!confirm) return;

			vm.selectedTable.header.splice(index, 1);
			_.forEach(vm.selectedTable.body, item => {
				item.value.splice(index, 1);
			});
		}

		function removeAmountOfValuation(index) {
			SweetAlert.swal({
				title: 'Are you sure want to remove this valuation?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: `Yes, let's do it!`,
				cancelButtonText: 'No, cancel please!',
				closeOnConfirm: true,
				closeOnCancel: true
			}, confirm => removeRow(confirm, index));
		}

		function removeRow(confirm, index) {
			if (!confirm) return;

			vm.selectedTable.body.splice(index, 1);
		}

		function sortByRows() {
			vm.selectedTable.body.sort((row1, row2) => +row1.name - +row2.name);
		}

		function replaceColumns(index1, index2) {
			let temp;
			temp = vm.selectedTable.header[index1];
			vm.selectedTable.header[index1] = vm.selectedTable.header[index2];
			vm.selectedTable.header[index2] = temp;
			_.forEach(vm.selectedTable.body, row => {
				temp = row.value[index1];
				row.value[index1] = row.value[index2];
				row.value[index2] = temp;
			});
		}

		function trackValuationTable (index) {
			return `${moment().unix()}#${index}`;
		}

	}
}
