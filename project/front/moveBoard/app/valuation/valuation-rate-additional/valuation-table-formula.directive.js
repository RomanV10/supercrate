'use strict';

import './valuation-table-formula.styl';
import {
	ACTIONS,
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.directive('valuationTableFormula', valuationTableFormula);

valuationTableFormula.$inject = [];

function valuationTableFormula () {
	return {
		restrict: 'E',
		template: require('./valuation-table-formula.tpl.html'),
		require: '^valuationPlanSettings',
		scope: {},
		link: valuationTableFormulaLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'ctrl',
	};
	
	function valuationTableFormulaLink ($scope, element, attr, vm) {
		$scope.vm = vm;
		$scope.deductibleLevel = '0';
		$scope.amountOfLiability = 0;
		$scope.result = 0;
		$scope.calculateValuation = calculateValuation;

		$scope.directivePresets = $scope.ctrl.init({
			setting_name: 'valuation_rate_coefficient',
			optimisticText: 'Setting was successfully updated',
			errorText: 'Setting was not updated',
			changes: false,
			setting: {},
		});
		
		function calculateValuation () {
			let tableRow = findTableRow($scope.amountOfLiability, $scope.vm.selectedTable);
			let rate = +$scope.vm.selectedTable.body[tableRow].value[$scope.deductibleLevel];
			$scope.result = _.round(+$scope.amountOfLiability * rate / $scope.directivePresets.setting, 2);
		}
		
		function findTableRow(amountOfLiability, table) {
			let rowCount = table.body.length;
			let tableRow = table.body.reduce((selectedRow, currentRow, index) => {
				if (+amountOfLiability > +currentRow.name)
					return +index + 1;
				return selectedRow;
			}, 0);
			if (tableRow >= rowCount)
				tableRow = rowCount - 1;
			return tableRow;
		}

		$scope.$watch('directivePresets.setting', (current, original) => {
			if (!_.isEqual(current, original)) $scope.$emit(ACTIONS.coefficientWasChanged);
		});
		
	}
}
