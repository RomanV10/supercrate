describe('valuation-table-formula.directive:', () => {
	let rootScope, element, $scope;
	
	beforeEach(inject(() => {
		rootScope = $rootScope.$new();
		element = $compile('<valuation-plan-settings><valuation-table/></valuation-plan-settings>')(rootScope);
		$scope = element.isolateScope();
		$scope.changeCurrentPlan(2);
		$scope.$apply();
		$scope = element.find('valuation-table-formula').isolateScope();
	}));
	
	describe('After init,', () => {
		it('Should have vm in scope', () => {
			expect($scope.vm).toBeDefined();
		});
		it('Should have deductibleLevel in scope', () => {
			expect($scope.deductibleLevel).toEqual('0');
		});
		it('Should have amountOfLiability in scope', () => {
			expect($scope.amountOfLiability).toEqual(0);
		});
		it('Should have calculateValuation in scope', () => {
			expect($scope.calculateValuation).toBeDefined(0);
		});
	});
	
	describe('function calculateValuation,', () => {
		it('Should return correct value on first row', () => {
			$scope.amountOfLiability = 10;
			$scope.calculateValuation();
			expect($scope.result).toEqual(0.1);
		});
		it('Should return correct value on second row', () => {
			$scope.amountOfLiability = 40;
			$scope.calculateValuation();
			expect($scope.result).toEqual(4);
		});
		it('Should return correct value after last row', () => {
			$scope.amountOfLiability = 60;
			$scope.calculateValuation();
			expect($scope.result).toEqual(6);
		});
		it('Should return 8 valuation charge when coefficient is 500', () => {
			//given
			$scope.directivePresets.setting = 500;
			$scope.amountOfLiability = 40;

			//when
			$scope.calculateValuation();

			//then
			expect($scope.result).toEqual(8.00);
		});
	});
});
