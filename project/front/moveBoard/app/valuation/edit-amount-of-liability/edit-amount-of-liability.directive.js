'use strict';

import './edit-amount-of-liability.styl';
import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.directive('editAmountOfLiability', editAmountOfLiability);

editAmountOfLiability.$inject = [];

function editAmountOfLiability () {
	return {
		restrict: 'E',
		template: require('./edit-amount-of-liability.tpl.html'),
		scope: {
			state: '=',
		},
		link: editAmountOfLiabilityLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'vm',
	};
	
	function editAmountOfLiabilityLink ($scope, element, attr, vm) {
		$scope.directivePresets = vm.init({
			setting_name: 'edit_amount_of_liability',
			optimisticText: 'Setting was successfully updated',
			errorText: 'Setting was not updated',
			changes: false,
			setting: {},
			index: 4,
		});

		$scope.$watch('directivePresets', (current) => {
			if (current.changes) $scope.$emit(ACTIONS.childrenHasChangesValuationAccountShowSettings);
		}, true);
	}
}
