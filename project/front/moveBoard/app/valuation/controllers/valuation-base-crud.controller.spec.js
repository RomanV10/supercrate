describe('valuation-base-crud.controller.js: ', () => {
	let controller,
		valuationService,
		vm,
		baseMockObject,
		apiService;
	
	beforeEach(inject((_$controller_, _valuationService_, _apiService_) => {
		controller = _$controller_;
		valuationService = _valuationService_;
		apiService = _apiService_;
		baseMockObject = {
			setting_name: 'valuation_account_show_setting',
			optimisticText: 'Setting was successfully updated',
			errorText: 'Setting was not updated',
			changes: false,
			setting: {},
		};
		vm = controller('ValuationBaseCRUDController', {
			valuationService: valuationService,
			$scope: {}
		});
	}));
	
	describe('on init', () => {
		it('vm.applySettingChanges should be defined', () => {
			expect(vm.applySettingChanges).toBeDefined();
		});
		
		it('vm.updateValuationSetting should be defined', () => {
			expect(vm.updateValuationSetting).toBeDefined();
		});
		
		describe('vm.init', () => {
			it('method should be defined', () => {
				expect(vm.init).toBeDefined();
			});
		});
	});
	
	describe('on change', () => {
		it('valuationService.setSetting should been called', () => {
			// given
			let setSettingSpy = spyOn(valuationService, 'setSetting');
			// when
			vm.applySettingChanges(baseMockObject);
			// then
			expect(setSettingSpy).toHaveBeenCalled();
		});
		
		it('changes should be equal TRUE', () => {
			// when
			vm.applySettingChanges(baseMockObject);
			// then
			expect(baseMockObject.changes).toBeTruthy();
		});
	});
	
	describe('on save', () => {
		it('valuationService.updateSetting have to been called', () => {
			// given
			let updateSettingSpy = spyOn(valuationService, 'updateSetting').and.callThrough();
			// when
			vm.updateValuationSetting(baseMockObject);
			// then
			expect(updateSettingSpy).toHaveBeenCalled();
		});
		
		it('apiService.postData have to been called', () => {
			// given
			let postDataSpy = spyOn(apiService, 'postData').and.callThrough();
			// when
			vm.updateValuationSetting(baseMockObject);
			// then
			expect(postDataSpy).toHaveBeenCalled();
		});
	});
});
