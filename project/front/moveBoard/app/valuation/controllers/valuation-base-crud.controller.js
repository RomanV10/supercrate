'use strict';

import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.controller('ValuationBaseCRUDController', ValuationBaseCRUDController);

ValuationBaseCRUDController.$inject = ['$scope', 'valuationService'];

function ValuationBaseCRUDController ($scope, valuationService) {
	let vm = this;
	vm.applySettingChanges = applySettingChanges;
	vm.updateValuationSetting = updateValuationSetting;
	vm.init = init;
	vm.checkAll = checkAll;
	vm.getCurrentAll = getCurrentAll;
	vm.switchItem = switchItem;
	let allSettingLength = _.get(valuationService, 'VALUATION_TABS.length', 0);
	
	function checkAll (directivePresets) {
		for (let key in directivePresets.setting) {
			directivePresets.setting[key] = directivePresets.all;
		}
		vm.applySettingChanges(directivePresets);
	}
	
	function getCurrentAll (directivePresets) {
		return Object.keys(directivePresets.setting)
			.every(item => directivePresets.setting[item]);
	}
	
	function switchItem(directivePresets, key, value) {
		directivePresets.setting[key] = value;
		applySettingChanges(directivePresets);
		directivePresets.all = vm.getCurrentAll(directivePresets);
	}
	
	function updateValuationSetting (directivePresets) {
		valuationService.updateSetting()
			.then(() => {
				toastr.success(directivePresets.optimisticText);
				directivePresets.changes = false;
				
				for (let i = 0; i < allSettingLength; i++) {
					$scope.$emit(ACTIONS.valuationUnsavedChanges, i, false);
				}
				
			}, () => {
				toastr.error(directivePresets.errorText);
			});
	}
	
	function applySettingChanges (directivePresets) {
		valuationService.setSetting(directivePresets.setting_name, directivePresets.setting);
		directivePresets.changes = true;
	}
	
	function init (directivePresets) {
		directivePresets.setting = valuationService.getSetting(directivePresets.setting_name);
		directivePresets.changes = valuationService.checkValuationChanges(directivePresets.setting_name);
		
		if (!_.isUndefined(directivePresets.all)) {
			directivePresets.all = vm.getCurrentAll(directivePresets);
		}
		
		return directivePresets;
	}
}
