'use strict';

import './valuation-move-board-setting.styl';

angular
	.module('moveboard-valuation')
	.directive('valuationMoveBoardSetting', valuationMoveBoardSetting);

valuationMoveBoardSetting.$inject = [];

function valuationMoveBoardSetting () {
	return {
		restrict: 'E',
		template: require('./valuation-move-board-setting.tpl.html'),
		scope: {},
		controller: 'ValuationMoveBoardSettingController',
		controllerAs: 'vm',
	};
}
