'use strict';

import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.controller('ValuationMoveBoardSettingController', ValuationMoveBoardSettingController);

ValuationMoveBoardSettingController.$inject = ['$scope', 'valuationService'];

function ValuationMoveBoardSettingController ($scope, valuationService) {
	let vm = this;
	
	vm.changeActiveTab = changeActiveTab;
	
	vm.valuationTabs = _.cloneDeep(valuationService.VALUATION_TABS);
	
	vm.template = vm.valuationTabs[0].directiveTemplate;
	
	function changeActiveTab(index) {
		vm.template = vm.valuationTabs[index].directiveTemplate;
		for (let tab of vm.valuationTabs) {
			tab.active = false;
		}
		vm.valuationTabs[index].active = true;
	}
	
	$scope.$on(ACTIONS.valuationUnsavedChanges, (_, index, value) => {
		vm.valuationTabs[index].unsavedChanges = value;
	});
}
