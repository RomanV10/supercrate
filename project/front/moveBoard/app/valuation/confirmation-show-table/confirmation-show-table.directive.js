'use strict';

import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.directive('confirmationShowTable', confirmationShowTable);

confirmationShowTable.$inject = [];

function confirmationShowTable() {
	return {
		restrict: 'E',
		template: require('./confirmation-show-table.tpl.html'),
		scope: {},
		link: confirmationShowTableLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'vm'
	};
	
	function confirmationShowTableLink($scope, element, attr, vm) {
		$scope.directivePresets = vm.init({
			setting_name: 'confirmation_table_show',
			optimisticText: 'Setting was successfully updated',
			errorText: 'Setting was not updated',
			changes: false,
			setting: {},
			index: 3,
		});
		
		$scope.$watch('directivePresets', (current) => {
			if (current.changes) $scope.$emit(ACTIONS.childrenHasChangesValuationAccountShowSettings);
		}, true);
	}
}
