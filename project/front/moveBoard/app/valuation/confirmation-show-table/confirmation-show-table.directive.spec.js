describe('confirmation-show-table.directive.js', () => {
	let rootScope, element, $scope;
	
	beforeEach(() => {
		rootScope = $rootScope.$new();
		let directive = '<confirmation-show-table></confirmation-show-table>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	});
	
	describe('on init', () => {
		it('$scope.directivePresets should be defined', () => {
			expect($scope.directivePresets).toBeDefined();
		});
		
		it('$scope.directivePresets have to be equals result', () => {
			expect($scope.directivePresets).toEqual({
				setting_name: 'confirmation_table_show',
				optimisticText: 'Setting was successfully updated',
				errorText: 'Setting was not updated',
				changes: false,
				setting: false,
				index: 3,
			});
		});
	});
});
