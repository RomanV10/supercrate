'use strict';

angular
	.module('moveboard-valuation', [
		'valuation-shared',
		'move-board-compile',
	]);
