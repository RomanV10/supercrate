'use strict';

import './valuation-show.styl';

angular
	.module('moveboard-valuation')
	.directive('valuationShow', valuationShow);

valuationShow.$inject = [];

function valuationShow () {
	return {
		restrict: 'E',
		template: require('./valuation-show.tpl.html'),
		scope: {},
	};
}
