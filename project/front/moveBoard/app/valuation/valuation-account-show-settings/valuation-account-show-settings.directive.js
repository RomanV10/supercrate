'use strict';

import './valuation-account-show-settings.styl';
import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.directive('valuationAccountShowSettings', valuationAccountShowSettings);

valuationAccountShowSettings.$inject = [];

function valuationAccountShowSettings () {
	return {
		restrict: 'E',
		template: require('./valuation-account-show-settings.tpl.html'),
		scope: {},
		link: valuationAccountShowSettingsLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'vm',
	};
	
	function valuationAccountShowSettingsLink($scope, element, attr, vm) {
		$scope.directivePresets = vm.init({
			setting_name: 'valuation_account_show_setting',
			optimisticText: 'Setting was successfully updated',
			errorText: 'Setting was not updated',
			changes: false,
			setting: {},
			index: 3,
		});
		
		function childrenHasChangesValuationAccountShowSettings() {
			$scope.directivePresets.changes = true;
		}
		function showValuationProposalToContact(_, state) {
			$scope.state = state;
		}
		
		$scope.$on(ACTIONS.childrenHasChangesValuationAccountShowSettings, childrenHasChangesValuationAccountShowSettings);
		
		$scope.$on(ACTIONS.showValuationProposalToContact, showValuationProposalToContact);
		
		$scope.$on('$destroy', () => {
			if ($scope.directivePresets.changes) $scope.$emit(ACTIONS.valuationUnsavedChanges, $scope.directivePresets.index, $scope.directivePresets.changes);
		});
	}
}
