describe('valuation-account-show-settings.directive.js', () => {
	let rootScope, element, $scope;

	beforeEach(() => {
		rootScope = $rootScope.$new();
		let directive = '<valuation-account-show-settings></valuation-account-show-settings>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	});

	describe('on init', () => {
		it('$scope.directivePresets have to be defined', () => {
			expect($scope.directivePresets).toBeDefined();
		});
		
		it('$scope.directivePresets have to be equals expectedResult', () => {
			// given
			let expectedResult = {
				setting_name: 'valuation_account_show_setting',
				optimisticText: 'Setting was successfully updated',
				errorText: 'Setting was not updated',
				changes: false,
				setting: {
					cent_per_pound: true,
					full_value_protection: false
				},
				index: 3,
			};
			//then
			expect($scope.directivePresets).toEqual(expectedResult);
		});
	});
});
