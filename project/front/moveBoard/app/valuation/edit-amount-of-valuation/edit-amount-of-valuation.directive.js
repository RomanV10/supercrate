'use strict';

import './edit-amount-of-valuation.styl';
import {
	ACTIONS
} from '../../../../shared/valuation-shared/helper/events';

angular
	.module('moveboard-valuation')
	.directive('editAmountOfValuation', editAmountOfValuation);

editAmountOfValuation.$inject = [];

function editAmountOfValuation () {
	return {
		restrict: 'E',
		template: require('./edit-amount-of-valuation.tpl.html'),
		scope: {},
		link: editAmountOfValuationLink,
		controller: 'ValuationBaseCRUDController',
		controllerAs: 'vm',
	};
	
	function editAmountOfValuationLink ($scope, element, attr, vm) {
		$scope.directivePresets = vm.init({
			setting_name: 'edit_amount_of_valuation',
			optimisticText: 'Setting was successfully updated',
			errorText: 'Setting was not updated',
			changes: false,
			setting: {},
			index: 3,
		});
		
		$scope.$watch('directivePresets', (current) => {
			if (current.changes) $scope.$emit(ACTIONS.childrenHasChangesValuationAccountShowSettings);
			$scope.$emit(ACTIONS.showValuationProposalToContact, !current.setting);
		}, true);
	}
}
