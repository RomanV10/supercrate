describe('edit-amount-of-valuation.directive.js:', () => {
	let rootScope,
		element,
		$scope;
	
	beforeEach(() => {
		rootScope = $rootScope.$new();
		let directive = '<edit-amount-of-valuation></edit-amount-of-valuation>';
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	});
	
	describe('on init', () => {
		it('$scope.directivePresets have to be defined', () => {
			expect($scope.directivePresets).toBeDefined();
		});
	});
});
