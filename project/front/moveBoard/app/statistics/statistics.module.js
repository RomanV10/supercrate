(function () {
    'use strict';

    angular.module('app.statistics', ['tc.chartjs', 'app.lddispatch', 'paymentsCollected']);
})();
