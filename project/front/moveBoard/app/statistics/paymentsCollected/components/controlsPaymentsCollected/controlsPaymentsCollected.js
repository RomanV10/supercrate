'use strict';

class ControlsPaymentsCollected {
	constructor(PaymentsCollectedFactory, PaymentsCollectedValue, moment) {
		this.PaymentsCollectedFactory = PaymentsCollectedFactory;
		this.PaymentsCollectedValue = PaymentsCollectedValue;
		this.moment = moment;
		this.prevPayment, this.prevAdvanced, this.prevCardType = [], this.prevPaymentFlag = [];
		this.selectAllType = true;
		this.selectAllAdditionalFilters = true;
		this.showCreditCardTypes = false;
		this.showAdvancedFilters = false;
		this.additionalChoosedFilters = [];
		this.hasInvoice = true;
		this.hasApplyFilter = false;
		this.none = this.PaymentsCollectedValue.none;
		this.setDate();
		this.prevDate = angular.copy(this.date);
		this.paymentTypesFilters = this.PaymentsCollectedValue.paymentsType;
		this.creditCardTypes = Object.keys(this.PaymentsCollectedFactory.enums.credit_card);
		this.pickUpCreditCards = this.creditCardTypes;

		this.advancedFilters = angular.copy(this.PaymentsCollectedValue.advancedFilters);
		this.additionalFilters = angular.copy(this.PaymentsCollectedValue.additionalAdvancedFilters);
	}

	async applyFilters() {
		this.clearFilterData();

		if(this.date.from && this.date.to) {
			this.paymentsFilter.filters.created = {
				from: this.moment(this.date.from).startOf('day').unix(),
				to: this.moment(this.date.to).endOf('day').unix()
			};
		} else {
			this.paymentsFilter.filters.created = this.PaymentsCollectedFactory.createMonthDateRange();
		}

		if(this.pickUpPaymentFilters) {
			let index = _.findIndex(this.paymentTypesFilters, (payment) => payment == this.pickUpPaymentFilters);
			if(~index) {
				this.paymentsFilter.filters.payment_method = index + 1;
			}
		}

		if(this.pickUpCreditCards.length && this.paymentsFilter.filters.payment_method === this.PaymentsCollectedValue.creditCardCode) {
			this.paymentsFilter.filters.card_type = [];
			let index;

			_.forEach(this.pickUpCreditCards, (card) => {
				index = _.findIndex(Object.keys(this.PaymentsCollectedFactory.enums.credit_card), (enumCard) => card === enumCard);
				this.paymentsFilter.filters.card_type.push(index+1);
			});
		}

		if(this.advancedChoosedFilters) {
			this.paymentsFilter.filters.role = [];
			this.paymentsFilter.filters.payment_flag = [];
			this.paymentsFilter.filters.entity_type = [];

			this.addFilter(this.advancedChoosedFilters);
		}

		if(this.additionalChoosedFilters) {
			if(!this.paymentsFilter.filters.payment_flag) {
				this.paymentsFilter.filters.payment_flag = [];
			}

			_.forEach(this.additionalChoosedFilters, (addFilter) => {
				this.addFilter(addFilter);
			});
		}

		if(!_.get(this.paymentsFilter.filters, 'role.length', false)) {
			delete this.paymentsFilter.filters.role;
		}
		if(!_.get(this.paymentsFilter.filters, 'payment_flag.length', false)) {
			delete this.paymentsFilter.filters.payment_flag;
		}
		if(!_.get(this.paymentsFilter.filters, 'entity_type.length', false)) {
			delete this.paymentsFilter.filters.entity_type;
		}

		for(let [key, value] of Object.entries(this.column)) {
			if(value != '') {
				this.paymentsFilter.sort = {
					field: key,
					direction: value
				};
			}
		}

		this.paymentsFilter.page = 0;
		this.hasApplyFilter = false;

		this.getData(this.paymentsFilter);
	}

	checkReservationOnAdd() {
		if(_.isUndefined(_.find(this.paymentsFilter.filters.payment_flag, (flag) => flag == this.PaymentsCollectedValue.reservationStatus))) {
			this.paymentsFilter.filters.payment_flag.push(this.PaymentsCollectedFactory.enums.payment_flag['RESERVATION']);
		}
	}

	async removeFilters() {
		delete this.pickUpPaymentFilters;
		delete this.prevAdvancedFilters;
		delete this.advancedChoosedFilters;
		this.showCreditCardTypes = false;
		this.pickUpCreditCards = [];
		this.paymentsFilter.filters.created = this.PaymentsCollectedFactory.createMonthDateRange();
		this.setDate();
		this.prevDate = angular.copy(this.date);
		this.additionalChoosedFilters = [];
		this.prevPayment = undefined;
		this.prevAdvanced = undefined;
		this.prevCardType = [];
		this.prevPaymentFlag = [];
		this.checkContractType(false);
		this.resetSort();
		this.applyFilters();
	}

	checkCreditType() {
		if(this.pickUpPaymentFilters === 'Credit card') {
			this.showCreditCardTypes = true;
		} else {
			this.showCreditCardTypes = false;
			this.pickUpCreditCards = [];
		}

		if(this.pickUpPaymentFilters !== 'None') {
			this.checkChanges('payment');
			this.prevPayment = this.pickUpPaymentFilters;
		}
	}

	selectDeselect({flagName, nameOption, countName, defaultName}) {
		this[flagName] = !this[flagName];
		if(this[nameOption].length >= this.PaymentsCollectedValue[countName]) {
			this[nameOption] = [];
		} else {
			this[nameOption] = this[defaultName];
		}
	}

	checkStatusValue({choosedName, prevName, changeType}) {
		if(this[choosedName].length >= 1) {
			let index = _.findIndex(this[choosedName], (option) => option === 'All' || option === 'None');
			if(~index) {
				this[choosedName].splice(index, 1);
			}
		}

		this.checkChanges(changeType);
		this[prevName] = angular.copy(this[choosedName]);
	}

	clearFilterData() {
		this.paymentsFilter.filters = {
			payment_method: undefined,
			card_type: undefined,
			payment_flag: undefined,
			entity_id: undefined,
			pending: undefined,
			refunds: undefined,
			role: undefined,
			entity_type: undefined
		};

		this.paymentsFilter.sort = {
			direction: undefined,
			field: undefined
		};
	}

	setDate() {
		this.prevDate = angular.copy(this.date);
		this.date = {
			from: new Date(this.paymentsFilter.filters.created.from * 1000),
			to: new Date(this.paymentsFilter.filters.created.to * 1000)
		};
	}

	closeAdvancedFilter() {
		angular.element('md-backdrop').trigger('click');
	}

	addFilter(params) {
		switch(params) {
			case 'Reservation by customer':
				this.checkReservationOnAdd();
				this.paymentsFilter.filters.role.push(this.PaymentsCollectedFactory.enums.roles['CUSTOMER']);
				break;
			case 'Reservation by company':
				this.checkReservationOnAdd();
				this.paymentsFilter.filters.role.push(this.PaymentsCollectedFactory.enums.roles['ADMINISTRATOR']);
				this.paymentsFilter.filters.role.push(this.PaymentsCollectedFactory.enums.roles['CUSTOMER_SERVICE']);
				this.paymentsFilter.filters.role.push(this.PaymentsCollectedFactory.enums.roles['DRIVER']);
				this.paymentsFilter.filters.role.push(this.PaymentsCollectedFactory.enums.roles['FOREMAN']);
				this.paymentsFilter.filters.role.push(this.PaymentsCollectedFactory.enums.roles['HELPER']);
				this.paymentsFilter.filters.role.push(this.PaymentsCollectedFactory.enums.roles['MANAGER']);
				this.paymentsFilter.filters.role.push(this.PaymentsCollectedFactory.enums.roles['OWNER']);
				this.paymentsFilter.filters.role.push(this.PaymentsCollectedFactory.enums.roles['SALES']);
				break;
			case 'Contract':
				this.paymentsFilter.filters.payment_flag.push(this.PaymentsCollectedFactory.enums.payment_flag['CONTRACT']);
				break;
			case 'Carrier and Agent payment':
				this.paymentsFilter.filters.entity_type.push(this.PaymentsCollectedValue.carrierAgentStatus);
				break;
			case 'Storage Tenant':
				this.paymentsFilter.filters.entity_type.push(this.PaymentsCollectedFactory.enums.entities['STORAGEREQUEST']);
				break;
			case 'Invoices':
				this.paymentsFilter.filters.payment_flag.push(this.PaymentsCollectedFactory.enums.payment_flag['INVOICE']);
				break;
			case 'Pending':
				this.paymentsFilter.filters.pending = this.PaymentsCollectedValue.pendingRefundsStatus;
				break;
			case 'Refund':
				this.paymentsFilter.filters.refunds = this.PaymentsCollectedValue.pendingRefundsStatus;
				break;
			case 'Custom receipt':
				this.paymentsFilter.filters.payment_flag.push(this.PaymentsCollectedFactory.enums.payment_flag['REGULAR']);
				break;
			default:
				break;
		}
	}

	checkContractType(checkStatus) {
		let index;
		if(this.advancedChoosedFilters === 'Contract') {
			index = _.findIndex(this.additionalFilters, (addFilter) => addFilter === 'Invoices');
			if(~index) {
				this.additionalFilters.splice(index, 1);
			}
			index = _.findIndex(this.additionalChoosedFilters, (choosedAddFilter) => choosedAddFilter === 'Invoices');
			if(~index) {
				this.additionalChoosedFilters.splice(index, 1);
			}
			if(this.additionalChoosedFilters.length === this.PaymentsCollectedValue.countAddAdvancedMin) {
				this.selectAllAdditionalFilters = false;
			}

			this.hasInvoice = false;
		} else {
			index = _.findIndex(this.additionalFilters, (addFilter) => addFilter === 'Invoices');
			if(!~index) {
				this.additionalFilters.push('Invoices');
				this.additionalFilters.sort();
			}

			this.selectAllAdditionalFilters = true;
			this.hasInvoice = true;
		}

		if(checkStatus && this.advancedChoosedFilters !=='None') {
			this.checkChanges('advancedFilter');
			this.prevAdvanced = angular.copy(this.advancedChoosedFilters);
		}
	}

	checkChanges(params) {
		switch(params) {
			case 'dateFrom':
				this.hasApplyFilter = this.prevDate.from.toString() != this.date.from.toString();
				break;
			case 'dateTo':
				this.hasApplyFilter = this.prevDate.to.toString() != this.date.to.toString();
				break;
			case 'payment':
				this.hasApplyFilter = this.prevPayment != this.pickUpPaymentFilters;
				break;
			case 'cartType':
				this.hasApplyFilter = this.prevCardType.toString() != this.pickUpCreditCards.toString();
				break;
			case 'advancedFilter':
				this.hasApplyFilter = this.prevAdvanced != this.advancedChoosedFilters;
				break;
			case 'paymentFlag':
				this.hasApplyFilter = this.prevPaymentFlag.toString() != this.additionalChoosedFilters.toString();
				break;
			default:
				break;
		}
	}

	clearFilter({type, prevName, checkParams}) {
		delete this[type];
		this.checkChanges(checkParams);
		this[prevName] = undefined;
	}
}

angular
	.module('paymentsCollected')
	.component('controlsPaymentsCollected', {
		template: require('./controlsPaymentsCollected.html'),
		controller: ControlsPaymentsCollected,
		controllerAs: '$ctrl',
		bindings: {
			paymentsData: '=',
			paymentsFilter: '=',
			column: '<',
			getData: '&',
			resetSort: '&'
		}
	});

ControlsPaymentsCollected.$inject = ['PaymentsCollectedFactory', 'PaymentsCollectedValue', 'moment'];
