'use strict';

class TablePaymentsCollected {
	constructor(PaymentServices, SortingObj, PaymentsCollectedValue, StorageService, $state, SweetAlert) {
		this.PaymentsCollectedValue = PaymentsCollectedValue;
		this.PaymentServices = PaymentServices;
		this.SortingObj = SortingObj;
		this.StorageService = StorageService;
		this.$state = $state;
		this.SweetAlert = SweetAlert;
		this.sit = PaymentsCollectedValue.carrierAgentStatus;
		this.storage = PaymentsCollectedValue.storageRequestStatus;
		this.paymentsType = PaymentsCollectedValue.paymentsType;
	}

	async prevPage() {
		let prevPage = this.paymentsData.meta.currentPage - 1;
		if(prevPage > -1) {
			this.paymentsFilter.page = prevPage;
			this.getData(this.paymentsFilter);
		}
	}

	async nextPage() {
		if(this.paymentsData.meta.pageCount != (this.paymentsData.meta.currentPage + 1)) {
			this.paymentsFilter.page = this.paymentsData.meta.currentPage + 1;
			this.getData(this.paymentsFilter);
		}
	}

	totalFrom() {
		return this.paymentsData.meta.currentPage * this.paymentsData.meta.perPage + 1;
	}

	totalTo() {
		return (this.paymentsData.meta.currentPage + 1) * this.paymentsData.meta.perPage;
	}


	openReceits(index, event) {
		if(!angular.element(event.target).hasClass('td-order')) {
			let receipt = this.paymentsData.items[index];
			if(receipt.transaction_id === "") {
				receipt.transactionDefault = this.PaymentsCollectedValue.transactionDefault;
			}

			this.PaymentServices.openReceiptModal({}, receipt);
		}
	}

	 async openStorageRequest(index) {
		let requestEntity = this.paymentsData.items[index].entity_id;
		try {
			let reqData = await this.StorageService.getReqStorage(requestEntity);
			let req = await this.StorageService.getStorageReqByID(requestEntity);
			this.StorageService.openModal(req.data, requestEntity, reqData.data);
		} catch(err) {
			this.SweetAlert.swal({title: 'Error!', text: 'Error of getting data for opening storage request', type: 'error'});
		}
	};

	sort(field) {
		this.paymentsFilter.sort.field = field;
		this.paymentsFilter.sort.direction = this.changeSorting(field);
		this.getData(this.paymentsFilter);
	}

	changeSorting(field) {
		let value = this.column[field];
		this.resetSort();

		switch(value) {
			case 'ASC':
				this.column[field] = 'DESC';
				return 'DESC';
			default:
				this.column[field] = 'ASC';
				return 'ASC';
		}
	}

	goToTripPlanner(id) {
		this.$state.go('lddispatch.tripDetails', {id: id, tab: 0});
	}
}

angular
	.module('paymentsCollected')
	.component('tablePaymentsCollected', {
		template: require('./tablePaymentsCollected.html'),
		controller: TablePaymentsCollected,
		controllerAs: '$ctrl',
		bindings: {
			paymentsData: '=',
			paymentsFilter: '=',
			column: '<',
			getData: '&',
			resetSort: '&'
		}
	});

TablePaymentsCollected.$inject = ['PaymentServices', 'SortingObj', 'PaymentsCollectedValue', 'StorageService', '$state', 'SweetAlert'];
