'use strict';

angular.module('paymentsCollected')
	.factory('PaymentsCollectedFactory', PaymentsCollectedFactory);

PaymentsCollectedFactory.$inject = ['PaymentsCollectedValue', '$filter', 'datacontext', 'moment'];

function PaymentsCollectedFactory(PaymentsCollectedValue, $filter, datacontext, moment) {
	let service = {};

	let fieldData = datacontext.getFieldData();
	let enums = angular.fromJson(fieldData.enums);

	service.enums = enums;

	service.formatPaymentsCollected = formatPaymentsCollected;
	service.createMonthDateRange = createMonthDateRange;

	function formatPaymentsCollected(data) {
		let payments = data.items;

		_.forEach(payments, (payment) => {
			payment.payment_method = PaymentsCollectedValue.paymentsType[payment.payment_method - 1];
			payment.created = $filter('date')(payment.created * 1000, PaymentsCollectedValue.dateformat);
			payment.paymentBy = getPaymentByValue(payment);
			payment.payment_flag = getType(payment.payment_flag);
		});

		data.items = payments;

		return data;
	}

	function createMonthDateRange() {
		return {
			from: moment().startOf('month').unix(),
			to: moment().endOf('month').unix()
		};
	}

	function getPaymentByValue(payment) {
		let result = [];

		if(payment.payment_flag == service.enums.payment_flag['REGULAR']) {
			result.push(' Сustom receipt');
		}
		if(payment.pending === '1') {
			result.push(' Pending');
		}
		if(payment.refunds == PaymentsCollectedValue.pendingRefundsStatus) {
			result.push(' Refund');
		}
		if(payment.payment_flag == service.enums.payment_flag['RESERVATION']) {
			if(payment.role == service.enums.roles['CUSTOMER']) {
				result.push(' Reservation by customer');
			} else {
				result.push(' Reservation by company');
			}
		}
		if(payment.payment_flag == service.enums.payment_flag['CONTRACT']) {
			result.push(' Contract');
		}
		if(payment.entity_type == PaymentsCollectedValue.carrierAgentStatus) {
			result.push(' Сarrier and Agient payment');
		}
		if(payment.entity_type == service.enums.entities['STORAGEREQUEST']) {
			result.push(' Storage Tenant');
		}
		if(payment.payment_flag == service.enums.payment_flag['INVOICE']) {
			result.push(' Invoices');
		}

		return result.toString();
	}

	function getType(type) {
		return _.invert(service.enums.payment_flag)[type].toLowerCase();
	}

	return service;
}
