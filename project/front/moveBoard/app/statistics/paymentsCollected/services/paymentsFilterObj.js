'use strict';

angular.module('paymentsCollected')
	.service('PaymentsFilterObj', [ 'PaymentsCollectedFactory', function(PaymentsCollectedFactory) {
		this.filter = {
			page: undefined,
			filters: {
				created: PaymentsCollectedFactory.createMonthDateRange(),
				payment_method: undefined,
				card_type: undefined,
				payment_flag: undefined,
				entity_id: undefined,
				pending: undefined,
				refunds: undefined,
				role: undefined,
				entity_type: undefined
			},
			sort: {
				direction: undefined,
				field: undefined
			}
		};

	}]);
