'use strict';

angular.module('paymentsCollected')
	.factory('PaymentsHttpFactory', PaymentsHttpFactory);

PaymentsHttpFactory.$inject = ['apiService', 'moveBoardApi', 'PaymentsCollectedFactory'];

function PaymentsHttpFactory(apiService, moveBoardApi, PaymentsCollectedFactory) {
	return {
		collectedPaymentByFilter(params) {
			return apiService.postData(moveBoardApi.paymentCollected.getCollectedPaymentByFilter, params)
				.then(({data}) => {
					return PaymentsCollectedFactory.formatPaymentsCollected(data);
				});
		}
	}
}
