'use strict';

angular.module('paymentsCollected')
	.value('PaymentsCollectedValue', {
		advancedFilters: [
						  'Reservation by customer',
						  'Reservation by company',
						  'Contract',
						  'Carrier and Agent payment',
						  'Storage Tenant',
						  'Custom receipt'],
		additionalAdvancedFilters : [
			'Invoices',
			'Pending',
			'Refund'
		],
		paymentsType: [
			'Credit card',
			'Cash',
			'Check',
			'Custom'
		],
		countOptionsCardTypes: 4,
		creditCardCode: 1,
		carrierAgentStatus: 7,
		storageRequestStatus: 1,
		pendingRefundsStatus: 1,
		reservationStatus: 2,
		countAddAdvancedBase: 3,
		countAddAdvancedMin: 2,
		transactionDefault: 'Custom Payment',
		none: 'None',
		dateformat: 'MM-dd-yyyy'
});
