'use strict';

angular.module('paymentsCollected')
	.service('SortingObj', [ function () {
		this.column = {
			entity_id: '',
			customer_name: '',
			payment_method: '',
			invoice_id: '',
			transaction_id: '',
			created: '',
			amount: '',
			pending: '',
			description: ''
		};

	}]);
