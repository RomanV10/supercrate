'use strict';

angular.module('paymentsCollected')
	.controller('PaymentsCollectedCtrl', PaymentsCollectedCtrl);

PaymentsCollectedCtrl.$inject = ['$scope', 'PaymentsHttpFactory', 'PaymentsFilterObj', 'SweetAlert', 'SortingObj'];

function PaymentsCollectedCtrl($scope, PaymentsHttpFactory, PaymentsFilterObj, SweetAlert, SortingObj) {
	$scope.column = angular.copy(SortingObj.column);
	$scope.paymentsFilter = angular.copy(PaymentsFilterObj.filter);

	$scope.getData = async function(objFilters) {
		let filter = !_.get(objFilters, 'filters', false) ? $scope.paymentsFilter : objFilters;
		$scope.paymentsFilter.page = 0;
		try {
			$scope.busy = true;
			let res = await PaymentsHttpFactory.collectedPaymentByFilter(filter);
			if($scope.paymentsData) {
				$scope.paymentsData = Object.assign($scope.paymentsData, res);
			} else {
				$scope.paymentsData = res;
			}
			$scope.getActualTotal(res.sum, filter.filters);
		} catch(err) {
			SweetAlert.swal({title: 'Error!', text: 'Error of getting payments collected', type: 'error'});
		} finally {
			$scope.busy = false;
		}
	};

	$scope.getData();

	$scope.resetSort = function() {
		$scope.column = Object.assign($scope.column, angular.copy(SortingObj.column));
	};

	$scope.getActualTotal = (payment, filter) => {
		$scope.totalPayments = 0;
		if (filter.pending && filter.refunds && !filter.payment_flag) {
			$scope.totalPayments = _.head(payment.pending_refunds);
		} else if (filter.pending) {
			$scope.totalPayments = _.head(payment.pending);
		} else if (filter.refunds) {
			$scope.totalPayments = _.head(payment.refunds);
		} else {
			$scope.totalPayments = payment.total || 0;
		}
	}
}
