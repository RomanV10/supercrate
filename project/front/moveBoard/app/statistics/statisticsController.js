'use strict';

angular
	.module('app.statistics')
	.controller('Statistics', StatisticsCtrl);

StatisticsCtrl.$inject = ['$rootScope', '$timeout'];

function StatisticsCtrl($rootScope, $timeout) {
	let vm = this;
	const THIRTY_SECONDS = 30000;

	init();

	function init () {
		getRole();

		$timeout(function () {
			init();
		}, THIRTY_SECONDS);
	}

	vm.access_statistics = getRole();

	function getRole() {
		vm.role = _.get($rootScope, 'currentUser.userRole[1]');
		let accessRoles = ['sales', 'customer service', 'manager'];

		return !!~accessRoles.indexOf(vm.role);
	}
}
