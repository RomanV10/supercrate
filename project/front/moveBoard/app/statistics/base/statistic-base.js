export const STATISTICS_FIELDS = {
	1: 'get_count_entrance_from',
	2: 'count_move_request_type_booked_all',
	3: 'get_count_assigned_booked_all',
	4: 'count_request_size_of_move_all',
	5: 'statistics_source_all'
};

export const BASE_TOTAL_OBJ = {
	from: 0,
	to: 0,
	conversion: 0
};

export const BASE_ENTRANCE_OBJ = {
	from: 0,
	to: 0
};

export const STATISTIC_TABLE_HEADER = [
	{
		title: 'Assign',
		name: 'Manager',
		from: 'Amount',
		to: 'Booked',
		conversion: 'Conversion',
	},
	{
		title: 'Service type',
		name: 'Type',
		from: 'Amount',
		to: 'Booked',
		conversion: 'Conversion',
	},
	{
		title: 'Size of move',
		name: 'Size',
		from: 'Amount',
		to: 'Booked',
		conversion: 'Conversion',
	},
	{
		title: 'Source',
		name: 'Source type',
		from: 'Amount',
		to: 'Booked',
		conversion: 'Conversion',
	},
	{
		title: 'Entrance',
		name: 'Entrance type',
		from: 'From',
		to: 'To',
	},
];