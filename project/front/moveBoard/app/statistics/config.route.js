angular.module('app')
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			.state('statistics', {
				url: '/statistics',
				abstract: true,
				template: '<ui-view/>',
				data: {
					permissions: {
						except: ['anonymous', 'foreman', 'helper']
					}
				}
			})
			.state('statistics.byrole', {
				url: '/statistics',
				template: require('./statistics.html'),
				title: 'Statistics',
				data: {
					permissions: {
						except: ['anonymous', 'foreman', 'helper']
					}
				}
			})
			.state('statistics.reviews', {
				url: '/reviews',
				template: require('./directives/template/reviews.html'),
				title: 'Reviews',
				data: {
					permissions: {
						except: ['anonymous', 'foreman', 'helper']
					}
				}
			})
			.state('statistics.ProfitLose', {
				url: '/profitloss',
				template: require('./profit-and-lose/other-templates/profitLose.html'),
				title: 'Profit and Loss',
				data: {
					permissions: {
						except: ['anonymous', 'foreman', 'helper']
					}
				}
			})
			.state('statistics.invoices', {
				url: '/invoices',
				template: require('./invoices/invoices.tpl.html'),
				title: 'Invoices',
				data: {
					permissions: {
						except: ['anonymous', 'foreman', 'helper']
					}
				}
			})
			.state('statistics.paymentsCollected', {
				url: '/payments_collected',
				template: require('./paymentsCollected/paymentsCollected.html'),
				title: 'PaymentsCollected',
				data: {
					permissions: {
						except: ['anonymous', 'foreman', 'helper']
					}
				}
			});
	}]);
