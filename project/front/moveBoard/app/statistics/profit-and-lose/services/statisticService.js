'use strict';

angular
	.module('app.statistics')
	.factory('StatisticService', StatisticService);

StatisticService.$inject = ['$q', '$http', 'config', '$uibModal', 'SweetAlert', 'apiService', 'moveBoardApi', 'Raven'];

function StatisticService($q, $http, config, $uibModal, SweetAlert, apiService, moveBoardApi, Raven) {

	const STATUS_OK = 200;

	return {
		createExpense: createExpense,
		addNewExpense: addNewExpense,
		deleteLosses: deleteLosses,
		editLosses: editLosses,
		getLosses: getLosses,
		viewExpense: viewExpense,
		getProfitAndLossData: getProfitAndLossData
	};

	function getProfitAndLossData(data) {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.profitAndLoss.getAllInfo, data).then(resolve => {
			if (resolve.status === 200) {
				defer.resolve(resolve.data);
			} else {
				defer.reject();
			}
		});

		return defer.promise;
	}

	function getLosses(id) {
		var deferred = $q.defer();
		$http.get(config.serverUrl + 'server/profit_losses/' + id)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function deleteLosses(id) {
		var deferred = $q.defer();
		$http.delete(config.serverUrl + 'server/profit_losses/' + id)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function editLosses(id, lose) {
		var deferred = $q.defer();
		var data = {
			data: lose
		};
		$http.put(config.serverUrl + 'server/profit_losses/' + id, data)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function addNewExpense(data) {
		let deferred = $q.defer();

		apiService.postData(moveBoardApi.profitAndLoss.addNewExpense, data)
			.then(resolve => {
				if (resolve.status === STATUS_OK) deferred.resolve(_.head(resolve.data));
				else deferred.reject();
			})
			.catch(error => {
				Raven.captureException(`statisticService.js in addNewExpense: ${error}`);
				deferred.reject();
			});

		return deferred.promise;
	}

	function createExpense() {

		var modalInstance = $uibModal.open({
			template: require('../other-templates/createExpense.html'),
			controller: 'ExpenseModalInstanceCtrl',
			size: 'md',
			resolve: {}
		});
		modalInstance.result.then(function ($scope) {
		});
	}

	function viewExpense(expense) {

		var modalInstance = $uibModal.open({
			template: require('../other-templates/expense.html'),
			controller: 'ViewExpenseModalInstanceCtrl',
			size: 'md',
			resolve: {
				expense: function () {
					return expense;
				}
			}
		});
		modalInstance.result.then(function ($scope) {
		});
	}
}
