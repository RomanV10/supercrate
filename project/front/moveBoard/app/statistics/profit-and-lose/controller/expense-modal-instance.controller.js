'use strict';

angular
	.module('app.statistics')
	.controller('ExpenseModalInstanceCtrl', ExpenseModalInstanceCtrl);

ExpenseModalInstanceCtrl.$inject = ['$scope', 'StatisticService', '$uibModalInstance', '$rootScope', 'SweetAlert'];

function ExpenseModalInstanceCtrl($scope, StatisticService, $uibModalInstance, $rootScope, SweetAlert) {
	$scope.expense = {
		amount: 0,
		recurring: false,
		forever: true,
		recurring_to: '',
		date: new Date(),
		frequency: '',
		category: '',
		note: ''
	};
	$scope.frequencyOpt = [
		{name: 'Daily'},
		{name: 'Weekly'},
		{name: 'Monthly'}
	];
	$scope.categoryOpt = [
		{name: 'Salary'},
		{name: 'Trucks'},
		{name: 'Office'},
		{name: 'Tolls'},
		{name: 'Misc'}
	];
	$scope.setDateToRecurring = setDateToRecurring;

	function setDateToRecurring() {
		if ($scope.expense.forever) $scope.expense.recurring_to = 0;
		else $scope.expense.recurring_to = moment();
	}

	$scope.saveExpense = function () {
		if($scope.createExpense.$invalid) return false;
		SweetAlert.swal({
			title: 'Are you sure?',
			text: 'Create this expense?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes!',
			closeOnConfirm: false
		}, (confirm) => createExpense(confirm));
	};
	
	function createExpense (confirm) {
		if (!confirm) return;
		
		$scope.busy = true;
		let data = {
			amount: $scope.expense.amount,
			date: moment($scope.expense.date).format('YYYY-MM-DD'),
			data: {}
		};
		data.recurring = 0;
		if ($scope.expense.frequency)
			data.data.frequency = $scope.expense.frequency;
		if ($scope.expense.recurring) {
			data.recurring = 1;
			if ($scope.expense.frequency != '')
				data.data.frequency = $scope.expense.frequency;
		}
		if ($scope.expense.recurring_to) {
			data.recurring_to = moment($scope.expense.recurring_to).format('YYYY-MM-DD');
		}
		if ($scope.expense.notes)
			data.data.notes = $scope.expense.notes;
		if ($scope.expense.category)
			data.data.category = $scope.expense.category;
		StatisticService.addNewExpense(data)
			.then(() => {
				$scope.busy = false;
				$scope.cancel();
				$rootScope.$broadcast('initProfitAndLossData');
				SweetAlert.swal('Expense has been created', ' ', 'success');
			});
	}


	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

}
