'use strict';

angular
	.module('app.statistics')
	.controller('ViewExpenseModalInstanceCtrl', ViewExpenseModalInstanceCtrl);

ViewExpenseModalInstanceCtrl.$inject = ['$scope', 'StatisticService', '$uibModalInstance', 'expense', '$rootScope', 'SweetAlert'];

function ViewExpenseModalInstanceCtrl($scope, StatisticService, $uibModalInstance, expense, $rootScope, SweetAlert) {
	$scope.expense =  angular.copy(expense);
	delete $scope.expense.date_value;
	$scope.expense.date = moment.utc(moment.unix($scope.expense.date)).toDate();
	if ($scope.expense.recurring_to && $scope.expense.recurring_to != 0) {
		$scope.expense.forever = false;
		$scope.expense.recurring_to = moment.utc(moment.unix($scope.expense.recurring_to)).toDate();
	} else {
		$scope.expense.forever = true;
	}
	$scope.frequencyOpt = [
		{name: "Daily"},
		{name: "Weekly"},
		{name: "Monthly"}
	];
	$scope.categoryOpt = [
		{name: "Salary"},
		{name: "Trucks"},
		{name: "Office"},
		{name: "Tolls"},
		{name: "Misc"}
	];

	$scope.setDateToRecurring = setDateToRecurring;

	function setDateToRecurring() {
		if ($scope.expense.forever) {
			$scope.expense.recurring_to = 0;
		}
		else $scope.expense.recurring_to = moment().format('MMM D, YYYY');
	}

	$scope.saveExpense = function () {
		swal({
				title: "Are you sure?",
				text: "Change this expense?",
				type: "warning", showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes!",
				closeOnConfirm: false
			},
			function () {
				$scope.busy = true;
				var data = {
					amount: $scope.expense.amount,
					date: moment($scope.expense.date).format('YYYY-MM-DD'),
					data: {}
				};
				data.recurring = 0;
				if ($scope.expense.data.frequency)
					data.data.frequency = $scope.expense.data.frequency;
				if ($scope.expense.recurring) {
					data.recurring = 1;
					if ($scope.expense.data.frequency)
						data.data.frequency = $scope.expense.data.frequency;
				}
				if (!$scope.expense.forever && $scope.expense.recurring && $scope.expense.recurring_to != 0) {
					data.recurring_to = moment($scope.expense.recurring_to).format('YYYY-MM-DD');
				}
				if ($scope.expense.data.notes)
					data.data.notes = $scope.expense.data.notes;
				if ($scope.expense.data.category)
					data.data.category = $scope.expense.data.category;
				StatisticService.editLosses($scope.expense.id, data).then(function (data) {
					if ($scope.expense.parent_id) {
						StatisticService.getLosses($scope.expense.parent_id).then(function (parent) {
							if ($scope.expense.recurring) {
								parent.recurring = 1;
							} else {
								parent.recurring = 0;
							}
							StatisticService.editLosses($scope.expense.parent_id, parent).then(function (data) {
							})
						})
					}
					$scope.busy = false;
					_.assign(expense, $scope.expense);
					$scope.cancel();
					$rootScope.$broadcast('initProfitAndLossData');
					swal("Expense has been changed", " ", "success");
				})
			})
	}

	$scope.remove = function () {
		SweetAlert.swal({
				title: "Are you sure you want to remove this expense?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, let's do it!",
				cancelButtonText: "No, cancel pls!",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					$scope.busy = true;
					StatisticService.deleteLosses($scope.expense.id).then(function (data) {
						$scope.busy = false;
						$rootScope.$broadcast('getByDateProfit');
						$scope.cancel();
						toastr.success("Expense  removed");
					})
				}
			});
	}


	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

}
