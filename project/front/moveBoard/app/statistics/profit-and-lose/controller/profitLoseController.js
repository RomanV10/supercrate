'use strict';
angular

	.module('app.statistics')
	.controller('profitLoseController', profitLoseController);

profitLoseController.$inject = ['$scope', 'StatisticService', 'DTOptionsBuilder'];

function profitLoseController($scope, StatisticService, DTOptionsBuilder) {
	let vm = this;
	const SHORT_DATE_FORMAT = 'YYYY-MM-DD'; // => 2018-04-02
	const ROUND_UP = 2;
	const MAX_TABLE_LENGTH = 25;
	const DEFAULT_SORT = 0;
	const DEFAULT_PAGE = 1;
	let DEFAULT_SORTING = {
		direction: 'DESC',
		field: 'nid',
	};
	vm.dates = {
		from: moment().startOf('month').toDate(),
		to: moment().endOf('month').toDate(),
		maxDate: new Date(),
		options: {
			minDate: null,
			formatYear: 'yy',
			startingDay: 0
		}
	};

	vm.estimatiedProfitCount = 0;
	vm.actualProfitCount = 0;
	vm.losesCount = 0;
	vm.incomeCount = 0;

	vm.bDateChange = false;
	vm.dtOptions = DTOptionsBuilder.newOptions()
		.withOption('aaSorting', [[DEFAULT_SORT, 'desc']])
		.withOption('bInfo', false)
		.withDisplayLength(MAX_TABLE_LENGTH)
		.withOption('bPaginate', false);

	vm.busy = false;

	vm.initProfitAndLossData = initProfitAndLossData;
	vm.prevMonth = prevMonth;
	vm.nextMonth = nextMonth;
	vm.createExpense = createExpense;

	$scope.$on('request.updated', handlerUpdateRequest);

	function initProfitAndLossData(currentPage = DEFAULT_PAGE, sorting = DEFAULT_SORTING) {
		vm.busy = true;
		getProfitAndLossData({
			from: moment(vm.dates.from).format(SHORT_DATE_FORMAT),
			to: moment(vm.dates.to).format(SHORT_DATE_FORMAT),
			currentPage: currentPage,
			perPage: 20,
			sorting
		});
	}

	function getProfitAndLossData(params) {
		StatisticService.getProfitAndLossData({
			dateFrom: params.from,
			dateTo: params.to,
			currentPage: params.currentPage,
			perPage: params.perPage,
			sorting: params.sorting,
		})
			.then(response => {
				vm.profits = response.requests.items;
				vm.meta = response.requests.meta;
				vm.currentPage = response.requests.meta.currentPage;
				vm.estimatiedProfitCount = _.round(response['estimated_income_total'], ROUND_UP);
				vm.actualProfitCount = _.round(response['actual_income_total'], ROUND_UP);
				vm.losesCount = _.round(response['expenses_total'], ROUND_UP);
				vm.incomeCount = _.round(response['profit'], ROUND_UP);
				vm.sumPaycheck = _.round(response['sum_paycheck'], ROUND_UP);
				vm.expenses = response['expenses'];
				vm.busy = false;
			});
	}

	function createExpense() {
		StatisticService.createExpense();
	}

	function prevMonth() {
		vm.estimatiedProfitCount = 0;
		vm.actualProfitCount = 0;
		vm.losesCount = 0;
		vm.incomeCount = 0;
		let numMonth = moment(vm.dates.from).month();
		let year = moment(vm.dates.from).year();
		if (numMonth == 0) {
			year--;
		}
		vm.dates.from = moment().month(numMonth - 1).year(year).startOf('month').toDate();
		vm.dates.to = moment().month(numMonth - 1).year(year).endOf('month').toDate();
		initProfitAndLossData();
		vm.busy = true;
	}

	function nextMonth() {
		vm.estimatiedProfitCount = 0;
		vm.actualProfitCount = 0;
		vm.losesCount = 0;
		vm.incomeCount = 0;
		let numMonth = moment(vm.dates.from).month();
		let year = moment(vm.dates.from).year();
		if (numMonth == 11) {
			year++;
		}
		numMonth = moment(vm.dates.from).month();
		vm.dates.from = moment().month(numMonth + 1).year(year).startOf('month').toDate();
		vm.dates.to = moment().month(numMonth + 1).year(year).endOf('month').toDate();
		initProfitAndLossData();
		vm.busy = true;
	}

	function handlerUpdateRequest(event, data) {
		let nid = data.nid;
		vm.profits[nid] = angular.copy(data);
	}

	initProfitAndLossData();

	$scope.$on('initProfitAndLossData', () => {
		initProfitAndLossData();
	});
}
