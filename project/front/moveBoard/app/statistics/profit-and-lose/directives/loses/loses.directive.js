'use strict';

angular
	.module('move.requests')
	.directive('ccLoseDatatable', ccLoseDatatable);


ccLoseDatatable.$inject = ['DTDefaultOptions','datacontext', 'StatisticService', 'SweetAlert', 'SettingServices', '$location'];

function ccLoseDatatable (DTDefaultOptions,datacontext,StatisticService, SweetAlert, SettingServices,$location) {
	return  {
		scope: {
			'expenses': '=',
			'dateFrom': '=',
			'dateTo': '=',
			'sumPaycheck': '=',
			'initProfitAndLossData': '=',

		},
		link: linkFn,
		template: require('./lose.html'),
		restrict: 'AE'
	};

	function linkFn(scope) {

		DTDefaultOptions.setDisplayLength(20);
		scope.frequencyOpt = [
			{name: "Daily"},
			{name: "Weekly"},
			{name: "Monthly"}
		];
		scope.frequencyOptRecurring = [ "day", "week","month"];
		scope.categoryOpt = [
			{name: "Salary"},
			{name: "Trucks"},
			{name: "Office"},
			{name: "Tolls"},
			{name: "Misc"}
		];
		scope.clicked = '';
		scope.fieldData = datacontext.getFieldData();
		if(angular.isDefined(scope.fieldData.basicsettings)) {
			scope.basicSettings =  angular.fromJson(scope.fieldData.basicsettings);
		}

		scope.init = init;
		scope.openExpense = openExpense;
		scope.removeExpense = removeExpense;
		scope.openSalary = openSalary;
		scope.calculateNextRecurringDate = calculateNextRecurringDate;

		function init(){
			for(var i in scope.expenses){
				scope.expenses[i].date_value = moment.utc(moment.unix(scope.expenses[i].date)).format("MMM DD, YYYY");

				if(scope.expenses[i].recurring == "0") {
					scope.expenses[i].recurring = false;
				} else if(scope.expenses[i].recurring == "1") {
					scope.expenses[i].recurring = true;
				}
			}
		}

		init();

		function openExpense(expense){
			if(scope.clicked == expense.id){
				StatisticService.viewExpense(expense);
			}else{
				scope.clicked = expense.id;
			}
		}

		function removeExpense(expense){
			SweetAlert.swal({
					title: "Are you sure you want to remove this expense?",
					text: "",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",confirmButtonText: "Yes, let's do it!",
					cancelButtonText: "No, cancel pls!",
					closeOnConfirm: true,
					closeOnCancel: true },
				function(isConfirm){
					if (isConfirm) {
						scope.$parent.vm.busy = true;
						StatisticService.deleteLosses(expense.id).then(function(data){
							for(var i in scope.expenses){
								if(scope.expenses[i].id == expense.id){
									scope.expenses.splice(i, 1);
									scope.$parent.vm.busy = false;
									scope.initProfitAndLossData();
									toastr.success("Expense was removed");
								}
							}
						})
					}
				});
		}

		function openSalary(){
			if(scope.clicked == 'salary'){
				scope.$parent.vm.busy = true;
				var setting_name = 'basicsettings';
				if(angular.isUndefined(scope.basicSettings.payroll_time))
					scope.basicSettings.payroll_time = {};
				if(angular.isUndefined(scope.basicSettings.payroll_time.from))
					scope.basicSettings.payroll_time.from = '';
				if(angular.isUndefined(scope.basicSettings.payroll_time.to))
					scope.basicSettings.payroll_time.to ='';
				scope.basicSettings.payroll_time.from = moment(new Date(scope.dateFrom)).format('YYYY-MM-DD');
				scope.basicSettings.payroll_time.to = moment(new Date(scope.dateTo)).format('YYYY-MM-DD');
				//save time to payroll
				SettingServices.saveSettings(scope.basicSettings,setting_name).then(function(){
					scope.$parent.vm.busy = false;
					$location.path('dispatch/payroll');
				});
			}else{
				scope.clicked = 'salary';
			}
		}

		function calculateNextRecurringDate(expense) {
			return moment.utc(moment.unix(expense.date)).add(1,scope.frequencyOptRecurring[expense.data.frequency]).format('MMM DD, YYYY');
		}

	}

}
