'use strict';

angular
	.module('move.requests')
	.directive('ccProfitDatatable', ccProfitDatatable);

/* @ngInject */
function ccProfitDatatable(apiService, moveBoardApi, openRequestService) {
	return {
		scope: {
			'profits': '=',
			'dateFrom': '=',
			'dateTo': '=',
			'meta': '=',
			'currentPage': '=',
			'initProfitAndLossData': '=',
			'dtOptions': '=',
		},
		link: linkFn,
		template: require('./profit.html'),
		restrict: 'AE'
	};

	function linkFn($scope) {
		$scope.clicked = 0;
		const SORTING = ['DESC', 'ASC'];
		const DESCENDING = 0;
		const ASCENDING = 1;
		let defaultSorting = DESCENDING;

		$scope.requestEditModal = requestEditModal;
		$scope.getBookedDay = getBookedDay;
		$scope.exportAll = exportAll;
		$scope.changeSorting = changeSorting;

		apiService.postData(moveBoardApi.quickbooks.checkTokenDisconnectButton)
			.then(function (data) {
				$scope.showExportAllButton = _.head(data.data);
			});

		function requestEditModal(request) {
			if ($scope.clicked == request.nid) {
				openRequestService.open(request.nid);
			} else {
				$scope.clicked = request.nid;
			}
		}

		function getBookedDay(request) {
			if (!request) {
				return moment().format('M/D/YYYY');
			}

			return moment.unix(request.field_date_confirmed.value).format('M/D/YYYY');
		}

		function exportAll () {
			let filterDate = {
				date_filter_from: moment($scope.dateFrom).startOf('day').unix(),
				date_filter_to: moment($scope.dateTo).endOf('day').unix()
			};

			apiService.postData(moveBoardApi.quickbooks.exportAllQbo, filterDate)
				.then(() => {
					toastr.success('You started exporting requests to QuickBooks', 'Success');
				});
		}

		function changeSorting(field) {
			let direction = defaultSorting === DESCENDING ? SORTING[ASCENDING] : SORTING[DESCENDING];
			defaultSorting = Number(direction === SORTING[ASCENDING]);
			$scope.sorting = {
				field,
				direction,
			};
			$scope.initProfitAndLossData($scope.meta.currentPage, $scope.sorting);
		}
	}
}
