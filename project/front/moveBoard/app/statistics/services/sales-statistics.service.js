(function () {
	'use strict';
	
	angular
	.module('app.statistics')
	.factory('salesStatisticsService', salesStatisticsService);
	
	salesStatisticsService.$inject = ['apiService', 'moveBoardApi', '$q'];
	
	function salesStatisticsService(apiService, moveBoardApi, $q) {
		
		return {
			getStatisticsByRole: getStatisticsByRole,
			getSalesBySteps: getSalesBySteps,
			getCommonSalesStat: getCommonSalesStat,
			getCommonSalesStatMonthly: getCommonSalesStatMonthly
		};
		
		function getStatisticsByRole(from, to, user_id) {
			let defer = $q.defer();
			
			apiService.postData(moveBoardApi.statistics.statistics_by_sales_all, {
				datefrom: from,
				dateto: to,
				uid: user_id
			}).then(resolve => {
				defer.resolve(resolve.data);
			}, () => {
				defer.reject();
			});
			
			return defer.promise;
		}
		
		function getSalesBySteps(from, to, uid) {
			var dateRange = {
				datefrom: from,
				dateto: to
			};
			var statForSixMonth = {
				uid: uid
			}
			return {
				statistics_for_sales_by_day: apiService.postData(moveBoardApi.statistics.statistics_for_sales_by_day, dateRange),
				statistics_for_six_months: apiService.postData(moveBoardApi.statistics.statistics_for_six_months, statForSixMonth),
			}
		}
		function getCommonSalesStat(from, to, uid) {
			let deferred = $q.defer();
			
			apiService.postData(moveBoardApi.statistics.statistics_by_sales_interval, {
				datefrom: from,
				dateto: to,
				uid: uid
			}).then(resolve => {
				deferred.resolve(resolve.data);
			}, () => {
				deferred.reject();
			});
			
			return deferred.promise;
		}
		
		function getCommonSalesStatMonthly(from, to, uid) {
			let deferred = $q.defer();
			
			apiService.postData(moveBoardApi.statistics.statistics_by_sales_interval_month, {
				datefrom: from,
				dateto: to,
				uid: uid
			}).then(resolve => {
				deferred.resolve(resolve.data);
			}, () => {
				deferred.reject();
			});
			
			return deferred.promise;
		}
	}
})();