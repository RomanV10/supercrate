(function () {
    'use strict';

    angular.module('app.statistics').factory('ReviewService', ReviewService);

    ReviewService.$inject = ['apiService', 'moveBoardApi'];

    function ReviewService(apiService, moveBoardApi) {
        return {
            getAllReview: getAllReview,
            getSortedReviews: getSortedReviews,
            sortReviews: sortReviews
        };

        function getAllReview() {
            return {allReviews: apiService.getData(moveBoardApi.reviewMoveboard.getAllReviews)};
        }
        
        function getSortedReviews(date) {
            return {reviewsStatistics: apiService.postData(moveBoardApi.reviewMoveboard.getReviewStatistics, date)};
        }

        function sortReviews (sort, dir, page) {
            var data = {
                sort: sort,
                direction: dir,
                page: page
            };

            return {sortedReview: apiService.postData(moveBoardApi.reviewMoveboard.sortReview, data)};
        }
    }
})();