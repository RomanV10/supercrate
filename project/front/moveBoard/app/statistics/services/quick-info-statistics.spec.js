describe('Statistic: owner statistic service', () => {
	var quickStatisticsService;
	
	beforeEach(inject((_quickStatisticsService_) => {
		quickStatisticsService = _quickStatisticsService_;
	}));
	
	describe('percentFormatter', () => {
		describe('When should be 0', () => {
			it('null and undefined should be 0 conversion', () => {
				expect(quickStatisticsService.percentFormatter(null, undefined)).toBe(0);
			});
			it('10 and 0 should be 0 conversion', () => {
				expect(quickStatisticsService.percentFormatter(10, 0)).toBe(0);
			});
			it('0 and 50 should be 0 conversion', () => {
				expect(quickStatisticsService.percentFormatter(0, 50)).toBe(0);
			});
			it('null and 0 should be 0 conversion', () => {
				expect(quickStatisticsService.percentFormatter(null, 0)).toBe(0);
			});
			it('String and 70 should be 0 conversion', () => {
				expect(quickStatisticsService.percentFormatter('string', 70)).toBe(0);
			});
			it('0 and 0 should be 0 conversion', () => {
				expect(quickStatisticsService.percentFormatter(0, 0)).toBe(0);
			});
		});
		
		describe('When should not be 0', () => {
			it('should be 24', () => {
				expect(quickStatisticsService.percentFormatter(12, 50)).toBe(24);
			});
			it('should be 100', () => {
				expect(quickStatisticsService.percentFormatter(100, 100)).toBe(100);
			});
		});
	});
	
	describe('hexToRgba', () => {
		describe('When should be get RGBA color', () => {
			it('should be a rgba(52, 152, 219,1)', () => {
				expect(quickStatisticsService.hexToRgbA('#3498db', 1)).toBe('rgba(52,152,219,1)');
			});
			it('should be a rgba(231,76,60,0.5)', () => {
				expect(quickStatisticsService.hexToRgbA('#e74c3c', .5)).toBe('rgba(231,76,60,0.5)')
			})
		});
	})
});