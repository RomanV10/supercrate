(function () {
	'use strict';

	angular
	.module('app.statistics')
	.factory('quickStatisticsService', quickStatisticsService);

	quickStatisticsService.$inject = ['apiService', 'moveBoardApi', '$q'];

	function quickStatisticsService(apiService, moveBoardApi, $q) {
		return {
			periodUniqueMethod: periodUniqueMethod,
			getPayrollStatistic: getPayrollStatistic,
			getUniqueSiteVisitors: getUniqueSiteVisitors,
			getTodayStat: getTodayStat,
			getMonthly: getMonthly,
			getStatisticsByField: getStatisticsByField,
			hexToRgbA: hexToRgba,
			percentFormatter: percentFormatter,
			weekChartConstructor: weekChartConstructor,
			lineChartConstructor: lineChartConstructor,
			getLineChartOptions: getLineChartOptions,
			prepareStatisticTab: prepareStatisticTab,
			getFirstAssignDate: getFirstAssignDate
		};

		function getFirstAssignDate(uid) {
			let defer = $q.defer();

			apiService.postData(moveBoardApi.statistics.getFirstAssignDate, {
				uid: uid
			}).then(resolve => {
				defer.resolve(resolve.data[0]);
			}, () => {
				defer.reject();
			});

			return defer.promise;
		}

		function getTodayStat(date) {
			var dateRange = {
				date: date,
			};
			return {
				today_stat: apiService.postData(moveBoardApi.statistics.today_stat, dateRange)
			};
		}

		function getMonthly(from, to) {
			var dateRange = {
				datefrom: from,
				dateto: to
			};
			return {
				stat_for_months: apiService.postData(moveBoardApi.statistics.get_stat_for_months, dateRange)
			}
		}

		function getUniqueSiteVisitors() {
			var dateRange = {
				datefrom: moment().format('YYYY-MM-DD'),
				dateto: moment().format('YYYY-MM-DD'),
				type: 0
			};
			return {
				count_unique_site_visitors: apiService.postData(moveBoardApi.statistics.count_unique_site_visitors, dateRange)
			}
		}

		function periodUniqueMethod(from, to) {
			var dateRange = {
				datefrom: from,
				dateto: to
			};
			return {
				all_stat: apiService.postData(moveBoardApi.statistics.all_stat, dateRange)
			}
		}

		function getPayrollStatistic() {
			let defer = $q.defer();

			apiService.postData(moveBoardApi.statistics.statistics_for_six_months).then(resolve => {
				defer.resolve({
					statistics_for_six_months: resolve.data
				})
			});

			return defer.promise;
		}

		function prepareStatisticTab(data) {
			let result = {
				from: 0,
				to: 0,
			};
			let commonData = [];
			angular.forEach(data, function (item) {
				if (item.name == 'Roman Halavach') {
					return false;
				}
				if (!item.name.length) {
					item.name = 'Without source';
				}
				commonData.push(item);
				result.from += Number(item.from || 0);
				result.to += Number(item.to || 0);
			});
			result.conversion = percentFormatter(result.to, result.from);

			return {result, commonData};
		}

		function getStatisticsByField(field, date) {
			if (field.localeCompare('get_count_entrance_from') == 0) {
				return {field: apiService.postData(moveBoardApi.statistics.uniqueRequestStatisticMethod + field, date)};
			} else {
				return {field: apiService.postData(moveBoardApi.statistics.uniqueStatisticsMethod + field, date)};
			}
		}

		function hexToRgba(hex, alpha) {
			if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
				let c = hex.substring(1).split('');

				if (c.length == 3) {
					c = [c[0], c[0], c[1], c[1], c[2], c[2]];
				}

				c = '0x' + c.join('');
				return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${alpha})`
			}

			throw new Error('Bad Hex');
		}

		function percentFormatter(part, full) {
			var result = 0;
			if (!_.isNumber(part) || !_.isNumber(full) || _.isEqual(+full, 0) || _.isEqual(+part, 0)) {
				return result;
			} else {
				result = _.round(part / full * 100, 0);
			}
			return result;
		}

		function weekChartConstructor(date, color, data, title, tooltip, duration, count) {
			return {
				labels: date,
				datasets: [
					{
						backgroundColor: color,
						data: data,
						spanGaps: false,
						showLines: false
					}
				],
				color: color,
				title: title,
				tooltip: tooltip,
				duration: duration,
				count: count
			}
		}

		function lineChartConstructor(label, color, graphData) {
			return {
				label: label,
				fill: true,
				lineTension: 0.1,
				backgroundColor: hexToRgba(color, 0.5),
				borderColor: color,
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: color,
				pointBackgroundColor: color,
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: color,
				pointHoverBorderColor: color,
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: graphData,
				spanGaps: true,
				showLines: true
			}
		}

		function getLineChartOptions() {
			return {
				responsive: true,
				legend: {
					boxWidth: 20,
					hidden: true
				},
				scales: {
					xAxes: [{
						gridLines: {
							color: "rgba(0, 0, 0, 0)",
						},
						ticks: {
							suggestedMin: 0,
							beginAtZero: true
						}
					}],
					yAxes: [{
						gridLines: {
							color: "rgba(0, 0, 0, 0)",
						},
						ticks: {
							suggestedMin: 0,
							beginAtZero: true
						}
					}]
				}
			}
		}
	}
})();
