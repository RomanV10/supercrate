(function () {
    'use strict';

    angular
        .module('app.statistics')
        .factory('configStatistics', ConfigStatistics);

    function ConfigStatistics ()
    {
        return {
            INTERVAL_TIME: 20000, //ms
            MONTHS: {
                1:'January', 2:'February', 3:'March', 4:'April', 5:'May', 6:'June', 7: 'July', 8: 'August', 9: 'September',
                10: 'October', 11: 'November', 12:'December'
            },
            LINE_CHART_OPTIONS: {
                bezierCurve: true,
                bezierCurveTension: 0.4,
                datasetFill: true,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                legendTemplate: '<ul class="tc-chart-js-legend" style="text-align: center;"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                responsive: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                scaleShowGridLines: true
            },
            BAR_CHART_OPTIONS: {
                // Sets the chart to be responsive
                responsive: true,
                //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                scaleBeginAtZero : true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines : true,
                //String - Colour of the grid lines
                scaleGridLineColor : "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth : 1,
                //Boolean - If there is a stroke on each bar
                barShowStroke : true,
                //Number - Pixel width of the bar stroke
                barStrokeWidth : 2,
                //Number - Spacing between each of the X value sets
                barValueSpacing : 10,
                //Number - Spacing between data sets within X values
                barDatasetSpacing : 2,
                legendTemplate : '<ul class="tc-chart-js-legend" style="text-align: center;"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'

            },
            CIRCLE_CHART_OPTIONS: {
                animateRotate: true,
                animateScale: true,
                animationEasing: "easeOutBounce",
                animationSteps: 100,
                percentageInnerCutout: 50,
                responsive: true,
                segmentShowStroke: true,
                segmentStrokeColor: "#fff",
                segmentStrokeWidth: 2,
                legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
            }
        };

    }

})();
