(function () {
    'use strict';

    angular.module('app.statistics').controller('ReviewModalCtrl', ReviewModalCtrl);

    ReviewModalCtrl.$inject = ['$q', '$scope', '$uibModalInstance', 'reviewSettings', '$timeout', 'originalReviewTemplate', 'originalSuccessReviewTpl', 'originalFailReviewTpl', 'SendEmailsService', 'saveTemplate', 'SettingServices'];

    function ReviewModalCtrl($q, $scope, $uibModalInstance, reviewSettings, $timeout, originalReviewTemplate, originalSuccessReviewTpl, originalFailReviewTpl, SendEmailsService, saveTemplate, SettingServices) {

        $scope.stars = reviewSettings.count_positive_stars;
        $scope.negativeStars = reviewSettings.count_negative_stars;
        $scope.currentStars = Number(reviewSettings.current_stars_count);
        $scope.currentNegativeStars = Number(reviewSettings.current_negative_stars_count);
        $scope.daysToSendReminder = reviewSettings.days_to_send_review;
        $scope.selectedDay = reviewSettings.selectedDay;
        $scope.showReviewButton = reviewSettings.showReviewButton;
        $scope.currentStateShowReviewButton = reviewSettings.currentStateShowReviewButton;
        $scope.autoSend = reviewSettings.reviews_autosend;
        $scope.welcomeText = reviewSettings.welcomeReviewText;
        $scope.successText = reviewSettings.successReviewText;
        $scope.failText = reviewSettings.failReviewText;

        var previewOptionsTitleOfReview = 'Review Template';
        var previewOptionsTitleOfSuccessReview = 'Success Review Template';
        var previewOptionsTitleOfFailedReview = 'Failed Review Template';

        var blocksMenu = {};

        $scope.previewOptionsOfReview = makePreviewOptions(previewOptionsTitleOfReview);
        $scope.previewOptionsOfSuccessReview = makePreviewOptions(previewOptionsTitleOfSuccessReview);
        $scope.previewOptionsOfFailedReview = makePreviewOptions(previewOptionsTitleOfFailedReview);

        function makePreviewOptions(title) {
            return {
                previewTitle: title,
                isShowCloseButton: false,
                isShowSaveButton: false,
                isEditTemplateName: false,
                editBlockOptions: {
                    isShowVariableToolTip: true
                },
                isShowPreviewTabs: false
            }
        }


        $scope.tabs = [
            {name: 'Review Email', selected: true, id: 1},
            {name: 'Welcome Review', selected: false, id: 2},
            {name: 'Success Review', selected: false, id: 3},
            {name: 'Failed Review', selected: false, id: 4}
        ];

        $scope.changeMark = function (star) {
            if (star.mark <= $scope.currentNegativeStars) {
                toastr.warning('Positive assessment cannot be lower than negative');
            } else {
                for (var i = 0; i < star.mark; i++) {
                    $scope.stars[i].selected = true;
                }
                for (var j = 4; j >= star.mark; j--) {
                    $scope.stars[j].selected = false;
                }
                $scope.currentStars = star.mark;
            }
        };

        $scope.changeNegativeMark = function (starNegative) {
            if (starNegative.mark >= $scope.currentStars) {
                toastr.warning('Negative assessment cannot be higher than positive');
            } else {
                for (var i = 0; i < starNegative.mark; i++) {
                    $scope.negativeStars[i].selected = true;
                }
                for (var j = 4; j >= starNegative.mark; j--) {
                    $scope.negativeStars[j].selected = false;
                }
                $scope.currentNegativeStars = starNegative.mark;
            }
        };

        getReviewReminderTemplate().then(function (emailTemplate) {
            $scope.reviewReminderTemplate = emailTemplate;
            originalReviewTemplate = angular.copy(emailTemplate);
            $scope.blocksMenu = blocksMenu;
        });

        getReviewSuccessReminderTpl().then(function (successTemplate) {
            $scope.reviewSuccessReminderTemplate = successTemplate;
            originalSuccessReviewTpl = angular.copy(successTemplate);
            $scope.blocksMenu = blocksMenu;
        });

        getReviewFailReminderTpl().then(function (failTemplate) {
            $scope.reviewFailReminderTemplate = failTemplate;
            originalFailReviewTpl = angular.copy(failTemplate);
            $scope.blocksMenu = blocksMenu;
        });

        function getReviewReminderTemplate() {
            return getReviewTemplate('review_reminder_send');
        }

        function getReviewSuccessReminderTpl() {
            return getReviewTemplate('review_success_reminder');
        }

        function getReviewFailReminderTpl() {
            return getReviewTemplate('review_fail_reminder');
        }

        function getReviewTemplate(keyName) {
            var defer = $q.defer();

            var emailParameters = {
                keyNames: [keyName]
            };

            SendEmailsService.prepareForEditEmails(emailParameters).then(function (data) {
                var emails = data.emails;

                if (_.isEmpty(emails)) {
                    var emptyEmailObject = SendEmailsService.createEmptyEmailObject();
                    emptyEmailObject.key_name = keyName;
                    emails.push(emptyEmailObject);
                }

                if (_.isEmpty(blocksMenu)) {
                    blocksMenu = data.blocksMenu;
                }

                defer.resolve(emails[0]);
            }, function (error) {
                defer.reject('error');
            });

            return defer.promise;
        }

        $scope.setTab = function (tab) {
            angular.forEach($scope.tabs, function (item) {
                if (_.isEqual(item.name, tab.name)) {
                    item.selected = true;
                } else {
                    item.selected = false;
                }
            })
        };

        $scope.apply = function () {
            reviewSettings.current_stars_count = $scope.currentStars;
            reviewSettings.current_negative_stars_count = $scope.currentNegativeStars;
            reviewSettings.count_positive_stars = $scope.stars;
            reviewSettings.count_negative_stars = $scope.negativeStars;
            reviewSettings.selectedDay = $scope.selectedDay;
            reviewSettings.showReviewButton = $scope.showReviewButton;
            reviewSettings.currentStateShowReviewButton = $scope.currentStateShowReviewButton;
            reviewSettings.reviews_autosend = Number($scope.autoSend);
            reviewSettings.welcomeReviewText = $scope.welcomeText;
            reviewSettings.successReviewText = $scope.successText;
            reviewSettings.failReviewText = $scope.failText;
            saveTemplate($scope.reviewReminderTemplate, originalReviewTemplate);
            saveTemplate($scope.reviewSuccessReminderTemplate, originalSuccessReviewTpl);
            saveTemplate($scope.reviewFailReminderTemplate, originalFailReviewTpl);
            $scope.saveSettings();
            toastr.info('Review settings was updated');
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.saveSettings = function (type, value) {
            var setting = reviewSettings;
            var _setting_name = 'reviews_settings';

            if (angular.isDefined(type)) {
                _setting_name = type;
            }

            if (angular.isDefined(value)) {
                setting = value;
            }

            SettingServices.saveSettings(setting, _setting_name);
        };

        $scope.openEmailReview = function () {
            $scope.showTpl = !$scope.showTpl;
        }
    }
})();