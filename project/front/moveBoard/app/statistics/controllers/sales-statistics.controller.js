'use strict';

angular
	.module('app.statistics')
	.controller('SalesStatistics', SalesStatistics);

SalesStatistics.$inject = ['$scope', '$timeout', '$q', 'logger', 'salesStatisticsService', '$rootScope', 'quickStatisticsService', 'Raven'];

function SalesStatistics($scope, $timeout, $q, logger, salesStatisticsService, $rootScope, quickStatisticsService, Raven) {
	const TWO_MONTHS = 2;
	const MANAGER_UID = _.get($rootScope, 'currentUser.userId.uid');
	const CONFIRMED_STATUS = 3;
	var vm = this,
		timeout,
		now = moment(),
		$dateFrom = moment(now).startOf('month'),
		$dateTo = now;
	
	vm.getDate = getByDate;
	vm.dates = {
		from: moment().startOf('month').toDate(),
		to: moment().endOf('month').toDate(),
		options: {
			maxDate: moment().toDate(),
			minDate: null,
			formatYear: 'yy',
			startingDay: 0
		}
	};
	vm.current_month = moment().format('MMMM');
	vm.fromDate = moment(vm.dates.from).format('MM.DD.YYYY');
	vm.toDate = moment(vm.dates.to).format('MM.DD.YYYY');
	
	function getPeriodStatisticByRole() {
		let
			//daily
			today_to = moment().format('YYYY-MM-DD'),
			today_from = moment().format('YYYY-MM-DD'),
			//monthl
			month_from = moment().startOf('month').format('YYYY-MM-DD'),
			month_to = moment().endOf('month').format('YYYY-MM-DD'),
			today_promise = salesStatisticsService.getStatisticsByRole(today_from, today_to, MANAGER_UID),
			currentMonth_promise = salesStatisticsService.getStatisticsByRole(month_from, month_to, MANAGER_UID),
			graphic_data_promise = salesStatisticsService.getSalesBySteps(today_from, today_to, MANAGER_UID);
		let graphData = {
			daily: {
				requests: [],
				confirmed: [],
				commision: [],
				date: []
			},
			monthly: {
				requests: [],
				confirmed: [],
				commision: [],
				date: []
			}
		};
		$q.all(graphic_data_promise)
			.then(res => {
				//daily
				angular.forEach(res.statistics_for_sales_by_day.data, function (item, key) {
					graphData.daily.date.push(key);
					if (item[MANAGER_UID] == undefined) {
						item = {[MANAGER_UID]: {sales_payroll: {total: 0}, sales_by_approve: {[3]: {from: 0}}}};
					} else if (item[MANAGER_UID].sales_payroll.length == 0) {
						item[MANAGER_UID].sales_payroll = {total: 0};
					}
					graphData.daily.confirmed.push(Number(item[MANAGER_UID].sales_by_approve[3].from));
					graphData.daily.commision.push(item[MANAGER_UID].sales_payroll.total);
					var requests = 0;
					angular.forEach(item[MANAGER_UID].sales_by_approve, function (request) {
						requests += Number(request.from);
					});
					graphData.daily.requests.push(requests);
				});
				//monthly
				angular.forEach(res.statistics_for_six_months.data, function (item, key) {
					graphData.monthly.date.push(key);
					if (item.sales_payroll.length == 0) {
						graphData.monthly.commision.push(0);
					} else {
						graphData.monthly.commision.push(item.sales_payroll.total);
					}
					if (_.isUndefined(item.sales_by_assigned[MANAGER_UID])) {
						graphData.monthly.requests.push(0);
						graphData.monthly.confirmed.push(0);
					} else {
						graphData.monthly.requests.push(Number(item.sales_by_assigned[MANAGER_UID].from));
						graphData.monthly.confirmed.push(Number(item.sales_by_assigned[MANAGER_UID].to));
					}
				});
				graphData.daily.date.reverse();
				graphData.daily.commision.reverse();
				graphData.daily.confirmed.reverse();
				graphData.daily.requests.reverse();
				graphData.monthly.date.reverse();
				graphData.monthly.commision.reverse();
				graphData.monthly.confirmed.reverse();
				graphData.monthly.requests.reverse();
				
				
				//daily
				$q.all({today_promise, currentMonth_promise})
					.then(resolve => {
						vm.today_requests = 0;
						vm.today_commision = 0;
						vm.current_month_requests = 0;
						vm.current_month_commision = 0;
						vm.assign_today = {};
						vm.assign_monthly = {};
						vm.today_confirmed = resolve.today_promise.sales_by_approve[CONFIRMED_STATUS].from;
						vm.current_month_confirmed = resolve.currentMonth_promise.sales_by_approve[CONFIRMED_STATUS].from;
						
						if (!_.isUndefined(resolve.today_promise.sales_by_assigned[MANAGER_UID])) {
							vm.today_requests = _.round(resolve.today_promise.sales_by_assigned[MANAGER_UID].from, 0);
						}
						if (!_.isUndefined(resolve.currentMonth_promise.sales_by_assigned[MANAGER_UID])) {
							vm.current_month_requests = _.round(resolve.currentMonth_promise.sales_by_assigned[MANAGER_UID].from, 0);
						}
						
						if (!_.isUndefined(resolve.today_promise.sales_payroll.total)) {
							vm.today_commision = _.round(resolve.today_promise.sales_payroll.total, 2);
						}
						if (!_.isUndefined(resolve.currentMonth_promise.sales_payroll.total)) {
							vm.current_month_commision = _.round(resolve.currentMonth_promise.sales_payroll.total, 2);
						}
						
						angular.forEach(resolve.today_promise.sales_by_assigned, function (item) {
							vm.assign_today = item;
						});
						angular.forEach(resolve.currentMonth_promise.sales_by_assigned, function (item) {
							vm.assign_monthly = item;
						});
						
					})
					.catch(error => {
						Raven.captureException(`sales-statistics.controller.js:  ${error}`);
					})
					.finally(() => {
						vm.request_daily = quickStatisticsService.weekChartConstructor(
							graphData.daily.date,
							'#1c84c6',
							graphData.daily.requests,
							'Assign',
							'Today assigned',
							'Today',
							vm.today_requests
						);
						vm.request_monthly = quickStatisticsService.weekChartConstructor(
							graphData.monthly.date,
							'#1c84c6',
							graphData.monthly.requests,
							'Assign',
							`Assigned in ${vm.current_month}`,
							'Current month',
							vm.current_month_requests
						);
						vm.confirmed_daily = quickStatisticsService.weekChartConstructor(
							graphData.daily.date,
							'#ffa500',
							graphData.daily.confirmed,
							'Booked',
							'Today booked',
							'Today',
							vm.today_confirmed
						);
						vm.confirmed_monthly = quickStatisticsService.weekChartConstructor(
							graphData.monthly.date,
							'#ffa500',
							graphData.monthly.confirmed,
							'Booked',
							`Booked in ${vm.current_month}`,
							'Current month',
							vm.current_month_confirmed
						);
						vm.commision_daily = quickStatisticsService.weekChartConstructor(
							graphData.daily.date,
							'#ed5565',
							graphData.daily.commision,
							'Commission',
							'Today commission',
							'Today',
							`${vm.today_commision}$`
						);
						vm.commision_monthly = quickStatisticsService.weekChartConstructor(
							graphData.monthly.date,
							'#ed5565',
							graphData.monthly.commision,
							'Commission',
							`Commission in ${vm.current_month}`,
							'Current month',
							`${vm.current_month_commision}$`
						);
					});
				
			})
			.catch(error => {
				Raven.captureException(`sales-statistics.controller.js:  ${error}`);
			});
	}
	
	function getStatisticByRole() {
		var datefrom = moment(vm.dates.from).format('YYYY-MM-DD'),
			dateto = moment(vm.dates.to).format('YYYY-MM-DD');
		salesStatisticsService.getStatisticsByRole(datefrom, dateto, MANAGER_UID)
			.then(function (resolve) {
				let salesAssigned = _.get(resolve, `sales_by_assigned[${MANAGER_UID}]`, {});
				vm.periodRequests = salesAssigned.main || 0;
				vm.secondConfirmedJobs = salesAssigned.second || 0;
				vm.sales_by_service_type = [];
				vm.sales_by_source = [];
				angular.forEach(resolve.sales_by_service_type, function (item) {
					vm.sales_by_service_type.push(item);
				});
				angular.forEach(resolve.sales_by_source, function (item) {
					vm.sales_by_source.push(item);
				});
				vm.status = {
					confirmed: resolve.sales_by_approve[3].from,
					not_confirmed: resolve.sales_by_approve[2].from,
					canceled: resolve.sales_by_approve[5].from,
					expired: resolve.sales_by_approve[12].from
				};
			})
			.catch(error => {
				Raven.captureException(`sales-statistics.controller.js:  ${error}`);
			});
	}
	
	function getCommonStatistics() {
		let from = moment(vm.dates.from).format('YYYY-MM-DD'),
			to = moment(vm.dates.to).format('YYYY-MM-DD'),
			commonDaily = salesStatisticsService.getCommonSalesStat(from, to, MANAGER_UID),
			twoMonthStatistic = salesStatisticsService.getStatisticsByRole(from, to, MANAGER_UID),
			commonMonthly = salesStatisticsService.getCommonSalesStatMonthly(from, to, MANAGER_UID),
			periodForTwoMonth = moment(vm.dates.to).diff(moment(vm.dates.from), 'months', true);
		
		if (periodForTwoMonth > TWO_MONTHS) {
			$q.all({commonMonthly, twoMonthStatistic}).then(function (resolve) {
				vm.periodConfirmed = 0;
				angular.forEach(resolve.twoMonthStatistic, function (item) {
					if (_.isUndefined(item[MANAGER_UID])) {
						vm.periodConfirmed += 0;
					} else {
						vm.periodConfirmed += Number(item[MANAGER_UID].to || 0);
					}
				});
				paintNewChart(resolve.commonMonthly);
			});
		} else {
			commonDaily.then(function (resolve) {
				vm.periodConfirmed = 0;
				angular.forEach(resolve, function (item) {
					if (_.isUndefined(item.sales_by_assigned[MANAGER_UID])) {
						vm.periodConfirmed += 0;
					} else {
						vm.periodConfirmed += Number(item.sales_by_assigned[MANAGER_UID].to || 0);
					}
				});
				paintNewChart(resolve);
			});
		}
	}
	
	function paintNewChart(data) {
		let graphNewData = {
			confirmed: [],
			requests: [],
			commission: [],
			date: []
		};
		vm.current_requests = 0;
		vm.current_confirmed = 0;
		vm.periodComission = 0;
		angular.forEach(data, function (item, key) {
			if (_.isUndefined(item.sales_by_assigned[MANAGER_UID])) {
				graphNewData.confirmed.push(0);
				graphNewData.requests.push(0);
			} else {
				graphNewData.confirmed.push(Number(item.sales_by_assigned[MANAGER_UID].to));
				graphNewData.requests.push(Number(item.sales_by_assigned[MANAGER_UID].from));
				
				vm.current_requests += Number(item.sales_by_assigned[MANAGER_UID].from);
				vm.current_confirmed += Number(item.sales_by_assigned[MANAGER_UID].to);
			}
			if (_.isUndefined(item.sales_payroll.total)) {
				graphNewData.commission.push(0);
			} else {
				graphNewData.commission.push(Number(item.sales_payroll.total));
				vm.periodComission += item.sales_payroll.total;
			}
			graphNewData.date.push(key);
		});
		let requests = quickStatisticsService.lineChartConstructor('Requests', '#1c84c6', graphNewData.requests);
		let confirmed = quickStatisticsService.lineChartConstructor('Confirmed', '#ffa500', graphNewData.confirmed);
		let commision = quickStatisticsService.lineChartConstructor('Commission', '#1ab394', graphNewData.commission);
		vm.lineData = {
			labels: graphNewData.date,
			datasets: [requests, confirmed, commision]
		};
		vm.lineChartOptions = quickStatisticsService.getLineChartOptions();
	}
	
	function getByDate() {
		try {
			$dateFrom = $.datepicker.parseDate('M d, yy', $('#dateFrom').val());
			$dateTo = $.datepicker.parseDate('M d, yy', $('#dateTo').val());
			getCommonStatistics();
			getStatisticByRole();
			
			vm.fromDate = moment($dateFrom).format('MM.DD.YYYY');
			vm.toDate = moment($dateTo).format('MM.DD.YYYY');
		} catch (e) {
			logger.error('E2: Something going wrong', e, 'Error');
		} finally {
			vm.bDateChange = false;
		}
	}
	
	(function init() {
		getStatisticByRole();
		getCommonStatistics();
		getPeriodStatisticByRole();
		quickStatisticsService.getFirstAssignDate(MANAGER_UID).then(resolve => {
			vm.dates.options.minDate = new Date(resolve);
		});
	})();
	
	$scope.$on('$destroy', function () {
		$timeout.cancel(timeout);
	});
}

moment.createFromInputFallback = function (config) {
	config._d = new Date(config._i);
};
