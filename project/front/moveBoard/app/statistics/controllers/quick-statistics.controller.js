'use strict';

import {
	STATISTIC_TABLE_HEADER
} from '../base/statistic-base';
import MOMENT_TIMEZONE from 'moment-timezone';

angular
	.module('app.statistics')
	.controller('QuickInfoController', quickInfoController);

quickInfoController.$inject = ['$scope', '$timeout', '$q', 'logger', 'quickStatisticsService', 'RequestServices', '$rootScope', 'datacontext', 'StatisticService'];

function quickInfoController($scope, $timeout, $q, logger, quickStatisticsService, RequestServices, $rootScope, datacontext, StatisticService) {
	var vm = this,
		timeout,
		now = moment(),
		$dateFrom = moment(now).startOf('month'),
		$dateTo = now;
	
	const TWO_MONTH = 2;
	const TIME_ZONE = datacontext.getFieldData().timeZone;
	const NO_ESTIMATE = 0;
	MOMENT_TIMEZONE.tz.add(TIME_ZONE.initial);
	
	
	vm.dates = {
		from: moment().startOf('month').toDate(),
		to: moment().endOf('month').toDate(),
		maxDate: new Date(),
		options: {
			minDate: null,
			formatYear: 'yy',
			startingDay: 0
		}
	};
	
	vm.entranceSwitcher = false;
	vm.sourceSwitcher = false;
	vm.sizeSwitcher = false;
	vm.serviceTypeSwitcher = false;
	vm.assignSwitcher = false;
	
	vm.getDate = getByDate;
	
	vm.HEADING_TABLES = STATISTIC_TABLE_HEADER;
	
	(function () {
		getUniqueUsersToday();
		getExtimateProfit();
		getTodayStatistics();
		quickStatisticsService.getFirstAssignDate().then(resolve => {
			vm.dates.options.minDate = new Date(resolve);
		});
	})();
	function getExtimateProfit() {
		let dateFrom = moment(new Date(vm.dates.from)).format('YYYY-MM-DD');
		let dateTo = moment(new Date(vm.dates.to)).format('YYYY-MM-DD');
		let periodEstimate = {
			from: MOMENT_TIMEZONE.tz(dateFrom, TIME_ZONE.name).startOf('day').unix(),
			to: MOMENT_TIMEZONE.tz(dateTo, TIME_ZONE.name).endOf('day').unix()
		};
		vm.profit = {
			month: [],
			data: []
		};
		
		let sixMonth = quickStatisticsService.getPayrollStatistic();
		let currentMonth = StatisticService
			.getProfitAndLossData({
				dateFrom: moment(new Date(vm.dates.from)).format('YYYY-MM-DD'),
				dateTo: moment(new Date(vm.dates.to)).format('YYYY-MM-DD'),
				currentPage: 1,
				perPage: 20,
			});
		
		$q.all({sixMonth, currentMonth})
			.then(resolve => {
				angular.forEach(resolve.sixMonth.statistics_for_six_months, function (item, key) {
					vm.profit.month.push(key);
					let profit = _.get(item, 'income_estimate', NO_ESTIMATE);
					vm.profit.data.push(profit);
				});
				
				vm.profit.month.reverse();
				vm.profit.data.reverse();
				
				vm.profit.month.push(moment.unix(periodEstimate.from).format('YYYY-MM'));
				vm.estimatiedProfit = _.round(resolve.currentMonth['estimated_income_total'], 2);
				vm.profit.data.push(vm.estimatiedProfit);
			})
			.finally(() => {
				vm.halfYearProfit = quickStatisticsService.weekChartConstructor(
					vm.profit.month,
					'#e74c3c',
					vm.profit.data,
					'Estimate Income',
					'Estimated profit',
					'Monthly',
					`$ ${_.round(vm.estimatiedProfit, 2)}`
				);
			});
	}
	
	function getUniqueUsersToday() {
		var todayUsersPromise = quickStatisticsService.getUniqueSiteVisitors();
		$q.all(todayUsersPromise)
			.then(function (resolved) {
				vm.uniqueUsers = resolved.count_unique_site_visitors.data[0];
			});
	}
	
	function getTodayStatistics() {
		var today = moment().format('YYYY-MM-DD'),
			todayPromise = quickStatisticsService.getTodayStat(today);
		$q.all(todayPromise).then(function (today_resolve) {
			vm.todayConfirmed = +today_resolve.today_stat.data.confirmed;
			vm.todayRequest = +today_resolve.today_stat.data.count_of_all;
			getPeriodStat();
		});
	}
	
	function getLastYearStatistic(lastYearData) {
		let lastYearConfirmed = Number(lastYearData.get_stat_for_period.confirmed);
		let lastYearRequests = Number(lastYearData.get_stat_for_period.count_of_all);
		let lastYearVisitors = Number(lastYearData.count_site_visitors);
		
		if (lastYearConfirmed != 0) {
			vm.last_year_periodConfirmed = `/ ${lastYearConfirmed} a year ago`;
		}
		
		if (lastYearRequests != 0) {
			vm.last_year_periodRequests = `/ ${lastYearRequests} a year ago`;
		}
		
		if (lastYearVisitors != 0) {
			vm.last_year_periodUsers = `/ ${lastYearVisitors} a year ago`;
		}
	}
	
	function getPeriodStat() {
		var from = moment(vm.dates.from).format('YYYY-MM-DD'),
			to = moment(vm.dates.to).format('YYYY-MM-DD'),
			periodForTwoMonth = moment(vm.dates.to).diff(moment(vm.dates.from), 'months', true),
			
			yearAgo = {
				from: moment(vm.dates.from).subtract(1, 'years').format('YYYY-MM-DD'),
				to: moment(vm.dates.to).subtract(1, 'years').format('YYYY-MM-DD')
			},
			lastYearPromise = quickStatisticsService.periodUniqueMethod(yearAgo.from, yearAgo.to),
			
			periodPromises = quickStatisticsService.periodUniqueMethod(from, to);
		
		$q.all(lastYearPromise).then(function (last_year_resolve) {
			getLastYearStatistic(last_year_resolve.all_stat.data);
		});
		if (periodForTwoMonth < TWO_MONTH) {
			$q.all(periodPromises).then(function (res) {
				vm.noToolTip = false;
				let countByApprove = _.get(res, "all_stat.data.get_stat_for_period.count_by_approve['3']", 0);
				vm.periodConfirmed = parseInt(countByApprove);
				vm.periodRequests = parseInt(res.all_stat.data.get_stat_for_period.count_of_all);
				vm.siteVisitors = parseInt(res.all_stat.data.count_site_visitors);
				vm.accountVisitors = parseInt(res.all_stat.data.stat_user_visit_account);
				vm.confPageVisitors = parseInt(res.all_stat.data.stat_user_visit_conf_page);
				vm.bookedByManager = parseInt(res.all_stat.data.stat_booked_by.manager);
				vm.bookedByUser = parseInt(res.all_stat.data.stat_booked_by.user);
				vm.periodUniqueUsers = parseInt(res.all_stat.data.count_unique_site_visitors);
				vm.periodStepOne = 0;
				vm.periodStepTwo = 0;
				vm.periodStepThree = 0;
				vm.periodStepFour = 0;
				vm.periodStepFive = 0;
				vm.periodStepSix = 0;
				vm.urlStepOne = [];
				vm.urlStepTwo = [];
				vm.urlStepThree = [];
				vm.urlStepFour = [];
				vm.urlStepFive = [];
				let steps = res.all_stat.data.count_submit_form;
				angular.forEach(steps, function (item) {
					switch (item.form) {
						case 1:
							vm.periodStepOne += Number(item.count);
							vm.urlStepOne.push(item.page_url || 'No link');
							break;
						case 2:
							vm.periodStepTwo += Number(item.count);
							vm.urlStepTwo.push(item.page_url || 'No link');
							break;
						case 3:
							vm.periodStepThree += Number(item.count);
							vm.urlStepThree.push(item.page_url || 'No link');
							break;
						case 4:
							vm.periodStepFour += Number(item.count);
							vm.urlStepFour.push(item.page_url || 'No link');
							break;
						case 5:
							vm.periodStepFive += Number(item.count);
							vm.urlStepFive.push(item.page_url || 'No link');
							break;
						case 6:
							vm.periodStepSix += Number(item.count);
							break;
					}
				});
				paintNewChart(res.all_stat.data.get_statistics_for_days);
				paintWeekCharts(res.all_stat.data.get_statistics_for_days.slice(-7));
			});
		} else {
			let monthlyPromise = quickStatisticsService.getMonthly(from, to);
			$q.all(monthlyPromise).then(function (monthly_resolve) {
				vm.noToolTip = true;
				vm.periodUniqueUsers = 0;
				vm.periodRequests = 0;
				vm.periodConfirmed = 0;
				angular.forEach(monthly_resolve.stat_for_months.data, (month) => {
					vm.periodUniqueUsers += Number(month.unique_user);
					vm.periodRequests += Number(month.count_of_all);
					vm.periodConfirmed += Number(month.confirmed);
				});
				paintNewChart(monthly_resolve.stat_for_months.data);
			});
		}
	}
	
	function paintWeekCharts(data) {
		var weekBarData = {
			uniqueUser: [],
			requests: [],
			confirmed: [],
			date: []
		};
		angular.forEach(data, function (item) {
			weekBarData.confirmed.push(Number(item.confirmed));
			weekBarData.requests.push(Number(item.count_of_all));
			weekBarData.uniqueUser.push(Number(item.unique_user));
			weekBarData.date.push(item.date);
		});
		
		vm.weekUniqueUsers = quickStatisticsService.weekChartConstructor(
			weekBarData.date,
			'#1c84c6',
			weekBarData.uniqueUser,
			'Visitors',
			'Unique visitors',
			'Today',
			vm.uniqueUsers
		);
		vm.week_requests = quickStatisticsService.weekChartConstructor(
			weekBarData.date,
			'#ffa500',
			weekBarData.requests,
			'Requests',
			'Today requests',
			'Today',
			vm.todayRequest
		);
		vm.week_confirmed = quickStatisticsService.weekChartConstructor(
			weekBarData.date,
			'#1ab394',
			weekBarData.confirmed,
			'Confirmed',
			'Today confirmed',
			'Today',
			vm.todayConfirmed
		);
	}
	
	function paintNewChart(data) {
		var graphNewData = {
			confirmed: [],
			requests: [],
			unique_user: [],
			date: []
		};
		angular.forEach(data, function (item) {
			graphNewData.confirmed.push(Number(item.confirmed));
			graphNewData.requests.push(Number(item.count_of_all));
			graphNewData.unique_user.push(Number(item.unique_user));
			graphNewData.date.push(item.date);
		});
		let usersGraphicData = quickStatisticsService.lineChartConstructor('Visitors', '#1c84c6', graphNewData.unique_user);
		let requestGraphicData = quickStatisticsService.lineChartConstructor('Requests', '#ffa500', graphNewData.requests);
		let confirmedGraphicData = quickStatisticsService.lineChartConstructor('Confirmed', '#1ab394', graphNewData.confirmed);
		vm.lineData = {
			labels: graphNewData.date,
			datasets: [usersGraphicData, requestGraphicData, confirmedGraphicData]
		};
		vm.lineChartOptions = quickStatisticsService.getLineChartOptions();
	}
	
	function getByDate() {
		try {
			$dateFrom = $.datepicker.parseDate('M d, yy', $('#dateFrom').val());
			$dateTo = $.datepicker.parseDate('M d, yy', $('#dateTo').val());
			getPeriodStat();
			
			$rootScope.$broadcast('dates.update', vm.dates);
		}
		catch (e) {
			logger.error('E2: Something going wrong', e, 'Error');
		}
		finally {
			vm.bDateChange = false;
		}
	}
	
	$scope.$on('$destroy', function () {
		$timeout.cancel(timeout);
	});
}

moment.createFromInputFallback = function (config) {
	config._d = new Date(config._i);
};
