'use strict';

angular
	.module('app.statistics')
	.directive('salesStatistics', salesStatistics);

function salesStatistics() {
	return {
		restrict: 'E',
		scope: {},
		template: require('./template/sales-statistics.html'),
		controller: 'SalesStatistics',
		controllerAs: 'vm',
		bindToController: true
	};
}
