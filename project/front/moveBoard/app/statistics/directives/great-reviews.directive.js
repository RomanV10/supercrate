(function () {
    'use strict';

    angular.module('app.statistics').directive('greatestReviews', greatestReviews);

    greatestReviews.$inject = ['datacontext', 'SettingServices'];

    function greatestReviews(datacontext, SettingServices) {
        return {
            restrict: 'E',
	        template: require('./template/greatest-review.tpl.html'),
            scope: {},
            link: greatReviewLink
        };

        function greatReviewLink($scope, attr, elem) {
            var fieldData = datacontext.getFieldData();
            $scope.reviewSettings = angular.fromJson(fieldData.reviews_settings);
            $scope.greatestReviews = $scope.reviewSettings.reviewsFrontSite;

            $scope.removeReview = function (review) {
                $scope.greatestReviews.splice($scope.greatestReviews.indexOf(review), 1);
                $scope.saveSettings();
                toastr.info('That review won\'t be shown on site block');
            }

            $scope.saveSettings = function (type, value) {
                var setting = $scope.reviewSettings;
                var _setting_name = 'reviews_settings';

                if (angular.isDefined(type)) {
                    _setting_name = type;
                }

                if (angular.isDefined(value)) {
                    setting = value;
                }

                SettingServices.saveSettings(setting, _setting_name);
            };
        }
    }
})();
