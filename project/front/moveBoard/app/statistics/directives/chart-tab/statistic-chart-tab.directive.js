'use strict';

import './chart-tab.styl';

const INCOME_ESTIMATE_TITLE = "Estimate Income";
const COMMISIONS_TITLE = "Commission";

angular.module('app.statistics').directive('chartStatisticTab', chatStatisticTab);

chatStatisticTab.$inject = ['quickStatisticsService', '$timeout'];

function chatStatisticTab(quickStatisticsService, $timeout) {
	return {
		restrict: 'AE',
		scope: {
			graphicData: '=',
		},
		template: require('./chart-tab.tpl.pug'),
		link: renderChart
	};

	function renderChart($scope) {
		$scope.options = {
			responsive: true,
			legend: {
				display: false,
				scaleShowLabels: false
			},
			scales: {
				xAxes: [{
					display: false,
					categoryPercentage: 0.9,
					barPercentage: 0.9,
					gridLines: {
						display: false
					},
					ticks: {
						suggestedMin: 0,
						beginAtZero: true
					}
				}],
				yAxes: [{
					display: false,
					gridLines: {
						display: false
					},
					ticks: {
						suggestedMin: 0,
						beginAtZero: true
					}
				}],
			},
			tooltips: {
				custom: function(tooltipModel) {
					tooltipModel.caretSize = 0;
					tooltipModel.xPadding = 0;
					tooltipModel.yPadding = 0;
					tooltipModel.cornerRadius = 0;
					tooltipModel.width = 0;
					tooltipModel.height = 0;
				},
				filter: function(tooltipItem) {
					let format = tooltipItem.xLabel.length === 10 ? 'MMMM DD, YYYY' : 'MMMM, YYYY';
					$scope.showTooltip = true;
					$scope.tooltipDate = moment(tooltipItem.xLabel).format(format);

					let isCurrencyTitle = _.isEqual($scope.graphicData.title, INCOME_ESTIMATE_TITLE) || _.isEqual($scope.graphicData.title, COMMISIONS_TITLE);

					if (isCurrencyTitle) {
						$scope.globalTooltip = `$ ${tooltipItem.yLabel.toFixed(2)}`;
					} else {
						$scope.globalTooltip = `${tooltipItem.yLabel} ${$scope.graphicData.title}`;
					}
				}
			}
		};

		$scope.$watch('showTooltip', function (current) {
			if (!!current) {
				$timeout(function () {
					$scope.showTooltip = false;
				}, 5000);
			}
		});

		$scope.tooltipDate = null;
		$scope.globalTooltip = null;
		$scope.titleBG = angular.copy($scope.graphicData.color);
		$scope.graphicData.datasets["0"].backgroundColor = quickStatisticsService.hexToRgbA($scope.graphicData.color, 0.5);
	}
}