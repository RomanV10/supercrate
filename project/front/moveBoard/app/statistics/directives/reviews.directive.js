import './review-settings-modal.styl';

(function () {
    'use strict';

    angular.module('app.statistics').directive('reviewsDirective', reviewsDirective);

    /* @ngInject */
    function reviewsDirective($q, $uibModal, ReviewService, datacontext, SettingServices, logger, TemplateBuilderService, EmailBuilderRequestService, openRequestService) {
        return {
            restrict: 'E',
            scope: {},
	        template: require('./template/reviews.tpl.html'),
            link: linkReview
        };

        function linkReview($scope, attr, elem) {

            $scope.allReviews = [];
            var originalReviewTemplate = {};
            var originalSuccessReviewTpl = {};
            var originalFailReviewTpl = {};

            var fieldData = datacontext.getFieldData();
            $scope.reviewSettings = angular.fromJson(fieldData.reviews_settings);
            $scope.stars = $scope.reviewSettings.count_positive_stars;
            $scope.negativeStars = $scope.reviewSettings.count_negative_stars;

            $scope.markCount = 0;
            for (var i = 0; i < $scope.negativeStars.length; i++) {
                if ($scope.negativeStars[i].selected) {
                    $scope.markCount++;
                }
            }

            $scope.sorting = 'created';
            $scope.direction = 'DESC';
            $scope.page = 0;

            function initReviews() {
                sortReview($scope.sorting, $scope.direction, $scope.page);
            }
            initReviews();

            $scope.getNextPage = function () {
                $scope.page++;
                sortReview($scope.sorting, $scope.direction, $scope.page);
            };

            $scope.getPreviousPage = function () {
                $scope.page--;
                sortReview($scope.sorting, $scope.direction, $scope.page);
            };

            $scope.requestEditModal = function (nid) {
                $scope.busy = true;

	            openRequestService.open(nid)
                    .finally(() => {
	                    $scope.busy = false;
                    });
            };

            $scope.openReviewSettings = function () {
                var reviewModal = $uibModal.open({
	                template: require('./template/reviews-settings.tpl.html'),
                    controller: 'ReviewModalCtrl',
                    backdrop: false,
                    size: 'lg',
                    resolve: {
                        reviewSettings: function () {
                            return $scope.reviewSettings
                        },
                        originalReviewTemplate: function () {
                            return originalReviewTemplate;
                        },
                        originalSuccessReviewTpl: function () {
                            return originalSuccessReviewTpl;
                        },
                        originalFailReviewTpl: function () {
                            return originalFailReviewTpl;
                        },
                        saveTemplate: function () {
                            return saveTemplate;
                        }
                    }
                });

                reviewModal.result.then(function (resolve) {});
            };

            $scope.addGreatestReview = function (review) {
                if (_.find($scope.reviewSettings.reviewsFrontSite, {reviewID: review.reviewID})) {
                    toastr.warning('This review is already on the site');
                } else if ($scope.reviewSettings.reviewsFrontSite.length == 10) {
                    toastr.warning('Maximum 10 reviews allowed');
                } else {
                    $scope.reviewSettings.reviewsFrontSite.push(review);
                    SettingServices.saveSettings($scope.reviewSettings, 'reviews_settings');
                    toastr.success('Review has been successfully added');
                }
            };

            $scope.$watchCollection(function () {
                return [$scope.sorting, $scope.direction];
            }, function (current, original) {
                if (current != original) {
                    sortReview($scope.sorting, $scope.direction, $scope.page);
                }
            });

            function sortReview(sort, dir, page) {
                var promise = ReviewService.sortReviews(sort, dir, page);

                $q.all(promise).then(function (resolve) {
                    $scope.allReviews = [];
                    angular.forEach(resolve.sortedReview.data, function (item) {
                        $scope.allReviews.push({
                            reviewID: item.id,
                            requestID: item.entity_id,
                            stars: Number(item.count_stars),
                            reviewText: item.user_review,
                            userID: item.user_id,
                            userName: item.user_name + ' ' + item.user_last_name,
                            createdDate: moment.unix(item.created).format('MMM DD YYYY')
                        })
                    });
                })
            }

            function saveTemplate(template, original) {
                var defer = $q.defer();

                if (!_.isEqual(template, original)
                    && !_.isEmpty(template.blocks)) {

                    template.data.subject = template.subject;

                    if (_.isNull(template.id)) {
                        EmailBuilderRequestService.createTemplate(template).then(function (response) {
                            logger.success('Template was created successful!', response.status, 'Success');
                            template.id = _.head(response.data);
                            template.templateId = template.id;
                            TemplateBuilderService.updateGlobalEmailTemplates(template);
                            copyObjectProperty(template, original);
                            defer.resolve(response.data);
                        }, function (error) {
                            defer.reject();
                            logger.error('Error creating a template!', JSON.stringify(error), 'Error');
                        });
                    } else {
                        EmailBuilderRequestService.updateTemplate(template).then(function (response) {
                            defer.resolve(response.data);
                            TemplateBuilderService.updateGlobalEmailTemplates(template);
                            copyObjectProperty(template, original);
                            logger.success('Template was updated successfully!', response.status, 'Success');
                        }, function (error) {
                            defer.reject();
                            logger.error('Error updating a template!', JSON.stringify(error), 'Error');
                        });
                    }
                } else {
                    defer.resolve();
                }

                return defer.promise;
            }

            function copyObjectProperty(origin, destination) {
                _.each(origin, function (value, property) {
                    destination[property] = angular.copy(value);
                });
            }
        }
    }
})();
