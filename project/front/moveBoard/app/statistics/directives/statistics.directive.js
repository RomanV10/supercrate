(function () {
    'use strict';

    angular
        .module('app.statistics')
        .directive('ccStatistics', ccStatistics);


    function ccStatistics () {
        return {
	        template: require('../../schedule/templates/schedule.html'),
            restrict: 'A',
            scope: {
                requests: '=requests'
            },
            controllerAs: 'ErStatisticsCtrl as vm',
            bindToController: true

        };

        function ErStatisticsCtrl() {

        }
    }
})();




