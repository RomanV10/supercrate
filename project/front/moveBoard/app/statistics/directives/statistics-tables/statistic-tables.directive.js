'use strict';

import {
	STATISTICS_FIELDS
} from '../../base/statistic-base';

angular
	.module('app.statistics')
	.directive('statisticTable', statisticTable);

statisticTable.$inject = ['quickStatisticsService', '$q'];

function statisticTable(quickStatisticsService, $q) {
	return {
		restrict: 'AE',
		scope: {
			header: '=',
			index: '@',
			dates: '='
		},
		template: require('./statistic-table.tpl.pug'),
		link: renderTable
	};

	function renderTable($scope) {

		let dateParent = [
			{
				datefrom: moment($scope.dates.from).format('YYYY-MM-DD'),
				dateto: moment($scope.dates.to).format('YYYY-MM-DD')
			},
			{
				datefrom: moment().format('YYYY-MM-DD'),
				dateto: moment().format('YYYY-MM-DD')
			}
		];

		$scope.switcher = false;

		$scope.getStatiscticInfo = getStatiscticInfo;

		function getStatiscticInfo(field, date) {
			var promise = quickStatisticsService.getStatisticsByField(STATISTICS_FIELDS[field], dateParent[Number(date)]);

			$q.all(promise).then(function (resolve) {
				$scope.tableData = [];
				if (STATISTICS_FIELDS[field].localeCompare('get_count_entrance_from') == 0) {
					$scope.total = {
						from: 0,
						to: 0
					};
					angular.forEach(resolve.field.data.get_count_entrance_from, function (item, key) {
						var to = resolve.field.data.get_count_entrance_to[key];
						$scope.tableData.push({name: item.name, from: item.entrance_count, to: to.entrance_count});
						$scope.total.from += Number(item.entrance_count);
						$scope.total.to += Number(to.entrance_count);
					});
					return;
				}
				let {result, commonData} = quickStatisticsService.prepareStatisticTab(resolve.field.data);
				
				
				$scope.total = result;
				$scope.tableData = commonData;
			})
		}
		
		getStatiscticInfo($scope.index, $scope.switcher);
		
		$scope.$on('dates.update', function (ev, date) {
			dateParent[0].datefrom = date.from;
			dateParent[0].dateto = date.to;
			getStatiscticInfo($scope.index, $scope.switcher);
		});
	}
}
