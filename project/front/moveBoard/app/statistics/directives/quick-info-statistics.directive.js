'use strict';

angular
	.module('app.statistics')
	.directive('quickInfoStatistics', quickInfoStatistics);

function quickInfoStatistics() {
	return {
		restrict: 'E',
		scope: {},
		template: require('./template/quick-info-statistic.html'),
		controller: 'QuickInfoController',
		controllerAs: 'vm',
		bindToController: true
	};
}
