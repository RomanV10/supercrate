(function () {
    'use strict';

    angular.module('app.statistics').directive('reviewsStatisticsDirective', reviewsStatisticsDirective);

    reviewsStatisticsDirective.$inject = ['$q', 'ReviewService', 'MomentWrapperService'];

    function reviewsStatisticsDirective($q, ReviewService, MomentWrapperService) {
        return {
            restrict: 'E',
            scope: {},
	        template: require('./template/reviews-statistics.tpl.html'),
            link: linkReviewDirective
        };

        function linkReviewDirective($scope, attr, elem) {
            $scope.date = {
                datefrom: MomentWrapperService.getZeroMoment(new Date()).startOf('month').unix(),
                dateto: MomentWrapperService.getZeroMoment(new Date()).endOf('month').unix()
            };
            $scope.sortedReviews = {
                labels: [],
                datasets: [{
                    backgroundColor: 'rgba(41, 128, 185, .8)',
                    borderColor: 'rgba(41, 128, 185, 1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    data: [],
                    spanGaps: false,
                    showLines: false
                }]
            };
            $scope.profitOptions = {
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: false,
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            suggestedMin: 0,
                            beginAtZero:true
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            suggestedMin: 0,
                            beginAtZero:true
                        }
                    }]
                }
            };
            function renderSortedReviews() {
                var promise = ReviewService.getSortedReviews($scope.date);
                $q.all(promise).then(function (resolve) {
                    var totalAverage = 0;
                    $scope.average = 0;
                    $scope.total = 0;
                    angular.forEach(resolve.reviewsStatistics.data, function (item, key) {
                        $scope.sortedReviews.labels.push(key + ' ★');
                        $scope.sortedReviews.datasets[0].data.push(item);
                        totalAverage += Number(item);
                        $scope.average += (Number(item) * key);
                        $scope.total += Number(item);
                    });
                    $scope.totalAverage = ($scope.average / totalAverage) || 0;
                });
                $scope.isFloat = function() {
                    return $scope.totalAverage === +$scope.totalAverage && $scope.totalAverage !== ($scope.totalAverage|0);
                }
            } renderSortedReviews();
        }
    }
})();
