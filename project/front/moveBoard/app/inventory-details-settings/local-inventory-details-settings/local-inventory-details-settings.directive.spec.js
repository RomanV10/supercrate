describe('local-inventory-details-settings.directive.js: ', () => {
	let rootScope,
		element,
		$scope;

	beforeEach(() => {
		rootScope = $rootScope.$new();
		rootScope.allData = testHelper.loadJsonFile('all-data-local-inventory-details-settings.mock');
		rootScope.nid = '4534';
		let directive = `<local-inventory-details-settings nid='nid' 
							all-data="allData"></local-inventory-details-settings>`;
		element = $compile(directive)(rootScope);
		$scope = element.isolateScope();
	});

	describe('on init', () => {
		it('$scope.localInventoryDetails have to be defined', () => {
			expect($scope.localInventoryDetails).toBeDefined();
		});

		it('$scope.localInventoryDetails should be equals FALSE', () => {
			expect($scope.localInventoryDetails).toBeFalsy();
		});

		it('$scope.changeLocalInventoryDetails have to be defined', () => {
			expect($scope.changeLocalInventoryDetails).toBeDefined();
		});
	});
});
