'use strict';

import './local-inventory-details-settings.styl';

angular
	.module('inventory-details-settings')
	.directive('localInventoryDetailsSettings', localInventoryDetailsSettings);

localInventoryDetailsSettings.$inject = ['datacontext', 'RequestServices'];

function localInventoryDetailsSettings(datacontext, RequestServices) {
	return {
		restrict: 'E',
		scope: {
			nid: '=',
			allData: '='
		},
		template: require('./local-inventory-details-settings.tpl.html'),
		link: localInventoryDetailsSettingsLink,
	};

	function localInventoryDetailsSettingsLink($scope) {
		const DEFAULT_LONG_DISTANCE_SETTING = _.get(datacontext.getFieldData(), 'longdistance.isConfirmedAccountMenuEnabled', true);
		$scope.localInventoryDetails = _.get($scope.allData, 'localInventoryDetails', DEFAULT_LONG_DISTANCE_SETTING);

		$scope.changeLocalInventoryDetails = changeLocalInventoryDetails;

		function changeLocalInventoryDetails() {
			$scope.allData.localInventoryDetails = $scope.localInventoryDetails;
			RequestServices.saveReqData($scope.nid, $scope.allData);
		}
	}
}
