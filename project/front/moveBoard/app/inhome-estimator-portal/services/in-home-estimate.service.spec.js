describe('inHomeEstimateService:', () => {
	let inHomeEstimateService, testHelper;

	beforeEach(inject(function(_inHomeEstimateService_, _testHelperService_) {
		inHomeEstimateService = _inHomeEstimateService_;
		testHelper = _testHelperService_;
	}));

	it('inHomeEstimateService should be defined', () => {
		expect(inHomeEstimateService).toBeDefined();
	});

	describe('prepareRequestView:', () => {
		let initialRequest, resultRequest;
		beforeEach(() => {
			initialRequest = testHelper.loadJsonFile('in-home-estimate-request.mock');
			resultRequest = testHelper.loadJsonFile('inhome-estimate-request.mock')
		});

		it('should be return new object', () => {
			expect(inHomeEstimateService.prepareRequestView(initialRequest)).toEqual(resultRequest);
		})
	});
});