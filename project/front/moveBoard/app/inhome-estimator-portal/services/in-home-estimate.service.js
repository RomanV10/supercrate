'use strict';

angular
	.module('inhome-estimate-portal')
	.factory('inHomeEstimateService', inHomeEstimateService);

inHomeEstimateService.$inject = ['$q', 'apiService', 'moveBoardApi'];

function inHomeEstimateService($q, apiService, moveBoardApi) {
	return {
		getHomeEstimatorRequests: getHomeEstimatorRequests,
		prepareRequestView: prepareRequestView
	};
	
	function getHomeEstimatorRequests(param) {
		let deferred = $q.defer();

		apiService.postData(moveBoardApi.inHomeEstimatePortal.getInHomeEstimateRequests, param)
			.then(resolve => {
				deferred.resolve({
					requests: resolve.data.items,
					meta: resolve.data.meta
				});
			}, () => {
				deferred.reject();
			});

		return deferred.promise;
	}

	function prepareRequestView(request) {
		return {
			id: request.nid,
			time: `${request.home_estimate_actual_start.value} - ${request.home_estimate_start_time.value}`,
			moveSize: request.move_size.value,
			userName: request.name,
			phone: request.phone,
			from: `${request.field_moving_from.thoroughfare}, ${request.field_moving_from.locality}, ${request.field_moving_from.postal_code}`,
			to: `${request.field_moving_to.thoroughfare}, ${request.field_moving_to.locality}, ${request.field_moving_to.postal_code}`,
			date: moment(request.home_estimate_date.value).format('MMMM DD, YYYY')
		}
	}
}
