'use strict';

import './inhome-estimate-request.styl';

angular
	.module('inhome-estimate-portal')
	.directive('inhomeEstimateRequest', inhomeEstimateRequest);

inhomeEstimateRequest.$inject = ['inHomeEstimateService', '$state', '$rootScope'];

function inhomeEstimateRequest(inHomeEstimateService, $state, $rootScope) {
	return {
		restrict: 'E',
		scope: {
			request: '=',
			jobType: '=',
		},
		template: require('./inhome-estimate-request.tpl.pug'),
		link: inhomeEstimateLink
	};

	function inhomeEstimateLink($scope) {
		$scope.busy = false;
		$scope.displayingRequest = inHomeEstimateService.prepareRequestView($scope.request);

		$scope.openRequest = openRequest;
		$scope.openInventory = openInventory;

		function openRequest (request) {
			$rootScope.$broadcast('show_menu', false);
			$state.go('inhome.estimator-request', {
				id: request.id
			});
		}

		function openInventory(request) {
			$rootScope.$broadcast('show_menu', false);
			$state.go('inhome.estimator-inventory', {
				id: request.id
			});
		}
	}
}