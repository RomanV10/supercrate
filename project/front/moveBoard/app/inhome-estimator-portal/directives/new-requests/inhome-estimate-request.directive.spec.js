describe('inhomeEstimateRequest', () => {
	let $compile,
		$rootScope,
		testHelperService,
		element,
		$scope,
		elementScope,
		request,
		$httpBackend;

	beforeEach(inject((_$compile_, _$rootScope_, _testHelperService_, _$httpBackend_) => {
		$httpBackend = _$httpBackend_;
		testHelperService = _testHelperService_;
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		$compile = _$compile_;
		request = testHelperService.loadJsonFile('in-home-estimate-request.mock')
		$scope.request = request;
		element = $compile(angular.element("<inhome-estimate-request request='request' job-type='1'></inhome-estimate-request>"))($scope);
		elementScope = element.isolateScope();
	}));

	describe('init directive', () => {
		it('request should be defined', () => {
			expect(elementScope.request).toBeDefined();
		});

		it('all variables in scope should be falsy on init', () => {
			expect(elementScope.busy).toBeFalsy();
		});
	});
});