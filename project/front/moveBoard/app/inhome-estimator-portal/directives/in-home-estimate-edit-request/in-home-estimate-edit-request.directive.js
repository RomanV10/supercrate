'use strict';

import '../../controller/styles/in-home-estimate-request.styl';
import '../../controller/styles/request-tab.styl';

angular
	.module('inhome-estimate-portal')
	.directive('inHomeEstimateEditRequest', inHomeEstimateEditRequest);

inHomeEstimateEditRequest.$inject = [];

function inHomeEstimateEditRequest() {
	return {
		restrict: 'E',
		scope: {
			initialRequest: '=',
			requests: '=',
			superResponse: '=',
			isDelivery: '=',
			modalInstance: '='
		},
		template: require('./in-home-estimate-edit-request.pug'),
		controller: 'InHomeEstimateEditRequestModal',
		controllerAs: 'vm',
		transclude: true
	};
}