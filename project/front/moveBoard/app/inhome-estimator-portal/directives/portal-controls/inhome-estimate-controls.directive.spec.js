describe('inhomeEstimateControls:', () => {
	let $compile,
		$rootScope,
		testHelperService,
		element,
		$scope,
		elementScope,
		$httpBackend;

	beforeEach(inject((_$compile_, _$rootScope_, _testHelperService_, _$httpBackend_) => {
		$httpBackend = _$httpBackend_;
		testHelperService = _testHelperService_;
		$rootScope = _$rootScope_;
		$scope = $rootScope.$new();
		$compile = _$compile_;
		$scope.meta = testHelperService.loadJsonFile('meta-data.mock');
		element = $compile(angular.element('<inhome-estimate-controls meta="meta"></inhome-estimate-controls>'))($scope);
		elementScope = element.isolateScope();
	}));

	describe('init directive:', () => {
		it('meta should be defined', () => {
			expect(elementScope.meta).toBeDefined();
		});

		it('check default values of variables', () => {
			expect(elementScope.settingsAvailable).toBeFalsy();
			expect(elementScope.sortDirection).toBe('ASC');
			expect(elementScope.sortDate).toBe('field_home_estimate_date');
		});
	});

	describe("Directive's methods:", () => {
		describe('swipeUp:', () => {
			beforeEach(() => {
				elementScope.swipeUp();
			});
			it('settingsAvailable should be "true"', () => {
				expect(elementScope.settingsAvailable).toBeTruthy();
			});
		});

		describe('swipeDown:', () => {
			beforeEach(() => {
				elementScope.swipeDown();
			});
			it('settingsAvailable should be "false"', () => {
				expect(elementScope.settingsAvailable).toBeFalsy();
			});
		});

		describe('showSortingSettings:', () => {
			let prevState;
			beforeEach(() => {
				prevState = elementScope.settingsAvailable;
				elementScope.showSortingSettings();
			});
			it('after call settingsAvailable should be not to equal previous state', () => {
				expect(elementScope.settingsAvailable).not.toEqual(prevState);
			});
		});
	});
});