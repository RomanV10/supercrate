'use strict';

import './inhome-estimate-controls.styl';

angular
	.module('inhome-estimate-portal')
	.directive('inhomeEstimateControls', inhomeEstimateControls);

inhomeEstimateControls.$inject = ['$window', '$state'];

function inhomeEstimateControls($window, $state) {
	return {
		restrict: 'E',
		scope: {
			meta: '='
		},
		template: require('./inhome-estimate-controls.tpl.pug'),
		link: inhomeEstimateControlsLink
	};

	function inhomeEstimateControlsLink($scope, elem) {

		const CURRENT_STATE = $state.current.url;
		let jobsType = 1;

		if (CURRENT_STATE.contains('closed')) {
			jobsType = 2;
		}

		$scope.settingsAvailable = false;
		$scope.sortDirection = 'ASC';
		$scope.sortDate = 'field_home_estimate_date';

		$scope.showSortingSettings = showSortingSettings;
		$scope.changeSorting = changeSorting;
		$scope.changePage = changePage;
		$scope.swipeUp = swipeUp;
		$scope.swipeDown = swipeDown;

		function swipeUp() {
			$scope.settingsAvailable = true;
			$scope.$emit('control-panel-is-open', $scope.settingsAvailable)
		}
		
		function swipeDown() {
			$scope.settingsAvailable = false;
			$scope.$emit('control-panel-is-open', $scope.settingsAvailable);
		}

		function showSortingSettings() {
			$scope.settingsAvailable = !$scope.settingsAvailable;
			$scope.$emit('control-panel-is-open', $scope.settingsAvailable);
		}

		function changeSorting() {
			$scope.$emit('change-sorting.in-home-estimate', {
				type: jobsType,
				sorting: {
					[$scope.sortDate]: $scope.sortDirection
				}
			});
		}

		function changePage(page) {
			$scope.$emit('change-sorting.in-home-estimate', {
				type: jobsType,
				currentPage: page
			});
		}

		// hide on lost  focus
		angular.element($window).on('click', e => {
			if (!elem[0].contains(e.target)) {
				$scope.settingsAvailable = false;
				$scope.$emit('control-panel-is-open', $scope.settingsAvailable);
			}
		});

		$scope.$watch('meta', (current) => {
			$scope.disabledPrev = current.currentPage === 1;
			$scope.disabledNext = current.currentPage >= current.pageCount;
		});
	}
}
