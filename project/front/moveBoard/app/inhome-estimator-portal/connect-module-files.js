require('./inhome-estimator-portal.module');
require('./config.route');
require('./controller/inhome-estimate-portal.controller');
require('./controller/inhome-estimate-request-open.controller');
require('./controller/inhome-estimate-inventory-open.controller');
require('./directives/new-requests/inhome-estimate-request.directive');
require('./directives/portal-controls/inhome-estimate-controls.directive');
require('./services/in-home-estimate.service');
require('./directives/in-home-estimate-edit-request/in-home-estimate-edit-request.directive');
require('./controller/in-home-estimate-edit-request-modal.controller');