'use strict';

import { REQUEST_STATUS_BORDER } from '../constants/request-status-border';

angular
	.module('inhome-estimate-portal')
	.controller('InHomeEstimateEditRequestModal', InHomeEstimateEditRequestModal);

InHomeEstimateEditRequestModal.$inject = ['$scope', '$rootScope', '$window', '$controller', 'datacontext'];

function InHomeEstimateEditRequestModal($scope, $rootScope, $window, $controller, datacontext) {
	$rootScope.$broadcast('show_menu', false);

	angular.extend(this, $controller('EditRequestPageBaseController', {$scope: $scope}));
	const basicSettings = angular.fromJson(datacontext.getFieldData().basicsettings);

	const STATUSES_FOR_SHOW_GO_BUTTON = ['2', '3'];
	const WEB_SITE_URL = basicSettings.client_page_url;

	changeRequestBorder();

	$scope.showReservationBtn = false;

	$scope.openReservationPage = openReservationPage;

	function openReservationPage() {
		$rootScope.$broadcast('redirect-user-to-confirmation-page');
		let url = `${WEB_SITE_URL}/account/#/request/${$scope.request.nid}/reservation`;
		$window.open(url, '_self');
	}

	$scope.$watch('request.status.old', (current) => {
		$scope.showReservationBtn = _.indexOf(STATUSES_FOR_SHOW_GO_BUTTON, current) >= 0;
	});

	$scope.canedit = true;

	$scope.$watch('request.status.raw', (current, original) => {
		if (current != original) {
			changeRequestBorder();
		}
	});

	function changeRequestBorder() {
		angular.element('.inhome-estimate-request-portal').css({
			border: `10px solid ${REQUEST_STATUS_BORDER[$scope.request.status.raw]}`,
		})
	}
}
