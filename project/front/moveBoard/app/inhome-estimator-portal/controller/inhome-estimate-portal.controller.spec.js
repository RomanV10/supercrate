describe('InhomeEstimateController:', () => {
	let $controller,
		$rootScope,
		$scope,
		InhomeEstimateController,
		TestHelper,
		$httpBackend,
		moveBoardApi;

	function spyOnInhomeEstimateResolve(service) {
		spyOn(service, 'getHomeEstimatorRequests').and.callFake(() => {
			return {
				then: (resolve, reject) => {
					resolve(TestHelper.loadJsonFile('inhome-estimate-response.mock'));
					reject();
					return {
						finally: (finalCallback) => {
							finalCallback();
						}
					};
				},

			};
		});
	}

	beforeEach(inject((_$controller_, _$rootScope_, _testHelperService_, _$httpBackend_, _moveBoardApi_, _inHomeEstimateService_) => {
		$controller = _$controller_;
		$rootScope = _$rootScope_;
		TestHelper = _testHelperService_;
		$httpBackend = _$httpBackend_;
		moveBoardApi = _moveBoardApi_;
		$scope = $rootScope.$new();

		spyOnInhomeEstimateResolve(_inHomeEstimateService_);

		InhomeEstimateController = $controller('InhomeEstimateController', {
			$scope: $scope
		});
	}));

	describe('Init InhomeEstimateController', () => {
		it('controller should be defined', () => {
			expect(InhomeEstimateController).toBeDefined();
		});
	});

	describe('getRequests', () => {
		it('inHomeEstimateRequests should not be equal 0', () => {
			expect(InhomeEstimateController['inHomeEstimateRequests'].length).not.toBe(0);
		});

		it('meta should be contain data', () => {
			expect(InhomeEstimateController['meta']).toEqual({
				currentPage: 1,
				perPage: 25,
				pageCount: 1,
				totalCount: 2
			})
		});

		it('jobsType should be equal to 1', () => {
			expect(InhomeEstimateController['jobsType']).toBe(1);
		})
	})
});