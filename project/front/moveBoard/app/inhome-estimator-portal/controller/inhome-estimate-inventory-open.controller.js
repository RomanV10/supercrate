'use strict';

angular
	.module('inhome-estimate-portal')
	.controller('InhomeEstimateInventoryOpen', InhomeEstimateInventoryOpen);

InhomeEstimateInventoryOpen.$inject = ['$state', 'logger', 'RequestServices', '$rootScope', '$scope'];

function InhomeEstimateInventoryOpen($state, logger, RequestServices, $rootScope, $scope) {
	let vm = this;
	vm.busy = true;
	vm.modalInstance = {dismiss: closeEditRequest};
	vm.closeEditRequest = closeEditRequest;
	let requestId = $state.params.id;
	$rootScope.$broadcast('show_menu', false);

	RequestServices
		.getSuperRequest(requestId)
		.then(function (data) {
			$rootScope.$broadcast('show_menu', false);
			vm.requests = data.requests_day;
			vm.initialRequest = data.request;
			vm.request = data.request;
			vm.superResponse = data;
			vm.listInventories = _.get(vm, 'request.inventory.inventory_list', []);
			vm.busy = false;
			$rootScope.inventoryRequestData = data;
		}, function (reason) {
			vm.busy = false;
			logger.error(reason, reason, "Error");
		});

	$scope.$on('onInventorySaved', onInventorySaved);

	function onInventorySaved() {
		$state.go('inhome.back_to_request', {
			id: vm.request.nid
		});
	}

	function closeEditRequest() {}
}
