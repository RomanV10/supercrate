'use strict';

import './styles/in-home-estimate-request.styl';

angular
	.module('inhome-estimate-portal')
	.controller('InhomeEstimateRequestOpenController', InhomeEstimateRequestOpenController);

InhomeEstimateRequestOpenController.$inject = ['$state', 'logger', 'RequestServices', '$rootScope', '$stateParams', '$scope'];

function InhomeEstimateRequestOpenController($state, logger, RequestServices, $rootScope, $stateParams, $scope) {
	$rootScope.$broadcast('show_menu', false);
	let vm = this;
	vm.busy = true;
	vm.globallLoading = true;
	vm.redirectLoading = false;
	vm.modalInstance = {
		dismiss: closeEditRequest
	};
	let requestId = $state.params.id;

	RequestServices
		.getSuperRequest(requestId)
		.then(data => {
			vm.requests = data['requests_day'];
			vm.initialRequest = data.request;
			vm.initialRequest.initRequest = $stateParams.initRequest;
			vm.listInventories = _.get(vm, 'initialRequest.inventory.inventory_list', []);
			vm.superResponse = data;
		}, reason => {
			logger.error(reason, reason, "Error");
		}).finally(() => vm.busy = false);

	function closeEditRequest() {
		$state.go('inhome.estimator-jobs');
		$rootScope.$broadcast('show_menu', true);
	}

	$scope.$on('loading-request-is-stopped', () => {
		vm.globallLoading = false;
	});

	$scope.$on('redirect-user-to-confirmation-page', () => {
		vm.redirectLoading = true;
	})
}
