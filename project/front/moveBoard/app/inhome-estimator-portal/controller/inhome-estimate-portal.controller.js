'use strict';

import './styles/loading.styl';
import './styles/root.styl';

angular
	.module('inhome-estimate-portal')
	.controller('InhomeEstimateController', InhomeEstimateController);

InhomeEstimateController.$inject = ['inHomeEstimateService', '$scope', '$state', '$rootScope'];

function InhomeEstimateController(inHomeEstimateService, $scope, $state, $rootScope) {
	let vm = this;
	vm.busy = false;
	vm.isOpenPanel = false;
	vm.inHomeEstimateRequests = [];
	vm.jobsType = 1;
	$rootScope.$broadcast('show_menu', true);

	// for defined in unit tests
	vm.init = init;
	vm.getRequests = getRequests;
	vm.getOptimisticResponse = getOptimisticResponse;
	vm.getError = getError;

	const CURRENT_STATE = $state.current.url;
	let jobsType = 1;

	if (CURRENT_STATE.contains('closed')) {
		jobsType = 2;
	}

	function init() {
		vm.busy = true;
		getRequests({
			type: jobsType
		});
	}

	function getRequests(data) {
		inHomeEstimateService.getHomeEstimatorRequests(data)
			.then(
				resolve => getOptimisticResponse(resolve),
				() => getError())
			.finally(() => vm.busy = false);
	}

	function getOptimisticResponse(response) {
		vm.inHomeEstimateRequests = response.requests;
		vm.meta = response.meta;
		vm.jobsType = jobsType;
	}

	function getError() {
		toastr.error('Error', 'Something going wrong...');
	}

	$scope.$on('change-sorting.in-home-estimate', (ev, sortingData) => {
		vm.busy = true;
		getRequests(sortingData);
	});

	$scope.$on('control-panel-is-open', (ev, isOpenPanel) => {
		vm.isOpenPanel = isOpenPanel;
	});

	init();
}