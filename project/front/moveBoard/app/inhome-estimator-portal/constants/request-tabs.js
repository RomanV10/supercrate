export const TABS = [
	{
		name: 'Request #',
		url: 'app/requests/services/templates/editFormTemplate.html?3',
		selected: true
	},
	{
		name: 'Inventory',
		url: 'app/requests/services/templates/inventoryRequestForm.html',
		selected: false
	},
	{
		name: 'Details',
		url: 'app/requests/services/templates/detailsRequestForm.html',
		selected: false
	},
	{
		name: 'Messages',
		url: 'app/requests/services/templates/messagesRequestForm.html',
		selected: false
	},
	{
		name: 'Client',
		url: 'app/requests/services/templates/clientRequestForm.html?3',
		selected: false
	},
	{
		name: 'Log',
		url: 'app/requests/services/templates/logRequestForm.html',
		selected: false
	},
	{
		name: 'Options',
		url: 'app/requests/services/templates/optionsTab.html',
		selected: false
	},
	{
		name: 'Settings',
		url: 'app/requests/services/templates/request_settings.html',
		selected: false
	}
];