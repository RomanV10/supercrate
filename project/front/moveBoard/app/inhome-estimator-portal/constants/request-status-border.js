export const REQUEST_STATUS_BORDER = {
	1: 'rgba(255, 165, 0, .7)', // PENDING
	2: 'rgba(135, 196, 243, .7)', // NOT CONFIRMED
	3: 'rgba(19, 199, 19, .7)', // CONFIRMED
	4: 'rgba(241, 196, 15, .7)', // INHOME ESTIMATE
	5: 'rgba(247, 126, 126, .7)', // CANCELLED
	6: 'rgba(189, 195, 199, .7)', // FLAT RATE (IVENTORY NEEDED)
	7: 'rgba(149, 165, 166, .7)', // FLAT RATE (PROVIDE OPTIONS)
	9: 'rgba(127, 140, 141, .7)', // FLAT RATE (WAIT OPTIONS)
	10: 'rgba(231, 76, 60, .7)', // EXPIRED
	11: 'rgba(44, 62, 80, .7)', // WE ARE NOT AVAILABLE
	12: 'rgba(192, 57, 43, .7)', // SPAM
	13: 'rgba(142, 68, 173, .7)', // DEAD LEAD
	14: 'rgba(236, 240, 241, .7)', // ARCHIVE
};
