'use strict';

angular
	.module('app')
	.config(["$stateProvider", InhomeProviderRoute]);

function InhomeProviderRoute ($stateProvider) {
	$stateProvider
		.state('inhome', {
			url: '/inhome-estimator',
			abstract: true,
			template: '<ui-view/>'
		})
		.state('inhome.estimator-jobs', {
			url: '/jobs',
			template: require('./controller/templates/inhome-estimator.pug'),
			title: 'In-home Estimate Portal',
			controller: 'InhomeEstimateController',
			controllerAs: 'vm',
			data: {
				permissions: {
					only: ['home-estimator']
				}
			}
		})
		.state('inhome.estimator-closed-jobs', {
			url: '/closed',
			template: require('./controller/templates/inhome-estimator.pug'),
			title: 'In-home Estimate Portal',
			controller: 'InhomeEstimateController',
			controllerAs: 'vm',
			data: {
				permissions: {
					only: ['home-estimator']
				}
			}
		})
		.state('inhome.estimator-request', {
			url: '/request/:id',
			template: require('./controller/templates/inhome-estimate-request-open.tpl.pug'),
			title: 'In-home Estimate Request',
			controller: 'InhomeEstimateRequestOpenController',
			controllerAs: 'vm',
			data: {
				permissions: {
					only: ['home-estimator']
				}
			}
		})
		.state('inhome.estimator-inventory', {
			url: '/request-inventory/:id',
			template: require('./controller/templates/inhome-estimate-inventory-open.tpl.pug'),
			title: 'In-home Estimate Inventory',
			controller: 'InhomeEstimateInventoryOpen',
			controllerAs: 'vm',
			data: {
				permissions: {
					only: ['home-estimator']
				}
			}
		})
		.state('inhome.back_to_request', {
			url: '/request_/:id',
			template: require('./controller/templates/inhome-estimate-request-open.tpl.pug'),
			title: 'In-home Estimate Request',
			params: {
				initRequest: false
			},
			controller: 'InhomeEstimateRequestOpenController',
			controllerAs: 'vm',
			data: {
				permissions: {
					only: ['home-estimator']
				}
			}
		});
}
