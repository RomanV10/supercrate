(function () {
    'use strict';

    angular
        .module('app.dispatch')
        .directive('employersDTable', employersDTable);

    employersDTable.$inject = ['PayrollService', 'PayrollBuilder', 'PayrollConfigs', '$interval'];

    function employersDTable(PayrollService, PayrollBuilder, PayrollConfigs, $interval) {
        return {
            restrict: 'AE',
	        template: require('./templates/departmentDTable.html'),
            link: linkFn
        };

        function linkFn(scope, element, attrs) {

            var FQ = 20000;
            var intervalInstance = null;

            scope.busy = true;
            scope.clicked = '';

            scope.dataTbl = {};
            scope.columns = PayrollConfigs.columns.departments;

            scope.selectDepartment = selectDepartment;

            intervalInstance = $interval(function () {
                getByDate();
            }, FQ);

            function getByDate() {
                var filtering = {
                    date_from: moment(new Date(scope.dateRange.from)).format('M/D/YYYY'),
                    date_to: moment(new Date(scope.dateRange.to)).format('M/D/YYYY'),
                    // type: 0
                };
                PayrollService.getPayrollByDepartment(filtering)
                    .then(function (response) {
                        scope.dataTbl = response;
                    })
                    .finally(function () {
                        scope.busy = false;
                    });
            }

            getByDate();

            function selectDepartment(data) {
                scope.clicked = data.role_name;
                scope.dTable = 'employers';
            }

            scope.$on('$destroy', function () {
                $interval.cancel(intervalInstance);
            });

        }

    }


})();




