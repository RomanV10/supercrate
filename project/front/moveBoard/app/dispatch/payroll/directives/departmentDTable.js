(function () {
    'use strict';

    angular
        .module('app.dispatch')
        .directive('departmentDTable', departmentDTable);

    departmentDTable.$inject = ['PayrollService', 'PayrollConfigs', '$interval', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$rootScope'];

    function departmentDTable(PayrollService, PayrollConfigs, $interval, DTOptionsBuilder, DTColumnDefBuilder, $rootScope) {
        return {
            restrict: 'AE',
	        template: require('./templates/departmentDTable.html'),
            link: linkFn
        };

        function linkFn(scope, element, attrs) {

            var FQ = 26000;
            var intervalInstance = null;
            var dateRange = {};

            scope.busy = true;
            scope.clicked = '';

            scope.dataTbl = {};
            scope.columns = scope.$parent.mainColumns.departments;
            scope.departments = {};
            scope.selectDepartment = selectDepartment;
            // scope.refresh = refresh;
            scope.dtOptions = DTOptionsBuilder
                                .newOptions()
                                .withDisplayLength(100)
                                .withOption('order', [0, 'asc']) // Sort by department name
                                .withOption('bFilter', false)
                                .withOption('bInfo', false)
                                .withOption('paging', false)

            init();

            scope.$on(PayrollConfigs.events.GET_BY_DATE_DEPARTMENTS, function () {
                scope.busy = true;
                // scope.dataTbl = null;
                init();
            });

            scope.dtInstance = {};

            function init() {
                dateRange = {
                    from: _.clone(scope.dateRange.from),
                    to: _.clone(scope.dateRange.to)
                };

                _getByDate();
            }


            function _getByDate() {
                var filtering = {
                    date_from: moment(new Date(dateRange.from)).format('YYYY-MM-DD'),
                    date_to: moment(new Date(dateRange.to)).format('YYYY-MM-DD'),
                    // type:  Number(_.clone(scope.paidRadio))
                };
                scope.dataTbl = null;
                scope.departments = null;
                PayrollService.getPayrollByDepartment(filtering)
                    .then(function (response) {
                        if (response[0] == false) {
                            scope.busy = false;
                            toastr.error("Payroll of departments doesn't exist", "Error");
                        } else {
                            var departments = {};
                            departments.total = {
                                department: '',
                                jobs_count: 0,
                                hours: 0,
                                materials: 0,
                                m_extra: 0,
                                tip: 0,
                                total: 0,
                                paid: 0
                            };

                            var dataTbl = response;

                            _.each(dataTbl, function (row) {
                                row.department = row.role_name;
                                row.m_extra = row.extra;
                                departments.total.jobs_count += row.jobs_count;
                                departments.total.hours += row.hours;
                                departments.total.materials += row.materials;
                                departments.total.m_extra += row.m_extra;
                                departments.total.tip += row.tip;
                                departments.total.total += row.total;
                                departments.total.paid += row.paid;
                                delete row.extra;
                                delete row.hours_pay;
                                delete row.role_name;
                                delete row.grand_total;
                            });

                            departments.total.jobs_count = departments.total.jobs_count.toFixed(2);
                            departments.total.hours = departments.total.hours.toFixed(2);
                            departments.total.materials = departments.total.materials.toFixed(2);
                            departments.total.m_extra = departments.total.m_extra.toFixed(2);
                            departments.total.tip = departments.total.tip.toFixed(2);
                            departments.total.total = departments.total.total.toFixed(2);
                            departments.total.paid = departments.total.paid.toFixed(2);
                            scope.dataTbl = dataTbl;
                            scope.departments = departments;

                            $rootScope.$broadcast('payroll_table_loaded', scope.departments);
                        }
                    })
                    .finally(function () {
                        scope.busy = false;
                    });
            }

            function selectDepartment(key, data) {
                if (scope.clicked != key) {
                    scope.clicked = key;
                } else {
                    scope.busy = true;
                    scope.$parent.roleWorkers = data.department;
                    scope.$parent.keyRoleWorkers = key;
                    scope.$parent.dTable = 'workers';
                }
            }


            scope.$on('$destroy', function () {

            });

            scope.isShowDepartment = function (dataObj) {
                return dataObj.total != 0 || dataObj.jobs_count != 0;
            };
        }
    }


})();




