(function () {
    'use strict';

    angular
        .module('app.dispatch')
        .directive('workersDTable', workersDTable);

    workersDTable.$inject = ['PayrollService', 'PayrollConfigs', '$timeout', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$rootScope'];

    function workersDTable(PayrollService, PayrollConfigs, $timeout, DTOptionsBuilder, DTColumnDefBuilder, $rootScope) {
        return {
            restrict: 'AE',
	        template: require('./templates/workersDTable.html'),
            link: linkFn
        };

        function linkFn(scope, element, attrs) {
            var FQ = 26000;
            var intervalInstance = null;
            var dateRange = null;

            scope.busy = true;
            scope.clicked = '';
            scope.workers = {};
            scope.workersTbl = {};
            scope.columns = scope.$parent.mainColumns.workers;
            scope.selectUser = selectUser;
            scope.setUser = setUser;
            scope.getPayrollByRole = getPayrollByRole;
            // scope.refresh = refresh;
            scope.dtOptions = DTOptionsBuilder
                .newOptions()
                .withDisplayLength(100)
                .withOption('order', [1, 'asc'])

            init();

            scope.$on(PayrollConfigs.events.GET_BY_DATE_WORKERS, function () {
                // scope.workersTbl = null;
                scope.busy = true;
                init();
            });

            // // scope.$on('columns.updated', scope.refresh);

            // function refresh(){
            //     // scope.workersTbl.reloadData();
            //     // scope.workers.reloadData();
            // }

            scope.dtColumnDefs = [
                DTColumnDefBuilder
                    .newColumnDef(0)
                    .notSortable()
            ];

            function init() {
                dateRange = {
                    from: _.clone(scope.dateRange.from),
                    to: _.clone(scope.dateRange.to)
                };

                getPayrollByRole(scope.$parent.keyRoleWorkers);
            }


            function getPayrollByRole(value) {
                var role = {
                    date_from: moment(new Date(dateRange.from)).format('YYYY-MM-DD'),
                    date_to: moment(new Date(dateRange.to)).format('YYYY-MM-DD'),
                    role: Number(value),
                    type: Number(scope.paidRadio)
                };

                scope.workersTbl = null;
                scope.workers.total = null;
                PayrollService.getPayrollByRole(role)
                    .then(function (response) {
                        if (response[0] == false || response.length == 0) {
                            scope.busy = false;
                            toastr.error("Payroll of this department doesn't exist", "Error");
                        } else {
                            var workers = response;
                            var total = {
                                a_employee_selected: '',
                                a_name: '',
                                count_jobs: 0,
                                hours: 0,
                                materials: 0,
                                hours_pay: 0,
                                m_extra: 0,
                                tip: 0,
                                total: 0,
                                paid: 0
                            };

                            _.each(workers, function (worker) {
                            	if (!worker.user_info) return;

                                worker.a_name = worker.user_info.field_user_first_name.und[0].value + ' ' + worker.user_info.field_user_last_name.und[0].value;
                                worker.count_jobs = worker.jobs_count;
                                worker.m_extra = worker.extra;
                                worker.a_employee_selected = false;
                                delete worker.extra;
                                delete worker.jobs_count;
                                delete worker.user_info;
                                delete worker.grand_total;
                                total.count_jobs += worker.count_jobs;
                                total.hours += parseFloat(worker.hours.toFixed(2));
                                total.materials += parseFloat(worker.materials.toFixed(2));
                                total.hours_pay += worker.hours_pay;
                                total.m_extra += parseFloat(worker.m_extra.toFixed(2));
                                total.tip += worker.tip;
                                total.total += parseFloat(worker.total.toFixed(2));
                                total.paid += parseFloat(worker.paid.toFixed(2));
                            });

                            total.tip = total.tip.toFixed(2);
                            total.total = total.total.toFixed(2);

                            scope.workersTbl = workers;
                            scope.workers = {};
                            scope.workers.total = total;

                            $rootScope.$broadcast('payroll_table_loaded', scope.workers);
                        }

                        scope.busy = false;
                    })
                    .finally(function () {
                        scope.$parent.busy = false;
                    });
            }


            function selectUser(key, name) {
                if (scope.clicked != key) {
                    scope.clicked = key;
                } else {
                    scope.busy = true;
                    scope.$parent.userUid = key;
                    scope.$parent.userName = name.a_name;
                    scope.$parent.dTable = 'user';
                }
            }

            function setUser(key, name, isSet) {
                if (isSet) {
                    scope.$parent.userUid = key;
                    scope.$parent.userName = name.a_name;
                } else {
                    scope.$parent.userUid = "";
                    scope.$parent.userName = "";
                }
            }

            scope.$on('$destroy', function () {

            });
        }
    }
})();




