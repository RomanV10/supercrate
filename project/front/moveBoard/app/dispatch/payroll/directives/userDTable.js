'use strict';

angular
	.module('app.dispatch')
	.directive('userDTable', userDTable);

/*@ngInject*/
function userDTable(PayrollService, DTOptionsBuilder, PayrollConfigs, DispatchService, CalculatorServices, PaymentServices, InventoriesServices, sharedRequestServices, common, datacontext, DTColumnDefBuilder, $rootScope, $state, additionalServicesFactory, $filter, openRequestService) {
	return {
		restrict: 'AE',
		template: require('./templates/userDTable.html'),
		link: linkFn
	};

	function linkFn(scope) {
		let dateRange = null;

		scope.busy = true;
		scope.clicked = '';
		scope.userTbl = {};
		scope.columns = scope.$parent.mainColumns.user;
		scope.paymentOpt = {
			'0': 'Check',
			'1': 'Cash'
		};
		let fieldData = datacontext.getFieldData();
		let settings = angular.fromJson(fieldData.basicsettings);
		let totals = {};
		scope.contractTypes = fieldData.enums.contract_type;
		scope.dtOptions = DTOptionsBuilder
			.newOptions()
			.withDisplayLength(1000)
			.withOption('order', [3, 'asc']) // Sort by move date
			.withOption('bFilter', false);


		scope.dtColumnDefs = [
			DTColumnDefBuilder
				.newColumnDef(3)
				.withOption('type', 'date')
				.renderWith(renderTable),
			DTColumnDefBuilder
				.newColumnDef(0)
				.notSortable()
		];

		var utcOfset = Math.abs(moment.parseZone(new Date()).utcOffset() * 60);

		scope.dtOptionsPaycheck = DTOptionsBuilder
			.newOptions()
			.withDisplayLength(100)
			.withOption('order', [3, 'asc']) // Sort by move date
			.withOption('bFilter', false)
			.withOption('bInfo', false)
			.withOption('paging', false);

		scope.dtColumnDefsPaycheck = [
			DTColumnDefBuilder
				.newColumnDef(3)
				.withOption('type', 'date')
				.renderWith(renderTable)
		];

		function renderTable(data, type) {
			if (_.isEqual(type, 'sort')) {
				let value = data.match(/(\d{1,2}\/\d{1,2}\/\d{4})/);

				if (_.isNull(value)) {
					return 0;
				} else {
					return value[0];
				}
			}

			return data;
		}

		var currentUser = findWorkerByUID(scope.$parent.userUid);
		scope.userHourlyRate = getHourlyRateUserCommission();
		scope.jobClicked = {};

		scope.getPayrollByUser = getPayrollByUser;
		scope.editRequest = editRequest;
		scope.selectAll = selectAll;
		scope.calculateTotalBalance = calculateTotalBalance;
		scope.calculateAddServices = calculateAddServices;
		scope.calculatePackingServices = calculatePackingServices;
		scope.calculateTime = calculateTime;
		scope.updateRate = updateRate;
		scope.setNid = setNid;
		scope.isEmpty = _.isEmpty;
		scope.dtInstance = {};
		var removedJobs = {};

		scope.$watch("userUid", reInit);

		scope.$on(PayrollConfigs.events.GET_BY_DATE_USER, reInit);

		function reInit () {
			scope.busy = true;
			init();
		}

		scope.$on('request.updated', handlerUpdateRequest);

		function handlerUpdateRequest(event, data) {
			var nid = data.nid;

			if (scope.userTbl.copyJobs[nid]) {
				var job = scope.userTbl.copyJobs[nid];
				job.retrieve = angular.copy(data);
				job.delivery = data.field_moving_to.administrative_area;
				job.job_type = data.service_type.value;
				job.move_date = data.date.raw.toString();
				job.pickup = data.field_moving_from.administrative_area;
				job.extra = getRequestExtraService(data);
				job.material = getRequestMaterials(data);

				initPayrollUserTable();
			}
		}

		function getRequestExtraService(request) {
			var sum = sumExtraServices(request);
			var result = calculateCommission(request, sum, PayrollConfigs.commissions.EXTRASCOMMISSION);
			return result;
		}

		function sumExtraServices(request) {
			var result = 0;

			if (!_.isUndefined(request.request_all_data)
				&& !_.isUndefined(request.request_all_data.invoice)
				&& !_.isUndefined(request.request_all_data.invoice.extraServices)) {

				var extra = {};

				if (_.isArray(request.request_all_data.invoice.extraServices)) {
					extra = request.request_all_data.invoice.extraServices;
				} else {
					extra = angular.fromJson(request.request_all_data.invoice.extraServices);
				}

				_.forEach(extra, function (extraService, property) {
					if (extraService.hasOwnProperty('extra_services')) {
						if (_.isArray(extraService.extra_services)) {
							_.forEach(extraService.extra_services, function (extra_service, property) {
								if (!_.isUndefined(extra_service.services_default_value)) {
									result += extra_service.services_default_value;
								}
							})
						} else {
							result += extraService.extra_services.services_default_value;
						}
					}
				})
			}

			return result;
		}

		function calculateCommission(request, total, idCommission) {
			var result = 0;
			var userExtraCommission = getUserCommission(scope.$parent.userUid, scope.$parent.users, idCommission);

			if (scope.userCurrentTbl.jobs_info[request.nid] && scope.userCurrentTbl.jobs_info[request.nid].commissions && scope.userCurrentTbl.jobs_info[request.nid].commissions.hasOwnProperty(idCommission)) {
				var payrollCommisions = scope.userCurrentTbl.jobs_info[request.nid].commissions[idCommission].rate;
				var payrollExtraTotal = scope.userCurrentTbl.jobs_info[request.nid].commissions[idCommission].for_commission;

				if (payrollCommisions
					&& payrollExtraTotal) {

					result = (payrollCommisions / 100) * payrollExtraTotal;
				}
			} else if (userExtraCommission) {
				result = (userExtraCommission.input / 100) * total;
			}

			return result;
		}

		function getUserCommission(uid, users, idCommission) {
			var user = 0;

			_.each(users, function (currentUsers, index) {
				_.each(currentUsers, function (currentUser, currentUid) {
					if (uid == currentUid) {
						user = currentUser;
					}
				});
			});

			if (user
				&& user.settings
				&& user.settings.rateCommission) {

				var result = 0;

				_.each(user.settings.rateCommission, function (commission, index) {
					if (getIdCommissionByName(commission.option) == idCommission) {
						result = commission;
					}
				});

				return result;
			}

			return undefined;
		}

		function getIdCommissionByName(name) {
			switch (name) {
				case 'Hourly Rate':
					return PayrollConfigs.commissions.HOURLYRATE;
				case 'Extras Commission':
					return PayrollConfigs.commissions.EXTRASCOMMISSION;
				case 'Daily Rate':
					return PayrollConfigs.commissions.DAILYRATE;
				case 'Commission from total':
					return PayrollConfigs.commissions.COMMISSIONFROMTOTAL;
				case 'Packing Commission':
					return PayrollConfigs.commissions.MATERIALSCOMMISSION;
				case 'Hourly Rate (as helper)':
					return PayrollConfigs.commissions.HOURLYRATEHELPER;
				case 'Bonus':
					return PayrollConfigs.commissions.BONUS;
				default:
					return undefined;
			}
		}

		function getRequestMaterials(request) {
			var total = getSumMaterials(request);
			var result = calculateCommission(request, total, PayrollConfigs.commissions.MATERIALSCOMMISSION);
			return result;
		}

		function getSumMaterials(request) {
			var result = 0;

			if (request.request_data.value) {
				var decodeData = angular.fromJson(request.request_data.value);

				if (_.isObject(decodeData) && decodeData.hasOwnProperty('packings')) {
					_.each(decodeData, function (packing, property) {
						if (packing.rate && packing.quantity) {
							result += packing.rate * packing.quantity;
						}
					});
				}
			}

			return result;
		}

		scope.$on(PayrollConfigs.events.UPDATE_PAYROLL_OF_REQUEST, handlerUpdatePayrollOfRequest);

		function handlerUpdatePayrollOfRequest(event, data) {
			var nid = data.nid;
			var uid = scope.$parent.userUid;
			var currentWorker = false;

			_.each(data.data, worker => {
				if (worker.uid == uid) {
					currentWorker = worker.value;

					return false;
				}
			});

			if (currentWorker) { // If Worker was updated
				var extraHours = {};

				if (currentWorker.commissions[PayrollConfigs.commissions.HOURLYRATE]) {
					extraHours.hours = currentWorker.commissions[PayrollConfigs.commissions.HOURLYRATE].for_commission;
					extraHours.total = currentWorker.commissions[PayrollConfigs.commissions.HOURLYRATE].total;
				} else if (currentWorker.commissions[PayrollConfigs.commissions.HOURLYRATEHELPER]) {
					extraHours.hours = currentWorker.commissions[PayrollConfigs.commissions.HOURLYRATEHELPER].for_commission;
					extraHours.total = currentWorker.commissions[PayrollConfigs.commissions.HOURLYRATEHELPER].total;
				}

				if (_.isUndefined(scope.userTbl.copyJobs[nid])
					&& removedJobs[nid]) {

					scope.userTbl.copyJobs[nid] = angular.copy(removedJobs[nid]);
				}

				if (scope.userTbl.copyJobs[nid]) {
					var job = scope.userTbl.copyJobs[nid];

					if (!_.isEmpty(extraHours)) {
						job.hours = extraHours.hours;
						job.hours_pay = extraHours.total;
					}

					job.tip = _.isUndefined(currentWorker.commissions[PayrollConfigs.commissions.TIPS]) ? 0 : currentWorker.commissions[PayrollConfigs.commissions.TIPS].total;
					job.total = _.isUndefined(currentWorker.total) ? 0 : currentWorker.total;
					job.extra = _.isUndefined(currentWorker.commissions[PayrollConfigs.commissions.EXTRASCOMMISSION]) ? 0 : (currentWorker.commissions[PayrollConfigs.commissions.EXTRASCOMMISSION].rate / 100) * currentWorker.commissions[PayrollConfigs.commissions.EXTRASCOMMISSION].for_commission;
					job.materials = _.isUndefined(currentWorker.commissions[PayrollConfigs.commissions.MATERIALSCOMMISSION]) ? 0 : (currentWorker.commissions[PayrollConfigs.commissions.MATERIALSCOMMISSION].rate / 100) * currentWorker.commissions[PayrollConfigs.commissions.MATERIALSCOMMISSION].for_commission;
					job.bonus = _.isUndefined(currentWorker.commissions[PayrollConfigs.commissions.BONUS]) ? 0 : currentWorker.commissions[PayrollConfigs.commissions.BONUS].total;
				}

				if (scope.userTbl.copyJobs[nid]
					&& scope.userTbl.jobs_info[nid]) {

					var job = scope.userTbl.copyJobs[nid];

					if (job.commissions
						&& _.isObject(job.commissions)) {

						_.each(currentWorker.commissions, function (commission, index) {
							if (job.commissions.hasOwnProperty(index)) {
								job.commissions[index].rate = commission.rate;
								job.commissions[index].for_commission = commission.for_commission;
							} else {
								job.commissions[index] = {
									rate: commission.rate,
									for_commission: commission.for_commission
								}
							}
						});
					}
				}
			} else { // Then worker was deleted
				if (scope.userTbl.copyJobs[nid]) {
					removedJobs[nid] = angular.copy(scope.userTbl.copyJobs[nid]);
					delete scope.userTbl.copyJobs[nid];
				}
			}

			initPayrollUserTable();
		}

		scope.$on(PayrollConfigs.events.REMOVE_USER_FROM_PAYROLL_OF_REQUEST, handlerRemoveUserFromPayrollOfRequest);

		function handlerRemoveUserFromPayrollOfRequest(event, reqests) {
			_.forEach(reqests, (req) => {
				var currentUid = scope.$parent.userUid;
				let  { uid, nid } = req;
				let currentJob = scope.userTbl.copyJobs[nid];

				if (currentUid == uid && currentJob) {

					if (_.isArray(currentJob)) {
						if (_.isUndefined(removedJobs[nid])) {
							removedJobs[nid] = [];
						}

						let index = _.findIndex(currentJob, {contract_type: req.contract_type});

						if (index >= 0) {
							removedJobs[nid][index] = currentJob[index];

							currentJob.splice(index, 1);
						}

						if (_.isEmpty(currentJob)) {
							delete scope.userTbl.copyJobs[nid];
						}
					} else {
						removedJobs[nid] = angular.copy(scope.userTbl.copyJobs[nid]);
						delete scope.userTbl.copyJobs[nid];
					}
				} else if (!_.isUndefined(scope.userTbl.trip_jobs[nid])){
					delete scope.userTbl.trip_jobs[nid];
				}
			});

			initPayrollUserTable();
		}

		function init() {
			dateRange = {
				from: _.clone(scope.dateRange.from),
				to: _.clone(scope.dateRange.to)
			};

			getPayrollByUser(scope.$parent.userUid);
		}

		function getPayrollByUser(value) {
			scope.userTbl = '';

			PayrollService.getUsersPayroll({
				uid: value,
				date_from: moment(new Date(dateRange.from)).format('YYYY-MM-DD'),
				date_to: moment(new Date(dateRange.to)).format('YYYY-MM-DD'),
				type: Number(_.clone(scope.paidRadio))
			})
				.then(response => {
					initPayrollUserTable(response);
				})
				.catch(() => {
					toastr.error(`Payroll of this user doesn't exist`, 'Error');
					scope.userTbl = null;
				})
				.finally(() => {
					scope.busy = false;
				});
		}


		function editRequest(key, nid, type, tripJob) {
			if (key == 'a_a_selected') {
			} else {
				if (scope.jobClicked.nid == nid && scope.jobClicked.type == type) {
					scope.busy = true;

					if (type == 'request') {
						if(tripJob){
							$state.go('lddispatch.tripDetails', {id: nid, tab: 0});
							return false;
						}
						let realNid = nid.split('-')[0];
						openRequestService.open(realNid)
							.finally(() => {
								scope.busy = false;
							});
					} else if (type == 'misc') {
						let misc = angular.copy(scope.userTbl.hourly_misc[nid]);
						var users = null;
						DispatchService.getUsersByRole({
							active: 1,
							role: ['helper', 'foreman', 'manager', 'sales', 'driver', 'customer service']
						}).then(function (data) {
							users = data;
							PayrollService.miscDetailsModal(misc, users);
							scope.busy = false;
						});
					} else if (type = 'paycheck') {
						var paycheck = scope.userTbl.paychecks[nid];
						PayrollService.paymentDetailsModal(paycheck);
						scope.busy = false;
					}
				} else {
					scope.jobClicked.nid = nid;
					scope.jobClicked.type = type;
				}
			}
		}

		function selectAll(key) {
			_.each(scope.userTbl.jobs, function (job, index) {
				job.a_a_selected = key;
			});

			for (var index in scope.userTbl.misc) {
				let misc = scope.userTbl.hourly_misc[index];
				var isToPayOption = !(misc.type == '1' && misc.amount_options == '1');

				if (isToPayOption) {
					scope.userTbl.misc[index].a_a_selected = key;
				}
			}
		}

		function calculateAddServices(job) {
			var result = additionalServicesFactory.getExtraServiceTotal(job.extraServices);

			return parseFloat(result);
		}

		function calculatePackingServices(job) {
			var serviceType = (job.service_type.raw == '5' || job.service_type.raw == '7' );
			var packingSettingsLabor = false;
			if (angular.isUndefined(fieldData.basicsettings.packing_settings)) {
				var packingSettingsLabor = false;
			} else {
				if (angular.isDefined(fieldData.basicsettings.packing_settings.packingLabor)) {
					var packingSettingsLabor = fieldData.basicsettings.packing_settings.packingLabor;
				}
			}
			return PaymentServices.getPackingTotal(job.request_data.value, packingSettingsLabor, serviceType);
		}

		function updateRate(job) {
			job.trucks.raw = [];

			var rate = CalculatorServices.getRate(job.date.value, job.crew.value, 0);

			if (rate != job.rate.value) {
				job.rate.value = rate;
			}
		}

		function calculateTime(job) {
			var flatRate = (job.service_type.raw == 5 || job.service_type.raw == 7);

			if (job.field_usecalculator.value == null) {
				job.field_usecalculator.value = false;
			}

			if (job.field_usecalculator.value) {
				if (job.duration.value == null) {
					job.duration.value = 0;
				}

				var duration = parseFloat(job.duration.value);
				var distance = parseFloat(job.distance.value);

				// add duration to travel time
				if (!!settings.local_flat_miles && distance > settings.local_flat_miles && !flatRate) {
					duration = 0;
				}

				var total_cf = [];
				var request = {};

				if (job.move_size && job.rooms) {
					var total_weight = CalculatorServices.getRequestCubicFeet(job);
				}

				total_cf.weight = total_weight.weight;
				request.typeFrom = job.type_from.raw;
				request.typeTo = job.type_to.raw;
				request.serviceType = job.service_type.raw;

				var calcResults = CalculatorServices.calculateTime(request, total_cf);

				var min_hours = calcResults.min_hours;
				var moversCount = calcResults.movers_count;
				var trucksCount = calcResults.trucks;

				var minWorkTime = calcResults.work_time.min;
				var maxWorkTime = calcResults.work_time.max;

				if (!fieldData.calcsettings.doubleDriveTime) {
					minWorkTime += duration;
					maxWorkTime += duration;
				}

				if (job.minimum_time.raw != minWorkTime) {
					job.minimum_time.raw = minWorkTime;
					job.minimum_time.value = minWorkTime;
					job.minimum_time.value = common.decToTime(minWorkTime);
				}

				if (job.maximum_time.raw != maxWorkTime) {
					job.maximum_time.raw = maxWorkTime;
					job.maximum_time.value = maxWorkTime;
					job.maximum_time.value = common.decToTime(maxWorkTime);
				}

				if (job.crew.value != moversCount) {
					job.crew.value = moversCount;
					updateRate(job);
				}
			}
		}

		function calculateTotalBalance(type, job) {
			job.extraServices = angular.fromJson(job.extraServices);

			if (job.maximum_time && job.travel_time && job.minimum_time) {
				job = sharedRequestServices.calculateQuote(job);
			}

			if (job.move_size && job.rooms) {
				var total_weight = CalculatorServices.getRequestCubicFeet(job);
				job.total_weight = CalculatorServices.getRequestCubicFeet(job);
			}

			if (job.inventory) {
				job.inventory_weight = InventoriesServices.calculateTotals(job.inventory.inventory_list);
			}

			if (job.inventory.inventory_list != null && job.inventory.inventory_list.length && angular.isDefined(job.inventory.inventory_list)) {
				if (job.inventory.inventory_list.length && job.field_useweighttype.value == 2) {
					total_weight.weight = job.inventory_weight.cfs;
					calculateTime(job);
				} else if (job.inventory.inventory_list.length && job.field_useweighttype.old == null) {
					job.field_useweighttype.value = 2;
					total_weight.weight = job.inventory_weight.cfs;
					calculateTime(job);
				}
			}

			scope.min_weight = 0;

			if (job.service_type.value == 7) {
				var linfo = {
					min_weight: 0,
					days: 0
				};

				if (angular.isDefined(job.field_moving_to)) {
					if (angular.isDefined(job.field_moving_to.administrative_area) && job.field_moving_to.administrative_area != null) {
						linfo = CalculatorServices.getLongDistanceInfo(job);
					}
				}

				scope.min_weight = linfo.min_weight;
			}

			if (angular.isDefined(job.request_all_data)
				&& angular.isDefined(job.request_all_data.min_weight)) {

				scope.min_weight = job.request_all_data.min_weight;
			}

			var min_weight = scope.min_weight;

			if (scope.min_weight < job.inventory_weight.cfs) {
				min_weight = job.inventory_weight.cfs;
			}

			if (job.field_useweighttype.value == 3) {
				if (angular.isDefined(job.custom_weight)) {
					var custom = parseFloat(job.custom_weight.value);

					if (scope.min_weight < custom) {
						min_weight = custom;
					}
				}
			}

			if (job.service_type.value == 7) {
				if ((angular.isUndefined(job.extraServices)
					|| job.extraServices.length == 0)
					&& angular.isDefined(scope.min_weight)) {

					job.extraServices = additionalServicesFactory.getLongDistanceExtra(job, min_weight);
				} else {
					if (angular.isDefined(scope.min_weight)) {
						angular.forEach(additionalServicesFactory.getLongDistanceExtra(job, scope.min_weight), function (service, value) {
							if (!_.find(job.extraServices, {'name': service.name})) {
								job.extraServices.push(service);
							}
						});
					}
				}
			}

			var total = 0;

			if (angular.isUndefined(job.request_all_data.localdiscount)) {
				job.request_all_data.localdiscount = 0;
				scope.localdiscount = 0;
			} else {
				scope.localdiscount = parseInt(job.request_all_data.localdiscount);
			}

			if (angular.isUndefined(job.request_all_data.add_money_discount)) {
				job.request_all_data.add_money_discount = 0;
			}

			if (angular.isUndefined(job.request_all_data.add_percent_discount)) {
				job.request_all_data.add_percent_discount = 0;
			}

			if (type == 'min') {
				total = parseFloat(job.quote.min) + parseFloat(calculateAddServices(job)) + parseFloat(calculatePackingServices(job));
			} else if (type == 'max') {
				total = parseFloat(job.quote.max) + parseFloat(calculateAddServices(job)) + parseFloat(calculatePackingServices(job));
			} else if (type == 'invoice') {
				if (angular.isDefined(job.request_all_data.invoice)) {
					if (angular.isDefined(job.request_all_data.invoice.work_time)) {
						if (angular.isDefined(job.request_all_data.invoice.rate)) {

							total = _.round(common.convertStingToIntTime(job.request_all_data.invoice.work_time), 2)
								* parseFloat(job.request_all_data.invoice.rate.value)
								+ parseFloat(calculateAddServices(job)) + parseFloat(calculatePackingServices(job));
						} else {
							total = _.round(common.convertStingToIntTime(job.request_all_data.invoice.work_time), 2)
								* parseFloat(job.rate.value) + parseFloat(calculateAddServices(job))
								+ parseFloat(calculatePackingServices(job));
						}
					}
				} else if (angular.isDefined(job.contract_info.hours) && !_.isEmpty(job.contract_info.hours)) {
					if (_.isString(job.contract_info.hours)) {
						if ((job.contract_info.hours.split(".").length - 1) > 1) {
							job.contract_info.hours = 0
						} else {
							job.contract_info.hours = _.round(common.convertStingToIntTime(job.contract_info.hours), 2);
						}
					}
					if (angular.isDefined(job.request_all_data.invoice)) {
						if (angular.isDefined(job.request_all_data.invoice.rate)) {
							total = job.contract_info.hours * parseFloat(job.request_all_data.invoice.rate.value)
								+ parseFloat(calculateAddServices(job)) + parseFloat(calculatePackingServices(job));
						}
					} else {
						total = job.contract_info.hours * parseFloat(job.rate.value) + parseFloat(calculateAddServices(job)) + parseFloat(calculatePackingServices(job));
					}
				}
			}

			if (angular.isUndefined(job.request_all_data.add_money_discount) && angular.isUndefined(job.request_all_data.add_percent_discount)) {
				var discount = job.request_all_data.add_money_discount + total * (job.request_all_data.add_percent_discount / 100);

				return total - discount;
			}

			return _.round(total, 2);
		}

		scope.$on('$destroy', function () {

		});

		scope.$on(PayrollConfigs.events.LIVE_UPDATE_USER_PAYROLL_TABLE, function (event, data) {
			var action = data.action;
			var type = data.type;
			var paymentData = data.data;

			if (_.isEqual(type, 'misc')
				|| _.isEqual(type, 'paycheck')) {

				var momentFrom = moment(scope.$parent.dateRange.from).startOf('day');
				var momentTo = moment(scope.$parent.dateRange.to).endOf('day');
				var isNotUnixDate = _.isNaN(Number(paymentData.date));
				var currentMoment = isNotUnixDate
					? moment(paymentData.date)
					: moment.unix(Number(paymentData.date) + utcOfset);

				var isNotDisplayPayment = !(currentMoment.isBetween(momentFrom, momentTo)
					|| currentMoment.isSame(momentFrom)
					|| currentMoment.isSame(momentTo));

				if (isNotDisplayPayment) {
					action = PayrollConfigs.actions.REMOVE;
				}
			}

			if (_.isEqual(type, 'misc')) {
				if (_.isEmpty(scope.userTbl.hourly_misc)
					&& _.isArray(scope.userTbl.hourly_misc)) {

					scope.userTbl.hourly_misc = {};
				}

				makeActionWithCollection(scope.userTbl.hourly_misc, paymentData.id, paymentData, 'id', action);
			}

			if (_.isEqual(type, 'paycheck')) {
				if (_.isEmpty(scope.userTbl.paychecks)
					&& _.isArray(scope.userTbl.paychecks)) {

					scope.userTbl.paychecks = {};
				}

				makeActionWithCollection(scope.userTbl.paychecks, paymentData.payment_id, paymentData, 'payment_id', action);
			}

			initPayrollUserTable();
		});

		function makeActionWithCollection(collection, id, data, idField, action) {
			if (_.isEqual(action, PayrollConfigs.actions.REMOVE) && collection[id]) {
				delete collection[id];
			}

			if (_.isEqual(action, PayrollConfigs.actions.UPDATE)) {
				collection[id] = angular.extend(collection[id], data);
			}

			if (_.isEqual(action, PayrollConfigs.actions.CREATE)) {
				collection[id] = data;
			}
		}

		function setNid(nid) {
			let normalNid = angular.copy(nid);
			return normalNid.split('-')[0];
		}

		function createJobsObj(jobs) {
			let obj = {};
			_.forEach(jobs, function (job, nid) {
				if (_.isArray(job)) {
					_.forEach(job, function (item) {
						obj[nid + '-' + item.contract_type] = item;
					})
				} else {
					obj[nid] = job;
				}
			});

			return obj;
		}

		function initPayrollUserTable(response) {
			var showMiscPayment = false;
			var isFirstInitUserTable = !_.isUndefined(response);

			if (isFirstInitUserTable) {
				scope.userTbl = response;
			}

			scope.usersJobs = {};

			if (!isFirstInitUserTable) {
				totals.total = 0;
				totals.hours = 0;
				totals.grand_total = 0;
				totals.hours_pay = 0;
				totals.materials = 0;
				totals.advanced = 0;
				totals.extra = 0;
				totals.ins = 0;
				totals.tip = 0;
				totals.bonus = 0;
				totals.misc = 0;
				totals.paid = 0;
			} else {
				totals = scope.userTbl.totals;
			}

			totals.closing = 0;
			totals.to_pay = 0;
			totals.cb_hourly_rate = 0;

			if (isFirstInitUserTable) {
				scope.userTbl.copyJobs = angular.copy(scope.userTbl.jobs);
				scope.userTbl.copyTripJobs = angular.copy(scope.userTbl.trip_jobs);
			}

			if (scope.userTbl.copyJobs) {
				scope.userTbl.jobs = angular.copy(createJobsObj(scope.userTbl.copyJobs));
				scope.userTbl.trip_jobs = angular.copy(createJobsObj(scope.userTbl.copyTripJobs));

				var jobsArr = ['jobs', 'trip_jobs'];
				_.each(jobsArr, function (jobType) {
					_.each(scope.userTbl[jobType], function (job, index) {
						if (_.isObject(job.tips) || _.isUndefined(job.tip)) {
							job.tips = 0;
						}

						if(jobType == 'trip_jobs') {
							job.trip_job = true;
						}

						scope.usersJobs[index] = job.retrieve || {};
						job.a_job_misc = index;
						job.a_job_type = job.job_type || '';
						job.b_pickup = job.pickup || '';
						job.c_delivery = job.delivery || '';
						job.c_grand_total =  _.round(job.grand_total, 2);
						job.ca_hours = _.round(job.hours, 2);
						job.cb_hourly_rate = job.hours == 0 ? 0 : _.round(job.hours_pay, 2);
						job.c_materials = _.round(job.materials || 0, 2);
						job.a_a_selected = false;
						job.b_move_date = job.move_date ? moment.unix(Number(job.move_date) + utcOfset).format('M/D/YYYY') : '';
						var bookingDate = '';

						if (!_.isEmpty(scope.usersJobs[index])) {
							bookingDate = angular.copy(scope.usersJobs[index].field_date_confirmed.value);
						}

						job.c_estimate = _.get(scope, `usersJobs[${index}].contract_info[0].estimate_total`, 0);
						job.closing = jobType == 'jobs' ? calculateTotalBalance('invoice', scope.usersJobs[index]) : 0;
						job.booking_date = bookingDate ? moment.unix(Number(bookingDate) + utcOfset).format('M/D/YYYY') : bookingDate;
						job.tj_bonus = _.round(job.bonus || 0, 2);
						job.custom_payroll = _.isArray(job.custom_payroll) ? _.sum(job.custom_payroll) : _.round(job.custom_payroll || 0, 2);

						if (_.isUndefined(job.ins) || _.isNull(job.ins)) {
							job.ins = 0;
						}

						if (_.isUndefined(job.extra) || _.isNull(job.ins)) {
							job.ins = 0;
						}

						if (_.isUndefined(job.advanced) || _.isNull(job.advanced)) {
							job.advanced = 0;
						}

						if (!isFirstInitUserTable) {
							totals.total += _.isUndefined(job.total) ? 0 : parseFloat(job.total);
							totals.hours += _.isUndefined(job.hours) ? 0 : parseFloat(job.hours);
							totals.grand_total += _.isUndefined(job.grand_total) ? 0 : parseFloat(job.grand_total);
							totals.hours_pay += _.isUndefined(job.hours_pay) ? 0 : parseFloat(job.hours_pay);
							totals.materials += _.isUndefined(job.materials) ? 0 : parseFloat(job.materials);
							totals.advanced += _.isUndefined(job.advanced) ? 0 : parseFloat(job.advanced);
							totals.extra += _.isUndefined(job.extra) ? 0 : parseFloat(job.extra);
							totals.ins += _.isUndefined(job.ins) ? 0 : parseFloat(job.ins);
							totals.tip += _.isUndefined(job.tip) ? 0 : parseFloat(job.tip);
							totals.bonus += _.isUndefined(job.bonus) ? 0 : parseFloat(job.bonus);
						}

						totals.cb_hourly_rate += Number(job.hours_pay); // We need calculate all hours_pay
						totals.closing += Number(job.closing || 0);

						delete job.job_type;
						delete job.pickup;
						delete job.delivery;
						delete job.grand_total;
						delete job.hours;
						delete job.materials;
						delete job.advanced;
						delete job.retrieve;
						delete job.move_date;
						delete job.hours_pay;
						delete job.bonus;
						scope.userTbl.jobs[index] = job;
					});
				});
			}

			if (scope.userTbl.hourly_misc) {
				if (_.isArray(scope.userTbl.hourly_misc)) {
					scope.userTbl.misc = _.keyBy(scope.userTbl.hourly_misc, 'id');
				} else {
					scope.userTbl.misc = angular.copy(scope.userTbl.hourly_misc);
				}

				if (scope.userTbl.misc.hasOwnProperty('undefined')) {
					delete scope.userTbl.misc.undefined;
				}

				for (var index in scope.userTbl.misc) {
					if (scope.userTbl.misc[index]) {
						misc = scope.userTbl.misc[index];
					} else {
						var misc = {};
					}

					let obj = angular.copy(scope.userTbl.misc[index]);

					misc.a_a_selected = false;
					misc.a_job_misc = obj.note ? obj.note : 'Misc';

					var jobType = 'Check';

					if (obj.type == '1') {
						if (obj.amount_options == '0') {
							jobType = 'Custom To Pay';
						}

						if (obj.amount_options == '1') {
							jobType = 'Custom Paid';
						}

						if (obj.amount_options == '2') {
							jobType = 'Custom Deduct';
						}
					}


					misc.amount_options = obj.amount_options;
					misc.type = obj.type;
					misc.a_job_type = jobType;
					var isNotUnixDate = _.isNaN(Number(obj.date));
					let currentUtcOffset = !isNotUnixDate && !_.isString(obj.date) ? 0 : utcOfset;
					misc.b_move_date = isNotUnixDate ? moment(obj.date).format('M/D/YYYY') : moment.unix(Number(obj.date) + currentUtcOffset).format('M/D/YYYY');
					misc.date = obj.date;
					misc.b_pickup = '';
					misc.c_delivery = '';
					misc.c_grand_total = '';
					misc.ca_hours = obj.type == 0 ? parseFloat(obj.hours).toFixed(2) : parseFloat(0).toFixed(2);
					misc.c_materials = '';
					misc.extra = '';
					misc.ins = '';
					misc.tip = '';
					misc.c_estimate = '';
					misc.closing = '';
					misc.custom_payroll = '';
					misc.booking_date = '';
					misc.tj_bonus = '';
					misc.cb_hourly_rate = obj.type == 0 ? _.round((Number(obj.hours) * Number(obj.hourly_rate)), 2) : 0;
					misc.total = obj.type == 0 ? _.round(Number(obj.hours) * Number(obj.hourly_rate), 2) : _.round(obj.amount, 2); // 0 = hourly rate 1 = amount

					if (obj.type == '1' && (obj.amount_options == '1' || obj.amount_options == '2')) {
						misc.total *= -1;
					}

					if (obj.type == '1' && obj.amount_options == '1') {
						showMiscPayment = true;
					}

					if (!isFirstInitUserTable) {
						totals.hours += parseFloat(misc.ca_hours);
						totals.misc += parseFloat(misc.total);

						if (obj.type == '1' && obj.amount_options == '1') {
							totals.paid += Math.abs(misc.total);
						} else {
							totals.total += parseFloat(misc.total);
						}
					}

					totals.cb_hourly_rate += obj.type == 0 ? _.round(Number(obj.hours) * Number(obj.hourly_rate), 2) : 0;

					scope.userTbl.misc[index] = misc;
				}
			}

			if (scope.userTbl.paychecks) {
				if (_.isArray(scope.userTbl.paychecks)) {
					scope.userTbl.users_paychecks = _.keyBy(scope.userTbl.paychecks, 'id');
				} else {
					scope.userTbl.users_paychecks = angular.copy(scope.userTbl.paychecks);
				}

				if (scope.userTbl.users_paychecks.hasOwnProperty('undefined')) {
					delete scope.userTbl.users_paychecks.undefined;
				}

				for (let index in scope.userTbl.users_paychecks) {
					var paycheck = scope.userTbl.users_paychecks[index];
					let obj = angular.copy(paycheck);
					paycheck = {};
					paycheck.a_a_selected = false;
					var jobs = '';

					if (obj.jobs) {
						_.each(obj.jobs, function (job_id, index) {
							jobs += '#' + (job_id.nid || job_id);

							if (obj.jobs.length - 1 != index) {
								jobs += ", "
							}
						});
					}

					paycheck.a_job_works = jobs;
					paycheck.a_job_misc = 'Paycheck';
					paycheck.a_job_type = scope.paymentOpt[obj.form];
					paycheck.b_move_date = moment.unix(Number(obj.date)).utc().format('M/D/YYYY');
					paycheck.total = parseFloat(Number(obj.amount).toFixed(2));

					if (!isFirstInitUserTable) {
						totals.paid += parseFloat(obj.amount);
					}

					scope.userTbl.users_paychecks[index] = paycheck;
				}
			}

			totals.a_a_selected = false;
			totals.a_job_misc = '';
			totals.a_job_type = '';
			totals.booking_date = '';
			totals.b_move_date = '';
			totals.b_pickup = '';
			totals.c_delivery = '';
			totals.c_estimate = '';
			totals.c_grand_total = parseFloat(totals.grand_total).toFixed(2);
			totals.ca_hours = parseFloat(totals.hours).toFixed(2);
			totals.c_materials = parseFloat(totals.materials).toFixed(2);
			totals.closing = parseFloat(totals.closing).toFixed(2);
			totals.custom_payroll = _.isArray(totals.custom_payroll) ? _.sum(totals.custom_payroll) : parseFloat(totals.custom_payroll || 0);
			totals.tj_bonus = parseFloat(totals.bonus).toFixed(2);
			totals.total = isFirstInitUserTable ? (parseFloat(totals.total)).toFixed(2) : (parseFloat(totals.total) - parseFloat(totals.paid)).toFixed(2);
			totals.tot_paid = (parseFloat(totals.paid)).toFixed(2);
			totals.to_pay = (parseFloat(totals.total)).toFixed(2);
			delete totals.grand_total;
			delete totals.hours;
			delete totals.materials;
			delete totals.advanced;
			delete totals.hours_pay;
			delete totals.bonus;
			delete totals.paid;

			scope.userTbl.totals = totals;

			scope.busy = false;
			scope.userCurrentTbl = scope.userTbl;

			_.forEach(scope.userCurrentTbl.jobs, job => {
				let type = +_.get(job, 'a_job_type');
				if (_.isNaN(type)) return;
				job.a_job_type = $filter('serviceTypeFilter')(type);
			});

			$rootScope.$broadcast('payroll_table_loaded', scope.userTbl);
			scope.isVisiblePayrollTable = !_.isEmpty(scope.userTbl.users_paychecks) || showMiscPayment;
		}

		function findWorkerByUID(UID) {
			var user;

			_.each(scope.$parent.users, function (role, index) {
				_.each(role, function (roleUser, uid) {
					if (UID == uid) {
						user = roleUser;
					}
				});
			});

			return user;
		}

		function getHourlyRateUserCommission() {
			var result = 0;

			if (!_.isUndefined(currentUser)
				&& !_.isUndefined(currentUser.settings)
				&& !_.isUndefined(currentUser.settings.rateCommission)) {

				result = findCommissionByName(currentUser.settings.rateCommission, 'Hourly Rate')
			}

			return result;
		}

		function findCommissionByName(rateCommission, name) {
			var result = 0;

			if (!_.isEmpty(rateCommission)) {
				_.each(rateCommission, function (commission, index) {
					if (_.isEqual(commission.option, name)) {
						result = commission.input;

						return false;
					}
				})
			}

			return result;
		}
	}
}
