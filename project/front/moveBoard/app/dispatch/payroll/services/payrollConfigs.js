(function () {
    'use strict';

    angular
        .module('app.dispatch')
        .factory('PayrollConfigs', PayrollConfigs);


    function PayrollConfigs ()
    {
        var service = {};

        service.columns = {
            departments: {
                fields: {
                    department: { name: 'Department', selected: true },
                    jobs_count: { name: 'Jobs', selected: true },
                    hours: { name: 'Hours', selected: true },
                    hours_pay: { name: 'Paid', selected: true, prefix: '$' },
                    materials: { name: 'Packing', selected: true, prefix: '$' },
                    m_extra: { name: 'Extra', selected: true, prefix: '$' },
                    tip: { name: 'Tip', selected: true, prefix: '$' },
                    total: { name: 'Total', selected: true, prefix: '$' }
                },
                totalFields: {
                    jobs_count: { name: 'Jobs', selected: true },
                    hours: { name: 'Hours', selected: true },
                    hours_pay: { name: 'Paid', selected: true, prefix: '$' },
                    materials: { name: 'Packing', selected: true, prefix: '$' },
                    m_extra: { name: 'Extra', selected: true, prefix: '$' },
                    total: { name: 'Total', selected: true, prefix: '$' },
                    isVisible: true
                }
            },
            workers:{
                fields: {
                    a_employee_selected:{name:'', selected:true},
                    a_name: { name: 'Full Name', selected: true },
                    count_jobs: { name: 'Jobs', selected: true },
                    hours: { name: 'Hours', selected: true },
                    hours_pay: { name: 'Hours Pay', selected: true, prefix: '$' },
                    materials: { name: 'Packing', selected: true, prefix: '$' },
                    m_extra: { name: 'Extra', selected: true, prefix: '$' },
                    tip: { name: 'Tip', selected: true, prefix: '$' },
                    total: { name: 'Total', selected: true, prefix: '$' },
                    paid: { name: 'Paid', selected: true, prefix: '$'}
                },
                totalFields: {
                    count_jobs: { name: 'Jobs', selected: true },
                    hours: { name: 'Hours', selected: true },
                    hours_pay: { name: 'Hours Pay', selected: true, prefix: '$' },
                    materials: { name: 'Packing', selected: true, prefix: '$' },
                    m_extra: { name: 'Extra', selected: true, prefix: '$' },
                    total: { name: 'Total', selected: true, prefix: '$' },
                    paid: { name: 'Paid', selected: true, prefix: '$'},
                    isVisible: true
                }
            },
            user:{
                fields: {
                    a_a_selected:{name:'user Select', selected:true},
                    a_job_misc: { name: 'Job# / Misc', selected: true },
                    a_job_type: { name: 'Job Type', selected: true },
                    b_move_date: { name: 'Move Date', selected: true },
                    b_pickup: { name: 'Pickup', selected: true },
                    booking_date:{name: 'Booking Date', selected: true},
                    c_delivery: { name: 'Delivery', selected: true },
                    c_estimate:{name: 'Estimate', selected: true},
                    c_grand_total: { name: 'Grand Total', selected: true, prefix: '$' },
                    ca_hours: { name: 'Hours', selected: true },
                    cb_hourly_rate: { name: 'Hourly Rate', selected: true, prefix: '$' },
                    c_materials: { name: 'Packing', selected: true, prefix: '$' },
                    custom_payroll: { name: 'Custom Payroll', selected: true, prefix: '$' },
                    closing:{name: 'Closing', selected: true, prefix: '$'},
                    extra: { name: 'Extra', selected: true, prefix: '$' },
                    ins: { name: 'Ins.', selected: true, prefix: '$' },
                    tip: { name: 'Tip', selected: true, prefix: '$' },
                    tj_bonus: { name: 'Bonus', selected: true, prefix: '$' },
                    total: { name: 'Total', selected: true, prefix: '$' }
                },
                totalFields: {
                    a_a_selected:{name:'user Select', selected:true},
                    closing:{name: 'Closing', selected: true, prefix: '$'},
                    c_grand_total: { name: 'Grand Total', selected: true, prefix: '$' },
                    ca_hours: { name: 'Hours', selected: true },
                    c_materials: { name: 'Packing', selected: true, prefix: '$' },
                    custom_payroll: { name: 'Custom Payroll', selected: true, prefix: '$' },
                    extra: { name: 'Extra', selected: true, prefix: '$' },
                    ins: { name: 'Ins.', selected: true, prefix: '$' },
                    total: { name: 'Balance', selected: true, prefix: '$' },
                    tj_bonus: {name: 'Bonus', selected: true, prefix: '$' },
                    tot_paid: { name: 'Paid', selected: true, prefix: '$' },
                    to_pay: { name: 'To Pay', selected: true, prefix: '$' },
                    misc: { name: 'Misc', selected: true, prefix: '$' },
                    isVisible: true
                }
            }
        };

        service.events = {
            GET_BY_DATE_WORKERS: 'fire.getByDate',
            GET_BY_DATE_USER: 'fire.getByDate',
            GET_BY_DATE_DEPARTMENTS: 'fire.getByDate',
            LIVE_UPDATE_USER_PAYROLL_TABLE: 'fire.liveUpdateUserPayrollTable',
            UPDATE_PAYROLL_OF_REQUEST: 'payroll_of_request.updated',
            REMOVE_USER_FROM_PAYROLL_OF_REQUEST: 'payroll_of_reqest.remove_user'
        };

        service.commissions = {
            ADVANCEDSERVICE: 0,
            COMMISSIONFROMTOTAL: 1,
            EXTRASCOMMISSION: 2,
            DAILYRATE: 3,
            HOURLYRATE: 4,
            INSURANCE: 5,
            LOADINGPEROUFT: 6,
            MATERIALSCOMMISSION: 7,
            UNLOADINGPEROUFT: 8,
            HOURLYRATEHELPER: 9,
            TIPS: 10,
            BONUS: 11
        };

        service.actions = {
            CREATE: 'create',
            REMOVE: 'remove',
            UPDATE: 'update'
        };

        return service;
    }

})();
