(function () {
    'use strict';

    angular
        .module('app.dispatch')
        .factory('PayrollBuilder', PayrollBuilder);


    function PayrollBuilder ()
    {
        return {
            groupByField: groupByField
        };

        function groupByField (data, field) {

            var transObj = {};
            _.each (data, function (item , key) {
                if (_.has(item, field)) {
                    transObj[item[field]] = item;
                }
            });

            return transObj;
        }

    }

})();
