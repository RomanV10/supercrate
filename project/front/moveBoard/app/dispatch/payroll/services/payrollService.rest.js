'use strict';

angular
	.module('app.dispatch')
	.factory('PayrollService', PayrollService);

/*@ngInject*/
function PayrollService($q, $http, config, $uibModal, PayrollConfigs, apiService, moveBoardApi) {
	return {
		setMiscPayment: setMiscPayment,
		getPayrollByDepartment: getPayrollByDepartment,
		getPayrollByRole: getPayrollByRole,
		getUsersPayroll: getUsersPayroll,
		getPayrollForRequest: getPayrollForRequest,
		userPaycheck: userPaycheck,
		paymentDetailsModal: paymentDetailsModal,
		miscDetailsModal: miscDetailsModal,
		saveColumns: saveColumns,
		getSettings: getSettings,
		updateMiscPayment: updateMiscPayment,
		removeMiscPayment: removeMiscPayment,
		updatePaycheckPayment: updatePaycheckPayment,
		removePaycheckPayment: removePaycheckPayment,
		updatePayrollCache: updatePayrollCache,
		prepareEventDataObject: prepareEventDataObject,
		initPayrollColumns: initPayrollColumns
	};

	function getSettings() {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/settings/get_default_settings')
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function saveColumns(type, columns) {
		var deferred = $q.defer();
		var data = {};
		data.name = type;
		data.value = angular.toJson(columns);

		$http.post(config.serverUrl + 'server/settings/set_variable', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function userPaycheck(data) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payroll/user_paycheck', data)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function getPayrollByDepartment(filtering) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payroll/payroll_all', filtering)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function getPayrollByRole(filtering) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payroll/payroll_by_role', filtering)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function getUsersPayroll(filtering) {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.payroll.get, filtering)
			.then(resolve => {
				if (_.isEmpty(resolve.data)) return defer.reject();
				defer.resolve(resolve.data);
			})
			.catch(() => {
				defer.reject();
			});

		return defer.promise;
	}

	function setMiscPayment(req) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payroll/hourly_misc_payment', req)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function updateMiscPayment(req) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payroll/edit_hourly_misc', req)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function removeMiscPayment(req) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payroll/delete_hourly_misc', req)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function updatePaycheckPayment(req) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payroll/edit_receipt', req)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function removePaycheckPayment(req) {
		var deferred = $q.defer();

		$http.post(config.serverUrl + 'server/payroll/delete_receipt', req)
			.success(function (data, status, headers, config) {
				deferred.resolve(data);
			})
			.error(function (data, status, headers, config) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function getPayrollForRequest(nid) {

		var deferred = $q.defer();
		var data = {
			nid: nid
		};

		$http.post(config.serverUrl + 'server/payroll/get_contract_info', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function updatePayrollCache(data) {

		var deferred = $q.defer();
		$http.post(config.serverUrl + 'server/payroll/update_payroll_cache', data)
			.success(function (data) {
				deferred.resolve(data);
			})
			.error(function (data) {
				deferred.reject(data);
			});

		return deferred.promise;
	}

	function paymentDetailsModal(data) {
		var paymentDetailsModal = $uibModal.open({
			template: require('../templates/editPaymentDetails.html'),
			controller: 'PayrollEditPaymentCtrl',
			size: 'lg',
			resolve: {
				payment: function () {
					return data;
				}
			},
		});
	}

	function miscDetailsModal(misc, users) {
		var paymentDetailsInstanceModal = $uibModal.open({
			template: require('../templates/editMiscPayment.html'),
			controller: 'PayrollEditMiscPaymentCtrl',
			size: 'lg',
			resolve: {
				misc: function () {
					return [misc, users];
				}
			},
		});
	}

	function prepareEventDataObject(action, data, type) {
		var result = {
			"action": action,
			"data": data,
			"type": type
		};

		return result;
	}

	function initPayrollColumns(settings, mainColumns) {
		if (angular.isUndefined(settings.columns) || _.isEmpty(settings.columns)) {
			angular.copy(PayrollConfigs.columns, mainColumns);
		} else {
			angular.copy(settings.columns, mainColumns);

			var foundDiffs = false;
			_.each(PayrollConfigs.columns, function (column, key) {
				let fields = _.get(mainColumns[key], 'fields', {});
				_.each(column.fields, function (field, property) {
					if (!fields.hasOwnProperty(property)) {
						_.set(mainColumns, `[${key}].fields[${property}]`, field);
						foundDiffs = true;
					}

					if (fields.hasOwnProperty(property)) {
						let prefix = _.get(fields, `${[property]}.name`);
						if (!_.isEqual(prefix, field.name)) {
							_.set(mainColumns, `[${key}].fields[${property}].name`, field.name);
							foundDiffs = true;
						}

						if (!_.isEqual(prefix, field.prefix)) {
							if (_.isUndefined(field.prefix)) {
								delete mainColumns[key].fields[property].prefix;
							} else {
								mainColumns[key].fields[property].prefix = field.prefix;
							}
						}
					}
				});

				_.each(fields, function (field, property) {
					if (!column.fields.hasOwnProperty(property)) {
						delete mainColumns[key].fields[property];
						foundDiffs = true;
					}
				});

				if (_.isUndefined(mainColumns[key].totalFields)) {
					mainColumns[key].totalFields = {};
				}

				_.each(column.totalFields, function (field, property) {
					if (!mainColumns[key].totalFields.hasOwnProperty(property)) {
						mainColumns[key].totalFields[property] = field;
						foundDiffs = true;
					}

					if (mainColumns[key].totalFields.hasOwnProperty(property)) {
						if (!_.isEqual(mainColumns[key].totalFields[property].name, field.name)) {
							mainColumns[key].totalFields[property].name = field.name;
							foundDiffs = true;
						}

						if (!_.isEqual(mainColumns[key].totalFields[property].prefix, field.prefix)) {
							if (_.isUndefined(field.prefix)) {
								delete mainColumns[key].totalFields[property].prefix;
							} else {
								mainColumns[key].totalFields[property].prefix = field.prefix;
							}
						}
					}
				});

				_.each(mainColumns[key].totalFields, function (field, property) {
					if (!column.totalFields.hasOwnProperty(property)) {
						delete mainColumns[key].totalFields[property];
						foundDiffs = true;
					}
				});
			});

			if (foundDiffs) {
				angular.copy(mainColumns, settings.columns);

				saveColumns('basicsettings', settings)
					.then(function (data) {

					});
			}
		}
	}
}
