(function () {
	'use strict';
	angular

		.module('app.dispatch')
		.controller('PayrollCtrl', PayrollCtrl);

	PayrollCtrl.$inject = ['$scope', '$uibModal', '$q', 'SweetAlert', 'DTOptionsBuilder', 'DispatchService', 'PayrollConfigs', 'PayrollService', 'datacontext', 'SettingServices', '$rootScope', 'RequestServices', 'PermissionsServices'];

	function PayrollCtrl($scope, $uibModal, $q, SweetAlert, DTOptionsBuilder, DispatchService, PayrollConfigs, PayrollService, datacontext, SettingServices, $rootScope, RequestServices, PermissionsServices) {

		const FIRST_DAY_OF_MONTH = 1;
		const FIFTEENTH_DAY_OF_MONTH = 15;
		const SIXTEENTH_DAY_OF_MONTH = 16;
		const PAYROLL_HEADER_OFFSET = 30;
		const PAYROLL_HEADER_POSITION_OFFSET = 15;

		var unvisibleItem = ['a_a_selected', 'a_job_misc', 'a_job_type', 'booking_date', 'b_move_date', 'b_pickup', 'c_delivery', 'total', 'cb_hourly_rate', 'c_estimate', 'tip'];
		let fieldData = datacontext.getFieldData();
		let settings = angular.copy(fieldData.basicsettings);
		$scope.mainColumns = {};
		PayrollService.initPayrollColumns(settings, $scope.mainColumns);

		$scope.busy = true;
		$scope.isShowPayrollHeader = true;
		$scope.clicked = false;
		$scope.dTable = 'departments';
		$scope.roleWorkers = '';
		$scope.keyRoleWorkers = '';
		$scope.userName = '';
		$scope.userUid = '';
		$scope.paidRadio = 0;
		$scope.dtOptions = DTOptionsBuilder.newOptions()
			.withPaginationType('simple')
			.withDisplayLength(100)
			.withOption('bFilter', false)
			.withOption('aaSorting', [0, 'desc']);
		$scope.dateRange = {
			from: moment().subtract(1, 'months').toDate(),
			to: moment().toDate()
		};
		$scope.dateOptionsFrom = {
			formatYear: 'YYYY',
			startingDay: 0,
		};
		$scope.dateOptionsTo = {
			formatYear: 'YYYY',
			startingDay: 0,
			minDate: $scope.dateRange.from,
		};
		$scope.users = {};
		$scope.workersRoles = {
			manager: 1,
			helper: 2,
			foreman: 3,
			driver: 4,
			sales: 5
		};
		$scope.contractTypes = fieldData.enums.contract_type;
		$scope.isAdmin = PermissionsServices.isSuper();

		if (angular.isDefined(settings.payroll_time)) {
			if (angular.isDefined(settings.payroll_time.from)) {
				$scope.dateRange.from = moment(settings.payroll_time.from).toDate();
			}

			if (angular.isDefined(settings.payroll_time.to)) {
				$scope.dateRange.to = moment(settings.payroll_time.to).toDate();
			}
		}

		var payrollHeader = angular.element('#payroll_header');
		var fixedPayrollHeader = angular.element('#fixed_payroll_header');

		//@Functions
		$scope.openModal = openModal;
		$scope.getByDate = getByDate;
		$scope.changeperiod = changeperiod;
		$scope.chooseUser = chooseUser;
		$scope.applyPayment = applyPayment;
		$scope.minDateTo = minDateTo;
		$scope.dateChange = dateChange;
		$scope.isVisibleApplyPayment = isVisibleApplyPayment;
		$scope.isVisibleDeletePayrollButton = isVisibleDeletePayrollButton;
		$scope.deletePayroll = deletePayroll;
		$scope.isDisplayUserTable = isDisplayUserTable;
		$scope.isDisplayWorkersTable = isDisplayWorkersTable;
		$scope.isDisplayDepartmentTable = isDisplayDepartmentTable;
		$scope.updatePayrollCache = updatePayrollCache;
		$scope.changeDateFrom = changeDateFrom;

		function changeDateFrom() {
			$scope.bDateChange = true;
			$scope.dateOptionsTo.minDate = $scope.dateRange.from;
		}

		function dateChange() {
			if ((new Date($scope.dateRange.from)).getTime() < (new Date($scope.dateRange.to)).getTime()) {
				getByDate();
			} else {
				$scope.dateRange.to = angular.copy($scope.dateRange.from);
				getByDate();
			}
		}

		function minDateTo(date) {
			return (new Date(date)).getTime();
		}

		function applyPayment() {
			$scope.selectedWorkers = [];
			$scope.selectedItems = {
				misc: [], jobs: [], paychecks: []
			};

			if ($scope.dTable != 'departments') {
				calculateSelectedItems();

				if ($scope.selectedItems.paychecks.length > 0) {
					toastr.error('Paychecks are selected! Choose just Miscs and Jobs', 'Error');
				} else {
					if ($scope.selectedWorkers.length != 0 || $scope.selectedItems.misc.length != 0 || $scope.selectedItems.jobs.length != 0) {

						var ctrl = '', tpl = '', resolve = {};
						ctrl = 'ApplyPaymentModalCtrl';
						tpl = require('./templates/paymentDetails.html');
						resolve = {
							data: function () {
								if ($scope.dTable == 'workers') {
									return [$scope.dTable, $scope.$$childTail.workersTbl, $scope.keyRoleWorkers];
								} else if ($scope.dTable == 'user') {
									return [$scope.dTable, $scope.$$childTail.userCurrentTbl, $scope.userUid];
								}
							}, selectedPayrollJobs: function () {
								if ($scope.dTable == 'user') {
									return $scope.selectedItems.jobs;
								}
							}, date: function () {
								return $scope.dateRange.to;
							}
						};

						var modalInstance = $uibModal.open({
							template: tpl,
							size: 'md',
							controller: ctrl,
							resolve
						});

						modalInstance.result.then(function () {
						});
					} else {
						toastr.warning('Nothing selected', 'Warning');
					}
				}
			}
		}

		function calculateSelectedItems() {
			$scope.selectedWorkers = [];
			$scope.selectedItems = {
				misc: [], jobs: [], paychecks: []
			};

			if ($scope.dTable == 'workers') {
				_.each($scope.$$childTail.workersTbl, function (worker, key) {
					if (worker.a_employee_selected) {
						$scope.selectedWorkers.push(worker);
					}
				});
			} else if ($scope.dTable == 'user' && $scope.$$childTail.userCurrentTbl) {

				_.each($scope.$$childTail.userCurrentTbl.jobs, function (job, key) {
					if (job.a_a_selected) {
						$scope.selectedItems.jobs.push({
							nid: job.a_job_misc.split('-')[0],
							contract_type: angular.isDefined(job.contract_type) ? job.contract_type : $scope.contractTypes.PAYROLL
						});
					}
				});

				for (var index in $scope.$$childTail.userCurrentTbl.misc) {
					var misc = $scope.$$childTail.userCurrentTbl.misc[index];
					var hourlyMisc = angular.copy($scope.$$childTail.userCurrentTbl.hourly_misc[index]);

					if (misc.a_a_selected) {
						$scope.selectedItems.misc.push(hourlyMisc);
					}
				}

				for (var index in $scope.$$childTail.userCurrentTbl.users_paychecks) {
					var paycheck = angular.copy($scope.$$childTail.userCurrentTbl.users_paychecks[index]);

					if (paycheck.a_a_selected) {
						$scope.selectedItems.paychecks.push(paycheck);
					}
				}
			}
		}

		function deletePayroll() {
			if (!_.isEmpty($scope.selectedItems.jobs) || !_.isEmpty($scope.selectedItems.misc)) {

				SweetAlert
					.swal({
						title: 'Are you sure you want to do it?',
						type: 'warning',
						showCancelButton: true,
						cancelButtonText: 'No',
						confirmButtonText: 'Yes'
					}, function (isConfirm) {
						if (isConfirm) {
							$scope.busy = true;
							var promises = [];

							if (!_.isEmpty($scope.selectedItems.jobs)) {
								promises.push(removeJobs());
							}

							if (!_.isEmpty($scope.selectedItems.misc)) {
								promises.push(removeMiscPayment());
							}

							$q.all(promises)
								.then(function () {
								}).finally(function () {
								$scope.busy = false;
							});
						}
					});
			}
		}

		function removeJobs() {
			var deferred = $q.defer();
			var promises = [];
			var requests = [];

			_.each($scope.selectedItems.jobs, function (job, index) {
				var nid = job.a_job_misc || job.nid;
				var uid = $scope.userUid;

				let data = {
					nid: nid.split('-')[0],
					uid: uid,
					contract_type: angular.isDefined(job.contract_type) ? job.contract_type : $scope.contractTypes.PAYROLL
				};

				requests.push(angular.extend(data, {nid}));
				promises.push(RequestServices.removeUserPayroll(data));
			});

			$q.all(promises)
				.then(function () {
					let msgs = [{
						simpleText: 'Worker ' + $scope.userName + ' removed from request'
					}];

					$rootScope.$broadcast(PayrollConfigs.events.REMOVE_USER_FROM_PAYROLL_OF_REQUEST, requests);
					$q.all(_.map(requests, req => RequestServices.sendLogs(msgs, 'Worker was deleted', req.nid, 'MOVEREQUEST')));

					toastr.success('Delete user from payroll of request!', 'Success');
					deferred.resolve();
				}, function () {
					toastr.error('Error when delete user from payroll of request!', 'Error');
					deferred.reject();
				});

			return deferred.promise;
		}

		function removeMiscPayment() {
			var deferred = $q.defer();
			var miscKeys = _.keys($scope.selectedItems.misc);
			var misc = $scope.selectedItems.misc;
			var miscPromise = [];

			for (var i in miscKeys) {
				var data = {
					id: misc[i].id, date: misc[i].date
				};

				miscPromise.push(PayrollService.removeMiscPayment(data));
			}

			$q.all(miscPromise)
				.then(function (data) {
					for (var i in miscKeys) {
						$rootScope.$broadcast(PayrollConfigs.events.LIVE_UPDATE_USER_PAYROLL_TABLE, PayrollService.prepareEventDataObject(PayrollConfigs.actions.REMOVE, misc[i], 'misc'));
					}

					toastr.success('Delete misc payment from payroll!', 'Success');
					deferred.resolve();
				}, function () {
					toastr.error('Error when delete misc payment from payroll!', 'Error');
					deferred.reject();
				});
		}

		function chooseUser(id) {
			var b = id.split(',');
			$scope.userName = b[1];
			$scope.userUid = b[0];
			$scope.keyRoleWorkers = $scope.workersRoles[b[2]];
			$scope.roleWorkers = b[2];
			$scope.dTable = 'user';
		}


		function changeperiod(id) {
			var currentDayOfMonth = moment().date();

			if (currentDayOfMonth >= FIRST_DAY_OF_MONTH && currentDayOfMonth <= FIFTEENTH_DAY_OF_MONTH) {

				if (id == 1) {
					$scope.dateRange.from = moment().date(FIRST_DAY_OF_MONTH).toDate();
					$scope.dateRange.to = moment().date(FIFTEENTH_DAY_OF_MONTH).toDate();
				}

				if (id == 2) {
					var lastDayOfLastMonth = moment().subtract(FIRST_DAY_OF_MONTH, 'month').endOf('month').date();
					$scope.dateRange.from = moment().subtract(FIRST_DAY_OF_MONTH, 'month').date(SIXTEENTH_DAY_OF_MONTH).toDate();
					$scope.dateRange.to = moment().subtract(FIRST_DAY_OF_MONTH, 'month').date(lastDayOfLastMonth).toDate();
				}
			} else {
				if (id == 1) {
					var lastDayOfThisMonth = moment().endOf('month').date();
					$scope.dateRange.from = moment().date(SIXTEENTH_DAY_OF_MONTH).toDate();
					$scope.dateRange.to = moment().date(lastDayOfThisMonth).toDate();
				}

				if (id == 2) {
					$scope.dateRange.from = moment().date(FIRST_DAY_OF_MONTH).toDate();
					$scope.dateRange.to = moment().date(FIFTEENTH_DAY_OF_MONTH).toDate();
				}
			}

			getByDate();
		}

		function init() {
			var pUsers = DispatchService.getUsersByRole({
				active: 1, role: ['helper', 'foreman', 'manager', 'sales', 'driver', 'customer service']
			});

			$q.all([pUsers])
				.then(function (response) {
					$scope.users = response[0];

					if (_.isUndefined(settings.payroll_time)) {
						$scope.dateRangeFrom = moment(new Date($scope.dateRange.from)).format('MMM DD, YYYY');
						$scope.dateRangeTo = moment(new Date($scope.dateRange.to)).format('MMM DD, YYYY');
					} else {
						$scope.dateRangeFrom = moment(settings.payroll_time.from).format('MMM DD, YYYY');
						$scope.dateRangeTo = moment(settings.payroll_time.to).format('MMM DD, YYYY');
					}
				})
				.finally(function () {
					$scope.busy = false;
				});
		}

		function getByDate() {
			settings.payroll_time = {};
			settings.payroll_time.from = moment(new Date($scope.dateRange.from)).format('YYYY-MM-DD');
			settings.payroll_time.to = moment(new Date($scope.dateRange.to)).format('YYYY-MM-DD');
			$scope.dateRangeFrom = moment(settings.payroll_time.from).format('MMM DD, YYYY');
			$scope.dateRangeTo = moment(settings.payroll_time.to).format('MMM DD, YYYY');

			var setting_name = 'basicsettings';
			SettingServices.saveSettings(settings, setting_name);

			if ($scope.dTable == 'departments') {
				$scope.$broadcast(PayrollConfigs.events.GET_BY_DATE_DEPARTMENTS);
			} else if ($scope.dTable == 'workers') {
				$scope.$broadcast(PayrollConfigs.events.GET_BY_DATE_WORKERS);
			} else if ($scope.dTable == 'user') {
				$scope.$broadcast(PayrollConfigs.events.GET_BY_DATE_USER);
			}
		}

		function isVisibleApplyPayment() {
			var result = isDisplayUserTable();
			//hide for workers table not working apply payment
			//|| isDisplayWorkersTable();

			if (result) {
				calculateSelectedItems();

				return ($scope.selectedWorkers.length != 0 || $scope.selectedItems.misc.length != 0 || $scope.selectedItems.jobs.length != 0) && $scope.selectedItems.paychecks.length == 0;
			} else {
				return false;
			}
		}

		function isVisibleDeletePayrollButton() {
			var result = isDisplayUserTable();

			if (result) {
				var isEmptyJobs = _.isEmpty($scope.selectedItems.jobs);
				var isEmptyMiscPayment = _.isEmpty($scope.selectedItems.misc);
				var isEmptyPaychecks = _.isEmpty($scope.selectedItems.paychecks);

				result = (!isEmptyJobs || !isEmptyMiscPayment) && isEmptyPaychecks;
			}

			return result;
		}

		function isDisplayUserTable() {
			return $scope.dTable == 'user';
		}

		function isDisplayWorkersTable() {
			return $scope.dTable == 'workers';
		}

		function isDisplayDepartmentTable() {
			return $scope.dTable == 'departments';
		}

		function updatePayrollCache() {
			let data = {
				'date_from': typeof $scope.dateRange.from == 'string' ? moment($scope.dateRange.from, 'MMM DD, YYYY').format('YYYY-MM-DD') : moment($scope.dateRange.from).format('YYYY-MM-DD'),
				'date_to': typeof $scope.dateRange.to == 'string' ? moment($scope.dateRange.to, 'MMM DD, YYYY').format('YYYY-MM-DD') : moment($scope.dateRange.to).format('YYYY-MM-DD')
			};
			$scope.busy = true;
			PayrollService.updatePayrollCache(data).then(function () {
				//update payroll table (global/workers/users)
				if ($scope.dTable == 'departments') {
					$scope.$broadcast(PayrollConfigs.events.GET_BY_DATE_DEPARTMENTS);
				} else if ($scope.dTable == 'workers') {
					$scope.$broadcast(PayrollConfigs.events.GET_BY_DATE_WORKERS);
				} else if ($scope.dTable == 'user') {
					$scope.$broadcast(PayrollConfigs.events.GET_BY_DATE_USER);
				}
			});
		}

		$scope.isShowUserTotalItem = function (key) {
			return unvisibleItem.indexOf(key) < 0 && $scope.mainColumns[$scope.dTable].totalFields[key] && $scope.mainColumns[$scope.dTable].totalFields[key].selected && key !== 'closing';
		};

		$scope.$on('payroll_table_loaded', function (event, data) {
			switch ($scope.dTable) {
				case 'user':
					$scope.userCurrentTbl = data;
					break;
				case 'workers':
					$scope.workers = data;
					break;
				case 'departments':
					$scope.departments = data;
					break;
			}
			$scope.busy = false;
		});

		$scope.$watch('dTable', function (newValue, oldValue) {
			if (_.isEqual(newValue, 'workers')) {
				$scope.userUid = '';
			}
		});

		function openModal(modal) {
			var ctrl = '', tpl = '', resolve = {};

			switch (modal) {
				case 'columns':
					ctrl = 'PayrollColumnModalCtrl';
					tpl = require('./templates/edit-columns.html');
					resolve = {
						dTable: function () {
							return [$scope.dTable, $scope.mainColumns, settings];
						}
					};
					break;
				case 'totalColumns':
					ctrl = 'PayrollColumnModalCtrl';
					tpl = require('./templates/edit-total-columns.html');
					resolve = {
						dTable: function () {
							return [$scope.dTable, $scope.mainColumns, settings];
						}
					};
					break;
				case 'addMiscPayment' :
					ctrl = 'PayrollAddMiscModalCtrl';
					tpl = require('./templates/addMiscPayment.html');
					resolve = {
						users: function () {
							return [$scope.users, $scope.dTable];
						}, userUid: function () {
							return $scope.userUid;
						}, miscDate: function () {
							return $scope.dateRange.from;
						}
					};
					break;
			}

			var modalInstance = $uibModal.open({
				template: tpl,
				size: 'md',
				controller: ctrl,
				resolve
			});

			modalInstance.result.then(function () {
			});
		}

		init();

		$scope.setUpPayrollHeader = setUpPayrollHeader();

		function setUpPayrollHeader() {
			setPayrollHeader();

			var windowElement = angular.element('#main-wrapper');

			windowElement.scroll(function () {
				var scrollTop = windowElement.scrollTop();
				var offsetTop = Math.abs(payrollHeader.offset().top) + PAYROLL_HEADER_OFFSET;
				$scope.isShowPayrollHeader = scrollTop < offsetTop;
				fixedPayrollHeader.css('top', scrollTop - PAYROLL_HEADER_POSITION_OFFSET);
			});
		}

		function setPayrollHeader() {
			if (!payrollHeader.length) {
				payrollHeader = angular.element('#payroll_header');
			}

			if (!fixedPayrollHeader.length) {
				fixedPayrollHeader = angular.element('#fixed_payroll_header');
			}
		}
	}
})();
