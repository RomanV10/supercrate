(function () {
    'use strict';
    angular

        .module('app.dispatch')
        .controller('PayrollEditMiscPaymentCtrl', PayrollEditMiscPaymentCtrl);

    PayrollEditMiscPaymentCtrl.$inject = ['$scope', '$uibModalInstance', 'misc', 'PayrollService', 'PayrollConfigs', '$rootScope', 'MomentWrapperService', 'SweetAlert'];

    function PayrollEditMiscPaymentCtrl($scope, $uibModalInstance, misc, PayrollService, PayrollConfigs, $rootScope, MomentWrapperService, SweetAlert) {
        $scope.enumTypes = $rootScope.fieldData.enums;
        $scope.dateOptions = {
            formatYear: 'YYYY',
            startingDay: 0
        };
        $scope.misc = angular.copy(misc[0]);
        $scope.usersByRole = angular.copy(misc[1]);
        $scope.misc.amount = Number($scope.misc.amount);
        $scope.misc.amount_options = Number($scope.misc.amount_options || 0);
        $scope.misc.hourlyRate = Number($scope.misc.hourly_rate);
        $scope.misc.hours = Number($scope.misc.hours);
        $scope.misc.type = Number($scope.misc.type);

        var isNotUnixDate = _.isNaN(Number($scope.misc.date));

        if (isNotUnixDate) {
            $scope.misc.date = moment($scope.misc.date).toDate();
        } else {
            $scope.misc.date = moment(moment.unix($scope.misc.date).utcOffset(0).format('MMM DD, YYYY'), 'MMM DD, YYYY').toDate();
        }

        if ($scope.misc.type == 0) {
            $scope.misc.payType = 'hourlyRate';
        } else if ($scope.misc.type == 1) {
            $scope.misc.payType = 'amount';
        }

        if ($scope.misc.amount_options == 0) {
            $scope.misc.amountType = 'payable';
        } else if ($scope.misc.amount_options == 1) {
            $scope.misc.amountType = 'receivable';
        } else if ($scope.misc.amount_options == 2) {
            $scope.misc.amountType = 'deduct';
        }

        $scope.changeAmount = changeAmount;
        $scope.saveMisc = saveMisc;
        $scope.removeMisc = removeMisc;
        $scope.cancel = cancel;

        function changeAmount() {
            $scope.misc.amount = _.round($scope.misc.hourlyRate * $scope.misc.hours, 2);
        }

        function saveMisc() {
            $scope.savingMisc = false;
            $scope.message = '';

            if(_.isEmpty($scope.misc.payee)){
                $scope.savingMisc = true;
                $scope.message += "Choose Payee<br>"
            }

            if ($scope.misc.payType == 'hourlyRate') {
                if ($scope.misc.hourlyRate == '' || angular.isUndefined($scope.misc.hourlyRate) || $scope.misc.hourlyRate == null || $scope.misc.hourlyRate == 0) {
                    $scope.savingMisc = true;
                    $scope.message += "Enter Hourly Rate<br>"
                }

                if ($scope.misc.hours == '' || angular.isUndefined($scope.misc.hours) || $scope.misc.hours == null || $scope.misc.hours == 0) {
                    $scope.savingMisc = true;
                    $scope.message += "Enter the hours<br>"
                }
            } else {
                if ($scope.misc.amountType == '' || angular.isUndefined($scope.misc.amountType) || $scope.misc.amountType == null || $scope.misc.amountType == 0) {
                    $scope.savingMisc = true;
                    $scope.message += "Choose Amount Type<br>"
                }

                if ($scope.misc.amount == '' || angular.isUndefined($scope.misc.amount) || $scope.misc.amount == null) {
                    $scope.savingMisc = true;
                    $scope.message += "Enter the amount<br>"
                }
            }

            if ($scope.misc.date == '' || angular.isUndefined($scope.misc.date) || $scope.misc.date == null) {
                $scope.savingMisc = true;
                $scope.message += "Select date<br>"
            }

            if ($scope.savingMisc) {
                swal({
                    title: 'Error!',
                    text: $scope.message,
                    type: 'error',
                    html: true
                });
            } else {
                $scope.busy = true;
                var misc = angular.copy($scope.misc);
                var req = {
                    date: moment(misc.date).format('YYYY-MM-DD'),
                    payee: misc.payee,
                    note: misc.note,
                    type: _.has($scope.enumTypes, 'payroll_type_payment')
                        ? $scope.enumTypes.payroll_type_payment[misc.payType.toUpperCase()] : 0
                };

                if (misc.payType == 'hourlyRate') {
                    req.hourly_rate = misc.hourlyRate;
                    req.hours = misc.hours;
                } else {
                    req.amount = misc.amount;
                    req.amount_options = _.has($scope.enumTypes, 'payroll_amount_options') ? $scope.enumTypes.payroll_amount_options[misc.amountType.toUpperCase()] : 0;
                }

                var data = {
                    id: misc.id,
                    data: req
                };

                PayrollService.updateMiscPayment(data)
                    .then(function () {
                        toastr.success('Misc payment successfully updated', 'Success');
                        cancel();
                    })
                    .finally(function () {
                        req.id = misc.id;
                        $rootScope.$broadcast(PayrollConfigs.events.LIVE_UPDATE_USER_PAYROLL_TABLE, PayrollService.prepareEventDataObject(PayrollConfigs.actions.UPDATE, req, "misc"));
                        $scope.busy = false;
                    });
            }
        }

        function removeMisc() {
            SweetAlert.swal({
                title: "Delete Misc ?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E47059",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $scope.busy = true;
                    var data = {
                        id: $scope.misc.id,
                    };

                    PayrollService.removeMiscPayment(data)
                        .then(function () {
                            toastr.success('Misc successfully removed', 'Success');
                            cancel();
                        })
                        .finally(function () {
                            $rootScope.$broadcast(PayrollConfigs.events.LIVE_UPDATE_USER_PAYROLL_TABLE, PayrollService.prepareEventDataObject(PayrollConfigs.actions.REMOVE, $scope.misc, "misc"));
                            $scope.busy = false;
                        });
                }
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();