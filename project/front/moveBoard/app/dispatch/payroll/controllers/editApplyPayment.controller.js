(function () {
    'use strict';
    angular

        .module('app.dispatch')
        .controller('PayrollEditPaymentCtrl', PayrollEditPaymentCtrl);

    PayrollEditPaymentCtrl.$inject = ['$scope', '$uibModalInstance', 'payment', 'PayrollService', '$http', '$q', '$rootScope', 'PayrollConfigs', 'SweetAlert'];

    function PayrollEditPaymentCtrl($scope, $uibModalInstance, payment, PayrollService, $http, $q, $rootScope, PayrollConfigs, SweetAlert) {
        $scope.paymentOpt = {
            '0': 'Check',
            '1': 'Cash'
        };
        $scope.payment = angular.copy(payment);
        $scope.payment.date = moment(moment.unix($scope.payment.date).utcOffset(0).format('MMM DD, YYYY'), 'MMM DD, YYYY').toDate();

        if ($scope.payment.created != 0) {
            $scope.payment.created = moment.unix($scope.payment.created).utcOffset(0).format('MMM DD, YYYY')
        }

        $scope.payment.formPayment = $scope.payment.form;

        if (Number($scope.payment.pending)) {
            $scope.payment.pending = true;
        } else {
            $scope.payment.pending = false;
        }

        $scope.removePaycheck = removePaycheck;
        $scope.savePaycheck = savePaycheck;
        $scope.cancel = cancel;

        function savePaycheck() {
            $scope.savingPaycheck = false;
            $scope.message = '';

            if ($scope.payment.formPayment == '' || angular.isUndefined($scope.payment.formPayment) || $scope.payment.formPayment == null) {
                $scope.savingPaycheck = true;
                $scope.message += "Select form of payment<br>"
            }

            if ($scope.payment.amount == '' || angular.isUndefined($scope.payment.amount) || $scope.payment.amount == null) {
                $scope.savingPaycheck = true;
                $scope.message += "Enter the amount<br>"
            }

            if ($scope.payment.date == '' || angular.isUndefined($scope.payment.date) || $scope.payment.date == null) {
                $scope.savingPaycheck = true;
                $scope.message += "Select payment date<br>"
            }

            if ($scope.savingPaycheck) {
                swal({
                    title: 'Error!',
                    text: $scope.message,
                    type: 'error',
                    html: true
                });
            } else {
                $scope.busy = true;
                $scope.selectedJobs = [];
                $scope.selectedMisc = [];
                var check = {
                    rid: $scope.payment.payment_id,
                    data: {
                        date: moment($scope.payment.date).format('YYYY-MM-DD'),
                        form: $scope.payment.formPayment,
                        amount: $scope.payment.amount,
                        note: $scope.payment.note,
                        pending: $scope.payment.pending,
                        created: moment.utc($scope.payment.created, 'MMM DD, YYYY').unix()
                    }
                };

                if ($scope.payment.checkNo) {
                    check.data.check_numb = $scope.payment.checkNo;
                }

                if ($scope.selectedMisc.length > 0) {
                    check.data.hourly_misc = $scope.selectedMisc;
                }

                if ($scope.selectedJobs.length > 0) {
                    check.data.jobs = $scope.selectedJobs;
                }

                PayrollService
                    .updatePaycheckPayment(check)
                    .then(function (data) {
                        check.data.payment_id = $scope.payment.payment_id;
                        check.data.date = moment.utc(check.data.date, 'YYYY-MM-DD').unix();

                        if (_.isUndefined(check.data.check_numb)) {
                            check.data.check_numb = 0;
                        }

                        $rootScope.$broadcast(PayrollConfigs.events.LIVE_UPDATE_USER_PAYROLL_TABLE, PayrollService.prepareEventDataObject(PayrollConfigs.actions.UPDATE, check.data, "paycheck"));
                        $scope.busy = false;
                        $uibModalInstance.dismiss('cancel');
                    })
            }
        }

        function removePaycheck() {
            SweetAlert.swal({
                title: "Delete paycheck?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E47059",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $scope.busy = true;
                    $scope.selectedJobs = [];
                    $scope.selectedMisc = [];
                    var check = {
                        rid: $scope.payment.payment_id,
                    };

                    PayrollService.removePaycheckPayment(check).then(function (data) {
                        $rootScope.$broadcast(PayrollConfigs.events.LIVE_UPDATE_USER_PAYROLL_TABLE, PayrollService.prepareEventDataObject(PayrollConfigs.actions.REMOVE, $scope.payment, "paycheck"));
                        $scope.busy = false;
                        $uibModalInstance.dismiss('cancel');
                    })
                }
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();
