(function () {
    'use strict';
    angular

        .module('app.dispatch')
        .controller('PayrollColumnModalCtrl', ColumnModalInstCtrl);

    ColumnModalInstCtrl.$inject = ['$scope', '$uibModalInstance', 'dTable', '$rootScope', 'PayrollService'];

    function ColumnModalInstCtrl($scope, $uibModalInstance, dTable, $rootScope, PayrollService) {

        let settings = dTable[2];
        $scope.columns = angular.copy(dTable[1][dTable[0]]);

        $scope.$on('$destroy', () => {
            angular.extend(dTable[1][dTable[0]], $scope.columns);
			if(_.isUndefined(settings.columns)) {
				settings.columns = {};
			}
			settings.columns[dTable[0]] = angular.copy($scope.columns);
			PayrollService
                .saveColumns('basicsettings', settings)
        });
    }
})();
