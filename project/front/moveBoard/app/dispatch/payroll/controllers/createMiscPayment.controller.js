(function () {
    'use strict';
    angular

        .module('app.dispatch')
        .controller('PayrollAddMiscModalCtrl', AddMiscModalInstCtrl);

    AddMiscModalInstCtrl.$inject = ['$scope', '$uibModalInstance', 'users', 'userUid', 'PayrollConfigs', '$rootScope', 'datacontext', 'PayrollService', 'miscDate'];

    function AddMiscModalInstCtrl($scope, $uibModalInstance, users, userUid, PayrollConfigs, $rootScope, datacontext, PayrollService, miscDate) {

        $scope.usersByRole = users[0];
        $scope.userUid = userUid;
        $scope.dTable = users[1];
        $scope.dateOptions = {
            formatYear: 'YYYY',
            startingDay: 0
        };

        let fieldData = datacontext.getFieldData();
        let enumTypes = fieldData.enums;

        $scope.misc = {
            date: new Date(),
            payType: 'hourlyRate',
            amountType: 'payable',
            payee: '',
            note: '',
            hourlyRate: 0,
            hours: 0,
            amount: 0
        };

        if ($scope.dTable == "user" && !_.isEmpty($scope.userUid)) {
            $scope.misc.payee = $scope.userUid;
        }

        $scope.$watch('misc.payee', function (newValue) {
            if (!angular.equals(newValue, "")) {
                var user = findWorkerByUID(newValue);

                if (angular.isDefined(user) && angular.isDefined(user.settings.rateCommission)) {
                    var rateCommission = user.settings.rateCommission;

                    for (var key in rateCommission) {
                        if (rateCommission[key]["option"] == "Hourly Rate") {
                            $scope.misc.hourlyRate = rateCommission[key]["input"];
                        }
                    }
                }
            }
        });

        function findWorkerByUID(UID) {
            var user;

            _.each($scope.usersByRole, function (role, index) {
                _.each(role, function (roleUser, uid) {
                    if (UID == uid) {
                        user = roleUser;
                    }
                });
            });

            return user;
        }

        $scope.changeAmount = changeAmount;
        $scope.saveMisc = saveMisc;
        $scope.cancel = cancel;

        function changeAmount() {
            $scope.misc.amount = _.round($scope.misc.hourlyRate * $scope.misc.hours, 2);
        }

        function saveMisc() {
            $scope.savingMisc = false;
            $scope.message = '';

            if(_.isEmpty($scope.misc.payee)){
                $scope.savingMisc = true;
                $scope.message += "Choose Payee<br>"
            }

            if ($scope.misc.payType == 'hourlyRate') {
                if ($scope.misc.hourlyRate == '' || angular.isUndefined($scope.misc.hourlyRate) || $scope.misc.hourlyRate == null) {
                    $scope.savingMisc = true;
                    $scope.message += "Enter Hourly Rate<br>"
                }

                if ($scope.misc.hours == '' || angular.isUndefined($scope.misc.hours) || $scope.misc.hours == null) {
                    $scope.savingMisc = true;
                    $scope.message += "Enter the hours<br>"
                }
            } else {
                if ($scope.misc.amountType == '' || angular.isUndefined($scope.misc.amountType) || $scope.misc.amountType == null) {
                    $scope.savingMisc = true;
                    $scope.message += "Choose Amount Type<br>"
                }

                if ($scope.misc.amount == '' || angular.isUndefined($scope.misc.amount) || $scope.misc.amount == null) {
                    $scope.savingMisc = true;
                    $scope.message += "Enter the amount<br>"
                }
            }

            if ($scope.misc.date == '' || angular.isUndefined($scope.misc.date) || $scope.misc.date == null) {
                $scope.savingMisc = true;
                $scope.message += "Select date<br>"
            }

            if ($scope.savingMisc) {
                swal({
                    title: 'Error!',
                    text: $scope.message,
                    type: 'error',
                    html: true
                });
            } else {
                $scope.busy = true;
                var misc = _.clone($scope.misc);
                var req = {
                    date: moment(misc.date).format('YYYY-MM-DD'),
                    payee: misc.payee,
                    note: misc.note,
                    type: _.has(enumTypes, 'payroll_type_payment')
                        ? enumTypes.payroll_type_payment[misc.payType.toUpperCase()] : 0
                };

                if (misc.payType == 'hourlyRate') {
                    req.hourly = {
                        hourly_rate: misc.hourlyRate,
                        hours: misc.hours
                    };
                } else {
                    req.amount = {
                        sum: misc.amount,
                        amount_options: _.has(enumTypes, 'payroll_amount_options')
                            ? enumTypes.payroll_amount_options[misc.amountType.toUpperCase()] : 0
                    };
                }

                PayrollService.setMiscPayment(req)
                    .then(function (data) {
                        switch ($scope.dTable) {
                            case 'departments':
                                $rootScope.$broadcast(PayrollConfigs.events.GET_BY_DATE_DEPARTMENTS);
                                break;
                            case 'workers':
                                $rootScope.$broadcast(PayrollConfigs.events.GET_BY_DATE_WORKERS);
                                break;
                            case 'user':
                                if (data[0]) {
                                    req.id = data[0];
                                }

                                if (misc.payType == 'hourlyRate') {
                                    req.hourly_rate = misc.hourlyRate;
                                    req.hours = misc.hours;

                                    delete req.hourly;
                                } else {
                                    req.sum = misc.amount;
                                    req.amount_options = _.has(enumTypes, 'payroll_amount_options')
                                        ? enumTypes.payroll_amount_options[misc.amountType.toUpperCase()] : 0;
                                }

                                req.date = moment(req.date, "YYYY-MM-DD").unix();

                                req.amount = _.isUndefined(misc.amount) ? 0 : misc.amount;

                                $rootScope.$broadcast(PayrollConfigs.events.LIVE_UPDATE_USER_PAYROLL_TABLE, PayrollService.prepareEventDataObject(PayrollConfigs.actions.CREATE, req, "misc"));
                                break;
                        }

                        toastr.success('Successfully created misc. payment', 'Success');
                        cancel();
                    })
                    .finally(function () {
                        $scope.busy = false;
                    });
            }
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();