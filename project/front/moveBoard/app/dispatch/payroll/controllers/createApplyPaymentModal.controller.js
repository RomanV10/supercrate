(function () {
    'use strict';
    angular

        .module('app.dispatch')
        .controller('ApplyPaymentModalCtrl', ApplyPaymentModalCtrl);

    ApplyPaymentModalCtrl.$inject = ['$scope', '$uibModalInstance', 'data', '$rootScope', 'PayrollConfigs', 'selectedPayrollJobs', 'PayrollService', 'date'];

    function ApplyPaymentModalCtrl($scope, $uibModalInstance, data, $rootScope, PayrollConfigs, selectedPayrollJobs, PayrollService, date) {
        $scope.paymentOpt = {
            '0': 'Check',
            '1': 'Cash'
        };
        $scope.payment = {};
        $scope.selectedPayrollJobs = [];

        if (angular.isDefined(selectedPayrollJobs)) {
            $scope.selectedPayrollJobs = selectedPayrollJobs;
        }

		$scope.payment.date = new Date(date);

        $scope.allJobs = _.clone(data[1]);
        $scope.switchTable = _.clone(data[0]);
        $scope.savePayment = savePayment;
        $scope.userUidSelect = _.clone(data[2]);
        $scope.cancel = cancel;
        $scope.amount = 0;
        $scope.payment.formPayment = '1';

        if ($scope.switchTable == 'workers') {
            for (var i in $scope.allJobs) {
                if ($scope.allJobs[i].a_employee_selected == true) {
                    $scope.amount += $scope.allJobs[i].total;
                }
            }
        } else if ($scope.switchTable == 'user') {
            for (var i in $scope.allJobs.jobs) {
                if ($scope.allJobs.jobs[i].a_a_selected) {
                    $scope.amount += $scope.allJobs.jobs[i].total;
                }
            }

            for (var i in $scope.allJobs.misc) {
                var misc = $scope.allJobs.misc[i];

                if (misc.a_a_selected) {
                    $scope.amount += misc.total;
                }
            }

            $scope.amount = $scope.amount.toFixed(2);
        }

        $scope.payment.amount = $scope.amount;
        $scope.payment.pending = false;

        function savePayment() {
            $scope.savingPaycheck = false;
            $scope.message = '';

            if ($scope.payment.formPayment == '' || angular.isUndefined($scope.payment.formPayment) || $scope.payment.formPayment == null) {
                $scope.savingPaycheck = true;
                $scope.message += "Select form of payment<br>"
            }

            if ($scope.payment.amount == '' || angular.isUndefined($scope.payment.amount) || $scope.payment.amount == null) {
                $scope.savingPaycheck = true;
                $scope.message += "Enter the amount<br>"
            }

            if ($scope.payment.date == '' || angular.isUndefined($scope.payment.date) || $scope.payment.date == null) {
                $scope.savingPaycheck = true;
                $scope.message += "Select payment date<br>"
            }

            if ($scope.savingPaycheck) {
                swal({
                    title: 'Error!',
                    text: $scope.message,
                    type: 'error',
                    html: true
                });
            } else {
                $scope.busy = true;
                $scope.selectedJobs = $scope.selectedPayrollJobs;
                $scope.selectedMisc = [];

                if ($scope.switchTable == 'workers') {
                    for (var i in $scope.allJobs) {
                        if ($scope.allJobs[i].a_employee_selected == true) {

                        }
                    }
                } else if ($scope.switchTable == 'user') {
                    for (var i in $scope.allJobs.misc) {
                        if ($scope.allJobs.misc[i].a_a_selected == true) {
                            $scope.selectedMisc.push(i);
                        }
                    }

                    var check = {
                        uid: $scope.userUidSelect,
                        data: {
                            date: moment($scope.payment.date).format('YYYY-MM-DD'),
                            form: $scope.payment.formPayment.toString(),
                            amount: $scope.payment.amount.toString()
                        }
                    };

                    if ($scope.payment.note) {
                        check.data.note = $scope.payment.note;
                    }

                    if ($scope.payment.pending) {
                        check.data.pending = '1';
                    } else {
                        check.data.pending = '0';
                    }

                    if ($scope.payment.checkNo) {
                        check.data.check_numb = $scope.payment.checkNo;
                    }

                    if ($scope.selectedMisc.length > 0) {
                        check.data.hourly_misc = $scope.selectedMisc;
                    }

                    if ($scope.selectedJobs.length > 0) {
                        check.data.jobs = $scope.selectedJobs;
                    }

                    PayrollService
                        .userPaycheck(check)
                        .then(function (data) {
                            $scope.busy = false;

                            if ($scope.switchTable == 'workers') {
                                $rootScope.$broadcast(PayrollConfigs.events.GET_BY_DATE_WORKERS);
                                toastr.success('Successfully created payment', 'Success');
                            } else if ($scope.switchTable == 'user') {
                                check.data.payment_id = data[0];
                                check.data.date = moment.utc(check.data.date, 'YYYY-MM-DD').unix();
                                check.data.created = moment.utc().unix();

                                if (_.isUndefined(check.data.check_numb)) {
                                    check.data.check_numb = 0;
                                }

                                $rootScope.$broadcast(PayrollConfigs.events.LIVE_UPDATE_USER_PAYROLL_TABLE, PayrollService.prepareEventDataObject(PayrollConfigs.actions.CREATE, check.data, "paycheck"));
                                toastr.success('Successfully created payment', 'Success');
                            }

                            $uibModalInstance.dismiss('cancel');
                        })
                }
            }
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }

})();
