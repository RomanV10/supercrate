'use strict';
angular

	.module('app.dispatch')
	.controller('ClosedJobs', ClosedJobs);

ClosedJobs.$inject = ['$scope', '$rootScope', 'RequestServices', 'common'];

function ClosedJobs($scope, $rootScope, RequestServices, common) {
	$scope.busy = false;
	$scope.dates = {
		from: moment().startOf('month').toDate(),
		to: moment().endOf('month').toDate()
	};
	var importedFlags = $rootScope.availableFlags;
	var Job_Completed = 'Completed';

	$scope.prevMonth = prevMonth;
	$scope.nextMonth = nextMonth;
	$scope.getClosedJobs = getClosedJobs;

	function prevMonth() {
		$scope.dates.from = moment($scope.dates.from)
			.subtract(1, 'M')
			.startOf('month')
			.toDate();

		$scope.dates.to = moment($scope.dates.from)
			.endOf('month')
			.toDate();

		getClosedJobs();
	}

	function nextMonth() {
		$scope.dates.from = moment($scope.dates.from)
			.add(1, 'M')
			.startOf('month')
			.toDate();
		$scope.dates.to = moment($scope.dates.from)
			.endOf('month')
			.toDate();
		getClosedJobs();
	}


	function getClosedJobs() {
		$scope.busy = true;
		var data = {
			from: moment($scope.dates.from).format('YYYY-MM-DD'),
			to: moment($scope.dates.to).format('YYYY-MM-DD')
		};
		if (_.isEmpty(importedFlags)) {
			common.$timeout(function () {
				getClosedJobs();
			}, 300);
		} else {
			$scope.company_flags = importedFlags.company_flags[Job_Completed];

			RequestServices.getClosedJobs(data, $scope.company_flags).then(function (data) {
				$scope.requests = [];

				angular.forEach(data.nodes, function (item) {
					$scope.requests.push(item);
				});

				$scope.totalCount = data.count;
				$scope.busy = false;
			});
		}
	}

	getClosedJobs();
}