(function(){

    angular.module("app")
        .config([
            "$stateProvider",
            function ($stateProvider)
            {
                $stateProvider
                    .state('dispatch', {
                        url: '/dispatch',
                        abstract: true,
                        template: '<ui-view/>'
                    })
                    .state('dispatch.local', {
                        url: '/local',
                        template: require('./local.dispatch/index.html'),
                        title: 'Dispatch Local',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('dispatch.payroll', {
                        url: '/payroll',
	                    template: require('./payroll/templates/payroll.html'),
                        title: 'Payroll',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('dispatch.closed-jobs', {
                        url: '/closed-jobs',
	                    template: require('./closed.jobs/closedJobs.html'),
                        title: 'Closed jobs',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('dispatch.opened-jobs', {
                        url: '/opened-jobs',
	                    template: require('./opened.jobs/openedJobs.html'),
                        title: 'Opened jobs',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    })
                    .state('dispatch.payroll-status', {
                        url: '/payroll-status',
	                    template: require('./payroll.status/payrollStatus.html'),
                        title: 'payrollStatus',
                        data: {
                            permissions: {
                                except: ['anonymous', 'foreman', 'helper']
                            }
                        }
                    });


            }
        ]
    );
})();
