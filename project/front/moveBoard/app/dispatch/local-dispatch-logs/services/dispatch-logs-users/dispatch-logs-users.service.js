'use strict';

angular
	.module('app.dispatch')
	.factory('dispatchLogsUsersService', dispatchLogsUsersService);

/* @ngInject */
function dispatchLogsUsersService() {
	let _workers = {};
	let _crews = [];

	return {
		initDispatchWorkers,
		initDispatchCrews,
		getDispatchCrews,
		getWorkersNameByIds,
		getRequestWorkersById,
		getWorkerById,

		_workers,
		_crews,
	};

	function initDispatchWorkers(workers = {}) {
		for (let worker in workers) {
			_workers[worker] = workers[worker];
		}
	}

	function initDispatchCrews(crews = []) {
		_crews.length = 0;

		_crews.push(...crews);
	}

	function getDispatchCrews() {
		return _crews;
	}

	function getWorkersNameByIds(workers = []) {
		let names = getRequestWorkersById(workers)
			.map(worker => worker.name)
			.join(', ');

		return names;
	}

	function getRequestWorkersById(helperIds = []) {
		let result = [];

		helperIds.forEach(helperId => {
			let worker = getWorkerById(helperId);

			if (worker && helperId) {
				result.push(worker);
			}
		});

		return result;
	}

	function getWorkerById(uid) {
		let result = {
			uid,
			name: '',
		};

		let helper = _.get(_workers, `helper[${uid}]`, {});
		let driver = _.get(_workers, `driver[${uid}]`, {});

		if (!_.isEmpty(helper)) {
			let worker = helper;
			let firstName = worker.field_user_first_name || '';
			let lastName = worker.field_user_last_name || '';
			result.name = firstName + ' ' + lastName;
		} else if (!_.isEmpty(driver)) {
			let worker = driver;
			let firstName = worker.field_user_first_name || '';
			let lastName = worker.field_user_last_name || '';
			result.name = firstName + ' ' + lastName;
		} else {
			let foreman = _workers.foreman.find((worker) => worker.uid == uid);

			if (foreman) {
				result.name = foreman.name;
				result.isForeman = true;
			}
		}

		return result;
	}
}
