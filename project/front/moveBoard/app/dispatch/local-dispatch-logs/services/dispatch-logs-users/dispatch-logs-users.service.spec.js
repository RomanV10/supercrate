describe('dispatch logs users service', () => {
	let dispatchLogsUsers;

	beforeEach(inject((_dispatchLogsUsersService_) => {
		dispatchLogsUsers = _dispatchLogsUsersService_;
	}));

	describe('when init', () => {
		it('initDispatchWorkers should be defined', () => {
			expect(dispatchLogsUsers.initDispatchWorkers).toBeDefined();
		});

		it('initDispatchCrews should be defined', () => {
			expect(dispatchLogsUsers.initDispatchCrews).toBeDefined();
		});

		it('getDispatchCrews should be defined', () => {
			expect(dispatchLogsUsers.getDispatchCrews).toBeDefined();
		});

		it('getWorkersNameByIds should be defined', () => {
			expect(dispatchLogsUsers.getWorkersNameByIds).toBeDefined();
		});

		it('getRequestWorkersById should be defined', () => {
			expect(dispatchLogsUsers.getRequestWorkersById).toBeDefined();
		});

		it('getWorkerById should be defined', () => {
			expect(dispatchLogsUsers.getWorkerById).toBeDefined();
		});
	});

	describe('initDispatchWorkers', () => {
		it('should init workers object', () => {
			//given
			let expectedResult = {foremans: [], helpers: {}, drivers: {}};

			//when
			dispatchLogsUsers.initDispatchWorkers(expectedResult);

			//then
			expect(dispatchLogsUsers._workers).toEqual(expectedResult);
		});
	});

	describe('initDispatchCrews', () => {
		it('should init dispatch crew', () => {
			//given
			let expectedResult = [{teamId: 1}];

			//when
			dispatchLogsUsers.initDispatchCrews(expectedResult);

			//then
			expect(dispatchLogsUsers._crews).toEqual(expectedResult);
		});

		it('should be empty array by default', () => {
			//given
			let expectedResult = [];

			//when
			dispatchLogsUsers.initDispatchCrews();

			//then
			expect(dispatchLogsUsers._crews).toEqual(expectedResult);
		});
	});

	describe('getDispatchCrews', () => {
		it('should return dispatch crews', () => {
			//given
			let expectedResult = [{teamId: 1}, {teamId: 2}];
			let result;

			//when
			dispatchLogsUsers.initDispatchCrews(expectedResult);
			result = dispatchLogsUsers.getDispatchCrews();

			//then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('need init workers', () => {
		beforeEach(() => {
			let workers = testHelper.loadJsonFile('dispatch-logs-dispatcher__workers-team.mock');
			dispatchLogsUsers.initDispatchWorkers(workers);
		});

		describe('getWorkersNameByIds', () => {
			it('should return one name for one id', () => {
				//given
				let expectedResult = 'Test Helper2';
				let workerIds = [5902];
				let result;

				//when
				result = dispatchLogsUsers.getWorkersNameByIds(workerIds);

				//then
				expect(result).toEqual(expectedResult);
			});

			it('should return two names using separator ", " ', () => {
				//given
				let expectedResult = 'Test Helper2, Test Helper1';
				let workerIds = [5902, 5901];
				let result;

				//when
				result = dispatchLogsUsers.getWorkersNameByIds(workerIds);

				//then
				expect(result).toEqual(expectedResult);
			});

			it('should return empty string when no ids', () => {
				//given
				let expectedResult = '';
				let result;

				//when
				result = dispatchLogsUsers.getWorkersNameByIds();

				//then
				expect(result).toEqual(expectedResult);
			});
		});

		describe('getRequestWorkersById', () => {
			it('should return one worker for one id', () => {
				//given
				let expectedResult = [ { uid : 5902, name : 'Test Helper2' } ];
				let workerIds = [5902];
				let result;

				//when
				result = dispatchLogsUsers.getRequestWorkersById(workerIds);

				//then
				expect(result).toEqual(expectedResult);
			});
		});

		describe('getWorkerById', () => {
			it('should return worker by id', () => {
				//given
				let expectedResult = { uid : 5902, name : 'Test Helper2' };
				let workerId = 5902;
				let result;

				//when
				result = dispatchLogsUsers.getWorkerById(workerId);

				//then
				expect(result).toEqual(expectedResult);
			});
		});
	});
});