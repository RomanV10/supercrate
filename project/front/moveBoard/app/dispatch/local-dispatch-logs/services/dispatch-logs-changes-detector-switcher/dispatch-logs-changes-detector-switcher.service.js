'use strict';

import {DISPATCH_REQUEST_PATH} from '../../../constants/dispatch-request-path.constants';

angular
	.module('app.dispatch')
	.factory('dispatchLogsChangesDetectorSwitcherService', dispatchLogsChangesDetectorSwitcherService);

/* @ngInject */
function dispatchLogsChangesDetectorSwitcherService(logsViewTextIds, dispatchLogsUsersService) {
	let service = {
		assignSwitcherLog,
		unassignSwitcherLog,
		isReassignSwitcher,
	};

	return service;

	function assignSwitcherLog(initial, actual) {
		let logs;

		if (isReassignSwitcher(initial, actual)) {
			logs = writeReassignSwitcherLog(initial, actual);
		} else if (isRemoveSwitcherLog(initial, actual)) {
			logs = writeRemoveSwitcherLog(initial, actual);
		} else {
			logs = writeChooseSwitcherLog(initial, actual);
		}

		return logs;
	}

	function isReassignSwitcher(initial, actual) {
		let switchers = getDiffSwitcher(initial, actual);

		return !_.isEmpty(switchers.old) && !_.isEmpty(switchers.new);
	}

	function writeReassignSwitcherLog(initial, actual) {
		let logs = [];
		let switchers = getDiffSwitcher(initial, actual);

		_.each(switchers.new, (switcherId, workerId) => {
			let currentWorker = dispatchLogsUsersService.getWorkerById(workerId);
			let newSwicher = dispatchLogsUsersService.getWorkerById(switcherId);

			if (isChangeSwitcher(switcherId, workerId, switchers.old)) {
				let oldSwicher = dispatchLogsUsersService.getWorkerById(switchers.old[workerId]);

				let textId = currentWorker.isForeman
					? logsViewTextIds.dispatchCrew.action.changeForemanSwicher
					: logsViewTextIds.dispatchCrew.action.changeHelperSwicher;

				logs.push({
					textId,
					for: currentWorker.name,
					from: oldSwicher.name,
					to: newSwicher.name,
				});
			}

			if (isAddNewSwitcher(workerId, switchers.old)) {
				let textId = currentWorker.isForeman
					? logsViewTextIds.dispatchCrew.action.addSwicherToForeman
					: logsViewTextIds.dispatchCrew.action.addSwicherToHelper;

				logs.push({
					textId,
					for: currentWorker.name,
					to: newSwicher.name,
				});
			}
		});

		let removeSwithcersLog = writeRemoveSwitcherLog(initial, actual);
		logs.push(...removeSwithcersLog);

		return logs;
	}

	function isRemoveSwitcherLog(initial, actual) {
		let switchers = getDiffSwitcher(initial, actual);

		return !_.isEmpty(switchers.old) && _.isEmpty(switchers.new);
	}

	function writeRemoveSwitcherLog(initial, actual) {
		let logs = [];
		let switchers = getDiffSwitcher(initial, actual);

		_.each(switchers.old, (switcherId, workerId) => {
			let currentWorker = dispatchLogsUsersService.getWorkerById(workerId);

			if (isRemoveSwitcher(workerId, switchers.new)) {
				let oldSwitcher = dispatchLogsUsersService.getWorkerById(switcherId);
				let textId = currentWorker.isForeman
					? logsViewTextIds.dispatchCrew.action.removeForemanSwitcher
					: logsViewTextIds.dispatchCrew.action.removeHelperSwitcher;

				logs.push({
					textId,
					for: currentWorker.name,
					to: oldSwitcher.name,
				});
			}
		});

		return logs;
	}

	function isAddNewSwitcher(worker, oldSwitchers) {
		let isNotExistOldSwitcher = !oldSwitchers[worker];
		return isNotExistOldSwitcher;
	}

	function isChangeSwitcher(switcher, worker, oldSwichers) {
		let isExistOldSwicher = oldSwichers[worker];
		let isChangeOldSwicher = isExistOldSwicher && oldSwichers[worker] != switcher;

		return isChangeOldSwicher;
	}

	function isRemoveSwitcher(worker, newSwitchers) {
		let isWorkerNotExist = !newSwitchers[worker];
		return isWorkerNotExist;
	}

	function writeChooseSwitcherLog(initial, actual) {
		let logs = [];

		if (isChooseSwitcher(initial, actual)) {
			let switchers = getDiffSwitcher(initial, actual);

			_.each(switchers.new, (switcher, worker) => {
				let workerObj = dispatchLogsUsersService.getWorkerById(worker);
				let switcherObj = dispatchLogsUsersService.getWorkerById(switcher);

				if (workerObj && switcherObj) {
					let textId = workerObj.isForeman
						? logsViewTextIds.dispatchCrew.action.addSwicherToForeman
						: logsViewTextIds.dispatchCrew.action.addSwicherToHelper;

					logs.push({textId, from: workerObj.name, to: switcherObj.name});
				}
			});
		}

		return logs;
	}

	function isChooseSwitcher(initial, actual) {
		let switchers = getDiffSwitcher(initial, actual);

		return _.isEmpty(switchers.old) && !_.isEmpty(switchers.new);
	}

	function getDiffSwitcher(initial, actual) {
		let result = {
			old: _.get(initial, DISPATCH_REQUEST_PATH.CREWS_SWITCHERS, {}),
			new: _.get(actual, DISPATCH_REQUEST_PATH.CREWS_SWITCHERS, {}),
		};

		if (result.old.switcher) {
			delete result.old.switcher;
		}

		if (result.new.switcher) {
			delete result.new.switcher;
		}

		return result;
	}

	function unassignSwitcherLog(initial) {
		let logs = [];

		if (isAssignedSwitchers(initial)) {
			let switchers = getDiffSwitcher(initial).old;

			_.each(switchers, (switcherId, workerId) => {
				let switcher = dispatchLogsUsersService.getWorkerById(switcherId);
				let worker = dispatchLogsUsersService.getWorkerById(workerId);
				let textId = worker.isForeman
					? logsViewTextIds.dispatchCrew.action.unassignForemanSwitcher
					: logsViewTextIds.dispatchCrew.action.unassignHelperSwitcher;

				logs.push({
					textId,
					for: worker.name,
					to: switcher.name,
				});
			});
		}

		return logs;
	}

	function isAssignedSwitchers(initial) {
		let switchers = getDiffSwitcher(initial);
		let isNotEmptySwitchers = !_.isEmpty(switchers.old);

		return isNotEmptySwitchers;
	}
}
