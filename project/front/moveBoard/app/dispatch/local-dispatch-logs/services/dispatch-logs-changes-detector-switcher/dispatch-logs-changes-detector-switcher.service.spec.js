describe('dispatch logs changes detector switcher service', () => {
	let dispatchChangesDetectorSwitcher;
	let dispatchLogsUsers;

	beforeEach(inject((_dispatchLogsChangesDetectorSwitcherService_, _dispatchLogsUsersService_) => {
		dispatchChangesDetectorSwitcher = _dispatchLogsChangesDetectorSwitcherService_;
		dispatchLogsUsers = _dispatchLogsUsersService_;

		let workers = testHelper.loadJsonFile('dispatch-logs-dispatcher__workers-team.mock');
		dispatchLogsUsers.initDispatchWorkers(workers);
	}));

	describe('when init', () => {
		it('should be define assignSwitcherLog', () => {
			expect(dispatchChangesDetectorSwitcher.assignSwitcherLog).toBeDefined();
		});
	});

	describe('assignSwitchers', () => {
		describe('first time assign', () => {
			let request;
			let updatedRequest;

			beforeEach(() => {
				request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request.mock');
				updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-with-switcher.mock');
			});

			it('should write assign swithcer for foreman', () => {
				//given
				let expectedResult = {textId: 5, from: 'FlatRate Foreman', to: 'Foreman Flow1'};
				let result;

				//when
				result = dispatchChangesDetectorSwitcher.assignSwitcherLog(request, updatedRequest);

				//then
				expect(result[0]).toEqual(expectedResult);
			});

			it('should write assign swithcer for helper', () => {
				//given
				let expectedResult = {textId: 6, from: 'helper test2', to: 'Test Helper1' };
				let result;

				//when
				result = dispatchChangesDetectorSwitcher.assignSwitcherLog(request, updatedRequest);

				//then
				expect(result[1]).toEqual(expectedResult);
			});
		});

		describe('reassign switcher', () => {
			let request;
			let updatedRequest;

			describe('reassign', () => {
				beforeEach(() => {
					request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-with-switcher.mock');
					updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-with-reassign-switcher.mock');
				});

				it('should write reassign swithcer for foreman', () => {
					//given
					let expectedResult = {textId: 15, for: 'FlatRate Foreman', from: 'Foreman Flow1', to: 'ForemanExclude Test'};
					let result;

					//when
					result = dispatchChangesDetectorSwitcher.assignSwitcherLog(request, updatedRequest);

					//then
					expect(result[0]).toEqual(expectedResult);
				});

				it('should write reassign swithcer for helper', () => {
					//given
					let expectedResult = {textId: 16, for: 'helper test2', from: 'Test Helper1', to: 'Test Helper3' };
					let result;

					//when
					result = dispatchChangesDetectorSwitcher.assignSwitcherLog(request, updatedRequest);

					//then
					expect(result[1]).toEqual(expectedResult);
				});

				it('should write log remove swithcer for helper', () => {
					//given
					let expectedResult = {textId: 18, for: 'helper test2', to: 'Test Helper1' };
					delete updatedRequest.request_data.value.switchers['5988'];
					let result;

					//when
					result = dispatchChangesDetectorSwitcher.assignSwitcherLog(request, updatedRequest);

					//then
					expect(result[1]).toEqual(expectedResult);
				});

				it('should write log about add swithcer for helper', () => {
					//given
					let expectedResult = {textId: 6, for: 'helper test2', to: 'Test Helper3'};
					delete request.request_data.value.switchers['5988'];

					let result;

					//when
					result = dispatchChangesDetectorSwitcher.assignSwitcherLog(request, updatedRequest);

					//then
					expect(result[1]).toEqual(expectedResult);
				});
			});

			describe('remove', () => {
				beforeEach(() => {
					request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-with-switcher.mock');
					request.request_data.value.switchers = {};
				});

				it('should write log about remove all foreman switcher', () => {
					//given
					let expectedResult = { textId : 17, for : 'FlatRate Foreman', to : 'ForemanExclude Test' };
					let result;

					//when
					result = dispatchChangesDetectorSwitcher.assignSwitcherLog(updatedRequest, request);

					//then
					expect(result[0]).toEqual(expectedResult);
				});

				it('should write log about remove all helper switcher', () => {
					//given
					let expectedResult = {textId: 18, for: 'helper test2', to: 'Test Helper3'};
					let result;

					//when
					result = dispatchChangesDetectorSwitcher.assignSwitcherLog(updatedRequest, request);

					//then
					expect(result[1]).toEqual(expectedResult);
				});
			});
		});
	});

	describe('when unassign switcher', () => {
		it('should write unassign switchers log', () => {
			//given
			let updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-with-switcher.mock');
			let expectedResult = {textId: 19, for: 'helper test2', to: 'Test Helper3'};
			updatedRequest.request_data.value.switchers = {"5988": "5903"};
			let result;

			//when
			result = dispatchChangesDetectorSwitcher.unassignSwitcherLog(updatedRequest);

			//then
			expect(result[0]).toEqual(expectedResult);
		});
	});
});