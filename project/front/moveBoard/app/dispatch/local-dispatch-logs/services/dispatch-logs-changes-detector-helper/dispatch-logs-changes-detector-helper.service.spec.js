describe('dispatch-logs-changes-detector-helper service', () => {
	let dispatchChangesDetectorHelper;
	let dispatchLogsUsers;
	let request;
	let updatedRequest;

	beforeEach(inject((_dispatchLogsChangesDetectorHelperService_, _dispatchLogsUsersService_) => {
		dispatchChangesDetectorHelper = _dispatchLogsChangesDetectorHelperService_;
		dispatchLogsUsers = _dispatchLogsUsersService_;

		request = testHelper.loadJsonFile('dispatch-logs-changes-detector-foreman__initial-request.mock');
		updatedRequest = testHelper.loadJsonFile('dispatch-logs-changes-detector-foreman__actual-request.mock');

		let workers = testHelper.loadJsonFile('dispatch-logs-dispatcher__workers-team.mock');
		dispatchLogsUsers.initDispatchWorkers(workers);
	}));

	describe('when init', () => {
		it('should be define assignHelperLog', () => {
			expect(dispatchChangesDetectorHelper.assignHelperLog).toBeDefined();
		});

		it('should be define changeHelperLog', () => {
			expect(dispatchChangesDetectorHelper.changeHelperLog).toBeDefined();
		});

		it('should be define chooseHelperLog', () => {
			expect(dispatchChangesDetectorHelper.chooseHelperLog).toBeDefined();
		});

		it('should be define unassignHelperLog', () => {
			expect(dispatchChangesDetectorHelper.unassignHelperLog).toBeDefined();
		});

		it('should be define isReassignHelper', () => {
			expect(dispatchChangesDetectorHelper.isReassignHelper).toBeDefined();
		});

		it('should be define isRemoveHelper', () => {
			expect(dispatchChangesDetectorHelper.isRemoveHelper).toBeDefined();
		});
	});

	describe('assignHelperLog', () => {
		it('when choose helper should return log', () => {
			//given
			let expectedResult = [{textId: 3, to: 'helper test1'}];
			spyOn(dispatchChangesDetectorHelper, 'isReassignHelper').and.returnValue(false);
			spyOn(dispatchChangesDetectorHelper, 'chooseHelperLog').and.returnValue(expectedResult);
			let result;

			//when
			result = dispatchChangesDetectorHelper.assignHelperLog(request, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should detect change helper', () => {
			//given
			let expectedResult = [{textId: 10, from: 'helper test1', to: 'Test Foreman'}];
			spyOn(dispatchChangesDetectorHelper, 'isReassignHelper').and.returnValue(true);
			spyOn(dispatchChangesDetectorHelper, 'changeHelperLog').and.returnValue(expectedResult);
			let result;

			//when
			result = dispatchChangesDetectorHelper.assignHelperLog(request, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should return empty array when no changes', () => {
			//given
			let expectedResult = [];
			let result;

			//when
			result = dispatchChangesDetectorHelper.assignHelperLog(request, request);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should call removeHelpersLog when isRemoveHelpers', () => {
			// given
			spyOn(dispatchChangesDetectorHelper, 'isRemoveHelper').and.returnValue(true);
			spyOn(dispatchChangesDetectorHelper, 'isReassignHelper').and.returnValue(false);
			spyOn(dispatchChangesDetectorHelper, 'removeHelpersLog').and.callFake(() => {});

			// when
			dispatchChangesDetectorHelper.assignHelperLog();

			// then
			expect(dispatchChangesDetectorHelper.removeHelpersLog).toHaveBeenCalled();
		});
	});

	describe('changeHelperLog', () => {
		it('should detect change helper', () => {
			//given
			let expectedResult = [{textId: 10, from: 'helper test1', to: 'Test Foreman'}];
			request = angular.copy(updatedRequest);
			let foremanId = '5906';
			updatedRequest.request_data.value.crews.baseCrew.helpers = [foremanId];
			let result;

			//when
			result = dispatchChangesDetectorHelper.changeHelperLog(request, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should return empty array when no changes', () => {
			//given
			let expectedResult = [];
			let result;

			//when
			result = dispatchChangesDetectorHelper.changeHelperLog(request, request);

			//then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('chooseHelperLog', () => {
		it('should detect choose helper', () => {
			//given
			let expectedResult = [{textId: 3, to: 'helper test1'}];
			let result;

			//when
			result = dispatchChangesDetectorHelper.chooseHelperLog(request, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should detect choose foreman as helper', () => {
			//given
			let expectedResult = [{textId: 3, to: 'Test Foreman'}];
			let result;
			let foremanId = '5906';
			updatedRequest.request_data.value.crews.baseCrew.helpers = [foremanId];

			//when
			result = dispatchChangesDetectorHelper.chooseHelperLog(request, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('unassignHelperLog', () => {
		it('should write unassign helper log', () => {
			//given
			let expectedResult = [{ textId : 13, from : 'helper test1' }];
			let result;

			//when
			result = dispatchChangesDetectorHelper.unassignHelperLog(updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('flat rate', () => {
		let request;
		let updatedRequest;
		let PICKUP_CREW = 'request_data.value.crews.pickedUpCrew.helpers';
		let DELIVERY_CEW = 'request_data.value.crews.deliveryCrew.helpers';

		beforeEach(() => {
			request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request_flat-rate_initial.mock');
			updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request_flat-rate_actual.mock');
		});

		describe('pickup', () => {
			describe('assignHelperLog', () => {
				it('when choose helper should return log', () => {
					//given
					let expectedResult = [{textId: 3, to: 'helper test1'}];
					spyOn(dispatchChangesDetectorHelper, 'isReassignHelper').and.returnValue(false);
					spyOn(dispatchChangesDetectorHelper, 'chooseHelperLog').and.returnValue(expectedResult);
					let result;

					//when
					result = dispatchChangesDetectorHelper.assignHelperLog(request, updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should detect change helper', () => {
					//given
					let expectedResult = [{textId: 10, from: 'helper test1', to: 'Test Foreman'}];
					spyOn(dispatchChangesDetectorHelper, 'isReassignHelper').and.returnValue(true);
					spyOn(dispatchChangesDetectorHelper, 'changeHelperLog').and.returnValue(expectedResult);
					let result;

					//when
					result = dispatchChangesDetectorHelper.assignHelperLog(request, updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when no changes', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorHelper.assignHelperLog(request, request, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('changeHelperLog', () => {
				beforeEach(() => {
					request = angular.copy(updatedRequest);
					updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request_flat-rate-changed_actual.mock');
				});

				it('should detect change helper', () => {
					//given
					let expectedResult = [ { textId : 10, from : 'helper test1', to : 'Test Helper2, Test Helper1, helper test2, Test Helper1' } ];
					let result;

					//when
					result = dispatchChangesDetectorHelper.changeHelperLog(request, updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when no changes', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorHelper.changeHelperLog(request, request, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('chooseHelperLog', () => {
				it('should detect choose helper', () => {
					//given
					let expectedResult = [{textId: 3, to: 'helper test1'}];
					let result;

					//when
					result = dispatchChangesDetectorHelper.chooseHelperLog(request, updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('unassignHelperLog', () => {
				it('should write unassign helper log', () => {
					//given
					let expectedResult = [{ textId : 13, from : 'helper test1' }];
					let result;

					//when
					result = dispatchChangesDetectorHelper.unassignHelperLog(updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});
		});

		describe('delivery', () => {
			describe('assignHelperLog', () => {
				it('when choose helper should return log', () => {
					//given
					let expectedResult = [{textId: 3, to: 'helper test1'}];
					spyOn(dispatchChangesDetectorHelper, 'isReassignHelper').and.returnValue(false);
					spyOn(dispatchChangesDetectorHelper, 'chooseHelperLog').and.returnValue(expectedResult);
					let result;

					//when
					result = dispatchChangesDetectorHelper.assignHelperLog(request, updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should detect change helper', () => {
					//given
					let expectedResult = [{textId: 10, from: 'helper test1', to: 'Test Foreman'}];
					spyOn(dispatchChangesDetectorHelper, 'isReassignHelper').and.returnValue(true);
					spyOn(dispatchChangesDetectorHelper, 'changeHelperLog').and.returnValue(expectedResult);
					let result;

					//when
					result = dispatchChangesDetectorHelper.assignHelperLog(request, updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when no changes', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorHelper.assignHelperLog(request, request, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('changeHelperLog', () => {
				beforeEach(() => {
					request = angular.copy(updatedRequest);
					updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request_flat-rate-changed_actual.mock');
				});

				it('should detect change helper', () => {
					//given
					let expectedResult = [ { textId : 10, from : 'helper test2', to : 'Test Helper1' } ];
					let result;

					//when
					result = dispatchChangesDetectorHelper.changeHelperLog(request, updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when no changes', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorHelper.changeHelperLog(request, request, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('chooseHelperLog', () => {
				it('should detect choose helper', () => {
					//given
					let expectedResult = [ { textId : 3, to : 'helper test2' } ];
					let result;

					//when
					result = dispatchChangesDetectorHelper.chooseHelperLog(request, updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('unassignHelperLog', () => {
				it('should write unassign helper log', () => {
					//given
					let expectedResult = [ { textId : 13, from : 'helper test2' } ];
					let result;

					//when
					result = dispatchChangesDetectorHelper.unassignHelperLog(updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});
		});
	});

	describe('isRemoveHelper', () => {
		it('should return true when old helpers not empty and new helpers empty', () => {
			// given
			let oldHelpers = {path: ['0']};
			let newHelpers = {path: []};
			let path = 'path';
			let result;
			let exptectedResult = true;

			// when
			result = dispatchChangesDetectorHelper.isRemoveHelper(oldHelpers, newHelpers, path);

			// then
			expect(result).toEqual(exptectedResult);
		});

		it('should return false when old helpers empty and new helpers not empty', () => {
			// given
			let oldHelpers = {path: []};
			let newHelpers = {path: ['0']};
			let path = 'path';
			let result;
			let exptectedResult = false;

			// when
			result = dispatchChangesDetectorHelper.isRemoveHelper(oldHelpers, newHelpers, path);

			// then
			expect(result).toEqual(exptectedResult);
		});
	});

	describe('removeHelpersLog', () => {
		it('should return empty log when no find workers name', () => {
			// given
			spyOn(dispatchLogsUsers, 'getRequestWorkersById').and.returnValue([]);
			let result;
			let expectedResult = [];

			// when
			result = dispatchChangesDetectorHelper.removeHelpersLog();

			// then
			expect(result).toEqual(expectedResult);
		});

		it('should return not empty log when find workers name', () => {
			// given
			spyOn(dispatchLogsUsers, 'getRequestWorkersById').and.returnValue([{name: 'John'}]);
			let result;
			let expectedResult = [ { textId : 28, from : 'John' } ];

			// when
			result = dispatchChangesDetectorHelper.removeHelpersLog();

			// then
			expect(result).toEqual(expectedResult);
		});
	});
});