'use strict';

import {DISPATCH_REQUEST_PATH} from '../../../constants/dispatch-request-path.constants';

angular
	.module('app.dispatch')
	.factory('dispatchLogsChangesDetectorHelperService', dispatchLogsChangesDetectorHelperService);

/* @ngInject */
function dispatchLogsChangesDetectorHelperService(logsViewTextIds, dispatchLogsUsersService) {
	let service = {
		assignHelperLog,
		changeHelperLog,
		unassignHelperLog,
		chooseHelperLog,
		isReassignHelper,
		isRemoveHelper,
		removeHelpersLog,
	};

	return service;

	function assignHelperLog(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let logs;

		if (service.isReassignHelper(initial, actual, path)) {
			logs = service.changeHelperLog(initial, actual, path);
		} else if(service.isRemoveHelper(initial, actual, path)) {
			logs = service.removeHelpersLog(initial, path);
		} else {
			logs = service.chooseHelperLog(initial, actual, path);
		}

		return logs;
	}

	function changeHelperLog(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let logs = [];

		if (isChangeHelpers(initial, actual, path)) {
			let oldHelpers = _.get(initial, path);
			let newHelpers = _.get(actual, path);

			let oldHelperNames = dispatchLogsUsersService.getWorkersNameByIds(oldHelpers);
			let newHelperNames = dispatchLogsUsersService.getWorkersNameByIds(newHelpers);

			if (oldHelperNames && newHelperNames) {
				logs.push({
					textId: logsViewTextIds.dispatchCrew.action.changeHelper,
					from: oldHelperNames, to:
					newHelperNames
				});
			}
		}

		return logs;
	}

	function isChangeHelpers(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let oldHelpers = getWorkers(initial, path);
		let newHelpers = getWorkers(actual, path);

		return !_.isEqual(oldHelpers, newHelpers);
	}

	function unassignHelperLog(initial, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let logs = [];

		if (isAssignedHelpers(initial, path)) {
			let helperIds = _.get(initial, path, []);

			dispatchLogsUsersService.getRequestWorkersById(helperIds)
				.forEach((helper) => {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.unassignHelper,
						from: helper.name
					});
				});
		}

		return logs;
	}

	function isAssignedHelpers(initial, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let oldHelpers = _.get(initial, path, []);
		let isHelpersWasSelected = !_.isEmpty(oldHelpers);
		return isHelpersWasSelected;
	}

	function chooseHelperLog(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let logs = [];

		if (isChooseHelper(initial, actual, path)) {
			let helperIds = getWorkers(actual, path);

			dispatchLogsUsersService.getRequestWorkersById(helperIds)
				.forEach((helper) => {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.chooseHelper,
						to: helper.name
					});
				});
		}

		return logs;
	}

	function isChooseHelper(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let oldHelpers = getWorkers(initial, path);
		let newHelpers = getWorkers(actual, path);
		let isHelpersWasNotSelected = _.isEmpty(oldHelpers);
		let isChooseNewHelper = !_.isEmpty(newHelpers);
		return isHelpersWasNotSelected && isChooseNewHelper;
	}

	function isReassignHelper(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let oldHelpers = getWorkers(initial, path);
		let newHelpers = getWorkers(actual, path);
		let isAssignedOldHelpers = !_.isEmpty(oldHelpers);
		let isAssignedNewHelpers = !_.isEmpty(newHelpers);

		return isAssignedOldHelpers && isAssignedNewHelpers;
	}

	function removeEmptyEmptyWorker(workersId = []) {
		let result = [];

		workersId.forEach(item => {
			if (!_.isEmpty(item)) {
				result.push(item);
			}
		});

		return result;
	}

	function isRemoveHelper(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let oldHelpers = getWorkers(initial, path);
		let newHelpers = getWorkers(actual, path);
		let isAssignedOldHelpers = !_.isEmpty(oldHelpers);
		let isNotAssignedNewHelpers = _.isEmpty(newHelpers);

		return isAssignedOldHelpers && isNotAssignedNewHelpers;
	}

	function getWorkers(request, path) {
		let result = _.get(request, path, []);
		result = removeEmptyEmptyWorker(result);

		return result;
	}

	function removeHelpersLog(initial, path = DISPATCH_REQUEST_PATH.CREWS_HELPERS) {
		let oldHelpers = getWorkers(initial, path);
		let logs = [];

		dispatchLogsUsersService.getRequestWorkersById(oldHelpers)
			.forEach((helper) => {
				logs.push({
					textId: logsViewTextIds.dispatchCrew.action.removeHelper,
					from: helper.name
				});
			});

		return logs;
	}
}
