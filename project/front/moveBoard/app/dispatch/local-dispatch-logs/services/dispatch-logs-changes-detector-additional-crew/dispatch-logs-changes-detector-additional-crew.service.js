'use strict';

import {DISPATCH_REQUEST_PATH} from '../../../constants/dispatch-request-path.constants';

angular
	.module('app.dispatch')
	.factory('dispatchLogsChangesDetectorAdditionalCrewService', dispatchLogsChangesDetectorAdditionalCrewService);

/* @ngInject */
function dispatchLogsChangesDetectorAdditionalCrewService(logsViewTextIds, dispatchLogsUsersService) {
	const DEFAULT_CREW_NAMBER = 2;

	let service = {
		assignAdditionalCrewLog,
		unassignAdditionalCrewLog,
	};

	return service;

	function assignAdditionalCrewLog(initial, actual) {
		let logs;

		if (isReassignAdditionalCrews(initial, actual)) {
			logs = writeReassignAdditionalCrewLog(initial, actual);
		} else if (isRemoveAdditionalCrewsLog(initial, actual)) {
			logs = writeRemoveAdditionalCrewLog(initial, actual);
		} else {
			logs = writeAssignAdditionalCrewLog(initial, actual);
		}

		return logs;
	}

	function isReassignAdditionalCrews(initial, actual) {
		let crew = getAdditionalCrewsDiff(initial, actual);
		let isNotEmptyOldCrew = !_.isEmpty(crew.old);
		let isNotEmptyNewCrew = !_.isEmpty(crew.new);
		let isNotEqualCrew = !_.isEqual(crew.old, crew.new);

		return isNotEmptyOldCrew && isNotEmptyNewCrew && isNotEqualCrew;
	}

	function writeReassignAdditionalCrewLog(initial, actual) {
		let logs = [];
		let crews = getAdditionalCrewsDiff(initial, actual);

		crews.new.forEach((crew, index) => {
			if (isAddAdiitionalCrew(crew, crews.old)) {
				let helperNames = dispatchLogsUsersService.getWorkersNameByIds(crew.helpers);

				if (!_.isEmpty(helperNames)) {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.addAdditionalCrew,
						for: getCurrentCrewNumber(index),
						to: helperNames,
					});
				}

				if (!_.isEmpty(crew.trucks)) {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.addAdditionalTruck,
						for: getCurrentCrewNumber(index),
						to: crew.trucks,
					});
				}
			} else if (isChnageAdditionalCrew(crew, crews.old)) {
				let oldCrew = crews.old.find(old => old.id == crew.id);
				let isChangeHelpers = !_.isEqual(oldCrew.helpers, crew.helpers);

				if (isChangeHelpers) {
					let oldCrewNames = dispatchLogsUsersService.getWorkersNameByIds(oldCrew.helpers);
					let newCrewNames = dispatchLogsUsersService.getWorkersNameByIds(crew.helpers);

					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.changeAdditionalCrew,
						for: getCurrentCrewNumber(index),
						from: oldCrewNames,
						to: newCrewNames,
					});
				}

				let isChangeTrucks = oldCrew.trucks != crew.trucks;

				if (isChangeTrucks) {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.changeAdditionalTruck,
						for: getCurrentCrewNumber(index),
						from: oldCrew.trucks,
						to: crew.trucks,
					});
				}
			}
		});

		let removeAdditionslCrewLogs = writeRemoveAdditionalCrewLog(initial, actual);
		logs.push(...removeAdditionslCrewLogs);

		return logs;
	}

	function isAddAdiitionalCrew(crew, oldCrews) {
		let isAddCrew = !oldCrews.find(oldCrew => oldCrew.id == crew.id);
		return isAddCrew;
	}

	function isChnageAdditionalCrew(crew, oldCrews) {
		let oldCrew = oldCrews.find(old => old.id == crew.id);
		let isChangeCrew = oldCrew && !_.isEqual(oldCrew, crew);

		return isChangeCrew;
	}

	function isRemoveAdditionalCrewsLog(initial, actual) {
		let crews = getAdditionalCrewsDiff(initial, actual);

		return !_.isEmpty(crews.old) && _.isEmpty(crews.new);
	}

	function writeRemoveAdditionalCrewLog(initial, actual) {
		let logs = [];
		let crews = getAdditionalCrewsDiff(initial, actual);

		crews.old.forEach((crew, index) => {
			if (isRemovedCrew(crew, crews.new)) {
				let helperNames = dispatchLogsUsersService.getWorkersNameByIds(crew.helpers);

				if (!_.isEmpty(helperNames)) {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.removeAdditionalCrew,
						for: getCurrentCrewNumber(index),
						from: helperNames,
					});
				}

				if (!_.isEmpty(crew.trucks)) {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.removeAdditionalTruck,
						for: getCurrentCrewNumber(index),
						from: crew.trucks,
					});
				}
			}
		});

		return logs;
	}

	function isRemovedCrew(oldCrew, newCrews) {
		let isNotFindOldCrew = !newCrews.find(crew => crew.id == oldCrew.id);
		return isNotFindOldCrew;
	}

	function getAdditionalCrewsDiff(initial, actual) {
		let result = {
			old: _.get(initial, DISPATCH_REQUEST_PATH.CREWS_ADDITIONAL, []),
			new: _.get(actual, DISPATCH_REQUEST_PATH.CREWS_ADDITIONAL, []),
		};

		return result;
	}

	function writeAssignAdditionalCrewLog(initial, actual) {
		let logs = [];

		if (isAssignAdditionalCrew(initial, actual)) {
			let additionalCews = getAdditionalCrewsDiff(initial, actual);

			additionalCews.new.forEach((crew, index) => {
				let crewNames = dispatchLogsUsersService.getWorkersNameByIds(crew.helpers);

				if (!_.isEmpty(crewNames)) {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.addAdditionalCrew,
						for: getCurrentCrewNumber(index),
						to: crewNames,
					});
				}

				if (!_.isEmpty(crew.trucks)) {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.addAdditionalTruck,
						for: getCurrentCrewNumber(index),
						to: crew.trucks,
					});
				}
			});
		}

		return logs;
	}

	function isAssignAdditionalCrew(initial, actual) {
		let crew = getAdditionalCrewsDiff(initial, actual);
		let isEmptyOldCrew = _.isEmpty(crew.old);
		let isNotEmptyNewCrew = !_.isEmpty(crew.new);

		return isEmptyOldCrew && isNotEmptyNewCrew;
	}

	function getCurrentCrewNumber(index) {
		return index + DEFAULT_CREW_NAMBER;
	}

	function unassignAdditionalCrewLog(initial) {
		let logs = [];

		if (isAssignedAdditionalCrew(initial)) {
			let additionalCrew = getAdditionalCrewsDiff(initial);

			additionalCrew.old.forEach((crew, index) => {
				let crewNames = dispatchLogsUsersService.getWorkersNameByIds(crew.helpers);

				if (!_.isEmpty(crewNames)) {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.unassignAdditionalCrew,
						for: getCurrentCrewNumber(index),
						from: crewNames,
					});
				}

				if (!_.isEmpty(crew.trucks)) {
					logs.push({
						textId: logsViewTextIds.dispatchCrew.action.unassignAdditionalTruck,
						for: getCurrentCrewNumber(index),
						from: crew.trucks,
					});
				}
			});
		}

		return logs;
	}

	function isAssignedAdditionalCrew(initial) {
		let additonalCrew = getAdditionalCrewsDiff(initial);
		let isNotEmptyAdditionalCrews = !_.isEmpty(additonalCrew.old);

		return isNotEmptyAdditionalCrews;
	}
}
