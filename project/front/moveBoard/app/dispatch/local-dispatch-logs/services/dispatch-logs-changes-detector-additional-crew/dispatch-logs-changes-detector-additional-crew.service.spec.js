describe('dispatch logs changes detector switcher service', () => {
	let dispatchChangesDetectorAdditionalCrew;
	let dispatchLogsUsers;
	let request;
	let updatedRequest;

	beforeEach(inject((_dispatchLogsChangesDetectorAdditionalCrewService_, _dispatchLogsUsersService_) => {
		dispatchChangesDetectorAdditionalCrew = _dispatchLogsChangesDetectorAdditionalCrewService_;
		dispatchLogsUsers = _dispatchLogsUsersService_;

		let workers = testHelper.loadJsonFile('dispatch-logs-dispatcher__workers-team.mock');
		dispatchLogsUsers.initDispatchWorkers(workers);
	}));

	describe('when init', () => {
		it('should be define assignAdditionalCrewLog', () => {
			expect(dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog).toBeDefined();
		});
	});

	describe('assign additional crew', () => {
		describe('when choose additional crew', () => {
			beforeEach(() => {
				request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request.mock');
				request.nid = '13899';
				updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-assign-additional-crew.mock');
			});

			it('should detect add additional helper', () => {
				//given
				let expectedResult = {textId: 4, for: 2, to: 'helper test2, Test Helper1, Test Helper2'};
				let result;

				//when
				result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

				//then
				expect(result[0]).toEqual(expectedResult);
			});

			it('should detect add additional trucks', () => {
				//given
				let expectedResult = {textId: 8, for: 2, to: '3'};
				let result;

				//when
				result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

				//then
				expect(result[1]).toEqual(expectedResult);
			});
		});

		describe('when remove additional crew', () => {
			beforeEach(() => {
				request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-assign-additional-crew.mock');
				updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request.mock');
				updatedRequest.nid = '13899';
			});

			it('should detect remove additional helper', () => {
				//given
				let expectedResult = {textId: 22, for: 2, from: 'helper test2, Test Helper1, Test Helper2'};
				let result;

				//when
				result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

				//then
				expect(result[0]).toEqual(expectedResult);
			});

			it('should detect remove additional trucks', () => {
				//given
				let expectedResult = {textId: 23, for: 2, from: '3'};
				let result;

				//when
				result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

				//then
				expect(result[1]).toEqual(expectedResult);
			});
		});

		describe('when change additional crew', () => {
			beforeEach(() => {
				request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-assign-additional-crew.mock');
				request.request_data.value.crews.additionalCrews.push({
					"name": "Crew",
					"rate": 0,
					"id": 3,
					"helpers": [
						"5988",
						"5901",
						"5902"
					],
					"trucks": "3"
				});
			});

			describe('should detect remove', () => {
				beforeEach(() => {
					updatedRequest = angular.copy(request);
					updatedRequest.request_data.value.crews.additionalCrews.pop();
				});

				it('additional crew', () => {
					//given
					let expectedResult = { textId : 22, for : 3, from : 'helper test2, Test Helper1, Test Helper2' };
					let result;

					//when
					result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

					//then
					expect(result[0]).toEqual(expectedResult);
				});

				it('additional trucks', () => {
					//given
					let expectedResult = { textId : 23, for : 3, from : '3' };
					let result;

					//when
					result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

					//then
					expect(result[1]).toEqual(expectedResult);
				});
			});

			describe('should detect change', () => {
				beforeEach(() => {
					updatedRequest = angular.copy(request);
					request.request_data.value.crews.additionalCrews[0].helpers.pop();
					request.request_data.value.crews.additionalCrews[0].trucks = '2';
				});

				it('additional crew', () => {
					//given
					let expectedResult = { textId : 24, for : 2, from : 'helper test2, Test Helper1', to : 'helper test2, Test Helper1, Test Helper2' };
					let result;

					//when
					result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

					//then
					expect(result[0]).toEqual(expectedResult);
				});

				it('additional trucks', () => {
					//given
					let expectedResult = { textId : 7, for : 2, from : '2', to : '3' };
					let result;

					//when
					result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

					//then
					expect(result[1]).toEqual(expectedResult);
				});

				it('to empty helpers', () => {
					//given
					let expectedResult = { textId : 24, for : 2, from : '', to : 'helper test2, Test Helper1, Test Helper2' };
					request.request_data.value.crews.additionalCrews[0].helpers = [];
					let result;

					//when
					result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

					//then
					expect(result[0]).toEqual(expectedResult);
				});
			});

			describe('should detect assign', () => {
				beforeEach(() => {
					updatedRequest = angular.copy(request);
					updatedRequest.request_data.value.crews.additionalCrews.push({
						"name": "Crew",
						"rate": 0,
						"id": 2,
						"helpers": [
							"5988",
							"5901",
							"5902"
						],
						"trucks": "3"
					});
				});

				it('additional crew', () => {
					//given
					let expectedResult = {textId: 4, for: 4, to: 'helper test2, Test Helper1, Test Helper2'};
					let result;

					//when
					result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

					//then
					expect(result[0]).toEqual(expectedResult);
				});

				it('addtional trucks', () => {
					//given
					let expectedResult = {textId: 8, for: 4, to: '3'};
					let result;

					//when
					result = dispatchChangesDetectorAdditionalCrew.assignAdditionalCrewLog(request, updatedRequest);

					//then
					expect(result[1]).toEqual(expectedResult);
				});
			});
		});
	});

	describe('when unassign additional crew', () => {
		let request;

		beforeEach(() => {
			request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-assign-additional-crew.mock');
		});

		it('should detect unassign additional crew', () => {
			//given
			let expectedResult = {textId: 21, for: 2, from: 'helper test2, Test Helper1, Test Helper2'};
			let result;

			//when
			result = dispatchChangesDetectorAdditionalCrew.unassignAdditionalCrewLog(request);

			//then
			expect(result[0]).toEqual(expectedResult);
		});

		it('should detect unassign additional trucks', () => {
			//given
			let expectedResult = {textId: 20, for: 2, from: '3'};
			let result;

			//when
			result = dispatchChangesDetectorAdditionalCrew.unassignAdditionalCrewLog(request);

			//then
			expect(result[1]).toEqual(expectedResult);
		});
	});
});