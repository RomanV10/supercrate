describe('dispatch logs changes detector switcher service', () => {
	let dispatchChangesDetectorCrews;
	let dispatchLogsUsers;
	let request;
	let updatedRequest;

	beforeEach(inject((_dispatchLogsChangesDetectorCrewsService_, _dispatchLogsUsersService_) => {
		dispatchChangesDetectorCrews = _dispatchLogsChangesDetectorCrewsService_;
		dispatchLogsUsers = _dispatchLogsUsersService_;

		let workers = testHelper.loadJsonFile('dispatch-logs-dispatcher__workers-team.mock');
		dispatchLogsUsers.initDispatchWorkers(workers);
		let crews = testHelper.loadJsonFile('dispatch-logs-dispatcher__crews.mock').crews;
		dispatchLogsUsers.initDispatchCrews(crews);
	}));

	describe('when init', () => {
		it('should be define assignCrewLog', () => {
			expect(dispatchChangesDetectorCrews.assignCrewLog).toBeDefined();
		});

		it('should be define unassignCrewLog', () => {
			expect(dispatchChangesDetectorCrews.unassignCrewLog).toBeDefined();
		});
	});


	describe('assignCrewLog', () => {
		beforeEach(() => {
			request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request.mock');
		});

		it('should write log when choose crew', () => {
			//given
			let expectedResult = {textId: 1, to: 'FlatRate Foreman 1123'};
			let logs;

			//when
			logs = dispatchChangesDetectorCrews.assignCrewLog(request, 1);

			//then
			expect(logs[0]).toEqual(expectedResult);
		});

		it('should write log when choose crew', () => {
			//given
			let expectedResult = {textId: 25, from: 'FlatRate Foreman Test', to: 'FlatRate Foreman 1123'};
			_.set(request, 'request_data.value.crews.crew', 2);
			let logs;

			//when
			logs = dispatchChangesDetectorCrews.assignCrewLog(request, 1);

			//then
			expect(logs[0]).toEqual(expectedResult);
		});
	});

	describe('unassignCrewLog', () => {
		it('should write log when unassign team', () => {
			//given
			let expectedResult = { textId : 27, from : 'FlatRate Foreman Test' };
			_.set(request, 'request_data.value.crews.crew', 2);
			let logs;

			//when
			logs = dispatchChangesDetectorCrews.unassignCrewLog(request);

			//then
			expect(logs[0]).toEqual(expectedResult);
		});
	});
});