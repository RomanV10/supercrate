'use strict';

import {DISPATCH_REQUEST_PATH} from '../../../constants/dispatch-request-path.constants';

angular
	.module('app.dispatch')
	.factory('dispatchLogsChangesDetectorCrewsService', dispatchLogsChangesDetectorCrewsService);

/* @ngInject */
function dispatchLogsChangesDetectorCrewsService(logsViewTextIds, dispatchLogsUsersService) {
	let service = {
		assignCrewLog,
		unassignCrewLog,
	};

	return service;

	function assignCrewLog(initial, newCrewId, path = DISPATCH_REQUEST_PATH.AUTO_ASSIGN_CREW) {
		let logs = [];
		let crews = dispatchLogsUsersService.getDispatchCrews();
		let oldCrewId = _.get(initial, path);
		let oldCrew = crews.find(crew => crew.team_id == oldCrewId);
		let newCrew = crews.find(crew => crew.team_id == newCrewId);

		if (oldCrewId && newCrewId && oldCrewId != newCrewId) {
			logs.push({
				textId: logsViewTextIds.dispatchCrew.action.changeCrewTeam,
				from: oldCrew.name,
				to: newCrew.name,
			});
		} else if (newCrew && newCrewId && !oldCrewId) {
			logs.push({
				textId: logsViewTextIds.dispatchCrew.action.chooseCrewTeam,
				to: newCrew.name,
			});
		} else if(oldCrew && !newCrewId) {
			logs.push({
				textId: logsViewTextIds.dispatchCrew.action.removeCrewTeam,
				from: oldCrew.name,
			});
		}

		return logs;
	}

	function unassignCrewLog(initial, path = DISPATCH_REQUEST_PATH.AUTO_ASSIGN_CREW) {
		let logs = [];
		let crews = dispatchLogsUsersService.getDispatchCrews();
		let oldCrewId = _.get(initial, path);
		let oldCrew = crews.find(crew => crew.team_id == oldCrewId);

		if(oldCrew) {
			logs.push({
				textId: logsViewTextIds.dispatchCrew.action.unassignCrewTeam,
				from: oldCrew.name,
			});
		}

		return logs;
	}
}
