describe('dispatch-logs-changes-detector-foreman service', () => {
	let dispatchChangesDetectorForeman;
	let dispatchLogsUsers;
	let request;
	let updatedRequest;

	beforeEach(inject((_dispatchLogsChangesDetectorForemanService_, _dispatchLogsUsersService_) => {
		dispatchChangesDetectorForeman = _dispatchLogsChangesDetectorForemanService_;
		dispatchLogsUsers = _dispatchLogsUsersService_;

		request = testHelper.loadJsonFile('dispatch-logs-changes-detector-foreman__initial-request.mock');
		updatedRequest = testHelper.loadJsonFile('dispatch-logs-changes-detector-foreman__actual-request.mock');

		let workers = testHelper.loadJsonFile('dispatch-logs-dispatcher__workers-team.mock');
		dispatchLogsUsers.initDispatchWorkers(workers);
	}));

	describe('when init', () => {
		it('should be define assignForemanLog', () => {
			expect(dispatchChangesDetectorForeman.assignForemanLog).toBeDefined();
		});

		it('should be define changeForemanLog', () => {
			expect(dispatchChangesDetectorForeman.changeForemanLog).toBeDefined();
		});

		it('should be define chooseForemanLog', () => {
			expect(dispatchChangesDetectorForeman.chooseForemanLog).toBeDefined();
		});

		it('should be define unassignForemanLog', () => {
			expect(dispatchChangesDetectorForeman.unassignForemanLog).toBeDefined();
		});

		it('should be define isReassignForemanLog', () => {
			expect(dispatchChangesDetectorForeman.isReassignForeman).toBeDefined();
		});

		it('should be define isRemoveForeman', () => {
			expect(dispatchChangesDetectorForeman.isRemoveForeman).toBeDefined();
		});

		it('should be define removeForemanLog', () => {
			expect(dispatchChangesDetectorForeman.removeForemanLog).toBeDefined();
		});
	});

	describe('assignForemanLog', () => {
		it('should return logs when assign first time foreman', () => {
			//given
			let expectedResult = [{textId: 2, to: 'FlatRate Foreman'}];
			let result;

			//when
			result = dispatchChangesDetectorForeman.assignForemanLog(request, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should return logs when change foreman', () => {
			//given
			let expectedResult = [{textId: 9, from: 'FlatRate Foreman', to: 'Foreman Flow1'}];
			let result;
			request = angular.copy(updatedRequest);
			updatedRequest.request_data.value.crews.foreman = 5907;

			//when
			result = dispatchChangesDetectorForeman.assignForemanLog(request, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should return empty array when not assign foreman', () => {
			//given
			let expectedResult = [];
			let result;

			//when
			result = dispatchChangesDetectorForeman.assignForemanLog(request, request);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should call remove foreman log when is return foreman log true', () => {
			// given
			spyOn(dispatchChangesDetectorForeman, 'removeForemanLog').and.callFake(() => {});
			spyOn(dispatchChangesDetectorForeman, 'isReassignForeman').and.returnValue(false);
			spyOn(dispatchChangesDetectorForeman, 'isRemoveForeman').and.returnValue(true);
			// when
			dispatchChangesDetectorForeman.assignForemanLog();

			// then
			expect(dispatchChangesDetectorForeman.removeForemanLog).toHaveBeenCalled();
		});
	});

	describe('chooseForemanLog', () => {
		it('should return logs when assign foreman', () => {
			//given
			let expectedResult = [{textId: 2, to: 'FlatRate Foreman'}];
			let result;

			//when
			result = dispatchChangesDetectorForeman.chooseForemanLog(request, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should return empty array when not assign foreman', () => {
			//given
			let expectedResult = [];
			let result;

			//when
			result = dispatchChangesDetectorForeman.chooseForemanLog(request, request);

			//then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('changeForemanLog', () => {
		beforeEach(() => {
			request = angular.copy(updatedRequest);
			updatedRequest.request_data.value.crews.foreman = 5907;
		});

		it('should return logs when change foreman', () => {
			//given
			let expectedResult = [{textId: 9, from: 'FlatRate Foreman', to: 'Foreman Flow1'}];
			let result;

			//when
			result = dispatchChangesDetectorForeman.changeForemanLog(request, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should return empty array when not change foreman', () => {
			//given
			let expectedResult = [];
			let result;

			//when
			result = dispatchChangesDetectorForeman.changeForemanLog(request, request);

			//then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('unassignForemanLog', () => {
		it('should return logs when change foreman', () => {
			//given
			let expectedResult = [{textId: 12, from: 'FlatRate Foreman'}];
			let result;

			//when
			result = dispatchChangesDetectorForeman.unassignForemanLog(updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});

		it('should return empty array when not assign foreman', () => {
			//given
			let expectedResult = [];
			let result;

			//when
			result = dispatchChangesDetectorForeman.unassignForemanLog(request);

			//then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('isReassignForeman', () => {
		it('should detect is init foreman field', () => {
			//given
			let expectedResult = true;
			let result;

			//when
			result = dispatchChangesDetectorForeman.isReassignForeman(updatedRequest, updatedRequest);

			//then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('flat rate', () => {
		let request;
		let updatedRequest;
		let PICKUP_CREW = 'request_data.value.crews.pickedUpCrew.foreman';
		let DELIVERY_CEW = 'request_data.value.crews.deliveryCrew.foreman';

		beforeEach(() => {
			request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request_flat-rate_initial.mock');
			updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request_flat-rate_actual.mock');
		});

		describe('pickup', () => {
			describe('assignForemanLog', () => {
				it('should return logs when assign first time foreman', () => {
					//given
					let expectedResult = [{textId: 2, to: 'FlatRate Foreman'}];
					let result;

					//when
					result = dispatchChangesDetectorForeman.assignForemanLog(request, updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when not assign foreman', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorForeman.assignForemanLog(request, request, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('chooseForemanLog', () => {
				it('should return logs when assign foreman', () => {
					//given
					let expectedResult = [{textId: 2, to: 'FlatRate Foreman'}];
					let result;

					//when
					result = dispatchChangesDetectorForeman.chooseForemanLog(request, updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when not assign foreman', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorForeman.chooseForemanLog(request, request, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('changeForemanLog', () => {
				beforeEach(() => {
					request = angular.copy(updatedRequest);
					updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request_flat-rate-changed_actual.mock');
				});

				it('should return logs when change foreman', () => {
					//given
					let expectedResult = [ { textId : 9, from : 'FlatRate Foreman', to : 'ForemanExclude Test' } ];
					let result;

					//when
					result = dispatchChangesDetectorForeman.changeForemanLog(request, updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when not change foreman', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorForeman.changeForemanLog(request, request, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('unassignForemanLog', () => {
				it('should return logs when change foreman', () => {
					//given
					let expectedResult = [{textId: 12, from: 'FlatRate Foreman'}];
					let result;

					//when
					result = dispatchChangesDetectorForeman.unassignForemanLog(updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when not assign foreman', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorForeman.unassignForemanLog(request, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('isReassignForeman', () => {
				it('should detect is init foreman field', () => {
					//given
					let expectedResult = true;
					let result;

					//when
					result = dispatchChangesDetectorForeman.isReassignForeman(updatedRequest, updatedRequest, PICKUP_CREW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});
		});

		describe('delivery', () => {
			describe('assignForemanLog', () => {
				it('should return logs when assign first time foreman', () => {
					//given
					let expectedResult = [ { textId : 2, to : 'ForemanExclude Test' } ];
					let result;

					//when
					result = dispatchChangesDetectorForeman.assignForemanLog(request, updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when not assign foreman', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorForeman.assignForemanLog(request, request, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('chooseForemanLog', () => {
				it('should return logs when assign foreman', () => {
					//given
					let expectedResult = [ { textId : 2, to : 'ForemanExclude Test' } ];
					let result;

					//when
					result = dispatchChangesDetectorForeman.chooseForemanLog(request, updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when not assign foreman', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorForeman.chooseForemanLog(request, request, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('changeForemanLog', () => {
				beforeEach(() => {
					request = angular.copy(updatedRequest);
					updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request_flat-rate-changed_actual.mock');
				});

				it('should return logs when change foreman', () => {
					//given
					let expectedResult = [ { textId : 9, from : 'ForemanExclude Test', to : 'Foreman Flow1' } ];
					let result;

					//when
					result = dispatchChangesDetectorForeman.changeForemanLog(request, updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when not change foreman', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorForeman.changeForemanLog(request, request, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('unassignForemanLog', () => {
				it('should return logs when change foreman', () => {
					//given
					let expectedResult = [ { textId : 12, from : 'ForemanExclude Test' } ];
					let result;

					//when
					result = dispatchChangesDetectorForeman.unassignForemanLog(updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});

				it('should return empty array when not assign foreman', () => {
					//given
					let expectedResult = [];
					let result;

					//when
					result = dispatchChangesDetectorForeman.unassignForemanLog(request, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});

			describe('isReassignForeman', () => {
				it('should detect is init foreman field', () => {
					//given
					let expectedResult = true;
					let result;

					//when
					result = dispatchChangesDetectorForeman.isReassignForeman(updatedRequest, updatedRequest, DELIVERY_CEW);

					//then
					expect(result).toEqual(expectedResult);
				});
			});
		});
	});

	describe('isRemoveForeman', () => {
		it('should return true when remove foreman', () => {
			// given
			let expectedResult = true;
			let result;
			let initial = {foreman: '1111'};
			let actual = {foreman: ''};
			let patch = 'foreman';

			// when
			result = dispatchChangesDetectorForeman.isRemoveForeman(initial, actual, patch);

			// then
			expect(result).toEqual(expectedResult);
		});

		it('should return false when not remove foreman', () => {
			// given
			let expectedResult = false;
			let result;
			let initial = {foreman: '1111'};
			let actual = {foreman: '1111'};
			let patch = 'foreman';

			// when
			result = dispatchChangesDetectorForeman.isRemoveForeman(initial, actual, patch);

			// then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('removeForemanLog', () => {
		it('should return empty log when foreman not found', () => {
			// given
			let expectedResult = [];
			let result;
			spyOn(dispatchLogsUsers, 'getWorkerById').and.returnValue();

			// when
			result = dispatchChangesDetectorForeman.removeForemanLog();

			// then
			expect(result).toEqual(expectedResult);
		});

		it('should return empty log when foreman not found', () => {
			// given
			let expectedResult = [ { textId : 29, from : 'John' } ];
			let result;
			spyOn(dispatchLogsUsers, 'getWorkerById').and.returnValue({name: 'John'});

			// when
			result = dispatchChangesDetectorForeman.removeForemanLog();

			// then
			expect(result).toEqual(expectedResult);
		});
	});
});