'use strict';

import {DISPATCH_REQUEST_PATH} from '../../../constants/dispatch-request-path.constants';

angular
	.module('app.dispatch')
	.factory('dispatchLogsChangesDetectorForemanService', dispatchLogsChangesDetectorForemanService);

/* @ngInject */
function dispatchLogsChangesDetectorForemanService(logsViewTextIds, dispatchLogsUsersService) {
	let service = {
		assignForemanLog,
		changeForemanLog,
		unassignForemanLog,
		chooseForemanLog,
		isReassignForeman,
		isRemoveForeman,
		removeForemanLog,
	};

	return service;

	function assignForemanLog(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let logs = [];
		let isReassignForeman = service.isReassignForeman(initial, actual, path);
		let isRemvoeForeman = service.isRemoveForeman(initial, actual, path);

		if (isReassignForeman) {
			logs = service.changeForemanLog(initial, actual, path);
		} else if (isRemvoeForeman) {
			logs = service.removeForemanLog(initial, path);
		} else {
			logs = service.chooseForemanLog(initial, actual, path);
		}

		return logs;
	}

	function chooseForemanLog(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let logs = [];

		if (isChooseForeman(initial, actual, path)) {
			let foremanId = _.get(actual, path);
			let foreman = dispatchLogsUsersService.getWorkerById(foremanId);

			if (foreman) {
				logs.push({
					textId: logsViewTextIds.dispatchCrew.action.chooseForeman,
					to: foreman.name
				});
			}
		}

		return logs;
	}

	function isChooseForeman(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let isForemanWasNotSelected = !_.get(initial, path);
		let isChoooseNewForeman = _.get(actual, path);
		return isForemanWasNotSelected && isChoooseNewForeman;
	}

	function changeForemanLog(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let logs = [];

		if (isChangeForeman(initial, actual, path)) {
			let oldForeman = _.get(initial, path);
			let newForeman = _.get(actual, path);
			let foremanIds = [oldForeman, newForeman];
			let workers = dispatchLogsUsersService.getRequestWorkersById(foremanIds);

			logs.push({
				textId: logsViewTextIds.dispatchCrew.action.changeForeman,
				from: _.get(workers, '[0].name'),
				to: _.get(workers, '[1].name')
			});
		}

		return logs;
	}

	function isChangeForeman(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let oldForeman = _.get(initial, path);
		let newForeman = _.get(actual, path);

		return oldForeman != newForeman;
	}

	function unassignForemanLog(initial, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let logs = [];

		if (isHasAssignedForeman(initial, path)) {
			let foremanId = _.get(initial, path);
			let foreman = dispatchLogsUsersService.getWorkerById(foremanId);

			if (foreman) {
				logs.push({
					textId: logsViewTextIds.dispatchCrew.action.unassignForeman,
					from: foreman.name
				});
			}
		}

		return logs;
	}

	function isHasAssignedForeman(initial, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let isAssignedForeman = _.get(initial, path);
		return isAssignedForeman;
	}

	function isReassignForeman(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let oldForeman = _.get(initial, path);
		let newForeman = _.get(actual, path);

		return !!(oldForeman && newForeman);
	}

	function isRemoveForeman(initial, actual, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let oldForeman = _.get(initial, path);
		let newForeman = _.get(actual, path);

		return !!(oldForeman && !newForeman);
	}

	function removeForemanLog(initial, path = DISPATCH_REQUEST_PATH.CREWS_FOREMAN) {
		let logs = [];
		let foremanId = _.get(initial, path);
		let foreman = dispatchLogsUsersService.getWorkerById(foremanId);

		if (foreman) {
			logs.push({
				textId: logsViewTextIds.dispatchCrew.action.removeForeman,
				from: foreman.name
			});
		}

		return logs;
	}
}
