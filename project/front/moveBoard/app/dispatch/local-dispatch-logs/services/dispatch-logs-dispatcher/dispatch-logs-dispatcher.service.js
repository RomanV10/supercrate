'use strict';

import {DISPATCH_LOGS_BROADCASTS} from '../../constants/dispatch-logs-broadcasts.constants';
import {DISPATCH_REQUEST_PATH} from '../../../constants/dispatch-request-path.constants';

angular
	.module('app.dispatch')
	.factory('dispatchLogsDispatcherService', dispatchLogsDispatcherService);

/* @ngInject */
function dispatchLogsDispatcherService(logsService, logsViewTextIds, $rootScope, dispatchLogsUsersService, dispatchLogsChangesDetectorForemanService, dispatchLogsChangesDetectorHelperService, dispatchLogsChangesDetectorSwitcherService, dispatchLogsChangesDetectorAdditionalCrewService, dispatchLogsChangesDetectorCrewsService) {
	const FLAT_RATE = 5;
	const BASE_CREW_TYPE = 'baseCrew';
	const PICKUP_CREW_TYPE = 'pickedUpCrew';
	const DELIVERY_CREW_TYPE = 'deliveryCrew';

	// private property allowed only for test
	let _requestsInfo = {};
	let service = {
		initRequestLog,
		assignTeamLogs,
		unassingTeamLogs,
		chooseCrewLog,

		_requestsInfo,
	};

	return service;

	function initRequestLog(request, mainPurposeId, logType) {
		if (!_.get(request, 'nid')) return;

		let currentRequest = request.source || request;

		if (_requestsInfo[request.nid]) {
			_requestsInfo[request.nid].actual = currentRequest;
		} else {
			let requestInfo = {
				initial: angular.copy(currentRequest),
				actual: currentRequest,
				log: logsService.getNewLogObject(currentRequest, mainPurposeId, logType),
			};

			if (_.get(request, 'service_type.raw') == FLAT_RATE) {
				requestInfo.pickUpLog = logsService.getNewLogObject(currentRequest, mainPurposeId, logType);
				requestInfo.deliveryLog = logsService.getNewLogObject(currentRequest, mainPurposeId, logType);
				requestInfo.deliveryLog.setCrewType(logsViewTextIds.crewType.delivery);
			}

			_requestsInfo[request.nid] = requestInfo;
		}
	}

	function chooseCrewLog(request = {}, crewType = BASE_CREW_TYPE) {
		let nid = request.nid;
		let newCrews = _.get(request, DISPATCH_REQUEST_PATH.CREWS, {});

		if (!_.get(_requestsInfo, nid)) return;

		if (crewType === BASE_CREW_TYPE) {
			chooseBaseCrewLog(nid, newCrews);
		} else {
			chooseFlatRateCrewLog(nid, newCrews, crewType);
		}
	}

	function chooseBaseCrewLog(nid, newCrews) {
		let {initial, log} = _requestsInfo[nid];

		let newCrewId = newCrews.crew;

		dispatchLogsChangesDetectorCrewsService.assignCrewLog(initial, newCrewId)
			.forEach(simpleLog => log.addSimpleLog(simpleLog));

		if (_.get(initial, DISPATCH_REQUEST_PATH.CREWS)) {
			initial.request_data.value.crews.crew = newCrewId;
		} else {
			_.set(initial, DISPATCH_REQUEST_PATH.AUTO_ASSIGN_CREW, newCrewId);
		}
	}

	function chooseFlatRateCrewLog(nid, newCrews, crewType) {
		let {initial, pickUpLog, deliveryLog} = _requestsInfo[nid];

		let newCrewId = newCrews[crewType].crew;
		let log = crewType === PICKUP_CREW_TYPE ? pickUpLog : deliveryLog;
		let pathToCrew = DISPATCH_REQUEST_PATH.CREWS + `.${crewType}.crew`;

		dispatchLogsChangesDetectorCrewsService.assignCrewLog(initial, newCrewId, pathToCrew)
			.forEach(simpleLog => log.addSimpleLog(simpleLog));

		if (_.get(initial, pathToCrew)) {
			initial.request_data.value.crews[crewType].crew = newCrewId;
		} else {
			_.set(initial, pathToCrew, newCrewId);
		}
	}

	function assignTeamLogs(request = {}) {
		let nid = request.nid;

		if (!_.get(_requestsInfo, nid)) return;

		let {initial, actual, log} = _requestsInfo[nid];
		actual = request;

		if (initial.service_type.raw == FLAT_RATE) {
			assignFlatRateRequestLogs(_requestsInfo[nid], request);
		} else {
			assignUsualRequestLogs(initial, actual, log);
		}

		_requestsInfo[nid].initial = angular.copy(actual);
	}

	function assignUsualRequestLogs(initial, actual, log) {
		setAssignTeamMainPurpose(initial, actual, log);

		service.chooseCrewLog(actual);

		dispatchLogsChangesDetectorForemanService.assignForemanLog(initial, actual)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorHelperService.assignHelperLog(initial, actual)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorSwitcherService.assignSwitcherLog(initial, actual)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorAdditionalCrewService.assignAdditionalCrewLog(initial, actual)
			.forEach(addSimpleLog);

		log.createNewLogs()
			.then(() => {
				$rootScope.$broadcast(DISPATCH_LOGS_BROADCASTS.reloadLogsListAfterCreatingLog, initial.nid);
			});

		function addSimpleLog(simpleLog) {
			log.addSimpleLog(simpleLog);
		}
	}

	function assignFlatRateRequestLogs({initial, actual, pickUpLog, deliveryLog, log}, request) {
		actual = request;

		assignPickUpRequestLogs(initial, actual, pickUpLog);
		assignDeliveryRequestLog(initial, actual, deliveryLog);

		dispatchLogsChangesDetectorSwitcherService.assignSwitcherLog(initial, actual)
			.forEach(addSimpleLog);

		setAssignSwithersMainPurpose(initial, actual, log);

		log.addAnotherLog(pickUpLog);
		log.addAnotherLog(deliveryLog);
		log.setCrewType();

		log.createNewLogs()
			.then(() => {
				$rootScope.$broadcast(DISPATCH_LOGS_BROADCASTS.reloadLogsListAfterCreatingLog, initial.nid);
			});

		function addSimpleLog(simpleLog) {
			log.addSimpleLog(simpleLog);
		}
	}

	function assignPickUpRequestLogs(initial, actual, log) {
		setAssignPickupTeamMainPurpose(initial, actual, log);

		service.chooseCrewLog(actual, PICKUP_CREW_TYPE);

		dispatchLogsChangesDetectorForemanService.assignForemanLog(initial, actual, DISPATCH_REQUEST_PATH.PICK_UP_CREWS_FOREMAN)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorHelperService.assignHelperLog(initial, actual, DISPATCH_REQUEST_PATH.PICK_UP_CREWS_HELPERS)
			.forEach(addSimpleLog);

		function addSimpleLog(simpleLog) {
			log.addSimpleLog(simpleLog);
		}
	}

	function assignDeliveryRequestLog(initial, actual, log) {
		setAssignDeliveryTeamMainPurpose(initial, actual, log);

		service.chooseCrewLog(actual, DELIVERY_CREW_TYPE);

		dispatchLogsChangesDetectorForemanService.assignForemanLog(initial, actual, DISPATCH_REQUEST_PATH.DELIVERY_CREWS_FOREMAN)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorHelperService.assignHelperLog(initial, actual, DISPATCH_REQUEST_PATH.DELIVERY_CREWS_HELPERS)
			.forEach(addSimpleLog);

		function addSimpleLog(simpleLog) {
			log.addSimpleLog(simpleLog);
		}
	}

	function setAssignSwithersMainPurpose(initial, actual, log) {
		if (dispatchLogsChangesDetectorSwitcherService.isReassignSwitcher(initial, actual)) {
			log.setMainPurpose(logsViewTextIds.dispatchCrew.reassignCrew);
		} else {
			log.setMainPurpose(logsViewTextIds.dispatchCrew.assignCrew);
		}
	}

	function setAssignPickupTeamMainPurpose(initial, actual, log) {
		if (isReassignPickupCrew(initial, actual)) {
			log.setMainPurpose(logsViewTextIds.dispatchCrew.reassignCrew);
		} else {
			log.setMainPurpose(logsViewTextIds.dispatchCrew.assignCrew);
		}
	}

	function isReassignPickupCrew(initial, actual) {
		let isReassignForeman = dispatchLogsChangesDetectorForemanService.isReassignForeman(initial, actual, DISPATCH_REQUEST_PATH.PICK_UP_CREWS_FOREMAN);
		let isReassignHelper = dispatchLogsChangesDetectorHelperService.isReassignHelper(initial, actual, DISPATCH_REQUEST_PATH.PICK_UP_CREWS_HELPERS);

		return isReassignForeman && isReassignHelper;
	}

	function setAssignDeliveryTeamMainPurpose(initial, actual, log) {
		if (isReassignDeliveryCrew(initial, actual)) {
			log.setMainPurpose(logsViewTextIds.dispatchCrew.reassignCrew);
		} else {
			log.setMainPurpose(logsViewTextIds.dispatchCrew.assignCrew);
		}
	}

	function isReassignDeliveryCrew(initial, actual) {
		let isReassignForeman = dispatchLogsChangesDetectorForemanService.isReassignForeman(initial, actual, DISPATCH_REQUEST_PATH.DELIVERY_CREWS_FOREMAN);
		let isReassignHelper = dispatchLogsChangesDetectorHelperService.isReassignHelper(initial, actual, DISPATCH_REQUEST_PATH.DELIVERY_CREWS_HELPERS);

		return isReassignForeman && isReassignHelper;
	}

	function setAssignTeamMainPurpose(initial, actual, log) {
		if (isReassignCrew(initial, actual)) {
			log.setMainPurpose(logsViewTextIds.dispatchCrew.reassignCrew);
		} else {
			log.setMainPurpose(logsViewTextIds.dispatchCrew.assignCrew);
		}
	}

	function isReassignCrew(initial, actual) {
		let isReassignForeman = dispatchLogsChangesDetectorForemanService.isReassignForeman(initial, actual);
		let isReassignHelper = dispatchLogsChangesDetectorHelperService.isReassignHelper(initial, actual);

		return isReassignForeman && isReassignHelper;
	}

	function unassingTeamLogs(request) {
		let nid = request.nid;

		if (!_.get(_requestsInfo, nid)) return;

		let {initial, actual, log} = _requestsInfo[nid];
		actual = request.source || request;

		log.setMainPurpose(logsViewTextIds.dispatchCrew.unassignCrew);

		if (initial.service_type.raw == FLAT_RATE) {
			unassignFlatRateRequestLog(_requestsInfo[nid]);
		} else {
			unassignUsualRequestLog(initial, log);
		}

		_requestsInfo[nid].initial = angular.copy(actual);
	}

	function unassignFlatRateRequestLog({initial, log, pickUpLog, deliveryLog}) {
		unassignPickupCrew(initial, pickUpLog);
		unassignDeliveryCrew(initial, deliveryLog);

		dispatchLogsChangesDetectorSwitcherService.unassignSwitcherLog(initial)
			.forEach(addSimpleLog);

		log.addAnotherLog(pickUpLog);
		log.addAnotherLog(deliveryLog);
		log.setMainPurpose(logsViewTextIds.dispatchCrew.unassignCrew);
		log.setCrewType();
		log.createNewLogs()
			.then(() => {
				$rootScope.$broadcast(DISPATCH_LOGS_BROADCASTS.reloadLogsListAfterCreatingLog, initial.nid);
			});

		function addSimpleLog(simpleLog) {
			log.addSimpleLog(simpleLog);
		}
	}

	function unassignPickupCrew(initial, log) {
		log.setMainPurpose(logsViewTextIds.dispatchCrew.unassignCrew);

		dispatchLogsChangesDetectorCrewsService.unassignCrewLog(initial, DISPATCH_REQUEST_PATH.PICK_UP_AUTO_ASSIGN_CREW)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorForemanService.unassignForemanLog(initial, DISPATCH_REQUEST_PATH.PICK_UP_CREWS_FOREMAN)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorHelperService.unassignHelperLog(initial, DISPATCH_REQUEST_PATH.PICK_UP_CREWS_HELPERS)
			.forEach(addSimpleLog);

		log.setMainPurpose(logsViewTextIds.dispatchCrew.unassignCrew);

		function addSimpleLog(simpleLog) {
			log.addSimpleLog(simpleLog);
		}
	}

	function unassignDeliveryCrew(initial, log) {
		log.setMainPurpose(logsViewTextIds.dispatchCrew.unassignCrew);

		dispatchLogsChangesDetectorCrewsService.unassignCrewLog(initial, DISPATCH_REQUEST_PATH.DELIVERY_AUTO_ASSIGN_CREW)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorForemanService.unassignForemanLog(initial, DISPATCH_REQUEST_PATH.DELIVERY_CREWS_FOREMAN)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorHelperService.unassignHelperLog(initial, DISPATCH_REQUEST_PATH.DELIVERY_CREWS_HELPERS)
			.forEach(addSimpleLog);

		function addSimpleLog(simpleLog) {
			log.addSimpleLog(simpleLog);
		}
	}

	function unassignUsualRequestLog(initial, log) {
		dispatchLogsChangesDetectorCrewsService.unassignCrewLog(initial)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorForemanService.unassignForemanLog(initial)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorHelperService.unassignHelperLog(initial)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorSwitcherService.unassignSwitcherLog(initial)
			.forEach(addSimpleLog);

		dispatchLogsChangesDetectorAdditionalCrewService.unassignAdditionalCrewLog(initial)
			.forEach(addSimpleLog);

		log.createNewLogs()
			.then(() => {
				$rootScope.$broadcast(DISPATCH_LOGS_BROADCASTS.reloadLogsListAfterCreatingLog, initial.nid);
			});

		function addSimpleLog(simpleLog) {
			log.addSimpleLog(simpleLog);
		}
	}
}
