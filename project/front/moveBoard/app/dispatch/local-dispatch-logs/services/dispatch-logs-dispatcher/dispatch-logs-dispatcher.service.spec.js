describe('Dispatch logs dispatcher', () => {
	let dispatchLogsDispatcherService;
	let logsService;
	let request;
	var logs;
	let dispatchLogsUsers;
	let changesDetectorForemanService;
	let dispatchChangesDetectorHelper;
	let dispatchChangesDetectorSwitcher;
	let dispatchChangesDetectorAdditionalCrew;
	let dispatchLogsChangesDetectorCrews;
	let assignForemanFake;
	let unassignForemanFake;
	let assignHelperFake;
	let unassignHelperFake;
	let assignSwitcherFake;
	let unassignSwitcherFake;
	let assignAdditionalCrew;
	let unassignAdditionalCrew = [];
	let assignCrewLogFake;
	let crewType;
	let logObject = {
		createNewLogs() {
			return this;
		},
		setMainPurpose() {},
		addSimpleLog(log) {
			logs.push(log);
		},
		setCrewType(type) {
			crewType = type;
		},
		addAnotherLog() {},
		then(callback) {callback();}
	};

	beforeEach(inject((_dispatchLogsDispatcherService_, _logsService_, _dispatchLogsUsersService_, _dispatchLogsChangesDetectorForemanService_, _dispatchLogsChangesDetectorHelperService_, _dispatchLogsChangesDetectorSwitcherService_, _dispatchLogsChangesDetectorAdditionalCrewService_, _dispatchLogsChangesDetectorCrewsService_) => {
		changesDetectorForemanService = _dispatchLogsChangesDetectorForemanService_;
		dispatchLogsDispatcherService = _dispatchLogsDispatcherService_;
		dispatchChangesDetectorHelper = _dispatchLogsChangesDetectorHelperService_;
		dispatchChangesDetectorSwitcher = _dispatchLogsChangesDetectorSwitcherService_;
		dispatchChangesDetectorAdditionalCrew = _dispatchLogsChangesDetectorAdditionalCrewService_;
		dispatchLogsChangesDetectorCrews = _dispatchLogsChangesDetectorCrewsService_;
		logsService = _logsService_;
		dispatchLogsUsers = _dispatchLogsUsersService_;
		request = {nid: 1111};
		logs = [];

		spyOn(logsService, 'getNewLogObject')
			.and
			.returnValue(logObject);

		let workers = testHelper.loadJsonFile('dispatch-logs-dispatcher__workers-team.mock');
		dispatchLogsUsers.initDispatchWorkers(workers);

		assignForemanFake = [];
		unassignForemanFake = [];
		assignHelperFake = [];
		unassignHelperFake = [];
		assignSwitcherFake = [];
		unassignSwitcherFake = [];
		assignAdditionalCrew = [];
		unassignAdditionalCrew = [];
		assignCrewLogFake = [];
		spyOn(changesDetectorForemanService, 'assignForemanLog').and.returnValue(assignForemanFake);
		spyOn(changesDetectorForemanService, 'unassignForemanLog').and.returnValue(unassignForemanFake);
		spyOn(dispatchChangesDetectorHelper, 'assignHelperLog').and.returnValue(assignHelperFake);
		spyOn(dispatchChangesDetectorHelper, 'unassignHelperLog').and.returnValue(unassignHelperFake);
		spyOn(dispatchChangesDetectorSwitcher, 'assignSwitcherLog').and.returnValue(assignSwitcherFake);
		spyOn(dispatchChangesDetectorSwitcher, 'unassignSwitcherLog').and.returnValue(unassignSwitcherFake);
		spyOn(dispatchChangesDetectorAdditionalCrew, 'assignAdditionalCrewLog').and.returnValue(assignAdditionalCrew);
		spyOn(dispatchChangesDetectorAdditionalCrew, 'unassignAdditionalCrewLog').and.returnValue(unassignAdditionalCrew);
		spyOn(dispatchLogsChangesDetectorCrews, 'assignCrewLog').and.returnValue(assignCrewLogFake);
	}));

	describe('when init', () => {
		it('initRequestLog should be defined', () => {
			expect(dispatchLogsDispatcherService.initRequestLog).toBeDefined();
		});

		it('assignTeamLogs should be defined', () => {
			expect(dispatchLogsDispatcherService.assignTeamLogs).toBeDefined();
		});
	});

	describe('initRequestLog', () => {
		it('should do nothing when no request', () => {
			//given
			let expectedResult = {};

			//when
			dispatchLogsDispatcherService.initRequestLog();

			//then
			expect(dispatchLogsDispatcherService._requestsInfo).toEqual(expectedResult);
		});

		it('should init _requestsInfo', () => {
			//given
			let expectedResult = {
				1111: {
					initial: request,
					actual: request,
					log: logObject,
				}
			};

			//when
			dispatchLogsDispatcherService.initRequestLog(request);

			//then
			expect(dispatchLogsDispatcherService._requestsInfo).toEqual(expectedResult);
		});

		it('should copy initial request', () => {
			//given
			let expectedResult = 1111;

			//when
			//when
			dispatchLogsDispatcherService.initRequestLog(request);
			request.nid = 22222;

			//then
			expect(dispatchLogsDispatcherService._requestsInfo['1111'].initial.nid).toEqual(expectedResult);
		});

		describe('when call function twice', () => {
			it('should update actual request', () => {
				//given
				let expectedResult = true;

				//when
				//when
				dispatchLogsDispatcherService.initRequestLog(request);
				request.nid = 1111;
				request.changes = true;
				dispatchLogsDispatcherService.initRequestLog(request);

				//then
				expect(dispatchLogsDispatcherService._requestsInfo['1111'].actual.changes).toEqual(expectedResult);
			});

			it('should not update initial request', () => {
				//given
				let expectedResult = undefined;

				//when
				//when
				dispatchLogsDispatcherService.initRequestLog(request);
				request.changes = true;
				dispatchLogsDispatcherService.initRequestLog(request);

				//then
				expect(dispatchLogsDispatcherService._requestsInfo['1111'].initial.changes).toEqual(expectedResult);
			});
		});

		it('should call get new log object', () => {
			//given
			let initRequestLogParameters = [request, 1, 1];

			//when
			dispatchLogsDispatcherService.initRequestLog(...initRequestLogParameters);

			//then
			expect(logsService.getNewLogObject).toHaveBeenCalledWith(...initRequestLogParameters);
		});
	});

	describe('assignTeamLogs', () => {
		let request;
		let updatedRequest;

		beforeEach(() => {
			request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request.mock');
			dispatchLogsDispatcherService.initRequestLog(request);
			updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-team_assigned.mock');
			dispatchLogsDispatcherService.initRequestLog(updatedRequest);
		});

		it('should not call addSimpleLog when no changes', () => {
			//given
			spyOn(logObject, 'addSimpleLog');

			//when
			dispatchLogsDispatcherService.assignTeamLogs({nid: 1111});

			//then
			expect(logObject.addSimpleLog).not.toHaveBeenCalled();
		});

		it('should call chooseCrewLog', () => {
			//given
			spyOn(dispatchLogsDispatcherService, 'chooseCrewLog');

			//when
			dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);

			//then
			expect(dispatchLogsDispatcherService.chooseCrewLog).toHaveBeenCalled();
		});

		describe('when update request', () => {
			it('should copy actual to initial after creating log', () => {
				//given
				let requestInfo;

				//when
				dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);
				requestInfo = dispatchLogsDispatcherService._requestsInfo[request.nid];

				//then
				expect(requestInfo.initial).toEqual(requestInfo.actual);
			});

			it('should try to create new log after making log', () => {
				//given
				spyOn(logObject, 'createNewLogs').and.callThrough();

				//when
				dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);

				//then
				expect(logObject.createNewLogs).toHaveBeenCalled();
			});

			it('should broadcast reload dispatch log', () => {
				//given
				let expectedResult = ['reload_request_dispatch_log', '13889'];
				spyOn($rootScope, '$broadcast');

				//when
				dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);

				//then
				expect($rootScope.$broadcast).toHaveBeenCalledWith(...expectedResult);
			});

			it('should change main purpose id in log', () => {
				//given
				spyOn(logObject, 'setMainPurpose');
				let expectedResult = 1;

				//when
				dispatchLogsDispatcherService.assignTeamLogs(request);

				//then
				expect(logObject.setMainPurpose).toHaveBeenCalledWith(expectedResult);
			});
		});
	});

	describe('unassingTeamLogs', () => {
		let request;
		let updatedRequest;

		beforeEach(() => {
			request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request-team_assigned.mock');
			dispatchLogsDispatcherService.initRequestLog(request);
			updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__request.mock');
			dispatchLogsDispatcherService.initRequestLog(updatedRequest);
		});

		it('should not call addSimpleLog when no changes', () => {
			//given
			spyOn(logObject, 'addSimpleLog');

			//when
			dispatchLogsDispatcherService.unassingTeamLogs({nid: 1111});

			//then
			expect(logObject.addSimpleLog).not.toHaveBeenCalled();
		});

		describe('when update request', () => {
			it('should change main purpose id in log', () => {
				//given
				spyOn(logObject, 'setMainPurpose');
				let expectedResult = 2;

				//when
				dispatchLogsDispatcherService.unassingTeamLogs(request);

				//then
				expect(logObject.setMainPurpose).toHaveBeenCalledWith(expectedResult);
			});

			describe('when unassign helper', () => {
				it('should call addSimpleLog', () => {
					//given
					let expectedResult = { textId : 13, from : 'helper test1' };
					unassignHelperFake.push(expectedResult);
					spyOn(logObject, 'addSimpleLog');

					//when
					dispatchLogsDispatcherService.unassingTeamLogs(updatedRequest);

					//then
					expect(logObject.addSimpleLog).toHaveBeenCalledWith(expectedResult);
				});
			});

			describe('when unassign foreman', () => {
				it('should write unassign foreman log', () => {
					//given
					let expectedResult = { textId : 12, from : 'FlatRate Foreman' };
					unassignForemanFake.push(expectedResult);

					spyOn(logObject, 'addSimpleLog');

					//when
					dispatchLogsDispatcherService.unassingTeamLogs(request);

					//then
					expect(logObject.addSimpleLog).toHaveBeenCalledWith(expectedResult);
				});
			});

			describe('when unassign switcher', () => {
				it('addSimpleLog should be called', () => {
					//given
					let expectedResult = {textId: 19, for: 'helper test2', to: 'Test Helper3'};
					unassignSwitcherFake.push(expectedResult);
					spyOn(logObject, 'addSimpleLog');

					//when
					dispatchLogsDispatcherService.unassingTeamLogs(request);

					//then
					expect(logObject.addSimpleLog).toHaveBeenCalledWith(expectedResult);
				});
			});

			describe('when unassign additional crew', () => {
				it('should call addSimpleLog when retrieve logs', () => {
					//given
					let expectedResult = {textId: 21, for: 2, from: 'helper test2, Test Helper1, Test Helper2'};
					unassignAdditionalCrew.push(expectedResult);
					spyOn(logObject, 'addSimpleLog');

					//when
					dispatchLogsDispatcherService.unassingTeamLogs(updatedRequest);

					//then
					expect(logObject.addSimpleLog).toHaveBeenCalledWith(expectedResult);
				});
			});

			it('should copy actual to initial after creating log', () => {
				//given
				let requestInfo;

				//when
				dispatchLogsDispatcherService.unassingTeamLogs(request);
				requestInfo = dispatchLogsDispatcherService._requestsInfo[request.nid];

				//then
				expect(requestInfo.initial).toEqual(request);
			});

			it('should try to create new log after making log', () => {
				//given
				spyOn(logObject, 'createNewLogs').and.callThrough();

				//when
				dispatchLogsDispatcherService.unassingTeamLogs(request);

				//then
				expect(logObject.createNewLogs).toHaveBeenCalled();
			});

			it('should broadcast reload dispatch log', () => {
				//given
				let expectedResult = ['reload_request_dispatch_log', '13889'];
				spyOn($rootScope, '$broadcast');

				//when
				dispatchLogsDispatcherService.unassingTeamLogs(request);

				//then
				expect($rootScope.$broadcast).toHaveBeenCalledWith(...expectedResult);
			});
		});
	});

	describe('reassignTeamLogs', () => {
		let request;
		let updatedRequest;

		beforeEach(() => {
			request = testHelper.loadJsonFile('dispatch-logs-dispatcher__reassign-crew-initial-request.mock');
			dispatchLogsDispatcherService.initRequestLog(request);
			updatedRequest = testHelper.loadJsonFile('dispatch-logs-dispatcher__reassig-crew-actual-request.mock');
			dispatchLogsDispatcherService.initRequestLog(updatedRequest);
		});

		describe('when update request', () => {
			describe('when change additional crew', () => {
				it('should detect change additional crew', () => {
					//given
					spyOn(logObject, 'addSimpleLog');
					let expectedResult = {};
					assignAdditionalCrew.push(expectedResult);

					//when
					dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);

					//then
					expect(logObject.addSimpleLog).toHaveBeenCalledWith(expectedResult);
				});
			});

			describe('when change foreman', () => {
				it('should detect change foreman', () => {
					//given
					spyOn(logObject, 'addSimpleLog');
					let expectedResult = { textId : 9, from : 'FlatRate Foreman', to : 'Test Foreman' };
					assignForemanFake.push(expectedResult);

					//when
					dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);

					//then
					expect(logObject.addSimpleLog).toHaveBeenCalledWith(expectedResult);
				});
			});

			describe('when choose helper', () => {
				it('should call addSimpleLog when return assign helper log', () => {
					//given
					let expectedResult = {};
					assignHelperFake.push(expectedResult);
					spyOn(logObject, 'addSimpleLog');

					//when
					dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);

					//then
					expect(logObject.addSimpleLog).toHaveBeenCalledWith(expectedResult);
				});
			});

			describe('when assign swithcer', () => {
				it('should call addSimpleLog', () => {
					//given
					spyOn(logObject, 'addSimpleLog');
					let expectedResult = {};
					assignSwitcherFake.push(expectedResult);

					//when
					dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);

					//then
					expect(logObject.addSimpleLog).toHaveBeenCalledWith(expectedResult);
				});
			});

			it('should copy actual to initial after creating log', () => {
				//given
				let requestInfo;

				//when
				dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);
				requestInfo = dispatchLogsDispatcherService._requestsInfo[request.nid];

				//then
				expect(requestInfo.initial).toEqual(requestInfo.actual);
			});

			it('should try to create new log after making log', () => {
				//given
				spyOn(logObject, 'createNewLogs').and.callThrough();

				//when
				dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);

				//then
				expect(logObject.createNewLogs).toHaveBeenCalled();
			});

			it('should broadcast reload dispatch log', () => {
				//given
				let expectedResult = ['reload_request_dispatch_log', '13896'];
				spyOn($rootScope, '$broadcast');

				//when
				dispatchLogsDispatcherService.assignTeamLogs(updatedRequest);

				//then
				expect($rootScope.$broadcast).toHaveBeenCalledWith(...expectedResult);
			});

			it('should change main purpose id in log', () => {
				//given
				spyOn(logObject, 'setMainPurpose');
				let expectedResult = 3;

				//when
				dispatchLogsDispatcherService.assignTeamLogs(request);

				//then
				expect(logObject.setMainPurpose).toHaveBeenCalledWith(expectedResult);
			});
		});
	});

	describe('chooseCrewLog', () => {
		beforeEach(() => {
			request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request.mock');
			dispatchLogsDispatcherService.initRequestLog(request);
		});

		it('should write request crew when no crew in request', () => {
			//given
			_.set(request, 'request_data.value.crews.crew', 1);
			let expectedResult = 1;

			//when
			dispatchLogsDispatcherService.chooseCrewLog(request);

			//then
			expect(dispatchLogsDispatcherService._requestsInfo[request.nid].initial.request_data.value.crews.crew).toEqual(expectedResult);
		});
	});

	describe('flat rate', () => {
		beforeEach(() => {
			request = testHelper.loadJsonFile('dispatch-logs-dispatcher__request_flat-rate_initial.mock');
			dispatchLogsDispatcherService.initRequestLog(request);
		});

		describe('logs', () => {
			it('should init pickup log', () => {
				expect(dispatchLogsDispatcherService._requestsInfo[request.nid].pickUpLog).toBeDefined();
			});

			it('should init delivery log', () => {
				expect(dispatchLogsDispatcherService._requestsInfo[request.nid].deliveryLog).toBeDefined();
			});

			it('should set delivery crew type', () => {
				//given
				let expectedResult = 2;

				//when

				//then
				expect(crewType).toEqual(expectedResult);
			});
		});

		describe('assignTeam', () => {
			it('should call chooseCrewLog twice', () => {
				//given
				let expectedResult = 2;
				spyOn(dispatchLogsDispatcherService, 'chooseCrewLog').and.returnValue([]);

				//when
				dispatchLogsDispatcherService.assignTeamLogs(request);

				//then
				expect(dispatchLogsDispatcherService.chooseCrewLog.calls.count()).toEqual(expectedResult);
			});
		});
	});
});
