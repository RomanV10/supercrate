'use strict';

import './full-view.styl';

angular
	.module('app.dispatch')
	.controller('DispatchLogsFullViewController', DispatchLogsFullViewController);

/* @ngInject */
function DispatchLogsFullViewController(logs, pagination, $uibModalInstance) {
	const DATE_VIEW = 1;
	let vm = this;

	vm.logs = logs;
	vm.pagination = pagination;
	vm.isShowNID = pagination.currentView == DATE_VIEW;

	vm.close = close;

	function close() {
		$uibModalInstance.close();
	}
}