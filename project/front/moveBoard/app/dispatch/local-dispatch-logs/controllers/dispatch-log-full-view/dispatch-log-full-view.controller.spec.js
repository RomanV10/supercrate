describe('DispatchLogsFullViewController', () => {
	let $controller;
	let controller;
	let $uibModalInstance = {close() {}};

	function compileController() {
		let logs = [];
		let pagination = {};
		controller = $controller('DispatchLogsFullViewController', {logs, pagination, $uibModalInstance});
	}

	beforeEach(inject((_$controller_) => {
		$controller = _$controller_;

		compileController();
	}));

	describe('when init', () => {
		it('controller should be defined', () => {
			expect(controller).toBeDefined();
		});

		it('logs to be defined', () => {
			expect(controller.logs).toBeDefined();
		});

		it('pagination to be defined', () => {
			expect(controller.pagination).toBeDefined();
		});

		it('close to be defined', () => {
			expect(controller.close).toBeDefined();
		});
	});

	describe('close', () => {
		it('should call dismiss', () => {
			//given
			spyOn($uibModalInstance, 'close');

			//when
			controller.close();

			//then
			expect($uibModalInstance.close).toHaveBeenCalled();
		});
	});
});