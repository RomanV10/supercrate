describe('Dispatch logs full view list directive', () => {
	let element;
	let elementScope;
	let $scope;

	beforeEach(() => {
		$scope = $rootScope.$new();
		$scope.logs = [];
		let elem = angular.element('<dispatch-logs-full-view-list logs="logs"></dispatch-logs-full-view-list>');
		element = $compile(elem)($scope);
		elementScope = element.isolateScope();
	});

	describe('When init', () => {
		it('should not render empty html', () => {
			expect(element.html()).not.toEqual('');
		});

		it('logs should be defined', () => {
			expect(elementScope.logs).toBeDefined();
		});
	});
});