'use strict';

import './full-view-list.styl';

angular
	.module('app.dispatch')
	.directive('dispatchLogsFullViewList', dispatchLogsFullViewList);

/* @ngInject */
function dispatchLogsFullViewList() {
	return {
		scope: {
			logs: '<',
			isShowNID: '<',
		},
		restrict: 'E',
		template: require('./full-view-list.template.html'),
	};
}