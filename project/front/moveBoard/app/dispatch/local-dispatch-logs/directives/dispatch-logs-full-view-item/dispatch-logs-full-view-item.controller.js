'use strict';

angular
	.module('app.dispatch')
	.controller('DispatchLogsFullViewItemController', DispatchLogsFullViewItemController);

/* @ngInject */
function DispatchLogsFullViewItemController($scope) {
	let log = $scope.log;

	if (_.get(log, 'data.fullLogs')) {
		let logType = log.data.logType;
		log.data.fullLogs.forEach((fullViewLog) => fullViewLog.logType = logType);
	}
}