'use strict';

import './full-view-item.styl';

angular
	.module('app.dispatch')
	.directive('dispatchLogsFullViewItem', dispatchLogsFullViewItem);

/* @ngInject */
function dispatchLogsFullViewItem() {
	return {
		scope: {
			log: '<',
			isShowNID: '<',
		},
		restrict: 'E',
		template: require('./full-view-item.template.html'),
		controller: 'DispatchLogsFullViewItemController',
	};
}