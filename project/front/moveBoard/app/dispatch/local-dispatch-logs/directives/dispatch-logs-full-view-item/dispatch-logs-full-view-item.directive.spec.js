describe('Dispatch logs full view item directive', () => {
	let element;
	let elementScope;
	let $scope;

	beforeEach(() => {
		$scope = $rootScope.$new();
		$scope.log = {};
		let elem = angular.element('<dispatch-logs-full-view-item log="log"></dispatch-logs-full-view-item>');
		element = $compile(elem)($scope);
		elementScope = element.isolateScope();
	});

	describe('When init', () => {
		it('should not render empty html', () => {
			expect(element.html()).not.toEqual('');
		});

		it('log should be defined', () => {
			expect(elementScope.log).toBeDefined();
		});
	});
});