describe('Dispatch full view item controller', () => {
	let $controller;
	let controller;
	let $scope

	function compileController() {
		controller = $controller('DispatchLogsFullViewItemController', {$scope});
	}

	beforeEach(inject((_$controller_) => {
		$controller = _$controller_;
		let log = {
			data: {
				logType: 1,
				fullLogs: [{}],
			}
		};
		$scope = {log};

		compileController();
	}));

	describe('when init', () => {
		it('Should write logType to log.data', () => {
			//given
			let exceptedResult = 1;

			//when

			//then
			expect($scope.log.data.fullLogs[0].logType).toEqual(exceptedResult);
		});
	});
});