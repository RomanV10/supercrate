describe('Dispatch logs controller', () => {
	const TODAY = '2018-06-19';
	let $controller;
	let $scope;
	let controller;
	let $uibModal;
	let logsService;
	let defaultLogs = {logs: [], current_page: 1, pages_count: 2};
	let getLogsFake;

	function compileController() {
		controller = $controller('DispatchLogsController', {$scope});
	}

	beforeEach(inject((_$controller_, _logsService_, _$uibModal_) => {
		$controller = _$controller_;
		logsService = _logsService_;
		$uibModal = _$uibModal_;
		$scope = $rootScope.$new();
		$scope.selectedDate = TODAY;
		getLogsFake = {
			then(callback, errorCallBack) {
				callback(defaultLogs);

				if (errorCallBack) {
					errorCallBack();
				}

				return this;
			},
			finally() {}
		};

		spyOn(logsService, 'getLogsByDay')
			.and
			.returnValue(getLogsFake);
		spyOn(logsService, 'getLogsByNid')
			.and
			.returnValue(getLogsFake);

		compileController();
	}));

	describe('when init', () => {
		it('controller should be defined', () => {
			expect(controller).toBeDefined();
		});

		it('busy should be true', () => {
			expect(controller.busy).toBeTruthy();
		});

		it('should retrieve logs by current day', () => {
			//given
			let expectedData = [TODAY, 1];

			//when

			//then
			expect(logsService.getLogsByDay).toHaveBeenCalledWith(...expectedData);
		});

		it('logs should be defined', () => {
			//given
			let expectedData = [];

			//when

			//then
			expect(controller.logs).toEqual(expectedData);
		});

		it('pagination should be defined', () => {
			expect(controller.pagination).toBeDefined();
		});

		it('pagination.setPage should be defiend', () => {
			expect(controller.pagination.setPage).toBeDefined();
		});

		it('openFullView should be defined', () => {
			expect(controller.openFullView).toBeDefined();
		});
	});

	describe('load request by nid', () => {
		beforeEach(() => {
			//given
			$scope.nid = 10;

			//when
			$scope.$apply();
		});

		it('should call get logs by nid', () => {
			//then
			expect(logsService.getLogsByNid).toHaveBeenCalled();
		});

		it('should set empty logs when error load logs', () => {
			//given
			let expectedResult = [];

			//then
			expect(controller.logs).toEqual(expectedResult);
		});
	});

	describe('openFullView', () => {
		it('should call $uibModal.open', () => {
			//given
			spyOn($uibModal, 'open')
				.and
				.returnValue();

			//when
			controller.openFullView();

			//then
			expect($uibModal.open).toHaveBeenCalled();
		});
	});

	describe('reload dispatch logs list', () => {
		describe('when request logs selected', () => {
			beforeEach(() => {
				//given
				$scope.nid = 10;

				//when
				$scope.$apply();
				$rootScope.$broadcast('reload_request_dispatch_log', 10);
				$scope.$apply();
			});

			it('should call get request by nid', () => {
				//then
				expect(logsService.getLogsByNid).toHaveBeenCalled();
			});

			it('should call get request by nid twice', () => {
				//given
				let expectedCallCount = 2;

				//when
				$timeout.flush();

				//then
				expect(logsService.getLogsByNid.calls.count()).toEqual(expectedCallCount);
			});
		});

		describe('when reload logs by date', () => {
			beforeEach(() => {
				//given

				//when
				$scope.$apply();
				$rootScope.$broadcast('load_logs_by_date');
				$scope.$apply();
			});

			it('should call get logs by day', () => {
				//then
				expect(logsService.getLogsByDay).toHaveBeenCalled();
			});

			it('should call get logs by day three times', () => {
				//given
				let expectedCallCount = 3;

				//when

				//then
				expect(logsService.getLogsByDay.calls.count()).toEqual(expectedCallCount);
			});
		})
	});

	describe('pagination', () => {
		beforeEach(() => {
			getLogsFake.then = (callback) => {
				callback(defaultLogs);
				return getLogsFake;
			};
		});

		describe('setPage', () => {
			describe('when current page less than page count', () => {
				it('should increase current page on 1', () => {
					//given
					getLogsFake.then = () => {return getLogsFake};
					let expectedResult = 2;
					controller.pagination.currentPage = 1;
					controller.pagination.pagesCount = 2;

					//when
					controller.pagination.setPage(2);

					//then
					expect(controller.pagination.currentPage).toEqual(expectedResult);
				});

				it('should call get logs by day when current view day', () => {
					//given
					getLogsFake.then = () => {return getLogsFake};
					let expectedResult = 2;
					controller.pagination.currentPage = 1;
					controller.pagination.pagesCount = 2;

					//when
					controller.pagination.setPage(2);

					//then
					expect(logsService.getLogsByDay.calls.count()).toEqual(expectedResult);
				});

				it('should call get logs by nid when selected request', () => {
					//given
					getLogsFake.then = () => {return getLogsFake};
					let expectedCallCount = 2;
					controller.pagination.currentPage = 1;
					controller.pagination.pagesCount = 2;
					$scope.nid = 10;

					//when
					$scope.$apply();
					controller.pagination.setPage(2);

					//then
					expect(logsService.getLogsByNid.calls.count()).toEqual(expectedCallCount);
				});
			});
		});
	});
});
