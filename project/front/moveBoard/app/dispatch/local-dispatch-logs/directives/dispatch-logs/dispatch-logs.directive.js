'use strict';

import './dispatch-logs.styl';

angular
	.module('app.dispatch')
	.directive('dispatchLogs', dispatchLogs);

/* @ngInject */
function dispatchLogs() {
	return {
		scope: {
			nid: '<?',
			selectedDate: '<'
		},
		restrict: 'E',
		template: require('./dispatch-logs.template.html'),
		controller: 'DispatchLogsController',
		controllerAs: 'vm',
	};
}