'use strict';

import {DISPATCH_LOGS_BROADCASTS} from '../../constants/dispatch-logs-broadcasts.constants';

angular
	.module('app.dispatch')
	.controller('DispatchLogsController', DispatchLogsController);

/* @ngInject */
function DispatchLogsController($scope, logsService, $uibModal, debounce) {
	const DISPATCH_LOGS_DATE_VIEW = 1;
	const DISPATCH_LOGS_REQUEST_VIEW = 2;
	const HALF_MINUTES_DEBOUCE = 500;

	let vm = this;
	vm.currentVeiw = DISPATCH_LOGS_DATE_VIEW;

	vm.logs = [];

	vm.pagination = {
		setPage(page) {
			this.currentPage = page;
			updateCurrentViewRequest();
		}
	};

	vm.openFullView = openFullView;
	vm.showShortLog = showShortLog;
	let onLoadLogsByRequestWithDebounce = debounce(HALF_MINUTES_DEBOUCE, onLoadLogsByRequest);

	$scope.$watch('selectedDate', onLoadLogsByDate);
	$scope.$watch('nid', onLoadLogsByRequest);
	$scope.$on(DISPATCH_LOGS_BROADCASTS.reloadLogsListAfterCreatingLog, reloadLogsList);
	$scope.$on(DISPATCH_LOGS_BROADCASTS.loadLogsByDate, onLoadLogsByDate);

	setDefaultPagination();
	onLoadLogsByDate();

	function onLoadLogsByDate() {
		vm.pagination.currentPage = 1;
		loadLogsByDate();
	}

	function loadLogsByDate() {
		vm.busy = true;
		vm.currentVeiw = DISPATCH_LOGS_DATE_VIEW;

		logsService.getLogsByDay($scope.selectedDate, vm.pagination.currentPage)
			.then(successGetLogs)
			.finally(() => vm.busy = false);
	}

	function successGetLogs({logs = [], current_page = 1, pages_count = 1, total_count = 0}) {
		vm.logs.length = 0;
		vm.logs.push(...logs);
		vm.pagination.currentPage = current_page;
		vm.pagination.pagesCount = pages_count;
		vm.pagination.totalCount = total_count;
	}

	function onLoadLogsByRequest(newValue) {
		if (!newValue) return;

		vm.pagination.currentPage = 1;
		loadLogsByRequest();
	}

	function loadLogsByRequest() {
		vm.currentVeiw = DISPATCH_LOGS_REQUEST_VIEW;
		vm.busy = true;

		logsService.getLogsByNid($scope.nid, vm.pagination.currentPage)
			.then(successGetLogs, errorGetLog)
			.finally(() => vm.busy = false);
	}

	function errorGetLog() {
		vm.logs = [];
		setDefaultPagination();
	}

	function openFullView() {
		vm.pagination.currentView = vm.currentVeiw;

		$uibModal.open({
			template: require('../../controllers/dispatch-log-full-view/full-view.template.html'),
			size: 'lg',
			controller: 'DispatchLogsFullViewController',
			controllerAs: 'vm',
			resolve: {
				logs: () => vm.logs,
				pagination: () => vm.pagination,
			}
		});
	}

	function reloadLogsList(event, data) {
		if (vm.currentVeiw === DISPATCH_LOGS_REQUEST_VIEW && $scope.nid == data) {
			onLoadLogsByRequestWithDebounce($scope.nid);
		}

		//now we don't need reload by date
	}

	function setDefaultPagination() {
		vm.pagination.isShow = false;
		vm.pagination.currentPage = 1;
		vm.pagination.pagesCount = 1;
		vm.pagination.totalCount = 0;
		vm.pagination.perPage = 20;
	}

	function updateCurrentViewRequest() {
		if (vm.currentVeiw == DISPATCH_LOGS_DATE_VIEW) {
			loadLogsByDate();
		} else {
			loadLogsByRequest();
		}
	}

	function showShortLog(index) {
		return vm.logs.length && (!_.get(vm.logs, `[${index}].data.fullLogs[0].signatureNumber`) ||
			_.get(vm.logs, `[${index}].data.fullLogs[0].signatureNumber`) == 1);
	}
}
