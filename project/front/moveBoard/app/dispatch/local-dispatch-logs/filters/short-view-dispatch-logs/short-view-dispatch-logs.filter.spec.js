describe('shortViewDispatchLog', () => {
	let filter;
	let log;

	beforeEach(() => {
		filter = $filter('shortViewDispatchLog');
		log = testHelper.loadJsonFile('local-dispatch__short-view-dispatch-log.mock');
	});

	it('should make log for usual request', () => {
		//given
		let expectedResult = 'Crew was assigned by Roman Halavach at 06/21/2018 on 2:48 pm';
		let result;

		//when
		result = filter(log);
		result = result.split('on');
		result[1] = ' 2:48 pm';
		result = result.join('on');

		//then
		expect(result).toEqual(expectedResult);
	});

	it('should make log for pickup request', () => {
		//given
		log.data.crewType = 1;
		let expectedResult = '(Pickup) Crew was assigned by Roman Halavach at 06/21/2018 on 2:48 pm';
		let result;

		//when
		result = filter(log);
		result = result.split('on');
		result[1] = ' 2:48 pm';
		result = result.join('on');

		//then
		expect(result).toEqual(expectedResult);
	});

	it('should return empty string if not valid log', () => {
		//given
		let expectedResult = '';
		let result;

		//when
		result = filter({});

		//then

		expect(result).toEqual(expectedResult);
	});
});