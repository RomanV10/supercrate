'use strict';

angular
	.module('app.dispatch')
	.filter('shortViewDispatchLog', shortViewDispatchLog);

/* @ngInject */
function shortViewDispatchLog(logsTextDictionary, logsViewTextIds, moment) {
	return function(log) {
		if (isNotValid(log)) return '';

		let result = '';
		let data = angular.fromJson(log.data);

		if (data.crewType) {
			result += logsTextDictionary.crewType[data.crewType];
		}

		if (data.logType == logsViewTextIds.logType.dispatchCrew) {
			result += logsTextDictionary.dispatchCrew[data.shortViewID];
		} else if (data.logType == logsViewTextIds.logType.contractPageSteps) {
			if (_.get(data, `fullLogs[0].signatureNumber`)) {
				result += `Step ${data.shortViewID} was ${_.get(data, 'fullLogs[0].signatureRemoved', false) ? 'declined' : 'completed'} on contract `;
			} else if (_.get(data, `fullLogs[0].method`)) {
				result += logsTextDictionary.contractPage.payments[data.shortViewID];
			} else if (_.get(data, `fullLogs[0].textId`)) {
				result += logsTextDictionary.contractPage.contractSubmit[data.shortViewID];
			}
		}

		let logMoment = moment.unix(Number(log.created));
		let date = logMoment.format('MM/DD/YYYY');
		let time = logMoment.format('hh:mm a');

		result += `by ${log.field_user_first_name_value} ${log.field_user_last_name_value} at ${date} on ${time}`;

		return result;
	};
}

function isNotValid(log) {
	return !log || _.isEmpty(log);
}
