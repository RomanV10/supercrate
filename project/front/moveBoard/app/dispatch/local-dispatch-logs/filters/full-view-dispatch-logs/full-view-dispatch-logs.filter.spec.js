describe('shortViewDispatchLog', () => {
	const DEFAULT_WORKER_NAME = 'John Dou';
	const DEFAULT_WORKER_NAME_NEW = 'John Dou Super';
	let filter;
	let log;

	describe('dispatch crew', () => {
		beforeEach(() => {
			filter = $filter('fullViewDispatchLog');
			log = {textId: 1, from: 0, to: 0, logType: 1};
		});

		it('should return empty string if not valid log', () => {
			//given
			let expectedResult = '';
			let result;

			//when
			result = filter({});

			//then

			expect(result).toEqual(expectedResult);
		});

		it('should return text when log exist', () => {
			//given
			let expectedResult = '';
			let result;

			//when
			result = filter({});

			//then

			expect(result).toEqual(expectedResult);
		});

		describe('should build log,', () => {
			it('when choose crew', () => {
				//given
				log.to = DEFAULT_WORKER_NAME;
				let expectedResult = 'Choose crew: John Dou';
				let result;

				//when
				result = filter(log);

				//then
				expect(result).toEqual(expectedResult);
			});

			it('when unassign crew', () => {
				//given
				log.from = DEFAULT_WORKER_NAME;
				log.textId = 12;
				let expectedResult = 'Unassign Foreman: John Dou';
				let result;

				//when
				result = filter(log);

				//then
				expect(result).toEqual(expectedResult);
			});

			it('when change crew', () => {
				//given
				log.from = DEFAULT_WORKER_NAME;
				log.to = DEFAULT_WORKER_NAME_NEW;
				log.textId = 9;
				let expectedResult = 'Change foreman: John Dou to John Dou Super';
				let result;

				//when
				result = filter(log);

				//then
				expect(result).toEqual(expectedResult);
			});
		});

		describe('when unassign additional crew log', () => {
			it('should write log about unassign additional crew', () => {
				//given
				log.from = 'Test helper';
				log.textId = 21;
				let expectedResult = 'Unassign additional crew: Test helper';
				let result;

				//when
				result = filter(log);

				//then
				expect(result).toEqual(expectedResult);
			});
		});

		describe('when for exist in log', () => {
			it('and additional crew should show for witch crew log', () => {
				//given
				log.to = 'Test helper';
				log.textId = 4;
				log.for = 2;
				let expectedResult = 'Add additional crew: for Crew #2 - Test helper';
				let result;

				//when
				result = filter(log);

				//then
				expect(result).toEqual(expectedResult);
			});

			it('and usual crew should show for what foreman change switcher', () => {
				//given
				log.from = 'John Dou';
				log.to = 'Test Foreman 2';
				log.textId = 15;
				log.for = 'Test Foreman';
				let expectedResult = 'Change foreman switcher: for Test Foreman - John Dou to Test Foreman 2';
				let result;

				//when
				result = filter(log);

				//then
				expect(result).toEqual(expectedResult);
			});
		});
	});
});