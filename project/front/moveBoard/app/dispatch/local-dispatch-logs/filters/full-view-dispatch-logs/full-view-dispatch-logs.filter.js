'use strict';

angular
	.module('app.dispatch')
	.filter('fullViewDispatchLog', fullViewDispatchLog);

/* @ngInject */
function fullViewDispatchLog(logsTextDictionary, logsViewTextIds) {
	return function(log) {
		if (isNotValid(log)) return '';

		let result = '';

		if (log.logType == logsViewTextIds.logType.dispatchCrew) {
			result += logsTextDictionary.dispatchCrew.actions[log.textId];

			if (log.for) {
				result += 'for ';

				if (isAdditionalCrewId(log.textId)) {
					result += `Crew #${log.for} `;
				} else {
					result += log.for + ' ';
				}

				result += '- ';
			}

			if (log.from) {
				result += log.from;
			}

			if (isChangeStateLog(log)) {
				result += ' to ';
			}

			if (log.to) {
				result += log.to;
			}
		}

		if (log.logType == logsViewTextIds.logType.contractPageSteps) {
			if (log.method) {
				if (log.amount) {
					result += `Amount: $${log.amount}`;
				} else if (log.receiptId){
					result += `Receipt id: ${log.receiptId}`
				}
			} else if (log.textId) {
				result += logsTextDictionary.contractPage.contractSubmit[log.textId];
			}
		}

		return result;
	};

	function isAdditionalCrewId(id) {
		let actions = logsViewTextIds.dispatchCrew.action;
		let additionalCrewIds = [
			actions.addAdditionalCrew,
			actions.unassignAdditionalCrew,
			actions.removeAdditionalCrew,
			actions.changeAdditionalCrew,
			actions.addAdditionalTruck,
			actions.changeAdditionalTruck,
			actions.unassignAdditionalTruck,
			actions.removeAdditionalTruck,
		];

		return ~additionalCrewIds.indexOf(id);
	}
}

function isNotValid(log) {
	return !log || _.isEmpty(log);
}

function isChangeStateLog(log) {
	return log.from && log.to;
}
