export const DISPATCH_LOGS_BROADCASTS = {
	reloadLogsListAfterCreatingLog: 'reload_request_dispatch_log',
	loadLogsByDate: 'load_logs_by_date',
};