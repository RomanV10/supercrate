'use strict';

angular
	.module('app.dispatch', [
		'dispatch-crews',
		'commonServices'
	])
	.run($templateCache => {});
