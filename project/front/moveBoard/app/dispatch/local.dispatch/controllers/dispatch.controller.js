import {DISPATCH_LEGEND} from '../../dispatch-legend.constant';
import {DISPATCH_REQUEST_PATH} from '../../constants/dispatch-request-path.constants';
import {DISPATCH_LOGS_BROADCASTS} from '../../local-dispatch-logs/constants/dispatch-logs-broadcasts.constants';
import './dispatch-controller.styl';
let uniqid = require('uniqid');

'use strict';
angular

	.module('app.dispatch')
	.controller('DispatchCtrl', DispatchCtrl);

/*@ngInject*/
function DispatchCtrl($scope, $state, $rootScope, $uibModal, RequestServices, DTOptionsBuilder,
	DispatchService, $q, logger, SweetAlert, datacontext, $interval, common, ForemanPaymentService,
	MomentWrapperService, $window, serviceTypeService, apiService, moveBoardApi, $sce, dispatchCrewService,
	dispatchLogsDispatcherService, dispatchLogsUsersService, logsViewTextIds, timeFormat, openRequestService) {

	$scope.showAdditionalActions = false;

	let vm = this;
	let importedFlags = angular.copy($rootScope.availableFlags);

	const FLAT_RATE_TYPE = 'flatRate';
	const TARGET_NAME = '_blank';
	const DEFAULT_FOREMAN_AVATAR = '/moveBoard/app/dispatch/src/foreman_anonymus.png';
	const TEAM_ASSIGNED = 'teamAssigned';
	const CONFIRMED_BY_CUSTOMER = 'ConfirmedbyCustomer';
	const DATE_FORMAT = 'MMM D, YYYY';
	const DATE_FORMAT_WORKLOAD = 'YYYY-MM-DD';
	const CONFIRMED_BY_CUSTOMER_FLAG_ID = _.get(importedFlags, `company_flags[${CONFIRMED_BY_CUSTOMER}]`);
	const TEAM_ASSIGNED_FLAG_ID = _.get(importedFlags, `company_flags[${TEAM_ASSIGNED}]`);

	vm.fieldData = datacontext.getFieldData();
	const SERVICE_TYPES = _.get(vm.fieldData, 'enums.request_service_types', {});
	vm.basicsettings = angular.fromJson(vm.fieldData.basicsettings);
	vm.contract_page = angular.fromJson(vm.fieldData.contract_page);
	vm.contractType = _.get(vm.fieldData, 'enums.contract_type');
	vm.legend = DISPATCH_LEGEND;
	vm.allowToCreateCrew = false;
	vm.lockedHelpers = [];

	angular.forEach(vm.legend, (legendItem, key) => {
		legendItem.name = serviceTypeService.getCurrentForLegend(key + 1, legendItem.name);
	});

	let contract = {};
	let currContract = {};
	let request_data = {};
	let crewsLabels = {
		local: ['baseCrew', 'additionalCrew'],
		flatRate: ['pickedUpCrew', 'deliveryCrew'],
		longDistance: ['baseCrew', 'additionalCrew'],
	};
	let intervalClear = null;
	const ROLES = {
		foreman: 'foreman',
		helper: 'helper',
		switcher: 'switcher'
	};
	let currentRequestLinkWatcher = false;

	vm.getByDate = getByDate;
	vm.hideCrewBox = hideCrewBox;
	vm.editRequest = editRequest;
	vm.assignTeam = assignTeam;
	vm.openContractInBlack = openContractInBlack;
	vm.showAdditionalActions = showAdditionalActions;
	vm.unAssignTeam = unAssignTeam;
	vm.confirmCustomer = confirmCustomer;
	vm.openSettingsModal = openSettingsModal;
	vm._removeFromCol = _removeFromCol;

	vm.addCrew = addCrew;
	vm.addWorker = addWorker;
	vm.changeWorker = changeWorker;
	vm.loggingAdditionalTruck = loggingAdditionalTruck;
	vm.openCrewModal = openCrewModal;

	vm.checkIsTrip = checkIsTrip;
	vm.pasteCrew = pasteCrew;
	vm.createCrewToPreset = createCrewToPreset;
	vm.setWorker = setWorker;
	vm.checkJobCompleted = checkJobCompleted;

	vm.moveDate = moment().format(DATE_FORMAT);
	vm.reqFilter = {
		type: '1' //scheduled jobs
	};

	vm.showheading = true;
	vm.users = {};
	vm.helpers = [];
	vm.formenTable = {};
	vm.crews = [];
	vm.chosenCrew = {};
	vm.dailyCrews = [];


	vm.data = {};
	vm.dataForLog = {};
	vm.logMsgs = [];
	vm.confirmedByCustomer = false;
	vm.canAddNewCrew = true;
	vm.contractStatus = null;
	vm.filteredReq = null;
	vm.inedxesRemoveCrews = [];
	vm.openMap = openMap;
	$scope.showCrewPreview = true;
	var geocoder = new google.maps.Geocoder();

	$scope.requestType = 'local';
	$scope.dtOptions = DTOptionsBuilder.newOptions()
		.withPaginationType('simple')
		.withDisplayLength(100)
		.withOption('aaSorting', [2, 'asc']);
	$scope.view = {
		grid: false,
		schedule: true,
		map: false
	};
	$scope.isLocalDispatch = true;


	$scope.markers = [];
	angular.extend($scope, {
		map: {
			center: {
				latitude: 42.3349940452867,
				longitude: -71.0353168884369
			},
			zoom: 11
		}
	});

	function openMap() {
		$scope.view.map = true;
		$scope.view.grid = false;
		$scope.view.schedule = false;
	}

	function getArrivyLink() {
		apiService.postData(moveBoardApi.arrivy.getLinkForDispatch)
			.then(res => {
				let link = _.head(res.data);
				$scope.arrivyLink = false;

				if (_.isString(link)) {
					$scope.baseArrivyLink = link;
					buildArrivyLinkForFrame();
				}
			}, () => {
				$scope.arrivyLink = false;
			});
	}

	function buildArrivyLinkForFrame() {
		if ($scope.baseArrivyLink) {
			$scope.arrivyLink = $sce.trustAsResourceUrl($scope.baseArrivyLink + '&date=' + vm.currentDate);
		}
	}
	getArrivyLink();

	function initMarkers() {
		_.forEach(vm.requests, request => {
			let address = `${request.adrfrom.value},${request.from}`;
			geocoder.geocode({'address': address}, function (results, status) {
				if (status == google.maps.GeocoderStatus.OK) {

					var lat = results[0].geometry.location.lat();
					var lon = results[0].geometry.location.lng();

					var marker = {
						id: Date.now(),
						coords: {
							latitude: lat,
							longitude: lon
						},
						window: {
							'title': results[0].formatted_address
						},
						options: {
							draggable: false,
							icon: 'content/img/gmap/house.png'
						},
					};

					$scope.markers.push(marker);
				}
			});

		});

		var address = vm.basicsettings.company_address;
		geocoder.geocode({'address': address}, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {

				var lat = results[0].geometry.location.lat();
				var lon = results[0].geometry.location.lng();

				var marker = {
					id: Date.now(),
					coords: {
						latitude: lat,
						longitude: lon
					},
					window: {
						title: results[0].formatted_address
					},
					options: {
						draggable: false,
						icon: 'content/img/gmap/high-school.png'
					}
				};

				$scope.map.center = {
					latitude: lat,
					longitude: lon
				};
				$scope.markers.push(marker);
			}
		});
	}


	$scope.errors = {
		helper: false,
		foreman: false
	};
	$scope.flatFeePaymentTypes = {
		hour: {
			id: 'hour',
			point: 'H'
		},
		money: {
			id: 'money',
			point: '$'
		},
		percent: {
			id: 'percent',
			point: '%'
		}
	};

	$scope.curRequest = null;
	$scope.$watch('vm.moveDate', function (newVal, oldVal) {
		vm.busy = true;
		let oldWeekNumber = moment(oldVal, DATE_FORMAT).week();
		let newWeekNumber = moment(newVal, DATE_FORMAT).week();
		if (oldWeekNumber != newWeekNumber) {
			getWorkloadForWeek(newWeekNumber);
		}
		common.$timeout(function () {
			if (!!newVal || !!oldVal) {
				vm.currentDate = moment(new Date(newVal)).format('dddd, MMMM DD, YYYY');
				getByDate();
				buildArrivyLinkForFrame();
			}
		}, 1000);
	});

	function getWorkloadForWeek(weekNumber) {
		let data = {
			date_from: moment().week(weekNumber).startOf('week').format(DATE_FORMAT_WORKLOAD),
			date_to: moment().week(weekNumber).endOf('week').format(DATE_FORMAT_WORKLOAD)
		};

		apiService.postData(moveBoardApi.dispatchCrews.workLoad, data)
			.then(res => {
				vm.workLoad = {};
				_.forEach(res.data.data, record => {
					vm.workLoad[record.uid] = parseFloatHours(record.work_time);
				});
			})
			.catch(err => {
				toastr.error('Error', 'Cannot load workload for week');
			});
	}

	function parseFloatHours(floatHours) {
		return timeFormat.parseFloatHours(floatHours);
	}

	getWorkloadForWeek(moment(vm.moveDate, DATE_FORMAT).week());

	$scope.$watch('vm.reqFilter.type', function (n, o) {
		vm.busy = true;
		common.$timeout(function () {
			if (!!n || !!o) {
				vm.filteredReq = _filterJobs();
				vm.busy = false;
			}
		}, 1000);
	});

	$scope.$on('request.updated', handlerUpdateRequest);

	function handlerUpdateRequest(event, data) {
		if (data.status.raw == 3) {
			var nid = data.nid;

			var indReq = _.findIndex(vm.requests, req => req && req.nid == nid);

			if (data.date.value == moment(vm.moveDate).format('MM/DD/YYYY') && indReq == -1) {
				vm.filteredReq.push(data);
			}

			if (indReq && indReq != -1 && data) {
				vm.filteredReq[indReq] = data;
				vm.requests[indReq] = data;
			} else {
				vm.requests.push(data);
			}
		}

		if (vm.clicked == data.nid) {
			let crewSizeNew = _.get(data, DISPATCH_REQUEST_PATH.INVOICE_CREW, data.crew.value);
			let crewSizeBase = _.get($scope.curRequest, DISPATCH_REQUEST_PATH.INVOICE_CREW, $scope.curRequest.crew.value);

			if (crewSizeBase != crewSizeNew) {
				$scope.curRequest = angular.copy(data);
				let crewSize = crewSizeNew;
				let crewType = crewsLabels[$scope.requestType][0];
				let allHelpers = vm.data[crewType].helpers.length;

				if (vm.data.additionalCrews) {
					_.forEach(vm.data.additionalCrews, (crew) => {
						allHelpers += crew.helpers.length;
					});
				}

				let neededHelpers = crewSize - allHelpers - 1;

				if (allHelpers < crewSize) {
					for (var i = 0; i < neededHelpers; i++) {
						if ($scope.requestType != FLAT_RATE_TYPE) {
							addWorker();
						}
					}
				} else {
					let count = 0;
					let msg = [];

					if ($scope.requestType == FLAT_RATE_TYPE) {
						let deliveryCrewCount = _.get(request, 'field_delivery_crew_size.value', 2);
						let deliveryCrewSize = _.get(data, DISPATCH_REQUEST_PATH.INVOICE_DELIVERY_CREW, deliveryCrewCount);

						_.forEach(crewsLabels.flatRate, function (type) {
							if (type == crewsLabels.flatRate[1]) {
								crewSize = deliveryCrewSize;
							}

							allHelpers = vm.data[type].helpers.length;
							neededHelpers = crewSize - allHelpers - 1;
							count = 0;

							_.forEachRight(vm.data[type].helpers, function (helper, helperIndex) {
								if (count == neededHelpers) {
									return false;
								}

								if (helper == '') {
									count--;
									vm.data[type].helpers.splice(helperIndex, 1);
								} else {
									count--;

									let index = vm.data.workersCollection.indexOf(helper);

									if (index > -1) {
										vm.data.workersCollection.splice(index, 1);
									}

									let name = dispatchLogsUsersService.getWorkerById(helper).name;
									msg.push(`Worker ${name} was removed from crew`);

									vm.data[type].helpers.splice(helperIndex, 1);
								}
							});
						});
					} else {
						let crewType = crewsLabels[$scope.requestType][0];

						if (vm.data.additionalCrews) {
							_.forEachRight(vm.data.additionalCrews, function (crew, crewIndex) {
								if (count == neededHelpers) {
									return false;
								}

								_.forEachRight(crew.helpers, function (helper, helperIndex) {
									if (count == neededHelpers) {
										return false;
									}

									if (helper == '') {
										count--;
										vm.data.additionalCrews[crewIndex].helpers.splice(helperIndex, 1);
									} else {
										count--;
										var workerIndex = vm.data.workersCollection.indexOf(helper);

										if (workerIndex > -1) {
											vm.data.workersCollection.splice(workerIndex, 1);
										}

										let name = dispatchLogsUsersService.getWorkerById(helper).name;
										msg.push(`Worker ${name} was removed from crew`);

										vm.data.additionalCrews[crewIndex].helpers.splice(helperIndex, 1);
									}

								});
							});
						}

						if (count != neededHelpers) {
							count = 0;
							_.forEachRight(vm.data[crewType].helpers, function (helper, helperIndex) {
								if (count == neededHelpers) {
									return false;
								}

								if (helper == '') {
									count--;
									vm.data[crewType].helpers.splice(helperIndex, 1);
								} else {
									count--;
									let workerIndex = vm.data.workersCollection.indexOf(helper);

									if (workerIndex > -1) {
										vm.data.workersCollection.splice(workerIndex, 1);
									}

									let name = dispatchLogsUsersService.getWorkerById(helper).name;

									msg.push(`Worker ${name} was removed from crew`);
									vm.data[crewType].helpers.splice(helperIndex, 1);
								}
							});
						}
					}

					let toastrMsg = '';

					_.forEach(msg, function (sentence) {
						toastrMsg += sentence + '. ';
					});

					if (toastrMsg != '') {
						toastr.warning(toastrMsg, 'Warning!');
					}

					assignTeam();
				}
			}
		}
	}

	function checkContractCrews() {
		var stepsData = {
			startTimeStep: 3,
			stopTimeStep: 4
		};
		var flatStepsData = {
			pickupStartTimeStep: 3,
			pickupStopTimeStep: 4,
			unloadStartTimeStep: 5,
			unloadStopTimeStep: 6
		};
		var flatMiles = false;
		if (_.get($scope, 'curRequest.request_all_data.flatMiles')) {
			flatMiles = $scope.curRequest.request_all_data.flatMiles;
		} else if ($scope.curRequest.distance) {
			if (!!vm.basicsettings.local_flat_miles
				&& parseFloat($scope.curRequest.distance.value) > parseFloat(vm.basicsettings.local_flat_miles)) {
				flatMiles = true;
			}
		}
		if (currContract) {
			if (currContract.factory) {
				if (!_.get($scope, 'curRequest.request_data.value')
					|| _.isEmpty($scope.curRequest.request_data.value)) {
					return;
				}

				var request_crews = angular.fromJson($scope.curRequest.request_data.value).crews;
				let isEmptyAdditionalCrew = !_.isUndefined(request_crews)
					&& !_.isUndefined(request_crews.additionalCrews) && !request_crews.additionalCrews.length;
				if (request_crews && isEmptyAdditionalCrew && currContract.factory.crews.length > 2) {
					var crewContract = currContract.factory.crews[0];
					currContract.factory.crews = [];
					currContract.factory.crews.push(crewContract);
				} else {
					_.forEach(vm.inedxesRemoveCrews, function (crew_ind) {
						//first crew it's base
						currContract.factory.crews.splice(crew_ind, 1);
						if (flatMiles) {
							_.forEachRight(flatStepsData, function (ind) {
								if (currContract.factory.signatures[ind + crew_ind * 4]) {
									delete currContract.factory.signatures[ind + crew_ind * 4];
									currContract.factory.signatures.length--;
									currContract.factory.stepSpace--;
								}
							});
						} else {
							_.forEachRight(stepsData, function (ind) {
								if (currContract.factory.signatures[ind + crew_ind * 2]) {
									delete currContract.factory.signatures[ind + crew_ind * 2];
									currContract.factory.signatures.length--;
									currContract.factory.stepSpace--;
								}
							});
						}
					});
				}
				DispatchService.setRequestContract(currContract, $scope.curRequest.nid);
				vm.inedxesRemoveCrews = [];
				vm.data.active = 0;
			}
		}
	}

	function _filterJobs() {
		let obj = [];

		angular.forEach(vm.requests, function (item, i) {
			let isCompleted = checkJobCompleted(item);

			if (isCompleted != vm.reqFilter.type && !_.find(obj, {nid: item.nid})) {
				obj.push(item);
			}
		});

		return obj;
	}

	function checkJobCompleted(job) {
		if (!job) return false;

		let isCompleted = vm.isFlagExist(job.field_company_flags, 'Completed');
		let isFlatRate = job.service_type.raw == 5 && !_.get(job, 'field_flat_rate_local_move.value');
		let isFlatRateFlags = vm.isFlagExist(job.field_company_flags, 'Pickup Completed') && vm.isFlagExist(job.field_company_flags, 'Delivery Completed');

		return isCompleted && (!isFlatRate || isFlatRateFlags);
	}

	_onInitAndReset();

	//** @Constructor
	(function () {
		DispatchService.getUsersByRole({
			active: 1,
			role: ['helper', 'foreman', 'driver']
		})
			.then(users => {
				vm.formenTable = users.foreman;
				_.forEach(vm.formenTable, function (foreman, uid) {
					vm.formenTable[uid].name = foreman.field_user_first_name + ' ' + foreman.field_user_last_name;
				});
				var foreman = [];
				_.forEach(users.foreman, function (worker) {
					foreman.push({
						name: worker.field_user_first_name + ' ' + worker.field_user_last_name,
						uid: worker.uid,
						mail: worker.settings.field_notification_mail || worker.mail,
						photoUrl: _.get(worker, 'user_picture.url', DEFAULT_FOREMAN_AVATAR)
					});
				});
				users.foreman = foreman;
				vm.users = users;
				vm.isSubmitted = {};
				angular.forEach(users.helper, function (helper, uid) {
					vm.helpers.push({
						name: helper.field_user_first_name + ' ' + helper.field_user_last_name,
						uid: uid,
						role: ' Helper' //not delete space between Helper , we need for ordering
					});
				});
				angular.forEach(users.foreman, function (helper, uid) {
					vm.helpers.push({
						name: helper.name,
						uid: helper.uid,
						role: 'Foreman'
					});
				});
				angular.forEach(users.driver, function (helper, uid) {
					vm.helpers.push({
						name: helper.field_user_first_name + ' ' + helper.field_user_last_name,
						uid: helper.uid,
						role: 'Driver'
					});
				});

				dispatchLogsUsersService.initDispatchWorkers(vm.users);
			}, reason => {
				logger.error(reason, reason, 'Error');
			});

		dispatchCrewService.getAllCrews()
			.then(resolve => {
				vm.crews = resolve;
				dispatchLogsUsersService.initDispatchCrews(resolve);
			});
	})();

	//Get Confirmed Requests By date
	function getByDate() {
		clearInterval();

		vm.busy = true;

		let date = MomentWrapperService.getZeroMoment(new Date(vm.moveDate));

		$scope.$broadcast(DISPATCH_LOGS_BROADCASTS.loadLogsByDate);

		RequestServices
			.getRequestParklot(date)
			.then(function (response) {
				vm.requests = _addDayParts(response.data);

				vm.requests = vm.requests.filter(function (item) {
					return (item.status.raw == 3 || (item.status.raw == 2 && item.field_reservation_received.value))
						? item
						: undefined;
				});
				getThisDayCrew();
				_onInitAndReset();
				$scope.$broadcast('parklot.init', vm.requests);
				initMarkers();
				vm.filteredReq = _filterJobs();

				$('.js-custom-scrollbar').mCustomScrollbar({
					theme: 'dark'
				});
			}, function (reason) {
				logger.error(reason, reason, 'Error');
			})
			.finally(function () {
				vm.busy = false;
				vm.clicked = false;
				$scope.curRequest = null;
			});
	}

	function getThisDayCrew() {
		let CREWS = [];
		_.forEach(vm.requests, request => {
			let crew = _.get(request, DISPATCH_REQUEST_PATH.CREWS, {});
			let pickupCrew = _.get(request, DISPATCH_REQUEST_PATH.PICK_UP_CREWS, {});
			let deliveryCrew = _.get(request, DISPATCH_REQUEST_PATH.DELIVERY_CREWS, {});
			let dailyCrew;

			if (crew.foreman && !_.isEmpty(crew.workersCollection)) {
				dailyCrew = angular.copy(crew);
				CREWS.push(dailyCrew);
			}

			if (pickupCrew.foreman && !_.isEmpty(pickupCrew.helpers)) {
				dailyCrew = angular.copy(pickupCrew);
				dailyCrew.workersCollection = angular.copy(pickupCrew.helpers);
				CREWS.push(dailyCrew);
			}

			if (deliveryCrew.foreman && !_.isEmpty(deliveryCrew.helpers)) {
				dailyCrew = angular.copy(deliveryCrew);
				dailyCrew.workersCollection = angular.copy(deliveryCrew.helpers);
				CREWS.push(dailyCrew);
			}
		});

		vm.dailyCrews = CREWS;
	}

	function checkContractStatus() {
		if (!vm.contractStatus) {
			vm.contractStatus = {};
		}

		if ($scope.curRequest && !!contract) {
			for (var i = 0, l = contract.crews.length; i < l; i++) {
				vm.contractStatus[i] = !contract.crews[i].workDuration;
			}
		}
	}

	function _addDayParts(requests) {
		var data = [];
		angular.forEach(requests, function (request, k) {
			if (request.start_time1.value) {
				request.start_time1.raw = common.convertTime(request.start_time1.value);
			}
			data.push(request);
		});

		return data;
	}

	function confirmCustomer() {
		if (!$scope.curRequest) {
			return;
		}

		if (_.isArray($scope.curRequest.field_company_flags.value) && _.isEmpty($scope.curRequest.field_company_flags.value)) {
			$scope.curRequest.field_company_flags.value = {};
		}

		var flags = _.keys($scope.curRequest.field_company_flags.value) || [], nid = $scope.curRequest.nid;
		var indReq = _.findIndex(vm.requests, req => req && req.nid == nid);

		if (vm.confirmedByCustomer) {
			flags.push(CONFIRMED_BY_CUSTOMER_FLAG_ID);
			vm.requests[indReq].field_company_flags.value[CONFIRMED_BY_CUSTOMER_FLAG_ID] = CONFIRMED_BY_CUSTOMER;
		} else {
			var ind = flags.indexOf(CONFIRMED_BY_CUSTOMER_FLAG_ID);
			if (ind > -1) {
				flags.splice(ind, 1);
			}
			delete vm.requests[indReq].field_company_flags.value[CONFIRMED_BY_CUSTOMER_FLAG_ID];
		}

		updateRequest(nid, {
			field_company_flags: flags
		});
		//Rollback
	}

	function openContractInBlack() {
		let contractURL = `/account/#/request/${$scope.curRequest.nid}/contract`;
		$window.open(contractURL, '_blank');
	}

	function showAdditionalActions() {
		$scope.showAdditionalActions = !$scope.showAdditionalActions;
	}

	function assignTeam() {
		let defer = $q.defer();
		let request = {};
		let nid = _.get($scope.curRequest, 'nid');
		let isFlatRate = $scope.requestType === FLAT_RATE_TYPE;

		request.field_foreman = isFlatRate
			? [
				vm.data.pickedUpCrew.foreman,
				vm.data.deliveryCrew.foreman
			]
			: [vm.data.foreman];
		request.field_helper = _getHelpersFromIterator();

		//@TODO: this is for old requests
		if ($scope.curRequest.hasOwnProperty('field_foreman_assign_time')) {
			request.field_foreman_assign_time = {
				value: moment().format('YYYY-MM-DD HH:mm:ss')
			};
		}

		if ($scope.curRequest.source) {
			request.field_company_flags = $scope.curRequest.source.field_company_flags
				? _.keys($scope.curRequest.source.field_company_flags.value)
				: [];
		} else {
			request.field_company_flags = $scope.curRequest.field_company_flags
				? _.keys($scope.curRequest.field_company_flags.value)
				: [];
		}

		if (request.field_company_flags.indexOf(TEAM_ASSIGNED_FLAG_ID) < 0) {
			request.field_company_flags.push(TEAM_ASSIGNED_FLAG_ID);
		}

		if (!_.isEmpty(vm.switchers)) {
			request_data.switchers = {};

			angular.forEach(vm.switchers, function (to, from) {
				if (to.length) {
					request_data.switchers[from] = to;
				}

				vm.data.workersCollection.push(to);
			});
		}

		request_data.crews = _removeDummyAddHelpers();

		request.request_data = {value: request_data};

		if (isFlatRate) {
			request.field_foreman_pickup = [request.request_data.value.crews.pickedUpCrew.foreman];
			request.field_foreman_delivery = [request.request_data.value.crews.deliveryCrew.foreman];
			request.field_helper_pickup = request.request_data.value.crews.pickedUpCrew.helpers;
			request.field_helper_delivery = request.request_data.value.crews.deliveryCrew.helpers;
		}

		//@validation
		$scope.errors.helper = request.field_helper.some((item) => !item.length);

		if ($scope.requestType == 'local' || $scope.requestType == 'longDistance') {
			$scope.errors.foreman = request.field_foreman.some(item =>	!item.length);
		} else {
			$scope.errors.foreman = !request.field_foreman[0].length;
		}

		let dataToSave = angular.copy(request);
		dataToSave.field_helper = vm.data.workersCollection;

		if ($scope.errors.foreman) {
			defer.resolve();
			return defer.promise;
		} else {
			vm.busy = true;

			DispatchService.assignTeam(nid, dataToSave)
				.then(() => {
					callbackAfterSendingCrew(nid, request, TEAM_ASSIGNED_FLAG_ID, defer);
				})
				.catch(() => {
					toastr.error('Job was not sent to crew!', 'Please, contact the maintaince');
					defer.reject();
				})
				.finally(() => vm.busy = false);

			checkContractCrews();
		}

		return defer.promise;
	}

	function callbackAfterSendingCrew(nid, request, flagId, defer) {
		RequestServices.sendLogs(vm.logMsgs, 'Request team was assigned', nid, 'MOVEREQUEST');

		vm.logMsgs = [];

		_.forEach(vm.requests, function (requestItem, ind) {
			if (requestItem.nid == nid) {
				if (angular.isUndefined(vm.requests[ind].field_helper)) {
					vm.requests[ind].field_helper = {};
				}
				vm.requests[ind].field_helper.value = request.field_helper;
				vm.requests[ind].field_foreman.value = _.head(request.field_foreman);
				if (_.isArray(vm.requests[ind].field_company_flags.value)
					&& _.isEmpty(vm.requests[ind].field_company_flags.value)) { //convert to object
					vm.requests[ind].field_company_flags.value = {};
				}
				vm.requests[ind].request_data.value = request_data;
				vm.requests[ind].field_company_flags.value[flagId] = TEAM_ASSIGNED;
				vm.crewBackup = _.cloneDeep(vm.requests[ind].request_data.value);

				if (!_.isUndefined($scope.curRequest.field_company_flags)) {
					if (_.isArray($scope.curRequest.field_company_flags.value)) {
						$scope.curRequest.field_company_flags.value = {};
					}

					$scope.curRequest.field_company_flags.value[flagId] = TEAM_ASSIGNED;
				}

				if (_.get($scope, 'curRequest.source.field_company_flags.value')) {
					if (_.isArray($scope.curRequest.source.field_company_flags.value)) {
						$scope.curRequest.source.field_company_flags.value = {};
					}

					$scope.curRequest.source.field_company_flags.value[flagId] = TEAM_ASSIGNED;
				}

				vm.isSubmitted[$scope.curRequest.nid] = true;
				toastr.success('Successfully Sent !', 'Job was Sent to the Crew.');
				$scope.$broadcast('parklot.init', vm.requests);
				$scope.$broadcast('showClicked', $scope.curRequest);
				//Cherry
				common.showAnimation(nid);

				dispatchLogsDispatcherService.assignTeamLogs(vm.requests[ind]);
				getThisDayCrew();
			}
		});

		if (defer) defer.resolve();
	}

	function unAssignTeam() {
		SweetAlert.swal({
			title: 'Confirm your action? ',
			text: 'Do you want to unassign the team?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes',
			cancelButtonText: 'Cancel',
			closeOnConfirm: true,
			closeOnCancel: true
		}, continueUnAssignTeam);
	}

	function continueUnAssignTeam(isConfirm) {
		if (!isConfirm) return;

		vm.busy = true;
		let unassignedForeman;
		let nid = $scope.curRequest.nid;
		let unassignedHelpers = _getHelpersFromIterator();
		let flagId = importedFlags.company_flags[TEAM_ASSIGNED];
		let flags = _.get($scope.curRequest, 'field_company_flags.value', {});

		if ($scope.requestType === FLAT_RATE_TYPE) {
			unassignedForeman = [
				_.get(vm.data, 'pickedUpCrew.foreman', 0),
				_.get(vm.data, 'deliveryCrew.foreman', 0)
			];
		} else {
			unassignedForeman = vm.data.foreman.length ? vm.data.foreman : 0;
		}

		let deprecatedFlag = _.has(flags, flagId);
		if (deprecatedFlag) delete $scope.curRequest.field_company_flags.value[flagId];

		if (!_.isEmpty(request_data.crews)) delete request_data.crews;
		if (!_.isEmpty(request_data.switchers)) delete request_data.switchers;
		if (!_.isEmpty(request_data.crew)) delete request_data.crew;

		if ($scope.curRequest.source) {
			$scope.curRequest.source.request_data.value = request_data;
		}

		if ($scope.curRequest.request_data) {
			$scope.curRequest.request_data.value = request_data;
		}


		let request = {
			request_data: {
				value: request_data
			},
			field_company_flags: _.keys(flags),
		};
		if (unassignedHelpers.length) {
			request.field_helper = {del: unassignedHelpers};
		}
		if (unassignedForeman.length) {
			request.field_foreman = {del: unassignedForeman};

			if ($scope.requestType === FLAT_RATE_TYPE) {
				request.field_foreman_pickup = {del: vm.data.pickedUpCrew.foreman};
				request.field_foreman_delivery = {del: vm.data.deliveryCrew.foreman};
			}
		}

		dispatchLogsDispatcherService.unassingTeamLogs($scope.curRequest);

		updateRequest(nid, request)
			.then(() => {
				vm.logMsgs = [{
					text: 'Team was unassigned on dispatch page.'
				}];
				RequestServices.sendLogs(vm.logMsgs, 'Request team was unassigned', nid, 'MOVEREQUEST');
				vm.logMsgs = [];

				if ($scope.curRequest.source) {
					$scope.curRequest.source.field_foreman.value = '';
					$scope.curRequest.source.field_helper.length = 0;
				}

				if (_.get($scope, 'curRequest.field_foreman')) {
					$scope.curRequest.field_foreman.value = '';
					$scope.curRequest.field_helper.length = 0;
				}

				vm.isSubmitted[$scope.curRequest.nid] = true;
				vm.getByDate();
				logger.success('Job was deleted from Team.');
				$scope.$broadcast('parklot.init', vm.requests);
				$scope.requestType = 'local';
			});
	}

	function updateRequest(nid, request) {
		let defer = $q.defer();

		DispatchService.updateRequest(nid, request)
			.then(function (response) {
				defer.resolve(response);
			}, function (reason) {
				defer.reject();
				logger.error(reason, reason, 'Error');
			})
			.finally(function () {
				vm.busy = false;
			});

		return defer.promise;
	}


	function openSettingsModal(index, data) {
		data = data || {};
		var activeCrew = {};

		data.crews = vm.data;

		if (angular.isDefined(index)) {
			activeCrew = vm.data.additionalCrews[index];
			data.page = 'simpleCrew';
			data.index = index;
			data.request = $scope.curRequest;
		} else {
			data.page = 'baseCrew';
			data.isAddHelpers = !!vm.data.additionalCrews ? vm.data.additionalCrews.length : 0;
			data.users = vm.helpers;
			data.paymentType = data.paymentType || request_data.paymentType || '';
			data.pricePerHelper = request_data.pricePerHelper;
			data.switchers = vm.switchers;
			data.selectedFlatFeeOption = request_data.selectedFlatFeeOption;
			data.flatFeeOptions = $scope.flatFeePaymentTypes;
		}

		let invoice;
		let modalInstance = $uibModal;

		_.forEach(vm.requests, function (requestItem, ind) {
			if (requestItem.nid == $scope.curRequest.nid) {
				$scope.reqIndex = ind;
				modalInstance = $uibModal.open({
					template: require('../settingsModal.html'),
					size: 'md',
					controller: 'dispatchSettingsModalCtrl',
					resolve: {
						data: () => data,
						request: () => {
							return RequestServices.getSuperRequest($scope.curRequest.nid)
								.then(({request}) => {
									invoice = request.request_all_data.invoice;
									return request;
								})
								.catch((err) => toastr.warning('Error of getting request data ', 'Warning!'));
						},
						settings: () => vm.basicsettings,
						contract_page: () => vm.contract_page,
						foreman: () => vm.data.foreman,
						enums: () => vm.contractType
					}
				});
			}
		});


		modalInstance.result.then(function (result) {
			vm.isSubmitted[$scope.curRequest.nid] = false;

			if (data.page == 'simpleCrew') {
				_saveAddCrewSettings(result);
			} else {
				_saveBaseCrewSettings(result);
			}

			let request = {
				request_data: {value: request_data}
			};

			updateRequest($scope.curRequest.nid, request)
				.then(() => {
					vm.requests[$scope.reqIndex].request_data.value = request_data;
					$scope.curRequest.request_data.value = request_data;
				});

			$scope.curRequest.request_all_data.flatMiles = result.flatMiles;
			vm.requests[$scope.reqIndex].request_all_data.flatMiles = result.flatMiles;
			$scope.curRequest.request_all_data.useInventory = result.useInventory;
			vm.requests[$scope.reqIndex].request_all_data.useInventory = result.useInventory;

			if (result.credit) {
				$scope.curRequest.request_all_data.credit = result.credit;
				vm.requests[$scope.reqIndex].request_all_data.credit = result.credit;
			}

			if (invoice) {
				vm.requests[$scope.reqIndex].request_all_data.invoice = invoice;
			}

			RequestServices.saveReqData($scope.curRequest.nid, vm.requests[$scope.reqIndex].request_all_data);
		});

		function _saveAddCrewSettings(result) {
			if (result.hasOwnProperty('rate')) {
				activeCrew.rate = result.rate.value || 0;
			}

			if (result.rateSaveManually) {
				activeCrew.rateSaveManually = true;
			}
		}

		function _saveBaseCrewSettings(result) {
			if (result.unpaids) {
				var baseCrew = vm.data.baseCrew;
				baseCrew.unpaidHelpers = [];

				angular.forEach(result.unpaids, function (status, uid) {
					if (status) {
						baseCrew.unpaidHelpers.push(uid);
					} else {
						var i = baseCrew.unpaidHelpers.indexOf(uid);
						if (i > -1) {
							baseCrew.unpaidHelpers.splice(i, 1);
						}
					}
				});
				vm.canAddNewCrew = true;

				if (!!baseCrew.unpaidHelpers) {
					vm.canAddNewCrew = !baseCrew.unpaidHelpers.length;
				}
			}

			if (result.paymentType) {
				request_data.paymentType = result.paymentType.alias;
			}

			if (!_.isEmpty(result.pricePerHelper)) {
				request_data.pricePerHelper = result.pricePerHelper;
			}

			if (!_.isEmpty(result.selectedFlatFeeOption)) {
				request_data.selectedFlatFeeOption = result.selectedFlatFeeOption;
			}
		}
	}


	//*** Crews
	function makeOpenDispatchUrl(branchUrl) {
		return `${branchUrl}/moveBoard/#/dispatch/local`;
	}

	function hideCrewBox() {
		vm.crewBoxHidden = true;
	}

	function goToTrip(id) {
		let tab = 0;
		$state.go('lddispatch.tripDetails', {id, tab});
	}

	function openTrip(request) {
		hideCrewBox();

		if (request && request.branch) {
			SweetAlert.swal({
				title: `This request is from branch ${request.branch.name}. Do you want to open that branch?`,
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: 'No',
				closeOnConfirm: false,
				closeOnCancel: true
			}, function (isConfirm) {
				if (isConfirm) {
					let url = makeOpenDispatchUrl(request.branch.url);

					$window.open(url, TARGET_NAME);
				} else {
					vm.clicked = '';
				}
			});
		} else {
			if (vm.clicked == request.nid) {
				goToTrip(request.nid);
			} else {
				vm.clicked = request.nid;
				$scope.curRequest = angular.copy(request);
			}
		}
	}

	function checkIsTrip(request) {
		if (request.ld_trip) {
			openTrip(request);
		} else {
			editRequest(request.nid);
		}
	}

	function getRequestCrew(dataValue) {
		let request_data = angular.fromJson(dataValue);

		if (request_data.hasOwnProperty('crews')) {
			setUpAdditionalCrewsId(request_data);
			vm.data = request_data.crews;
			vm.dataForLog = _.clone(request_data.crews);
			vm.canAddNewCrew = request_data.crews.baseCrew && (!request_data.crews.baseCrew.unpaidHelpers || !request_data.crews.baseCrew.unpaidHelpers.length);
		}

		return request_data;
	}

	function setUpAdditionalCrewsId(request_data) {
		if (_.get(request_data, 'crews.additionalCrews')) {
			request_data.crews.additionalCrews.forEach((crew) => {
				if (!crew.id) {
					crew.id = uniqid();
				}
			});
		}
	}

	function checkForSended(callback) {
		if (vm.clicked && !vm.isSubmitted[vm.clicked]) {
			SweetAlert.swal({
				title: 'You didn\'t assign current crew. Do you want to assign?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Assign',
				cancelButtonText: 'Cancel',
			}, isConfirm => {
				if (isConfirm) {
					vm.assignTeam()
						.finally(callback);
				} else {
					vm.currentRequestLink.request_data.value = _.cloneDeep(vm.crewBackup);
					callback();
				}
			});
		} else {
			callback();
		}
	}

	function editRequest(request, isDelivery) {
		vm.crewBoxHidden = false;

		if (!angular.isObject(request) && angular.isString(request)) {
			let indReq = _.findIndex(vm.requests, req => req && req.nid == request);

			request = vm.requests[indReq];

			if (request && request.branch) {
				SweetAlert.swal({
					title: `This request is from branch ${request.branch.name}. Do you want to open that branch?`,
					text: '',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#DD6B55',
					confirmButtonText: 'Yes',
					cancelButtonText: 'No',
					closeOnConfirm: false,
					closeOnCancel: true
				}, function (isConfirm) {
					if (isConfirm) {
						let url = makeOpenDispatchUrl(request.branch.url);

						$window.open(url, TARGET_NAME);
					}
				});

				return;
			}
		}

		vm.isSubmitted[request.nid] = true;

		if (vm.clicked == request.nid) {
			vm.busy = true;

			openRequestService.open(request.nid)
				.finally(function () {
					vm.busy = false;
				});
		} else {
			checkForSended(() => {
				vm.clicked = request.nid;
				let crew = _.get(request, 'request_data.value', {});
				vm.crewBackup = _.cloneDeep(crew);
				vm.currentRequestLink = request;
				$scope.curRequest = angular.copy(request);
				vm.inedxesRemoveCrews = [];

				switch (+request.service_type.raw) {
				case SERVICE_TYPES.FLAT_RATE:
					$scope.requestType = FLAT_RATE_TYPE;
					break;
				case SERVICE_TYPES.LONG_DISTANCE:
					$scope.requestType = 'longDistance';
					break;
				default:
					$scope.requestType = 'local';
				}

				_onInitAndReset();
				clearInterval();

				request_data = {};

				if (_.isEmpty(request.request_data)) {
					request.request_data = {value: {}};
				}

				//Touch with contract status
				var _getContract = function () {
					DispatchService.getRequestContract(request.nid)
						.then(function (data) {
							currContract = data;
							contract = data.factory;
							checkContractStatus();
						});
				};
				_getContract();

				intervalClear = $interval(function () {
					_getContract();
				}, 20000);

				vm.canAddNewCrew = true;

				if (!_.isEmpty(request.request_data.value)) {
					request_data = getRequestCrew(request.request_data.value);
				} else if (!_.isUndefined(request.source) && !_.isEmpty(request.source.request_data.value)) {
					request_data = getRequestCrew(request.source.request_data.value);
				}

				let crewSize = _.get(request, DISPATCH_REQUEST_PATH.INVOICE_CREW, request.crew.value);
				let crewType = crewsLabels[$scope.requestType][0];
				setUpEmptyWorkers(crewType, crewSize);

				if ($scope.requestType === FLAT_RATE_TYPE) {
					let deliveryCrewCount = _.get(request, 'field_delivery_crew_size.value', 2);
					crewSize = _.get(request, DISPATCH_REQUEST_PATH.INVOICE_DELIVERY_CREW, deliveryCrewCount);
					crewType = crewsLabels[$scope.requestType][1];

					setUpEmptyWorkers(crewType, crewSize);
				}

				setUpCurrentCrew(request);

				vm.switchers = request_data.switchers || {};
				vm.switchersForLog = request_data.switchers || {};

				if (!_.isUndefined(request.field_company_flags)) {
					vm.confirmedByCustomer = request.field_company_flags.value.hasOwnProperty(importedFlags.company_flags[CONFIRMED_BY_CUSTOMER]);
				}

				vm.data.active = 0;
				vm.isHideContractButtonLongDistance = !_.find(vm.contract_page.additionalContract, {mainContract: true}) && vm.data.type === 'longDistance';

				dispatchLogsDispatcherService.initRequestLog($scope.curRequest, logsViewTextIds.dispatchCrew.assignCrew, logsViewTextIds.logType.dispatchCrew);
				$scope.$broadcast('showClicked', $scope.curRequest);
			});
		}
	}

	function setUpCurrentCrew(request) {
		if ($scope.requestType === FLAT_RATE_TYPE) {
			vm.data.pickedUpCrew.crew = _.get(request, DISPATCH_REQUEST_PATH.PICK_UP_AUTO_ASSIGN_CREW, '');
			vm.data.deliveryCrew.crew = _.get(request, DISPATCH_REQUEST_PATH.DELIVERY_AUTO_ASSIGN_CREW, '');
		} else {
			vm.data.crew = _.get(request, DISPATCH_REQUEST_PATH.AUTO_ASSIGN_CREW, '');
		}
	}

	function setUpEmptyWorkers(crewType, crewSize) {
		let helpers = _.get(vm.data, `[${crewType}].helpers`, []);

		if (helpers.length < crewSize - 1) {
			let neededHelpers = crewSize - helpers.length - 1;

			for (let i = 0; i < neededHelpers; i++) {
				addWorker(undefined, crewType);
			}
		}
	}

	vm.isLockedHelper = helper => {
		return ~_.indexOf(vm.lockedHelpers, helper.uid);
	};

	function watchDisabledWorkers() {
		if (dispatchCrewService.IS_CREW_ALWAYS_AVAILABLE) return;

		_.forEach(vm.users.foreman, removeDisabled);
		_.forEach(vm.crews, removeDisabled);

		if (currentRequestLinkWatcher) {
			currentRequestLinkWatcher();
		}

		currentRequestLinkWatcher = $scope.$watch('vm.currentRequestLink', current => {
			vm.lockedHelpers = dispatchCrewService.getLockedForemen(current, vm.requests, $scope.currentDay);
			let lockedHelpers = dispatchCrewService.getLockedHelpers(current, vm.requests, $scope.currentDay);
			vm.lockedHelpers.push(...lockedHelpers);
			dispatchCrewService.lockBusyForemen(vm.lockedHelpers, vm.users.foreman);
			dispatchCrewService.updateLockedCrews(vm.crews, vm.lockedHelpers);
		}, true);
	}

	function _onInitAndReset() {
		let request = _.get($scope.curRequest, 'source', $scope.curRequest);
		let requestCrew = _.get(request, 'crew.value', 2);
		let helpersCount = _.get(request, DISPATCH_REQUEST_PATH.INVOICE_CREW, requestCrew) - 1;
		let emptyHelpersArray = getEmptyHelpersArray(helpersCount);

		vm.data = {
			crew: _.get(vm.data, 'crew', ''),
			workersCollection: [],
			type: $scope.requestType,
			active: 0
		};

		if ($scope.requestType == FLAT_RATE_TYPE) {
			vm.data.pickedUpCrew = {
				crew: '',
				name: 'Pick up crew',
				foreman: '',
				helpers: angular.copy(emptyHelpersArray)
			};

			let deliveryCrewCount = _.get(request, 'field_delivery_crew_size.value', 2);
			let deliveryHelpersCount = _.get(request, DISPATCH_REQUEST_PATH.INVOICE_DELIVERY_CREW, deliveryCrewCount) - 1;
			emptyHelpersArray = getEmptyHelpersArray(deliveryHelpersCount);

			vm.data.deliveryCrew = {
				crew: '',
				name: 'Delivery crew',
				foreman: '',
				helpers: angular.copy(emptyHelpersArray)
			};
		} else {
			vm.data.baseCrew = {
				name: 'Crew',
				rate: 0,
				helpers: angular.copy(emptyHelpersArray)
			};
			vm.data.additionalCrews = [];
			vm.data.foreman = '';
		}

		vm.switchers = {};
		vm.switchersForLog = {};
		vm.lockedHelpers = [];
		vm.dataForLog = _.clone(vm.data);
		$scope.$watch('vm.data', proposeToCreateCrew, true);
		watchDisabledWorkers();
	}

	function getEmptyHelpersArray(count) {
		let result = [];

		for (let i = 0; i < count; i++) {
			result.push('');
		}

		return result;
	}

	function removeDisabled(item) {
		if (item.disabled) delete item.disabled;
	}

	function _getHelpersFromIterator() {
		let crewType = _.get(crewsLabels, `[${$scope.requestType}]`);
		if (!crewType) throw new Error();
		let originalHelpers = _.get(vm.data, `[${_.head(crewType)}].helpers`, []);
		let originalAddHelpers = _.get(vm.data, `[${crewType[1]}].helpers`, []);

		let helpers = angular.copy(originalHelpers);
		let additionalHelpers = angular.copy(originalAddHelpers);

		if (!_.isEmpty(additionalHelpers)
			&& _.head(additionalHelpers).length) helpers.push(...additionalHelpers);

		return helpers;
	}

	function _removeDummyAddHelpers() {
		let tmpArr = _.clone(vm.data);

		if ($scope.requestType == 'local' || $scope.requestType == 'longDistance') {
			_.forEachRight(tmpArr.additionalCrews, function (crew, i) {
				_.forEachRight(crew.helpers, function (helper, ii) {
					if (!helper.length) {
						tmpArr.additionalCrews[i].helpers.splice(ii, 1);
					}
				});

				if (!crew.helpers.length) {
					vm.data.active = vm.data.active - 1;
					vm.inedxesRemoveCrews.push(i + 1);
					tmpArr.additionalCrews.splice(i, 1);
				}
			});

			if (tmpArr.deliveryCrew) {
				tmpArr.deliveryCrew.crew = '';
				tmpArr.deliveryCrew.foreman = '';

				if (tmpArr.deliveryCrew.helpers) {
					tmpArr.deliveryCrew.helpers.length = 0;
				}
			}

			if (tmpArr.pickedUpCrew) {
				tmpArr.pickedUpCrew.crew = '';
				tmpArr.pickedUpCrew.foreman = '';

				if (tmpArr.pickedUpCrew.helpers) {
					tmpArr.pickedUpCrew.helpers.length = 0;
				}
			}
		} else {
			tmpArr.crew = '';
			tmpArr.foreman = '';

			if (tmpArr.baseCrew && tmpArr.baseCrew.helpers) {
				tmpArr.baseCrew.helpers.length = 0;
			}
		}

		return tmpArr;
	}

	function _removeFromCol(uid) {
		let copyCollection = _.uniq(vm.data.workersCollection);
		vm.data.workersCollection.length = 0;
		vm.data.workersCollection.push(...copyCollection);

		var i = vm.data.workersCollection.indexOf(uid);

		if (i > -1) {
			vm.data.workersCollection.splice(i, 1);

			if (!_.isEmpty(vm.switchers)) {
				vm.switchers.hasOwnProperty(uid) && delete vm.switchers[uid];
			}
		}
	}

	function setWorker (newWorker, oldWorker, crewType) {
		if (!newWorker.length) return;

		let oldWorkerExists = _.get(oldWorker, 'length');
		let currentWorker = dispatchLogsUsersService.getWorkerById(newWorker);
		let originalWorker = dispatchLogsUsersService.getWorkerById(oldWorker);
		let msg = {
			text: 'Helper was set on dispatch page.'
		};

		if (oldWorkerExists) {
			msg.text = 'Helper was changed on dispatch page.';
			msg.from = originalWorker.name;
			_removeFromCol(oldWorker);
		}

		setCrewTypeInMessageText(msg, crewType);

		let crewInfo = {
			crewType,
			currendDate: moment(vm.moveDate, DATE_FORMAT),
		};

		dispatchCrewService.checkWorkerAvailable(currentWorker, vm.currentRequestLink, crewInfo)
			.then(() => {
				if (!~_.indexOf(vm.data.workersCollection, newWorker)) vm.data.workersCollection.push(newWorker);
				msg.to = currentWorker.name;

				vm.logMsgs.push(msg);
				$scope.errors.helper = false;
				vm.isSubmitted[$scope.curRequest.nid] = false;
			})
			.catch(() => {
				_onInitAndReset();
				toastr.error('Please choose another worker or change the time of the job', 'This worker is unavailable for this time.');
			});
	}

	function addWorker(isAdditional, crewType = crewsLabels.local[0]) {
		var active;

		if (!!isAdditional) {
			active = vm.data.active - 1; //active tab, except base crew tab

			if (!vm.data.additionalCrews[active]) {
				vm.data.additionalCrews[active] = {helpers: []};
			}

			vm.data.additionalCrews[active].helpers.push('');
		} else {
			if (!vm.data[crewType]) {
				vm.data[crewType] = {helpers: []};
			}

			vm.data[crewType].helpers.push('');
		}
	}

	vm.deleteWorker = function (isAdditional) {
		var arr = [];

		if (isAdditional) {
			var active = vm.data.active - 1;
			arr = vm.data.additionalCrews[active].helpers;
		} else {
			arr = vm.data.baseCrew.helpers;
		}

		var crewType = crewsLabels[$scope.requestType][0];
		var allHelpers = vm.data[crewType].helpers.length;

		if (vm.data.additionalCrews) {
			_.forEach(vm.data.additionalCrews, function (crew) {
				allHelpers += crew.helpers.length;
			});
		}

		let lastUid = _.last(arr);
		let worker = dispatchLogsUsersService.getWorkerById(lastUid);
		let role = 'Helper ';

		if (worker.isForeman) {
			role = 'Foreman ';
		}

		let crewSize = _.get($scope.curRequest, DISPATCH_REQUEST_PATH.INVOICE_CREW, $scope.curRequest.crew.value);

		if (crewSize - 1 == allHelpers) {
			toastr.warning('Crew size is ' + crewSize, 'Warning!');
			return false;
		}

		if (worker.name) {
			let msg = {
				text: `${role} "${worker.name}" was removed on dispatch page.`
			};

			setCrewTypeInMessageText(msg, crewType);

			vm.logMsgs.push(msg);
		}

		arr.splice(-1, 1);

		if (lastUid != '') {
			_removeFromCol(lastUid);
		}
	};

	function setCrewTypeInMessageText(message, crewType) {
		if (crewType && crewType != crewsLabels.local[0]) {
			let crewTitle = 'Delivery crew';

			if (crewType == crewsLabels.flatRate[0]) {
				crewTitle = 'Pick up crew';
			}

			message.text += ` (${crewTitle})`;
		}
	}

	function addCrew() {
		let lastIndex = vm.data.additionalCrews.push({
			name: 'Crew',
			rate: 0,
			helpers: [''],
			id: uniqid(), // need for correct works log
		});

		vm.data.active += 1;
		vm.isSubmitted[$scope.curRequest.nid] = false;
		vm.contractStatus[lastIndex] = true;

		let msg = {
			text: `Crew #${vm.data.active + 1} was added on dispatch page.`
		};

		vm.logMsgs.push(msg);
	}

	vm.removeCrew = function (index) {
		var innerHelpers = _.clone(vm.data.additionalCrews[index].helpers);
		vm.inedxesRemoveCrews.push(index + 1);
		vm.data.additionalCrews.splice(index, 1);
		vm.data.active = 0;
		vm.isSubmitted[$scope.curRequest.nid] = false;
		delete vm.contractStatus[index + 1];
		var i;

		while (i = innerHelpers.pop()) {
			_removeFromCol(i);
		}

		let msg = {
			text: `Crew #${index + 2} was removed on dispatch page.`
		};

		vm.logMsgs.push(msg);
	};

	vm.toggleSwitcher = function (uid) {
		if (vm.switchers.hasOwnProperty(uid)) {
			vm.removeSwitcher(uid);
			delete vm.switchersForLog[uid];
		} else {
			vm.switchers[uid] = 0;
			vm.switchersForLog[uid] = 0;
		}

		vm.isSubmitted[$scope.curRequest.nid] = false;
	};

	vm.isExistSwitchers = function (uid, reverse) {
		if (_.isEmpty(vm.switchers)) {
			return false;
		}

		if (reverse) {
			return _.indexOf(_.values(vm.switchers), uid) > -1;
		}

		return _.has(vm.switchers, uid);
	};

	vm.removeSwitcher = function (uid) {
		delete vm.switchers[uid];
		_.remove(vm.data.workersCollection, currentNID => currentNID === uid);
		vm.isSubmitted[$scope.curRequest.nid] = false;
	};

	vm.isSelected = function (uid_r, reverse) {
		if ($scope.curRequest) {
			let isAddedHelper = vm.data.workersCollection.indexOf(uid_r) > -1;
			let isCurrentForeman = vm.data.foreman == uid_r;
			let isAddedAsSwitcher = vm.isExistSwitchers(uid_r, reverse);
			let additionalCrews = _.get(vm, 'data.additionalCrews', []);
			let additionalHelpers = [];
			additionalCrews.forEach(crew => additionalHelpers.push(...crew.helpers));
			let isAddedAsAdditionalHelper = additionalHelpers.indexOf(uid_r) >= 0;

			let currentCrewLabel = crewsLabels[$scope.requestType][0];
			let currentCrew = vm.data[currentCrewLabel];
			let isAddedToFlatRate = currentCrew && _.indexOf(currentCrew.helpers, uid_r) > -1;

			return isAddedHelper || isCurrentForeman || isAddedAsSwitcher || isAddedToFlatRate || isAddedAsAdditionalHelper;
		}

		return false;
	};

	vm.availableWorkers = function () {
		let availableWorkers = vm.helpers.length;
		let selectedWorkers = vm.data.workersCollection.length + Object.keys(vm.switchers).length;

		return availableWorkers - selectedWorkers <= 0;
	};

	vm.isFlagExist = function (obj, flag) {
		if (angular.isDefined(obj)) {
			return !!obj && obj.value.hasOwnProperty(importedFlags.company_flags[flag]);
		} else {
			return '';
		}
	};

	function getHelperFullName(value) {
		let firstName = _.get(vm.users, `helper[${value}].field_user_first_name`);
		let lastName = _.get(vm.users, `helper[${value}].field_user_last_name`);
		if (firstName && lastName) return `${firstName} ${lastName}`;
	}

	function removeForemanCredit(msg, oldUID, creditID) {
		SweetAlert.swal({
			title: `Credit was apply to another ${msg.to}. Do you want to delete it ?`,
			text: '',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes',
			cancelButtonText: 'Cancel',
			closeOnConfirm: false,
			closeOnCancel: true
		}, isConfirm => {
			if (!isConfirm) return vm.data.foreman = oldUID;

			vm.busy = true;
			ForemanPaymentService.removeCustomPayroll(creditID)
				.then(() => {
					delete $scope.curRequest.request_all_data.credit;
					delete vm.requests[$scope.reqIndex].request_all_data.credit;
					RequestServices.saveReqData($scope.curRequest.nid, vm.requests[$scope.reqIndex].request_all_data);
					vm.busy = false;
					toastr.success('Deleted!');
				});
		});
	}

	function prepareSwitchers(type, foreman, UID) {
		let name;
		if (type === 'foreman') {
			name = _.get(foreman, 'name');
		} else if (type === 'helper') {
			if (foreman) {
				name = _.get(foreman, 'name');
			} else {
				name = getHelperFullName(UID);
			}
		}
		return name;
	}

	function changeWorker(field, value, type) {
		if (_.isEmpty(value)) return;

		let data = 'dataForLog';
		let msg = {};
		let role = field.charAt(0).toUpperCase() + field.slice(1);
		let oldUID = _.get(vm, `[${data}][${field}]`);
		let chosenForeman = _.find(vm.users.foreman, {uid: value});
		let oldForeman = _.find(vm.users.foreman, {uid: oldUID});
		let name;
		let oldName;
		let creditID = _.get($scope.curRequest, 'request_all_data.credit.id');

		switch (field) {
		case ROLES.switcher:
			data = 'switchersForLog';
			oldUID = _.get(vm, `[${data}][${field}]`);
			name = prepareSwitchers(type, chosenForeman, value);
			let baseWorker = '';
			role += ` for ${baseWorker}`;
			break;
		case ROLES.foreman:
			name = _.get(chosenForeman, 'name');
			let crewInfo = {
				crewType: type,
				currendDate: moment(vm.moveDate, DATE_FORMAT),
			};

			dispatchCrewService.checkWorkerAvailable(chosenForeman, vm.currentRequestLink, crewInfo)
				.then(optimisticResponse, onReject);
			break;
		case ROLES.helper:
			name = getHelperFullName(value);
			break;
		}

		if (_.isEmpty(oldUID)) {
			msg = {
				text: `${role} was set on dispatch page.`,
				to: name
			};
		} else {
			switch (field) {
			case ROLES.switcher:
				oldName = prepareSwitchers(type, oldForeman, oldUID);
				data = 'switchersForLog';
				oldUID = _.get(vm, `[${data}][${field}]`);
				role += ` ${type}`;
				break;
			case ROLES.foreman:
				oldName = _.get(oldForeman, 'name');
				break;
			case ROLES.helper:
				oldName = getHelperFullName(oldUID);
				break;
			}

			msg = {
				text: role + ' was changed on dispatch page.',
				to: name,
				from: oldName
			};
		}

		setCrewTypeInMessageText(msg, type);

		if (field === ROLES.foreman && creditID) removeForemanCredit(msg, oldUID, creditID);

		vm[data][field] = value;
		vm.logMsgs.push(msg);

		function optimisticResponse() {
			pasteDailyCrew(chosenForeman, type);
			vm.isSubmitted[$scope.curRequest.nid] = false;
		}

		function onReject() {
			_onInitAndReset();
			toastr.error('Please choose another worker or change the time of the job', 'This worker is unavailable for this time.');
		}
	}

	function pasteDailyCrew(foreman = {}, type = crewsLabels.local[0]) {
		let crew = _.find(vm.dailyCrews, {foreman: foreman.uid});
		if (!crew) return;

		preAssignCrew(crew, vm.currentRequestLink, optimisticResponse, onBadResponse);

		function optimisticResponse() {
			vm.data.workersCollection = [];
			vm.data.foreman = angular.copy(crew.foreman);
			let maxHelpers = _.get(vm.data, `[${type}].helpers.length`);

			for (let key = 0; key < maxHelpers; key++) {
				let worker = angular.copy(crew.workersCollection[key]);
				_.set(vm.data, `[${type}].helpers[${key}]`, worker);
				vm.data.workersCollection.push(worker);
				let name = dispatchLogsUsersService.getWorkersNameByIds([worker]);
				let msg = {
					text: 'Helper was set on dispatch page.',
					to: name
				};
				setCrewTypeInMessageText(msg, type);
				vm.logMsgs.push(msg);
			}

			vm.allowToCreateCrew = false;
		}

		function onBadResponse() {
			vm.allowToCreateCrew = false;
		}
	}

	function loggingAdditionalTruck(value) {
		let msg = {
			text: 'Additional trucks was set on dispatch page.',
			to: `${value} trucks`
		};
		vm.logMsgs.push(msg);
	}

	$scope.$on('$destroy', clearInterval);

	function clearInterval() {
		!!intervalClear && $interval.cancel(intervalClear);
	}

	function openCrewModal() {
		let CrewModalInstance = $uibModal.open({
			template: require('../../dispatch-crews/crew-modal/crew-modal.tpl.html'),
			size: 'lg',
			controller: 'CrewModalController',
			resolve: {
				crews: () => vm.crews,
				availableHelpers: () => vm.helpers,
				availableForemen: () => vm.formenTable,
			},
			backdrop: false,
		});

		CrewModalInstance.result.then(({crews}) => {
			vm.crews = crews;
			dispatchLogsUsersService.initDispatchCrews(crews);
		});
	}

	function proposeToCreateCrew(current, original) {
		let isChanged = !_.isEqual(current, original);

		if (isChanged && current.foreman && !_.isEmpty(current.workersCollection) && !_.get(current, 'crew')) {
			vm.allowToCreateCrew = true;
		}
	}

	function pasteCrew(crewType = 'baseCrew') {
		let choosenCrewId;
		if (crewType == 'baseCrew') {
			choosenCrewId = _.get(vm.data, 'crew');
		} else {
			choosenCrewId = _.get(vm.data, `${crewType}.crew`);
		}

		let chosenCrew = _.find(vm.crews, {
			team_id: choosenCrewId
		});

		if (!chosenCrew) return;

		let crewInfo = {
			crewType,
			currendDate: moment(vm.moveDate, DATE_FORMAT),
		};

		preAssignCrew(chosenCrew, vm.currentRequestLink, optimisticResponse, onBadResponse, crewInfo);

		function optimisticResponse() {
			if (crewType === 'baseCrew') {
				vm.data.foreman = _.get(chosenCrew, 'foreman.uid');
			} else {
				vm.data[crewType].foreman = _.get(chosenCrew, 'foreman.uid');
			}

			if (vm.data[crewType].helpers.length < chosenCrew.helpers.length) {
				resolveCrewConflict(crewType, chosenCrew.helpers);
			} else {
				for (let helperIndex = 0; helperIndex < vm.data[crewType].helpers.length; helperIndex++) {
					let helperUID = _.get(chosenCrew, `helpers[${helperIndex}]`);

					if (helperUID) {
						_.set(vm.data, `[${crewType}].helpers[${helperIndex}]`, helperUID || '');
					}
				}

				prepareLogsForCrew(crewType);
				let copyHelpers = angular.copy(vm.data[crewType].helpers);
				_.set(vm.data, 'workersCollection', copyHelpers);
			}

			vm.clicked = $scope.curRequest.nid;
			vm.isSubmitted[$scope.curRequest.nid] = false;
		}

		function onBadResponse() {
			toastr.error('Try to choose another crew', 'Sorry, this crew is not available for this time');
			delete vm.data.crew;
			_onInitAndReset();
		}
	}

	function prepareLogsForCrew (type) {
		let pathToCrew = type == crewsLabels.local[0] ? 'crew' : type + '.crew';
		let teamID = _.get(vm.data, pathToCrew);
		let chosenCrew = _.find(vm.crews, {team_id: teamID});
		if (!teamID || !chosenCrew) return;
		let to = 'Foreman: ';
		let crewName = _.get(chosenCrew, 'name');
		to += _.get(chosenCrew, 'foreman.name');
		let helpers = _.get(vm.data, `[${type}].helpers`, []);
		let helpersNames = dispatchLogsUsersService.getWorkersNameByIds(helpers);

		if (helpersNames) {
			to += '. Helpers: ' + helpersNames;
		}

		vm.logMsgs.push({
			text: `Crew ${crewName} was set on dispatch page.`,
			to
		});
	}

	function resolveCrewConflict(type, crewHelpers) {
		let resolveCrewConflictModal = $uibModal.open({
			template: require('../../dispatch-crews/conflict-crew-modal/conflict-crew.modal.tpl.html'),
			controller: 'ConflictCrewModal',
			size: 'lg',
			backdrop: false,
			resolve: {
				maxLength: () => _.get(vm.data, `[${type}].helpers`, []).length,
				crewHelpers: () => crewHelpers,
				helpers: () => vm.helpers,
			}
		});

		resolveCrewConflictModal.result.then(({helpers}) => {
			_.set(vm.data, `[${type}].helpers`, helpers);
			let copyHelpers = angular.copy(vm.data[type].helpers);
			_.set(vm.data, 'workersCollection', copyHelpers);
			prepareLogsForCrew(type);
		});
	}

	function createCrewToPreset() {
		let newCrew = dispatchCrewService.buildCrew(vm.data, vm.users.foreman);
		dispatchCrewService.addOneTeam(newCrew)
			.then(updatedCrew => {
				vm.crews.push(updatedCrew);
				vm.data.crew = updatedCrew.team_id;
				toastr.success('Crew was successfully created');
			}, () => toastr.error('Crew was not created'))
			.finally(() => vm.allowToCreateCrew = false);
	}

	function preAssignCrew(crew, request, optimisticCallback, pessimisticCallback, crewInfo) {
		dispatchCrewService.checkCrewAvailable(crew, request, crewInfo)
			.then(optimisticCallback, pessimisticCallback);
	}
}

