(function () {
    'use strict';

    angular
        .module('app.dispatch')
        .controller('dispatchSettingsModalCtrl', settingsModalCtrl);

    settingsModalCtrl.$inject = ['$scope', '$uibModalInstance', 'data', 'request', 'settings', 'contract_page', 'foreman', 'ForemanPaymentService', 'enums', 'SweetAlert', 'CalculatorServices', 'DispatchService', 'RequestServices'];

    function settingsModalCtrl($scope, $uibModalInstance, data, request, settings, contract_page, foreman, ForemanPaymentService, enums, SweetAlert, CalculatorServices, DispatchService, RequestServices) {

        var response = {};
        // $scope.settings = angular.fromJson((datacontext.getFieldData()).basicsettings);
        $scope.settings = settings;
        $scope.enums = enums;
        $scope.contract_page = contract_page;
        $scope.request = request;
        $scope.flatMiles = {};
        $scope.contract = {};
        $scope.flatMiles.flatMiles = false;
        $scope.contract.useInventory = contract_page.useInventory;
        $scope.creditPayroll = {};
        $scope.creditPayroll.amount = 0;
        $scope.creditPayroll.date = moment().unix();
        $scope.creditPayroll.notShow = false;
        $scope.foreman = foreman;
        $scope.busy = false;
        $scope.contractSigned = +_.get($scope.request, 'contract.currentStep', -1) >= 0;
	    let helpers, foremen;

	    let contractTypePayroll;
	    if ($scope.request.service_type.raw != 5 || $scope.invoice.request_all_data.localMove) {
		    contractTypePayroll = $scope.enums.PAYROLL;
	    } else if ($scope.request.service_type.raw == 5) {
		    if ($scope.type == 'pickedUpCrew') {
			    contractTypePayroll = $scope.enums.PICKUP_PAYROLL;
		    }

		    if ($scope.type == 'deliveryCrew') {
			    contractTypePayroll = $scope.enums.DELIVERY_PAYROLL;
		    }
	    }

	    DispatchService.getUsersByRole({  //get all selected users
		    active: 1,
		    role: ['helper', 'foreman']
	    }).then(function (users) {
		    helpers = users.helper;
		    foremen = users.foreman;
	    });
        if (foreman) {
            if (angular.isDefined($scope.request.request_all_data.credit)) {
                if (angular.isDefined($scope.request.request_all_data.credit.id)) {
                    $scope.busy = true;
                    ForemanPaymentService.getCustomPayroll(request.nid, foreman, contractTypePayroll).then(function (data) {
                        $scope.busy = false;
                        $scope.creditPayroll.notShow = true;
                        $scope.creditPayroll.amount = data[$scope.request.request_all_data.credit.id].amount;
                        $scope.creditPayroll.date = $scope.request.request_all_data.credit.date;
                    }, () => {
                    	SweetAlert('Error', '', 'error');
                    	$scope.busy = false;
                    });
                }
            }
        }
        if (angular.isDefined($scope.request.request_all_data.flatMiles)) {
            $scope.flatMiles.flatMiles = $scope.request.request_all_data.flatMiles;
        } else if ($scope.request.distance) {
            if (!!settings.local_flat_miles && parseFloat($scope.request.distance.value) > parseFloat(settings.local_flat_miles)) {
                $scope.flatMiles.flatMiles = true;
            }
        }
        if (angular.isDefined($scope.request.request_all_data.useInventory)) {
            $scope.contract.useInventory = $scope.request.request_all_data.useInventory;
        }
        $scope.page = data.page;
        if ($scope.page == 'simpleCrew') {
        	let rate = 0;
        	let lastCrewIndex = data.crews.additionalCrews.length-1;
			let selectedAdditionalCrew = data.crews.additionalCrews[data.index];
			let lastAdditionalCrew = data.crews.additionalCrews[lastCrewIndex];
			let penultimateAdditionalCrewRateSave = _.get(data.crews, `additionalCrews[${data.crews.additionalCrews.length-2}].rateSaveManually`);

        	if (selectedAdditionalCrew.rate && selectedAdditionalCrew.rateSaveManually) {
				rate = selectedAdditionalCrew.rate;
			} else if (lastAdditionalCrew.rateSaveManually && data.index == lastCrewIndex) {
				rate = lastAdditionalCrew.rate;
			} else if (data.crews.additionalCrews.length > 1 && penultimateAdditionalCrewRateSave) {
				rate = getSaveAdditionalCrewRate();
			} else {
				rate = getAdditionalCrewsRate(data.index) + getBaseCrewRate();
			}

            if (rate < 0 || isNaN(rate)) {
                rate = 0;
            }
            $scope.rate = {
                value: rate
            };
            response = {
                rate: $scope.rate
            }
        }
        else { //baseCrew
            $scope.usersCollection = {};
            _.forEach(data.users, function (user) {
                $scope.usersCollection[user.uid] = user;
            });
            $scope.crews = angular.copy(data.crews);
            $scope.switchers = data.switchers;
            $scope.flatFeeOptions = data.flatFeeOptions;
            $scope.isAddHelpers = data.isAddHelpers;
            $scope.selectedPayment = {
                alias: data.paymentType.length ? data.paymentType : $scope.contract_page.paymentTypes[0].alias
            };
            $scope.selectedFlatFeeOption = !_.isEmpty(data.selectedFlatFeeOption) ? data.selectedFlatFeeOption : {
                baseCrew: data.flatFeeOptions['hour'].id
            };
            $scope.pricePerHelperModel = data.pricePerHelper || {};

            if (data.page == 'baseCrew') {
                var i = 0;
                $scope.unpaidHelpers = {};

                if ($scope.crews.baseCrew.unpaidHelpers) {
                    while (i = $scope.crews.baseCrew.unpaidHelpers.pop()) {
                        $scope.unpaidHelpers[i] = true;
                    }
                }
            }

            $scope.getTotal = function () {
                var total = 0;
                if ($scope.selectedPayment.alias == 'FlatFee') {
                    angular.forEach($scope.pricePerHelperModel, function (item, i) {
                        total += item || 0;
                    });
                }
                return total;
            };

            response = {
                unpaids: $scope.unpaidHelpers || 0,
                paymentType: $scope.selectedPayment,
                pricePerHelper: $scope.pricePerHelperModel,
                selectedFlatFeeOption: $scope.selectedFlatFeeOption,
                flatMiles: $scope.flatMiles.flatMiles,
                useInventory: $scope.contract.useInventory
            }

        }

        let oldValues = {
            flatMiles: angular.copy($scope.flatMiles.flatMiles),
	        useInventory: angular.copy($scope.contract.useInventory),
	        unpaidHelpers: angular.copy($scope.unpaidHelpers),
	        selectedPayment: $scope.selectedPayment ? angular.copy($scope.selectedPayment.alias) : '',
	        creditPayroll: angular.copy($scope.creditPayroll.amount),
			flatFeeOption: $scope.selectedFlatFeeOption ? angular.copy($scope.selectedFlatFeeOption.baseCrew) : [],
	        pricePerHelper: angular.copy(data.pricePerHelper) || {},
	        addCrewRate: $scope.rate ? angular.copy($scope.rate.value) : ''
        };

	    function getCrewRatesLogs() {
		    let oldPoint = $scope.flatFeeOptions[oldValues.flatFeeOption].point;
		    let newPoint = $scope.flatFeeOptions[$scope.selectedFlatFeeOption.baseCrew].point;
		    let foremanLogs = [];
		    let helperLogs = [];
			let rateLogs = [];
		    for (let uid in $scope.pricePerHelperModel) {
		    	if ($scope.pricePerHelperModel[uid] != oldValues.pricePerHelper[uid] || oldValues.flatFeeOption != $scope.selectedFlatFeeOption.baseCrew) {
		    		let foreman = $scope.crews.foreman == uid || $scope.switchers[$scope.crews.foreman] == uid ? 'Foreman': '';
		    		let helper = $scope.crews.foreman != uid && $scope.switchers[$scope.crews.foreman] != uid ? 'Helper' : '';
		    		let isSwitcher = false;
				    _.forIn($scope.switchers, function(value, key) {
					    if (value == uid) {
						    isSwitcher = true;
					    }
				    });
		    		let switcher = isSwitcher ? '(Switcher)' : '';
				    let role = foreman + helper + switcher;
				    let action = oldValues.pricePerHelper && oldValues.pricePerHelper[uid] ? 'changed' : 'set';
				    let name = $scope.usersCollection[uid].name;
				    let oldPointIsDollar = oldPoint == '$';
				    let newPointIsDollar = newPoint == '$';
		    		let msg = {
		    			text: "Flat Fee for " + role  + ' ' +  name  + ' was ' + action,
					    from: (oldPointIsDollar  ? oldPoint : '') + oldValues.pricePerHelper && oldValues.pricePerHelper[uid] ? oldValues.pricePerHelper[uid] +  (!oldPointIsDollar ? oldPoint : '') : undefined,
					    to: (newPointIsDollar ? newPoint : '') + $scope.pricePerHelperModel[uid] + (!newPointIsDollar ? newPoint : '')
				    };
		    		if (foreman) {
		    			if (switcher) {
		    				foremanLogs.push(msg);
					    } else {
		    				foremanLogs.unshift(msg);
					    }
				    } else {
					    if (switcher) {
						    helperLogs.push(msg);
					    } else {
						    helperLogs.unshift(msg);
					    }
				    }
				    rateLogs = foremanLogs.concat(helperLogs);
			    }
		    }
		    if (!_.isEmpty(rateLogs)) {
			    let total = 0;
			    if ($scope.selectedPayment.alias == 'FlatFee') {
				    angular.forEach($scope.pricePerHelperModel, function (item) {
					    total += item || 0;
				    });
			    }
		    	let msg = {
		    		text: 'Total Flat Fee: ' + total +  ' ' + newPoint
			    };
			    rateLogs.push(msg);
		    }
		    return rateLogs;
	    }
        function getLogsArray() {
	        let logMsgs = [];
	        if ($scope.page == 'simpleCrew') {
	        	if (oldValues.addCrewRate != $scope.rate.value) {
			        let value = !oldValues.addCrewRate ? 'set' : 'changed';
			        let msg = {
				        text: "Crew Rate was " + value,
				        from: oldValues.addCrewRate ? oldValues.addCrewRate : undefined,
				        to: $scope.rate.value
			        };
			        logMsgs.push(msg);
		        }
	        } else {
		        if ($scope.flatMiles.flatMiles != oldValues.flatMiles) {
			        let value = $scope.flatMiles.flatMiles ? 'set' : 'unset';
			        let msg = {
				        text: "Pikcup/Unloading option was " + value
			        };
			        logMsgs.push(msg);
		        }
		        if ($scope.contract.useInventory != oldValues.useInventory) {
			        let value = $scope.contract.useInventory ? 'set' : 'unset';
			        let msg = {
				        text: "Use inventory on contract was " + value
			        };
			        logMsgs.push(msg);
		        }
		        if ($scope.selectedPayment.alias != oldValues.selectedPayment) {
			        let newType = _.find($scope.contract_page.paymentTypes, function(obj){return obj.alias == $scope.selectedPayment.alias});
			        let oldType = _.find($scope.contract_page.paymentTypes, function(obj){return obj.alias == oldValues.selectedPayment});
			        let msg = {
				        text: "Payment Type",
				        from: oldType.name,
				        to: newType.name
			        };
			        logMsgs.push(msg);
		        }
		        if ($scope.selectedPayment.alias == 'FlatFee') {
			        if (oldValues.flatFeeOption != $scope.selectedFlatFeeOption.baseCrew) {
				        let msg = {
					        text: "Pay options was changed",
					        from: oldValues.flatFeeOption != $scope.selectedFlatFeeOption.baseCrew ? _.capitalize(oldValues.flatFeeOption) : undefined,
					        to: _.capitalize($scope.selectedFlatFeeOption.baseCrew)
				        };
				        logMsgs.push(msg);
			        }
			        let rateLogs = getCrewRatesLogs();
			        _.forEach(rateLogs, function (msg) {
				        logMsgs.push(msg);
			        })
		        }
		        if ($scope.creditPayroll.amount != oldValues.creditPayroll) {
			        let msg = {
				        text: "Credit was changed",
				        from: '$' + oldValues.creditPayroll || '0',
				        to: '$' + $scope.creditPayroll.amount
			        };
			        logMsgs.push(msg);
		        }
		        for (let key in $scope.unpaidHelpers) {
			        if ($scope.unpaidHelpers[key] != oldValues.unpaidHelpers[key] && !_.isUndefined(helpers[key])) {
				        let name = helpers[key].field_user_first_name + ' ' + helpers[key].field_user_last_name;
				        let value = $scope.unpaidHelpers[key] ? "was set as unpaid helper" : 'was removed from unpaid helpers';
				        let msg = {
					        text: "Helper " + name + value
				        };
				        logMsgs.push(msg);
			        }
		        }
	        }
            return logMsgs;
        }
        $scope.saveSettings = function () {
            let logMsgs = getLogsArray();
            if ($scope.creditPayroll.amount != 0 && !$scope.creditPayroll.notShow) {
                if ($scope.foreman) {
                    response.credit = {
                        id: 0,
                        date: moment().unix()
                    };
                    var customPayroll = {
                        uid: $scope.foreman,
                        nid: $scope.request.nid,
                        amount: 0 - Number($scope.creditPayroll.amount),
                        name: 'Credit',
                        photo: [],
                        contract_type: $scope.enums.PAYROLL
                    };
                    ForemanPaymentService.createCustomPayroll(customPayroll).then(function (data) {
                        response.credit.id = _.head(data);
                        response.flatMiles = $scope.flatMiles.flatMiles;
                        response.useInventory = $scope.contract.useInventory;
						response.rateSaveManually = true;
                        $uibModalInstance.close(response);
                    });
	                if (!_.isEmpty(logMsgs)) {
		                RequestServices.sendLogs(logMsgs, "Crew settings were updated from Dispatch Page", $scope.request.nid, 'MOVEREQUEST');
	                }
                } else {
                    SweetAlert.swal('Please choose foreman or set $0 to Credit field', '', 'warning');
                    return false;
                }
            } else {
	            if (!_.isEmpty(logMsgs)) {
		            RequestServices.sendLogs(logMsgs, "Crew settings were updated from Dispatch Page", $scope.request.nid, 'MOVEREQUEST');
	            }
                response.flatMiles = $scope.flatMiles.flatMiles;
                response.useInventory = $scope.contract.useInventory;
				response.rateSaveManually = true;
				$uibModalInstance.close(response);
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        function getBaseCrewRate() {
        	return _.round($scope.request.request_all_data.invoice.rate.value, 2) || _.round($scope.request.rate.value, 2);
		}

		function getAdditionalCrewsRate(currentCrewIndex) {
        	let additionalCrewRate = 0;
        	let tempAdditionalCrews = data.crews.additionalCrews.slice(0, currentCrewIndex + 1);

        	_.forEach(tempAdditionalCrews, (crew) => {
        		if (crew.rateSaveManually) {
					additionalCrewRate += crew.rate;
				} else {
					additionalCrewRate += CalculatorServices.getRateAdditionalHelper(data.request.date.raw * 1000, crew) || crew.rate;
				}
			});

        	return additionalCrewRate;
		}

		function getSaveAdditionalCrewRate() {
			let lastAdditionalCrew = data.crews.additionalCrews[data.crews.additionalCrews.length-1];
			let penultimateAdditionalCrewRate = _.get(data, `crews.additionalCrews[${data.crews.additionalCrews.length-2}].rate`, 0);

			return penultimateAdditionalCrewRate +
				   CalculatorServices.getRateAdditionalHelper(data.request.date.raw * 1000, lastAdditionalCrew) || lastAdditionalCrew.rate;
		}
    }

})();
