(function () {
    'use strict';

    angular
        .module('app.dispatch')
        .directive('ccSelectWithOptions', ccSelectWithOptions)

    ccSelectWithOptions.$inject = ['DispatchService'];

    function ccSelectWithOptions(DispatchService) {
        return {
            restrict: 'A',
            require: 'ngModel',
            controller: ['$scope', ccSelectController]
        };


        function ccSelectController(scope, DispatchService) {
            var helperCounter = 0;
            scope.helpersIterator = [];
            scope.helpers = [];

            angular.forEach(users.helper, function (helper, uid) {
                vm.helpers.push({
                    name: helper.name,
                    uid: helper.uid,
                    selected: false
                });
            });
        }
    }

})();




