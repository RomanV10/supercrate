'use strict';

angular
	.module('app.dispatch')
	.directive('dispatchFlatRate', dispatchFlatRate);

/* @ngInject */
function dispatchFlatRate() {
	return {
		scope: {},
		restrict: 'E',
		template: require('./flat-rate.template.html'),
		controller: 'DispatchFlatRateCtrl',
		controllerAs: 'vm',
	};
}