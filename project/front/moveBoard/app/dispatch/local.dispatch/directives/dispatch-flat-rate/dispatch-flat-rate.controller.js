'use strict';

import {DISPATCH_REQUEST_PATH} from '../../../constants/dispatch-request-path.constants';

angular
	.module('app.dispatch')
	.controller('DispatchFlatRateCtrl', DispatchFlatRateCtrl);

/* @ngInject */
function DispatchFlatRateCtrl($scope, DispatchService, $uibModal, datacontext, $q, logger) {
	var vm = this;
	var _super = $scope.$parent.$parent;
	var request_data;
	vm.navigation = {
		values: ['Pick up crew', 'Delivery crew'],
		active: 0
	};
	var crewsLabels = {
		local: ['baseCrew', 'additionalCrew'],
		flatRate: ['pickedUpCrew', 'deliveryCrew']
	};
	$scope.super = _super;

	$scope.$watch('super.vm.clicked', (newVal) => {
		if (newVal && _.get($scope, 'super.curRequest')) {
			$scope.curRequest = $scope.super.curRequest.source || $scope.super.curRequest;
		}
	});

	$scope.$on('request.updated', handlerUpdateRequest);

	function handlerUpdateRequest(event, data) {
		if (data.service_type.raw != 5 || !$scope.curRequest) {
			return false;
		}

		let pickupCrewSize = _.get(data, DISPATCH_REQUEST_PATH.INVOICE_CREW, data.crew.value);
		let deliveryCrewCount = _.get(data, 'field_delivery_crew_size.value', 2);
		let deliveryCrewSize = _.get(data, DISPATCH_REQUEST_PATH.INVOICE_DELIVERY_CREW, deliveryCrewCount);

		addWorkerIfNeed(_super.vm.data.pickedUpCrew, pickupCrewSize, crewsLabels.flatRate[0]);
		addWorkerIfNeed(_super.vm.data.deliveryCrew, deliveryCrewSize, crewsLabels.flatRate[1]);

		if ($scope.curRequest.field_flat_rate_local_move.value != data.field_flat_rate_local_move.value) {
			vm.navigation.active = 0;
		}
	}

	function addWorkerIfNeed(crews, crewSize, type) {
		let allHelpers = _.get(crews, 'helpers.length');

		if (_.isUndefined(allHelpers)) {
			_super.vm.getByDate();

			return;
		}

		let neededHelpers = crewSize - allHelpers - 1;

		if (allHelpers < crewSize) {
			for (let index = 0; index < neededHelpers; index++) {
				addWorker(type);
			}
		}
	}

	$scope.openSettingsModal = openSettingsModal;
	$scope.isSelected = isSelected;
	$scope.addWorker = addWorker;
	$scope.deleteWorker = deleteWorker;

	function isSelected(uid_r, crewIndex, reverse) {
		let crew = $scope.super.vm.data[crewIndex];

		return crew && (crew.foreman == uid_r
			|| (crew.helpers && crew.helpers.indexOf(uid_r) > -1)
			|| $scope.super.vm.isExistSwitchers(uid_r, reverse));
	}

	function addWorker(crew) {
		_super.vm.data[crew].helpers.push('');
	}

	function deleteWorker(crewType) {
		let helpers = _super.vm.data[crewType].helpers;
		let lastUid = _.last(helpers);
		let crewSize = _.get($scope.curRequest, DISPATCH_REQUEST_PATH.INVOICE_CREW, $scope.curRequest.crew.value);

		if (crewType == crewsLabels.flatRate[1]) {
			let deliveryCrewCount = _.get($scope.curRequest, 'field_delivery_crew_size.value', 2);
			crewSize = _.get($scope.curRequest, DISPATCH_REQUEST_PATH.INVOICE_DELIVERY_CREW, deliveryCrewCount);
		}

		if (crewSize - 1 == helpers.length) {
			toastr.warning(`Crew size is ${crewSize}`, 'Warning!');

			return false;
		}

		helpers.splice(-1, 1);

		if (lastUid != '') {
			_super.vm._removeFromCol(lastUid);
		}
	}


	function openSettingsModal() {
		var calcSettings = angular.fromJson(datacontext.getFieldData().calcsettings);

		var travelTime = angular.isDefined(_super.curRequest.request_all_data.travelTime) ? _super.curRequest.request_all_data.travelTime : calcSettings.travelTime;

		var data = {};
		request_data = {};

		if (_super.curRequest.request_data.value.length) {
			request_data = angular.fromJson(_super.curRequest.request_data.value);
		}

		data.crews = _super.vm.data;
		data.paymentType = request_data.paymentType || {};
		data.pricePerHelper = request_data.pricePerHelper || {
		    pickedUpCrew: {},
            deliveryCrew: {}
		};
		data.switchers = _super.vm.switchers;
		data.users = _super.vm.helpers;
		data._super = _super;
		data.navigation = vm.navigation;
		data.flatFeeOptions = _super.flatFeePaymentTypes;
		data.selectedFlatFeeOption = request_data.selectedFlatFeeOption;
		data.total = 0;
		data.hours = 0;

		if (_super.curRequest.service_type.raw == (_.invert(datacontext.getFieldData().field_lists.field_move_service_type)[" Flat Rate"])) {
			data.total = +_super.curRequest.flat_rate_quote.value;
			data.hours = +(Number(_super.curRequest.maximum_time.raw) + Number(travelTime ? _super.curRequest.travel_time.raw : 0));
		}

		var modalInstance = $uibModal.open({
			template: require('../../settingsFlateModal.html'),
			size: 'md',
			controller: settingsModalCtrl,
			resolve: {
				data: () => data
			}
		});

		modalInstance.result.then(function (result) {
			_super.vm.busy = true;
			_super.$parent.vm.isSubmitted = false;

			if (!_.isEmpty(result.paymentType)) {
				request_data.paymentType = result.paymentType;
			}

			if (!_.isEmpty(result.pricePerHelper)) {
				request_data.pricePerHelper = result.pricePerHelper;
			}

			if (!_.isEmpty(result.selectedFlatFeeOption)) {
				request_data.selectedFlatFeeOption = result.selectedFlatFeeOption;
			}

			var request = {
				request_data: {value: angular.toJson(request_data)}
			};
			var nid = _super.curRequest.nid;

			updateRequest(nid, request)
				.then(function () {
					_super.vm.busy = false;
					var ind = _.findIndex(_super.vm.requests, {nid: nid});
					_super.vm.requests[ind].request_data.value = request_data;
					_super.curRequest.request_data.value = request_data;
				});
		});
	}

	function settingsModalCtrl($scope, $uibModalInstance, data) {
		var response = {};
		$scope.settings = angular.fromJson((datacontext.getFieldData()).basicsettings);
		$scope.contract_page = angular.fromJson((datacontext.getFieldData()).contract_page);
		$scope.flatFeeOptions = data.flatFeeOptions;
		//$scope.selectedFlatFeeOption = data.selectedFlatFeeOption;
		$scope.navigation = data.navigation;
		$scope.usersCollection = data.users;
		$scope.crews = angular.copy(data.crews);
		$scope.switchers = data.switchers;
		$scope.totalCost = data.total;
		$scope.totalTime = data.hours;
		$scope.paymentTypes = $scope.contract_page.paymentTypes || [];

		var custom = {
			alias: "Custom",
			name: "Custom",
			selected: true
		};
		$scope.paymentTypes.push(custom);

		$scope.selectedFlatFeeOption = !_.isEmpty(data.selectedFlatFeeOption) ? data.selectedFlatFeeOption : {
			pickedUpCrew: $scope.flatFeeOptions['hour'].id,
			deliveryCrew: $scope.flatFeeOptions['hour'].id
		};
		$scope.selectedPayment = !_.isEmpty(data.paymentType) ? data.paymentType : {
			pickup: $scope.contract_page.paymentTypes[2].alias,
			delivery: $scope.contract_page.paymentTypes[2].alias
		};
		$scope.pricePerHelperModel = data.pricePerHelper || {};

		$scope.getUserName = getUserName;

		function getUserName(uid) {
			if (!uid) {
				return '';
			}

			return _.find($scope.usersCollection, {uid: uid}).name;
		}

		$scope.getTotal = function (crewType) {
			var crew = $scope.crews[crewType];
			var total = 0;

			for (var i = 0, l = crew.helpers.length; i < l; i++) {
				angular.forEach($scope.pricePerHelperModel[crewType], function (item, ii) {
					if (ii == crew.helpers[i] && $scope.selectedFlatFeeOption[crewType] == "percent") {
						total = item * Number($scope.totalTime) / 100;
					} else if (ii == crew.helpers[i] && ($scope.selectedFlatFeeOption[crewType] == "hour" || $scope.selectedFlatFeeOption[crewType] == "money")) {
						total += item;
					}
				});
			}

			total += $scope.pricePerHelperModel[crewType][crew['foreman']];

			if ($scope.selectedFlatFeeOption[crewType] == "hour") {
				total = Number($scope.totalTime) * total;
			}

			return _.isNaN(total) ? 0 : total;
		};

		response = {
			paymentType: $scope.selectedPayment,
			pricePerHelper: $scope.pricePerHelperModel,
			selectedFlatFeeOption: $scope.selectedFlatFeeOption
		};

		$scope.saveSettings = function () {
			$uibModalInstance.close(response);
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}

	function updateRequest(nid, request) {
		var defer = $q.defer();

		DispatchService.updateRequest(nid, request)
			.then(function () {
				defer.resolve();
			}, function (reason) {
				defer.reject();
				logger.error(reason, reason, "Error");
			})
			.finally(function () {
				vm.busy = false;
			});

		return defer.promise;
	}

}
