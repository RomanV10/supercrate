import './day-dispatch-notes.styl';

angular.module('app.dispatch')
	.directive('dayDispatchNotes', dayDispatchNotes);

function dayDispatchNotes() {
	return {
		restrict: 'E',
		template: require('./day-dispatch-notes.html'),
		scope: {
			day: '<',
		},
		controller: 'DayDispatchNotesController',
		controllerAs: 'vm',
	};
}
