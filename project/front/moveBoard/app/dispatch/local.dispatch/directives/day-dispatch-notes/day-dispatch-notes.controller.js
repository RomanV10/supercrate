angular
	.module('app.dispatch')
	.controller('DayDispatchNotesController', DayDispatchNotesController);

/* @ngInject */
function DayDispatchNotesController($scope, dayDispatchNoteService, SweetAlert) {
	const title = 'You have unsaved dispatch notes. Do you want to save them?';
	let vm = this;

	vm.dayNote = '';
	vm.dayOriginalNote = '';

	let isLoadNote = false;

	$scope.$watch('day', onChangeCurrentDay);
	$scope.$on('$destroy', onDestroy);

	vm.saveNote = saveNote;
	vm.isNotChangeDayNote = isNotChangeDayNote;

	function saveNote(day = $scope.day) {
		vm.busy = true;

		let saveNotePromise = dayDispatchNoteService.saveNoteByDay(day, vm.dayNote)
			.then(() => {
				toastr.success('Dispatch note was saved');
				setDayNote(vm.dayNote);
			})
			.catch(() => {
				toastr.error( 'Please try again', 'Cannot save dispatch note');
			})
			.finally(() => {
				vm.busy = false;
			});

		return saveNotePromise;
	}

	function isNotChangeDayNote() {
		return _.isEqual(vm.dayNote, vm.dayOriginalNote);
	}

	function onChangeCurrentDay(newValue, oldValue) {
		if (!_.isEqual(newValue, oldValue) || !isLoadNote) {
			if (vm.isNotChangeDayNote()) {
				loadNoteByDay(newValue);
			} else {
				SweetAlert.swal({
					title,
					type: 'warning',
					showCancelButton: true,
					cancelButtonText: 'No',
					confirmButtonText: 'Yes'
				}, isConfirm => {
					if (isConfirm) {
						vm.saveNote(oldValue)
							.then(() => {
								loadNoteByDay(newValue);
							});
					} else {
						loadNoteByDay(newValue);
					}
				});
			}

			isLoadNote = true;
		}
	}

	function loadNoteByDay(day) {
		vm.busy = true;

		dayDispatchNoteService.getNoteByDay(day)
			.then((note = '') => {
				setDayNote(note);
			})
			.catch(() => {
				SweetAlert.swal('Cannot load dispatch note', '', 'error');
			})
			.finally(() => {
				vm.busy = false;
			});
	}

	function setDayNote(note = '') {
		vm.dayNote = note;
		vm.dayOriginalNote = note;
	}

	function onDestroy() {
		if (!vm.isNotChangeDayNote()) {
			SweetAlert.swal({
				title,
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'No',
				confirmButtonText: 'Yes'
			}, isConfirm => {
				if (isConfirm) {
					vm.saveNote();
				}
			});
		}
	}
}
