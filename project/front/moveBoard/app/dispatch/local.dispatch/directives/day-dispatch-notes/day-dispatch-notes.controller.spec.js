describe('Local dispatch day note', () => {
	let controller;
	let result;
	let expectedResult;
	let dayDispatchNoteService;
	let $scope;

	beforeEach(inject((_dayDispatchNoteService_) => {
		dayDispatchNoteService = _dayDispatchNoteService_;

		$scope = $rootScope.$new();
		$scope.day = 'Nov 20, 2018';
		controller = $controller('DayDispatchNotesController', {
			$scope
		});
	}));

	describe('on init', () => {
		it('dayDispatchNoteService.getNoteByDay should be called', () => {
			// given
			spyOn(dayDispatchNoteService, 'getNoteByDay').and.returnValue($q.resolve());
			expectedResult = 'Nov 20, 2018';

			// when
			$scope.$apply();

			// then
			expect(dayDispatchNoteService.getNoteByDay).toHaveBeenCalledWith(expectedResult);
		});

		it('should init vm.dayNote', () => {
			// given
			spyOn(dayDispatchNoteService, 'getNoteByDay').and.returnValue($q.resolve('Loaded note'));
			expectedResult = 'Loaded note';

			// when
			$scope.$apply();

			// then
			expect(controller.dayNote).toEqual(expectedResult);
		});

		it('when error should show error message', () => {
			// given
			spyOn(dayDispatchNoteService, 'getNoteByDay').and.returnValue($q.reject());
			spyOn(SweetAlert, 'swal').and.returnValue('');
			expectedResult = [ 'Cannot load dispatch note', '', 'error' ];

			// when
			$scope.$apply();

			// then
			expect(SweetAlert.swal).toHaveBeenCalledWith(...expectedResult);
		});
	});

	describe('save note', () => {
		beforeEach(() => {
			spyOn(dayDispatchNoteService, 'getNoteByDay').and.returnValue($q.resolve());
		});

		it('Should show message when save dispatch note', () => {
			// given
			expectedResult = 'Dispatch note was saved';
			spyOn(toastr, 'success').and.returnValue('');
			spyOn(dayDispatchNoteService, 'saveNoteByDay').and.returnValue($q.resolve());

			// when
			controller.saveNote();
			$scope.$apply();

			// then
			expect(toastr.success).toHaveBeenCalledWith(expectedResult);
		});

		it('Should show error message when do not save dispatch note', () => {
			// given
			expectedResult = ['Please try again', 'Cannot save dispatch note' ];
			spyOn(toastr, 'error').and.returnValue('');
			spyOn(dayDispatchNoteService, 'saveNoteByDay').and.returnValue($q.reject());

			// when
			controller.saveNote();
			$scope.$apply();

			// then
			expect(toastr.error).toHaveBeenCalledWith(...expectedResult);
		});
	});

	describe('when change day', () => {
		it('and have changes should ask about save changes', () => {
			// given
			let successGetNoteByDay = $q.resolve('Loaded note');
			spyOn(dayDispatchNoteService, 'getNoteByDay').and.returnValue(successGetNoteByDay);
			spyOn(SweetAlert, 'swal').and.returnValue('');
			$scope.$apply();

			// when
			$scope.day = 'Nov 21, 2018';
			controller.dayNote = "New note";
			$scope.$apply();

			// then
			expect(SweetAlert.swal).toHaveBeenCalled();
		});

		it('and have not changes should load notes by day second time', () => {
			// given
			spyOn(dayDispatchNoteService, 'getNoteByDay').and.returnValue($q.resolve());
			$scope.$apply();
			expectedResult = 2;

			// when
			$scope.day = 'Nov 21, 2018';
			$scope.$apply();
			result = dayDispatchNoteService.getNoteByDay.calls.count();

			// then
			expect(result).toEqual(expectedResult);
		});
	});
});