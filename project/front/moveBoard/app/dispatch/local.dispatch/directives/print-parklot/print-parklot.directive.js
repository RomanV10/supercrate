'use strict';

angular
	.module('app.dispatch')
	.directive('printParklot', printParklot);

function printParklot() {
	return {
		restrict: 'A',
		controller: 'printParklotCtrl',
		template: require('./print-parklot.tpl.html'),
		scope: {
			jobs: '<',
			date: '<',
			allUsersInfo: '<',
			allCrewsInfo: '<',
			allTrucksInfo: '<'
		}
	};
}
