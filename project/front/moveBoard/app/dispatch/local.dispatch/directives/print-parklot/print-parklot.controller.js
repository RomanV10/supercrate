'use strict';

angular
	.module('app.dispatch')
	.controller('printParklotCtrl', printParklotCtrl);

/* @ngInject */
function printParklotCtrl($scope, $element, $q, $timeout, printService, CalculatorServices) {
	$scope.printDay = printDay;
	const TIME_FORMAT = 'h:mm A';
	const TABLE_HEADERS = [
		'Job #',
		'From',
		'To',
		'Crew',
	];
	const STYLE_SHEET = require('!raw-loader!stylus-loader!./print-parklot.styl');
	$scope.tableRows = [];
	$element.bind('click', $scope.printDay);

	function buildTable() {
		$scope.currentDate = $scope.date;
		$scope.currentDay = moment($scope.date, 'MM/DD/YYYY').format('dddd');
		$scope.tableRows = [];
		_.forEach($scope.jobs, job => {
			let preparedRow = prepareUsualRow(job);
			if (job.ld_trip) {
				prepareLdTripRow(preparedRow, job.trip_info);
			}
			$scope.tableRows.push(preparedRow);
		});
		$scope.tableRows.sort((oneRow, anotherRow) => oneRow.rawTimeStart - anotherRow.rawTimeStart);
	}

	function prepareUsualRow(job) {
		let isLoadingHelp = job.service_type.raw == 3;
		let isUnloadingHelp = job.service_type.raw == 4;
		let result = {};
		result.headers = _.clone(TABLE_HEADERS);
		result.headers[0] += job.nid;
		result.headers.unshift(job.service_type.value);
		result.customer = job.name;
		result.phone = job.phone;
		result.from = isUnloadingHelp ? '' : _.get(job, 'field_moving_from.thoroughfare', '') + ' ' + job.from;
		result.to = isLoadingHelp ? '' : _.get(job, 'field_moving_to.thoroughfare', '') + ' ' + job.to;
		let weight = CalculatorServices.getTotalWeight(job).weight;
		result.weight = `Weight: ${weight * 7} Lbs,Volume: ${weight} Cf.`;
		result.time = buildTimeString(job);
		result.rawTimeStart = job.start_time1.raw;
		result.crew = prepareCrew(job);
		result.trucks = prepareTrucksArray(job.trucks);
		return result;
	}

	function prepareLdTripRow(preparedRow, trip_info) {
		preparedRow.from = '';
		_.forEach(trip_info.start_from, part => {
			preparedRow.from += part + ' ';
		});
		preparedRow.to = '';
		_.forEach(trip_info.ends_in, part => {
			preparedRow.to += part + ' ';
		});
		preparedRow.trucks = trip_info.trucks.map(truck => truck.name);
		let weight = trip_info.jobs.items.reduce((sum, tripJob) => sum + +tripJob.volume_cf, 0);
		preparedRow.weight = `Weight: ${weight * 7} Lbs,Volume: ${weight} Cf.`;
		preparedRow.crew = prepareCrewForLd(trip_info);
	}

	function prepareCrewForLd(trip_info) {
		let result = [];
		if (+trip_info.carrier.carrier_id) {
			result.push('Driver:');
			result.push(trip_info.carrier.driver_name);
			result.push('Phone:');
			result.push(trip_info.carrier.driver_phone);
		} else {
			result.push('Foreman:');
			result.push(trip_info.foreman.foreman_name);
			result.push('Helpers:');
			_.forEach(trip_info.helper, helper => result.push(helper.helper_name));
		}
		return result;
	}

	function prepareCrew(job) {
		let crews = _.get(job, 'request_data.value.crews');
		let result = [];
		if (!crews) return result;
		if (crews.crew) {
			let crewInfo = _.find($scope.allCrewsInfo, crew => crew.team_id == crews.crew);
			if (crewInfo) {
				result.push('Crew:');
				result.push(crewInfo.name);
			}
		}
		if (job.service_type.raw == 5) {
			let requestMoveDate = moment(job.date.value, 'MM/DD/YYYY');
			let requestDeliveryDateFrom = moment(job.delivery_date_from.value, 'D-M-YYYY');
			let requestDeliveryDateTo = moment(job.delivery_date_to.value, 'D-M-YYYY');
			let currentDateMoment = moment($scope.currentDay, 'MMM D, YYYY');
			if (requestMoveDate.isSame(currentDateMoment, 'day')) {
				crews = crews.pickedUpCrew || {};
			} else if (!requestDeliveryDateFrom.isAfter(currentDateMoment, 'day')
				&& !requestDeliveryDateTo.isBefore(currentDateMoment, 'day')) {
				crews = crews.deliveryCrew || {};
			}
		}
		let foremanInfo = _.find($scope.allUsersInfo.foreman, foreman => foreman.uid == crews.foreman);
		if (foremanInfo) {
			result.push('Foreman:');
			result.push(foremanInfo.name);
			let helpers = _.get(crews, 'baseCrew.helpers') || crews.helpers;
			if(helpers) {
				result.push('Helpers:');
				_.forEach(helpers, helperId => {
					let helperInfo = _.find($scope.allUsersInfo.helper, helper => helper.uid == helperId);
					if (!helperInfo) helperInfo = _.find($scope.allUsersInfo.foreman, foreman => foreman.uid == helperId);
					if (!helperInfo) helperInfo = _.find($scope.allUsersInfo.driver, driver => driver.uid == helperId);
					if (helperInfo) {
						let name = helperInfo.field_user_first_name
							? helperInfo.field_user_first_name + ' ' + helperInfo.field_user_last_name
							: helperInfo.name;
						result.push(name);
					}
				});
			}
		}
		return result;
	}

	function prepareTrucksArray(trucks) {
		let result = [];

		_.forEach(trucks.raw, truck => {
			let preparedTruck = $scope.allTrucksInfo[truck];
			result.push(preparedTruck);
		});
		return result;
	}

	function buildTimeString(job) {
		let endTime = moment(job.start_time1.value, TIME_FORMAT);
		let startTime = endTime.format(TIME_FORMAT);
		let fullDuration = job.maximum_time.raw + job.travel_time.raw;
		endTime = endTime.add(fullDuration, 'hours').format(TIME_FORMAT);
		return `${startTime} - ${endTime}`;
	}

	function waitForHtml() {
		let defer = $q.defer();
		let timeoutDelay = 200;
		let timeoutCount = 10;
		let watcherFunction = () => {
			if ($element.find('tbody').length != $scope.tableRows.length) {
				if (timeoutCount > 0) {
					timeoutCount--;
					$timeout(watcherFunction, timeoutDelay);
				} else {
					defer.reject();
				}
			} else {
				defer.resolve();
			}
		};
		watcherFunction();
		return defer.promise;
	}

	function printDay() {
		buildTable();
		if (!$scope.tableRows.length) {
			toastr.error('Nothing to print', 'Parklot is empty');
			return;
		}
		$scope.readyForPrint = true;
		waitForHtml()
			.then(() => {
				let printContents = $element.find('#printContent')[0].innerHTML;
				printService.printThisLayout(printContents, STYLE_SHEET);
				$scope.readyForPrint = false;
			})
			.catch(() => {
				toastr.error('Error', 'Cannot print parklot');
			});
	}
}
