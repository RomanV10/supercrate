(function () {
	'use strict';

	angular
		.module('app.dispatch')
		.directive('ccRequestScheduleCalendar', ccRequestScheduleCalendar);

	ccRequestScheduleCalendar.$inject = ['logger', 'datacontext', 'apiService', 'moveBoardApi', 'MomentWrapperService'];

	function ccRequestScheduleCalendar(logger, datacontext, apiService, moveBoardApi, MomentWrapperService) {
		return {
			restrict: 'A',
			require: '^ngModel',
			scope: {
				moveDate: '=ngModel'
			},
			link: linkFn
		};

		function linkFn(scope, element, attrs) {

			var startDate,
				endDate,
				calendar = {},
				$datePickerBlock = $('#datePicker-block'),
				dirDate = scope.moveDate;

			$datePickerBlock.addClass('disabled');

			element.datepicker({
				dateFormat: 'M d, yy',
				numberOfMonths: 1,
				showButtonPanel: false,
				changeMonth: false,
				changeYear: false,
				onSelect: function (date) {
					scope.moveDate = date;
				},
				beforeShow: function () {
					beforeShowCal();
				},
				beforeShowDay: function (date) {
					return checkDate(date);
				}
			});

			function checkDate(date) {
				var day = (date.getDate().toString()).slice(-2);

				if (day < 10) {
					day = '0' + day;
				}

				var month = ((date.getMonth() + 1).toString()).slice(-2);
				if (month < 10) {
					month = '0' + month;
				}

				var year = date.getFullYear();
				var dates = year + '-' + month + '-' + day;
				if (calendar.hasOwnProperty(dates)) {
					return [true, 'discount', 'Busy'];
				} else {
					return [true, '', ''];
				}
			}

			function beforeShowCal() {
				setTimeout(function () {
					var buttonPane = element
						.datepicker("widget")
						.find(".ui-datepicker-buttonpane");

					buttonPane.find(".ui-datepicker-current").hide();
					buttonPane.find(".ui-datepicker-close").hide();
				}, 1);
			}

			function getRequestSchedule(date) {
				$datePickerBlock.addClass('disabled');
				calendar = {};

				var data = {
					date: MomentWrapperService.getZeroMoment(date.dateFrom).unix(),
					condition: {
						field_approve: [3]
					}
				};

				let apiRequest = datacontext.getFieldData().is_common_branch_truck ? moveBoardApi.branchingTruck.getDispatchCalendarByMonth : moveBoardApi.requests.getDispatchCalendarByMonth;

				apiService.postData(apiRequest, data)
					.then(function (response) {
						angular.forEach(response.data, function (item, date) {
							if (item) {
								calendar[date] = '3';
							}
						});

						element.datepicker('refresh');

						$datePickerBlock.removeClass('disabled');
					}, function (reason) {
						logger.error(reason, reason, "Error");
					});
			}

			getRequestSchedule(getDate());

			function getDate(dir) {
				var pDate = moment(dirDate, 'MMM D, YYYY');
				if (!!dir) {
					if (dir > 0) {
						dirDate = moment(pDate).add(1, 'month').startOf('month').format('MMM D, YYYY');
					}
					else {
						dirDate = moment(pDate).subtract(1, 'month').startOf('month').format('MMM D, YYYY');
					}
				}
				//get last 5 month 5-x-5
				startDate = moment(dirDate, 'MMM D, YYYY').startOf('month').format('MM/DD/YYYY');
				endDate = moment(dirDate, 'MMM D, YYYY').endOf('month').format('MM/DD/YYYY');

				return {
					dateFrom: startDate,
					dateTo: endDate
				}
			}

			$(document).on('click', '.ui-datepicker-next', function () {
				var date = getDate(1);
				getRequestSchedule(date);
			});
			$(document).on('click', '.ui-datepicker-prev', function () {
				var date = getDate(-1);
				getRequestSchedule(date);
			});
		}

	}

})();




