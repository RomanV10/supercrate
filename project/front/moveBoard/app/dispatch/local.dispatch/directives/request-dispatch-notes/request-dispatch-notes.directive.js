'use strict';

import './request-dispatch-notes.styl';
import './request-dispatch-notes-popover.styl';

angular
	.module('app.dispatch')
	.directive('requestDispatchNotes', requestDispatchNotes);

function requestDispatchNotes() {
	return {
		restrict: 'AE',
		template: require('./request-dispatch-notes.html'),
		scope: {
			nid: '<',
			branch: '<',
		},
		controller: 'RequestDispatchNotesController',
		controllerAs: 'vm',
	};
}
