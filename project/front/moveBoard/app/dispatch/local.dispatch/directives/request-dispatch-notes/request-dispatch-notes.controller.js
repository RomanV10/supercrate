'use strict';

angular
	.module('app.dispatch')
	.controller('RequestDispatchNotesController', RequestDispatchNotesController);

/* @ngInject */
function RequestDispatchNotesController($scope, EditRequestServices, $q, datacontext) {
	const LOCAL_DISPATCH = 'dispatch_notes';
	const SALES = 'sales_notes';
	const LOADING_NOTE_TEXT = 'Loading...';
	const DISPATCH_NOTE_IS_EMPTY = 'Dispatch note is empty';
	const SALES_NOTE_IS_EMPTY = 'Sales note is empty';
	const SALES_NOTE_TITLE = '<div class="request-dispatch-notes-popover__notes-title">Sales Note</div>';
	const DISPATCH_NOTE_TITLE = '<div class="request-dispatch-notes-popover__notes-title">Dispatch Note</div>';
	let fieldData = datacontext.getFieldData();
	let branches = fieldData.branches || [];

	let vm = this;
	let getNotesPromise;
	vm.busy = false;
	vm.notes = wrapSalesNote(LOADING_NOTE_TEXT) + wrapDispatchNote(LOADING_NOTE_TEXT);

	$scope.$on('onUpdateDispatchNote', onUpdateDispatchNote);
	$scope.$watch('nid', onChangeNid);

	vm.loadNotes = loadNotes;

	function onChangeNid() {
		getNotesPromise = false;
	}

	function loadNotes() {
		let data = {
			role_note: [
				LOCAL_DISPATCH,
				SALES,
			]
		};

		vm.busy = true;

		if (!getNotesPromise) {
			if ($scope.branch) {
				getNotesPromise = EditRequestServices.getBranchNotes($scope.nid, data, getBranchApi());
			} else {
				getNotesPromise = EditRequestServices.getNotes($scope.nid, data).requestNotes;
			}
		}

		getNotesPromise
			.then(({data: {dispatch_notes, sales_notes}}) => {
				vm.notes = wrapSalesNote(sales_notes || SALES_NOTE_IS_EMPTY) + wrapDispatchNote(dispatch_notes || DISPATCH_NOTE_IS_EMPTY);
			})
			.catch(() => {
				vm.notes = wrapDispatchNote('Error when load notes. Please reload page!');
				getNotesPromise = undefined;
			})
			.finally(() => {
				vm.busy = false;
			});
	}

	function getBranchApi() {
		let api;
		let currentBranch = _.find(branches, {front_url: $scope.branch.url});

		if (currentBranch) {
			api = currentBranch.api_url;
		}

		return api;
	}

	function onUpdateDispatchNote(_, {nid, notes: {dispatch_notes = DISPATCH_NOTE_IS_EMPTY, sales_notes = SALES_NOTE_IS_EMPTY}}) {
		if (nid != $scope.nid) return;

		let result = {
			data: {
				dispatch_notes: dispatch_notes,
				sales_notes: sales_notes,
			}
		};
		getNotesPromise = $q.resolve(result);
		vm.notes = wrapSalesNote(sales_notes) + wrapDispatchNote(dispatch_notes);
	}

	function wrapSalesNote(note) {
		return SALES_NOTE_TITLE + wrapNote(note);
	}

	function wrapDispatchNote(note) {
		return DISPATCH_NOTE_TITLE + wrapNote(note);
	}

	function wrapNote(note) {
		return `<div>${note}</div>`;
	}
}
