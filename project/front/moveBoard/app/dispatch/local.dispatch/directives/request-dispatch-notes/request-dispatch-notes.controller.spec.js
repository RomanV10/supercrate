describe('request dispatch notes: controller', () => {
	let controller;
	let $scope;
	let EditRequestServices;
	let result;
	let expectedResult;

	beforeEach(inject((_EditRequestServices_) => {
		EditRequestServices = _EditRequestServices_;
		$scope = $rootScope.$new();
		$scope.nid = '1111';

		controller = $controller('RequestDispatchNotesController', {$scope});
	}));

	describe('load note', () => {
		it('get Notes to have been called for load dispatch notes only', () => {
			// given
			spyOn(EditRequestServices, 'getNotes')
				.and
				.returnValue({requestNotes: $q.resolve()});
			expectedResult = [ '1111', { role_note : [ 'dispatch_notes', 'sales_notes' ] } ];

			// when
			controller.loadNotes();

			//then
			expect(EditRequestServices.getNotes).toHaveBeenCalledWith(...expectedResult);
		});

		it('get Notes after success response should set dispatch_notes', () => {
			// given
			spyOn(EditRequestServices, 'getNotes')
				.and
				.returnValue({requestNotes: $q.resolve({data: {dispatch_notes: ''}})});
			expectedResult = '<div class="request-dispatch-notes-popover__notes-title">Dispatch Note</div><div>Dispatch note is empty</div>';

			// when
			controller.loadNotes();
			$timeout.flush();

			//then
			expect(controller.dispatch_notes).toEqual(expectedResult);
		});

		it('get Notes after success response should copy not empty dispatch_notes', () => {
			// given
			spyOn(EditRequestServices, 'getNotes')
				.and
				.returnValue({requestNotes: $q.resolve({data: {dispatch_notes: 'It works'}})});
			expectedResult = '<div class="request-dispatch-notes-popover__notes-title">Dispatch Note</div><div>It works</div>';

			// when
			controller.loadNotes();
			$timeout.flush();

			//then
			expect(controller.dispatch_notes).toEqual(expectedResult);
		});
	});
});
