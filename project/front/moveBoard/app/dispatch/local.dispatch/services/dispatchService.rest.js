(function () {
    'use strict';

    angular
        .module('app.dispatch')
        .factory('DispatchService', DispatchService);

    DispatchService.$inject = ['$q', '$http', 'config', 'MomentWrapperService', 'common', 'apiService', 'moveBoardApi'];

    function DispatchService($q, $http, config, MomentWrapperService, common, apiService, moveBoardApi) {
        return {
            getRequestsByDate: getRequestsByDate,
            getRequestContract: getRequestContract,
            getUsersByRole: getUsersByRole,
            updateRequest: updateRequest,
            getAllRequestsMonthSchedule: getAllRequestsMonthSchedule,
            setRequestContract: setRequestContract,
            flatRateRequestArr: flatRateRequestArr,
	        assignTeam,
        };

        function setRequestContract(data, nid) {
            var deferred = $q.defer();
            var httpSignature = {
                nid: nid,
                value: data
            };
            $http.post(config.serverUrl + 'server/move_request/set_contract', httpSignature)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function getRequestsByDate(filtering) {
            var deferred = $q.defer();
            $http.post(config.serverUrl + 'server/move_request/search', filtering)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function getRequestContract(nid) {
            var deferred = $q.defer();

            apiService.postData(`${moveBoardApi.requests.getRequestContract}${nid}`)
	            .then(function (response) {
		            deferred.resolve(response.data);
	            }, function (error) {
		            deferred.reject(error.data);
	            });

            return deferred.promise;
        }

        function getUsersByRole(filtering) {
            var deferred = $q.defer();
            $http.post(config.serverUrl + 'server/move_users_resource/get_user_by_role', filtering)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function getAllRequestsMonthSchedule(statuses, from, to, callback) {
            var deferred = $q.defer();
            var data = {
                pagesize: 1000,
                condition: {
                    field_approve: statuses
                },
                filtering: {
                    field_date_from: MomentWrapperService.getZeroMoment(from).startOf('day').unix(),
                    field_date_to: MomentWrapperService.getZeroMoment(to).endOf('day').unix()
                }
            };

            $http.post(config.serverUrl + 'server/move_request/search', data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function updateRequest(nid, request) {

            var data = {};

            data.data = request;
            var deferred = $q.defer();
            $http
                .put(config.serverUrl + 'server/move_request/' + nid, data)
                .success(function (data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        function assignTeam(nid, team) {
            let data = {
                nid,
                data: team,
            };

            return apiService.postData(moveBoardApi.dispatch.assignTeam, data);
        }

        function flatRateRequestArr(flatRateObj, moveDate) {
            var data = [];
            _.forEach(flatRateObj, function (requests, day) {
                _.forEach(requests, function (request, k) {
                    let startDate = moment(request.delivery_date_from.value, 'DD-M-YYYY').format('YYYY-MM-DD'),
                        endDate = moment(request.delivery_date_to.value, 'DD-M-YYYY').format('YYYY-MM-DD'),
                        date = moment(moveDate, 'MMM DD, YYYY').format('YYYY-MM-DD');
                    if (request.status.raw == 3 && (!moveDate || moment(date).isBetween(startDate, endDate, null, '[]') || date == endDate || date == startDate)) {
                        request.flatRate = true;
                        if (request.start_time1.value) {
                            request.start_time1.raw = common.convertTime(request.start_time1.value);
                        }
                        if (!_.find(data, {nid: request.nid})) {
                            data.push(request);
                        }
                    }
                });
            });
            return data;
        }

    }

})();
