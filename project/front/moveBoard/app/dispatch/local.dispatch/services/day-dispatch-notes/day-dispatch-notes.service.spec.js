describe('day dispatch notes service', () => {
	let dayDispatchNoteService;

	beforeEach(inject((_dayDispatchNoteService_) => {
		dayDispatchNoteService = _dayDispatchNoteService_;
	}));

	describe('on init', () => {
		it('getNoteByDay should be defined', () => {
			expect(dayDispatchNoteService.getNoteByDay).toBeDefined();
		});

		it('saveNoteByDay should be defined', () => {
			expect(dayDispatchNoteService.saveNoteByDay).toBeDefined();
		});
	});
});