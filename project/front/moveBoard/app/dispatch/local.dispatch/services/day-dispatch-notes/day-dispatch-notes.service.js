angular.module('app.dispatch')
	.service('dayDispatchNoteService', dayDispatchNoteService);

/* @ngInject */
function dayDispatchNoteService(moveBoardApi, apiService) {
	const DATE_FORMAT = 'MMM D, YYYY';
	const DATE_FORMAT_FOR_BACKEND = 'YYYY-MM-DD';

	let service = {
		getNoteByDay,
		saveNoteByDay,
	};

	return service;

	function getNoteByDay(day) {
		let date = moment(day, DATE_FORMAT).format(DATE_FORMAT_FOR_BACKEND);

		return apiService.postData(moveBoardApi.requestNotes.getParklotNotes, {date})
			.then(({data}) => {
				if (!data || _.isArray(data) && !data[0]) {
					return '';
				}

				if (_.isObject(data)) {
					return data.note;
				}

				if (_.isString(data)) {
					return data;
				}

				throw new Error('Day dispatch service: cannot receive dispatch note');
			});
	}

	function saveNoteByDay(day, note) {
		let date = moment(day, DATE_FORMAT).format(DATE_FORMAT_FOR_BACKEND);
		let data = {
			note,
			date,
		};

		return apiService.postData(moveBoardApi.requestNotes.setParklotNotes, data);
	}
}