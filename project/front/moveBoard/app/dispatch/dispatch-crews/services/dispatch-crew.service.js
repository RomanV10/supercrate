'use strict';

import {DISPATCH_REQUEST_PATH} from "../../constants/dispatch-request-path.constants";

angular
	.module('dispatch-crews')
	.factory('dispatchCrewService', dispatchCrewService);

/*@ngInject*/
function dispatchCrewService($q, apiService, moveBoardApi, datacontext, DISPATCH_INITIAL_SETTINGS) {
	const isCrewAlwaysAvailable = DISPATCH_INITIAL_SETTINGS.isCrewAlwaysAvailable;
	const IS_CREW_ALWAYS_AVAILABLE = _.get(datacontext.getFieldData(), 'localDispatchSettings.isCrewAlwaysAvailable', isCrewAlwaysAvailable);
	const SERVICE_TYPES = _.get(datacontext.getFieldData(), 'enums.request_service_types', {});
	const DELIVERY_CREW_TYPE = 'deliveryCrew';

	return {
		addOneTeam,
		getWorker,
		getAllCrews,
		removeCrew,
		addAll,
		buildCrew,
		checkCrewAvailable,
		checkWorkerAvailable,
		updateLockedCrews,
		getLockedForemen,
		getLockedHelpers,
		lockBusyForemen,
		IS_CREW_ALWAYS_AVAILABLE,
	};

	function getLockedHelpers(currentRequest, requests = [], currentDate) {
		let result = [];
		let currentStartTimeRaw = _.get(currentRequest, 'start_time1.raw');
		let currentNid = _.get(currentRequest, 'nid');

		let requestForCheck = requests.filter((request) => {
			const JOB_IS_DONE = currentRequestIsClosed(request);
			let isSkipThisRequest = !currentNid || currentNid === request.nid;
			let isNotInterestedRequest = isSkipThisRequest || JOB_IS_DONE;

			return !isNotInterestedRequest;
		});

		_.forEach(requestForCheck, request => {
			let requestStartTimeRaw = _.get(request, 'start_time1.raw');
			let requestWorkerCollection = _.get(request, DISPATCH_REQUEST_PATH.CREW_WORKER_COLLECTION, []);

			let isSamePickupDate = moment(request.date.value, 'MM/DD/YYYY')
				.isSame(currentDate, 'day');

			let pickupCrewHelpers = _.get(request, DISPATCH_REQUEST_PATH.PICK_UP_CREWS_HELPERS, []);

			if (isSamePickupDate && !_.isEmpty(pickupCrewHelpers)) {
				requestWorkerCollection.push(...pickupCrewHelpers);
			}

			if (currentStartTimeRaw === requestStartTimeRaw && !_.isEmpty(requestWorkerCollection)) {
				result.push(...requestWorkerCollection);
			}
		});

		// clear duplicates
		result = result.filter((uid, position, self) => self.indexOf(uid) === position);

		return result;
	}

	function lockBusyForemen(lockedWorkers = [], allForemen) {
		_.forEach(allForemen, foreman => {
			foreman.disabled = _.indexOf(lockedWorkers, foreman.uid) > -1;
		});
	}

	function getLockedForemen(currentRequest, requests = [], currentDate) {
		let result = [];
		let currentStartTimeRaw = _.get(currentRequest, 'start_time1.raw');
		let currentNid = _.get(currentRequest, 'nid');

		let requestForCheck = requests.filter((request) => {
			const JOB_IS_DONE = currentRequestIsClosed(request);
			let requestStartTimeRaw = _.get(request, 'start_time1.raw');
			let isDifferentTime = currentStartTimeRaw !== requestStartTimeRaw;
			let isSkipThisRequest = !currentNid || currentNid === request.nid;
			let isNotInterestedRequest = isDifferentTime || isSkipThisRequest || JOB_IS_DONE;

			return !isNotInterestedRequest;
		});

		_.forEach(requestForCheck, request => {
			let foremanId = _.get(request, DISPATCH_REQUEST_PATH.CREWS_FOREMAN);
			let pickupForemanId = _.get(request, DISPATCH_REQUEST_PATH.PICK_UP_CREWS_FOREMAN);

			let isSamePickupDate = moment(request.date.value, 'MM/DD/YYYY')
				.isSame(currentDate, 'day');

			if (foremanId) {
				result.push(foremanId);
			}

			if (isSamePickupDate && pickupForemanId) {
				result.push(pickupForemanId);
			}
		});

		return result;
	}

	function updateLockedCrews(crews, unavailableHelpers) {
		_.forEach(crews, crew => {
			let lockedForeman = _.indexOf(unavailableHelpers, crew.foreman.uid) > -1;
			let lockedHelpers = _.intersection(unavailableHelpers, crew.helpers).length > 0;
			crew.disabled = lockedForeman || lockedHelpers;
		});
	}

	function buildCrew(preset, foremen) {
		let chosenForeman = _.find(foremen, {
			uid: _.get(preset, 'foreman')
		});

		if (!chosenForeman) return;

		return {
			foreman: {
				uid: _.get(chosenForeman, 'uid')
			},
			helpers: _.get(preset, 'workersCollection'),
			name: _.get(chosenForeman, 'name'),
		};
	}

	function getWorker(fullWorkerData) {
		return {
			name: _.get(fullWorkerData, 'name'),
			uid: _.get(fullWorkerData, 'uid'),
		};
	}

	function addOneTeam(data) {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.dispatchCrews.create, {
			data
		})
			.then(resolve => {
				let newCrew = prepareCrewForEdit(resolve.data.data);
				defer.resolve(newCrew);
			})
			.catch(error => rejected(error, defer));

		return defer.promise;
	}

	function getAllCrews() {
		let defer = $q.defer();
		apiService.getData(moveBoardApi.dispatchCrews.all)
			.then(resolve => optimisticResponse(resolve, defer))
			.catch(error => rejected(error, defer));

		return defer.promise;
	}

	function removeCrew(id) {
		let defer = $q.defer();

		if (!id) return defer.reject();

		apiService.delData(`${moveBoardApi.dispatchCrews.all}/${id}`)
			.then(resolve => simpleOptimisticResponse(resolve, defer))
			.catch(error => rejected(error, defer));

		return defer.promise;
	}

	function addAll(data) {
		let defer = $q.defer();

		apiService.postData(moveBoardApi.dispatchCrews.addAll, {data})
			.then(resolve => optimisticResponse(resolve, defer))
			.catch(error => rejected(error, defer));

		return defer.promise;
	}

	function checkCrewAvailable(crew, request, crewInfo) {
		let defer = $q.defer();

		if (!_.get(crew, 'foreman.uid') || !_.get(crew, 'helpers')) crew = convertCrewToChecking(crew);
		let payloadData = prepareDataFotChecking(crew, request, crewInfo);
		checkAvailable(defer, payloadData);

		return defer.promise;
	}

	function checkWorkerAvailable(worker, request, crewInfo) {
		let defer = $q.defer();
		let payloadData = prepareWorkerForChecking(worker, request, crewInfo);
		checkAvailable(defer, payloadData);

		return defer.promise;
	}

	// Private methods
	function currentRequestIsClosed(request) {
		let isCompleted = !!_.get(request, 'field_company_flags.value[3]', false);
		let isPickUpCompleted = !!_.get(request, 'field_company_flags.value[5]', false);
		let isDeliveryCompleted = !!_.get(request, 'field_company_flags.value[6]', false);
		let isFlatRateLocalMove = _.get(request, 'field_flat_rate_local_move.value', false);
		return +request.service_type.raw !== SERVICE_TYPES.FLAT_RATE || isFlatRateLocalMove
			? isCompleted
			: isPickUpCompleted && isDeliveryCompleted;
	}

	function checkAvailable(defer, payloadData) {
		if (IS_CREW_ALWAYS_AVAILABLE) {
			defer.resolve();
		} else {
			apiService.postData(moveBoardApi.dispatchCrews.isAvailable, payloadData)
				.then(response => optimisticCheckWorkerResponse(response, defer))
				.catch(error => rejected(error, defer));
		}
	}

	function optimisticCheckWorkerResponse(resolve, defer) {
		let notAvailableWorker = _.get(resolve, 'data.data', []);
		if (_.isEmpty(notAvailableWorker)) {
			defer.resolve();
		} else {
			defer.reject();
		}
	}

	function optimisticResponse(resolve, defer) {
		let allCrews = _.get(resolve, 'data.data', []);
		let response = [];
		_.forEach(allCrews, crew => {
			response.push(prepareCrewForEdit(crew));
		});
		defer.resolve(response);
	}

	function simpleOptimisticResponse(resolve, defer) {
		let response = _.get(resolve, 'data.data');
		if (response) {
			defer.resolve();
		} else {
			defer.reject();
		}
	}

	function rejected(error, defer) {
		defer.reject(error.data);
	}

	function prepareWorkerForChecking(worker, requestData, crewInfo = {}) {
		let result = {
			users_ids: [worker.uid],
			date: moment(requestData.date.value).format('YYYY-MM-DD'),
			start_time: moment(requestData.start_time1.value, 'HH:mm A').format('hh:mm A'),
			nid_to_exclude: requestData.nid,
		};

		if (crewInfo.crewType == DELIVERY_CREW_TYPE) {
			result.type = 1;
			result.date = crewInfo.currendDate.format('YYYY-MM-DD');
		}

		return result;
	}

	function prepareDataFotChecking(crew, requestData, crewInfo = {}) {
		let workersUIDs = [crew.foreman.uid, ...crew.helpers].filter(uid => uid.length);
		let result = {
			users_ids: workersUIDs,
			date: moment(requestData.date.value).format('YYYY-MM-DD'),
			start_time: moment(requestData.start_time1.value, 'HH:mm A').format('hh:mm A'),
			nid_to_exclude: requestData.nid,
		};

		if (crewInfo.crewType == DELIVERY_CREW_TYPE) {
			result.type = 1;
			result.date = crewInfo.currendDate.format('YYYY-MM-DD');
		}

		return result;
	}

	function convertCrewToChecking(crew) {
		return {
			foreman: {
				uid: _.get(crew, 'foreman'),
			},
			helpers: _.get(crew, 'workersCollection', []),
		};
	}

	function prepareCrewForEdit(crew) {
		let helpers = _.get(crew, 'helpers', []);
		return {
			foreman: _.get(crew, 'foreman'),
			helpers: helpers.map(helper => helper.uid),
			name: crew.name,
			isChanging: false,
			team_id: crew.team_id,
		};
	}
}
