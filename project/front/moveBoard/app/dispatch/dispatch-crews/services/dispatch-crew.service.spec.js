describe('dispatch-crew.service.js:', () => {
	let DISPATCH_INITIAL_SETTINGS;
	let dispatchCrewService;
	let crews;
	let currentRequest;
	let foremen;
	let requests;
	let result;

	beforeEach(inject((_DISPATCH_INITIAL_SETTINGS_, _dispatchCrewService_) => {
		DISPATCH_INITIAL_SETTINGS = _DISPATCH_INITIAL_SETTINGS_;
		dispatchCrewService = _dispatchCrewService_;

		crews = testHelper.loadJsonFile('crews-dispatch-crew.service.mock');
		currentRequest = testHelper.loadJsonFile('current-request-dispatch-crew.service.mock');
		foremen = testHelper.loadJsonFile('foremen-dispatch-crew.service.mock');
		requests = testHelper.loadJsonFile('requests-dispatch-crew.service.mock');
	}));

	describe('on init:', () => {
		it('DISPATCH_INITIAL_SETTINGS have to be defined', () => {
			expect(DISPATCH_INITIAL_SETTINGS).toBeDefined();
		});

		it('dispatchCrewService have to be defined', () => {
			expect(dispatchCrewService).toBeDefined();
		});

		it('dispatchCrewService.addOneTeam have to be defined', () => {
			expect(dispatchCrewService.addOneTeam).toBeDefined();
		});

		it('dispatchCrewService.getWorker have to be defined', () => {
			expect(dispatchCrewService.getWorker).toBeDefined();
		});

		it('dispatchCrewService.getAllCrews have to be defined', () => {
			expect(dispatchCrewService.getAllCrews).toBeDefined();
		});

		it('dispatchCrewService.removeCrew have to be defined', () => {
			expect(dispatchCrewService.removeCrew).toBeDefined();
		});

		it('dispatchCrewService.addAll have to be defined', () => {
			expect(dispatchCrewService.addAll).toBeDefined();
		});

		it('dispatchCrewService.buildCrew have to be defined', () => {
			expect(dispatchCrewService.buildCrew).toBeDefined();
		});

		it('dispatchCrewService.checkCrewAvailable have to be defined', () => {
			expect(dispatchCrewService.checkCrewAvailable).toBeDefined();
		});

		it('dispatchCrewService.checkWorkerAvailable have to be defined', () => {
			expect(dispatchCrewService.checkWorkerAvailable).toBeDefined();
		});

		it('dispatchCrewService.updateLockedCrews have to be defined', () => {
			expect(dispatchCrewService.updateLockedCrews).toBeDefined();
		});

		it('dispatchCrewService.getLockedForemen have to be defined', () => {
			expect(dispatchCrewService.getLockedForemen).toBeDefined();
		});

		it('dispatchCrewService.getLockedHelpers have to be defined', () => {
			expect(dispatchCrewService.getLockedHelpers).toBeDefined();
		});

		it('dispatchCrewService.IS_CREW_ALWAYS_AVAILABLE have to be defined', () => {
			expect(dispatchCrewService.IS_CREW_ALWAYS_AVAILABLE).toBeDefined();
		});

		it('lockBusyForemen to be defined', () => {
			expect(dispatchCrewService.lockBusyForemen).toBeDefined();
		});
	});

	describe('getLockedHelpers:', () => {
		it('when detect same helper in another request, should return locked helper ids', () => {
			// given
			let currentDay = '07/15/2018';
			let expectedResult = ['5937'];
			// when
			result = dispatchCrewService.getLockedHelpers(currentRequest, requests, currentDay);
			// then
			expect(result).toEqual(expectedResult);
		});
	});

	describe('getLockedForemen:', () => {
		let expectedResultHelpers;

		beforeEach(() => {
			expectedResultHelpers = ['5935'];
		});

		describe('when detect locked worker', () => {
			it('should return locked foreman ids', () => {
				//given
				let currentDay = '07/15/2018';

				//when
				result = dispatchCrewService.getLockedForemen(currentRequest, requests, currentDay);
				// then
				expect(result).toEqual(expectedResultHelpers);
			});
		});

		describe('when don\'t detect locked worker', () => {
			beforeEach(() => {
				currentRequest.start_time1.raw = 37;
			});

			it('should return locked foreman ids', () => {
				//given
				let expectedResult = [];

				//when
				result = dispatchCrewService.getLockedForemen(currentRequest, requests);

				// then
				expect(result).toEqual(expectedResult);
			});
		});
	});

	describe('updateLockedCrews:', () => {
		it('when has locked crews should update crews parameter', () => {
			// given
			let expectedLockedCrews = testHelper.loadJsonFile('getLockedCrews-dispatch-crew.service.mock');
			// when
			dispatchCrewService.updateLockedCrews(crews, ['6151']);
			// then
			expect(crews).toEqual(expectedLockedCrews);
		});
	});

	describe('lockBusyForemen', () => {
		let expectedResultForeman;

		beforeEach(() => {
			expectedResultForeman = testHelper.loadJsonFile('foremen-updated-with-locked-dispatch-crew.service.mock');
		});

		it('should disable locked foreman', () => {
			//given
			let workers = ['5935'];

			//when
			dispatchCrewService.lockBusyForemen(workers, foremen);

			//then
			expect(foremen).toEqual(expectedResultForeman);
		});

		it('should disable nothing when undefined workers', () => {
			//given
			let workers = undefined;
			expectedResultForeman[0].disabled = false;

			//when
			dispatchCrewService.lockBusyForemen(workers, foremen);

			//then
			expect(foremen).toEqual(expectedResultForeman);
		});
	});
});
