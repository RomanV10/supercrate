'use strict';

import './oi-select.styl';
import './crew-modal.styl';

angular
	.module('dispatch-crews')
	.controller('CrewModalController', CrewModalController);

/*@ngInject*/

function CrewModalController($scope, $uibModalInstance, availableHelpers, availableForemen, dispatchCrewService, SweetAlert, crews) {
	const EMPTY_CREW_PRESET = {
		foreman: {},
		helpers: [],
		isChanging: false,
		name: 'Please choose foreman',
		isNew: true,
	};
	$scope.availableForemen = [];
	$scope.availableHelpers = [];
	$scope.crews = crews.map(crew => {
		if (!crew.team_id) crew.isNew = true;
		return crew;
	});
	$scope.unsavedChanges = false;
	$scope.unsavedNewCrew = false;

	$scope.closeCrewManagementModal = closeCrewManagementModal;
	$scope.changeCrewTitle = changeCrewTitle;
	$scope.showCrewMembers = showCrewMembers;
	$scope.addCrew = addCrew;
	$scope.updateCrews = updateCrews;
	$scope.removeCrew = removeCrew;
	$scope.saveAllCrews = saveAllCrews;

	function init() {
		initWorkersArray($scope.availableForemen, availableForemen);
		initWorkersArray($scope.availableHelpers, availableHelpers);

		$scope.$watch('activeCrew', prepareNewCrewName, true);
	}

	function initWorkersArray(visible ,available) {
		_.forEach(available, worker => {
			visible.push(dispatchCrewService.getWorker(worker));
		});
	}

	function changeCrewTitle(crew) {
		crew.isChanging = !crew.isChanging;
		if (crew.isNew) crew.isNew = false;
	}

	function showCrewMembers(crew) {
		$scope.activeCrew = crew;
	}

	function addCrew() {
		if ($scope.unsavedNewCrew) {
			SweetAlert.swal({
				title: 'You have unsaved new crew',
				text: 'Please save changes before add new crew',
				type: 'warning'
			});
			return;
		}

		let crew = angular.copy(EMPTY_CREW_PRESET);
		$scope.crews.push(crew);
		$scope.activeCrew = _.findLast($scope.crews);
		$scope.unsavedNewCrew = true;
	}

	function updateCrews() {
		let chosenForeman = _.get($scope.activeCrew, 'foreman.uid');
		let foremanAsHelperIndex = _.findIndex($scope.availableHelpers, {
			uid: chosenForeman
		});
		if (chosenForeman && ~foremanAsHelperIndex) {
			$scope.availableHelpers.splice(foremanAsHelperIndex, 1);
		}

		let currentIndex = _.findIndex($scope.crews, crew => crew.team_id === $scope.activeCrew.team_id);
		$scope.crews[currentIndex] = $scope.activeCrew;
		$scope.unsavedChanges = true;
	}

	function removeCrew(index) {
		let currentCrew = _.get($scope.crews, `${[index]}.name`);
		let teamID = _.get($scope.crews, `${index}.team_id`);
		if (!teamID) return;
		SweetAlert.swal({
			title: `${currentCrew} crew will be removed`,
			text: 'Are you sure to remove this crew?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#64C564',
			confirmButtonText: 'Confirm',
			closeOnConfirm: true
		}, confirm => spliceCrew(confirm, index, teamID));
	}

	function spliceCrew(confirm, index, teamID) {
		if (!confirm) return;
		$scope.busy = true;
		dispatchCrewService.removeCrew(teamID)
			.then(() => {
				$scope.crews.splice(index, 1);
				let currentLastCrew = _.findLast($scope.crews);
				if (currentLastCrew) {
					$scope.activeCrew = currentLastCrew;
				} else {
					delete $scope.activeCrew;
				}
			})
			.finally(() => $scope.busy = false);
	}

	function closeCrewManagementModal() {
		if ($scope.unsavedChanges) {
			SweetAlert.swal({
				title: 'You have unsaved changes',
				text: 'Do you want to save?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#64C564',
				confirmButtonText: 'Confirm',
				closeOnConfirm: true
			}, saveAllAndClose);
		}

		$uibModalInstance.close({
			crews: $scope.crews
		});
	}

	function saveAllAndClose(confirm) {
		if (!confirm) return;
		saveAllCrews();
	}

	function saveAllCrews() {
		let emptyForemen = validateCrews();
		if (!_.isEmpty(emptyForemen)) {
			toastr.error(`Crews have no foremen: ${emptyForemen}`);
			return;
		}
		$scope.busy = true;

		dispatchCrewService.addAll($scope.crews)
			.then(updatedCrews => {
				$scope.crews = updatedCrews;
			})
			.finally(() => {
				$scope.busy = false;
				$scope.unsavedChanges = false;
				$scope.unsavedNewCrew = false;
			});
	}

	function validateCrews() {
		let problemInCrews = '';
		_.forEach($scope.crews, crew => {
			if (_.isEmpty(crew.foreman)) problemInCrews += `${crew.name}`;
		});
		return problemInCrews;
	}

	function prepareNewCrewName(current) {
		let foremanUID = _.get(current, 'foreman.uid');
		if (_.get(current, 'isNew') && foremanUID) {
			let foremanName = _.find($scope.availableForemen, foreman => foreman.uid === foremanUID).name;
			current.name = angular.copy(foremanName);
			current.isNew = false;
		}
	}

	init();
}
