'use strict';

import '../crew-modal/oi-select.styl';
import './conflict-crew.modal.styl';

angular
	.module('dispatch-crews')
	.controller('ConflictCrewModal', ConflictCrewModal);

/*@ngInject*/
function ConflictCrewModal($scope, maxLength, crewHelpers, helpers, $uibModalInstance) {
	$scope.crewHelpers = crewHelpers;
	$scope.maxLength = maxLength;
	$scope.resolvedConflict = [];
	$scope.crewHelpers = [];

	$scope.resolveAndClose = resolveAndClose;
	$scope.closeConflictModal = closeConflictModal;

	_.forEach(crewHelpers, (helperUID) => {
		let helper = _.find(helpers, {uid: helperUID});
		$scope.crewHelpers.push(helper);
	});

	function resolveAndClose() {
		$uibModalInstance.close({
			helpers: $scope.resolvedConflict,
		});
	}

	function closeConflictModal() {
		$uibModalInstance.dismiss('close');
	}

	$scope.$watch('resolvedConflict.length', current => {
		if (current === $scope.maxLength) $scope.resolved = true;
		else $scope.resolved = false;
	});
}
