'use strict';
angular
	
	.module('app.dispatch')
	.controller('OpenedJobs', OpenedJobs);

OpenedJobs.$inject = ['$scope', 'RequestServices'];

function OpenedJobs($scope, RequestServices) {
	$scope.busy = false;
	$scope.requests = [];
	$scope.dates = {
		from: moment()
			.startOf('month')
			.toDate(),
		to: moment()
			.endOf('month')
			.toDate(),
	};
	$scope.ConfirmedStatus = [3];
	
	$scope.prevMonth = prevMonth;
	$scope.nextMonth = nextMonth;
	$scope.getOpenedJobs = getOpenedJobs;
	
	function prevMonth() {
		$scope.dates.from = moment($scope.dates.from)
			.subtract(1, 'M')
			.startOf('month')
			.toDate();
		$scope.dates.to = moment($scope.dates.from)
			.endOf('month')
			.toDate();

		getOpenedJobs();
	}
	
	function nextMonth() {
		$scope.dates.from = moment($scope.dates.from)
			.add(1, 'M')
			.startOf('month')
			.toDate();
		$scope.dates.to = moment($scope.dates.from)
			.endOf('month')
			.toDate();
		getOpenedJobs();
	}
	
	
	function getOpenedJobs() {
		$scope.busy = true;
		var data = {
			from: moment($scope.dates.from).format('YYYY-MM-DD'),
			to: moment($scope.dates.to).format('YYYY-MM-DD')
		};

		RequestServices.getOpenedJobs(data)
			.then(function (data) {
				$scope.requests.length = 0;
				$scope.totalCount = data.count;
				$scope.requests = data.nodes;
				$scope.busy = false;
			});
	}
	
	getOpenedJobs();
	
}