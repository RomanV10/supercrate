(function () {
    'use strict';
    angular

        .module('app.dispatch')
        .controller('PayrollStatus', PayrollStatus);

    PayrollStatus.$inject = ['$scope','$rootScope', '$uibModal', 'RequestServices', 'datacontext' ];

    function PayrollStatus($scope,$rootScope, $uibModal, RequestServices, datacontext)
    {
    	var importedFlags = $rootScope.availableFlags;
        $scope.busy = false;
        $scope.dates = {
            from: '',
            to: ''
        };
        $scope.dates.from = moment().startOf('month').format('MMM D, YYYY');
        $scope.dates.to = moment().endOf('month').format('MMM D, YYYY');
        $scope.requests = [];
        $scope.ConfirmedStatus = [3];

        $scope.prevMonth = prevMonth;
        $scope.nextMonth = nextMonth;
        $scope.getPayrollStatus = getPayrollStatus;

        function prevMonth(){
            var numMonth = moment(new Date($scope.dates.from)).month();
            var year = moment(new Date($scope.dates.from)).year();
            if(numMonth == 0)
                year--;
            $scope.dates.from = moment().month(numMonth-1).year(year).startOf('month').format('MMM D, YYYY');
            $scope.dates.to = moment().month(numMonth-1).year(year).endOf('month').format('MMM D, YYYY');
            getPayrollStatus();
        }

        function nextMonth(){
            var numMonth = moment(new Date($scope.dates.from)).month();
            var year = moment(new Date($scope.dates.from)).year();
            if(numMonth == 11)
                year++;
            var numMonth = moment(new Date($scope.dates.from)).month();
            $scope.dates.from = moment().month(numMonth+1).year(year).startOf('month').format('MMM D, YYYY');
            $scope.dates.to = moment().month(numMonth+1).year(year).endOf('month').format('MMM D, YYYY');
            getPayrollStatus();
        }


        function getPayrollStatus(){
            $scope.busy = true;
            var data = {
                from: moment(new Date($scope.dates.from)).format('YYYY-MM-DD'),
                to: moment(new Date($scope.dates.to)).format('YYYY-MM-DD')
            };
            RequestServices.getOpenedJobs(data).then(function(data){
                $scope.requests = angular.copy(data.nodes);
                $scope.totalCount = data.count;
                $scope.busy = false;
            })
        }
        getPayrollStatus();

    }
})();
