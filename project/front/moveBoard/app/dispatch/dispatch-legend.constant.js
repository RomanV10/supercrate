export const DISPATCH_LEGEND = [
	{
		name: 'Local Moving',
		style: {
			"background-color": 'lightgreen'
		}
	},
	{
		name: 'Moving & Storage',
		style: {
			"background-color": '#FFC800'
		}
	},
	{
		name: 'Loading and Unloading Help',
		style: {
			"background-color": '#D68FFA'
		}
	},
	{
		name: 'Overnight',
		style: {
			"background-color": '#F17373'
		}
	},
	{
		name: 'Long Distance',
		style: {
			"background-color": "#69B6F8"
		}
	},
	{
		name: 'Flat Rate',
		style: {
			"background-color": "#EEEB90"
		}
	},
	{
		name: 'LD Trip',
		style: {
			"background-color": "#b39ddb"
		}
	},
	{
		name: 'Packing Day',
		style: {
			"background-color": "#D35400"
		}
	},
];
