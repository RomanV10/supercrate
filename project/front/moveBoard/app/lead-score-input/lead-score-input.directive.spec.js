describe('Directive: leadScoreInput', function () {
	var element, scope;
	let $httpBackend,
		moveBoardApi,
		testHelper,
		elementScope,
		$rootScope,
		timeout,
		compile;

	function compileElement(elementHtml) {
		element = compile(angular.element(elementHtml))(scope);
	}

	beforeEach(inject(function (_$compile_, _$rootScope_, _$httpBackend_, _testHelperService_, _moveBoardApi_, _$timeout_) {
		testHelper = _testHelperService_;
		_$rootScope_.fieldData.timeZone = testHelper.loadJsonFile('time-zone.mock');
		$rootScope = _$rootScope_;
		scope = _$rootScope_.$new();
		timeout = _$timeout_;
		compile = _$compile_;
		$httpBackend = _$httpBackend_;
		moveBoardApi = _moveBoardApi_;
		scope.request = testHelper.loadJsonFile('request-with-score.mock');
		scope.leadscoringsettings = testHelper.loadJsonFile('lead-score-settings.mock');
		scope.sendNotificationAfterSaveChanges = function () {};
		compileElement('<lead-score-input request="request" send-notification="sendNotificationAfterSaveChanges" leadscoringsettings="leadscoringsettings"></lead-score-input>');
	}));


	describe('Directive DOM', () => {
		it('should not render empty html', function () {
			expect(element.html()).not.toBe('');
		});

	});
	describe('Directive Scope', () => {
		let elementScope;
		beforeEach(() => {
			elementScope = element.isolateScope();
		});

		it('request on isolated scope should be two-way bound', () => {
			elementScope.request = {
				field_total_score: 20
			};
			elementScope.$apply();

			expect(scope.request.field_total_score).toEqual(20);
		});
		it('leadscoringsettings on isolated scope should be one-way bound', () => {
			elementScope.leadscoringsettings = {
				enabled: false,
				scoring: {
					cold: 0,
					warm: 50,
					hot: 100
				}
			};

			elementScope.$apply();
			expect(scope.leadscoringsettings.enabled).not.toEqual(elementScope.leadscoringsettings.enabled);
		})
	});
	describe('Update score', () => {
		let elementScope;
		beforeEach(() => {
			elementScope = element.isolateScope();
		});
		it('should make request', () => {
			elementScope.request.field_total_score = 20;
			let data = {
				nid: elementScope.request.nid,
				score: elementScope.request.field_total_score
			};
			let response = {
				data: {
					field_total_score: 1
				}
			};
			$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.notifications.update_request_score), data).respond(200, response);
			elementScope.saveLeadScore();
			$httpBackend.flush();
		})
	});
	describe('Get style', () => {
		it('should get style string', () => {
			let elementScope = element.isolateScope();
			var score = 20;
			let style = elementScope.getLeadScoreStyle(score);
			expect(style).toEqual('cold');
		})
	})
});
