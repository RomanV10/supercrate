import momentTimezone from 'moment-timezone';

angular.module('leadScore')
    .directive('leadScoreInput', leadScoreInput);
leadScoreInput.$inject = ['apiService', 'moveBoardApi', 'RequestServices', 'datacontext', '$timeout'];

function leadScoreInput (apiService, moveBoardApi, RequestServices, datacontext, $timeout) {
    return {
	    restrict: 'EA',
        scope: {
	        request: '=',
	        sendNotification: '=',
	        leadscoringsettings: '<'
        },
        template: require('./templates/lead-score-input.tpl.pug'),
	    link: linkFunction
    };
    function linkFunction($scope) {
	    $scope.hideTooltip = hideTooltip;
	    $scope.saveLeadScore = saveLeadScore;
	    $scope.getLeadScoreStyle = getLeadScoreStyle;
	    const TIME_ZONE = datacontext.getFieldData().timeZone;
	    momentTimezone.tz.add(TIME_ZONE.initial);
	    let hideTooltipDelay;
	    let previousLeadScore = $scope.request.field_total_score;

	    function getLeadScoreStyle(score) {
		    let style;
		    switch (true) {
			    case score < $scope.leadscoringsettings.scoring.warm:
				    style = 'cold';
				    break;
			    case (score >= $scope.leadscoringsettings.scoring.warm && score < $scope.leadscoringsettings.scoring.hot):
				    style = 'warm';
				    break;
			    case score >= $scope.leadscoringsettings.scoring.hot:
				    style = 'hot';
				    break;
		    }
		    return style;
	    }

	    function hideTooltip(event) {
		    let input = angular.element(event.currentTarget);
		    var temp = input.val();
		    hideTooltipDelay = $timeout(function(){
			    input.val('').val(temp);
		    }, 0);
		    let tooltip = input.parent().next();
		    if (angular.element(tooltip[0]).hasClass('tooltip')) {
			    angular.element(tooltip[0]).css({display: 'none'});
		    }
	    }

	    function saveLeadScore () {
		    if ($scope.request.field_total_score != previousLeadScore) {
			    if ($scope.request.field_total_score != 'null') {
				    $scope.request.field_total_score = parseInt($scope.request.field_total_score) <= 9999 ? parseInt($scope.request.field_total_score) : 9999;
			    } else {
				    $scope.request.field_total_score = 0
			    }
			    let data = {
				    nid: $scope.request.nid,
				    score: $scope.request.field_total_score
			    };
			    apiService.postData(moveBoardApi.notifications.update_request_score, data).then(response => {
				    if (response.data.field_total_score) {
					    var arr = [{
						    text: "Score points were changed",
						    from: previousLeadScore.toString(),
						    to: $scope.request.field_total_score.toString()
					    }];
					    $scope.sendNotification(arr, $scope.request.nid, 'MOVEREQUEST');
					    RequestServices.sendLogs(arr, 'Request score updated', $scope.request.nid, 'MOVEREQUEST');
					    $scope.request.field_total_score_updated = momentTimezone.tz(TIME_ZONE.name).format('MM/DD/YY h:mm A');
					    previousLeadScore = Number($scope.request.field_total_score);
				    }
			    });
		    } else {
			    $scope.request.field_total_score = previousLeadScore;
		    }
	    }
	    $scope.$on('destroy' ,() => {
		    hideTooltipDelay.cancel();
	    })
	}
}
