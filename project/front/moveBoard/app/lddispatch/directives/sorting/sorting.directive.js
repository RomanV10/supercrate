'use strict';
import './sorting.styl'

angular
	.module('app.lddispatch')
	.directive('sorting', sorting);

sorting.$inject = ['$state'];

function sorting($state) {
	return {
		template: require('./sorting.pug'),
		restrict: 'EA',
		scope:{
			order: '=',
			params: '=',
			getList: '&'
		},
		link: function($scope) {
			$scope.sortBy = () => {
				if ($scope.params.sorting.orderKey === $scope.order) {
					if ($scope.params.sorting.orderValue === 'DESC') {
						$scope.params.sorting.orderValue = 'ASC';
					} else {
						$scope.params.sorting.orderValue = 'DESC';
					}
				} else {
					$scope.params.sorting.orderKey = $scope.order;
					$scope.params.sorting.orderValue = 'ASC';
				}
				$scope.getList();
			}
		}
	};
}
