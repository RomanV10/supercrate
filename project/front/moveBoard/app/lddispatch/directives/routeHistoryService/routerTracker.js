'use strict';

angular
	.module('app.lddispatch')
    .factory('routerTracker', function routerTracker($rootScope, $state) {
        var routeHistory = [];
        	
        var service = {
            getRouteHistory: getRouteHistory
        };
        
        $rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {
            routeHistory.unshift({route: from, routeParams: fromParams});
        });

        function getRouteHistory() {
            return routeHistory;
        }

        return service;       
    });