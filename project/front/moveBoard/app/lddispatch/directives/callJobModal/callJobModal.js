'use strict';
import './callJobModal.styl';

(function () {
  angular
    .module('app.lddispatch')
    .directive('callJobModal', ['tripRequest', function(tripRequest) {

      return {
        template: require('./callJobModal.pug'),
        restrict: 'A',
        scope: {
          id: '=',
          showText: '@'
        },
        link: function($scope) {
          $scope.openRequest = tripRequest.openRequest
          
          angular.element(document).bind('mouseup', function(){
              $scope.mouseClicked = false
            }) 
          }
      }
    }
    ])
})()