'use strict';
import './elromco-select-list.styl'

(function () {
	angular
		.module('app.lddispatch')
		.directive('elromcoSelectList', ['$state', 'moveBoardApi', 'apiService', function($state, moveBoardApi, apiService){
			return {
				template: require('./elromco-select-list.pug'),
				restrict: 'E',
				scope: {
					jobId:"=",
					jobStatus:"="
				},
				link: function($scope) {
					$scope.showArrowIcon = 0;
					$scope.jobStatusArr = [
						{
							id:1,
							type:'DRAFT'
						},
						{
							id:2,
							type:'PENDING TRIP'
						},
						{
							id:3,
							type:'IN ROUTE'
						},
						{
							id:4,
							type:'DELIVERED'
						}
					];

					$scope.changeStatus = (ev, stat) => {
						apiService.postData(moveBoardApi.trip.changeJobStatus, {job_id: $scope.jobId, status: stat}).then(data => {
							$scope.jobStatus = stat;
						});
					}
				}
			};
		}])
})();
