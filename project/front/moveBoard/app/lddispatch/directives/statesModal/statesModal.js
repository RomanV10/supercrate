'use strict';
import './statesModal.styl'

(function () {
  angular
    .module('app.lddispatch')
    .directive('statesModal', ['$state', 'statesService', function($state, statesService) {
      return {
        template: require('./statesModal.pug'),
        restrict: 'EA',
        scope:{
            modalOpened: '=',
            chosenStates: '=',
            getList: '&'
        },
        link: function($scope) {
          $scope.allStates = angular.copy(statesService.states())
          $scope.statesAfterSearch = angular.copy($scope.allStates)
          $scope.stateSearchInput = undefined
          $scope.addState = (item) => {
            $scope.chosenStates.push(item)
            $scope.statesAfterSearch.splice($scope.statesAfterSearch.indexOf(item), 1)
            $scope.allStates.splice($scope.allStates.indexOf(item), 1)

            $scope.chosenStates.sort()
          }

          $scope.deleteState = (item) => {
            $scope.statesAfterSearch.push(item)
            $scope.allStates.push(item)

            $scope.chosenStates.splice($scope.chosenStates.indexOf(item), 1)

            $scope.statesAfterSearch.sort()
            $scope.allStates.sort()
          }

          function compareStrings(state) {
            return state.toUpperCase().indexOf($scope.stateSearchInput.toUpperCase()) != -1
          }
          $scope.searchStates = () => {
            $scope.statesAfterSearch = $scope.allStates.filter(compareStrings)
          }

          $scope.closeModal = () => {
            $scope.stateSearchInput = ''
            $scope.searchStates()
            $scope.modalOpened = false
            $scope.getList()
          }

          $(document).on("mouseup touchstart", function (e) {
            var container = $(".states-modal");
            if (!container.is(e.target)
            && container.has(e.target).length === 0
            && $scope.modalOpened == true) {
              $scope.stateSearchInput = ''
              $scope.searchStates()
              $scope.modalOpened = false
              $scope.getList()
            }
          });

          $scope.$on('removeAllStatesFilters', removeAllStatesFilters);

          function removeAllStatesFilters() {
	          $scope.chosenStates.forEach($scope.deleteState);
          }
        }
      };
    }])
})();
