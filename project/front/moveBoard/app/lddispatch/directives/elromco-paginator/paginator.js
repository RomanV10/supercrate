'use strict';
import './paginator.styl'

(function () {
  angular
    .module('app.lddispatch')
    .directive('elromcoPaginator', ['$state', function($state) {
      return {
        template: require('./paginator.pug'),
        restrict: 'A',
        scope:{
            meta: '=',
            setPage: '='
        },
        link: function($scope) {

        }
      };
    }])
})();
