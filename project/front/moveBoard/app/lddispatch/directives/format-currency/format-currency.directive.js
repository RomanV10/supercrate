'use strict';

angular
	.module('app.lddispatch')
	.directive('format', format);

format.$inject = ['$filter'];

function format($filter) {
	return {
		require: '?ngModel',
		link: function (scope, elem, attrs, ctrl) {
			if (!ctrl) return;

			let tempValue;

			ctrl.$formatters.unshift(function (a) {
				return $filter(attrs.format)(ctrl.$modelValue)
			});

			elem.bind('focus', function (event) {
				tempValue = elem.val();
				elem.val('')
			});

			elem.bind('blur', function (event) {
				if (elem.val() == '') {
					elem.val(tempValue)
				}

				let plainNumber = elem.val().replace(/[^\d|\-+|\.+]/g, '');
				elem.val($filter(attrs.format)(plainNumber))
			})
		}
	}
}

