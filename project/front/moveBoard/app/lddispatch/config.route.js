angular.module("app.lddispatch")
	.config(
		["$stateProvider", function ($stateProvider) {
			$stateProvider
				.state('lddispatch', {
					abstract: true,
					template: '<ui-view/>'
				})
				.state('lddispatch.couriers', {
					url: '/lddispatch/couriers?page=',
					template: '<div carrier-list> </div>',
					title: 'Carriers and Agents',
					params: {
						'page': null,
					},
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.carrierDetails', {
					url: '/lddispatch/couriers/:id?page=',
					template: '<div add-carrier> </div>',
					title: 'Carriers and Agents',
					params: {
						'page': null,
					},
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.trip', {
					url: '/lddispatch/trip-planner?page=',
					template: '<div trip-list></div>',
					title: 'Trip Planner',
					params: {
						'page': null,
					},
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.tripDetails', {
					url: '/lddispatch/trip-planner/:id/:tab?page=',
					template: '<div trip-tabs></div>',
					title: 'Trip',
					params: {
						'tab': null,
						'page': null
					},
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.tpDelivery', {
					url: '/lddispatch/tp-delivery/:id/tp/:tpId/:paymentExist',
					template: '<div tp-delivery></div>',
					title: 'TP Delivery',
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.ld_delivery', {
					url: '/lddispatch/delivery',
					template: '<div delivery-list></div>',
					title: 'Delivery',
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.pick_up', {
					url: '/lddispatch/pick-up',
					template: '<div pickup-list></div>',
					title: 'pickup',
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.sitJobs', {
					url: '/lddispatch/jobs-in-sit',
					template: '<div jobs-in-sit></div>',
					title: 'Jobs In SIT',
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.agentFolio', {
					url: '/lddispatch/agent-folio?page=',
					template: '<div agent-folio-list> </div>',
					title: 'Agent Folio',
					params: {
						'page': null,
					},
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.payments', {
					url: '/lddispatch/payments/:id/:tab',
					template: '<div payment-tabs></div>',
					title: 'Payments',
					params: {'tab': null},
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.sitTpCollected', {
					url: '/lddispatch/trip/:trip_id/receipts/:job_id/:balance/:paymentExist',
					params: {
						userInfo: null
					},
					template: '<div sit-tp-collected></div>',
					title: 'Receipt Details',
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.lddispatchStorages', {
					url: '/lddispatch/storages?page=',
					template: '<div lddispatch-storages> </div>',
					title: 'Storages',
					params: {
						'page': null,
					},
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
				.state('lddispatch.lddispatchStorageCreate', {
					url: '/lddispatch/storages/:id',
					template: '<div lddispatch-storage-create> </div>',
					title: 'Storages',
					params: {
						'page': null,
					},
					data: {
						permissions: {
							except: ['anonymous', 'foreman', 'helper']
						}
					}
				})
		}
		]
	);
