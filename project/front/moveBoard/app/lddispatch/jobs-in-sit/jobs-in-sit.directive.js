'use strict';
import './jobsInSit.styl';
import './sitLog.styl';

angular
	.module('app.lddispatch')
	.directive('jobsInSit', jobsInSit);

jobsInSit.$inject = ['$state', '$document', 'moveBoardApi', 'apiService', 'statesService', 'datacontext'];

function jobsInSit($state, $document, moveBoardApi, apiService, statesService, datacontext) {
	return {
		template: require('./jobsInSit.pug'),
		restrict: 'A',

		link: function($scope) {
			$scope.getList = function () {
				$scope.makeStatesArray();
				$scope.busy = true;
				apiService.postData(moveBoardApi.request.pickup, $scope.params).then(({data}) => {
					$scope.busy = false;
					calculateTotals(data._meta);
					if (data.items) {
						$scope.jobs = data.items;
					} else {
						$scope.jobs = [];
					}
				}).catch(error => {
					$scope.busy = false;
				});
			};
			
			function calculateTotals(meta) {
				if (meta.pageCount < $scope.params.page)
					$scope.params.page = meta.pageCount || 1;
				$scope.selectInfo.totalItems = meta.totalCount;
				$scope.selectInfo.totalPages = meta.pageCount;
				let firstOnPage = (meta.currentPage - 1) * meta.perPage + 1;
				let lastOnPage = meta.currentPage * meta.perPage;
				$scope.selectInfo.firstItemNumber = $scope.selectInfo.totalItems > 0
					? firstOnPage
					: 0;
				$scope.selectInfo.lastItemNumber = meta.totalCount < lastOnPage
					? meta.totalCount
					: lastOnPage;
				$scope.selectInfo.loaded = true;
			}
			
			$scope.getStorageList = () => {
				apiService.getData(moveBoardApi.ldStorages.list, {short: 1, pageSize:999}).then(data => {
					$scope.storages = data.data.items
				}).catch(error => {
				});
			};
			$scope.getForemanList = () => {
				apiService.postData(moveBoardApi.foreman.list, {active:1, role:["foreman"]}).then(data => {
					$scope.foreman = $scope.getArrayFromObject(data.data.foreman)
				}).catch(error => {
				});
			};
			$scope.makeStatesArray = () => {
				if ($scope.chosenStatesFrom.length > 0) {
					$scope.params.condition.filters.from_state = statesService.getOnlyPrefixes($scope.chosenStatesFrom)
				} else {
					delete $scope.params.condition.filters.from_state
				}
				if ($scope.chosenStatesTo.length > 0) {
					$scope.params.condition.filters.to_state = statesService.getOnlyPrefixes($scope.chosenStatesTo)
				} else {
					delete $scope.params.condition.filters.to_state
				}
			};

			$scope.getArrayFromObject = (obj) => {
				var array = [];
				for(var key in obj) {
					array.push(obj[key])
				}
				return array
			};

			$scope.changeFromFilter = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.date.from = moment(timestamp).startOf('day').unix() + $scope.offset
				} else {
					delete $scope.params.condition.filters.date.from
				}
				$scope.getList()
			};

			$scope.changeToFilter = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.date.to = moment(timestamp).endOf('day').unix() + $scope.offset
				} else {
					delete $scope.params.condition.filters.date.to
				}
				$scope.getList()
			};

			$scope.openModalFrom = () => {
				$scope.modalFromOpened = true
			};

			$scope.openModalTo = () => {
				$scope.modalToOpened = true
			};

			$scope.$watch(() => $scope.chosenStatesFrom.length, () => {
				$scope.selectStringFrom = statesService.getPrefixesString($scope.chosenStatesFrom)
			});
			$scope.$watch(() => $scope.chosenStatesTo.length, () => {
				$scope.selectStringTo = statesService.getPrefixesString($scope.chosenStatesTo)
			});

			$scope.setStorage = () => {
				if ($scope.selectedStorage.storage_id) {
					$scope.params.condition.filters.storage_id = $scope.selectedStorage.storage_id
				} else {
					delete $scope.params.condition.filters.storage_id
				}
				$scope.getList()
			};
			$scope.setForeman = () => {
				if ($scope.selectedForeman.uid) {
					$scope.params.condition.filters.foreman = $scope.selectedForeman.uid
				} else {
					delete $scope.params.condition.filters.foreman
				}
				$scope.getList()
			};

			$scope.removeFilters = () => {
				delete $scope.from;
				delete $scope.to;
				delete $scope.selectedForeman;
				delete $scope.selectedStorage;
				$scope.chosenStatesFrom = [];
				$scope.chosenStatesTo = [];
				$scope.offset = - new Date().getTimezoneOffset()*60;
				$scope.params = {
					pagesize: 25,
					page: 1,
					filtering: '',
					sorting: {
						orderKey:'date',
						orderValue: 'ASC'
					},
					condition: {
						display_type: 3,
						filters: {
							to_state: [],
							from_state: [],
							date: {},
							sit_status: 1
						}
					}
				};
				$scope.getList();
			};
			
			$scope.nextPage = () => {
				if ($scope.params.page >= $scope.selectInfo.totalPages) return;
				$scope.params.page++;
				$scope.getList();
			};
			
			$scope.prevPage = () => {
				if ($scope.params.page <= 1) return;
				$scope.params.page--;
				$scope.getList();
			};

			$scope.init = () => {
				$scope.selectInfo = {};
				$scope.chosenStatesFrom = [];
				$scope.chosenStatesTo = [];
				$scope.offset = - new Date().getTimezoneOffset()*60;
				$scope.params = {
					pagesize: 25,
					page: 1,
					filtering: '',
					sorting: {
						orderKey:'date',
						orderValue: 'DESC'
					},
					condition: {
						display_type: 3,
						filters: {
							to_state: [],
							from_state: [],
							date: {},
							sit_status: 1
						}
					}
				};
				$scope.getList();
				$scope.getStorageList();
				$scope.getForemanList()
			};
			$scope.init()
		}
	};
}

