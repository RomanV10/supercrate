(function () {
  'use strict';

  angular.module('app.lddispatch')
    .service('tripRequest', tripRequest);

  /* @ngInject */
  function tripRequest(openRequestService) {
    var service = {};

    service.openRequest = openRequest;

    return service;


    function openRequest(id) {
      let spinner = angular.element(document.getElementsByClassName('call-job-modal__overlay')).eq(0).clone()
      spinner.css({display: 'block'})
      angular.element(document.getElementsByTagName('body')).append(spinner)
      var request = {
        nid : id
      };
      //GET SAME DATE REQUESTS 07/31/2015


	    openRequestService.open(request.nid)
		    .finally(() => {
			    spinner.remove();
			    spinner.css({display: 'none'});
		    });
    }
  }

})();

