'use strict';

angular
    .module('app.lddispatch')
    .factory('statesService', statesService);

function statesService() {
	return {
		getPrefixesString: function(array){
			var string = '';
			if (array.length > 0) {
				var onlyPrefixes = this.getOnlyPrefixes(array);
				for (var i = 0; i < onlyPrefixes.length; i++) {
					string = string + onlyPrefixes[i];
					if (i == 3) {
						string = string + '...';
						break
					}
					else if (i < onlyPrefixes.length - 1) {
						string = string + ', ';
						continue;
					}
				}
			} else {
				string = 'None'
			}
			return string
		},
		getOnlyPrefixes: function (array){
			var res = [];
			array.forEach(item => {
				res.push(item.substring(0,2))
			});
			return res
		},
		states: function(){
			var states = [
				"AK - Alaska",
				"AL - Alabama",
				"AR - Arkansas",
				"AS - American Samoa",
				"AZ - Arizona",
				"CA - California",
				"CO - Colorado",
				"CT - Connecticut",
				"DC - District of Columbia",
				"DE - Delaware",
				"FL - Florida",
				"GA - Georgia",
				"GU - Guam",
				"HI - Hawaii",
				"IA - Iowa",
				"ID - Idaho",
				"IL - Illinois",
				"IN - Indiana",
				"KS - Kansas",
				"KY - Kentucky",
				"LA - Louisiana",
				"MA - Massachusetts",
				"MD - Maryland",
				"ME - Maine",
				"MI - Michigan",
				"MN - Minnesota",
				"MO - Missouri",
				"MS - Mississippi",
				"MT - Montana",
				"NC - North Carolina",
				"ND - North Dakota",
				"NE - Nebraska",
				"NH - New Hampshire",
				"NJ - New Jersey",
				"NM - New Mexico",
				"NV - Nevada",
				"NY - New York",
				"OH - Ohio",
				"OK - Oklahoma",
				"OR - Oregon",
				"PA - Pennsylvania",
				"PR - Puerto Rico",
				"RI - Rhode Island",
				"SC - South Carolina",
				"SD - South Dakota",
				"TN - Tennessee",
				"TX - Texas",
				"UT - Utah",
				"VA - Virginia",
				"VI - Virgin Islands",
				"VT - Vermont",
				"WA - Washington",
				"WI - Wisconsin",
				"WV - West Virginia",
				"WY - Wyoming"];
			return states
		}

	};
}
