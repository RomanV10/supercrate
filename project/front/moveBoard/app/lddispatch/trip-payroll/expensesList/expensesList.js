'use strict';
import './expensesList.styl';

angular
	.module('app.lddispatch')
	.directive('expensesList', expensesList);

expensesList.$inject = ['$state', '$stateParams', 'moveBoardApi', 'apiService', '$mdDialog', 'datacontext', '$timeout'];

function expensesList($state, $stateParams, moveBoardApi, apiService, $mdDialog, datacontext, $timeout) {
	return {
		template: require('./expensesList.pug'),
		restrict: 'E',
		scope:{
			expenses: '=items',
			mode: '@',
			update: '&',
			loading: '=',
			total: '='
		},
		link: function ($scope) {
			$scope.closeDialog = closeDialog;
			$scope.openEditDialog = openEditDialog;
			$scope.updateDialog = updateDialog;
			$scope.selectAllExpenses = selectAllExpenses;
			$scope.addExpenses = addExpenses;
			$scope.addNewExpense = addNewExpense;
			$scope.removeExpenses = removeExpenses;

			$scope.dialogOpened = false;
			$scope.editMode = false;

			let expensesType = "";

			const EXPENSES_MODE = 'expenses';
			const CASH_MODE = 'cash';

			function init(){
				//when update expenses - dialog window is not exist
				$scope.$watch('expenses',()=>{
					$scope.dialogOpened = false;
				});

				if(_.isEqual($scope.mode, EXPENSES_MODE)){
					$scope.label = 'Driver Expenses';
					expensesType = 'expenses';
				}
				if(_.isEqual($scope.mode, CASH_MODE)){
					$scope.label = 'Cash Advanced and Wires';
					expensesType = 'cash';
				}
			}

			function openEditDialog(item, type) {
				if (!$scope.dialogOpened && !item[type]) {
					item[type] = true;
					$scope.dialogOpened = true;
				}
			}

			function closeDialog(item) {
				delete item.amountEditDialogOpen;
				delete item.dateEditDialogOpen;
				delete item.descriptionEditDialogOpen;
				$scope.dialogOpened = false;
			}

			function updateDialog(item) {
				$scope.closeDialog(item);
				updateData();
			}

			function selectAllExpenses(value) {
				$scope.expenses.forEach( item => item.selected = value);
			}

			function addExpenses(ev) {
				$mdDialog.show({
					controller: AddExpensesDialogController,
					template: require('.//addExpensesListTemplate.pug'),
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose:true
				});
			}

			function AddExpensesDialogController($scope, $mdDialog){
				var fieldData = datacontext.getFieldData();
				var longDistance = angular.fromJson(fieldData.longdistance);

				$scope.expensesDialogItem = longDistance.ldDriver[expensesType];
				if(_.isEqual(expensesType, CASH_MODE)){
					$scope.label = 'Cash Advanced and Wires';
				}
				if(_.isEqual(expensesType, EXPENSES_MODE)){
					$scope.label = 'Driver Expenses';
				}
				$scope.selectAll = value => {
					$scope.expensesDialogItem.forEach( item => item.selected = value);
				};

				$scope.cancel = function() {
					$mdDialog.cancel();
				};
				$scope.add = function() {
					addExpensesFromList($scope.expensesDialogItem);
					$mdDialog.cancel();
				};
			}

			function addExpensesFromList(arr){
				arr.forEach((item) => {
					if(item.selected) {
						item.date = moment().unix();
						delete item.selected;
						$scope.expenses.push(item);
					}
				});
				updateData();
			}

			function addNewExpense() {
				var expense = {
					amount: 0,
					date: moment().unix(),
					description: 'new'
				};
				$scope.expenses.push(expense);
				updateData();
			}

			function removeExpenses() {
				$scope.expenses = $scope.expenses.filter(item => {
					return !item.selected;
				});
				updateData();
			}

			function updateData() {
				let timer = setTimeout(()=>{
					$scope.update();
					clearTimeout(timer);
				},100);
			}

			init();
		}
	};
}
