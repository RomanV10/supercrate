'use strict';
import './editItemDialog.styl';

angular
	.module('app.lddispatch')
	.directive('editItemDialog', editItemDialog);

editItemDialog.$inject = ['$state', 'apiService', 'moveBoardApi'];

function editItemDialog($state, apiService, moveBoardApi) {
	return {
		template: require('./editItemDialog.pug'),
		restrict: 'EA',
		scope: {
			value: '=',
			closeDialog: '&',
			updateDialog: '&',
			mode: '@',
			position: '@',
			extraDialogStyle: '@'
		},
		link: linkFunction
	};

	function linkFunction($scope) {
		$scope.data = {};
		$scope.data.value = setDataFormat($scope.mode, $scope.value);

		$scope.$watch('value', () => {
			$scope.data.value = setDataFormat($scope.mode, $scope.value);
		});

		function setDataFormat(mode, value) {
			if (mode === 'datePicker') {
				return new Date(value * 1000);
			}

			if (mode === 'number' || mode === 'currency') {
				return +value;
			}

			return value;
		}

		$scope.close = () => {
			$scope.closeDialog();
		};

		$scope.update = () => {
			if ($scope.mode === 'datePicker') {
				$scope.value = Date.parse($scope.data.value) / 1000;
			} else {
				$scope.value = $scope.data.value;
			}

			$scope.updateDialog();
		};
	}
}
