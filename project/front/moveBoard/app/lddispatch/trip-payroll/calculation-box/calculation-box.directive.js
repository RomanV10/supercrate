'use strict';
import './calculationBox.styl'

angular
	.module('app.lddispatch')
	.directive('calculationBox', calculationBox);

calculationBox.$inject = ['$state', '$stateParams','apiService','moveBoardApi'];

function calculationBox($state, $stateParams, apiService, moveBoardApi) {
	return {
		template: require('./calculationBox.pug'),
		restrict: 'AE',
		link: function ($scope) {
		}
	}
}
