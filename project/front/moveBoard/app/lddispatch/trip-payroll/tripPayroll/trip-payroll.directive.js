'use strict';
import './tripPayroll.styl';

angular
	.module('app.lddispatch')
	.directive('tripPayroll', tripPayroll);

tripPayroll.$inject = ['$state', '$stateParams', 'apiService', 'moveBoardApi', '$timeout', 'SweetAlert', 'MomentWrapperService'];

function tripPayroll($state, $stateParams, apiService, moveBoardApi, $timeout, SweetAlert, MomentWrapperService) {
	return {
		template: require('./tripPayroll.pug'),
		restrict: 'AE',
		scope: {
			id: '=',
			jobsTotal: '=',
			payrollStatus: '='
		},
		link: linkFunction
	};

	function linkFunction($scope) {
		var timeWrapper = MomentWrapperService.getZeroMoment;
		$scope.loading = false;
		$scope.dialogOpened = false;
		$scope.payrollSubmit = false;

		$scope.getPayroll = () => {
			$scope.loading = true;
			apiService.postData(moveBoardApi.ldPayroll.get, {trip_id: $scope.id}).then(data => {
				$scope.payroll = {
					ld_trip_id: data.data.details.ld_trip_id,
					total_payroll: data.data.details.total_payroll,
					date_start: data.data.details.date_start,
					date_end: data.data.details.date_end,
					expenses: data.data.expenses,
					foreman: data.data.foreman,
					helper: data.data.helper,
					received: data.data.received,
					total_payroll_helpers: data.data.total_payroll_helpers
				};
				$scope.dateStart = new Date(data.data.details.date_start * 1000);
				$scope.dateEnd = new Date(data.data.details.date_end * 1000);
				$scope.loading = false;
				$scope.totalForemanDays = data.data.foreman.total_days;
				let foreman = $scope.payroll.foreman;
				$scope.foremanName = `${foreman.field_user_first_name_value} ${foreman.field_user_last_name_value}`;
			});
		};

		$scope.updatePayroll = (params) => {
			if ($scope.payrollStatus == 1) {
				SweetAlert.swal("Payroll is submitted!", "If you wont to change some data revoke payroll!", "error");
				$scope.getPayroll();
				return;
			}

			if (+$scope.payroll.foreman.mileage_end > +$scope.payroll.foreman.mileage_start) {
				$scope.payroll.foreman.mileage = $scope.payroll.foreman.mileage_end - $scope.payroll.foreman.mileage_start;
			}
			if ($scope.payroll.helper.length && $scope.payroll.foreman.total_days !== $scope.totalForemanDays) {
				$scope.payroll.helper.forEach((item) => {
					item.total_days = $scope.payroll.foreman.total_days;
				});
			}
			if ($scope.payroll.helper.length) {
				$scope.payroll.helper.forEach((item) => {
					delete item.dailyAmountEditDialogOpen;
					delete item.otherEditDialogOpen;
					delete item.totalDaysEditDialogOpen;
				});
			}
			$scope.loading = true;
			if (params && params.dateStart && params.dateEnd) {
				var date_start = angular.copy(moment(params.dateStart));
				var date_end = angular.copy(moment(params.dateEnd));
				$scope.payroll.date_start = timeWrapper(moment(date_start).set({
					hour: 15,
					minute: 10,
					second: 0,
					millisecond: 0
				})).unix();
				$scope.payroll.date_end = timeWrapper(moment(date_end).set({
					hour: 15,
					minute: 10,
					second: 0,
					millisecond: 0
				})).unix();
				$scope.payroll.foreman.total_days = moment(date_end).diff(moment(date_start), 'days') + 1
			}
			apiService.postData(moveBoardApi.ldPayroll.update, {
				trip_id: $scope.payroll.ld_trip_id,
				data: $scope.payroll
			}).then(data => {
				$scope.payroll = data.data;
				$scope.dateStart = new Date(data.data.date_start * 1000);
				$scope.dateEnd = new Date(data.data.date_end * 1000);
				$scope.loading = false;
				$scope.dialogOpened = false;
				$scope.totalForemanDays = data.data.foreman.total_days;
			});
		};

		$scope.openDailyAmountEditDialog = (item) => {
			if (!$scope.dialogOpened && !item.dailyAmountEditDialogOpen) {
				item.dailyAmountEditDialogOpen = true;
				$scope.dialogOpened = true;
			}
		};

		$scope.openTotalDaysEditDialog = (item) => {
			if (!$scope.dialogOpened && !item.totalDaysEditDialogOpen) {
				item.totalDaysEditDialogOpen = true;
				$scope.dialogOpened = true;
			}
		};

		$scope.openOtherEditDialog = (item) => {
			if (!$scope.dialogOpened && !item.otherEditDialogOpen) {
				item.otherEditDialogOpen = true;
				$scope.dialogOpened = true;
			}
		};

		$scope.closeDialog = (item) => {
			item.dailyAmountEditDialogOpen = false;
			item.totalDaysEditDialogOpen = false;
			item.otherEditDialogOpen = false;
			$scope.dialogOpened = false;
		};

		$scope.updateDialog = (item) => {
			$scope.closeDialog(item);
			$timeout(() => {
				$scope.updatePayroll();
			}, 1000);
		};

		$scope.getPayroll();
	}
}
