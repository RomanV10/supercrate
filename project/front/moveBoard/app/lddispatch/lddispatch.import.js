
function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

require("./lddispatch.module.js");
require("./config.route.js");

//Carriers
require("./carriers/couriers/couriers.directive");
require("./carriers/add_couriers/add-couriers.directive");

//Storages
require("./storages/lddispatch-storages.directive.js");
require("./storages/create/lddispatch-storage-create.directive.js");

//Agent Folio
require("./agent-folio/agent-folio-list/agentFolio");
require("./agent-folio/agent-folio-list-item/agent-folio-list-item.directive");
require("./agent-folio-payments/agent-folio-footer/agentFolioFooter");

//agent Folio Payments
require("./agent-folio-payments/agent-folio-details/agent-folio-details.directive");
require("./agent-folio-payments/payment-tabs/paymentTabs");
require("./agent-folio-payments/receiptsList/receipts-list.directive");
require("./agent-folio-payments/invoices-list/invoices-list.directive");
require("./agent-folio-payments/receiptModal/receipt-modal.directive");
require("./agent-folio-payments/payment-tabs/payment-totals/paymentTotals");
require("./agent-folio-payments/invoices-list/invoice-status.filter");

//Requests Lists
require("./pickup-list/pickup-list.directive");
require("./tp-delivery-list/delivery-list.directive");
require("./jobs-in-sit/jobs-in-sit.directive");

//Trip Planner
require("./trip-planner/trucksTimeline/timeline/trucks-timeline.directive");
require("./trip-planner/trucksTimeline/trip-info-tooltip/trip-info-tooltip.directive");
require("./trip-planner/trucksTimeline/truck-item/truck-item.directive");
require("./trip-planner/trucksTimeline/unavailable-item/unavailable-item.directive");
require("./trip-planner/trucksTimeline/timeline/trucksTimeline.service.js");
require("./trip-planner/tripTabs/trip-tabs.directive");
require("./trip-planner/tpDelivery/tp-delivery.directive");
require("./trip-planner/tpDelivery/editSitListModal/edit-sit-list-modal.directive");
require("./trip-planner/tpCollected/tp-collected.directive");
require("./trip-planner/jobs-trip-list/rate-modal/rate-modal.directive");
require("./trip-planner/addTripForm/add-trip-form.directive");
require("./trip-planner/trip-log/trip-log.directive");
require("./trip-planner/jobs-trip-list/jobsTripList");
require("./trip-planner/requestList/request-list.directive");
require("./trip-planner/requestListItem/request-list-item.directive");
require("./trip-planner/tripListItem/trip-list-item.directive");
require("./trip-planner/tripListItem/job-status.filter");
require("./trip-planner/tripListItem/trip-status.filter");
require("./trip-planner/tripListItem/trip-type.filter");
require("./trip-planner/trips/trips.directive");
require("./trip-planner/tpDelivery/tp-sit-status.filter.js");

//Trip Payroll
require("./trip-payroll/tripPayroll/trip-payroll.directive");
require("./trip-payroll/expensesList/expensesList");
require("./trip-payroll/editItemDialog/edit-item-dialog.directive");
require("./trip-payroll/calculation-box/calculation-box.directive");

//Additional directives
require("./directives/elromco-select-list/elromco-select-list");
require("./directives/routeHistoryService/routerTracker.js");
require("./directives/sorting/sorting.directive");
require("./directives/statesModal/statesModal");
require("./directives/callJobModal/callJobModal");
require("./directives/elromco-paginator/paginator");
require("./directives/format-currency/format-currency.directive");

//Services
require("./services/tripRequest");
require("./services/states-service.directive.js");

//Filters
require("./filters/carrierStatus");
require("./filters/set-decimal.filter");
require("./filters/trip-status.filter");
