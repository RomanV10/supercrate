'use strict';
import './receiptsList.styl'
import { checkPaymentsExists } from '../export-functions/payment.export.js';

angular
	.module('app.lddispatch')
	.directive('receiptsList', receiptsList);

receiptsList.$inject = ['$state', 'moveBoardApi', 'apiService', '$mdToast', '$mdDialog'];

function receiptsList($state, moveBoardApi, apiService, $mdToast, $mdDialog) {
	return {
		template: require('./receiptsList.pug'),
		restrict: 'A',
		scope: {
			getReceipts: '&',
			receipts: '=',
			openCustomPayment: '=',
			hideZero: '='
		},
		link: function($scope) {
			$scope.isReceiptsList = true;

			$scope.showList = (item) => {
				item.isShowlist = !item.isShowlist
			};

			$scope.removeReceipt = (id, index, ev) => {
				var confirm = $mdDialog.confirm()
					.title('Are you sure you want to remove Receipt #' + id)
					.targetEvent(ev)
					.ok('Yes')
					.cancel('No');

				$mdDialog.show(confirm)
					.then(function () {
						apiService.postData(moveBoardApi.carrier_payments.removeRequestReceipt, {id})
							.then(data => {
								showToest('Receipt was removed!');
								$scope.receipts.splice(index, 1)
							})
					});
			};

			function showToest(msg) {
				$mdToast.show(
					$mdToast.simple()
						.textContent(msg)
						.position('top right')
						.hideDelay(3000)
				);
			}

			$scope.openTpDelivery = (item) => {
				$state.go('lddispatch.tpDelivery', { id : $state.params.id, tpId: item.job_id, paymentExist: checkPaymentsExists(item) })
			};

			$scope.getReceiptAmount = (item) => {
				let amount;
				if (item.partially_paid) {
					amount = parseFloat(item.total_amount) - parseFloat(item.partially_paid)
				} else {
					amount = parseFloat(item.total_amount)
				}
				return amount
			};

			$scope.init = () => {
				$scope.carrierId = $state.params.id;
				$scope.getReceipts()
			};

			$scope.hidePaidInvoices = (item) => {
				return $scope.hideZero && item.value.amount == item.value.total_amount && !item.value.partially_paid && !item.value.overpay && !item.value.pending
			};

			$scope.init()
		}
	}
}
