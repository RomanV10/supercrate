(function () {
    'use strict';
    angular
        .module('app.lddispatch')
        .directive('agentFolioFooter', ['$state', 'moveBoardApi', 'apiService', function($state, moveBoardApi, apiService) {
            return {
                template: require('./agentFolioFooter.pug'),
                restrict: 'A',
                scope: {
                	item: '='
                },

                link: function($scope) {
                  
                }
            };
        }]);
})();
