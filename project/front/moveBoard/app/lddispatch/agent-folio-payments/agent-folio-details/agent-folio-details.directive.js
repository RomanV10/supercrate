'use strict';
import './agentFolioDetails.styl'
import { checkPaymentsExists } from '../export-functions/payment.export.js';

angular
	.module('app.lddispatch')
	.directive('agentFolioDetails', agentFolioDetails);

agentFolioDetails.$inject = ['$state', 'moveBoardApi', 'apiService', 'tripRequest', '$mdToast', '$mdDialog', '$timeout'];

function agentFolioDetails($state, moveBoardApi, apiService, tripRequest, $mdToast, $mdDialog, $timeout) {
	return {
		template: require('./agentFolioDetails.pug'),
		restrict: 'A',
		scope: {
			defaultTotal: "=",
			jobsTripItems: "=",
			getJobs: '&',
			changeSelectList: "=",
			openCustomPayment: '=',
			openInvoice: '=',
			getCharge: '=',
			selectAll: '=',
			userInfo: '=',
			hideZero: '='
		},

		link: function($scope) {
			let delayTimer;

			$scope.getStatusClass = function(item) {
				let result = '';

				if (item.invoice_id != 0) {
					switch (item.invoice.flag.toString()) {
						case '0':
							result = 'sit-payment-status_status-0';
							break;
						case '1':
							result =  'sit-payment-status_status-1';
							break;
						case '2':
							result =  'sit-payment-status_status-2';
							break;
						case '3':
							result =  'sit-payment-status_status-3';
							break;
					}
				}

				if (item.receipt_id != 0 && _.isEmpty(result)) {
					result =  'sit-payment-status_receipt'
				}

				return result;
			};

			$scope.openTpDelivery = (item) => {
				$state.go('lddispatch.tpDelivery', { id : $state.params.id, tpId: item.job_id, paymentExist: checkPaymentsExists(item) })
			};


			$scope.selectAllItems = () => {
				$scope.jobsTripItems.forEach( item => item.selected = ( item.receipt_id === 0 && item.invoice_id === 0 ) ?  $scope.selectAll : item.selected);
				$scope.toggleSelect()
			};

			$scope.getAmount = (item) => {
				let amount;
				if (item.partially_paid) {
					amount = parseFloat(item.amount) - parseFloat(item.partially_paid)
				} else {
					amount = parseFloat(item.amount)
				}
				return amount
			};

			$scope.getReceiptAmount = (item) => {
				let amount;
				if (item.partially_paid) {
					amount = parseFloat(item.total_amount) - parseFloat(item.partially_paid)
				} else {
					amount = parseFloat(item.total_amount)
				}
				return amount
			};

			$scope.toggleSelect = () => {
				$timeout.cancel(delayTimer);
				delayTimer = $timeout(function () {
					$scope.selectAll = $scope.jobsTripItems.filter(item => item.selected === true).length === $scope.jobsTripItems.filter(item => item.state === 0).length && $scope.jobsTripItems.filter(item => item.state === 0).length !== 0;
					$scope.selectedJobs = $scope.jobsTripItems
						.map(item => item.selected ? item.job_id : 0)
						.filter(item => item !== 0);
					if ($scope.selectedJobs.length > 0) {
						apiService.postData(moveBoardApi.trip_carrier.getTotal, {jobs_id: $scope.selectedJobs})
							.then(data => {
								$scope.changeSelectList(data.data, $scope.selectedJobs);
							});
					} else {
						$scope.changeSelectList(null, []);
						$scope.selectAll = false;
					}
				})
			};

			$scope.resendInvoice = (event, invoice) => {
				apiService.getData(moveBoardApi.invoice.update, {id: invoice.invoice_id})
					.then(res => {
						var charge = res.data.data.charges;
						var descriptionAndNotes = {
							description: res.data.data.description,
							notes: res.data.data.notes
						};
						let selectedJobs = $scope.jobsTripItems
							.filter(item => item.invoice_id === invoice.invoice_id)
							.map(job => job.job_id);

						$scope.openInvoice(event, charge, false, selectedJobs, invoice.invoice.data.total, invoice.invoice_id, false, descriptionAndNotes)
					})
			};

			$scope.setPaid = (id) => {
				apiService.postData(moveBoardApi.trip_carrier.paidInvoice, {id})
					.then(data => {
						showToest('Invoice was	paid!');
						$scope.getJobs();
					})
			};

			$scope.removeInvoice = (id, ev) => {
				var confirm = $mdDialog.confirm()
					.title('Are you sure you want to remove Invoice #' + id)
					.targetEvent(ev)
					.ok('Yes')
					.cancel('No');
				$mdDialog.show(confirm)
					.then(function () {
						apiService.postData(moveBoardApi.carrier_payments.removeRequestInvoice, {id})
							.then(data => {
								showToest('Invoice was removed!');
								$scope.getJobs();
							})
					});

			};

			$scope.removeReceipt = (id, ev) => {
				var confirm = $mdDialog.confirm()
					.title('Are you sure you want to remove Receipt #' + id)
					.targetEvent(ev)
					.ok('Yes')
					.cancel('No');

				$mdDialog.show(confirm)
					.then(function () {
						apiService.postData(moveBoardApi.carrier_payments.removeRequestReceipt, {id})
							.then(data => {
								showToest('Receipt was removed!');
								$scope.getJobs();
							})
					});
			};

			function showToest(msg) {
				$mdToast.show(
					$mdToast.simple()
						.textContent(msg)
						.position('top right')
						.hideDelay(3000)
				);
			}

			$scope.getJobs();
		}
	};
}
