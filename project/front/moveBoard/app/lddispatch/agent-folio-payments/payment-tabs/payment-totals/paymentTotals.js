'use strict';
import './paymentTotals.styl'
(function () {
	angular
		.module('app.lddispatch')
		.directive('paymentTotals', ['$state', 'moveBoardApi', 'apiService','$mdDialog', function($state, moveBoardApi, apiService, $mdDialog) {
			return {
				template: require('./paymentTotals.pug'),
				restrict: 'A',
				scope:{
					defaultTotal: '='
				},
				link: function($scope) {
				}
			}
		}])
})();
