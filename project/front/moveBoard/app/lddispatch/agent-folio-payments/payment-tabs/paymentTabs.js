'use strict';
import './paymentTabs.styl';

(function () {
	angular
		.module('app.lddispatch')
		.directive('paymentTabs', paymentTabs);

	paymentTabs.$inject = ['$state', 'moveBoardApi', 'apiService', 'routerTracker', '$location'];

	function paymentTabs($state, moveBoardApi, apiService, routerTracker, $location) {
		return {
			template: require('./paymentTabs.pug'),
			restrict: 'A',

	        link: function($scope, element) {
	          $scope.currentTab = $state.params.tab;
	          $scope.selectAll = false;
	          $scope.jobsTripList = [];
	          $scope.selectedJobs = [];
	          $scope.charge = [];
	          $scope.userInfo = {};
	          $scope.userInfo.ld_carrier_id  = $state.params.id;
	          $scope.hideZero = JSON.parse(localStorage.getItem('agentFolioDetailsHideZeroValue'));
	          if ($scope.hideZero == null) {
	            $scope.hideZero = 1;
	            localStorage.setItem('agentFolioDetailsHideZeroValue', JSON.stringify($scope.hideZero));
	          }
	          $scope.setHideZero = () => {
	            localStorage.setItem('agentFolioDetailsHideZeroValue', JSON.stringify($scope.hideZero));
	            if ($scope.currentTab == 0) {
	            	$scope.getJobs()
	            }
	          };
	          $scope.changeState = function(state) {
	              $scope.currentTab = state;
	              $state.go($state.current.name, {id : $state.params.id, tab: state}, {notify: false});
	          };

				$scope.back = () => {
					$state.go('lddispatch.agentFolio');
					/*var routeHistory = routerTracker.getRouteHistory();
					if (routeHistory.length > 0) {
						var routeName = routeHistory[0].route.name;
						var routeParams = routeHistory[0].routeParams;
						$state.go(routeName, routeParams)
					} else {
						$state.go('lddispatch.agent-folio-list');
					}*/
				};

				function fieldSorter(fields) {
					return function (a, b) {
						return fields
							.map(function (o) {
								var dir = 1;
								if (o[0] === '-') {
									dir = -1;
									o = o.substring(1);
								}
								if (a[o] > b[o]) return dir;
								if (a[o] < b[o]) return -(dir);
								return 0;
							})
							.reduce(function firstNonZeroValue(p, n) {
								return p ? p : n;
							}, 0);
					};
				}

				$scope.getJobs = () => {
					$scope.selectAll = false;
					$scope.changeSelectList(null ,[]);
					apiService.getData(moveBoardApi.trip_carrier.getJobs, {
						id: $state.params.id,
						jobs_only: 1
					}).then(data => {
						$scope.jobsTripList = data.data.items || [];
						$scope.jobsTripList.sort(fieldSorter(['invoice_id', 'receipt_id']));
						let uniqeArray = [];
						let uniqInvoiceArray = [];
						let currentRecId = 0;
						let currentInvId = 0;
						for (var i = $scope.jobsTripList.length - 1; i >= 0; i--) {
							$scope.jobsTripList[i].state = 0;
							if ($scope.hideZero
								&& (($scope.jobsTripList[i].invoice_id != 0
									&& $scope.jobsTripList[i].invoice.flag == 3
									&& !$scope.jobsTripList[i].invoice.data.partially_paid
									&& !$scope.jobsTripList[i].invoice.data.overpay)
								|| ($scope.jobsTripList[i].receipt_id != 0
									&& $scope.jobsTripList[i].receipt.value.amount == $scope.jobsTripList[i].receipt.value.total_amount
									&& !$scope.jobsTripList[i].receipt.value.pending
									&& !$scope.jobsTripList[i].receipt.value.partially_paid
									&& !$scope.jobsTripList[i].receipt.value.overpay))) {
								$scope.jobsTripList.splice(i, 1)
							} else {
								if ($scope.jobsTripList[i].receipt_id !== currentRecId) {
									uniqeArray.push($scope.jobsTripList[i]);
									currentRecId = $scope.jobsTripList[i].receipt_id
								}
								if ($scope.jobsTripList[i].invoice_id !== currentInvId) {
									uniqInvoiceArray.push($scope.jobsTripList[i]);
									currentInvId = $scope.jobsTripList[i].invoice_id
								}
							}

						};

						uniqeArray.forEach((item, index) => {
							buildJoins('receipt_id', item.receipt_id)
						});
						uniqInvoiceArray.forEach((item, index) => {
							buildJoins('invoice_id', item.invoice_id)
						});
						$scope.carrier = data.data.info;
						$scope.sumJobsTripList = data.data.total;
						$scope.total = $scope.defaultTotal = angular.copy(data.data.total);
					});
				};

				function buildJoins(field, id) {
					let count = 0;
					$scope.jobsTripList.forEach((subItem, index) => {
						if (subItem[field] === id) {
							count++;
						}
					});
					let index = 0;
					$scope.jobsTripList.forEach(subItem => {
						if (subItem[field] === id && count > 0 && id !== 0 && !subItem.derty) {
							if (index === 0 && count !== 1) {
								subItem.state = 1;
							} else if (count === 1) {
								subItem.state = 4;
							} else if (index === count - 1) {
								subItem.state = 3;
							} else {
								subItem.state = 2;
							}
							subItem.derty = true;
							index++;
						}
					});
				}

				$scope.getInvoices = () => {
					apiService.postData(moveBoardApi.carrier_payments.invoices, {
						id: $scope.userInfo.ld_carrier_id,
						page: 1,
						pageSize: 999
					}).then(data => {
						if (data.data._meta) {
							$scope.meta = data.data._meta;
							$scope.invoices = data.data.items;
							$scope.invoices.forEach(obj => {
								buildPaymentJoins(obj.items)
							})
						} else {
							$scope.invoices = []
						}
					})
				};
				$scope.getReceipts = () => {
					apiService.postData(moveBoardApi.carrier_payments.receipts, {
						id: $scope.userInfo.ld_carrier_id,
						page: 1,
						pageSize: 999
					}).then(data => {
						if (data.data.items) {
							$scope.receipts = data.data.items;
							$scope.receipts.forEach(obj => {
								buildPaymentJoins(obj.items)
							})
						} else {
							$scope.receipts = []
						}
						$scope.meta = data.data._meta;
					})
				};

				function buildPaymentJoins(array) {
					let count = array.length;
					array.forEach((obj, index) => {
						if (index == 0 && count != 1) {
							obj.state = 1
						} else if (index == count - 1) {
							obj.state = 3
						} else if (count == 1) {
							obj.state = 4
						} else {
							obj.state = 2
						}
					})
				}


				$scope.changeSelectList = (list, selectedJobs) => {
					let width = element.find('#agent-folio_carrier-name').width();
					element.find('.payment-tabs__toolbar__total-bg').css({'margin-left': `${width}px`});
					if (selectedJobs.length) {
						$scope.charge = $scope.getCharge(list.items);
						$scope.total = list.total;
					} else {
						$scope.total = 0;
					}
					$scope.selectedJobs = selectedJobs;

					$scope.payType = ($scope.total && Number($scope.total.balance) > 0 ) ? '1' : '2';
				};

				apiService.getData(moveBoardApi.trip_carrier.getById, {carrier_id: $state.params.id}).then(data => {
					$scope.userInfo = data.data;
				});

				$scope.updateList = () => {
					switch ($scope.currentTab) {
						case 0:
							$scope.getJobs();
							break;
						case 1:
							$scope.getReceipts();
							break;
						case 2:
							$scope.getInvoices();
							break;
					}
		        };

				$scope.getCharge = (jobs) => {
					let result = [];
					jobs.forEach(job => {
						if (job.ld_tp_delivery_id) {
							result.push({
								name: job.job_id,
								description: `Trip # ${job.ld_trip_id}, From: ${job.from_a_city || ''} ${job.from_a_state || 'none'}, To: ${job.to_a_city || ''} ${job.to_a_state || 'none'}, Customer name: ${job.owner},\n Volume Cf: ${job.volume_cf}, Rate per cf: $${job.rate_per_cf}, TP Collected: $${job.tp_collected}, Services Total: $${job.services_total}, Received: $${job.received}, Job Cost: $${job.job_cost}, Job Total: $${job.job_total}`,
								cost: job.balance,
								qty: "1"
							});
						} else {
							result.push({
								name: job.job_id,
								description: `Trip # ${job.ld_trip_id}, From: ${job.from_a_city || ''} ${job.from_a_state || 'none'}, To: ${job.to_a_city || ''} ${job.to_a_state || 'none'}, Customer name: ${job.owner},\n Volume Cf: ${job.volume_cf}, Rate per cf: $${job.rate_per_cf}, TP Collected: $${job.tp_collected}, Services Total: $${job.services_total}, Received: $${job.received}, Job Cost: $${job.job_cost}, Job Total: $${job.job_total} `,
								cost: job.balance,
								qty: "1"
							});
						}

					});
					return result;
				}

			}
		};
	}
})();
