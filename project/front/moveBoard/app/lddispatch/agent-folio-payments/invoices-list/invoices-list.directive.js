'use strict';
import './invoicesList.styl'

angular
	.module('app.lddispatch')
	.directive('invoicesList', invoicesList);

invoicesList.$inject = ['$state', 'moveBoardApi', 'apiService', '$mdToast', '$mdDialog'];

function invoicesList($state, moveBoardApi, apiService, $mdToast, $mdDialog) {
	return {
		template: require('./invoicesList.pug'),
		restrict: 'A',
		scope: {
			openInvoice: '=',
			getCharge: '=',
			getInvoices: '=',
			invoices: '=',
			openCustomPayment: '=',
			userInfo : '=',
			hideZero: '='
		},

		link: function($scope) {

			$scope.getStatusClass = function(flag) {
				switch (flag.toString()) {
					case '0':
						return 'sit-payment-status_status-0';
						break;
					case '1':
						return 'sit-payment-status_status-1';
						break;
					case '2':
						return 'sit-payment-status_status-2';
						break;
					case '3':
						return 'sit-payment-status_status-3';
						break
				}
			};
			$scope.getOpenedClass = (item) => {
				let resultCss = '';
				switch (item.flag.toString()) {
					case '0':
						resultCss = 'sit-payment-status_status-0';
						break;
					case '1':
						resultCss = 'sit-payment-status_status-1';
						break;
					case '2':
						resultCss = 'sit-payment-status_status-2';
						break;
					case '3':
						resultCss = 'sit-payment-status_status-3';
						break;
					default:
						resultCss = '';
						break
				}
				if (item.isShowlist) {
					resultCss = resultCss + ' invoices-list__job-list__body__line_open'
				}
				return resultCss
			};


			$scope.resendInvoice = (item) => {
				var jobIds = $scope.getIdArray(item.items);
				$scope.openInvoice(event, item.value.charges, false, jobIds, item.total, item.invoice_id, true, item.value)
			};


			$scope.removeInvoice = (id, index, ev) => {
				var confirm = $mdDialog.confirm()
					.title('Are you sure you want to remove Invoice #' + id)
					.targetEvent(ev)
					.ok('Yes')
					.cancel('No');
				$mdDialog.show(confirm).then(function() {
					apiService.postData(moveBoardApi.carrier_payments.removeRequestInvoice, {id}).then(data => {
						showToest('Invoice was removed!');
						$scope.invoices.splice(index, 1)
					})
				});
			};
			function showToest(msg) {
				$mdToast.show(
					$mdToast.simple()
						.textContent(msg)
						.position('top right')
						.hideDelay(3000)
				);
			}
			$scope.showList = (item) => {
				if (!item.isShowlist) {
					item.isShowlist = true
				} else {
					item.isShowlist = false
				}
			};
			$scope.getIdArray = (array) => {
				var ids = [];
				array.forEach(item => {
					ids.push(item.job_id)
				});
				return ids
			};

			$scope.getAmount = (item) => {
				let amount;
				if (item.partially_paid) {
					amount = parseFloat(item.amount) - parseFloat(item.partially_paid)
				} else {
					amount = parseFloat(item.amount)
				}
				return amount
			};
			$scope.openTpDelivery = (item) => {
				$state.go('lddispatch.tpDelivery', { id : $state.params.id, tpId: item.job_id, paymentExist: checkPaymentsExists(item) })
			};
			$scope.init = () => {
				$scope.carrierId = $state.params.id;
				$scope.getInvoices()
			};
			$scope.hidePaidInvoices = (item) => {
				var data = item.data || item.value;
				return $scope.hideZero && item.flag == 3 && !data.partially_paid && !data.overpay
			};
			$scope.init()
		}
	}
}

