'use strict';

angular
	.module('app.lddispatch')
	.filter('invoiceStatus', function(){
		return function(text){
			if(text.toString() === '0'){
				return "Draft";
			}
			else if(text.toString() === '1'){
				return "Sent";
			}
			else if(text.toString() === '2'){
				return "Viewed";
			}
			else{
				return "Paid";
			}
		}
	});
