'use strict';
import './receipt-modal.styl'

angular
	.module('app.lddispatch')
	.directive('receiptModal', receiptModal);

receiptModal.$inject = ['$state', 'moveBoardApi', 'apiService', '$mdToast', '$mdDialog', '$timeout'];

function receiptModal($state, moveBoardApi, apiService, $mdToast, $mdDialog, $timeout) {
	return {
		template: require('./receipt-modal.pug'),
		restrict: 'E',
		scope: {
			receipt: '=?',
			receipts: '=?',
			invoice: '=?',
			multipleReceipts: '=',
			isReceiptsList: '=',
			refundButton: '=',
			getJobs: '&?',
			userInfo: '=',
			openCustomPayment: '='
		},
		link: function ($scope) {
			let responseDellay;

			$scope.showMenu = showMenu;
			$scope.showReceiptModal = showReceiptModal;
			$scope.getReceiptAmount = getReceiptAmount;
			$scope.getMaxAmountForEditReceipt = getMaxAmountForEditReceipt;
			$scope.setPending = setPending;
			$scope.removeReceipt = removeReceipt;

			function showMenu($mdMenu, ev) {
				$mdMenu.open(ev)
			}

			function showReceiptModal(ev, item) {
				let receipt = angular.copy(item);
				receipt.receipt_id = receipt.receipt_id || receipt.id;
				if (receipt.value.ccardN) {
					receipt.value.ccardN = receipt.value.ccardN.slice(-4)
				}
				$mdDialog.show({
					controller: DialogController,
					template: require('.//receipt-modal-template.pug'),
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose: true,
					locals: {
						item: receipt
					}
				})
			}

			function getReceiptAmount(item) {
				let amount;
				if (item.partially_paid) {
					amount = parseFloat(item.total_amount) - parseFloat(item.partially_paid)
				} else {
					amount = parseFloat(item.total_amount)
				}
				return amount
			}

			function getMaxAmountForEditReceipt(receipt) {
				let total;
				if (receipt.value.refunds) {
					total = 0;
					$scope.receipts.forEach(item => {
						if(!item.value.pending && item.id !== receipt.id) {
							if (item.value.refunds) {
								total -= parseFloat(item.value.amount);
							} else {
								total += parseFloat(item.value.amount);
							}
						}
					})
				} else {
					total = $scope.invoice ? $scope.invoice.data.amount : $scope.receipt.data.totalInvoice;
					$scope.receipts.forEach(item => {
						if(!item.value.pending && item.id !== receipt.id) {
							if (item.value.refunds) {
								total += parseFloat(item.value.amount);
							} else {
								total -= parseFloat(item.value.amount);
							}
						}
					})
				}

				return total
			}

			function setPending(receipt) {
				$scope.disabledSwitch = true;
				let id = receipt.receipt_id || receipt.id;
				let pending = receipt.value.pending ? 1 : 0;
				apiService.postData(moveBoardApi.carrier_payments.setReceiptPending, {id, pending})
					.then(data => {
						responseDellay = $timeout(function () {
							$scope.disabledSwitch = false;
						}, 2000);
						showToest('Receipt status updated!');
						$scope.getJobs()
					})
			}

			function removeReceipt(id, index, ev) {
				let confirm = $mdDialog.confirm()
					.title('Are you sure you want to delete Receipt #' + id)
					.targetEvent(ev)
					.ok('Yes')
					.cancel('No');
				$mdDialog.show(confirm)
					.then(function () {
						apiService.postData(moveBoardApi.carrier_payments.removeRequestReceipt, {id})
							.then(() => {
								showToest('Receipt was removed!');
								$scope.getJobs()
							})
					})
			}

			$scope.$on("$destroy", () => {
					$timeout.cancel(responseDellay);
				}
			);

			function showToest(msg) {
				$mdToast.show(
					$mdToast.simple()
						.textContent(msg)
						.position('top right')
						.hideDelay(3000)
				)
			}

			function DialogController($scope, $mdDialog, item) {
				$scope.item = item;
				$scope.cancel = function () {
					$mdDialog.cancel();
				}
			}
		}
	}
}

