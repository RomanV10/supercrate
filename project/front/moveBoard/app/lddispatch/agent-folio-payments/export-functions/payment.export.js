function checkPaymentsExists(item) {
	return item.invoice || !_.isEmpty(item.receipt);
}

export { checkPaymentsExists }
