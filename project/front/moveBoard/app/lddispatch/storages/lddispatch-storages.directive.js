'use strict';
import './lddispatchStorages.styl'

angular
	.module('app.lddispatch')
	.directive('lddispatchStorages', lddispatchStorages);

lddispatchStorages.$inject = ['$state', '$stateParams', 'moveBoardApi', 'apiService', '$timeout'];

function lddispatchStorages($state, $stateParams, moveBoardApi, apiService, $timeout) {
	return {
		template: require('./lddispatchStorages.pug'),
		restrict: 'A',

		link: function ($scope) {
			var searchDelay;
			$scope.params = {
				pageSize: 25,
				page: 1,
				sorting: {
					orderKey: 'name',
					orderValue: 'ASC'
				},
				condition: {
					filters: {}
				}
			};
			$scope.params.page = $state.params.page || 1;
			$scope.params.active = angular.fromJson(localStorage.getItem('sitStoragesActiveOnly'));
			if (!$scope.params.active) {
				$scope.params.active = 0;
				localStorage.setItem('sitStoragesActiveOnly', angular.toJson($scope.params.active));
			}
			$scope.setActiveOnly = (ldTrucksOnly) => {
				localStorage.setItem('sitStoragesActiveOnly', angular.toJson($scope.params.active));
				$scope.getStorages()
			};
			$scope.getStorages = () => {
				$scope.busy = true;
				apiService.getData(moveBoardApi.ldStorages.list, $scope.params).then(data => {
					$scope.storages = data.data.items || [];
					$scope.meta = data.data._meta;
					$state.go('lddispatch.lddispatchStorages', {'page': $scope.params.page}, {notify: false});
					$scope.busy = false;
				});
			};
			$scope.setStorageActive = (storage, index) => {
				apiService.postData(moveBoardApi.ldStorages.setActive, {
					storage_id: storage.storage_id,
					value: storage.active
				}).then(data => {
					if ($scope.params.active) {
						$scope.storages.splice(index, 1)
					}
				});
			};
			$scope.addStorage = () => {
				$state.go('lddispatch.lddispatchStorageCreate', {id: 'new'});
			};

			$scope.editStorage = (id) => {
				$state.go('lddispatch.lddispatchStorageCreate', {id: id});
			};

			$scope.updateDefaultStorage = item => {
				var default_storage = item.default_storage;
				$scope.storages.forEach(el => el.default_storage = '0');
				item.default_storage = default_storage;
				apiService.putData(moveBoardApi.ldStorages.update, {id: item.storage_id, data: item}).then(data => {
				})
			};

			$scope.setPage = page => {
				$scope.params.page = page;
				$scope.getStorages()
			};
			$scope.searchByName = (searchTerm) => {
				$timeout.cancel(searchDelay);
				searchDelay = $timeout(function () {
					$scope.params.page = 1;
					$state.go('lddispatch.lddispatchStorages', {'page': $scope.params.page}, {notify: false});
					$scope.params.condition.filters.name = searchTerm;
					$scope.getStorages();
				}, 1000)
			};
			$scope.$on("$destroy", () => {
					$timeout.cancel(searchDelay);
				}
			);
			$scope.getStorages()

		}
	};
}
