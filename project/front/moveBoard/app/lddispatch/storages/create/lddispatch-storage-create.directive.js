import './lddispatch-storage-create.styl'

'use strict';

angular
	.module('app.lddispatch')
	.directive('lddispatchStorageCreate', lddispatchStorageCreate);

lddispatchStorageCreate.$inject = ['$state', 'moveBoardApi', 'apiService', 'routerTracker', '$mdDialog', 'CalculatorServices', 'SweetAlert', 'geoCodingService'];

function lddispatchStorageCreate($state, moveBoardApi, apiService, routerTracker, $mdDialog, CalculatorServices, SweetAlert, geoCodingService) {
	return {
		template: require('./lddispatch-storage-create.pug'),
		restrict: 'A',
		link: function ($scope) {

			$scope.back = () => {
				var routeHistory = routerTracker.getRouteHistory();
				if (routeHistory.length > 0) {
					var routeName = routeHistory[0].route.name;
					var routeParams = routeHistory[0].routeParams;
					$state.go(routeName, routeParams)
				} else {
					$state.go('lddispatch.lddispatchStorages');
				}
			};

			if ($state.params.id === 'new') {
				$scope.newStorage = {
					active: '1',
					phones: ['']
				};
			} else {
				apiService.getData(moveBoardApi.ldStorages.get, {id: $state.params.id}).then(data => {
					$scope.newStorage = data.data;
				});
			}

			$scope.addPhone = function () {
				$scope.newStorage.phones.push('');
			};
			$scope.deletePhone = function (index) {
				$scope.newStorage.phones.splice(index, 1);
			};

			$scope.changePostalCode = function () {
				var postal_code = $scope.newStorage.zip_code;
				if (!_.isUndefined(postal_code)
					&& postal_code.length == 5) {
					geoCodingService.geoCode(postal_code)
						.then(function (result) {
							$scope.newStorage.city = result.city;
							$scope.newStorage.state = result.state;
						}, function () {
							SweetAlert.swal("You entered the wrong zip code.", '', 'error');
							$scope.newStorage.city = '';
							$scope.newStorage.state = '';
						});
				}
			};

			function checkValidation() {
				if ($scope.storageModel.$valid) {
					return true
				} else {
					$scope.storageModel.$setSubmitted();
					$mdDialog.show(
						$mdDialog.alert()
							.parent(angular.element(document.body))
							.clickOutsideToClose(true)
							.title('Some fields are required!')
							.textContent('You must enter a value for all required fields!')
							.ok('OK')
					);
					return false
				}
			}

			function checkPhones(model) {
				model.phones.forEach((item, index) => {
					if (_.isEmpty(item)) {
						model.phones.splice(index, 1)
					}
				});
				if (_.isEmpty(model.phones)) {
					model.phones = ['']
				}
			}

			$scope.create = () => {
				checkPhones($scope.newStorage);
				if (checkValidation()) {
					if ($state.params.id === 'new') {
						apiService.postData(moveBoardApi.ldStorages.create, {data: $scope.newStorage}).then(data => {
							$state.go('lddispatch.lddispatchStorages');
						})
					} else {
						apiService.putData(moveBoardApi.ldStorages.update, {
							id: $scope.newStorage.storage_id,
							data: $scope.newStorage
						}).then(data => {
							$state.go('lddispatch.lddispatchStorages');
						})
					}
				}
			}

		}
	};
}
