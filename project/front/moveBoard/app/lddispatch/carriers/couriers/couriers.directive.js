'use strict';
import './couriers.styl'

angular
	.module('app.lddispatch')
	.directive('carrierList', carrierList);

carrierList.$inject = ['$state', '$stateParams', 'moveBoardApi', 'apiService', '$timeout'];

function carrierList($state, $stateParams, moveBoardApi, apiService, $timeout) {
	return {
		template: require('./couriers.pug'),
		restrict: 'A',

		link: function ($scope) {
			var searchDelay;
			$scope.params = {
				pageSize: 25,
				page: 1,
				active: 1,
				sorting: {
					orderKey: 'name',
					orderValue: 'ASC'
				},
				condition: {
					filters: {}
				}
			};
			$scope.params.page = $state.params.page || 1;

			$scope.addCarrier = () => {
				$state.go('lddispatch.carrierDetails', {id: 'new'});
			};
			$scope.editCarrier = (id) => {
				$state.go('lddispatch.carrierDetails', {id: id});
			};

			$scope.getCarriers = () => {
				$scope.busy = true;
				apiService.getData(moveBoardApi.trip_carrier.list, $scope.params)
					.then(data => {
						$scope.carriers = data.data.items;
						$scope.meta = data.data._meta;
						$state.go('lddispatch.couriers', {'page': $scope.params.page}, {notify: false});
						$scope.busy = false;
					});
			};

			$scope.setPage = page => {
				$scope.params.page = page;
				$scope.getCarriers();
			};

			$scope.searchByName = (searchTerm) => {
				$timeout.cancel(searchDelay);
				searchDelay = $timeout(function () {
					$scope.params.page = 1;
					$state.go('lddispatch.couriers', {'page': 1}, {notify: false});
					$scope.params.condition.filters.name = searchTerm;
					$scope.getCarriers();
				}, 1000)
			};

			$scope.$on("$destroy", () => {
					$timeout.cancel(searchDelay);
				}
			);

			$scope.getCarriers();
		}
	};
}
