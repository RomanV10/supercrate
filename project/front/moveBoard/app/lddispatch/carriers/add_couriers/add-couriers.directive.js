import './add-couriers.styl'

'use strict';

angular
	.module('app.lddispatch')
	.directive('addCarrier', addCarrier);

addCarrier.$inject = ['$state', 'moveBoardApi', 'apiService', 'routerTracker', '$mdDialog', 'CalculatorServices', 'SweetAlert', 'geoCodingService'];

function addCarrier($state, moveBoardApi, apiService, routerTracker, $mdDialog, CalculatorServices, SweetAlert, geoCodingService) {
	return {
		template: require('./add-couriers.pug'),
		restrict: 'A',
		link: function ($scope) {

			$scope.showModal = false;
			$scope.addCarrier = function () {
				$scope.showModal = true;
			};


			if ($state.params.id !== "new") {
				apiService.getData(moveBoardApi.trip_carrier.getById, {carrier_id: $state.params.id})
					.then(res => {
						$scope.agentModel = res.data;
						if (_.isEmpty($scope.agentModel.phones)) {
							$scope.agentModel.phones = ['']
						}
					});
			} else {
				$scope.agentModel = {
					"name": "",
					"icc_mc_number": null,
					"usdot_number": null,
					"contact_person": "",
					"address": "",
					"zip_code": null,
					"city": "",
					"state": null,
					"country": "",
					"per_cf": "",
					"phones": [
						""
					],
					"email": "",
					"web_site": "",
					"company_carrier": '0',
					"active": '1'
				}
			}

			$scope.close = () => {
				var routeHistory = routerTracker.getRouteHistory();
				if (routeHistory.length > 0) {
					var routeName = routeHistory[0].route.name;
					var routeParams = routeHistory[0].routeParams;
					$state.go(routeName, routeParams)
				} else {
					$state.go('lddispatch.couriers')
				}
			};

			$scope.addPhone = function () {
				$scope.agentModel.phones.push('');
			};
			$scope.deletePhone = (index) => {
				$scope.agentModel.phones.splice(index, 1);
			};

			$scope.changePostalCode = function () {
				var postal_code = $scope.agentModel.zip_code;
				if (!_.isUndefined(postal_code) && postal_code.length == 5) {
					geoCodingService.geoCode(postal_code)
						.then(function (result) {
							$scope.agentModel.city = result.city;
							$scope.agentModel.state = result.state;
						}, function () {
							SweetAlert.swal("You entered the wrong zip code.", '', 'error');
							$scope.agentModel.city = '';
							$scope.agentModel.state = '';
						});
				}
			};

			function checkValidation(model) {
				if ($scope.carrierModel.$valid) {
					return true
				} else {
					$scope.carrierModel.$setSubmitted();
					$mdDialog.show(
						$mdDialog.alert()
							.parent(angular.element(document.body))
							.clickOutsideToClose(true)
							.title('Some fields are required!')
							.textContent('You must enter a value for all required fields!')
							.ok('OK')
					);
					return false
				}
			}

			function checkPhones(model) {
				model.phones.forEach((item, index) => {
					if (_.isEmpty(item)) {
						model.phones.splice(index, 1)
					}
				});
				if (_.isEmpty(model.phones)) {
					model.phones = ['']
				}
			}

			$scope.create = function (data) {
				checkPhones(data);
				if (checkValidation(data)) {
					if ($state.params.id !== "new") {
						apiService.putData(moveBoardApi.trip_carrier.update, {
							data,
							carrier_id: $state.params.id
						}).then(res => {
							$scope.close()
						});
					} else {
						apiService.postData(moveBoardApi.trip_carrier.create, {data})
							.then(res => {
								$scope.close()
							});
					}
				}
			};
		}
	};
}
