describe('Unit: LD Dispatch route test', function () {
	let $rootScope;
	let $state;

	beforeEach(inject(function (_$rootScope_, _$state_) {
		$rootScope = _$rootScope_;
		$state = _$state_;
	}));


	it('LD dispatch state lddispatch.couriers', function () {
		expect($state.href('lddispatch.couriers')).toEqual('#/lddispatch/couriers');
		$rootScope.$digest();
		$rootScope.$apply()
	});


	it('LD dispatch state lddispatch.carrierDetails', function () {
		expect($state.href('lddispatch.carrierDetails', {id: 10})).toEqual('#/lddispatch/couriers/10');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.trip', function () {
		expect($state.href('lddispatch.trip', {page: 2})).toEqual('#/lddispatch/trip-planner?page=2');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.tripDetails', function () {
		expect($state.href('lddispatch.tripDetails', {
			id: 4,
			tab: 2,
			page: 2
		})).toEqual('#/lddispatch/trip-planner/4/2?page=2');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.tpDelivery', function () {
		expect($state.href('lddispatch.tpDelivery', {id: 2, tpId: 4})).toEqual('#/lddispatch/tp-delivery/2/tp/4/');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.ld_delivery', function () {
		expect($state.href('lddispatch.ld_delivery')).toEqual('#/lddispatch/delivery');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.pick_up', function () {
		expect($state.href('lddispatch.pick_up')).toEqual('#/lddispatch/pick-up');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.sitJobs', function () {
		expect($state.href('lddispatch.sitJobs')).toEqual('#/lddispatch/jobs-in-sit');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.agentFolio', function () {
		expect($state.href('lddispatch.agentFolio', {page: 2})).toEqual('#/lddispatch/agent-folio?page=2');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.payments', function () {
		expect($state.href('lddispatch.payments', {id: 2, tab: 2})).toEqual('#/lddispatch/payments/2/2');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.sitTpCollected', function () {
		expect($state.href('lddispatch.sitTpCollected', {
			trip_id: 3,
			job_id: 2,
			balance: 3333
		})).toEqual('#/lddispatch/trip/3/receipts/2/3333/');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.lddispatchStorages', function () {
		expect($state.href('lddispatch.lddispatchStorages', {page: 2})).toEqual('#/lddispatch/storages?page=2');
		$rootScope.$digest();
		$rootScope.$apply()
	});

	it('LD dispatch state lddispatch.lddispatchStorageCreate', function () {
		expect($state.href('lddispatch.lddispatchStorageCreate', {id: 10})).toEqual('#/lddispatch/storages/10');
		$rootScope.$digest();
		$rootScope.$apply()
	});
});
