'use strict';
import './addTripForm.styl';
import { checkPaymentsExists } from '../../agent-folio-payments/export-functions/payment.export.js';

angular
	.module('app.lddispatch')
	.directive('addTripForm', addTripForm);

addTripForm.$inject = ['$state', 'moveBoardApi', 'apiService', 'ElromcoObserver', 'events', '$window', '$mdDialog', 'datacontext', '$rootScope', '$timeout', ];

function addTripForm($state, moveBoardApi, apiService, ElromcoObserver, events, $window, $mdDialog, datacontext, $rootScope, $timeout) {
	return {
		template: require('./add-trip-form.pug'),
		restrict: 'A',
		scope: {
			trip: "=",
			jobs: "=",
			jobsTotal: "=",
			getJobs: '&',
			createTrip: '=',
			carrierFormBind: '=',
			foremanFormBind: '=',
			changeState: '=',
			jobsBusy: "=",
			carrierBusy: '=',
			foremanBusy: '=',
			carriers: '=',
			params: '=',
			paymentsExist: '='
		},
		link: function ($scope) {
			$scope.offset = -new Date().getTimezoneOffset() * 60;
			$scope.newTpDelivery = {
				job_id: 'new',
				receipt: []
			};
			$scope.ldTrucksOnly = angular.fromJson(localStorage.getItem('parklotLdTrucksOnly'));

			if (!$scope.ldTrucksOnly) {
				$scope.ldTrucksOnly = 0;
				localStorage.setItem('parklotLdTrucksOnly', angular.toJson($scope.ldTrucksOnly));
			}

			$scope.modalOpened = false;
			$scope.truckList = [];
			$scope.selectAll = false;
			$scope.selectedItems = [];
			$scope.personallyTrucks = 0;
			$scope.printSheet = null;
			$scope.tripPrintData = {};
			$scope.trip.data.trip_id = !$scope.trip.data.trip_id ? undefined : $scope.trip.data.trip_id;
			$scope.trip.data.details = !$scope.trip.data.details ? {} : $scope.trip.data.details;
			$scope.trip.data.details.start = !$scope.trip.data.details.date_start ? new Date() : new Date($scope.trip.data.details.date_start * 1000);
			$scope.trip.data.details.end = !$scope.trip.data.details.date_end ? undefined : new Date($scope.trip.data.details.date_end * 1000);
			$scope.trip.data.details.type = !$scope.trip.data.details.type ? '1' : $scope.trip.data.details.type;
			$scope.type = $scope.trip.data.details.type;
			$scope.carrierId = $scope.trip.data.carrier.ld_carrier_id;
			$scope.dateNow = new Date();
			var fieldData = datacontext.getFieldData();
			var basicSettings = angular.fromJson(fieldData.basicsettings);
			var longDistanceSettings = angular.fromJson(fieldData.longdistance);

			$scope.company = {
				name: basicSettings.company_name,
				email: basicSettings.company_email,
				phone: basicSettings.company_phone,
				address: basicSettings.company_address
			};

			$scope.printNotes = longDistanceSettings.loading_receipt_notes;
			$scope.printNotes = $scope.printNotes.replace('{companyName}', $scope.company.name);

			let user = $rootScope.currentUser.userId;
			$scope.currentUserName = `${user.field_user_first_name} ${user.field_user_last_name}`;

			$scope.carrierFormBind.form = $scope.carrierForm;
			$scope.foremanFormBind.form = $scope.foremanForm;

			$scope.openDialog = (item) => {
				item.confirmedVolumeToOpen = true;
				$scope.modalOpened = true;
			};

			$scope.closeDialog = () => {
				$scope.modalOpened = false;
			};

			$scope.$on('$destroy', () => {
				ElromcoObserver.removeEventListener(events.trip.updateTripDate, $scope);
			});

			$scope.setLdTrucksOnly = (ldTrucksOnly) => {
				localStorage.setItem('parklotLdTrucksOnly', angular.toJson(ldTrucksOnly));
			};

			$scope.getForemanList = () => {
				$scope.foremanBusy = true;
				apiService.postData(moveBoardApi.foreman.list, {active: 1, role: ["foreman"]}).then(data => {
					var formenAllData = $scope.getArrayFromObject(data.data.foreman);
					$scope.foremen = $scope.addForeman(formenAllData);
					$scope.foremanBusy = false
				}).catch(error => {

				});
			};

			$scope.getHelperList = () => {
				apiService.postData(moveBoardApi.foreman.list, {active: 1, role: ["helper"]}).then(data => {
					var helpersAllData = $scope.getArrayFromObject(data.data.helper);
					$scope.helpers = $scope.addHelper(helpersAllData)
				}).catch(error => {

				});
			};

			$scope.getArrayFromObject = (obj) => {
				var array = [];
				for (var key in obj) {
					array.push(obj[key])
				}
				return array
			};

			$scope.isItemsSelected = () => {
				return $scope.selectedItems.length > 0
			};

			$scope.addForeman = (array) => {
				var newArray = [];
				array.forEach(item => {
					var foreman = {
						foreman_name: item.field_user_first_name + " " + item.field_user_last_name,
						foreman_id: item.uid
					};
					if ($scope.trip.data.foreman) {
						if (foreman.foreman_id == $scope.trip.data.foreman.foreman_id) {
							$scope.trip.data.foreman = foreman
						}
					}
					newArray.push(foreman)
				});
				return newArray
			};

			$scope.addHelper = (array) => {
				var newArray = [];
				array.forEach(item => {
					var helper = {
						helper_name: item.field_user_first_name + " " + item.field_user_last_name,
						helper_id: item.uid
					};
					newArray.push(helper)
				});
				return newArray
			};

			$scope.closeSelectBox = (event) => {
				angular.element("md-backdrop").trigger("click");
			};

			$scope.setCarrier = () => {
				if ($scope.paymentsExist && $scope.trip.data.carrier.ld_carrier_id) {
					showDialog("You can't change carrier", "Payments for this carrier already exists");
					$scope.carrierId = $scope.trip.data.carrier.ld_carrier_id;
				} else {
					let carrier = _.find($scope.carriers, {ld_carrier_id: $scope.carrierId});
					$scope.trip.data.carrier.ld_carrier_id = $scope.carrierId;
					$scope.trip.data.carrier.name = carrier.name;
					$scope.trip.data.carrier.carrier_id = carrier.ld_carrier_id;
				}
			};

			$scope.setType = (type) => {
				if (type == 1) {
					$scope.carrierFormBind.form = $scope.carrierForm
				} else {
					$scope.foremanFormBind.form = $scope.foremanForm
				}
				if ($scope.paymentsExist) {
					$scope.type = type == 2 ? 1 : 2;
					let text = type == 2 ? 'Carrier Payments already exists' : 'Carrier Payments for TP Delivery already exists';
					showDialog("You can't change trip type", text);
				} else {
					if (!_.isEmpty($scope.jobs)) {
						$scope.type = type == 2 ? 1 : 2;
						showDialog("Remove requests from trip!", "Remove all requests from trip to change trip type");
					} else {
						showConfirm()
					}
				}
			};

			function showConfirm() {
				if ($scope.type == 1) {
					if (!_.isEmpty($scope.trip.data.trucks)) {
						var confirm = $mdDialog.confirm()
							.title('Delete Trucks?')
							.textContent('All Trucks for this Trip would be deleted')
							.ariaLabel('Lucky day')
							.ok('Delete')
							.cancel('Cancel');
						$mdDialog.show(confirm).then(function () {
							$scope.trip.data.details.type = 1;
							delete $scope.trip.data.trucks;
							$scope.createTrip($scope.trip);
						}, function () {
							$scope.trip.data.details.type = 2;
							$scope.type = 2
						});
					} else {
						$scope.trip.data.details.type = 1;
						$scope.createTrip($scope.trip);
					}
				}
				if ($scope.type == 2 && $scope.trip.data.carrier.carrier_id != 0) {
					var confirm = $mdDialog.confirm()
						.title('Delete Carrier?')
						.textContent('Carrier for this Trip would be deleted')
						.ariaLabel('Lucky day')
						.ok('Delete')
						.cancel('Cancel');
					$mdDialog.show(confirm).then(function () {
						$scope.trip.data.details.type = 2;
						$scope.createTrip($scope.trip);
					}, function () {
						$scope.trip.data.details.type = 1;
						$scope.type = 1
					});
				}
				if ($scope.type == 2 && $scope.trip.data.carrier.carrier_id == 0 || $scope.type == 2 && !$scope.trip.data.carrier) {
					$scope.trip.data.details.type = 2;
					$scope.createTrip($scope.trip);
				}
			}

			$scope.changeDates = () => {
				$scope.createTrip($scope.trip);
				ElromcoObserver.dispatch(events.trip.updateTripDate, {})
			};

			$scope.selectAllItems = (value) => {
				$scope.selectAll = value;
				$scope.selectedItems = [];
				$scope.jobs.forEach((item, index) => {
					if (value) {
						if (!checkPaymentsExists(item)) {
							item.selected = value;
							$scope.selectedItems.push(item)
						}
					} else {
						item.selected = value
					}
				});
			};

			function isAllSelected() {
				let allowedToRemoveLength = 0;
				let selectedLength = 0;
				$scope.jobs.forEach(item => {
					if (!checkPaymentsExists(item)) {
						allowedToRemoveLength++
					}
					if (item.selected) {
						selectedLength++
					}
				});

				return allowedToRemoveLength == selectedLength;
			};

			$scope.toggleSelect = (item) => {
				if (item.selected) {
					$scope.selectedItems.push(item);
					$scope.selectAll = isAllSelected()
				} else {
					var result = $.grep($scope.selectedItems, function (e) {
						return e.job_id == item.job_id;
					});
					$scope.selectedItems.splice($scope.selectedItems.indexOf(result[0]), 1);
					$scope.selectAll = isAllSelected()
				}
			};

			$scope.removeJobs = (jobs) => {
				apiService.postData(moveBoardApi.trip.removeJobs, {
					jobs_id: jobs,
					trip_id: $scope.trip.data.trip_id
				}).then(() => {
					$scope.busy = false;
					$scope.selectedItems = [];
					$scope.selectAll = false;
					jobs.forEach(job => {
						$scope.jobs.forEach((item, index) => {
							if (item.job_id === job) {
								$scope.jobs.splice(index, 1);

							}
						})
					});

					$scope.getJobs();
				});
			};

			$scope.removeSelectedJobs = () => {
				var selectedToDelete = [];
				$scope.busy = true;
				if ($scope.selectedItems.length > 0) {
					angular.forEach($scope.selectedItems, item => selectedToDelete.unshift(item.job_id));
					$scope.removeJobs(selectedToDelete)
				}

			};
			function showDialog(label, text) {
				var alert = $mdDialog.alert()
					.parent(angular.element(document.body))
					.clickOutsideToClose(true)
					.title(label)
					.textContent(text)
					.ok('OK')
				$mdDialog.show(alert)
			};

			$scope.openTpDelivery = (item) => {
				if (_.isUndefined($scope.trip.data.payroll_status)) $scope.$emit('createTripPayroll');
				
				$state.go('lddispatch.tpDelivery', {id: $state.params.id, tpId: item.job_id, paymentExist: checkPaymentsExists(item)})
			};

			$scope.openPrintPage = (page) => {
				$scope.busy = true;
				$scope.printPage = page;
				apiService.getData(moveBoardApi.trip.getTrip, {id: $scope.trip.data.trip_id}).then(data => {
					$scope.tripPrintData = data;
					$timeout(function () {
						$scope.busy = false;
						$window.print();
					}, 0);
				});
			};

			$scope.$watch(() => $(document).width(), () => {
				$scope.selectMaxWidth = $(".trip-create-modal-form__trip-form__inline__half").width()
			});

			$scope.getForemanList();
			$scope.getHelperList();
		}
	};
}
