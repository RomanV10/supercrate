'use strict';

import '../trip-info-tooltip/trip-info-tooltip.styl';

angular
	.module('app.lddispatch')
	.directive('truckItem', truckItem);

truckItem.$inject = ['$timeout'];

function truckItem($timeout) {
	return {
		template: require('./truck-item.pug'),
		restrict: 'EA',
		scope: {
			item: '=',
			truck: '=',
			openModal: '=',
			tripId: '='
		},
		link: function ($scope, $element) {
			var cssTimeout;
			var container = $element[0].querySelector(".truck__item");
			$scope.offset = -new Date().getTimezoneOffset() * 60;
			$scope.showItem = (item) => {
				if (item.width < 350 && item.margin >= 0) {
					item.resizeble = true
				}

				$timeout.cancel( cssTimeout );
				cssTimeout = $timeout(function(){
					angular.element(container).addClass('truck__item_bigger-index')
				}, 400)
			};
			$scope.hideItem = (item) => {
				item.resizeble = false;
				$timeout.cancel( cssTimeout );
				angular.element(container).removeClass('truck__item_bigger-index')
			};
			$scope.$on('destroy', () => {
				$timeout.cancel( cssTimeout );
			})
		}
	};
}
