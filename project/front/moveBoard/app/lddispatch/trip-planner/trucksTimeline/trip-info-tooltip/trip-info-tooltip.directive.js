import './trip-info-tooltip.styl';

'use strict';

angular
	.module('app.lddispatch')
	.directive('tripInfoTooltip', tripInfoTooltip);

tripInfoTooltip.$inject = ['$state', 'tripRequest', 'datacontext', '$document'];

function tripInfoTooltip($state, tripRequest, datacontext, $document) {
	return {
		template: require('./trip-info-tooltip.pug'),
		restrict: 'A',
		scope: {
			trip: '=',
			truck: '=',
			position: '=',
			showTooltip: '=',
			closeModal: '&',
			deleteTruck: '&',
			tripId: '='
		},
		link: function ($scope) {
			$scope.timezoneOffset = -new Date().getTimezoneOffset() * 60;
			var fieldData = datacontext.getFieldData();
			var calcSettings = angular.fromJson(fieldData.calcsettings);

			var localServices = [1, 2, 3, 4, 6, 8];
			$scope.isDoubleDriveTime = calcSettings.doubleDriveTime
				&& $scope.trip.full_info.field_double_travel_time
				&& !_.isNull($scope.trip.full_info.field_double_travel_time.raw)
				&& localServices.indexOf(parseInt($scope.trip.full_info.service_type.raw)) >= 0;

			$document.on("mouseup touchstart", function (e) {
				let container = angular.element('.trip-info-tooltip');

				if (!container.is(e.target)
					&& container.has(e.target).length === 0
					&& $scope.showTooltip == true) {
					$scope.closeModal($scope.trip)
				}
			});

			angular.element('.trip-info-tooltip').mouseover(function (event) {
				event.stopPropagation()
			});

			$scope.openTrip = (id) => {
				let tab = 0;
				$state.go("lddispatch.tripDetails", { id, tab});
			};

			$scope.openRequest = tripRequest.openRequest

		}
	};
}
