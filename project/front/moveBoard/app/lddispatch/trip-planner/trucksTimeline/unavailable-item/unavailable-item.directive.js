'use strict';

import '../timeline/trucksTimeline.styl';

angular
	.module('app.lddispatch')
	.directive('unavailableItem', unavailableItem);

function unavailableItem() {
	return {
		template: require('./unavailable-item.pug'),
		restrict: 'EA',
		scope: {
			item: '='
		},
		link: function ($scope) {

		}
	};
}
