import './trucksTimeline.styl';

'use strict';

angular
	.module('app.lddispatch')
	.directive('trucksTimeline', trucksTimeline);

trucksTimeline.$inject = ['$state', 'apiService', 'moveBoardApi', '$mdDateRangePicker', '$mdDialog', 'TrucksTimelineService', 'MomentWrapperService'];

function trucksTimeline($state, apiService, moveBoardApi, $mdDateRangePicker, $mdDialog, TrucksTimelineService, MomentWrapperService) {
	return {
		template: require('./trucksTimeline.pug'),
		restrict: 'A',
		scope: {
			from: '=',
			to: '=',
			status: '=',
			setStep: '=',
			ldTrucksOnly: '=',
			createTrip: '=',
			trip: '=',
			changeState: '='
		},
		link: function ($scope, element) {
			var timeWrapper = MomentWrapperService.getZeroMoment;
			$scope.offset = -new Date().getTimezoneOffset() * 60;
			var parseTimestampToJsDate = MomentWrapperService.parseTimestampToJsDate;
			const TRUCK_HEIGHT = 30;
			const TIMELINE_HEADER_HEIGHT = 45;
			const HOURS_STEP_WIDTH = 100;
			const MARGIN_FROM_LEFT_HEADER = 50;
			const MINUTES_IN_HOUR = 60;
			const THIS_DAY_SECONDS = 86399;
			const WHOLE_DAY_SECONDS = 86400;
			let validLeftHeaderWidth;
			$scope.tripCopy = angular.copy($scope.trip);
			var isShowDeleteDialog = false;

			var changeTripDatesAndSetTripBool = false;
			$scope.tripId = $state.params.id;
			$scope.showTooltip = false;
			setWrappedValues();
			$scope.selectedRange = {
				dateStart: parseTimestampToJsDate($scope.date_from),
				showTemplate: false,
				fullscreen: false,
				disableTemplates: true
			};
			$scope.selectedRange.dateEnd = (!$scope.to || $scope.fromUnix > $scope.toUnix) ? parseTimestampToJsDate($scope.date_from) : parseTimestampToJsDate($scope.date_to),

			$scope.selectRange = function ($event, showTemplate) {
				$scope.selectedRange.showTemplate = showTemplate;
				$mdDateRangePicker
					.show({
						targetEvent: $event,
						model: $scope.selectedRange
					})
					.then(function (result) {
						if (result) $scope.selectedRange = result;
						$scope.setStep();
					});
			};

			$scope.openModal = (trip, truck, event) => {
				if (!$scope.showTooltip) {
					let scrollOffset = angular.element("#main-wrapper").scrollTop();
					let offsetX = event.clientX - 50;
					let offsetY = event.clientY - 180 + scrollOffset;
					$scope.tripCopy = angular.copy(trip);
					$scope.truck = truck;
					$scope.showTooltip = true;
					if ($scope.trip.entity_type != 8) {
						if (event.clientY- 180 + scrollOffset + 454 > angular.element('#main-wrapper').prop('scrollHeight')) {
							offsetY = offsetY - 452
						}
					}
					if (angular.element(document).width() - 400 < event.clientX) {
						offsetX = offsetX - 350;
					}
					$scope.tooltipPosition = {
						posX: offsetX,
						posY: offsetY
					};
				}
			};

			$scope.closeModal = () => {
				$scope.showTooltip = false;
				delete $scope.tooltipPosition
			};

			function checkActiveTruck(truck) {
				truck.active = truck.items && truck.items.reduce((accum, currentItem) => {
					return accum || currentItem.entity_type == 8 && currentItem.entity_id == $scope.tripId;
				}, false);
				if (truck.active) {
					truck.blockSelectTruckButton = false;
				}
			}

			$scope.getTrucks = () => {
				$scope.showTrucks = true;
				$scope.showTrailers = true;
				$scope.busy = true;
				var date_from = angular.copy(timeWrapper(moment($scope.selectedRange.dateStart).startOf('day')).unix());
				var date_to = angular.copy(timeWrapper(moment($scope.selectedRange.dateEnd).endOf('day')).unix());
				$scope.trucksCount = 0;
				$scope.trailersCount = 0;
				var data = {
					date_from: date_from,
					date_to: date_to,
					by_time: 0,
					full_info: 1,
					only_trailer: $scope.ldTrucksOnly,
					trucks_only: 0,
					conditions: {
						status: [2, 3]
					}
				};
				apiService.getData(moveBoardApi.truck.list, data).then(data => {
					$scope.busy = false;
					$scope.trucks = data.data.trucks || [];
					$scope.trailers = data.data.trailers || [];
					let dayBeforeDateStart = Number(date_from) - WHOLE_DAY_SECONDS;
					let dayAfterDateEnd = Number(date_to) + WHOLE_DAY_SECONDS;
					if ($scope.trucks) {
						for (var key in $scope.trucks) {
							if (!_.isNull($scope.trucks[key].unavailable)) {
								_.forEach($scope.trucks[key].unavailable, date => {

									if (Number(date) >= dayBeforeDateStart && Number(date) <= dayAfterDateEnd) {
										let disabledDate = {
											from_date: Number(date),
											to_date: Number(date) + THIS_DAY_SECONDS,
											blocked: true,
										};
										if (!$scope.trucks[key].items) {
											$scope.trucks[key].items = [];
										}
										$scope.trucks[key].items.push(disabledDate);
									}
								});
							}
							$scope.trucks[key] = $scope.addWidthAndMargin($scope.trucks[key], date_from);
							checkActiveTruck($scope.trucks[key]);
							$scope.trucksCount++;
						}
						$scope.trucksTimelineHeight = $scope.trucksCount * TRUCK_HEIGHT + TIMELINE_HEADER_HEIGHT;
					}
					if ($scope.trailers) {
						for (var key in $scope.trailers) {
							if (!_.isNull($scope.trailers[key].unavailable)) {
								_.forEach($scope.trailers[key].unavailable, date => {

									if (Number(date) >= dayBeforeDateStart && Number(date) <= dayAfterDateEnd) {
										let disabledDate = {
											from_date: Number(date),
											to_date: Number(date) + THIS_DAY_SECONDS,
											blocked: true,
										};
										if (!$scope.trailers[key].items) {
											$scope.trailers[key].items = [];
										}
										$scope.trailers[key].items.push(disabledDate);
									}
								});
							}
							$scope.trailers[key] = $scope.addWidthAndMargin($scope.trailers[key], date_from);
							checkActiveTruck($scope.trailers[key]);
							$scope.trailersCount++;
						}
						$scope.trailersTimelineHeight = $scope.trailersCount * TRUCK_HEIGHT + TIMELINE_HEADER_HEIGHT;
					}
				});
			};

			function addTruck(item) {
				var data = {
					entity_type: 8,
					entity_id: $state.params.id,
					date: 0,
					from_date: $scope.fromUnix,
					to_date: $scope.toUnix,
					type: $scope.status,
					status: 3,
					parking_truck: {
						truck_id: item.truck_id,
						truck_name: item.truck_name
					}
				};
				$scope.busy = true;
				item.blockSelectTruckButton = true;
				apiService.postData(moveBoardApi.truck.list, {data: data}).then(data => {
					$scope.busy = false;
					if (changeTripDatesAndSetTripBool) {
						$scope.createTrip($scope.trip);
						changeTripDatesAndSetTripBool = false;
					}
					var newTruck = data.data;
					if (!item.items || item.items.length <= 0) {
						item.items = [];
					}
					item.items.push(newTruck);

					$scope.addWidthAndMargin(item,
						timeWrapper(moment($scope.selectedRange.dateStart).startOf('day')).unix());
					checkActiveTruck(item);
				}).catch(error => {
					$scope.busy = false;
				});
			}

			$scope.deleteTruck = (truck, busy) => {
				$scope.busy = true;
				apiService.postData(moveBoardApi.truck.deleteTruck, {
					pid: $scope.tripCopy.pid,
					truck_id: truck.truck_id
				}).then(data => {
					$scope.busy = false;
					$scope.closeModal();
					var result = $.grep(truck.items, function (e) {
						return e.pid == $scope.tripCopy.pid;
					});
					truck.items.splice(truck.items.indexOf(result[0]), 1);
					if (truck.items && truck.items.length > 0) {
						$scope.checkValidationForSelect(truck);
					} else if (!truck.items || truck.items.length <= 0) {
						truck.blockSelectTruckButton = false;
					}
					checkActiveTruck(truck);
				}).catch(error => {
					$scope.busy = false;
					console.log(error);
				});
			};
			$scope.deleteAllTrucksForTrip = () => {
				$scope.busy = true;
				apiService.postData(moveBoardApi.truck.deleteByEntity, {
					entity_id: $scope.tripId,
					entity_type: 8
				}).then(data => {
					$scope.busy = false;
					if (changeTripDatesAndSetTripBool) {
						addTruck($scope.temporaryTruck);
						delete $scope.temporaryTruck;
					}
					for (var key in $scope.trucks) {
						if ($scope.trucks[key].items && $scope.trucks[key].items.length > 0) {
							$scope.trucks[key].items.forEach((item, index) => {
								if (item.entity_id == $scope.tripId) {
									$scope.trucks[key].items.splice(index, 1);
								}
							});
						}
						isShowDeleteDialog = false;
						$scope.checkValidationForSelect($scope.trucks[key]);
						checkActiveTruck($scope.trucks[key]);
					}
					for (var key in $scope.trailers) {
						if ($scope.trailers[key].items && $scope.trailers[key].items.length > 0) {
							$scope.trailers[key].items.forEach((item, index) => {
								if (item.entity_id == $scope.tripId) {
									$scope.trailers[key].items.splice(index, 1);
								}
							});
						}
						isShowDeleteDialog = false;
						$scope.checkValidationForSelect($scope.trailers[key]);
						checkActiveTruck($scope.trailers[key]);
					}
				}).catch(error => {
					$scope.busy = false;
				});
			};
			function setValidToDelete() {
				isShowDeleteDialog = true;
			}

			$scope.checkValidationForSelect = (object) => {
				object.isValidForSelect = true;
				object.isValidRangeForSelect = true;
				object.blockSelectTruckButton = false;

				if (object.items && object.items.length > 0) {
					object.items.forEach(item => {
						let from = Number(item.from_date);
						let to = Number(item.to_date);
						let tripFromTimestamp = Number($scope.fromUnix);
						let tripToTimestamp = Number($scope.toUnix);
						let fromSelecetedRangeUnix = timeWrapper(
							moment($scope.selectedRange.dateStart).startOf('day')).unix();
						let toSelectedRangeUnix = timeWrapper(moment($scope.selectedRange.dateEnd).endOf('day')).unix();

						if (((from >= tripFromTimestamp && from <= tripToTimestamp)
							|| (tripFromTimestamp >= from && tripFromTimestamp <= to)) || !tripToTimestamp) {
							object.isValidForSelect = false;
						}
						if (((from >= fromSelecetedRangeUnix && from <= toSelectedRangeUnix)
							|| (fromSelecetedRangeUnix >= from && fromSelecetedRangeUnix <= to))
						) {
							object.isValidRangeForSelect = false;
						}

						if ((fromSelecetedRangeUnix >= tripFromTimestamp && fromSelecetedRangeUnix <= tripToTimestamp)
							|| (tripFromTimestamp >= fromSelecetedRangeUnix && tripFromTimestamp <= toSelectedRangeUnix)) {
							if (!object.isValidForSelect) {
								object.blockSelectTruckButton = true;
							}
						} else {
							if (!object.isValidRangeForSelect) {
								object.blockSelectTruckButton = true;
							}
						}


						if (item.entity_id == $scope.tripId) {
							setValidToDelete();
						}
					});
				}
			};
			
			function findCurrentTripInParklot(currentItem) {
				return currentItem.entity_type == 8 && currentItem.entity_id == $scope.tripId;
			};

			$scope.chackValidationBeforeAddingTrip = (truck) => {
				if (truck.active) {
					$scope.tripCopy = angular.copy(truck.items.find(findCurrentTripInParklot));
					$scope.deleteTruck(truck, null);
					return;
				}
				if (!$scope.trip.data.foreman.foreman_id) {
					$scope.changeState(0);
				} else {
					let fromSelecetedRangeUnix = timeWrapper(
						moment($scope.selectedRange.dateStart).startOf('day')).unix();
					let toSelectedRangeUnix = timeWrapper(moment($scope.selectedRange.dateEnd).endOf('day')).unix();
					if (toSelectedRangeUnix < $scope.toUnix
						|| fromSelecetedRangeUnix > $scope.fromUnix
						|| !$scope.to || $scope.fromUnix > $scope.toUnix) {
						TrucksTimelineService.showChangeDatesDialog($scope.selectedRange.dateStart,
							$scope.selectedRange.dateEnd)
							.then(function () {
								$scope.temporraryDates = {
									date_from: $scope.from,
									date_to: $scope.to
								};
								$scope.temporaryTruck = truck;
								changeTripDatesAndSetTripBool = true;
								updateDatesValues($scope.selectedRange.dateStart, $scope.selectedRange.dateEnd);
								$scope.deleteAllTrucksForTrip();
							});
					} else {
						addTruck(truck);
					}
				}
			};

			function updateDatesValues(start, end) {
				$scope.from = start;
				$scope.to = end;
				setWrappedValues();
			}

			function setWrappedValues() {
				$scope.date_from = angular.copy(timeWrapper(moment($scope.from).startOf('day')));
				$scope.fromUnix = $scope.date_from.unix();
				$scope.date_to = !$scope.to ? undefined : angular.copy(timeWrapper(moment($scope.to).endOf('day')));
				$scope.toUnix = !$scope.date_to ? undefined : $scope.date_to.unix();
			}

			$scope.addWidthAndMargin = (object, from_date) => {
				if (object.items && object.items.length > 0) {
					$scope.checkValidationForSelect(object);
					object.items.forEach(trip => {
						trip.margin = 0;
						trip.width = 0;
						var marginDiff = moment(trip.from_date * 1000).diff(from_date * 1000, 'minutes');
						var widthDiff = moment(trip.to_date * 1000).diff(moment(trip.from_date * 1000), 'minutes');
						trip.margin = (marginDiff / $scope.hoursStep / MINUTES_IN_HOUR * HOURS_STEP_WIDTH) + MARGIN_FROM_LEFT_HEADER;
						trip.width = widthDiff / $scope.hoursStep / MINUTES_IN_HOUR * HOURS_STEP_WIDTH;
						if (trip.blocked) {
							trip.unavailable_labels_count = getNumberOfUnavailableLabelsCount(trip.width);
						}
					});
				}
				if (!object.items || object.items.length <= 0) {
					$scope.checkValidationForSelect(object);
				}
				return object;
			};

			function getNumberOfUnavailableLabelsCount(width) {
				let num;
				switch (true) {
				case (width <= 400) :
					num = 1;
					break;
				case (width > 400 && width <= 800) :
					num = 2;
					break;
				case (width > 800 && width <= 1200) :
					num = 3;
					break;
				case (width > 1200 && width <= 1600) :
					num = 4;
					break;
				case (width > 2000) :
					num = 5;
					break;
				}
				return new Array(num);
			}

			$scope.makeArray = (step, hoursStep) => {
				$scope.dates = [];
				var date = angular.copy(timeWrapper(moment($scope.selectedRange.dateStart).startOf('day')));
				for (var i = 0; i <= step; i++) {
					if (i > 0) {
						date = moment(date, 'MM-DD-YY').add(1, 'days');
					}
					if (step == 0) {

					}
					let item = {
						index: i,
						date: parseTimestampToJsDate(date)
					};
					$scope.dates.push(item);
					for (var j = 1; j < 24 / hoursStep && i != step; j++) {
						var blank = {
							index: i,
							hour: (24 / Math.round(24 / hoursStep)) * j + 'hr'
						};
						$scope.dates.push(blank);
					}
				}
				$scope.$watch(() => angular.element('.trucks-timeline').width(), () => {
					let timelineWidth = $scope.dates.length * 100;
					let leftOffset = angular.element('.trucks-timeline').offset().left;
					let containerWidth = angular.element('.trucks-timeline').width();
					let leftHeaderElement = element.find('.trucks-timeline__left-header');
					if (!validLeftHeaderWidth) {
						let headerWidth = leftHeaderElement.width();
						leftHeaderElement.css('width', headerWidth + 'px');
						validLeftHeaderWidth = headerWidth;
					}
					var minX = leftOffset + containerWidth - timelineWidth - validLeftHeaderWidth;
					var maxX = leftOffset;
					angular.element('.trucks-timeline__line')
						.draggable({
							axis: 'x',
							cursor: 'move',
							containment: [minX, 0, maxX, 0],
							scroll: true
						})
						.css('padding-left', validLeftHeaderWidth + 'px');
				});

			};
			$scope.setStep = () => {
				let fromSelecetedRange = timeWrapper(moment($scope.selectedRange.dateStart).startOf('day'));
				let toSelectedRange = timeWrapper(moment($scope.selectedRange.dateEnd).endOf('day'));
				var step = toSelectedRange.diff(fromSelecetedRange, 'days') + 1;
				if (step > 24) {
					$scope.hoursStep = 24;
				} else {
					if (step >= 10 && step < 24) {
						$scope.hoursStep = 12;
					} else {
						if (step == 5) {
							$scope.hoursStep = 4;
						}
						else if (step == 7) {
							$scope.hoursStep = 6;
						}
						else if (step == 9) {
							$scope.hoursStep = 8;
						} else {
							$scope.hoursStep = step;
						}
					}
				}
				if (step > 0) {
					$scope.makeArray(step, $scope.hoursStep);
					$scope.getTrucks();
				}
			};
			$scope.showConfirm = function (dateFrom, dateTo, oldDateFrom, oldDateTo,
				isDateStartChangedAndBeforeSelectedRangeFrom, isDateEndChangedAndAfterSelectedRangeTo) {
				var confirm = $mdDialog.confirm()
					.title('Delete Trucks?')
					.textContent('All Trucks for this Trip would be deleted')
					.ariaLabel('Lucky day')
					.ok('Delete')
					.cancel('Cancel');
				$mdDialog.show(confirm).then(function () {
					setWrappedValues();
					$scope.deleteAllTrucksForTrip();
					$scope.createTrip($scope.trip);
					if (isDateStartChangedAndBeforeSelectedRangeFrom) {
						changeDateRangeFrom(dateFrom);
					}
					if (isDateEndChangedAndAfterSelectedRangeTo) {
						changeDateRangeTo(dateTo);
					}
				}, function () {
					$scope.datesChangeWatchers();
					updateDatesValues(oldDateFrom, oldDateTo);
					changeTripDatesAndSetTripBool = false;
					dateWatchers();
				});
			};

			function changeDateRangeFrom(date) {
				$scope.selectedRange.dateStart = parseTimestampToJsDate(timeWrapper(moment(date).startOf('day')));
				$scope.setStep();
			}

			function changeDateRangeTo(date) {
				$scope.selectedRange.dateEnd = parseTimestampToJsDate(timeWrapper(moment(date).endOf('day')));
				$scope.setStep();
			}

			function dateWatchers() {
				$scope.datesChangeWatchers = $scope.$watchGroup(['from', 'to'], function (newV, oldV) {
					if (newV[0] != oldV[0] || newV[1] != oldV[1]) {
						$scope.from = newV[0];
						$scope.to = newV[1];
						let isDateStartChangedAndBeforeSelectedRangeFrom = newV[0] != oldV[0] && moment(newV[0]).isBefore($scope.selectedRange.dateStart);
						let isDateEndChangedAndAfterSelectedRangeTo = newV[1] != oldV[1] && moment(newV[1]).isAfter($scope.selectedRange.dateEnd);
						if (!changeTripDatesAndSetTripBool) {
							if (isShowDeleteDialog) {
								$scope.showConfirm($scope.from, $scope.to, oldV[0], oldV[1], isDateStartChangedAndBeforeSelectedRangeFrom, isDateEndChangedAndAfterSelectedRangeTo)
							} else {
								isShowDeleteDialog = false;
								setWrappedValues();
								if (isDateStartChangedAndBeforeSelectedRangeFrom || isDateEndChangedAndAfterSelectedRangeTo) {
									if (isDateStartChangedAndBeforeSelectedRangeFrom) {
										changeDateRangeFrom(newV[0]);
									}
									if (isDateEndChangedAndAfterSelectedRangeTo) {
										changeDateRangeTo(newV[1]);
									}
								} else {
									for (var key in $scope.trucks) {
										$scope.checkValidationForSelect($scope.trucks[key]);
									}
									for (var key in $scope.trailers) {
										$scope.checkValidationForSelect($scope.trailers[key]);
									}
								}
							}
						} else {
							if (isDateStartChangedAndBeforeSelectedRangeFrom) {
								changeDateRangeFrom(newV[0]);
							}
							if (isDateEndChangedAndAfterSelectedRangeTo) {
								changeDateRangeTo(newV[1]);
							}
						}
					}
				});

			}

			dateWatchers();
			$scope.setStep();

			$scope.hideTrucksParklot = () => {
				$scope.showTrucks = false;
				angular.element('#trucksTimelineId').animate({
					height: '0px'
				}, 300);
			};
			$scope.showTrucksParklot = () => {
				$scope.showTrucks = true;
				angular.element('#trucksTimelineId').animate({
					height: $scope.trucksTimelineHeight + 'px'
				}, 300);
			};
			$scope.hideTrailersParklot = () => {
				$scope.showTrailers = false;
				angular.element('#trailersTimelineId').animate({
					height: '0px'
				}, 300);
			};
			$scope.showTrailersParklot = () => {
				$scope.showTrailers = true;
				angular.element('#trailersTimelineId').animate({
					height: $scope.trailersTimelineHeight + 'px'
				}, 300);
			};

		}
	};
}
