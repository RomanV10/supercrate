'use strict';

angular
	.module('app.lddispatch')
	.factory('TrucksTimelineService', TrucksTimelineService)
TrucksTimelineService.$inject = ['$q' ,'$mdDialog']

function TrucksTimelineService ($q, $mdDialog) {
	var service = {}
	service.showChangeDatesDialog = showChangeDatesDialog

	return service

	function showChangeDatesDialog(newDateFrom, newDateTo, oldDateFrom, oldDateTo) {
		var deferred = $q.defer()

		let newFrom = moment(newDateFrom).format('MM/DD/YYYY')
		let newTo = moment(newDateTo).format('MM/DD/YYYY')
		let oldFrom = moment(oldDateFrom).format('MM/DD/YYYY')
		let oldTo = moment(oldDateTo).format('MM/DD/YYYY')
		let text = `<br><span>Date From: ${oldFrom} => ${newFrom}</span> <br> <span>Date To: ${oldTo} => ${newTo}</span>`
		var confirm = $mdDialog.confirm()
			.title('Do you want to change trip dates, remove all previous trucks and place new one?')
			.htmlContent(text)
			.ariaLabel('Lucky day')
			.ok('Yes')
			.cancel('No');
		$mdDialog.show(confirm).then(function() {
			deferred.resolve()
		}, function() {

		});
		return deferred.promise
	}
}
