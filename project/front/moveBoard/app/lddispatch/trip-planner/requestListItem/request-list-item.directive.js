'use strict';
import './requestListItem.styl'

angular
	.module('app.lddispatch')
	.directive('requestListItem', requestListItem);

requestListItem.$inject = ['$state', 'apiService', 'moveBoardApi', '$q'];

function requestListItem($state, apiService, moveBoardApi, $q) {
	return {
		template: require('./requestListItem.pug'),
		restrict: 'EA',
		scope: {
			item: '=',
			selectedItems: '=',
			isAllSelected: '&',
			offset: '='
		},

		link: function ($scope) {
			$scope.toggleSelect = (item) => {
				if (item.a_a_selected) {
					$scope.selectedItems.unshift(item)
					$scope.isAllSelected()
				} else {
					var result = $.grep($scope.selectedItems, function (e) {
						return e.ld_nid == item.ld_nid;
					})
					$scope.selectedItems.splice($scope.selectedItems.indexOf(result[0]), 1)
					$scope.isAllSelected()
				}
			};
		}
	};
}
