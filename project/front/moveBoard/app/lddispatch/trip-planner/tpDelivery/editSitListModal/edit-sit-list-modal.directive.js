'use strict';
import './editSitListModal.styl';
import tripPlannerConstants from './../../../trip-planner-constants.json';

angular
	.module('app.lddispatch')
	.directive('editSitListModal', editSitListModal);

editSitListModal.$inject = ['$state',  'apiService', 'moveBoardApi'];

function editSitListModal($state, apiService, moveBoardApi) {
	return {
		template: require('./editSitListModal.pug'),
		restrict: 'EA',
		scope: {
			closeModal: '&',
			id: '=',
			tp: '=',
			serviceTypes: '=',
			modalPosition: '='
		},
		link: function($scope) {
			if (!$scope.tp.sit) {
				$scope.sit = tripPlannerConstants.initialSit;
				$scope.sit.lot_number = []
				$scope.sit.lots_info = []
				$scope.sit.lot_number.push({})
				$scope.sit.lots_info.push({})
				$scope.date = new Date()
				delete $scope.sit.blankets
				delete $scope.sit.rooms
			} else {
				$scope.sit = angular.copy($scope.tp.sit)
				$scope.date = new Date($scope.sit.date * 1000)
				if (_.isEmpty($scope.sit.lot_number)) {
					$scope.sit.lot_number = []
					$scope.sit.lot_number.push({})
				}
				if (_.isEmpty($scope.sit.lots_info)) {
					$scope.sit.lots_info = []
					$scope.sit.lots_info.push({})
				}
			}

			$scope.error = false
			$scope.colors = [
				{color:'blue', name:'Blue'},
				{color:'green', name:'Green'},
				{color:'orange', name:'Orange'},
				{color:'red', name:'Red'},
				{color:'white', name:'White'},
				{color:'yellow', name:'Yellow'}]
			$scope.close = () => {
				delete $scope.item.confirmedToOpen;
				$scope.closeModal();
			};
			$scope.addLotsInfo = () => {
				$scope.sit.lots_info.push({})
			}
			$scope.addLotNumber = () => {
				$scope.sit.lot_number.push({})
			}
			$scope.removeLotsInfo = (index) => {
				$scope.sit.lots_info.splice(index, 1)
			}
			$scope.removeLotNumber = (index) => {
				$scope.sit.lot_number.splice(index, 1)
			}
			$scope.getForman = () => {
				$scope.busy = true
				apiService.postData(moveBoardApi.foreman.list, {active:1, role:["foreman"]}).then(res => {
					$scope.busy = false
					$scope.foremen = getArrayFromObject(res.data.foreman)
					if ($scope.sit.foreman) {

						$scope.foreman = _.find($scope.foremen, {uid: $scope.sit.foreman})
					}
				}).catch(error => {
				});
				function getArrayFromObject(obj){
					var array = []
					for(var key in obj) {
						array.push(obj[key])
					}
					return array
				}
			}
			$scope.getStorages = () => {
				apiService.getData(moveBoardApi.ldStorages.list, {short: 1, pageSize: 999}).then(res => {
					$scope.storages = res.data.items
					if ($scope.sit.storage_id) {
						$scope.storage = _.find($scope.storages, {storage_id: $scope.sit.storage_id})
					}
				}).catch(error => {
				});
			}
			$scope.setStorage = (item) => {
				if (item) {
					$scope.sit.storage_id = item.storage_id
					$scope.sit.storage_name = item.name
				} else {
					delete $scope.sit.first_name
					delete $scope.sit.storage_name
				}
			}
			$scope.setForeman = (item) => {
				if (item) {
					$scope.sit.foreman = item.uid
					$scope.sit.first_name = item.field_user_first_name
					$scope.sit.last_name = item.field_user_last_name
				} else {
					delete $scope.sit.foreman
					delete $scope.sit.first_name
					delete $scope.sit.last_name
				}
			}

			$scope.checkLots = (lots) => {
				lots.forEach((item, index) => {
					if (_.isEmpty(item)) {
						lots.splice(index, 1)
					}
				})
				return lots

			}
			$scope.updateSit = () => {
				$scope.sit.lot_number = $scope.checkLots($scope.sit.lot_number)
				$scope.sit.lots_info = $scope.checkLots($scope.sit.lots_info)
				if ($scope.id == 'new') {
					$scope.tp.sit = angular.copy($scope.sit)
					$scope.tp.sit.date = moment($scope.date).unix()
					$scope.closeModal()
				} else {
					$scope.sit.date = moment($scope.date).unix()
					$scope.busy = true
					apiService.putData(moveBoardApi.requestSIT.create, {id: $scope.id, entity_type:1, data: $scope.sit}).then(res => {
						$scope.busy = false;
						$scope.tp.sit = angular.copy($scope.sit)
						$scope.tp.sit.date = new Date(res.data.date)
						$scope.closeModal()
					}).catch(error => {
						if (error.status < 0) {
							$scope.busy = false;
							$scope.error = true
						}
					});
				}
			}

			$scope.getForman()
			$scope.getStorages()
		}
	}
}
