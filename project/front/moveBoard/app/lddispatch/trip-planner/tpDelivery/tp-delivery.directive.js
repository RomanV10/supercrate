'use strict';
import './tpDelivery.styl'
import tripPlannerConstants from './../../trip-planner-constants.json';

angular
	.module('app.lddispatch')
	.directive('tpDelivery', tpDelivery);

/*@ngInject*/
function tpDelivery($state, apiService, moveBoardApi, routerTracker, statesService, PaymentServices, $mdDialog, $q, $timeout, additionalServicesFactory) {
	return {
		template: require('./tpDelivery.pug'),
		restrict: 'A',

		link: function ($scope) {
			var responseDellay;
			const ZERO_AMOUNT = 0;
			const ENTITY_TYPE = 1;
			$scope.zeroData = {
				volume_cf: 0,
				rate_per_cf: 0,
				paid: 0,
				to_pay: 0,
				received: 0,
				to_receive: 0,
				tp_collected: 0,
				weight: 0,
				services_total: 0,
				job_cost: 0,
				job_total: 0,
				order_balance: 0
			};
			$scope.weight = 0.00;
			$scope.modalOpened = false;
			$scope.newSitItem = {};
			$scope.tripId = $state.params.id;
			$scope.tpId = $state.params.tpId;
			$scope.paymentExist = $state.params.paymentExist === 'true';

			$scope.allStates = angular.copy(statesService.states());
			$scope.serviceTypes = tripPlannerConstants.sitServiceTypes;

			$scope.getBack = function () {
				var routeHistory = routerTracker.getRouteHistory();
				if (!_.isEmpty(routeHistory)) {
					_.each(routeHistory, function (history, i) {
						let name = history.route.name;
						if (name !== 'lddispatch.tpDelivery') {
							var routeName = history.route.name;
							var routeParams = history.routeParams;
							$state.go(routeName, routeParams);
							return false
						}
						if (i === routeHistory.length - 1) {
							$state.go('lddispatch.tripDetails', {id: $scope.tripId, tab: 0})
						}
					})
				} else {
					$state.go('lddispatch.tripDetails', {id: $scope.tripId, tab: 0})
				}
			};

			$scope.openModal = (type) => {
				if (!$scope.modalOpened) {
					$scope.modalOpened = true
				}
				$scope.$watch(() => $("#main-wrapper").width(), () => {
					$scope.modalPosition = {
						leftOffset: $('.tp-delivery__list').offset().left - 46,
						topOffset: $('.tp-delivery__list').offset().top + $("#main-wrapper").scrollTop() + 4
					}
				})
			};

			$scope.closeModal = () => {
				$scope.modalOpened = false;
			};

			function addZeroValues(object) {
				var zeroData = angular.copy($scope.zeroData);
				for (var key in zeroData) {
					for (var key2 in object) {
						if (key == key2) {
							zeroData[key] = object[key]
						}
					}
				}
				return zeroData
			}
			$scope.setSellaryData = () => {
				var data = addZeroValues($scope.tp.closing);
				data.tp_collected = data.order_balance;
				$scope.closingBusy = true;
				apiService.postData(moveBoardApi.tpDelivery.calcClosing, {type: 1, data: data}).then(data => {
					$scope.tp.closing.job_cost = data.data.job_cost;
					$scope.tp.closing.job_total = data.data.job_total;
					$scope.tp.closing.tp_collected = data.data.tp_collected;
					$scope.tp.closing.received = data.data.received;
					$scope.tp.closing.to_receive = data.data.to_receive;
					$scope.tp.closing.paid = data.data.paid;
					$scope.tp.closing.to_pay = data.data.to_pay;
					if (!$scope.tp.closing.volume_cf || $scope.tp.closing.volume_cf == 0) {
						delete $scope.weight
					} else {
						$scope.weight = ($scope.tp.closing.volume_cf * 7).toFixed(2);
					}
					$scope.closingBusy = false
				}).catch(error => {
					$scope.closingBusy = false
				});
			};

			$scope.addServices = (job) => {
				if ($scope.tpId != 'new') {
					apiService.postData(moveBoardApi.trip.getServices, {job_id: job.id}).then(data => {
						job.services = data.data;
						job.reservation_rate = {};
						job.reservation_rate.value = "0";
						job.field_company_flags = {};
						job.field_company_flags.value = "0";
						job.nid = job.job_id;
						$scope.openServices(job)
					});
				}
				else {
					if (!job.services || job.services.length <= 0) {
						job.services = [];
					}
					job.reservation_rate = {};
					job.reservation_rate.value = "0";
					job.field_company_flags = {};
					job.field_company_flags.value = "0";
					job.nid = 'New';
					$scope.openServices(job)
				}
			};

			function calcServicesTotal(array) {
				var total = 0;
				array.forEach(service => {
					var services_total = 0;
					service.extra_services.forEach((item, index) => {
						if (index == 0) {
							services_total = item.services_default_value
						} else {
							services_total = services_total * parseFloat(item.services_default_value)
						}
					});
					total = total + services_total
				});
				return total
			}

			$scope.openServices = (job) => {
				additionalServicesFactory.openAddServicesModal(job, job, 'services', job);
				let servicesWatcher = $scope.$watch(() => job.services, (newV, oldV) => {
					if (!compareArrays(oldV, newV)) {
						servicesWatcher();
						$scope.tp.closing.services_total = calcServicesTotal(newV);
						$scope.setSellaryData()
					}
				});
			};
			function compareArrays(array1, array2) {
				Array.prototype.equals = function (array) {
					if (!array)
						return false;

					if (this.length != array.length)
						return false;

					for (var i = 0, l = this.length; i < l; i++) {
						if (this[i] instanceof Array && array[i] instanceof Array) {
							if (!this[i].equals(array[i]))
								return false;
						} else if (this[i] != array[i]) {
							return false;
						}
					}
					return true;
				};
				return array1.equals(array2)
			}
			$scope.setState = (state) => {
				var pseudoArray = [];
				if (state) {
					pseudoArray.push(state);
					$scope.tp.details.state = statesService.getPrefixesString(pseudoArray)
				} else {
					delete $scope.tp.details.state
				}
			};

			$scope.setCarrier = () => {
				if ($scope.selectedCarrier != undefined) {
					$scope.tp.details.delivery_for_company = $scope.selectedCarrier.ld_carrier_id;
					getClosing($scope.selectedCarrier.ld_carrier_id)
				}
			};

			function getClosing(id) {
				apiService.postData(moveBoardApi.trip_carrier.ratePerCf, {carrier_id: id}).then(data => {
					if (data.data) {
						$scope.tp.closing.rate_per_cf = data.data[0];
						$scope.setSellaryData()
					} else {
						$scope.tp.closing.rate_per_cf = 'Carrier hav\'t rate per cf'
					}
				}).catch(error => {
				});
			}
			function checkState(string) {
				for (var i = 0; i < $scope.allStates.length; i++) {
					if ($scope.allStates[i].substring(0, 2) == string) {
						$scope.state = $scope.allStates[i];
						break
					}
				}
			}
			$scope.getTpDelivery = () => {
				if ($scope.tpId != 'new') {
					apiService.getData(moveBoardApi.tpDelivery.get, {id: $scope.tpId}).then(data => {
						$scope.tp = data.data;
						$scope.date_from = new Date($scope.tp.details.date_from * 1000);
						$scope.date_to = new Date($scope.tp.details.date_to * 1000);
						$scope.weight = ($scope.tp.closing.volume_cf * 7).toFixed(2);
						getCarriers();
						$scope.getReceipts();
						checkState($scope.tp.details.state);
						if ($scope.tp.details.phones.length < 2) {
							$scope.tp.details.phones.push('')
						}
					}).catch(error => {
					});
				} else {
					getCarriers();
					$scope.date_from = new Date();
					$scope.date_to = new Date();
					$scope.tp = {
						details: {
							phones: ['', '']
						},
						closing: {}
					};
					$scope.receipts = []
				}
			};

			function getCarriers() {
				apiService.getData(moveBoardApi.trip_carrier.list, {shortlist: 1, pageSize: 999}).then(data => {
					if (data.data.items) {
						$scope.carrierList = getArrayFromObject(data.data.items)
					} else {
						$scope.carrierList = []
					}
					if ($scope.tpId != 'new') {
						$scope.carrierList.forEach(item => {
							if (item.ld_carrier_id == $scope.tp.details.delivery_for_company) {
								$scope.selectedCarrier = item
							}
						})
					}
				});
			}
			function getArrayFromObject(obj) {
				var array = [];
				for (var key in obj) {
					array.push(obj[key])
				}
				return array
			}
			$scope.createTpDelivery = (ev) => {
				if ($scope.tpModel.$valid) {
					$scope.tp.closing = addZeroValues($scope.tp.closing);
					$scope.tp.details.date_from = moment($scope.date_from).unix();
					$scope.tp.details.date_to = moment($scope.date_to).unix();
					$scope.tp.payments = $scope.receipts;
					if ($scope.tpId == 'new') {
						apiService.postData(moveBoardApi.tpDelivery.create, {
							trip_id: $scope.tripId,
							data: $scope.tp
						}).then(res => {
							if ($scope.createTpDeliveryWithoutRoute) {
								$scope.tpId = res.data.id;
								$scope.tp.id = $scope.tpId;
								$state.go('lddispatch.tpDelivery', {
									id: $scope.tripId,
									tpId: $scope.tpId
								}, {notify: false});
								$scope.openNewPayment(ev);
								$scope.createTpDeliveryWithoutRoute = false
							} else {
								$state.go('lddispatch.tripDetails', {id: $scope.tripId, tab: 0, paymentExist: $scope.paymentExist})
							}

						}).catch(error => {
						});
					} else {
						apiService.putData(moveBoardApi.tpDelivery.update, {
							tpId: $scope.tpId,
							ld_trip_id: $scope.tripId,
							data: $scope.tp
						}).then(data => {
							$scope.getBack()
						}).catch(error => {
						});
					}
				} else {
					$scope.tpModel.$setSubmitted();
					if ($scope.createTpDeliveryWithoutRoute) {
						showAlert('Some field are required!', 'Enter value for all required fields before creating payment!');
						$scope.createTpDeliveryWithoutRoute = false;
					} else {
						showAlert('Some field are required!', 'You must enter value for all required fields!');
					}
				}
			};
			function showAlert(label, text) {
				var alert = $mdDialog.alert()
					.parent(angular.element(document.body))
					.clickOutsideToClose(true)
					.title(label)
					.textContent(text)
					.ok('OK')
				$mdDialog.show(alert);
			}
			function parseZipToAddress (arrAddress) {
				var city, state, state_full, country, postal_code;

				$.each(arrAddress, function (i, address_component) {

					if (address_component.types[0] == "administrative_area_level_1") {// State
						if (address_component.short_name.length < 3) {
							state = address_component.short_name;
							state_full = address_component.short_name + ' - ' + address_component.long_name
						}
					}

					if (address_component.types.indexOf("locality") > -1 || address_component.types.indexOf("neighborhood") > -1 || address_component.types.indexOf("sublocality") > -1) {// locality type
						city = address_component.long_name;
					}

					if (address_component.types[0] == "country") {// locality type
						country = address_component.short_name;
					}

					if (address_component.types[0] == "postal_code") {// "postal_code"
						postal_code = address_component.short_name;
					}
				});
				var address = {
					'city': city,
					'state': state,
					'state_full': state_full,
					'postal_code': postal_code,
					'country': country,
					'full_adr': city + ', ' + state + ' ' + postal_code
				};

				return address;
			}
			function geoCode(zip_code) {
				var geocoder = new google.maps.Geocoder();
				var deferred = $q.defer();
				var address = {
					address: zip_code,
					componentRestrictions: {
						country: 'US',
						postalCode: zip_code
					}
				};
				geocoder.geocode(address, function (results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						var arrAddress = results[0]['address_components'];
						var address = parseZipToAddress(arrAddress);

						deferred.resolve(address);
					} else {

						deferred.reject();
					}

				});

				return deferred.promise;
			}
			$scope.changePostalCode = function () {
				var postal_code = $scope.tp.details.zip;

				if (!_.isUndefined(postal_code)
					&& postal_code.length == 5) {

					geoCode(postal_code).then(function (result) {
						$scope.tp.details.city = result.city;
						$scope.tp.details.state = result.state;
						$scope.state = result.state_full
					}, function () {
						$scope.tp.details.city = '';
						$scope.tp.details.state = '';
					});
				}
			};
			function getCollectedTotal() {
				let collectedTotal = 0;
				if ($scope.receipts.length) {
					$scope.receipts.forEach(item => {
						if (!item.value.pending) {
							collectedTotal = collectedTotal + parseFloat(item.amount);
						}
					});
					$scope.tp.closing.received = collectedTotal;
					$scope.setSellaryData();
				} else {
					delete $scope.tp.closing.received;
					$scope.setSellaryData();
				}
			}
			$scope.setPending = (receipt) => {
				$scope.disabledSwitch = true;
				$scope.receiptsBusy = true;
				var id = receipt.receipt_id || receipt.id;
				var pending = receipt.value.pending ? 1 : 0;
				apiService.postData(moveBoardApi.carrier_payments.setReceiptPending, {
					id,
					pending
				}).then(data => {
					responseDellay = $timeout(function () {
						$scope.disabledSwitch = false;
						$scope.receiptsBusy = false;
					}, 2000);
					receipt.amount = pending ? 0 : receipt.value.amount;
					getCollectedTotal()
				})
			};
			$scope.$on("$destroy", () => {
					$timeout.cancel(responseDellay);
				}
			);
			$scope.openNewPayment = ($event) => {
				if ($scope.tpId === 'new') {
					$scope.createTpDeliveryWithoutRoute = true;
					$scope.createTpDelivery($event);
				} else {
					$scope.openCustomPayment($event, ZERO_AMOUNT, undefined, [$scope.tpId], ENTITY_TYPE)
				}
			};

			$scope.removeReceipt = (item, index) => {
				apiService.postData(moveBoardApi.carrier_payments.removeRequestReceipt, {id: item.receipt_id}, $scope).then(data => {
					$scope.receipts.splice(index, 1);
					getCollectedTotal()
				});
			};
			$scope.getReceipts = () => {
				$scope.receiptsBusy = true;
				apiService.postData(moveBoardApi.carrier_payments.getRequestReceipts, {
					id: $scope.tpId,
					entity_type: 11,
					types: [1, 4],
					hide_tpd_agent_folio: 1
				}).then(data => {
					$scope.receiptsBusy = false;
					if (data.data) {
						$scope.receipts = data.data;
						if (!$scope.paymentExist) {
							getCollectedTotal()
						}
					} else {
						$scope.receipts = []
					}

				});
			};
			$scope.showReceiptModal = (ev, item) => {
				$scope.item = item;
				$scope.item.receipt_id = item.receipt_id ? item.receipt_id : item.id;
				$mdDialog.show({
					controller: DialogController,
					template: require('../../agent-folio-payments/receiptModal/receipt-modal-template.pug'),
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose: true,
					locals: {
						item: $scope.item,
					}
				})
			};

			function DialogController($scope, $mdDialog, item) {
				$scope.item = item;
				if (item.value.ccardN) {
					item.value.ccardN = item.value.ccardN.slice(-4)
				}
				$scope.cancel = function () {
					$mdDialog.cancel();
				};
			}

			let windowElement = angular.element("#main-wrapper");

			windowElement.scrollTop(0);

			$scope.getTpDelivery()
		}
	};
}
