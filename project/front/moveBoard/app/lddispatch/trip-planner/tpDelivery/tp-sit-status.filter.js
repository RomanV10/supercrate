'use strict';

angular
	.module('app.lddispatch')
	.filter('tpSitStatus', tpSitStatus);

function tpSitStatus() {
	return function (status, types) {
		var result;
		if (types) {
			for (var key in types) {
				if (key == status) {
					result = types[key];
					return result
				}
			}
			return result
		} else {
			return 'No status'
		}
	}
}
