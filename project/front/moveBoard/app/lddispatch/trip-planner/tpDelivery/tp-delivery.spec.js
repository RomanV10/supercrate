describe('Unit: tp-delivery.directive,', () => {
	let $scope, $localScope, element;
	let $state, apiService, $q;
	let long_distance_tp_delivery, long_distance_carrier, get_job_receipt, calc_closing;
	
	beforeEach(inject(function (_$state_, _apiService_, _$q_) {
		$state = _$state_;
		apiService = _apiService_;
		$q = _$q_;
		$state.params = {
			tpId: '2',
			id: '26'
		};
		
		$scope = $rootScope.$new();
		
		long_distance_tp_delivery = testHelper.loadJsonFile('long_distance_tp_delivery.mock');
		long_distance_carrier = testHelper.loadJsonFile('long_distance_carrier.mock');
		get_job_receipt = testHelper.loadJsonFile('get_job_receipt.mock').data;
		calc_closing = testHelper.loadJsonFile('calc_closing.mock');
		
		$httpBackend.expectGET(testHelper.makeRequest(moveBoardApi.tpDelivery.get, {id: '2'}))
			.respond(200, long_distance_tp_delivery);
		element = $compile('<div tp-delivery></div>')($scope);
		
		$httpBackend.expectGET(testHelper.makeRequest(moveBoardApi.trip_carrier.list + '?pageSize=999&shortlist=1'))
			.respond(200, long_distance_carrier);
		$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.carrier_payments.getRequestReceipts, {id: '2'}))
			.respond(200, get_job_receipt);
		$httpBackend.flush();
		
		$httpBackend.expectPOST(testHelper.makeRequest(moveBoardApi.tpDelivery.calcClosing))
			.respond(200, calc_closing);
		$httpBackend.flush();
		$httpBackend.flush();
		
		$localScope = element.scope();
	}));
	
	describe('After Initializing,', () => {
		it('Should set tripId to value from $state.params', () => {
			expect($localScope.tripId).toEqual('26');
		});
		it('Should calculate "received from 3rt party" properly', () => {
			expect($localScope.tp.closing.received).toEqual(200);
		});
	});
	
	describe('function: removeReceipt,', () => {
		let firstReceipt, secondReceipt;
		beforeEach(() => {
			firstReceipt = $localScope.receipts[0];
			secondReceipt = $localScope.receipts[1];
			let whatShouldBeSended = {
				job_cost: 0,
				job_total: 0,
				order_balance: "0",
				paid: 0,
				rate_per_cf: "2.00",
				received: 200,
				services_total: "0.00",
				to_pay: 200,
				to_receive: 0,
				tp_collected: "0",
				volume_cf: "0.00",
				weight: 0,
			};
			
			$httpBackend.expectPOST(
				testHelper.makeRequest(moveBoardApi.carrier_payments.removeRequestReceipt, {id: '186'}))
				.respond(200, [true]);
			$localScope.removeReceipt(firstReceipt, 0);
			$httpBackend.expectPOST(
				testHelper.makeRequest(moveBoardApi.tpDelivery.calcClosing), {type: 1, data: whatShouldBeSended})
				.respond(200, calc_closing);
			$httpBackend.flush();
			$httpBackend.flush();
		});
		
		it('Should remove receipt from receipts', () => {
			expect($localScope.receipts.length).toEqual(1);
			expect($localScope.receipts[0]).toBe(secondReceipt);
		});
	});
	
	describe('function: setPending to true,', () => {
		let receipt;
		beforeEach(() => {
			receipt = $localScope.receipts[1];
			receipt.value.pending = true;
			$httpBackend.expectPOST(
				testHelper.makeRequest(moveBoardApi.carrier_payments.setReceiptPending,
					{id: receipt.receipt_id || receipt.id}),
				{
					pending: 1
				})
				.respond(200, {});
			
			$localScope.setPending(receipt);
			
			let whatShouldBeSended = {
				job_cost: 0,
				job_total: 0,
				order_balance: "0",
				paid: 0,
				rate_per_cf: "2.00",
				received: 0,
				services_total: "0.00",
				to_pay: 200,
				to_receive: 0,
				tp_collected: "0",
				volume_cf: "0.00",
				weight: 0,
			};
			$httpBackend.expectPOST(
				testHelper.makeRequest(moveBoardApi.tpDelivery.calcClosing), {type: 1, data: whatShouldBeSended})
				.respond(200, calc_closing);
			$httpBackend.flush();
			
		});
		it('Should set receipt\'s amount to 0', () => {
			expect(receipt.amount).toEqual(0);
		});
	});
	
	describe('function: setPending to false,', () => {
		let receipt;
		beforeEach(() => {
			receipt = $localScope.receipts[0];
			receipt.value.pending = false;
			$httpBackend.expectPOST(
				testHelper.makeRequest(moveBoardApi.carrier_payments.setReceiptPending,
					{id: receipt.receipt_id || receipt.id}),
				{
					pending: 0
				})
				.respond(200, {});
			
			$localScope.setPending(receipt);
			
			let whatShouldBeSended = {
				job_cost: 0,
				job_total: 0,
				order_balance: "0",
				paid: 0,
				rate_per_cf: "2.00",
				received: 600,
				services_total: "0.00",
				to_pay: 200,
				to_receive: 0,
				tp_collected: "0",
				volume_cf: "0.00",
				weight: 0,
			};
			$httpBackend.expectPOST(
				testHelper.makeRequest(moveBoardApi.tpDelivery.calcClosing), {type: 1, data: whatShouldBeSended})
				.respond(200, calc_closing);
			$httpBackend.flush();
			
		});
		it('Should set receipt\'s amount to 400', () => {
			expect(receipt.amount).toEqual('400');
		});
	});
	
	describe('function: createTpDelivery,', () => {
		let newReceipt;
		beforeEach(() => {
			newReceipt = angular.copy($localScope.receipts[0]);
			newReceipt.name = 'testingNewReceipt';
			$localScope.receipts.push(newReceipt);
			function testForSended(url, whatWasSended) {
				let defer = $q.defer();
				let urlMatch = url == moveBoardApi.tpDelivery.update;
				let bodyMatch = whatWasSended.tpId == '2'
					&& whatWasSended.ld_trip_id == '26'
					&& whatWasSended.data.payments == $localScope.receipts
					&& whatWasSended.data.payments[2].name == 'testingNewReceipt';
				if (urlMatch && bodyMatch) defer.resolve();
				else defer.reject();
				return defer.promise;
			}
			
			spyOn(apiService, 'putData').and.callFake(testForSended);
			spyOn($state, 'go');
			
			$localScope.createTpDelivery();
			$timeout.flush();
		});
		
		it('Just testing requests to api', () => {
			expect(apiService.putData).toHaveBeenCalled();
		});
		it('Should return to tripDetails if no history', () => {
			expect($state.go).toHaveBeenCalled();
		});
	});
});