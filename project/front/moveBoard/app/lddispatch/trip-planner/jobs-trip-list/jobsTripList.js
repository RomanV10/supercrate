'use strict';
import './jobsTripList.styl';
import { checkPaymentsExists } from '../../agent-folio-payments/export-functions/payment.export.js';

angular
	.module('app.lddispatch')
	.directive('jobsTripList', jobsTripList);

/*@ngInject*/
function jobsTripList($state, apiService, moveBoardApi, PaymentServices, $mdDialog, additionalServicesFactory) {
	return {
		template: require('./jobsTripList.pug'),
		restrict: 'A',
		scope: {
			id: '=',
			getJobs: '=',
			jobs: '=',
			jobsTotal: '=',
			params: '=',
			trip: '='
		},
		link: function ($scope) {
			$scope.modalOpened = false;

			$scope.openRateModal = (item) => {
				if (!$scope.modalOpened && !item.confirmedVolumeToOpen) {
					item.confirmedRateToOpen = true;
					$scope.modalOpened = true
				}
			};
			$scope.openVolumeModal = (item) => {

				if (!$scope.modalOpened && !item.confirmedRateToOpen) {
					item.confirmedVolumeToOpen = true;
					$scope.modalOpened = true
				}

			};

			$scope.closeModal = () => {
				$scope.modalOpened = false;
			};


			$scope.showTpCollected = (item, balance) => {
				let userInfo = {
					name: item.owner,
					phones: [item.phone],
					email: item.email
				};
				$state.go('lddispatch.sitTpCollected', {trip_id: $scope.id, job_id: item.job_id, balance: balance, paymentExist: checkPaymentsExists(item), userInfo})
			};

			$scope.addServices = (job) => {
				apiService.postData(moveBoardApi.trip.getServices, {job_id: job.job_id}).then(data => {
					job.services = data.data;
					job.reservation_rate = {};
					job.reservation_rate.value = "0";
					job.field_company_flags = {};
					job.field_company_flags.value = "0";
					job.nid = job.job_id;
					additionalServicesFactory.openAddServicesModal(job, job, 'services', job);
					let servicesWatcher = $scope.$watch(() => job.services, (newV, oldV) => {
						if (!$scope.compareArrays(oldV, newV)) {
							servicesWatcher();
							apiService.postData(moveBoardApi.trip.createServices, {
								data: {
									services: job.services,
									job_id: job.job_id,
									trip_id: job.ld_trip_id
								}
							}).then(res => {
								updateCurrenJob(job, res.data);
							}).catch(error => {
								var alert = $mdDialog.alert()
									.parent(angular.element(document.body))
									.clickOutsideToClose(true)
									.title("You can't change job values")
									.textContent("Carrier Payments for this job already exists")
									.ok('OK')

								$mdDialog.show(alert);
							});
						}
					});
				});
			};

			$scope.compareArrays = (array1, array2) => {
				Array.prototype.equals = function (array) {
					if (!array)
						return false;

					if (this.length != array.length)
						return false;

					for (var i = 0, l = this.length; i < l; i++) {
						if (this[i] instanceof Array && array[i] instanceof Array) {
							if (!this[i].equals(array[i]))
								return false;
						}
						else if (this[i] != array[i]) {
							return false;
						}
					}
					return true;
				};
				return array1.equals(array2)
			};

			function updateCurrenJob(item, data) {
				item.to_receive = data.fields.to_receive;
				item.to_pay = data.fields.to_pay;
				item.rate_per_cf = data.fields.rate_per_cf;
				item.volume_cf = data.fields.volume_cf;
				item.balance = data.fields.balance;
				item.job_total = data.fields.job_total;
				item.job_cost = data.fields.job_cost;
				item.services_total = data.fields.services_total;

				if (data.total) {
					$scope.jobsTotal.volume_cf = data.total.volume_cf;
					$scope.jobsTotal.to_receive = data.total.to_receive;
					$scope.jobsTotal.to_pay = data.total.to_pay;
					$scope.jobsTotal.services_total = data.total.services_total;
					$scope.jobsTotal.balance = data.total.balance;
					$scope.jobsTotal.job_total = data.total.job_total;
					$scope.jobsTotal.job_cost = data.total.job_cost;
				}
			}

			$scope.openTpDelivery = (item) => {
				$state.go('lddispatch.tpDelivery', {id: $state.params.id, tpId: item.job_id, paymentExist: checkPaymentsExists(item)})
			};

		}
	};
}
