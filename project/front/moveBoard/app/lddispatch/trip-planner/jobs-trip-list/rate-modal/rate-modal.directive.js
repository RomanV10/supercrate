'use strict';
import './rateModal.styl';

angular
	.module('app.lddispatch')
	.directive('rateModal', rateModal);
rateModal.$inject = ['$state', 'apiService', 'moveBoardApi', '$mdDialog'];

function rateModal($state, apiService, moveBoardApi, $mdDialog) {

	return {
		template: require('./rateModal.pug'),
		restrict: 'EA',
		scope: {
			item: '=',
			modalOpened: '=',
			closeModal: '&',
			sumJobsTripList: '=',
			getJobs: '=',
			tripType: '=?'
		},
		link: function ($scope) {
			$scope.error = false;
			$scope.rate_per_cf = angular.copy($scope.item.rate_per_cf);
			$scope.volume_cf = angular.copy($scope.item.volume_cf);

			$scope.close = () => {
				delete $scope.item.confirmedRateToOpen;
				delete $scope.item.confirmedVolumeToOpen;
				$scope.closeModal();

			};

			$scope.update = () => {
				$scope.busy = true;
				$scope.error = false;
				let fields_to_update = {};
				fields_to_update.rate_per_cf = $scope.rate_per_cf;
				fields_to_update.volume_cf = $scope.volume_cf;
				if (_.isFinite(Number(fields_to_update.rate_per_cf)) && _.isFinite(Number(fields_to_update.volume_cf))) {
					apiService.postData(moveBoardApi.trip.update_closing_values, {
						trip_id: $scope.item.ld_trip_id,
						id: $scope.item.id,
						fields_to_update
					}).then(res => {
						if (res.data && res.data.status_code && res.data.status_code == 406) {
							$mdDialog.show(
								$mdDialog.alert()
									.parent(angular.element(document.body))
									.clickOutsideToClose(true)
									.title("You can't change job values")
									.textContent("Carrier Payments for this job already exists")
									.ok('OK')
							);
						} else {
							if ($scope.tripType == 2) {
								$scope.getJobs();
							} else {
								updateCurrenJob($scope.item, res.data);
							}
						}
						$scope.busy = false;
						$scope.close($scope.item)
					}).catch(() => {
						$scope.close($scope.item);
						$scope.busy = false;
						$mdDialog.show(
							$mdDialog.alert()
								.parent(angular.element(document.body))
								.clickOutsideToClose(true)
								.title("You can't change job values")
								.textContent("Carrier Payments for this job already exists")
								.ok('OK')
						);
					})
				} else {
					$scope.busy = false;
					$scope.error = true;
				}

			};

			function updateCurrenJob(item, data) {
				item.to_receive = data.fields.to_receive;
				item.to_pay = data.fields.to_pay;
				item.rate_per_cf = data.fields.rate_per_cf;
				item.volume_cf = data.fields.volume_cf;
				item.balance = data.fields.balance;
				item.job_total = data.fields.job_total;
				item.job_cost = data.fields.job_cost;
				item.services_total = data.fields.services_total;

				if (data.total) {
					$scope.sumJobsTripList.volume_cf = data.total.volume_cf;
					$scope.sumJobsTripList.to_receive = data.total.to_receive;
					$scope.sumJobsTripList.to_pay = data.total.to_pay;
					$scope.sumJobsTripList.services_total = data.total.services_total;
					$scope.sumJobsTripList.balance = data.total.balance;
					$scope.sumJobsTripList.job_total = data.total.job_total;
					$scope.sumJobsTripList.job_cost = data.total.job_cost;
				}
			}

		}
	}
}
