import './trip-tabs.styl';
import { checkPaymentsExists } from '../../agent-folio-payments/export-functions/payment.export.js';

'use strict';

angular
	.module('app.lddispatch')
	.directive('tripTabs', tripTabs);

tripTabs.$inject = ['$state', '$stateParams', 'moveBoardApi', 'apiService', '$mdToast', '$mdDialog', 'routerTracker', 'MomentWrapperService'];

function tripTabs($state, $stateParams, moveBoardApi, apiService, $mdToast, $mdDialog, routerTracker, MomentWrapperService) {
	return {
		template: require('./trip-tabs.pug'),
		restrict: 'A',

		link: function ($scope, element) {
			var timeWrapper = MomentWrapperService.getZeroMoment;
			$scope.foreman = {};
			$scope.carrier = {};
			$scope.showPayrollButton = false;
			$scope.loading = false;
			$scope.state_id = $stateParams.id;
			$scope.currentTab = $stateParams.tab;
			$scope.params = {
				pageSize: 20,
				total: 1,
				tripDetails: 1,
				sorting: {
					orderKey: 'job_id',
					orderValue: 'ASC'
				}
			};
			$scope.tripStatusStyleArr = ['',
				'trip-create-modal-form__toolbar__gradient-box__draft',
				'trip-create-modal-form__toolbar__gradient-box__pending',
				'trip-create-modal-form__toolbar__gradient-box__active',
				'trip-create-modal-form__toolbar__gradient-box__delivered'];

			$scope.createTrip = function (trip) {
				var date_start = angular.copy(trip.data.details.start);
				var date_end = angular.copy(trip.data.details.end);
				trip.data.details.date_start = moment(date_start).unix();
				trip.data.details.date_end = !date_end ? 0 : moment(date_end).unix();
				trip.trip_id = trip.data.trip_id;
				if (trip.data.details.type == 1) {
					$scope.trip.data.foreman = {};
					$scope.trip.data.helper = [];
				}
				if (trip.data.details.type == 2) {
					trip.data.carrier = {
						carrier_id: '0',
						driver_name: '',
						driver_phone: ''
					};
				}
				apiService.putData(moveBoardApi.trip.update, trip).then(data => {
					$scope.showToastTripUpd();
					trip.data.details.date_start = moment(date_start).unix();
					trip.data.details.date_end = moment(date_end).unix();

					$scope.getJobs();
					$scope.tripOriginal = _.cloneDeep($scope.trip);
				});
			};

			$scope.showToastTripUpd = () => {
				$mdToast.show(
					$mdToast.simple()
						.textContent('Trip updated!')
						.position('top right')
						.parent(element)
						.action('x')
						.hideDelay(3000)
				);
			};

			$scope.closeTrip = function () {
				if (_.isUndefined($scope.trip.data.payroll_status)) {
					$scope.createPayroll();
				}
				
				var routeHistory = routerTracker.getRouteHistory();
				if ($scope.currentTab === 0) {
					if (!_.isEmpty(routeHistory)) {
						_.each(routeHistory, function (history, i) {
							let name = history.route.name;
							let isAllowedRouting = name !== 'lddispatch.tpDelivery'
								&& name !== 'lddispatch.tripDetails'
								&& name !== 'lddispatch.sitTpCollected';

							if (isAllowedRouting) {
								var routeParams = history.routeParams;
								$state.go(name, routeParams);

								return false;
							}

							if (i === routeHistory.length - 1 && !isAllowedRouting) {
								$state.go('lddispatch.trip');
							}
						});
					} else {
						$state.go('lddispatch.trip');
					}
				} else {
					$scope.currentTab = 0;
					$state.go('lddispatch.tripDetails', {'id': $scope.trip_id, 'tab': 0}, {notify: false});
				}

			};

			var alertTripHaveJobs = (ev) => {
				var alert = $mdDialog.alert()
					.clickOutsideToClose(true)
					.title('You can not remove the trip with jobs')
					.ok('OK')
					.targetEvent(ev);
				$mdDialog.show(alert);
			};

			var removeTripWithOutJobs = (ev, id) => {
				var confirm = $mdDialog.confirm()
					.title('Are you sure you want to remove the trip #' + id)
					.targetEvent(ev)
					.ok('Yes')
					.cancel('No');
				$mdDialog.show(confirm).then(function () {
					removeTripRequest(id);
				});
			};

			var removeTripRequest = (id) => {
				apiService.delData(moveBoardApi.trip.removeTrip, {tripId: id}).then(data => {
					$state.go('lddispatch.trip');
				});
			};

			$scope.removeTrip = function (ev, id) {
				if (!_.isEmpty($scope.jobs)) {
					alertTripHaveJobs(ev);
				} else {
					removeTripWithOutJobs(ev, id);
				}
			};

			$scope.changeTab = state => {
				$scope.currentTab = state;
				$state.go('lddispatch.tripDetails', {'id': $scope.trip_id, 'tab': state}, {notify: false});
			};

			$scope.changeState = function (state, vm) {
				if ($scope.trip.data.details.type == 1) {
					if (!$scope.carrier.form.$valid) {
						$scope.carrier.form.$setSubmitted();
						state = 0;
						if (vm) {
							vm.tab = 0;
						}
						$scope.changeTab(0);
						showAlert('Some field are required!', 'You must enter a value for all required fields!');
					} else {
						$scope.foreman.form.$setSubmitted();
						$scope.foreman.form.$setValidity('required', true);
						if (!_.isEqual($scope.trip.data, $scope.tripOriginal.data)) {
							$scope.createTrip($scope.trip);
						}
						$scope.changeTab(state);
					}
				}
				if ($scope.trip.data.details.type == 2) {
					if (!$scope.foreman.form.$valid) {
						$scope.foreman.form.$setSubmitted();
						state = 0;
						if (vm) {
							vm.tab = 0;
						}
						$scope.changeTab(0);
						showAlert('Add a Foreman!', 'The trip with Foreman/Helper type must have a foreman!');
					} else {
						if (!$scope.trip.data.helper) {
							$scope.trip.data.helper = [];
						}
						if ( (_.isUndefined($scope.trip.data.payroll_status) || $scope.trip.data.payroll_status === false)
							&& state === 2) {
							$scope.createPayroll(state);
						} else {
							if (!_.isEqual($scope.trip.data, $scope.tripOriginal.data)) {
								$scope.createTrip($scope.trip);
							}
							$scope.changeTab(state);
						}
					}
				}

				if (state == 0) {
					$scope.getJobs();
				}
			};

			function showAlert(title, text) {
				$mdDialog.show(
					$mdDialog.alert()
						.parent(angular.element(document.body))
						.clickOutsideToClose(true)
						.title(title)
						.textContent(text)
						.ok('OK')
				);
			}

			apiService.getData(moveBoardApi.trip_carrier.list, {shortlist: 1, pageSize: 999}).then(data => {
				$scope.carrierList = data.data.items;
			});

			$scope.getJobs = () => {
				$scope.jobsBusy = true;
				$scope.params.id = $scope.trip_id;
				if ($scope.trip.data.details.type == 1) {
					delete $scope.params.tripDetails;
				}
				apiService.postData(moveBoardApi.trip.getTripClosing, $scope.params).then(data => {
					$scope.jobsBusy = false;
					$scope.paymentsExist = false;
					if (data.data.items) {
						$scope.jobs = $scope.addSelectTag(data.data.items);
						for (var i = 0; i < $scope.jobs.length; i++) {
							if ($scope.jobs[i].is_total == 1) {
								$scope.jobs_total = $scope.jobs[i];
								$scope.jobs.splice(i, 1);
								break;
							}
						}
						$scope.jobs.forEach(item => {
							if (checkPaymentsExists(item)) {
								$scope.paymentsExist = true;
							}
						});
					} else {
						$scope.jobs = [];
					}
					if($scope.jobs_total){
						$scope.jobs_total.totalBlankets = getTotalBlankets();
					}
				});
				$scope.addSelectTag = (jobs) => {
					angular.forEach(jobs => {
						jobs.selected = false;
					});
					$scope.busy = false;
					return jobs;
				};
			};

			$scope.createPayroll = (state = false) => {
				var date_start = angular.copy(moment($scope.trip.data.details.start));
				var date_end = angular.copy(moment($scope.trip.data.details.end));
				var dateStartUnix = timeWrapper(moment(date_start).set({hour:15,minute:10,second:0,millisecond:0})).unix();
				var dateEndUnix = timeWrapper(moment(date_end).set({hour:15,minute:10,second:0,millisecond:0})).unix();
				var totalDays = moment(date_end).diff(moment(date_start), 'days') + 1;
				var totalCollected = (() => {
					if ($scope.trip.data.jobs && $scope.trip.data.jobs.length) {
						var jobsTotal = $scope.trip.data.jobs.items.filter(item => {
							return item.is_total;
						});
						if (jobsTotal.length) {
							return jobsTotal[0].tp_collected;
						} else {
							return 0;
						}
					} else {
						return 0;
					}
				})();

				var payroll = {
					'ld_trip_id': $scope.trip_id,
					'total_payroll': 0,
					'date_start': dateStartUnix,
					'date_end': dateEndUnix,
					'received': [],
					'expenses': [],
					'foreman': {
						uid: _.isObject($scope.trip.data.foreman) ? $scope.trip.data.foreman.foreman_id : 0,
						total_received: 0,
						total_expenses: 0,
						daily_amount: 0,
						total_days: totalDays,
						total_daily: 0,
						hourly_rate: 0,
						total_hours: 0,
						total_hourly: 0,
						loading_rate: 0,
						loading_volume: $scope.jobs_total.volume_cf,
						total_loading: 0,
						unloading_rate: 0,
						unloading_volume: $scope.jobs_total.volume_cf,
						total_unloading: 0,
						mileage_start: 0,
						mileage_end: 0,
						mileage_rate: 0,
						mileage: 0,
						total_mileage: 0,
						total_payroll: 0,
						total_collected: totalCollected
					},
					'helper': []
				};
				if ($scope.trip.data.helper) {
					$scope.trip.data.helper.forEach((item) => {
						payroll.helper.push({
							uid: item.helper_id,
							daily_amount: 0,
							total_days: totalDays,
							total_daily: 0,
							other: 0,
							total_payroll: 0
						});
					});
				}
				apiService.postData(moveBoardApi.ldPayroll.update, {
					trip_id: $scope.trip_id,
					data: payroll
				}).then(() => {
					if (!_.isEqual($scope.trip.data, $scope.tripOriginal.data)) {
						$scope.createTrip($scope.trip);
					}
					$scope.trip.data.payroll_status = 0; // payroll exist
					
					if (!state) return;
					$scope.currentTab = state;
					$state.go('lddispatch.tripDetails', {'id': $scope.trip_id, 'tab': state}, {notify: false});
				});
			};
			
			$scope.$on('createTripPayroll', $scope.createPayroll);

			$scope.submitPayroll = () => {
				$scope.loading = true;
				apiService.postData(moveBoardApi.ldPayroll.submit, {trip_id: $scope.trip_id}).then(data => {
					$scope.loading = false;
					$scope.trip.data.payroll_status = 1;
					$mdDialog.show(
						$mdDialog.alert()
							.parent(angular.element(document.body))
							.clickOutsideToClose(true)
							.textContent('Payroll was submitted!')
							.ok('OK')
					);
				});
			};

			$scope.revokePayroll = () => {
				$scope.loading = true;
				apiService.postData(moveBoardApi.ldPayroll.revoke, {trip_id: $scope.trip_id}).then(data => {
					$scope.loading = false;
					$scope.trip.data.payroll_status = 0;
					$mdDialog.show(
						$mdDialog.alert()
							.parent(angular.element(document.body))
							.clickOutsideToClose(true)
							.textContent('Payroll was revoked!')
							.ok('OK')
					);
				});
			};

			function getTotalBlankets() {
				var total = 0;
				$scope.jobs.forEach((item) => {
					if (item.sit) {
						total += +item.sit.blankets;
					}
				});
				if ($scope.trip.data.jobs.items[0]) {
					$scope.trip.data.jobs.items[0].totalBlankets = total;
				} else {
					var tempBlankets = {totalBlankets: total};
					$scope.trip.data.jobs.items.push(tempBlankets);
				}
				return total;
			}

			function loadTrip(id) {
				if (id === 'new') {
					apiService.postData(moveBoardApi.trip.create, {data: {}}).then(data => {
						$scope.trip = data;
						if ($scope.trip.data.details.date_end == 0) {
							$scope.trip.data.details.date_end = undefined;
						}
						if (!$scope.trip.data.jobs) {
							$scope.trip.data.jobs = {
								items: []
							};
						}
						$scope.tripOriginal = _.cloneDeep($scope.trip);
						$scope.trip_id = $scope.trip.data.trip_id;
						$scope.currentTab = 0;
						$state.go('lddispatch.tripDetails', {'id': $scope.trip_id, 'tab': 0}, {notify: false});
					});
				} else {
					apiService.getData(moveBoardApi.trip.getTrip, {id: id}).then(data => {
						$scope.trip = data;
						if ($scope.trip.data.details.date_end == 0) {
							$scope.trip.data.details.date_end = undefined;
						}
						if (!$scope.trip.data.jobs) {
							$scope.trip.data.jobs = {
								items: []
							};
						}
						$scope.tripOriginal = _.cloneDeep($scope.trip);
						$scope.trip_id = $scope.trip.data.trip_id;
						$scope.getJobs();
					});

				}

			}

			loadTrip($scope.state_id);
		}
	};
}
