'use strict';
import './requestList.styl';

angular
	.module('app.lddispatch')
	.directive('requestList', requestList);

requestList.$inject = ['$state', 'apiService', 'moveBoardApi', 'datacontext', 'DTOptionsBuilder', '$interval', '$timeout', 'statesService'];

function requestList($state, apiService, moveBoardApi, datacontext, DTOptionsBuilder, $interval, $timeout, statesService) {
	return {
		scope: {
			trip: '=',
			tripId: '=',
			getJobs: '&',
			updateTrip: '=',
			changeState: '='
		},
		template: require("./requestList.pug"),
		restrict: 'A',
		controller: ($scope) => {
			var searchDelay

			$scope.searchByRequestId = (searchTerm) => {
				$timeout.cancel(searchDelay);
				searchDelay = $timeout(function () {
					$scope.params.condition.filters.job_id = searchTerm
					$scope.getrequestsData();
				}, 1000)
			}

			$scope.$on("$destroy", () => {
				$timeout.cancel(searchDelay);
			})

			$scope.openModalFrom = () => {
				$scope.modalFromOpened = true
			}

			$scope.openModalTo = () => {
				$scope.modalToOpened = true
			}

			$scope.$watch(() => $scope.chosenStatesFrom.length, () => {
				$scope.selectStringFrom = statesService.getPrefixesString($scope.chosenStatesFrom)
			})
			$scope.$watch(() => $scope.chosenStatesTo.length, () => {
				$scope.selectStringTo = statesService.getPrefixesString($scope.chosenStatesTo)
			})
			$scope.changePickupDateFrom = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.pickup_date.from = moment(timestamp).startOf('day').unix() + $scope.offset
				} else {
					delete $scope.params.condition.filters.pickup_date.from
				}
				$scope.getrequestsData()
			}
			$scope.changePickupDateTo = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.pickup_date.to = moment(timestamp).endOf('day').unix() + $scope.offset
				} else {
					delete $scope.params.condition.filters.pickup_date.to
				}
				$scope.getrequestsData()
			}

			$scope.changeDeliveryDateFrom = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.delivery_date.from = moment(timestamp).startOf('day').unix() + $scope.offset
				} else {
					delete $scope.params.condition.filters.delivery_date.from
				}
				$scope.getrequestsData()
			}
			$scope.changeDeliveryDateTo = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.delivery_date.to = moment(timestamp).endOf('day').unix() + $scope.offset
				} else {
					delete $scope.params.condition.filters.delivery_date.to
				}
				$scope.getrequestsData()

			}

			$scope.setStorage = () => {
				if ($scope.selectedStorage.storage_id) {
					$scope.params.condition.filters.storage_id = $scope.selectedStorage.storage_id
				} else {
					delete $scope.params.condition.filters.storage_id
				}
				$scope.getrequestsData()
			}
			$scope.setStatus = () => {
				if ($scope.selectedStatus.id) {
					$scope.params.condition.filters.ld_status = $scope.selectedStatus.id
				} else {
					delete $scope.params.condition.filters.ld_status
				}
				$scope.getrequestsData()
			}
			$scope.selectSitOnly = (bool) => {
				if (bool) {
					$scope.params.condition.filters.sit_status = 1
				} else {
					delete $scope.params.condition.filters.sit_status
				}
				$scope.getrequestsData()
			}

			$scope.selectDeliveryOnly = (bool) => {
				if (bool) {
					$scope.params.condition.filters.ready_for_delivery = 1
				} else {
					delete $scope.params.condition.filters.ready_for_delivery
				}
				$scope.getrequestsData()
			}

			$scope.selectAll = function (key) {
				_.each($scope.requests, function (request, index) {
					request.a_a_selected = key;
				});
				$scope.selectedAll = key
				if (key) {
					$scope.selectedItems = angular.copy($scope.requests)
				} else {
					$scope.selectedItems = []
				}
			};

			$scope.isAllSelected = () => {
				if ($scope.selectedItems.length == $scope.requests.length) {
					$scope.selectedAll = true
				} else {
					$scope.selectedAll = false
				}
			}

			$scope.toggleAnimation = () => {
				if ($scope.selectedItems.length > 0) {
					return true
				} else {
					return false
				}
			}


			$scope.addRequestsToTrip = () => {
				var jobs = [];
				$scope.busy = true;
				_.each($scope.selectedItems, function (request, index) {
					jobs.push(request.ld_nid);
				});

				apiService.postData(moveBoardApi.trip.addRequest, {
					"trip_id": Number($scope.tripId),
					"jobs_id": jobs
				}).then(data => {

					$scope.busy = false;
					if (jobs.length) {
						$scope.trip.data.details.flag = "2";
						$scope.updateTrip($scope.trip);
					}
					$scope.changeState(0)
				}, (error, status, data) => {

					$scope.busy = false;
				});
			};


			$scope.getrequestsData = () => {
				$scope.makeStatesArray()
				$scope.busy = true;
				apiService.postData(moveBoardApi.request.pickup, $scope.params
				).then(data => {
					if (data.data.items) {
						$scope.requests = $scope.getLdStatusName(data.data.items)
					} else {
						$scope.requests = []
					}
					$scope.filterBusy = false;
					$scope.busy = false;
				})
			};
			$scope.getLdStatusName = (array) => {
				$scope.makeStatesArray()
				array.forEach(item => {
					if ($scope.ldStatuses) {
						for (var i = 0; i < $scope.ldStatuses.length; i++) {
							if ($scope.ldStatuses[i].id.toString() == item.ld_status && item.ld_status) {
								item.ld_status_name = $scope.ldStatuses[i].name
								item.ld_status_color = $scope.ldStatuses[i].color
								break
							} else {
								item.ld_status_name = 'No Status'
							}
						}
					} else {
						item.ld_status_name = 'No Status'
					}
				})
				return array
			}
			$scope.getLdStatuses = () => {
				$scope.fieldData = datacontext.getFieldData();
				if ($scope.fieldData.longdistance.ldStatus) {
					$scope.ldStatuses = angular.fromJson($scope.fieldData.longdistance.ldStatus);
				} else {
					$scope.ldStatuses = []
				}
			}
			$scope.makeStatesArray = () => {
				if ($scope.chosenStatesFrom.length > 0) {
					$scope.params.condition.filters.from_state = statesService.getOnlyPrefixes($scope.chosenStatesFrom)
				} else {
					delete $scope.params.condition.filters.from_state
				}
				if ($scope.chosenStatesTo.length > 0) {
					$scope.params.condition.filters.to_state = statesService.getOnlyPrefixes($scope.chosenStatesTo)
				} else {
					delete $scope.params.condition.filters.to_state
				}
			}
			$scope.getStorageList = () => {
				apiService.getData(moveBoardApi.ldStorages.list, {short: 1, pagesize: 999}).then(data => {
					if (data.data.items) {
						$scope.storages = data.data.items
					} else {
						$scope.storages = []
					}
					var stNull = {
						name: 'Choose storage'
					}
					$scope.storagesforOption = angular.copy(data.data.items)
					$scope.storagesforOption.unshift(stNull)
					$scope.selectedStorage = stNull
				}).catch(error => {
				});
			}

			$scope.removeFilters = () => {
				delete $scope.pickupDateFrom
				delete $scope.pickupDateTo
				delete $scope.deliveryDateFrom
				delete $scope.deliveryDateTo
				delete $scope.selectedStorage
				delete $scope.selectedStatus
				delete $scope.sitOnly
				delete $scope.deliveryOnly
				delete $scope.searchTerm
				delete $scope.searchTerm
				$scope.chosenStatesFrom = []
				$scope.chosenStatesTo = []
				$scope.params = {
					pagesize: -1,
					page: 1,
					filtering: '',
					sorting: {
						orderKey: 'pickup_date',
						orderValue: 'DESC'
					},
					condition: {
						filters: {
							in_trip: 0,
							to_state: [],
							from_state: [],
							pickup_date: {},
							delivery_date: {}
						}
					}
				}
				$scope.getrequestsData()
			}
			$scope.init = () => {
				$scope.chosenStatesFrom = []
				$scope.chosenStatesTo = []
				$scope.offset = -new Date().getTimezoneOffset() * 60
				var sortedColumn = _.isUndefined($scope.isModal) ? 0 : 1;
				$scope.dtOptions = DTOptionsBuilder
					.newOptions()
					.withDisplayLength(100)
					.withOption('order', [sortedColumn, 'desc'])
					.withOption('bFilter', false);
				$scope.dtColumnDefs = _.isUndefined($scope.isModal) ? [] : [
					DTColumnDefBuilder
						.newColumnDef(0)
						.notSortable()
				];
				$scope.date = {};
				$scope.isEmpty = _.isEmpty;
				$scope.selectedItems = [];
				$scope.filter = {
					origin: {},
					destination: {}
				};
				var setDates = function () {
					$scope.date.date_from = new Date()
					$scope.date.date_from.setMonth($scope.date.date_from.getMonth() - 1);
					$scope.date.date_to = new Date()
				};

				setDates();
				$scope.defaults = {
					statusList: [
						{
							id: "1",
							title: "Draft"
						},
						{
							id: "3",
							title: "Active"
						},
						{
							id: "2",
							title: "Pending "
						},
						{
							id: "4",
							title: "Delivered/Closed"
						}
					],
					dateTypes: [
						{
							id: 0,
							title: "Created date"
						},
						{
							id: 1,
							title: "Move date"
						}
					],
					jobType: [
						{
							id: "1",
							title: "Carrier/Agent"
						},
						{
							id: "2",
							title: "Personally"
						}
					],
					allowedstate: angular.copy(statesService.states())
				};

				$scope.dateType = 1;
				$scope.params = {
					pagesize: -1,
					page: 1,
					filtering: '',
					sorting: {
						orderKey: 'ld_nid',
						orderValue: 'DESC'
					},
					condition: {
						filters: {
							in_trip: 0,
							to_state: [],
							from_state: [],
							pickup_date: {},
							delivery_date: {}
						}
					}
				}
				$scope.getrequestsData()
				$scope.getStorageList()
				$scope.getLdStatuses()
			}
			$scope.init()
		}
	};
}
