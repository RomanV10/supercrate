'use strict';
import './tripListItem.styl'
import { checkPaymentsExists } from '../../agent-folio-payments/export-functions/payment.export.js';

angular
	.module('app.lddispatch')
	.directive('tripListItem', tripListItem);

tripListItem.$inject = ['$state'];

function tripListItem($state) {
	return {
		template: require('./tripListItem.pug'),
		restrict: 'A',
		scope: {
			item: '='
		},

		link: function($scope) {
			$scope.offset = -new Date().getTimezoneOffset() * 60;
			$scope.openTpDelivery = (tripId, job) => {
				$state.go('lddispatch.tpDelivery', { id : tripId, tpId: job.job_id, paymentExist: checkPaymentsExists(job) })
			}
		}
	};
}
