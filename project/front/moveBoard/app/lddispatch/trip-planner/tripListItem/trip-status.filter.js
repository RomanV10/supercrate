'use strict';

angular
	.module('app.lddispatch')
	.filter('tripStatus', tripStatus);

function tripStatus(){
	return function(text){
		if(text === '1'){
			return "Draft";
		} else if(text === '2'){
			return "Pending";
		} else if(text === '3'){
			return "On Trip";
		} else{
			return "Delivered/Closed";
		}
	}
}
