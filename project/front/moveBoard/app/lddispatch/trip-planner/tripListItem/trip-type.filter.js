'use strict';

angular
	.module('app.lddispatch')
	.filter('tripType', tripType);

function tripType(){
	return function(text){
		if(text === '1'){
			return "Carrier/Agent";
		}
		else if(text === '2'){
			return "Foreman/Helper";
		}
	}
}
