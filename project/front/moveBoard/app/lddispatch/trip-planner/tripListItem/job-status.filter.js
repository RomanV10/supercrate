'use strict';

angular
	.module('app.lddispatch')
	.filter('jobStatus', jobStatus);

function jobStatus() {
	return function (text) {
		switch (text.toString()) {
			case '1':
				return 'DRAFT';
				break;
			case '2':
				return 'PENDING TRIP';
				break;
			case '3':
				return 'IN ROUTE';
				break;
			case '4':
				return 'DELIVERED';
				break;
		}
	}
}
