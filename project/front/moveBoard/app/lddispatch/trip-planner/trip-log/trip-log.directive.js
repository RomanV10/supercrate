'use strict';
import './trip-log.styl'

angular
	.module('app.lddispatch')
	.directive('tripLogDirective', tripLogDirective);

tripLogDirective.$inject = ['$state','$rootScope', 'apiService', 'moveBoardApi','$sce', ];

function tripLogDirective($state, $rootScope, apiService, moveBoardApi, $sce) {
	return {
		template: require('./trip-log.pug'),
		restrict: 'E',
		scope: {
			tripId: '=',
			entityType: '='
		},
		link: function(scope,elem,attrs) {
			scope.allLogsShow = [];
			scope.allLogs = [];
			scope.toTrustedHTML = toTrustedHTML;
			scope.getIconType = getIconType;
			scope.getIconLogType = getIconLogType;
			apiService.getData(moveBoardApi.trip.getTripLog,{'entity_id': scope.tripId, 'entity_type': scope.entityType}).then(data => {
				scope.allLogs = data.data;
			});

			function toTrustedHTML( html ){
				if(angular.isString(html)){
					return $sce.trustAsHtml( html );
				}
			}
			function getIconType(type) {
				switch(type) {
					case 'mail':
						return 'fa-envelope-o';
						break;
					case 'request':
						return 'fa-folder-o';
						break;
					case 'custom_log':
						return 'fa-comment-o';
						break;
					default:
						return 'fa-bookmark';
				}
			}
			function getIconLogType(title, arr) {
				var types = ['payment', 'request', 'message','contract', 'surcharge', 'valuation', 'packing', 'discount', 'additional', 'mail'];
				if(!title) return false;
				var head = title.toLowerCase();
				var type = '';
				_.forEach(types, function (t) {
					if(head.search(t) >= 0){
						type = t;
					}
				});
				if(arr){
					_.forEach(types, function (t) {
						_.forEach(arr.text, function (item) {
							if(item.simpleText) {
								var simpleText = item.simpleText.toLowerCase();
								if (simpleText.search(t) >= 0) {
									type = t;
								}
							}
						});
					});
				}
				switch (type) {
					case 'valuation':
						return 'icon-wallet';
						break;
					case 'surcharge':
						return 'fa-tint';
						break;
					case 'payment':
						return 'fa-credit-card';
						break;
					case 'request':
						return 'fa-cog';
						break;
					case 'message':
						return 'fa-comment-o';
						break;
					case 'contract':
						return 'fa-pencil-square-o';
						break;
					case 'packing':
						return 'fa-archive';
						break;
					case 'discount':
						return 'fa-tags';
						break;
					case 'additional':
						return 'fa-list-alt';
						break;
					case 'mail':
						return 'fa-envelope-o';
						break;
					default:
						return 'fa-bookmark';
				}
			}

		}
	};
}

