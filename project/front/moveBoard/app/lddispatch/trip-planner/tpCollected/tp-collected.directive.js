'use strict';
import './tpCollected.styl'

angular
	.module('app.lddispatch')
	.directive('sitTpCollected', sitTpCollected);

sitTpCollected.$inject = ['$state', '$rootScope', 'RequestServices', 'apiService', 'moveBoardApi', 'ElromcoObserver', 'events', 'routerTracker', '$mdDialog', '$timeout',];

function sitTpCollected($state, $rootScope, RequestServices, apiService, moveBoardApi, ElromcoObserver, events, routerTracker,  $mdDialog, $timeout) {
	return {
		template: require('./tpCollected.pug'),
		restrict: 'A',
		scope: {},
		link: function ($scope) {
			var responseDellay;
			var showPaymentDellay;
			$scope.job_id = $state.params.job_id;
			$scope.trip_id = $state.params.trip_id;
			$scope.balance = $state.params.balance;
			$scope.userInfo = $state.params.userInfo;
			$scope.paymentExist = $state.params.paymentExist === 'true';
			$scope.busy = false;

			$scope.back = () => {
				let routeHistory = routerTracker.getRouteHistory();
				if (!_.isEmpty(routeHistory)) {
					_.each(routeHistory, function (history, i) {
						if (history.route.name !== 'lddispatch.sitTpCollected') {
							let routeName = history.route.name;
							let routeParams = history.routeParams;
							$state.go(routeName, routeParams);
							return false
						}
						if (i === routeHistory.length - 1) {
							$state.go('lddispatch.tripDetails', {id: $scope.trip_id, tab: 2});
						}
					})
				} else {
					$state.go('lddispatch.tripDetails', {id: $scope.trip_id, tab: 2});
				}
			};
			$scope.setPending = (receipt) => {
				$scope.disabledSwitch = true;
				$scope.busy = true;
				var id = receipt.receipt_id || receipt.id;
				var pending = receipt.value.pending ? 1 : 0;
				apiService.postData(moveBoardApi.carrier_payments.setReceiptPending, {
					id,
					pending
				}).then(data => {
					responseDellay = $timeout(function () {
						$scope.disabledSwitch = false;
						$scope.busy = false;
					}, 2000);
					let balanceFloat = parseFloat($scope.balance);
					let amountFloat = parseFloat(receipt.value.amount);
					if (pending) {
						$scope.balance = balanceFloat - amountFloat;
					} else {
						$scope.balance = balanceFloat + amountFloat;
					}
					$state.go('lddispatch.sitTpCollected', {
						trip_id: $scope.trip_id,
						job_id: $scope.job_id,
						balance: $scope.balance
					}, {notify: false});
				})
			};
			$scope.$on("$destroy", () => {
					$timeout.cancel(responseDellay);
					$timeout.cancel(showPaymentDellay);
				}
			);

			$scope.updatePayments = () => {
				$state.go('lddispatch.sitTpCollected', {
					trip_id: $scope.trip_id,
					job_id: $scope.job_id,
					balance: $scope.balance
				}, {notify: false});
				$scope.getReceipts($scope.job_id);
			};

			ElromcoObserver.addEventListener(events.trip.tpCollectedRemoved, $scope.updatePayments, $scope);


			$scope.getReceipts = job_id => {
				apiService.postData(moveBoardApi.carrier_payments.getRequestReceipts, {
					id: job_id,
					types: [3, 4]
				}).then(data => {
					$scope.receipts = data.data;
					if ($scope.paymentExist) {
						showDialog("You can't create or change receipts", "Carrier Payments for this job already exists");
					}
				});
			};

			function showDialog(label, text) {
				var alert = $mdDialog.alert()
					.parent(angular.element(document.body))
					.clickOutsideToClose(true)
					.title(label)
					.textContent(text)
					.ok('OK')

				$mdDialog.show(alert);
			}

			$scope.removeReceipt = (item, index) => {
				apiService.postData(moveBoardApi.carrier_payments.removeRequestReceipt, {id: item.receipt_id}, $scope).then(data => {
					ElromcoObserver.dispatch(events.trip.tpCollectedRemoved);
					$scope.balance = parseFloat($scope.balance) - parseFloat(item.total);
					$state.go('lddispatch.sitTpCollected', {
						trip_id: $scope.trip_id,
						job_id: $scope.job_id,
						balance: $scope.balance
					}, {notify: false})
				});
			};

			$scope.getReceipts($scope.job_id);

			var event = new MouseEvent('click', {
				'view': window,
				'bubbles': true,
				'cancelable': true
			});
			$scope.showReceiptModal = (ev, item) => {
				$scope.item = item;
				$scope.item.receipt_id = item.receipt_id ? item.receipt_id : item.id;
				$mdDialog.show({
					controller: DialogController,
					template: require('../../agent-folio-payments/receiptModal/receipt-modal-template.pug'),
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose: true,
					locals: {
						item: $scope.item,
					}
				})
			};
			function DialogController($scope, $mdDialog, item) {
				$scope.item = item;
				if (item.value.ccardN) {
					item.value.ccardN = item.value.ccardN.slice(-4)
				}
				$scope.cancel = function () {
					$mdDialog.cancel();
				};
			}

			function openPaymentModal() {
				angular.element('#tpCollectedCustomPayment').ready(function () {
					showPaymentDellay = $timeout(function () {
						$scope.openCustomPayment(event, 0, undefined, [$scope.job_id], 4);
						event.preventDefault()
					}, 500);
				});
			}

			if (!$scope.paymentExist) {
				if (_.isEmpty($scope.userInfo)) {
					RequestServices.getSuperRequest($scope.job_id).then(res => {
						$scope.userInfo = {
							name: res.request.name,
							phones: [res.request.phone],
							email: res.request.email
						};
						openPaymentModal()
					})
				} else {
					openPaymentModal()
				}

			}

		}
	};
}
