'use strict';
import './trips.styl';

angular
	.module('app.lddispatch')
	.directive('tripList', tripList);

tripList.$inject = ['$state', 'moveBoardApi', 'apiService', '$mdDialog'];

function tripList($state, moveBoardApi, apiService, $mdDialog) {
	return {
		template: require('./trips.pug'),
		restrict: 'A',

		link: function($scope) {
			var delayTimer, searchDelay;
			$scope.searchId = '';
			$scope.selectedStatus = JSON.parse(localStorage.getItem('tripStatusFilterTripPlanner'));
			$scope.params = {
				pageSize: 20,
				page: 1,
				sorting: {
					orderKey:'ld_trip_id',
					orderValue: 'DESC'
				},
				condition: {
					filters: {
						flag: []
					}
				}
			};
			if ($state.params.page) {
				$scope.params.page = $state.params.page;
			}

			if(!_.isEmpty($scope.selectedStatus)){
				$scope.params.condition.filters.flag = $scope.selectedStatus;
			} else {
				$scope.params.condition.filters.flag = []
			}

			$scope.tripStatus = [
				{
					id: "1",
					title: "Draft"
				},
				{
					id: "3",
					title: "Active"
				},
				{
					id: "2",
					title: "Pending "
				},
				{
					id: "4",
					title: "Delivered/Closed"
				}
			];
			$scope.tripType = [
				{
					id: "1",
					title: "Carrier/Agent"
				},
				{
					id: "2",
					title: "Foreman/Helper"
				}
			];

			function removeSelection() {
				_.forEach($scope.trips, item => {
					item.selected = false;
				});
			}

			$scope.editTrip = function (trip) {
				if (trip.selected) {
					$state.go("lddispatch.tripDetails", { "id": trip.ld_trip_id, "tab": '0'})
				} else {
					removeSelection();
					trip.selected = true;
				}
			};

			$scope.setTripType = () => {
				if ($scope.selectedType.id) {
					$scope.params.condition.filters.type = $scope.selectedType.id;
				} else {
					delete $scope.params.condition.filters.type;
				}
				$scope.getTrips();
			};


			$scope.closeSelectBox = (event) => {
				$("md-backdrop").trigger ("click")
			};
			$scope.searchByRequestId = (searchTerm) => {
				clearTimeout(searchDelay);
				searchDelay = setTimeout(function() {
					$scope.params.page = 1;
					$state.go('lddispatch.trip', { 'page': 1}, {notify: false});
					$scope.params.condition.filters.job_id = searchTerm;
					$scope.getTrips();
				}, 1000)
			};
			$scope.setTripStatus = () => {
				if (!_.isEmpty($scope.selectedStatus)) {
					$scope.params.condition.filters.flag = $scope.selectedStatus;
					localStorage.setItem('tripStatusFilterTripPlanner', JSON.stringify($scope.selectedStatus));
				} else {
					delete $scope.params.condition.filters.flag;
					localStorage.removeItem('tripStatusFilterTripPlanner');
				}
				clearTimeout(delayTimer);
				delayTimer = setTimeout(function() {
					$scope.getTrips();
				}, 1000)
			};


			$scope.setPage = page => {
				$scope.busy = true;
				$scope.params.page = page;
				$scope.params.pageSize = 20;

				apiService.postData(moveBoardApi.trip.getAllTrips, $scope.params).then(data => {

					$scope.trips = data.data.items;
					$scope.total = data.data.total;
					$scope.meta = data.data._meta;
					$state.go('lddispatch.trip', { 'page': page}, {notify: false});
					$scope.busy = false;
				});
			};

			$scope.addTrip = () => {
				$state.go("lddispatch.tripDetails", { "id": 'new'});
			};
			$scope.removeFilters = () => {
				$scope.selectedStatus = [];
				delete $scope.params.condition.filters.type;
				delete $scope.params.condition.filters.flag;
				delete $scope.params.condition.filters.job_id;
				$scope.params.sorting =  {
					orderKey:'ld_trip_id',
					orderValue: 'ASC'
				};
				$scope.searchId = '';
				localStorage.removeItem('tripStatusFilterTripPlanner');
				$scope.getTrips()

			};
			$scope.removeTrip = (ev, item, index) => {
				var confirm = $mdDialog.confirm()
					.title('Are you sure you want to remove trip #' + item.ld_trip_id)
					.targetEvent(ev)
					.ok('Yes')
					.cancel('No');
				$mdDialog.show(confirm).then(function() {
					removeTripRequest(item, index);
				});
			};

			function removeTripRequest(item, index) {
				item.jobsBusy = true;
				apiService.delData(moveBoardApi.trip.removeTrip, {tripId: item.ld_trip_id}).then(data => {
					$scope.trips.splice(index, 1);
					item.jobsBusy = false
				});
			}
			$scope.getTripJobs = (item) => {
				item.showJobs = !item.showJobs;
				if (item.showJobs && !item.jobs) {
					if (item.jobs_count) {
						var tripDetails = 1;
						item.jobsBusy = true;
						if (item.type == 1) {
							tripDetails = 0
						}
						apiService.postData(moveBoardApi.trip.getTripClosing, {id:item.ld_trip_id, pageSize: 40, total: 1, tripDetails: tripDetails}).then(data => {
							item.jobsBusy = false;
							if (data.data.items) {
								item.jobs = data.data.items;
								for (var i = 0; i < item.jobs.length; i++) {
									if (item.jobs[i].is_total == 1) {
										item.jobs.splice(i, 1)
									}
									if (item.jobs[i].job_id.substring(0, $scope.searchId.length) == $scope.searchId && $scope.searchId) {
										item.jobs[i].searchedItem = true
									}

								}
							} else {
								item.jobs = []
							}
						});
					}else{
						item.jobs = []
					}
				}
			};

			$scope.getTrips = () => {
				$scope.busy = true;
				$scope.params.pageSize = 20;
				apiService.postData(moveBoardApi.trip.getAllTrips, $scope.params).then(data => {
					$scope.trips = data.data.items || [];
					$scope.total = data.data.total;
					$scope.meta = data.data._meta;
					$scope.busy = false;
					if ($scope.searchId) {
						$scope.trips.forEach(item => {
							item.showJobs = false;
							$scope.getTripJobs(item)
						})
					}
				});
			};
			$scope.getTrips();
		}
	};
}
