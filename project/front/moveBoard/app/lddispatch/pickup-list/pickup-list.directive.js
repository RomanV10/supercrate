'use strict';
import './pickupList.styl'

angular
	.module('app.lddispatch')
	.directive('pickupList', pickupList);

pickupList.$inject = ['moveBoardApi', 'apiService', 'statesService', 'datacontext'];

function pickupList(moveBoardApi, apiService, statesService, datacontext) {
	return {
		template: require('./pickupList.pug'),
		restrict: 'A',

		link: function($scope) {

			$scope.getList = function () {
				$scope.makeStatesArray();
				$scope.busy = true;
				apiService.postData(moveBoardApi.request.pickup, $scope.params).then(({data}) => {
					$scope.busy = false;
					calculateTotals(data._meta);
					$scope.meta = data._meta;
					if (data.items) {
						$scope.jobs = $scope.getLdStatusName(data.items);
					} else {
						$scope.jobs = [];
					}
				}).catch(error => {
					$scope.busy = false;
				});
			};

			function calculateTotals(meta) {
				if (meta.pageCount < $scope.params.page)
					$scope.params.page = meta.pageCount || 1;
			}

			$scope.getLdStatuses = () => {
				$scope.fieldData = datacontext.getFieldData();
				if ($scope.fieldData.longdistance.ldStatus) {
					$scope.ldStatuses = angular.fromJson($scope.fieldData.longdistance.ldStatus);
				} else {
					$scope.ldStatuses = []
				}
			};
			$scope.getLdStatusName = (array) => {
				array.forEach(item => {
					if ($scope.ldStatuses.length > 0) {
						for (var i = 0; i < $scope.ldStatuses.length; i++) {
							if ($scope.ldStatuses[i].id.toString() == item.ld_status && item.ld_status) {
								item.ld_status_name = $scope.ldStatuses[i].name
								item.ld_status_color = $scope.ldStatuses[i].color
								break
							} else {
								item.ld_status_name = 'No Status'
							}
						}
					} else {
						item.ld_status_name = 'No Status'
					}

					item.distance_from_st = parseFloat(item.distance_from_st.replace(',', '.').replace(' ', '') || 0)
					item.distance_to_st = parseFloat(item.distance_to_st.replace(',', '.').replace(' ', '') || 0)
				});
				return array
			};
			$scope.makeStatesArray = () => {
				if ($scope.chosenStatesFrom.length > 0) {
					$scope.params.condition.filters.from_state = statesService.getOnlyPrefixes($scope.chosenStatesFrom)
				} else {
					delete $scope.params.condition.filters.from_state
				}
				if ($scope.chosenStatesTo.length > 0) {
					$scope.params.condition.filters.to_state = statesService.getOnlyPrefixes($scope.chosenStatesTo)
				} else {
					delete $scope.params.condition.filters.to_state
				}
			};
			$scope.changeFromFilter = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.pickup_date.from = moment(timestamp).startOf('day').unix()
				} else {
					$scope.from = new Date();
					$scope.params.condition.filters.pickup_date.from = moment($scope.from).startOf('day').unix()
				}
				$scope.getList()
			};
			$scope.changeToFilter = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.pickup_date.to = moment(timestamp).endOf('day').unix()
				} else {
					delete $scope.params.condition.filters.pickup_date.to

				}
				$scope.getList()

			};

			$scope.openModalFrom = () => {
				$scope.modalFromOpened = true
			};

			$scope.openModalTo = () => {
				$scope.modalToOpened = true
			};

			$scope.setStatus = () => {
				if ($scope.selectedStatus.id) {
					$scope.params.condition.filters.ld_status = $scope.selectedStatus.id
				} else {
					delete $scope.params.condition.filters.ld_status
				}
				$scope.getList()
			};

			$scope.$watch(() => $scope.chosenStatesFrom.length, () => {
				$scope.selectStringFrom = statesService.getPrefixesString($scope.chosenStatesFrom)
			});
			$scope.$watch(() => $scope.chosenStatesTo.length, () => {
				$scope.selectStringTo = statesService.getPrefixesString($scope.chosenStatesTo)
			});

			$scope.removeFilters = () => {
				$scope.from = new Date();
				delete $scope.to;
				delete $scope.selectedStatus;
				$scope.$broadcast('removeAllStatesFilters');
				$scope.params = {
					pagesize: 25,
					page: 1,
					filtering: '',
					sorting: {
						orderKey:'pickup_date',
						orderValue: 'ASC'
					},
					condition: {
						display_type: 1,
						filters: {
							pickup_date: {
								from: moment($scope.from).startOf('day').unix()
							}
						}
					}
				};
				$scope.getList()
			};

			$scope.setList = (page) => {
				$scope.params.page = page;
				$scope.getList();
			};

			$scope.init = () => {
				$scope.chosenStatesFrom = [];
				$scope.chosenStatesTo = [];
				$scope.from = new Date();
				$scope.offset = - new Date().getTimezoneOffset() * 60;
				$scope.params = {
					pagesize: 25,
					page: 1,
					filtering: '',
					sorting: {
						orderKey:'pickup_date',
						orderValue: 'DESC'
					},
					condition: {
						display_type: 1,
						filters: {
							pickup_date: {
								from: moment($scope.from).startOf('day').unix() + $scope.offset
							}
						}
					}
				};
				$scope.getList();
				$scope.getLdStatuses();
			};

			$scope.init()
		}
	};
}
