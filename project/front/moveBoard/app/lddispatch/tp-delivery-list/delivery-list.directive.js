'use strict';
import "./deliveryList.styl";

angular
	.module('app.lddispatch')
	.directive('deliveryList', deliveryList);

deliveryList.$inject = ['$state', '$document', 'moveBoardApi', 'apiService', 'statesService', 'datacontext'];

function deliveryList($state, $document, moveBoardApi, apiService, statesService, datacontext) {
	return {
		template: require('./deliveryList.pug'),
		restrict: 'A',
		
		link: function ($scope) {
			
			$scope.getList = function () {
				$scope.makeStatesArray();
				$scope.busy = true;
				apiService.postData(moveBoardApi.request.pickup, $scope.params).then(({data}) => {
					$scope.busy = false;
					calculateTotals(data._meta);
					if (data.items) {
						$scope.jobs = $scope.getLdStatusName(data.items);
					} else {
						$scope.jobs = [];
					}
				}).catch(error => {
					$scope.busy = false;
				});
			};
			
			function calculateTotals(meta) {
				if (meta.pageCount < $scope.params.page)
					$scope.params.page = meta.pageCount || 1;
				$scope.selectInfo.totalItems = meta.totalCount;
				$scope.selectInfo.totalPages = meta.pageCount;
				let firstOnPage = (meta.currentPage - 1) * meta.perPage + 1;
				let lastOnPage = meta.currentPage * meta.perPage;
				$scope.selectInfo.firstItemNumber = $scope.selectInfo.totalItems > 0
					? firstOnPage
					: 0;
				$scope.selectInfo.lastItemNumber = meta.totalCount < lastOnPage
					? meta.totalCount
					: lastOnPage;
				$scope.selectInfo.loaded = true;
			}
			
			$scope.routeTrip = (id) => {
				$state.go('lddispatch.tripDetails', {id: id, tab: 0});
			};
			$scope.getLdStatuses = () => {
				$scope.fieldData = datacontext.getFieldData();
				if ($scope.fieldData.longdistance.ldStatus) {
					$scope.ldStatuses = angular.fromJson($scope.fieldData.longdistance.ldStatus);
				} else {
					$scope.ldStatuses = [];
				}
			};
			$scope.getLdStatusName = (array) => {
				array.forEach(item => {
					if ($scope.ldStatuses.length > 0) {
						for (var i = 0; i < $scope.ldStatuses.length; i++) {
							if ($scope.ldStatuses[i].id.toString() == item.ld_status && item.ld_status) {
								item.ld_status_name = $scope.ldStatuses[i].name;
								item.ld_status_color = $scope.ldStatuses[i].color;
								break;
							} else {
								item.ld_status_name = 'No Status';
							}
						}
					} else {
						item.ld_status_name = 'No Status';
					}
					
					item.distance_from_st = parseFloat(item.distance_from_st.replace(',', '.').replace(' ', '') || 0);
					item.distance_to_st = parseFloat(item.distance_to_st.replace(',', '.').replace(' ', '') || 0);
				})
				return array;
			};
			$scope.getStorageList = () => {
				apiService.getData(moveBoardApi.ldStorages.list, {short: 1, pagesize: 999}).then(data => {
					$scope.storages = data.data.items;
					var stNull = {
						name: 'Choose storage'
					};
					$scope.storagesforOption = angular.copy(data.data.items);
					$scope.storagesforOption.unshift(stNull);
					$scope.selectedStorage = stNull;
				}).catch(error => {
				});
			};
			$scope.makeStatesArray = () => {
				if ($scope.chosenStatesFrom.length > 0) {
					$scope.params.condition.filters.from_state = statesService.getOnlyPrefixes($scope.chosenStatesFrom);
				} else {
					delete $scope.params.condition.filters.from_state;
				}
				if ($scope.chosenStatesTo.length > 0) {
					$scope.params.condition.filters.to_state = statesService.getOnlyPrefixes($scope.chosenStatesTo);
				} else {
					delete $scope.params.condition.filters.to_state;
				}
			};
			$scope.changeFromFilter = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.delivery_date = $scope.params.condition.filters.delivery_date || {};
					$scope.params.condition.filters.delivery_date.from = moment(timestamp).startOf('day').unix() + $scope.offset;
				} else {
					/*$scope.from = new Date()*/
					delete $scope.params.condition.filters.delivery_date.from;
				}
				$scope.getList();
			};
			$scope.changeToFilter = (timestamp) => {
				if (timestamp) {
					$scope.params.condition.filters.delivery_date = $scope.params.condition.filters.delivery_date || {};
					$scope.params.condition.filters.delivery_date.to = moment(timestamp).endOf('day').unix() + $scope.offset;
				} else {
					delete $scope.params.condition.filters.delivery_date.to;
					
				}
				$scope.getList();
				
			};
			
			$scope.openModalFrom = () => {
				$scope.modalFromOpened = true;
			};
			
			$scope.openModalTo = () => {
				$scope.modalToOpened = true;
			};
			
			$scope.setStatus = () => {
				if ($scope.selectedStatus.id) {
					$scope.params.condition.filters.ld_status = $scope.selectedStatus.id;
				} else {
					delete $scope.params.condition.filters.ld_status;
				}
				$scope.getList();
			};
			$scope.setStorage = () => {
				if ($scope.selectedStorage.storage_id) {
					$scope.params.condition.filters.storage_id = $scope.selectedStorage.storage_id;
				} else {
					delete $scope.params.condition.filters.storage_id;
				}
				$scope.getList();
			};
			$scope.selectSitOnly = (bool) => {
				if (bool) {
					$scope.params.condition.filters.sit_status = 1;
				} else {
					delete $scope.params.condition.filters.sit_status;
				}
				$scope.getList();
			};
			
			$scope.selectDeliveryOnly = (bool) => {
				if (bool) {
					$scope.params.condition.filters.ready_for_delivery = 1;
				} else {
					delete $scope.params.condition.filters.ready_for_delivery;
				}
				$scope.getList();
			};
			
			$scope.$watch(() => $scope.chosenStatesFrom.length, () => {
				$scope.selectStringFrom = statesService.getPrefixesString($scope.chosenStatesFrom);
			});
			$scope.$watch(() => $scope.chosenStatesTo.length, () => {
				$scope.selectStringTo = statesService.getPrefixesString($scope.chosenStatesTo);
			});
			
			$scope.removeFilters = () => {
				delete $scope.from;
				delete $scope.to;
				delete $scope.selectedStatus;
				delete $scope.selectedStorage;
				delete $scope.sitOnly;
				delete $scope.deliveryOnly;
				$scope.chosenStatesFrom = [];
				$scope.chosenStatesTo = [];
				$scope.params = {
					pagesize: 25,
					page: 1,
					filtering: '',
					sorting: {
						orderKey: 'delivery_date',
						orderValue: 'ASC'
					},
					condition: {
						display_type: 2,
						filters: {
							sit: {},
							ld: {},
							from_state: [],
							to_state: [],
							pickup_date: {
								to: moment(new Date()).startOf('day').unix() + $scope.offset
							}
						}
					}
				};
				$scope.getList();
			};
			
			$scope.nextPage = () => {
				if ($scope.params.page >= $scope.selectInfo.totalPages) return;
				$scope.params.page++;
				$scope.getList();
			};
			
			$scope.prevPage = () => {
				if ($scope.params.page <= 1) return;
				$scope.params.page--;
				$scope.getList();
			};
			
			$scope.init = () => {
				$scope.selectInfo = {};
				$scope.chosenStatesFrom = [];
				$scope.chosenStatesTo = [];
				$scope.offset = -new Date().getTimezoneOffset() * 60;
				$scope.params = {
					pagesize: 25,
					page: 1,
					filtering: '',
					sorting: {
						orderKey: 'delivery_date',
						orderValue: 'DESC'
					},
					condition: {
						display_type: 2,
						filters: {
							sit: {},
							ld: {},
							from_state: [],
							to_state: [],
							pickup_date: {
								to: moment(new Date()).startOf('day').unix() + $scope.offset
							}
						}
					}
				};
				$scope.getList();
				$scope.getStorageList();
				$scope.getLdStatuses();
			};
			$scope.init();
		}
	};
}
