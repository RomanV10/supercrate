'use strict';
import './agentFolioListItem.styl'

angular
	.module('app.lddispatch')
	.directive('agentFolioItem', agentFolioItem);

agentFolioItem.$inject = ['$state', 'moveBoardApi', 'apiService'];

function agentFolioItem($state, moveBoardApi, apiService) {
	return {
		template: require('./agentFolioListItem.pug'),
		restrict: 'A',

		link: function($scope) {

			$scope.showDetails = (item) => {
				$state.go('lddispatch.payments', {id: item.ld_carrier_id, tab: 0});
			};

			$scope.closePayments = (id) => {
				$state.go('lddispatch.showAgent', {id: id});
			};
		}
	};
}
