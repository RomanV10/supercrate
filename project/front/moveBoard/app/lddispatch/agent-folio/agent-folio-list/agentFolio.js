'use strict';
import './agentFolio.styl'

angular
	.module('app.lddispatch')
	.directive('agentFolioList', agentFolioList);
agentFolioList.$inject = ['$state', 'moveBoardApi', 'apiService', '$timeout'];

function agentFolioList($state, moveBoardApi, apiService, $timeout) {
	return {
		template: require('./agentFolio.pug'),
		restrict: 'A',

		link: function ($scope) {
			var searchDelay;
			$scope.hideZero = JSON.parse(localStorage.getItem('agentFolioHideZeroValue'));

			if ($scope.hideZero == null) {
				$scope.hideZero = 1;
				localStorage.setItem('agentFolioHideZeroValue', JSON.stringify($scope.hideZero));
			}
			$scope.params = {
				shortlist: 1,
				total: 1,
				page: 1,
				pageSize: 20,
				hide_zero: $scope.hideZero,
				hide_no_trips: 1,
				sorting: {
					orderKey: 'name',
					orderValue: 'ASC'
				},
				condition: {
					filters: {}
				}
			};
			$scope.params.page = $state.params.page || 1;
			$scope.agentFolio = [];

			$scope.setHideZero = () => {
				$scope.params.hide_zero = $scope.hideZero;
				localStorage.setItem('agentFolioHideZeroValue', JSON.stringify($scope.hideZero));
				$scope.getAgentFolio()
			};

			$scope.setPage = function (page) {
				$scope.params.page = page;
				$scope.getAgentFolio();
			};

			$scope.searchByName = (searchTerm) => {
				$timeout.cancel(searchDelay);
				searchDelay = $timeout(function () {
					$scope.params.page = 1;
					$state.go('lddispatch.agentFolio', {'page': $scope.params.page}, {notify: false});
					$scope.params.condition.filters.name = searchTerm;
					$scope.getAgentFolio();
				}, 1000)
			};

			$scope.$on("$destroy", () => {
					$timeout.cancel(searchDelay);
				}
			);

			$scope.getAgentFolio = function () {
				$scope.busy = true;
				apiService.getData(moveBoardApi.trip_carrier.getAgentFolio, $scope.params).then(data => {
					$scope.agentFolio = data.data.items;
					$scope.meta = data.data._meta;
					$state.go('lddispatch.agentFolio', {'page': $scope.params.page}, {notify: false});
					$scope.busy = false;
				});
			};
			$scope.getAgentFolio();
		}
	};
}

