'use strict';
import '../trip-planner/trips/trips.styl';

angular
	.module('app.lddispatch')
	.filter('setDecimal', function () {
		return function (input, places) {
			if (isNaN(input)) return input;

			var factor = "1" + Array(+(places > 0 && places + 1)).join("0");
			return Math.round(input * factor) / factor;
		};
	});
