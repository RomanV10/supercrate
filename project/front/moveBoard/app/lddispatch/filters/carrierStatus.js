(function () {
	angular
	.module('app.lddispatch')
	.filter('carrierStatus', function(){
        return function(text){
            if(text === '0'){
                return "Unactive";
            }
            else {
                return "Active";
            }
        }
    })

})();