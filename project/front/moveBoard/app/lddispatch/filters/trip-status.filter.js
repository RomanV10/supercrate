(function () {
	'use strict';

	angular
		.module('app.lddispatch')
		.filter('tripStatus', function () {
			return function (text) {
				if (text == '1') {
					return "Draft";
				} else if (text == '2') {
					return "Pending";
				} else if (text == '3') {
					return "Active";
				} else if (text == '4') {
					return "Delivered / Closed";
				}
			}
		})
})();