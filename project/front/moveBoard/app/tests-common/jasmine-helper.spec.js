// This will run before any it function.
// Resetting a global state so the change in this function is testable

beforeEach(module('app'));
beforeEach(module('ngMockE2E'));

var $compile, $rootScope, testHelper, moveBoardApi, frontpage, getCurrent, getFlags, getManagers, getReminders, $httpBackend, $timeout, datacontext, $filter, $controller, $q, SweetAlert;

beforeEach(inject(function (_$httpBackend_, _testHelperService_, _moveBoardApi_, _datacontext_, _$timeout_, _$rootScope_, _$compile_, _$filter_, _$controller_, _$q_, _SweetAlert_) {
	$httpBackend = _$httpBackend_;
	$compile = _$compile_;
	$rootScope = _$rootScope_;
	testHelper = _testHelperService_;
	moveBoardApi = _moveBoardApi_;
	datacontext = _datacontext_;
	$timeout = _$timeout_;
	$filter = _$filter_;
	$controller = _$controller_;
	$q = _$q_;
	SweetAlert = _SweetAlert_;
	let frontpageResponse = testHelper.loadJsonFile('frontpage.mock');
	let getFlagsResponce = testHelper.loadJsonFile('get-flags.mock');

	$httpBackend.whenGET((url) => {
		return url.indexOf('app/dashboard/dashboard') != -1
	}).respond(200, {});
	$httpBackend.whenGET((url) => {
		return url.indexOf('moveBoard/content/img/icons') != -1;
	}).respond(200, {});
	$httpBackend.whenGET((url) => {
		return url.indexOf('server/moveBoard/content/audio/') != -1;
	}).respond(200, {});

	let getcurrent = testHelper.loadJsonFile('getcurrent-user.mock');
	let get_valuation_plan_mock = testHelper.loadJsonFile('moveBoard_get_valuation_plan.mock');
	$rootScope.currentUser = {userId: getcurrent};
	$rootScope.fieldData = frontpageResponse;
	$rootScope.fieldData.calcsettings = frontpageResponse.calcsettings;
	$rootScope.fieldData.basicsettings = frontpageResponse.basicsettings;
	$rootScope.fieldData.branches = [];
	$rootScope.availableFlags = getFlagsResponce;
	$rootScope.fieldData.valuation_plan_tables = get_valuation_plan_mock;
}));
