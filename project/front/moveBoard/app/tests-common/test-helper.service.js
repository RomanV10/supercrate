angular
	.module('app')
	.factory('testHelperService', testHelperService);

function testHelperService (config) {
	const services = {
		makeRequest,
		loadJsonFile,
	};

	return services;

	function getUrlWithParams(url, params) {
		for (let key in params) {
			if (url.indexOf('%' + key + '%') !== -1) {
				url = url.replace('%' + key + '%', params[key]);
				delete params[key];
			}
		}

		return url;
	}


	function makeRequest(url, params) {
		url = getUrlWithParams(url, params);
		return config.serverUrl + url;
	}

	function loadJsonFile(fileName) {
		return fileName && angular.copy(window.mocks[fileName]);
	}
}
