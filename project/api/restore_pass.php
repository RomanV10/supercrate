<?php
define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
require_once DRUPAL_ROOT . '/includes/password.inc';

$newhash = user_hash_password('root');
$updatepass = db_update('users')->fields(array('pass' => $newhash))->condition('uid', '1', '=')->execute();
echo "Done";
drupal_exit();