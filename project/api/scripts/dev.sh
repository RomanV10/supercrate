#!/usr/bin/env bash

echo '-> Enable Dev Modules'
drush -y en environment

drush -y vset cache FALSE
drush -y vset preprocess_js FALSE
drush -y vset preprocess_css FALSE
drush -y vset raven_enabled FALSE

echo '-> Flush Cache'
drush cc all

echo '-> Reset Password to roma4ke user'
drush upwd --password="root" roma4ke

echo '-> Swithing environment to development'
drush environment-switch development

echo '-> Disabling google calendar'
drush dgc 0

echo '-> Flush Cache'
drush cc all
