<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_inventory\Services\FilterAction;

class MoveInventoryFilterTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new FilterAction();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createFilter() {
    $file_path = dirname(__FILE__) . '/data/filter.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_filter', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/filterUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_filter_update', $parse_json['data']);
  }

  public function testCreateFilter() {
    self::createFilter();
    $rsp = variable_get('move_inventory_filter');
    $rsid = $this->fixture->create($rsp);
    $rsid = $rsid['filter_id'];
    $this->assertGreaterThanOrEqual(1, $rsid);

    return (int) $rsid;
  }

  /**
   * @depends testCreateFilter
   */
  public function testRetriveFilter($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertGreaterThanOrEqual(3, $ret);
  }

  /**
   * @depends testCreateFilter
   */
  public function testUpdateFilter($rsid) {
    $rs_up = variable_get('move_inventory_filter_update');
    $rs_update = new FilterAction($rsid);
    $upd = $rs_update->update($rs_up);
    $this->assertEquals(1, $upd);
  }

  /**
   * @depends testCreateFilter
   */
  public function testDeleteFilter($rsid) {
    $rsc_delete = new FilterAction($rsid);
    $del = $rsc_delete->delete();
    $this->assertEquals(TRUE, $del);
  }

}