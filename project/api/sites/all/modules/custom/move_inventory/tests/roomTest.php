<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_inventory\Services\RoomActions;
class MoveInventoryRoomTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new RoomActions();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createRoom() {
    $file_path = dirname(__FILE__) . '/data/room.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_room', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/roomUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_room_update', $parse_json['data']);
  }

  public function testCreateRoom() {
    self::createRoom();
    $rsp = variable_get('move_inventory_room');
    $rsid = $this->fixture->create($rsp);
    $rsid = $rsid['room_id'];
    $this->assertGreaterThanOrEqual(1, $rsid);

    return (int) $rsid;
  }

  /**
   * @depends testCreateRoom
   */
  public function testRietriveRoom($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertGreaterThanOrEqual(3, $ret);
  }

  /**
   * @depends testCreateRoom
   */
  public function testUpdateRoom($rsid) {
    $rs_up = variable_get('move_inventory_room_update');
    $rs_update = new RoomActions($rsid);
    $upd = $rs_update->update($rs_up);
    $this->assertEquals(1, $upd);
  }

  /**
   * @depends testCreateRoom
   */
  public function testDeleteRoom($rsid) {
    $rsc_delete = new RoomActions($rsid);
    $del = $rsc_delete->delete();
    $this->assertEquals(TRUE, $del);
  }

}