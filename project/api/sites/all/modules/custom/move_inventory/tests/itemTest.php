<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_inventory\Services\ItemActions;
class MoveInventoryItemTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new ItemActions();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createItem() {
    $file_path = dirname(__FILE__) . '/data/item.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_item', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/itemUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_item_update', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/itemToRequest.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_item_to_request', $parse_json['data']);
  }

  public function testCreateItem() {
    self::createItem();
    $rsp = variable_get('move_inventory_item');
    $rsid = $this->fixture->create($rsp);
    $rsid = $rsid['item_id'];
    $this->assertGreaterThanOrEqual(1, $rsid);

    return (int) $rsid;
  }

  /**
   * @depends testCreateItem
   */
  public function testUpdateItem($rsid) {
    $rs_up = variable_get('move_inventory_item_update');
    $rs_update = new ItemActions($rsid);
    $upd = $rs_update->update($rs_up);
    $this->assertEquals(1, $upd);
  }
  /**
   * @depends testCreateItem
   */
//  public function testAddItemToRequest($rsid) {
//    $rs_up = variable_get('move_inventory_item_to_request');
//    $upd = MoveInventoryItem::addItemToRequest($rs_up);
//    $this->assertEquals(TRUE, $upd);
//  }
  /**
   * @depends testCreateItem
   */
  public function testRietriveItem($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertGreaterThanOrEqual(3, $ret);
  }

  /**
   * @depends testCreateItem
   */
  public function testDeleteItem($rsid) {
    $rsc_delete = new ItemActions($rsid);
    $del = $rsc_delete->delete();
    $this->assertEquals(TRUE, $del);
  }

}