<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_inventory\Services\PackageActions;
use Drupal\move_inventory\Services\ItemActions;
class MoveInventoryPackageTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new PackageActions();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createPackage() {
    $file_path = dirname(__FILE__) . '/data/package.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_package', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/packageUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_package_update', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/packageRelation.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('move_inventory_package_relation', $parse_json['data']);
  }

  public function testCreatePackage() {
    self::createPackage();
    $rsp = variable_get('move_inventory_package');
    $rsid = $this->fixture->create($rsp);
    $rsid = $rsid['package_id'];
    $this->assertGreaterThanOrEqual(1, $rsid);

    return (int) $rsid;
  }

  /**
   * @depends testCreatePackage
   */
  public function testUpdatePackage($rsid) {
    $rs_up = variable_get('move_inventory_room_update');
    $rs_update = new PackageActions($rsid);
    $upd = $rs_update->update($rs_up);
    $this->assertEquals(1, $upd);
  }
  /**
   * @depends testCreatePackage
   */
  public function testAddPackageRelations($rsid) {
    $rs_up = variable_get('move_inventory_package_relation');
    $rs_up['package_id'] = $rsid;
    $rs_update = new ItemActions();
    $upd = $rs_update->addItemToPackage($rs_up);
    $this->assertGreaterThanOrEqual(1, $upd);
    return (int) $rsid;
  }

  /**
   * @depends testAddPackageRelations
   */
  public function testRietrivePackage($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertGreaterThanOrEqual(3, $ret);
  }
//  /**
//   * @depends testCreatePackage
//   */
//  public function testDeletePackage($rsid) {
//    $rsc_delete = new MoveInventoryPackage($rsid);
//    $del = $rsc_delete->delete();
//    $this->assertEquals(TRUE, $del);
//  }

}