<?php

use PHPUnit\Framework\TestCase;
use Drupal\move_inventory\Actions\ItemActions;

/**
 * Class InventoryItemTest.
 */
class InventoryItemTest extends TestCase {

  protected $fixture;
  protected $item;

  protected function setUp() {
    $this->fixture = new ItemActions();
    $this->item = array(
      'cf' => 10,
      'extra_service' => 20,
      'extra_service_name' => 'Test Item',
      'image_link' => 'https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png',
      'name' => 'Test',
      'status' => 0,
      'type' => 0,
      'uri' => 'public://inventory/Hodor.jpg',
    );
  }

  public function testCreateItem() {
    $result = $this->fixture->create($this->item);
    $this->assertArrayHasKey('item_id', $result);
    $this->assertEquals(1, $result['item_id']);
  }

  public function testUpdateItem() {
    $this->fixture->setId(1);
    $result = $this->fixture->update($this->item);
    $this->assertEquals(1, $result);
  }

  public function testDeleteItem() {
    $this->fixture->setId(1);
    $result = $this->fixture->delete();
    $this->assertEquals(TRUE, $result);
  }



}
