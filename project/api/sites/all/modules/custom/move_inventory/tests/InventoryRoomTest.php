<?php

use PHPUnit\Framework\TestCase;
use Drupal\move_inventory\Tasks\RoomTasks;

/**
 * Class InventoryRoomTests.
 */
class InventoryRoomTests extends TestCase {

  /**
   * @throws Exception
   */
  public function testCreateRoom() {
    $room = array(
      'image_link' => 'https://www.google.com.ua/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png',
      'name' => 'Test Room',
      'uri' => 'public://inventory/googlelogo_color_272x92dp.png',
    );
    $result = (new RoomTasks())::create($room);
    $this->assertArrayHasKey('room_id', $result);
    $this->assertEquals(1, $result['room_id']);
  }

  public function testGetCountBedroomCreate() {
    $this->assertEquals(1, (new RoomTasks())::getCountBedroomCreate(1));
    $this->assertEquals(1, (new RoomTasks())::getCountBedroomCreate(2));
    $this->assertEquals(1, (new RoomTasks())::getCountBedroomCreate(3));
    $this->assertEquals(1, (new RoomTasks())::getCountBedroomCreate(4));
    $this->assertNotEquals(2, (new RoomTasks())::getCountBedroomCreate(1));
    $this->assertNotEquals(3, (new RoomTasks())::getCountBedroomCreate(2));
    $this->assertNotEquals(4, (new RoomTasks())::getCountBedroomCreate(4));

    $this->assertEquals(2, (new RoomTasks())::getCountBedroomCreate(5));
    $this->assertEquals(2, (new RoomTasks())::getCountBedroomCreate(6));
    $this->assertEquals(2, (new RoomTasks())::getCountBedroomCreate(8));
    $this->assertNotEquals(1, (new RoomTasks())::getCountBedroomCreate(5));
    $this->assertNotEquals(3, (new RoomTasks())::getCountBedroomCreate(6));
    $this->assertNotEquals(4, (new RoomTasks())::getCountBedroomCreate(8));

    $this->assertEquals(3, (new RoomTasks())::getCountBedroomCreate(7));
    $this->assertEquals(3, (new RoomTasks())::getCountBedroomCreate(9));
    $this->assertNotEquals(1, (new RoomTasks())::getCountBedroomCreate(7));
    $this->assertNotEquals(2, (new RoomTasks())::getCountBedroomCreate(9));
    $this->assertNotEquals(4, (new RoomTasks())::getCountBedroomCreate(7));

    $this->assertEquals(4, (new RoomTasks())::getCountBedroomCreate(10));
    $this->assertNotEquals(1, (new RoomTasks())::getCountBedroomCreate(10));
    $this->assertNotEquals(2, (new RoomTasks())::getCountBedroomCreate(10));
    $this->assertNotEquals(3, (new RoomTasks())::getCountBedroomCreate(10));
  }

}
