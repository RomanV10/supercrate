<?php

namespace Drupal\move_inventory\Entity;

/**
 * Class Rooms.
 *
 * @package Drupal\move_inventory\Entity
 */
class Rooms {

  /**
   * Name.
   *
   * @var string
   */
  public $name = '';

  /**
   * Image link.
   *
   * @var string
   */
  public $image_link = '';

  /**
   * Type.
   *
   * @var int
   */
  public $type = 0;

  /**
   * Get name.
   *
   * @return string
   *   Name.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Set name.
   *
   * @param string $name
   *   Name.
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * Get image link.
   *
   * @return string
   *   Image link.
   */
  public function getImageLink(): string {
    return $this->image_link;
  }

  /**
   * Set image link.
   *
   * @param string $image_link
   *   Image link.
   */
  public function setImageLink(string $image_link) {
    $this->image_link = $image_link;
  }

  /**
   * Get type.
   *
   * @return int
   *   Type.
   */
  public function getType(): int {
    return $this->type;
  }

  /**
   * Set type.
   *
   * @param int $type
   *   Type.
   */
  public function setType(int $type) {
    $this->type = $type;
  }

}
