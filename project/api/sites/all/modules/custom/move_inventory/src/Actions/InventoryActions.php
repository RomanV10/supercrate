<?php

namespace Drupal\move_inventory\Actions;

use Drupal\move_inventory\Actions\SubActions\FilterSubActions;
use Drupal\move_inventory\Actions\SubActions\InventorySubActions;
use Drupal\move_inventory\Actions\SubActions\RoomSubActions;
use Drupal\move_inventory\Models\RequestInventoryModel;
use Drupal\move_inventory\Tasks\InventoryTasks;
use Drupal\move_inventory\Tasks\RoomTasks;
use Drupal\move_inventory\Util\enum\MoveInventoryRoomType;
use Drupal\move_inventory\Configs\Config;
use Drupal\move_new_log\Services\Log;
use Drupal\move_services_new\Services\Inventory;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Services\Cache;

/**
 * Class MoveInventory.
 *
 * @package Drupal\move_inventory\Services
 */
class InventoryActions extends AbstractActions {

  /**
   * Check if exist additional inventory for request.
   *
   * @return bool
   *   True or false.
   *
   * @throws \ServicesException
   */
  public function checkExistAddInventory() {
    $result = FALSE;
    if (InventoryTasks::checkExistAddInventory(
      (new InventorySubActions())->getEntityIdStoragePacking($this->getEntityId()))) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Change type inventory.
   *
   * @param array $data
   *   Entity id.
   *
   * @return bool
   *   Result.
   *
   * @throws \ServicesException
   */
  public function removeUnnecessary(array $data) {
    $result = TRUE;
    foreach ($data as $row) {
      $count = InventoryTasks::getCountItemInRequest($row['id']);
      $diff = $count - $row['count'];
      if ($diff) {
        InventoryTasks::updateCountItem($row['id'], $diff);
      }
      else {
        $conditions = array(
          ['field' => 'id', 'value' => $row['id'], 'operation' => '='],
        );
        InventoryTasks::deleteItemFromRequest($conditions);
      }
    }

    return $result;
  }

  /**
   * Create rooms and room weight for inventory request.
   *
   * @param object $node
   *   Node.
   *
   * @throws \Exception
   */
  public function createRoomsForRequest($node) {
    $filterSubActions = new FilterSubActions();
    $roomSubActions = new RoomSubActions();

    $weight = Config::getConfig()->getMinWeightForRequestRoom();
    $wrapper = entity_metadata_wrapper('node', $node);
    $count = RoomTasks::getCountBedroomCreate($wrapper->field_size_of_move->value());
    $default_rooms = RoomTasks::getDefaultRoom();
    $filters = InventoryTasks::getFilterIdsForRoom(Config::getConfig()->getBedroomId(), TRUE);
    // All room.
    InventoryTasks::insertRoomWeightForRequest(Config::getConfig()->getAllId(), $this->getEntityId(), $weight++);
    $data = RoomTasks::retrieve(Config::getConfig()->getBedroomId());
    $data['type'] = MoveInventoryRoomType::BEDROOM;
    $name = $data['name'];
    // Create bedroom.
    $counter_name = 1;
    for ($i = 1; $i <= $count; $i++) {
      if ($i === 1) {
        // Bedroom.
        InventoryTasks::insertRoomWeightForRequest(Config::getConfig()->getBedroomId(), $this->getEntityId(), $weight++);
        $data['name'] = $name . ' ' . ++$counter_name;
        continue;
      }
      $room_id = $roomSubActions->create($data)['room_id'];

      RoomTasks::insertRoomRelation($this->getEntityId(), $room_id);
      InventoryTasks::insertRoomWeightForRequest($room_id, $this->getEntityId(), $weight++);
      $filterSubActions->copyFiltersForRoom($filters, $room_id, MoveInventoryRoomType::BEDROOM);
      $data['name'] = $name . ' ' . ++$counter_name;
    }
    // Add relations for default rooms.
    foreach ($default_rooms as $id) {
      InventoryTasks::insertRoomWeightForRequest($id['room_id'], $this->getEntityId(), $weight++);
    }
  }

  /**
   * Check if change move size.
   *
   * @param int $selected
   *   Move of size selected.
   *
   * @return mixed
   *   Custom answer.
   *
   * @throws \Exception
   */
  public function checkChangeMoveSize(int $selected) {
    $filterSubActions = new FilterSubActions();
    $inventorySubActions = new InventorySubActions();
    $roomSubActions = new RoomSubActions();

    $new_count = RoomTasks::getCountBedroomCreate($selected);
    $old_count = RoomTasks::getCountBedroomInRequest($this->getEntityId());
    $last_name = RoomTasks::getLastNameBedroomInRequest($this->getEntityId());
    $diff = $new_count - $old_count;
    if ($diff > 0) {
      // Create the missing bedroom.
      $data = RoomTasks::retrieve(Config::getConfig()->getBedroomId());
      $data['type'] = MoveInventoryRoomType::BEDROOM;
      $filters = InventoryTasks::getFilterIdsForRoom(Config::getConfig()->getBedroomId(), TRUE);
      for ($i = 1; $i <= $diff; $i++) {
        if ($last_name == 'Bedroom') {
          $new_name = 'Bedroom 2';
        }
        else {
          $room_name_explode = explode(' ', $last_name);
          $new_name = $room_name_explode[0] . " " . (int) ++$room_name_explode[1];
        }
        $data['name'] = $new_name;
        $room_id = $roomSubActions->create($data)['room_id'];
        $filterSubActions->copyFiltersForRoom($filters, $room_id, MoveInventoryRoomType::BEDROOM);
        // Update weight.
        $inventorySubActions->updateWeightForRequest($last_name, $room_id, $this->getEntityId());
        // Add relation room_id in request.
        RoomTasks::insertRoomRelation($this->getEntityId(), $room_id);
        $last_name = $data['name'];
      }
    }
    elseif ($diff < 0) {
      $not_deleted_rooms = array();
      $can_delete_rooms = array();
      $counter_name = $old_count;
      for ($i = $old_count; $i > $new_count; $i--) {
        // Check if delete.
        $room_id = RoomTasks::getRoomIdForNameInRequest($last_name, $this->getEntityId());
        if (InventoryTasks::isRoomInRequest($room_id)) {
          $not_deleted_rooms[] = array('name' => $last_name);
        }
        else {
          $can_delete_rooms[] = array('name' => $last_name, 'room_id' => $room_id);
        }
        $last_name = 'Bedroom ' . --$counter_name;
      }

      if (empty($not_deleted_rooms)) {
        // Delete bedroom.
        foreach ($can_delete_rooms as $room) {
          $roomSubActions->setId($room['room_id']);
          $roomSubActions->delete();
        }
      }
    }

    return TRUE;
  }

  /**
   * Create custom room.
   *
   * @param mixed $conditions
   *   Conditions.
   *
   * @return mixed
   *   Room.
   *
   * @throws \Exception
   */
  public function createCustomRoom($conditions) {
    $inventorySubActions = new InventorySubActions();
    $roomSubActions = new RoomSubActions();
    $filterSubActions = new FilterSubActions();

    $room = array();
    $inventorySubActions->setId(
      $inventorySubActions->getEntityIdStoragePacking($this->getEntityId())
    );
    switch ($conditions['type']) {
      case MoveInventoryRoomType::CUSTOM:
        $roomSubActions->setId(Config::getConfig()->getAllId());
        $data = $roomSubActions->retrieve();
        $data['name'] = $conditions['name'];
        $data['type'] = MoveInventoryRoomType::CUSTOM;
        $room = $roomSubActions->create($data);
        $room['weight'] = InventoryTasks::getMaxWeight($this->getEntityId()) + 1;
        $room_id = $room['room_id'];
        $this->insertWeightForRequestRoom($room_id, $room['weight']);

        $filters = InventoryTasks::getFilterIdsForRoom(Config::getConfig()->getAllId(), TRUE);
        $filterSubActions->copyFiltersForRoom($filters, $room_id, MoveInventoryRoomType::CUSTOM);
        // Add relation room_id in request.
        RoomTasks::insertRoomRelation($this->getEntityId(), $room_id);

        $filters = $filterSubActions->index(array('room_id' => $room['room_id'], 'entity_id' => $this->getEntityId()));
        if (!empty($filters)) {
          $room['filters'] = $filters;
        }
        break;

      case MoveInventoryRoomType::BEDROOM:
        $last_name = RoomTasks::getLastNameBedroomInRequest($this->getEntityId());
        $roomSubActions->setId(Config::getConfig()->getBedroomId());
        $data = $roomSubActions->retrieve();
        if ($last_name == 'Bedroom') {
          $new_name = 'Bedroom 2';
        }
        else {
          $room_name_explode = explode(' ', $last_name);
          $new_name = $room_name_explode[0] . " " . (int) ++$room_name_explode[1];
        }
        $data['name'] = $new_name;
        $data['type'] = MoveInventoryRoomType::BEDROOM;

        $room = $roomSubActions->create($data);
        $room_id = $room['room_id'];
        $filters = InventoryTasks::getFilterIdsForRoom(Config::getConfig()->getBedroomId(), TRUE);

        $filterSubActions->copyFiltersForRoom($filters, $room_id, MoveInventoryRoomType::BEDROOM);
        // Update weight.
        $inventorySubActions->updateWeightForRequest($last_name, $room_id, $this->getEntityId());
        // Add relation room_id in request.
        RoomTasks::insertRoomRelation($this->getEntityId(), $room_id);
        $room['weight'] = InventoryTasks::getWeightForRoomInRequest($room['room_id'], $this->getEntityId());

        $filters = $filterSubActions->index(array('room_id' => $room['room_id'], 'entity_id' => $this->getEntityId()));
        if (!empty($filters)) {
          $room['filters'] = $filters;
        }
        break;
    }

    return $room;
  }

  /**
   * Delete custom room.
   *
   * @param int $room_id
   *   Room id.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function deleteCustomRoom(int $room_id) {
    $inventorySubActions = new InventorySubActions();
    $roomSubActions = new RoomSubActions();

    $inventorySubActions->setId(
      $inventorySubActions->getEntityIdStoragePacking($this->getEntityId())
    );
    $roomSubActions->setId($room_id);
    $roomSubActions->delete();
    RoomTasks::deleteRoomWeightForRequest($room_id, $this->getEntityId());

    return TRUE;
  }

  /**
   * Insert weight for custom room.
   *
   * @param int $room_id
   *   Room id.
   * @param int $weight
   *   Weight.
   *
   * @throws \Exception
   */
  public function insertWeightForRequestRoom(int $room_id, int $weight) {
    RoomTasks::deleteWeightRoom($room_id);
    RoomTasks::insertWeightRoom($room_id, $this->getEntityId(), $weight);
  }

  /**
   * Move items between rooms.
   *
   * @param int $id
   *   Int mi_request_inventory id.
   * @param int $room_id
   *   Room id.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function moveItems(int $id, int $room_id) {
    InventoryTasks::moveItem($room_id, $id);

    return TRUE;
  }

  /**
   * Cancel inventory changes.
   *
   * @param array $data
   *   Save inventory points.
   *
   * @return bool
   *   Result.
   *
   * @throws \Exception
   */
  public function cancelInventoryChange(array $data) {
    $conditions = array(
      [
        'field' => 'entity_id',
        'value' => $this->getEntityId(),
        'operation' => '=',
      ],
      [
        'field' => 'type_inventory',
        'value' => Config::getConfig()->getDefaultInventoryType(),
        'operation' => '=',
      ],
    );
    RequestInventoryModel::deleteInventory($conditions);
    foreach ($data as $row) {
      InventoryTasks::insertItemInRequest(InventoryTasks::prepareDataForInsert($row));
    }

    return TRUE;
  }

  /**
   * Transfer inventory.
   *
   * @param array $retrieveEntityTo
   *   Request retrieve to.
   * @param array $retrieveEntityFrom
   *   Request retrieve from.
   * @param string $fromInventoryTabName
   *   Inventory tab name.
   *
   * @return bool
   *   Custom answer.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function transferInventory(array $retrieveEntityTo, array $retrieveEntityFrom, string $fromInventoryTabName) {
    if ($retrieveEntityTo['field_request_settings']['inventoryVersion'] != $retrieveEntityFrom['field_request_settings']['inventoryVersion']) {
      services_error('Inventory version does not match', 406);
    }
    $type_inventory = InventoryTasks::getInventoryTypeForTabName($fromInventoryTabName);
    $inventory = RequestInventoryModel::getInventoryItems(['type_inventory' => $type_inventory, 'entity_id' => $this->getEntityId()]);
    // Get request all data for entity form and to.
    $req_all_data_to = MoveRequest::getRequestAllData($retrieveEntityTo['nid']);
    $req_all_data_from = MoveRequest::getRequestAllData($this->getEntityId());

    if (in_array($retrieveEntityTo['nid'], $req_all_data_from['transferedTo'])) {
      services_error('Inventory has been transferred already to request #' . $retrieveEntityTo['nid'], 406);
    }

    $type_inventory = InventoryTasks::getTypeInventoryFreeForSaveInNewInventory($req_all_data_to);
    $copies_inventory_id = [];

    if ($retrieveEntityTo['field_request_settings']['inventoryVersion'] == 2 && $retrieveEntityFrom['field_request_settings']['inventoryVersion'] == 2) {
      $logTo = new Log($retrieveEntityTo['nid'], EntityTypes::MOVEREQUEST);
      $logFrom = new Log($retrieveEntityFrom['nid'], EntityTypes::MOVEREQUEST);
      foreach ($inventory as $row) {
        $row['entity_id'] = $retrieveEntityTo['nid'];
        $row['type_inventory'] = $type_inventory;
        $inserted_id = InventoryTasks::insertItemInRequest(InventoryTasks::prepareDataForInsert($row));
        $copies_inventory_id[$row['id']] = (int) $inserted_id;
        $logTo->create(InventoryTasks::prepareLogTo($row));
      }
      $logFrom->create(InventoryTasks::prepareLogFrom($retrieveEntityTo['nid']));
    }

    $from_contract = MoveRequest::getContract($this->getEntityId());
    $to_contract = MoveRequest::getContract($retrieveEntityTo['nid']);

    if (empty($to_contract['factory'])) {
      $to_contract = array(
        'factory' => move_settings_load_from_file('factory_obj_default.json'),
      );

      $to_contract['declarationValue'] = $from_contract['declarationValue'];
    }

    if (empty($to_contract['finance'])) {
      $to_contract['finance'] = move_settings_load_from_file('finance_obj_default.json');
    }

    $type_inventory = InventoryTasks::getTypeInventoryFreeForSaveOnContract($req_all_data_to, $to_contract);
    $to_inventory_field = InventoryTasks::getFieldInventoryByType($type_inventory);
    $from_inventory_field = InventoryTasks::getFieldInventoryByType($fromInventoryTabName);
    $req_all_data_to[$to_inventory_field] = $req_all_data_from[$from_inventory_field] ?? [];

    $req_all_data_to['transferedFrom'][] = $this->getEntityId();
    $req_all_data_from['transferedTo'][] = $retrieveEntityTo['nid'];

    $to_inventory_tab_name = InventoryTasks::getTabNameByField($to_inventory_field);
    $to_contract['factory'][$to_inventory_tab_name] = $from_contract['factory'][$fromInventoryTabName];
    $to_contract['factory'][$to_inventory_tab_name]['tranfer'] = TRUE;
    $to_contract['factory'][$to_inventory_tab_name]['isSubmitted'] = FALSE;

    if ($fromInventoryTabName == 'inventoryMoving') {
      $inventory = new Inventory($this->getEntityId());
      $from_inventory = $inventory->retrieve();
      $inventory_list_from = $from_inventory['inventory_list'] ?? [];

      $req_all_data_to[$to_inventory_field] = array_merge($req_all_data_to[$to_inventory_field], $inventory_list_from);
    }

    // Old id update => new just created id.
    if ($retrieveEntityTo['field_request_settings']['inventoryVersion'] == 2 && !empty($req_all_data_to[$to_inventory_field])) {
      foreach ($req_all_data_to[$to_inventory_field] as &$item) {
        if (!empty($copies_inventory_id[$item['id']])) {
          $item['id'] = $copies_inventory_id[$item['id']];
        }
      }

      if (!empty($to_contract['factory'][$to_inventory_tab_name]['inventory'])) {
        foreach ($to_contract['factory'][$to_inventory_tab_name]['inventory'] as &$item) {
          if (!empty($copies_inventory_id[$item['article']])) {
            $item['article'] = $copies_inventory_id[$item['article']];
          }
        }
      }
    }

    if (!empty($retrieveEntityTo['storage_id'])) {
      $from_storage_request_all_data = MoveRequest::getRequestAllData($retrieveEntityTo['storage_id']);
      $from_storage_request_all_data[$to_inventory_field] = $req_all_data_to[$to_inventory_field];
      MoveRequest::setRequestAllData($retrieveEntityTo['storage_id'], $from_storage_request_all_data);
      Cache::updateCacheData($retrieveEntityTo['storage_id']);
    }

    MoveRequest::setRequestAllData($this->getEntityId(), $req_all_data_from);
    Cache::updateCacheData($this->getEntityId());
    MoveRequest::setRequestAllData($retrieveEntityTo['nid'], $req_all_data_to);
    Cache::updateCacheData($retrieveEntityTo['storage_id']);
    MoveRequest::setContract($retrieveEntityTo['nid'], $to_contract);

    return TRUE;
  }

}
