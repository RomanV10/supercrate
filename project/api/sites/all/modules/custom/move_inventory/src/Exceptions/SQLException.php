<?php

namespace Drupal\move_inventory\Exceptions;

use RuntimeException;
use Throwable;

/**
 * Class SQLException.
 *
 * @package Drupal\move_inventory\Exceptions
 */
class SQLException extends RuntimeException {

  /**
   * SQLException constructor.
   *
   * @param \Throwable $exception
   *   Exception interface.
   *
   * @throws \ServicesException
   */
  public function __construct(Throwable $exception) {
    $message = "{$exception->getMessage()} in {$exception->getFile()}: {$exception->getLine()} </br> Trace: {$exception->getTraceAsString()}";
    watchdog('SQL exception', $message, array(), WATCHDOG_CRITICAL);
    // Use services_error, when RESTServer is exists.
    if (array_search('RESTServer', get_declared_classes())) {
      services_error('DB error', 500);
    }
  }

}
