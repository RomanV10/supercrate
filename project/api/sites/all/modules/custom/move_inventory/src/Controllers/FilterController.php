<?php

namespace Drupal\move_inventory\Controllers;

use Drupal\move_inventory\Actions\FilterActions;
use Drupal\move_inventory\Util\enum\MoveInventoryEntity;

/**
 * Class FiltersController.
 *
 * @package Drupal\move_inventory\Controllers
 */
class FilterController extends AbstractController {

  /**
   * Create filter.
   *
   * @param mixed $data
   *   Data.
   *
   * @return array|mixed
   *   Filter
   *
   * @throws \Exception
   */
  public static function createInventoryFilter($data = array()) {
    return self::run(FilterActions::class, 'create', [$data]);
  }

  /**
   * Retrieve filter.
   *
   * @param int $id
   *   Id filter.
   *
   * @return array|mixed
   *   Filter.
   *
   * @throws \Exception
   */
  public static function retrieveInventoryFilter(int $id) {
    $filterActions = new FilterActions();
    $filterActions->setId($id);

    return self::run($filterActions, 'retrieve', []);
  }

  /**
   * Update filter.
   *
   * @param int $id
   *   Filter id.
   * @param mixed $data
   *   Data.
   *
   * @return bool|\DatabaseStatementInterface|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function updateInventoryFilter(int $id, $data) {
    $filterActions = new FilterActions();
    $filterActions->setId($id);

    return self::run($filterActions, 'update', [$data]);
  }

  /**
   * Delete filter.
   *
   * @param int $id
   *   Filter id.
   *
   * @return bool|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function deleteInventoryFilter(int $id) {
    $filterActions = new FilterActions();
    $filterActions->setId($id);

    return self::run($filterActions, 'delete', []);
  }

  /**
   * Index method for filters.
   *
   * @param array $conditions
   *   Conditions.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function indexInventoryFilter(array $conditions = array()) {
    return self::run(FilterActions::class, 'getAllFilters', [$conditions]);
  }

  /**
   * Update weight.
   *
   * @param array $data
   *   Data.
   *
   * @throws \Exception
   */
  public static function filterSorting(array $data) {
    self::run(FilterActions::class, 'updateWeight', [$data, MoveInventoryEntity::FILTER]);
  }

}
