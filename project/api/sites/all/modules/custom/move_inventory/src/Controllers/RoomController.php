<?php

namespace Drupal\move_inventory\Controllers;

use Drupal\move_inventory\Actions\RoomActions;
use Drupal\move_inventory\Util\enum\MoveInventoryEntity;

/**
 * Class RoomsController.
 *
 * @package Drupal\move_inventory\Controllers
 */
class RoomController extends AbstractController {

  /**
   * Create default room.
   *
   * @param array $data
   *   Data.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function createInventoryRoom(array $data = array()) {
    return self::run(RoomActions::class, 'create', [$data]);
  }

  /**
   * Update room.
   *
   * @param int $id
   *   Room id.
   * @param mixed $data
   *   Data.
   *
   * @return bool|\DatabaseStatementInterface|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function updateInventoryRoom(int $id, $data) {
    $roomAction = new RoomActions();
    $roomAction->setId($id);

    return self::run($roomAction, 'update', [$data]);
  }

  /**
   * Delete room.
   *
   * @param int $id
   *   Room id.
   *
   * @return bool|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function deleteInventoryRoom(int $id) {
    $roomAction = new RoomActions();
    $roomAction->setId($id);

    return self::run($roomAction, 'delete', []);
  }

  /**
   * Get rooms.
   *
   * @param array $conditions
   *   Conditions.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function getRooms(array $conditions = array()) {
    return self::run(RoomActions::class, 'getRooms', [$conditions]);
  }

  /**
   * Room sorting.
   *
   * @param mixed $data
   *   Data.
   *
   * @throws \Exception
   */
  public static function roomSorting($data) {
    self::run(RoomActions::class, 'updateWeight', [$data, MoveInventoryEntity::ROOM]);
  }

}
