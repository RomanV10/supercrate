<?php

namespace Drupal\move_inventory\Models;

/**
 * Class ItemRelationModel.
 *
 * @package Drupal\move_inventory\Models
 */
class ItemRelationModel extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'mi_items_relations';


  public static $alias = 'mir';

  /**
   * Delete item relation.
   *
   * @param array $conditions
   *   Array conditions.
   *
   * @return int
   *   Count of deleted row.
   *
   * @throws \ServicesException
   */
  public static function deleteItemRelation(array $conditions) {
    return self::deleteEntity(self::$table, $conditions);
  }

  /**
   * Merge item relation.
   *
   * @param array $keys
   *   Keys.
   * @param array $fields
   *   Fields.
   * @param array $expressions
   *   Expressions example isNull, count, avg etc.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeItemRelation(array $keys, array $fields, array $expressions = array()) {
    self::mergeEntity(self::$table, $keys, $fields, $expressions);
  }

  /**
   * Select item relation data.
   *
   * @param array $joins
   *   Array join table.
   * @param array $fields
   *   Array fields.
   * @param array $conditions
   *   Array conditions.
   * @param array $expressions
   *   Array expressions.
   * @param array $fetching
   *   Get result.
   *
   * @return mixed
   *   Result select.
   *
   * @throws \ServicesException
   */
  public static function selectItemRelation(array $joins = array(), array $fields = array(), array $conditions = array(), array $expressions = array(), array $fetching = array()) {
    return self::selectEntity(
      [
        'table' => self::$table,
        'alias' => self::$alias,
      ],
      $joins, $fields, $conditions, $expressions, $fetching);
  }

}
