<?php

namespace Drupal\move_inventory\Actions\SubActions;

use Drupal\move_inventory\Configs\Config;
use Drupal\move_inventory\Entity\Filter;
use Drupal\move_inventory\Models\FilterModel;
use Drupal\move_inventory\Models\ItemModel;
use Drupal\move_inventory\Models\RequestInventoryModel;
use Drupal\move_inventory\Tasks\FilterTasks;
use Drupal\move_inventory\Tasks\ItemTasks;
use Drupal\move_inventory\Util\enum\MoveInventoryRoomType;

/**
 * Class FilterSubActions.
 *
 * @package Drupal\move_inventory\Actions\SubActions
 */
class FilterSubActions extends AbstractSubActions implements SubActionsInterface {

  /**
   * Create service.
   *
   * @param array $data
   *   Data for create entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function create(array $data = array()) {
    $filter = (array) $this->serializer->deserialize(json_encode($data), Filter::class, 'json');
    $rooms_ids = $filter['rooms_ids'];
    unset($filter['rooms_ids']);
    $filter['filter_id'] = FilterTasks::createFilter($filter);
    $this->updateFilterRelations($filter['filter_id'], $rooms_ids);

    return $filter;
  }

  /**
   * Retrieve service.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function retrieve() {
    $filter = array(); $rooms = array();

    $result = FilterTasks::retrieveFilter($this->getId());
    if (!empty($result)) {
      foreach ($result as $row) {
        $rooms[$row['room_id']] = $row['room_id'];
        $filter = $row;
      }
      unset($filter['room_id']);
      $filter['rooms_ids'] = array_values($rooms);
    }

    return $filter;
  }

  /**
   * Service for update entity.
   *
   * @param array $data
   *   Data for update entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function update(array $data = array()) {
    $filter = (array) $this->serializer->deserialize(json_encode($data), Filter::class, 'json');
    $rooms_ids = $filter['rooms_ids'];
    unset($filter['rooms_ids']);
    $result = FilterTasks::updateFilter($this->getId(), $filter);
    $this->updateFilterRelations($this->getId(), $rooms_ids);

    return $result;
  }

  /**
   * Service for remove entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function delete() {
    if (empty(ItemModel::getItemsIdsForFilter($this->getId()))) {
      // Delete filter from filter table.
      FilterTasks::deleteFilter($this->getId());
      // Delete filter from filter relations table.
      FilterTasks::deleteFilterFromRelations($this->getId());
      // Delete filter from item relations table.
      FilterTasks::deleteFilterFromItemRelations($this->getId());
      // Delete filter from weight table.
      FilterTasks::deleteFilterFromWeight($this->getId());

      return TRUE;
    }

    return services_error('We cannot delete filter', 406);
  }

  /**
   * Index method for filters.
   *
   * @param array $conditions
   *   Conditions.
   *
   * @return array|mixed
   *   Filters data.
   *
   * @throws \Exception
   */
  public function index(array $conditions = array()) {
    $filters = FilterModel::selectFiltersWithConditions($conditions);
    $roomsId = $conditions['rooms_in_request'] ?? [];
    unset($conditions['rooms_in_request']);
    foreach ($filters as &$filter) {
      $filter['rooms'] = FilterTasks::getExistingRoom($filter['filter_id'], $roomsId);
      unset($filter['room_id']);

      // Get total for filter if exist entity_id.
      if (!empty($conditions['entity_id'])) {
        $conditions['filter_id'] = $filter['filter_id'];
        $filter += $this->filterTotalForEntity($conditions);
      }
    }

    return $filters ?? [];
  }

  /**
   * Copy filters, items for room and weight.
   *
   * @param array $filters
   *   Filters.
   * @param int $room_id
   *   Room id.
   * @param int $room_type
   *   Room type.
   *
   * @throws \Exception
   */
  public function copyFiltersForRoom(array $filters, int $room_id, int $room_type) {
    foreach ($filters as $filter) {
      FilterTasks::mergeFilterRelation($filter['filter_id'], $room_id);
      FilterTasks::mergeWeightFilter($filter, $room_id);

      $conditions = array('room_id' => $room_type == MoveInventoryRoomType::BEDROOM ? Config::getConfig()->getBedroomId() : Config::getConfig()->getAllId());
      $itemsId = ItemModel::getItemsIdsForFilter($filter['filter_id'], $conditions);
      if (!empty($itemsId)) {
        foreach ($itemsId as $id) {
          ItemTasks::insertWeightForItem($id['item_id'], $room_id, $filter['filter_id'], $id['weight']);
        }
      }
    }
  }

  /**
   * Update relations between filters and rooms.
   *
   * @param int $filter_id
   *   ID of the filter.
   * @param array $rooms_ids
   *   Array of rooms ids.
   *
   * @throws \Exception
   */
  public function updateFilterRelations(int $filter_id, array $rooms_ids) {
    if (!empty($rooms_ids)) {
      $existing_rooms = FilterTasks::getExistingRoom($filter_id, []);
      // Check and not remove needed rooms.
      $room_for_remove = array_diff($existing_rooms, $rooms_ids);
      if (!empty($room_for_remove)) {
        // Remove rooms.
        FilterTasks::removeRoomsFromFilterRelation($filter_id, $room_for_remove);
        // Clean weight table.
        FilterTasks::deleteFilterFromWeightWithRoom($filter_id, $room_for_remove);
        // Clean weight table for items.
        ItemTasks::deleteItemForFilterFromWeight($filter_id, $room_for_remove);
      }
      // Keep only new filters.
      $rooms_ids = array_diff($rooms_ids, $existing_rooms);

      // Add new field if they dose not exist.
      foreach ($rooms_ids as $room_id) {
        FilterTasks::mergeFilterRelation($filter_id, $room_id);
        // Insert default weight.
        $this->insertWeight($filter_id, $room_id);

        // Insert weigh for items and new room.
        $this->updateItemWeighRoom($filter_id, $room_id);
      }
    }
    else {
      FilterTasks::deleteFilterFromRelations($filter_id);
    }
  }

  /**
   * Insert weight for filter. Before delete old.
   *
   * @param int $filter_id
   *   Filter id.
   * @param int $room_id
   *   Room id.
   *
   * @throws \Exception
   */
  public function insertWeight(int $filter_id, int $room_id) {
    FilterTasks::deleteFilterFromWeightWithRoom($filter_id, array($room_id));
    FilterTasks::insertFilterWeight($filter_id, $room_id);
  }

  /**
   * Update for items when filter change rooms.
   *
   * @param int $filter_id
   *   Filter id.
   * @param int $room_id
   *   Room id.
   *
   * @throws \Exception
   */
  public function updateItemWeighRoom(int $filter_id, int $room_id) : void {
    $filter_items = ItemModel::getItemsIdsForFilter($filter_id);
    if (!empty($filter_items)) {
      foreach ($filter_items as $item_id) {
        ItemTasks::insertWeightForItem($item_id, $room_id, $filter_id, 0);
      }
    }
  }

  /**
   * Count totals fo filters.
   *
   * @param array $conditions
   *   Conditions.
   *
   * @return mixed
   *   Totals.
   */
  public function filterTotalForEntity(array $conditions) {
    $total_items = RequestInventoryModel::getEntityInventoryTotals($conditions);
    $result['total_count'] = (int) $total_items['total_count'] ?? 0;
    $result['total_cf'] = (int) $total_items['total_cf'] ?? 0;

    return $result;
  }

  /**
   * Get inventory for entity.
   *
   * @param array|null $conditions
   *   Conditions.
   *
   * @return mixed
   *   Result.
   *
   * @throws \ServicesException
   */
  public function getEntityInventory(?array $conditions) {
    $inventoryItems = RequestInventoryModel::getInventoryItems($conditions);
    if (!empty($inventoryItems)) {
      foreach ($inventoryItems as $key => $item) {
        $inventoryItems[$key]['count'] = (int) $item['count'];
        $inventoryItems[$key]['total_cf'] = (float) ($item['cf'] * $item['count']);
        $inventoryItems[$key]['moved'] = (int) $inventoryItems[$key]['moved'];
        $fields = ItemTasks::getItemFields($item['item_id'], [
          'extra_service',
          'extra_service_name',
          'image_link',
          'packing_fee',
        ]);
        $fields['uri'] = $fields['image_link'];
        $fields['image_link'] = $this->showPresetUrl($fields['image_link'] ?? '');
        $field_to_convert = ['extra_service' => 'float', 'packing_fee' => 'float'];
        $this->convertFieldsToNumber($fields, $field_to_convert);
        $inventoryItems[$key] += $fields;
      }
    }

    return $inventoryItems;
  }

}
