<?php

namespace Drupal\move_inventory\Entity;

/**
 * Class PackageRelation.
 *
 * @package Drupal\move_inventory\Entity
 */
class PackageRelation {


  /**
   * Count.
   *
   * @var int
   */
  public $count = 0;

  /**
   * Package id.
   *
   * @var int
   */
  public $package_id = 0;

  /**
   * Room id.
   *
   * @var int
   */
  public $room_id = 0;

  /**
   * Filter id.
   *
   * @var int
   */
  public $filter_id = 0;

  /**
   * Item id.
   *
   * @var int
   */
  public $item_id = 0;

  /**
   * Get count.
   *
   * @return int
   *   Count.
   */
  public function getCount(): int {
    return $this->count;
  }

  /**
   * Set count.
   *
   * @param int $count
   *   Count.
   */
  public function setCount(int $count) {
    $this->count = $count;
  }

  /**
   * Get package id.
   *
   * @return int
   *   Package id.
   */
  public function getPackageId(): int {
    return $this->package_id;
  }

  /**
   * Set package id.
   *
   * @param int $package_id
   *   Package id.
   */
  public function setPackageId(int $package_id) {
    $this->package_id = $package_id;
  }

  /**
   * Get room id.
   *
   * @return int
   *   Room id.
   */
  public function getRoomId(): int {
    return $this->room_id;
  }

  /**
   * Set room id.
   *
   * @param int $room_id
   *   Room id.
   */
  public function setRoomId(int $room_id) {
    $this->room_id = $room_id;
  }

  /**
   * Get filter id.
   *
   * @return int
   *   Filter id.
   */
  public function getFilterId(): int {
    return $this->filter_id;
  }

  /**
   * Set filter id.
   *
   * @param int $filter_id
   *   Filter id.
   */
  public function setFilterId(int $filter_id) {
    $this->filter_id = $filter_id;
  }

  /**
   * Get item id.
   *
   * @return int
   *   Item id.
   */
  public function getItemId(): int {
    return $this->item_id;
  }

  /**
   * Set item id.
   *
   * @param int $item_id
   *   Item id.
   */
  public function setItemId(int $item_id) {
    $this->item_id = $item_id;
  }

}
