<?php

namespace Drupal\move_inventory\Controllers;

use ReflectionMethod;

/**
 * Class AbstractController.
 *
 * @package Drupal\move_inventory\Controllers
 */
abstract class AbstractController {

  /**
   * Controller wrapper.
   *
   * @param mixed $class
   *   Object or class.
   * @param string $method
   *   Method name.
   * @param array $args
   *   Method argument.
   *
   * @return array
   *   Result.
   *
   * @throws \ServicesException
   */
  public static function run($class, string $method, array $args) {
    $result = NULL;
    try {
      $class = is_object($class) ? $class : new $class();

      $reflectionMethod = new ReflectionMethod($class, $method);
      $parameters = $reflectionMethod->getParameters();
      if (count($parameters) == count($args)) {
        $result = $reflectionMethod->invokeArgs($class, $args);
      }
      else {
        services_error('The number of arguments does not match', 406);
      }
    }
    catch (\Throwable $exception) {
      $message = "Method: $method </br>: {$exception->getMessage()} in {$exception->getFile()}: {$exception->getLine()} </br> Trace: {$exception->getTraceAsString()}";
      watchdog(get_class($class), $message, array(), WATCHDOG_CRITICAL);
      services_error($exception->getMessage(), $exception->getCode());
    }

    return self::getGoodCustomAnswer(200, 'OK', $result);
  }

  /**
   * Custom data for front.
   *
   * @param int $code
   *   Code exception.
   * @param string $message
   *   Message exception.
   * @param mixed $data
   *   Result method.
   *
   * @return array
   *   Result.
   */
  protected static function getGoodCustomAnswer(int $code, string $message, $data) {
    return [
      'status_code' => $code,
      'status_message' => $message,
      'data' => $data,
    ];
  }

}
