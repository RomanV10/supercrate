<?php

namespace Drupal\move_inventory\Tasks;

use Drupal\move_inventory\Models\FilterRelationModel;
use Drupal\move_inventory\Models\ItemModel;
use Drupal\move_inventory\Models\ItemRelationModel;
use Drupal\move_inventory\Models\WeightModel;
use Drupal\move_inventory\Util\enum\MoveInventoryEntity;
use Drupal\move_inventory\Util\enum\MoveInventoryItemType;

/**
 * Class ItemTasks.
 *
 * @package Drupal\move_inventory\Tasks
 */
class ItemTasks extends AbstractTasks {

  /**
   * Create custom item.
   *
   * @param array $item
   *   Item data.
   *
   * @return \DatabaseStatementInterface|int
   *   item id.
   *
   * @throws \Exception
   */
  public static function createCustomItem(array $item) {
    unset($item['room_id']);
    unset($item['count']);
    unset($item['entity_id']);
    unset($item['entity_type']);
    unset($item['filters']);
    unset($item['type_inventory']);

    return ItemModel::insertItem($item);
  }

  /**
   * Get needed field.
   *
   * @param array $data
   *   Data.
   *
   * @return array
   *   New data.
   */
  public static function prepareDataForAddItem(array $data) {
    $needed_field = [
      'id' => '',
      'entity_id' => '',
      'entity_type' => '',
      'item_id' => '',
      'room_id' => '',
      'name' => '',
      'count' => '',
      'price' => '',
      'cf' => '',
      'type' => '',
      'type_inventory' => '',
    ];

    return array_intersect_key($data, $needed_field);
  }

  /**
   * Create default or boxes item.
   *
   * @param array $item
   *   Item data.
   *
   * @return \DatabaseStatementInterface|int
   *   Id item.
   *
   * @throws \Exception
   */
  public static function createDefaultBoxedItem(array $item) {
    unset($item['filters']);
    unset($item['room_id']);
    unset($item['count']);
    unset($item['entity_id']);
    unset($item['entity_type']);
    unset($item['type_inventory']);

    return ItemModel::insertItem($item);
  }

  /**
   * Update item data.
   *
   * @param int $id
   *   Item id.
   * @param array $item
   *   Item data.
   *
   * @return \DatabaseStatementInterface
   *   Row affected.
   *
   * @throws \ServicesException
   */
  public static function updateItem(int $id, array $item) {
    unset($item['room_id']);
    unset($item['filters']);
    unset($item['room_id']);
    unset($item['count']);
    unset($item['entity_id']);
    unset($item['entity_type']);
    unset($item['type_inventory']);
    $conditions = array(
      ['field' => 'item_id', 'value' => $id, 'operation' => '='],
    );

    return ItemModel::updateItem($item, $conditions);
  }

  /**
   * Get field number.
   *
   * @return array
   *   Field => number type.
   */
  public static function getFieldNumber() {
    return [
      'total_count' => 'int',
      'count' => 'int',
      'total_cf' => 'float',
      'total_inventory_price' => 'float',
      'extra_service' => 'float',
      'total_price' => 'float',
      'cf' => 'float',
      'packing_fee' => 'float',
      'handle_fee' => 'float',
    ];
  }

  /**
   * Add item to request.
   *
   * @param array $data
   *   Data.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function addItemToRequest(array $data) {
    if (isset($data['filter_id'])) {
      InventoryTasks::mergeWithFilterId($data);
    }
    else {
      InventoryTasks::mergeWithoutFilter($data);
    }
  }

  /**
   * Conditions allowed for remove from request.
   *
   * @return array
   *   Conditions.
   */
  public static function conditionsAllowedForRemoveFromRequest() {
    return [
      'item_id' => NULL,
      'filter_id' => NULL,
      'room_id' => NULL,
      'id' => NULL,
    ];
  }

  /**
   * Delete weight for item.
   *
   * @param int $item_id
   *   Item id.
   * @param int $filter_id
   *   Filter id.
   * @param int $room_id
   *   Room id.
   *
   * @throws \ServicesException
   */
  public static function deleteWeightForItem(int $item_id, int $filter_id, int $room_id) {
    $conditions = array(
      ['field' => 'entity_id', 'value' => $item_id, 'operation' => '='],
      ['field' => 'filter_id', 'value' => $filter_id, 'operation' => '='],
      ['field' => 'room_id', 'value' => $room_id, 'operation' => '='],
      [
        'field' => 'entity_type',
        'value' => MoveInventoryEntity::ITEM,
        'operation' => '=',
      ],
    );
    WeightModel::deleteWeight($conditions);
  }

  /**
   * Delete item from weight table.
   *
   * @param int $itemId
   *   Item id.
   *
   * @throws \ServicesException
   */
  public static function deleteWeight(int $itemId) {
    $conditions = array(
      ['field' => 'entity_id', 'value' => $itemId, 'operation' => '='],
      [
        'field' => 'entity_type',
        'value' => MoveInventoryEntity::ITEM,
        'operation' => '=',
      ],
    );
    WeightModel::deleteWeight($conditions);
  }

  /**
   * Delete weight for item without room_id conditions.
   *
   * @param int $item_id
   *   Item id.
   * @param array $filter_for_remove
   *   Filters for remove.
   *
   * @throws \ServicesException
   */
  public static function deleteWeightForItemWithoutRoom(int $item_id, array $filter_for_remove) {
    $conditions = array(
      [
        'field' => 'filter_id',
        'value' => $filter_for_remove,
        'operation' => 'IN',
      ],
      [
        'field' => 'entity_type',
        'value' => MoveInventoryEntity::ITEM,
        'operation' => '=',
      ],
      ['field' => 'entity_id', 'value' => $item_id, 'operation' => '='],
    );
    WeightModel::deleteWeight($conditions);
  }

  /**
   * Delete items for filter from weight table.
   *
   * @param int $filterId
   *   Filter id.
   * @param array $room_for_remove
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function deleteItemForFilterFromWeight(int $filterId, array $room_for_remove) {
    $conditions = array(
      ['field' => 'room_id', 'value' => $room_for_remove, 'operation' => 'IN'],
      ['field' => 'filter_id', 'value' => $filterId, 'operation' => '='],
      [
        'field' => 'entity_type',
        'value' => MoveInventoryEntity::ITEM,
        'operation' => '=',
      ],
    );
    WeightModel::deleteWeight($conditions);
  }

  /**
   * Delete weight for item.
   *
   * @param int $entityId
   *   Entity id.
   *
   * @throws \ServicesException
   */
  public static function deleteWeightForItemWithoutFilter(int $entityId) {
    $conditions = array(
      ['field' => 'entity_id', 'value' => $entityId, 'operation' => '='],
      ['field' => 'room_id', 'value' => 0, 'operation' => '='],
      [
        'field' => 'entity_type',
        'value' => MoveInventoryEntity::ITEM,
        'operation' => '=',
      ],
    );
    WeightModel::deleteWeight($conditions);
  }

  /**
   * Insert weight for item.
   *
   * @param int $item_id
   *   Item id.
   * @param int $room_id
   *   Room id.
   * @param int $filter_id
   *   Filter id.
   * @param int $weight
   *   Weight.
   *
   * @throws \Exception
   */
  public static function insertWeightForItem(int $item_id, int $room_id, int $filter_id, int $weight) {
    $fields = array(
      'entity_id' => $item_id,
      'entity_type' => MoveInventoryEntity::ITEM,
      'room_id' => $room_id,
      'filter_id' => $filter_id,
      'weight' => $weight,
    );
    WeightModel::insertWeight($fields);
  }

  /**
   * Delete row from item relations.
   *
   * @param int $itemId
   *   Item id.
   * @param array $filter_for_remove
   *   Filters for remove.
   *
   * @throws \ServicesException
   */
  public static function deleteFromItemRelations(int $itemId, array $filter_for_remove) {
    $conditions = array(
      ['field' => 'item_id', 'value' => $itemId, 'operation' => '='],
      [
        'field' => 'filter_id',
        'value' => $filter_for_remove,
        'operation' => 'IN',
      ],
    );

    ItemRelationModel::deleteItemRelation($conditions);
  }

  /**
   * Merge item relations.
   *
   * @param int $filter_id
   *   Filter id.
   * @param int $item_id
   *   Item id.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeItemRelations(int $filter_id, int $item_id) {
    $keys = array(
      'filter_id' => $filter_id,
      'item_id' => $item_id,
    );
    $fields = array(
      'filter_id' => $filter_id,
      'item_id' => $item_id,
    );

    ItemRelationModel::mergeItemRelation($keys, $fields);
  }

  /**
   * Delete from relations table without filter conditions.
   *
   * @param int $item_id
   *   Item id.
   *
   * @throws \ServicesException
   */
  public static function deleteFromItemRelationsWithoutFilter(int $item_id) {
    $conditions = array(
      ['field' => 'item_id', 'value' => $item_id, 'operation' => '='],
    );

    ItemRelationModel::deleteItemRelation($conditions);
  }

  /**
   * Retrieve item data.
   *
   * @param int $itemId
   *   Item id.
   *
   * @return mixed
   *   Data.
   *
   * @throws \ServicesException
   */
  public static function retrieveItem(int $itemId) {
    $joins = array(
      [
        'table' => ItemRelationModel::$table,
        'alias' => ItemRelationModel::$alias,
        'condition' => 'mi.item_id = mir.item_id',
      ],
    );
    $fields = array(
      ['alias' => ItemModel::$alias, 'fields' => []],
      ['alias' => ItemRelationModel::$alias, 'fields' => []],
    );
    $conditions = array(
      ['field' => 'mi.item_id', 'value' => $itemId, 'operation' => '='],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return ItemModel::selectItem($joins, $fields, $conditions, [], $fetching);
  }

  /**
   * Get item fields.
   *
   * @param int $item_id
   *   Item id.
   * @param array $fields
   *   Array of the fields which must be selected.
   *
   * @return mixed
   *   Items field.
   *
   * @throws \ServicesException
   */
  public static function getItemFields(int $item_id, array $fields) {
    $fields = array(
      ['alias' => ItemModel::$alias, 'fields' => $fields],
    );
    $conditions = array(
      ['field' => 'mi.item_id', 'value' => $item_id, 'operation' => '='],
    );
    $fetching = array('fetch' => 'fetchAssoc', 'value' => '');

    return ItemModel::selectItem([], $fields, $conditions, [], $fetching);
  }

  /**
   * Get items data.
   *
   * @return mixed
   *   Array with data or FALSE.
   *
   * @throws \ServicesException
   */
  public static function getItems() {
    $fields = array(
      ['alias' => ItemModel::$alias, 'fields' => []],
    );
    $conditions = array(
      [
        'field' => 'mi.type',
        'value' => MoveInventoryItemType::CUSTOM,
        'operation' => '<>',
      ],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return ItemModel::selectItem([], $fields, $conditions, [], $fetching);
  }

  /**
   * Get existing filter for item id.
   *
   * @param int $itemId
   *   Item id.
   *
   * @return mixed
   *   Filters.
   *
   * @throws \ServicesException
   */
  public static function getExistingFilter(int $itemId) {
    $fields = array(
      ['alias' => ItemRelationModel::$alias, 'fields' => ['filter_id']],
    );
    $conditions = array(
      ['field' => 'item_id', 'value' => $itemId, 'operation' => '='],
    );
    $fetching = array('fetch' => 'fetchCol', 'value' => '');

    return ItemRelationModel::selectItemRelation([], $fields, $conditions, [], $fetching) ?? array();
  }

  /**
   * Get Items Relation data.
   *
   * @return mixed
   *   Array with data or FALSE.
   *
   * @throws \ServicesException
   */
  public static function getItemsRelations() {
    $fields = array(
      ['alias' => ItemRelationModel::$alias, 'fields' => []],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return ItemRelationModel::selectItemRelation([], $fields, [], [], $fetching);
  }

  /**
   * Get items.
   *
   * @param int $itemId
   *   Item id.
   * @param array $rooms
   *   Rooms id.
   *
   * @return array
   *   Result.
   *
   * @throws \ServicesException
   */
  public static function getItemRooms(int $itemId, array $rooms) : array {
    $joins = array(
      [
        'table' => FilterRelationModel::$table,
        'alias' => FilterRelationModel::$alias,
        'condition' => 'mfr.filter_id = mir.filter_id',
      ],
    );
    $fields = array(
      ['alias' => FilterRelationModel::$alias, 'fields' => ['room_id']],
    );
    $conditions = array(
      ['field' => 'mir.item_id', 'value' => $itemId, 'operation' => '='],
    );
    if (!empty($rooms)) {
      $conditions[] = [
        'field' => 'mfr.room_id',
        'value' => $rooms,
        'operation' => 'IN',
      ];
    }
    $expressions = array(
      ['expression' => 'groupBy', 'value' => 'mfr.room_id'],
      ['expression' => 'distinct', 'value' => 'mfr.room_id'],
    );
    $fetching = array('fetch' => 'fetchCol', 'value' => '');

    return ItemRelationModel::selectItemRelation($joins, $fields, $conditions, $expressions, $fetching) ?? [];
  }

}
