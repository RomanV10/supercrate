<?php

namespace Drupal\move_inventory\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class MoveInventoryEntity.
 *
 * @package Drupal\move_inventory\Util
 */
class MoveInventoryEntity extends BaseEnum {
  const ROOM = 0;
  const FILTER = 1;
  const ITEM = 2;

}
