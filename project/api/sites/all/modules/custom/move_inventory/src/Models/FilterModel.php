<?php

namespace Drupal\move_inventory\Models;

use Drupal\move_inventory\Util\enum\MoveInventoryEntity;

/**
 * Class FilterModel.
 *
 * @package Drupal\move_inventory\Models
 */
class FilterModel extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'mi_filter';

  /**
   * The table alias associated with the model.
   *
   * @var string
   */
  public static $alias = 'mf';

  /**
   * Insert entity.
   *
   * @param array $fields
   *   Fields.
   *
   * @return \DatabaseStatementInterface|int
   *   Id entity.
   *
   * @throws \Exception
   */
  public static function insertFilter(array $fields) {
    return self::insertEntity(self::$table, $fields);
  }

  /**
   * Delete entity.
   *
   * @param array $conditions
   *   Array conditions.
   *
   * @return int
   *   Count of deleted row.
   *
   * @throws \ServicesException
   */
  public static function deleteFilter(array $conditions) {
    return self::deleteEntity(self::$table, $conditions);
  }

  /**
   * Merge filter.
   *
   * @param array $keys
   *   Keys.
   * @param array $fields
   *   Fields.
   * @param array $expressions
   *   Expressions example isNull, count, avg etc.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeFilter(array $keys, array $fields, array $expressions = array()) {
    self::mergeEntity(self::$table, $keys, $fields, $expressions);
  }

  /**
   * Update entity.
   *
   * @param array $fields
   *   Fields.
   * @param array $conditions
   *   Conditions.
   *
   * @return \DatabaseStatementInterface
   *   Result.
   *
   * @throws \ServicesException
   */
  public static function updateFilter(array $fields = array(), array $conditions = array()) {
    return self::updateEntity(self::$table, $fields, $conditions);
  }

  /**
   * Select entity.
   *
   * @param array $joins
   *   Array join table.
   * @param array $fields
   *   Array fields.
   * @param array $conditions
   *   Array conditions.
   * @param array $expressions
   *   Array expressions.
   * @param array $fetching
   *   Get result.
   *
   * @return mixed
   *   Result select.
   *
   * @throws \ServicesException
   */
  public static function selectFilter(array $joins, array $fields, array $conditions, array $expressions, array $fetching) {
    return self::selectEntity(
      [
        'table' => self::$table,
        'alias' => self::$alias,
      ],
      $joins, $fields, $conditions, $expressions, $fetching);
  }

  /**
   * Select filters for conditions.
   *
   * @param array $conditions
   *   Conditions.
   *
   * @return mixed
   *   Filters.
   */
  public static function selectFiltersWithConditions(array $conditions) {
    $filters = ['room_id' => 0];

    $query = db_select('mi_filter_relations', 'mfr');
    $query->leftJoin('mi_filter', 'mf', 'mfr.filter_id = mf.filter_id');
    if (!empty($conditions)) {
      $filters = array_intersect_key($conditions, $filters);
      // Connect weight table.
      $query->leftJoin('mi_weight', 'mw', 'mfr.filter_id = mw.entity_id AND mw.entity_type = ' . MoveInventoryEntity::FILTER);
      $query->fields('mw', array('weight'));
      $query->orderBy('mw.weight', 'ASC');

      foreach ($filters as $field_name => $val) {
        if (!empty($val)) {
          $operator = is_array($val) ? 'IN' : '=';
          $query->condition('mfr.' . $field_name, $val, $operator);
          $query->condition('mw.' . $field_name, $val, $operator);
        }
      }
    }
    $query->fields('mfr');
    $query->fields('mf', array('name', 'type'));
    $query->groupBy('mfr.filter_id');
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return !empty($result) ? $result : [];
  }

}
