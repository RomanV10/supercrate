<?php

namespace Drupal\move_inventory\Actions;

use Drupal\move_inventory\Entity\Package;
use Drupal\move_services_new\Util\enum\EntityTypes;
use PDO;

/**
 * Class MoveInventoryPackage.
 *
 * @package Drupal\move_inventory\Services
 */
class PackageActions extends AbstractActions implements BaseCRUDInterface {
  private $id = NULL;
  private $pacRelId = NULL;

  /**
   * MoveInventoryPackage constructor.
   *
   * @param mixed $id
   *   Package id.
   * @param mixed $pac_rel_id
   *   Package relation.
   */
  public function __construct($id = NULL, $pac_rel_id = NULL) {
    $this->id = $id;
    $this->pacRelId;
  }

  /**
   * Set package id.
   *
   * @param int $id
   *   Package id.
   */
  public function setId(int $id) {
    $this->id = $id;
  }

  /**
   * Create package.
   *
   * @param array $data
   *   Data.
   *
   * @return array|mixed
   *   Result.
   */
  public function create(array $data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $package = array();
    try {
      $package = (array) $this->serializer->deserialize(json_encode($data), Package::class, 'json');
      $package['created'] = time();
      $inserted_id = db_insert('mi_package')
        ->fields($package)
        ->execute();
      $package['package_id'] = $inserted_id;
    }
    catch (\Throwable $e) {
      $message = "Create Inventory Package Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('inventory Package', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $package;
    }
  }

  /**
   * Retrieve package.
   *
   * @return array|mixed
   *   Package.
   */
  public function retrieve() {
    $error_flag = FALSE;
    $error_message = "";
    $package = array();
    try {
      $query = db_select('mi_package', 'mp');
      $query->leftJoin('mi_package_relations', 'mpr', 'mp.package_id = mpr.package_id');
      $query->fields('mp', array('name'))
        ->fields('mpr')
        ->condition('mp.package_id', $this->id);
      $result = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
      if (!empty($result)) {
        foreach ($result as $row) {
          $package['package_id'] = $row['package_id'];
          $package['name'] = $row['name'];
          $package['pac_rel_id'] = $row['pac_rel_id'];
          if (!isset($package['rooms'][$row['room_id']]) && !empty($row['room_id'])) {
            $room = new RoomActions($row['room_id']);
            $package['rooms'][$row['room_id']] = $room->retrieve();
          }

          if (!isset($package['rooms'][$row['room_id']]['filters'][$row['filter_id']]) && !empty($row['filter_id'])) {
            $filter = new FilterActions($row['filter_id']);
            $package['rooms'][$row['room_id']]['filters'][$row['filter_id']] = $filter->retrieve();
          }

          if (!isset($package['rooms'][$row['room_id']]['filters'][$row['filter_id']]['items'][$row['item_id']]) && !empty($row['item_id'])) {
            $item = new ItemActions($row['item_id']);
            $item_to_save = $item->retrieve();
            $item_to_save['count'] = $row['count'];
            $package['rooms'][$row['room_id']]['filters'][$row['filter_id']]['items'][$row['item_id']] = $item_to_save;
          }
        }
      }
      if (!empty($package)) {
        $this->resetPackageKeys($package, TRUE);
        $package = $package[0];
      }
    }
    catch (\Throwable $e) {
      $message = "Retrieve Inventory Package Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('inventory Package', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $package;
    }
  }

  /**
   * Update package.
   *
   * @param array $data
   *   Data.
   *
   * @return bool|\DatabaseStatementInterface|mixed
   *   Result.
   */
  public function update(array $data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      $package = (array) $this->serializer->deserialize(json_encode($data), Package::class, 'json');
      $result = db_update('mi_package')
        ->fields($package)
        ->condition('package_id', $this->id)
        ->execute();
    }
    catch (\Throwable $e) {
      $message = "Update Inventory Package Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('inventory Package', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Delete package.
   *
   * @return bool|mixed
   *   Result.
   */
  public function delete() {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      db_delete('mi_package')
        ->condition('package_id', $this->id)
        ->execute();

      db_delete('mi_package_relations')
        ->condition('package_id', $this->id)
        ->execute();

      $result = TRUE;
    }
    catch (\Throwable $e) {
      $message = "Delete Inventory Item Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('inventory item', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Index for package.
   *
   * @param int $current_page
   *   Current page.
   * @param int $per_page
   *   Count package on page.
   * @param array $conditions
   *   Conditions.
   * @param bool $total_only
   *   Total only.
   *
   * @return array|mixed
   *   Result.
   */
  public function index(int $current_page = 1, int $per_page = -1, array $conditions = array(), bool $total_only = FALSE) {
    $error_flag = FALSE;
    $error_message = "";
    $packages = array();
    try {
      $query = db_select('mi_package', 'mp');
      $query->leftJoin('mi_package_relations', 'mpr', 'mp.package_id = mpr.package_id');
      if (!empty($conditions)) {
        foreach ($conditions as $field_name => $val) {
          if (!empty($val)) {
            $operator = is_array($val) ? 'IN' : '=';
            $query->condition('mpr.' . $field_name, $val, $operator);
          }
        }
      }
      if ($total_only) {
        $query->leftJoin('mi_items', 'mii', 'mii.item_id = mpr.item_id');
        $query->addExpression('SUM(count)', 'total_count');
        $query->addExpression('SUM(mii.cf  * mpr.count)', 'total_cf');
        $packages = $query->execute()->fetchAssoc(PDO::FETCH_ASSOC);
      }
      else {
        $query->fields('mp', array('name', 'package_id'));
        $query->fields('mpr', array(
          'item_id',
          'pac_rel_id',
          'room_id',
          'filter_id',
          'count',
        ));
        $result = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($result)) {
          foreach ($result as $row) {
            $packages[$row['package_id']]['package_id'] = $row['package_id'];
            $packages[$row['package_id']]['name'] = $row['name'];

            if (!isset($packages[$row['package_id']]['rooms'][$row['room_id']]) && !empty($row['room_id'])) {
              $room = new RoomActions($row['room_id']);
              $packages[$row['package_id']]['rooms'][$row['room_id']] = $room->retrieve();
            }

            if (!isset($packages[$row['package_id']]['rooms'][$row['room_id']]['filters'][$row['filter_id']]) && !empty($row['filter_id'])) {
              $filter = new FilterActions($row['filter_id']);
              $packages[$row['package_id']]['rooms'][$row['room_id']]['filters'][$row['filter_id']] = $filter->retrieve();
            }

            if (!isset($packages[$row['package_id']]['rooms'][$row['room_id']]['filters'][$row['filter_id']]['items'][$row['item_id']]) && !empty($row['item_id'])) {
              $item = new ItemActions($row['item_id']);
              $packages[$row['package_id']]['rooms'][$row['room_id']]['filters'][$row['filter_id']]['items'][$row['item_id']] = $item->retrieve();
            }
          }
        }
        $this->resetPackageKeys($packages);
      }
    }
    catch (\Throwable $e) {
      $message = "Index Inventory Package Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('inventory Package', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $packages;
    }
  }

  /**
   * Get package.
   *
   * @param bool $with_filters
   *   With filters.
   * @param bool $with_total_items
   *   With totals items.
   * @param int $entity_id
   *   Entity id.
   * @param int $entity_type
   *   Entity type.
   *
   * @return mixed
   *   Result.
   */
  public function getPackage(bool $with_filters = FALSE, bool $with_total_items = FALSE, int $entity_id = 0, int $entity_type = EntityTypes::MOVEREQUEST) {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    try {
      $query = db_select('mi_package', 'mp');
      $query->leftJoin('mi_package_relations', 'mpr', 'mp.package_id = mpr.package_id');
      $query->fields('mp', array('name', 'package_id'));
      $query->fields('mpr', array(
        'item_id',
        'pac_rel_id',
        'room_id',
        'filter_id',
        'count',
      ));
      $query->condition('mpr.package_id', $this->id);
      $query_result = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
      if (($with_filters || $with_total_items) && !empty($query_result)) {
        foreach ($query_result as $key => $row) {
          $package_id = $row['package_id'];
          $room_id = $row['room_id'];
          $filter_id = $row['filter_id'];
          $item_id = $row['item_id'];

          $result[$package_id]['package_id'] = $row['package_id'];
          $result[$package_id]['name'] = $row['name'];
          if (!isset($result[$package_id]['rooms'][$room_id]) && !empty($room_id)) {
            $room = new RoomActions($room_id);
            $result[$package_id]['rooms'][$room_id] = $room->retrieve();
            $result[$package_id]['rooms'][$room_id]['total_cf'] = 0;
            $result[$package_id]['rooms'][$room_id]['total_count'] = 0;
          }

          if (!isset($result[$package_id]['rooms'][$room_id]['filters'][$filter_id]) && !empty($row['filter_id'])) {
            $filter = new FilterActions($filter_id);
            $result[$package_id]['rooms'][$room_id]['filters'][$filter_id] = $filter->retrieve();
            $result[$package_id]['rooms'][$room_id]['filters'][$filter_id]['total_cf'] = 0;
            $result[$package_id]['rooms'][$room_id]['filters'][$filter_id]['total_count'] = 0;
          }

          if (!isset($result[$package_id]['rooms'][$room_id]['filters'][$filter_id]['items'][$item_id]) && !empty($item_id)) {
            $item_inst = new ItemActions($item_id);
            $item = $item_inst->retrieve();

            $item['count'] = $row['count'];
            $item['total_cf'] = (float) $item['count'] * (float) $item['cf'];

            $result[$package_id]['rooms'][$room_id]['filters'][$filter_id]['total_cf'] += $item['total_cf'];
            $result[$package_id]['rooms'][$room_id]['filters'][$filter_id]['total_count'] += $item['count'];

            $result[$package_id]['rooms'][$room_id]['total_cf'] += $item['total_cf'];
            $result[$package_id]['rooms'][$room_id]['total_count'] += $item['count'];

            $result[$package_id]['rooms'][$room_id]['filters'][$filter_id]['items'][$item_id] = $item;
          }
        }
      }
      // Reset array keys for front and count items.
      if (!empty($result)) {
        $result = array_values($result);
        foreach ($result as $key => $val) {
          $result[$key]['rooms'] = array_values($result[$key]['rooms']);
          foreach ($result[$key]['rooms'] as $room_key => $room_val) {
            $result[$key]['rooms'][$room_key]['filters'] = array_values($result[$key]['rooms'][$room_key]['filters']);
            foreach ($result[$key]['rooms'][$room_key]['filters'] as $filter_key => $filter_val) {
              $result[$key]['rooms'][$room_key]['filters'][$filter_key]['items'] = array_values($result[$key]['rooms'][$room_key]['filters'][$filter_key]['items']);
            }
          }
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Index Inventory Room Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('inventory room', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result[0]['rooms'];
    }
  }

  /**
   * Get package items.
   *
   * @param array $conditions
   *   Conditions.
   *
   * @return mixed
   *   Result.
   */
  public static function getPackageItems(array $conditions = array()) {

    $query = db_select('mi_package', 'mp');
    $query->leftJoin('mi_package_relations', 'mpr', 'mp.package_id = mpr.package_id');
    $query->leftJoin('mi_items', 'mi', 'mpr.item_id = mi.item_id');
    $query->fields('mpr');
    $query->fields('mi');
    foreach ($conditions as $field_name => $val) {
      $query->condition('mpr.' . $field_name, $val);
    }
    $result = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
    if (!empty($result)) {
      foreach ($result as $key => $val) {
        $val['id'] = $val['pac_rel_id'];
        $result[$key] = $val;
        $result[$key]['totals']['total_count'] = $val['count'];
        $result[$key]['totals']['total_cf'] = $val['count'] * $val['cf'];
      }
    }
    return $result;
  }

  /**
   * Add package to request.
   *
   * @param int $entity_id
   *   Entity id.
   * @param int $entity_type
   *   Entity type.
   *
   * @return bool
   *   Result.
   */
  public function addPackageToRequest(int $entity_id, int $entity_type = EntityTypes::MOVEREQUEST) {
    $items_info = [];
    $package_items = db_select('mi_package_relations', 'mpr')
      ->fields('mpr')
      ->condition('package_id', $this->id)
      ->execute()
      ->fetchAll(PDO::FETCH_ASSOC);
    if (!empty($package_items)) {
      $item_inst = new ItemActions();
      foreach ($package_items as $row) {
        // Get item info only once. And add it to the temp array.
        if (!isset($items_info[$row['item_id']])) {
          $item_inst->setId($row['item_id']);
          $items_info[$row['item_id']] = $item_inst->retrieve();
        }
        $row['price'] = (float) $items_info[$row['item_id']]['price'];
        $row['cf'] = (float) $items_info[$row['item_id']]['cf'];
        $row['entity_id'] = $entity_id;
        $row['entity_type'] = $entity_type;
        $row['name'] = $items_info[$row['item_id']]['name'];
        db_merge('mi_request_inventory')
          ->key(array(
            'entity_id' => $row['entity_id'],
            'entity_type' => $row['entity_type'],
            'item_id' => $row['item_id'],
            'room_id' => $row['room_id'],
            'filter_id' => $row['filter_id'],
          )
          )
          ->fields($row)
          ->expression('count', 'count + :count', array(':count' => $row['count']))
          ->execute();
      }
    }
    return TRUE;
  }

  /**
   * Remove package items from request.
   *
   * @param int $entity_id
   *   Request id.
   * @param int $entity_type
   *   Entity type. Default is Request.
   *
   * @return bool
   *   Result.
   */
  public function removePackageFromRequest(int $entity_id, int $entity_type = EntityTypes::MOVEREQUEST) {
    $package_items = db_select('mi_package_relations', 'mpr')
      ->fields('mpr')
      ->condition('package_id', $this->id)
      ->execute()
      ->fetchAll(PDO::FETCH_ASSOC);
    if (!empty($package_items)) {
      foreach ($package_items as $item) {
        $package_request_item_values = db_select('mi_request_inventory', 'mri')
          ->fields('mri', array('count', 'cf'))
          ->condition('pac_rel_id', $item['pac_rel_id'])
          ->condition('entity_id', $entity_id)
          ->condition('entity_type', $entity_type)
          ->execute()
          ->fetchAssoc(PDO::FETCH_ASSOC);
        if (!empty($package_request_item_values)) {
          if ($package_request_item_values['count'] == $item['count']) {
            db_delete('mi_request_inventory')
              ->condition('pac_rel_id', $item['pac_rel_id'])
              ->execute();
          }

          if ($package_request_item_values['count'] > $item['count']) {
            $new_count = $package_request_item_values['count'] - $item['count'];
            db_update('mi_request_inventory')
              ->fields(array(
                'count' => $new_count,
                'package_id' => 0,
                'pac_rel_id' => 0,
              ))
              ->condition('pac_rel_id', $item['pac_rel_id'])
              ->condition('entity_id', $entity_id)
              ->condition('entity_type', $entity_type)
              ->execute();
          }
        }

      }
    }
    return TRUE;
  }

  /**
   * Remove only one package item from request.
   *
   * @param mixed $pac_rel_id
   *   Package relation id.
   * @param array $package_item
   *   Package item.
   * @param mixed $entity_id
   *   Entity id.
   * @param int $entity_type
   *   Entity type.
   *
   * @return array
   *   Result.
   */
  public static function removeOnePackageItemFromRequest($pac_rel_id = NULL, array $package_item = array(), $entity_id = FALSE, int $entity_type = EntityTypes::MOVEREQUEST) {
    $result = array();
    if (!empty($pac_rel_id)) {
      $package_item = db_select('mi_package_relations', 'mpr')
        ->fields('mpr')
        ->condition('pac_rel_id', $pac_rel_id)
        ->execute()
        ->fecthAssoc(PDO::FETCH_ASSOC);
    }
    if (!empty($package_item)) {
      $packages_in_request = db_select('mi_request_inventory', 'mri')
        ->fields('mri')
        ->condition('package_id', $package_item['package_id'])
        ->condition('pac_rel_id', $package_item['pac_rel_id'])
        ->condition('entity_id', $entity_id)
        ->condition('entity_type', $entity_type)
        ->execute()
        ->fetchAll(PDO::FETCH_ASSOC);
      if (!empty($packages_in_request)) {
        foreach ($packages_in_request as $one_package_item_in_request) {
          // Remove count of items from request inventory.
          // And remove ids of package from request inventory.
          $updated_count_items = (int) $one_package_item_in_request['count'] - (int) $package_item['count'];
          // If count items less then zero set it to zero.
          // There is can't be -1 item.
          $updated_count_items = ($updated_count_items > 0) ? $updated_count_items : 0;
          db_update('mi_request_inventory')
            ->fields(array(
              'count' => $updated_count_items,
              'pac_rel_id' => 0,
              'package_id' => 0,
            ))
            ->execute();
          $result[] = $one_package_item_in_request['item_id'];
        }
      }
    }

    return $result;
  }

  /**
   * Remove item.
   *
   * @param int $pac_rel_id
   *   Package relation id.
   *
   * @return bool
   *   Result.
   */
  public static function removeItemFromPackage(int $pac_rel_id) {
    db_delete('mi_package_relations')
      ->condition('pac_rel_id', $pac_rel_id)
      ->execute();
    return TRUE;
  }

  /**
   * Reset package keys.
   *
   * @param mixed $package
   *   Package.
   * @param bool $one
   *   One.
   */
  public static function resetPackageKeys(&$package, $one = FALSE) {
    if ($one) {
      $package = array(0 => $package);
    }
    if (!empty($package)) {
      foreach ($package as $key => $rooms) {
        $package[$key]['rooms'] = array_values($package[$key]['rooms']);
        foreach ($package[$key]['rooms'] as $key_r => $filters) {
          $package[$key]['rooms'][$key_r]['filters'] = array_values($package[$key]['rooms'][$key_r]['filters']);
          foreach ($package[$key]['rooms'][$key_r]['filters'] as $f_key => $item) {
            $package[$key]['rooms'][$key_r]['filters'][$f_key]['items'] = array_values($package[$key]['rooms'][$key_r]['filters'][$f_key]['items']);
          }
        }
      }
      $package = array_values($package);
    }
  }

}
