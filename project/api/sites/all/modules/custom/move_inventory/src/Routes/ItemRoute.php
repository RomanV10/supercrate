<?php

namespace Drupal\move_inventory\Routes;

use Drupal\move_inventory\Controllers\ItemController;
use Drupal\move_inventory\ValidationRules\GetInventoryRequest;
use Drupal\move_inventory\ValidationRules\RetrieveInventoryItem;

/**
 * Class ItemRoute.
 *
 * @package Drupal\move_inventory\Routes
 */
class ItemRoute {

  /**
   * Item routes.
   *
   * @return array
   *   Definition.
   */
  public static function getRoutes() {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_inventory',
      'name' => 'src/Controllers/ItemController',
    ];

    return array(
      'move_inventory_item' => array(
        'operations' => array(
          'create' => array(
            'callback' => ItemController::class . '::createInventoryItem',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => ItemController::class . '::retrieveInventoryItem',
            'rules' => RetrieveInventoryItem::class,
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'callback' => ItemController::class . '::updateInventoryItem',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete' => array(
            'callback' => ItemController::class . '::deleteInventoryItem',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'update_item_in_request' => array(
            'callback' => ItemController::class . '::updateItemInRequest',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'add_item_to_package' => array(
            'callback' => ItemController::class . '::addItemToPackage',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_custom_item_in_request' => array(
            'callback' => ItemController::class . '::updateCustomItemInRequest',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'sorting' => array(
            'callback' => ItemController::class . '::itemSorting',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_items' => array(
            'callback' => ItemController::class . '::indexInventoryItem',
            'file' => $controller_file,
            'args' => array(
             array(
               'name' => 'conditions',
               'type' => 'array',
               'source' => array('data' => 'conditions'),
               'optional' => TRUE,
               'default value' => array(),
             ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_custom_items' => array(
            'callback' => ItemController::class . '::getInventoryCustomItems',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'conditions',
                'type' => 'array',
                'source' => array('data' => 'conditions'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete_custom_items' => array(
            'callback' => ItemController::class . '::deleteInventoryCustomItems',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'optional' => FALSE,
                'type' => 'int',
                'source' => array('data' => 'id'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete_item_from_request' => array(
            'callback' => ItemController::class . '::deleteItemFromRequest',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'optional' => FALSE,
                'type' => 'int',
                'source' => array('data' => 'id'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'search_item' => array(
            'callback' => ItemController::class . '::searchItem',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'conditions',
                'optional' => TRUE,
                'type' => 'array',
                'source' => array('data' => 'conditions'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'search_item_settings' => array(
            'callback' => ItemController::class . '::searchItemInSettings',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_inventory_request' => array(
            'callback' => ItemController::class . '::getInventoryRequest',
            'rules' => GetInventoryRequest::class,
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id',
                'optional' => FALSE,
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
              ),
              array(
                'name' => 'conditions',
                'type' => 'array',
                'source' => array('data' => 'conditions'),
                'optional' => TRUE,
                'default value' => array(),

              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

}
