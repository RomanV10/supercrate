<?php

namespace Drupal\move_inventory\Routes;

use Drupal\move_inventory\Controllers\FilterController;

/**
 * Class FilterRoute.
 *
 * @package Drupal\move_inventory\Routes
 */
class FilterRoute {

  /**
   * Filter routes.
   *
   * @return array
   *   Definition.
   */
  public static function getRoutes() {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_inventory',
      'name' => 'src/Controllers/FilterController',
    ];

    return array(
      'move_inventory_filter' => array(
        'operations' => array(
          'create' => array(
            'callback' => FilterController::class . '::createInventoryFilter',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => FilterController::class . '::retrieveInventoryFilter',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'callback' => FilterController::class . '::updateInventoryFilter',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete' => array(
            'callback' => FilterController::class . '::deleteInventoryFilter',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'index' => array(
            'callback' => FilterController::class . '::indexInventoryFilter',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'conditions',
                'type' => 'array',
                'source' => array('data' => 'conditions'),
                'optional' => TRUE,
                'default value' => array(),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'sorting' => array(
            'callback' => FilterController::class . '::filterSorting',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_filters' => array(
            'callback' => FilterController::class . '::indexInventoryFilter',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'conditions',
                'type' => 'array',
                'source' => array('data' => 'conditions'),
                'optional' => TRUE,
                'default value' => array(),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

}
