<?php

namespace Drupal\move_inventory\Routes;

use Drupal\move_inventory\Controllers\FileController;

/**
 * Class FileRoute.
 *
 * @package Drupal\move_inventory\Routes
 */
class FileRoute {

  /**
   * File routes.
   *
   * @return array
   *   Definition.
   */
  public static function getRoutes() {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_inventory',
      'name' => 'src/Controllers/FileController',
    ];

    return array(
      'file_inventory' => array(
        'operations' => array(
          'create' => array(
            'file' => $controller_file,
            'help' => 'Create a file with base64 encoded data',
            'callback' => FileController::class . '::createFile',
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

}
