<?php

namespace Drupal\move_inventory\Entity;

/**
 * Class Item.
 *
 * @package Drupal\move_inventory\Entity
 */
class Item {

  /**
   * Item name.
   *
   * @var string
   */
  public $name = '';

  /**
   * Image link.
   *
   * @var string
   */
  public $image_link = '';

  /**
   * Filters array.
   *
   * @var array
   */
  public $filters = array();

  /**
   * Price.
   *
   * @var int
   */
  public $price = 0;

  /**
   * Cubic feat.
   *
   * @var int
   */
  public $cf = 0;

  /**
   * Status.
   *
   * @var int
   */
  public $status = 0;

  /**
   * Type.
   *
   * @var int
   */
  public $type = 0;

  /**
   * Handle fee.
   *
   * @var int
   */
  public $handle_fee = 0;

  /**
   * Packing fee.
   *
   * @var int
   */
  public $packing_fee = 0;

  /**
   * Extra service.
   *
   * @var float
   */
  public $extra_service = 0;

  /**
   * Room id.
   *
   * @var int
   */
  public $room_id = 0;

  /**
   * Count.
   *
   * @var int
   */
  public $count = 0;

  /**
   * Entity id.
   *
   * @var int
   */
  public $entity_id = 0;

  /**
   * Entity type.
   *
   * @var int
   */
  public $entity_type = 0;

  /**
   * Entity type.
   *
   * @var string
   */
  public $extra_service_name = '';

  /**
   * Inventory type.
   *
   * @var int
   */
  public $type_inventory = 0;

  /**
   * Get inventory type.
   *
   * @return int
   *   Inventory type.
   */
  public function getInventoryType(): int {
    return $this->type_inventory;
  }

  /**
   * Set inventory type.
   *
   * @param int $inventory_type
   *   Inventory type.
   */
  public function setInventoryType(int $inventory_type): void {
    $this->type_inventory = $inventory_type;
  }

  /**
   * Get name.
   *
   * @return string
   *   Name.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Set name.
   *
   * @param string $name
   *   Name.
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * Get image link.
   *
   * @return string
   *   image link.
   */
  public function getImageLink(): string {
    return $this->image_link;
  }

  /**
   * Set image link.
   *
   * @param string $image_link
   *   Image link.
   */
  public function setImageLink(string $image_link) {
    $this->image_link = $image_link;
  }

  /**
   * Get filters.
   *
   * @return array
   *   Filters ids.
   */
  public function getFiltersIds(): array {
    return $this->filters;
  }

  /**
   * Set filters ids.
   *
   * @param array $filters
   *   Filters ids.
   */
  public function setFiltersIds(array $filters) {
    $this->filters = $filters;
  }

  /**
   * Get price.
   *
   * @return int
   *   Price.
   */
  public function getPrice(): int {
    return $this->price;
  }

  /**
   * Set price.
   *
   * @param int $price
   *   Price.
   */
  public function setPrice(int $price) {
    $this->price = $price;
  }

  /**
   * Get cf.
   *
   * @return float
   *   Cubic feet.
   */
  public function getCf(): float {
    return $this->cf;
  }

  /**
   * Set cf.
   *
   * @param float $cf
   *   Cubic feet.
   */
  public function setCf(float $cf) {
    $this->cf = $cf;
  }

  /**
   * Set extra service.
   *
   * @param float $extra_service
   *   Extra service.
   */
  public function setExtraService(float $extra_service) {
    $this->extra_service = $extra_service;
  }

  /**
   * Set status.
   *
   * @param int $status
   *   Status.
   */
  public function setStatus(int $status) {
    $this->status = $status;
  }

  /**
   * Get status.
   *
   * @return int
   *   Status.
   */
  public function getStatus(): int {
    return $this->status;
  }

  /**
   * Get extra service name.
   *
   * @return string
   *   Extra ser. name.
   */
  public function getExtraServiceName() : string {
    return $this->extra_service_name;
  }

  /**
   * Get type.
   *
   * @return int
   *   Type.
   */
  public function getType(): int {
    return $this->type;
  }

  /**
   * Set type.
   *
   * @param int $type
   *   Type.
   */
  public function setType(int $type) {
    $this->type = $type;
  }

  /**
   * Get handle fee.
   *
   * @return int
   *   Hadle fee.
   */
  public function getHandleFee(): int {
    return $this->handle_fee;
  }

  /**
   * Set hadle fee.
   *
   * @param int $handle_fee
   *   Handle fee.
   */
  public function setHandleFee(int $handle_fee) {
    $this->handle_fee = $handle_fee;
  }

  /**
   * Get packing fee.
   *
   * @return int
   *   Packing fee.
   */
  public function getPackingFee(): int {
    return $this->packing_fee;
  }

  /**
   * Set packing fee.
   *
   * @param int $packing_fee
   *   Packing fee.
   */
  public function setPackingFee(int $packing_fee) {
    $this->packing_fee = $packing_fee;
  }

  /**
   * Get extra service.
   *
   * @return float
   *   Extra service.
   */
  public function getExtraService(): float {
    return $this->extra_service;
  }

  /**
   * Get room id.
   *
   * @return int
   *   Room id.
   */
  public function getRoomId(): int {
    return $this->room_id;
  }

  /**
   * Set room id.
   *
   * @param int $room_id
   *   Room id.
   */
  public function setRoomId(int $room_id) {
    $this->room_id = $room_id;
  }

  /**
   * Get count.
   *
   * @return int
   *   Count.
   */
  public function getCount(): int {
    return $this->count;
  }

  /**
   * Set count.
   *
   * @param int $count
   *   Count.
   */
  public function setCount(int $count) {
    $this->count = $count;
  }

  /**
   * Get entity id.
   *
   * @return int
   *   Entity id.
   */
  public function getEntityId(): int {
    return $this->entity_id;
  }

  /**
   * Set entity id.
   *
   * @param int $entity_id
   *   Entity id.
   */
  public function setEntityId(int $entity_id) {
    $this->entity_id = $entity_id;
  }

  /**
   * Get entity type.
   *
   * @return int
   *   Entity type.
   */
  public function getEntityType(): int {
    return $this->entity_type;
  }

  /**
   * Set entity type.
   *
   * @param int $entity_type
   *   Entity type.
   */
  public function setEntityType(int $entity_type) {
    $this->entity_type = $entity_type;
  }

  /**
   * Set extra service name.
   *
   * @param string $extra_service_name
   *   Extra service.
   */
  public function setExtraServiceName(string $extra_service_name) {
    $this->extra_service_name = (string) $extra_service_name;
  }

}
