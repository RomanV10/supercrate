<?php

namespace Drupal\move_inventory\Controllers;

use Drupal\move_inventory\Actions\FileActions;

/**
 * Class FileController.
 *
 * @package Drupal\move_inventory\Controllers
 */
class FileController extends AbstractController {

  /**
   * Create file.
   *
   * @return array
   *   File.
   *
   * @throws \Exception
   */
  public static function createFile() {
    return self::run(FileActions::class, 'create', [[]]);
  }

}
