<?php

namespace Drupal\move_inventory\Actions\SubActions;

use Drupal\move_inventory\Tasks\FileTasks;

/**
 * Class FileSubActions.
 *
 * @package Drupal\move_inventory\Actions\SubActions
 */
class FileSubActions extends AbstractSubActions implements SubActionsInterface {
  private $allowExtImage = array('jpg', 'png', 'jpeg', 'gif', 'tiff');
  private $sizeLimit = 1000000;

  /**
   * Create service.
   *
   * @param array $data
   *   Data for create entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function create(array $data = array()) {
    $result = array();

    if (!empty($_FILES['file'])) {
      // We need this to make drupal api work.
      foreach ($_FILES['file'] as $key => $val) {
        if ($key == 'name') {
          $val = FileTasks::servicesFileCheckName($val);
        }

        if ($key == 'size' && $val > $this->sizeLimit) {
          return services_error('File size not allowed more then 1MB.', 406);
        }

        if ($key == 'type' && !FileTasks::checkImageExt($val, $this->allowExtImage)) {
          return services_error('File type not allowed.', 406);
        }
        $_FILES['files'][$key]['inventory'] = $val;
      }
      if (empty($result['error'])) {
        $file_dir = 'public://inventory';
        file_prepare_directory($file_dir, FILE_CREATE_DIRECTORY);
        $file = file_save_upload('inventory', array(), $file_dir, FILE_EXISTS_RENAME);
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        if (!empty($file->uri)) {
          $image_style = image_style_load($this->imagePreset);
          $image_uri = $file->uri;
          $preset_uri = image_style_path($this->imagePreset, $image_uri);
          $preset_created = image_style_create_derivative($image_style, $image_uri, $preset_uri);
          $url = ($preset_created) ? file_create_url($preset_uri) : file_create_url($file->uri);

          $result['uri'] = $file->uri;
          $result['url'] = $url;
        }
      }
    }

    return $result;
  }

  /**
   * Retrieve service.
   *
   * @return mixed
   *   Result of operation.
   */
  public function retrieve() {
    return NULL;
  }

  /**
   * Service for update entity.
   *
   * @param array $data
   *   Data for update entity.
   *
   * @return mixed
   *   Result of operation.
   */
  public function update(array $data = array()) {
    return NULL;
  }

  /**
   * Service for remove entity.
   *
   * @return mixed
   *   Result of operation.
   */
  public function delete() {
    return NULL;
  }

}
