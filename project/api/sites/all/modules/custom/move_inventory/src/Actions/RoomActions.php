<?php

namespace Drupal\move_inventory\Actions;

use Drupal\move_inventory\Actions\SubActions\FilterSubActions;
use Drupal\move_inventory\Actions\SubActions\RoomSubActions;
use Drupal\move_inventory\Configs\Config;
use Drupal\move_inventory\Models\RequestInventoryModel;
use Drupal\move_inventory\Tasks\InventoryTasks;
use Drupal\move_inventory\Tasks\RoomTasks;
use Drupal\move_inventory\Util\enum\MoveInventoryItemType;
use Drupal\move_inventory\Util\enum\MoveInventoryRoomType;

/**
 * Class MoveInventoryRoom.
 *
 * @package Drupal\move_inventory\Services
 */
class RoomActions extends AbstractActions implements BaseCRUDInterface {

  /**
   * Create default room.
   *
   * @param array $data
   *   Room data.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function create(array $data = array()) {
    return (new RoomSubActions())->create($data);
  }

  /**
   * Retrieve room.
   *
   * @return bool|mixed
   *   Rooms.
   *
   * @throws \Exception
   */
  public function retrieve() {
    return NULL;
  }

  /**
   * Update room.
   *
   * @param array $data
   *   Data.
   *
   * @return bool|\DatabaseStatementInterface|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function update(array $data = array()) {
    $roomSubActions = new RoomSubActions();

    $roomSubActions->setId($this->getId());

    return $roomSubActions->update($data);
  }

  /**
   * Delete room.
   *
   * @return bool|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function delete() {
    $roomSubActions = new RoomSubActions();

    $roomSubActions->setId($this->getId());

    return $roomSubActions->delete();
  }

  /**
   * Index for rooms.
   *
   * @param null|array $conditions
   *   Int $entity_id
   *   Int $inventory_type.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function getRooms(?array $conditions) {
    if (!empty($conditions['entity_id'])) {
      return $this->getRoomsForRequest($conditions);
    }
    else {
      return $this->getRoomsForSettings();
    }
  }

  /**
   * Get rooms with filters for request modal.
   *
   * @param array $conditions
   *   Int $entity_id
   *   Int $inventory_type.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function getRoomsForRequest(array $conditions) {
    $filterSubActions = new FilterSubActions();

    $conditions['entity_id'] = $filterSubActions->getEntityIdStoragePacking($conditions['entity_id']);
    $this->setEntityId($conditions['entity_id']);
    $rooms = array_merge(RoomTasks::getDefaultRoomWithWeight($this->getEntityId()), RoomTasks::getCustomRoom($this->getEntityId()));
    if (!empty($rooms)) {
      $roomsId = RoomTasks::getRoomsId($rooms);
      foreach ($rooms as $key => $room) {
        $new_condition['room_id'] = $room['room_id'];
        $new_condition['type_inventory'] = $conditions['type_inventory'] ?? Config::getConfig()->getDefaultInventoryType();
        $new_condition['entity_id'] = $this->getEntityId();

        $rooms[$key]['uri'] = $room['image_link'];
        $rooms[$key]['image_link'] = $filterSubActions->showPresetUrl($room['image_link']);

        $new_condition['rooms_in_request'] = $roomsId;
        $rooms[$key]['filters'] = $filterSubActions->index($new_condition);
        unset($new_condition['rooms_in_request']);

        // Default item.
        $this->totalDefaultItem($key, $rooms, $new_condition);
        // Boxes item.
        $this->totalBoxesItem($key, $rooms, $new_condition);
        // Custom item.
        $this->totalCustomItem($key, $rooms, $new_condition);
      }
    }

    return $rooms;
  }

  /**
   * Get rooms with filters for settings/inventory.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  private function getRoomsForSettings() {
    $filterSubActions = new FilterSubActions();

    $rooms = RoomTasks::getRoomForSettings();
    if (!empty($rooms)) {
      $roomsId = RoomTasks::getRoomsId($rooms);
      foreach ($rooms as $key => $row) {
        if (!empty($row['image_link'])) {
          $rooms[$key]['uri'] = $row['image_link'];
          $rooms[$key]['image_link'] = $filterSubActions->showPresetUrl($row['image_link']);
        }

        $filter_condition['room_id'] = $row['room_id'];
        $filter_condition['rooms_in_request'] = $roomsId;
        $rooms[$key]['filters'] = $filterSubActions->index($filter_condition);
      }
    }

    return $rooms;
  }

  /**
   * Update weight.
   *
   * @param mixed $data
   *   Data.
   * @param int $entity_type
   *   Entity type.
   *
   * @throws \Exception
   */
  public function updateWeight($data, int $entity_type) {
    foreach ($data as $val) {
      $fields['entity_id'] = $val['id'];
      $fields['entity_type'] = $entity_type;
      RoomTasks::mergeWeightForRooms($fields, $val['weight']);
    }
  }

  /**
   * Calculate total for default item.
   *
   * @param int $key
   *   Key.
   * @param array $rooms
   *   Result array.
   * @param array $item_condition
   *   Conditions such as room_id and type_inventory.
   */
  private function totalDefaultItem(int $key, array &$rooms, array $item_condition) {
    $item_condition['type'] = MoveInventoryItemType::USUAL;
    $total = RequestInventoryModel::getEntityInventoryTotals($item_condition);
    if (!empty($total['total_count'])) {
      $rooms[$key] += $total;
    }
  }

  /**
   * Calculate total for boxes item.
   *
   * @param int $key
   *   Key.
   * @param array $rooms
   *   Result array.
   * @param array $item_condition
   *   Conditions such as room_id and type_inventory.
   */
  private function totalBoxesItem(int $key, array &$rooms, array $item_condition) {
    $item_condition['type'] = MoveInventoryItemType::BOXES;
    $total = RequestInventoryModel::getEntityInventoryTotals($item_condition);
    if (!empty($total['total_count'])) {
      // Initialization index: total_count, boxes, total_cf.
      isset($rooms[$key]['total_count']) ? $rooms[$key]['total_count'] : $rooms[$key]['total_count'] = 0;
      isset($rooms[$key]['boxes']) ? $rooms[$key]['boxes'] : $rooms[$key]['boxes'] = 0;
      isset($rooms[$key]['total_cf']) ? $rooms[$key]['total_cf'] : $rooms[$key]['total_cf'] = 0;
      $rooms[$key]['total_count'] += $total['total_count'] ?? 0;
      $rooms[$key]['boxes'] += $total['total_count'] ?? 0;
      $rooms[$key]['total_cf'] += $total['total_cf'] ?? 0;
    }
  }

  /**
   * Calculate total for custom item.
   *
   * @param int $key
   *   Key.
   * @param array $rooms
   *   Result array.
   * @param array $item_condition
   *   Conditions such as room_id and type_inventory.
   */
  private function totalCustomItem(int $key, array &$rooms, array $item_condition) {
    $item_condition['type'] = MoveInventoryItemType::CUSTOM;
    $total_custom_item = RequestInventoryModel::getCustomItemTotals($item_condition);
    if (!empty($total_custom_item['total_count'])) {
      $rooms[$key]['total_custom_count'] = $total_custom_item['total_count'] ?? 0;
      $rooms[$key]['total_custom_cf'] = $total_custom_item['total_cf'] ?? 0;
    }
  }

  /**
   * Clone rooms form request.
   *
   * @param int $old_nid
   *   From nid.
   * @param int $new_nid
   *   To nid.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function cloneRoomToOtherRequest(int $old_nid, int $new_nid) {
    $filterSubAction = new FilterSubActions();

    $result = array();
    $old_nid = $filterSubAction->getEntityIdStoragePacking($old_nid);

    $rooms = RoomTasks::getRoomForCloneRequest($old_nid);
    foreach ($rooms as $room) {
      // For Bedroom and All room.
      if ($room['type'] == MoveInventoryRoomType::DEFAULT) {
        $room_id_new = $room['room_id'];
      }
      else {
        $room_id_new = $this->create($room)['room_id'];
        $result[] = array('old_room' => $room['room_id'], 'new_room' => $room_id_new);
      }
      RoomTasks::insertRoomRelation($new_nid, $room_id_new);
      InventoryTasks::insertRoomWeightForRequest($room_id_new, $new_nid, $room['weight']);
      $filters = InventoryTasks::getFilterIdsForRoom($room['room_id'], TRUE);
      $filterSubAction->copyFiltersForRoom($filters, $room_id_new, $room['type']);
    }

    return $result;
  }

}
