<?php

namespace Drupal\move_inventory\Controllers;

use Drupal\move_inventory\Actions\InventoryActions;
use Drupal\move_services_new\Services\move_request\MoveRequest;

/**
 * Class InventoryController.
 *
 * @package Drupal\move_inventory\Controllers
 */
class InventoryController extends AbstractController {

  /**
   * Check if exist additional inventory for request.
   *
   * @param int $entity_id
   *   Entity id.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function checkExistAddInventory(int $entity_id) {
    $moveInventory = new InventoryActions();
    $moveInventory->setEntityId($entity_id);

    return self::run($moveInventory, 'checkExistAddInventory', []);
  }

  /**
   * Change type inventory.
   *
   * @param array $data
   *   Entity id.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function removeUnnecessaryItems(array $data) {
    return self::run(InventoryActions::class, 'removeUnnecessary', [$data]);
  }

  /**
   * Check if change move size.
   *
   * @param int $entity_id
   *   Entity id.
   * @param int $selected
   *   Parameter selected.
   *
   * @return mixed
   *   Custom answer.
   *
   * @throws \Exception
   */
  public static function checkIfChangeMoveSize(int $entity_id, int $selected) {
    $moveInventory = new InventoryActions();
    $moveInventory->setEntityId($entity_id);

    return self::run($moveInventory, 'checkChangeMoveSize', [$selected]);
  }

  /**
   * Create custom room.
   *
   * @param int $entity_id
   *   Entity id.
   * @param mixed $conditions
   *   Conditions.
   *
   * @return mixed
   *   Room.
   *
   * @throws \Exception
   */
  public static function createCustomRoom($entity_id, $conditions) {
    $moveInventory = new InventoryActions();
    $moveInventory->setEntityId($entity_id);

    return self::run($moveInventory, 'createCustomRoom', [$conditions]);
  }

  /**
   * Delete custom room.
   *
   * @param int $entity_id
   *   Entity id.
   * @param int $room_id
   *   Room id.
   *
   * @return array
   *   Result.
   *
   * @throws \Exception
   */
  public static function deleteCustomRoom($entity_id, $room_id) {
    $moveInventory = new InventoryActions();
    $moveInventory->setEntityId($entity_id);

    return self::run($moveInventory, 'deleteCustomRoom', [$room_id]);
  }

  /**
   * Move items between rooms.
   *
   * @param int $id
   *   Int mi_request_inventory id.
   * @param int $room_id
   *   Room id.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function moveItems($id, $room_id) {
    return self::run(InventoryActions::class, 'moveItems', [$id, $room_id]);
  }

  /**
   * Cancel inventory changes.
   *
   * @param int $entityId
   *   Entity id.
   * @param array $backUp
   *   Inventory data.
   *
   * @return array
   *   Result.
   *
   * @throws \Exception
   */
  public static function cancelInventoryChanges(int $entityId, array $backUp) {
    $moveInventory = new InventoryActions();
    $moveInventory->setEntityId($entityId);

    return self::run($moveInventory, 'cancelInventoryChange', [$backUp]);
  }

  /**
   * Transfer inventory.
   *
   * @param int $entityIdFrom
   *   Entity id from.
   * @param int $entityIdTo
   *   Entity id to.
   * @param string $fromInventoryTabName
   *   Tab name from inventory type.
   *
   * @return mixed
   *   Result.
   *
   * @throws \ServicesException
   */
  public static function transferInventory(int $entityIdFrom, int $entityIdTo, string $fromInventoryTabName) {
    $moveInventory = new InventoryActions();
    $moveInventory->setEntityId($entityIdFrom);
    $destinationNodeTo = (new MoveRequest($entityIdTo))->retrieve();
    $destinationNodeFrom = (new MoveRequest($entityIdFrom))->retrieve();

    return self::run($moveInventory, 'transferInventory',
      [$destinationNodeTo, $destinationNodeFrom, $fromInventoryTabName]);
  }

}
