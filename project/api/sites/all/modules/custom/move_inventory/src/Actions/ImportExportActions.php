<?php

namespace Drupal\move_inventory\Actions;

use Drupal\move_inventory\Actions\SubActions\ImportExportSubActions;
use Drupal\move_inventory\Tasks\ImportExportTasks;
use ZipArchive;

/**
 * Class MoveInventoryImportExport.
 *
 * @package Drupal\move_inventory\Services
 */
class ImportExportActions extends AbstractActions {
  private $zip;
  private $chunkNumber = 1000;
  public $zipUri = 'public://inventory_export.zip';
  private $moveBoardInventoryImagesPath = '/app/project/front/moveBoard/content/img/icons';
  private $publicTempUri = 'public://inventory_temp';

  /**
   * Method to export inventory.
   *
   * Create archive, add json data to it, add images to it.
   *
   * @return bool|string
   *   If everything is good return url of this archive.
   */
  public function exportInventory() {
    $importExportSubActions = new ImportExportSubActions();

    $this->zip = new ZipArchive();
    $path_to_zip = drupal_realpath($this->zipUri);

    if ($this->zip->open($path_to_zip, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
      $this->zip->addFromString('inventory_data.json', $importExportSubActions->addDataToZip());
      $importExportSubActions->addImagesToZip($this->zip);
      $this->zip->close();
      return file_create_url($this->zipUri);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Import inventory from zip archive.
   *
   * Get path of the archive. Export it to the temp folder.
   * Move images to the needed folder.
   * Import all data to db from json.
   * Remove all temp files and folder and archive.
   *
   * @return string
   *   Return message or FALSE.
   */
  public function importInventory() {
    $result = 'Import is done';
    $this->zip = new ZipArchive();
    $path_to_zip = drupal_realpath($this->zipUri);
    $inventory_images_path = drupal_realpath('public://inventory');
    $public_temp_path = drupal_realpath($this->publicTempUri);
    $dir_created = ImportExportTasks::createDir($public_temp_path);

    if ($dir_created) {
      $res = $this->zip->open($path_to_zip);
      if ($res === TRUE) {
        $this->zip->extractTo($public_temp_path);
        $this->zip->close();
        $json_file = file_get_contents($this->publicTempUri . '/inventory_data.json');
        // Insert inventory data to db.
        if (!empty($json_file)) {
          $array_to_import = drupal_json_decode($json_file);
          foreach ($array_to_import as $method => $data) {
            // Use set methods for add.
            $new_method_name = str_replace('get', 'set', $method);
            $array_chunk = array_chunk($data, $this->chunkNumber);
            foreach ($array_chunk as $data_to_add) {
              $method_path = 'Drupal\move_inventory\Actions\SubActions\ImportExportSubActions::' . $new_method_name;
              $batch_operations[] = array($method_path, [$data_to_add]);
            }
          }

          if (!empty($batch_operations)) {
            $batch = array(
              'title' => t('Inventory import processing'),
            );
            $batch['operations'] = $batch_operations;
            batch_set($batch);
            $result_import = TRUE;
          }
        }
        // Import inventory images.
        if (!empty($result_import)) {
          ImportExportTasks::createDir($inventory_images_path);
          ImportExportTasks::recurseCopy($public_temp_path . '/inventory', $inventory_images_path, 0777);
          // Add images for rooms.
          ImportExportTasks::recurseCopy($public_temp_path . '/moveboard', $this->moveBoardInventoryImagesPath);
          // Remove temp folder.
          file_unmanaged_delete_recursive($public_temp_path);
        }
        // Remove zip archive.
        if (file_exists($path_to_zip)) {
          unlink($path_to_zip);
        }
      }
      else {
        $result = "ZIP '$path_to_zip' was not opened";
      }
    }
    else {
      $result = "Temp dir '$public_temp_path' was not created.";
    }

    return $result;
  }

}
