<?php

namespace Drupal\move_inventory\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class MoveInventoryStatus.
 *
 * @package Drupal\move_inventory\Util
 */
class MoveInventoryRoomType extends BaseEnum {
  const DEFAULT = 0;
  const CUSTOM = 1;
  const BEDROOM = 2;

}
