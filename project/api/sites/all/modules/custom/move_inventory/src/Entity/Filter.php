<?php

namespace Drupal\move_inventory\Entity;

/**
 * Class Filter.
 *
 * @package Drupal\move_inventory\Entity
 */
class Filter {

  /**
   * Name.
   *
   * @var string
   */
  public $name = '';

  /**
   * Rooms ids.
   *
   * @var array
   */
  public $rooms_ids = 0;

  /**
   * Type.
   *
   * @var int
   */
  public $type = 0;

  /**
   * Get name.
   *
   * @return string
   *   Name.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Set name.
   *
   * @param string $name
   *   Name.
   */
  public function setName(string $name) {
    $this->name = $name;
  }

  /**
   * Get rooms ids.
   *
   * @return array
   *   Array rooms ids.
   */
  public function getRoomsIds(): array {
    return $this->rooms_ids;
  }

  /**
   * Set rooms ids.
   *
   * @param array $rooms_ids
   *   Rooms ids.
   */
  public function setRoomsIds(array $rooms_ids) {
    $this->rooms_ids = $rooms_ids;
  }

  /**
   * Get type.
   *
   * @return int
   *   Type.
   */
  public function getType(): int {
    return $this->type;
  }

  /**
   * Set type.
   *
   * @param int $type
   *   Type.
   */
  public function setType(int $type) {
    $this->type = $type;
  }

}
