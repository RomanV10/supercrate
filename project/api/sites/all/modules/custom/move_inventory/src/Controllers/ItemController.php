<?php

namespace Drupal\move_inventory\Controllers;

use Drupal\move_inventory\Actions\ItemActions;
use Drupal\move_inventory\Util\enum\MoveInventoryEntity;

/**
 * Class ItemController.
 *
 * @package Drupal\move_inventory\Controllers
 */
class ItemController extends AbstractController {

  /**
   * Create Item.
   *
   * @param array $data
   *   Data.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function createInventoryItem(array $data = array()) {
    return self::run(ItemActions::class, 'create', [$data]);
  }

  /**
   * Retrieve item.
   *
   * @param int $id
   *   Id item.
   *
   * @return array|mixed
   *   Item data.
   *
   * @throws \Exception
   */
  public static function retrieveInventoryItem(int $id) {
    $itemActions = new ItemActions();

    $itemActions->setId($id);
    return self::run($itemActions, 'retrieve', []);
  }

  /**
   * Update item.
   *
   * @param int $id
   *   Id item.
   * @param mixed $data
   *   Data.
   *
   * @return bool|\DatabaseStatementInterface|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function updateInventoryItem(int $id, $data) {
    $itemActions = new ItemActions();

    $itemActions->setId($id);
    return self::run($itemActions, 'update', [$data]);
  }

  /**
   * Delete item.
   *
   * @param int $id
   *   Item id.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function deleteInventoryItem(int $id) {
    $itemActions = new ItemActions();

    $itemActions->setId($id);
    return self::run($itemActions, 'delete', []);
  }

  /**
   * Index method.
   *
   * @param array|null $condition
   *   Conditions.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function indexInventoryItem(?array $condition) {
    return self::run(ItemActions::class, 'index', [$condition]);
  }

  /**
   * Add inventory items for request to db.
   *
   * @param array $data
   *   Array of fields to add.
   *
   * @return array
   *   TRUE if everything is good.
   *
   * @throws \Exception
   */
  public static function updateItemInRequest(array $data) {
    return self::run(ItemActions::class, 'updateItemInRequest', [$data]);
  }

  /**
   * Add custom item.
   *
   * @param mixed $data
   *   Data.
   *
   * @return array
   *   Result.
   *
   * @throws \Exception
   */
  public static function updateCustomItemInRequest($data) {
    if (!empty($data['item_id'])) {
      $result = self::run(ItemActions::class, 'updateCustomItemInRequest', [$data]);
    }
    else {
      $result = self::getGoodCustomAnswer(500, 'OK', 'Undefined item_id');
    }

    return $result;
  }

  /**
   * Add items with rooms, filters, count to package.
   *
   * @return array|mixed
   *   Result.
   */
  public static function addItemToPackage() {
    return NULL;
  }

  /**
   * Insert/update weight for items.
   *
   * @param array $data
   *   Data.
   *
   * @throws \Exception
   */
  public static function itemSorting(array $data) {
    self::run(ItemActions::class, 'updateWeight', [$data, MoveInventoryEntity::ITEM]);
  }

  /**
   * Search item by name.
   *
   * @param string $name
   *   Item name.
   * @param array $conditions
   *   Conditions.
   *
   * @return array
   *   Result.
   *
   * @throws \Exception
   */
  public static function searchItem(string $name, array $conditions) {
    return self::run(ItemActions::class, 'searchItemInRequest', [$name, $conditions]);
  }

  /**
   * Search item in settings inventory.
   *
   * @param string $name
   *   Name.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function searchItemInSettings(string $name) {
    return self::run(ItemActions::class, 'searchItemInSettings', [$name]);
  }

  /**
   * Retrieve only custom inventory.
   *
   * @param int $entity_id
   *   Entity id.
   * @param array|null $conditions
   *   Conditions.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function getInventoryCustomItems(int $entity_id, ?array $conditions) {
    $itemActions = new ItemActions();

    $itemActions->setEntityId($entity_id);
    return self::run($itemActions, 'getCustomItems', [$conditions]);
  }

  /**
   * Delete custom item.
   *
   * @param int $id
   *   Custom item id.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function deleteInventoryCustomItems(int $id) {
    return self::run(ItemActions::class, 'deleteCustomItem', [$id]);
  }

  /**
   * Remove item from request by id position in request.
   *
   * @param int $id
   *   Id.
   *
   * @return array
   *   Result.
   *
   * @throws \Exception
   */
  public static function deleteItemFromRequest(int $id) {
    return self::run(ItemActions::class, 'deleteItemFromRequest', [$id]);
  }

  /**
   * GetEntityInventory.
   *
   * @param int $entity_id
   *   Entity id.
   * @param mixed $conditions
   *   Conditions.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public static function getInventoryRequest(int $entity_id, $conditions = array()) {
    return self::run(ItemActions::class, 'getEntityInventory', [$entity_id, $conditions]);
  }

}
