<?php

namespace Drupal\move_inventory\Actions;

use Drupal\move_inventory\Actions\SubActions\FilterSubActions;
use Drupal\move_inventory\Actions\SubActions\ItemSubActions;
use Drupal\move_inventory\Models\ItemModel;
use Drupal\move_inventory\Models\RequestInventoryModel;
use Drupal\move_inventory\Tasks\FilterTasks;
use Drupal\move_inventory\Tasks\InventoryTasks;
use Drupal\move_inventory\Tasks\ItemTasks;
use Drupal\move_inventory\Tasks\RoomTasks;

/**
 * Class MoveInventoryItem.
 *
 * @package Drupal\move_inventory\Services
 */
class ItemActions extends AbstractActions implements BaseCRUDInterface {

  /**
   * Create Item.
   *
   * @param array $data
   *   Data.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function create(array $data = array()) {
    return (new ItemSubActions())->create($data);
  }

  /**
   * Retrieve item.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function retrieve() {
    $itemSubActions = new ItemSubActions();

    $itemSubActions->setId($this->getId());
    return $itemSubActions->retrieve();
  }

  /**
   * Update item.
   *
   * @param array $data
   *   Data.
   *
   * @return bool|\DatabaseStatementInterface|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function update(array $data = array()) {
    $itemSubActions = new ItemSubActions();

    $itemSubActions->setId($this->getId());
    return $itemSubActions->update($data);
  }

  /**
   * Delete item.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function delete() {
    $itemSubActions = new ItemSubActions();

    $itemSubActions->setId($this->getId());
    return $itemSubActions->delete();
  }

  /**
   * Index method for items.
   *
   * @param array|null $conditions
   *   Conditions.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function index(?array $conditions) {
    $itemSubActions = new ItemSubActions();

    $filters = ['room_id' => 0, 'filter_id' => 0];
    $field_number_format = ItemTasks::getFieldNumber();
    $items = ItemModel::getItemsData($conditions, $filters);
    if (!empty($items)) {
      if (!empty($conditions['entity_id'])) {
        $roomsId = RoomTasks::getRoomsId(array_merge(RoomTasks::getDefaultRoomWithWeight($conditions['entity_id']), RoomTasks::getCustomRoom($conditions['entity_id'])));
      }
      else {
        $roomsId = RoomTasks::getRoomsId(RoomTasks::getRoomForSettings());
      }
      foreach ($items as $key => $item) {
        // Add items only once.
        if (!empty($item['image_link'])) {
          $item['uri'] = $item['image_link'];
          $item['image_link'] = $itemSubActions->showPresetUrl($item['image_link']);
        }

        $item['filters'] = ItemTasks::getExistingFilter($item['item_id']);
        $item['rooms'] = ItemTasks::getItemRooms($item['item_id'], $roomsId);
        $itemSubActions->convertFieldsToNumber($item, $field_number_format);

        $items[$key] = $item;
      }
    }

    return $items;
  }

  /**
   * Search item by name.
   *
   * @param string $item_name
   *   Item name.
   * @param array $conditions
   *   Conditions.
   *
   * @return array
   *   Result.
   *
   * @throws \Exception
   */
  public function searchItemInRequest(string $item_name, array $conditions) {
    $itemSubActions = new ItemSubActions();

    $conditions['entity_id'] = $itemSubActions->getEntityIdStoragePacking($conditions['entity_id']);
    $result = ItemModel::seatchItemInRequest($item_name, $conditions);
    if (!empty($result)) {
      foreach ($result as $key => $item) {
        if (!empty($item['image_link'])) {
          $result[$key]['uri'] = $item['image_link'];
          $result[$key]['image_link'] = $itemSubActions->showPresetUrl($item['image_link']);
        }
      }
    }

    return $result;
  }

  /**
   * Search item in settings inventory.
   *
   * @param string $name
   *   Name.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function searchItemInSettings(string $name) {
    $itemSubActions = new ItemSubActions();

    $field_number_format = ItemTasks::getFieldNumber();
    $result = ItemModel::searchItemInsettings($name);
    if (!empty($result)) {
      foreach ($result as $key => $item) {
        // Add items only once.
        if (!empty($item['image_link'])) {
          $item['uri'] = $item['image_link'];
          $item['image_link'] = $itemSubActions->showPresetUrl($item['image_link']);
        }

        $item['filters'] = ItemTasks::getExistingFilter($item['item_id']);
        $item['rooms'] = ItemTasks::getItemRooms($item['item_id'], []);
        $itemSubActions->convertFieldsToNumber($item, $field_number_format);

        $result[$key] = $item;
      }
    }

    return $result;

  }

  /**
   * Retrieve only custom inventory.
   *
   * @param array|null $conditions
   *   Conditions.
   *
   * @return mixed
   *   Result.
   */
  public function getCustomItems(?array $conditions) {
    $itemSubActions = new ItemSubActions();

    $this->setEntityId(
      $itemSubActions->getEntityIdStoragePacking($this->getEntityId())
    );

    return RequestInventoryModel::getCustomInventory($this->getEntityId(), $conditions);
  }

  /**
   * Update item in request. Add or remove.
   *
   * @param array $data
   *   Array for add or remove.
   *
   * @return string
   *   item id when added or updated. Or 1|0 when removed.
   *
   * @throws \Exception
   */
  public function updateItemInRequest(array $data) : string {
    if (empty($data['count'])) {
      $result = $this->deleteItemFromRequestByConditions($data);
    }
    else {
      $result = $this->addItemToRequest($data);
    }
    return (string) $result;
  }

  /**
   * Add or remove custom item in request.
   *
   * @param array $data
   *   Array with data for add or remove.
   *
   * @return string
   *   If add - item id. If remove: 1 - when remove, 0 - if not.
   *
   * @throws \Exception
   */
  public function updateCustomItemInRequest(array $data) : string {
    $itemSubActions = new ItemSubActions();

    if (empty($data['count'])) {
      $result = $this->deleteCustomItem($data['id']);
    }
    else {
      $result = $itemSubActions->addCustomItem($data);
    }
    return $result;
  }

  /**
   * Add inventory items for request to db.
   *
   * @param array $data
   *   Array of fields to add.
   *
   * @return mixed
   *   Item id.
   *
   * @throws \Exception
   */
  public function addItemToRequest(array $data) {
    $itemSubActions = new ItemSubActions();

    $data['entity_id'] = $itemSubActions->getEntityIdStoragePacking($data['entity_id']);
    ItemTasks::addItemToRequest($data);

    return InventoryTasks::getInsertedItemInRequest($data);

  }

  /**
   * Insert/update weight for items.
   *
   * @param mixed $data
   *   Data.
   * @param int $entity_type
   *   Entity type.
   *
   * @throws \Exception
   */
  public function updateWeight($data, int $entity_type) {
    foreach ($data as $val) {
      $parents = $val['parents_ids'];
      // Remove default room value for this item.
      if (isset($parents['room_id'])) {
        ItemTasks::deleteWeightForItem($val['id'], $parents['filter_id'], 0);
      }
      $fields = $parents;
      $fields['entity_id'] = $val['id'];
      $fields['entity_type'] = $entity_type;

      FilterTasks::mergeWeightForSorting($fields, $val['weight']);
    }
  }

  /**
   * Delete custom item.
   *
   * @param int $id
   *   Id of item.
   *
   * @return int
   *   1 if removed, 0 - if not.
   *
   * @throws \ServicesException
   */
  public function deleteCustomItem(int $id) : int {
    $customItemData = InventoryTasks::getCustomItem($id);
    $conditions = ['id' => $id];
    $deleted = InventoryTasks::deleteItemFromRequest($conditions);
    if ($deleted) {
      $conditions = array(
        [
          'field' => 'item_id',
          'value' => $customItemData['item_id'],
          'operation' => '=',
        ],
      );
      $deleted = ItemModel::deleteItem($conditions);
    }

    return $deleted;
  }

  /**
   * Remove item from request by id position in request.
   *
   * @param int $id
   *   Id of item position in request.
   *
   * @return bool
   *   TRUE if was deleted.
   *
   * @throws \ServicesException
   */
  public function deleteItemFromRequest(int $id) {
    InventoryTasks::deleteItemFromRequest(['id' => $id]);

    return TRUE;
  }

  /**
   * Remove item from request by conditions.
   *
   * @param array $conditions
   *   Array of the conditions.
   *
   * @return int
   *   1 if removed, 0 if not.
   *
   * @throws \ServicesException
   */
  public function deleteItemFromRequestByConditions(array $conditions) : int {
    $itemSubActions = new ItemSubActions();

    $conditions['entity_id'] = $itemSubActions->getEntityIdStoragePacking($conditions['entity_id']);
    $conditions = array_intersect_key($conditions, ItemTasks::conditionsAllowedForRemoveFromRequest());

    return InventoryTasks::deleteItemFromRequest($conditions);
  }

  /**
   * GetEntityInventory.
   *
   * @param int $entity_id
   *   Entity id.
   * @param array|null $conditions
   *   Conditions.
   *
   * @return mixed
   *   Items in request.
   *
   * @throws \ServicesException
   */
  public function getEntityInventory(int $entity_id, ?array $conditions) {
    $conditions['entity_id'] = (new ItemSubActions())->getEntityIdStoragePacking($entity_id);

    return (new FilterSubActions())->getEntityInventory($conditions);
  }

}
