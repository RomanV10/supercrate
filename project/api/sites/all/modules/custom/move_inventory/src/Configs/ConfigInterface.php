<?php

namespace Drupal\move_inventory\Configs;

/**
 * Interface ConfigInterface.
 *
 * @package Drupal\move_inventory\Configs
 */
interface ConfigInterface {

  /**
   * Get config data.
   *
   * @return array
   *   Data.
   */
  public static function getConfig();

}
