<?php

namespace Drupal\move_inventory\Actions\SubActions;

use Drupal\move_inventory\Tasks\InventoryTasks;
use Drupal\move_inventory\Tasks\RoomTasks;

/**
 * Class InventorySubActions.
 *
 * @package Drupal\move_inventory\Actions\SubActions
 */
class InventorySubActions extends AbstractSubActions {

  /**
   * Update weight for request.
   *
   * @param string $last_name
   *   Name last bedroom.
   * @param int $room_id
   *   Id new room.
   * @param int $entityId
   *   Entity id.
   *
   * @throws \Exception
   */
  public function updateWeightForRequest(string $last_name, int $room_id, int $entityId) {
    $flag_increment_rooms = FALSE;

    $result = array_merge(RoomTasks::getDefaultRoomWithWeight($entityId), RoomTasks::getCustomRoom($entityId));
    foreach ($result as $room) {
      if ($room['name'] == $last_name) {
        // Insert weight for new bedroom here.
        InventoryTasks::insertRoomWeightForRequest($room_id, $entityId, ++$room['weight']);
        $flag_increment_rooms = TRUE;
        continue;
      }
      if ($flag_increment_rooms) {
        // Increment weight +1.
        InventoryTasks::insertRoomWeightForRequest($room['room_id'], $entityId, ++$room['weight']);
      }
    }
  }

}
