<?php

namespace Drupal\move_inventory\Tasks;

use Drupal\move_inventory\Configs\Config;
use Drupal\move_inventory\Models\RoomModel;
use Drupal\move_inventory\Models\RoomRelationModel;
use Drupal\move_inventory\Models\WeightModel;
use Drupal\move_inventory\Util\enum\MoveInventoryEntity;
use Drupal\move_inventory\Util\enum\MoveInventoryRoomType;

/**
 * Class RoomTasks.
 *
 * @package Drupal\move_inventory\Tasks
 */
class RoomTasks extends AbstractTasks {

  /**
   * Get count bedroom create.
   *
   * @param int $moveSize
   *   Move size.
   *
   * @return int
   *   Count bedroom.
   */
  public static function getCountBedroomCreate(int $moveSize) {
    if (in_array($moveSize, Config::getConfig()->getOneBedroom())) {
      return 1;
    }
    elseif (in_array($moveSize, Config::getConfig()->getTwoBedroom())) {
      return 2;
    }
    elseif (in_array($moveSize, Config::getConfig()->getThreeBedroom())) {
      return 3;
    }
    elseif (in_array($moveSize, Config::getConfig()->getFourBedroom())) {
      return 4;
    }
    // Default.
    return 1;
  }

  /**
   * Create default room.
   *
   * @param array $room
   *   Room data.
   *
   * @return array
   *   Result.
   *
   * @throws \Exception
   */
  public static function create(array $room) {
    $room['created'] = time();
    $room['room_id'] = RoomModel::insertRoom($room);
    $room['uri'] = $room['image_link'];
    $room['image_link'] = file_create_url($room['image_link']);

    return $room;
  }

  /**
   * Delete room from weight.
   *
   * @param int $id
   *   Room id.
   *
   * @throws \ServicesException
   */
  public static function deleteWeightRoom(int $id) {
    $conditions = array(
      ['field' => 'entity_id', 'value' => $id, 'operation' => '='],
      [
        'field' => 'entity_type',
        'value' => MoveInventoryEntity::ROOM,
        'operation' => '=',
      ],
    );
    WeightModel::deleteWeight($conditions);
  }

  /**
   * Delete weight for room in request.
   *
   * @param int $room_id
   *   Room id.
   * @param int $request_id
   *   Request id.
   *
   * @throws \ServicesException
   */
  public static function deleteRoomWeightForRequest(int $room_id, int $request_id) {
    $conditions = array(
      ['field' => 'entity_id', 'value' => $room_id, 'operation' => '='],
      ['field' => 'request_id', 'value' => $request_id, 'operation' => '='],
      [
        'field' => 'entity_type',
        'value' => MoveInventoryEntity::ROOM,
        'operation' => '=',
      ],
    );
    WeightModel::deleteWeight($conditions);
  }

  /**
   * Insert weight room.
   *
   * @param int $roomId
   *   Room id.
   * @param int|null $entityId
   *   Entity id.
   * @param int $weight
   *   Weight.
   *
   * @throws \Exception
   */
  public static function insertWeightRoom(int $roomId, ?int $entityId, int $weight) {
    $fields = array(
      'entity_id' => $roomId,
      'entity_type' => MoveInventoryEntity::ROOM,
      'request_id' => $entityId,
      'weight' => $weight,
    );
    WeightModel::insertWeight($fields);
  }

  /**
   * Merge weight when room sorting.
   *
   * @param array $fields
   *   Fields.
   * @param int $weight
   *   Weight.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeWeightForRooms(array $fields, int $weight) {
    $keys = $fields;
    $fields['weight'] = $weight;
    $expressions = array(
      ['expression' => 'isNull', 'value' => 'request_id'],
    );
    WeightModel::mergeWeight($keys, $fields, $expressions);
  }

  /**
   * Insert room relation.
   *
   * @param int $entityId
   *   Request id.
   * @param int $room_id
   *   Room id.
   *
   * @throws \Exception
   */
  public static function insertRoomRelation(int $entityId, int $room_id) {
    $fields = array(
      'entity_id' => $entityId,
      'room_id' => $room_id,
    );

    RoomRelationModel::insertRoomRelation($fields);
  }

  /**
   * Update room.
   *
   * @param array $room
   *   Room data.
   * @param int $id
   *   Id room.
   *
   * @return \DatabaseStatementInterface
   *   Result.
   *
   * @throws \ServicesException
   */
  public static function updateRoom(array $room, int $id) {
    $conditions = array(
      ['field' => 'room_id', 'value' => $id, 'operation' => '='],
    );

    return RoomModel::updateRoom($room, $conditions);
  }

  /**
   * Delete room from room table.
   *
   * @param int $id
   *   Room id.
   *
   * @throws \ServicesException
   */
  public static function deleteRoom(int $id) {
    $conditions = array(
      ['field' => 'room_id', 'value' => $id, 'operation' => '='],
    );

    RoomModel::deleteRoom($conditions);
  }

  /**
   * Delete room from filter relations table.
   *
   * @param int $id
   *   Room id.
   *
   * @throws \ServicesException
   */
  public static function deleteRoomFromFilterRelations(int $id) {
    $conditions = array(
      ['field' => 'room_id', 'value' => $id, 'operation' => '='],
    );

    RoomRelationModel::deleteRoomRelation($conditions);
  }

  /**
   * Retrieve room.
   *
   * @param int $id
   *   Retrieve room.
   *
   * @return bool|mixed
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function retrieve(int $id) {
    $fields = array(
      ['alias' => RoomModel::$alias, 'fields' => []],
    );
    $conditions = array(
      ['field' => 'room_id', 'value' => $id, 'operation' => '='],
    );
    $fetching = array('fetch' => 'fetchAssoc', 'value' => \PDO::FETCH_ASSOC);

    return RoomModel::selectRoom([], $fields, $conditions, [], $fetching);
  }

  /**
   * Get rooms data.
   *
   * @return mixed
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function getRooms() {
    $fields = array(
      ['alias' => RoomModel::$alias, 'fields' => []],
    );
    $conditions = array(
      [
        'field' => 'mr.type',
        'value' => MoveInventoryRoomType::DEFAULT,
        'operation' => '=',
      ],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return RoomModel::selectRoom([], $fields, $conditions, [], $fetching);
  }

  /**
   * Get default rooms.
   *
   * @return mixed
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function getDefaultRoom() {
    $fields = array(
      ['alias' => RoomModel::$alias, 'fields' => ['room_id']],
    );
    $conditions = array(
      [
        'field' => 'room_id',
        'value' => array(Config::getConfig()->getAllId(), Config::getConfig()->getBedroomId()),
        'operation' => 'NOT IN',
      ],
      [
        'field' => 'type',
        'value' => MoveInventoryRoomType::DEFAULT,
        'operation' => '=',
      ],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return RoomModel::selectRoom([], $fields, $conditions, [], $fetching);
  }

  /**
   * Get default room with weight.
   *
   * @param int $entityId
   *   Entity id.
   *
   * @return array
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function getDefaultRoomWithWeight(int $entityId) : array {
    $joins = array(
      [
        'table' => WeightModel::$table,
        'alias' => WeightModel::$alias,
        'condition' => 'mr.room_id = mw.entity_id AND mw.entity_type = ' . MoveInventoryEntity::ROOM . ' AND mw.request_id = ' . $entityId,
      ],
    );
    $fields = array(
      ['alias' => WeightModel::$alias, 'fields' => ['weight']],
      ['alias' => RoomModel::$alias, 'fields' => []],
    );
    $conditions = array(
      [
        'field' => 'mr.type',
        'value' => MoveInventoryRoomType::DEFAULT,
        'operation' => '=',
      ],
    );
    $expressions = array(
      ['expression' => 'orderBy', 'value' => ['mw.weight', 'ASC']],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return RoomModel::selectRoom($joins, $fields, $conditions, $expressions, $fetching) ?? array();
  }

  /**
   * Get count bedroom in inventory.
   *
   * @param int $entityId
   *   Entity id.
   *
   * @return mixed
   *   Count.
   *
   * @throws \ServicesException
   */
  public static function getCountBedroomInRequest(int $entityId) {
    $joins = array(
      [
        'table' => RoomRelationModel::$table,
        'alias' => RoomRelationModel::$alias,
        'condition' => 'mrr.room_id = mr.room_id',
      ],
    );
    $conditions = array(
      [
        'field' => 'mr.name',
        'value' => '%' . db_like('Bedroom') . '%',
        'operation' => 'LIKE',
      ],
      [
        'field' => 'mrr.entity_id',
        'value' => $entityId,
        'operation' => '=',
      ],
    );
    $expressions = array(
      [
        'expression' => 'addExpression',
        'value' => ['COUNT(mr.room_id)', 'old_count'],
      ],
    );
    $fetching = array('fetch' => 'fetchField', 'value' => 0);
    $result = RoomModel::selectRoom($joins, [], $conditions, $expressions, $fetching);

    return !empty($result) ? ++$result : 1;
  }

  /**
   * Get last name bedroom in request.
   *
   * @param int $entityId
   *   Entity id.
   *
   * @return string
   *   Name.
   *
   * @throws \ServicesException
   */
  public static function getLastNameBedroomInRequest(int $entityId) {
    $joins = array(
      [
        'table' => RoomRelationModel::$table,
        'alias' => RoomRelationModel::$alias,
        'condition' => 'mrr.room_id = mr.room_id',
      ],
    );
    $conditions = array(
      [
        'field' => 'mr.name',
        'value' => '%' . db_like('Bedroom') . '%',
        'operation' => 'LIKE',
      ],
      [
        'field' => 'mrr.entity_id',
        'value' => $entityId,
        'operation' => '=',
      ],
    );
    $expressions = array(
      ['expression' => 'addExpression', 'value' => 'MAX(name)'],
    );
    $fetching = array('fetch' => 'fetchField', 'value' => 0);

    return RoomModel::selectRoom($joins, [], $conditions, $expressions, $fetching) ?? 'Bedroom';
  }

  /**
   * Get room id.
   *
   * @param string $name
   *   Name room.
   * @param int $entityId
   *   Entity id.
   *
   * @return mixed
   *   Room id.
   *
   * @throws \ServicesException
   */
  public static function getRoomIdForNameInRequest(string $name, int $entityId) {
    $joins = array(
      [
        'table' => RoomRelationModel::$table,
        'alias' => RoomRelationModel::$alias,
        'condition' => 'mrr.room_id = mr.room_id',
      ],
    );
    $fields = array(
      ['alias' => RoomModel::$alias, 'fields' => ['room_id']],
    );
    $conditions = array(
      ['field' => 'mr.name', 'value' => $name, 'operation' => '='],
      ['field' => 'mrr.entity_id', 'value' => $entityId, 'operation' => '='],
    );
    $fetching = array('fetch' => 'fetchField', 'value' => 0);

    return RoomModel::selectRoom($joins, $fields, $conditions, [], $fetching) ?? Config::getConfig()->getBedroomId();
  }

  /**
   * Get custom room with weight.
   *
   * @param int $entityId
   *   Entity id.
   *
   * @return array
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function getCustomRoom(int $entityId) : array {
    $joins = array(
      [
        'table' => RoomRelationModel::$table,
        'alias' => RoomRelationModel::$alias,
        'condition' => 'mrr.room_id = mr.room_id',
      ],
      [
        'table' => WeightModel::$table,
        'alias' => WeightModel::$alias,
        'condition' => 'mr.room_id = mw.entity_id AND mw.entity_type = ' . MoveInventoryEntity::ROOM . ' AND mw.request_id = ' . $entityId,
      ],
    );
    $fields = array(
      ['alias' => RoomModel::$alias, 'fields' => []],
      ['alias' => WeightModel::$alias, 'fields' => ['weight']],
    );
    $conditions = array(
      [
        'field' => 'mr.type',
        'value' => MoveInventoryRoomType::DEFAULT,
        'operation' => '<>',
      ],
      ['field' => 'mrr.entity_id', 'value' => $entityId, 'operation' => '='],
    );
    $expressions = array(
      ['expression' => 'orderBy', 'value' => ['mw.weight', 'ASC']],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return RoomModel::selectRoom($joins, $fields, $conditions, $expressions, $fetching) ?? [];
  }

  /**
   * Get rooms for settings/inventory.
   *
   * @return mixed
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function getRoomForSettings() {
    $joins = array(
      [
        'table' => WeightModel::$table,
        'alias' => WeightModel::$alias,
        'condition' => 'mr.room_id = mw.entity_id AND mw.entity_type = ' . MoveInventoryEntity::ROOM,
      ],
    );
    $fields = array(
      ['alias' => RoomModel::$alias, 'fields' => []],
      ['alias' => WeightModel::$alias, 'fields' => ['weight']],
    );
    $conditions = array(
      [
        'field' => 'mr.type',
        'value' => MoveInventoryRoomType::DEFAULT,
        'operation' => '=',
      ],
    );
    $expressions = array(
      ['expression' => 'orderBy', 'value' => ['mw.weight', 'ASC']],
      ['expression' => 'isNull', 'value' => 'mw.request_id'],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return RoomModel::selectRoom($joins, $fields, $conditions, $expressions, $fetching);
  }

  /**
   * Get rooms for clone.
   *
   * @param int $old_nid
   *   Old nid.
   *
   * @return mixed
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function getRoomForCloneRequest(int $old_nid) {
    $joins = array(
      [
        'table' => RoomModel::$table,
        'alias' => RoomModel::$alias,
        'condition' => 'mrr.room_id = mr.room_id',
      ],
      [
        'table' => WeightModel::$table,
        'alias' => WeightModel::$alias,
        'condition' => 'mw.entity_id = mr.room_id',
      ],
    );
    $fields = array(
      ['alias' => RoomModel::$alias, 'fields' => []],
      ['alias' => WeightModel::$alias, 'fields' => ['weight']],
    );
    $conditions = array(
      ['field' => 'mrr.entity_id', 'value' => $old_nid, 'operation' => '='],
      ['field' => 'mw.request_id', 'value' => $old_nid, 'operation' => '='],
      [
        'field' => 'mw.entity_type',
        'value' => MoveInventoryEntity::ROOM,
        'operation' => '=',
      ],
    );
    $expressions = array(
      ['expression' => 'orderBy', 'value' => 'mw.weight'],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return RoomRelationModel::selectRoomRelation($joins, $fields, $conditions, $expressions, $fetching);
  }

  /**
   * Get rooms id with array room.
   *
   * @param array $rooms
   *   Rooms data.
   *
   * @return array
   *   Rooms id.
   */
  public static function getRoomsId(array $rooms) {
    $roomsId = [];
    foreach ($rooms as $room) {
      $roomsId[] = $room['room_id'];
    }
    return $roomsId;
  }

  /**
   * Get max weight room for settings inventory.
   *
   * @return mixed
   *   Weight.
   *
   * @throws \ServicesException
   */
  public static function getMaxWeightForRoomSettings() {
    $conditions = array(
      [
        'field' => 'mw.entity_type',
        'value' => MoveInventoryEntity::ROOM,
        'operation' => '=',
      ],
    );
    $expressions = array(
      ['expression' => 'addExpression', 'value' => 'MAX(weight)'],
      ['expression' => 'isNull', 'value' => 'mw.request_id'],
    );
    $fetching = ['fetch' => 'fetchField', 'value' => 0];

    return WeightModel::selectWeight([], [], $conditions, $expressions, $fetching);
  }

}
