<?php

namespace Drupal\move_inventory\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class MoveInventoryStatus.
 *
 * @package Drupal\move_inventory\Util
 */
class MoveInventoryStatus extends BaseEnum {
  const DRAFT = 0;
  const SUBMIT = 1;

}
