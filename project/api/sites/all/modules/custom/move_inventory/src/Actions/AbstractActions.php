<?php

namespace Drupal\move_inventory\Actions;

/**
 * Class AbstractBaseService.
 *
 * @package Drupal\move_inventory\Actions
 */
abstract class AbstractActions {

  /**
   * Room id, filter id, item id.
   *
   * @var int
   */
  private $id;

  /**
   * Request id.
   *
   * @var int
   */
  private $entityId;

  /**
   * Get id.
   *
   * @return int
   *   Inventory entity id.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Set inventory entity id.
   *
   * @param int $id
   *   Entity id.
   */
  public function setId(int $id): void {
    $this->id = $id;
  }

  /**
   * Get entity id.
   *
   * @return int
   *   Entity id.
   */
  public function getEntityId(): int {
    return $this->entityId;
  }

  /**
   * Set entity id.
   *
   * @param int $entityId
   *   Entity id.
   */
  public function setEntityId(int $entityId): void {
    $this->entityId = $entityId;
  }

}
