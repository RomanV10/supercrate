<?php

namespace Drupal\move_inventory\Configs;

/**
 * Class Config.
 *
 * @package Drupal\move_inventory\Configs
 */
class Config implements ConfigInterface {

  private $config = [
    'defaultInventoryType' => 0,
    'additionalInventoryType' => 1,
    'secondAdditionalInventory' => 2,
    'minWeightForRequestRoom' => 1,
    'minWeightForItem' => 0,
    'allId' => 15,
    'bedroomId' => 5,
    'oneBedroom' => [1, 2, 3, 4],
    'twoBedroom' => [5, 6, 8],
    'threeBedroom' => [7, 9],
    'fourBedroom' => [10],
  ];

  private static $instance = NULL;

  /**
   * Get config instance.
   *
   * @return array|\Drupal\move_inventory\Configs\Config|null
   *   Config Instance.
   */
  public static function getConfig() {
    self::$instance = self::$instance ?? new Config();

    return self::$instance;
  }

  /**
   * Get default inventory type.
   *
   * @return mixed
   *   Config.
   */
  public function getDefaultInventoryType() {
    return $this->config['defaultInventoryType'];
  }

  /**
   * Get additional inventory type.
   *
   * @return mixed
   *   Config.
   */
  public function getAdditionalInventoryType() {
    return $this->config['additionalInventoryType'];
  }

  /**
   * Get second additional inventory.
   *
   * @return mixed
   *   Config.
   */
  public function getSecondAdditionalInventory() {
    return $this->config['secondAdditionalInventory'];
  }

  /**
   * Get min weight for request room.
   *
   * @return mixed
   *   Config.
   */
  public function getMinWeightForRequestRoom() {
    return $this->config['minWeightForRequestRoom'];
  }

  /**
   * Get room "All" id.
   *
   * @return mixed
   *   Config.
   */
  public function getAllId() {
    return $this->config['allId'];
  }

  /**
   * Get room "Bedroom" id.
   *
   * @return mixed
   *   Config.
   */
  public function getBedroomId() {
    return $this->config['bedroomId'];
  }

  /**
   * Get one Bedroom for move of size.
   *
   * @return mixed
   *   Config.
   */
  public function getOneBedroom() {
    return $this->config['oneBedroom'];
  }

  /**
   * Get two Bedroom for move of size.
   *
   * @return mixed
   *   Config.
   */
  public function getTwoBedroom() {
    return $this->config['twoBedroom'];
  }

  /**
   * Get three Bedroom for move of size.
   *
   * @return mixed
   *   Config.
   */
  public function getThreeBedroom() {
    return $this->config['threeBedroom'];
  }

  /**
   * Get four Bedroom for move of size.
   *
   * @return mixed
   *   Config.
   */
  public function getFourBedroom() {
    return $this->config['fourBedroom'];
  }

  /**
   * Get min weight for item.
   *
   * @return mixed
   *   Config.
   */
  public function getMinWeightForItem() {
    return $this->config['minWeightForItem'];
  }

}
