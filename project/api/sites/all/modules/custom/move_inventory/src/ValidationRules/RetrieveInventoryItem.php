<?php

namespace Drupal\move_inventory\ValidationRules;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class TeamRetrieve.
 *
 * @package Drupal\move_dispatch\ValidationRules\Team
 */
class RetrieveInventoryItem implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'id' => ['int'],
    ];
  }

}
