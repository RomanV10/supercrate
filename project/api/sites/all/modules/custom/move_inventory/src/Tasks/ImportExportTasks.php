<?php

namespace Drupal\move_inventory\Tasks;

use Drupal\move_inventory\Models\FilterModel;
use Drupal\move_inventory\Models\FilterRelationModel;
use Drupal\move_inventory\Models\RoomModel;
use Drupal\move_inventory\Models\WeightModel;
use Drupal\move_inventory\Util\enum\MoveInventoryRoomType;

/**
 * Class ImportExportTasks.
 *
 * @package Drupal\move_inventory\Tasks
 */
class ImportExportTasks extends AbstractTasks {

  /**
   * Add images from results to object.
   *
   * @param array $result
   *   Data with images path.
   * @param array $images
   *   Curr images.
   *
   * @return array
   *   New array images.
   */
  public static function setImagesFromResult(array $result, array $images) {
    return array_merge($images, array_column($result, 'image_link'));
  }

  /**
   * Create dir. Remove old before.
   *
   * @param string $dir_path
   *   Path to folder.
   * @param int $mode
   *   Chmod.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public static function createDir(string $dir_path, int $mode = 0777) : bool {
    if (is_dir($dir_path)) {
      file_unmanaged_delete_recursive($dir_path);
    }
    drupal_mkdir($dir_path);
    $result = drupal_chmod($dir_path, $mode);
    return $result;
  }

  /**
   * Clean old data from tables.
   *
   * Use batch context to know if wsa cleaned before.
   *
   * @param string $table_name
   *   Name of the db table.
   * @param array $context
   *   Batch context.
   */
  public static function cleanOldData(string $table_name, array &$context = array()) : void {
    if (empty($context['results'][$table_name]['cleaned'])) {
      db_delete($table_name)->execute();
      $context['results'][$table_name]['cleaned'] = TRUE;
    }
  }

  /**
   * Helper function to add data to db.
   *
   * @param array $data
   *   Data to add.
   * @param string $table_name
   *   Name of the table to add.
   *
   * @return \DatabaseStatementInterface|int
   *   1 if updated, 0 - if not.
   *
   * @throws \Exception
   */
  public static function insertData(array $data, string $table_name) {
    $result = 0;
    if (!empty($data)) {
      $fields = array_keys($data[0]);
      $query = db_insert($table_name)->fields($fields);
      foreach ($data as $item) {
        $query->values($item);
      }
      $result = $query->execute();
    }
    return $result;
  }

  /**
   * Copy one folder to other with it content.
   *
   * @param string $src
   *   From path.
   * @param string $dst
   *   To path.
   * @param bool $mode
   *   Chmod.
   */
  public static function recurseCopy(string $src, string $dst, $mode = FALSE) {
    $dir = opendir($src);
    @mkdir(dirname($dst));

    while (FALSE !== ($file = readdir($dir))) {
      if ($file != '.' && $file != '..') {
        if (is_dir($src . DIRECTORY_SEPARATOR . $file)) {
          self::recurseCopy($src . DIRECTORY_SEPARATOR . $file, $dst . DIRECTORY_SEPARATOR . $file);
        }
        else {
          $file_dst_path = $dst . DIRECTORY_SEPARATOR . $file;
          copy($src . DIRECTORY_SEPARATOR . $file, $file_dst_path);
          if ($mode) {
            drupal_chmod($file_dst_path, $mode);
          }
        }
      }
    }
    closedir($dir);
  }

  /**
   * Get weight.
   *
   * @return mixed
   *   Array with data or FALSE.
   *
   * @throws \ServicesException
   */
  public static function getWeight() {
    $fields = array(['alias' => WeightModel::$alias, 'fields' => '']);
    $expressions = array(['expression' => 'isNull', 'value' => 'request_id']);
    $fetching = ['fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC];

    return WeightModel::selectWeight([], $fields, [], $expressions, $fetching);
  }

  /**
   * Get filters data.
   *
   * @return mixed
   *   Array with data or FALSE.
   *
   * @throws \ServicesException
   */
  public static function getFilters() {
    $fields = array(
      ['alias' => FilterModel::$alias, 'fields' => []],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return FilterModel::selectFilter([], $fields, [], [], $fetching);
  }

  /**
   * Get Filter relations.
   *
   * @return mixed
   *   Array with data  or FALSE.
   *
   * @throws \ServicesException
   */
  public static function getFiltersRelations() {
    $joins = array(
      [
        'table' => RoomModel::$table,
        'alias' => RoomModel::$alias,
        'condition' => 'mr.room_id = mfr.room_id',
      ],
    );
    $fields = array(
      ['alias' => FilterRelationModel::$alias, 'fields' => []],
    );
    $conditions = array(
      [
        'field' => 'type',
        'value' => MoveInventoryRoomType::DEFAULT,
        'operation' => '=',
      ],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return FilterRelationModel::selectFilterRelation($joins, $fields, $conditions, [], $fetching);
  }

}
