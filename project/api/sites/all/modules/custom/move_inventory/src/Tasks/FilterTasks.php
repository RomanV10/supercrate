<?php

namespace Drupal\move_inventory\Tasks;

use Drupal\move_inventory\Models\FilterModel;
use Drupal\move_inventory\Models\FilterRelationModel;
use Drupal\move_inventory\Models\ItemRelationModel;
use Drupal\move_inventory\Models\WeightModel;
use Drupal\move_inventory\Util\enum\MoveInventoryEntity;

/**
 * Class FilterTasks.
 *
 * @package Drupal\move_inventory\Tasks
 */
class FilterTasks extends AbstractTasks {

  /**
   * Delete filters from weight table with room condition.
   *
   * @param int $filterId
   *   Filter id.
   * @param array $room_for_remove
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function deleteFilterFromWeightWithRoom(int $filterId, array $room_for_remove) {
    $conditions = array(
      ['field' => 'room_id', 'value' => $room_for_remove, 'operation' => 'IN'],
      ['field' => 'entity_id', 'value' => $filterId, 'operation' => '='],
      [
        'field' => 'entity_type',
        'value' => MoveInventoryEntity::FILTER,
        'operation' => '=',
      ],
    );
    WeightModel::deleteWeight($conditions);
  }

  /**
   * Delete filter from weight.
   *
   * @param int $id
   *   Filter id.
   *
   * @throws \ServicesException
   */
  public static function deleteFilterFromWeight(int $id) {
    $conditions = array(
      ['field' => 'entity_id', 'value' => $id, 'operation' => '='],
      [
        'field' => 'entity_type',
        'value' => MoveInventoryEntity::FILTER,
        'operation' => '=',
      ],
    );
    WeightModel::deleteWeight($conditions);
  }

  /**
   * Insert weight for filter.
   *
   * @param int $filterId
   *   Filter id.
   * @param int $roomId
   *   Room id.
   *
   * @throws \Exception
   */
  public static function insertFilterWeight(int $filterId, int $roomId) {
    $fields = array(
      'entity_id' => $filterId,
      'entity_type' => MoveInventoryEntity::FILTER,
      'room_id' => $roomId,
    );
    WeightModel::insertWeight($fields);
  }

  /**
   * Merge weight filter.
   *
   * @param array $filter
   *   Filter array.
   * @param int $room_id
   *   Room id.
   *
   * @throws \Exception
   */
  public static function mergeWeightFilter(array $filter, int $room_id) {
    $keys = array(
      'entity_id' => $filter['filter_id'],
      'entity_type' => MoveInventoryEntity::FILTER,
      'room_id' => $room_id,
    );
    $fields = array(
      'entity_id' => $filter['filter_id'],
      'entity_type' => MoveInventoryEntity::FILTER,
      'room_id' => $room_id,
      'weight' => $filter['weight'],
    );
    WeightModel::mergeWeight($keys, $fields);
  }

  /**
   * Merge weight for sorting.
   *
   * @param array $fields
   *   Fields.
   * @param int $weight
   *   Weight.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeWeightForSorting(array $fields, int $weight) {
    $keys = $fields;
    $fields['weight'] = $weight;
    WeightModel::mergeWeight($keys, $fields);
  }

  /**
   * Insert filter in db.
   *
   * @param array $filter
   *   Filter data.
   *
   * @return \DatabaseStatementInterface|int
   *   Id filter.
   *
   * @throws \Exception
   */
  public static function createFilter(array $filter) {
    return FilterModel::insertFilter($filter);
  }

  /**
   * Delete filter.
   *
   * @param int $id
   *   Filter id.
   *
   * @throws \ServicesException
   */
  public static function deleteFilter(int $id) {
    $conditions = array(
      ['field' => 'filter_id', 'value' => $id, 'operation' => '='],
    );
    FilterModel::deleteFilter($conditions);
  }

  /**
   * Remove rooms from filter relation table.
   *
   * @param int $filterId
   *   Filter id.
   * @param array $room_for_remove
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function removeRoomsFromFilterRelation(int $filterId, array $room_for_remove) {
    $conditions = array(
      ['field' => 'filter_id', 'value' => $filterId, 'operation' => '='],
      ['field' => 'room_id', 'value' => $room_for_remove, 'operation' => 'IN'],
    );
    FilterRelationModel::deleteFilterRelation($conditions);
  }

  /**
   * Delete filter from relation.
   *
   * @param int $filterId
   *   Filter id.
   *
   * @throws \ServicesException
   */
  public static function deleteFilterFromRelations(int $filterId) {
    $conditions = array(
      ['field' => 'filter_id', 'value' => $filterId, 'operation' => '='],
    );
    FilterRelationModel::deleteFilterRelation($conditions);
  }

  /**
   * Update filter data.
   *
   * @param int $id
   *   Filter id.
   * @param array $filter
   *   Filter data.
   *
   * @return \DatabaseStatementInterface
   *   Row affected.
   *
   * @throws \ServicesException
   */
  public static function updateFilter(int $id, array $filter) {
    $conditions = array(
      ['field' => 'filter_id', 'value' => $id, 'operation' => '='],
    );

    return FilterModel::updateFilter($filter, $conditions);
  }

  /**
   * Merge filter relation.
   *
   * @param int $filter_id
   *   Filter id.
   * @param int $room_id
   *   Room id.
   *
   * @throws \Exception
   */
  public static function mergeFilterRelation(int $filter_id, int $room_id) {
    $keys = array(
      'filter_id' => $filter_id,
      'room_id' => $room_id,
    );
    $fields = array(
      'filter_id' => $filter_id,
      'room_id' => $room_id,
    );
    FilterRelationModel::mergeFilterRelation($keys, $fields);
  }

  /**
   * Retrieve filter.
   *
   * @param int $id
   *   Filter id.
   *
   * @return mixed
   *   Filter data.
   *
   * @throws \ServicesException
   */
  public static function retrieveFilter(int $id) {
    $fields = array(
      ['alias' => FilterModel::$alias, 'fields' => []],
    );
    $conditions = array(
      ['field' => 'filter_id', 'value' => $id, 'operation' => '='],
    );
    $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);

    return FilterModel::selectFilter([], $fields, $conditions, [], $fetching);
  }

  /**
   * Get existing room for filter.
   *
   * @param int $filterId
   *   Filter id.
   * @param array $rooms
   *   Filter id.
   *
   * @return mixed
   *   Rooms.
   *
   * @throws \ServicesException
   */
  public static function getExistingRoom(int $filterId, array $rooms) {
    $fields = array(
      ['alias' => FilterRelationModel::$alias, 'fields' => ['room_id']],
    );
    $conditions = array(
      ['field' => 'filter_id', 'value' => $filterId, 'operation' => '='],
    );
    if (!empty($rooms)) {
      $conditions[] = [
        'field' => 'room_id',
        'value' => $rooms,
        'operation' => 'IN',
      ];
    }
    $expressions = array(
      ['expression' => 'distinct', 'value' => 'mfr.filter_id'],
    );
    $fetching = array('fetch' => 'fetchCol', 'value' => '');

    return FilterRelationModel::selectFilterRelation([], $fields, $conditions, $expressions, $fetching) ?? [];
  }

  /**
   * Delete filter from item relations table.
   *
   * @param int $id
   *   Filter id.
   *
   * @throws \ServicesException
   */
  public static function deleteFilterFromItemRelations(int $id) {
    $conditions = array(
      ['field' => 'filter_id', 'value' => $id, 'operation' => '='],
    );

    ItemRelationModel::deleteItemRelation($conditions);
  }

}
