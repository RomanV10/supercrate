<?php

namespace Drupal\move_inventory\Models;

/**
 * Class RoomModel.
 *
 * @package Drupal\move_inventory\Models
 */
class RoomModel extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'mi_rooms';


  public static $alias = 'mr';

  /**
   * Insert room.
   *
   * @param array $fields
   *   Fields.
   *
   * @return \DatabaseStatementInterface|int
   *   Id entity.
   *
   * @throws \Exception
   */
  public static function insertRoom(array $fields) {
    return self::insertEntity(self::$table, $fields);
  }

  /**
   * Update room.
   *
   * @param array $fields
   *   Fields.
   * @param array $conditions
   *   Conditions.
   *
   * @return \DatabaseStatementInterface
   *   Result.
   *
   * @throws \ServicesException
   */
  public static function updateRoom(array $fields = array(), array $conditions = array()) {
    return self::updateEntity(self::$table, $fields, $conditions);
  }

  /**
   * Delete room.
   *
   * @param array $conditions
   *   Array conditions.
   *
   * @return int
   *   Count of deleted row.
   *
   * @throws \ServicesException
   */
  public static function deleteRoom(array $conditions) {
    return self::deleteEntity(self::$table, $conditions);
  }

  /**
   * Select room data.
   *
   * @param array $joins
   *   Array join table.
   * @param array $fields
   *   Array fields.
   * @param array $conditions
   *   Array conditions.
   * @param array $expressions
   *   Array expressions.
   * @param array $fetching
   *   Get result.
   *
   * @return mixed
   *   Result select.
   *
   * @throws \ServicesException
   */
  public static function selectRoom(array $joins = array(), array $fields = array(), array $conditions = array(), array $expressions = array(), array $fetching = array()) {
    return self::selectEntity(
      [
        'table' => self::$table,
        'alias' => self::$alias,
      ],
      $joins, $fields, $conditions, $expressions, $fetching);
  }

}
