<?php

namespace Drupal\move_inventory\ValidationRules;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class TeamRetrieve.
 *
 * @package Drupal\move_dispatch\ValidationRules\Team
 */
class GetInventoryRequest implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'entity_id' => ['required', 'int'],
    ];
  }

}
