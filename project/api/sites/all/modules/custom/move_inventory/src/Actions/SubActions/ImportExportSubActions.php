<?php

namespace Drupal\move_inventory\Actions\SubActions;

use Drupal\move_inventory\Tasks\ImportExportTasks;
use Drupal\move_inventory\Tasks\ItemTasks;
use Drupal\move_inventory\Tasks\RoomTasks;
use ZipArchive;

/**
 * Class ImportExportSubActions.
 *
 * @package Drupal\move_inventory\Actions\SubActions
 */
class ImportExportSubActions extends AbstractSubActions {
  private $images = [];
  private $exportEntities = [
    'Items' => ['getItems', 'getItemsRelations'],
    'Filters' => ['getFilters', 'getFiltersRelations'],
    'Rooms' => ['getRooms'],
    'Weight' => ['getWeight'],
  ];

  /**
   * Add data from db to zip archive.
   */
  public function addDataToZip() {
    $result = array();
    foreach ($this->exportEntities as $entity_name => $properties_array) {
      foreach ($properties_array as $property) {
        if (method_exists($this, $property)) {
          if ($response = $this->$property()) {
            $result[$property] = $response;
          }
        }
      }
    }

    return $json_data = drupal_json_encode($result);
  }

  /**
   * Get rooms data.
   *
   * @return mixed
   *   Updated number or FALSE.
   *
   * @throws \ServicesException
   */
  public function getRooms() {
    $result = RoomTasks::getRooms();
    if (!empty($result)) {
      $this->images = ImportExportTasks::setImagesFromResult($result, $this->images);
    }

    return $result;
  }

  /**
   * Get filters data.
   *
   * @return mixed
   *   Array with data or FALSE.
   *
   * @throws \ServicesException
   */
  public function getFilters() {
    return ImportExportTasks::getFilters();
  }

  /**
   * Get items data.
   *
   * @return mixed
   *   Array with data or FALSE.
   *
   * @throws \ServicesException
   */
  public function getItems() {
    $result = ItemTasks::getItems();
    if (!empty($result)) {
      ImportExportTasks::setImagesFromResult($result, $this->images);
    }

    return $result;
  }

  /**
   * Get Items Relation data.
   *
   * @return mixed
   *   Array with data or FALSE.
   *
   * @throws \ServicesException
   */
  public function getItemsRelations() {
    return ItemTasks::getItemsRelations();
  }

  /**
   * Get Filter relations.
   *
   * @return mixed
   *   Array with data  or FALSE.
   *
   * @throws \ServicesException
   */
  public function getFiltersRelations() {
    return ImportExportTasks::getFiltersRelations();
  }

  /**
   * Get weight.
   *
   * @return mixed
   *   Array with data or FALSE.
   *
   * @throws \ServicesException
   */
  public function getWeight() {
    return ImportExportTasks::getWeight();
  }

  /**
   * Add images to zip archive.
   *
   * @param \ZipArchive $zip
   *   Zip object.
   */
  public function addImagesToZip(ZipArchive &$zip) {
    foreach ($this->images as $file_path) {
      if (!empty($file_path)) {
        $api_search = strpos($file_path, '/api.');
        $move_board_search = strpos($file_path, '/moveBoard/');
        $public_search = strpos($file_path, 'public://');

        $path_array = explode('/', $file_path);
        $file_name = end($path_array);

        if ($public_search !== FALSE) {
          $file_path = drupal_realpath($file_path);
          $file_name = 'inventory/' . $file_name;
        }

        if ($api_search !== FALSE) {
          $uri = 'public://inventory/' . $file_name;
          $file_path = drupal_realpath($uri);
          $file_name = 'inventory/' . $file_name;
        }

        if ($move_board_search !== FALSE) {
          $file_path = '/app/project/front' . $file_path;
          $file_name = 'moveboard/' . $file_name;
        }
        $zip->addFile($file_path, $file_name);
      }
    }
  }

  /**
   * Set Rooms data.
   *
   * @param array $data
   *   Array with info to add.
   * @param array $context
   *   Batch context.
   *
   * @return \DatabaseStatementInterface|int
   *   1 if updated, 0 - if not.
   *
   * @throws \Exception
   */
  public static function setRooms(array $data, array &$context = array()) {
    $table_name = 'mi_rooms';
    ImportExportTasks::cleanOldData($table_name, $context);

    return ImportExportTasks::insertData($data, $table_name);
  }

  /**
   * Set filter data.
   *
   * @param array $data
   *   Array with info to add.
   * @param array $context
   *   Batch context.
   *
   * @return \DatabaseStatementInterface|int
   *   1 if updated, 0 - if not.
   *
   * @throws \Exception
   */
  public static function setFilters(array $data, array &$context = array()) {
    $table_name = 'mi_filter';
    ImportExportTasks::cleanOldData($table_name, $context);

    return ImportExportTasks::insertData($data, $table_name);
  }

  /**
   * Set items data.
   *
   * @param array $data
   *   Array with info to add.
   * @param array $context
   *   Batch context.
   *
   * @return \DatabaseStatementInterface|int
   *   1 if updated, 0 - if not.
   *
   * @throws \Exception
   */
  public static function setItems(array $data, array &$context = array()) {
    $table_name = 'mi_items';
    ImportExportTasks::cleanOldData($table_name, $context);

    return ImportExportTasks::insertData($data, $table_name);
  }

  /**
   * Set items relations data.
   *
   * @param array $data
   *   Array with info to add.
   *   MoveInventoryImportExport instance.
   * @param array $context
   *   Batch context.
   *
   * @return \DatabaseStatementInterface|int
   *   1 if updated, 0 - if not.
   *
   * @throws \Exception
   */
  public static function setItemsRelations(array $data, array &$context) {
    $table_name = 'mi_items_relations';
    ImportExportTasks::cleanOldData($table_name, $context);

    return ImportExportTasks::insertData($data, $table_name);
  }

  /**
   * Set filter relations data.
   *
   * @param array $data
   *   Array with info to add.
   * @param array $context
   *   Batch context.
   *
   * @return \DatabaseStatementInterface|int
   *   1 if updated, 0 - if not.
   *
   * @throws \Exception
   */
  public static function setFiltersRelations(array $data, array &$context = array()) {
    $table_name = 'mi_filter_relations';
    ImportExportTasks::cleanOldData($table_name, $context);

    return ImportExportTasks::insertData($data, $table_name);
  }

  /**
   * Set weight data.
   *
   * @param array $data
   *   Array with info to add.
   * @param array $context
   *   Batch context.
   *
   * @return \DatabaseStatementInterface|int
   *   1 if updated, 0 - if not.
   *
   * @throws \Exception
   */
  public static function setWeight(array $data, array &$context = array()) {
    $table_name = 'mi_weight';
    ImportExportTasks::cleanOldData($table_name, $context);

    return ImportExportTasks::insertData($data, $table_name);
  }

}
