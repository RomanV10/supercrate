<?php

namespace Drupal\move_inventory\Actions\SubActions;

use Drupal\move_inventory\Configs\Config;
use Drupal\move_inventory\Entity\Item;
use Drupal\move_inventory\Models\ItemModel;
use Drupal\move_inventory\Tasks\FilterTasks;
use Drupal\move_inventory\Tasks\InventoryTasks;
use Drupal\move_inventory\Tasks\ItemTasks;
use Drupal\move_inventory\Util\enum\MoveInventoryItemType;

/**
 * Class ItemSubActions.
 *
 * @package Drupal\move_inventory\Actions\SubActions
 */
class ItemSubActions extends AbstractSubActions implements SubActionsInterface {

  /**
   * Create service.
   *
   * @param array $data
   *   Data for create entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function create(array $data = array()) {
    $data['image_link'] = !empty($data['uri']) ? $data['uri'] : $data['image_link'];
    $item = (array) $this->serializer->deserialize(json_encode($data), Item::class, 'json');
    if ($item['type'] == MoveInventoryItemType::CUSTOM) {
      // Custom item.
      $item['item_id'] = ItemTasks::createCustomItem($item);
      $item['id'] = $this->addCustomItem($item);
    }
    else {
      // Usual and Boxes.
      $item['item_id'] = ItemTasks::createDefaultBoxedItem($item);
      $item['uri'] = $item['image_link'];
      $item['image_link'] = file_create_url($item['image_link']);

      $this->updateItemRelations($item['item_id'], $item['filters']);
    }

    return $item;
  }

  /**
   * Retrieve service.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function retrieve() {
    $item = array();
    $item_data = ItemTasks::retrieveItem($this->getId());
    if (!empty($item_data)) {
      $filters = array();
      foreach ($item_data as $row) {
        $filters[] = $row['filter_id'];
        $item = $row;
      }
      unset($item['weight']);
      unset($item['filter_id']);
      $item['filters'] = $filters;
    }

    if (!empty($item['image_link'])) {
      $item['uri'] = $item['image_link'];
      $item['image_link'] = $this->showPresetUrl($item['image_link']);
    }

    return $item;
  }

  /**
   * Service for update entity.
   *
   * @param array $data
   *   Data for update entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function update(array $data = array()) {
    $data['image_link'] = !empty($data['uri']) ? $data['uri'] : $data['image_link'];

    $item = (array) $this->serializer->deserialize(json_encode($data), Item::class, 'json');
    $result = ItemTasks::updateItem($this->getId(), $item);
    $this->updateItemRelations($this->getId(), $item['filters']);

    return $result;
  }

  /**
   * Service for remove entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function delete() {
    $count = InventoryTasks::countItemInRequest($this->getId());
    if ($count) {
      services_error("We can`t delete inventory item!", 500);
    }
    $conditions = array(
      ['field' => 'item_id', 'value' => $this->getId(), 'operation' => '='],
    );
    ItemModel::deleteItem($conditions);
    ItemTasks::deleteFromItemRelationsWithoutFilter($this->getId());
    ItemTasks::deleteWeight($this->getId());

    return TRUE;
  }

  /**
   * Insert weight for item. Before delete old.
   *
   * @param int $item_id
   *   Item id.
   * @param int $filter_id
   *   Filter id.
   * @param int $room_id
   *   Room id.
   *
   * @throws \Exception
   */
  public function insertWeight(int $item_id, int $filter_id, int $room_id) {
    ItemTasks::deleteWeightForItem($item_id, $filter_id, $room_id);
    ItemTasks::insertWeightForItem($item_id, $room_id, $filter_id, Config::getConfig()->getMinWeightForItem());
  }

  /**
   * Add custom item to request.
   *
   * @param array $data
   *   Data for update. If isset $data['id'] = it will update item.
   *
   * @return \DatabaseStatementInterface|int|mixed
   *   Id of custom item.
   *
   * @throws \Exception
   */
  public function addCustomItem(array $data) {
    $data['entity_id'] = $this->getEntityIdStoragePacking($data['entity_id']);
    $data = ItemTasks::prepareDataForAddItem($data);
    if (!empty($data['id'])) {
      InventoryTasks::updateItemInRequest($data);
      $id = $data['id'];
    }
    else {
      $id = InventoryTasks::insertItemInRequest($data);
    }
    return $id;
  }

  /**
   * Update relations between items and filters.
   *
   * @param int $item_id
   *   ID of the item.
   * @param array $filters_ids
   *   Array of filters ids.
   *
   * @throws \Exception
   */
  public function updateItemRelations(int $item_id, array $filters_ids) {
    $existing_filers = ItemTasks::getExistingFilter($item_id);
    // Check and remove not needed filters.
    $filter_for_remove = array_diff($existing_filers, $filters_ids);
    if (!empty($filter_for_remove)) {
      // Remove filters from item relation table.
      ItemTasks::deleteFromItemRelations($item_id, $filter_for_remove);
      // Clean weight table.
      ItemTasks::deleteWeightForItemWithoutRoom($item_id, $filter_for_remove);
    }
    // Keep only new filters.
    $filters_ids = array_diff($filters_ids, $existing_filers);
    foreach ($filters_ids as $filter_id) {
      ItemTasks::mergeItemRelations($filter_id, $item_id);
      foreach (FilterTasks::getExistingRoom($filter_id, []) as $room_id) {
        $this->insertWeight($item_id, $filter_id, $room_id);
      }
    }
  }

}
