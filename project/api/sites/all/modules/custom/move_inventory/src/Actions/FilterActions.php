<?php

namespace Drupal\move_inventory\Actions;

use Drupal\move_inventory\Actions\SubActions\FilterSubActions;
use Drupal\move_inventory\Tasks\FilterTasks;
use Drupal\move_inventory\Tasks\ItemTasks;

/**
 * Class MoveInventoryFilter.
 *
 * @package Drupal\move_inventory\Services
 */
class FilterActions extends AbstractActions implements BaseCRUDInterface {

  /**
   * Create filter.
   *
   * @param array $data
   *   Array of filter data for save.
   *
   * @return array|mixed
   *   Array of filter data with filter id.
   *
   * @throws \Exception
   */
  public function create(array $data = array()) {
    return (new FilterSubActions())->create($data);
  }

  /**
   * Retrieve filter.
   *
   * @return array|mixed
   *   Filter data.
   *
   * @throws \Exception
   */
  public function retrieve() {
    $filterSubActions = new FilterSubActions();

    $filterSubActions->setId($this->getId());
    return $filterSubActions->retrieve();
  }

  /**
   * Update filter.
   *
   * @param array $data
   *   Data for update.
   *
   * @return bool|\DatabaseStatementInterface|mixed
   *   Result update.
   *
   * @throws \Exception
   */
  public function update(array $data = array()) {
    $filterSubActions = new FilterSubActions();

    $filterSubActions->setId($this->getId());
    return $filterSubActions->update($data);
  }

  /**
   * Delete filter.
   *
   * @return bool|mixed
   *   Result delete.
   *
   * @throws \Exception
   */
  public function delete() {
    $filterSubActions = new FilterSubActions();

    $filterSubActions->setId($this->getId());
    return $filterSubActions->delete();
  }

  /**
   * Get all filters, index method.
   *
   * @param array $conditions
   *   Conditions.
   *
   * @return array|mixed
   *   Filters.
   *
   * @throws \Exception
   */
  public function getAllFilters(array $conditions) {
    return (new FilterSubActions())->index($conditions);
  }

  /**
   * Update weight.
   *
   * @param mixed $data
   *   Data.
   * @param int $entity_type
   *   Entity type.
   *
   * @throws \Exception
   */
  public function updateWeight($data, int $entity_type) {
    foreach ($data as $val) {
      $parent = $val['parents_ids'];
      // Remove default room value for this item.
      if (!empty($parent)) {
        ItemTasks::deleteWeightForItemWithoutFilter($val['id']);
        $fields['room_id'] = $parent;
      }
      $fields['entity_id'] = $val['id'];
      $fields['entity_type'] = $entity_type;
      FilterTasks::mergeWeightForSorting($fields, $val['weight']);
    }
  }

}
