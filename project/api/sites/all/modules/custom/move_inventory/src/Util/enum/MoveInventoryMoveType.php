<?php

namespace Drupal\move_inventory\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class MoveInventoryDisplay.
 *
 * @package Drupal\move_inventory\Util
 */
class MoveInventoryMoveType extends BaseEnum {
  const NO_MOVED = 0;
  const MOVED = 1;
}
