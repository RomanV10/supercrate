<?php

namespace Drupal\move_inventory\Actions;

use Drupal\move_inventory\Actions\SubActions\FileSubActions;

/**
 * Class MoveInventoryFile.
 *
 * @package Drupal\move_inventory\Services
 */
class FileActions extends AbstractActions implements BaseCRUDInterface {

  /**
   * Download and save file.
   *
   * @param array $data
   *   Array.
   *
   * @return mixed
   *   Error or array.
   *
   * @throws \Exception
   */
  public function create(array $data = array()) {
    return (new FileSubActions())->create($data);
  }

  /**
   * Service for update entity.
   *
   * @param array $data
   *   Data for update entity.
   *
   * @return mixed
   *   Result of operation.
   */
  public function update(array $data = array()) {
    return NULL;
  }

  /**
   * Retrieve service.
   *
   * @return mixed
   *   Result of operation.
   */
  public function retrieve() {
    return NULL;
  }

  /**
   * Service for remove entity.
   *
   * @return mixed
   *   Result of operation.
   */
  public function delete() {
    return NULL;
  }

}
