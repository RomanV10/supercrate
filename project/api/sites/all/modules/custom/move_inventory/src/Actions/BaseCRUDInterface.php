<?php

namespace Drupal\move_inventory\Actions;

/**
 * Interface BaseServiceInterface.
 *
 * @package Drupal\move_inventory\Services
 */
interface BaseCRUDInterface {

  /**
   * Create service.
   *
   * @param array $data
   *   Data for create entity.
   *
   * @return mixed
   *   Result of operation.
   */
  public function create(array $data = array());

  /**
   * Retrieve service.
   *
   * @return mixed
   *   Result of operation.
   */
  public function retrieve();

  /**
   * Service for update entity.
   *
   * @param array $data
   *   Data for update entity.
   *
   * @return mixed
   *   Result of operation.
   */
  public function update(array $data = array());

  /**
   * Service for remove entity.
   *
   * @return mixed
   *   Result of operation.
   */
  public function delete();

}
