<?php

namespace Drupal\move_inventory\Models;

/**
 * Class RoomRelationModel.
 *
 * @package Drupal\move_inventory\Models
 */
class RoomRelationModel extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'mi_rooms_relations';

  /**
   * The table alias associated with the model.
   *
   * @var string
   */
  public static $alias = 'mrr';

  /**
   * Insert room relation.
   *
   * @param array $fields
   *   Fields.
   *
   * @return \DatabaseStatementInterface|int
   *   Id entity.
   *
   * @throws \Exception
   */
  public static function insertRoomRelation(array $fields) {
    return self::insertEntity(self::$table, $fields);
  }

  /**
   * Delete room relation.
   *
   * @param array $conditions
   *   Array conditions.
   *
   * @return int
   *   Count of deleted row.
   *
   * @throws \ServicesException
   */
  public static function deleteRoomRelation(array $conditions) {
    return self::deleteEntity(self::$table, $conditions);
  }

  /**
   * Select room relation data.
   *
   * @param array $joins
   *   Array join table.
   * @param array $fields
   *   Array fields.
   * @param array $conditions
   *   Array conditions.
   * @param array $expressions
   *   Array expressions.
   * @param array $fetching
   *   Get result.
   *
   * @return mixed
   *   Result select.
   *
   * @throws \ServicesException
   */
  public static function selectRoomRelation(array $joins = array(), array $fields = array(), array $conditions = array(), array $expressions = array(), array $fetching = array()) {
    return self::selectEntity(
      [
        'table' => self::$table,
        'alias' => self::$alias,
      ],
      $joins, $fields, $conditions, $expressions, $fetching);
  }

}
