<?php

namespace Drupal\move_inventory\Entity;

/**
 * Class Package.
 *
 * @package Drupal\move_inventory\Entity
 */
class Package {

  /**
   * Name.
   *
   * @var string
   */
  public $name = '';

  /**
   * Get name.
   *
   * @return string
   *   Name.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Set name.
   *
   * @param string $name
   *   Name.
   */
  public function setName(string $name) {
    $this->name = $name;
  }

}
