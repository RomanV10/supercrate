<?php

namespace Drupal\move_inventory\Routes;

use Drupal\move_inventory\Controllers\RoomController;

/**
 * Class RoomRoute.
 *
 * @package Drupal\move_inventory\Routes
 */
class RoomRoute {

  /**
   * Room routes.
   *
   * @return array
   *   Definition.
   */
  public static function getRoutes() {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_inventory',
      'name' => 'src/Controllers/RoomController',
    ];

    return array(
      'move_inventory_room' => array(
        'operations' => array(
          'create' => array(
            'callback' => RoomController::class . '::createInventoryRoom',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'callback' => RoomController::class . '::updateInventoryRoom',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete' => array(
            'callback' => RoomController::class . '::deleteInventoryRoom',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'sorting' => array(
            'callback' => RoomController::class . '::roomSorting',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_rooms' => array(
            'callback' => RoomController::class . '::getRooms',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'conditions',
                'type' => 'array',
                'source' => array('data' => 'conditions'),
                'optional' => TRUE,
                'default value' => array(),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

}
