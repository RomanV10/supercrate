<?php

namespace Drupal\move_inventory\Tasks;

/**
 * Class FileTasks.
 *
 * @package Drupal\move_inventory\Tasks
 */
class FileTasks extends AbstractTasks {

  /**
   * Check file name.
   *
   * @param string $name
   *   Name.
   *
   * @return mixed
   *   Allowed name.
   */
  public static function servicesFileCheckName(string $name) {
    // Punctuation characters that are allowed, but not as first/last character.
    $punctuation = '-_. ';

    $map = array(
      // Replace (groups of) whitespace characters.
      '!\s+!' => ' ',
      // Replace multiple dots.
      '!\.+!' => '.',
      // Remove characters that are not alphanumeric or the allowed punctuation.
      "![^0-9A-Za-z$punctuation]!" => '',
    );

    // Apply the regex replacements. Remove any leading or hanging punctuation.
    return str_replace(' ', '-', trim(preg_replace(array_keys($map), array_values($map), $name), $punctuation));
  }

  /**
   * Check if allowed extension.
   *
   * @param string $extension
   *   Extension.
   * @param array $allowExt
   *   Allowed extensions.
   *
   * @return bool
   *   True, false.
   */
  public static function checkImageExt(string $extension, array $allowExt) {
    $type = preg_replace('/image\//', '', $extension);
    if (array_search($type, $allowExt)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}
