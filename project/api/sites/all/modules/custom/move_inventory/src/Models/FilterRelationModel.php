<?php

namespace Drupal\move_inventory\Models;

/**
 * Class FilterRelationModel.
 *
 * @package Drupal\move_inventory\Models
 */
class FilterRelationModel extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'mi_filter_relations';

  /**
   * The table alias associated with the model.
   *
   * @var string
   */
  public static $alias = 'mfr';

  /**
   * Delete filter with conditions.
   *
   * @param array $conditions
   *   Array conditions.
   *
   * @throws \ServicesException
   */
  public static function deleteFilterRelation(array $conditions) {
    self::deleteEntity(self::$table, $conditions);
  }

  /**
   * Merge filter.
   *
   * @param array $keys
   *   Keys.
   * @param array $fields
   *   Fields.
   * @param array $expressions
   *   Expressions example isNull, count, avg etc.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeFilterRelation(array $keys, array $fields, array $expressions = array()) {
    self::mergeEntity(self::$table, $keys, $fields, $expressions);
  }

  /**
   * Select filter.
   *
   * @param array $joins
   *   Array join table.
   * @param array $fields
   *   Array fields.
   * @param array $conditions
   *   Array conditions.
   * @param array $expressions
   *   Array expressions.
   * @param array $fetching
   *   Get result.
   *
   * @return mixed
   *   Result select.
   *
   * @throws \ServicesException
   */
  public static function selectFilterRelation(array $joins = array(), array $fields = array(), array $conditions = array(), array $expressions = array(), array $fetching = array()) {
    return self::selectEntity(
      [
        'table' => self::$table,
        'alias' => self::$alias,
      ],
      $joins, $fields, $conditions, $expressions, $fetching);
  }

}
