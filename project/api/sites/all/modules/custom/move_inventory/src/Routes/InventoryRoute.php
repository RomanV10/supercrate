<?php

namespace Drupal\move_inventory\Routes;

use Drupal\move_inventory\Controllers\InventoryController;
use Drupal\move_inventory\ValidationRules\CheckIfChangeMoveSize;

/**
 * Class Service.
 *
 * @package Drupal\move_inventory\Routes
 */
class InventoryRoute {

  /**
   * Definition schema.
   *
   * @return array
   *   .
   */
  public static function getRoutes() {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_inventory',
      'name' => 'src/Controllers/InventoryController',
    ];

    return array(
      'move_inventory' => array(
        'operations' => array(),
        'actions' => array(
          'check_exist_add_inventory' => array(
            'callback' => InventoryController::class . '::checkExistAddInventory',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'remove_unnecessary_items' => array(
            'callback' => InventoryController::class . '::removeUnnecessaryItems',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'check_if_change_move_size' => array(
            'callback' => InventoryController::class . '::checkIfChangeMoveSize',
            'rules' => CheckIfChangeMoveSize::class,
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'selected',
                'type' => 'int',
                'source' => array('data' => 'selected'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'create_custom_room' => array(
            'callback' => InventoryController::class . '::createCustomRoom',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'conditions',
                'type' => 'int',
                'source' => array('data' => 'conditions'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete_custom_room' => array(
            'callback' => InventoryController::class . '::deleteCustomRoom',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'room_id',
                'type' => 'int',
                'source' => array('data' => 'room_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'move_item' => array(
            'callback' => InventoryController::class . '::moveItems',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'room_id',
                'type' => 'int',
                'source' => array('data' => 'room_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'cancel_inventory_changes' => array(
            'callback' => InventoryController::class . '::cancelInventoryChanges',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'back_up',
                'type' => 'int',
                'source' => array('data' => 'back_up'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'transfer_inventory' => array(
            'callback' => InventoryController::class . '::transferInventory',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id_from',
                'type' => 'int',
                'source' => array('data' => 'entity_id_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_id_to',
                'type' => 'int',
                'source' => array('data' => 'entity_id_to'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'from_inventory_tab_name',
                'type' => 'int',
                'source' => array('data' => 'from_inventory_tab_name'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

}
