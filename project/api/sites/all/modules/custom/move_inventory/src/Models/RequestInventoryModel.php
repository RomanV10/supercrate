<?php

namespace Drupal\move_inventory\Models;

use Drupal\move_inventory\Util\enum\MoveInventoryItemType;
use Drupal\move_services_new\Util\enum\EntityTypes;
use PDO;

/**
 * Class RequestInventoryModel.
 *
 * @package Drupal\move_inventory\Models
 */
class RequestInventoryModel extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'mi_request_inventory';


  public static $alias = 'mri';

  /**
   * Merge request inventory.
   *
   * @param array $keys
   *   Keys.
   * @param array $fields
   *   Fields.
   * @param array $expressions
   *   Expressions example isNull, count, avg etc.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeInventory(array $keys, array $fields, array $expressions = array()) {
    self::mergeEntity(self::$table, $keys, $fields, $expressions);
  }

  /**
   * Delete inventory.
   *
   * @param array $conditions
   *   Array conditions.
   *
   * @return int
   *   Count of deleted row.
   *
   * @throws \ServicesException
   */
  public static function deleteInventory(array $conditions) {
    return self::deleteEntity(self::$table, $conditions);
  }

  /**
   * Update request inventory.
   *
   * @param array $fields
   *   Fields.
   * @param array $conditions
   *   Conditions.
   *
   * @return \DatabaseStatementInterface
   *   Result.
   *
   * @throws \ServicesException
   */
  public static function updateRequestInventory(array $fields = array(), array $conditions = array()) {
    return self::updateEntity(self::$table, $fields, $conditions);
  }

  /**
   * Insert request inventory.
   *
   * @param array $fields
   *   Fields.
   *
   * @return \DatabaseStatementInterface|int
   *   Id entity.
   *
   * @throws \Exception
   */
  public static function insertRequestInventory(array $fields) {
    return self::insertEntity(self::$table, $fields);
  }

  /**
   * Select request inventory data.
   *
   * @param array $joins
   *   Array join table.
   * @param array $fields
   *   Array fields.
   * @param array $conditions
   *   Array conditions.
   * @param array $expressions
   *   Array expressions.
   * @param array $fetching
   *   Get result.
   *
   * @return mixed
   *   Result select.
   *
   * @throws \ServicesException
   */
  public static function selectRequestInventory(array $joins = array(), array $fields = array(), array $conditions = array(), array $expressions = array(), array $fetching = array()) {
    return self::selectEntity(
      [
        'table' => self::$table,
        'alias' => self::$alias,
      ],
      $joins, $fields, $conditions, $expressions, $fetching);
  }

  /**
   * Get inventory totals for entity.
   *
   * @param array|null $conditions
   *   Conditions.
   *
   * @return mixed
   *   Totals.
   */
  public static function getEntityInventoryTotals(?array $conditions) {
    $query = db_select('mi_request_inventory', 'mri');
    if (!empty($conditions)) {
      foreach ($conditions as $field_name => $val) {
        $operator = is_array($val) ? "IN" : "=";
        $query->condition($field_name, $val, $operator);
      }
    }
    $query->condition('entity_id', $conditions['entity_id']);
    $query->condition('entity_type', EntityTypes::MOVEREQUEST);
    $query->addExpression('SUM(count)', 'total_count');
    $query->addExpression('SUM(cf * count)', 'total_cf');

    return $query->execute()->fetchAssoc(PDO::FETCH_ASSOC);
  }

  /**
   * Get inventory for entity.
   *
   * @param array|null $conditions
   *   Conditions.
   *
   * @return mixed
   *   Inventory items.
   */
  public static function getInventoryItems(?array $conditions) {
    $query = db_select('mi_request_inventory', 'mri');
    if (!empty($conditions)) {
      foreach ($conditions as $field_name => $val) {
        $operator = is_array($val) ? "IN" : "=";
        $query->condition($field_name, $val, $operator);
      }
    }
    $query->condition('entity_id', $conditions['entity_id']);
    $query->condition('entity_type', EntityTypes::MOVEREQUEST);
    $query->fields('mri');

    return $query->execute()->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Get custom item totals in request.
   *
   * @param array|null $conditions
   *   Conditions.
   *
   * @return mixed
   *   Totals.
   */
  public static function getCustomItemTotals(?array $conditions) {
    $query = db_select('mi_request_inventory', 'mri');
    if (!empty($conditions)) {
      foreach ($conditions as $field_name => $val) {
        $operator = is_array($val) ? 'IN' : '=';
        $query->condition($field_name, $val, $operator);
      }
    }
    $query->condition('entity_id', $conditions['entity_id']);
    $query->condition('entity_type', EntityTypes::MOVEREQUEST);
    $query->condition('type', MoveInventoryItemType::CUSTOM);
    $query->addExpression('SUM(count)', 'total_count');
    $query->addExpression('SUM(cf * count)', 'total_cf');

    return $query->execute()->fetchAssoc();
  }

  /**
   * Retrieve only custom inventory.
   *
   * @param int $entityId
   *   Entity id.
   * @param array|null $conditions
   *   Conditions.
   *
   * @return mixed
   *   Custom inventory.
   */
  public static function getCustomInventory(int $entityId, ?array $conditions) {
    $query = db_select('mi_request_inventory', 'mri');
    if (!empty($conditions)) {
      foreach ($conditions as $field_name => $val) {
        $operator = is_array($val) ? 'IN' : '=';
        $query->condition($field_name, $val, $operator);
      }
    }
    $query->condition('entity_id', $entityId);
    $query->condition('entity_type', EntityTypes::MOVEREQUEST);
    $query->condition('type', MoveInventoryItemType::CUSTOM);
    $query->fields('mri');
    $inventory = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
    if (!empty($inventory)) {
      foreach ($inventory as $key => $row) {
        $inventory[$key]['total_cf'] = (float) ($row['cf'] * $row['count']);
        $inventory[$key]['total_count'] = (int) $row['count'];
      }
    }

    return $inventory;
  }

}
