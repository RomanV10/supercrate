<?php

namespace Drupal\move_inventory\Models;

/**
 * Class WeightModel.
 *
 * @package Drupal\move_inventory\Models
 */
class WeightModel extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'mi_weight';

  /**
   * The table alias associated with the model.
   *
   * @var string
   */
  public static $alias = 'mw';

  /**
   * Insert in weight table.
   *
   * @param array $fields
   *   Fields.
   *
   * @throws \Exception
   */
  public static function insertWeight(array $fields) {
    self::insertEntity(self::$table, $fields);
  }

  /**
   * Delete weight with conditions.
   *
   * @param array $conditions
   *   Array conditions.
   *
   * @throws \ServicesException
   */
  public static function deleteWeight(array $conditions) {
    self::deleteEntity(self::$table, $conditions);
  }

  /**
   * Merge weight.
   *
   * @param array $keys
   *   Keys.
   * @param array $fields
   *   Fields.
   * @param array $expressions
   *   Expressions example isNull, count, avg etc.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeWeight(array $keys, array $fields, array $expressions = array()) {
    self::mergeEntity(self::$table, $keys, $fields, $expressions);
  }

  /**
   * Select weight.
   *
   * @param array $joins
   *   Array join table.
   * @param array $fields
   *   Array fields.
   * @param array $conditions
   *   Array conditions.
   * @param array $expressions
   *   Array expressions.
   * @param array $fetching
   *   Get result.
   *
   * @return mixed
   *   Result select.
   *
   * @throws \ServicesException
   */
  public static function selectWeight(array $joins = array(), array $fields = array(), array $conditions = array(), array $expressions = array(), array $fetching = array()) {
    return self::selectEntity(
      [
        'table' => self::$table,
        'alias' => self::$alias,
      ],
      $joins, $fields, $conditions, $expressions, $fetching);
  }

}
