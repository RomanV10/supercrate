<?php

namespace Drupal\move_inventory\Models;

use Drupal\move_inventory\Exceptions\SQLException;

/**
 * Class AbstractModel.
 *
 * @package Drupal\move_inventory\Models
 */
abstract class AbstractModel {

  /**
   * Insert entity.
   *
   * @param string $table
   *   Table name.
   * @param array $fields
   *   Fields.
   *
   * @return \DatabaseStatementInterface|int
   *   Id entity.
   *
   * @throws \Exception
   */
  protected static function insertEntity(string $table, array $fields) {
    try {
      $queryInsert = db_insert($table);
      $queryInsert->fields($fields);
      return $queryInsert->execute();
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

  /**
   * Delete entity.
   *
   * @param string $table
   *   Table name.
   * @param array $conditions
   *   Array conditions.
   *
   * @return int
   *   Count of deleted row.
   *
   * @throws \ServicesException
   */
  protected static function deleteEntity(string $table, array $conditions) {
    try {
      $deleteQuery = db_delete($table);
      if (!empty($conditions)) {
        foreach ($conditions as $condition) {
          $deleteQuery->condition($condition['field'], $condition['value'], $condition['operation']);
        }
      }
      return $deleteQuery->execute();
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

  /**
   * Update entity.
   *
   * @param string $table
   *   Table name.
   * @param array $fields
   *   Fields.
   * @param array $conditions
   *   Conditions.
   *
   * @return \DatabaseStatementInterface
   *   Result.
   */
  protected static function updateEntity(string $table, array $fields = array(), array $conditions = array()) {
    try {
      $updateQuery = db_update($table);

      if (!empty($fields)) {
        $updateQuery->fields($fields);
      }

      if (!empty($conditions)) {
        foreach ($conditions as $condition) {
          $updateQuery->condition($condition['field'], $condition['value'], $condition['operation']);
        }
      }

      return $updateQuery->execute();
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

  /**
   * Merge entity.
   *
   * @param string $table
   *   Table name.
   * @param array $keys
   *   Keys.
   * @param array $fields
   *   Fields.
   * @param array $expressions
   *   Expressions example isNull, count, avg etc.
   */
  protected static function mergeEntity(string $table, array $keys, array $fields, array $expressions = array()) {
    try {
      $queryMerge = db_merge($table);
      $queryMerge->key($keys);
      $queryMerge->fields($fields);
      if (!empty($expressions)) {
        foreach ($expressions as $expression) {
          $queryMerge->{$expression['expression']}($expression['value']);
        }
      }
      $queryMerge->execute();
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

  /**
   * Select entity.
   *
   * @param array $table
   *   Table name and alias.
   * @param array $joins
   *   Array join table.
   * @param array $fields
   *   Array fields.
   * @param array $conditions
   *   Array conditions.
   * @param array $expressions
   *   Array expressions.
   * @param array $fetching
   *   Get result.
   *
   * @return mixed
   *   Result select.
   */
  protected static function selectEntity(array $table, array $joins = array(), array $fields = array(), array $conditions = array(), array $expressions = array(), array $fetching = array()) {
    try {
      $querySelect = db_select($table['table'], $table['alias']);

      if (!empty($joins)) {
        foreach ($joins as $join) {
          $querySelect->leftJoin($join['table'], $join['alias'], $join['condition']);
        }
      }

      if (!empty($fields)) {
        foreach ($fields as $field) {
          $querySelect->fields($field['alias'], !empty($field['fields']) ? $field['fields'] : []);
        }
      }

      if (!empty($conditions)) {
        foreach ($conditions as $condition) {
          $querySelect->condition($condition['field'], $condition['value'], $condition['operation']);
        }
      }

      if (!empty($expressions)) {
        foreach ($expressions as $expression) {
          if (is_array($expression['value'])) {
            $querySelect->{$expression['expression']}($expression['value'][0], $expression['value'][1]);
          }
          else {
            $querySelect->{$expression['expression']}($expression['value']);
          }
        }
      }

      return $querySelect->execute()->{$fetching['fetch']}($fetching['value']);
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

}
