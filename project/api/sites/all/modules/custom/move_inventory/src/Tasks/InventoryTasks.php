<?php

namespace Drupal\move_inventory\Tasks;

use Drupal\move_inventory\Configs\Config;
use Drupal\move_inventory\Models\FilterRelationModel;
use Drupal\move_inventory\Models\RequestInventoryModel;
use Drupal\move_inventory\Models\WeightModel;
use Drupal\move_inventory\Util\enum\MoveInventoryEntity;
use Drupal\move_inventory\Util\enum\MoveInventoryMoveType;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class InventoryTasks.
 *
 * @package Drupal\move_inventory\Tasks
 */
class InventoryTasks extends AbstractTasks {

  /**
   * Update item in request.
   *
   * @param array $data
   *   Data.
   *
   * @throws \ServicesException
   */
  public static function updateItemInRequest(array $data) {
    $id = $data['id'];
    unset($data['id']);
    $conditions = array(
      ['field' => 'id', 'value' => $id, 'operation' => '='],
    );
    RequestInventoryModel::updateRequestInventory($data, $conditions);
  }

  /**
   * Get inventory type for tab name.
   *
   * @param string $fromInventoryTabName
   *   Inventory tab name.
   *
   * @return array|int
   *   Type inventory.
   */
  public static function getInventoryTypeForTabName(string $fromInventoryTabName) {
    // Get inventory for entity from.
    $type_inventory = [0, 1];
    switch ($fromInventoryTabName) {
      case 'addInventoryMoving':
        $type_inventory = 1;
        break;

      case 'secondAddInventoryMoving':
        $type_inventory = 2;
        break;
    }

    return $type_inventory;
  }

  /**
   * Get type inventory from request all data.
   *
   * @param array $request_all_data
   *   Request all data.
   *
   * @return int
   *   Type inventory.
   *
   * @throws \ServicesException
   */
  public static function getTypeInventoryFreeForSaveInNewInventory(array $request_all_data) {
    $type = Config::getConfig()->getDefaultInventoryType();

    if (empty($request_all_data['additional_inventory'])) {
      $type = Config::getConfig()->getAdditionalInventoryType();
    }
    elseif (empty($request_all_data['second_additional_inventory'])) {
      $type = Config::getConfig()->getSecondAdditionalInventory();
    }

    return $type ?: services_error('Cannot copy inventory. Additional inventory is full.', 406);
  }

  /**
   * Prepare log for transfer inventory in to entity.
   *
   * @param array $row
   *   Item data.
   *
   * @return array
   *   Log data.
   */
  public static function prepareLogTo(array $row) {
    $log_data[] = array(
      'details' => array([
        'activity' => 'System',
        'title' => 'Transfer inventory',
        'text' => array([
          'text' => 'Inventory ' . $row['name'] . ' was increased.',
          'from' => 0,
          'to' => $row['count'],
        ],
        ),
        'date' => time(),
      ],
      ),
      'source' => 'System',
    );

    return $log_data;
  }

  /**
   * Prepare log for transfer inventory in from entity.
   *
   * @param int $nid
   *   Node id.
   *
   * @return array
   *   Log data.
   */
  public static function prepareLogFrom(int $nid) {
    $log_data[] = array(
      'details' => array([
        'activity' => 'System',
        'title' => 'Transfer inventory',
        'text' => array([
          'text' => 'Inventory transfer to request ' . $nid,
        ],
        ),
        'date' => time(),
      ],
      ),
      'source' => 'System',
    );

    return $log_data;
  }

  /**
   * Get type inventory free for save on contract.
   *
   * @param array $request_all_data
   *   All data.
   * @param array $to_contract
   *   Contract.
   *
   * @return int
   *   Inventory type.
   */
  public static function getTypeInventoryFreeForSaveOnContract(array $request_all_data, array $to_contract) {
    $type = Config::getConfig()->getDefaultInventoryType();

    // Fake array, like object.
    if (empty($request_all_data['inventory']) && empty($to_contract['factory']['inventoryMoving']['signatures']['length'])) {
      $type = Config::getConfig()->getDefaultInventoryType();
    }
    elseif (empty($request_all_data['additional_inventory'])) {
      $type = Config::getConfig()->getAdditionalInventoryType();
    }
    elseif (empty($request_all_data['second_additional_inventory'])) {
      $type = Config::getConfig()->getSecondAdditionalInventory();
    }

    return $type;
  }

  /**
   * Get tab name field.
   *
   * @param string $field_name
   *   Field name.
   *
   * @return string
   *   Tab name.
   */
  public static function getTabNameByField(string $field_name) {
    $result = 'inventoryMoving';

    if ($field_name == 'additional_inventory') {
      $result = 'addInventoryMoving';
    }
    elseif ($field_name == 'second_additional_inventory') {
      $result = 'secondAddInventoryMoving';
    }

    return $result;
  }

  /**
   * Get field inventory by type.
   *
   * @param int|string $type
   *   Type inventory.
   *
   * @return string
   *   Field inventory.
   */
  public static function getFieldInventoryByType($type) {
    $result = 'inventory';

    if ($type === 'addInventoryMoving' || $type === Config::getConfig()->getAdditionalInventoryType()) {
      $result = 'additional_inventory';
    }
    elseif ($type === 'secondAddInventoryMoving' || $type === Config::getConfig()->getSecondAdditionalInventory()) {
      $result = 'second_additional_inventory';
    }

    return $result;
  }

  /**
   * Insert weight for room in request.
   *
   * @param int $room_id
   *   Room id.
   * @param int $request_id
   *   Request id.
   * @param int $weight
   *   Weight.
   *
   * @throws \Exception
   */
  public static function insertRoomWeightForRequest(int $room_id, int $request_id, int $weight) {
    $keys = array(
      'entity_id' => $room_id,
      'entity_type' => MoveInventoryEntity::ROOM,
      'request_id' => $request_id,
    );
    WeightModel::mergeWeight($keys, ['weight' => $weight]);
  }

  /**
   * Get max weight room for request.
   *
   * @param int $entityId
   *   Entity id.
   *
   * @return mixed
   *   Weight.
   *
   * @throws \ServicesException
   */
  public static function getMaxWeight(int $entityId) {
    $conditions = array(
      ['field' => 'mw.request_id', 'value' => $entityId, 'operation' => '='],
      [
        'field' => 'mw.entity_type',
        'value' => MoveInventoryEntity::ROOM,
        'operation' => '=',
      ],
    );
    $expressions = array(
      ['expression' => 'addExpression', 'value' => 'MAX(weight)'],
    );
    $fetching = ['fetch' => 'fetchField', 'value' => ''];

    return WeightModel::selectWeight([], [], $conditions, $expressions, $fetching);
  }

  /**
   * Get weight room.
   *
   * @param int $room_id
   *   Room id.
   * @param int $requestId
   *   Request id.
   *
   * @return mixed
   *   Weight.
   *
   * @throws \ServicesException
   */
  public static function getWeightForRoomInRequest(int $room_id, int $requestId) {
    $fields = array(
      ['alias' => WeightModel::$alias, 'fields' => ['weight']],
    );
    $conditions = array(
      ['field' => 'mw.entity_id', 'value' => $room_id, 'operation' => '='],
      ['field' => 'mw.request_id', 'value' => $requestId, 'operation' => '='],
      [
        'field' => 'mw.entity_type',
        'value' => MoveInventoryEntity::ROOM,
        'operation' => '=',
      ],
    );
    $fetching = ['fetch' => 'fetchField', 'value' => ''];

    return WeightModel::selectWeight([], $fields, $conditions, [], $fetching);
  }

  /**
   * Get all filters ids for Room.
   *
   * @param int $room_id
   *   Room id.
   * @param bool $withWeight
   *   With weight.
   *
   * @return mixed
   *   Array with filter ids or FALSE.
   *
   * @throws \ServicesException
   */
  public static function getFilterIdsForRoom(int $room_id, bool $withWeight = FALSE) {
    if ($withWeight) {
      $joins = array(
        [
          'table' => WeightModel::$table,
          'alias' => WeightModel::$alias,
          'condition' => 'mw.entity_id = mfr.filter_id AND mfr.room_id =' . $room_id,
        ],
      );
      $fields = array(
        ['alias' => FilterRelationModel::$alias, 'fields' => ['filter_id']],
        ['alias' => WeightModel::$alias, 'fields' => ['weight']],
      );
      $conditions = array(
        ['field' => 'mw.room_id', 'value' => $room_id, 'operation' => '='],
        [
          'field' => 'mw.entity_type',
          'value' => MoveInventoryEntity::FILTER,
          'operation' => '=',
        ],
      );
      $expressions = array(
        ['expression' => 'distinct', 'value' => 'mfr.filter_id'],
      );
      $fetching = array('fetch' => 'fetchAll', 'value' => \PDO::FETCH_ASSOC);
    }
    else {
      $joins = array();
      $fields = array(
        ['alias' => FilterRelationModel::$alias, 'fields' => ['filter_id']],
      );
      $conditions = array(
        ['field' => 'mfr.room_id', 'value' => $room_id, 'operation' => '='],
      );
      $expressions = array(
        ['expression' => 'distinct', 'value' => 'mfr.filter_id'],
      );
      $fetching = array('fetch' => 'fetchCol', 'value' => '');
    }

    return FilterRelationModel::selectFilterRelation($joins, $fields, $conditions, $expressions, $fetching) ?? [];
  }

  /**
   * Merge inventory with filter.
   *
   * @param array $data
   *   Data.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeWithFilterId(array $data) {
    $keys = array(
      'entity_id' => $data['entity_id'],
      'entity_type' => $data['entity_type'],
      'item_id' => $data['item_id'],
      'room_id' => $data['room_id'],
      'filter_id' => $data['filter_id'],
      'type_inventory' => $data['type_inventory'],
    );

    RequestInventoryModel::mergeInventory($keys, $data);
  }

  /**
   * Merge inventory without filter.
   *
   * @param array $data
   *   Data.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public static function mergeWithoutFilter(array $data) {
    $keys = array(
      'entity_id' => $data['entity_id'],
      'entity_type' => $data['entity_type'],
      'item_id' => $data['item_id'],
      'room_id' => $data['room_id'],
      'type_inventory' => $data['type_inventory'],
    );

    RequestInventoryModel::mergeInventory($keys, $data);
  }

  /**
   * Delete item from request.
   *
   * @param array $conditionsOldFormat
   *   Conditions.
   *
   * @return int
   *   Deleted item.
   *
   * @throws \ServicesException
   */
  public static function deleteItemFromRequest(array $conditionsOldFormat) {
    $conditions = [];
    foreach ($conditionsOldFormat as $key => $value) {
      $conditions[] = ['field' => $key, 'value' => $value, 'operation' => '='];
    }

    return RequestInventoryModel::deleteInventory($conditions);
  }

  /**
   * Update count item.
   *
   * @param int $id
   *   Id.
   * @param int $newCount
   *   New count.
   *
   * @throws \ServicesException
   */
  public static function updateCountItem(int $id, int $newCount) {
    $conditions = array(
      ['field' => 'id', 'value' => $id, 'operation' => '='],
    );

    RequestInventoryModel::updateRequestInventory(array('count' => $newCount), $conditions);
  }

  /**
   * Move item and change status to moved.
   *
   * @param int $room_id
   *   Room id.
   * @param int $id
   *   Row id.
   *
   * @throws \ServicesException
   */
  public static function moveItem(int $room_id, int $id) {
    $fields = array(
      'room_id' => $room_id,
      'moved' => MoveInventoryMoveType::MOVED,
      'filter_id' => NULL,
    );
    $conditions = array(
      ['field' => 'id', 'value' => $id, 'operation' => '='],
    );

    RequestInventoryModel::updateRequestInventory($fields, $conditions);
  }

  /**
   * Insert item.
   *
   * @param array $data
   *   Data.
   *
   * @return \DatabaseStatementInterface|int
   *   Id.
   *
   * @throws \Exception
   */
  public static function insertItemInRequest(array $data) {
    return RequestInventoryModel::insertRequestInventory($data);
  }

  /**
   * Prepare data for insert in request.
   *
   * @param array $row
   *   Data.
   *
   * @return array
   *   Needed data.
   */
  public static function prepareDataForInsert(array $row) {
    return [
      'entity_id'      => $row['entity_id'],
      'entity_type'    => $row['entity_type'],
      'item_id'        => $row['item_id'],
      'filter_id'      => $row['filter_id'],
      'room_id'        => $row['room_id'],
      'package_id'     => $row['package_id'],
      'pac_rel_id'     => $row['pac_rel_id'],
      'name'           => $row['name'],
      'count'          => $row['count'],
      'price'          => $row['price'],
      'cf'             => $row['cf'],
      'type'           => $row['type'],
      'type_inventory' => $row['type_inventory'],
      'moved'          => $row['moved'],
    ];
  }

  /**
   * Get inserted item to request.
   *
   * @param array $data
   *   Data.
   *
   * @return mixed
   *   Item id.
   *
   * @throws \ServicesException
   */
  public static function getInsertedItemInRequest(array $data) {
    $fields = array(
      ['alias' => RequestInventoryModel::$alias, 'fields' => ['id']],
    );
    $conditions = array(
      [
        'field' => 'mri.entity_id',
        'value' => $data['entity_id'],
        'operation' => '=',
      ],
      [
        'field' => 'mri.entity_type',
        'value' => $data['entity_type'],
        'operation' => '=',
      ],
      [
        'field' => 'mri.item_id',
        'value' => $data['item_id'],
        'operation' => '=',
      ],
      [
        'field' => 'mri.room_id',
        'value' => $data['room_id'],
        'operation' => '=',
      ],
      [
        'field' => 'mri.type_inventory',
        'value' => $data['type_inventory'],
        'operation' => '=',
      ],
    );

    if (isset($data['filter_id'])) {
      $conditions[] =
        [
          'field' => 'mri.filter_id',
          'value' => $data['filter_id'],
          'operation' => '=',
        ];
    }

    $fetching = array('fetch' => 'fetchField', 'value' => 0);

    return RequestInventoryModel::selectRequestInventory([], $fields, $conditions, [], $fetching);
  }

  /**
   * Get custom item.
   *
   * @param int $id
   *   Row id.
   *
   * @return mixed
   *   Item id.
   *
   * @throws \ServicesException
   */
  public static function getCustomItem(int $id) {
    $fields = array(
      [
        'alias' => RequestInventoryModel::$alias,
        'fields' => ['item_id', 'entity_id'],
      ],
    );
    $conditions = array(
      ['field' => 'id', 'value' => $id, 'operation' => '='],
    );
    $fetching = array('fetch' => 'fetchAssoc', 'value' => '');

    return RequestInventoryModel::selectRequestInventory([], $fields, $conditions, [], $fetching);
  }

  /**
   * Get count for entity id.
   *
   * @param int $entityId
   *   Entity id.
   *
   * @return mixed
   *   Int.
   *
   * @throws \ServicesException
   */
  public static function checkExistAddInventory(int $entityId) {
    $fields = array(
      ['alias' => RequestInventoryModel::$alias, 'fields' => []],
    );
    $conditions = array(
      ['field' => 'mri.entity_id', 'value' => $entityId, 'operation' => '='],
      [
        'field' => 'mri.type_inventory',
        'value' => Config::getConfig()->getDefaultInventoryType(),
        'operation' => '>',
      ],
    );
    $expressions = array(
      ['expression' => 'countQuery', 'value' => ''],
    );
    $fetching = array('fetch' => 'fetchField', 'value' => 0);

    return RequestInventoryModel::selectRequestInventory([], $fields, $conditions, $expressions, $fetching);
  }

  /**
   * Count item in request.
   *
   * @param int $id
   *   Id.
   *
   * @return int
   *   Count.
   *
   * @throws \ServicesException
   */
  public static function getCountItemInRequest(int $id) {
    $fields = array(
      ['alias' => RequestInventoryModel::$alias, 'fields' => ['count']],
    );
    $conditions = array(
      ['field' => 'id', 'value' => $id, 'operation' => '='],
    );
    $fetching = array('fetch' => 'fetchField', 'value' => '');

    return RequestInventoryModel::selectRequestInventory([], $fields, $conditions, [], $fetching);
  }

  /**
   * Count item in request.
   *
   * @param int $itemId
   *   Item id.
   *
   * @return mixed
   *   Count item.
   *
   * @throws \ServicesException
   */
  public static function countItemInRequest(int $itemId) {
    $fields = array(
      ['alias' => RequestInventoryModel::$alias, 'fields' => []],
    );
    $conditions = array(
      ['field' => 'mri.item_id', 'value' => $itemId, 'operation' => '='],
    );
    $expressions = array(
      ['expression' => 'countQuery', 'value' => ''],
    );
    $fetching = array('fetch' => 'fetchField', 'value' => 0);

    return RequestInventoryModel::selectRequestInventory([], $fields, $conditions, $expressions, $fetching);
  }

  /**
   * Check if room in request.
   *
   * @param int $room_id
   *   Room id.
   *
   * @return bool
   *   TRUE or FALSE.
   *
   * @throws \ServicesException
   */
  public static function isRoomInRequest(int $room_id) : bool {
    $fields = array(
      ['alias' => RequestInventoryModel::$alias, 'fields' => ['id']],
    );
    $conditions = array(
      ['field' => 'room_id', 'value' => $room_id, 'operation' => '='],
    );
    $fetching = array('fetch' => 'fetchField', 'value' => 0);

    return !empty(RequestInventoryModel::selectRequestInventory([], $fields, $conditions, [], $fetching)) ? TRUE : FALSE;

  }

  /**
   * Get packing Day request.
   *
   * We will always use the original not packing day request.
   *
   * @param array $requestAllData
   *   Entity id.
   * @param int $entityId
   *   Entity id.
   *
   * @return int|null
   *   Result.
   */
  public static function getPackingDayRequest(array $requestAllData, int $entityId) {
    if (isset($requestAllData['packing_request_id'])) {
      $result = db_select('field_data_field_move_service_type', 'service_type')
        ->fields('service_type', array('field_move_service_type_value'))
        ->condition('entity_id', $requestAllData['packing_request_id'], '=')
        ->condition('field_move_service_type_value', RequestServiceType::PACKING_DAY, '=')
        ->execute()
        ->fetchField();
      if ($result) {
        return $entityId;
      }
      else {
        return $requestAllData['packing_request_id'];
      }
    }

    return NULL;
  }

  /**
   * Get to storage request.
   *
   * @param int $entity_id
   *   Entity id.
   *
   * @return mixed
   *   False or id of request.
   */
  public static function toStorage(int $entity_id) {
    return db_select('field_data_field_to_storage_move', 'to_storage')
      ->fields('to_storage', array('field_to_storage_move_target_id'))
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetchField();
  }

  /**
   * Get from storage request.
   *
   * @param int $entity_id
   *   Entity id.
   *
   * @return mixed
   *   False or id of request.
   */
  public static function fromStorage(int $entity_id) {
    return db_select('field_data_field_from_storage_move', 'from_storage')
      ->fields('from_storage', array('field_from_storage_move_target_id'))
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetchField();
  }

}
