<?php

namespace Drupal\move_inventory\Actions\SubActions;

use Drupal\move_inventory\Tasks\InventoryTasks;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Traits\SymfonySerializer;

/**
 * Class AbstractSubActions.
 *
 * @package Drupal\move_inventory\Actions\SubActions
 */
abstract class AbstractSubActions {
  use SymfonySerializer;

  /**
   * Entity id.
   *
   * @var int
   */
  private $id;

  /**
   * Preset image folder.
   *
   * @var string
   */
  protected $imagePreset = 'inventory';

  /**
   * AbstractBaseService constructor.
   */
  public function __construct() {
    $this->initSerializer();
  }

  /**
   * Get id inventory entity such as item, filter, room.
   *
   * @return int
   *   Id.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set id.
   *
   * @param int $id
   *   Id of item, room, filter.
   */
  public function setId(int $id) {
    $this->id = $id;
  }

  /**
   * Get entity id for packing or storage.
   *
   * @param int $entityId
   *   Entity id.
   *
   * @return int|null
   *   Result.
   */
  public function getEntityIdStoragePacking(int $entityId) {
    // For storage.
    $result['to'] = InventoryTasks::toStorage($entityId);
    $result['from'] = InventoryTasks::fromStorage($entityId);
    if (!$result['to'] && $result['from']) {
      $result['to'] = $entityId;
    }
    $res = $result['to'] ?: $entityId;
    // For packing day.
    $res = InventoryTasks::getPackingDayRequest(MoveRequest::getRequestAllData($entityId), $entityId) ?? $res;

    return $res;
  }

  /**
   * Show url to image preset.
   *
   * @param string $image_uri
   *   Image uri or full url.
   *
   * @return bool|string
   *   URL.
   */
  public function showPresetUrl(string $image_uri) {
    $move_board_search = strpos($image_uri, '/moveBoard/');
    $api_search = strpos($image_uri, '/api.');
    if ($move_board_search !== FALSE || $api_search !== FALSE) {
      $url = $image_uri;
    }
    else {
      $uri = image_style_path($this->imagePreset, $image_uri);
      if (!empty($uri)) {
        $api_search = strpos($image_uri, '/image');
        if ($api_search === FALSE) {
          $url = image_style_url($this->imagePreset, $image_uri);
        }
        else {
          $url = file_create_url($uri);
        }
      }
      else {
        $url = image_style_url($this->imagePreset, $image_uri);
      }
    }

    return $url;
  }

  /**
   * Convert numbers from string to needed format.
   *
   * @param array $all_fields
   *   All fields links.
   * @param array $fields_to_convert
   *   Fields names and types to convert.
   */
  public function convertFieldsToNumber(array &$all_fields, array $fields_to_convert) : void {
    foreach ($all_fields as $field_name => $val) {
      if (isset($val) && isset($fields_to_convert[$field_name])) {
        settype($all_fields[$field_name], $fields_to_convert[$field_name]);
      }
    }
  }

}
