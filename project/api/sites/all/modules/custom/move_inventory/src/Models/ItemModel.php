<?php

namespace Drupal\move_inventory\Models;

use Drupal\move_inventory\Util\enum\MoveInventoryEntity;
use Drupal\move_inventory\Util\enum\MoveInventoryItemType;
use PDO;

/**
 * Class ItemModel.
 *
 * @package Drupal\move_inventory\Models
 */
class ItemModel extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'mi_items';


  public static $alias = 'mi';

  /**
   * Insert item in mi_items.
   *
   * @param array $itemData
   *   Item data.
   *
   * @return \DatabaseStatementInterface|int
   *   Row id.
   *
   * @throws \Exception
   */
  public static function insertItem(array $itemData) {
    return self::insertEntity(self::$table, $itemData);
  }

  /**
   * Update item in mi_items.
   *
   * @param array $fields
   *   Item data.
   * @param array $conditions
   *   Conditions.
   *
   * @return \DatabaseStatementInterface
   *   Updated row.
   *
   * @throws \ServicesException
   */
  public static function updateItem(array $fields = array(), array $conditions = array()) {
    return self::updateEntity(self::$table, $fields, $conditions);
  }

  /**
   * Delete item.
   *
   * @param array $conditions
   *   Item id.
   *
   * @return int
   *   Deleted item.
   *
   * @throws \ServicesException
   */
  public static function deleteItem(array $conditions) {
    return self::deleteEntity(self::$table, $conditions);
  }

  /**
   * Select item data.
   *
   * @param array $joins
   *   Array join table.
   * @param array $fields
   *   Array fields.
   * @param array $conditions
   *   Array conditions.
   * @param array $expressions
   *   Array expressions.
   * @param array $fetching
   *   Get result.
   *
   * @return mixed
   *   Result select.
   *
   * @throws \ServicesException
   */
  public static function selectItem(array $joins = array(), array $fields = array(), array $conditions = array(), array $expressions = array(), array $fetching = array()) {
    return self::selectEntity(
      [
        'table' => self::$table,
        'alias' => self::$alias,
      ],
      $joins, $fields, $conditions, $expressions, $fetching);
  }

  /**
   * Get items data.
   *
   * @param array|null $conditions
   *   Conditions.
   * @param array $filters
   *   Filters for conditions.
   *
   * @return mixed
   *   Items.
   */
  public static function getItemsData(?array $conditions, array $filters) {
    $query = db_select('mi_items_relations', 'mir');
    $query->leftJoin('mi_items', 'mi', 'mir.item_id = mi.item_id');
    $query->leftJoin('mi_filter_relations', 'mfr', 'mir.filter_id = mfr.filter_id');

    if (!empty($conditions['entity_id'])) {
      $condition_string = '';
      $conditions_placeholders = array();
      foreach ($conditions as $field_name => $val) {
        $condition_string .= " AND mri.$field_name = :$field_name";
        $conditions_placeholders[":$field_name"] = $val;
      }
      $query->leftJoin('mi_request_inventory', 'mri', "mir.item_id = mri.item_id $condition_string", $conditions_placeholders);
      $query->fields('mri', array('count'));
      $query->addExpression('mri.count * mi.cf', 'total_cf');
      $query->addExpression('mri.count * mi.cf * mi.price', 'total_inventory_price');
    }

    if (!empty($conditions)) {
      $query->leftJoin('mi_weight', 'mw', 'mir.item_id = mw.entity_id AND mw.entity_type = ' . MoveInventoryEntity::ITEM);

      $filters = array_intersect_key($conditions, $filters);
      foreach ($filters as $field_name => $val) {
        if (!empty($val)) {
          $prefix = ($field_name == 'room_id') ? 'mfr' : 'mir';
          $operator = is_array($val) ? 'IN' : '=';
          $query->condition('mw.' . $field_name, $val, $operator);
          $query->condition($prefix . '.' . $field_name, $val, $operator);
        }
      }
      $query->fields('mw', array('weight'));
      $query->addExpression('mi.price * mi.cf', 'total_price');
      $query->orderBy('mw.weight', 'ASC');
      $query->isNotNull('mi.item_id');
    }
    $query->fields('mi');
    $query->groupBy('mi.item_id');

    return $query->execute()->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Search item in settings.
   *
   * @param string $name
   *   Name.
   *
   * @return mixed
   *   Items.
   */
  public static function searchItemInSettings(string $name) {
    // Query get id of items that are not in the request.
    $getItemIdInRooms = db_select('mi_items', 'mi');
    $getItemIdInRooms->fields('mi', array('item_id'))
      ->groupBy('mi.item_id');
    $getItemIdInRooms->leftJoin('mi_items_relations', 'mir', 'mi.item_id = mir.item_id');
    $getItemIdInRooms->leftJoin('mi_filter_relations', 'mfr', 'mir.filter_id = mfr.filter_id');
    $getItemIdInRooms->condition('mi.name', '%' . db_like($name) . '%', 'LIKE');
    $getItemIdInRooms->condition('mi.type', MoveInventoryItemType::CUSTOM, '<>');

    // Get data of items that are not in request.
    $getItemsData = db_select('mi_items', 'mi');
    $getItemsData->leftJoin('mi_items_relations', 'mir', 'mi.item_id = mir.item_id');
    $getItemsData->leftJoin('mi_filter_relations', 'mfr', 'mir.filter_id = mfr.filter_id');
    $getItemsData->fields('mi')
      ->fields('mir', array('filter_id'))
      ->fields('mfr', array('room_id'))
      ->groupBy('item_id');
    $getItemsData->condition('mi.item_id', $getItemIdInRooms, 'IN');

    return $getItemsData->execute()->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Search item in request.
   *
   * @param string $item_name
   *   Search name.
   * @param array $conditions
   *   Conditions.
   *
   * @return array
   *   Items.
   */
  public static function seatchItemInRequest(string $item_name, array $conditions) {
    // Query get id of items that are in the request.
    $getItemIdInRequest = db_select('mi_request_inventory', 'mri');
    $getItemIdInRequest->fields('mri', array('item_id'));
    $getItemIdInRequest->condition('mri.entity_id', $conditions['entity_id']);
    $getItemIdInRequest->condition('mri.entity_type', $conditions['entity_type']);
    $getItemIdInRequest->condition('mri.type_inventory', $conditions['type_inventory']);
    $getItemIdInRequest->condition('mri.name', '%' . db_like($item_name) . '%', 'LIKE');
    if (!empty($conditions['room_id'])) {
      $getItemIdInRequest->condition('mri.room_id', $conditions['room_id'], '=');
      unset($conditions['room_id']);
    }

    // Query get id of items that are not in the request.
    $getItemIdInRooms = db_select('mi_items', 'mi');
    $getItemIdInRooms->fields('mi', array('item_id'))
      ->groupBy('mi.item_id');
    $getItemIdInRooms->leftJoin('mi_items_relations', 'mir', 'mi.item_id = mir.item_id');
    $getItemIdInRooms->leftJoin('mi_filter_relations', 'mfr', 'mir.filter_id = mfr.filter_id');
    $getItemIdInRooms->condition('mi.name', '%' . db_like($item_name) . '%', 'LIKE');
    $getItemIdInRooms->condition('mi.type', MoveInventoryItemType::CUSTOM, '<>');

    // Get data of items that are not in request.
    $getItemsData = db_select('mi_items', 'mi');
    $getItemsData->leftJoin('mi_items_relations', 'mir', 'mi.item_id = mir.item_id');
    $getItemsData->leftJoin('mi_filter_relations', 'mfr', 'mir.filter_id = mfr.filter_id');
    $getItemsData->fields('mi')
      ->fields('mir', array('filter_id'))
      ->fields('mfr', array('room_id'))
      ->groupBy('item_id');
    $getItemsData->condition('mi.item_id', $getItemIdInRooms, 'IN');
    $getItemsData->condition('mi.item_id', $getItemIdInRequest, 'NOT IN');
    $result_part_1 = $getItemsData->execute()->fetchAll(PDO::FETCH_ASSOC);

    // Get data of items that are in request.
    $getItemsDataInRequest = db_select('mi_request_inventory', 'mri');
    $getItemsDataInRequest->innerJoin('mi_items', 'mi', 'mri.item_id = mi.item_id');
    $getItemsDataInRequest->fields('mri', array(
      'room_id',
      'filter_id',
      'count',
      'id',
    ))->fields('mi');
    $getItemsDataInRequest->condition('mi.item_id', $getItemIdInRequest, 'IN');
    $getItemsDataInRequest->condition('mri.entity_id', $conditions['entity_id'], '=');
    $result_part_2 = $getItemsDataInRequest->execute()
      ->fetchAll(PDO::FETCH_ASSOC);

    return array_merge($result_part_2, $result_part_1);
  }

  /**
   * Get items for filter by his id.
   *
   * @param int $filter_id
   *   Filter id.
   * @param mixed $conditions
   *   With weight.
   *
   * @return mixed
   *   FALSE or Array with ids.
   */
  public static function getItemsIdsForFilter(int $filter_id, $conditions = array()) {
    $query = db_select('mi_items_relations', 'mir');
    $query->fields('mir', array('item_id'));
    $query->condition('mir.filter_id', $filter_id, '=');
    if (!empty($conditions)) {
      $query->leftJoin('mi_weight', 'mw', 'mw.entity_id = mir.item_id');
      $query->condition('mw.filter_id', $filter_id, '=');
      $query->condition('mw.entity_type', MoveInventoryEntity::ITEM, '=');
      $query->condition('mw.room_id', $conditions['room_id'], '=');
      $query->fields('mw', array('weight'));
      $result = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
    }
    else {
      $result = $query->execute()->fetchCol();
    }

    return $result;
  }

}
