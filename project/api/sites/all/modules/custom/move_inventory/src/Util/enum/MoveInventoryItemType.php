<?php

namespace Drupal\move_inventory\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class MoveInventoryItemType.
 *
 * @package Drupal\move_inventory\Util
 */
class MoveInventoryItemType extends BaseEnum {
  const USUAL = 0;
  const CUSTOM = 1;
  const BOXES = 2;

}
