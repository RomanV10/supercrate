<?php

namespace Drupal\move_inventory\Routes;

use Drupal\move_inventory\Controllers\PackageController;
use Drupal\move_services_new\Util\enum\EntityTypes;

/**
 * Class PackageRoute.
 *
 * @package Drupal\move_inventory\Routes
 */
class PackageRoute {

  /**
   * Package routes.
   *
   * @return array
   *   Definition.
   */
  public static function getRoutes() {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_inventory',
      'name' => 'src/Controllers/PackageController',
    ];

    return array(
      'move_inventory_package' => array(
        'operations' => array(
          'create' => array(
            'callback' => PackageController::class . '::createInventoryPackage',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => PackageController::class . '::retrieveInventoryPackage',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'callback' => PackageController::class . '::updateInventoryPackage',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete' => array(
            'callback' => PackageController::class . '::deleteInventoryPackage',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'index' => array(
            'callback' => PackageController::class . '::indexInventoryPackage',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pageSize'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'add_item_to_request' => array(
            'callback' => PackageController::class . '::addItemToRequest',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'add_package_to_request' => array(
            'callback' => PackageController::class . '::addPackageToRequest',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'package_id',
                'type' => 'int',
                'source' => array('data' => 'package_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'default value' => 0,
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'remove_package_from_request' => array(
            'callback' => PackageController::class . '::removePackageFromRequest',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => TRUE,
                'default value' => EntityTypes::MOVEREQUEST,
              ),
              array(
                'name' => 'package_id',
                'type' => 'int',
                'source' => array('data' => 'package_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'remove_item_from_package' => array(
            'callback' => PackageController::class . '::removeItemFromPackage',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'pac_rel_id',
                'type' => 'int',
                'source' => array('data' => 'pac_rel_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

}
