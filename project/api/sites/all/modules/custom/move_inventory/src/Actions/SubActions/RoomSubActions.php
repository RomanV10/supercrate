<?php

namespace Drupal\move_inventory\Actions\SubActions;

use Drupal\move_inventory\Entity\Rooms;
use Drupal\move_inventory\Tasks\FilterTasks;
use Drupal\move_inventory\Tasks\InventoryTasks;
use Drupal\move_inventory\Tasks\RoomTasks;

/**
 * Class RoomSubActions.
 *
 * @package Drupal\move_inventory\Actions\SubActions
 */
class RoomSubActions extends AbstractSubActions implements SubActionsInterface {

  /**
   * Create service.
   *
   * @param array $data
   *   Data for create entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function create(array $data = array()) {
    $data['image_link'] = !empty($data['uri']) ? $data['uri'] : $data['image_link'];
    $room = (array) $this->serializer->deserialize(json_encode($data), Rooms::class, 'json');
    $room = RoomTasks::create($room);
    $maxWeight = RoomTasks::getMaxWeightForRoomSettings();
    RoomTasks::insertWeightRoom($room['room_id'], NULL, ++$maxWeight);

    return $room;
  }

  /**
   * Retrieve service.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function retrieve() {
    return RoomTasks::retrieve($this->getId());
  }

  /**
   * Service for update entity.
   *
   * @param array $data
   *   Data for update entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function update(array $data = array()) {
    $data['image_link'] = !empty($data['uri']) ? $data['uri'] : $data['image_link'];
    $room = (array) $this->serializer->deserialize(json_encode($data), Rooms::class, 'json');
    $result = RoomTasks::updateRoom($room, $this->getId());

    return $result;
  }

  /**
   * Service for remove entity.
   *
   * @return mixed
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function delete() {
    $result = FALSE;
    if (!InventoryTasks::isRoomInRequest($this->getId())) {
      $filters_ids = InventoryTasks::getFilterIdsForRoom($this->getId());
      foreach ($filters_ids as $filter_id) {
        if (count(FilterTasks::getExistingRoom($filter_id, [])) < 2) {
          // Delete filter from filter table.
          FilterTasks::deleteFilter($filter_id);
          // Delete filter from filter relations table.
          FilterTasks::deleteFilterFromRelations($filter_id);
          // Delete filter from item relations table.
          FilterTasks::deleteFilterFromItemRelations($filter_id);
          // Delete filter from weight table.
          FilterTasks::deleteFilterFromWeight($filter_id);
        }
      }
      RoomTasks::deleteRoom($this->getId());
      RoomTasks::deleteRoomFromFilterRelations($this->getId());
      RoomTasks::deleteWeightRoom($this->getId());
      $result = TRUE;
    }
    else {
      services_error('Room in request', 406);
    }

    return $result;
  }

}
