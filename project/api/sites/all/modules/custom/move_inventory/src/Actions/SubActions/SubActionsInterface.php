<?php

namespace Drupal\move_inventory\Actions\SubActions;

use Drupal\move_inventory\Actions\BaseCRUDInterface;

/**
 * Interface CRUDInterface.
 *
 * @package Drupal\move_inventory\Actions\SubActions
 */
interface SubActionsInterface extends BaseCRUDInterface {

}
