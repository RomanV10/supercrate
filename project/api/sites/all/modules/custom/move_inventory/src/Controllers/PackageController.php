<?php

namespace Drupal\move_inventory\Controllers;

use Drupal\move_inventory\Actions\PackageActions;
use Drupal\move_services_new\Util\enum\EntityTypes;

/**
 * Class PackageController.
 *
 * @package Drupal\move_inventory\Controllers
 */
class PackageController extends AbstractController {

  public static function createInventoryPackage($data = array()) {
    $al_instance = new PackageActions();
    return $al_instance->create($data);
  }

  public static function retrieveInventoryPackage(int $id) {
    $al_instance = new PackageActions($id);
    return $al_instance->retrieve();
  }

  public static function updateInventoryPackage(int $id, $data) {
    $al_instance = new PackageActions($id);
    return $al_instance->update($data);
  }

  public static function deleteInventoryPackage(int $id) {
    $al_instance = new PackageActions($id);
    return $al_instance->delete();
  }

  public static function indexInventoryPackage($current_page, $per_page) {
    $al_instance = new PackageActions();
    return $al_instance->index($current_page, $per_page);
  }

  public static function addPackageToRequest($package_id, $entity_id, $entity_type = EntityTypes::MOVEREQUEST) {
    $al_instance = new PackageActions($package_id);
    return $al_instance->addPackageToRequest($entity_id, $entity_type);
  }

  public static function removePackageFromRequest(int $entity_id, int $entity_type = EntityTypes::MOVEREQUEST, int $package_id) {
    $al_instance = new PackageActions($package_id);
    return $al_instance->removePackageFromRequest($entity_id, $entity_type);
  }

  public static function removeItemFromPackage(int $pac_rel_id) {
    return PackageActions::removeItemFromPackage($pac_rel_id);
  }

}
