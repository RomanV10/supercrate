<?php

namespace Drupal\move_notification\Cron\Tasks;

use Drupal\move_services_new\Cron\Tasks\CronInterface;

/**
 * Class RemoveOldNotification.
 *
 * @package Drupal\move_notification\Cron
 */
class RemoveOldNotification implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $period = variable_get('notification_period_when_remove_old', 1) * -1;
    $time = new \DateTime('now');
    $start_day = $time->modify($period . ' month')->getTimestamp();
    $notifications_ids = db_select('move_notification', 'mn')
      ->fields('mn', array('id'))
      ->condition('created', $start_day, '<')
      ->execute()
      ->fetchCol();
    if (!empty($notifications_ids)) {
      db_delete('move_notification')
        ->condition('id', $notifications_ids, 'IN')
        ->execute();
      db_delete('move_notification_status')
        ->condition('notif_id', $notifications_ids, 'IN')
        ->execute();
    }
  }

}
