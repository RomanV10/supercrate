<?php

namespace Drupal\move_notification\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class NotificationCategories.
 *
 * @package Drupal\move_notification\Util
 */
class NotificationCategories extends BaseEnum {
  const REQUEST = 0;
  const USER = 1;
  const SITE_SETTINGS = 2;
}
