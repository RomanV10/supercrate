<?php

namespace Drupal\move_notification\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\Users;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\NotificationTypes;

/**
 * Class Invoice.
 *
 * @package Drupal\move_invoice\Services
 */
class NotificationTemplate extends BaseService {
  private $tid;

  public function __construct($tid = NULL) {
    $this->tid = $tid;
  }

  public function create($data = array()) {
    $insert = db_insert('move_notification_templates')
      ->fields($data)
      ->execute();
    return $insert;
  }

  public function retrieve() {
    $query = db_select('move_notification_templates', 'mnt')
      ->fields('mnt')
      ->condition('tid', $this->tid)
      ->execute()
      ->fetchAssoc(\PDO::FETCH_ASSOC);
    return $query;
  }


  public function update($data = array()) {
    $updated = db_update('move_notification_templates')
      ->fields($data)
      ->condition('tid', $this->tid)
      ->execute();
    return $updated;
  }

  public function delete() {
    $delete = db_delete('move_notification_templates')
      ->condition('tid', $this->tid)
      ->execute();
    return $delete;
  }


  public function index() {
    $query = db_select('move_notification_templates', 'mnt')
      ->fields('mnt')
      ->condition('tid', $this->tid)
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
    return $query;
  }

  public static function tokenList() {
    $tokens_list = array(
      'Request id' => 'node:nid',
      'Request created day' => 'node:created',
      'User login' => 'user:name',
      'User id' => 'user:uid',
      'Request approve status' => 'node:field_approve',
      'User last name' => 'user:field_user_last_name',
      'User first name' => 'user:field_user_first_name',
      'Notification text' => 'notification:text',
    );
    return $tokens_list;
  }

}
