<?php

namespace Drupal\move_notification\Cron\Queues;

use Drupal\move_notification\Services\Notification;
use Drupal\move_services_new\Cron\Queues\QueueInterface;
use Drupal\move_services_new\Services\Users;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class NotificationEmailQueue.
 *
 * @package Drupal\move_notification\Queues
 */
class NotificationEmailQueue implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   */
  public static function execute($data): void {
    // Check if notification is viewed.
    $is_viewed = Notification::isNotificationViewed($data['id'], $data['uid']);
    if (empty($is_viewed)) {
      $user = user_load($data['uid']);
      $user_settings = Users::getUserSettings($data['uid']);
      if (!empty($user)) {
        $user_mail = !empty($user_settings['field_notification_mail']) ? $user_settings['field_notification_mail'] : $user->mail;
        $allow_send = TRUE;
        // Check If user is sales only, we continue.
        $is_sales_only = Notification::isSalesOnly($user);
        if ($is_sales_only) {
          $allow_send = Notification::isUserAllowSeeAssigned($data['uid'], $data['ntid'], $data['entity_id'], $data['entity_type']);
        }
        if ($allow_send) {
          $custom = array();
          $template_builder = new TemplateBuilder();
          $notification_info = unserialize($data['data']);
          switch ($data['entity_type']) {
            case EntityTypes::MOVEREQUEST:
              $entity_name = 'Request';
              $node = node_load($data['entity_id']);
              $custom = $template_builder->moveRequestGetVariablesArray($node);
              break;

            case 20:
            case 30:
              $entity_name = 'User';

              break;

            default:
              $entity_name = '';
              break;

          }
          $notification_title = Notification::getNotificationTitleForType($data['ntid']);
          $data['subject'] = 'Notification: ' . $entity_name . ' #' . $data['entity_id'];
          $template = Notification::getNotificationTemplate('notification_send', $data['subject']);
          if (!empty($template)) {
            $notification_main_text = 'Notification type is <strong>"' . $notification_title . '"</strong><br>';
            $notification_main_text .= 'User who did action is <strong>"' . $notification_info['source'] . '"</strong><br>';
            $notification_main_text .= 'Notification text: <br><strong>' . $notification_info['text'] . '</strong>';
            $custom['message-text'] = $notification_main_text;
            $custom['notification'] = TRUE;
            $send_mail = $template_builder->sendEmailTemplates($data['id'], $template, $custom, $user_mail, 20);
            if ($send_mail['send']) {
              Notification::updateUserLastSendEmailDate($user->uid);
            }
          }
          else {
            watchdog('notification_send_mail', 'Template is not exist', array(), WATCHDOG_DEBUG);
          }
        }
      }
    }

  }

}
