<?php

namespace Drupal\move_notification\System;

use Drupal\move_notification\Services\Notification;
use Drupal\move_notification\Services\NotificationTemplate;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'move_notification' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_notification\System\Service::createNotification',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => TRUE,
              ),

            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_notification\System\Service::retrieveNotification',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_notification\System\Service::updateNotification',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_notification\System\Service::deleteNotification',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_notification\System\Service::indexNotification',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 0.',
                'default value' => 0,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'pagesize',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pagesize'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'get_notifications' => array(
            'help' => 'Get all notifications for user',
            'callback' => 'Drupal\move_notification\System\Service::getNotifications',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'role',
                'type' => 'int',
                'source' => array('data' => 'role'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_new_notifications' => array(
            'help' => 'Get all new notifications for user',
            'callback' => 'Drupal\move_notification\System\Service::getNewNotifications',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'type' => 'int',
                'source' => array('data' => 'date'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'filters',
                'type' => 'array',
                'source' => array('data' => 'filters'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_all_notifications' => array(
            'help' => 'Get all notifications for user',
            'callback' => 'Drupal\move_notification\System\Service::getAllNotifications',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('data' => 'page'),
              ),
              array(
                'name' => 'page_size',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('data' => 'page_size'),
              ),
              array(
                'name' => 'filters',
                'optional' => TRUE,
                'type' => 'array',
                'description' => 'Filters for notification',
                'source' => array('data' => 'filters'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_all_notifications_with_status' => array(
            'help' => 'Get all notifications for user',
            'callback' => 'Drupal\move_notification\System\Service::getAllNotificationsWithStatus',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('data' => 'page'),
              ),
              array(
                'name' => 'page_size',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('data' => 'page_size'),
              ),
              array(
                'name' => 'status',
                'optional' => FALSE,
                'type' => 'int',
                'description' => 'Status of notification',
                'default value' => 1,
                'source' => array('data' => 'status'),
              ),
              array(
                'name' => 'filters',
                'optional' => TRUE,
                'type' => 'array',
                'description' => 'Filters for notification',
                'source' => array('data' => 'filters'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_notification_perm_role' => array(
            'help' => 'Add/update notification role',
            'callback' => 'Drupal\move_notification\System\Service::updateNotificationRole',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'role',
                'type' => 'int',
                'source' => array('data' => 'role'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_notification_perm_user' => array(
            'help' => 'Add/update notification user',
            'callback' => 'Drupal\move_notification\System\Service::updateNotificationUser',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_notification_perm_user_multiple' => array(
            'help' => 'Add/update notification user',
            'callback' => 'Drupal\move_notification\System\Service::updateNotificationUserMultiple',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'types',
                'type' => 'array',
                'source' => array('data' => 'types'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete_notification_perm_role' => array(
            'help' => 'Add/update notification role',
            'callback' => 'Drupal\move_notification\System\Service::deleteNotificationRole',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'role',
                'type' => 'int',
                'source' => array('data' => 'role'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete_notification_perm_user' => array(
            'help' => 'Add/update notification user',
            'callback' => 'Drupal\move_notification\System\Service::deleteNotificationUser',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_info_for_notification_type' => array(
            'help' => 'Add/update notification user',
            'callback' => 'Drupal\move_notification\System\Service::getInfoForNotificationType',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_all_notification_types_with_info' => array(
            'help' => 'Add/update notification user',
            'callback' => 'Drupal\move_notification\System\Service::getAllNotificationTypesWithInfo',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'access arguments' => ['administer site configuration'],
          ),
          'update_notification_status' => array(
            'help' => 'Add/update notification user statuses',
            'callback' => 'Drupal\move_notification\System\Service::updateNotificationStatus',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'status',
                'type' => 'int',
                'source' => array('data' => 'status'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_notification_status_multiple' => array(
            'help' => 'Add/update notification statuses multiple.',
            'callback' => 'Drupal\move_notification\System\Service::updateNotificationStatusMultiple',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'ids',
                'type' => 'array',
                'source' => array('data' => 'ids'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'status',
                'type' => 'int',
                'source' => array('data' => 'status'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_notification_status_by_types' => array(
            'help' => 'Add/update notification statuses multiple.',
            'callback' => 'Drupal\move_notification\System\Service::updateNotificationStatusByTypes',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'ntids',
                'type' => 'array',
                'source' => array('data' => 'ntids'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'status',
                'type' => 'int',
                'source' => array('data' => 'status'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'notification_type_template' => array(
            'help' => 'Add/update notification user',
            'callback' => 'Drupal\move_notification\System\Service::updateNotificationTypeTemplate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'text',
                'type' => 'int',
                'source' => array('data' => 'text'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete_notification_type_template' => array(
            'help' => 'Add/update notification user',
            'callback' => 'Drupal\move_notification\System\Service::deleteNotificationTypeTemplate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'user_allowed_notification_types' => array(
            'help' => 'Show all allowed notification types for user.',
            'callback' => 'Drupal\move_notification\System\Service::getAllowedNotificationTypesForUser',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'turn_off_on_notification_for_user' => array(
            'help' => 'Show all allowed notification types for user.',
            'callback' => 'Drupal\move_notification\System\Service::updateSubscribeForNotification',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'types',
                'type' => 'array',
                'source' => array('data' => 'types'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'status',
                'type' => 'int',
                'source' => array('data' => 'status'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_user_mail_send_setting' => array(
            'help' => 'Show all allowed notification types for user.',
            'callback' => 'Drupal\move_notification\System\Service::updateUserMailSendSetting',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'ntid',
                'type' => 'int',
                'source' => array('data' => 'ntid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'how_often',
                'type' => 'int',
                'source' => array('data' => 'how_often'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_notification_type_mail_settings' => array(
            'help' => 'Show all allowed notification types for user.',
            'callback' => 'Drupal\move_notification\System\Service::updateNotificationTypeMailSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'ntid',
                'type' => 'int',
                'source' => array('data' => 'ntid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'how_often',
                'type' => 'int',
                'source' => array('data' => 'how_often'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_user_notification_permissions_by_entity' => array(
            'help' => 'Update user permission settings tp see notification by entity id.',
            'callback' => 'Drupal\move_notification\System\Service::updateUserNotificationPermissionByEntity',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'ntid',
                'type' => 'int',
                'source' => array('data' => 'ntid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'status',
                'type' => 'int',
                'source' => array('data' => 'status'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
      'move_notification_templates' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_notification\System\Service::createTemplate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_notification\System\Service::retrieveTemplate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_notification\System\Service::updateTemplate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_notification\System\Service::deleteTemplate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_notification\System\Service::indexTemplates',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'get_tokens_list' => array(
            'help' => 'Show token list',
            'callback' => 'Drupal\move_notification\System\Service::getTokensList',
            'file' => array(
              'type' => 'php',
              'module' => 'move_notification',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function createNotification(int $type, $data = array(), $entity_id = 0, $entity_type = 0, $uid = NULL) {
    Notification::createNotification($type, $data, $entity_id, $entity_type, $uid);
  }

  public static function retrieveNotification() {
    $al_instance = new Notification();
    return $al_instance->retrieve();
  }

  public static function updateNotification($id, $data) {
    $al_instance = new Notification($id);
    return $al_instance->update($data);
  }

  public static function deleteNotification(int $id) {
    $al_instance = new Notification($id);
    return $al_instance->delete();
  }

  public static function indexNotification($page, $pagesize) {
    $al_instance = new Notification();
    return $al_instance->index($page, $pagesize);
  }

  public static function updateNotificationRole($type, $rid) {
    $al_instance = new Notification();
    return $al_instance->updateNotificationRole($type, $rid);
  }

  public static function updateNotificationUser($type, $uid) {
    $al_instance = new Notification();
    return $al_instance->updateNotificationUser($type, $uid);
  }

  public static function updateNotificationUserMultiple($uid, $types) {
    $al_instance = new Notification();
    return $al_instance->addUserToManyNotificationTypes($uid, $types);
  }

  public static function updateNotificationTypeTemplate($type, $text) {
    $al_instance = new Notification();
    return $al_instance->updateNotificationTypeTemplate($type, $text);
  }
  public static function deleteNotificationRole($type, $rid) {
    $al_instance = new Notification();
    return $al_instance->deleteNotificationRole($type, $rid);
  }

  public static function deleteNotificationUser($type, $uid) {
    $al_instance = new Notification();
    return $al_instance->deleteNotificationUser($type, $uid);
  }

  public static function deleteNotificationTypeTemplate($type) {
    $al_instance = new Notification();
    return $al_instance->deleteNotificationTypeTemplate($type);
  }


  public static function getNotifications($uid, $rid, $type) {
    $al_instance = new Notification();
    return $al_instance->retrieve();
  }

  /**
   * All new notification.
   *
   * @param bool $date
   * @param array $filters
   * @return mixed
   */
  public static function getNewNotifications($date = FALSE, $filters = array()) {
    $al_instance = new Notification();
    return $al_instance->showNewNotificationForCurrentUser($date, $filters);
  }

  /**
   * All  notification
   * @param int $page
   * @param int $page_size
   * @param array $filters
   * @return array
   */
  public static function getAllNotifications($page = 1, $page_size = -1, $filters = array()) {
    $al_instance = new Notification();
    return $al_instance->getAllowedNotification($page, $page_size, $filters);
  }

  public static function getAllNotificationsWithStatus($page = 1, $page_size = -1, int $status = 0, $filters = array()) {
    $al_instance = new Notification();
    return $al_instance->showNotificationWithStatusForCurrentUser($page, $page_size, $status, $filters);
  }

  public static function updateNotificationStatus($notif_id, $status){
    $al_instance = new Notification();
    return $al_instance->changeNotificationStatus($notif_id, $status);
  }

  public static function updateNotificationStatusMultiple($ids, $status) {
    $al_instance = new Notification();
    return $al_instance->changeNotificationStatusMultiple($ids, $status);
  }

  public static function updateNotificationStatusByTypes(array $ntids, int $status) {
    $al_instance = new Notification();
    return $al_instance->updateNotificationStatusByTypes($ntids, $status);
  }

  public static function getInfoForNotificationType($type) {
    return Notification::getInfoForNotificationType($type);
  }

  public static function getAllNotificationTypesWithInfo() {
    return Notification::getAllNotificationTypesWithInfo();
  }

  public static function getAllowedNotificationTypesForUser($uid = NULL) {
    $al_instance = new Notification(NULL, NULL, NULL, $uid);
    return $al_instance->getAllowedNotificationTypesForUser();
  }

  public static function updateSubscribeForNotification(array $types, int $status) {
    $al_instance = new Notification();
    return $al_instance->updateSubscribeForNotification($types, $status);
  }

  public static function updateUserMailSendSetting($ntid, $how_often, $uid = NULL) {
    $al_instance = new Notification(NULL, NULL, NULL, $uid);
    return $al_instance->updateUserMailSendSetting($ntid, $how_often);
  }

  public static function updateNotificationTypeMailSettings($ntid, $how_often) {
    return Notification::updateNotificationTypeMailSettings($ntid, $how_often);
  }

  public static function updateUserNotificationPermissionByEntity($ntid, $status) {
    $al_instance = new Notification();
    return $al_instance->updateUserNotificationPermissionByEntity($ntid, $status);
  }



  //Tempalates
  public static function createTemplate($data) {
    $al_instance = new NotificationTemplate();
    return $al_instance->create($data);
  }
  public static function retrieveTemplate($tid) {
    $al_instance = new NotificationTemplate($tid);
    return $al_instance->retrieve();
  }

  public static function updateTemplate($tid, $data) {
    $al_instance = new NotificationTemplate($tid);
    return $al_instance->update($data);
  }
  public static function deleteTemplate($tid) {
    $al_instance = new NotificationTemplate($tid);
    return $al_instance->delete();
  }

  public static function indexTemplates() {
    $al_instance = new NotificationTemplate();
    return $al_instance->index();
  }

  public static function getTokensList() {
    return NotificationTemplate::tokenList();
  }
}
