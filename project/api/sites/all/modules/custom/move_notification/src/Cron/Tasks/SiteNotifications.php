<?php

namespace Drupal\move_notification\Cron\Tasks;

use Drupal\move_mail\System\Common;
use Drupal\move_notification\Services\Notification;
use Drupal\move_services_new\Cron\Tasks\CronInterface;
use Drupal\move_services_new\Util\enum\NotificationTypes;
use Drupal\move_notification\Util\enum\NotificationCategories;

/**
 * Class SiteNotifications.
 *
 * @package Drupal\move_notification\System
 */
class SiteNotifications implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute() : void {
    self::checkingSMTPSiteSettings();
    // TODO удалить после валидации basic_setting variable.
    self::checkingBasicSettings();
  }

  private static function checkingSMTPSiteSettings() : void {
    $common_instance = new Common();
    $outgoing = array('active' => TRUE);
    $outgoing['server'] = variable_get('swiftmailer_smtp_host');
    $outgoing['port'] = variable_get('swiftmailer_smtp_port');
    $encrypt = variable_get('swiftmailer_smtp_encryption');
    if ($encrypt) {
      $outgoing['ssl'] = ($encrypt == 'ssl') ? TRUE : FALSE;
      $outgoing['tls'] = ($encrypt == 'tls') ? TRUE : FALSE;
    }

    $name = variable_get('swiftmailer_smtp_username');
    $password = variable_get('swiftmailer_smtp_password');

    $names = array('info@themoversboard.com', 'info@themoveboard.com');

    if (!in_array($name, $names)) {
      $result_test = $common_instance->testMailConnectionForCron($name, $outgoing, $password);
      if ($result_test !== 1) {
        $notification_text = array('text' => $result_test);
        $notification_info = array('notification' => $notification_text);
        static::createNotification($notification_info);
      }
    }
  }

  private static function checkingBasicSettings() : void {
    $basic_settings = json_decode(variable_get('basicsettings'));
    $message_error = "";

    // Checking Parking Settings.
    if (empty($basic_settings->packing_settings)) {
      $message_error = "Packing settings in Site Settings is empty!";
    }

    // Service Settings.
    if (empty($basic_settings->services)) {
      $message_error = "Services settings in Site Settings is empty!";
    }

    // Rate Settings.
    if (empty($basic_settings->ratesSettings)) {
      $message_error = "Rate settings in Site Settings is empty!";
    }

    if ($message_error) {
      $notification_text = array('text' => $message_error);
      $notification_info = array('notification' => $notification_text);
      static::createNotification($notification_info);
    }
  }

  private static function createNotification(array $notification_info) : void {
    Notification::createNotification(NotificationTypes::SITE_SETTINGS, $notification_info, 0, NotificationCategories::SITE_SETTINGS, 0);
  }

}
