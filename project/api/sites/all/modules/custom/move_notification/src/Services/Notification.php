<?php

namespace Drupal\move_notification\Services;

use Drupal\move_request_score\Models\NotificationType;
use Drupal\move_request_score\Services\MoveRequestScore;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\NotificationTypes;
use Drupal\move_notification\Util\enum\NotificationCategories;

/**
 * Class Invoice.
 *
 * @package Drupal\move_invoice\Services
 */
class Notification extends BaseService {

  const ONCE_PER_HOUR = 1;
  const ALWAYS = 2;
  const NEVER = 3;
  const NOTIFICATION_SENT = 4;
  const NOTIFICATION_VIEWED = 1;

  private $uid = NULL;
  private $entity_id = NULL;
  private $entity_type = 0;
  private $notificationType = NULL;
  private $user = NULL;
  private $userRoleIds = array();

  public static $addScoring = TRUE;

  /**
   * Notification constructor.
   *
   * @param int|null $notification_type
   *   Notification type.
   * @param int|null $entity_id
   *   Entity id.
   * @param int $entity_type
   *   Entity type.
   * @param int|null $uid
   *   User id.
   */
  public function __construct($notification_type = NULL, $entity_id = NULL, $entity_type = 0, $uid = NULL) {
    if (is_null($uid)) {
      global $user;
      $this->user = user_load($user->uid);
    }
    else {
      $this->user = user_load($uid, FALSE);
    }
    $this->userRoleIds = array_keys($this->user->roles);
    $this->notificationType = $notification_type;
    $this->entity_id = $entity_id;
    $this->entity_type = $entity_type;
  }

  /**
   * Create notification.
   *
   * @param array $data
   *   Data for notification.
   *
   * @throws \Exception
   */
  public function create($data = array()): void {
    if ($this->isAllowToSaveNotification($this->notificationType)) {
      $this->insertNotification($this->user->uid, $this->entity_id, $this->entity_type, $this->notificationType, $data);
    }

    if ($this->entity_type == EntityTypes::MOVEREQUEST) {
      if ($this->checkIfAdminWatchAccountAndConfirmation() && self::$addScoring) {
        (new MoveRequestScore(new MoveRequest($this->entity_id)))
          ->processNotificationType(NotificationType::retrieve($this->notificationType));
      }
    }
  }

  /**
   * Check when don`t add scoring.
   *
   * @return bool
   *   False = not add.
   */
  private function checkIfAdminWatchAccountAndConfirmation() {
    $check = TRUE;
    $clients = new Clients();
    $clients->setUser($this->user);
    if ($clients->checkUserRoles(array('administrator')) &&
      ($this->notificationType == NotificationTypes::CUSTOMER_VIEW_CONFORMATION_PAGE || $this->notificationType == NotificationTypes::CUSTOMER_VIEW_REQUEST)) {
      $check = FALSE;
    }

    return $check;
  }

  /**
   * Create notification in one line.
   *
   * @param int|null $notification_type
   *   Notification type.
   * @param array $data
   *   Data.
   * @param int $entity_id
   *   Entity id.
   * @param null $entity_type
   *   Entity type.
   * @param int $uid
   *   User id.
   */
  public static function createNotification($notification_type = NULL, $data = array(), $entity_id = 0, $entity_type = NULL, $uid = NULL) {
    $notification = new Notification($notification_type, $entity_id, $entity_type, $uid);
    $notification->create($data);
  }

  public function retrieve() {}

  public function update($data = array()) {}

  public function delete() {}


  public function index($page, $pagesize) {
    if ($page > 0) {
      $page = $page * $pagesize;
    }
    $result = db_select('move_notification', 'mn')
      ->fields('mn')
      ->range($page, $pagesize)
      ->execute()
      ->fetchAllAssoc('id');
    return $result;
  }

  /**
   * Insert notification to db.
   *
   * @param int $uid
   *   User id.
   * @param int $entity_id
   *   Entity id.
   * @param int $entity_type
   *   Entity type.
   * @param int $notification_type
   *   Notification type.
   * @param array $data
   *   Data.
   *
   * @return int
   *   Record id.
   *
   * @throws \Exception
   */
  public function insertNotification($uid, $entity_id , $entity_type, $notification_type, $data) {
    if ((!is_null($entity_id) && $entity_type == NotificationCategories::REQUEST) || $entity_type == NotificationCategories::SITE_SETTINGS) {
      $notification_message = $this->notificationText($notification_type, $data, $uid, $entity_id);

      $notif_id = db_insert('move_notification')
        ->fields(array(
          'notification_type' => $notification_type,
          'a_uid' => $uid,
          'entity_id' => $entity_id,
          'entity_type' => $entity_type,
          'data' => $notification_message,
          'created' => REQUEST_TIME,
        ))
        ->execute();

      $params = array(
        'data' => $notification_message,
        'id' => $notif_id,
        'ntid' => $notification_type,
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
      );
      // Check is allow ro sending email for notification types.
      $is_type_allow_send_mail = self::IsAllowToSendNotificationTypeMail($notification_type);

      if ($is_type_allow_send_mail) {
        self::addNotificationToQueue(2, $notification_type, $params);
      }

      $uids = self::getUsersForNotificationType($notification_type);

      if (!empty($uids)) {
        foreach ($uids as $uid) {
          db_insert('move_notification_status')
            ->fields(array(
              'notif_id' => $notif_id,
              'uid' => $uid['uid'],
              'status' => 0,
              'ntid' => $notification_type,
            ))
            ->execute();
        }
      }

      return $notif_id;
    }
  }

  public function updateNotificationRole($type, $rid) {
    db_merge('move_notification_roles_permissions')
      ->key(array('ntid' => $type, 'urid' => $rid))
      ->fields(array(
        'ntid' => $type,
        'urid' => $rid,
      ))
      ->execute();
    $result['role'] = $rid;
    $result['type'] = $type;
    return $result;
  }

  public function updateNotificationUser($type, $uid) {
    db_merge('move_notification_user_permissions')
      ->key(array('ntid' => $type, 'uid' => $uid))
      ->fields(array(
        'ntid' => $type,
        'uid' => $uid,
        'added' => time(),
      ))
      ->execute();
    $result['user'] = $uid;
    $result['type'] = $type;
    $user_mail_settings = $this->getUserMailSettingForNotificationType($type, $uid);
    if (empty($user_mail_settings)) {
      self::helplerUpdateUserMailSendSetting($uid, $type, 3);
    }
    return $result;
  }

  /**
   * Add user to many notifications types.
   *
   * @param int $uid
   *   Id of user.
   * @param array $types
   *   Array of notification types.
   *
   * @return array|bool
   *   Array with user id and types. Or FALSE if error.
   */
  public function addUserToManyNotificationTypes(int $uid, array $types) {
    $result = FALSE;
    // Delete all notification permissions for user.
    db_delete('move_notification_user_permissions')
      ->condition('uid', $uid)
      ->execute();
    if (!empty($types && is_array($types))) {
      foreach ($types as $type) {
        $result[] = $this->updateNotificationUser($type, $uid);
      }
    }
    return $result;
  }

  /**
   * Insert/Update templates for notification.
   *
   * @param int $type
   *   Type of notification.
   * @param string $text
   *   Text of notification.
   *
   * @return mixed
   */
  public function updateNotificationTypeTemplate(int $type, string $text) {
    db_merge('move_notification_type_templates')
      ->key(array('ntid' => $type))
      ->fields(array(
        'ntid' => $type,
        'text' => $text,
      ))
      ->execute();
    $result['type'] = $type;
    return $result;
  }

  /**
   * @param $type
   * @param $rid
   */
  public function deleteNotificationRole($type, $rid) {
    db_delete('move_notification_roles_permissions')
      ->condition('ntid', $type)
      ->condition('urid', $rid)
      ->execute();
  }

  /**
   * @param $type
   * @param $uid
   */
  public function deleteNotificationUser($type, $uid) {
    db_delete('move_notification_user_permissions')
      ->condition('ntid', $type)
      ->condition('uid', $uid)
      ->execute();
  }

  /**
   * @param $type
   * @param $tid
   */
  public function deleteNotificationTypeTemplate($type) {
    db_delete('move_notification_type_templates')
      ->condition('ntid', $type)
      ->execute();
  }

  public function getAllowedNotification($page = 1, $page_size = -1, $filters = array(), $status = FALSE) {

    $uid = $this->user->uid;
    $rid = $this->userRoleIds;
    if (!is_array($rid)) {
      $rid = array($rid);
    }
    // TODO need some way to make it in one db query.
    if ($status === FALSE) {
//      $all_notification_with_status = self::getAllNotificationWithStatus($uid, $rid);
//      $all_new_notification = self::getAllNewNotification($uid, $rid);
//      $result = array_merge($all_notification_with_status, $all_new_notification);
      $result = (array) self::getAllAllowedNotification($uid, $rid, $page, $page_size, $filters, $this->user);
    }
    elseif ($status === 0) {
      $result = (array) self::getAllNewNotification($uid, $rid, FALSE, $filters);
    }
    else {
      $result = (array) self::getAllNotificationWithStatus($uid, $rid, $status, $filters);
    }
    return $result;
  }

  /**
   * Get all allowed notification for current user.
   *
   * @param int $uid
   *   User id.
   * @param mixed $rid
   *   Role/Roles.
   * @param int $page
   *   Page number.
   * @param int $page_size
   *   NUmber elements on page.
   *
   * @return mixed
   *   Array of notification with text and other useful info.
   */
  public static function getAllAllowedNotification(int $uid, $rid, $page = 1, $page_size = -1, $filters = array(), $user = FALSE) {
    if (!is_array($rid)) {
      $rid = array($rid);
    }


    $is_sales_only = self::isSalesOnly($user);
    // Get all not assigned request for this user
    // and for this notification type.
    if ($is_sales_only) {
      $query = db_select('move_notification', 'mv');
      $query->leftjoin('move_notification_user_allowed_by_id', 'allowed_by_id', 'mv.notification_type = allowed_by_id.ntid');
      $query->leftjoin('move_request_sales_person', 'assigned_person', 'mv.entity_id = assigned_person.nid');
      $query->fields('mv', array('id'));
      $query->condition('mv.entity_type', EntityTypes::MOVEREQUEST);
      $query->condition('allowed_by_id.value', 1);
      $query->condition('assigned_person.uid', $uid, '<>');
      $query->groupBy('mv.id');
      $not_assigned_to_user_ids = $query->execute()->fetchCol();
    }
    $sub_query = db_select('move_notification', 'mv');
    $sub_query->leftjoin('move_notification_status', 'mns', 'mv.id = mns.notif_id');
    $sub_query->fields('mv', array('id'));
    $sub_query->condition('mns.uid', $uid);

    // Remove notification types which user has turn off.
    $excluded_notification_types = db_select('move_notification_users_dont_see_notif', 'mnudsn')
      ->fields('mnudsn', array('ntid'))
      ->condition('mnudsn.uid', $uid)
      ->execute()
      ->fetchCol();

    $or = db_or();
    $query = db_select('move_notification', 'mv');
    $query->leftjoin('move_notification_roles_permissions', 'mnr', 'mv.notification_type = mnr.ntid');
    $query->leftjoin('move_notification_user_permissions', 'mnu', 'mv.notification_type = mnu.ntid AND mv.created > mnu.added');
    $query->leftjoin('move_notification_status', 'mns', 'mv.id = mns.notif_id');
    $query->leftjoin('move_notification_users_dont_see_notif', 'mnudsn', 'mv.notification_type = mnudsn.ntid');
    $query->leftjoin('move_notification_types', 'mnt', 'mv.notification_type = mnt.ntid');
    $query->fields('mv');
    $query->fields('mnt', array('title'));
    $query->fields('mns', array('status', 'uid'));
    $or->condition('mnr.urid', $rid, 'IN');
    $or->condition('mnu.uid', $uid);
    $query->condition($or);
    if (!empty($excluded_notification_types)) {
      $query->condition('mv.notification_type', $excluded_notification_types, 'NOT IN');
    }

    if (!empty($filters)) {
      foreach ($filters as $filter_name => $filter_val) {
        if (!empty($filter_val) || $filter_val === 0) {
          $operator = '=';
          if (is_array($filter_val)) {
            $operator = 'IN';
          }
          $query->condition($filter_name, $filter_val, $operator);
        }
      }
    }

    // We don't need request assigned to other.
    if (!empty($not_assigned_to_user_ids)) {
      $query->condition('mv.id', $not_assigned_to_user_ids, 'NOT IN');
    }
    $query->condition('mv.a_uid', $uid, '<>');

//    $total_count = $query->execute()->rowCount();
//    // Calculate number of pages.

    if ($page_size > 0) {
      //$query->range($start_from_page, $page_size);
    }
    $query->orderBy('mv.created', 'DESC');
    $query_result = $query->execute()->fetchAllAssoc('id');
    $result['items'] = array();
    // TODO need to create other sql query to get all new results without foreach.
    if (!empty($query_result)) {
      foreach ($query_result as $key => $val) {
        if (!empty($val->data)) {
          $val->data = unserialize($val->data);
        }
        // TODO refactor this.
        $is_viewed = self::isNotificationViewed($val->id, $uid);
        $val->status = (empty($is_viewed)) ? 0 : $val->status;
        $val->uid = $uid;
        $result['items'][] = (array) $val;
      }

      // Build pager info for front.
      $total_count = count($result['items']);
      $page_count = ceil($total_count / $page_size);
      $start_from_page = ($page - 1) * $page_size;
      if ($page_size > 0) {
        $result['items'] = array_slice($result['items'], $start_from_page, $page_size);
      }
      $meta = array(
        'currentPage' => (int) $page,
        'perPage' => (int) $page_size,
        'pageCount' => (int) $page_count,
        'totalCount' => (int) $total_count,
      );
      $result['_meta'] = $meta;
      $new_notification = self::getAllNewNotification($uid, $rid, FALSE, $filters, $user);
      $result['count_new'] = !empty($new_notification['items']) ? count($new_notification['items']) : 0;
    }

    return $result;
  }

  /**
   * Get all allowed notification types for user. Used in admin part.
   *
   * @return mixed
   */
  public function getAllowedNotificationTypesForUser() {
    $query = db_select('move_notification_types', 'mnt');
    $query->leftjoin('move_notification_user_permissions', 'mnup', 'mnup.ntid = mnt.ntid');
    $query->leftjoin('move_notification_roles_permissions', 'mnrp', 'mnrp.ntid = mnt.ntid');
    $query->leftjoin('move_notification_type_mail_settings', 'type_mail', 'mnup.ntid = type_mail.ntid AND type_mail.how_often = 2');
    $query->leftjoin('move_notification_user_mail_settings', 'user_mail', 'type_mail.ntid = user_mail.ntid');
    $query->fields('mnt', array('ntid', 'title'));
    $query->addField('user_mail', 'how_often', 'mail');
    $db_or = db_or();
    $db_or->condition('mnup.uid', $this->user->uid);
    $db_or->condition('mnrp.urid  ', $this->userRoleIds, 'IN');
    $query->condition($db_or);
    $query->groupBy('ntid');
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    if (!empty($result)) {
      foreach ($result as $key => $value) {
        if (is_null($result[$key]['mail'])) {
          unset($result[$key]['mail']);
        }
        else {
          $usre_mail_setting = $this->getUserMailSettingForNotificationType($value['ntid']);
          $result[$key]['mail'] = $usre_mail_setting;
        }
        $off_type = $this->getUserOffType($value['ntid']);
        $result[$key]['on'] = empty($off_type) ? 1 : 0;
      }
    }
    return $result;
  }

  /**
   * Change notification status for by notification type and user.
   *
   * @param $notif_id
   * @param $status
   * @return int
   */
  public function changeNotificationStatus($notif_id, $status) {
    return self::updateNotificationStatus($notif_id, $status, $this->user->uid);
  }

  /**
   * Update notification status for use multiple.
   *
   * @param array $ntids
   *   Ids for notification.
   *
   * @param int $status
   *   Status of request.
   */
  public function changeNotificationStatusMultiple(array $ids, $status) {
    $result = array();
    foreach ($ids as $id) {
      $this->changeNotificationStatus($id, $status);
      $result[$id] = $status;
    }
    return $result;
  }

  /**
   * Update status for one notification.
   * @param $notif_id
   * @param int $status
   * @param $uid
   * @return int
   */
  public static function updateNotificationStatus($notif_id, int $status, $uid) {
    $inserted = db_update('move_notification_status')
      ->fields(array(
        'status' => $status,
      ))
      ->condition('notif_id', $notif_id)
      ->condition('uid', $uid)
      ->execute();
    return $inserted;
  }

  /**
   * Update status for notiications by they types ids.
   *
   * @param array $ntids
   *   Array of notification type ids.
   * @param int $status
   *   Status to update.
   */
  public function updateNotificationStatusByTypes(array $ntids, int $status) {
    $result = array();
    if (!empty($ntids)) {
      db_update('move_notification_status')
        ->fields(array('status' => $status))
        ->condition('uid', $this->user->uid)
        ->condition('ntid', $ntids, 'IN')
        ->execute();
    }

    return TRUE;
  }

  /**
   * Get only new notification for user.
   *
   * @param $uid
   * @param $rid
   * @param bool $date
   * @return mixed
   */
  public static function getAllNewNotification($uid, $rid = FALSE, $date = FALSE, $filters = array(), $user = FALSE, $page = 1, $page_size = -1) {

    if (!is_array($rid) && !empty($rid)) {
      $rid = array($rid);
    }

    $is_sales_only = self::isSalesOnly($user);
    // Get all not assigned request for this user
    // and for this notification type.
    if ($is_sales_only) {
      $query = db_select('move_notification', 'mv');
      $query->leftjoin('move_notification_user_allowed_by_id', 'allowed_by_id', 'mv.notification_type = allowed_by_id.ntid');
      $query->leftjoin('move_request_sales_person', 'assigned_person', 'mv.entity_id = assigned_person.nid');
      $query->fields('mv', array('id'));
      $query->condition('mv.entity_type', EntityTypes::MOVEREQUEST);
      $query->condition('allowed_by_id.value', 1);
      $query->condition('assigned_person.uid', $uid, '<>');
      $query->groupBy('mv.id');
      $not_assigned_to_user_ids = $query->execute()->fetchCol();
    }

    $sub_query = db_select('move_notification', 'mv');
    $sub_query->leftjoin('move_notification_status', 'mns', 'mv.id = mns.notif_id');
    $sub_query->fields('mv', array('id'));
    $sub_query->condition('mns.uid', $uid);
    $sub_query->condition('mns.status', 1);
    $readed = $sub_query->execute()->fetchCol();

    // Remove notification types which user has turn off.
    $excluded_notification_types = db_select('move_notification_users_dont_see_notif', 'mnudsn')
      ->fields('mnudsn', array('ntid'))
      ->condition('mnudsn.uid', $uid)
      ->execute()
      ->fetchCol();

    $or = db_or();
    $query = db_select('move_notification', 'mv');
    $query->leftjoin('move_notification_roles_permissions', 'mnr', 'mv.notification_type = mnr.ntid');
    $query->leftjoin('move_notification_user_permissions', 'mnu', 'mv.notification_type = mnu.ntid AND mv.created > mnu.added');
    $query->leftjoin('move_notification_status', 'mns', 'mv.id = mns.notif_id AND mns.uid = ' . $uid);
    $query->leftjoin('move_notification_types', 'mnt', 'mv.notification_type = mnt.ntid');
    $query->fields('mv');
    $query->fields('mnt', array('title'));
    $query->fields('mns', array('status', 'uid'));
    if (!empty($rid)) {
      $or->condition('mnr.urid', $rid, 'IN');
    }
    $or->condition('mnu.uid', $uid);
    $query->condition($or);
    if (!empty($readed)) {
      $query->condition('mv.id', $readed, 'NOT IN');
    }
    if (!empty($excluded_notification_types)) {
      $query->condition('mv.notification_type', $excluded_notification_types, 'NOT IN');
    }
    $query->condition('mv.a_uid', $uid, '<>');
    $query->isNotNull('mns.status');
    // We don't need request assigned to other.
    if (!empty($not_assigned_to_user_ids)) {
      $query->condition('mv.id', $not_assigned_to_user_ids, 'NOT IN');
    }

    if (!empty($filters)) {
      foreach ($filters as $filter_name => $filter_val) {
        if (!empty($filter_val) || $filter_val === 0) {
          $operator = '=';
          if (is_array($filter_val)) {
            $operator = 'IN';
          }
          $query->condition($filter_name, $filter_val, $operator);
        }
      }
    }

    $query_for_total = clone $query;
    $total = $query_for_total->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);
    $result['count_new'] = !empty($total) ? count($total) : 0;

    if (!empty($date)) {
      $query->condition('mv.created', $date, '>');
    }
    $query->orderBy('mv.created', 'DESC');
    $query_result = $query->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

    // Build pager info for front.
    $total_count = count($query_result);
    $page_count = ceil($total_count / $page_size);
    $start_from_page = ($page - 1) * $page_size;
    if ($page_size > 0) {
      $query_result = array_slice($query_result, $start_from_page, $page_size);
    }
    $meta = array(
      'currentPage' => (int) $page,
      'perPage' => (int) $page_size,
      'pageCount' => (int) $page_count,
      'totalCount' => (int) $total_count,
    );
    $result['_meta'] = $meta;

    // TODO need to create other sql query to get all new results without foreach.
    if (!empty($query_result)) {
      foreach ($query_result as $key => $val) {
        if (!empty($val['data'])) {
          $val['data'] = unserialize($val['data']);
        }
        $query_result[$key] = $val;
        $query_result[$key]['status'] = 0;
        $query_result[$key]['uid'] = $uid;
      }
    }
    $result['items'] = array_values($query_result);
    return $result;
  }

  /**
   * @param bool $date
   * @return mixed
   */
  public function showNewNotificationForCurrentUser($date = FALSE, $filters = array()) {
    return self::getAllNewNotification($this->user->uid, $this->userRoleIds, $date, $filters, $this->user);
  }

  /**
   *
   * Method for crud service.
   * @param int $page
   * @param int $page_size
   * @param int $status
   * @param array $filters
   * @return mixed
   */
  public function showNotificationWithStatusForCurrentUser($page = 1, $page_size = -1, int $status = 0, $filters = array()) {
    return self::getAllNotificationWithStatus($this->user->uid, $this->userRoleIds, $page, $page_size, $status, $filters, $this->user);
  }

  /**
   * @param $uid
   * @param $rid
   * @param int $status
   * @return mixed
   */
  public static function getAllNotificationWithStatus($uid, $rid, $page = 1, $page_size = -1, int $status = 0, $filters = array(), $user = FALSE) {
    if (!is_array($rid)) {
      $rid = array($rid);
    }

    if ($status === 0) {
      $result = self::getAllNewNotification($uid, $rid, FALSE, $filters = array(), $user, $page, $page_size);
    }
    else {
      $is_sales_only = self::isSalesOnly($user);
      // Get all not assigned request for this user
      // and for this notification type.
      if ($is_sales_only) {
        $query = db_select('move_notification', 'mv');
        $query->leftjoin('move_notification_user_allowed_by_id', 'allowed_by_id', 'mv.notification_type = allowed_by_id.ntid');
        $query->leftjoin('move_request_sales_person', 'assigned_person', 'mv.entity_id = assigned_person.nid');
        $query->fields('mv', array('id'));
        $query->condition('mv.entity_type', EntityTypes::MOVEREQUEST);
        $query->condition('allowed_by_id.value', 1);
        $query->condition('assigned_person.uid', $uid, '<>');
        $query->groupBy('mv.id');
        $not_assigned_to_user_ids = $query->execute()->fetchCol();
      }

      // Remove notification types which user has turn off.
      $excluded_notification_types = db_select('move_notification_users_dont_see_notif', 'mnudsn')
        ->fields('mnudsn', array('ntid'))
        ->condition('mnudsn.uid', $uid)
        ->execute()
        ->fetchCol();

      $or = db_or();
      $query = db_select('move_notification', 'mv');
      $query->leftjoin('move_notification_roles_permissions', 'mnr', 'mv.notification_type = mnr.ntid');
      $query->leftjoin('move_notification_user_permissions', 'mnu', 'mv.notification_type = mnu.ntid AND mv.created > mnu.added');
      $query->leftjoin('move_notification_status', 'mns', 'mv.id = mns.notif_id');
      $query->leftjoin('move_notification_users_dont_see_notif', 'mnudsn', 'mv.notification_type = mnudsn.ntid');
      $query->leftjoin('move_notification_types', 'mnt', 'mv.notification_type = mnt.ntid');
      $query->fields('mv');
      $query->fields('mnt', array('title'));
      $query->fields('mns', array('status', 'uid'));
      $or->condition('mnr.urid', $rid, 'IN');
      $or->condition('mnu.uid', $uid);
      $query->condition($or);
      $or2 = db_or();
      $or2->condition('mns.uid', $uid);
      $query->condition($or2);
      if (!empty($excluded_notification_types)) {
        $query->condition('mv.notification_type', $excluded_notification_types, 'NOT IN');
      }

      if (!empty($filters)) {
        foreach ($filters as $filter_name => $filter_val) {
          if (!empty($filter_val) || $filter_val === 0) {
            $operator = '=';
            if (is_array($filter_val)) {
              $operator = 'IN';
            }
            $query->condition($filter_name, $filter_val, $operator);
          }
        }
      }

      // We don't need request assigned to other.
      if (!empty($not_assigned_to_user_ids)) {
        $query->condition('mv.id', $not_assigned_to_user_ids, 'NOT IN');
      }
      $query->condition('mv.a_uid', $uid, '<>');

      if ($status) {
        $query->condition('mns.status', $status);
      }
      else {
        $query->IsNotNull('mns.status');
      }

      $query->orderBy('mv.created', 'DESC');
      $query_result = $query->execute()->fetchAllAssoc('id');
      $result['items'] = array();
      // TODO need to create other sql query to get all new results without foreach.
      if (!empty($query_result)) {
        foreach ($query_result as $key => $val) {
          if (!empty($val->data)) {
            $val->data = unserialize($val->data);
          }
          // TODO refactor this.
          $is_viewed = self::isNotificationViewed($val->id, $uid);
          $val->status = (empty($is_viewed)) ? 0 : $val->status;
          $val->uid = $uid;
          $result['items'][] = (array) $val;
        }

        // Build pager info for front.
        $total_count = count($result['items']);
        $page_count = ceil($total_count / $page_size);
        $start_from_page = ($page - 1) * $page_size;
        if ($page_size > 0) {
          $result['items'] = array_slice($result['items'], $start_from_page, $page_size);
        }
        $meta = array(
          'currentPage' => (int) $page,
          'perPage' => (int) $page_size,
          'pageCount' => (int) $page_count,
          'totalCount' => (int) $total_count,
        );
        $result['_meta'] = $meta;
        $new_notification = self::getAllNewNotification($uid, $rid, FALSE, $filters, $user);
        $result['count_new'] = !empty($new_notification['items']) ? count($new_notification['items']) : 0;
      }
    }

    return $result;
  }

  public static function getUsersForNotificationType($ntid, $display = 'full') {
    $query = db_select('move_notification_user_permissions', 'mnup');
    $query->leftJoin('users', 'u', 'mnup.uid = u.uid');
    $query->leftJoin('field_data_field_user_first_name', 'first_name', 'mnup.uid = first_name.entity_id');
    $query->leftJoin('field_data_field_user_last_name', 'last_name', 'mnup.uid = last_name.entity_id');
    $query->fields('u', array('uid', 'name'));
    $query->condition('mnup.ntid', $ntid);
    $query->groupBy('u.uid');
    switch ($display) {
      case 'uid_only':
        $result = $query->execute()->fetchCol();
        break;

      default:
        $query->fields('first_name', array('field_user_first_name_value'));
        $query->fields('last_name', array('field_user_last_name_value'));

        $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
        if (!empty($result)) {
          foreach ($result as $key => $item) {
            $result[$key]['name'] = $item['field_user_first_name_value'] . ' ' . $item['field_user_last_name_value'];
            unset($result[$key]['field_user_first_name_value']);
            unset($result[$key]['field_user_last_name_value']);
          }
        }
        break;
    }
    return $result;
  }

  public static function getRolesForNotificationType($ntid) {
    $query = db_select('move_notification_roles_permissions', 'mnrp');
    $query->leftJoin('role', 'ur', 'mnrp.urid = ur.rid');
    $query->fields('ur', array('rid', 'name'))
      ->condition('mnrp.ntid', $ntid);
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  public static function getTemplatesForNotificationType($ntid) {
    $query = db_select('move_notification_type_templates', 'mnt');
    $query->fields('mnt', array('text'))
      ->condition('mnt.ntid', $ntid);
    $result = $query->execute()->fetchField();
    return $result;
  }

  /**
   * Get notification info. Users, email, roles.
   *
   * @param $ntid
   * @return mixed
   */
  public static function getInfoForNotificationType($ntid) {
    $result['users'] = self::getUsersForNotificationType($ntid);
    $result['roles'] = self::getRolesForNotificationType($ntid);
    $result['templates'] = self::getTemplatesForNotificationType($ntid);
    $result['mail'] = self::getNotificationTypeMailSettings($ntid);
    $result['sales_permission'] = self::getNotificationTypePermissionByEntity($ntid);
    return $result;
  }

  /**
   * Get all notification types with all info. USed in admin part.
   *
   * @return array
   */
  public static function getAllNotificationTypesWithInfo() {
    $result = array();
    $query = db_select('move_notification_types', 'mnt')
      ->fields('mnt')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($query as $row) {
      $row['info'] = self::getInfoForNotificationType($row['ntid']);
      $row['notification_with_score'] = static::scoreNotificationType($row['ntid']);
      $result[] = $row;
    }

    return $result;
  }

  /**
   * Notification score flags.
   *
   * @param $ntid
   * @return int
   */
  private static function scoreNotificationType($ntid) {
    $result = 0;

    switch ($ntid) {
      case NotificationTypes::CUSTOMER_SUBMIT_INVENTORY:
      case NotificationTypes::CUSTOMER_DO_CHANGE_WITH_REQUEST:
      case NotificationTypes::CUSTOMER_VIEW_REQUEST:
      case NotificationTypes::CUSTOMER_READ_EMAIL:
      case NotificationTypes::CUSTOMER_SEND_MESSAGE:
      case NotificationTypes::VIEW_INVOICE:
      case NotificationTypes::PAY_INVOICE:
      case NotificationTypes::CUSTOMER_BUY_COUPON:
      case NotificationTypes::CUSTOMER_VIEW_CONFORMATION_PAGE:
        $result = 1;
    }

    return $result;
  }

  /**
   * Get all notification types only. Array key is type id.
   * @return mixed
   */
  public static function getAllNotificationTypesOnly() {
    $result = array();
    $query = db_select('move_notification_types', 'mnt')
      ->fields('mnt', array('ntid', 'title'))
      ->execute()
      ->fetchAllAssoc('ntid');
    if(!empty($query)) {
      foreach ($query as $row) {
        $result[$row->ntid] = $row->title;
      }
    }
    return $result;
  }


  /**
   * Add or remove unsubscribe user from notification.
   *
   * @param array $ntids
   *   Notification types id.
   * @param bool $status
   *   Status.
   *
   * @return bool
   */
  public function updateSubscribeForNotification(array $ntids, bool $status = FALSE) {
    if (!empty($ntids)) {
      db_delete('move_notification_users_dont_see_notif')
        ->condition('ntid', $ntids, 'IN')
        ->condition('uid', $this->user->uid)
        ->execute();

      if (!$status) {
        foreach ($ntids as $ntid) {
          db_insert('move_notification_users_dont_see_notif')
            ->fields(array('ntid' => $ntid, 'uid' => $this->user->uid))
            ->execute();
        }
      }
    }
    return TRUE;
  }

  /**
   * Get user unsubscribe notification type.
   * @param $ntid
   * @return mixed
   */
  public function getUserOffType($ntid) {
    $unsubscribed = db_select('move_notification_users_dont_see_notif', 'unsub')
      ->fields('unsub', array('ntid'))
      ->condition('ntid', $ntid)
      ->condition('uid', $this->user->uid)
      ->execute()
      ->fetchField();
    return $unsubscribed;
  }

  /**
   * How often send emails with notifications to user.
   *
   * @param int $how_often
   *   How often send emails.
   *
   * @return int
   */
  public function updateUserMailSendSetting(int $ntid, int $how_often) {
    return self::helplerUpdateUserMailSendSetting($this->user->uid, $ntid, $how_often);
  }

  public static function helplerUpdateUserMailSendSetting($uid, $type, $how_often) {
      $inserted = db_merge('move_notification_user_mail_settings')
        ->key(array('uid' => $uid, 'ntid' => $type))
        ->fields(array('uid' => $uid, 'ntid' => $type, 'how_often' => $how_often))
        ->execute();
      return $how_often;
  }

  /**
   * Update notification type mail settings.
   * @param $ntid
   * @param $how_often
   * @return int
   */
  public static function updateNotificationTypeMailSettings(int $ntid, int $how_often) {
    $inserted = db_merge('move_notification_type_mail_settings')
      ->key(array('ntid' => $ntid))
      ->fields(array('ntid' => $ntid, 'how_often' => $how_often))
      ->execute();
    return $how_often;
  }

  /**
   * Get notification type mail settings.
   * @param int $ntid
   * @return mixed
   */
  public static function getNotificationTypeMailSettings(int $ntid) {
    $mail_settings = db_select('move_notification_type_mail_settings', 'mail_settings')
      ->fields('mail_settings', array('how_often'))
      ->condition('ntid', $ntid)
      ->execute()
      ->fetchField();
    return $mail_settings;
  }

  /**
   *
   * @param $ntid
   * @return mixed
   */
  public function getUserMailSettingForNotificationType($ntid, $uid = FALSE) {
    if (empty($uid)) {
      $uid = $this->user->uid;
    }
    $mail_setting = db_select('move_notification_user_mail_settings', 'ms')
      ->fields('ms', array('how_often'))
      ->condition('uid', $uid)
      ->condition('ntid', $ntid)
      ->execute()
      ->fetchField();
    return $mail_setting;

  }

  /**
   * Is allow to send notification type mail.
   *
   * @param $ntid
   * @return mixed
   */
  public static function IsAllowToSendNotificationTypeMail($ntid) {
    $mail_settings = db_select('move_notification_type_mail_settings', 'mail_settings')
      ->fields('mail_settings', array('how_often'))
      ->condition('ntid', $ntid)
      ->condition('how_often', 2)
      ->execute()
      ->fetchField();
    return $mail_settings;
  }

  /**
   * Set/update when was send last email.
   *
   * @param int $uid
   * @return int
   */
  public static function updateUserLastSendEmailDate(int $uid) {
    $inserted = db_merge('move_notification_user_mail_last_send')
      ->key(array('uid' => $uid))
      ->fields(array('uid' => $uid, 'date' => time()))
      ->execute();
    return $inserted;
  }

  /**
   * Add notification mail to the queue.
   *
   * @param int $how_often
   *   How often send email.
   * @param bool $ntid
   *   Notification type id.
   * @param bool $params
   *   Body, subject. other nedded things.
   */
  public static function addNotificationToQueue($how_often = 2, $ntid = FALSE, $params = FALSE) {
    switch ($how_often) {
      case 2:
        $uids = self::getUsersToSendEmailNotificationNow($ntid);
        if (!empty($uids)) {
          foreach ($uids as $uid) {
            if (!empty($uid)) {
              $params['uid'] = $uid;
              $info_for_queue[$uid][] = $params;
            }
          }
        }
        break;

      case 1:
        $info_for_queue = self::getUsersToSendNotificationByTime(1);
    }
    if (!empty($info_for_queue)) {
      $queue = \DrupalQueue::get('move_notification_send_notification');
      $queue->createQueue();
      foreach ($info_for_queue as $items) {
        foreach ($items as $item) {
          $queue->createItem((array) $item);
        }
      }
    }
  }

  /**
   * Get all users to send email now.
   *
   * @param int $ntid
   *   Id of notification type.
   *
   * @return array
   *   Array of users id.
   */
  public static function getUsersToSendEmailNotificationNow(int $ntid) {
    $uids_to_send_now = db_select('move_notification_user_mail_settings', 'mnums')
      ->fields('mnums', array('uid'))
      ->condition('how_often', array(2))
      ->condition('ntid', $ntid)
      ->execute()
      ->fetchCol();
    return $uids_to_send_now;
  }

  /**
   * Get all user id to send email by time.
   *
   * @param int $time
   */
  public static function getUsersToSendNotificationByTime($time = 1) {
    $notifications = array();
    $time_ago = time() - $time * 3600;
    $query = db_select('move_notification_user_mail_settings', 'setting');
    $query->leftJoin('move_notification_user_mail_last_send', 'last_send', 'last_send.uid = setting.uid');
    $query->fields('setting', array('uid'));
    $query->condition('how_often', 1);
    $db_or = db_or();
    $db_or->condition('last_send.date', $time_ago, '<=');
    $db_or->isNull('last_send.date');
    $query->condition($db_or);
    $result = $query->execute()->fetchCol();
    if (!empty($result)) {
      foreach ($result as $uid) {
        if (!empty($uid)) {
          $user_notifications = self::getAllNewNotification($uid);
          if (!empty($user_notifications)) {
            $notifications[$uid] = $user_notifications;
          }
        }
      }
    }
    return $notifications;
  }

  /**
   * Get title for notification type by id.
   *
   * @param int $ntid
   *   Notification type id.
   *
   * @return mixed
   */
  public static function getNotificationTitleForType(int $ntid) {
    $title = db_select('move_notification_types', 'mvt')
      ->fields('mvt', array('title'))
      ->condition('ntid', $ntid)
      ->execute()
      ->fetchField();
    return $title;
  }

  /**
   * Get user id by they role for the notification type.
   *
   * @param int $ntid
   * @return mixed
   */
  public static function getUsersIdByRoleForNotificationType(int $ntid) {
    $query = db_select('move_notification_roles_permissions', 'mnrp');
    $query->leftJoin('users_roles', 'ur', 'mnrp.urid = ur.rid');
    $query->fields('ur', array('uid'))
      ->condition('mnrp.ntid', $ntid);
    $result = $query->execute()->fetchCol();
    return $result;
  }

  /**
   * Check if notification is viewed.
   *
   * @param $id
   *   Id of notification.
   * @param $uid
   *   Id of user.
   *
   * @return mixed
   */
  public static function isNotificationViewed(int $id, int $uid) {
    $title = db_select('move_notification_status', 'mvs')
      ->fields('mvs', array('status'))
      ->condition('notif_id', $id)
      ->condition('uid', $uid)
      ->condition('status', 1)
      ->execute()
      ->fetchField();
    return $title;
  }

  /**
   * Allow user with role sales see only his requests.
   * @param int $ntid
   * @param int $status
   * @return int
   */
  public function updateUserNotificationPermissionByEntity(int $ntid, int $status = 0) {
    db_merge('move_notification_user_allowed_by_id')
      ->key(array('ntid' => $ntid))
      ->fields(array('ntid' => $ntid, 'value' => $status))
      ->execute();
    return $status;
  }

  /**
   * Get status for notification type. Dose sales can see hes own notification requests or all.
   *
   * @param int $ntid
   *  Notification type id.
   *
   * @return mixed
   */
  public static function getNotificationTypePermissionByEntity(int $ntid) {
    $status = db_select('move_notification_user_allowed_by_id', 'allowed')
      ->fields('allowed', array('value'))
      ->condition('ntid', $ntid)
      ->execute()
      ->fetchField();
    return (int) $status;
  }

  public static function isUserAllowSeeAssigned($uid, $ntid, $entity_id, $entity_type) {
    $type_allowed_see_assigned = self::getNotificationTypePermissionByEntity($ntid);
    if ($type_allowed_see_assigned && $entity_type === EntityTypes::MOVEREQUEST) {
      $sales_uid = MoveRequest::getRequestManager($entity_id);
      if ($sales_uid == $uid) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * @param array $roles
   * @return bool
   */
  public static function isSalesOnly($user) {
    $needed_roles = array('sales', 'manager', 'administrator');
    $roles = array_intersect($needed_roles, $user->roles);
    $roles_count = count($roles);
    if(in_array('sales', $roles) && $roles_count === 1) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Build notification text by type.
   *
   * @param int $type
   *   Notification type.
   * @param array $variables
   *   Array of variables for replace in token.
   *
   * @return mixed
   *   Text for notification.
   */
  public function notificationText(int $type, $variables = array(), int $uid, $entity_id) {
    if ($type == NotificationTypes::SITE_SETTINGS) {
      $source = 'System';
    }
    else {
      $user_wrapper = entity_metadata_wrapper('user', $this->user);
      $source = $user_wrapper->field_user_first_name->value() . ' ' . $user_wrapper->field_user_last_name->value();
    }

    $template = self::getTemplatesForNotificationType($type);

    if (empty($template)) {
      $template = '[notification:text]';
    }
    // If empty user in variable add current user for tokens.
    if (empty($variables['user'])) {
      $variables['user'] = $this->user;
    }

    if(isset($variables['node']) && !is_array($variables['node'])) {
      $variables['node'] = (object) $variables['node'];
      $variables['node']->type = 'move_request';
    }
    else {
      $variables['node'] = node_load($entity_id);
    }
    $text = token_replace($template, $variables, array('sanitize' => FALSE));
    $result = array(
      'entity_id' => $entity_id,
      'source' => $source,
      'text' => $text,
    );
    return serialize($result);
  }

  public function isAllowToSaveNotification($type) {
    $allow = FALSE;
    switch ($type) {
      case NotificationTypes::CUSTOMER_BOOK_JOB:
        $allow = TRUE;
        break;

      case NotificationTypes::CUSTOMER_SUBMIT_INVENTORY:
        $allow = TRUE;
        break;

      case NotificationTypes::CUSTOMER_DO_CHANGE_WITH_REQUEST:
        $allow = TRUE;
        break;

      case NotificationTypes::CUSTOMER_VIEW_REQUEST:
        if (count($this->userRoleIds) === 1 && in_array('authenticated user', $this->user->roles)) {
          $allow = TRUE;
        }
        break;

      case NotificationTypes::CUSTOMER_PAY_DEPOSIT:
        $allow = TRUE;
        break;

      case NotificationTypes::CUSTOMER_PAY_CASH_FROM_CONTRACT_ANY_PAYMENT:
        $allow = TRUE;
        break;

      case NotificationTypes::CUSTOMER_READ_EMAIL:
        $allow = TRUE;
        break;

      case NotificationTypes::CUSTOMER_SEND_MESSAGE:
        if (count($this->userRoleIds) === 1 && in_array('authenticated user', $this->user->roles)) {
          $allow = TRUE;
        }
        break;

      case NotificationTypes::CLIENT_ADD_REVIEW:
        $allow = TRUE;
        break;

      case NotificationTypes::FORMENT_VIEW_CONTACT:
        if (in_array('foreman', $this->user->roles)) {
          $allow = TRUE;
        }
        break;

      case NotificationTypes::FORMENT_CLOSE_CONTRACT:
        $allow = TRUE;
        break;

      case NotificationTypes::VIEW_INVOICE:
        $allow = TRUE;
        break;

      case NotificationTypes::PAY_INVOICE:
        $allow = TRUE;
        break;

      case NotificationTypes::NEW_REQUEST:
        if (count($this->userRoleIds) === 1 && in_array('authenticated user', $this->user->roles)) {
          $allow = TRUE;
        }
        break;

      case NotificationTypes::CONTRACT_ACTIONS:
        $allow = TRUE;
        break;

      case NotificationTypes::JOB_EXPIRED_CANCELED:
        $allow = TRUE;
        break;

      case NotificationTypes::JOB_IS_COMPLETED:
        $allow = TRUE;

        break;

      case NotificationTypes::FORMENT_CLOSE_JOB:
        $allow = TRUE;
        break;

      case NotificationTypes::CUSTOMER_VIEW_CONFORMATION_PAGE:
        if (count($this->userRoleIds) === 1 && in_array('authenticated user', $this->user->roles)) {
          $allow = TRUE;
        }
        break;

      case NotificationTypes::SITE_SETTINGS:
         $allow = TRUE;
        break;

      case NotificationTypes::SMS_LOW_BALANCE:
        $allow = TRUE;
        break;

      default:
        $allow = TRUE;
    }
    return $allow;
  }
  /**
   * Method return template by key name.
   *
   * @param string $review_template
   *   Key name template.
   *
   * @return array
   *   Template and subject.
   */
  public static function getNotificationTemplate(string $review_template, $custom_subject = FALSE) {
    $data = array();
    $query_template = db_select('move_template_builder', 'mtb')
      ->fields('mtb')
      ->condition('mtb.key_name', '%' . db_like($review_template) . '%', 'LIKE')
      ->execute()
      ->fetchAll();

    if (isset($query_template) && $query_template) {
      $query_template = reset($query_template);
      $subject = json_decode($query_template->data);

      $data = array(
        [
          'template' => $query_template->template,
          'subject'   => !empty($custom_subject) ? $custom_subject : $subject->subject,
          'key_name'   => $query_template->key_name,
        ],
      );
    }

    return $data;
  }
}
