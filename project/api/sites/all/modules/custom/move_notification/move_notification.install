<?php

/**
 * @file
 * Contains install and update functions for this module.
 */

/**
 * Implements hook_schema().
 */
function move_notification_schema() {

  $schema['move_notification'] = array(
    'description' => 'Table for notification',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique notification ID.',
      ),
      'notification_type' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Id of notification type',
        'not null' => FALSE,
      ),
      'a_uid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Id users who create this notification',
        'not null' => FALSE,
      ),
      'entity_id' => array(
        'type' => 'int',
        'description' => 'The id of entity.',
        'not null' => TRUE,
      ),
      'entity_type' => array(
        'type' => 'int',
        'description' => 'The type of entity.',
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'medium',
        'serialize' => TRUE,
        'description' => 'Notification text',
      ),
      'created' => array(
        'type' => 'int',
        'description' => 'The Unix timestamp when the notification was created.',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'notification_type' => array('notification_type'),
      'a_uid' => array('a_uid'),
      'created' => array('created'),
      'entity_id' => array('entity_id'),
      'entity_type' => array('entity_type'),
    ),
  );

  $schema['move_notification_user_permissions'] = array(
    'description' => 'Table for notification permissions by user',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique notification ID.',
      ),
      'ntid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Notification type id',
        'not null' => FALSE,
      ),
      'uid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'User id who can see this notification',
        'not null' => FALSE,
      ),
      'added' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Time when user was add to see notification',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'uid' => array('uid'),
      'ntid' => array('ntid'),
      'added' => array('added'),
    ),
  );

  $schema['move_notification_roles_permissions'] = array(
    'description' => 'Table for notification permissions by user role',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique notification ID.',
      ),
      'ntid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Notification type id',
        'not null' => FALSE,
      ),
      'urid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Users role who can see this notification',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'urid' => array('urid'),
      'ntid' => array('ntid'),
    ),
  );

  $schema['move_notification_type_templates'] = array(
    'description' => 'Template for notification types',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique record id',
      ),
      'ntid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Notification type id',
        'not null' => FALSE,
      ),
      'text' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'medium',
        'serialize' => TRUE,
        'description' => 'Template text',
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'ntid' => array('ntid'),
    ),
  );

  $schema['move_notification_status'] = array(
    'description' => 'Table for notification permissions by user or role',
    'fields' => array(
      'notif_id' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Notification id',
        'not null' => FALSE,
      ),
      'uid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'User id who can see this notification',
        'not null' => FALSE,
      ),
      'status' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Notification status',
        'not null' => FALSE,
      ),
      'ntid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Notification type',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'uid' => array('uid'),
      'status' => array('status'),
      'notif_id' => array('notif_id'),
      'ntid' => array('ntid'),
    ),
  );

  $schema['move_notification_users_dont_see_notif'] = array(
    'description' => 'Table for users who dotn\'t want to see notification',
    'fields' => array(
      'ntid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Notification type id',
        'not null' => FALSE,
      ),
      'uid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'User id who can see this notification',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'uid' => array('uid'),
      'ntid' => array('ntid'),
    ),
  );

  $schema['move_notification_templates'] = array(
    'description' => 'Template for notification',
    'fields' => array(
      'tid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique notification ID.',
      ),
      'title' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
        'description' => 'Title text',
      ),
      'body' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'medium',
        'serialize' => TRUE,
        'description' => 'Template text',
      ),
    ),
    'primary key' => array('tid'),
  );

  $schema['move_notification_types'] = array(
    'description' => 'Notification types with name',
    'fields' => array(
      'ntid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique notification ID.',
      ),
      'title' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
        'description' => 'Title text',
      ),
      'type' => array(
        'type' => 'int',
        'not null' => FALSE,
        'length' => 1,
        'default' => 0,
      ),
    ),
    'primary key' => array('ntid'),
  );

  $schema['move_notification_user_mail_settings'] = array(
    'description' => 'Setting when to send email',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique record id',
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
      'how_often' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
      'ntid' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'uid' => array('uid'),
      'ntid' => array('ntid'),
      'how_often' => array('how_often'),
    ),
  );

  $schema['move_notification_type_mail_settings'] = array(
    'description' => 'Setting is allow notification type to be send by email',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique record id',
      ),
      'ntid' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
      'how_often' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'ntid' => array('ntid'),
      'how_often' => array('how_often'),
    ),
  );

  $schema['move_notification_user_mail_last_send'] = array(
    'description' => 'Setting when was lst send email',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique record id',
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
      'date' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'uid' => array('uid'),
      'date' => array('date'),
    ),
  );

  $schema['move_notification_user_allowed_by_id'] = array(
    'description' => 'Show notification user for only specific user',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique record id',
      ),
      'ntid' => array(
        'type' => 'int',
        'not null' => FALSE,
      ),
      'value' => array(
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'ntid' => array('ntid'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function move_notification_install() {
  $schema = move_notification_schema();
  foreach ($schema as $table_name => $fields) {
    if (!db_table_exists($table_name)) {
      db_create_table($table_name, $schema[$table_name]);
    }
  }
  add_default_values();
}

/**
 * Default notification.
 *
 * @return array
 *   Array with notification types and titles.
 */
function notification_types() {
  return array(
    1 => array('title' => 'When a customer submits inventory', 'type' => 0),
    2 => array('title' => 'A customer does any changes to their request', 'type' => 0),
    3 => array('title' => 'When a customer views a request', 'type' => 0),
    4 => array('title' => 'When a customer pays deposit', 'type' => 0),
    5 => array('title' => 'A customer pays by cash from the contract / any kind of payments', 'type' => 1),
    6 => array('title' => 'When a customer reads an email', 'type' => 0),
    7 => array('title' => 'A customer sends a message', 'type' => 0),
    8 => array('title' => 'When a customer submits a review', 'type' => 1),
    9 => array('title' => 'A foreman views the contract', 'type' => 2),
    10 => array('title' => 'A foreman closes the contract', 'type' => 2),
    11 => array('title' => 'A customer views an invoice', 'type' => 0),
    12 => array('title' => 'A customer pays an invoice', 'type' => 0),
    13 => array('title' => 'When New Request was submitted', 'type' => 0),
    14 => array('title' => 'All contract steps and moves', 'type' => 2),
    15 => array('title' => 'When job is expired, canceled, archived, etc.', 'type' => 0),
    16 => array('title' => 'Job is completed', 'type' => 0),
    17 => array('title' => 'A foreman closes the job', 'type' => 0),
    18 => array('title' => 'A customer buys a coupon', 'type' => 1),
    19 => array('title' => 'A customer approves a deposit', 'type' => 0),
    20 => array('title' => 'A manager does any changes to the request', 'type' => 0),
    21 => array('title' => 'A customer views the confirmation page', 'type' => 0),
    22 => array('title' => 'When a customer pays by cash from the contract', 'type' => 1),
    23 => array('title' => 'When a customer pays by check from the contract', 'type' => 2),
    24 => array('title' => 'When a customer pays by credit card from the contract', 'type' => 2),
    25 => array('title' => 'When a customer pays by cash from the storage agreement', 'type' => 2),
    26 => array('title' => 'When a customer pays by check from the storage agreement', 'type' => 2),
    27 => array('title' => 'When a customer pays by credit card from the storage agreement', 'type' => 2),
    28 => array('title' => 'When a customer pays from the storage agreement', 'type' => 2),
    29 => array('title' => 'When a customer pays from the contract', 'type' => 2),
    30 => array('title' => 'Site settings', 'type' => 2),
    31 => array('title' => 'When a customer books the job', 'type' => 0),
  );
}

/**
 * Add first notification types.
 */
function add_default_values() {
  $schema = move_notification_schema();
  $table_name = 'move_notification_types';
  $field_name = 'type';
  if (!db_field_exists($table_name, $field_name)) {
    db_add_field($table_name, $field_name, $schema[$table_name]['fields'][$field_name]);
  }

  $notification_templates = array(
    1 => '[notification:text]',
    2 => '[notification:text]',
    3 => '[notification:text]',
    4 => '[notification:text]',
    5 => '[notification:text]',
    6 => '[notification:text]',
    7 => '[notification:text]',
    8 => 'Review was added to request #[node:nid]. User name is  [user:field_user_first_name] [user:field_user_last_name]. [notification:text]',
    9 => '[notification:text]',
    10 => '[notification:text]',
    11 => '[notification:text]',
    12 => '[notification:text]',
    13 => 'Request #[node:nid] was created for [user:mail]',
    14 => '[notification:text]',
    15 => 'Request #[node:nid] status was changed to [node:field_approve]',
    16 => 'Request #[node:nid] is completed.',
    17 => '[notification:text]',
    18 => '[notification:text]',
    19 => '[notification:text]',
    20 => '[notification:text]',
    21 => '[notification:text]',
    22 => '[notification:text]',
    23 => '[notification:text]',
    24 => '[notification:text]',
    25 => '[notification:text]',
    26 => '[notification:text]',
    27 => '[notification:text]',
    28 => '[notification:text]',
    29 => '[notification:text]',
    30 => '[notification:text]',
    31 => '[notification:text]',
  );

  $notification_types = notification_types();
  foreach ($notification_types as $id => $val) {
    db_merge('move_notification_types')
      ->key(array('ntid' => $id))
      ->fields(array('ntid' => $id, 'title' => $val['title'], 'type' => $val['type']))
      ->execute();

    db_merge('move_notification_type_templates')
      ->key(array('ntid' => $id))
      ->fields(array('ntid' => $id, 'text' => $notification_templates[$id]))
      ->execute();
  }


  $template_path = dirname(__FILE__) . '/data/template.html';
  $html_content = file_get_contents($template_path);

  $block_path = dirname(__FILE__) . '/data/blocks.json';
  $block_content = file_get_contents($block_path);

  $data_path = dirname(__FILE__) . '/data/data.json';
  $data_content = file_get_contents($data_path);

  db_insert('move_template_builder')
    ->fields(array(
      'template_type' => 0,
      'template' => $html_content,
      'name' => 'Notification mail',
      'blocks' => $block_content,
      'data' => $data_content,
      'key_name' => 'notification_send',
    ))
    ->execute();
}

/**
 * Notification type for status table.
 */
function move_notification_update_7101() {
  $schema = move_notification_schema();
  $table = 'move_notification_status';
  $fields = array(
    'ntid',
  );
  if (db_table_exists($table)) {
    foreach ($fields as $field) {
      if (!db_field_exists($table, $field)) {
        db_add_field($table, $field, $schema[$table]['fields'][$field]);
      }
    }
  }
}

/**
 * Change notification type CUSTOMER_LOGIN to CUSTOMER_VIEW_REQUEST, FORMENT_LOGIN_ACCOUNT to FORMENT_VIEW_CONTACT.
 */
function move_notification_update_7102() {
  $notification_types = array(
    3 => array('title' => 'When a customer views a request'),
    9 => array('title' => 'A foreman views the contract'),
  );

  $notification_templates = array(
    3 => '[notification:text]',
    9 => '[notification:text]',
  );

  foreach ($notification_types as $id => $val) {
    db_merge('move_notification_types')
      ->key(array('ntid' => $id))
      ->fields(array('title' => $val['title']))
      ->execute();

    db_merge('move_notification_type_templates')
      ->key(array('ntid' => $id))
      ->fields(array('text' => $notification_templates[$id]))
      ->execute();
  }
}

/**
 * Add new notification type: Site settings.
 */
function move_notification_update_7103() {
  $notification_types = array(
    30 => array('title' => 'Site settings'),
  );

  $notification_templates = array(
    30 => '[notification:text]',
  );

  foreach ($notification_types as $id => $val) {
    db_merge('move_notification_types')
      ->key(array('ntid' => $id))
      ->fields(array('title' => $val['title']))
      ->execute();

    db_merge('move_notification_type_templates')
      ->key(array('ntid' => $id))
      ->fields(array('text' => $notification_templates[$id]))
      ->execute();
  }
}

/**
 * Add new notification type: When customer book the job.
 */
function move_notification_update_7104() {
  $notification_types = array(
    31 => array('title' => 'When a customer books the job'),
  );

  $notification_templates = array(
    31 => '[notification:text]',
  );

  foreach ($notification_types as $id => $val) {
    db_merge('move_notification_types')
      ->key(array('ntid' => $id))
      ->fields(array('title' => $val['title']))
      ->execute();

    db_merge('move_notification_type_templates')
      ->key(array('ntid' => $id))
      ->fields(array('text' => $notification_templates[$id]))
      ->execute();
  }
}

/**
 * Update notification titles.
 */
function move_notification_update_7105() {
  $notification_types = notification_types();
  if (!empty($notification_types)) {
    foreach ($notification_types as $ntid => $item) {
      db_update('move_notification_types')
        ->fields(array('title' => $item['title']))
        ->condition('ntid', $ntid)
        ->execute();
    }
  }
}

/**
 * Add new notification type: When a customer updated valuation.
 */
function move_notification_update_7106() {
  $notification_types = array(
    32 => array('title' => 'When a customer updated valuation'),
  );

  $notification_templates = array(
    32 => '[notification:text]',
  );

  foreach ($notification_types as $id => $val) {
    db_merge('move_notification_types')
      ->key(array('ntid' => $id))
      ->fields(array('title' => $val['title']))
      ->execute();

    db_merge('move_notification_type_templates')
      ->key(array('ntid' => $id))
      ->fields(array('text' => $notification_templates[$id]))
      ->execute();
  }
}

/**
 * Update indexes move_notification table.
 */
function move_notification_update_7107() {
  if (db_table_exists('move_notification')) {
    $schema = move_notification_schema();
    db_drop_index('move_notification', 'uid');
    db_add_index('move_notification', 'a_uid', $schema['move_notification']['indexes']['a_uid']);
    db_add_index('move_notification', 'created', $schema['move_notification']['indexes']['created']);
    db_add_index('move_notification', 'entity_id', $schema['move_notification']['indexes']['entity_id']);
    db_add_index('move_notification', 'entity_type', $schema['move_notification']['indexes']['entity_type']);
  }
}

/**
 * Update indexes move_notification_user_permissions table.
 */
function move_notification_update_7108() {
  if (db_table_exists('move_notification_user_permissions')) {
    $schema = move_notification_schema();
    db_add_index('move_notification_user_permissions', 'added', $schema['move_notification_user_permissions']['indexes']['added']);
  }
}

/**
 * Update indexes move_notification_status table.
 */
function move_notification_update_7109() {
  if (db_table_exists('move_notification_status')) {
    $schema = move_notification_schema();
    db_add_index('move_notification_status', 'ntid', $schema['move_notification_status']['indexes']['ntid']);
    db_query("ALTER TABLE move_notification_status ADD id INT PRIMARY KEY AUTO_INCREMENT");
  }
}

/**
 * Update indexes move_notification_users_dont_see_notif table.
 */
function move_notification_update_7110() {
  if (db_table_exists('move_notification_users_dont_see_notif')) {
    db_query("ALTER TABLE move_notification_users_dont_see_notif ADD id INT PRIMARY KEY AUTO_INCREMENT");
  }
}

/**
 * Update indexes move_notification_templates table.
 */
function move_notification_update_7111() {
  if (db_table_exists('move_notification_templates')) {
    db_add_primary_key('move_notification_templates', array('tid'));
    db_drop_index('move_notification_templates', 'tid');
  }
}

/**
 * Update indexes move_notification_types table.
 */
function move_notification_update_7112() {
  if (db_table_exists('move_notification_types')) {
    db_add_primary_key('move_notification_types', array('ntid'));
    db_drop_index('move_notification_types', 'ntid');
  }
}

/**
 * Update indexes move_notification_user_mail_settings table.
 */
function move_notification_update_7113() {
  if (db_table_exists('move_notification_user_mail_settings')) {
    db_query("ALTER TABLE move_notification_user_mail_settings ADD id INT PRIMARY KEY AUTO_INCREMENT");
  }
}

/**
 * Update indexes move_notification_type_mail_settings table.
 */
function move_notification_update_7114() {
  if (db_table_exists('move_notification_type_mail_settings')) {
    db_query("ALTER TABLE move_notification_type_mail_settings ADD id INT PRIMARY KEY AUTO_INCREMENT");
  }
}

/**
 * Update indexes move_notification_user_mail_last_send table.
 */
function move_notification_update_7115() {
  if (db_table_exists('move_notification_user_mail_last_send')) {
    db_query("ALTER TABLE move_notification_user_mail_last_send ADD id INT PRIMARY KEY AUTO_INCREMENT");
  }
}

/**
 * Update indexes move_notification_user_allowed_by_id table.
 */
function move_notification_update_7116() {
  if (db_table_exists('move_notification_user_allowed_by_id')) {
    db_query("ALTER TABLE move_notification_user_allowed_by_id ADD id INT PRIMARY KEY AUTO_INCREMENT");
  }
}

/**
 * Add notification for sms.
 */
function move_notification_update_7117() {
  $notification_types = array(
    33 => array('title' => 'Sms low balance'),
  );

  $notification_templates = array(
    33 => 'Your SMS Balance is low, please recharge your balance.',
  );

  foreach ($notification_types as $id => $val) {
    db_merge('move_notification_types')
      ->key(array('ntid' => $id))
      ->fields(array('title' => $val['title']))
      ->execute();

    db_merge('move_notification_type_templates')
      ->key(array('ntid' => $id))
      ->fields(array('text' => $notification_templates[$id]))
      ->execute();
  }
}
