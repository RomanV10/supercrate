<?php

namespace Drupal\move_admin_logs\Services;

use Drupal\move_services_new\Services\BaseService;

class AdminLogs extends BaseService {
  public function __construct($id = NULL) {}

  public function create($data = array()) {
    $token = variable_get('elromco_token', '');
    $last_access = variable_get('elromco_token_access', '');
    if (!$token || !$last_access || $last_access <= time() - 3600) {
      $token = $this->authorize();
    }
    else {
      variable_set('elromco_token_access', time());
    }

    $send = $this->sendLog($data, $token);
    if ($send['status'] == 403) {
      $token = $this->authorize();
      $send = $this->sendLog($data, $token);
    }

    return $send;
  }

  private function authorize() {
    $token = '';

    try {
      $login = array(
        'name' => 'stat',
        'pass' => '9HdZdZXLMp7s8xEX',
      );
      $login_string = json_encode($login);
      $ch = curl_init('http://api.stat.moversboard.net/server/user/login');
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $login_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($login_string),
      ));
      $result = curl_exec($ch);
      $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);
      $parse_result = json_decode($result);
      if ($status == 200 && is_object($parse_result) && property_exists($parse_result, 'token')) {
        $token = $parse_result->token;
        variable_set('elromco_token', $token);
        variable_set('elromco_token_access', time());
      }
      else {
        throw new \Exception(curl_error($ch), curl_errno($ch));
      }
    }
    catch (\Throwable $e) {
      watchdog('AdminLogs', "Error in authentication: Code: {$e->getCode()} Message: {$e->getMessage()}");
    }

    return $token;
  }

  private function sendLog($data_arg = array(), $token) {
    $type = $data_arg['type'];
    $data = $data_arg['data'];
    $stat_id = variable_get('elromco_stat_id', '');
    $data_string = json_encode(array(
      'site_id' => $stat_id,
      'data' => $data,
      'log_type' => $type,
    ));
    $ch = curl_init('http://api.stat.moversboard.net/server/logs');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data_string),
      'X-CSRF-Token: ' . $token,
      'Origin: ' . variable_get('elromco_site_name', ''),
    ));
    $result = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    watchdog("stat.moversboard.net/server/log", $result);
    curl_close($ch);

    return array('result' => json_decode($result), 'status' => $status);
  }

  public function retrieve() {}
  public function update($data = array()) {}
  public function delete() {}

  public static function writeId($id, $site_name) {
    variable_set('elromco_stat_id', $id);
    variable_set('elromco_site_name', $site_name);
  }

}
