<?php

namespace Drupal\move_admin_logs\System;

use Drupal\move_admin_logs\Services\AdminLogs;

class Service {
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'admin_logs' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_admin_logs\System\Service::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_admin_logs',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'log_type',
                'type' => 'array',
                'source' => array('data' => 'log_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'write_site_id' => array(
            'callback' => 'Drupal\move_admin_logs\System\Service::siteId',
            'file' => array(
              'type' => 'php',
              'module' => 'move_admin_logs',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'site_name',
                'type' => 'string',
                'source' => array('data' => 'site_name'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  public static function create($log_type, $data = array()) {
    $al_instance = new AdminLogs();
    $data2 = array(
      'type' => $log_type,
      'data' => $data,
    );

    return $al_instance->create($data2);
  }

  public static function siteId($id, $site_name) {
    return AdminLogs::writeId($id, $site_name);
  }

}
