<?php

namespace Drupal\move_template_builder\System;

use Drupal\move_template_builder\Services\EmailTemplate;

/**
 * Class Service.
 *
 * @package Drupal\move_template_builder\System
 */
class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3003,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'template_builder' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'template_type',
                'type' => 'int',
                'source' => array('data' => 'template_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'string',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'blocks',
                'type' => 'array',
                'source' => array('data' => 'blocks'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'key_name',
                'type' => 'text',
                'source' => array('data' => 'key_name'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'template_type',
                'type' => 'int',
                'source' => array('data' => 'template_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'string',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'blocks',
                'type' => 'array',
                'source' => array('data' => 'blocks'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'key_name',
                'type' => 'text',
                'source' => array('data' => 'key_name'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'create_block_base' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::createBlockBase',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'template_type',
                'type' => 'int',
                'source' => array('data' => 'template_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'block_type',
                'type' => 'int',
                'source' => array('data' => 'block_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'string',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve_blocks_base' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::retrieveBlocksBase',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update_blocks_base' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::updateBlockBase',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'template_type',
                'type' => 'int',
                'source' => array('data' => 'template_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'block_type',
                'type' => 'int',
                'source' => array('data' => 'block_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'string',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'key_name',
                'type' => 'text',
                'source' => array('data' => 'key_name'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete_base_block' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::deleteBlocksBase',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'remove_templates' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::removeTemplates',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'send_email_template' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::sendEmailTemplate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'send_email_template_to' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::sendEmailTemplateTo',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'send_email_template_invoice' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::sendEmailTemplateInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_template_preview' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::templatePreview',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'request_id',
                'type' => 'int',
                'source' => array('data' => 'request_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'request_type',
                'type' => 'int',
                'source' => array('data' => 'request_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'template',
                'type' => 'array',
                'source' => array('data' => 'template'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('template preview'),
          ),
        ),
      ),
      'get_mail_image_hash' => array(
        'operations' => array(
          'retrieve' => array(
            'callback' => 'Drupal\move_template_builder\System\Service::retrieveHash',
            'file' => array(
              'type' => 'php',
              'module' => 'move_template_builder',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'hash',
                'type' => 'string',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function create($temp_type, $value, $name, $blocks = array(), $data = array(), $key_name) {
    $et_instance = new EmailTemplate();
    $data_array = array(
      'name' => $name,
      'template_type' => $temp_type,
      'template' => $value,
      'blocks' => json_encode($blocks),
      'data' => json_encode($data),
      'key_name' => $key_name,
    );

    return $et_instance->create($data_array);
  }

  public static function retrieve($id) {
    $et_instance = new EmailTemplate($id);
    return $et_instance->retrieve();
  }

  public static function update($id, $temp_type, $value, $name, $blocks = array(), $data = array(), $key_name) {
    $et_instance = new EmailTemplate($id);
    $data_array = array(
      'id' => $id,
      'name' => $name,
      'template_type' => $temp_type,
      'template' => $value,
      'blocks' => json_encode($blocks),
      'data' => json_encode($data),
      'key_name' => $key_name,
    );

    return $et_instance->update($data_array);
  }

  public static function delete($id) {
    $et_instance = new EmailTemplate($id);
    return $et_instance->delete();
  }

  public static function index() {
    $et_instance = new EmailTemplate();
    return $et_instance->index();
  }

  public static function createBlockBase($temp_type, $block_type, $template, $data = array()) {
    $et_instance = new EmailTemplate();
    return $et_instance->createBaseBlock($temp_type, $block_type, $template, $data);
  }

  public static function deleteBlocksBase($id) {
    $et_instance = new EmailTemplate($id);
    return $et_instance->removeBaseBlock();
  }

  public static function updateBlockBase($id, $temp_type, $block_type, $template, $data = array()) {
    $et_instance = new EmailTemplate($id);
    return $et_instance->updateBaseBlock($temp_type, $block_type, $template, $data);
  }

  public static function retrieveBlocksBase() {
    return EmailTemplate::retrieveBaseBlocks();
  }

  public static function sendEmailTemplate($nid, $data = array()) {
    return EmailTemplate::sendEmailTemplate($nid, $data);
  }

  public static function sendEmailTemplateTo($nid, $data = array()) {
    return EmailTemplate::sendEmailTemplateTo($nid, $data);
  }

  public static function sendEmailTemplateInvoice($id, $data = array()) {
    return EmailTemplate::sendEmailTemplateInvoice($id, $data);
  }

  public static function removeTemplates() {
    return EmailTemplate::removeTemplates();
  }

  /**
   * Get template preview.
   *
   * @param int $id
   *   Request id.
   * @param int $req_type
   *   Request type.
   * @param mixed $template
   *   Template data.
   *
   * @return mixed
   *   HTML template.
   */
  public static function templatePreview(int $id, int $req_type, $template = array()) {
    return (new EmailTemplate())->previewTemplate($id, $req_type, $template);
  }

  /**
   * Update mail visit by image hash and return image src.
   *
   * @param string $hash
   *   Image hash.
   */
  public static function retrieveHash(string $hash) {
    try {
      // Here we creating image with size 1px
      // And set header Content-type - image.
      // We need this to show image in template.
      // Otherwise there is will no image icon.
      $image = imagecreatetruecolor(1, 1);
      imagefill($image, 0, 0, 0xFFFFFF);
      header('Pragma: public');
      header('Expires: 0');
      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Cache-Control: private', FALSE);
      header('Content-Transfer-Encoding: binary');
      header('Content-type: image/png');
      imagepng($image);
      imagedestroy($image);
      (new EmailTemplate())->updateMaiVisitByHash($hash);
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("Update email view", $error_text, array(), WATCHDOG_ERROR);
    }
  }

}
