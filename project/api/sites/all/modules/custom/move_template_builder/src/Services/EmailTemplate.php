<?php

namespace Drupal\move_template_builder\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\TemplateType;
use Drupal\move_services_new\Util\enum\TemplateBlockType;
use Drupal\move_services_new\Util\enum\NotificationTypes;
use Drupal\move_invoice\Services\Invoice;
use Drupal\move_storage\Services\RequestStorage;
use Drupal\move_notification\Services\Notification;

/**
 * Class EmailTemplate.
 *
 * @package Drupal\move_template_builder\Services
 */
class EmailTemplate extends BaseService {

  private const VIEWED = 1;
  private const NOT_VIEWED = 0;

  private $id = NULL;

  public function __construct($id = NULL) {
    $this->id = $id;
  }

  public function create($data = array()) {
    $result = FALSE;
    $t_type = $this->checkTemplateType($data['template_type']);

    if ($t_type !== FALSE) {
      if (!isset($this->id)) {
        $result = db_insert('move_template_builder')
          ->fields($data)
          ->execute();
      }
      else {
        $result = db_merge('move_template_builder')
          ->key(array('id' => $this->id))
          ->fields($data)
          ->execute();
      }
    }

    return $result;
  }

  public function retrieve() {
    $result = FALSE;
    $data = db_select('move_template_builder', 'mtb')
      ->fields('mtb')
      ->condition('mtb.id', $this->id)
      ->execute()
      ->fetchAllAssoc('id');

    if ($data) {
      $result = (array) reset($data);
      $result['blocks'] = json_decode($result['blocks']);
      $result['data'] = json_decode($result['data']);
    }

    return $result;
  }

  public function update($data = array()) {
    return $this->create($data);
  }

  public function delete() {
    return db_delete('move_template_builder')->condition('id', $this->id)->execute();
  }

  public function index() {
    $result = array();
    $templates = db_select('move_template_builder', 'mtb')
      ->fields('mtb')
      ->execute()
      ->fetchAllAssoc('id');

    foreach ($templates as $id => $template) {
      $result[$id] = (array) $template;
      $result[$id]['blocks'] = json_decode($template->blocks);
      $result[$id]['data'] = json_decode($template->data);
    }

    return $result;
  }

  /**
   * Get template by key_name.
   *
   * @param string $key_name
   *   Key name.
   *
   * @return array
   *   Template data.
   */
  public static function getTemplateByKeyName(string $key_name) : array {
    $result = [];

    $templates = db_select('move_template_builder', 'mtb')
      ->fields('mtb', ['id', 'key_name', 'template', 'data'])
      ->condition('key_name', $key_name)
      ->execute()
      ->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

    foreach ($templates as $id => $template) {
      $result[$id] = $template;
      $result[$id]['data'] = json_decode($template['data'], TRUE);
    }

    return $result;
  }

  public static function sendEmailTemplate($nid, $sendingTemplates) {
    $template_builder = new TemplateBuilder();
    $node = node_load($nid);
    $custom = $template_builder->moveRequestGetVariablesArray($node);
    // Init Variables for Templates from $NODE.
    $wrapper = entity_metadata_wrapper('node', $node);
    $to = $wrapper->field_e_mail->value();

    return $template_builder->sendEmailTemplates($node->nid, $sendingTemplates, $custom, $to, EntityTypes::MOVEREQUEST);
  }

  public static function sendEmailTemplateTo(int $nid, array $data) {
    $template_build = new TemplateBuilder();
    $node = node_load($nid);
    $sendingTemplates = $data['templates'];
    $to = $data['to'];

    if (empty($to)) {
      $result = FALSE;
    }
    else {
      $result = $template_build->moveTemplateBuilderSendTemplatesTo($node, $sendingTemplates, $to, EntityTypes::MOVEREQUEST);
    }

    return $result;
  }

  public static function sendEmailTemplateInvoice($id_invoice, $data) {
    $template_build = new TemplateBuilder();
    $result = $template_build->moveTemplateBuilderSendTemplatesInvoice($data, $id_invoice);

    return $result;
  }

  /**
   * Create new Base block.
   *
   * @param int $temp_type
   *   The type of template.
   * @param int $block_type
   *   The type of block.
   * @param string $template
   *   Itself template.
   * @param array $data
   *   Additional data for template.
   *
   * @return bool|int
   *   Id of new Base Block.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function createBaseBlock($temp_type = TemplateType::COMMON, $block_type = TemplateBlockType::HEADER, $template = '', $data = array()) {
    $result = FALSE;
    if ($this->checkTypes($temp_type, $block_type)) {
      $result = db_insert('move_template_builder_base')
        ->fields(array(
          'id' => $this->id,
          'template_type' => (int) $temp_type,
          'block_type' => (int) $block_type,
          'template' => $template,
          'data' => json_encode($data),
        ))
        ->execute();
    }

    return $result;
  }

  public function removeBaseBlock() {
    return db_delete('move_template_builder_base')
      ->condition('id', $this->id)
      ->execute();
  }

  private function checkTypes($temp_type, $block_type) {
    $t_type = $this->checkTemplateType($temp_type);
    $b_type = $this->checkBlockType($block_type);
    return ($t_type !== FALSE && $b_type !== FALSE) ? TRUE : FALSE;
  }

  public function updateBaseBlock($temp_type = TemplateType::COMMON, $block_type = TemplateBlockType::HEADER, $template = '', $data = array()) {
    $result = FALSE;
    if ($this->checkTypes($temp_type, $block_type)) {
      $block = array(
        'id' => $this->id,
        'template_type' => (int) $temp_type,
        'block_type' => (int) $block_type,
        'template' => $template,
        'data' => json_encode($data),
      );

      $result = db_merge('move_template_builder_base')
        ->key(array('id' => $this->id))
        ->fields($block)
        ->execute();
    }

    return $result;
  }

  public static function retrieveBaseBlocks() {
    $result = FALSE;

    $blocks = db_select('move_template_builder_base', 'mtb')
      ->fields('mtb')
      ->execute()
      ->fetchAllAssoc('id');

    foreach ($blocks as $id => $block) {
      $result[$id] = (array) $block;
      $result[$id]['data'] = json_decode($block->data);
    }

    return $result;
  }

  private function checkTemplateType(int $type) {
    $check = FALSE;

    try {
      $check = new TemplateType($type);
    }
    catch (\Throwable $uve) {
      watchdog("EmailTemplate", "Check template type exception: {$uve->getMessage()}", array(), WATCHDOG_WARNING);
    }

    return $check;
  }

  private function checkBlockType(int $type) {
    $check = FALSE;

    try {
      $check = new TemplateBlockType($type);
    }
    catch (\Throwable $uve) {
      watchdog("EmailTemplate", "Check block type exception: {$uve->getMessage()}", array(), WATCHDOG_WARNING);
    }

    return $check;
  }

  public static function removeTemplates() {
    $result = FALSE;
    try {
      db_truncate('move_template_builder')->execute();
      $result = TRUE;
    }
    catch (\Throwable $e) {
      watchdog("EmailTemplate", "Appear error when trying remove all templates: {$e->getMessage()}", array(), WATCHDOG_ERROR);
    }
    finally {
      return $result;
    }
  }

  /**
   * Get html body template.
   *
   * @param int $id
   *   Entity id.
   * @param int $request_type
   *   Request type.
   * @param mixed $template
   *   Template.
   *
   * @return mixed
   *   HTML.
   */
  public function previewTemplate(int $id, int $request_type, $template = array()) {
    $custom = array();
    $template_builder = new TemplateBuilder();

    // Move Request.
    $move_request = function ($id) use ($template_builder) {
      $node = node_load($id);
      return $template_builder->moveRequestGetVariablesArray($node);
    };

    // Storage Request.
    $storage_request = function ($id) use ($template_builder) {
      $request_storage = (new RequestStorage($id))->retrieve();
      return $template_builder->storageRequestGetVariablesArray($request_storage);
    };

    if ($request_type == EntityTypes::MOVEREQUEST) {
      $custom = $move_request($id);
    }
    elseif ($request_type == EntityTypes::STORAGEREQUEST) {
      $custom = $storage_request($id);
    }
    else {
      // Loading invoice.
      $invoice = (new Invoice($id))->retrieve();
      if ($invoice) {
        $entity_type = $invoice['entity_type'];
        $entity_id = $invoice['entity_id'];

        // Move Request.
        if ($entity_type == EntityTypes::MOVEREQUEST) {
          $custom = $move_request($entity_id);
        }
        elseif ($entity_type == EntityTypes::STORAGEREQUEST) {
          $custom = $storage_request($entity_id);
        }
        elseif ($entity_type == EntityTypes::LDREQUEST) {
          // Long Distance.
          // TODO Soon. After create functional for LD.
        }
        $custom = array_merge($custom, $template_builder->moveTemplateBuilderGetVariablesArrayForInvoice($invoice));
      }
    }

    return token_replace($template['template'], $custom);
  }

  /**
   * Build imageHash from email entity_id and time. Hash will be unique.
   *
   * @param int $nid
   *   Nid.
   *
   * @return string
   *   Hash.
   */
  public function buildImageHash(int $nid) {
    return md5($nid . time());
  }

  /**
   * Build image html from hash.
   *
   * @param string $mail_hash
   *   Mail hash.
   *
   * @return string
   *   Html.
   */
  public function buildImageOutput(string $mail_hash) {
    $image_hash_src = url('server/get_mail_image_hash/' . $mail_hash, ['absolute' => TRUE]);
    $image_for_mail = '<img src="' . $image_hash_src . '">';

    return $image_for_mail;
  }

  /**
   * Updating email visit by it's hash.
   *
   * If visit is already exist we no need to do nothing.
   *
   * @param string $hash
   *   Image hash.
   */
  public function updateMaiVisitByHash(string $hash) {
    $query = db_select('movesales_log_mail', 'mlm');
    $query->leftJoin('move_logs', 'ml', 'mlm.msl_id = ml.id');
    $query->fields('mlm', ['image_mail_hash', 'id', 'subject']);
    $query->fields('ml', ['entity_id', 'entity_type', 'details']);
    $query->condition('image_mail_hash', $hash, '=');
    $query->condition('visit', self::NOT_VIEWED, '=');
    $result = $query->execute()->fetchAssoc(\PDO::FETCH_ASSOC);

    if (!empty($result)) {
      $add_visit = FALSE;
      $request_author_id = NULL;
      global $user;
      if ($result['entity_type'] == EntityTypes::MOVEREQUEST) {
        $request_author_id = MoveRequest::getRequestUserId($result['entity_id']);
      }

      // Id use is anonymous - we can add add visit.
      if (user_is_anonymous()) {
        $add_visit = TRUE;
      }
      else {
        // Entity type is request we need to get author
        // and must know is author look this mail.
        if ($request_author_id == $user->uid) {
          $add_visit = TRUE;
        }
      }
      if ($add_visit) {
        db_update('movesales_log_mail')
          ->fields([
            'visit' => self::VIEWED,
            'date_viewed' => time(),
            'ip' => $user->hostname,
          ])
          ->condition('id', $result['id'])
          ->execute();
        // Build notification variables for token.
        $node = new \stdClass();
        $node->nid = $result['entity_id'];
        $node->type = 'move_request';
        $notification_text = array('text' => 'Client read email "' . $result['subject'] . '"');
        $notification_info = array('node' => $node, 'notification' => $notification_text);

        if ($request_author_id) {
          $mail = Clients::getUserFields($request_author_id, ['mail']);
          if (!empty($mail['mail'])) {
            preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", unserialize($result['details'])[0]['text'][0]['text'], $matches);
            if (!in_array($mail['mail'], $matches[0])) {
              Notification::$addScoring = FALSE;
            }
          }
        }
        // Add notification.
        Notification::createNotification(NotificationTypes::CUSTOMER_READ_EMAIL, $notification_info, $result['entity_id'], $result['entity_type']);
      }
    }
  }

}
