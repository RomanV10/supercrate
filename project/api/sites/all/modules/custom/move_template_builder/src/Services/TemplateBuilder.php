<?php

namespace Drupal\move_template_builder\Services;

use Drupal\move_calculator\Services\MoveRequestExtraService;
use Drupal\move_inventory\Actions\RoomActions;
use Drupal\move_long_distance\Services\Actions\SitInvoiceActions;
use Drupal\move_mpdf\Services\Mpdf;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\Comments;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\Front;
use Drupal\move_invoice\Services\Invoice;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\InvoiceFlags;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;
use Drupal\move_services_new\Util\enum\ValuationTypes;
use Drupal\move_storage\Services\RequestStorage;
use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Services\Users;
use Drupal\move_services_new\Services\Inventory;
use Drupal\move_inventory\Actions\ItemActions;

/**
 * Class TemplateBuilder.
 *
 * @package Drupal\move_template_builder\Services
 */
class TemplateBuilder {

  /**
   * @var array
   */
  private $basic_settings;

  /**
   * @var array
   */
  private $calculate_settings;

  public function __construct() {
    $this->basic_settings = json_decode(variable_get('basicsettings', array()));
    $this->calculate_settings = json_decode(variable_get('calcsettings', array()));
  }

  public function moveRequestGetVariablesArray(\stdClass $node) : array {
    $custom = array();
    $wrapper = entity_metadata_wrapper('node', $node);
    $nid = $node->nid;
    $first_name = $wrapper->field_first_name->value();
    $last_name = $wrapper->field_last_name->value();
    $phone = $wrapper->field_phone->value();
    $mail = $wrapper->field_e_mail->value();
    $crew = $wrapper->field_movers_count->value();
    $foreman_uid = $wrapper->field_foreman->raw();
    $home_estimate_date = !empty($wrapper->field_home_estimate_date->value()) ? date('F j, Y', $wrapper->field_home_estimate_date->value()) : '';
    $inhome_estimate_arrival_time_window = $wrapper->field_home_estimate_actual_start->value() . ' - ' . $wrapper->field_home_estimate_start_time->value();

    $req = $this->getRequestArray($node);
    $start_time = $req['start_time1'] . ' - ' . $req['start_time2'];
    $move_date = date('F j, Y', strtotime($req['date']));
    $rate = $req['rate'];
    $adr_from = $req['adrfrom'] . ',' . $req['from'];
    $adr_to = $req['adrto'] . ',' . $req['to'];
    $pickup = $req['pickup_adrto'] . ',' . $req['pickup_data'];
    $dropoff = $req['dropoff_adrto'] . ',' . $req['dropoff_data'];
    $pickup_apt = $req['pickup_premise'];
    $dropoff_apt = $req['dropoff_premise'];
    $pickup_entrance = $req['pickup_entarence'];
    $dropoff_entrance = $req['dropoff_entarence'];
    $apt_from = $req['apt_from'];
    $apt_to = $req['apt_to'];
    $type_to = $req['type_to'];
    $type_from = $req['type_from'];
    $move_size = $req['move_size'];
    $rooms_text = $req['room_text'];
    $est_time = $req['est_time'];
    $est_qoute = $req['est_qoute'];
    $travel_time = $req['tt'];
    $current_truck_number = $req['current_truck_number'];
    $company_name = $this->basic_settings->company_name;
    $site_url = $this->basic_settings->website_url;
    $client_url = !empty($this->basic_settings->client_page_url) ? $this->basic_settings->client_page_url : '';
    $company_phone = $this->basic_settings->company_phone;
    $company_email = $this->basic_settings->company_email;
    $flatRateQuote = !empty($wrapper->field_estimated_prise->value()) ? number_format($wrapper->field_estimated_prise->value(), 2) : 0.00;

    if ($mail) {
      $data = (new Clients())->oneTimeHash($mail);
      if ($data) {
        $link_reservation = $client_url . "/account/#/request/{$node->nid}/reservation?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
        $link = $client_url . "/account/#/request/{$node->nid}?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
        $link_message = $client_url . "/account/#/request/{$node->nid}/chat?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
        $link_receipt = $client_url . "/account/#/request/{$node->nid}/receipt?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
        $link_print_contract = $client_url . "/account/#/request/{$node->nid}/contract_print?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
        $review_url = $client_url . "/account/#/request/{$node->nid}/review?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
        $custom['proposal_link_main_page'] = $client_url . "account/#/request/{$node->nid}/contract_main_page_read?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
        $custom['proposal_link_additional_pages'] = $client_url . "account/#/request/{$node->nid}/contract_proposal_additional_pages_read?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
        $reviews_count_stars = 5;
        $review_url_stars = '';
        $module_path = drupal_get_path('module', 'move_reviews');
        global $base_url;
        for ($i = 1; $i <= $reviews_count_stars; $i++) {
          $path = $base_url . '/' . $module_path . '/icons/emo_' . $i . '.png';
          $href = $client_url . "/account/#/request/{$node->nid}/mark/{$i}?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
          // TODO refactor later.
          $review_url_stars .= '<a style="padding:0 10px;" href=" ' . $href . ' "><img height="48"  width="48" src="' . $path . '"></a>';
        }
      }
    }
    else {
      $link_reservation = '';
      $link = '';
      $link_message = '';
      $link_receipt = '';
      $link_print_contract = '';
    }

    // Add foreman url to request contract with onetime hash.
    if ($foreman_uid) {
      $foreman = user_load(reset($foreman_uid));
      $foreman_url = static::oneTimeHashForeman($client_url, $nid, $foreman->mail);
    }

    $sales_first_name = "";
    $sales_full_name = "";
    $sales_signature = "";

    $sales_id = MoveRequest::getRequestManager($node->nid);
    if ($sales_id) {
      $sales_settings = Users::getUserSettings($sales_id);
      // Remove this for check for "signature".
      // $is_outgoing_active = isset($sales_settings['email_account']['outgoing']['active']) && $sales_settings['email_account']['outgoing']['active'];

      if (isset($sales_settings['signature'])) {
        $sales_first_name = isset($sales_settings['signature']['firstName']) ? $sales_settings['signature']['firstName'] : "";
        $sales_full_name = isset($sales_settings['signature']['fullName']) ? $sales_settings['signature']['fullName'] : "";
        $sales_signature = isset($sales_settings['signature']['textSignature']) ? $sales_settings['signature']['textSignature'] : "";
      }
    }

    $user_password = variable_get($mail);
    if (isset($user_password)) {
      variable_del($mail);
    }
    $user = user_load_by_mail($mail);
    if (isset($user)) {
      $created_user_time = date("F j, Y, g:i a", $user->created);
    }
    $send_message_time = "";
    if (isset($node->comment_count) && ($node->comment_count > 0)) {
      $comment_time_zone = Comments::getCommentTimeZone($node->cid) ?: Extra::getTimezone();
      $send_message_time = Extra::date("F j, Y, g:i a", $node->last_comment_timestamp, $comment_time_zone) . '. Time zone is "' . $comment_time_zone . '".';
    }

    $custom['foreman-url'] = isset($foreman_url) ? $foreman_url : '';
    $custom['review-url'] = isset($review_url) ? $review_url : '';
    $custom['review-url-stars'] = isset($review_url_stars) ? $review_url_stars : '';
    $custom['request-id'] = $nid;
    $custom['fname'] = $first_name;
    $custom['lname'] = $last_name;
    $custom['client-email'] = $mail;
    $custom['client-phone'] = $phone;
    $custom['crew-size'] = $crew;
    $custom['start-window'] = $start_time;
    $custom['move-date'] = $move_date;
    $custom['rate'] = $rate;
    $custom['address-from'] = $adr_from;
    $custom['address-to'] = $adr_to;
    $custom['pickup'] = $pickup;
    $custom['dropoff'] = $dropoff;
    $custom['pickup-apt'] = $pickup_apt;
    $custom['dropoff-apt'] = $dropoff_apt;
    $custom['pickup-entrance'] = $pickup_entrance;
    $custom['dropoff-entrance'] = $dropoff_entrance;
    $custom['apartment-from'] = $apt_from;
    $custom['apartment-to'] = $apt_to;
    $custom['entrance-to'] = $type_to;
    $custom['entrance-from'] = $type_from;
    $custom['move-size'] = $move_size;
    $custom['rooms'] = $rooms_text;
    $custom['estimated-time'] = $est_time;
    $custom['estimated-quote'] = $est_qoute;
    $custom['travel-time'] = $travel_time;
    $custom['request-url'] = isset($link) ? $link : "";
    $custom['site-url'] = $site_url;
    $custom['company-name'] = $company_name;
    $custom['company-phone'] = $company_phone;
    $custom['company-email'] = $company_email;
    $custom['user-password'] = $user_password;
    $custom['request-extra-services'] = $req['extra_services'];
    $custom['manager'] = MoveRequest::getRequestManagerInfo((int) $nid);
    $custom['packing'] = $req['packing_total'];
    $custom['discount'] = $req['discount'];
    $custom['valuation'] = $req['valuation'];
    $custom['fuel'] = $req['fuel'];
    $custom['ld-grand-total'] = $req['ld_grand_total'];
    $custom['fr-grand-total'] = $req['fr_grand_total'];
    $custom['labor-time'] = $req['labor_time'];
    $custom['created-user-time'] = isset($created_user_time) ? $created_user_time : "";
    $custom['send-message-time'] = $send_message_time;
    $custom['manager'] = $req['assign_manager'];
    $custom['packing-table'] = $req['packing_table'];
    $custom['additional-services-table'] = $req['additional_services_table'];
    $custom['reservation-link'] = isset($link_reservation) ? $link_reservation : "";
    $custom['sales-first-name'] = $sales_first_name;
    $custom['sales-full-name'] = $sales_full_name;
    $custom['sales-signature'] = $sales_signature;
    $custom['request-url-reply-message'] = isset($link_message) ? $link_message : "";
    $custom['request-url-reply-receipt'] = isset($link_receipt) ? $link_receipt : "";
    $custom['bill_of_landing_receipt_url'] = isset($link_print_contract) ? $link_print_contract : "";
    $custom['inventory-table'] = $req['inventory_table'];
    $custom['inventory-table-without-cf'] = $req['inventory_table_without_cf'];
    $custom['closing-packing'] = $req['closing_packing'];
    $custom['closing-additional-services'] = $req['closing_additional_services'];
    $custom['closing-discount'] = $req['closing_discount'];
    $custom['closing-grand-total'] = "$" . $req['closing_grand_total'];
    $custom['closing-fuel'] = $req['closing_fuel'];
    $custom['closing-cubic-feet'] = $req['closing_cubic_feet'];
    $custom['rate-per-cf-for-ld'] = $req['rate_per_cf_for_ld'];
    $custom['rate-per-lbs-for-ld'] = $req['rate_per_lbs_for_ld'];
    $custom['min-cf'] = $req['min_cf'];
    $custom['service-type'] = $req['service_type'];
    $custom['distance'] = $req['distance'] . " miles";
    $custom['created'] = $req['created'];
    $custom['avl-delv-date'] = $req['avl_delv_date'];
    $custom['closing-payment'] = $req['closing_payment'];
    $custom['pickup-date-range'] = $req['pick_up_date_range'];
    $custom['closing-fuel-percent'] = $req['closing_fuel_percent'];
    $custom['foreman-note'] = $req['foreman_note'];
    $custom['client-note'] = $req['client_note'];
    $custom['current-truck-number'] = $current_truck_number;
    $custom['inhome_estimate_date'] = $home_estimate_date;
    $custom['inhome_estimate_arrival_time_window'] = $inhome_estimate_arrival_time_window;
    $custom['inhome_estimator'] = $req['home_estimator'];
    $custom['tax_name'] = variable_get('taxName', 'Tax');
    $custom['valuation-table'] = $req['valuation_table'];
    $custom['flat-rate-quote'] = '$' . $flatRateQuote;

    return $custom;
  }

  public static function oneTimeHashForeman($client_url, $nid, $mail) {
    $hash_data = (new Clients())->oneTimeHash($mail);
    $foreman_url = $client_url . "/account/#/request/{$nid}/contract?uid={$hash_data['uid']}&timestamp={$hash_data['timestamp']}&hash={$hash_data['hash']}";

    return $foreman_url;
  }

  public function moveTemplateBuilderSendTemplatesTo(\stdClass $node, array $sendingTemplates, $to, $entity_type) {
    // Init Variables for Templates from $NODE.
    $custom = $this->moveRequestGetVariablesArray($node);

    if (is_array($to) && !empty($to)) {
      $result = $this->sendEmailTemplatesManyUsers($node->nid, $sendingTemplates, $custom, $to, $entity_type, FALSE, FALSE);
    }
    else {
      $result = $this->sendEmailTemplates($node->nid, $sendingTemplates, $custom, $to, $entity_type, FALSE, FALSE);
    }

    return $result;
  }

  function moveTemplateBuilderSendTemplatesInvoice(array $sendingTemplates, int $id_invoice) {
    $result = FALSE;

    // TODO. Fix for storage request page - not invoice page.
    if (isset($sendingTemplates['storage_id']) && $sendingTemplates['storage_id']) {
      // Storage Request.
      $request_storage_instance = new RequestStorage($sendingTemplates['storage_id']);
      $request_storage = $request_storage_instance->retrieve();
      $to = $request_storage['user_info']['email'];
      $custom = $this->storageRequestGetVariablesArray($request_storage);

      $result = $this->sendEmailTemplates($sendingTemplates['storage_id'], $sendingTemplates, $custom, $to, EntityTypes::STORAGEREQUEST);
    }
    else {
      // Loading invoice.
      $invoice_instance = new Invoice($id_invoice);
      $invoice = $invoice_instance->retrieve();
      if ($invoice) {
        $entity_type = $invoice['entity_type'];
        $entity_id = $invoice['entity_id'];
        $custom = array();
        $to = '';

        // Move Request.
        if ($entity_type == EntityTypes::MOVEREQUEST) {
          $node = node_load($entity_id);
          $wrapper = entity_metadata_wrapper('node', $node);
          // Init Variables for Templates from $NODE.
          $custom = $this->moveRequestGetVariablesArray($node);
          $to = $invoice['data']['email'] ?? $wrapper->field_e_mail->value();
        }
        elseif ($entity_type == EntityTypes::STORAGEREQUEST) {
          // Storage Request.
          $request_storage_instance = new RequestStorage($entity_id);
          $request_storage = $request_storage_instance->retrieve();
          $to = $request_storage['user_info']['email'];
          $custom = $this->storageRequestGetVariablesArray($request_storage);
        }
        elseif ($entity_type == EntityTypes::LDREQUEST || $entity_type == EntityTypes::LDCARRIER) {
          $custom = $this->longDistanceGetVariablesArray($invoice['data']);
          $to = $custom['client-email'];
        }

        $custom = array_merge($custom, $this->moveTemplateBuilderGetVariablesArrayForInvoice($invoice));
        $result = $this->sendEmailTemplates($entity_id, $sendingTemplates, $custom, $to, $entity_type, TRUE);

        // Change invoice flag if entity type is Long Distance carrier.
        if ($result['send']) {
          SitInvoiceActions::changeInvoiceToStatus($id_invoice, InvoiceFlags::SENT, $entity_type);
        }
      }
    }

    return $result;
  }

  /**
   * Prepare invoice data.
   *
   * @param array $invoice
   *   Invoice data.
   *
   * @return array
   *   Prepared invoice data.
   */
  public function moveTemplateBuilderGetVariablesArrayForInvoice(array $invoice) : array {
    $custom = array();
    $hash_data = (new Clients())->oneTimeHash($invoice['data']['email']);
    $invoice_link = $this->basic_settings->client_page_url . '/account/#/invoice/' . $invoice['hash'];
    if (!empty($hash_data)) {
      $invoice_link .= "?uid={$hash_data['uid']}&timestamp={$hash_data['timestamp']}&hash={$hash_data['hash']}";
    }

    $invoice_id = $invoice['id'];
    $invoice_total = $invoice['data']['totalInvoice'];

    $custom['invoice-link'] = $invoice_link;
    $custom['invoice-id'] = $invoice_id;
    $custom['invoice-total'] = number_format((float) $invoice_total, 2);
    return $custom;
  }

  public function sendEmailTemplates(int $entity_id, array $sendingTemplates, array $custom, string $to, int $entity_type = EntityTypes::MOVEREQUEST, $is_invoce = FALSE, $use_additional = TRUE) {
    $result = FALSE;
    // Check when status request == Archive don`t send mail.
    if ($entity_type == EntityTypes::MOVEREQUEST) {
      $node = node_load($entity_id);
      $emv_node = entity_metadata_wrapper('node', $node);
      if ($emv_node->field_approve->raw() == 22) {
        return;
      }
    }

    if (isset($sendingTemplates['templates']) && is_array($sendingTemplates['templates'])) {
      $sendingTemplates = $sendingTemplates['templates'];
    }

    foreach ($sendingTemplates as $template) {
      // We will overide this for admin. That why we need new variable.
      $to_send = $to;
      if (isset($template['template'])) {
        $template_body = $template['template'];
      }
      else {
        $et_instance = new EmailTemplate($template['id']);
        $template_ret = $et_instance->retrieve();
        $template_body = $template_ret['template'];
      }

      $template_ready = token_replace($template_body, $custom);
      $template_subject = token_replace($template['subject'], $custom);

      $manager_uid = 0;
      $manager_have_email = 0;
      if ($entity_type == EntityTypes::MOVEREQUEST && !$is_invoce) {
        $manager_uid = MoveRequest::getRequestManager($entity_id);
        $clients = new Clients();
        if ($manager_uid) {
          $clients->setUser(user_load($manager_uid));
        }
        else {
          global $user;
          $clients->setUser($user);
        }
        $user_data = $clients->getUserFieldsData($clients->getUid(), FALSE);
        if (!empty($user_data['settings']['email_account']['outgoing']['active'])) {
          $manager_have_email = !empty($user_data['settings']['email_account']['name']);
        }
        else {
          $manager_have_email = FALSE;
        }
      }

      $params = array(
        'body' => $template_ready,
        'subject' => $template_subject,
        'nid' => $entity_id,
        'entity_type' => $entity_type,
        'manager' => $manager_uid,
      );
      $params['notification'] = !empty($custom['notification']) ? TRUE : FALSE;
      if (!empty($template['key_name']) && ($template['key_name'] == 'admin_new_request' || $template['key_name'] == 'admin_confirmed_request')) {
        $to_send = $this->basic_settings->company_email;
      }

      if ($use_additional) {
        $this->checkAdditionalUser($params);
      }

      if ($manager_have_email && $manager_uid) {
        $result = drupal_mail('move_template_builder', 'notify', $to_send, language_default(), $params, $user_data['settings']['email_account']['name']);
      }
      else {
        $result = drupal_mail('move_template_builder', 'notify', $to_send, language_default(), $params);
      }
    }

    return $result;
  }

  /**
   * Method for send email templates for many users.
   *
   * @param int $entity_id
   *   Entity id.
   * @param array $sendingTemplates
   *   Sending Templates.
   * @param array $custom
   *   Custom variable.
   * @param array $to
   *   Users emails.
   * @param int $entity_type
   *   Entity type.
   * @param bool $is_invoce
   *   Invoice template or not.
   *
   * @return array
   *   Result drupal_mail function.
   */
  public function sendEmailTemplatesManyUsers(int $entity_id, array $sendingTemplates, array $custom, array $to, int $entity_type = EntityTypes::MOVEREQUEST, $is_invoce = FALSE, $use_additional = TRUE) {
    $result = FALSE;
    // Check when status request == Archive don`t send mail.
    if ($entity_type != EntityTypes::STORAGEREQUEST) {
      $node = node_load($entity_id);
      $emv_node = entity_metadata_wrapper('node', $node);
      if ($emv_node->field_approve->raw() == 22) {
        return;
      }
    }

    if (isset($sendingTemplates['templates']) && is_array($sendingTemplates['templates'])) {
      $sendingTemplates = $sendingTemplates['templates'];
    }

    foreach ($sendingTemplates as $template) {
      $to_send = $to;
      if (isset($template['template'])) {
        $template_body = $template['template'];
      }
      else {
        $et_instance = new EmailTemplate($template['id']);
        $template_ret = $et_instance->retrieve();
        $template_body = $template_ret['template'];
      }

      $template_ready = token_replace($template_body, $custom);
      $template_subject = token_replace($template['subject'], $custom);

      $manager_uid = 0;
      $manager_have_email = 0;
      if ($entity_type == EntityTypes::MOVEREQUEST && !$is_invoce) {
        $manager_uid = MoveRequest::getRequestManager($entity_id);
        $clients = new Clients();
        if ($manager_uid) {
          $clients->setUser(user_load($manager_uid));
        }
        else {
          global $user;
          $clients->setUser($user);
        }
        $user_data = $clients->getUserFieldsData($clients->getUid(), FALSE);
        if (!empty($user_data['settings']['email_account']['outgoing']['active'])) {
          $manager_have_email = !empty($user_data['settings']['email_account']['name']);
        }
        else {
          $manager_have_email = FALSE;
        }
      }

      // We always using first email as main to send.
      $main_to_send = reset($to_send);
      unset($to_send[0]);

      $from_name = NULL;
      if ($manager_have_email && $manager_uid) {
        $from_name = $user_data['settings']['email_account']['name'];
      }

      $params = array(
        'body' => $template_ready,
        'subject' => $template_subject,
        'nid' => $entity_id,
        'entity_type' => $entity_type,
        'manager' => $manager_uid,
        'cc' => $to_send,
      );

      if ($use_additional) {
        $this->checkAdditionalUser($params);
      }

      if (isset($template['key_name']) && $template['key_name'] == 'send_when_contract_submit') {
        $this->attachFilesCloseJob($params);
      }

      if (isset($template['key_name']) && ($template['key_name'] == 'admin_new_request' || $template['key_name'] == 'admin_confirmed_request')) {
        $main_to_send = $this->basic_settings->company_email;
      }

      if ($main_to_send) {
        $result = drupal_mail('move_template_builder', 'notify', $main_to_send, language_default(), $params, $from_name);
      }
    }

    return (array) $result;
  }

  /**
   * Send template by key name.
   *
   * Getting tokens from node.
   *
   * @param string $key_name
   *   Template key name.
   * @param \stdClass $node
   *   Node.
   *
   * @return mixed
   *   Array with sending result.
   *
   * @throws \Exception
   */
  public function moveTemplateBuilderSendTemplateRules(string $key_name, \stdClass $node) {
    $is_send = array();

    // Check when status request == Archive don`t send mail.
    $wrapper = entity_metadata_wrapper('node', $node);
    if ($wrapper->field_approve->raw() == RequestStatusTypes::ARCHIVE) {
      return;
    }

    $need_templates = EmailTemplate::getTemplateByKeyName($key_name);

    if (!empty($need_templates)) {
      $clientsInstance = new Clients();

      $field_move_service_type = $wrapper->field_move_service_type->value();
      $entity_type = ($field_move_service_type == RequestServiceType::LONG_DISTANCE) ? EntityTypes::LDREQUEST : EntityTypes::MOVEREQUEST;
      $to = $wrapper->field_e_mail->value();
      $manager_uid = MoveRequest::getRequestManager($node->nid);
      $custom = $this->moveRequestGetVariablesArray($node);

      foreach ($need_templates as $template) {
        $to_send = $to;

        $template_body = $template['template'];
        $template_ready = token_replace($template_body, $custom);
        $template_subject = token_replace($template['data']['subject'], $custom);
        $params = array(
          'body' => $template_ready,
          'subject' => $template_subject,
          'nid' => $node->nid,
          'entity_type' => $entity_type,
          'manager' => $manager_uid,
        );

        $this->checkAdditionalUser($params);

        if ($template['key_name'] == 'send_when_contract_submit') {
          $this->attachFilesCloseJob($params);
        }

        if ($key_name == 'admin_new_request' || $key_name == 'admin_confirmed_request') {
          $to_send = $this->basic_settings->company_email;
        }

        if (!empty($manager_uid)) {
          $user_data = $clientsInstance->getUserFieldsData($manager_uid, FALSE);
          if (!empty($user_data['settings']['email_account']['outgoing']['active'])) {
            $manager_have_email = !empty($user_data['settings']['email_account']['name']);
          }
        }

        if (!empty($manager_have_email) && $manager_uid) {
          $is_send[$key_name] = drupal_mail('move_template_builder', 'notify', $to_send, language_default(), $params, $user_data['settings']['email_account']['name']);
        }
        else {
          $is_send[$key_name] = drupal_mail('move_template_builder', 'notify', $to_send, language_default(), $params);
        }
      }
    }

    return $is_send;
  }

  public function moveTemplateBuilderSendTemplateRestorePassword(string $email) : array {
    $result = [];
    $et_instance = new EmailTemplate();
    $all_templates = $et_instance->index();

    $need_templates = array_filter($all_templates, function ($template) {
      return $template['key_name'] == 'restore_password';
    });

    foreach ($need_templates as $template) {
      $et_instance = new EmailTemplate($template['id']);
      $template_ret = $et_instance->retrieve();
      $template_body = $template_ret['template'];
      $custom = array();
      $site_url = $this->basic_settings->client_page_url;
      $data = (new Clients())->oneTimeHash($email);
      $custom['request-url'] = url("$site_url/account/#/login?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}");
      $template_ready = token_replace($template_body, $custom);
      $template_subject = $template['data']->subject;
      $params = array(
        'body' => $template_ready,
        'subject' => $template_subject,
        'entity_type' => EntityTypes::SYSTEM,
      );
      // TODO change logic ($result is overwritten immediately in loop).
      $result = drupal_mail('move_template_builder', 'notify', $email, language_default(), $params);
    }

    return $result;
  }

  public function moveTemplateBuilderSendTemplateNewMessage(\stdClass $comment, \stdClass $node, int $is_manager) : void {
    // Check when status request == Archive don`t send mail.
    $emv_node = entity_metadata_wrapper('node', $node);
    if ($emv_node->field_approve->raw() == 22) {
      return;
    }
    $et_instance = new EmailTemplate();
    $all_templates = $et_instance->index();
    $wrapper = entity_metadata_wrapper('node', $node);
    $manager_uid = MoveRequest::getRequestManager($node->nid);
    $need_templates = array_filter($all_templates, function ($template) {
        return $template['key_name'] == 'new_message';
    });
    $current_user = new Clients($comment->uid);
    $manager = new Clients();
    if ($manager_uid) {
      $manager->setUser(user_load($manager_uid));
    }
    else {
      global $user;
      $manager->setUser($user);
    }
    $user_data = $manager->getUserFieldsData($manager->getUid(), FALSE);
    // Check if active outgoing sms.
    if (!empty($user_data['settings']['email_account']['outgoing']['active'])) {
      $manager_have_email = !empty($user_data['settings']['email_account']['name']);
    }
    else {
      $manager_have_email = FALSE;
    }
    $site_mail = variable_get('site_mail');

    foreach ($need_templates as $template) {
      $custom = $this->moveRequestGetVariablesArray($node);

      // Client was sent new massage (email).
      if (!$is_manager) {
        // Manager email.
        if ($manager_have_email && $user_data['settings']['email_account']['name']) {
          $to = $user_data['settings']['email_account']['name'];
        }
        else {
          // System site mail.
          $to = $site_mail;
        }
        $custom['message-author'] = $current_user->getUser()->field_user_first_name['und'][0]['value'];
        $custom['fname'] = $user_data['field_user_first_name'];
        $custom['lname'] = $user_data['field_user_last_name'];
        $from = $current_user->getUser()->mail;
      }
      else {
        $to = $wrapper->field_e_mail->value();
        $custom['message-author'] = $user_data['field_user_first_name'];
        $from = $is_manager &&  $manager_have_email ? $user_data['settings']['email_account']['name'] : $site_mail;
      }

      $et_instance = new EmailTemplate($template['id']);
      $template_ret = $et_instance->retrieve();
      $template_body = $template_ret['template'];
      $custom['message-text'] = $comment->comment_body['und'][0]['value'];
      $template_ready = token_replace($template_body, $custom);
      $template_subject = token_replace($template['data']->subject, $custom);
      $params = array(
        'body' => $template_ready,
        'subject' => $template_subject,
        'nid' => $node->nid,
        'entity_type' => EntityTypes::MOVEREQUEST,
        'manager' => $manager_uid,
      );

      $this->checkAdditionalUser($params);

      drupal_mail('move_template_builder', 'notify', $to, language_default(), $params, $from);
    }
  }

  private function getRoomText(array $rooms, array $id_rooms_text) : string {
    $extra_room_text = '';
    $room_count = count($rooms);

    for ($i = 1; $i < $room_count; ++$i) {
      $rooms[$i] = $id_rooms_text[$i];
      if ($i == 1 && $room_count == $i) {
        $extra_room_text = '(with ' . $rooms[$i] . ')';
      }
      elseif ($i == 1 && $room_count == 2) {
        $extra_room_text = '(with ' . $rooms[$i] . ' and ';
      }
      elseif ($i == 2 && $room_count == 2) {
        $extra_room_text .= $rooms[$i] . ')';
      }
      elseif ($i == 1 && $room_count > 2) {
        $extra_room_text = '(with ' . $rooms[$i] . ', ';
      }
      elseif ($i == $room_count) {
        $extra_room_text .= $rooms[$i] . ')';
      }
      elseif ($i + 1 == $room_count) {
        $extra_room_text .= $rooms[$i] . ' and ';
      }
      else {
        $extra_room_text .= $rooms[$i] . ', ';
      }
    }

    return $extra_room_text;
  }

  private function getRequestArray(\stdClass $node) : array {
    global $base_path;
    $long_distance = FALSE;
    $flat_rate = '';

    $current_request_retrieve = $this->getCurrentMoveRequest($node);
    $wrapper = entity_metadata_wrapper('node', $node);
    $move_service_type = $wrapper->field_move_service_type->value();
    $move_field_date = $wrapper->field_date->value();
    $move_field_minimum_move_time = $wrapper->field_minimum_move_time->value();
    $move_field_maximum_move_time = $wrapper->field_maximum_move_time->value();
    $move_field_movers_count = $wrapper->field_movers_count->value();
    $move_field_delivery_date = $wrapper->field_delivery_date->value();
    $move_field_delivery_start_time1 = $wrapper->field_delivery_start_time1->value();
    $move_field_delivery_start_time2 = $wrapper->field_delvery_start_time2->value();
    $move_field_delivery_date_from = $wrapper->field_delivery_date_from->value();
    $move_field_delivery_date_to = $wrapper->field_delivery_date_to->value();
    $move_field_delivery_start = $wrapper->field_delivery_start->value();
    $move_field_delivery_crew_size = $wrapper->field_delivery_crew_size->value();
    $move_field_storage_price = $wrapper->field_storage_price->value();
    $move_field_reservation_price = $wrapper->field_reservation_price->value();
    $move_field_contract = $wrapper->field_contract->value();
    $move_field_first_name = $wrapper->field_first_name->value();
    $move_field_last_name = $wrapper->field_last_name->value();
    $move_field_e_mail = $wrapper->field_e_mail->value();
    $move_field_phone = $wrapper->field_phone->value();
    $move_field_apt_from = $wrapper->field_apt_from->value();
    $move_field_apt_to = $wrapper->field_apt_to->value();
    $move_field_extra_furnished_rooms = $wrapper->field_extra_furnished_rooms->value();
    $move_field_commercial_extra_rooms = $wrapper->field_commercial_extra_rooms->value();
    $move_field_list_truck = $wrapper->field_list_truck->value();
    $move_field_suggested_truck = $wrapper->field_suggested_truck->value();
    // Temporarily hide.
    //$suggested_trucks_number = !empty($wrapper->field_suggested_trucks_number->value()) ? $wrapper->field_suggested_trucks_number->value() : 0;
    $move_field_delivery_time = $wrapper->field_delivery_time->value();
    $move_field_start_time = $wrapper->field_start_time->value();
    $move_field_actual_start_time = $wrapper->field_actual_start_time->value();
    $move_field_start_time_max = $wrapper->field_start_time_max->value();
    $field_price_per_hour = $wrapper->field_price_per_hour->value();
    $move_field_price_per_hour = $this->getRequestPricePerHour($current_request_retrieve, $field_price_per_hour);
    $move_field_distance = $wrapper->field_distance->value();
    $move_field_to_storage_move = $wrapper->field_to_storage_move->raw();
    $move_field_from_storage_move = $wrapper->field_from_storage_move->raw();
    $move_field_reservation_received = $wrapper->field_reservation_received->value();
    $move_field_approve = $wrapper->field_approve->value();
    $move_field_size_of_move = $wrapper->field_size_of_move->value();
    $move_field_type_of_entrance_from = $wrapper->field_type_of_entrance_from->value();
    $move_field_type_of_entrance_to_ = $wrapper->field_type_of_entrance_to_->value();
    $move_field_move_service_type = $wrapper->field_move_service_type->value();
    $move_field_moving_from = $wrapper->field_moving_from->value();
    $move_field_moving_to = $wrapper->field_moving_to->value();
    $move_field_pickup = $wrapper->field_extra_pickup->value();
    $move_field_dropoff = $wrapper->field_extra_dropoff->value();

    $extraServiceCalculator = (new MoveRequestExtraService())->setRequestRetrieve($current_request_retrieve)->build()->calculate();

    $is_enable_double_drive_time = ($this->calculate_settings->doubleDriveTime && isset($current_request_retrieve['field_double_travel_time']['raw'])) ? TRUE : FALSE;
    $is_show_travel_time = isset($current_request_retrieve['request_all_data']['travelTime']) ? $current_request_retrieve['request_all_data']['travelTime'] : $this->calculate_settings->travelTime;

    if ($is_enable_double_drive_time) {
      $move_field_travel_time = isset($current_request_retrieve['field_double_travel_time']['raw']) ? $current_request_retrieve['field_double_travel_time']['raw'] : 0;
    }
    else {
      $move_field_travel_time = $is_show_travel_time ? $wrapper->field_travel_time->value() : 0;
    }

    $fields_list = Front::getFieldListsValue();
    $enter_from = $fields_list['field_type_of_entrance_from'][$move_field_type_of_entrance_from];
    $enter_to = $fields_list['field_type_of_entrance_to_'][$move_field_type_of_entrance_to_];
    $size_of_move = isset($fields_list['field_size_of_move'][$move_field_size_of_move]) ? $fields_list['field_size_of_move'][$move_field_size_of_move] : 0;
    $service_type = $fields_list['field_move_service_type'][$move_field_move_service_type];
    $status = $fields_list['field_approve'][$move_field_approve];

    if (isset($move_service_type)) {
      $flat_rate = $move_service_type == "5";
      $long_distance = $move_service_type == "7";
    }

    $date = isset($move_field_date) ? date("d-n-Y", $move_field_date) : '';
    $min_time = isset($move_field_minimum_move_time) ? $move_field_minimum_move_time : 0;
    $max_time = isset($move_field_maximum_move_time) ? $move_field_maximum_move_time : 0;
    $delivery_crew_size = isset($move_field_movers_count) ? $move_field_movers_count : 0;
    $storage_rate = isset($move_field_storage_price) ? $move_field_storage_price : 0;
    $reservation_rate = isset($move_field_reservation_price) ? $move_field_reservation_price : 0;

    $delivery_date = $date;
    $delivery_from = '';
    $delivery_from2 = '';
    $delivery_to = '';
    $delivery_to2 = '';
    $delivery_start_time = '';
    $delivery_end_time = '';
    $dstart_time1 = '';
    $dstart_time2 = '';
    $flat_rate_grand_total = 0;
    $contract_img_url = '';
    $name = '';

    // TODO.Вернуться и проверить!
    if ($flat_rate) {
      $delivery_date = isset($move_field_delivery_date) ? date("d-n-Y", $move_field_delivery_date) : '';
      $delivery_from = isset($move_field_delivery_date_from) ? date("Y-m-d", $move_field_delivery_date_from) : '';
      $delivery_from2 = isset($move_field_delivery_date_from) ? date("j F, Y", $move_field_delivery_date_from) : '';
      $delivery_to = isset($move_field_delivery_date_to) ? $move_field_delivery_date_to : '';
      $dstart_time1 = isset($move_field_delivery_start_time1) ? $move_field_delivery_start_time1 : '';
      $dstart_time2 = isset($move_field_delivery_start_time2) ? $move_field_delivery_start_time2 : '';
      $delivery_start_time = isset($move_field_delivery_start) ? $move_field_delivery_start : '';
      $delivery_crew_size = isset($move_field_delivery_crew_size) ? $move_field_delivery_crew_size : 0;

      if ($delivery_from && $delivery_to && strtotime($delivery_from) < strtotime($delivery_to)) {
        $delivery_to = date("Y-m-d", strtotime($delivery_to));
        $delivery_to2 = date("j F, Y", strtotime($delivery_to));
      }

      $flat_rate_grand_total = $this->calculateFlatRateGrandTotal($current_request_retrieve);
    }

    // Contract URL.
    if ($move_field_approve == 8) {
      if (isset($move_field_contract)) {
        $fid = $move_field_contract;
        $file = file_load($fid);
        $uri = $file->uri;
        $contract_img_url = file_create_url($uri);
      }
    }

    if (isset($move_field_first_name) && isset($move_field_last_name)) {
      $name = $move_field_first_name . ' ' . $move_field_last_name;
    }

    $first_name = isset($move_field_first_name) ? ucfirst($move_field_first_name) : '';
    $last_name = isset($move_field_last_name) ? ucfirst($move_field_last_name) : '';
    $email = isset($move_field_e_mail) ? $move_field_e_mail : '';
    $phone = isset($move_field_phone) ? $move_field_phone : '';
    $apt_from = isset($move_field_apt_from) ? $move_field_apt_from : '';
    $apt_to = isset($move_field_apt_to) ? $move_field_apt_to : '';

    if ($move_field_size_of_move == 11) {
      $field_commercial_extra_rooms = reset($move_field_commercial_extra_rooms);
      $room_text = $fields_list['field_commercial_extra_rooms'][$field_commercial_extra_rooms] ?? '';
    }
    else {
      $room_text = $this->getRoomText($move_field_extra_furnished_rooms, $fields_list['field_extra_furnished_rooms']);
    }

    $trucks = array();
    if (isset($move_field_list_truck)) {
      $truck_list = $move_field_list_truck;
      foreach ($truck_list as $i => $truck) {
        array_push($trucks, $truck);
      }
    }

    $move_size = isset($move_field_size_of_move) ? $move_field_size_of_move : 1;

    $suggested_trucks = '';
    if (empty($move_field_suggested_truck)) {
      $truck_rooms = count($move_field_extra_furnished_rooms);
      if ($move_size != 1 && $move_size != 2) {
        $truck_rooms++;
      }
      $suggested_trucks = $move_field_suggested_truck;
    }

    $city_from = isset($move_field_moving_from['locality']) ? $move_field_moving_from['locality'] : '';
    $state_from = isset($move_field_moving_from['administrative_area']) ? $move_field_moving_from['administrative_area'] : '';
    $zip_from = isset($move_field_moving_from['postal_code']) ? $move_field_moving_from['postal_code'] : '';
    $adrfrom = isset($move_field_moving_from['thoroughfare']) ? $move_field_moving_from['thoroughfare'] : '';
    $from = $city_from . ', ' . $state_from . ', ' . $zip_from;

    $city_to = isset($move_field_moving_to['locality']) ? $move_field_moving_to['locality'] : '';
    $state_to = isset($move_field_moving_to['administrative_area']) ? $move_field_moving_to['administrative_area'] : '';
    $zip_to = isset($move_field_moving_to['postal_code']) ? $move_field_moving_to['postal_code'] : '';
    $adrto = isset($move_field_moving_to['thoroughfare']) ? $move_field_moving_to['thoroughfare'] : '';
    $to = $city_to . ', ' . $state_to . ', ' . $zip_to;

    $pickup_city = $move_field_pickup['locality'] ?? '';
    $pickup_state = $move_field_pickup['administrative_area'] ?? '';
    $pickup_zip = $move_field_pickup['postal_code'] ?? '';
    $pickup_adrto = $move_field_pickup['thoroughfare'] ?? '';
    $pickup_premise = $move_field_pickup['premise'] ?? '';
    $pickup_data = $pickup_city . ', ' . $pickup_state . ', ' . $pickup_zip;
    $pickup_entarence = $move_field_pickup['organisation_name'] ?? '';
    $pickup_entarence = !empty($pickup_entarence) ? $fields_list['field_type_of_entrance_from'][$pickup_entarence] : '';

    $dropoff_city = $move_field_dropoff['locality'] ?? '';
    $dropoff_state = $move_field_dropoff['administrative_area'] ?? '';
    $dropoff_zip = $move_field_dropoff['postal_code'] ?? '';
    $dropoff_adrto = $move_field_dropoff['thoroughfare'] ?? '';
    $dropoff_premise = $move_field_dropoff ?? '';
    $dropoff_data = $dropoff_city . ', ' . $dropoff_state . ', ' . $dropoff_zip;
    $dropoff_entarence = $move_field_dropoff['organisation_name'] ?? '';
    $dropoff_entarence = !empty($dropoff_entarence) ? $fields_list['field_type_of_entrance_to_'][$dropoff_entarence] : '';

    $start_time = '';
    $start_time2 = '';
    $pref_time = '';

    // Set delivery time
    // Don't know where use $move_field_delivery_time.
    if (isset($move_field_delivery_time)) {
      $delivery_time = $move_field_delivery_time;
    }
    else {
      // Если нету delivery time, инициализируем delivery time через $move_field_start_time
      // delivery time немного позже чем start time
      // Preferred Time.
      $s_time = $move_field_start_time;
      // By default.
      if ($s_time == 1) {
        $start_time = '9:00 AM';
        $start_time2 = '';
        $pref_time = 'Any Time';
      }
      if ($s_time == 2) {
        $start_time = '9:00 AM';
        $start_time2 = '12:00 PM';
        $pref_time = '9AM-12PM';
      }
      if ($s_time == 3) {
        $start_time = '12:00 PM';
        $start_time2 = '3:00 PM';
        $pref_time = '12:00 PM - 3:00 PM';
      }
      if ($s_time == 4) {
        $start_time = '3:00 PM';
        $start_time2 = '6:00 PM';
        $pref_time = '3:00 PM - 6:00 PM';
      }

      if (isset($start_time2) && !empty($start_time2)) {
        $delivery_time = $start_time . ' - ' . $start_time2;
      }
      else {
        $delivery_time = $start_time;
      }
    }

    // IF start Time min and max is empty - take from preferred.
    $s_time = isset($move_field_start_time) ? $move_field_start_time : 1;

    if ($s_time == 1) {
      // By default.
      $start_time = '8:00 AM';
      $start_time2 = '10:00 AM';
      $pref_time = 'Any Time';
    }
    if ($s_time == 2) {
      $start_time = '8:00 AM';
      $start_time2 = '10:00 AM';
      $pref_time = '8:00 AM - 10:00 AM';
    }
    if ($s_time == 3) {
      $start_time = '11:00 AM';
      $start_time2 = '2:00 PM';
      $pref_time = '11:00 AM - 2:00 PM';
    }
    if ($s_time == 4) {
      $start_time = '1:00 PM';
      $start_time2 = '4:00 PM';
      $pref_time = '1:00 PM - 4:00 PM';
    }
    if ($s_time == 5) {
      $start_time = '3:00 PM';
      $start_time2 = '7:00 PM';
      $pref_time = '3:00 PM - 7:00 PM';
    }

    // Set start time.
    if (isset($start_time2)) {
      $sT = $start_time . ' - ' . $start_time2;
    }
    else {
      $sT = $start_time;
    }

    if (isset($move_field_actual_start_time)) {
      $start_time = $move_field_actual_start_time;
    }
    if (isset($move_field_start_time_max)) {
      $start_time2 = $move_field_start_time_max;
    }
    $crew = '';
    if (isset($move_field_movers_count)) {
      $crew = $move_field_movers_count;
    }
    $travel_time = '';
    if (isset($move_field_travel_time)) {
      $travel_time = $move_field_travel_time;
    }
    $price_per_hour = '';
    if (isset($move_field_price_per_hour)) {
      $price_per_hour = $move_field_price_per_hour;
    }
    $distance = 0;
    if (isset($move_field_distance)) {
      $distance = $move_field_distance;
    }

    // IF MOVING AND STORAGE THE CREATE REQ LINK.
    $storage_id = 0;
    if (isset($move_field_move_service_type) && $move_field_move_service_type == 2) {
      if (isset($move_field_to_storage_move)) {
        $storage_id = $move_field_to_storage_move;
      }
      else {
        if (isset($move_field_from_storage_move)) {
          $storage_id = $move_field_from_storage_move;
        }
      }
    }

    $from_storage = 0;
    if (isset($move_field_from_storage_move)) {
      $from_storage = 1;
    }
    if (isset($move_field_to_storage_move)) {
      $from_storage = 2;
    }

    $request_url = 'http://' . $_SERVER["HTTP_HOST"] . $base_path . 'request/' . $node->nid;
    $request_link = '<a href="' . $request_url . '"><i class="fa fa-external-link"></i></a>';
    $reservation_paid = FALSE;

    if (isset($move_field_reservation_received)) {
      $reservation_paid = $move_field_reservation_received;
    }

    $extra_services = "$" . number_format($extraServiceCalculator->extraServiceTotal, 2);

    $packing = "$" . number_format($this->calculatePackingServices($current_request_retrieve), 2);
    $fuel = $this->getFuelCharge($current_request_retrieve);

    $discount = '$0.00';

    if (isset($current_request_retrieve["request_all_data"]["add_money_discount"])) {
      if (floatval($current_request_retrieve["request_all_data"]["add_money_discount"]) > 0) {
        $discount = "$" . number_format(floatval($current_request_retrieve["request_all_data"]["add_money_discount"]), 2);
      }
    }

    if (isset($current_request_retrieve["request_all_data"]["add_percent_discount"])) {
      if (floatval($current_request_retrieve["request_all_data"]["add_percent_discount"]) > 0) {
        $discount = floatval($current_request_retrieve["request_all_data"]["add_percent_discount"]) . "%";
      }
    }

    $valuation = "$" . number_format(floatval($this->getValuationCharge($current_request_retrieve)), 2);

    $estimated_total_time = $this->getReadableTime($min_time + $travel_time, FALSE) . ' - ' . $this->getReadableTime($max_time + $travel_time, FALSE);
    $labor_time = $this->getReadableTime($min_time, FALSE) . ' - ' . $this->getReadableTime($max_time, FALSE);

    $long_distance_grand_total = '';
    $estimated_price_number = 0.00;

    if ($long_distance) {
      $long_distance_grand_total = $this->getLongDistanceGrandTotal($current_request_retrieve);
      $instance = new DistanceCalculate();
      $estimated_price_number = $instance->getLongDistanceQuote($current_request_retrieve);
      $estimated_price = '$' . number_format($estimated_price_number, 2);
    }
    else {
      $estimated_price = $this->getEstimatedPrice($current_request_retrieve, TRUE, TRUE, $is_enable_double_drive_time);
      $temp = preg_replace('/\$/', '', $estimated_price);
      $temp = preg_replace('/\,/', '', $temp);
      $explode_quote = explode('-', $temp);
      $quote_min = (float) (trim($explode_quote[0]));
      $quote_max = isset($explode_quote[1]) ? (float) (trim($explode_quote[1])) : 0;
      $estimated_price_number = (float) ($quote_max + $quote_min) / 2;
    }

    $assign_manager = "";

    if (isset($current_request_retrieve["manager"]["first_name"]) && isset($current_request_retrieve["manager"]["last_name"])) {
      $assign_manager = $current_request_retrieve["manager"]["first_name"] . ' ' . $current_request_retrieve["manager"]["last_name"];
    }

    $packing_table = $this->makePackingTable($current_request_retrieve);

    $additional_services_table = $this->makeAdditionalServicesTable($current_request_retrieve);

    $inventory_table = $this->getInventoryTable($current_request_retrieve, TRUE);
    $inventory_table_without_cf = $this->getInventoryTable($current_request_retrieve, FALSE);
    $valuation_table = static::prepareValuationDeductibleTable($current_request_retrieve);

    $closing_discount = '$0.00';
    if (!empty($current_request_retrieve['request_all_data']['invoice']['request_all_data']['add_money_discount'])) {
      $closing_discount = "$" . number_format(floatval($current_request_retrieve['request_all_data']['invoice']['request_all_data']['add_money_discount']), 2);
    }
    elseif (!empty($current_request_retrieve['request_all_data']['invoice']['request_all_data']['add_percent_discount'])) {
      $closing_discount = floatval($current_request_retrieve['request_all_data']['invoice']['request_all_data']['add_percent_discount']) . "%";
    }
    $closing_packing = "$" . number_format($this->calculatePackingServices($current_request_retrieve, TRUE), 2);
    $per_rate_cf = $current_request_retrieve['request_all_data']['invoice']['field_long_distance_rate']['value'] ?? $current_request_retrieve['field_long_distance_rate']['value'] ?? "0.00";

    $per_rate_lbs = "0.00";
    if ($per_rate_cf != "0.00") {
      $per_rate_lbs = number_format(floatval($per_rate_cf / 7), 2);
    }
    $date_created = date("m/d/Y", $current_request_retrieve['created']);
    $invetory = new Inventory($node->nid);
    $invetory_retrive = $invetory->retrieve();
    $avl_delv_date = !empty($invetory_retrive['move_details']['delivery']) ? $invetory_retrive['move_details']['delivery'] : '';

    // Pickup date range parsing.
    $pickup_range_date = "";
    if (!empty($invetory_retrive['move_details']['pickup'])) {
      $dates = explode(",", $invetory_retrive['move_details']['pickup']);
      // TODO Сейчас timestamp нужно делить на 1000 чтобы получить адекватное значение. Рефакторить на фронте и бекенде.
      if (count($dates) == 2) {
        $one_time = intval($dates[0] / 1000);
        $two_time = intval($dates[1] / 1000);
        // Compare two dates.
        if ($one_time > $two_time) {
          $date_start = date("m/d/Y", $two_time);
          $date_end = date("m/d/Y", $one_time);
        }
        else {
          $date_start = date("m/d/Y", $one_time);
          $date_end = date("m/d/Y", $two_time);
        }
        $pickup_range_date = $date_start . " To " . $date_end;
      }
      else {
        $one_time = intval($dates[0] / 1000);
        $date_start = date("m/d/Y", $one_time);
        $pickup_range_date = $date_start;
      }
    }

    $closing_payment = 0.00;
    foreach ($current_request_retrieve['receipts'] as $receipt) {
      $closing_payment += (float) $receipt['amount'];
    }

    $closing_fuel_percent = "";
    if (!empty($current_request_retrieve['request_all_data']['invoice']['request_all_data']['surcharge_fuel'])) {
      $closing_fuel = floatval($current_request_retrieve['request_all_data']['invoice']['request_all_data']['surcharge_fuel']);
      $closing_fuel_percent = number_format(($closing_fuel / $estimated_price_number) * 100, 2) . "%";
    }
    $request_inst = new MoveRequest($node->nid);
    $notes = $request_inst->get_notes(array('client_notes', 'foreman_notes'));

    $min_cf = $current_request_retrieve['request_all_data']['invoice']['request_all_data']['min_weight'] ?? $current_request_retrieve['request_all_data']['min_weight'] ?? 0;
    $min_cf_str = $min_cf . " c.f.";

    $move_request = array(
      'name' => $name,
      'client_firstname' => $first_name,
      'client_lastname' => $last_name,
      'email' => $email,
      'phone' => $phone,
      'nid' => $node->nid,
      'status' => $status,
      'statusid' => isset($move_field_approve) ? $move_field_approve : 1,
      'oldstatusid' => isset($move_field_approve) ? $move_field_approve : 1,
      'date' => date("Y-n-j", $move_field_date),
      'day' => date("j", $move_field_date),
      'month' => date("n", $move_field_date),
      'year' => date("Y", $move_field_date),
      'move_size' => isset($size_of_move) ? $size_of_move : '',
      'move_size_id' => isset($move_field_size_of_move) ? $move_field_size_of_move : 1,
      'crew' => $crew,
      'start_time' => $sT,
      'delivery_time' => $delivery_time,
      'sT1' => _move_convert_time($start_time),
      'sT2' => _move_convert_time($start_time2),
      'start_time1' => $start_time,
      'start_time2' => $start_time2,
      'type_from' => isset($enter_from) ? $enter_from : '',
      'type_from_id' => isset($move_field_type_of_entrance_from) ? $move_field_type_of_entrance_from : 1,
      'type_to' => isset($enter_to) ? $enter_to : '',
      'type_to_id' => isset($move_field_type_of_entrance_to_) ? $move_field_type_of_entrance_to_ : 1,
      'tt' => $this->getReadableTime($move_field_travel_time, FALSE),
      'rate' => $price_per_hour,
      'service_type' => isset($service_type) ? $service_type : '',
      'service_type_id' => isset($move_field_move_service_type) ? $move_field_move_service_type : 1,
      'flat_rate' => $flat_rate,
      'from' => $from,
      'adrto' => $adrto,
      'adrfrom' => $adrfrom,
      'apt_from' => $apt_from,
      'city_from' => $city_from,
      'state_from' => $state_from,
      'zip_from' => $zip_from,
      'to' => $to,
      'apt_to' => $apt_to,
      'city_to' => $city_to,
      'state_to' => $state_to,
      'zip_to' => $zip_to,
      'pickup_city' => $pickup_city,
      'pickup_state' => $pickup_state,
      'pickup_zip' => $pickup_zip,
      'pickup_adrto' => $pickup_adrto,
      'pickup_data' => $pickup_data,
      'pickup_premise' => $pickup_premise,
      'pickup_entarence' => $pickup_entarence,
      'dropoff_city' => $dropoff_city,
      'dropoff_state' => $dropoff_state,
      'dropoff_zip' => $dropoff_zip,
      'dropoff_adrto' => $dropoff_adrto,
      'dropoff_data' => $dropoff_data,
      'dropoff_premise' => $dropoff_premise,
      'dropoff_entarence' => $dropoff_entarence,
      'trucks' => $trucks,
      'current_truck_number' => count($trucks),
      'est_time' => $estimated_total_time,
      'est_qoute' => $estimated_price,
      'request_link' => $request_link,
      'request_url' => $request_url,
      'delivery_date_from' => $delivery_from,
      'delivery_date_to' => $delivery_to,
      'delivery_date_from2' => $delivery_from2,
      'delivery_date_to2' => $delivery_to2,
      'delivery_end_time' => $delivery_end_time,
      'delivery_start_time' => $delivery_start_time,
      'ddate' => $delivery_date,
      'dday' => date("j", $move_field_date),
      'dmonth' => date("n", $move_field_date),
      'dyear' => date("Y", $move_field_date),
      'dstart_time1' => $dstart_time1,
      'dstart_time2' => $dstart_time2,
      'delivery_crew_size' => $delivery_crew_size,
      'rooms' => $move_field_extra_furnished_rooms,
      'min_time' => $this->getReadableTime($min_time - $move_field_travel_time, TRUE),
      'max_time' => $this->getReadableTime($max_time - $move_field_travel_time, TRUE),
      'minimum_time' => $min_time,
      'maximum_time' => $max_time,
      'storage_rate' => $storage_rate,
      'reservation_rate' => $reservation_rate,
      'contract_img' => $contract_img_url,
      'storage_id' => $storage_id,
      'pref_time' => $pref_time,
      'from_storage' => $from_storage,
      'suggested_trucks' => $suggested_trucks,
      'distance' => $distance,
      'driving_time' => 0,
      'reservation_paid' => $reservation_paid,
      'extra_services' => $extra_services,
      'room_text' => $room_text,
      'packing_total' => $packing,
      'discount' => $discount,
      'valuation' => $valuation,
      'fuel' => "$" . number_format($fuel, 2),
      'ld_grand_total' => $long_distance_grand_total ? '$' . number_format(floatval($long_distance_grand_total), 2) : '$' . number_format(floatval($long_distance_grand_total), 2),
      'fr_grand_total' => $flat_rate_grand_total ? '$' . number_format(floatval($flat_rate_grand_total), 2) : '$' . number_format($flat_rate_grand_total, 2),
      'labor_time' => $labor_time,
      'assign_manager' => $assign_manager,
      'packing_table' => $packing_table,
      'additional_services_table' => $additional_services_table,
      'inventory_table' => $inventory_table,
      'inventory_table_without_cf' => $inventory_table_without_cf,
      'closing_packing' => $closing_packing,
      'closing_additional_services' => !empty($current_request_retrieve['request_all_data']['invoice']['extraServices']) ? '$' . number_format($extraServiceCalculator->extraServiceTotalClosing, 2) : "$0.00",
      'closing_discount' => $closing_discount,
      'closing_grand_total' => isset($current_request_retrieve['request_all_data']['grand_total']) ? number_format(floatval($current_request_retrieve['request_all_data']['grand_total']), 2) : 0.00,
      'closing_fuel' => !empty($current_request_retrieve['request_all_data']['invoice']['request_all_data']['surcharge_fuel']) ? "$" . number_format(floatval($current_request_retrieve['request_all_data']['invoice']['request_all_data']['surcharge_fuel']), 2) : "$" . 0.00,
      'closing_fuel_percent' => $closing_fuel_percent,
      'closing_cubic_feet' => !empty($current_request_retrieve['request_all_data']['invoice']['closing_weight']['value']) ? $current_request_retrieve['request_all_data']['invoice']['closing_weight']['value'] . "c.f./" . $current_request_retrieve['request_all_data']['invoice']['closing_weight']['value'] * 7 . "lbs" : 0 . "c.f." ,
      'rate_per_cf_for_ld' => "$" . $per_rate_cf . " per c.f.",
      'rate_per_lbs_for_ld' => "$" . $per_rate_lbs . " per lbs",
      'min_cf' => $min_cf_str,
      'created' => $date_created,
      'avl_delv_date' => $avl_delv_date,
      'closing_payment' => "$" . number_format(floatval($closing_payment), 2),
      'pick_up_date_range' => $pickup_range_date,
      'foreman_note' => !empty($notes->foreman_notes) ? $notes->foreman_notes : '',
      'client_note' => !empty($notes->client_notes) ? $notes->client_notes : '',
       //'suggested_trucks_number' => $suggested_trucks_number,
      'home_estimator' => $current_request_retrieve['home_estimator']['first_name'] . ' ' . $current_request_retrieve['home_estimator']['last_name'],
      'valuation_table' => $valuation_table,
    );

    return $move_request;
  }

  public function getCurrentMoveRequest(\stdClass $node) : array {
    $instance = new MoveRequest($node->nid);
    return $instance->retrieve();
  }

  /**
   * Calculate grand total for flatrate.
   *
   * @param array $request_retrieve
   *   Request retrieve data.
   * @param bool $closing
   *   Is closing.
   *
   * @return float
   *   Grand total value.
   */
  public function calculateFlatRateGrandTotal(array $request_retrieve, bool $closing = FALSE) : float {
    $result = 0;

    if (isset($request_retrieve['flat_rate_quote']) && isset($request_retrieve['flat_rate_quote']['value'])) {
      $result += (float) $request_retrieve['flat_rate_quote']['value'];
    }

    $extraServiceCalculator = (new MoveRequestExtraService())->setRequestRetrieve($request_retrieve)->build()->calculate();

    $result += !empty($extraServiceCalculator->extraServiceTotalClosing) ? $extraServiceCalculator->extraServiceTotalClosing : $extraServiceCalculator->extraServiceTotal;
    $result += $this->calculatePackingServices($request_retrieve, $closing);
    $result += (float) $this->getValuationCharge($request_retrieve);
    $result += (float) $this->getFuelCharge($request_retrieve);

    $result -= $this->getCoupon($request_retrieve);

    $result -= $this->getMoneyDiscount($request_retrieve, $closing);

    $percent_discount = $this->getPercentDiscount($request_retrieve, $closing);
    if ($percent_discount > 0) {
      $result = $result * (1 - $percent_discount / 100);
    }

    return $result;
  }

  /**
   * Calculate grand total.
   *
   * @param array $request_retrieve
   *   Request retrieve data.
   * @param bool $closing
   *   Is closing.
   *
   * @return array
   *   Array with maximum and minimum grand total.
   */
  public function calculateGrandTotal(array $request_retrieve, bool $closing = FALSE) : array {
    $result = [];

    // Add Flatrate part.
    if ($request_retrieve['service_type']['raw'] == RequestServiceType::FLAT_RATE) {
      $result[0] = $this->calculateFlatRateGrandTotal($request_retrieve);
    }
    elseif ($request_retrieve['service_type']['raw'] == RequestServiceType::LONG_DISTANCE) {
      $instance = new DistanceCalculate();
      $result[0] = $instance->getLongDistanceQuote($request_retrieve, $closing);
    }
    else {
      $calculate_settings = json_decode(variable_get('calcsettings', array()));
      $is_enable_double_drive_time = ($calculate_settings->doubleDriveTime && isset($current_request_retrieve['field_double_travel_time']['raw']));
      $result = $this->getEstimatedPrice($request_retrieve, FALSE, TRUE, $is_enable_double_drive_time, $closing);
    }

    return $result;
  }

  public function getValuationCharge(array $request_retrieve) : float {
    if (isset($request_retrieve['request_all_data']['valuation']['selected']['valuation_charge'])) {
      $result = (float) $request_retrieve['request_all_data']['valuation']['selected']['valuation_charge'];
    }

    return $result ?? 0;
  }

  private function getFuelCharge(array $request_retrieve) : float {
    $result = 0;
    if (isset($request_retrieve["request_all_data"]["surcharge_fuel"])) {
      $result = (float) $request_retrieve["request_all_data"]["surcharge_fuel"];
    }
    return $result;
  }

  /**
   * Get coupon from request_all_data.
   *
   * @param array $request_retrieve
   *   Request object.
   *
   * @return float
   *   Coupon value.
   */
  private function getCoupon(array $request_retrieve) : float {
    $result = 0;
    if (isset($request_retrieve["request_all_data"]["couponData"]["discount"])) {
      $result = (float) $request_retrieve["request_all_data"]["couponData"]["discount"];
    }
    return $result;
  }

  public function getFuelChargeClosing(array $request_retrieve) : float {
    $result = 0;
    if (isset($request_retrieve["request_all_data"]['invoice']["request_all_data"]["surcharge_fuel"])) {
      $result = (float) $request_retrieve["request_all_data"]['invoice']["request_all_data"]["surcharge_fuel"];
    }
    return $result;
  }

  /**
   * Prepare data for calculating packing.
   *
   * @param array $request_retrieve
   *   Request data.
   * @param bool $closing
   *   Bool true = calculate packing for closing.
   *
   * @return float
   *   Packing total.
   */
  public function calculatePackingServices(array $request_retrieve, bool $closing = FALSE) : float {
    $type_service = FALSE;

    if (isset($request_retrieve["service_type"])) {
      $raw = $request_retrieve["service_type"]["raw"];
      $raw_types = array(5, 7, 9, 10, 11);
      $type_service = in_array($raw, $raw_types);
    }

    $request_retrieve["request_data"] = isset($request_retrieve["request_data"]) ? $request_retrieve["request_data"] : array();
    $temp = !empty($request_retrieve['request_all_data']['invoice']['request_data']) ? $request_retrieve['request_all_data']['invoice']['request_data'] : array();
    if ($closing) {
      $result = $this->getPackingTotal($temp, $type_service);
    }
    else {
      $result = $this->getPackingTotal($request_retrieve["request_data"], $type_service);
    }

    return $result;
  }

  public function getPackingTotal(array $packings, bool $type_service) : float {
    $add_extra_charges = array();
    $total_services = 0;
    // Check show labor rate only on long distance and flat rate.
    $settings = json_decode(variable_get('basicsettings'));
    $checkShowLaborRate = $settings->packing_settings->packingLabor;

    if (!empty($packings["value"])) {
      $extra_charges = $packings["value"];
      if (is_array($extra_charges) && isset($extra_charges['packings'])) {
        $add_extra_charges = (array) $extra_charges['packings'];
      }
      elseif (is_object($extra_charges) && property_exists($extra_charges, 'packings')) {
        $add_extra_charges = (array) $extra_charges->packings;
      }
    }

    foreach ($add_extra_charges as $charge) {
      $charge = (object) $charge;
      if ($type_service) {
        if (!isset($charge->laborRate)) {
          $charge->laborRate = 0.0;
        }
        else {
          $charge->laborRate = (float) $charge->laborRate;
        }
        if ($checkShowLaborRate) {
          $charge_cost = floatval($charge->quantity) * ($charge->LDRate + $charge->laborRate);
        }
        else {
          $charge_cost = floatval($charge->quantity) * ($charge->LDRate ?? 0);
        }
      }
      else {
        $charge_cost = floatval($charge->rate ?? 0) * floatval($charge->quantity);
      }

      $total_services += $charge_cost;
    }

    return $total_services;
  }

  /**
   * Get grandTotal for Long Distance.
   *
   * @param array $request_retrieve
   *   Array with request data.
   * @param bool $closing
   *   Show for closing or not.
   *
   * @return float
   *   Grand total value.
   */
  public function getLongDistanceGrandTotal(array $request_retrieve, bool $closing = FALSE) : float {
    $result = 0.0;

    $result += $this->calculatePackingServices($request_retrieve, $closing);
    $result += (float) $this->getValuationCharge($request_retrieve);
    $result += $closing ? (float) $this->getFuelChargeClosing($request_retrieve) : (float) $this->getFuelCharge($request_retrieve);

    $extraServiceCalculator = (new MoveRequestExtraService())->setRequestRetrieve($request_retrieve)->build()->calculate();
    $result += !empty($extraServiceCalculator->extraServiceTotalClosing) ? $extraServiceCalculator->extraServiceTotalClosing : $extraServiceCalculator->extraServiceTotal;

    $result -= $this->getMoneyDiscount($request_retrieve, $closing);
    $result -= $this->getCoupon($request_retrieve);

    $instance = new DistanceCalculate();
    $result += $instance->getLongDistanceQuote($request_retrieve, $closing);

    $percent_discount = $this->getPercentDiscount($request_retrieve, $closing);

    if ($percent_discount > 0) {
      $result = $result * (1 - $percent_discount / 100);
    }

    if ($closing && !empty($request_retrieve['request_all_data']['req_tips'])) {
      $result += (float) $request_retrieve['request_all_data']['req_tips'];
    }

    return $result;
  }

  /**
   * Get money discount.
   *
   * @param array $request_retrieve
   *   Array with request data.
   * @param bool $closing
   *   Show for closing or not.
   *
   * @return float
   *   Money discount.
   */
  public function getMoneyDiscount(array $request_retrieve, bool $closing = FALSE) : float {
    $result = 0.00;

    if (isset($request_retrieve['request_all_data']['add_money_discount']) && !$closing) {
      $result = $request_retrieve['request_all_data']['add_money_discount'];
    }

    if (isset($request_retrieve['request_all_data']['invoice']['request_all_data']['add_percent_discount']) && $closing) {
      $result = $request_retrieve['request_all_data']['invoice']['request_all_data']['add_money_discount'];
    }
    return (float) $result;
  }

  /**
   * Get percent discount.
   *
   * @param array $request_retrieve
   *   Array with request data.
   * @param bool $closing
   *   Show for closing or not.
   *
   * @return float
   *   Percent discount.
   */
  public function getPercentDiscount(array $request_retrieve, bool $closing = FALSE) : float {
    $percent_discount = 0.00;

    if (isset($request_retrieve['request_all_data']['add_percent_discount']) && !$closing) {
      $percent_discount = (float) $request_retrieve['request_all_data']['add_percent_discount'];
    }

    if (isset($request_retrieve['request_all_data']['invoice']['request_all_data']['add_percent_discount']) && $closing) {
      $percent_discount = (float) $request_retrieve['request_all_data']['invoice']['request_all_data']['add_percent_discount'];
    }

    return (float) $percent_discount;
  }

  public function getCubicFeet(array $request_retrieve) {
    $request_size_index = (int) $request_retrieve["move_size"]["raw"];
    $move_size = isset($this->calculate_settings->size->{$request_size_index}) ? (int) $this->calculate_settings->size->{$request_size_index} : 1;
    $total_room_weight = 0;

    foreach ($request_retrieve["rooms"]["raw"] as $key => $rid) {
      $total_room_weight += (int) $this->calculate_settings->room_size->{$rid};
    }

    $total = new \stdClass();
    $total->weight = $total_room_weight + $move_size + (int) $this->calculate_settings->room_kitchen;
    $total->min = $total->weight - 100;
    $total->max = $total->weight + 100;

    return $total;
  }

  public function getEstimatedPrice(array $request_retrieve, $dollar = TRUE, $is_calculate_fuel = TRUE, $is_california = FALSE, $closing = FALSE) {
    if ($closing && isset($request_retrieve['request_all_data']['invoice'])) {
      // Если нет $request_retrieve['request_all_data']['invoice'] - выдаем ошибку.
      $request_retrieve = $request_retrieve['request_all_data']['invoice'];
    }

    $all_data = $request_retrieve['request_all_data'];

    $is_show_travel_time = isset($all_data['travelTime']) ? $all_data['travelTime'] : $this->calculate_settings->travelTime;
    $move_field_travel_time = $is_show_travel_time ? $request_retrieve['travel_time']['raw'] : 0;
    $move_field_price_per_hour = $this->getRequestPricePerHour($request_retrieve, $request_retrieve['rate']['value']);

    if ($is_california) {
      $travel_time = $request_retrieve['field_double_travel_time']['raw'];
    }
    else {
      $travel_time = isset($move_field_travel_time) ? $move_field_travel_time : 0;
    }



    $min_hours = $this->calculate_settings->min_hours;

    $move_field_minimum_work_time = ($closing) ? $request_retrieve['work_time_int'] : $request_retrieve['minimum_time']['raw'];
    $move_field_maximum_work_time = ($closing) ? $request_retrieve['work_time_int'] : $request_retrieve['maximum_time']['raw'];

    $min_time = isset($move_field_minimum_work_time) ? $move_field_minimum_work_time : 0;
    $max_time = isset($move_field_maximum_work_time) ? $move_field_maximum_work_time : 0;



    $price_per_hour = isset($move_field_price_per_hour) ? $move_field_price_per_hour : 0;

    $min_time_t = $min_time + $travel_time;

    if ($min_time_t < $min_hours) {
      $min_time_t = $min_hours;
    }

    $min_time_t = DistanceCalculate::roundTimeInterval($min_time_t);

    $max_time_t = $max_time + $travel_time;
    $move_max_job_time = $max_time_t;

    if ($max_time_t < $min_hours) {
      $max_time_t = $min_hours;
    }

    $max_time_t = DistanceCalculate::roundTimeInterval($max_time_t);

    $min_quote_t = (float) $min_time_t * $price_per_hour;
    $max_quote_t = (float) $max_time_t * $price_per_hour;

    $base_quote = 0;
    $request_retrieve['nid'] = isset($request_retrieve['nid']) ? $request_retrieve['nid'] : 0;

    $extraServiceCalculator = (new MoveRequestExtraService())->setRequestRetrieve($request_retrieve)->build()->calculate();
    $extra_services_total = !empty($extraServiceCalculator->extraServiceTotalClosing) ? $extraServiceCalculator->extraServiceTotalClosing : $extraServiceCalculator->extraServiceTotal;

    $base_quote += $extra_services_total;

    $packing_service = $this->calculatePackingServices($request_retrieve);
    $base_quote += $packing_service;

    $valuation = $this->getValuationCharge($request_retrieve);
    $base_quote += $valuation;

    $fuel = 0;
    if ($is_calculate_fuel) {
      $fuel = $this->getFuelCharge($request_retrieve);
      $base_quote += $fuel;
    }

    if (!isset($request_retrieve['quote'])) {
      $request_retrieve['quote'] = [];
      $request_retrieve['quote']['min'] = ($min_time_t) * $price_per_hour;
      $request_retrieve['quote']['max'] = ($max_time_t) * $price_per_hour;
    }

    $discount_apply = FALSE;
    $percent_discount_min = 0;
    $percent_discount_max = 0;

    if ((isset($all_data['add_percent_discount']) && $all_data['add_percent_discount'] > 0)
      || (isset($all_data['add_money_discount']) && $all_data['add_money_discount'] > 0)
      || (isset($all_data['add_rate_discount']) && $all_data['add_rate_discount'] > 0)) {

      $discount_apply = TRUE;

      if (isset($all_data['add_percent_discount'])) {
        // Min quote with discount + additional sevices.
        $total_min = $packing_service + $extra_services_total + $valuation + $fuel + $request_retrieve['quote']['min'];
        // Max quote with discount + additional sevices.
        $total_max = $packing_service + $extra_services_total + $valuation + $fuel + $request_retrieve['quote']['max'];
        $percent_discount_min = $total_min * $all_data['add_percent_discount'] / 100;
        $percent_discount_max = $total_max * $all_data['add_percent_discount'] / 100;
      }
    }

    if ($discount_apply) {
      if ($min_hours < $move_max_job_time) {
        $min_quote_t = $request_retrieve['quote']['min'];
        $min_quote_t += $base_quote;
        $min_quote_t -= $all_data['add_money_discount'];
        $min_quote_t -= $percent_discount_min;

        $max_quote_t = $request_retrieve['quote']['max'];
        $max_quote_t += $base_quote;
        $max_quote_t -= $all_data['add_money_discount'];
        $max_quote_t -= $percent_discount_max;
      }
      else {
        $max_quote_t = $min_hours * $price_per_hour + $base_quote - $all_data['add_money_discount'] - $percent_discount_min;
        $min_quote_t = $max_quote_t;
      }
    }
    else {
      if ($min_hours < $move_max_job_time) {
        $min_quote_t += $base_quote;
        $max_quote_t += $base_quote;
      }
      else {
        $min_quote_t = $min_hours * $request_retrieve['rate']['value'] + $base_quote;
        $max_quote_t = $min_quote_t;
      }
    }

    $min_quote_t = number_format($min_quote_t, 2);
    $max_quote_t = number_format($max_quote_t, 2);

    if ($dollar) {
      if ($max_time_t != $min_time_t) {
        $result = "\${$min_quote_t} - \${$max_quote_t}";
      }
      else {
        $result = "\${$max_quote_t}";
      }
    }
    else {
      $result = array($min_quote_t, $max_quote_t);
    }

    return $result;
  }

  private function getRequestPricePerHour(array $request_retrieve, $rate) {
    $result = 0;

    if (isset($request_retrieve['request_all_data']['add_rate_discount'])) {
      $rate_discount = $request_retrieve['request_all_data']['add_rate_discount'];
    }
    if (isset($rate_discount) && $rate_discount > 0) {
      $result = $rate_discount;
    }
    elseif (isset($rate)) {
      $result = (float) $rate;
    }

    return $result;
  }

  private function calculateTotalBalance(array $request_retrieve, string $type) {
    $total = 0;

    if (!isset($request_retrieve['request_all_data']['add_money_discount'])) {
      $request_retrieve['request_all_data']['add_money_discount'] = 0;
    }

    if (!isset($request_retrieve['request_all_data']['add_percent_discount'])) {
      $request_retrieve['request_all_data']['add_percent_discount'] = 0;
    }

    if ($type == 'min') {
      // TODO See requestController -> calculateTotalBalance.
      $total = $request_retrieve['quote']['min'];
    }
    elseif ($type == 'max') {
      $total = $request_retrieve['quote']['max'];
    }
    elseif ($type == 'longdistance') {
      $extraServiceCalculator = (new MoveRequestExtraService())->setRequestRetrieve($request_retrieve)->build()->calculate();

      $total = (new DistanceCalculate())->getLongDistanceQuote($request_retrieve) + intval($extraServiceCalculator->extraServiceTotal) - $request_retrieve['request_all_data']['add_money_discount'];
    }

    $discount = $request_retrieve['request_all_data']['add_money_discount'] + $total * $request_retrieve['request_all_data']['add_percent_discount'] / 100;

    return $total - $discount;
  }

  private function makePackingTable(array $request_retrieve) : string {
    $total = 0;
    $result = '';
    $packings = isset($request_retrieve['request_data']) ? $request_retrieve['request_data'] : [];
    // Check show labor rate only on long distance and flat rate.
    $settings = json_decode(variable_get('basicsettings'));
    $checkShowLaborRate = $settings->packing_settings->packingLabor;

    if ($packings) {
      $is_long_distance = $request_retrieve['service_type']['raw'] == 5;
      $is_flat_rate = $request_retrieve['service_type']['raw'] == 7;
      $rows = [];

      if (($is_flat_rate || $is_long_distance) && $checkShowLaborRate) {
        $rows[] = array(
          'data' => array(
            array('data' => ''),
            array('data' => 'Quantity'),
            array('data' => 'Unit Cost'),
            array('data' => 'Labor Rate'),
            array('data' => 'Total'),
          ),
          'style' => array(
            'border-bottom: 1px dotted #dddddd;',
            'line-height: 20px;',
            'color: #4d93ff;',
          ),
        );
      }
      else {
        $rows[] = array(
          'data' => array(
            array('data' => ''),
            array('data' => 'Quantity'),
            array('data' => 'Unit Cost'),
            array('data' => 'Total'),
          ),
          'style' => array(
            'border-bottom: 1px dotted #dddddd;',
            'line-height: 20px;',
            'color: #4d93ff;',
          ),
        );
      }

      if (isset($packings["value"])) {
        if (!is_array($packings["value"])) {
          $packing = json_decode($packings["value"]);
        }
        else {
          $packing = (object) $packings["value"];
        }
        if ($packing && property_exists($packing, 'packings')) {
          $packings = $packing->packings;
          foreach ($packings as $packing) {
            $packing = (object) $packing;
            if ($is_flat_rate || $is_long_distance) {
              if (!isset($packing->laborRate)) {
                $packing->laborRate = 0;
              }
              $newData = array(
                array(
                  'data' => $packing->name,
                  'style' => array('text-align: left;'),
                ),
                array('data' => $packing->quantity),
                array('data' => '$' . number_format($packing->LDRate, 2)),
              );
              if ($checkShowLaborRate) {
                $packing_total = floatval($packing->quantity) * ($packing->LDRate + $packing->laborRate);
                $newData[] = array('data' => '$' . number_format($packing->laborRate, 2));
              }
              else {
                $packing_total = floatval($packing->quantity) * $packing->LDRate;
              }
              $newData[] = array('data' => '$' . number_format($packing_total, 2));
              $rows[] = array(
                'data' => $newData,
                'style' => array('border-bottom: 1px dotted #dddddd;',
                  'line-height: 20px;',
                  'color: #4d93ff;',
                ),
              );
            }
            else {
              $packing_total = floatval($packing->rate) * floatval($packing->quantity);
              $rows[] = array(
                'data' => array(
                  array(
                    'data' => $packing->name,
                    'style' => array('text-align: left;'),
                  ),
                  array('data' => $packing->quantity),
                  array('data' => '$' . number_format($packing->rate, 2)),
                  array('data' => '$' . number_format($packing_total, 2)),
                ),
                'style' => array(
                  'border-bottom: 1px dotted #dddddd;',
                  'line-height: 20px;',
                  'color: #4d93ff;',
                ),
              );
            }

            $total += $packing_total;
          }
          if (count($packings) > 1) {
            if (($is_flat_rate || $is_long_distance) && $checkShowLaborRate) {
              $rows[] = array(
                'data' => array(
                  array(
                    'data' => 'Total',
                    'style' => array('text-align: left;'),
                  ),
                  array('data' => ''),
                  array('data' => ''),
                  array('data' => ''),
                  array('data' => '$' . number_format($total, 2)),
                ),
                'style' => array('color: #4C4C4C;',
                  'font-weight: 700;',
                  'line-height: 20px;',
                ),
              );
            }
            else {
              $rows[] = array(
                'data' => array(
                  array(
                    'data' => 'Total',
                    'style' => array('text-align: left;'),
                  ),
                  array(
                    'data' => '',
                  ),
                  array(
                    'data' => '',
                  ),
                  array(
                    'data' => '$' . number_format($total, 2),
                  ),
                ),
                'style' => array(
                  'color: #4C4C4C;',
                  'font-weight: 700;',
                  'line-height: 20px;',
                ),
              );
            }
          }
        }
      }

      $table_element = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#empty' => t('Packing table is empty'),
        '#attributes' => array(
          'width' => '100%',
          'style' => array('width: 100%;'),
        ),
      );

      $result = drupal_render($table_element);
    }

    return $result;
  }

  /**
  * @TODO Convert all extraServices to array!
  *
  * @param array $request_retrieve
  *
  * @return string
  */
  private function makeAdditionalServicesTable(array $request_retrieve) : string {
    $result = '';
    $extra_services = isset($request_retrieve['extraServices']) ? (array) $request_retrieve['extraServices'] : array();

    if ($extra_services) {
      $rows = [];
      foreach ($extra_services as $extra_service) {
        $total = 1;
        $extra_service = is_object($extra_service) ? (array) $extra_service : $extra_service;

        foreach ($extra_service['extra_services'] as $item) {
          // @TODO Check it!
          $item = is_object($item) ? (array) $item : $item;
          $total *= $item['services_default_value'];
        }

        $rows[] = array(
          'data' => array(
            array(
              'data' => $extra_service['name'],
              'style' => array('text-align: left;', 'color: #4d93ff;'),
            ),
            array(
              'data' => '$' . number_format($total, 2),
              'style' => array('color: #4d93ff;'),
            ),
          ),
          'style' => array('border-bottom: 1px dotted #dddddd;', 'line-height: 20px;'),
        );
      }

      if (count($extra_services) > 1) {
        $extraServicesCalculator = (new MoveRequestExtraService())->setRequestRetrieve($request_retrieve)->build()->calculate();
        $rows[] = array(
          'data' => array(
            array(
              'data' => 'Total',
              'style' => array('text-align: left;',
                'color: #4C4C4C;',
                'font-weight: 700;',
              ),
            ),
            array(
              'data' => '$' . number_format($extraServicesCalculator->extraServiceTotal, 2),
              'style' => array('color: #4C4C4C;', 'font-weight: 700;'),
            ),
          ),
          'style' => array('line-height: 20px;'),
        );
      }

      $table_element = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#empty' => t('Extra services table is empty'),
        '#attributes' => array(
          'width' => '100%',
          'style' => array('width: 100%;'),
        ),
      );

      $result = drupal_render($table_element);
    }

    return $result;
  }

  /**
   * Get table for inventory.
   *
   * @param mixed $request
   *   Request data.
   * @param bool $with_cf
   *   With cf.
   *
   * @return string
   *   Table html.
   *
   * @throws \Exception
   */
  private function getInventoryTable($request, bool $with_cf = TRUE) {
    // For new inventory.
    if (isset($request['field_request_settings']['inventoryVersion']) && $request['field_request_settings']['inventoryVersion'] == 2) {
      return $this->getNewInventoryTable($request, $with_cf);
    }
    $inventory = new Inventory($request['nid']);
    $rooms = $inventory->rooms;
    $inventory = $inventory->retrieve();

    // Get table for each room.
    $items = function ($inventory, $room_id, $room_name) {
      $rows = [];
      $flag_empty = FALSE;

      $rows[] = array(
        'data' => array(
          array('data' => 'Qty'),
          array('data' => $room_name),
          array('data' => 'CuFt'),
        ),
        'style' => array('border-bottom: 1px solid #A4A4A4;',
          'font-weight: bold;', 'color: #1F1F1F;',
          'padding-bottom: 5px;', 'margin-bottom:5px;',
        ),
      );
      if (!empty($inventory['inventory_list'])) {
        foreach ($inventory['inventory_list'] as $inventory_list) {
          if ($room_id == $inventory_list['rid']) {
            $rows[] = array(
              'data' => array(
                array(
                  'data' => $inventory_list['count'],
                  'style' => array('text-align: left;', 'color: #0E0E0E;'),
                ),
                array(
                  'data' => $inventory_list['title'],
                  'style' => array('text-align: left;', 'color: #0E0E0E;'),
                ),
                array(
                  'data' => $inventory_list['count'] * $inventory_list['cf'],
                  'style' => array('text-align: left;', 'color: #0E0E0E;'),
                ),
              ),
              'style' => array('width: 100%', 'line-height: 20px;'),
            );
            $flag_empty = TRUE;
          }
        }
      }
      $table_element = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#empty' => t('Inventory in this room is empty'),
        '#attributes' => array(
          'width' => '95%',
          'style' => array('width: 95%;'),
        ),
      );
      return $flag_empty ? drupal_render($table_element) : NULL;
    };

    // Calculate total variable.
    $total_items = 0; $total_volume = 0; $total_boxes = 0;
    if (!empty($inventory['inventory_list'])) {
      foreach ($inventory['inventory_list'] as $inventory_list) {
        $total_items += $inventory_list['count'];
        $inventory_list['cubicFeet'] = $inventory_list['cubicFeet'] ?? $inventory_list['cf'] ?? 0;
        $total_volume += $inventory_list['cubicFeet'] * $inventory_list['count'];
        if ($inventory_list['fid'] == 14) {
          $total_boxes += $inventory_list['count'];
        }
      }
    }

    $rows = [];
    $header = array(
      array('data' => "Total items: " . $total_items . " Total Boxes: " . $total_boxes),
      array('data' => "Total Volume/Weight: " . $total_volume . "CuFt / " . $total_volume * 7 . "lbs"),
    );
    $temp_arr = array('data' => array(), 'style' => array('line-height: 20px;'));
    foreach ($rooms as $room_id => $room_name) {
      $res = $items($inventory, $room_id, $room_name);
      if (count($temp_arr['data']) == 2) {
        $rows[] = $temp_arr;
        $temp_arr['data'] = [];
      }
      if ($res != NULL) {
        $temp_arr['data'][] = array(
          'data' => $res,
          'style' => array('text-align: left;', 'color: #0E0E0E;'),
        );
      }
    }

    $rows[] = $temp_arr;
    $table_element = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('Inventory table is empty'),
      '#attributes' => array(
        'width' => '100%',
        'style' => array('width: 100%;'),
      ),
    );

    $result = drupal_render($table_element);
    return $result;
  }

  private function getReadableTime($number, bool $euformat = FALSE) : string {
    $zero = '0';
    $zero2 = '0';
    $minutes = 0;
    $hour = floor($number);
    $fraction = $number - $hour;

    // 0, 15min - 0.25 , 30min - 0.5 , 45 min - 0.75.
    if ($fraction > 0.15 && $fraction < 0.36) {
      $minutes = 15;
    }
    if ($fraction >= 0.36 && $fraction < 0.64) {
      $minutes = 30;
    }
    if ($fraction >= 0.64 && $fraction < 0.87) {
      $minutes = 45;
    }
    if ($fraction >= 0.87) {
      $minutes = 0;
      $hour++;
    }

    if (!$euformat) {
      if ($minutes == 0 && $hour != 1) {
        return $hour . ' hrs';
      }
      elseif ($hour == 1 && $minutes == 0) {
        return $hour . ' hr';
      }
      elseif ($hour == 1 && $minutes != 0) {
        return $hour . ' hr ' . $minutes . ' min';
      }
      elseif ($hour == 0) {
        return $minutes . ' min';
      }
      else {
        return $hour . ' hrs ' . $minutes . ' min';
      }
    }
    else {
      if ($hour > 9) {
        $zero = '';
      }
      if ($minutes > 9) {
        $zero2 = '';
      }
      if ($minutes == 0) {
        return $zero . $hour . ':00';
      }
      elseif ($hour == 0) {
        return '00:' . $minutes;
      }
      else {
        return $zero . $hour . ':' . $zero2 . $minutes;
      }
    }
  }

  public function storageRequestGetVariablesArray(array $storage_request) : array {
    $result = array();

    if (isset($storage_request) && $storage_request) {
      $result['request-id'] = $storage_request['id'];
      $user = explode(' ', $storage_request['user_info']['name']);

      if (isset($user)) {
        $result['fname'] = ($user[0]) ? $user[0] : '';
        $result['lname'] = ($user[1]) ? $user[1] : '';
      }

      $result['client-email'] = $storage_request['user_info']['email'];

      if (isset($storage_request['user_info']['phone1'])) {
        $result['client-phone'] = $storage_request['user_info']['phone1'];
      }
      elseif (isset($storage_request['user_info']['phone2'])) {
        $result['client-phone'] = $storage_request['user_info']['phone2'];
      }

      $company_name = $this->basic_settings->company_name;
      $site_url = $this->basic_settings->website_url;
      $company_phone = $this->basic_settings->company_phone;
      $company_email = $this->basic_settings->company_email;

      $result['site-url'] = $site_url;
      $result['company-name'] = $company_name;
      $result['company-phone'] = $company_phone;
      $result['company-email'] = $company_email;

      $result['storage-statement-url'] = $this->oneTimeHashStorageStatement($storage_request['id'], $storage_request['user_info']['email']);
    }

    return $result;
  }

  /**
   * Method for create storage statement.
   *
   * @param $storage_id
   *   Storage unique id.
   * @param $email
   *   User email.
   *
   * @return string
   *   Storage statement url.
   */
  public function oneTimeHashStorageStatement($storage_id, $email) {
    $client_url = $this->basic_settings->client_page_url;
    $hash_data = (new Clients())->oneTimeHash($email);
    if (!empty($hash_data) && !empty($hash_data['uid'])) {
      $storage_statement_url = $client_url . '/account/#/storage_tenant/' . $storage_id . "?uid={$hash_data['uid']}&timestamp={$hash_data['timestamp']}&hash={$hash_data['hash']}";
    }
    else {
      $storage_statement_url = $client_url . '/account/#/storage_tenant/' . $storage_id;
    }

    return $storage_statement_url;
  }

  private function longDistanceGetVariablesArray(array $long_distance_info) : array {
    if (!empty($long_distance_info)) {
      $result['fname'] = ($long_distance_info['client_name']) ? $long_distance_info['client_name'] : '';
      $result['client-email'] = $long_distance_info['email'];
      $result['client-phone'] = $long_distance_info['phone'];
    }

    $company_name = $this->basic_settings->company_name;
    $site_url = $this->basic_settings->website_url;
    $company_phone = $this->basic_settings->company_phone;
    $company_email = $this->basic_settings->company_email;

    $result['site-url'] = $site_url;
    $result['company-name'] = $company_name;
    $result['company-phone'] = $company_phone;
    $result['company-email'] = $company_email;

    return $result;
  }

  /**
   * Create table for new inventory.
   *
   * @param array $request
   *   Request data.
   * @param bool $with_cf
   *   With cf.
   *
   * @return string
   *   Html.
   *
   * @throws \Exception
   */
  private function getNewInventoryTable(array $request, bool $with_cf) {
    $itemActions = new ItemActions();
    $roomAction = new RoomActions();
    $totals = [
      'total_count' => 0,
      'total_cf' => 0,
      'boxes' => 0,
    ];
    $nid = $request['nid'];

    $requestInventory = $roomAction->getRoomsForRequest(['entity_id' => $nid]);
    // Get table for each room.
    $items = function ($room) use ($itemActions, $nid, &$totals, $with_cf) {
      $totals['total_count'] += $room['total_count'] ?? 0;
      $totals['total_count'] += $room['total_custom_count'] ?? 0;

      $totals['total_cf'] += $room['total_cf'] ?? 0;
      $totals['total_cf'] += $room['total_custom_cf'] ?? 0;

      $totals['boxes'] += $room['boxes'] ?? 0;
      $rows = [];
      $flag_empty = FALSE;
      // Get row for each item.
      $generate_row = function ($count, $name, $cf) use ($with_cf) {
        $rows = array(
          'data' => array(
            array(
              'data' => $count,
              'style' => array('text-align: left;', 'color: #0E0E0E;'),
            ),
            array(
              'data' => $name,
              'style' => array('text-align: left;', 'color: #0E0E0E;'),
            ),
            $with_cf ?
            array(
              'data' => $count * $cf,
              'style' => array('text-align: left;', 'color: #0E0E0E;'),
            ) : [],
          ),
          'style' => array('width: 100%', 'line-height: 20px;'),
        );
        return $rows;
      };

      $rows[] = array(
        'data' => array(
          array('data' => 'Qty'),
          array('data' => $room['name']),
          $with_cf ? array('data' => 'CuFt') : [],
        ),
        'style' => array(
          'border-bottom: 1px solid #A4A4A4;',
          'font-weight: bold;',
          'color: #1F1F1F;',
          'padding-bottom: 5px;',
          'margin-bottom:5px;',
        ),
      );
      $items = $itemActions->getEntityInventory($nid, array('room_id' => $room['room_id']));
      if (!empty($items)) {
        foreach ($items as $item) {
          $rows[] = $generate_row($item['count'], $item['name'], $item['cf']);
        }
      }
      else {
        $flag_empty = TRUE;
      }
      $table_element = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#empty' => t('Inventory in this room is empty'),
        '#attributes' => array(
          'width' => '95%',
          'style' => array('width: 95%;'),
        ),
      );
      return $flag_empty ? NULL : drupal_render($table_element);
    };
    // Logic begin.
    $rows = array();
    $temp_arr = array('data' => array(), 'style' => array('line-height: 20px;'));
    foreach ($requestInventory as $room) {
      $res = $items($room);
      if (count($temp_arr['data']) == 2) {
        $rows[] = $temp_arr;
        $temp_arr['data'] = [];
      }
      if (!empty($res)) {
        $temp_arr['data'][] = array(
          'data' => $res,
          'style' => array('text-align: left;', 'color: #0E0E0E;'),
        );
      }
    }

    if ($request['field_useweighttype']['value'] == 1) {
      $totals['total_cf'] = ($this->getCubicFeet($request))->weight;
    }
    elseif ($request['field_useweighttype']['value'] == 3) {
      $totals['total_cf'] = $request['custom_weight']['value'];
    }

    $header = array(
      array('data' => "Total items: " . $totals['total_count'] . " Total Boxes: " . $totals['boxes']),
      array('data' => "Total Volume/Weight: " . $totals['total_cf'] . "CuFt / " . $totals['total_cf'] * 7 . "lbs"),
    );

    $rows[] = $temp_arr;
    $table_element = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('Inventory table is empty'),
      '#attributes' => array(
        'width' => '100%',
        'style' => array('width: 100%;'),
      ),
    );

    $result = drupal_render($table_element);
    return $result;
  }

  /**
   * Checking for additional user settings to add his to cc field.
   *
   * @param array $params
   *   Params array with additional information for sending email.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  private function checkAdditionalUser(array &$params) : void {
    $entity_check = isset($params['entity_type']) && $params['entity_type'] == 0;

    if (!empty($params['nid'] && $entity_check)) {
      $user_impl = new MoveRequest($params['nid']);
      $user = $user_impl->getAdditionalUser();
      if ($user) {
        $params['cc'][] = $user['mail'];
      }
    }
  }

  /**
   * Calculate commercial transportation.
   *
   * @param array $request_retrieve
   *   Move request retrieve.
   * @param bool $closing
   *   Flag? closed request or not.
   *
   * @return float|int
   *   Transportation value.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function calcCommercialTransportation(array $request_retrieve, bool $closing = FALSE) {
    if ($closing) {
      $commercial_cubic_feet = (new MoveRequest())->getCommercialItemCubicFeet($request_retrieve['request_all_data']['invoice']['commercial_extra_rooms']['value'][0]);
      $rate = $request_retrieve['request_all_data']['invoice']['field_long_distance_rate']['value'] ?? 0;
    }
    else {
      $commercial_cubic_feet = (new MoveRequest())->getCommercialItemCubicFeet($request_retrieve['commercial_extra_rooms']['value'][0]);
      $rate = $request_retrieve['field_long_distance_rate']['value'] ?? 0;
    }

    return $commercial_cubic_feet * $rate;
  }

  /**
   * Attach file to move request close job.
   *
   * @param array $params
   *   Params.
   */
  private function attachFilesCloseJob(array &$params) : void {
    $mpdf_instance = new Mpdf();
    $mpdf_instance->setEntityType(EntityTypes::MOVEREQUEST);
    $mpdf_instance->setEntityId($params['nid']);
    $mpdf_instance->setIsFullPath(FALSE);
    $files = $mpdf_instance->index();
    foreach ($files as $file) {
      $params['files'][] = array(
        'url' => $file['url'],
        'name' => $file['file_name'],
      );
    }
  }

  /**
   * Prepare valuation deductible level.
   *
   * @param array $request_retrieve
   *   Move request data.
   *
   * @return string
   *   Html table.
   */
  private static function prepareValuationDeductibleTable(array $request_retrieve = array()) :string {
    $valuation_data = $request_retrieve['request_all_data']['valuation']['deductible_data'] ?? [];
    $lability_amount = $request_retrieve['request_all_data']['valuation']['selected']['liability_amount'] ?? 0;
    $explanation = $request_retrieve['request_all_data']['valuation']['settings']['explanation'] ?? '';
    $valuation_type = $request_retrieve['request_all_data']['valuation']['selected']['valuation_type'] ?? 1;
    $valuation_charge = $request_retrieve['request_all_data']['valuation']['selected']['valuation_charge'] ?? 0;
    if (!empty($valuation_data)) {
      if (in_array(3, $request_retrieve['field_company_flags']['value'])) {
        $total_estimate = $request_retrieve['request_all_data']['grandTotalRequest']['invoice'] - $valuation_charge;
      }
      else {
        switch ($request_retrieve['service_type']['raw']) {
          case RequestServiceType::FLAT_RATE:
            $total_estimate = $request_retrieve['request_all_data']['grandTotalRequest']['flatrate'] - $valuation_charge;
            break;

          case RequestServiceType::LONG_DISTANCE:
            $total_estimate = $request_retrieve['request_all_data']['grandTotalRequest']['longdistance'] - $valuation_charge;
            break;

          default:
            $total_estimate['min'] = $request_retrieve['request_all_data']['grandTotalRequest']['min'] - $valuation_charge;
            $total_estimate['max'] = $request_retrieve['request_all_data']['grandTotalRequest']['max'] - $valuation_charge;
            break;
        }
      }

      if ($valuation_type == ValuationTypes::PER_POUND) {
        $valuation_title = json_decode(variable_get('valuationSettings', ''))->valuation_titles->{ValuationTypes::PER_POUND}->title;
        $header = 'Basic Value Protection: ' . $valuation_title . ' (included) </br> Full Value Protection Amount of Liability: ' . $lability_amount . '$ (optional)';
      }
      else {
        $header = "Full Value Protection Amount of Liability: " . $lability_amount . '$';
      }
      $header = "<div style=\"text-align: left; padding-bottom: 5px \">" . $header . "</div> ";
      $style = array(
        'border-bottom: 1px solid #A4A4A4;',
        'border-top: 1px solid #A4A4A4;',
        'border-left: 1px solid #A4A4A4;',
        'border-right: 1px solid #A4A4A4;',
        'font-weight: bold;',
        'color: #1F1F1F;',
        'padding-bottom: 5px;',
        'margin-bottom:5px;',
        'width: 10%;',
      );

      $prepared_table = static::prepareTableBody(static::prepareValuationData($valuation_data, $valuation_type, $total_estimate), TRUE);
      $prepared_table = static::styleTableRow($prepared_table, 0, $style);
      $prepared_table = static::styleTableRow($prepared_table, 1, $style);
      $prepared_table = static::styleTableRow($prepared_table, 2, $style);
      $result = $header . static::renderTable($prepared_table) . $explanation;
    }

    return $result ?? '';
  }

  /**
   * Prepare selected valuation title.
   *
   * @param array $valuation_data
   *   Valuation data.
   * @param int $valuation_type
   *   Valuation type.
   * @param mixed $total_estimate
   *   Grand total.
   *
   * @return mixed
   *   Valuation data.
   */
  private static function prepareValuationData(array $valuation_data, int $valuation_type, $total_estimate) {
    $new_valuation_data = $valuation_with_total = [];
    $selected_deductible_level = isset($valuation_data['selected']) ? array_search(1, $valuation_data['selected']) : NULL;
    foreach ($valuation_data as $key => $value) {
      if (strcasecmp($key, 'deductible_level') === 0) {
        array_walk($value, function (&$item1, $key) use ($selected_deductible_level, $valuation_type) {
          if ($key == $selected_deductible_level && $valuation_type == ValuationTypes::FULL_VALUE) {
            $item1 = $item1 . "$ <span style=\"color:red\">(selected)</span>";
          }
          else {
            $item1 = $item1 . '$';
          }
        });
        $new_valuation_data['Deductible level'] = $value;
      }
      if (strcasecmp($key, 'valuation_charge') === 0) {
        foreach ($valuation_data['valuation_charge'] as $charge) {
          if (is_array($total_estimate)) {
            $min = $total_estimate['min'] + $charge;
            $max = $total_estimate['max'] + $charge;
            $valuation_with_total[] = $min . "$ - " . $max . '$';
          }
          else {
            $valuation_with_total[] = $total_estimate + $charge . '$';
          }
        }
        array_walk($value, function (&$item1) {
          $item1 = $item1 . '$';
        });
        $new_valuation_data['Valuation charge'] = $value;
      }
    }
    $new_valuation_data["Total Estimate Plus </br> Valuation charge"] = $valuation_with_total;

    return $new_valuation_data;
  }

  /**
   * Prepare table structure from multidimensional array.
   *
   * @param array $data
   *   Array data.
   * @param bool $useArrayKey
   *   Flag use array keys or not.
   *
   * @return array
   *   Prepared data table.
   */
  private static function prepareTableBody(array $data = array(), bool $useArrayKey = FALSE) {
    $table = [];
    $row = 0;
    foreach ($data as $key => $values) {
      if (!empty($useArrayKey)) {
        // Optional operations.
        array_unshift($values, $key);
      }
      foreach ($values as $key2 => $value) {
        $table[$row][] = array(
          'data' => $value,
        );
      }

      $row += 1;
    }

    return $table;
  }

  /**
   * Method add css styles to table column.
   *
   * @param array $table
   *   Table data.
   * @param int|null $col
   *   Number column.
   * @param int|null $row
   *   Number table row.
   * @param array $styles
   *   Css styles. Example css styles array:
   *   array('text-align: left;', 'color: #FF0000;').
   *
   * @return array
   *   Table.
   */
  private static function styleTableCeil(array $table = array(), int $col = NULL, int $row = NULL, array $styles = array()) {
    return $table[$col][$row]['style'] = $styles;
  }

  /**
   * Method add css styles to table column.
   *
   * @param array $table
   *   Table data.
   * @param int|null $col
   *   Number table column.
   * @param array $styles
   *   Css styles.
   *
   * @return array
   *   Table.
   */
  private static function styleTableCol(array $table = array(), int $col = NULL, array $styles = []) {
    foreach ($table as $key => $rows) {
      foreach ($rows as $key2 => $value) {
        if ($key2 == $col) {
          $table[$key][$key2]['style'] = $styles;
        }
      }
    }

    return $table;
  }

  /**
   * Method add css styles to table row.
   *
   * @param array $table
   *   Table data.
   * @param int|null $row
   *   Number table row.
   * @param array $styles
   *   Css styles.
   *
   * @return array
   *   Table.
   */
  private static function styleTableRow(array $table = array(), int $row = NULL, array $styles = []) {
    foreach ($table as $key => $rows) {
      foreach ($rows as $key2 => $value) {
        if ($key == $row) {
          $table[$key][$key2]['style'] = $styles;
        }
      }
    }

    return $table;
  }

  /**
   * Render html table.
   *
   * @param array $table
   *   Table data.
   * @param array $header
   *   Header data.
   *
   * @return string
   *   Html string data.
   */
  private static function renderTable(array $table = array(), array $header = []) {
    $table_element = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $table,
      '#empty' => t('Table empty'),
      '#attributes' => array(
        'width' => '100%',
        'style' => array('width: 100%;'),
      ),
    );

    $result = drupal_render($table_element);

    return $result;
  }

}
