<?php

use Drupal\move_global_search\Services\Search;
use Drupal\move_services_new\Services\Receipt;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Services\move_request\MoveRequest;

function move_global_search_solr_index() {
  $form = array();

  $linklist = array(
    'item1' => array('title' => 'Execute all reindexation Requests by batch', 'href' => 'admin/config/custom/solr/execute'),
    'item4' => array('title' => 'Execute all reindexation Receipts by batch', 'href' => 'admin/config/custom/solr/execute_receipt'),
    'item5' => array('title' => 'Execute all reindexation Storage Requests by batch', 'href' => 'admin/config/custom/solr/execute_storage_request'),
    'item6' => array('title' => 'Execute all reindexation Storages by batch', 'href' => 'admin/config/custom/solr/execute_storage'),
    'item7' => array('title' => 'Execute all reindexation Trips by batch', 'href' => 'admin/config/custom/solr/execute_trip'),
    'item2' => array('title' => 'Execute all reindexation by cron', 'href' => 'admin/config/custom/solr/cron'),
  );
  $form['links'] = array(
    '#markup' => theme('links', array('links' => $linklist)),
  );

  return $form;
}

function move_global_search_solr_index_execute_cron() {
  $default_variable = array(
    EntityTypes::MOVEREQUEST => 0,
    EntityTypes::STORAGEREQUEST => 0,
    EntityTypes::STORAGE => 0,
    EntityTypes::RECEIPT => 0,
    EntityTypes::LDREQUEST => 0,
  );
  variable_set('solr_indexation', $default_variable);
  drupal_set_message('Solr index was recreated');
  drupal_goto('admin/config/custom/solr');
}

function move_global_search_solr_index_execute($form, &$form_state) {
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Start'),
  );

  return $form;
}

function move_global_search_solr_index_execute_submit($form, &$form_state) {
  // @todo Only move_request.
  $operations = array();
  $result = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('n.type', 'move_request')
    ->execute();

  $instance = new Search(EntityTypes::MOVEREQUEST);
  $instance->clearIndex();

  foreach ($result as $item) {
    $operations[] = array('move_global_search_solr_batch_execute', array($item->nid));
  }

  $batch = array(
    'operations' => $operations,
    'finished' => 'move_global_search_solr_batch_finished',
    'file' => drupal_get_path('module', 'move_global_search') . '/move_global_search.admin.inc',
  );

  batch_set($batch);
}

function move_global_search_solr_index_execute_receipt($form, &$form_state) {
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Start'),
  );

  return $form;
}

function move_global_search_solr_index_execute_receipt_submit($form, &$form_state) {
  $operations = array();
  $result = Receipt::getAllIndexReceipt();
  $instance = new Search(EntityTypes::RECEIPT);
  $instance->clearIndex();

  foreach ($result as $receipt_id) {
    $operations[] = array('move_global_search_solr_receipt_batch_execute', array($receipt_id));
  }

  $batch = array(
    'operations' => $operations,
    'file' => drupal_get_path('module', 'move_global_search') . '/move_global_search.admin.inc',
  );

  batch_set($batch);
}

function move_global_search_solr_index_execute_trip($form, &$form_state) {
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Start'),
  );

  return $form;
}

function move_global_search_solr_index_execute_trip_submit($form, &$form_state) {}

function move_global_search_solr_batch_execute($nid, &$context) {
  $instance = new Search(EntityTypes::MOVEREQUEST);
  $instance->create(node_load($nid));
}

function move_global_search_solr_receipt_batch_execute($receipt_id, &$context) {
  $receipt = Receipt::getReceiptById($receipt_id);
  $receipt['id'] = $receipt_id;
  $receipt['entity_id'] = Receipt::getReceiptEntityId($receipt_id);
  $receipt['entity_type'] = Receipt::getReceiptType($receipt_id);
  $instance = new Search(EntityTypes::RECEIPT);
  $instance->create($receipt);
}

function move_global_search_solr_trip_batch_execute($trip_id, &$context) {}

/**
 * Batch finish callback.
 */
function move_global_search_solr_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Indexed' . count($results['titles']) . ' requests:' . theme('item_list', array('items' => $results['titles'])));
  }
  else {
    drupal_set_message('Ended with errors', 'error');
  }
}
