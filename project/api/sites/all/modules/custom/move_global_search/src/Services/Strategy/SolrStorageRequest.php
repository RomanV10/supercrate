<?php

namespace Drupal\move_global_search\Services\Strategy;

use Drupal\move_services_new\Util\enum\EntityTypes;

/**
 * Class StorageRequestInterface.
 *
 * @package Drupal\move_global_search\Services
 */
class SolrStorageRequest implements SearchInterface {
  use SearchFields;

  /**
   * SolrStorageRequest constructor.
   */
  public function __construct() {
    $this->setEntityType(EntityTypes::STORAGEREQUEST);
    $this->endPointPath = '/solr/requeststorage';
  }

  /**
   * @inheritdoc
   */
  public function prepareObject(array $data) {
    if (isset($data['entity_id'])) {
      $this->setEntityId((int) $data['entity_id']);
      $this->entityObject['entity_id'] = $this->entityId;
      $this->entityObject['address'] = isset($data['user_info']['address']) ? $data['user_info']['address'] : '';
      $this->entityObject['zip'] = isset($data['user_info']['zip']) ? $data['user_info']['zip'] : '';
      $this->entityObject['name'] = isset($data['user_info']['name']) ? $data['user_info']['name'] : '';
      $this->entityObject['city'] = isset($data['user_info']['city']) ? $data['user_info']['city'] : '';
      $this->entityObject['phone1'] = isset($data['user_info']['phone1']) ? $data['user_info']['phone1'] : '';
      $this->entityObject['phone2'] = isset($data['user_info']['phone2']) ? $data['user_info']['phone2'] : '';
      $this->entityObject['email'] = isset($data['user_info']['email']) ? $data['user_info']['email'] : '';
      $this->entityObject['location'] = isset($data['rentals']['location']) ? $data['rentals']['location'] : '';
    }
  }

  /**
   * @inheritdoc
   */
  public static function dismaxFields() : array {
    return array(
      'location' => 'location',
      'email' => 'email^1.1',
      'phone1' => 'phone1',
      'phone2' => 'phone2',
      'city' => 'city',
      'name' => 'name^1.5',
      'zip' => 'zip^2',
      'address' => 'address',
    );
  }

}
