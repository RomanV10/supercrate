<?php

namespace Drupal\move_global_search\Services\Strategy;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\EntityTypes;

/**
 * Class SearchMoveRequest.
 *
 * @package Drupal\move_global_search\Services
 */
class SolrMoveRequest implements SearchInterface {
  use SearchFields;

  public function prepareObject(array $data) {
    if (isset($data['nid'])) {
      $this->setEntityId((int) $data['nid']);
      $request_instance = new MoveRequest();
      $prepare_moving = function ($moving) : string {
        $result = "";
        $result .= isset($moving['locality']) ? "{$moving['locality']} " : '';
        $result .= isset($moving['administrative_area']) ? "{$moving['administrative_area']} " : '';
        $result .= isset($moving['thoroughfare']) ? "{$moving['thoroughfare']} " : '';
        $result .= isset($moving['postal_code']) ? "{$moving['postal_code']} " : '';
        $result .= isset($moving['premise']) ? "{$moving['premise']}" : '';
        return $result;
      };

      $node = $request_instance->getNode($this->entityId);
      $object = array();
      $object['nid'] = $node['nid'];
      $object['email'] = $node['email'];
      $object['name'] = "{$node['first_name']} {$node['last_name']}";
      $object['phone'] = isset($node['phone']) ? $node['phone'] : '';
      $object['additional_phone'] = isset($node['field_additional_phone']) ? $node['field_additional_phone'] : '';
      $object['user_created'] = isset($node['user_created']) ? $node['user_created'] : '';
      $object['moving_from'] = $prepare_moving($node['field_moving_from']);
      $object['moving_to'] = $prepare_moving($node['field_moving_to']);
      $object['field_commercial_company_name'] = $node['field_commercial_company_name']['value'] ?? '';
      $this->setEntityObject($object);
    }
  }

  /**
   * @inheritdoc
   */
  public static function dismaxFields() : array {
    return array(
      'nid' => 'nid^2',
      'name' => 'name^1.5',
      'phone' => 'phone',
      'additional_phone' => 'additional_phone',
      'email' => 'email^1.1',
      'moving_from' => 'moving_from',
      'moving_to' => 'moving_to',
      'field_commercial_company_name' => 'field_commercial_company_name',
    );
  }

}
