<?php

namespace Drupal\move_global_search\Cron\Queues;

use Drupal\move_global_search\Services\Search;
use Drupal\move_services_new\Cron\Queues\QueueInterface;
use Drupal\move_services_new\Services\Receipt;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_storage\Services\RequestStorage;
use Drupal\move_storage\Services\Storage;

/**
 * Class SolarIndexation.
 *
 * @package Drupal\move_global_search\Queues
 */
class SolarIndexation implements QueueInterface {

  /**
   * Queue worker callback function.
   *
   * @param mixed $data
   *   A portion with data.
   */
  public static function execute($data): void {
    if (isset($data['type']) && isset($data['ids'])) {
      $ids = $data['ids'];
      switch ($data['type']) {
        case EntityTypes::MOVEREQUEST:
          self::moveGlobalSearchSolarQueueMoveRequestCreate($ids);
          break;

        case EntityTypes::STORAGEREQUEST:
          self::moveGlobalSearchSolarQueueRequestStorage($ids);
          break;

        case EntityTypes::STORAGE:
          self::moveGlobalSearchSolarQueueStorage($ids);
          break;

        case EntityTypes::RECEIPT:
          self::moveGlobalSearchSolarQueueReceipt($ids);
          break;

        case EntityTypes::LDREQUEST:
          self::moveGlobalSearchSolarQueueTrip($ids);
          break;
      }
    }

  }

  /**
   * Callback for indexation queue move request.
   *
   * @param array $nids
   *   Nids.
   */
  private static function moveGlobalSearchSolarQueueMoveRequestCreate(array $nids) {
    $instance = new Search(EntityTypes::MOVEREQUEST);
    $create = function ($nid) use ($instance) {
      $instance->create(node_load($nid));
    };

    array_walk($nids, $create);
  }

  /**
   * Callback for indexation queue request storage.
   *
   * @param array $entity_ids
   *   Ids.
   */
  private static function moveGlobalSearchSolarQueueRequestStorage(array $entity_ids) {
    $instance_search = new Search(EntityTypes::STORAGEREQUEST);
    $create = function ($entity_id) use ($instance_search) {
      $requestStorage = new RequestStorage($entity_id, FALSE);
      $data = $requestStorage->retrieve();
      $data['entity_id'] = $entity_id;
      $instance_search->create($data);
    };

    array_walk($entity_ids, $create);
  }

  /**
   * Callback for indexation queue storage.
   *
   * @param array $entity_ids
   *   Entity ids.
   */
  private static function moveGlobalSearchSolarQueueStorage(array $entity_ids) {
    $instance_search = new Search(EntityTypes::STORAGE);
    $create = function ($entity_id) use ($instance_search) {
      $instance_stor = new Storage($entity_id);
      $data = $instance_stor->retrieve();
      $data['entity_id'] = $entity_id;
      $instance_search->create($data);
    };

    array_walk($entity_ids, $create);
  }

  /**
   * Callback for indexation queue receipt.
   *
   * @param array $ids
   *   Ids.
   */
  private static function moveGlobalSearchSolarQueueReceipt(array $ids) {
    $create = function ($id) {
      $instance_search = new Search(EntityTypes::RECEIPT, array(), $id);
      $data = Receipt::getReceiptById($id);
      $data['id'] = $id;
      $data['entity_id'] = Receipt::getReceiptEntityId($id);
      $data['entity_type'] = Receipt::getReceiptType($id);
      $instance_search->create($data);
    };

    array_walk($ids, $create);
  }

  /**
   * Callback for indexation queue trip.
   *
   * @param array $ids
   *   Ids.
   */
  private static function moveGlobalSearchSolarQueueTrip(array $ids) {
    /*foreach ($ids as $id) {
    $instance_search = new Search(EntityTypes::LDREQUEST, array(), $id);
    $trip_instance = new LongDistanceTrip($id);
    $trip = $trip_instance->retrieve();
    $trip['entity_id'] = $id;
    $instance_search->create($trip);
    }*/
  }

}
