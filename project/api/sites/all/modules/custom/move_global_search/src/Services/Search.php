<?php

namespace Drupal\move_global_search\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Sales;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Solarium\QueryType\Select\Result\Result as ResultQuery;
use Solarium\Client;
use Drupal\move_global_search\Services\Strategy\SolrMoveRequest;
use Drupal\move_global_search\Services\Strategy\SolrStorage;
use Drupal\move_global_search\Services\Strategy\SolrStorageRequest;
use Drupal\move_global_search\Services\Strategy\SolrTrip;
use Drupal\move_global_search\Services\Strategy\SolrReceipt;

/**
 * Class Search.
 *
 * @package Drupal\move_global_search\Services
 */
class Search extends BaseService {

  /**
   * @var Object
   */
  private $user;
  private $client;
  private $config;
  private $query;
  private $dismax;
  private $entityId = NULL;
  private $entityObject;
  private $entityType = EntityTypes::MOVEREQUEST;

  public function __construct($entity_type = NULL, $entity_object = array(), $id = NULL) {
    global $user;

    $this->entityObject = $entity_object;
    $this->entityId = $id;
    $this->user = $user;

    if (EntityTypes::isValidValue($entity_type)) {
      $this->entityType = $entity_type;
    }

    $this->client = new Client();
    $this->client->setAdapter('Solarium\Core\Client\Adapter\Http');
    $this->config = array(
      'endpoint' => array(
        'localhost' => array(
          'host' => 'solr',
          'port' => 8983,
          'path' => '/solr/',
        ),
      ),
    );
    $this->client->setOptions($this->config);
    $this->query = $this->client->createSelect();
    $this->dismax = $this->query->getDisMax();
  }

  public function create($data = array()) {
    try {
      $this->prepareObject($data);
      if ($this->entityObject) {
        $this->setFieldTypes($this->entityType);
        $update = $this->client->createUpdate();
        $doc = $update->createDocument($this->entityObject);
        $update->addDocument($doc)->addCommit();
        $this->client->update($update);
      }
      else {
        throw new SolrCreateException();
      }
    }
    catch (\Throwable $e) {
      $this->saveLog($e);
    }
  }

  private function saveLog($e) {
    watchdog('Solr Indexation', "<pre>:log</pre>", array(":log" => print_r($e, TRUE)), WATCHDOG_ERROR);
  }

  public function retrieve() {}

  public function update($data = array()) {
    // Solr server himself decides inserting or updating document.
    $this->create($data);
  }

  public function delete() {
    $this->setFieldTypes($this->entityType);
    $update = $this->client->createUpdate();
    $update->addDeleteById($this->entityId);
    $update->addCommit();
    return $this->client->update($update);
  }

  public function index(string $value) {
    $result = array();
    $constants = array(
      EntityTypes::MOVEREQUEST,
      EntityTypes::STORAGEREQUEST,
      EntityTypes::STORAGE,
      EntityTypes::RECEIPT,
      EntityTypes::LDREQUEST,
    );

    foreach ($constants as $constant) {
      $this->setFieldTypes($constant, $value);
      $this->query->setQuery($value);
      $this->query->setStart(0);
      $this->query->setRows(6);
      $resultset = $this->executeSearch();
      foreach ($resultset as $document) {
        $permission = TRUE;
        $document = reset($document);
        switch ($constant) {
          case EntityTypes::MOVEREQUEST:
            $permission = (new Sales())->checkUserPermRequest($document['nid'], array('administrator'), 'canSearchOtherLeads');
            break;

          case EntityTypes::RECEIPT:
            if ($document['entity_type'] == EntityTypes::MOVEREQUEST) {
              $permission = (new Sales())->checkUserPermRequest($document['entity_id'], array('administrator'), 'canSearchOtherLeads');
            }
            break;
        }

        if ($permission) {
          $result[$constant][] = $document;
        }
      }
    }

    return $result;
  }

  /**
   * Execute request to Solr server.
   *
   * @return ResultQuery
   *   Result of operation.
   */
  private function executeSearch() : ResultQuery {
    return $this->client->select($this->query);
  }

  /**
   * Set dismax list of query fields.
   *
   * @param int $type
   *   The entity type.
   */
  private function setFieldTypes(int $type, string $value = '') {
    $qf = array();
    $skip = FALSE;
    switch ($type) {
      case EntityTypes::MOVEREQUEST:
        $instance = new SolrMoveRequest();
        $qf = SolrMoveRequest::dismaxFields();
        $this->config['endpoint']['localhost']['path'] = $instance->getEndPoint();
        break;

      case EntityTypes::STORAGEREQUEST:
        $instance = new SolrStorageRequest();
        $qf = SolrStorageRequest::dismaxFields();
        $this->config['endpoint']['localhost']['path'] = $instance->getEndPoint();
        break;

      case EntityTypes::STORAGE:
        $instance = new SolrStorage();
        $qf = SolrStorage::dismaxFields();
        $this->config['endpoint']['localhost']['path'] = $instance->getEndPoint();
        break;

      case  EntityTypes::RECEIPT:
        $instance = new SolrReceipt();
        $qf = SolrReceipt::dismaxFields();
        $this->config['endpoint']['localhost']['path'] = $instance->getEndPoint();
        break;

      case EntityTypes::LDREQUEST:
        $instance = new SolrTrip();
        $qf = SolrTrip::dismaxFields();
        $this->config['endpoint']['localhost']['path'] = $instance->getEndPoint();
        break;

      default:
        $skip = TRUE;

    }
    if (!$skip) {
      $this->client->setOptions($this->config);
      if ($qf) {
        $this->changeRateDismax($value, $qf);
        $this->dismax->setQueryFields(implode(' ', $qf));
        $this->dismax->setTie(0.1);
      }
    }
  }

  private function changeRateDismax(string $value, array &$qf) {
    // Increase phone rate.
    $phone = function (&$item, $key) {
      if (preg_match("/phone/", $key)) {
        $item = "$key^2.1";
      }
    };

    // Increase email rate.
    if (strpos($value, '@') !== FALSE) {
      $qf['email'] = 'email^3.0';
    }
    elseif (is_numeric($value) && strlen((string) $value) > 5) {
      array_walk($qf, $phone);
    }
  }

  private function prepareObject($object) {
    $instance = NULL;

    switch ($this->entityType) {
      case EntityTypes::MOVEREQUEST:
        $instance = new SolrMoveRequest();
        break;

      case EntityTypes::STORAGEREQUEST:
        $instance = new SolrStorageRequest();
        break;

      case EntityTypes::STORAGE:
        $instance = new SolrStorage();
        break;

      case EntityTypes::RECEIPT:
        $instance = new SolrReceipt();
        break;

      case EntityTypes::LDREQUEST:
        $instance = new SolrTrip();
    }

    if ($instance) {
      $instance->prepareObject((array) $object);
      $this->entityId = $instance->getEntityId();
      $this->entityType = $instance->getEntityType();
      $this->entityObject = $instance->getEntityObject();
    }
  }

  public function clearIndex() {
    $this->setFieldTypes($this->entityType);
    $update = $this->client->createUpdate();
    $update->addDeleteQuery('*:*');
    $update->addCommit();
    $this->client->update($update);
  }

}

/**
 * Class SolrCreateException.
 *
 * @package Drupal\move_global_search\Services
 */
class SolrCreateException extends \RuntimeException {}
