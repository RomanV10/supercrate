<?php

namespace Drupal\move_global_search\Services\Strategy;

/**
 * Interface SearchInterface.
 *
 * @package Drupal\move_global_search\Services
 */
interface SearchInterface {

  /**
   * @param array $data
   *   The data of object.
   *
   * @return mixed
   */
  public function prepareObject(array $data);

  /**
   * The setter method for $entityObject.
   *
   * @param array $entity_object
   *   The entity object.
   */
  public function setEntityObject(array $entity_object);

  /**
   * The getter method for $entityObject.
   *
   * @return mixed
   */
  public function getEntityObject();

  /**
   * The setter method for $entityId.
   *
   * @param int $entity_id
   */
  public function setEntityId($entity_id);

  /**
   * The getter method for $entityId.
   *
   * @return mixed
   */
  public function getEntityId();

  /**
   * The getter method for $entityType.
   *
   * @return mixed
   */
  public function getEntityType();

  /**
   * @return array
   */
  public static function dismaxFields() : array;

  /**
   * @return mixed
   */
  public function getEndPoint();

}
