<?php

namespace Drupal\move_global_search\Services\Strategy;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\EntityTypes;

/**
 * Class SearchMembers.
 *
 * @package Drupal\move_global_search\Services
 */
trait SearchFields {

  /**
   * The object of entity.
   *
   * @var array
   */
  private $entityObject = array();

  /**
   * Unique id of entity.
   *
   * @var int
   */
  private $entityId = 0;

  /**
   * The type of entity.
   *
   * @var EntityTypes
   */
  public $entityType;

  /**
   * Solr EndPoint path.
   *
   * @var string
   */
  public $endPointPath;

  public function __construct() {
    $this->setEntityType(EntityTypes::MOVEREQUEST);
    $this->endPointPath = '/solr/moverequest';
  }

  /**
   * The setter method for $entityObject.
   *
   * @param array $entity_object
   *   The entity object.
   */
  public function setEntityObject(array $entity_object) {
    $this->entityObject = $entity_object;
  }

  /**
   * The getter method for $entityObject.
   *
   * @return mixed
   */
  public function getEntityObject() {
    return $this->entityObject;
  }

  /**
   * The setter method for $entityId.
   *
   * @param $entity_id
   */
  public function setEntityId($entity_id) {
    $this->entityId = $entity_id;
  }

  /**
   * The getter method for $entityId.
   *
   * @return mixed
   */
  public function getEntityId() {
    return $this->entityId;
  }

  /**
   * The setter method for $entityType.
   *
   * @param $entity_type
   */
  public function setEntityType($entity_type) {
    $this->entityType = $entity_type;
  }

  /**
   * The getter method for $entityType.
   *
   * @return mixed
   */
  public function getEntityType() {
    return $this->entityType;
  }

  public function getEndPoint() {
    return $this->endPointPath;
  }

}
