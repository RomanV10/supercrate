<?php

namespace Drupal\move_global_search\System;

use Drupal\move_global_search\Services\Search;

/**
 * Class Service.
 *
 * @package Drupal\move_global_search\System
 */
class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() : array {
    return array(
      'move_global_search' => array(
        'operations' => array(
          'index' => array(
            'callback' => 'Drupal\move_global_search\System\Service::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_global_search',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'value',
                'optional' => FALSE,
                'type' => 'string',
                'source' => array('param' => 'value'),
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  public static function index(string $value) {
    $instance = new Search();
    return $instance->index($value);
  }

}
