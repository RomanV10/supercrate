<?php

namespace Drupal\move_global_search\Cron\Tasks;

use Drupal\move_global_search\Services\Search;
use Drupal\move_services_new\Cron\Tasks\CronInterface;
use Drupal\move_services_new\Services\Receipt;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_storage\Services\RequestStorage;
use Drupal\move_storage\Services\Storage;

/**
 * Class SolarIndexation.
 *
 * @package Drupal\move_global_search\Cron
 */
class PrepareSolarData implements CronInterface {

  /**
   * Execute task by queue.
   */
  public static function execute(): void {
    $default_variable = array(
      EntityTypes::MOVEREQUEST => 0,
      EntityTypes::STORAGEREQUEST => 0,
      EntityTypes::STORAGE => 0,
      EntityTypes::RECEIPT => 0,
      EntityTypes::LDREQUEST => 0,
    );
    $variable_cron = variable_get('solr_indexation', $default_variable);

    foreach ($variable_cron as $entity_type => &$value) {
      if (!$value) {
        $instance = new Search($entity_type);
        $instance->clearIndex();
        $queue = \DrupalQueue::get('move_global_search_solar_indexation');
        $queue->createQueue();

        switch ($entity_type) {
          case EntityTypes::MOVEREQUEST:
            self::moveGlobalSearchCronAllRequests($queue);
            break;

          case EntityTypes::STORAGEREQUEST:
            self::moveGlobalSearchCronAllRequestsStorage($queue);
            break;

          case EntityTypes::STORAGE:
            self::moveGlobalSearchCronAllStorage($queue);
            break;

          case EntityTypes::RECEIPT:
            self::moveGlobalSearchCronAllReceipts($queue);
            break;
        }

        $value = 1;
      }
    }

    variable_set('solr_indexation', $variable_cron);
  }

  /**
   * Set all move request queues.
   *
   * @param \SystemQueue $queue
   *   Queue.
   */
  private static function moveGlobalSearchCronAllRequests(\SystemQueue $queue) {
    $result = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('n.type', 'move_request')
      ->execute()
      ->fetchCol();

    $prepare_queue = function ($nids) use ($queue) {
      $args = array();
      $args['type'] = EntityTypes::MOVEREQUEST;
      $args['ids'] = $nids;
      $queue->createItem($args);
    };
    $data = array_chunk($result, 25, TRUE);
    array_walk($data, $prepare_queue);
  }

  /**
   * Set all request storage queues.
   *
   * @param \SystemQueue $queue
   *   Queue.
   */
  private static function moveGlobalSearchCronAllRequestsStorage(\SystemQueue $queue) {
    $requestStorage = new RequestStorage();
    if ($data = $requestStorage->index(0)) {
      $ids = self::arrayValueRecursive('id', $data);
      $prepare_queue = function ($entity_ids) use ($queue) {
        $args = array();
        $args['type'] = EntityTypes::STORAGEREQUEST;
        $args['ids'] = $entity_ids;
        $queue->createItem($args);
      };
      $chunk = array_chunk($ids, 25, TRUE);
      array_walk($chunk, $prepare_queue);
    }
  }

  /**
   * Set all storage queues.
   *
   * @param \SystemQueue $queue
   *   Queue.
   */
  private static function moveGlobalSearchCronAllStorage(\SystemQueue $queue) {
    $instance_reqstor = new Storage();
    if ($data = $instance_reqstor->index(0)) {
      $ids = self::arrayValueRecursive('id', $data);
      $prepare_queue = function ($entity_ids) use ($queue) {
        $args = array();
        $args['type'] = EntityTypes::STORAGE;
        $args['ids'] = $entity_ids;
        $queue->createItem($args);
      };
      $chunk = array_chunk($ids, 25, TRUE);
      array_walk($chunk, $prepare_queue);
    }
  }

  /**
   * Set all receipts queues.
   *
   * @param \SystemQueue $queue
   *   Queue.
   */
  private static function moveGlobalSearchCronAllReceipts(\SystemQueue $queue) {
    $result = Receipt::getAllIndexReceipt();
    $prepare_queue = function ($ids) use ($queue) {
      $args = array();
      $args['type'] = EntityTypes::RECEIPT;
      $args['ids'] = $ids;
      $queue->createItem($args);
    };
    $data = array_chunk($result, 25, TRUE);
    array_walk($data, $prepare_queue);
  }

  /**
   * Helper function for extract id entities.
   *
   * @param int $key
   *   Key.
   * @param array $arr
   *   Array.
   *
   * @return array|mixed
   *   Result.
   */
  private static function arrayValueRecursive(int $key, array $arr) {
    $val = array();
    array_walk_recursive($arr, function ($v, $k) use ($key, &$val) {
      if ($k == $key) {
        array_push($val, $v);
      }
    });
    return count($val) > 1 ? $val : array_pop($val);
  }

}
