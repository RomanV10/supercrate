<?php

namespace Drupal\move_global_search\Services\Strategy;

use Drupal\move_services_new\Util\enum\EntityTypes;

/**
 * Class SolrStorage.
 *
 * @package Drupal\move_global_search\Services
 */
class SolrStorage implements SearchInterface {
  use SearchFields;

  public function __construct() {
    $this->setEntityType(EntityTypes::STORAGE);
    $this->endPointPath = '/solr/storage';
  }

  /**
   * @inheritdoc
   */
  public function prepareObject(array $data) {
    if (isset($data['entity_id'])) {
      $this->setEntityId((int) $data['entity_id']);
      $this->entityObject['entity_id'] = $this->entityId;
      $this->entityObject['address'] = isset($data['address']) ? $data['address'] : '';
      $this->entityObject['city'] = isset($data['city']) ? $data['city'] : '';
      $this->entityObject['email'] = isset($data['email']) ? $data['email'] : '';
      $this->entityObject['fax'] = isset($data['fax']) ? $data['fax'] : '';
      $this->entityObject['phone1'] = isset($data['phone1']) ? $data['phone1'] : '';
      $this->entityObject['phone2'] = isset($data['phone2']) ? $data['phone2'] : '';
      $this->entityObject['zip'] = isset($data['zip']) ? $data['zip'] : '';
    }
  }

  /**
   * @inheritdoc
   */
  public static function dismaxFields() : array {
    return array(
      'address' => 'address',
      'city' => 'city',
      'email' => 'email^1.1',
      'fax' => 'fax',
      'phone1' => 'phone1',
      'phone2' => 'phone2',
      'zip' => 'zip^2',
    );
  }

}
