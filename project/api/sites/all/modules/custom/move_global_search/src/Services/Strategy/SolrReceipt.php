<?php

namespace Drupal\move_global_search\Services\Strategy;

use Drupal\move_services_new\Util\enum\EntityTypes;

/**
 * Class SolrReceipt.
 *
 * @package Drupal\move_global_search\Services
 */
class SolrReceipt implements SearchInterface {
  use SearchFields;

  /**
   * SolrReceipt constructor.
   */
  public function __construct() {
    $this->setEntityType(EntityTypes::RECEIPT);
    $this->endPointPath = '/solr/receipts';
  }

  /**
   * @inheritdoc
   */
  public function prepareObject(array $data) {
    if (isset($data['id'])) {
      $this->setEntityId((int) $data['id']);
      $this->entityObject['id'] = $this->entityId;
      $this->entityObject['entity_id'] = $data['entity_id'];
      $this->entityObject['entity_type'] = $data['entity_type'];
      if (isset($data['account_number']) || isset($data['ccardN'])) {
        $this->entityObject['card_number'] = isset($data['account_number']) ? $data['account_number'] : $data['ccardN'];
      }
      $this->entityObject['card_type'] = isset($data['card_type']) ? $data['card_type'] : '';
      $this->entityObject['description'] = isset($data['description']) ? $data['description'] : '';
      $this->entityObject['email'] = isset($data['email']) ? $data['email'] : '';
      // Old version receipt's.
      if (isset($data['first_name']) && isset($data['last_name'])) {
        $this->entityObject['name'] = $data['first_name'] . ' ' . $data['last_name'];
      }
      elseif (isset($object['name'])) {
        $this->entityObject['name'] = $data['name'];
      }
      $this->entityObject['zip'] = isset($data['zip_code']) ? $data['zip_code'] : '';
      $this->entityObject['date'] = isset($data['date']) ? $this->prepareDate($data['date']) : '';
    }
  }

  /**
   * @inheritdoc
   */
  public static function dismaxFields() : array {
    return array(
      'id' => 'id',
      'card_code' => 'card_code',
      'card_number' => 'card_number^2.5',
      'card_type' => 'card_type',
      'description' => 'description',
      'email' => 'email^1.1',
      'exp_date' => 'exp_date',
      'name' => 'name^1.5',
      'phone' => 'phone',
      'zip' => 'zip^2',
    );
  }

  private function prepareDate($date) {
    $string_date = strtotime($date);
    return date('Y-m-d\TH:i:s\Z', $string_date);
  }

}
