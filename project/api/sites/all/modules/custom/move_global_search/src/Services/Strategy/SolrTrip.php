<?php

namespace Drupal\move_global_search\Services\Strategy;

use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\long_distance\Services\LongDistanceCarrier;

/**
 * Class SolrTrip.
 *
 * @package Drupal\move_global_search\Services
 */
class SolrTrip implements SearchInterface {
  use SearchFields;

  /**
   * SolrTrip constructor.
   */
  public function __construct() {
    $this->setEntityType(EntityTypes::LDREQUEST);
    $this->endPointPath = '/solr/trip';
  }

  /**
   * @inheritdoc
   */
  public function prepareObject(array $trip) {
    if (isset($trip['entity_id'])) {
      $this->setEntityId((int) $trip['entity_id']);
      $this->entityObject['entity_id'] = $this->entityId;

      if (isset($trip['carrier'])) {
        $carrier_instance = new LongDistanceCarrier($trip['carrier']);
        $carrier = $carrier_instance->retrieve();
        $this->entityObject['email_carrier'] = $carrier['email'];
        $this->entityObject['city_carrier'] = $carrier['city'];
        $this->entityObject['name'] = $carrier['name'];
        $this->entityObject['phone_number1'] = $carrier['phone_number_1'];
        $this->entityObject['phone_number2'] = $carrier['phone_number_2'];
        $this->entityObject['phone_number3'] = $carrier['phone_number_3'];
        $this->entityObject['phone_number4'] = $carrier['phone_number_4'];
      }

      $this->entityObject['driver_name'] = isset($trip['driver_name']) ? $trip['driver_name'] : '';
      $this->entityObject['driver_phone'] = isset($trip['driver_phone']) ? $trip['driver_phone'] : '';
      $this->entityObject['jobs'] = $this->tripJobs($trip);
    }
  }

  /**
   * @inheritdoc
   */
  public static function dismaxFields() : array {
    return array(
      'email_carrier' => 'email_carrier',
      'city_carrier' => 'city_carrier',
      'name' => 'name',
      'phone_number1' => 'phone_number1',
      'phone_number2' => 'phone_number2',
      'phone_number3' => 'phone_number3',
      'phone_number4' => 'phone_number4',
      'zip' => 'zip',
      'city' => 'city',
      'driver_name' => 'driver_name',
      'driver_phone' => 'driver_phone',
      'jobs' => 'jobs',
    );
  }

  private function tripJobs(array $trip) : array {
    $result = array();
    if (isset($trip['jobs'])) {
      foreach ($trip['jobs'] as $job) {
        $result[] = $job['nid'];
      }
    }

    return $result;
  }

}
