<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_storage\Services\Storage;
use Drupal\move_storage\Services\RequestStorage;
use Drupal\move_storage\Services\RequestStorageCharge;
use Drupal\move_storage\Services\RequestStoragePayment;

class MoveStorageTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new Storage();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createStorage() {
    $file_path = dirname(__FILE__) . '/data/storage.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('storage', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/storageUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json_up = drupal_json_decode($file_content);
    variable_set('storage_update', $parse_json_up['data']);
  }

  public function testCreateStorage() {
    self::createStorage();
    $data = variable_get('storage');

    $storage = $this->fixture->create($data);
    $this->assertGreaterThanOrEqual(1, $storage);
    return $storage;
  }

  /**
   * Test retrieve storage.
   * @depends testCreateStorage
   *
   * @param $storage
   */
  public function testRetriveStorage($storage) {
    $this->fixture->setId($storage);
    $ret = $this->fixture->retrieve();
    $this->assertCount(14, $ret);
}

  /**
   * @depends testCreateStorage
   *
   * @param $storage
   */
  public function testUpdateStorage($storage) {
    $stor_up = variable_get('storage_update');
    $storage_update = new Storage($storage);
    $upd = $storage_update->update($stor_up, TRUE);
    $this->assertEquals(2, $upd);
  }

  /**
   * @depends testCreateStorage
   *
   * @param $storage
   */
//  public function testDeleteStorage($storage) {
//    $storage_delete = new Storage($storage);
//    $del = $storage_delete->delete();
//    var_dump($del);
//    $this->assertEquals(1, $del);
//  }

  public function testStorageIndex($flag) {
    $storage_index = new Storage();
    $upd = $storage_index->index(1);
    $countDefStor = 0;
    foreach ($upd as $key => $value) {
      if ($value['data']['default'] == 1) {
        $countDefStor = $countDefStor + 1;
      }
    }
    $this->assertEquals(1, $countDefStor);
  }

  /**
   * @expectedException ServicesException
   * @depends testCreateStorage
   * @dataProvider providerVariantsUpdate
   * @param $storage
   */
  public function testUpdateStorageException($storage, $id) {
    $id = $id + 1;
    $storage_update = new Storage($id);
    $upd = $storage_update->update($storage, TRUE);
    $this->assertEquals(2, $upd);
  }

  public function providerVariantsUpdate() {
    return array(
      array(
        'data' => array(
          'name' => '1',
          'address' => 'Lenina st.6',
          'zip' => '12323',
          'state' => 'Usa',
          'city' => 'NY',
          'phone1' => '44433322',
          'phone2' => '44433322',
          'fax' => '44433322',
          'email' => 'sdfsdfsd@gmailcom',
          'default' => '1',
          'tax' => '10',
          'rate_per_cuft' => '15',
          'late_fee' => '5',
          'apply_late_fee_in' => '2',
        ),
      ),
      array(
        'data' => array(
          'name' => '1',
          'address' => 'Lenina st.6',
          'zip' => '1232',
          'state' => 'Usa',
          'city' => 'NY',
          'phone1' => '44433322',
          'phone2' => '44433322',
          'fax' => '44433322',
          'email' => 'sdfsdfsd@gmail.com',
          'default' => '1',
          'tax' => '10',
          'rate_per_cuft' => '15',
          'late_fee' => '5',
          'apply_late_fee_in' => '2',
        ),
      ),
    );
  }

  /**
   * @expectedException ServicesException
   * @depends testCreateStorage
   * @dataProvider providerVariantsKeysEx
   * @param $storage
   */
  public function testCreateStorageException($data) {
    $storage_create = new Storage();
    $storage = $storage_create->create($data);
    $this->assertGreaterThanOrEqual(1, $storage);
  }

  public function providerVariantsKeysEx() {
    return array(
      array(
        'data' => array(
          'address' => 'Lenina st.6',
          'zip' => '12323',
          'state' => 'Usa',
          'city' => 'NY',
          'phone1' => '44433322',
          'phone2' => '44433322',
          'fax' => '44433322',
          'email' => 'sdfsdfsd@gmail.com',
          'default' => '1',
          'tax' => '10',
          'rate_per_cuft' => '15',
          'late_fee' => '5',
          'apply_late_fee_in' => '2',
        ),
      ),
      array(
        'data' => array(
          'name' => 'TestUp2',
          'zip' => '12323',
          'state' => 'Usa',
          'city' => 'NY',
          'phone1' => '44433322',
          'phone2' => '44433322',
          'fax' => '44433322',
          'email' => 'sdfsdfsd@gmail.com',
          'default' => '1',
          'tax' => '10',
          'rate_per_cuft' => '15',
          'late_fee' => '5',
          'apply_late_fee_in' => '2',
        ),
      ),
      array(
        'data' => array(
          'name' => 'TestUp3',
          'address' => 'Lenina st.6',
          'state' => 'Usa',
          'city' => 'NY',
          'phone1' => '44433322',
          'phone2' => '44433322',
          'fax' => '44433322',
          'email' => 'sdfsdf@sdgmail.com',
          'default' => '1',
          'tax' => '10',
          'rate_per_cuft' => '15',
          'late_fee' => '5',
          'apply_late_fee_in' => '2',
        ),
      ),
      array(
        'data' => array(
          'name' => 'TestUp4',
          'address' => 'Lenina st.6',
          'zip' => '12323',
          'state' => 'Usa',
          'city' => 'NY',
          'phone2' => '44433322',
          'fax' => '44433322',
          'email' => 'sdfsdf@sdgmail.com',
          'default' => '1',
          'tax' => '10',
          'rate_per_cuft' => '15',
          'late_fee' => '5',
          'apply_late_fee_in' => '2',
        ),
      ),
      array(
        'data' => array(
          'name' => 'TestUp5',
          'address' => 'Lenina st.6',
          'zip' => '12323',
          'state' => 'Usa',
          'city' => 'NY',
          'phone1' => '44433322',
          'phone2' => '44433322',
          'fax' => '44433322',
          'default' => '1',
          'tax' => '10',
          'rate_per_cuft' => '15',
          'late_fee' => '5',
          'apply_late_fee_in' => '2',
        ),
      ),
      array(
        'data' => array(
          'name' => 'TestUp6',
          'address' => 'Lenina st.6',
          'zip' => '12323',
          'state' => 'Usa',
          'city' => 'NY',
          'phone1' => '44433322',
          'phone2' => '44433322',
          'fax' => '44433322',
          'email' => 'sdfsdf@sdgmail.com',
          'default' => '1',
          'tax' => '10',
          'late_fee' => '5',
          'apply_late_fee_in' => '2',
        ),
      ),
    );
  }

}
