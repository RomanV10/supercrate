<?php

use PHPUnit\Framework\TestCase;
use Drupal\move_storage\Services\RequestStorage;
use Drupal\move_invoice\Services\Invoice;

class RequestStorageTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new RequestStorage();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createReqStorage() {
    $file_path = dirname(__FILE__) . '/data/requestStorage.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('request_storage', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/requestStorageUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('request_storage_update', $parse_json['data']);
  }

  public function testCreateReqStor() {
    self::createReqStorage();
    $rs = variable_get('request_storage');

    $rsid = $this->fixture->create($rs);
    $this->assertGreaterThanOrEqual(1, $rs);

    return (int) $rsid;
  }

  /**
   * @depends testCreateReqStor
   */
  public function testRetriveReqStor($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertCount(6, $ret);
    $this->assertArrayHasKey('user_info', $ret);
    $this->assertArrayHasKey('rentals', $ret);
    $this->assertArrayHasKey('recurring', $ret);
  }

  /**
   * @depends testCreateReqStor
   *
   * @param int $rsid.
   */
  public function testUpdateReqStorage($rsid) {
    $rs_up = variable_get('request_storage_update');
    $equal_current_time = time();
    $date = new DateTime();
    $lastDate = $date->setTimestamp(time())->modify('+2 month +1 day')->getTimestamp();
    $rs_up['recurring']['start_date'] = $equal_current_time;
    $rs_up['recurring']['start_point'] = $equal_current_time;
    $rs_up['recurring']['next_run_on'] = $equal_current_time;
    $rs_up['recurring']['next_run_on_fee'] = $equal_current_time;
    $rs_up['recurring']['how_many'] = $lastDate;
    $rs_update = new RequestStorage($rsid);
    $upd = $rs_update->update($rs_up, TRUE);
    $this->assertEquals(2, $upd);
  }

  /**
   * @depends testCreateReqStor
   */
  public function testExecuteCronReqStor($rsid) {
    $date = new DateTime();
    $date->setTimezone(new \DateTimeZone('UTC'));
    $date->setTimestamp(time());

    $invoice = new Invoice();
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result = count($invoice_records);

    // First run cron: "start_date" = current time.
    // "Current date" = yesterday.
    // Cron will be run: no changes in result; count invoices = 0.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate = $date->setTimestamp(time())->modify('-1 day')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result, $result2);

    // Cron will be run and create 1 recurring charge.
    // "Current date" = tomorrow after start date.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate1 = $date->setTimestamp(time())->modify('+1 day')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate1, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 1, $result2);

    // Cron will be run after 2 days.
    // No changes in result.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate2 = $date->setTimestamp(time())->modify('+2 day')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate2, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 1, $result2);

    // Cron will be run after 5 days.
    // Create 1 late fee charge.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate3 = $date->setTimestamp(time())->modify('+5 day')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate3, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 2, $result2);

    // Cron will be run after 7 days.
    // No changes in result.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate4 = $date->setTimestamp(time())->modify('+7 day')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate4, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 2, $result2);

    // Cron will be run after 1 month.
    // Create 1 recurring charge.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate5 = $date->setTimestamp(time())->modify('+1 month')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate5, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 3, $result2);

    // Cron will be run after 1 month and 1 day.
    // No changes in result.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate6 = $date->modify('+1 day')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate6, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 3, $result2);

    // Cron will be run after 1 month and 5 day.
    // Create 1 late fee charge.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate7 = $date->modify('+4 day')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate7, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 4, $result2);

    // Cron will be run after 2 month.
    // Create 1 recurring charge.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate8 = $date->setTimestamp(time())->modify('+2 month')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate8, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 5, $result2);

    // Cron will be run after 2 month and 5 day.
    // Create 1 late fee charge.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate9 = $date->setTimestamp(time())->modify('+2 month +5 day')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate9, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 6, $result2);

    // Cron will be run after 4.
    // No changes in result.
    $allStartRec = (array) RequestStorage::indexStartRec();
    $curDate10 = $date->setTimestamp(time())->modify('+4 month')->getTimestamp();
    var_dump($date->format('Y-m-d H:i:s'));
    $this->fixture->executeCronCharge($allStartRec, $curDate10, 'day');
    $invoice_records = $invoice->getInvoiceForEntity($rsid, 1);
    $result2 = count($invoice_records);
    $this->assertEquals($result + 6, $result2);
  }

  /**
   * @depends testCreateReqStor
   */
  public function testEndRecurring($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertArrayHasKey('start', $ret['recurring']);
    $this->assertEquals(0, $ret['recurring']['start']);
  }

}
