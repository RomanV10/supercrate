<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_storage\Services\Storage;
use Drupal\move_storage\Services\RequestStorage;
use Drupal\move_storage\Services\RequestStorageCharge;
use Drupal\move_storage\Services\RequestStoragePayment;

class RequestStoragePaymentTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new RequestStoragePayment();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createRequestStoragePayment() {
    $file_path = dirname(__FILE__) . '/data/requestStoragePayment.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('request_storage_payment', $parse_json);


    $file_path = dirname(__FILE__) . '/data/requestStoragePaymentUp.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('request_storage_payment_update', $parse_json['data']);
  }

  public function testCreateRequestStoragePayment() {
    self::createRequestStoragePayment();
    $rsp = variable_get('request_storage_payment');
    $rsid = $this->fixture->create($rsp['data']);
    $this->assertGreaterThanOrEqual(1, $rsid);

    return (int) $rsid;
  }

  /**
   * @depends testCreateRequestStoragePayment
   */
  public function testRetriveRequestStoragePayment($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertCount(3, $ret);
  }

  /**
   * @depends testCreateRequestStoragePayment
   */
  public function testUpdateRequestStoragePayment($rsid) {
    $rs_up = variable_get('request_storage_charge_update');
    $rs_update = new RequestStoragePayment($rsid);
    $upd = $rs_update->update($rs_up, TRUE);
    $this->assertEquals(2, $upd);
  }

  /**
   * @depends testCreateRequestStoragePayment
   */
  public function testDeleteRequestStoragePayment($rsid) {
    $rsc_delete = new RequestStoragePayment($rsid);
    $del = $rsc_delete->delete();
    $this->assertEquals(1, $del);
  }

  /**
   * @expectedException ServicesException
   * @dataProvider providerRequestStoragePaymentCreate
   */
  public function testCreateRequestStoragePaymentException($data) {
    $rsid = $this->fixture->create($data);
  }

  public function providerRequestStoragePaymentCreate() {
    return array(
      array(
        'data' => array(
          'id' => '9999999',
          'type' => 2,
          'description' => 'Pro-rated storage 29 days for 08/16',
          'payment' => '160',
        ),
      ),
    );
  }

}
