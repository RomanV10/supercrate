<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/custom/move_storage/tests/StorageTest.php';
require_once DRUPAL_ROOT . '/sites/all/modules/custom/move_storage/tests/RequestStorageTest.php';
require_once DRUPAL_ROOT . '/sites/all/modules/custom/move_storage/tests/RequestStoragePaymentTest.php';

set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

class AllTests {

  public static function suite() {
    $suite = new PHPUnit_Framework_TestSuite('AllMySuite');
    $suite->addTestSuite('MoveStorageTest');
    $suite->addTestSuite('RequestStorageTest');
    $suite->addTestSuite('RequestStoragePaymentTest');
    return $suite;
  }
}