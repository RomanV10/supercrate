<?php

namespace Drupal\move_storage\System;

use Drupal\move_storage\Services\Storage;
use Drupal\move_storage\Services\RequestStorage;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'move_storage' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_storage\System\Service::createStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_storage\System\Service::retrieveStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_storage\System\Service::updateStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_storage\System\Service::deleteStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_storage\System\Service::indexStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'flag',
                'type' => 'int',
                'source' => array('param' => 'flag'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'get_default_storage' => array(
            'callback' => 'Drupal\move_storage\System\Service::getDefaultStorage',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
      'request_storage' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_storage\System\Service::createReqStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_storage\System\Service::retrieveReqStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('owner retrieve request storage'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_storage\System\Service::updateReqStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_storage\System\Service::deleteReqStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_storage\System\Service::indexReqStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),

            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'get_debtor' => array(
            'callback' => 'Drupal\move_storage\System\Service::getDebtor',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_request_status' => array(
            'callback' => 'Drupal\move_storage\System\Service::flagRequestStatus',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'flag',
                'type' => 'int',
                'source' => array('param' => 'flag'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'request_storage_profit' => array(
            'callback' => 'Drupal\move_storage\System\Service::profitReqStorage',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'storage_id',
                'type' => 'int',
                'source' => array('data' => 'storage_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'request_storage_revenue' => array(
            'callback' => 'Drupal\move_storage\System\Service::reqStorRevenue',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_receipt_storage' => array(
            'callback' => 'Drupal\move_storage\System\Service::getReceiptStorage',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_storage',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_storage_invoices' => array(
            'callback' => 'Drupal\move_storage\System\Service::getStorageInvoices',
            'file' => array(
              'type' => 'php',
              'module' => 'move_storage',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'sort',
                'type' => 'string',
                'source' => array('data' => 'sort'),
                'optional' => TRUE,
                'default value' => 'created',
              ),
              array(
                'name' => 'direction',
                'type' => 'string',
                'source' => array('data' => 'direction'),
                'optional' => TRUE,
                'default value' => 'desc',
              ),
              array(
                'name' => 'page',
                'type' => 'int',
                'source' => array('data' => 'page'),
                'optional' => TRUE,
                'default value' => 0,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  public static function createStorage($data = array()) {
    $al_instance = new Storage();
    return $al_instance->create($data);
  }

  public static function retrieveStorage($id) {
    $al_instance = new Storage($id);
    return $al_instance->retrieve();
  }

  public static function updateStorage($id, $data) {
    $al_instance = new Storage($id);
    return $al_instance->update($data);
  }

  public static function deleteStorage(int $id) {
    $al_instance = new Storage($id);
    return $al_instance->delete();
  }

  public static function indexStorage($flag) {
    $al_instance = new Storage();
    return $al_instance->index($flag);
  }

  public static function getDefaultStorage() {
    $al_instance = new Storage();
    return $al_instance->getDefaultStorage();
  }

  public static function createReqStorage($data = array()) {
    $al_instance = new RequestStorage();
    return $al_instance->create($data);
  }

  public static function retrieveReqStorage($id) {
    $al_instance = new RequestStorage($id);
    return $al_instance->retrieve();
  }

  public static function updateReqStorage($id, $data) {
    $al_instance = new RequestStorage($id);
    return $al_instance->update($data);
  }

  public static function deleteReqStorage(int $id) {
    $al_instance = new RequestStorage($id);
    return $al_instance->delete();
  }

  public static function indexReqStorage() {
    $al_instance = new RequestStorage();
    return $al_instance->index();
  }

  public static function reqStorRevenue() {
    $al_instance = new RequestStorage();
    return $al_instance->getRecurringRevenue();
  }

  public static function getDebtor() {
    $al_instance = new RequestStorage();
    return $al_instance->getDebtor();
  }

  public static function flagRequestStatus($flag) {
    $al_instance = new RequestStorage();
    return $al_instance->requestStatus($flag);
  }

  public static function profitReqStorage($storage_id, $dateto, $datefrom) {
    $al_instance = new RequestStorage();
    return $al_instance->getProfit($storage_id, $dateto, $datefrom);
  }

  public static function getReceiptStorage($dateto, $datefrom) {
    $al_instance = new RequestStorage();
    return $al_instance->getReceiptsForStorage($dateto, $datefrom);
  }

  /**
   * Callback function get all storage invoices with sort.
   *
   * @param string $sort
   *    Variable count_stars or created. Default created.
   * @param string $direction
   *    Variable directions: ASC or DESC.
   * @param int $page
   *    Number page.
   *
   * @return array
   *   Sorted array of storage invoices.
   */
  public static function getStorageInvoices(string $sort, string $direction, int $page) {
    $al_instance = new RequestStorage();
    return $al_instance->getInvoices($sort, $direction, $page);
  }

}
