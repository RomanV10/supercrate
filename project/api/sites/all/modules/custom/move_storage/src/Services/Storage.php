<?php

namespace Drupal\move_storage\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_new_log\Services\Log;

/**
 * Class Storage.
 *
 * @package Drupal\move_storage\Services
 */
class Storage extends BaseService {
  private $id = NULL;

  public function __construct($id = NULL) {
    $this->id = $id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function create($data = array()) {
    $this->checkSetStorageKeys($data);
    $storage_name = 'move_storage';
    $new_storage = new moveStorage($data);

    $default = $new_storage->default;
    unset($new_storage->default);

    $id = db_insert($storage_name)
      ->fields(array(
        'value' => $new_storage->serialize(),
        'default_storage' => (int)$default

      ))
      ->execute();

    if ($default) {
      $this->cleanDefStorage($id);
    }

    $this->invokeStorage($id, 'create');
    $log_title = 'Storage created';
    $textArray = $this->prepareTextArray($data);
    $log_data = $this->prepareStorageLog($log_title, $textArray);
    $instance = new Log($this->id, EntityTypes::STORAGE);
    $instance->create($log_data);

    return (int) $id;
  }

  private function invokeStorage(int $id, $op = 'create') {
    $this->id = $id;
    $solr_req_stor = $this->retrieve();
    $solr_req_stor['entity_id'] = $this->id;
    move_global_search_solr_invoke($op, EntityTypes::STORAGE, $solr_req_stor);
  }

  public function retrieve() {
    $result = FALSE;
    $query = db_select('move_storage', 'ms')
      ->fields('ms')
      ->condition('ms.id', $this->id)
      ->execute()
      ->fetchAssoc();

    if ($query['value']) {
      $storage = new moveStorage();
      $storage->unserialize($query['value']);
      $result = $storage->getObject();
      $result['default'] = (bool)$query['default_storage'];
    }

    $instance = new Log($this->id, EntityTypes::STORAGE);
    $result['logs'] = $instance->index();

    return $result;
  }

  public function update($data = array(), $key = TRUE) {
    $result = FALSE;
    $storage_name = 'move_storage';
    $retrieve_storage = $this->retrieve();
    $storage = new moveStorage($data);
    $result_merge = $retrieve_storage ? array_merge($retrieve_storage, $storage->getObject()) : FALSE;

    if ($key) {
      $this->checkSetStorageKeys($result_merge);
    }
    if ($result_merge) {
      $default = $result_merge['default'];
      unset($result_merge['default']);

      $record = array(
        'id' => $this->id,
        'value' => $result_merge,
        'default_storage' => (int)$default
      );
      $result = drupal_write_record($storage_name, $record, 'id');

      if ($default) {
        $this->cleanDefStorage($this->id);
      }

      if ($result && $key) {
        $log_title = 'Storage updated';
        $textArray = $this->diffOnUpdate($retrieve_storage, $data);
        $log_data = $this->prepareStorageLog($log_title, $textArray);
        $instance = new Log($this->id, EntityTypes::STORAGE);
        if (!empty($textArray)) {
          $instance->create($log_data);
        }
      }
    }

    $this->checkDefStorage($this->id, $data);
    $this->invokeStorage($this->id, 'update');

    return $result;
  }

  public function delete() {
    $result = db_delete('move_storage')->condition('id', $this->id)->execute();
    $this->invokeStorage($this->id, 'delete');
    return $result;
  }

  public function index($flag = 1) {
    $result = array();
    $query = db_select('move_storage', 'rs')
      ->fields('rs')
      ->execute()
      ->fetchAll();

    foreach ($query as $key => $value) {
      $resultStor = new moveStorage();
      $resultStor->unserialize($value->value);
      $data = $flag ? $resultStor->getObject() : $resultStor->getObject()['name'];
      $data['default'] = (bool)$value->default_storage;
      $result[] = array(
        'id' => $value->id,
        'data' => $data,
      );
    }
    return $result;
  }

  private function checkSetStorageKeys($data) {
    $required_keys = array(
      'name',
      'address',
      'zip',
      'phone1',
      'email',
      'rate_per_cuft',
    );
    foreach ($required_keys as $key) {
      if (!array_key_exists($key, $data) || !$data[$key]) {
        throw new \ServicesException(t('Required keys not exist or not set'), 406);
      }
    }
    return TRUE;
  }

  /**
   * Check set default storage or not.
   *
   * @param int $id
   * @param array $data
   */
  private function checkDefStorage(int $id, array $data = array()) {
    $check = $data['default'];
    if ($check) {
      variable_set('defaultstorage', $id);
      $this->cleanDefStorage($id);
    }
  }

  /**
   * Get default storage.
   *
   * @return array
   *   Default storage data.
   */
  public function getDefaultStorage() {
    $result = [];

    $query = db_select('move_storage', 'ms')
      ->fields('ms')
      ->condition('ms.default_storage', 1)
      ->execute()
      ->fetchObject();

    if (!empty($query)) {
      $result = [
        'id' => $query->id,
        'value' => unserialize($query->value),
      ];
    }

    return $result;
  }

  /**
   * Clean default storage if in another storage set default.
   * @param int $id
   */
  private function cleanDefStorage(int $id) {
    db_update('move_storage')
      ->fields(['default_storage' => 0])
      ->condition('id', $id, '<>')
      ->execute();

  }

  public static function getAllStorages() {
    $query = db_select('move_storage', 'rs')
      ->fields('rs', array('id'))
      ->execute()
      ->fetchCol();

    return $query;
  }

  /**
   * Get diff between old and new.
   *
   * @param array $old
   *   Old values.
   * @param array $new
   *   New values.
   *
   * @return array
   *   Difference between values.
   */
  public static function diffOnUpdate($old, $new) {
    $new = (array) $new;
    $text = array();
    foreach ($old as $field_name => $val) {
      $name = ucfirst(str_replace('_', ' ', $field_name));
      if ($name !== "Logs" && ($old[$field_name] != $new[$field_name])) {
        if (empty($old[$field_name])) {
          $text[]['simpleText'] = $name . ' "' . $new[$field_name] . '" was added';
        }
        elseif (empty($new[$field_name])) {
          $text[]['simpleText'] = $name . ' "' . $old[$field_name] . '" was removed';
        }
        else {
          $text[]['simpleText'] = $name . ' was changed from "' . $old[$field_name] . '" to "' . $new[$field_name] . '"';
        }
      }
    }

    return $text;
  }

  /**
   * Method for create logs for storage.
   *
   * @param $title
   *
   * @return array
   *   Prepared log data.
   */
  public function prepareStorageLog($title, $textArray) {
    $log_data[] = array(
      'details' => array([
        'activity' => 'System',
        'title' => $title,
        'text' => array([
          'text' => $textArray,
        ]),
        'date' => time(),
      ]),
      'source' => 'System',
    );

    return $log_data;
  }

  public function prepareTextArray($data) {
    $result = array();
    foreach ($data as $key => $value) {
      $key = ucfirst(str_replace(array('_', '-'), ' ', $key));
      $result[] = array(
        'simpleText' => "$key: $value",
      );
    }

    return $result;
  }

}

class moveStorage implements \Serializable {

  public function __construct(array $data = array()) {
    foreach ($data as $key => $value) {
      switch ($key) {
        case 'default':
          $this->{$key} = (bool) $value;
          break;

        case 'name':
        case 'address':
        case 'state':
        case 'city':
        case 'phone1':
        case 'phone2':
        case 'fax':
          $this->{$key} = (string) $value;
          break;

        case 'zip':
          $this->{$key} = $this->correctZip((string) $value);
          break;

        case 'email':
          $this->{$key} = $this->correctEmail((string) $value);
          break;

        case 'tax':
        case 'rate_per_cuft':
        case 'late_fee':
        case 'apply_late_fee_in':
          $this->{$key} = (float) $value;
          break;

        default:
          $this->{$key} = $value;
          break;
      }
    }
  }

  public function serialize() {
    return serialize(get_object_vars($this));
  }

  public function unserialize($serialized) {
    $values = unserialize($serialized);
    foreach ($values as $key => $value) {
      $this->$key = $value;
    }
  }

  public function getObject() : array {
    return get_object_vars($this);
  }

  /**
   * Validate by regular expression zip code.
   * @param $storage_zip
   * @return mixed
   * @throws \ServicesException
   */
  private function correctZip($zip) {
    if (!preg_match("/^([0-9]{5})?$/i", $zip)) {
      throw new \ServicesException(t('Form validation error. Try again.'), 406);
    }
    else {
      return $zip;
    }
  }

  /**
   * Validate by regular expression email.
   * @param $storage_email
   * @return mixed
   * @throws \ServicesException
   */
  private function correctEmail($email) {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      throw new \ServicesException(t('Form validation error. Try again.'), 406);
    }
    else {
      return $email;
    }
  }

}
