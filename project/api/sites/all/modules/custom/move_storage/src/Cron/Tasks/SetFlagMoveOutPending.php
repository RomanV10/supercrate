<?php

namespace Drupal\move_storage\Cron\Tasks;

use Drupal\move_services_new\Cron\Tasks\CronInterface;
use Drupal\move_services_new\Util\enum\RequestStorageFlags;
use Drupal\move_storage\Services\RequestStorage;

/**
 * Class SetFlagMoveOutPending.
 *
 * @package Drupal\move_storage\Cron\Tasks
 */
class SetFlagMoveOutPending implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $date = new \DateTime('now', new \DateTimeZone('GMT'));
    $minus_one_days = strtotime("-1 days");
    $date->setTimestamp($minus_one_days);
    $day_before = $date->getTimestamp();
    $day_start = strtotime(date('Y-m-d', $day_before) . '00:00:00');
    $date_end = strtotime(date('Y-m-d', $day_before) . '23:59:59');

    $query = db_select('request_storage_additional', 'rsa');
    $query->leftJoin('request_storage', 'rs', 'rs.id = rsa.request_storage_id');
    $query->fields('rsa', array('request_storage_id'))
      ->fields('rs', array('value'))
      ->condition('how_many', array($day_start, $date_end), 'BETWEEN')
      ->condition('rsa.start_recurring', 0);
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    if (!empty($result)) {
      $storage_inst = new RequestStorage();
      foreach ($result as $row) {
        $row['value'] = unserialize($row['value']);
        $row['value']['rentals']['status_flag'] = RequestStorageFlags::MOVEOUT_PENDING;
        $updated_r = db_update('request_storage')
          ->fields(array('value' => serialize($row['value'])))
          ->condition('id', $row['request_storage_id'])
          ->execute();
        $updated_a = db_update('request_storage_additional')
          ->fields(array('status_flag' => RequestStorageFlags::MOVEOUT_PENDING))
          ->condition('request_storage_id', $row['request_storage_id'])
          ->execute();
        if ($updated_r && $updated_a) {
          $title = 'Storage flag was changed to "MOVE OUT PENDING"';
          $storage_inst->createReqStorLog($row['request_storage_id'], $title);
        }
      }
    }
  }

}
