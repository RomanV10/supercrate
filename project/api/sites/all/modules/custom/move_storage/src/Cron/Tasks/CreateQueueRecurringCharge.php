<?php

namespace Drupal\move_storage\Cron\Tasks;

use Drupal\move_services_new\Cron\Tasks\CronInterface;
use Drupal\move_storage\Services\RequestStorage;

/**
 * Class CreateQueueRecurringCharge.
 *
 * @package Drupal\move_storage\Cron\Tasks
 */
class CreateQueueRecurringCharge implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $queue = \DrupalQueue::get('move_storage_execute_charge');
    $queue->deleteQueue();
    $queue->createQueue();
    $allStartRec = (array) RequestStorage::indexStartRec();
    $chunk = array_chunk($allStartRec, 1);
    foreach ($chunk as $item) {
      $queue->createItem($item);
    }
  }

}
