<?php

namespace Drupal\move_storage\ValidationRules;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class StorageCreateUpdate.
 *
 * @package Drupal\move_storage\ValidationRules
 */
class StorageCreateUpdate implements ValidationRulesInterface {

  /**
   * Rules for validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'data' => ['array'],
      'data.recurring' => ['present', 'array'],
      'data.rentals' => ['present', 'array'],
      'data.additional_user' => ['array'],
      'data.user_info' => ['present', 'array'],
      'data.user_info.city' => ['present', 'string'],
      'data.user_info.email' => ['required', 'string', 'email'],
      'data.user_info.phone1' => ['present', 'string'],
      'data.user_info.phone2' => ['sometimes', 'present', 'string'],
      'data.user_info.realRequest' => ['present', 'bool'],
      'data.user_info.state' => ['present', 'string'],
      'data.user_info.uid' => ['present', 'int'],
      'data.user_info.zip' => ['present', 'string'],
      'data.user_info.zip_from' => ['present', 'string'],
    ];
  }
}