<?php

namespace Drupal\move_storage\Cron\Queues;

use Drupal\move_services_new\Cron\Queues\QueueInterface;
use Drupal\move_storage\Services\RequestStorage;

/**
 * Class ExecuteStorageCharge.
 *
 * @package Drupal\move_storage\Cron\Queues
 */
class ExecuteStorageCharge implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \ServicesException
   */
  public static function execute($data): void {
    (new RequestStorage())->executeCronCharge($data);
  }

}
