<?php

namespace Drupal\move_storage\Services;

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\move_request\MoveRequestSearch;
use Drupal\move_invoice\Services\Invoice;
use Drupal\move_new_log\Services\Log;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\InvoiceFlags;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\RequestStorageTimePeriod;
use Drupal\move_services_new\Util\enum\RequestStorageTypeCharge;
use Drupal\move_services_new\Util\enum\SmsActionTypes;
use Drupal\move_sms\Services\Sms;
use Drupal\move_template_builder\Services\EmailTemplate;
use Drupal\move_services_new\Services\Clients;

/**
 * Class Storage.
 *
 * @package Drupal\move_storage\Services
 */
class RequestStorage extends BaseService {
  private $id;
  private $retrieveFlag;

  /**
   * The number of page.
   *
   * @var int
   */
  private $page = 0;

  /**
   * The page size.
   *
   * @var int
   */
  private $pageSize = 25;

  public function __construct($id = NULL, $retr_flag = TRUE) {
    $this->id = $id;
    $this->retrieveFlag = $retr_flag;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function create($data = array()) {
    $request_storage = 'request_storage';
    if (!empty($data['recurring']['how_many'])) {
      $data['rentals']['moved_out_date'] = $data['recurring']['how_many'];
    }
    else {
      $data['rentals']['moved_out_date'] = NULL;
    }

    if (!empty($data['user_info']['phone1'])) {
      $data['user_info']['phone1'] = Extra::phoneNumberConvert($data['user_info']['phone1']);
    }

    if (!empty($data['user_info']['phone2'])) {
      $data['user_info']['phone2'] = Extra::phoneNumberConvert($data['user_info']['phone2']);
    }

    $start = $this->checkStartRec($data);

    $id = db_insert($request_storage)
      ->fields(array(
        'value' => serialize($data),
        'start_recurring' => $start,
      ))
      ->execute();

    $res_create_rsa = $this->reqStorAdd($id, $data);
    $this->invokeRequestStorage($id);
    if (!$res_create_rsa) {
      throw new \ServicesException(t('Error. Request Storage Add not create.'), 406);
    }

    return $id;
  }

  private function invokeRequestStorage(int $id, $op = 'create') {
    $this->id = $id;
    $this->retrieveFlag = FALSE;
    $solr_req_stor = $this->retrieve();
    $solr_req_stor['entity_id'] = $this->id;
    move_global_search_solr_invoke($op, EntityTypes::STORAGEREQUEST, $solr_req_stor);
  }

  public function retrieve() {
    $query = db_select('request_storage', 'rs')
      ->fields('rs')
      ->condition('rs.id', $this->id)
      ->execute()
      ->fetchAssoc();

    $result = unserialize($query['value']);
    $result['id'] = $this->id;

    // TODO. Create one method for move storage and storage request.
    if (!empty($result['additional_user']['uid'])) {
      $additional_user_wrap = (new Clients())->getUserFieldsData($result['additional_user']['uid'], FALSE);
      $result['field_additional_user'] = array(
        'uid' => $additional_user_wrap['uid'],
        'first_name' => $additional_user_wrap['field_user_first_name'],
        'last_name' => $additional_user_wrap['field_user_last_name'],
        'mail' => $additional_user_wrap['mail'],
        'phone' => $additional_user_wrap['field_primary_phone'],
      );
    }

    if ($this->retrieveFlag) {
      $query_invoice = db_select('move_invoice', 'mi')
        ->fields('mi')
        ->condition('mi.entity_id', $this->id)
        ->condition('mi.entity_type', EntityTypes::STORAGEREQUEST)
        ->execute()
        ->fetchAll();

      foreach ($query_invoice as $key => $value) {
        $value->data = unserialize($value->data);
        $result['invoices'][] = $value;
      }

      // Chnage this fields to new. Now calculate it from last run charge.
      if (empty($result['recurring']['last_charge_run'])) {
        $result['recurring']['next_run_on'] = $result['recurring']['start_date'];
      }
      else {
        $result['recurring']['next_run_on'] = $this->calcTimePeriod($result['recurring']['last_charge_run'], $result['recurring']['how_often']);
      }

      $result['payments'] = MoveRequest::getReceipt($this->id, EntityTypes::STORAGEREQUEST);
      $result['balance'] = 0;
      if ($balance = $this->getReqStorBalance($this->id)) {
        $result['balance'] = $balance;
      }
    }

    return $result;
  }

  /**
   * Method return query and result for invoice.
   *
   * @param string $sort
   *   Sort date created or unique id.
   * @param string $direction
   *   Direction asc or desc.
   * @param int $page
   *   Number page.
   * @param int $entity_type
   *   Entity type.
   * @param array $filter
   *   Filter data.
   *
   * @return array
   *   All sorted storage invoices.
   */
  public function getInvoicesQuery(string $sort, string $direction, int $page, int $entity_type = EntityTypes::STORAGEREQUEST, array $filter = array()) {
    $result = array();
    // Init count and sum.
    $this->resetInvoiceTotal($result);

    if ($sort != 'created' && $sort != 'id') {
      $sort = 'id';
    }
    if ($direction != 'DESC' && $direction != 'ASC') {
      $direction = 'DESC';
    }

    $query_invoice = db_select('move_invoice', 'mi');
    $query_invoice->fields('mi');
    $query_invoice->condition('mi.entity_type', $entity_type);
    if (!empty($filter['date_from']) && !empty($filter['date_to'])) {
      $query_invoice->condition('mi.created', array($filter['date_from'], $filter['date_to']), 'BETWEEN');
    }
    if (isset($filter['flag'])) {
      if (is_array($filter['flag'])) {
        $query_invoice->condition('mi.flag', $filter['flag'], 'IN');
      }
      else {
        $query_invoice->condition('mi.flag', $filter['flag']);
      }
    }
    $query_invoice->orderBy($sort, $direction);

    // Clone this to get all totals.
    $query_invoice_total = clone $query_invoice;
    $total_invoices = $query_invoice_total->execute()->rowCount();
    if (!empty($total_invoices)) {
      $pages_number = ceil($total_invoices / $this->pageSize);
      $result['_meta'] = [
        'currentPage' => $page,
        'pageCount' => $pages_number,
        'perPage' => $this->pageSize,
        'totalCount' => $total_invoices,
      ];
    }
    else {
      $result['_meta'] = [
        'currentPage' => $page,
        'pageCount' => 0,
        'perPage' => $this->pageSize,
        'totalCount' => $total_invoices,
      ];
    }

    return ['result' => $result, 'query' => $query_invoice];
  }

  /**
   * Get data for query with range total.
   *
   * @param array $result
   *   Result.
   * @param \SelectQuery $query
   *   Quary.
   * @param int $page
   *   Page.
   */
  public function getInvoiceData(array &$result, \SelectQuery $query, int $page = NULL) {
    if ($page === NULL) {
      $query->range();
    }
    else {
      $query->range($page * $this->pageSize, $this->pageSize);
    }
    $query_invoice_result = $query->execute()->fetchAll();
    if (!empty($query_invoice_result)) {
      foreach ($query_invoice_result as $key => $value) {
        $value->data = unserialize($value->data);
        $result['items'][$key] = array(
          'id' => $value->id,
          'client_name' => $value->data['client_name'] ?? '',
          'description' => $value->data['charges'][0]['description'] ?? '',
          'invoice_date' => $value->created,
          'total' => $value->data['total'] ?? 0,
          'status' => $value->data['flag'] ?? 0,
          'storage_request_id' => $value->entity_id,
          'hash' => $value->hash,
        );
        if ($value->data['flag'] == InvoiceFlags::PAID) {
          $result['sum']['paid'] += $result['items'][$key]['total'];
          $result['count']['paid']++;
        }
        else {
          $result['sum']['unpaid'] += $result['items'][$key]['total'];
          $result['count']['unpaid']++;
        }
      }
      $result['sum']['all'] = $result['sum']['paid'] + $result['sum']['unpaid'];
      $result['count']['all'] = $result['count']['paid'] + $result['count']['unpaid'];
    }
  }

  /**
   * Reset sum and count.
   *
   * @param array $result
   *   Result.
   */
  private function resetInvoiceTotal(array &$result) {
    $result['sum']['paid'] = 0;
    $result['sum']['unpaid'] = 0;
    $result['sum']['all'] = 0;

    $result['count']['all'] = 0;
    $result['count']['paid'] = 0;
    $result['count']['unpaid'] = 0;
  }

  /**
   * Method return all storage invoice.
   *
   * @param string $sort
   *   Sort date created or unique id.
   * @param string $direction
   *   Direction asc or desc.
   * @param int $page
   *   Number page.
   * @param int $entity_type
   *   Entity type.
   * @param array $filter
   *   Filter data.
   *
   * @return array
   *   All sorted storage invoices.
   */
  public function getInvoices(string $sort, string $direction, int $page, int $entity_type = EntityTypes::STORAGEREQUEST, array $filter = array()) {
    $invoices_data = $this->getInvoicesQuery($sort, $direction, $page, $entity_type, $filter);
    $this->getInvoiceData($invoices_data['result'], $invoices_data['query'], $page);

    return $invoices_data['result'];
  }

  public function update($data = array()) {
    $storage_name = 'request_storage';

    if (!empty($data['user_info']['phone1'])) {
      $data['user_info']['phone1'] = Extra::phoneNumberConvert($data['user_info']['phone1']);
    }

    if (!empty($data['user_info']['phone2'])) {
      $data['user_info']['phone2'] = Extra::phoneNumberConvert($data['user_info']['phone2']);
    }

    $this->retrieveFlag = FALSE;
    $retrieve_storage = $this->retrieve();
    $start = $this->checkStartRec($data);
    if (is_array($retrieve_storage)) {
      $merge_storage = array_merge($retrieve_storage, $data);
    }
    else {
      $merge_storage = $data;
    }
    $id = $this->id;
    $record = array(
      'id' => $id,
      'value' => $merge_storage,
      'start_recurring' => $start,
    );
    $addRes = array(
      'request_storage_id' => $id,
      'storage_id' => $data['rentals']['storage'],
      'total_monthly' => $data['rentals']['total_monthly'],
      'status_flag' => $data['rentals']['status_flag'],
      'start_recurring' => $start,
      'start_date' => $data['recurring']['start_date'],
      'how_often' => $data['recurring']['how_often'],
      'how_many' => $data['recurring']['how_many'],
      'next_run_on' => (isset($data['recurring']['next_run_on']) ? $data['recurring']['next_run_on'] : NULL),
      'next_run_on_fee' => (isset($data['recurring']['next_run_on_fee']) ? $data['recurring']['next_run_on_fee'] : NULL),
      'start_point' => (isset($data['recurring']['start_point']) ? $data['recurring']['start_point'] : NULL),
      'move_request_id' => (isset($data['rentals']['move_request_id']) ? $data['rentals']['move_request_id'] : NULL),
      'auto_send' => $data['recurring']['auto_send'],
      'changed' => time(),
    );
    // If recurring was stopped = reset last charge and fee dates.
    if (empty($start)) {
      $addRes['last_charge_run'] = NULL;
      $addRes['last_fee_run'] = NULL;
      $addRes['first_run'] = 0;
    }
    $result_rsa = db_merge('request_storage_additional')
      ->key(array('request_storage_id' => $id))
      ->fields($addRes)
      ->execute();

    if (!$result_rsa) {
      throw new \ServicesException(t('Error. Request Storage Add not create.'), 406);
    }

    $result = drupal_write_record($storage_name, $record, 'id');
    $this->invokeRequestStorage($this->id, 'update');
    return $result;
  }

  public function delete() {
    db_delete('request_storage_additional')->condition('id', $this->id)->execute();
    $result = db_delete('request_storage')->condition('id', $this->id)->execute();
    move_global_search_solr_invoke('delete', EntityTypes::STORAGEREQUEST, $this->id);
    return $result;
  }

  public function index($flag = 1) {
    $result = array();
    $query = db_select('request_storage', 'rs')
      ->fields('rs')
      ->execute()
      ->fetchAll();
    foreach ($query as $key => $value) {
      $data = $flag ? unserialize($value->value) : '';

      $result[] = array(
        'id' => $value->id,
        'data' => $data,
      );

    }
    return $result;
  }

  /**
   * Save data storage request in additional table.
   *
   * @param int $id
   *   Unique id of request storage.
   * @param array $data
   *   Contain info about recurring.
   *
   * @return object
   *   New MergeQuery object for the active database.
   */
  private function reqStorAdd(int $id, $data = array()) {
    $table = 'request_storage_additional';
    $start = $this->checkStartRec($data);

    $fields = array(
      'request_storage_id' => $id,
      'storage_id' => $data['rentals']['storage'],
      'total_monthly' => $data['rentals']['total_monthly'],
      'status_flag' => $data['rentals']['status_flag'],
      'start_recurring' => $start,
      'start_date' => $data['recurring']['start_date'],
      'how_often' => $data['recurring']['how_often'],
      'how_many' => $data['recurring']['how_many'],
      'next_run_on' => $data['recurring']['next_run_on'],
      'next_run_on_fee' => $data['recurring']['next_run_on_fee'],
      'start_point' => $data['recurring']['start_point'],
      'move_request_id' => (isset($data['rentals']['move_request_id']) ? $data['rentals']['move_request_id'] : NULL),
      'auto_send' => $data['recurring']['auto_send'],
      'created' => time(),
      'changed' => time(),
    );

    $result = db_merge($table)
      ->key(array('request_storage_id' => $id))
      ->fields($fields)
      ->execute();
    return $result;
  }

  /**
   * Check set recurring or not.
   *
   * @param array $data
   *   Contain info about user, rentals, recurring.
   *
   * @return int
   *   Set 1 if recurring set, else 0.
   */
  private function checkStartRec(array $data = array()) {
    return $result = (isset($data['recurring']['start']) && $data['recurring']['start']) ? 1 : 0;
  }

  /**
   * Get all request storage where balance < 0.
   *
   * @return mixed
   */
  public function getDebtor() {
    $debtors = array();
    $query = db_select('request_storage_additional', 'rsa')
      ->fields('rsa', array('request_storage_id'))
      ->condition('balance', 0, '<')
      ->execute()
      ->fetchCol();

    foreach ($query as $key) {
      $this->setId($key);
      $debtors[$key] = $this->retrieve();
    }
    return $debtors;
  }

  public function requestStatus($flag = 1) {
    $result = array();
    $query = db_select('request_storage_additional', 'rsa')
      ->fields('rsa', array('request_storage_id'))
      ->condition('status_flag', $flag)
      ->execute()
      ->fetchCol();

    foreach ($query as $key) {
      $this->setId($key);
      $result[$key] = $this->retrieve();
    }
    return $result;
  }

  private function createInvoiceDescription($next_run_on, $how_often, $how_many, $time) {
    if ($time == 'day') {
      $dte_current = new \DateTime();
      $dte_current->setTimezone(new \DateTimeZone('UTC'));
      $dte_current->setTimestamp($next_run_on);
      $current = $dte_current->format('m/d/Y');

      $next_full_date = $this->calcTimePeriod($next_run_on, $how_often);

      if ($next_full_date < $how_many || $how_many == 0) {
        $dte_next = new \DateTime();
        $dte_next->setTimezone(new \DateTimeZone('UTC'));
        $dte_next->setTimestamp($next_full_date);
        $next = $dte_next->format('m/d/Y');
      }
      else {
        $dte_next = new \DateTime();
        $dte_next->setTimezone(new \DateTimeZone('UTC'));
        $dte_next->setTimestamp($how_many);
        $next = $dte_next->format('m/d/Y');
      }
    }
    else {
      $dte_current = new \DateTime();
      $dte_current->setTimezone(new \DateTimeZone('UTC'));
      $dte_current->setTimestamp($next_run_on);
      $current = $dte_current->format('m/d/Y H:i:s');

      $next_full_date = $this->calcTimePeriod($next_run_on, $how_often);

      if ($next_full_date < $how_many || $how_many == 0) {
        $dte_next = new \DateTime();
        $dte_next->setTimezone(new \DateTimeZone('UTC'));
        $dte_next->setTimestamp($next_full_date);
        $next = $dte_next->format('m/d/Y H:i:s');
      }
      else {
        $dte_next = new \DateTime();
        $dte_next->setTimezone(new \DateTimeZone('UTC'));
        $dte_next->setTimestamp($how_many);
        $next = $dte_next->format('m/d/Y H:i:s');
      }
    }

    $result = "Storage Service $current - $next";
    return $result;
  }

  public function executeCronCharge(array $allStartRec, $curDate = NULL, string $time = 'day') {
    $result = array();
    $data_for_cron_log = array();
    try {
      $curDate = !$curDate ? time() : $curDate;
      $data_for_cron_log['curDate'] = $curDate;
      foreach ($allStartRec as $storage_request) {
        $this->runCharge($storage_request, $curDate, $time);
        $this->runFeeCharge($storage_request, $curDate, $time);
      }
      $this->getInvoiceNotPaid($time);
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('cron_recurring', $message, array(), WATCHDOG_ERROR);
      $error_msg = array(
        'status' => FALSE,
        'text' => $e->getMessage(),
      );
      throw new \ServicesException('', 400, $error_msg);
    }

    return $result;
  }

  /**
   * Run request storage charge.
   *
   * @param $storage_request
   * @param $curDate
   * @param $time
   *
   * @throws \Exception
   */
  public function runCharge(&$storage_request, $curDate, $time) {
    $not_full_period = FALSE;
    // Auto send email.
    $auto_send = $storage_request->auto_send;
    // Date when recurring will stop.
    $end_recurring_date = $storage_request->how_many;
    $rsid = $storage_request->request_storage_id;
    // Interval between create invoice(charge).
    $how_often = $storage_request->how_often;
    // If this first run recurring.
    $first_run = $storage_request->first_run;
    // Recurring start day.
    $start_run = (int) $storage_request->start_date;
    // When charge was run last time.
    $last_charge_run = !empty($storage_request->last_charge_run) ? $storage_request->last_charge_run : $start_run;
    // Storage balance.
    $balance = $this->requestStorageBalance($rsid);
    // Create next run charge date.
    if (empty($first_run)) {
      $next_recurring_date = $last_charge_run;
    }
    else {
      $next_recurring_date = $this->calcTimePeriod($last_charge_run, $how_often);
    }
    // Price for period.
    $unit_cost = $storage_request->total_monthly;

    $charge_type = RequestStorageTypeCharge::RECURRING_CHARGE;
    // If isset recurring end date, check next run date.
    if ($end_recurring_date) {
      // Get temp value only for check.
      $temp_next_run = $this->calcTimePeriod($next_recurring_date, $how_often);
      if ($temp_next_run > $end_recurring_date) {
        $not_full_period = TRUE;
      }
    }
    if ($next_recurring_date <= $curDate) {
      // Create description for ledger.
      $description_rec = $this->createInvoiceDescription($next_recurring_date, $how_often, $end_recurring_date, $time);
      // THis check if recurring for only part of period.
      if ($not_full_period) {
        $unit_cost = $this->calculateNotFullCharges($storage_request, $unit_cost, $time);
      }
      // Create invoice(charge) for user payment.
      $invoice_id = $this->createInvoice($rsid, $description_rec, $curDate + 1, $unit_cost, $charge_type);
      if ($invoice_id) {
        // THis check if recurring for only part of period.
        if ($not_full_period) {
          if ($balance >= $unit_cost) {
            $this->changeRecurringInPaid($invoice_id);
          }
          $this->updateStopRecurring($rsid);
          $title = 'Recurring was end';
          $this->createReqStorLog($rsid, $title, $curDate + 2);
        }
        // This is for full time period.
        else {
          if ($balance >= $unit_cost) {
            $this->changeRecurringInPaid($invoice_id);
            $this->updateNextRunOnFee($rsid);
          }
        }

        $this->createReqStorLog($rsid, $description_rec, $curDate + 1);
        if (!$first_run) {
          $this->changeFirstRun($rsid);
        }
        $this->updateNextRunOn($rsid);
        // Update last date when recurring was run.
        $this->updateLastSendDate($rsid, $next_recurring_date);
        $storage_request->last_charge_run = $next_recurring_date;
        (new Sms(SmsActionTypes::STORAGE_INVOICE))->execute($rsid, NULL, NULL);

        if ($auto_send) {
          $this->sendRecurringEmail($invoice_id);
        }

        $data_for_cron_log['invoice_id'] = $invoice_id;
        $data_for_cron_log['charge_type'] = $charge_type;
        $data_for_cron_log['request_storage_id'] = $rsid;
      }
    }
  }

  /**
   * Run request storage charge fee.
   *
   * @param $storage_request
   * @param $curDate
   * @param $time
   *
   * @throws \Throwable
   */
  public function runFeeCharge($storage_request, $curDate, $time) {
    global $user;

    $rsid = $storage_request->request_storage_id;
    // Get balance to know do wee need to run fee.
    $balance = $this->requestStorageBalance($rsid);
    $charge_type = RequestStorageTypeCharge::FEE_CHARGE;
    // Auto send email.
    $auto_send = $storage_request->auto_send;

    $not_full_period = FALSE;
    if ($balance < 0) {
      $unit_cost = $this->getLateFee($rsid);
      // Get all non fee and not paid invoices.
      $invoices = $this->getStorageNotPaidNoFeeInvoices($rsid);
      $app_late_fee_in = $this->getLateFeeDate($rsid);
      if (!empty($invoices)) {
        foreach ($invoices as $invoice) {
          $charge_created_date = $invoice['created'];
          $dateMod = "+$app_late_fee_in $time";
          $date = new \DateTime();
          $date->setTimezone(new \DateTimeZone('UTC'));
          $date->setTimestamp($charge_created_date);
          $date->modify($dateMod);
          $when_must_run_late_fee = $date->getTimestamp();
          if (($when_must_run_late_fee <= $curDate)) {
            $description_fee = 'Late fee charge';
            $this->createReqStorLog($rsid, $description_fee, $curDate + 2);
            // Work only for part of period.
            if ($not_full_period) {
              $this->updateStopRecurring($rsid);
              $title = 'Recurring was end';
              $this->createReqStorLog($rsid, $title, $curDate);
              if ($unit_cost > 0) {
                $unit_cost = $this->calculateNotFullCharges($storage_request, $unit_cost, $time);
              }
            }
            else {
              $this->updateNextRunOnFee($rsid);
            }
            // If fee amount more then 0 - create charge fee and send invoice.
            if ($unit_cost > 0) {
              $invoice_id = $this->createInvoice($rsid, $description_fee, $curDate + 1, $unit_cost, $charge_type);
              (new DelayLaunch())->createTask(TaskType::SMS_SEND, [
                'actionId' => SmsActionTypes::STORAGE_INVOICE,
                'nid' => $rsid,
                'uid' => NULL,
                'user' => $user->uid,
              ]);

              if ($auto_send) {
                $this->sendRecurringEmail($invoice_id);
              }

            }
            // Add fee to invoice.
            db_insert('move_invoice_fee')
              ->fields(array(
                'invoice_id' => $invoice['id'],
                'entity_id' => $rsid,
                'entity_type' => EntityTypes::STORAGEREQUEST,
                'fee_amount' => $unit_cost,
                'created' => time(),
              ))
              ->execute();
            $this->updateLastSendFeeDate($rsid, $curDate);
          }
        }
      }
    }
  }

  /**
   * Send recuring email for charge and fee.

   * @param int $invoice_id
   *   Invoice id.
   */
  private function sendRecurringEmail($invoice_id) : void {
    try {
      $data = static::getInvoiceTemplate('storage_invoice_template');
      $send_email = EmailTemplate::sendEmailTemplateInvoice($invoice_id, $data);
      if ($send_email) {
        $this->changeRecurringInSent($invoice_id);
      }
    }
    catch (\Throwable $e) {
      watchdog('Storage, send recurring email', ['invoice_id' => $invoice_id], array(), WATCHDOG_CRITICAL);
    }
  }

  /**
   * Get template for invoice.
   *
   * @param string $invoice_template
   *   Template name.
   *
   * @return array
   *   Array with template data.
   */
  public static function getInvoiceTemplate(string $invoice_template) {
    $data = array();
    $query_template = db_select('move_template_builder', 'fuk')
      ->fields('fuk')
      ->condition('fuk.key_name', '%' . db_like($invoice_template) . '%', 'LIKE')
      ->execute()
      ->fetchAll();

    if (isset($query_template) && $query_template) {
      $query_template = reset($query_template);
      $subject = json_decode($query_template->data);

      $data = array(
        [
          'template' => $query_template->template,
          'subject'   => $subject->subject,
        ],
      );
    }

    return $data;
  }

  public function getInvoiceNotPaid($time) {
    $cur_date = time();
    $invoices = Invoice::getInvoiceWithFlag(1, array(InvoiceFlags::SENT, InvoiceFlags::VIEWED));
    foreach ($invoices as $key => $value) {
      $invoice_data = unserialize($value->data);
      $charge_type = !empty($invoice_data['charges'][0]['charge_type']) ? $invoice_data['charges'][0]['charge_type'] : RequestStorageTypeCharge::CHARGE;

      // This is check for balance. We need only negative balance.
      $request_storage_balance = $this->getReqStorBalance($value->entity_id);
      if ($request_storage_balance < 0 && $charge_type != RequestStorageTypeCharge::FEE_CHARGE) {
        $rem = $this->checkReminder($value->id);
        if (!$rem) {
          continue;
        }
        $reminder = $this->convertDayToSecTEST(reset($rem), $time);
        $reminder_date = $reminder + $value->created;

        $invoice_template = '';
        if (isset($rem['reminder1']) && $rem['reminder1']) {
          $invoice_template = 'storage_reminder1';
        }

        if (isset($rem['reminder2']) && $rem['reminder2']) {
          $invoice_template = 'storage_reminder2';
        }

        if (isset($rem['reminder3']) && $rem['reminder3']) {
          $invoice_template = 'storage_reminder3';
        }
        $cur_date_format = new \DateTime(date('Y-m-d', $cur_date));
        $reminder_date_format = new \DateTime(date('Y-m-d', $reminder_date));
        $interval = $cur_date_format->diff($reminder_date_format);
        // Interval between reminder date and current date.
        $days_interval = (int) $interval->days;
        if ($days_interval === 0 && $invoice_template) {
          $app_late_fee_in = $this->getLateFeeDate($value->entity_id);
          $dateMod = "+$app_late_fee_in $time";
          $date = new \DateTime();
          $date->setTimezone(new \DateTimeZone('UTC'));
          $date->setTimestamp($value->created);
          $date->modify($dateMod);
          // Interval between next run fee date and reminder date.
          $interval = $date->diff($reminder_date_format);
          $days_interval = (int) $interval->days;
          if ($days_interval !== 0) {
            $data = static::getInvoiceTemplate($invoice_template);
            $send_email = EmailTemplate::sendEmailTemplateInvoice($value->id, $data);
            if ($send_email) {
              $this->writeReminderInDb($value->id, key($rem));
            }
          }
        }
      }
    }
  }

  /**
   * Get invoices for storage entity by charge type.
   *
   * @param int $entity_id
   *
   * @return array
   *   Storage Request data.
   */
  public function getNotPaidChargesByType($entity_id) {
    $result = array();
    $instance = new Invoice();
    $invoices = $instance->getNotPaidInvoiceForEntity($entity_id, EntityTypes::STORAGEREQUEST);
    foreach ($invoices as $invoice) {
      if (!empty($invoice['data']['charges'])) {
        foreach ($invoice['data']['charges'] as $charge) {
          if (isset($charge['charge_type']) && ($charge['charge_type'] !== RequestStorageTypeCharge::FEE_CHARGE)) {
            $result[] = $invoice;
          }
        }
      }
    }

    return $result;
  }

  /**
   *  Retrieve all not paid invoices that doesn't have fee.
   *
   * @param $entity_id
   *   Storage id.
   *
   * @return mixed
   */
  public function getStorageNotPaidNoFeeInvoices($entity_id) {
    $result = array();
    $invoice_flags = array(
      InvoiceFlags::DRAFT,
      InvoiceFlags::SENT,
      InvoiceFlags::VIEWED,
    );

    // Get all invoices with that have fee.
    $invoices_with_fee = db_select('move_invoice_fee', 'mif')
      ->fields('mif', array('invoice_id'))
      ->condition('mif.entity_id', $entity_id)
      ->condition('mif.entity_type', EntityTypes::STORAGEREQUEST)
      ->execute()
      ->fetchCol();

    // Get all not paid and non fee invoices.
    $query = db_select('move_invoice', 'mi');
    $query->fields('mi');
    $query->condition('mi.entity_id', $entity_id);
    if (!empty($invoices_with_fee)) {
      $query->condition('mi.id', $invoices_with_fee, 'NOT IN');
    }
    $query->condition('mi.entity_type', EntityTypes::STORAGEREQUEST)
      ->condition('mi.flag', $invoice_flags, 'IN');
    $invoices = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($invoices)) {
      foreach ($invoices as $invoice) {
        $invoice['data'] = unserialize($invoice['data']);
        if (!empty($invoice['data']['charges'])) {
          foreach ($invoice['data']['charges'] as $charge) {
            if (!isset($charge['charge_type']) || $charge['charge_type'] !== RequestStorageTypeCharge::FEE_CHARGE) {
              $result[] = $invoice;
            }
          }
        }
      }
    }
    return $result;
  }

  private function checkReminder(int $invoice_id) {
    $query = db_select('request_storage_reminder', 'rsr');
    $query->innerJoin('move_invoice', 'mi', 'rsr.invoice_id=mi.id');
    $query->innerJoin('request_storage_additional', 'rsa', 'mi.entity_id=rsa.request_storage_id');
    $query->innerJoin('move_storage', 'ms', 'rsa.storage_id=ms.id');
    $query->condition('rsr.invoice_id', $invoice_id)
      ->condition('mi.entity_type', EntityTypes::STORAGEREQUEST);
    $query->fields('rsr', array('reminder1', 'reminder2', 'reminder3'));
    $query->fields('ms', array('value'));
    $reminders = $query->execute()->fetchAll();
    $result = array();
    if ($reminders) {
      $reminders = reset($reminders);
      $reminders->value = unserialize($reminders->value);
      $keys = array('reminder1', 'reminder2', 'reminder3');

      foreach ($keys as $key) {
        if (!$reminders->$key) {
          $result[$key] = $reminders->value["{$key}Days"];
          break;
        }
      }
    }

    return $result;
  }

  private function writeReminderInDb(int $invoice_id, string $reminder) {
    $id = db_update('request_storage_reminder')
      ->fields(array('created' => time(), $reminder => 1))
      ->condition('invoice_id', $invoice_id)
      ->execute();

    return $id;
  }

  private function convertDayToSecTEST(int $day, string $time) {
    if ($time == 'minute') {
      $result = $day * 60;
    }
    else {
      $result = $day * 86400;
    }
    return $result;
  }

  private function changeRecurringInPaid($invoice_id) {
    $query = db_select('move_invoice', 'mi')
      ->fields('mi')
      ->condition('entity_type', EntityTypes::STORAGEREQUEST)
      ->condition('id', $invoice_id)
      ->execute()
      ->fetchObject();

    $unserialize = unserialize($query->data);
    $unserialize['flag'] = InvoiceFlags::PAID;
    Invoice::changeInvoiceFlag($invoice_id, $unserialize);
  }

  private function changeRecurringInSent($invoice_id) {
    $query = db_select('move_invoice', 'mi')
      ->fields('mi')
      ->condition('entity_type', EntityTypes::STORAGEREQUEST)
      ->condition('id', $invoice_id)
      ->execute()
      ->fetchObject();

    $unserialize = unserialize($query->data);
    if ($unserialize['flag'] == InvoiceFlags::DRAFT) {
      $unserialize['flag'] = InvoiceFlags::SENT;
    }

    Invoice::changeInvoiceFlag($invoice_id, $unserialize);
  }

  private function calculateNotFullCharges($value, int $unit_cost, $time) {
    $next_run_on = $value->next_run_on;
    $how_many = $value->how_many;
    $how_often = $value->how_often;
    $next_full_date = $this->calcTimePeriod($next_run_on, $how_often);

    $dte_next = new \DateTime();
    $dte_next->setTimestamp($next_run_on);

    $dte_end = new \DateTime();
    $dte_end->setTimestamp($how_many);

    $dte_full = new \DateTime();
    $dte_full->setTimestamp($next_full_date);

    if ($time == 'day') {
      $interval_next_end = $dte_next->diff($dte_end);
      $next_end = $interval_next_end->days + 1;
      $interval_full = $dte_next->diff($dte_full);
      $full = $interval_full->days;
    }
    else {
      $interval_next_end = $dte_next->diff($dte_end);
      $next_end = $interval_next_end->i;
      $interval_full = $dte_next->diff($dte_full);
      $full = $interval_full->i;
    }

    $total = $unit_cost / $full * $next_end;

    return $total;
  }

  public function getUserInfoInvoice(int $rsid) {
    $query = db_select('request_storage', 'rs')
      ->fields('rs')
      ->condition('rs.id', $rsid)
      ->execute()
      ->fetchAssoc();

    $query = unserialize($query['value']);
    $result = $query['user_info'];
    $result['storage_id'] = $query['rentals']['storage'];

    return $result;
  }

  public function createInvoice(int $entity_id, $description_rec, $curDate, $unit_cost, $charge_type) {
    $move_storage_terms = '';
    $client_info = $this->getUserInfoInvoice($entity_id);
    $date = new \DateTime("@$curDate");
    $cur_date = $date->format('F d, Y H:i:s');

    if (isset($client_info['storage_id']) && $client_info['storage_id']) {
      $instance = new Storage($client_info['storage_id']);
      $move_storage = $instance->retrieve();
      $move_storage_terms = isset($move_storage['terms']) ? $move_storage['terms'] : '';
    }

    $data = array(
      'entity_id' => $entity_id,
      'entity_type' => EntityTypes::STORAGEREQUEST,
      'charges' => array(
        array(
          'charge_type' => $charge_type,
          'name' => 'Charge',
          'description' => $description_rec,
          'cost' => $unit_cost,
          'qty' => 1,
          'tax' => 0,
        ),
      ),
      'storage_invoice' => TRUE,
      'field_moving_from' => array(
        'locality' => isset($client_info['city']) ? $client_info['city'] : '',
        'administrative_area' => isset($client_info['state']) ? $client_info['state'] : '',
        'postal_code' => isset($client_info['zip']) ? $client_info['zip'] : '',
        'thoroughfare' => isset($client_info['address']) ? $client_info['address'] : '',
      ),
      'client_name' => isset($client_info['name']) ? $client_info['name'] : '',
      'phone' => isset($client_info['phone1']) ? $client_info['phone1'] : '',
      'email' => isset($client_info['email']) ? $client_info['email'] : '',
      'date' => $cur_date,
      'discount' => 0,
      'tax' => 0,
      'terms' => $move_storage_terms,
      'notes' => '',
      'flag' => 0,
      'totalInvoice' => $unit_cost,
      'total' => $unit_cost,
    );

    $invoice = new Invoice(NULL, $entity_id, EntityTypes::STORAGEREQUEST);
    $new_invoice_id = $invoice->create($data);
    return $new_invoice_id;
  }

  public function getValueStorageTerm(int $rsid, $options = array()) {
    $result = FALSE;

    $query_request = db_select('request_storage_additional', 'rsa')
      ->fields('rsa', array('storage_id'))
      ->condition('request_storage_id', $rsid)
      ->execute()
      ->fetchField();

    $query_storage = db_select('move_storage', 'ms')
      ->fields('ms', array('value'))
      ->condition('id', $query_request)
      ->execute()
      ->fetchField();

    $storage = unserialize($query_storage);

    if ($options) {
      if (in_array('reminder1Days', $options)) {
        $result = $storage['reminder1Days'];
      }
      if (in_array('reminder2Days', $options)) {
        $result = $storage['reminder2Days'];
      }
      if (in_array('reminder3Days', $options)) {
        $result = $storage['reminder3Days'];
      }
    }

    return $result;
  }

  /**
   * Function get storage requests where recurring is start.
   *
   * @return object
   *   Request where recurring on.
   */
  public static function indexStartRec() {
    $query = db_select('request_storage_additional', 'rsa')
      ->fields('rsa')
      ->condition('start_recurring', 1)
      ->execute()
      ->fetchAll();

    return $query;
  }

  /**
   * Update record in db.
   *
   * @param int $id
   *   Request storage id.
   *
   * @return int
   *    \DatabaseStatementInterface
   */
  private function changeFirstRun(int $id) {
    $query = db_update('request_storage_additional')
      ->fields(array('first_run' => 1))
      ->condition('request_storage_id', $id)
      ->execute();
    return $query;
  }

  /**
   * Update record value next_run_on.
   *
   * @param int $id
   *   Request storage id.
   *
   * @return int
   *    Result of update
   */
  private function updateNextRunOn(int $id) {
    $this->id = $id;
    $this->retrieveFlag = FALSE;
    $request_storage = $this->retrieve();
    $next_run = (int) $request_storage['recurring']['next_run_on'];
    $how_often = (int) $request_storage['recurring']['how_often'];
    $new_next_run = $this->calcTimePeriod($next_run, $how_often);
    $request_storage['recurring']['start_date'] = $request_storage['recurring']['next_run_on'];
    $request_storage['recurring']['next_run_on'] = $new_next_run;
    return $this->update($request_storage);
  }

  private function updateLastSendDate(int $id, $date) {
    db_update('request_storage_additional')
      ->fields(array('last_charge_run' => $date))
      ->condition('request_storage_id', $id)
      ->execute();
    $request_storage = $this->retrieve();
    $request_storage['recurring']['last_charge_run'] = $date;
    $this->update($request_storage);
  }

  private function updateNextRunOnFee(int $id) {
    $this->id = $id;
    $this->retrieveFlag = FALSE;
    $request_storage = $this->retrieve();
    $request_storage['recurring']['next_run_on_fee'] = $request_storage['recurring']['next_run_on'];
    return $this->update($request_storage);
  }

  private function updateLastSendFeeDate(int $id, $date) {
    db_update('request_storage_additional')
      ->fields(array('last_fee_run' => $date))
      ->condition('request_storage_id', $id)
      ->execute();
  }
  private function updateStopRecurring(int $id) {
    $this->id = $id;
    $this->retrieveFlag = FALSE;
    $request_storage = $this->retrieve();
    $request_storage['recurring']['start'] = 0;
    return $this->update($request_storage);
  }

  /**
   * Method create log for storage request.
   *
   * @param int $rsid
   *   Request storage id.
   * @param string $title
   *   Log title.
   * @param int $date
   *   Unix time date.
   */
  public function createReqStorLog($rsid, $title, $date = FALSE) {
    $data[] = array(
      'details' => $title,
      'source' => array('System'),
    );

    $create_log = new Log($rsid, 1);
    $create_log->create($data);
  }

  /**
   * Get value storage late fee.
   *
   * @param int $id
   *   Storage id.
   *
   * @return mixed
   */
  public function getLateFee(int $id) {
    $late_fee = 0;
    $query = db_select('request_storage', 'rs')
      ->fields('rs', array('value'))
      ->condition('id', $id)
      ->execute()
      ->fetchField();
    $unserialize = unserialize($query);
    if (isset($unserialize['rentals']['late_fee'])) {
      $late_fee = $unserialize['rentals']['late_fee'];
    }
    return $late_fee;
  }

  private function getLateFeeDate(int $id) {
    $apply_late_fee_in = NULL;
    $query = db_select('request_storage', 'rs')
      ->fields('rs', array('value'))
      ->condition('id', $id)
      ->execute()
      ->fetchField();
    $unserialize = unserialize($query);
    if (isset($unserialize['rentals']['apply_late_fee_in'])) {
      $apply_late_fee_in = $unserialize['rentals']['apply_late_fee_in'];
    }

    return $apply_late_fee_in;
  }
  private function getLateFeeNextRunOnDate(int $id) {
    $result = db_select('request_storage_additional', 'rsa')
      ->fields('rsa', array('next_run_on_fee'))
      ->condition('request_storage_id', $id)
      ->execute()
      ->fetchField();
    return $result;
  }

  /**
   * Return summ all charges for request storage.
   *
   * @param int $entity_id
   *   Request storage id.
   *
   * @return int
   *   Request storage charge sum.
   */
  public function requestStorageChargeBalance(int $entity_id) {
    $result = 0;
    $query = db_select('move_invoice', 'mi')
      ->fields('mi')
      ->condition('entity_id', $entity_id)
      ->condition('entity_type', EntityTypes::STORAGEREQUEST)
      ->execute()
      ->fetchAll();

    foreach ($query as $key => $value) {
      $unserialize = unserialize($value->data);
      if (isset($unserialize['total']) && $unserialize['total']) {
        $result += $unserialize['total'];
      }
    }

    return $result;
  }

  /**
   * Return difference between Payment and Charges.
   *
   * @param int $id
   *   Request storage id.
   *
   * @return int
   *   Request storage balance.
   */
  public function requestStorageBalance(int $id) {
    $chargeAmount = $this->requestStorageChargeBalance($id);
    $paymentAmount = $this->getInvoicePayments($id);

    return $paymentAmount - $chargeAmount;
  }

  /**
   * Insert in database table "request_storage_additional" record "balance".
   *
   * @param int $id
   *   Request storage id.
   *
   * @return object
   *   Result db_update.
   */
  public function insertReqStorBalance(int $id) {
    $balance = (float) $this->requestStorageBalance($id);
    $query = db_update('request_storage_additional')
      ->fields(array('balance' => $balance))
      ->condition('request_storage_id', $id)
      ->execute();
    if ($balance >= 0) {
      self::setAllStorageInvoicesStatusToPaid($id);
    }

    return $query;
  }

  /**
   * Method get request storage balance.
   *
   * @param int $id
   *   Request storage id.
   *
   * @return float
   *   Request storage balance.
   */
  public function getReqStorBalance(int $id) {
    $query = db_select('request_storage_additional', 'rsa')
      ->fields('rsa', array('balance'))
      ->condition('request_storage_id', $id)
      ->execute()
      ->fetchField();

    return $query;
  }

  public function getInvoicePayments(int $id) {
    $total_invoice = 0;

    $all_invoice = MoveRequest::getReceipt($id, EntityTypes::STORAGEREQUEST);
    foreach ($all_invoice as $value) {
      if (isset($value['refunds'])) {
        $total_invoice -= $value['amount'];
      }
      else {
        $total_invoice += $value['amount'];
      }
    }
    return $total_invoice;

  }

  /**
   * Calculate period for next_run_on.
   *
   * @param int $next_run_on
   *   Timestamp value when start next_run_on.
   * @param int $how_often
   *   Value how_often create recurring charges.
   *
   * @return int
   */
  public function calcTimePeriod(int $next_run_on, int $how_often) {
    $period = '';
    $how_often = (RequestStorageTimePeriod::isValidValue($how_often)) ? $how_often : RequestStorageTimePeriod::MOUNTH;

    switch ($how_often) {
      case RequestStorageTimePeriod::WEEK:
        $period = '+1 week';
        break;

      case RequestStorageTimePeriod::EVERY_TEN_MINUTE:
        $period = '+10 minute';
        break;

      case RequestStorageTimePeriod::THREE_WEEK:
        $period = '+3 week';
        break;

      case RequestStorageTimePeriod::MOUNTH:
        $period = '+1 month';
        break;

      case RequestStorageTimePeriod::TWO_MOUNTH:
        $period = '+2 month';
        break;

      case RequestStorageTimePeriod::THREE_MOUNTH:
        $period = '+3 month';
        break;

      case RequestStorageTimePeriod::FOUR_MOUNTH:
        $period = '+4 month';
        break;

      case RequestStorageTimePeriod::HALF_YEAR:
        $period = '+6 month';
        break;
    }

    $date = new \DateTime();
    $date->setTimezone(new \DateTimeZone('UTC'));
    $date->setTimestamp($next_run_on);
    $date->modify($period);
    $result = $date->getTimestamp();

    return $result;
  }

  public function getProfit($storage_id, $dateto, $datefrom) {
    $search_instance = new MoveRequestSearch();
    $date_to = $search_instance->prepareDate($dateto, array('midnight'));
    $date_from = $search_instance->prepareDate($datefrom, array('today'));

    $query = db_select('request_storage_additional', 'rsa');
    $query->innerJoin('moveadmin_receipt_search', 'mrs', 'rsa.request_storage_id = mrs.entity_id');
    $query->condition('rsa.storage_id', $storage_id)
      ->condition('mrs.entity_type', EntityTypes::STORAGEREQUEST)
      ->condition('mrs.date', array($date_from, $date_to), 'BETWEEN');

    $query->addExpression('SUM(mrs.amount)');
    $sum = $query->execute()->fetchCol();
    return $sum;
  }

  public function getRecurringRevenueQuery($dateto, $datefrom, $month) {
    $search_instance = new MoveRequestSearch();

    $date_to = $search_instance->prepareDate($dateto, array('midnight'));
    $date_from = $search_instance->prepareDate($datefrom, array('today'));

    $query = db_select('request_storage_additional', 'rsa');
    $query->fields('rsa');
    $query->condition('rsa.start_recurring', 1);
    $query->condition('rsa.start_point', $date_to, '<=');

    $query_or = db_or();
    $query_or->condition('rsa.how_many', 0);
    $query_or->condition('rsa.how_many', $date_from, '>=');
    $query->condition($query_or);
    $result = $query->execute()->fetchAll();

    foreach ($result as $key => &$value) {
      $result[$key]->request_client = $this->getClientRequest($value->id);
      if ($value->how_often == RequestStorageTimePeriod::WEEK) {
        $value->total_monthly *= 4;
      }
      if ($value->how_often == RequestStorageTimePeriod::TWO_MOUNTH) {
        $value->total_monthly /= 2;
      }
      if ($value->how_often == RequestStorageTimePeriod::THREE_MOUNTH) {
        $value->total_monthly /= 3;
      }
      if ($value->how_often == RequestStorageTimePeriod::FOUR_MOUNTH) {
        $value->total_monthly /= 4;
      }
      if ($value->how_often == RequestStorageTimePeriod::HALF_YEAR) {
        $value->total_monthly /= 6;
      }
      if ($value->how_many && $value->how_many <= $date_to) {
        $value->total_monthly = $this->calculateRevenue($value);
      }

    }

    return $result;
  }

  private function calculateRevenue($value) {
    $next_run_on = $value->next_run_on;
    $how_many = $value->how_many;

    $dte_end = new \DateTime();
    $dte_end->setTimestamp($how_many);

    $dte_next = new \DateTime();
    $dte_next->setTimestamp($next_run_on);

    $interval_next_end = $dte_next->diff($dte_end);
    $next_end = $interval_next_end->days + 1;

    while ($next_end >= 31) {
      $next_end -= 30;
    }

    $total = $value->total_monthly / 30 * $next_end;

    return $total;
  }

  public function getRecurringRevenue() {
    $result = array();
    $months = $this->getMounthRevenue();

    foreach ($months as $month => $value) {
      $datefrom = ($month == 'cur_month') ? $this->formatDate($value, array('current')) : $this->formatDate($value, array('first'));
      $dateto = $this->formatDate($value, array('last'));

      $result[$value] = $this->getRecurringRevenueQuery($dateto, $datefrom, $month);
    }

    return $result;
  }

  public function formatDate($value = '', $options = array()) {
    $date = new \DateTime($value);
    $date->setTimezone(new \DateTimeZone('UTC'));

    if ($options) {
      if (in_array('current', $options)) {
        $date->setTimestamp(time());
      }
      if (in_array('first', $options)) {
        $date->modify('first day of this month');
      }
      if (in_array('last', $options)) {
        $date->modify('last day of this month');
      }
      if (in_array('last_with_time', $options)) {
        $date->modify('last day of this month');
        $result = $date->format('Y-m-d') . " 23:59:59";
        return $result;
      }
    }

    return $date->format('Y-m-d');
  }

  private function getMounthRevenue() {
    $months = array();
    $date = new \DateTime();
    $date->setTimezone(new \DateTimeZone('UTC'));
    $date->setTimestamp(time());
    $months['cur_month'] = $date->format('Y-m');
    $months['second_month'] = $date->modify('first day of next month')->format('Y-m');
    $months['third_mounth'] = $date->modify('first day of next month')->format('Y-m');
    $months['last_mounth'] = $date->modify('first day of next month')->format('Y-m');

    return $months;
  }

  public function getClientRequest(int $rsid) {
    $query = db_select('request_storage', 'rs')
      ->fields('rs', array('value'))
      ->condition('id', $rsid)
      ->execute()
      ->fetchField();

    $unserialize = unserialize($query);
    return $unserialize['user_info']['name'];
  }

  public function getReceiptsForStorage($dateto, $datefrom) {
    $result = array();
    $receipt = array();
    $search_instance = new MoveRequestSearch();
    $date_to = $search_instance->prepareDate($dateto, array('midnight'));
    $date_from = $search_instance->prepareDate($datefrom, array('today'));

    $storages = Storage::getAllStorages();
    foreach ($storages as $key => $storage) {
      $query = db_select('request_storage_additional', 'rsa');
      $query->innerJoin('moveadmin_receipt', 'mr', 'rsa.request_storage_id = mr.entity_id');
      $query->condition('rsa.storage_id', $storage)
        ->condition('mr.entity_type', EntityTypes::STORAGEREQUEST)
        ->condition('mr.created', array($date_from, $date_to), 'BETWEEN');
      $query->fields('mr');
      $query->fields('rsa', array('move_request_id'));

      $receipt[$storage] = $query->execute()->fetchAll();
    }

    foreach ($receipt as $key => $value) {
      foreach ($value as $key2 => $item) {
        $result[$item->id] = array(
          'id' => $item->id,
          'entity_id' => $item->entity_id,
          'receipt' => unserialize($item->receipt),
          'entity_type' => $item->entity_type,
          'created' => $item->created,
          'move_request_id' => $item->move_request_id,
        );
      }
    }

    return $result;
  }

  /**
   * Set all storage invoices status to paid.
   *
   * Work for all invoices statuses except DRAFT.
   *
   * @param int $storage_id
   *   Id of the storage.
   */
  public static function setAllStorageInvoicesStatusToPaid(int $storage_id) : void {
    $invoice_instance = new Invoice();
    $storage_invoices = $invoice_instance->getNotPaidInvoiceForEntity($storage_id, EntityTypes::STORAGEREQUEST);
    if (!empty($storage_invoices)) {
      foreach ($storage_invoices as $invoice) {
        $invoice['data']['flag'] = InvoiceFlags::PAID;
        $invoice['data']['entity_id'] = $invoice['entity_id'];
        $invoice['data']['entity_type'] = $invoice['entity_type'];
        Invoice::changeInvoiceFlag($invoice['id'], $invoice['data']);
      }
    }
  }

  /**
   * Get all additional info for storage request.
   *
   * @param int $storage_id
   *   Id of the storage.
   *
   * @return mixed
   *   Array of storage info.
   */
  public static function getAllAdditionalInfo(int $storage_id) {
    $result = db_select('request_storage_additional', 'rsa')
      ->fields('rsa')
      ->condition('request_storage_id', $storage_id)
      ->execute()
      ->fetchAssoc(\PDO::FETCH_ASSOC);
    return $result;
  }

  public static function migrateToNewStorageCode() {
    $query = db_select('request_storage_additional', 'rsa');
    $query->leftJoin('request_storage', 'rs', 'rsa.request_storage_id = rs.id');
    $query->fields('rs', array('value'));
    $query->fields('rsa');
    $storages = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    if (!empty($storages)) {
      foreach ($storages as $storage) {
        $storage['value'] = unserialize($storage['value']);
        $next_tun_on = $storage['next_run_on'];
        $start_date = $storage['start_date'];
        $first_run = $storage['first_run'];
        $start_recurring = $storage['start_recurring'];
        if (!empty($start_recurring) && $start_date != $next_tun_on) {
          db_update('request_storage_additional')
            ->fields(array('last_charge_run' => $start_date))
            ->condition('request_storage_id', $storage['request_storage_id'])
            ->execute();
          $storage['value']['recurring']['last_charge_run'] = $start_date;
          $storage['value']['started'] = TRUE;
          db_update('request_storage')
            ->fields(array('value' => serialize($storage['value'])))
            ->condition('id', $storage['request_storage_id'])
            ->execute();
        }

        if (empty($first_run) && $start_date != $next_tun_on) {
          db_update('request_storage_additional')
            ->fields(array('first_run' => 1))
            ->condition('request_storage_id', $storage['request_storage_id'])
            ->execute();
        }
      }
    }
  }

  public static function addFeeMarkToAllNotPaidInvoices() {
    $invoices = db_select('move_invoice', 'mi')
      ->fields('mi', array('id', 'entity_id', 'data'))
      ->condition('entity_type', EntityTypes::STORAGEREQUEST)
      ->condition('flag', InvoiceFlags::PAID, '<>')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
    if (!empty($invoices)) {
      foreach ($invoices as $invoice) {
        $invoice['data'] = unserialize($invoice['data']);
        // Add fee to invoice.
        db_insert('move_invoice_fee')
          ->fields(array(
            'invoice_id' => $invoice['id'],
            'entity_id' => $invoice['entity_id'],
            'entity_type' => EntityTypes::STORAGEREQUEST,
            'fee_amount' => (float) $invoice['data']['total'],
            'created' => time(),
          ))
          ->execute();
      }
    }
  }

}

/**
 * Class cronRecurringException.
 *
 * @package Drupal\move_storage\Services
 */
class cronRecurringException extends \RuntimeException {

  public function saveDataBase() {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()}";
    watchdog('cron_recurring', $message, array(), WATCHDOG_WARNING);
  }

}
