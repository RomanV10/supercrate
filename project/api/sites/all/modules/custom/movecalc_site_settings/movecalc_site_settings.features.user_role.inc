<?php
/**
 * @file
 * movecalc_site_settings.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function movecalc_site_settings_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
    'machine_name' => 'administrator',
  );

  // Exported role: customer service.
  $roles['customer service'] = array(
    'name' => 'customer service',
    'weight' => 8,
    'machine_name' => 'customer_service',
  );

  // Exported role: driver.
  $roles['driver'] = array(
    'name' => 'driver',
    'weight' => 5,
    'machine_name' => 'driver',
  );

  // Exported role: foreman.
  $roles['foreman'] = array(
    'name' => 'foreman',
    'weight' => 7,
    'machine_name' => 'foreman',
  );

  // Exported role: helper.
  $roles['helper'] = array(
    'name' => 'helper',
    'weight' => 6,
    'machine_name' => 'helper',
  );

  // Exported role: manager.
  $roles['manager'] = array(
    'name' => 'manager',
    'weight' => 3,
    'machine_name' => 'manager',
  );

  // Exported role: sales.
  $roles['sales'] = array(
    'name' => 'sales',
    'weight' => 4,
    'machine_name' => 'sales',
  );

  return $roles;
}
