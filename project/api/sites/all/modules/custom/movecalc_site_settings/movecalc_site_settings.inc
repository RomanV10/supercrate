<?php

use Drupal\move_services_new\Services\Settings;
use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_services_new\Services\Payment;

function movecalc_site_settings_custom() {
  $form = array();

  $linklist = array(
    'item1' => array('title' => 'Save default settings', 'href' => 'admin/config/custom/settings/save'),
    'item2' => array('title' => 'Restore default settings', 'href' => 'admin/config/custom/settings/restore'),
  );
  $form['links'] = array(
    '#markup' => theme('links', array('links' => $linklist)),
    '#weight' => 0,
  );

  $authorize = Payment::getAuthorizenet();
  $login_id = $authorize['loginid'];
  $trans_key = $authorize['transactionkey'];

  $form['authnet_login_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Authnet Login ID'),
    '#default_value' => $login_id,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#weight' => 1,
  );

  $form['authnet_transaction_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Authnet Transaction key'),
    '#default_value' => $trans_key,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#weight' => 2,
  );

  $form['dev_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Default email for dev environment.'),
    '#default_value' => variable_get('movecalc_mail', 'stagethemoveboard@gmail.com'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#weight' => 3,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 50,
  );

  $form['#submit'][] = 'movecalc_site_settings_custom_submit';
  return $form;
}

function movecalc_site_settings_custom_submit($form, $form_state) {
  Payment::setAuthorizenet($form_state['values']['authnet_login_id'], $form_state['values']['authnet_transaction_key']);

  $environments = (array) variable_get('environment', array());
  if ($environments['default'] != 'production') {
    variable_set('movecalc_mail', $form_state['values']['dev_email']);
  }
}

function movecalc_site_settings_custom_cache() {
  $linklist = array(
    'item1' => array('title' => 'Flush all services cache.', 'href' => 'admin/config/custom/cache/flush_services_cache'),
    'item2' => array('title' => 'Flush all services admin cache.', 'href' => 'admin/config/custom/cache/flush_services_admin_cache'),
    'item3' => array('title' => 'Full reindexation Payroll cache.', 'href' => 'admin/config/custom/cache/payroll'),
    'item4' => array('title' => 'Full reindexation Move Request Statistics cache.', 'href' => 'admin/config/custom/cache/move_request_statistics'),
    'item5' => array('title' => 'Update move_request cache.', 'href' => 'admin/config/custom/cache/update_move_request_cache'),
  );

  return theme('links', array('links' => $linklist));
}

/**
 * Callback for crear cache.
 */
function movecalc_site_settings_flush_services_cache($table) {
  db_truncate($table)->execute();
  drupal_set_message(t('Services cache was cleared.'));

  $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
  drupal_goto($url);
}

function movecalc_site_settings_save_default() {
  $result = Settings::setDefaultSettings();
  if ($result) {
    drupal_set_message(t('Default settings was updated.'));
  }
  else {
    drupal_set_message(t('Something went wrong.'));
  }

  $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
  drupal_goto($url);
}

function movecalc_site_settings_restore_default() {
  Settings::restoreSetting();
  drupal_set_message(t('Settings was restore.'));
  $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
  drupal_goto($url);
}

function movecalc_site_settings_comments() {
  $linklist = array(
    'item1' => array('title' => 'Fixing bugs in comments', 'href' => 'admin/config/custom/comments/fix'),
  );

  return theme('links', array('links' => $linklist));
}

function movecalc_site_settings_fixing_comments() {
  $query = db_query("SELECT move_request_comments.cid FROM move_request_comments LEFT JOIN node ON move_request_comments.nid=node.nid WHERE node.nid IS NULL;");
  $cids = $query->fetchCol();
  if ($cids) {
    db_delete('move_request_comments')
      ->condition('cid', array((array) $cids))
      ->execute();
    drupal_set_message(t('Removed orphan comments'));
  }

  $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
  drupal_goto($url);
}

function movecalc_site_settings_long_distance() {
  $form = array();

  $linklist = array(
    'item1' => array('title' => 'Fixing long distance variable.', 'href' => 'admin/config/custom/ls/fix'),
  );
  $form['links'] = array(
    '#markup' => theme('links', array('links' => $linklist)),
  );

  $form['google_direction_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google direction ley'),
    '#default_value' => variable_get('google_directions_key', 'AIzaSyAqJtLkKqgyjyL7sV-5gQbKC_IBfJV2Ge0'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#submit'][] = 'movecalc_site_settings_g_dir_key_submit';
  return $form;
}

function movecalc_site_settings_long_distance_fix() {
  $ld = json_decode(variable_get('longdistance', array()), TRUE);
  unset($ld['undefined']);
  $fl_array = preg_grep("/^([a-z]{2})/i", $ld);
  // Monkey fix.
  unset($fl_array['stateRates']);
  foreach ($fl_array as $key => $item) {
    $ld['stateRates']['MA'][$key] = $item;
    unset($ld[$key]);
  }

  variable_set('longdistance', json_encode($ld));
  $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
  drupal_goto($url);
}

function movecalc_site_settings_g_dir_key_submit($form, $form_state) {
  DistanceCalculate::setGoogleDirectionsKey($form_state['values']['google_direction_key']);
}

function movecalc_site_settings_all_fixes() {
  $form = array();

  $linklist = array(
    'item1' => array('title' => 'Fixing move_mail_folders "Numeric value out of range: 1264 Out of range value for column \'uid\'"', 'href' => 'admin/config/custom/fix/mail_folders'),
  );

  $form['links'] = array(
    '#markup' => theme('links', array('links' => $linklist)),
  );

  return $form;
}

function movecalc_site_settings_fix_mail_folders() {
  $schema = drupal_get_schema_unprocessed('move_mail');

  if (db_table_exists('move_mail_messages')) {
    db_change_field('move_mail_messages', 'user_id', 'user_id', $schema['move_mail_messages']['fields']['user_id']);
  }
  if (db_table_exists('move_mail_folders')) {
    db_change_field('move_mail_folders', 'uid', 'uid', $schema['move_mail_folders']['fields']['uid']);
  }
  if (db_table_exists('move_mail_files')) {
    db_change_field('move_mail_files', 'uid', 'uid', $schema['move_mail_files']['fields']['uid']);
  }

  $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
  drupal_goto($url);
}
