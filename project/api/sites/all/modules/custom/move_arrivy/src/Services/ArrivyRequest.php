<?php

namespace Drupal\move_arrivy\Services;

use Drupal\move_new_log\Services\Log;
use DateTime;
use DateTimeZone;
use DateInterval;
use PDO;
use Drupal\move_services_new\Services\Clients;

/**
 * Class ArrivyRequest.
 *
 * @package Drupal\move_arrivy\Services
 */
class ArrivyRequest {

  const CREATEURL = 'https://www.arrivy.com/api/tasks/new';
  const REQUESTAUTOLOGINURL = 'https://www.arrivy.com/authlogin?';
  const UPDATEDELETEURL = 'https://www.arrivy.com/api/tasks/';

  private $nid;

  /**
   * ArrivyRequest constructor.
   *
   * @param int|null $nid
   *   Move request id.
   */
  public function __construct($nid = NULL) {
    $this->nid = $nid;
  }

  /**
   * Method to create multiple confirmed requests on connection.
   *
   * @return string
   *   Returns the result message.
   *
   * @throws \Exception
   */
  public static function createMultiple() {
    // First get all confirmed requests from tomorrow and on.
    $confirmed_requests = static::getConfirmedRequestsInfoQuery();
    if (!empty($confirmed_requests)) {
      // Go over each of the requests and try to create a task in Arrivy.
      $post_error_ids = array();
      foreach ($confirmed_requests as $request) {
        $arrivy_request_id = static::getArrivyRequestIdQuery($request['nid']);
        if (empty($arrivy_request_id)) {
          $arrivy_client_id = ArrivyClient::create($request['nid'], $request['uid'], $request['fname'], $request['lname'], $request['phone'], $request['email']);
          if (!empty($arrivy_client_id)) {
            $result = static::createArrivyRequest($request['nid'], $arrivy_client_id, $request);
            if (empty($result)) {
              $post_error_ids[] = $request['nid'];
            }
          }
          else {
            $post_error_ids[] = $request['nid'];
            watchdog('move_arrivy', 'Request !request_id was not created in Arrivy because we could not create or get client arrivy id', array('!request_id' => $request['nid']), WATCHDOG_ERROR);
          }
        }
      }
      if (!empty($post_error_ids)) {
        $request_ids_not_posted = implode(" ,", $post_error_ids);
        watchdog('move_arrivy', 'Requests !request_ids were not posted to Arrivy on connect', array('!request_ids' => $request_ids_not_posted), WATCHDOG_ERROR);
        return 'Requests $request_ids_not_posted were not posted to Arrivy';
      }
      return 'All confirmed requests has been sent to Arrivy. All good.';
    }
    else {
      return 'All confirmed requests has been sent to Arrivy. All good.';
    }
  }

  /**
   * Primary method for creating/updating a new request in Arrivy.
   *
   * Initiated when request becomes confirmed.
   *
   * @return int|bool
   *   Returns new Arrivy request ID
   *   if successfully created or updated, else returns false.
   *
   * @throws \Exception
   */
  public function create() {
    if (ArrivyConnection::connectedToArrivy()) {
      $request = static::getRequestInfoQuery($this->nid);
      $arrivy_request_id = static::getArrivyRequestIdQuery($this->nid);
      // Get client arrivy id.
      $client_arrivy_id = ArrivyClient::create($this->nid, $request['client_id'], $request['fname'], $request['lname'], $request['phone'], $request['email']);
      if (!empty($client_arrivy_id)) {
        if (empty($arrivy_request_id)) {
          return static::createArrivyRequest($this->nid, $client_arrivy_id, $request);
        }
        else {
          return static::updateArrivyRequest($this->nid, $arrivy_request_id[0], $client_arrivy_id, $request);
        }
      }
      else {
        watchdog('move_arrivy', 'Request !request_id not created because we could not get or create client arrivy id', array('!request_id' => $this->nid), WATCHDOG_ERROR);
        return FALSE;
      }
    }
  }

  /**
   * Primary method that returns request tracking URL.
   *
   * @return array|bool
   *   Returns tracking url of the request in Arrivy, else returns false.
   */
  public function retrieve() {
    global $user;
    if (ArrivyConnection::connectedToArrivy()) {
      $auth_key = variable_get('arrivy_api_auth_key', FALSE);
      $auth_token = variable_get('arrivy_api_auth_token', FALSE);
      $request_arrivy_id = static::getArrivyRequestIdQuery($this->nid);
      if (!empty($request_arrivy_id)) {
        $superRole = (new Clients($user->uid))->checkUserRoles(['manager', 'administrator']);
        if (!$superRole) {
          $sales_entity_id = variable_get('arrivy_api_sales_id', FALSE);
        }
        if (!empty($request_arrivy_id[1])) {
          $result = array(
            self::REQUESTAUTOLOGINURL . "auth_key={$auth_key}&auth_token={$auth_token}" . (isset($sales_entity_id) ? "&entity_id={$sales_entity_id}" : "") . "&ru=/tasks/{$request_arrivy_id[0]}",
            self::REQUESTAUTOLOGINURL . "auth_key={$auth_key}&auth_token={$auth_token}" . (isset($sales_entity_id) ? "&entity_id={$sales_entity_id}" : "") . "&ru=/tasks/'{$request_arrivy_id[1]}",
          );
        }
        else {
          $result = array(
            self::REQUESTAUTOLOGINURL . "auth_key={$auth_key}&auth_token={$auth_token}" . (isset($sales_entity_id) ? "&entity_id={$sales_entity_id}" : "") . "&ru=/tasks/{$request_arrivy_id[0]}",
          );
        }

        return $result;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Primary method that for assigning foremans to a request.
   *
   * @param int $foreman_id
   *   Foreman User ID.
   * @param int $delivery_foreman_id
   *   Delivery Foreman User ID if flat rate.
   *
   * @return string
   *   Returns success or fail message after assigning foreman.
   */
  public function assignForeman($foreman_id, $delivery_foreman_id) {
    if (ArrivyConnection::connectedToArrivy()) {
      $arrivy_request_id = static::getArrivyRequestIdQuery($this->nid);
      if (!empty($arrivy_request_id)) {
        $foreman_arrivy_id = ArrivyForeman::getArrivyUidQuery($foreman_id);
        if (!empty($foreman_arrivy_id)) {
          $autologin_url = static::generateForemanLoginHash($foreman_id, $this->nid);
          $delivery_foreman_arrivy_id = NULL;
          $delivery_arrivy_request_id = NULL;
          $delivery_autologin_url = NULL;
          if (isset($delivery_foreman_id)) {
            $delivery_foreman_arrivy_id = ArrivyForeman::getArrivyUidQuery($delivery_foreman_id);
            if (!empty($delivery_foreman_arrivy_id)) {
              $delivery_autologin_url = static::generateForemanLoginHash($delivery_foreman_id, $this->nid);
            }
            if (count($arrivy_request_id) == 2) {
              $delivery_arrivy_request_id = $arrivy_request_id[1];
            }
          }
          return static::updateArrivyRequestForeman($this->nid, $arrivy_request_id[0], $delivery_arrivy_request_id, $foreman_arrivy_id, $autologin_url, $delivery_foreman_arrivy_id, $delivery_autologin_url);
        }
        else {
          watchdog('move_arrivy', 'Foreman !foreman_id is not in Arrivy to assign (assignForeman)', array('!foreman_id' => $foreman_id), WATCHDOG_INFO);
          return 'Current Foreman is not in Arrivy to be assigned to a Request $this->nid';
        }
      }
      else {
        watchdog('move_arrivy', 'Request !request_id is not in Arrivy to assign foreman (assignForeman)', array('!request_id' => $arrivy_request_id[0]), WATCHDOG_INFO);
        return 'Request $this->nid is not in Arrivy';
      }
    }
  }

  /**
   * Primary method that for unassigning foremans from a request.
   *
   * @return string
   *   Returns success or fail message after unassigning a foreman.
   */
  public function unassignForeman() {
    if (ArrivyConnection::connectedToArrivy()) {
      $delivery_arrivy_request_id = NULL;
      $arrivy_request_id = static::getArrivyRequestIdQuery($this->nid);
      if (!empty($arrivy_request_id)) {
        if (count($arrivy_request_id) == 2) {
          $delivery_arrivy_request_id = $arrivy_request_id[1];
        }
        return static::deleteArrivyRequestForeman($this->nid, $arrivy_request_id[0], $delivery_arrivy_request_id);
      }
      else {
        watchdog('move_arrivy', 'Request !request_id is not in Arrivy to unassign foreman', array('!request_id' => $arrivy_request_id[0]), WATCHDOG_ERROR);
        return 'Request $this->nid is not in Arrivy';
      }
    }
  }

  /**
   * Primary method for creating a new task status in Arrivy.
   *
   * @param string $status_type
   *   Status Type (STARTED OR COMPLETED)
   *
   * @return string
   *   Returns success or fail message.
   */
  public function createTaskStatus($status_type) {
    if (ArrivyConnection::connectedToArrivy()) {
      $arrivy_request_id = static::getArrivyRequestIdQuery($this->nid);
      if (!empty($arrivy_request_id)) {
        return static::createNewTaskStatus($arrivy_request_id[0], $status_type, $this->nid);
      }
      else {
        watchdog('move_arrivy', 'Record for request id !request_id doesnt exist in our drupal database', array('!request_id' => $arrivy_request_id[0]), WATCHDOG_ERROR);
        return 'Unable to post status for your Arrivy request';
      }
    }
    else {
      return 'Not connected to Arrivy';
    }
  }

  /**
   * Supporting method for сalculating start and end times.
   *
   * @param string $move_date
   *   Move Date.
   * @param int $min_start_time
   *   Minimum start time.
   * @param string $timezone
   *   Timezone.
   * @param int $max_work_time
   *   Maximum work time.
   * @param int $travel_time
   *   Travel time.
   *
   * @return array
   *   Returns an array with start datetime and end datetime.
   *
   * @throws \Exception
   */
  public static function calculateStartEndTime($move_date, $min_start_time, $timezone, $max_work_time, $travel_time) {
    $start_datetime = substr($move_date, 0, strrpos($move_date, ' ')) . ' ' . $min_start_time;
    $formatted_start_datetime = new DateTime($start_datetime, new DateTimeZone($timezone));
    $start_datetime = $formatted_start_datetime->format('c');
    $work_time = 3600 * ($max_work_time + $travel_time);
    $end_datetime = $formatted_start_datetime->add(new DateInterval('PT' . $work_time . 'S'))->format('c');
    return array(
      'start_datetime' => $start_datetime,
      'end_datetime' => $end_datetime,
    );
  }

  /**
   * Supporting method to correct status time with timezones.
   *
   * @param string $mb_timezone
   *   MoveBoard Timezone.
   *
   * @return string
   *   Returns string formatted status time.
   */
  public static function getTaskStatusTime($mb_timezone) {
    $status_time = new DateTime('now');
    $status_time->setTimezone(new DateTimeZone($mb_timezone));
    return $status_time->format('c');
  }

  /**
   * Supporting method to generate foreman hash url.
   *
   * @param int $foreman_id
   *   Foreman User ID.
   * @param int $request_id
   *   Request ID.
   *
   * @return string
   *   Returns foreman login hash url or error message.
   */
  public static function generateForemanLoginHash($foreman_id, $request_id) {
    if ($foreman_id) {
      $basic_settings = json_decode(variable_get('basicsettings', array()));
      $foreman_object = user_load($foreman_id);
      $hash_data = Clients::prepareOneTimeHashParams($foreman_object);
      $foreman_url = "{$basic_settings->client_page_url}/account/#/request/{$request_id}/contract?uid={$hash_data['uid']}&timestamp={$hash_data['timestamp']}&hash={$hash_data['hash']}";
      return $foreman_url;
    }
    else {
      watchdog('move_arrivy', 'No foreman ID passed to generate foreman login hash for foreman with id !foreman_id', array('!foreman_id' => $foreman_id), WATCHDOG_ERROR);
    }
  }

  /**
   * Supporting method to create request logs.
   *
   * @param string $title_text
   *   Log title text.
   * @param string $primary_text
   *   Log description text.
   * @param int $moveboard_request_id
   *   MoveBoard Request ID.
   * @param int $moveboard_request_type
   *   Request type.
   */
  public static function saveArrivyLogs($title_text, $primary_text, $moveboard_request_id, $moveboard_request_type = NULL) {
    $arrivyLog = new Log($moveboard_request_id, $moveboard_request_type);
    $log_data[] = array(
      'details' => array(
        [
          'activity' => 'System',
          'title' => $title_text,
          'text' => array(
            [
              'text' => $primary_text,
            ],
          ),
          'date' => time(),
        ],
      ),
      'source' => 'System',
    );
    $arrivyLog->create($log_data);
  }

  /**
   * Supporting API method to post an Arrivy request.
   *
   * @param int $request_id
   *   Request ID.
   * @param int $client_arrivy_id
   *   Customer Arrivy ID.
   * @param array $request
   *   Array containing request info.
   *
   * @return int|bool
   *   Returns request ID or false.
   *
   * @throws \Exception
   */
  public static function createArrivyRequest($request_id, $client_arrivy_id, $request) {
    $addresses = [];
    $create_flat_rate_delivery = FALSE;
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    // Get moveboard timezone from dot_env.
    $mb_timezone = getenv('MOVE_GOOGLE_CALENDAR_TIMEZONE');
    if (!empty($mb_timezone)) {
      // Prepare information to send.
      $start_end_time = static::calculateStartEndTime($request['move_date'], $request['min_start_time'], $mb_timezone, $request['max_move_time'], $request['travel_time']);
      $start_time_window = (strtotime($request['max_start_time']) - strtotime($request['min_start_time'])) / 60;
      // Sms and email notification.
      $notifications = array(
        'sms' => variable_get('arrivy_api_sms_notification', FALSE),
        'email' => variable_get('arrivy_api_email_notification', FALSE),
      );
      // Data information.
      $data = array(
        'title' => "Request #{$request_id}",
        'start_datetime' => $start_end_time['start_datetime'],
        'end_datetime' => $start_end_time['end_datetime'],
        'customer_id' => $client_arrivy_id,
        'customer_first_name' => $request['fname'],
        'customer_last_name' => $request['lname'],
        'customer_email' => $request['email'],
        'customer_mobile_number' => $request['phone'],
        'customer_address_line_1' => $request['moving_from_street'],
        'customer_address_line_2' => $request['apt_from'],
        'customer_city' => $request['moving_from_city'],
        'customer_state' => $request['moving_from_state'],
        'customer_zipcode' => $request['moving_from_zip'],
        'customer_country' => 'USA',
        'source' => 'MoveBoard',
        'source_id' => $request_id,
        'enable_time_window_display' => 'true',
        'time_window_start' => $start_time_window,
        'use_assignee_color' => 'true',
        'notifications' => json_encode($notifications),
      );
      // If service type unloading help, make moving to address Primary.
      if ($request['move_service_type'] == 4) {
        $data['customer_address_line_1'] = $request['moving_to_street'];
        $data['customer_address_line_2'] = $request['apt_to'];
        $data['customer_city'] = $request['moving_to_city'];
        $data['customer_state'] = $request['moving_to_state'];
        $data['customer_zipcode'] = $request['moving_to_zip'];
      }
      // If service type storage move, make moving to address Primary.
      if ($request['move_service_type'] == 2 && !empty($request['storage_to_move'])) {
        $data['customer_address_line_1'] = $request['moving_to_street'];
        $data['customer_address_line_2'] = $request['apt_to'];
        $data['customer_city'] = $request['moving_to_city'];
        $data['customer_state'] = $request['moving_to_state'];
        $data['customer_zipcode'] = $request['moving_to_zip'];
      }
      // If service type overnight, make moving to address Primary.
      if ($request['move_service_type'] == 6 && !empty($request['storage_to_move'])) {
        $data['customer_address_line_1'] = $request['moving_to_street'];
        $data['customer_address_line_2'] = $request['apt_to'];
        $data['customer_city'] = $request['moving_to_city'];
        $data['customer_state'] = $request['moving_to_state'];
        $data['customer_zipcode'] = $request['moving_to_zip'];
      }
      // If service type is flat rate long distance version,
      // create two requests for pickup and for delivery.
      if ($request['move_service_type'] == 5 && $request['flat_rate_local_move_val'] == 0 && isset($request['flat_delivery_date'])) {
        $data['title'] = "Request #{$request_id} Pickup";
        $create_flat_rate_delivery = TRUE;
      }
      // Add extra pickup.
      if (!empty($request['extra_pickup_zip'])) {
        $addresses[] = array(
          'title' => 'Extra Pickup',
          'address_line_1' => $request['extra_pickup_street'],
          'address_line_2' => $request['extra_pickup_apt'],
          'city' => $request['extra_pickup_city'],
          'state' => $request['extra_pickup_state'],
          'country' => 'USA',
          'zipcode' => $request['extra_pickup_zip'],
          'exact_location' => '',
          'complete_address' => $request['extra_pickup_street'] . ' ' . $request['extra_pickup_apt'] . ', ' . $request['extra_pickup_city'] . ' ' . $request['extra_pickup_state'] . ' ' . $request['extra_pickup_zip'],
        );
      }
      // Add extra dropoff if not creating second request for flat rate
      // long distance.
      if (!empty($request['extra_dropoff_zip']) && !$create_flat_rate_delivery) {
        $addresses[] = array(
          'title' => 'Extra Dropoff',
          'address_line_1' => $request['extra_dropoff_street'],
          'address_line_2' => $request['extra_dropoff_apt'],
          'city' => $request['extra_dropoff_city'],
          'state' => $request['extra_dropoff_state'],
          'country' => 'USA',
          'zipcode' => $request['extra_dropoff_zip'],
          'exact_location' => '',
          'complete_address' => $request['extra_dropoff_street'] . ' ' . $request['extra_dropoff_apt'] . ', ' . $request['extra_dropoff_city'] . ' ' . $request['extra_dropoff_state'] . ' ' . $request['extra_dropoff_zip'],
        );
      }
      // If service type is not overnight, moving & storage,
      // loading or unloading help, flat rate long distance,
      // do not add destination address.
      if ($request['move_service_type'] != 2 && $request['move_service_type'] != 6 && $request['move_service_type'] != 3 && $request['move_service_type'] != 4 && !$create_flat_rate_delivery) {
        $addresses[] = array(
          'title' => 'Destination Address',
          'address_line_1' => $request['moving_to_street'],
          'address_line_2' => $request['apt_to'],
          'city' => $request['moving_to_city'],
          'state' => $request['moving_to_state'],
          'country' => 'USA',
          'zipcode' => $request['moving_to_zip'],
          'exact_location' => '',
          'complete_address' => $request['moving_to_street'] . ' ' . $request['apt_to'] . ', ' . $request['moving_to_city'] . ' ' . $request['moving_to_state'] . ' ' . $request['moving_to_zip'],
        );
      }
      $data['additional_addresses'] = json_encode($addresses);
      // Foreman and client notes.
      if (!empty($request['foreman_notes'])) {
        $data['details'] = strip_tags($request['foreman_notes']);
      }
      if (!empty($request['client_notes'])) {
        $data['customer_notes'] = strip_tags($request['client_notes']);
      }
      // Http_request options.
      $options = array(
        'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
        'method' => 'POST',
        'timeout' => 30,
        'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
      );
      // Make a call.
      $response = drupal_http_request(self::CREATEURL, $options);

      // Work with call response.
      if ($response->code == 200) {
        $data = json_decode($response->data);
        static::saveRequestIdQuery($request_id, $data->id, $data->url_safe_id, $client_arrivy_id);
        static::saveArrivyLogs("New request created in Arrivy", "Request #{$request_id} has been created in your Arrivy account", $request_id, 0);
        // Create a delivery request if isset.
        if ($create_flat_rate_delivery) {
          static::createFlatRateDelivery($auth_key, $auth_token, $request_id, $client_arrivy_id, $request, $mb_timezone);
        }
        return $data->id;
      }
      else {
        watchdog('move_arrivy', "$response->message when posting !request_id", array('!request_id' => $request_id), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    else {
      watchdog('move_arrivy', 'No enivronment variable called MOVE_GOOGLE_CALENDAR_TIMEZONE exists in the system', array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Supporting API method to post an Arrivy delivery request.
   *
   * @param string $auth_key
   *   Authorization key.
   * @param string $auth_token
   *   Authorization token.
   * @param int $request_id
   *   Request ID.
   * @param int $client_arrivy_id
   *   Client Arrivy ID.
   * @param array $request
   *   Request info.
   * @param string $timezone
   *   Timezone.
   *
   * @return int|bool
   *   Returns request ID or false.
   *
   * @throws \Exception
   */
  public static function createFlatRateDelivery($auth_key, $auth_token, $request_id, $client_arrivy_id, $request, $timezone) {
    $start_datetime = substr($request['flat_delivery_date'], 0, strrpos($request['flat_delivery_date'], ' ')) . ' ' . $request['flat_delivery_start_time'];
    $formatted_start_datetime = new DateTime($start_datetime, new DateTimeZone($timezone));
    $start_datetime = $formatted_start_datetime->format('c');
    $start_time_window = (strtotime($request['flat_delivery_end_time']) - strtotime($request['flat_delivery_start_time'])) / 60;
    // Sms and email notification.
    $notifications = array(
      'sms' => variable_get('arrivy_api_sms_notification', FALSE),
      'email' => variable_get('arrivy_api_email_notification', FALSE),
    );
    $data = array(
      'title' => 'Request #{$request_id} Delivery',
      'start_datetime' => $start_datetime,
      'customer_id' => $client_arrivy_id,
      'customer_first_name' => $request['fname'],
      'customer_last_name' => $request['lname'],
      'customer_email' => $request['email'],
      'customer_mobile_number' => $request['phone'],
      'customer_address_line_1' => $request['moving_to_street'],
      'customer_address_line_2' => $request['apt_to'],
      'customer_city' => $request['moving_to_city'],
      'customer_state' => $request['moving_to_state'],
      'customer_zipcode' => $request['moving_to_zip'],
      'customer_country' => 'USA',
      'source' => 'MoveBoard',
      'source_id' => $request_id,
      'enable_time_window_display' => 'true',
      'time_window_start' => $start_time_window,
      'use_assignee_color' => 'true',
      'notifications' => json_encode($notifications),
    );
    // Add extra dropoff.
    if (!empty($request['extra_dropoff_zip'])) {
      $addresses[] = array(
        'title' => 'Extra Dropoff',
        'address_line_1' => $request['extra_dropoff_street'],
        'address_line_2' => $request['extra_dropoff_apt'],
        'city' => $request['extra_dropoff_city'],
        'state' => $request['extra_dropoff_state'],
        'country' => 'USA',
        'zipcode' => $request['extra_dropoff_zip'],
        'exact_location' => '',
        'complete_address' => $request['extra_dropoff_street'] . ' ' . $request['extra_dropoff_apt'] . ', ' . $request['extra_dropoff_city'] . ' ' . $request['extra_dropoff_state'] . ' ' . $request['extra_dropoff_zip'],
      );
      $data['additional_addresses'] = json_encode($addresses);
    }
    // Foreman and client notes.
    if (!empty($request['foreman_notes'])) {
      $data['details'] = strip_tags($request['foreman_notes']);
    }
    if (!empty($request['client_notes'])) {
      $data['customer_notes'] = strip_tags($request['client_notes']);
    }
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'POST',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::CREATEURL, $options);
    if ($response->code == 200) {
      $data = json_decode($response->data);
      static::saveRequestIdQuery($request_id, $data->id, $data->url_safe_id, $client_arrivy_id);
      static::saveArrivyLogs("New delivery request created in Arrivy", "Request #{$request_id} has been created in your Arrivy account", $request_id, 0);
      return $data->id;
    }
    else {
      watchdog('move_arrivy', "$response->message when creating delivery for flat rate !request_id", array('!request_id' => $request_id), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Supporting API method to update an Arrivy request.
   *
   * @param int $request_id
   *   Request ID.
   * @param int $request_arrivy_id
   *   Request Arrivy ID.
   * @param int $client_arrivy_id
   *   Client Arrivy ID.
   * @param array $request
   *   Request info array.
   *
   * @return int|bool
   *   Returns request ID or false.
   *
   * @throws \Exception
   */
  public static function updateArrivyRequest($request_id, $request_arrivy_id, $client_arrivy_id, $request) {
    $addresses = [];
    $update_flat_rate_delivery = FALSE;
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    $mb_timezone = getenv('MOVE_GOOGLE_CALENDAR_TIMEZONE');
    if (!empty($mb_timezone)) {
      $start_end_time = static::calculateStartEndTime($request['move_date'], $request['min_start_time'], $mb_timezone, $request['max_move_time'], $request['travel_time']);
      $start_time_window = (strtotime($request['max_start_time']) - strtotime($request['min_start_time'])) / 60;
      $data = array(
        'start_datetime' => $start_end_time['start_datetime'],
        'end_datetime' => $start_end_time['end_datetime'],
        'customer_id' => $client_arrivy_id,
        'customer_first_name' => $request['fname'],
        'customer_last_name' => $request['lname'],
        'customer_email' => $request['email'],
        'customer_mobile_number' => $request['phone'],
        'customer_address_line_1' => $request['moving_from_street'],
        'customer_address_line_2' => $request['apt_from'],
        'customer_city' => $request['moving_from_city'],
        'customer_state' => $request['moving_from_state'],
        'customer_zipcode' => $request['moving_from_zip'],
        'customer_country' => 'USA',
        'time_window_start' => $start_time_window,
      );
      // If service type unloading help, make moving to address Primary.
      if ($request['move_service_type'] == 4) {
        $data['customer_address_line_1'] = $request['moving_to_street'];
        $data['customer_address_line_2'] = $request['apt_to'];
        $data['customer_city'] = $request['moving_to_city'];
        $data['customer_state'] = $request['moving_to_state'];
        $data['customer_zipcode'] = $request['moving_to_zip'];
      }
      // If service type storage move, make moving to address Primary.
      if ($request['move_service_type'] == 2 && !empty($request['storage_to_move'])) {
        $data['customer_address_line_1'] = $request['moving_to_street'];
        $data['customer_address_line_2'] = $request['apt_to'];
        $data['customer_city'] = $request['moving_to_city'];
        $data['customer_state'] = $request['moving_to_state'];
        $data['customer_zipcode'] = $request['moving_to_zip'];
      }
      // If service type overnight, make moving to address Primary.
      if ($request['move_service_type'] == 6 && !empty($request['storage_to_move'])) {
        $data['customer_address_line_1'] = $request['moving_to_street'];
        $data['customer_address_line_2'] = $request['apt_to'];
        $data['customer_city'] = $request['moving_to_city'];
        $data['customer_state'] = $request['moving_to_state'];
        $data['customer_zipcode'] = $request['moving_to_zip'];
      }
      // If service type is flat rate long distance version,
      // create two requests for pickup and for delivery.
      if ($request['move_service_type'] == 5 && $request['flat_rate_local_move_val'] == 0) {
        $data['title'] = "Request #{$request_id} Pickup";
        $update_flat_rate_delivery = TRUE;
      }
      // Add extra pickup.
      if (!empty($request['extra_pickup_zip'])) {
        $addresses[] = array(
          'title' => 'Extra Pickup',
          'address_line_1' => $request['extra_pickup_street'],
          'address_line_2' => $request['extra_pickup_apt'],
          'city' => $request['extra_pickup_city'] ?? '',
          'state' => $request['extra_pickup_state'] ?? '',
          'country' => 'USA',
          'zipcode' => $request['extra_pickup_zip'],
          'exact_location' => '',
          'complete_address' => $request['extra_pickup_street'] . ' ' . $request['extra_pickup_apt'] . ', ' . $request['extra_pickup_city'] . ' ' . $request['extra_pickup_state'] . ' ' . $request['extra_pickup_zip'],
        );
      }
      // Add extra dropoff if not creating second request
      // for flat rate long distance.
      if (!empty($request['extra_dropoff_zip']) && !$update_flat_rate_delivery) {
        $addresses[] = array(
          'title' => 'Extra Dropoff',
          'address_line_1' => $request['extra_dropoff_street'],
          'address_line_2' => $request['extra_dropoff_apt'],
          'city' => $request['extra_dropoff_city'] ?? '',
          'state' => $request['extra_dropoff_state'] ?? '',
          'country' => 'USA',
          'zipcode' => $request['extra_dropoff_zip'],
          'exact_location' => '',
          'complete_address' => $request['extra_dropoff_street'] . ' ' . $request['extra_dropoff_apt'] . ', ' . $request['extra_dropoff_city'] . ' ' . $request['extra_dropoff_state'] . ' ' . $request['extra_dropoff_zip'],
        );
      }
      // If service type is not loading, unloading help or flat rate
      // long distance , do not add destination address.
      if ($request['move_service_type'] != 2
        && $request['move_service_type'] != 6
        && $request['move_service_type'] != 3
        && $request['move_service_type'] != 4
        && !$update_flat_rate_delivery) {
        $addresses[] = array(
          'title' => 'Destination Address',
          'address_line_1' => $request['moving_to_street'],
          'address_line_2' => $request['apt_to'],
          'city' => $request['moving_to_city'],
          'state' => $request['moving_to_state'],
          'country' => 'USA',
          'zipcode' => $request['moving_to_zip'],
          'exact_location' => '',
          'complete_address' => $request['moving_to_street'] . ' ' . $request['apt_to'] . ', ' . $request['moving_to_city'] . ' ' . $request['moving_to_state'] . ' ' . $request['moving_to_zip'],
        );
      }
      $data['additional_addresses'] = json_encode($addresses);
      // Foreman and client notes.
      if (!empty($request['foreman_notes'])) {
        $data['details'] = strip_tags($request['foreman_notes']);
      }
      if (!empty($request['client_notes'])) {
        $data['customer_notes'] = strip_tags($request['client_notes']);
      }
      $options = array(
        'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
        'method' => 'PUT',
        'timeout' => 30,
        'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
      );
      $response = drupal_http_request(self::UPDATEDELETEURL . $request_arrivy_id, $options);
      if ($response->code == 200) {
        static::saveArrivyLogs("Request updated in Arrivy", "Request #" . $request_id . " has been updated in your Arrivy account", $request_id, 0);
        if ($update_flat_rate_delivery) {
          static::updateFlatRateDelivery($request_id, $client_arrivy_id, $request, $auth_key, $auth_token, $mb_timezone);
        }
        return TRUE;
      }
      else {
        watchdog('move_arrivy', "$response->message when updating request !request_id", array('!request_id' => $request_id), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    else {
      watchdog('move_arrivy', 'No environment variable called MOVE_GOOGLE_CALENDAR_TIMEZONE exists in the system', array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Supporting API method to update an Arrivy flat rate delivery request.
   *
   * @param int $request_id
   *   Request ID.
   * @param int $client_arrivy_id
   *   Client Arrivy ID.
   * @param array $request
   *   Request info.
   * @param string $auth_key
   *   Authorization key.
   * @param string $auth_token
   *   Authorization token.
   * @param string $timezone
   *   MoveBoard Timezone.
   *
   * @return int|bool
   *   Returns request ID or false.
   *
   * @throws \Exception
   */
  public static function updateFlatRateDelivery($request_id, $client_arrivy_id, $request, $auth_key, $auth_token, $timezone) {
    $start_datetime = substr($request['flat_delivery_date'], 0, strrpos($request['flat_delivery_date'], ' ')) . ' ' . $request['flat_delivery_start_time'];
    $formatted_start_datetime = new DateTime($start_datetime, new DateTimeZone($timezone));
    $start_datetime = $formatted_start_datetime->format('c');
    $start_time_window = (strtotime($request['flat_delivery_end_time']) - strtotime($request['flat_delivery_start_time'])) / 60;
    $data = array(
      'start_datetime' => $start_datetime,
      'customer_id' => $client_arrivy_id,
      'customer_first_name' => $request['fname'],
      'customer_last_name' => $request['lname'],
      'customer_email' => $request['email'],
      'customer_mobile_number' => $request['phone'],
      'customer_address_line_1' => $request['moving_to_street'],
      'customer_address_line_2' => $request['apt_to'],
      'customer_city' => $request['moving_to_city'],
      'customer_state' => $request['moving_to_state'],
      'customer_zipcode' => $request['moving_to_zip'],
      'customer_country' => 'USA',
      'time_window_start' => $start_time_window,
    );
    // Add extra dropoff.
    if (!empty($request['extra_dropoff_zip'])) {
      $addresses[] = array(
        'title' => 'Extra Dropoff',
        'address_line_1' => $request['extra_dropoff_street'],
        'address_line_2' => $request['extra_dropoff_apt'],
        'city' => $request['extra_dropoff_city'],
        'state' => $request['extra_dropoff_state'],
        'country' => 'USA',
        'zipcode' => $request['extra_dropoff_zip'],
        'exact_location' => '',
        'complete_address' => $request['extra_dropoff_street'] . ' ' . $request['extra_dropoff_apt'] . ', ' . $request['extra_dropoff_city'] . ' ' . $request['extra_dropoff_state'] . ' ' . $request['extra_dropoff_zip'],
      );
      $data['additional_addresses'] = json_encode($addresses);
    }
    // Foreman and client notes.
    if (!empty($request['foreman_notes'])) {
      $data['details'] = strip_tags($request['foreman_notes']);
    }
    if (!empty($request['client_notes'])) {
      $data['customer_notes'] = strip_tags($request['client_notes']);
    }
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'PUT',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::UPDATEDELETEURL, $options);
    if ($response->code == 200) {
      $data = json_decode($response->data);
      static::saveRequestIdQuery($request_id, $data->id, $data->url_safe_id, $client_arrivy_id);
      static::saveArrivyLogs("Delivery request updated in Arrivy", "Request #{$request_id} has been updated in your Arrivy account", $request_id, 0);
      return $data->id;
    }
    else {
      watchdog('move_arrivy', "$response->message when updating delivery for flat rate !request_id", array('!request_id' => $request_id), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Supporting API method to update Arrivy Foreman.
   *
   * @param int $request_id
   *   Request ID.
   * @param int $request_arrivy_id
   *   Arrivy request ID.
   * @param int $delivery_request_arrivy_id
   *   Arrivy delivery request ID.
   * @param int $foreman_arrivy_id
   *   Foreman Arrivy ID.
   * @param string $autologin_url
   *   Foreman Autologin URL.
   * @param int $delivery_foreman_arrivy_id
   *   Delivery Foreman Arrivy ID.
   * @param string $delivery_autologin_url
   *   Delivery foreman autologin URL.
   *
   * @return bool
   *   Returns true or false depending on the result.
   */
  public static function updateArrivyRequestForeman($request_id, $request_arrivy_id, $delivery_request_arrivy_id = NULL , $foreman_arrivy_id, $autologin_url, $delivery_foreman_arrivy_id = NULL, $delivery_autologin_url = NULL) {
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    $data = array(
      'entity_ids' => $foreman_arrivy_id,
      'external_url' => $autologin_url,
    );
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'PUT',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::UPDATEDELETEURL . $request_arrivy_id, $options);
    if ($response->code == 200) {
      static::updateRequestForemanQuery($request_arrivy_id, $foreman_arrivy_id);
      static::saveArrivyLogs("Request updated in Arrivy", "New foreman assigned to request #{$request_id} in Arrivy.", $request_id, 0);
      if (isset($delivery_request_arrivy_id) && isset($delivery_foreman_arrivy_id) && isset($delivery_autologin_url)) {
        static::updateDeliveryRequestForeman($auth_key, $auth_token, $request_id, $delivery_request_arrivy_id, $delivery_foreman_arrivy_id, $delivery_autologin_url);
      }
      return TRUE;
    }
    else {
      watchdog('move_arrivy', "$response->message when updating foreman for request !request_id", array('!request_id' => $request_id), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Supporting API method to update Arrivy Foreman on flat rate delivery part.
   *
   * @param string $auth_key
   *   Authorization key.
   * @param string $auth_token
   *   Authorization token.
   * @param int $request_id
   *   Request ID.
   * @param int $delivery_request_arrivy_id
   *   Arrivy delivery request ID.
   * @param int $delivery_foreman_arrivy_id
   *   Delivery flat rate foreman Arrivy ID.
   * @param string $delivery_autologin_url
   *   Delivery foreman autologin URL.
   *
   * @return bool
   *   Returns true or false depending on the result.
   */
  public static function updateDeliveryRequestForeman($auth_key, $auth_token, $request_id, $delivery_request_arrivy_id, $delivery_foreman_arrivy_id, $delivery_autologin_url) {
    $data = array(
      'entity_ids' => $delivery_foreman_arrivy_id,
      'external_url' => $delivery_autologin_url,
    );
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'PUT',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::UPDATEDELETEURL . $delivery_request_arrivy_id, $options);
    if ($response->code == 200) {
      static::updateRequestForemanQuery($delivery_request_arrivy_id, $delivery_foreman_arrivy_id);
      static::saveArrivyLogs("Delivery request updated in Arrivy", "New foreman assigned to delivery request #{$request_id} in Arrivy.", $request_id, 0);
      return TRUE;
    }
    else {
      watchdog('move_arrivy', "$response->message when updating foreman for delivery request !request_id", array('!request_id' => $request_id), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Supporting API method to unassign foreman from a request.
   *
   * @param int $request_id
   *   Request ID.
   * @param int $request_arrivy_id
   *   Arrivy request ID.
   * @param int $delivery_arrivy_request_id
   *   Arrivy delivery request ID.
   *
   * @return string
   *   Returns result message.
   */
  public static function deleteArrivyRequestForeman($request_id, $request_arrivy_id, $delivery_arrivy_request_id = NULL) {
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    $data = array(
      'entity_ids' => '',
      'external_url' => '',
    );
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'PUT',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::UPDATEDELETEURL . $request_arrivy_id, $options);
    if ($response->code == 200) {
      static::updateRequestForemanQuery($request_arrivy_id, NULL);
      static::saveArrivyLogs("Request updated in Arrivy", "Foreman unassigned from request #{$request_id} in Arrivy.", $request_id, 0);
      if (isset($delivery_arrivy_request_id)) {
        static::deleteDeliveryRequestForeman($auth_key, $auth_token, $request_id, $delivery_arrivy_request_id);
      }
      return 'Successfully Unassigned Foreman from Request in your Arrivy Account';

    }
    else {
      watchdog('move_arrivy', "$response->message when unassigning foreman for request !request_id", array('!request_id' => $request_id), WATCHDOG_ERROR);
      return 'Error Updating Request $request_id in your Arrivy account';
    }
  }

  /**
   * Supporting API method to unassign foreman from a delivery request.
   *
   * @param string $auth_key
   *   Authorization key.
   * @param int $auth_token
   *   Authorization token.
   * @param int $request_id
   *   Request ID.
   * @param int $delivery_arrivy_request_id
   *   Arrivy delivery request ID.
   *
   * @return string
   *   Returns result message.
   */
  public static function deleteDeliveryRequestForeman($auth_key, $auth_token, $request_id, $delivery_arrivy_request_id = NULL) {
    $data = array(
      'entity_ids' => '',
      'external_url' => '',
    );
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'PUT',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::UPDATEDELETEURL . $delivery_arrivy_request_id, $options);
    if ($response->code == 200) {
      static::updateRequestForemanQuery($delivery_arrivy_request_id, NULL);
      static::saveArrivyLogs("Delivery request updated in Arrivy", "Foreman unassigned from delivery request #{$request_id} in Arrivy.", $request_id, 0);
      return 'Successfully Unassigned Foreman from Request in your Arrivy Account';
    }
    else {
      watchdog('move_arrivy', "$response->message when  unassigning foreman for delivery request !request_id", array('!request_id' => $request_id), WATCHDOG_ERROR);
      return 'Error Updating Request $request_id in your Arrivy account';
    }
  }

  /**
   * Supporting API method to create new task status.
   *
   * @param int $arrivy_request_id
   *   Arrivy request ID.
   * @param string $status_type
   *   Status type (STARTED or COMPLETED).
   * @param string $moveboard_request_id
   *   MoveBoard request ID.
   *
   * @return string
   *   Returns result message.
   */
  public static function createNewTaskStatus($arrivy_request_id, $status_type, $moveboard_request_id) {
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    $moveboard_timezone = getenv('MOVE_GOOGLE_CALENDAR_TIMEZONE');
    if (!empty($moveboard_timezone)) {
      $status_time = static::getTaskStatusTime($moveboard_timezone);
      $data = array(
        'type' => $status_type,
        'time' => $status_time,
        'source' => 'MoveBoard',
        'extra_fields' => json_encode(array('visible_to_customer' => TRUE)),
      );
      $options = array(
        'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
        'method' => 'POST',
        'timeout' => 30,
        'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
      );
      $response = drupal_http_request(self::UPDATEDELETEURL . "{$arrivy_request_id}/status/new", $options);
      if ($response->code == 200) {
        static::saveArrivyLogs("Status created in Arrivy", ucfirst($status_type) . " status has been created for request #{$moveboard_request_id} in your Arrivy account", $moveboard_request_id, 0);
        return 'Successfully Created ' . ucfirst($status_type) . ' for request $moveboard_request_id in your Arrivy Account';
      }
      else {
        watchdog('move_arrivy', "$response->message when creating status for request !request_id", array('!request_id' => $moveboard_request_id), WATCHDOG_ERROR);
        return 'Error creating status for request $moveboard_request_id in your Arrivy account';
      }
    }
    else {
      watchdog('move_arrivy', 'No enivronment variable called MOVE_GOOGLE_CALENDAR_TIMEZONE exists in the system', array(), WATCHDOG_ERROR);
      return 'Error Posting Status for Arrivy Request. Please let us know about this issue';
    }
  }

  /**
   * Supporting database method to get request info.
   *
   * @param int $request_id
   *   Request ID.
   *
   * @return array
   *   Returns array with the request info.
   */
  public static function getRequestInfoQuery($request_id) {
    return db_query('SELECT n.uid as client_id, first_name.field_user_first_name_value AS fname, 
    last_name.field_user_last_name_value AS lname, phone.field_primary_phone_value AS phone, 
    u.mail AS email, service_type.field_move_service_type_value AS move_service_type,
    move_date.field_date_value AS move_date, 
    min_start_time.field_actual_start_time_value AS min_start_time, 
    max_start_time.field_start_time_max_value AS max_start_time, 
    max_move_time.field_maximum_move_time_value AS max_move_time,
    travel_time.field_travel_time_value AS travel_time,
    moving_from.field_moving_from_thoroughfare AS moving_from_street, 
    apt_from.field_apt_from_value AS apt_from, 
    moving_from.field_moving_from_locality AS moving_from_city, 
    moving_from.field_moving_from_administrative_area AS moving_from_state, 
    moving_from.field_moving_from_postal_code AS moving_from_zip, 
    moving_to.field_moving_to_thoroughfare AS moving_to_street, 
    apt_to.field_apt_to_value AS apt_to, moving_to.field_moving_to_locality AS moving_to_city, 
    moving_to.field_moving_to_administrative_area AS moving_to_state, 
    moving_to.field_moving_to_postal_code AS moving_to_zip, 
    extra_pickup.field_extra_pickup_premise AS extra_pickup_apt,
    extra_pickup.field_extra_pickup_thoroughfare AS extra_pickup_street,
    extra_pickup.field_extra_pickup_locality AS extra_pickup_city,
    extra_pickup.field_extra_pickup_administrative_area AS extra_pickup_state,
    extra_pickup.field_extra_pickup_postal_code AS extra_pickup_zip,
    extra_dropoff.field_extra_dropoff_premise AS extra_dropoff_apt,
    extra_dropoff.field_extra_dropoff_thoroughfare AS extra_dropoff_street,
    extra_dropoff.field_extra_dropoff_locality AS extra_dropoff_city,
    extra_dropoff.field_extra_dropoff_administrative_area AS extra_dropoff_state,
    extra_dropoff.field_extra_dropoff_postal_code AS extra_dropoff_zip,
    notes.client_notes AS client_notes, notes.foreman_notes AS foreman_notes,
    flat_rate_local_move.field_flat_rate_local_move_value AS flat_rate_local_move_val,
    flat_delivery_date.field_delivery_date_value AS flat_delivery_date,
    flat_start.field_delivery_start_time1_value AS flat_delivery_start_time,
    flat_end.field_delvery_start_time2_value AS flat_delivery_end_time,
    to_storage_move.field_to_storage_move_target_id AS storage_to_move
    FROM {node} n LEFT JOIN {users} u ON u.uid = n.uid 
    LEFT JOIN {field_data_field_user_first_name} first_name ON first_name.entity_id = u.uid 
    LEFT JOIN {field_data_field_user_last_name} last_name ON last_name.entity_id = u.uid 
    LEFT JOIN {field_data_field_primary_phone} phone ON phone.entity_id = u.uid
    LEFT JOIN {field_data_field_date} move_date ON move_date.entity_id = n.nid 
    LEFT JOIN {field_data_field_actual_start_time} min_start_time ON min_start_time.entity_id = n.nid 
    LEFT JOIN {field_data_field_start_time_max} max_start_time ON max_start_time.entity_id = n.nid 
    LEFT JOIN {field_data_field_maximum_move_time} max_move_time ON max_move_time.entity_id = n.nid
    LEFT JOIN {field_data_field_travel_time} travel_time ON travel_time.entity_id = n.nid  
    LEFT JOIN {field_data_field_moving_from} moving_from ON moving_from.entity_id = n.nid 
    LEFT JOIN {field_data_field_apt_from} apt_from ON apt_from.entity_id = n.nid 
    LEFT JOIN {field_data_field_moving_to} moving_to ON moving_to.entity_id = n.nid 
    LEFT JOIN {field_data_field_apt_to} apt_to ON apt_to.entity_id = n.nid 
    LEFT JOIN {field_data_field_extra_pickup} extra_pickup ON extra_pickup.entity_id = n.nid
    LEFT JOIN {field_data_field_extra_dropoff} extra_dropoff ON extra_dropoff.entity_id = n.nid
    LEFT JOIN {field_data_field_move_service_type} service_type ON service_type.entity_id = n.nid
    LEFT JOIN {users_notes} notes ON notes.nid = n.nid
    LEFT JOIN {field_data_field_flat_rate_local_move} flat_rate_local_move ON flat_rate_local_move.entity_id = n.nid
    LEFT JOIN {field_data_field_delivery_date} flat_delivery_date ON flat_delivery_date.entity_id = n.nid
    LEFT JOIN {field_data_field_delivery_start_time1} flat_start ON flat_start.entity_id = n.nid
    LEFT JOIN {field_data_field_delvery_start_time2} flat_end ON flat_end.entity_id = n.nid
    LEFT JOIN {field_data_field_to_storage_move} to_storage_move ON to_storage_move.entity_id = n.nid
    WHERE (n.nid = :nid)', array(':nid' => $request_id))->fetchAssoc();
  }

  /**
   * Supporting database method to get confirmed requests info.
   *
   * @return array
   *   Returns array with confirmed requests info.
   */
  public static function getConfirmedRequestsInfoQuery() {
    return db_query('SELECT n.nid as nid, n.uid as uid, first_name.field_user_first_name_value AS fname, 
    last_name.field_user_last_name_value AS lname, phone.field_primary_phone_value AS phone, 
    u.mail AS email, service_type.field_move_service_type_value AS move_service_type,
    move_date.field_date_value AS move_date, 
    min_start_time.field_actual_start_time_value AS min_start_time, 
    max_start_time.field_start_time_max_value AS max_start_time, 
    max_move_time.field_maximum_move_time_value AS max_move_time,
    travel_time.field_travel_time_value AS travel_time,
    moving_from.field_moving_from_thoroughfare AS moving_from_street, 
    apt_from.field_apt_from_value AS apt_from, 
    moving_from.field_moving_from_locality AS moving_from_city, 
    moving_from.field_moving_from_administrative_area AS moving_from_state, 
    moving_from.field_moving_from_postal_code AS moving_from_zip, 
    moving_to.field_moving_to_thoroughfare AS moving_to_street, 
    apt_to.field_apt_to_value AS apt_to, moving_to.field_moving_to_locality AS moving_to_city, 
    moving_to.field_moving_to_administrative_area AS moving_to_state, 
    moving_to.field_moving_to_postal_code AS moving_to_zip,
    extra_pickup.field_extra_pickup_premise AS extra_pickup_apt,
    extra_pickup.field_extra_pickup_thoroughfare AS extra_pickup_street,
    extra_pickup.field_extra_pickup_locality AS extra_pickup_city,
    extra_pickup.field_extra_pickup_administrative_area AS extra_pickup_state,
    extra_pickup.field_extra_pickup_postal_code AS extra_pickup_zip,
    extra_dropoff.field_extra_dropoff_premise AS extra_dropoff_apt,
    extra_dropoff.field_extra_dropoff_thoroughfare AS extra_dropoff_street,
    extra_dropoff.field_extra_dropoff_locality AS extra_dropoff_city,
    extra_dropoff.field_extra_dropoff_administrative_area AS extra_dropoff_state,
    extra_dropoff.field_extra_dropoff_postal_code AS extra_dropoff_zip,   
    notes.client_notes AS client_notes, notes.foreman_notes AS foreman_notes, 
    flat_rate_local_move.field_flat_rate_local_move_value AS flat_rate_local_move_val,
    flat_delivery_date.field_delivery_date_value AS flat_delivery_date,
    flat_start.field_delivery_start_time1_value AS flat_delivery_start_time,
    flat_end.field_delvery_start_time2_value AS flat_delivery_end_time,
    to_storage_move.field_to_storage_move_target_id AS storage_to_move
    FROM {node} n LEFT JOIN {users} u ON u.uid = n.uid 
    LEFT JOIN {field_data_field_user_first_name} first_name ON first_name.entity_id = u.uid 
    LEFT JOIN {field_data_field_user_last_name} last_name ON last_name.entity_id = u.uid 
    LEFT JOIN {field_data_field_primary_phone} phone ON phone.entity_id = u.uid
    LEFT JOIN {field_data_field_date} move_date ON move_date.entity_id = n.nid 
    LEFT JOIN {field_data_field_actual_start_time} min_start_time ON min_start_time.entity_id = n.nid 
    LEFT JOIN {field_data_field_start_time_max} max_start_time ON max_start_time.entity_id = n.nid 
    LEFT JOIN {field_data_field_maximum_move_time} max_move_time ON max_move_time.entity_id = n.nid
    LEFT JOIN {field_data_field_travel_time} travel_time ON travel_time.entity_id = n.nid  
    LEFT JOIN {field_data_field_moving_from} moving_from ON moving_from.entity_id = n.nid 
    LEFT JOIN {field_data_field_apt_from} apt_from ON apt_from.entity_id = n.nid 
    LEFT JOIN {field_data_field_moving_to} moving_to ON moving_to.entity_id = n.nid 
    LEFT JOIN {field_data_field_apt_to} apt_to ON apt_to.entity_id = n.nid
    LEFT JOIN {field_data_field_extra_pickup} extra_pickup ON extra_pickup.entity_id = n.nid
    LEFT JOIN {field_data_field_extra_dropoff} extra_dropoff ON extra_dropoff.entity_id = n.nid
    LEFT JOIN {field_data_field_approve} request_status ON request_status.entity_id = n.nid
    LEFT JOIN {field_data_field_move_service_type} service_type ON service_type.entity_id = n.nid
    LEFT JOIN {users_notes} notes ON notes.nid = n.nid
    LEFT JOIN {field_data_field_flat_rate_local_move} flat_rate_local_move ON flat_rate_local_move.entity_id = n.nid
    LEFT JOIN {field_data_field_delivery_date} flat_delivery_date ON flat_delivery_date.entity_id = n.nid
    LEFT JOIN {field_data_field_delivery_start_time1} flat_start ON flat_start.entity_id = n.nid
    LEFT JOIN {field_data_field_delvery_start_time2} flat_end ON flat_end.entity_id = n.nid
    LEFT JOIN {field_data_field_to_storage_move} to_storage_move ON to_storage_move.entity_id = n.nid
    WHERE (request_status.field_approve_value = :request_status) && (move_date.field_date_value > DATE_ADD(CURDATE(), INTERVAL 1 DAY))', array(':request_status' => 3))->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Supporting database method to get Arrivy request ID.
   *
   * @param int $request_id
   *   Request ID.
   *
   * @return array
   *   Returns array with request.
   */
  public static function getArrivyRequestIdQuery($request_id) {
    return db_query('SELECT mar.arrivy_request_id FROM {move_arrivy_requests} mar
      WHERE (mar.moveboard_request_id = :request_id)', array(':request_id' => $request_id))->fetchCol();
  }

  /**
   * Supporting database method to save Arrivy request ID.
   *
   * @param int $request_id
   *   Request ID.
   * @param int $arrivy_request_id
   *   Arrivy request ID.
   * @param string $arrivy_url_id
   *   Arrivy request URL.
   * @param int $arrivy_client_id
   *   Arrivy client ID.
   *
   * @return int
   *   Returns result of db_insert.
   *
   * @throws \Exception
   */
  public static function saveRequestIdQuery($request_id, $arrivy_request_id, $arrivy_url_id, $arrivy_client_id) {
    return db_insert('move_arrivy_requests')
      ->fields(array(
        'moveboard_request_id' => $request_id,
        'arrivy_request_id' => $arrivy_request_id,
        'arrivy_safe_url_id' => $arrivy_url_id,
        'client_arrivy_id' => $arrivy_client_id,
        'created' => time(),
      ))
      ->execute();
  }

  /**
   * Supporting database method to update the foreman on a request.
   *
   * @param int $arrivy_request_id
   *   Arrivy request ID.
   * @param int $arrivy_foreman_id
   *   Arrivy foreman ID.
   *
   * @return int
   *   Returns result of db_update.
   */
  public static function updateRequestForemanQuery($arrivy_request_id, $arrivy_foreman_id) {
    return db_update('move_arrivy_requests')
      ->fields(array(
        'foreman_arrivy_id' => $arrivy_foreman_id,
      ))
      ->condition('arrivy_request_id', $arrivy_request_id)
      ->execute();
  }

}
