<?php

namespace Drupal\move_arrivy\Services;

/**
 * Class ArrivyClient.
 *
 * @package Drupal\move_arrivy\Services
 */
class ArrivyClient {

  const CREATEURL = 'https://www.arrivy.com/api/customers/new';

  /**
   * Create a new Arrivy client.
   *
   * @param int $nid
   *   Request ID.
   * @param int $uid
   *   User ID.
   * @param string $fname
   *   First name.
   * @param string $lname
   *   Last name.
   * @param string $phone
   *   Phone number.
   * @param string $email
   *   Email address.
   *
   * @return int|bool
   *   Returns the ID of the newly created client or returns false.
   */
  public static function create($nid, $uid, $fname, $lname, $phone, $email) {
    $client_arrivy_id = ArrivyForeman::getArrivyUidQuery($uid);
    if (empty($client_arrivy_id)) {
      return static::postClient($nid, $uid, $fname, $lname, $phone, $email);
    }
    else {
      return $client_arrivy_id;
    }
  }

  /**
   * Supporting API method to create a new Arrivy customer.
   *
   * @param int $nid
   *   Request ID.
   * @param int $uid
   *   User ID.
   * @param string $fname
   *   First name.
   * @param string $lname
   *   Last name.
   * @param string $phone
   *   Phone number.
   * @param string $email
   *   Email address.
   *
   * @return int|bool
   *   Returns the ID of the newly created client or returns false.
   */
  public static function postClient($nid, $uid, $fname, $lname, $phone, $email) {
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    $notifications = array(
      'sms' => variable_get('arrivy_api_sms_notification', FALSE),
      'email' => variable_get('arrivy_api_email_notification', FALSE),
    );
    $data = array(
      'external_id' => $uid,
      'first_name' => $fname,
      'last_name' => $lname,
      'mobile_number' => $phone,
      'email' => $email,
      'notifications' => json_encode($notifications),
    );
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'POST',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::CREATEURL, $options);
    if ($response->code == 200) {
      $client_arrivy_id = json_decode($response->data)->id;
      ArrivyForeman::saveArrivyUidQuery($uid, $client_arrivy_id);
      ArrivyRequest::saveArrivyLogs('Client created in Arrivy', "Client $fname $lname has been created in Arrivy", $nid, 0);
      return $client_arrivy_id;
    }
    else {
      $error_message = json_decode($response->data)->description;
      watchdog(
        'move_arrivy',
        "$error_message when creating client !first_name !last_name",
        array('!first_name' => $fname, '!last_name' => $lname),
        WATCHDOG_ERROR);
      return FALSE;
    }
  }

}
