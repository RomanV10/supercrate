<?php

namespace Drupal\move_arrivy\Services;

use phpDocumentor\Reflection\Types\Null_;

/**
 * Class ArrivyForeman.
 *
 * @package Drupal\move_arrivy\Services
 */
class ArrivyForeman {

  const CREATEURL = 'https://www.arrivy.com/api/entities/new';
  const RETRIEVEUPDATEDELETEURL = 'https://www.arrivy.com/api/entities/';

  private $uid;
  private $error_message;

  /**
   * ArrivyForeman constructor.
   *
   * @param int $uid
   *   User id.
   */
  public function __construct($uid = 0) {
    $this->uid = $uid;
  }

  /**
   * Primary method to get an error message.
   *
   * @return string
   *   Returns error message string.
   */
  public function getErrorMessage() {
    return $this->error_message;
  }

  /**
   * Creating a new foreman or linking an existing foreman in Arrivy.
   *
   * Call initiated in department.
   *
   * @param bool $create_new
   *   Create new arrivy account if true or link an existing one if false.
   * @param int $arrivy_uid
   *   Arrivy Foreman ID.
   * @param string $login_email
   *   Foreman email address for new account.
   * @param string $login_pass
   *   Foreman account password for new account.
   * @param string $fname
   *   Foreman first name.
   * @param string $lname
   *   Foreman last name.
   * @param string $phone
   *   Foreman phone number.
   *
   * @return array
   *   Returns new arrivy foreman ID if successfully created or updated,
   *   else returns false.
   */
  public function create($create_new, $arrivy_uid, $login_email, $login_pass, $fname, $lname, $phone) {
    $arrivy_id = NULL;
    if (ArrivyConnection::connectedToArrivy()) {
      if ($create_new) {
        if (!empty($login_email) && !empty($login_pass)) {
          $arrivy_id = $this->createForemanHttpRequest($this->uid, $fname, $lname, $phone, $login_email, $login_pass);
        }
        else {
          return array(
            'arrivy_id' => FALSE,
            'message' => 'Foreman Arrivy email and/or password fields are empty.',
          );
        }
      }
      elseif (!$create_new) {
        if (!empty($arrivy_uid)) {
          $arrivy_id = $this->linkForemanHttpRequest($this->uid, $arrivy_uid);
        }
        else {
          return array(
            'arrivy_id' => FALSE,
            'message' => 'Foreman Arrivy ID field is empty.',
          );
        }
      }
      if (!empty($arrivy_id)) {
        $message = 'This foreman has been created in your Arrivy Account.';
      }
      else {
        $message = $this->getErrorMessage();
      }
      return array(
        'arrivy_id' => $arrivy_id,
        'message' => $message,
      );
    }
    else {
      return array(
        'arrivy_id' => FALSE,
        'message' => 'Arrivy not connected in the settings.',
      );
    }
  }

  /**
   * Primary method for retrieving Arrivy Foreman UID.
   *
   * Call initiated in department to know if foreman is already in Arrivy.
   *
   * @return int|bool
   *   Returns new arrivy foreman ID if successfully created or updated,
   *   else returns false.
   */
  public function retrieve() {
    if (ArrivyConnection::connectedToArrivy()) {
      $arrivy_uid = static::getArrivyUidQuery($this->uid);
      if (!empty($arrivy_uid)) {
        return $arrivy_uid;
      }
    }

    return FALSE;
  }

  /**
   * Primary method for updating information about the foreman in Arrivy.
   *
   * Call initiated in department for updating foreman.
   *
   * @param string $first_name
   *   Foreman first name.
   * @param string $last_name
   *   Foreman last name.
   * @param string $phone
   *   Foreman phone number.
   *
   * @return string
   *   Returns message after updating foreman.
   */
  public function update($first_name, $last_name, $phone) {
    if (ArrivyConnection::connectedToArrivy()) {
      $arrivy_uid = static::getArrivyUidQuery($this->uid);
      if (!empty($arrivy_uid)) {
        return static::updateForemanHttpRequest($arrivy_uid, $first_name, $last_name, $phone);
      }
      else {
        return 'Foreman is not in Arrivy to be updated.';
      }
    }
    else {
      return 'Arrivy not connected in the settings.';
    }
  }

  /**
   * Primary method for deleting foreman in Arrivy.
   *
   * @return string
   *   Returns message after deleting foreman.
   */
  public function delete() {
    if (ArrivyConnection::connectedToArrivy()) {
      $arrivy_uid = static::getArrivyUidQuery($this->uid);
      if (!empty($arrivy_uid)) {
        return static::deleteForemanHttpRequest($this->uid, $arrivy_uid);
      }
      else {
        watchdog('move_arrivy', 'Foreman !uid doesnt exist in our drupal database to be deleted', array('!uid' => $this->uid), WATCHDOG_ERROR);
        return 'Foreman has not been deleted in your Arrivy account';
      }
    }
  }

  /**
   * Primary method for disconnecting foreman.
   *
   * @return array
   *   Returns message and disconnect_status for a foreman.
   */
  public function disconnect() {
    $result = static::saveArrivyUidQuery($this->uid, NULL);
    if ($result) {
      return array(
        'disconnect_status' => TRUE,
        'message' => 'Foreman has been successfully disconnected.',
      );
    }
    return array(
      'disconnect_status' => FALSE,
      'message' => 'Foreman not in Arrivy to be disconnected.',
    );
  }

  /**
   * Supporting method to generate random color for Foremans.
   *
   * @return string
   *   Returns color hex code string.
   */
  public static function generateForemanRandomColor() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
  }

  /**
   * Supporting API method to create a foreman in Arrivy.
   *
   * @param string $uid
   *   Foreman user ID.
   * @param string $first_name
   *   Foreman first name.
   * @param string $last_name
   *   Foreman last name.
   * @param string $phone
   *   Foreman phone number.
   * @param string $email
   *   Foreman email address.
   * @param string $password
   *   Foreman login password.
   *
   * @return int|bool
   *   Returns new arrivy foreman ID if successfully created,else returns false.
   */
  public function createForemanHttpRequest($uid, $first_name, $last_name, $phone, $email, $password) {
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    $data = array(
      'name' => $first_name . ' ' . $last_name,
      'type' => 'Foreman',
      'phone' => $phone,
      'color' => static::generateForemanRandomColor(),
      'email' => $email,
      'password' => $password,
      'invite_to_join' => 'false',
      'create_account' => 'true',
      'ref' => 'ELROMCO',
      'external_id' => $uid,
      'permission' => 'FIELDCREW',
    );
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'POST',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::CREATEURL, $options);
    if ($response->code == 200) {
      $foreman_arrivy_id = json_decode($response->data)->id;
      static::saveArrivyUidQuery($uid, $foreman_arrivy_id);
      return $foreman_arrivy_id;
    }
    else {
      $error_message = json_decode($response->data)->description;
      $this->error_message = $error_message;
      watchdog(
        'move_arrivy',
        "$error_message for function createArrivyForeman when creating foreman !first_name !last_name in Arrivy",
        array('!first_name' => $first_name, '!last_name' => $last_name),
        WATCHDOG_ERROR
      );
      return FALSE;
    }
  }

  /**
   * Supporting API method to link an existing foreman in Arrivy.
   *
   * @param int $foreman_id
   *   Foreman User ID.
   * @param int $foreman_arrivy_id
   *   Foreman Arrivy ID.
   *
   * @return int|bool
   *   Returns linked arrivy foreman ID if successfully created,
   *   else returns false.
   */
  public function linkForemanHttpRequest($foreman_id, $foreman_arrivy_id) {
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    $options = array(
      'method' => 'GET',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token, FALSE),
    );
    $response = drupal_http_request(self::RETRIEVEUPDATEDELETEURL . $foreman_arrivy_id, $options);
    if ($response->code == 200) {
      static::saveArrivyUidQuery($foreman_id, $foreman_arrivy_id);
      return $foreman_arrivy_id;
    }
    else {
      $error_message = json_decode($response->data)->description;
      $this->error_message = $error_message;
      watchdog(
        'move_arrivy',
        "$error_message for function retrieveArrivyForeman when retrieving foreman with moveboard id of !moveboard_id",
        array('!arrivy_id' => $foreman_id, '!moveboard_id' => $foreman_arrivy_id),
        WATCHDOG_ERROR
      );
      return FALSE;
    }
  }

  /**
   * Supporting API method to update Arrivy foreman information.
   *
   * @param int $foreman_arrivy_id
   *   Foreman Arrivy User ID.
   * @param string $foreman_first_name
   *   Foreman First Name.
   * @param string $foreman_last_name
   *   Foreman Last Name.
   * @param string $foreman_phone
   *   Foreman Phone Number.
   *
   * @return string
   *   Returns message after updating the foreman information.
   */
  public static function updateForemanHttpRequest($foreman_arrivy_id, $foreman_first_name, $foreman_last_name, $foreman_phone) {
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    $data = array(
      'name' => $foreman_first_name . ' ' . $foreman_last_name,
      'type' => 'Foreman',
      'phone' => $foreman_phone,
    );
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'PUT',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::RETRIEVEUPDATEDELETEURL . $foreman_arrivy_id, $options);
    if ($response->code == 200) {
      return 'Successfully updated Arrivy Foreman';
    }
    else {
      watchdog(
        'move_arrivy',
        "$response->message when updating !foreman_arrivy_id",
        array('!foreman_arrivy_id' => $foreman_arrivy_id),
        WATCHDOG_ERROR
      );
      return 'Error updating Arrivy Foreman';
    }
  }

  /**
   * Supporting API method to delete Arrivy foreman.
   *
   * @param int $uid
   *   Foreman User ID.
   * @param int $arrivy_id
   *   Foreman Arrivy ID.
   *
   * @return string
   *   Returns message after deleting the foreman.
   */
  public static function deleteForemanHttpRequest($uid, $arrivy_id) {
    $auth_key = variable_get('arrivy_api_auth_key', FALSE);
    $auth_token = variable_get('arrivy_api_auth_token', FALSE);
    $options = array(
      'method' => 'DELETE',
      'timeout' => 30,
      'headers' => ArrivyConnection::getHeaders($auth_key, $auth_token, FALSE),
    );
    $response = drupal_http_request(self::RETRIEVEUPDATEDELETEURL . $arrivy_id, $options);
    if ($response->code == 200) {
      if (!empty(static::saveArrivyUidQuery($uid, NULL))) {
        return 'Successfully removed Arrivy Foreman';
      }
      else {
        watchdog('move_arrivy', 'Error when deleting foreman !foreman_arrivy_id in drupal database', array('!foreman_arrivy_id' => $arrivy_id), WATCHDOG_ERROR);
        return 'Error removing Arrivy Foreman';
      }
    }
    else {
      watchdog(
        'move_arrivy',
        "$response->message when deleting !foreman_arrivy_id",
        array('!foreman_arrivy_id' => $arrivy_id),
        WATCHDOG_ERROR
      );
      return 'Error removing Arrivy Foreman';
    }
  }

  /**
   * Supporting database method to get Arrivy user ID.
   *
   * @param int $uid
   *   Foreman User ID.
   *
   * @return int|null
   *   Returns foreman user Arrivy ID.
   */
  public static function getArrivyUidQuery($uid) {
    $arrivy_uid = db_query('
      SELECT arrivy.field_arrivy_user_id_value 
      FROM {field_data_field_arrivy_user_id} arrivy 
      WHERE (arrivy.entity_id = :foreman_id)',
      array(':foreman_id' => $uid))->fetchCol();

    return !empty($arrivy_uid[0]) ? (int) $arrivy_uid[0] : NULL;
  }

  /**
   * Supporting database method to save Arrivy user ID.
   *
   * @param int $uid
   *   Foreman User ID.
   * @param int $arrivy_uid
   *   Foreman Arrivy User ID.
   *
   * @return bool
   *   Returns true or false depending on if saving was successful.
   */
  public static function saveArrivyUidQuery($uid, $arrivy_uid) {
    try {
      $user = user_load($uid);
      /* @var \EntityDrupalWrapper $user_wrapper */
      $user_wrapper = entity_metadata_wrapper('user', $user);
      $user_wrapper->field_arrivy_user_id->set($arrivy_uid);
      $user_wrapper->save();
      return TRUE;
    }
    catch (\Throwable $e) {
      watchdog('move_arrivy', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

}
