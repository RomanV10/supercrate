<?php

namespace Drupal\move_arrivy\System;

use Drupal\move_arrivy\Services\ArrivyConnection;
use Drupal\move_arrivy\Services\ArrivyRequest;
use Drupal\move_arrivy\Services\ArrivyForeman;

/**
 * Class Service.
 *
 * @package Drupal\move_arrivy\System
 */
class Service {

  /**
   * Function getResources.
   *
   * @return array
   *   Resources.
   */
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );
    $resources += self::definition();
    return $resources;
  }

  /**
   * Private function definition.
   *
   * @return array
   *   Operations and actions.
   */
  private static function definition() {
    return array(
      'move_arrivy' => array(
        'operations' => array(
          'create' => array(
            'help' => 'Creates an Arrivy Task for the request',
            'callback' => 'Drupal\move_arrivy\System\Service::createArrivyRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
                'description' => t('The request ID which is sent to Arrivy'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'help' => 'Retrieves request post status into an Arrivy account',
            'callback' => 'Drupal\move_arrivy\System\Service::retrieveArrivyRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
                'description' => t('The request ID for which the Arrivy task status is returned'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'save_authorization_keys' => array(
            'help' => 'Saves authorization credentials into variable',
            'callback' => 'Drupal\move_arrivy\System\Service::saveArrivyKeys',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'auth_key',
                'optional' => FALSE,
                'source' => array('data' => 'auth_key'),
                'description' => t('Authorization key header for Arrivy authorization'),
                'type' => 'string',
              ),
              array(
                'name' => 'auth_token',
                'optional' => FALSE,
                'source' => array('data' => 'auth_token'),
                'description' => t('Authorization token header for Arrivy authorization'),
                'type' => 'string',
              ),
              array(
                'name' => 'sms_notification',
                'optional' => FALSE,
                'source' => array('data' => 'sms_notification'),
                'description' => t('Turn on or off task sms notification'),
                'type' => 'bool',
              ),
              array(
                'name' => 'email_notification',
                'optional' => FALSE,
                'source' => array('data' => 'email_notification'),
                'description' => t('Turn on or off task phone notification'),
                'type' => 'bool',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_authorization_keys' => array(
            'help' => 'Returns authorization credentials into variable',
            'callback' => 'Drupal\move_arrivy\System\Service::getArrivyKeys',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
          'connect' => array(
            'help' => 'Connects to Arrivy and synchronizes foremans',
            'callback' => 'Drupal\move_arrivy\System\Service::connectArrivy',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'auth_key',
                'optional' => FALSE,
                'source' => array('data' => 'auth_key'),
                'description' => t('Authorization key header for Arrivy authorization'),
                'type' => 'string',
              ),
              array(
                'name' => 'auth_token',
                'optional' => FALSE,
                'source' => array('data' => 'auth_token'),
                'description' => t('Authorization token header for Arrivy authorization'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'disconnect' => array(
            'help' => 'Turns off Arrivy integration',
            'callback' => 'Drupal\move_arrivy\System\Service::disconnectArrivy',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
          'get_arrivy_dashboard_link' => array(
            'help' => 'Returns Arrivy dashboard autologin link',
            'callback' => 'Drupal\move_arrivy\System\Service::getArrivyDashboardLink',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
          'create_foreman' => array(
            'help' => 'Adds a foreman to Arrivy, also assigns all his requests',
            'callback' => 'Drupal\move_arrivy\System\Service::createForemanAccount',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'create_new',
                'optional' => FALSE,
                'source' => array('data' => 'create_new'),
                'description' => t('Create new foreman in Arrivy or link the existing one?'),
                'type' => 'bool',
              ),
              array(
                'name' => 'arrivy_id',
                'optional' => TRUE,
                'default value' => 0,
                'source' => array('data' => 'arrivy_id'),
                'description' => t('Arrivy foreman id'),
                'type' => 'int',
              ),
              array(
                'name' => 'arrivy_email',
                'optional' => TRUE,
                'default value' => '',
                'source' => array('data' => 'arrivy_email'),
                'description' => t('Arrivy login email address'),
                'type' => 'string',
              ),
              array(
                'name' => 'arrivy_pass',
                'optional' => TRUE,
                'default value' => '',
                'source' => array('data' => 'arrivy_pass'),
                'description' => t('Arrivy login password'),
                'type' => 'string',
              ),
              array(
                'name' => 'id',
                'optional' => FALSE,
                'source' => array('data' => 'id'),
                'description' => t('MoveBoard foreman user ID'),
                'type' => 'int',
              ),
              array(
                'name' => 'first_name',
                'optional' => FALSE,
                'source' => array('data' => 'first_name'),
                'description' => t('MoveBoard foreman first name'),
                'type' => 'string',
              ),
              array(
                'name' => 'last_name',
                'optional' => FALSE,
                'source' => array('data' => 'last_name'),
                'description' => t('MoveBoard foreman last name'),
                'type' => 'string',
              ),
              array(
                'name' => 'phone',
                'optional' => FALSE,
                'source' => array('data' => 'phone'),
                'description' => t('MoveBoard foreman phone number'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve_foreman' => array(
            'help' => 'Returns arrivy foreman id',
            'callback' => 'Drupal\move_arrivy\System\Service::retrieveForemanAccount',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'foreman_id',
                'optional' => FALSE,
                'source' => array('data' => 'foreman_id'),
                'description' => t('MoveBoard foreman user ID'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_foreman' => array(
            'help' => 'Updates arrivy foreman info',
            'callback' => 'Drupal\move_arrivy\System\Service::updateForemanAccount',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'optional' => FALSE,
                'source' => array('data' => 'id'),
                'description' => t('MoveBoard foreman user ID'),
                'type' => 'int',
              ),
              array(
                'name' => 'first_name',
                'optional' => FALSE,
                'source' => array('data' => 'first_name'),
                'description' => t('Foreman first name'),
                'type' => 'string',
              ),
              array(
                'name' => 'last_name',
                'optional' => FALSE,
                'source' => array('data' => 'last_name'),
                'description' => t('Foreman last name'),
                'type' => 'string',
              ),
              array(
                'name' => 'phone',
                'optional' => FALSE,
                'source' => array('data' => 'first_name'),
                'description' => t('Foreman phone number'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete_foreman' => array(
            'help' => 'Deletes foreman in Arrivy',
            'callback' => 'Drupal\move_arrivy\System\Service::deleteForemanAccount',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'foreman_id',
                'optional' => FALSE,
                'source' => array('data' => 'foreman_id'),
                'description' => t('MoveBoard foreman user ID'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'disconnect_foreman' => array(
            'help' => 'Disconnects foreman in Arrivy',
            'callback' => 'Drupal\move_arrivy\System\Service::disconnectForemanAccount',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'foreman_id',
                'optional' => FALSE,
                'source' => array('data' => 'foreman_id'),
                'description' => t('MoveBoard foreman user ID'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'assign_foreman' => array(
            'help' => 'Assigns foreman to a request',
            'callback' => 'Drupal\move_arrivy\System\Service::assignForemanRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'foreman_id',
                'optional' => FALSE,
                'source' => array('data' => 'foreman_id'),
                'description' => t('MoveBoard foreman user ID'),
                'type' => 'int',
              ),
              array(
                'name' => 'delivery_foreman_id',
                'optional' => TRUE,
                'source' => array('data' => 'delivery_foreman_id'),
                'description' => t('Flat Rate Delivery Foreman ID'),
                'type' => 'int',
              ),
              array(
                'name' => 'entity_id',
                'default value' => NULL,
                'optional' => FALSE,
                'source' => array('data' => 'entity_id'),
                'description' => t('Request ID'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'unassign_foreman' => array(
            'help' => 'Assigns foreman to a request',
            'callback' => 'Drupal\move_arrivy\System\Service::unassignForemanRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'optional' => FALSE,
                'source' => array('data' => 'entity_id'),
                'description' => t('Request ID'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'post_task_status' => array(
            'help' => 'Posts new task status to Arrivy',
            'callback' => 'Drupal\move_arrivy\System\Service::postNewTaskStatus',
            'file' => array(
              'type' => 'php',
              'module' => 'move_arrivy',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'request_id',
                'optional' => FALSE,
                'source' => array('data' => 'request_id'),
                'description' => t('MoveBoard request ID'),
                'type' => 'int',
              ),
              array(
                'name' => 'status_type',
                'optional' => FALSE,
                'source' => array('data' => 'status_type'),
                'description' => t('Arrivy task status'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function saveArrivyKeys($auth_key, $auth_token, $sms_notification, $email_notification) {
    return (new ArrivyConnection())->saveSettings($auth_key, $auth_token, $sms_notification, $email_notification);
  }

  public static function getArrivyKeys() {
    return (new ArrivyConnection())->getSettings();
  }

  public static function connectArrivy($auth_key, $auth_token) {
    return (new ArrivyConnection())->connect($auth_key, $auth_token);
  }

  public static function disconnectArrivy() {
    return (new ArrivyConnection())->disconnect();
  }

  public static function getArrivyDashboardLink() {
    return (new ArrivyConnection())->getDashboardLink();
  }

  public static function createForemanAccount($create_new, $arrivy_id, $arrivy_email, $arrivy_pass, $id, $first_name, $last_name, $phone) {
    return (new ArrivyForeman($id))->create($create_new, $arrivy_id, $arrivy_email, $arrivy_pass, $first_name, $last_name, $phone);
  }

  public static function retrieveForemanAccount($foreman_id) {
    return (new ArrivyForeman($foreman_id))->retrieve();
  }

  public static function updateForemanAccount($id, $first_name, $last_name, $phone) {
    return (new ArrivyForeman($id))->update($first_name, $last_name, $phone);
  }

  public static function deleteForemanAccount($foreman_id) {
    return (new ArrivyForeman($foreman_id))->delete();
  }

  public static function disconnectForemanAccount($foreman_id) {
    return (new ArrivyForeman($foreman_id))->disconnect();
  }

  public static function createArrivyRequest($entity_id) {
    return (new ArrivyRequest($entity_id))->create();
  }

  public static function retrieveArrivyRequest($entity_id) {
    return (new ArrivyRequest($entity_id))->retrieve();
  }

  public static function assignForemanRequest($foreman_id, $entity_id, $delivery_foreman_id) {
    return (new ArrivyRequest($entity_id))->assignForeman($foreman_id, $delivery_foreman_id);
  }

  public static function unassignForemanRequest($entity_id) {
    return (new ArrivyRequest($entity_id))->unassignForeman();
  }

  public static function postNewTaskStatus($request_id, $status_type) {
    return (new ArrivyRequest($request_id))->createTaskStatus($status_type);
  }

}
