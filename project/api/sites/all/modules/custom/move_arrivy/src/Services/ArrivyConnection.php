<?php

namespace Drupal\move_arrivy\Services;

use Drupal\move_services_new\Services\Clients;

/**
 * Class ArrivyConnection.
 *
 * @package Drupal\move_arrivy\Services
 */
class ArrivyConnection {

  const CREATESALESURL = 'https://www.arrivy.com/api/entities/new';
  const ARRIVYDASHBOARDURL = 'https://www.arrivy.com/authlogin?';
  const TESTCONNECTIONAPI = "https://www.arrivy.com/api/users/profile";

  /**
   * Returns all saved arrivy connection settings.
   *
   * @return array
   *   Returns an array with connection status(boolean),
   *   authorization key(key or false), authorization token(key or false),
   *   sms notification status(boolean) and email notification status(boolean).
   */
  public function getSettings() {
    return array(
      'arrivy_connection_status' => variable_get('arrivy_api_connection', FALSE),
      'arrivy_authorization_key' => variable_get('arrivy_api_auth_key', FALSE),
      'arrivy_authorization_token' => variable_get('arrivy_api_auth_token', FALSE),
      'arrivy_sms_notification' => variable_get('arrivy_api_sms_notification', FALSE),
      'arrivy_email_notification' => variable_get('arrivy_api_email_notification', FALSE),
    );
  }

  /**
   * Saves authorization credentials such as authorization key.
   *
   * Authorization token and notifications settings.
   *
   * @param mixed $auth_key
   *   Authorization key.
   * @param mixed $auth_token
   *   Authorization token.
   * @param mixed $sms_notification
   *   SMS notification boolean.
   * @param mixed $email_notification
   *   Email notification boolean.
   *
   * @return string
   *   Returns 'Successfully Saved' after setting the authorization credentials.
   */
  public function saveSettings($auth_key, $auth_token, $sms_notification, $email_notification) {
    variable_set('arrivy_api_auth_key', $auth_key);
    variable_set('arrivy_api_auth_token', $auth_token);
    variable_set('arrivy_api_sms_notification', $sms_notification);
    variable_set('arrivy_api_email_notification', $email_notification);
    return 'Successfully Saved';
  }

  /**
   * Connects to Arrivy.
   *
   * @param mixed $auth_key
   *   Authorization key.
   * @param mixed $auth_token
   *   Authorization token.
   *
   * @return array
   *   Returns array with boolean connection status and message.
   *
   * @throws \Exception
   */
  public function connect($auth_key, $auth_token) {
    $data = array(
      'name' => 'MoveBoard Sales',
      'type' => 'Sales',
      'email' => 'sales@moveboard.com',
      'password' => 'Sales@123',
      'invite_to_join' => 'false',
      'create_account' => 'true',
      'ref' => 'ELROMCO',
      'permission' => 'SCHEDULER',
    );
    $options = array(
      'data' => http_build_query($data, NULL, "&", PHP_QUERY_RFC1738),
      'method' => 'POST',
      'timeout' => 30,
      'headers' => self::getHeaders($auth_key, $auth_token),
    );
    $response = drupal_http_request(self::CREATESALESURL, $options);
    $response_data = json_decode($response->data);
    if ($response->code == 200) {
      variable_set('arrivy_api_sales_id', $response_data->id);
      variable_set('arrivy_api_connection', TRUE);
      variable_set('arrivy_api_auth_key', $auth_key);
      variable_set('arrivy_api_auth_token', $auth_token);
      $message = ArrivyRequest::createMultiple();
      $status = TRUE;
    }
    elseif ($response->code == 400 && $response_data->message == 'EMAIL_EXISTS') {
      $options = array(
        'method' => 'GET',
        'timeout' => 30,
        'headers' => self::getHeaders($auth_key, $auth_token, FALSE),
      );
      $call_response = drupal_http_request(self::TESTCONNECTIONAPI, $options);
      if ($call_response->code == 200) {
        variable_set('arrivy_api_connection', TRUE);
        variable_set('arrivy_api_auth_key', $auth_key);
        variable_set('arrivy_api_auth_token', $auth_token);
        $message = ArrivyRequest::createMultiple();
        $status = TRUE;
      }
      else {
        $status = FALSE;
        $message = 'Incorrect Arrivy Authorization Keys';
      }
    }
    else {
      $status = FALSE;
      $message = 'Incorrect Arrivy Authorization Keys';
    }
    return array(
      'connection_status' => $status,
      'message' => $message,
    );
  }

  /**
   * Disconnects from Arrivy.
   *
   * @return array
   *   Returns array with boolean connection status and message.
   */
  public function disconnect() {
    variable_del('arrivy_api_connection');
    return array(
      'connection_status' => variable_get('arrivy_api_connection', FALSE),
      'message' => 'Successfully Disconnected',
    );
  }

  /**
   * Primary method to return Arrivy dashboard autologin link.
   *
   * Used to show Arrivy dashboard on dispatch page.
   *
   * @return string|bool
   *   Returns url string if Arrivy is on, else returns false.
   */
  public function getDashboardLink() {
    global $user;
    if (static::connectedToArrivy()) {
      $arrivy_auth_key = variable_get('arrivy_api_auth_key', FALSE);
      $arrivy_auth_token = variable_get('arrivy_api_auth_token', FALSE);
      $user_manager_or_sales = (new Clients($user->uid))->checkUserRoles(array('manager', 'sales'));
      if (!$user_manager_or_sales) {
        $sales_entity_id = variable_get('arrivy_api_sales_id', FALSE);
      }
      return self::ARRIVYDASHBOARDURL . "auth_key={$arrivy_auth_key}&auth_token={$arrivy_auth_token}" . (isset($sales_entity_id) ? "&entity_id={$sales_entity_id}" : "");
    }
    else {
      return FALSE;
    }
  }

  /**
   * Checks if Arrivy connection is established.
   *
   * @return bool
   *   Returns the connection status.
   */
  public static function connectedToArrivy() {
    return variable_get('arrivy_api_connection', FALSE);
  }

  /**
   * TODO call variables inside of this method.
   *
   * Get headers for query.
   *
   * @param string $auth_key
   *   Auth key.
   * @param string $auth_token
   *   Auth token.
   * @param bool $with_content_type
   *   Add content type or not.
   *
   * @return array
   *   Array with headers
   */
  public static function getHeaders($auth_key, $auth_token, $with_content_type = TRUE) {
    $headers = [
      'X-Auth-Key' => $auth_key,
      'X-Auth-Token' => $auth_token,
    ];

    if ($with_content_type) {
      $headers['Content-Type'] = 'application/x-www-form-urlencoded';
    }

    return $headers;
  }

}
