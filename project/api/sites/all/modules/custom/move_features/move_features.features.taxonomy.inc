<?php
/**
 * @file
 * move_features.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function move_features_taxonomy_default_vocabularies() {
  return array(
    'inventory_items_filter' => array(
      'name' => 'Inventory Items Filter',
      'machine_name' => 'inventory_items_filter',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'inventory_rooms' => array(
      'name' => 'Inventory Rooms',
      'machine_name' => 'inventory_rooms',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'trucks' => array(
      'name' => 'Trucks',
      'machine_name' => 'trucks',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
