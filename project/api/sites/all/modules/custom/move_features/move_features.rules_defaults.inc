<?php
/**
 * @file
 * move_features.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function move_features_default_rules_configuration() {
  $items = array();
  $items['rules_activate_company_user_'] = entity_import('rules_config', '{ "rules_activate_company_user_" : {
      "LABEL" : "Activate company user.",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Custom" ],
      "REQUIRES" : [ "rules", "move_services_new" ],
      "ON" : { "user_update" : [] },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : {
                "52310416" : "52310416",
                "90107672" : "90107672",
                "183771710" : "183771710",
                "238415434" : "238415434",
                "4301886" : "4301886"
              }
            },
            "operation" : "OR"
          }
        },
        { "user_is_blocked" : { "account" : [ "account-unchanged" ] } },
        { "NOT user_is_blocked" : { "account" : [ "account" ] } }
      ],
      "DO" : [ { "move_services_new_blocked_user" : { "obj" : [ "account:uid" ] } } ]
    }
  }');
  $items['rules_after_updating_user_profile'] = entity_import('rules_config', '{ "rules_after_updating_user_profile" : {
      "LABEL" : "After Updating User Profile",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_update" : [] },
      "DO" : [ { "redirect" : { "url" : "user" } } ]
    }
  }');
  $items['rules_block_company_user_'] = entity_import('rules_config', '{ "rules_block_company_user_" : {
      "LABEL" : "Block company user.",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Custom" ],
      "REQUIRES" : [ "rules", "move_services_new" ],
      "ON" : { "user_update" : [] },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : {
                "52310416" : "52310416",
                "90107672" : "90107672",
                "183771710" : "183771710",
                "238415434" : "238415434",
                "4301886" : "4301886"
              }
            },
            "operation" : "OR"
          }
        },
        { "user_is_blocked" : { "account" : [ "account" ] } },
        { "NOT user_is_blocked" : { "account" : [ "account-unchanged" ] } }
      ],
      "DO" : [ { "move_services_new_blocked_user" : { "obj" : [ "account:uid" ] } } ]
    }
  }');
  $items['rules_canceled_date'] = entity_import('rules_config', '{ "rules_canceled_date" : {
      "LABEL" : "Canceled Date",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Custom" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update--move_request" : { "bundle" : "move_request" } },
      "IF" : [
        { "AND" : [
            { "data_is" : { "data" : [ "node:field-approve" ], "value" : "5" } },
            { "NOT data_is" : { "data" : [ "node-unchanged:field-approve" ], "value" : "5" } }
          ]
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-date-canceled" ], "value" : "now" } }
      ]
    }
  }');
  $items['rules_confirmed_date'] = entity_import('rules_config', '{ "rules_confirmed_date" : {
      "LABEL" : "Confirmed date",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Custom" ],
      "REQUIRES" : [ "rules", "move_services_new" ],
      "ON" : { "node_presave--move_request" : { "bundle" : "move_request" } },
      "IF" : [
        { "data_is" : { "data" : [ "node:field-approve" ], "value" : "3" } },
        { "NOT data_is" : { "data" : [ "node-unchanged:field-approve" ], "value" : "3" } }
      ],
      "DO" : [
        { "move_services_new_confirmed_date" : { "obj" : [ "node" ] } },
        { "data_set" : { "data" : [ "node:field-date-confirmed" ], "value" : "now" } }
      ]
    }
  }');
  $items['rules_delete_sales_user'] = entity_import('rules_config', '{ "rules_delete_sales_user" : {
      "LABEL" : "Delete sales user",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "move_services_new", "rules" ],
      "ON" : { "user_delete" : [] },
      "DO" : [
        { "move_services_new_delete_sales_user" : { "sales" : [ "account" ] } }
      ]
    }
  }');
  $items['rules_updated_flat_rate_request'] = entity_import('rules_config', '{ "rules_updated_flat_rate_request" : {
      "LABEL" : "Updated Flat Rate Request",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Custom" ],
      "REQUIRES" : [ "rules", "move_services_new" ],
      "ON" : { "node_update--move_request" : { "bundle" : "move_request" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node-unchanged" ], "field" : "field_approve" } },
        { "data_is" : { "data" : [ "node:field-approve" ], "value" : "10" } }
      ],
      "DO" : [
        { "move_services_new_custom_send_mail" : { "param" : "flat_rate_submitted", "node" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_updated_flat_rate_request_step_3'] = entity_import('rules_config', '{ "rules_updated_flat_rate_request_step_3" : {
      "LABEL" : "Updated Flat Rate Request Step 3",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Custom" ],
      "REQUIRES" : [ "rules", "move_services_new" ],
      "ON" : { "node_update--move_request" : { "bundle" : "move_request" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node-unchanged" ], "field" : "field_approve" } },
        { "data_is" : { "data" : [ "node:field-approve" ], "value" : "11" } }
      ],
      "DO" : [
        { "move_services_new_custom_send_mail" : { "param" : "choose_options", "node" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_updated_new_request_send_mail'] = entity_import('rules_config', '{ "rules_updated_new_request_send_mail" : {
      "LABEL" : "Updated new Request Send Mail",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Custom" ],
      "REQUIRES" : [ "rules", "move_services_new" ],
      "ON" : { "node_update--move_request" : { "bundle" : "move_request" } },
      "IF" : [
        { "entity_is_new" : { "entity" : [ "node" ] } },
        { "AND" : [
            { "NOT data_is" : {
                "data" : [ "node:field-approve" ],
                "op" : "IN",
                "value" : { "value" : { "10" : "10", "11" : "11" } }
              }
            },
            { "entity_has_field" : { "entity" : [ "node-unchanged" ], "field" : "field_approve" } },
            { "NOT data_is" : {
                "data" : [ "node:field-approve" ],
                "value" : [ "node-unchanged:field-approve" ]
              }
            },
            { "data_is" : {
                "data" : [ "node:field-approve" ],
                "op" : "IN",
                "value" : { "value" : { "1" : "1", "9" : "9" } }
              }
            }
          ]
        }
      ],
      "DO" : [
        { "move_services_new_custom_send_mail" : { "param" : "new", "node" : [ "node" ] } }
      ]
    }
  }');
  $items['rules_user_save'] = entity_import('rules_config', '{ "rules_user_save" : {
      "LABEL" : "Manager Save",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-10",
      "OWNER" : "rules",
      "TAGS" : [ "Custom" ],
      "REQUIRES" : [ "rules", "move_services_new" ],
      "ON" : {
        "move_services_save_new_account" : [],
        "move_services_user_update" : []
      },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : {
                "30037204" : "30037204",
                "52310416" : "52310416",
                "90107672" : "90107672"
              }
            },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [ { "move_services_new_updated_user" : [] } ]
    }
  }');
  return $items;
}
