<?php
/**
 * @file
 * move_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function move_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'add score'.
  $permissions['add score'] = array(
    'name' => 'add score',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'move_request_score',
  );

  // Exported permission: 'administer content types'.
  $permissions['administer content types'] = array(
    'name' => 'administer content types',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'administer elysia_cron'.
  $permissions['administer elysia_cron'] = array(
    'name' => 'administer elysia_cron',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'elysia_cron',
  );

  // Exported permission: 'administer nodes'.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'administer search'.
  $permissions['administer search'] = array(
    'name' => 'administer search',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'search',
  );

  // Exported permission: 'administer search_api'.
  $permissions['administer search_api'] = array(
    'name' => 'administer search_api',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'search_api',
  );

  // Exported permission: 'administer services'.
  $permissions['administer services'] = array(
    'name' => 'administer services',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'services',
  );

  // Exported permission: 'administer uuid'.
  $permissions['administer uuid'] = array(
    'name' => 'administer uuid',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'uuid',
  );

  // Exported permission: 'create custom payroll'.
  $permissions['create custom payroll'] = array(
    'name' => 'create custom payroll',
    'roles' => array(
      'administrator' => 'administrator',
      'foreman' => 'foreman',
      'manager' => 'manager',
    ),
    'module' => 'move_services_new',
  );

  // Exported permission: 'create inventory_item content'.
  $permissions['create inventory_item content'] = array(
    'name' => 'create inventory_item content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create log'.
  $permissions['create log'] = array(
    'name' => 'create log',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'move_new_log',
  );

  // Exported permission: 'create move_request content'.
  $permissions['create move_request content'] = array(
    'name' => 'create move_request content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create move_service flag'.
  $permissions['create move_service flag'] = array(
    'name' => 'create move_service flag',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'move_services_new',
  );

  // Exported permission: 'delete any inventory_item content'.
  $permissions['delete any inventory_item content'] = array(
    'name' => 'delete any inventory_item content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any move_request content'.
  $permissions['delete any move_request content'] = array(
    'name' => 'delete any move_request content',
    'roles' => array(
      'administrator' => 'administrator',
      'foreman' => 'foreman',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete custom payroll'.
  $permissions['delete custom payroll'] = array(
    'name' => 'delete custom payroll',
    'roles' => array(
      'administrator' => 'administrator',
      'foreman' => 'foreman',
      'manager' => 'manager',
    ),
    'module' => 'move_services_new',
  );

  // Exported permission: 'delete move_service flag'.
  $permissions['delete move_service flag'] = array(
    'name' => 'delete move_service flag',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'move_services_new',
  );

  // Exported permission: 'delete own inventory_item content'.
  $permissions['delete own inventory_item content'] = array(
    'name' => 'delete own inventory_item content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own move_request content'.
  $permissions['delete own move_request content'] = array(
    'name' => 'delete own move_request content',
    'roles' => array(
      'administrator' => 'administrator',
      'foreman' => 'foreman',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any inventory_item content'.
  $permissions['edit any inventory_item content'] = array(
    'name' => 'edit any inventory_item content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any move_request content'.
  $permissions['edit any move_request content'] = array(
    'name' => 'edit any move_request content',
    'roles' => array(
      'administrator' => 'administrator',
      'foreman' => 'foreman',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own inventory_item content'.
  $permissions['edit own inventory_item content'] = array(
    'name' => 'edit own inventory_item content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own move_request content'.
  $permissions['edit own move_request content'] = array(
    'name' => 'edit own move_request content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'manager' => 'manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'execute elysia_cron'.
  $permissions['execute elysia_cron'] = array(
    'name' => 'execute elysia_cron',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'elysia_cron',
  );

  // Exported permission: 'get a system variable'.
  $permissions['get a system variable'] = array(
    'name' => 'get a system variable',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'foreman' => 'foreman',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'services',
  );

  // Exported permission: 'get any binary files'.
  $permissions['get any binary files'] = array(
    'name' => 'get any binary files',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'services',
  );

  // Exported permission: 'get credit card slip'.
  $permissions['get credit card slip'] = array(
    'name' => 'get credit card slip',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'move_services_new',
  );

  // Exported permission: 'get custom payroll'.
  $permissions['get custom payroll'] = array(
    'name' => 'get custom payroll',
    'roles' => array(
      'administrator' => 'administrator',
      'foreman' => 'foreman',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'move_services_new',
  );

  // Exported permission: 'get own binary files'.
  $permissions['get own binary files'] = array(
    'name' => 'get own binary files',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'services',
  );

  // Exported permission: 'owner retrieve request storage'.
  $permissions['owner retrieve request storage'] = array(
    'name' => 'owner retrieve request storage',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'move_storage',
  );

  // Exported permission: 'perform unlimited index queries'.
  $permissions['perform unlimited index queries'] = array(
    'name' => 'perform unlimited index queries',
    'roles' => array(
      'administrator' => 'administrator',
      'foreman' => 'foreman',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'services',
  );

  // Exported permission: 'retrieve log'.
  $permissions['retrieve log'] = array(
    'name' => 'retrieve log',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'move_new_log',
  );

  // Exported permission: 'save file information'.
  $permissions['save file information'] = array(
    'name' => 'save file information',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'services',
  );

  // Exported permission: 'search content'.
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'search',
  );

  // Exported permission: 'send javascript errors to sentry'.
  $permissions['send javascript errors to sentry'] = array(
    'name' => 'send javascript errors to sentry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'raven',
  );

  // Exported permission: 'set a system variable'.
  $permissions['set a system variable'] = array(
    'name' => 'set a system variable',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'services',
  );

  // Exported permission: 'set_payment request'.
  $permissions['set_payment request'] = array(
    'name' => 'set_payment request',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'foreman' => 'foreman',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'move_services_new',
  );

  // Exported permission: 'update log'.
  $permissions['update log'] = array(
    'name' => 'update log',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'move_new_log',
  );

  // Exported permission: 'update move_service flag'.
  $permissions['update move_service flag'] = array(
    'name' => 'update move_service flag',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'move_services_new',
  );

  // Exported permission: 'use advanced search'.
  $permissions['use advanced search'] = array(
    'name' => 'use advanced search',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'search',
  );

  // Exported permission: 'view elysia_cron'.
  $permissions['view elysia_cron'] = array(
    'name' => 'view elysia_cron',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'elysia_cron',
  );

  // Exported permission: 'view own unpublished content'.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view revisions'.
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
      'sales' => 'sales',
    ),
    'module' => 'node',
  );

  return $permissions;
}
