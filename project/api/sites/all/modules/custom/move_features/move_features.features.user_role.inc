<?php
/**
 * @file
 * move_features.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function move_features_user_default_roles() {
  $roles = array();

  // Exported role: home-estimator.
  $roles['home-estimator'] = array(
    'name' => 'home-estimator',
    'weight' => 9,
    'machine_name' => 'home_estimator',
  );

  return $roles;
}
