<?php
/**
 * @file
 * move_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function move_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "elysia_cron" && $api == "default_elysia_cron_rules") {
    return array("version" => "1");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function move_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function move_features_image_default_styles() {
  $styles = array();

  // Exported image style: inventory.
  $styles['inventory'] = array(
    'label' => 'Inventory',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function move_features_node_info() {
  $items = array(
    'move_request' => array(
      'name' => t('Move Request'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_default_search_api_index().
 */
function move_features_default_search_api_index() {
  $items = array();
  $items['move_request'] = entity_import('search_api_index', '{
    "name" : "Move request",
    "machine_name" : "move_request",
    "description" : null,
    "server" : "server",
    "item_type" : "node",
    "options" : {
      "datasource" : { "bundles" : [ "move_request" ] },
      "index_directly" : 1,
      "cron_limit" : "50",
      "fields" : {
        "author" : { "type" : "integer", "entity_type" : "user" },
        "author:mail" : { "type" : "text" },
        "author:name" : { "type" : "text" },
        "author:uid" : { "type" : "integer" },
        "changed" : { "type" : "date" },
        "created" : { "type" : "date" },
        "field_added_to_trip" : { "type" : "boolean" },
        "field_approve" : { "type" : "integer", "boost" : "2.0" },
        "field_company_flags" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "move_service_company_flag"
        },
        "field_date" : { "type" : "date" },
        "field_date_confirmed" : { "type" : "date" },
        "field_delivery_crew_size" : { "type" : "integer" },
        "field_delivery_date" : { "type" : "date" },
        "field_e_mail" : { "type" : "text" },
        "field_first_name" : { "type" : "text" },
        "field_flags" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "move_service_flag"
        },
        "field_foreman" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "user" },
        "field_foreman_assign_time" : { "type" : "date" },
        "field_from_parser" : { "type" : "boolean" },
        "field_helper" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "user" },
        "field_home_estimate_date" : { "type" : "date" },
        "field_home_estimator" : { "type" : "integer" },
        "field_labor_time" : { "type" : "decimal" },
        "field_last_name" : { "type" : "text" },
        "field_ld_status" : { "type" : "integer" },
        "field_list_truck" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_manager_assign_time" : { "type" : "date" },
        "field_move_service_type" : { "type" : "string" },
        "field_movers_count" : { "type" : "integer" },
        "field_moving_from:administrative_area" : { "type" : "text" },
        "field_moving_from:locality" : { "type" : "text" },
        "field_moving_from:postal_code" : { "type" : "text" },
        "field_moving_to:administrative_area" : { "type" : "text" },
        "field_moving_to:locality" : { "type" : "text" },
        "field_moving_to:postal_code" : { "type" : "text" },
        "field_parser_provider_id" : { "type" : "integer" },
        "field_parser_provider_name" : { "type" : "string" },
        "field_phone" : { "type" : "text" },
        "field_request_manager" : { "type" : "integer", "entity_type" : "user" },
        "field_reservation_received" : { "type" : "boolean" },
        "field_schedule_delivery_date" : { "type" : "integer" },
        "field_size_of_move" : { "type" : "string" },
        "field_total_score" : { "type" : "integer" },
        "field_total_score_updated" : { "type" : "date" },
        "field_type_of_entrance_from" : { "type" : "string" },
        "field_type_of_entrance_to_" : { "type" : "string" },
        "nid" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "status" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_e_mail" : true,
              "field_first_name" : true,
              "field_last_name" : true,
              "field_phone" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_e_mail" : true,
              "field_first_name" : true,
              "field_last_name" : true,
              "field_phone" : true
            },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_e_mail" : true,
              "field_first_name" : true,
              "field_last_name" : true,
              "field_phone" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_e_mail" : true,
              "field_first_name" : true,
              "field_last_name" : true,
              "field_phone" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 1,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function move_features_default_search_api_server() {
  $items = array();
  $items['server'] = entity_import('search_api_server', '{
    "name" : "Server",
    "machine_name" : "server",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "1",
      "partial_matches" : 0,
      "indexes" : { "move_request" : {
          "author" : {
            "table" : "search_api_db_move_request",
            "column" : "author",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_e_mail" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0",
            "column" : "field_e_mail"
          },
          "field_first_name" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0",
            "column" : "field_first_name"
          },
          "field_last_name" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0",
            "column" : "field_last_name"
          },
          "field_approve" : {
            "table" : "search_api_db_move_request",
            "column" : "field_approve",
            "type" : "integer",
            "boost" : "2.0"
          },
          "field_phone" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0",
            "column" : "field_phone"
          },
          "search_api_language" : {
            "table" : "search_api_db_move_request",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "status" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "created" : {
            "table" : "search_api_db_move_request",
            "column" : "created",
            "type" : "date",
            "boost" : "1.0"
          },
          "author:name" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "author:mail" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_date" : {
            "table" : "search_api_db_move_request",
            "column" : "field_date",
            "type" : "date",
            "boost" : "1.0"
          },
          "nid" : {
            "table" : "search_api_db_move_request",
            "column" : "nid",
            "type" : "integer",
            "boost" : "1.0"
          },
          "changed" : {
            "table" : "search_api_db_move_request",
            "column" : "changed",
            "type" : "date",
            "boost" : "1.0"
          },
          "author:uid" : {
            "table" : "search_api_db_move_request",
            "column" : "author_uid",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_move_service_type" : {
            "table" : "search_api_db_move_request",
            "column" : "field_move_service_type",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_list_truck" : {
            "table" : "search_api_db_move_request_field_list_truck",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_date_confirmed" : {
            "table" : "search_api_db_move_request",
            "column" : "field_date_confirmed",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_flags" : {
            "table" : "search_api_db_move_request_field_flags",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_foreman" : {
            "table" : "search_api_db_move_request_field_foreman",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_helper" : {
            "table" : "search_api_db_move_request_field_helper",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_foreman_assign_time" : {
            "table" : "search_api_db_move_request",
            "column" : "field_foreman_assign_time",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_reservation_received" : {
            "table" : "search_api_db_move_request",
            "column" : "field_reservation_received",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_company_flags" : {
            "table" : "search_api_db_move_request_field_company_flags",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_moving_from:administrative_area" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_moving_from:locality" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_moving_from:postal_code" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_moving_to:administrative_area" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_moving_to:locality" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_moving_to:postal_code" : {
            "table" : "search_api_db_move_request_text",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_type_of_entrance_from" : {
            "table" : "search_api_db_move_request",
            "column" : "field_type_of_entrance_from",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_type_of_entrance_to_" : {
            "table" : "search_api_db_move_request",
            "column" : "field_type_of_entrance_to_",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_movers_count" : {
            "table" : "search_api_db_move_request",
            "column" : "field_movers_count",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_delivery_date" : {
            "table" : "search_api_db_move_request",
            "column" : "field_delivery_date",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_from_parser" : {
            "table" : "search_api_db_move_request",
            "column" : "field_from_parser",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_parser_provider_id" : {
            "table" : "search_api_db_move_request",
            "column" : "field_parser_provider_id",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_size_of_move" : {
            "table" : "search_api_db_move_request",
            "column" : "field_size_of_move",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_delivery_crew_size" : {
            "table" : "search_api_db_move_request",
            "column" : "field_delivery_crew_size",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_parser_provider_name" : {
            "table" : "search_api_db_move_request",
            "column" : "field_parser_provider_name",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_added_to_trip" : {
            "table" : "search_api_db_move_request",
            "column" : "field_added_to_trip",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_request_manager" : {
            "table" : "search_api_db_move_request",
            "column" : "field_request_manager",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_manager_assign_time" : {
            "table" : "search_api_db_move_request",
            "column" : "field_manager_assign_time",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_home_estimate_date" : {
            "table" : "search_api_db_move_request",
            "column" : "field_home_estimate_date",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_labor_time" : {
            "table" : "search_api_db_move_request",
            "column" : "field_labor_time",
            "type" : "decimal",
            "boost" : "1.0"
          },
          "field_ld_status" : {
            "table" : "search_api_db_move_request",
            "column" : "field_ld_status",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_schedule_delivery_date" : {
            "table" : "search_api_db_move_request",
            "column" : "field_schedule_delivery_date",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_total_score" : {
            "table" : "search_api_db_move_request",
            "column" : "field_total_score",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_total_score_updated" : {
            "table" : "search_api_db_move_request",
            "column" : "field_total_score_updated",
            "type" : "date",
            "boost" : "1.0"
          },
          "field_home_estimator" : {
            "table" : "search_api_db_move_request",
            "column" : "field_home_estimator",
            "type" : "integer",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1"
  }');
  return $items;
}
