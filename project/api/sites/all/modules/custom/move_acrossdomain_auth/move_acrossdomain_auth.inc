<?php

/**
 * Authenticates a call to verify the request.
 *
 * @param array $settings
 *    The settings for the authentication module.
 * @param array $method
 *    The method that's being called
 * @param array $args
 *    The arguments that are being used to call the method
 */
function _move_acrossdomain_auth_authenticate_call($settings, $method, $args) {
  global $user;

  if ($method['callback'] != '_user_resource_get_token') {
    $csrf_token = NULL;

    if (isset($_SERVER['HTTP_X_CSRF_TOKEN'])) {
      $csrf_token = $_SERVER['HTTP_X_CSRF_TOKEN'];
    }
    elseif (isset($_REQUEST['services_token'])) {
      $csrf_token = $_REQUEST['services_token'];
    }

    if ($csrf_token) {
      $auth = db_select('move_acrossdomain_auth', 'auth');
      $auth->join('sessions', 's', 'auth.sid = s.sid');
      $auth->fields('auth');
      $auth->fields('s', array('uid'));
      $auth->condition('auth.token', $csrf_token);
      $session_info = $auth->execute()->fetchAssoc();

      if (!$session_info) {
        // CSRF validation failed.
        $user = drupal_anonymous_user();
        return;
      }

      session_name($session_info['session_name']);
      $_COOKIE[$session_info['session_name']] = $session_info['sid'];
      _drupal_session_read($session_info['sid']);
      $user->token = $csrf_token;
    }
    else {
      if (session_status() === PHP_SESSION_ACTIVE) {
        session_regenerate_id(TRUE);
      }
      $user = drupal_anonymous_user();
    }
  }

}
