<?php

use Drupal\move_sms\Services\Company;

/**
 * Drupal form register company to sms java app.
 *
 * @return array
 *   Form.
 */
function move_sms_register_java() {
  $form = array();

  $form['drupal_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API Site URL'),
    '#default_value' => variable_get("drupal_url", 'http://api.movecalc.local'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['drupal_port'] = array(
    '#type' => 'textfield',
    '#title' => t('API Site port'),
    '#default_value' => variable_get("drupal_port", 80),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['uuid'] = array(
    '#type' => 'textfield',
    '#title' => t('UUID'),
    '#default_value' => variable_get("smsCompanyUuid", ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
    '#disabled' => TRUE,
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Connect'),
    '#submit' => ['move_sms_register_java_submit'],
  );

  $form['update_button'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => ['move_sms_update_java_submit'],
  );

  return $form;
}

function move_sms_register_java_submit($form, &$form_state) {
  variable_set("drupal_url", $form_state['values']['drupal_url']);
  variable_set("drupal_port", $form_state['values']['drupal_port']);
  (new Company())->register();
}

function move_sms_update_java_submit($form, &$form_state) {
  variable_set("drupal_url", $form_state['values']['drupal_url']);
  variable_set("drupal_port", $form_state['values']['drupal_port']);
  (new Company())->update();
}
