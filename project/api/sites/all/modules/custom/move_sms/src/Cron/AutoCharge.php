<?php

namespace Drupal\move_sms\Cron;

use Drupal\move_notification\Services\Notification;
use Drupal\move_notification\Util\enum\NotificationCategories;
use Drupal\move_services_new\Cron\Tasks\CronInterface;
use Drupal\move_services_new\Util\enum\NotificationTypes;
use Drupal\move_sms\Services\Company;
use Drupal\move_sms\Services\Payment;
use Drupal\move_sms\Traits\Constants;

/**
 * Class AutoCharge.
 *
 * @package Drupal\move_sms\Cron
 */
class AutoCharge implements CronInterface  {

  use Constants;

  /**
   * Execute task by cron.
   *
   * @throws \Drupal\move_sms\Cron\AutoChargeException
   */
  public static function execute(): void {
    $customer = variable_get('stripeCustomerId');
    $auto_charge_flag = variable_get('stripeAutoCharge', 0);
    $company_usage = (new Company())->companyUsage();

    if ($auto_charge_flag && $customer) {
      if ($company_usage && $company_usage->balance < self::$MINIMUM_BALANCE) {
        (new Payment())->chargeCustomerCard(self::$AMOUNT_CHARGE, $customer);
      }
    }
    elseif ($auto_charge_flag && !$customer) {
      throw new AutoChargeException("Stripe Customer not created!");
    }
    elseif (!$auto_charge_flag && $company_usage->balance < self::$MINIMUM_BALANCE) {
      // @TODO Add notification for sms here.
      $notification_info = array('notification' => 'Your SMS Balance is low, please recharge your balance.');
      Notification::createNotification(NotificationTypes::SMS_LOW_BALANCE, $notification_info, 0, NotificationCategories::SITE_SETTINGS, 0);
    }
  }

}

/**
 * Class AutoChargeException.
 *
 * @package Drupal\move_sms\Cron
 */
class AutoChargeException extends \Exception {}