<?php

namespace Drupal\move_sms\ValidationRules;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class PhoneNumber.
 *
 * @package Drupal\move_sms\ValidationRules
 */
class TwilioPhoneNumber implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'number' => ['required', 'regex:/^[0-9]{11}$/'],
    ];
  }

}
