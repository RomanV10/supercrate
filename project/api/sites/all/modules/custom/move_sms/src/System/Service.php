<?php

namespace Drupal\move_sms\System;

use Drupal\move_sms\Services\Company;
use Drupal\move_sms\Services\Payment;
use Drupal\move_sms\Services\Phone;
use Drupal\move_sms\Services\Sms;
use Drupal\move_sms\ValidationRules\TwilioPhoneNumber;

/**
 * Class Service.
 *
 * @package Drupal\move_sms\System
 */
class Service {

  /**
   * Function get resources.
   *
   * @return array
   *   Resources.
   */
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );
    $resources += self::definition();
    return $resources;
  }

  /**
   * Definition operations and actions services.
   *
   * @return array
   *   Services.
   */
  private static function definition() {
    return array(
      'move_sms' => array(
        'operations' => array(),
        'actions' => array(
          'company_usage' => [
            'callback' => 'Drupal\move_sms\System\Service::companyUsage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('sms settings access'),
          ],
          'phone_buy' => array(
            'help' => 'Buy twilio phone number',
            'callback' => 'Drupal\move_sms\System\Service::buy',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'rules' => TwilioPhoneNumber::class,
            'args' => array(
              array(
                'name' => 'number',
                'type' => 'string',
                'source' => array('data' => 'number'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('sms settings access'),
          ),
          'phone_delete' => array(
            'callback' => 'Drupal\move_sms\System\Service::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'rules' => TwilioPhoneNumber::class,
            'args' => array(
              array(
                'name' => 'number',
                'type' => 'string',
                'source' => array('data' => 'number'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('sms settings access'),
          ),
          'phone_find' => array(
            'callback' => 'Drupal\move_sms\System\Service::find',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'number',
                'type' => 'string',
                'source' => array('data' => 'number'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('sms settings access'),
          ),
          'phone_numbers_all' => array(
            'callback' => 'Drupal\move_sms\System\Service::getCompanyPhones',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('sms settings access'),
          ),
          'sms_send' => array(
            'callback' => 'Drupal\move_sms\System\Service::sendSms',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'body',
                'type' => 'string',
                'source' => array('data' => 'body'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('sms settings access'),
          ),
          'sms_create' => array(
            'callback' => 'Drupal\move_sms\System\Service::smsCreate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'body',
                'type' => 'string',
                'source' => array('data' => 'body'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'sid',
                'type' => 'string',
                'source' => array('data' => 'sid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'to',
                'type' => 'string',
                'source' => array('data' => 'to'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'sms_status_update' => [
            'callback' => 'Drupal\move_sms\System\Service::smsStatusUpdate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'sms_sid',
                'type' => 'string',
                'source' => array('data' => 'sms_sid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'sms_status',
                'type' => 'string',
                'source' => array('data' => 'sms_status'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ],
          'sms_all' => [
            'help' => 'Get messages from date for number',
            'callback' => 'Drupal\move_sms\System\Service::smsAll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'rules' => TwilioPhoneNumber::class,
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'number',
                'type' => 'string',
                'source' => array('data' => 'number'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ],
          'sms_get_templates' => [
            'callback' => 'Drupal\move_sms\System\Service::getSmsTemplates',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('sms settings access'),
          ],
          'sms_update_template' => [
            'help' => 'Update sms template settings',
            'callback' => 'Drupal\move_sms\System\Service::updateSmsTemplate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('sms settings access'),
          ],
          'payment' => [
            'help' => 'Get messages from date for number',
            'callback' => 'Drupal\move_sms\System\Service::payment',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('sms settings access'),
          ],
          'payment_create_customer_charge' => [
            'help' => 'Get messages from date for number',
            'callback' => 'Drupal\move_sms\System\Service::createCustomerAndCharge',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'token',
                'type' => 'string',
                'source' => array('data' => 'token'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'email',
                'type' => 'string',
                'source' => array('data' => 'email'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'amount',
                'type' => 'int',
                'source' => array('data' => 'amount'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('sms settings access'),
          ],
          'payment_charge_customer' => [
            'help' => 'Get messages from date for number',
            'callback' => 'Drupal\move_sms\System\Service::chargeCustomerCard',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'amount',
                'type' => 'int',
                'source' => array('data' => 'amount'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'customerId',
                'type' => 'string',
                'source' => array('data' => 'customerId'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('sms settings access'),
          ],
          'get_payments' => [
            'help' => 'Get company payments',
            'callback' => 'Drupal\move_sms\System\Service::getPayments',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('sms settings access'),
          ],
          'get_receipts' => [
            'help' => 'Get company payments',
            'callback' => 'Drupal\move_sms\System\Service::getReceipts',
            'file' => array(
              'type' => 'php',
              'module' => 'move_sms',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('sms settings access'),
          ],
        ),
      ),
    );
  }

  /**
   * Buy 'twilio' phone number.
   *
   * @param string $number
   *   Phone number.
   *
   * @return bool
   *   Buy phone number or not.
   *
   * @throws \Exception
   */
  public static function buy(string $number) {
    return (new Phone())->buy($number);
  }

  /**
   * Find 'twilio' phone number.
   *
   * @param string $number
   *   Number.
   *
   * @return bool
   *   Find.
   */
  public static function find(string $number) {
    return (new Phone())->find($number);
  }

  /**
   * Send sms.
   *
   * @param int $nid
   *   Move request id.
   * @param string $body
   *   Sms text body.
   *
   * @return bool|string
   *   Sent text or exception.
   *
   * @throws \Drupal\move_sms\Cron\UpdateBalanceException
   * @throws \Exception
   */
  public static function sendSms(int $nid, string $body) {
    return (new Sms())->execute($nid, NULL, NULL, '', $body);
  }

  /**
   * Receive sms.
   *
   * @param string $body
   *   Sms text body.
   * @param string $sid
   *   Message sid.
   * @param string $to
   *   From client number.
   *
   * @return bool|string
   *   Sent text or exception.
   *
   * @throws \Exception
   */
  public static function smsCreate(string $body, string $sid, string $to) {
    return (new Sms())->smsCreate($body, $sid, $to);
  }

  /**
   * Delete phone number.
   *
   * @param string $number
   *   Number.
   *
   * @return bool|string
   *   Sent text or exception.
   *
   * @throws \Exception
   */
  public static function delete(string $number) {
    return (new Phone())->delete($number);
  }

  /**
   * Get company usage.
   *
   * @return array|bool|mixed
   *   Data: usage twilio services.
   *
   * @throws \Exception
   */
  public static function companyUsage() {
    return (new Company())->companyUsage();
  }

  /**
   * Get company phones.
   *
   * @return array|bool|mixed
   *   Phone numbers.
   *
   * @throws \Exception
   */
  public static function getCompanyPhones() {
    return (new Sms())->getCompanyPhones();
  }

  /**
   * Get company phones.
   *
   * @param string $datefrom
   *   Date format example: "2018-11-28".
   * @param string $dateto
   *   Date format example: "2018-11-28".
   * @param string $number
   *   Phone number.
   *
   * @return array|bool|mixed
   *   Phone numbers.
   *
   * @throws \Exception
   */
  public static function smsAll(string $datefrom, string $dateto, string $number) {
    return (new Sms())->smsAll($datefrom, $dateto, $number);
  }

  /**
   * Update sms comment.
   *
   * @param string $sms_sid
   *   Unique sms id.
   * @param string $sms_status
   *   Sms status.
   *
   * @return bool
   *   Sms entity.
   *
   * @throws \Exception
   */
  public static function smsStatusUpdate(string $sms_sid, string $sms_status) {
    return (new Sms())->smsStatusUpdate($sms_sid, $sms_status);
  }

  /**
   * Create payment for twilio.
   *
   * @param mixed $data
   *   Payment data.
   *
   * @return array|bool|mixed|string
   *   Result.
   */
  public static function payment($data) {
    return (new Company())->paymentTwilio($data);
  }

  /**
   * Get sms templates.
   *
   * @return array
   *   All templates.
   *
   * @throws \Exception
   */
  public static function getSmsTemplates() {
    return (new Sms())->getSmsTemplates();
  }

  /**
   * Update sms template.
   *
   * @param int $id
   *   Template id.
   * @param array $data
   *   Template data.
   *
   * @return array
   *   Result db update.
   *
   * @throws \Exception
   */
  public static function updateSmsTemplate(int $id, array $data) {
    return (new Sms())->updateTemplate($id, $data);
  }

  /**
   * Create customer in 'stripe' and charge them.
   *
   * @param string $token
   *   Stripe token.
   * @param string $email
   *   User email.
   * @param int $amount
   *   Charge amount.
   *
   * @return mixed
   *   Response.
   */
  public static function createCustomerAndCharge(string $token, string $email, $amount) {
    return (new Payment())->createCustomerAndCharge($token, $email, $amount);
  }

  /**
   * Create customer in 'stripe' and charge them.
   *
   * @param int $amount
   *   Charge amount.
   * @param string $customerId
   *   Unique customer id in 'stripe'.
   *
   * @return mixed
   *   Response.
   */
  public static function chargeCustomerCard($amount, string $customerId = '') {
    return (new Payment())->chargeCustomerCard($amount, $customerId);
  }

  /**
   * Get stripe payments.
   *
   * @return bool|int|string
   *   Payments.
   */
  public static function getPayments() {
    return (new Payment())->getPayments();
  }

}
