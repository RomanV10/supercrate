<?php

namespace Drupal\move_sms\Traits;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

/**
 * Trait SendToJava.
 *
 * @package Drupal\move_sms\Traits
 */
trait SendToJava {

  private $uuid;
  private $token;
  private $client;
  private $host;
  private $port;

  public function initParams() {
    $this->token = variable_get('smsCompanyToken', "");
    $this->uuid = variable_get('smsCompanyUuid', "");
    $this->host = variable_get('drupal_url', 'http://api.movecalc.local');
    $this->port = variable_get('drupal_port', 80);
    $this->client = new GuzzleClient([
      'base_uri' => $_ENV['SMS_JAVA_URL'],
    ]);
  }

  public function send(array $params) : ?ResponseInterface {
    $response = NULL;
    try {
      $this->initParams();
      $headers = $this->prepareHeaders();
      $path = "/api/v1{$params['url']}";
      // Send an asynchronous request.
      $response = $this->client->request($params['type'], $path, array(
        'body' => json_encode($params['post_fields']),
        'headers' => $headers,
      ));
    }
    catch (RequestException $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog('SendToJava: ', $error_text, array(), WATCHDOG_ERROR);
    }

    return $response;
  }

  private function prepareHeaders() {
    $authorization = array();
    if ($this->token) {
      $authorization = array("Authorization" => "Bearer $this->token");
    }

    $headers = [
      'Content-Type' => 'application/json',
    ];
    $headers += $authorization;

    return $headers;
  }

}
