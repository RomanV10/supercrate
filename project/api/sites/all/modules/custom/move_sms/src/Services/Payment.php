<?php

namespace Drupal\move_sms\Services;

use Drupal\move_sms\Traits\SendToJava;

/**
 * Class Payment.
 *
 * @package Drupal\move_sms\Services
 */
class Payment {

  use SendToJava;

  private const PAYMENT_PAY = '/payment/pay';
  private const PAYMENT_CREATE_CUSTOMER = '/payment/create/customer';
  private const PAYMENT_CHARGE_CUSTOMER = '/payment/charge/customer';
  private const PAYMENT_RECEIPTS = '/payment/receipts';

  /**
   * Payment constructor.
   */
  public function __construct() {
    $this->initParams();
  }

  /**
   * Method for pay authorize.net.
   *
   * @param array $data
   *   Pay data.
   *
   * @return bool|string
   *   Pay result.
   */
  public function pay(array $data) {
    $result = FALSE;
    $payData = $this->preparePayData($data);

    $params = [
      'url' => self::PAYMENT_PAY,
      'type' => 'POST',
      'post_fields' => [$payData],
    ];

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $result = TRUE;
      // Need log for pay.
//        $log = new Log($nid, EntityTypes::MOVEREQUEST);
//        $log->create(self::prepareLog($nid, $sms));
    }
    else {
      watchdog('Sms: payment', "Error sms pay", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Prepare pay data.
   *
   * @param array $data
   *   Client data for pay.
   *
   * @return array
   *   Prepared data.
   */
  private function preparePayData(array $data) {
    return [
      'amount' => (int) ($data['amount'] ?? 123),
      'zip_code' => $data['zip_code'] ?? '02135',
      'credit_card' => [
        'card_number' => $data['card_number'] ?? '4111111111111111',
        'card_code' => $data['card_code'] ?? '123',
        'exp_date' => $data['exp_date'] ?? '11/19',
      ],
    ];
  }

  /**
   * Create customer in stripe.
   *
   * @param string $token
   *   Stripe token.
   * @param string $email
   *   Email.
   * @param int $amount
   *   Charge amount.
   *
   * @return mixed
   *   Response.
   */
  public function createCustomerAndCharge(string $token, string $email, int $amount) {
    $result = FALSE;
    $customerData = $this->prepareCustomerData($token, $email, $amount);

    $params = [
      'url' => self::PAYMENT_CREATE_CUSTOMER,
      'type' => 'POST',
      'post_fields' => $customerData,
    ];

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $result = TRUE;
      $response = json_decode($response->getBody());
      // Need set variable.
      variable_set('stripeCustomerId', $response->customer);
      variable_set('stripeCustomerEmail', $customerData['email']);
      variable_set('stripeAutoCharge', 1);
      variable_set('stripeCustomerCardLast4', $response->last4);
      variable_set('stripeCustomerCardBrand', $response->brand);
      // Need log for pay.
    }
    else {
      watchdog('Sms: payment', "Create customer stripe error.", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Prepare customer data for create customer in stripe.
   *
   * @param string $token
   *   Stripe token.
   * @param string $email
   *   Email.
   * @param int $amount
   *   Charge amount.
   *
   * @return array
   *   Customer data.
   */
  private function prepareCustomerData(string $token, string $email, int $amount) {
    $basicSettings = json_decode(variable_get('basicsettings', ''));
    if (empty($email)) {
      $email = $basicSettings->company_email;
    }
    $customerData = [
      'email' => $email,
      'description' => $basicSettings->company_name,
      'amount' => $amount,
      'token' => $token,
    ];

    return $customerData;
  }

  /**
   * Charge customer credit card.
   *
   * @param int $amount
   *   Charge amount.
   * @param string $customerId
   *   Stripe unique customer id.
   *
   * @return bool|string
   *   Response.
   */
  public function chargeCustomerCard(int $amount, string $customerId = '') {
    $result = FALSE;
    $customerId = $customerId ?? variable_get('stripeCustomerId');
    if (empty($customerId)) {
      return 'Stripe customer id is empty.';
    }

    $params = [
      'url' => self::PAYMENT_CHARGE_CUSTOMER,
      'type' => 'POST',
      'post_fields' => [
        'customerId' => $customerId,
        'amount' => $amount,
      ],
    ];

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $result = TRUE;
      // Need log for pay.
    }
    else {
      watchdog('Sms: payment', "Create customer stripe error.", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Get all company payments with amount sum.
   *
   * @return array|bool
   *   Payments with amount sum.
   */
  public function getPayments() {
    $result = [
      'payments' => [],
      'amount' => 0,
    ];

    $params = [
      'url' => self::PAYMENT_RECEIPTS,
      'type' => 'GET',
      'post_fields' => [],
    ];

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $result['payments'] = json_decode($response->getBody());
      foreach ($result['payments'] as $key => $value) {
        $result['amount'] += $value->amount;
      }
      // Need log for pay.
    }
    else {
      watchdog('Get payments error: ', $response->getBody(), array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

}
