<?php

namespace Drupal\move_sms\Services;

use Drupal\move_sms\Traits\SendToJava;

/**
 * Class Phone.
 */
class Phone {

  use SendToJava;

  private const PHONE_BUY_URI = '/phone/buy';
  private const PHONE_FIND_URI = '/phone/find';
  private const PHONE_DELETE_URI = '/phone/delete';

  /**
   * Phone constructor.
   */
  public function __construct() {
    $this->initParams();
  }

  /**
   * Insert 'twilio' phone to db.
   *
   * @param string $number
   *   Phone number.
   * @param string $sid
   *   Twilio phone sid.
   *
   * @return bool|\DatabaseStatementEmpty|\DatabaseStatementInterface|\InsertQuery|\InsertQuery_test|int
   *   Result db insert.
   *
   * @throws \Exception
   */
  private function insertPhone(string $number, string $sid) {
    return db_insert('twilio_phones')
      ->fields([
        'number' => $number,
        'phone_sid' => $sid,
      ])
      ->execute();
  }

  /**
   * Retrieve phone numbers.
   *
   * @return \DatabaseStatementEmpty|\DatabaseStatementInterface|null
   *   Twilio phones data.
   */
  public function getPhoneNumbers() {
    return db_select('twilio_phones', 'tp')
      ->fields('tp')
      ->execute()
      ->fetchAssoc();
  }

  /**
   * Get twilio phone sid by phone number.
   *
   * @param string $phoneNumber
   *   Phone number.
   *
   * @return mixed
   *   Twilio phone sid or empty.
   */
  public function getPhoneSidByNumber(string $phoneNumber) {
    return db_select('twilio_phones', 'tp')
      ->fields('tp', ['phone_sid'])
      ->condition('number', $phoneNumber)
      ->execute()
      ->fetchField();
  }

  /**
   * Delete twilio phone from db.
   *
   * @param string $phoneNumber
   *   Phone number.
   *
   * @return bool|\DeleteQuery|\DeleteQuery_test|int|\QueryConditionInterface
   *   Reulte db_delete.
   */
  private function deletePhone(string $phoneNumber) {
    return db_delete('twilio_phones')
      ->condition('number', $phoneNumber)
      ->execute();
  }

  /**
   * Buy 'twilio' phone number.
   *
   * @param string $number
   *   Phone number.
   *
   * @return bool
   *   Buy or not.
   *
   * @throws \Exception
   */
  public function buy(string $number) {
    $result = FALSE;
    $params = [
      'url' => self::PHONE_BUY_URI,
      'type' => 'POST',
      'post_fields' => '+' . $number,
    ];

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $result = $this->insertPhone($number, $response->getBody());
    }
    else {
      watchdog('Sms: phone', "Sms: buy phone", array(), WATCHDOG_ERROR);
    }

    return $result;
  }

  /**
   * Find 'twilio' phone numbers.
   *
   * @param string $number
   *   Number for find.
   *
   * @return bool
   *   Search results.
   */
  public function find($number) {
    $result = FALSE;

    $params = [
      'url' => self::PHONE_FIND_URI,
      'type' => 'POST',
      'post_fields' => (int) $number,
    ];

    $response = $this->send($params);

    if ($response->getStatusCode() == 200) {
      $response = json_decode($response->getBody());
      $result = $this->prepareFindResponse($response);
    }
    else {
      watchdog('Sms: phone', "Sms: find phone", array(), WATCHDOG_ERROR);
    }

    return $result;
  }

  /**
   * Get token for work with java sms app.
   *
   * @return bool|null
   *   Token.
   */
  public function getToken() {
    return variable_get('smsCompanyToken') ?? (new Company())->login();
  }

  /**
   * Prepare find response.
   *
   * @param mixed $numbers
   *   Numbers.
   *
   * @return array
   *   List of numbers.
   */
  private function prepareFindResponse($numbers) {
    foreach ($numbers as $key => $number) {
      $result[] = $number->phoneNumber->endpoint;
    }

    return $result ?? [];
  }

  /**
   * Delete twilio phone number.
   *
   * @param string $number
   *   Twilio phone number.
   *
   * @return array|bool
   *   True if phone number deleted.
   */
  public function delete(string $number) {
    $result = FALSE;

    $sid = $this->getPhoneSidByNumber($number);

    $params = [
      'url' => self::PHONE_DELETE_URI,
      'type' => 'POST',
      'post_fields' => [
        'number' => $sid,
      ],
    ];

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $result = json_decode($response->getBody());
      $this->deletePhone($number);
    }
    else {
      watchdog('Sms: phone', "Sms: delete phone", array(), WATCHDOG_ERROR);
    }

    return $result;
  }

}
