<?php

namespace Drupal\move_sms\Services;

use Drupal\move_new_log\Services\Log;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\Comments;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\SmsActionTypes;
use Drupal\move_sms\Traits\Constants;
use Drupal\move_sms\Traits\SendToJava;
use Drupal\move_sms\Cron\UpdateBalance;
use Drupal\move_template_builder\Services\TemplateBuilder;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Sms.
 *
 * @package Drupal\move_sms\Services
 */
class Sms {

  use SendToJava;
  use Constants;

  private const SEND_MESSAGE = '/sms/send';
  private const SMS_GET_ALL = '/company/sms/get/all';
  private $smsActionId = 0;
  private $needSend = 0;
  private $fromPhone = '';

  /**
   * Sms constructor.
   *
   * @param int $smsActionId
   *   Sms action id.
   *
   * @throws \Exception
   */
  public function __construct(int $smsActionId = 0) {
    $this->initParams();
    $this->fromPhone = $this->prepareFromNumber();
    $this->smsActionId = $smsActionId;
    if ($smsActionId) {
      $this->needSend = $this->checkTheNeedToSendSms($smsActionId);
    }
  }

  /**
   * Run sending a message.
   *
   * @param int $nid
   *   Move request id.
   * @param object|null $node
   *   Move request object.
   * @param int|null $uid
   *   User id.
   * @param string|null $phoneTo
   *   Phone number.
   * @param null|string $body
   *   Sms text body.
   *
   * @return bool|string
   *   Result.
   *
   * @throws \Drupal\move_sms\Cron\UpdateBalanceException
   * @throws \Exception
   */
  public function execute(int $nid, $node = NULL, ?int $uid = NULL, ?string $phoneTo = '', ?string $body = '') {
    $result = FALSE;

    $wrong_status = array("invalid phone", "blacklist");

    if (empty($this->needSend) && empty($body) || in_array($this->lastSmsStatus($nid), $wrong_status)) {
      return 'Message sending is disabled.';
    }
    $toPhone = $this->prepareToNumber($nid, $node, $uid, $phoneTo);
    if ($toPhone == '11111111111') {
      return FALSE;
    }
    if (empty($body)) {
      $body = $this->prepareTemplate($nid, $node);
    }
    else {
      $body = strip_tags($body);
    }
    $sendResponse = $this->sendSms($this->fromPhone, $toPhone, $body);

    if ($sendResponse && $sendResponse->getStatusCode() == 200) {
      $response = json_decode($sendResponse->getBody());
      $this->createComment($nid, $body, $response->sid, $response->status, $toPhone, $this->fromPhone);
      $this->prepareLog($nid, $this->fromPhone, $toPhone, $body, $response->sid);
      $result = TRUE;
    }
    else {
      watchdog('Sms: sent', "Error with sending a sms", array(), WATCHDOG_ERROR);
    }

    return $result;
  }

  /**
   * Prepare phone number from.
   *
   * @return mixed
   *   Phone number.
   *
   * @throws \Exception
   */
  private function prepareFromNumber() {
    return $this->getCompanyPhones();
  }

  /**
   * Prepare phone number to.
   *
   * @param int $nid
   *   Move request author phone number.
   * @param object|null $node
   *   Move request.
   * @param int|null $uid
   *   User id.
   * @param string $phoneTo
   *   Phone number.
   *
   * @return string
   *   Phone number.
   *
   * @throws \Exception
   */
  private function prepareToNumber(int $nid, $node = NULL, ?int $uid = NULL, string $phoneTo = '') {
    $phoneNumberTo = '';
    switch ($this->smsActionId) {
      case SmsActionTypes::NEW_CLIENT:
//        $phoneNumberTo = Clients::getUserPrimaryPhoneNumber($uid);
        break;

      case SmsActionTypes::NEW_REQUEST:
        $phoneNumberTo = Clients::getAuthorPhoneNumberByNid($nid);
        break;

      case SmsActionTypes::REMINDER_TOMORROW:
        $phoneNumberTo = Clients::getAuthorPhoneNumberByNid($nid);
        break;

      case SmsActionTypes::REMINDER_5DAYS:
        $phoneNumberTo = Clients::getAuthorPhoneNumberByNid($nid);
        break;

      case SmsActionTypes::FOREMAN_ASSIGN:
        $phoneNumberTo = Clients::getUserPrimaryPhoneNumber($uid);
        break;

      case SmsActionTypes::REMINDER_RESERVATION:
        $phoneNumberTo = Clients::getUserPrimaryPhoneNumber($uid);
        break;

      case SmsActionTypes::HOME_ESTIMATE_REMINDER:
        $phoneNumberTo = Clients::getUserPrimaryPhoneNumber($uid);
        break;

      case SmsActionTypes::JOB_DONE:
        $phoneNumberTo = Clients::getUserPrimaryPhoneNumber($uid);
        break;

      case SmsActionTypes::JOB_NOT_CONFIRMED:
        $phoneNumberTo = Clients::getUserPrimaryPhoneNumber($uid);
        break;

      case SmsActionTypes::JOB_CONFIRMED:
        $phoneNumberTo = Clients::getUserPrimaryPhoneNumber($uid);
        break;

      case SmsActionTypes::COMPLETE_RESERVATION:
        $phoneNumberTo = Clients::getUserPrimaryPhoneNumber($uid);
        break;

      case SmsActionTypes::STORAGE_INVOICE:
        $phoneNumberTo = $phoneTo;
        break;

      case SmsActionTypes::FOREMAN_ASSIGN_TO_FOREMAN:
        $phoneNumberTo = Clients::getUserPrimaryPhoneNumber($uid);
        break;

      case SmsActionTypes::REVIEW:
        $phoneNumberTo = Clients::getAuthorPhoneNumberByNid($nid);
        break;

      default:
        $phoneNumberTo = Clients::getAuthorPhoneNumberByNid($nid);
        break;
    }

    $toPhone = '1' . $phoneNumberTo;
    self::validatePhoneNumber($toPhone);
    return $toPhone;
  }

  /**
   * Prepare sms body.
   *
   * @param int $nid
   *   Move request id.
   * @param mixed $node
   *   Move request object.
   *
   * @return mixed
   *   Prepared body.
   */
  private function prepareTemplate(int $nid, $node = NULL) {
    if (empty($node)) {
      $node = node_load($nid);
    }
    $template = $this->getSmsTemplate($this->smsActionId);
    return $this->prepareSmsTemplateTokens($node, $template);
  }

  /**
   * Send sms.
   *
   * @param string $fromNumber
   *   From number.
   * @param string $toNumber
   *   To number.
   * @param string $body
   *   Sms body.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response.
   *
   * @throws \Drupal\move_sms\Cron\UpdateBalanceException
   */
  public function sendSms(string $fromNumber, string $toNumber, string $body) : ?ResponseInterface {
    $result = NULL;
    $params = [
      'url' => self::SEND_MESSAGE,
      'type' => 'POST',
      'post_fields' => [
        'from' => "+{$fromNumber}",
        'to' => "+{$toNumber}",
        'body' => $body,
      ],
    ];

    // Check Balance before sending.
    if (variable_get("stripeBalance", 0) > self::$MINIMUM_BALANCE) {
      $result = $this->send($params);
      UpdateBalance::execute();
    }

    return $result;
  }

  /**
   * Create comment inn request after send sms.
   *
   * @param int $nid
   *   Move request id.
   * @param string $body
   *   Message body.
   * @param string|null $smsSid
   *   Unique sms id.
   * @param string|null $smsStatus
   *   Sms status.
   * @param null|string $toPhone
   *   Where send sms(Client number).
   * @param null|string $fromPhone
   *   Sender phone number.
   *
   * @return bool|int
   *   Comment id.
   *
   * @throws \Exception
   */
  private function createComment(int $nid, string $body, ?string $smsSid, ?string $smsStatus, ?string $toPhone, ?string $fromPhone) {
    $comment = $this->prepareCommentData($nid, $body, $smsSid, $smsStatus, $toPhone, $fromPhone);
    return (new Comments($nid))->create($comment);
  }

  /**
   * Prepare log for move request.
   *
   * @param int $nid
   *   Move request id.
   * @param string $fromPhone
   *   Phone number from.
   * @param string $toPhone
   *   Phone number to.
   * @param string $body
   *   Message body.
   * @param null|string $smsSid
   *   Sms unique id.
   */
  private function prepareLog(int $nid, string $fromPhone, string $toPhone, string $body, ?string $smsSid) {
    $log = new Log($nid, EntityTypes::MOVEREQUEST);
    $log->create(self::prepareLogText($nid, $fromPhone, $toPhone, $body, $smsSid));
  }

  /**
   * Send sms.
   *
   * @param int $uid
   *   User id.
   *
   * @return bool|string
   *   Sent text or exception.
   *
   * @throws \Exception
   */
  public function sendSmsNewUser(int $uid) {
    $result = FALSE;
    $sms = [];

    if (empty($this->needSend)) {
      return 'Message sending is disabled.';
    }

    $sms['fromPhone'] = $this->getCompanyPhones();
    self::validatePhoneNumber($sms['fromPhone']);

    $sms['toPhone'] = '1' . Clients::getUserPrimaryPhoneNumber($uid);
    self::validatePhoneNumber($sms['toPhone']);
    if ($sms['toPhone'] == '11111111111') {
      return FALSE;
    }

    $sms['text'] = $this->getSmsTemplate($this->smsActionId);

    $params = [
      'url' => self::SEND_MESSAGE,
      'type' => 'POST',
      'post_fields' => [
        'from' => "+{$sms['fromPhone']}",
        'to' => "+{$sms['toPhone']}",
        'body' => $sms['text'],
      ],
    ];

    $response = $this->send($params);
    if ($response->getStatusCode() == 200) {
      $result = TRUE;
    }
    else {
      watchdog('Sms: sent', "Error with sending a sms", array(), WATCHDOG_ERROR);
    }

    return $result;
  }

  /**
   * Prepare data for comments.
   *
   * @param int $nid
   *   Move request id.
   * @param string $smsText
   *   Sms text.
   * @param string|null $smsSid
   *   Unique sms id.
   * @param string|null $smsStatus
   *   Sms status.
   * @param null|string $toPhone
   *   Receiver phone number.
   * @param null|string $fromPhone
   *   Sender phone number.
   *
   * @return array
   *   Prepared data.
   */
  private function prepareCommentData(int $nid, string $smsText, ?string $smsSid = '', ?string $smsStatus = 'queued', ?string $toPhone = '', ?string $fromPhone = '') {
    return [
      'format' => 'full_html',
      'message' => "{$smsText}",
      'nid' => $nid,
      'time_zone' => NULL,
      'sms' => TRUE,
      'sms_sid' => $smsSid,
      'sms_status' => $smsStatus,
      'sms_to' => $toPhone,
      'sms_from' => $fromPhone,
    ];
  }

  /**
   * Update sms status.
   *
   * @param string $smsSid
   *   Unique sms id.
   * @param string $smsStatus
   *   Sms status.
   *
   * @return bool
   *   Comment entity.
   */
  public function smsStatusUpdate(string $smsSid, string $smsStatus) {
    try {
      $cid = db_select('field_data_field_sms_sid', 'fss')
        ->condition('fss.field_sms_sid_value', $smsSid)
        ->fields('fss', ['entity_id'])
        ->execute()
        ->fetchField();

      if (!empty($cid)) {
        $entity = entity_metadata_wrapper('comment', $cid);
        $entity->field_sms_status->set($smsStatus);
        $entity->save();
        $result = TRUE;
      }
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog('Update sms status: ', $error_text, array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Get 'twilio' phone numbers.
   *
   * @return array
   *   Phone numbers.
   */
  public function getCompanyPhones() {
    $phones = db_select('twilio_phones', 'tp')
      ->fields('tp', ['number'])
      ->execute()
      ->fetchField();

    return !empty($phones) ? $phones : [];
  }

  /**
   * Prepare message text if set request status to not confirmed.
   *
   * @param int $nid
   *   Move request id.
   *
   * @return string
   *   Message.
   */
  public function prepareSmsMessageNotConfirmed(int $nid) {
    $email = Clients::getAuthorEmailByNid($nid);
    $data = (new Clients())->oneTimeHash($email);
    $basicSettings = json_decode(variable_get('basicsettings', ''));
    $client_url = $basicSettings->client_page_url ?? '';
    $link = $client_url . "/account/#/request/{$nid}?uid={$data['uid']}&timestamp={$data['timestamp']}&hash={$data['hash']}";
    return 'Please check your request - ' . $link;
  }

  /**
   * Get sms template by action id.
   *
   * @param int $smsActionId
   *   Action id.
   *
   * @return mixed
   *   Template data.
   */
  public function getSmsTemplate(int $smsActionId) {
    return db_select('twilio_sms_templates', 'tst')
      ->fields('tst', ['template'])
      ->condition('tst.id', $smsActionId)
      ->execute()
      ->fetchField();
  }

  /**
   * Prepare sms template with token.
   *
   * @param mixed $node
   *   Move request node.
   * @param mixed $template
   *   Sms template.
   *
   * @return mixed
   *   Prepared template.
   */
  private function prepareSmsTemplateTokens($node, $template) {
    $custom = (new TemplateBuilder())->moveRequestGetVariablesArray($node);
    $preparedTemplate = token_replace($template, $custom);

    return $preparedTemplate;
  }

  /**
   * Validate phone number.
   *
   * @param mixed $phone
   *   Phone number.
   *
   * @return bool
   *   True if phone is valid.
   *
   * @throws \Exception
   */
  public static function validatePhoneNumber($phone) {
    if (!preg_match("/^[0-9]{11}$/", $phone)) {
      throw new \Exception("Not valid phone number - $phone", 400);
    }
    return TRUE;
  }

  /**
   * Prepare log for .
   *
   * @param int $nid
   *   Node id.
   * @param string $fromNumber
   *   Phone number.
   * @param string $toNumber
   *   Phone number.
   * @param string $text
   *   Body.
   * @param string|null $smsSid
   *   Sms status.
   *
   * @return array
   *   Log data.
   */
  public static function prepareLogText(int $nid, string $fromNumber, string $toNumber, string $text, ?string $smsSid) {
    if (!empty($smsSid)) {
      $title = "Request#$nid Sms message sent";
    }
    else {
      $title = "Request#$nid Sms message not sent";
    }
    $log_data[] = array(
      'details' => array([
        'activity' => 'System',
        'title' => $title,
        'text' => array([
          'text' => 'Sms message: ' . $text,
          'from' => $fromNumber,
          'to' => $toNumber,
        ],
        ),
        'date' => time(),
      ],
      ),
      'source' => 'System',
    );

    return $log_data;
  }

  /**
   * Get messages for current month.
   *
   * @param string $datefrom
   *   Date from.
   * @param string $dateto
   *   Date to.
   * @param string $number
   *   Twilio phone number.
   *
   * @return array|bool|mixed|string
   *   Messages for phone number.
   */
  public function smsAll(string $datefrom, string $dateto, string $number) {
    $uuid = variable_get('smsCompanyUuid', FALSE);

    if (empty($uuid)) {
      return "Company not registered. Please register button first";
    }

    $params = array(
      'url' => self::SMS_GET_ALL,
      'type' => 'POST',
      'post_fields' => [
        'uuid' => $uuid,
        'number' => '+' . $number,
        'dateFrom' => $datefrom,
        'dateTo' => $dateto,
      ],
    );

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $response = json_decode($response->getBody());
    }
    else {
      watchdog('Sms: sent', "Can not get sms", array(), WATCHDOG_ERROR);
    }

    return $response ?? FALSE;
  }

  /**
   * Get move request author sms.
   *
   * @param int $nid
   *   Move request id.
   *
   * @throws \Exception
   */
  public function getSmsForUser(int $nid) {
    $sms['toPhone'] = '1' . Clients::getAuthorPhoneNumberByNid($nid);
    self::validatePhoneNumber($sms['toPhone']);

    $params = [];

    $this->send($params);
  }

  /**
   * @param $nid
   *
   * @throws \Exception
   */
  public function getSmsForRequest($nid) {
    $comments = (new Comments($nid))->getCommentsByNode();
    $sms_comments = array();
    foreach ($comments as $comment) {
      if ($comment['sms']) {
        array_push($sms_comments, $comment);
      }
    }

    return $sms_comments;
  }

  /**
   * Get sms templates.
   *
   * @return array
   *   All templates.
   */
  public function getSmsTemplates() {
    return db_select('twilio_sms_templates', 'st')
      ->fields('st', [])
      ->execute()
      ->fetchAll();
  }

  /**
   * Update sms template setting.
   *
   * @param int $id
   *   Sms template id.
   * @param array $data
   *   Template settings.
   *
   * @return array
   *   All templates.
   */
  public function updateTemplate(int $id, array $data) {
    $preparedData = $this->prepareTemplateData($data);
    if (empty($data)) {
      return ["Nothing to update."];
    }
    return db_update('twilio_sms_templates')
      ->fields($preparedData)
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Prepare data for update db sms template record.
   *
   * @param array $data
   *   Template data for db record update.
   *
   * @return array
   *   Prepared array.
   */
  private function prepareTemplateData(array $data) {
    $result = [];
    if (isset($data['template'])) {
      $result['template'] = $data['template'];
    }
    if (isset($data['active'])) {
      if ($data['active'] > 0) {
        $result['active'] = 1;
      }
      else {
        $result['active'] = 0;
      }
    }

    return $result;
  }

  /**
   * Check the need to send sms message.
   *
   * @param int $smsActionId
   *   Unique action id.
   *
   * @return int
   *   True if we need send.
   */
  public function checkTheNeedToSendSms(int $smsActionId) : int {
    if ($this->checkSmsEnable() && !empty($this->fromPhone)) {
      $result = db_select('twilio_sms_templates', 'tst')
        ->fields('tst', ['active'])
        ->condition('tst.id', $smsActionId)
        ->execute()
        ->fetchField();
    }

    return $result ?? 0;
  }

  /**
   * Check sms enabled or not.
   *
   * @return bool
   *   True and false.
   */
  public function checkSmsEnable() {
    return variable_get("smsEnable", FALSE);
  }

  /**
   * Receive sms from client.
   *
   * @param string $body
   *   Message body.
   * @param string $sid
   *   Sms id.
   * @param string $to
   *   To number.
   *
   * @return bool|int
   *   Comment id.
   *
   * @throws \Exception
   */
  public function smsCreate(string $body, string $sid, string $to) {
    global $user;

    if (!empty($to)) {
      $to = preg_replace('/[^0-9]/', '', $to);
    }
    $query = db_select('field_data_field_sms_to', 'st');
    $query->innerJoin('move_request_comments', 'mrc', 'mrc.cid=st.entity_id');
    $query->condition('st.field_sms_to_value', $to);
    $query->fields('mrc', array('nid'));
    $query->orderBy('st.entity_id', 'DESC');
    $query->range(0, 1);
    $nid = $query->execute()->fetchField();

    $uid = db_select('node', 'n')
      ->fields('n', array('uid'))
      ->condition('n.nid', $nid)
      ->condition('n.type', 'move_request')
      ->execute()
      ->fetchField();
    $user = user_load($uid);

    // If received command STOP when change status to blacklist.
    $status = 'delivered';
    if (strtolower($body) == 'stop') {
      $status = 'blacklist';
    }

    if ($nid) {
      $result = $this->createComment($nid, $body, $sid, $status, $this->fromPhone, $to);
    }

    return (!empty($result)) ?? FALSE;
  }

  /**
   * @param $nid
   *
   * @return mixed
   * @throws \Exception
   */
  public function lastSmsStatus($nid) {
    $list_sms = $this->getSmsForRequest($nid);
    $last_sms = end($list_sms);
    return $last_sms['sms_status'];
  }

}
