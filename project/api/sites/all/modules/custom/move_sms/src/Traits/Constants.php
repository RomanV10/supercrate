<?php

namespace Drupal\move_sms\Traits;

/**
 * Trait Constants.
 *
 * @package Drupal\move_sms\Traits
 */
trait Constants {

  private static $MINIMUM_BALANCE = 10;
  private static $AMOUNT_CHARGE = 50;

}
