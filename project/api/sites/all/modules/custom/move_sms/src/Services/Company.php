<?php

namespace Drupal\move_sms\Services;

use Drupal\move_sms\Traits\SendToJava;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

/**
 * Class Company.
 *
 * @package Drupal\move_sms\Services
 */
class Company {

  use SendToJava;

  private const REGISTER_COMPANY_URI = '/company/register';
  private const UPDATE_COMPANY_URI = '/company/update';
  private const LOGIN_COMPANY_URI = '/company/login';
  private const SUBACCOUNT_CREATE = '/subaccount/create';
  private const SUBACCOUNT_USAGE = '/company/usage';
  private const SUBACCOUNT_CLOSE = '/company/close';
  private const SUBACCOUNT_SUSPEND = '/company/suspend';
  private const SUBACCOUNT_REACTIVATE = '/company/reactivate';
  private const PAYMENT = '/payment/charge';
  private const SMS_RATE = 3;

  /**
   * Company constructor.
   */
  public function __construct() {
    $this->initParams();
  }

  /**
   * Register company in java application.
   *
   * @return bool
   *   Register company or not.
   */
  public function register() {
    $params = array(
      'url' => self::REGISTER_COMPANY_URI,
      'type' => 'POST',
      'post_fields' => array(
        'host' => $this->host,
        'port' => $this->port,
      ),
    );

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $response = json_decode($response->getBody());
      variable_set('smsCompanyUuid', $response->uuid);
      variable_set('smsCompanyToken', $response->token);
      $result = TRUE;
    }
    else {
      watchdog('Sms: Register', "Company was not created.", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Update company data in java app and "Twilio".
   *
   * @return bool|string
   *   Result.
   */
  public function update() {
    if (empty($this->uuid)) {
      return "Company not registered. Please register button first";
    }

    $params = array(
      'url' => self::UPDATE_COMPANY_URI,
      'type' => 'POST',
      'post_fields' => array(
        'host' => $this->host,
        'port' => $this->port,
        'uuid' => $this->uuid,
      ),
    );

    $response = $this->send($params);
    if ($response->getStatusCode() == 200) {
      $result = TRUE;
    }
    else {
      watchdog('Sms: Update', "Company was not updated.", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Login company in java sms app.
   *
   * @return bool
   *   Success or not.
   */
  public function login() {
    $params = array(
      'url' => self::LOGIN_COMPANY_URI,
      'type' => 'POST',
      'post_fields' => array(
        'uuid' => $this->uuid,
      ),
    );

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $response = json_decode($response->getBody());
      variable_set('smsCompanyToken', $response->token);
      $result = $response->token;
    }
    else {
      watchdog('Sms: Login', "Company login(token).", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Create twilio subaccount.
   *
   * @return bool
   *   Java app token.
   */
  public function createSubAccount() {
    $params = array(
      'url' => self::SUBACCOUNT_CREATE,
      'type' => 'POST',
      'post_fields' => [],
    );

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $response = json_decode($response->getBody());
      $result = $response->token;
    }
    else {
      watchdog('Sms: SubAccount', "Sms: SubAccount do not created", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Permanently close a twilio subaccount.
   *
   * @return bool
   *   True if sub account closed.
   */
  public function closeSubAccount() {
    if (empty($this->uuid)) {
      return "Company not registered. Please register button first";
    }
    $params = array(
      'url' => self::SUBACCOUNT_CLOSE,
      'type' => 'POST',
      'post_fields' => [],
    );

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $result = json_decode($response->getBody());
    }
    else {
      watchdog('Sms: SubAccount', "Sms: SubAccount do not closed.", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Suspending a Subaccount.
   *
   * @return bool
   *   True if sub account suspend.
   */
  public function suspendSubaccount() {
    if (empty($this->uuid)) {
      return "Company not registered. Please register button first";
    }
    $params = array(
      'url' => self::SUBACCOUNT_SUSPEND,
      'type' => 'POST',
      'post_fields' => [],
    );

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $result = json_decode($response->getBody());
    }
    else {
      watchdog('Sms: SubAccount', "Sms: SubAccount do not suspend.", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Re-activate a Subaccount.
   *
   * @return bool
   *   True if sub account suspend.
   */
  public function reactivateSubaccount() {
    if (empty($this->uuid)) {
      return "Company not registered. Please register button first";
    }
    $params = array(
      'url' => self::SUBACCOUNT_REACTIVATE,
      'type' => 'POST',
      'post_fields' => [],
    );

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $result = json_decode($response->getBody());
    }
    else {
      watchdog('Sms: SubAccount', "Sms: SubAccount do not reactivated.", array(), WATCHDOG_ERROR);
    }

    return $result ?? FALSE;
  }

  /**
   * Get info about company usage Twilio services.
   *
   * @return array|bool|mixed
   *   Data: usage twilio services.
   */
  public function companyUsage() {
    if (empty($this->uuid)) {
      return "Company not registered. Please register button first";
    }

    $params = array(
      'url' => self::SUBACCOUNT_USAGE,
      'type' => 'POST',
      'post_fields' => [],
    );

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $response = json_decode($response->getBody());
      $response->balance = $this->calcBalance($response);
    }
    else {
      watchdog('Sms: SubAccount', "Sms: Can not get subAccount usage.", array(), WATCHDOG_ERROR);
    }

    return $response ?? FALSE;
  }

  // Remove if not use.
  public function calcBillingForSubAcc() {
    if (empty($this->uuid)) {
      return 'Company uuid is empty. Please register your company';
    }
  }

  /**
   * Prepare company balance.
   *
   * @param mixed $response
   *   Object data expenses: smsSum and phone rent.
   *
   * @return float
   *   Balance.
   */
  private function calcBalance($response) : float {
    $paymentsSum = (new Payment())->getPayments()['amount'] / 100;
    $expensesSum = $response->smsPrice * self::SMS_RATE + $response->phoneNumbersPrice;
    return round($paymentsSum - $expensesSum, 4);
  }

  /**
   * Create payment for sms.
   *
   * @param mixed $data
   *   Data from stripe: token and amount.
   *
   * @return array|bool|mixed|string
   *   Result.
   */
  public function paymentTwilio($data) {
    if (empty($this->uuid)) {
      return "Company not registered. Please register button first";
    }

    $params = array(
      'url' => self::PAYMENT,
      'type' => 'POST',
      'post_fields' => $data,
    );

    $response = $this->send($params);
    if ($response && $response->getStatusCode() == 200) {
      $response = json_decode($response->getBody());
    }
    else {
      watchdog('Sms: Payment', "Sms: Can not create payment.", array(), WATCHDOG_ERROR);
    }

    return $response ?? FALSE;
  }

}
