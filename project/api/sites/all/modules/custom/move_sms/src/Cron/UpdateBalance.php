<?php

namespace Drupal\move_sms\Cron;

use Drupal\move_services_new\Cron\Tasks\CronInterface;
use Drupal\move_sms\Services\Company;
use Drupal\move_sms\Traits\Constants;

/**
 * Class UpdateBalance.
 *
 * @package Drupal\move_sms\Cron
 */
class UpdateBalance implements CronInterface  {

  use Constants;

  /**
   * Execute task by cron.
   *
   * @throws \Drupal\move_sms\Cron\UpdateBalanceException
   */
  public static function execute(): void {
    $company_usage = (new Company())->companyUsage();
    if ($company_usage && property_exists($company_usage, "balance")) {
      variable_set("stripeBalance", $company_usage->balance);
    }
    else {
      throw new UpdateBalanceException("Error when attempt calculate or update twilio balance.");
    }
  }

}

/**
 * Class AutoChargeException.
 *
 * @package Drupal\move_sms\Cron
 */
class UpdateBalanceException extends \Exception {}