<?php
/**
 * @file
 * move_sms.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function move_sms_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'sms settings access'.
  $permissions['sms settings access'] = array(
    'name' => 'sms settings access',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'move_sms',
  );

  return $permissions;
}
