<?php

use Drupal\move_new_log\Services\Log;
use Drupal\move_parser\Services\Parser;
use Drupal\move_services_new\Util\enum\EntityTypes;

/**
 * Batch for create move request.
 *
 * @throws ServicesException
 */
function granot_execute_batch($read_data, &$context) {
  try {
    $data_for_requests = federalmoving_set_data($read_data);
    $notes = prepare_omega_notes($read_data);
    $result = granot_create_request($data_for_requests, $notes);
    $context['results']['titles'][] = $result['nid'];

    $log = (new Parser())->arrayToStringLog($read_data);

    // Create log with all data from granot xls file.
    if ($result['nid']) {
      $instance = new Log($result['nid'], EntityTypes::MOVEREQUEST);
      $data[] = array(
        'details' => array([
          'activity' => 'System',
          'title' => 'Request Created',
          'text' => array(['text' => $log]),
          'date' => time(),
          'info' => array(),
        ],
        ),
        'source' => 'System',
        'date' => time(),
      );
      $instance->create($data);
    }
    // Set reservation if status confirmed or not confirmed.
    if ($data_for_requests['field_approve'] == 2 || $data_for_requests['field_approve'] == 3) {
      $node_wrapper = entity_metadata_wrapper('node', $result['nid']);
      $node_wrapper->field_reservation_price->set(0);
      $node_wrapper->field_reservation_received->set(1);
      $node_wrapper->save();
    }

    $context['message'] = 'Создан реквест номер <em>' . check_plain($result['nid']) . '</em>';
  }
  catch (\Throwable $e) {
    $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()}";
    watchdog('Granot execute batch', $message, array(), WATCHDOG_ERROR);
    $error_msg = array(
      'status' => FALSE,
      'text' => $e->getMessage(),
    );
    throw new \ServicesException('', 400, $error_msg);
  }
}

/**
 * Batch finished.
 */
function move_granot_parser_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Created ' . count($results['titles']) . ' requests.');
  }
  else {
    drupal_set_message('Ended with errors', 'error');
  }
}
