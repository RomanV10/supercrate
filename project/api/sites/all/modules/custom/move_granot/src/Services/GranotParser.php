<?php

namespace Drupal\move_granot\Services;

use Drupal\move_services_new\Services\Payroll;
use Drupal\move_parser\Parser\ParserExtends;

/**
 * Class GranotParser.
 *
 * @package Drupal\move_granot\Services
 */
class GranotParser extends ParserExtends {

  public static function fileCountRow($xls) {
    static::checkFileExist($xls);
    $objPHPExcel = \PHPExcel_IOFactory::load($xls);
    $count_row = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

    return $count_row;
  }

  public static function getAllFileRecords($xls) {
    static::checkFileExist($xls);
    $objPHPExcel = \PHPExcel_IOFactory::load($xls);
    $count_row = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

    return $count_row;
  }

  public function granot_request_status($move_date, $booked_date) {
    $field_approve = 1;
    $move_date = new \DateTime($move_date);
    $booked = preg_match("/[\d]+/", $booked_date, $match);
    $booked_date = ($booked) ? new \DateTime($booked_date) : '';
    if (is_object($booked_date)) {

    }
    $current_date = new \DateTime(date('m/d/Y', time()));

    if ($move_date < $current_date && !$booked_date) {
      $field_approve = 12;
    }
    elseif ($move_date <= $current_date && $booked_date) {
      $field_approve = 3;
    }
    elseif ($move_date >= $current_date && !$booked_date) {
      $field_approve = 1;
    }
    elseif ($move_date > $current_date && $booked_date) {
      $field_approve = 2;
    }

    return $field_approve;
  }

  /**
   * Check move request status by move date.
   *
   * @param $move_date
   *   Move date.
   *
   * @return mixed
   *   Request status and flag.
   */
  public static function oscar_request_status($move_date) {
    $data['field_approve'] = 1;
    $data['field_company_flags'] = [];

    $current_date = new \DateTime(date('Y-m-d', time()));
    $move_date = new \DateTime($move_date);

    if ($current_date > $move_date) {
      $data['field_approve'] = 3;
      $completed_flag = Payroll::getCompletedFlag();
      $data['field_company_flags'] = [$completed_flag];
    }

    return $data;
  }

  /**
   * Prepare move request status.
   *
   * @param string|null $request_status
   *   Request status from xls file.
   * @param string $move_date
   *   Move date.
   * @param string $booked_date
   *   Booked date.
   *
   * @return array
   *   field_approve and field_company_flags.
   */
  public function omega_request_status_flag(?string $request_status = '', $move_date = '', $booked_date = '') {
    $current_date = new \DateTime(date('m/d/Y', time()));
    $job_status = strtolower($request_status);
    $data['field_approve'] = 1;
    $data['field_company_flags'] = [];

    if ($job_status == 'cancelled') {
      $data['field_approve'] = 5;
    }
    elseif ($job_status == 'done') {
      $data['field_approve'] = 3;
      $completed_flag = Payroll::getCompletedFlag();
      $data['field_company_flags'] = [$completed_flag];
    }
    elseif ($move_date <= $current_date && $booked_date) {
      $data['field_approve'] = 3;
    }
    elseif ($move_date >= $current_date && !$booked_date) {
      $data['field_approve'] = 1;
    }
    elseif ($move_date > $current_date && $booked_date) {
      $data['field_approve'] = 2;
    }

    return $data;
  }

  public function omega_prepare_user_phone($phone_data) {
    if (isset($phone_data)) {
      preg_match_all('((\(\d*\)\s*\d*-\d*)|(\d*-\d*-\d*))', $phone_data, $matches);
    }

    return array(
      'primary' => !empty($matches[0][0]) ? $matches[0][0] : '',
      'work' => !empty($matches[0][1]) ? $matches[0][1] : '',
      'home' => !empty($matches[0][2]) ? $matches[0][2] : '',
    );
  }

  public function granot_request_rate($estimate, $cuft) {
    if ($cuft) {
      $rate = $estimate / $cuft;
    }
    else {
      $rate = 0;
    }

    return round($rate, 1);
  }

  public function granot_request_cbf($lbs, $cuft) {
    $cbf = $lbs / $cuft;

    return $cbf;
  }

  public function granot_request_service_type(string $move_service_type) {
    $field_move_service_type = 1;
    if ($move_service_type == 'long Dist.' || $move_service_type == 'Intl') {
      $field_move_service_type = 7;
    }
    elseif ($move_service_type == 'Local') {
      $field_move_service_type = 1;
    }

    return $field_move_service_type;
  }

  public function granot_request_size_of_move($lbs) {
    $lbs_string = $lbs . 'lbs';
    return $this->calcMoveSize($lbs_string);
  }

}
