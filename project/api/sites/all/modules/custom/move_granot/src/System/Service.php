<?php

namespace Drupal\move_granot\System;

/**
 * Class Service.
 *
 * @package Drupal\move_granot\System
 */
class Service {

  /**
   * Get resources.
   *
   * @return array
   *   Definitions.
   */
  public function getResources() {
    $resources = array(
      '#api_version' => 3003,
    );

    $resources += self::definition();
    return $resources;
  }

  /**
   * Definition.
   *
   * @return array
   *   Empty array.
   */
  private static function definition() {
    return array();
  }

}
