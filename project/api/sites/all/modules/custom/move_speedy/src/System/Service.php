<?php

namespace Drupal\move_speedy\System;

/**
 * Class Service.
 *
 * @package Drupal\move_speedy\System
 */
class Service {

  /**
   * Function get resources.
   *
   * @return array
   *   Resources.
   */
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );
    $resources += self::definition();
    return $resources;
  }

  /**
   * Definition operations and actions services.
   *
   * @return array
   *   Services.
   */
  private static function definition() {
    return array(
      'move_speedy' => array(
        'operations' => array(),
        'actions' => array(),
      ),
    );
  }

}
