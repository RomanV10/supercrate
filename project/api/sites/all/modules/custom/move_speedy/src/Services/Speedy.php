<?php

namespace Drupal\move_speedy\Services;

use Drupal\move_new_log\Services\Log;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\move_request\MoveRequestRetrieve;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class Speedy.
 *
 * @package Drupal\move_speedy\Services
 */
class Speedy {

  /**
   * Move request id.
   *
   * @var int|null
   */
  public $nid = NULL;

  /**
   * Node data.
   *
   * @var \entityMetadataWrapper
   */
  public $node_wrapper = NULL;

  /**
   * Speedy settings.
   *
   * @var object
   */
  private $speedySettings;

  /**
   * Speedy constructor.
   *
   * @param int|null $nid
   *   Move request id.
   * @param \stdClass|null $node_wrapper
   *   Node wrapper object.
   */
  public function __construct($nid = NULL, $node_wrapper = NULL) {
    if ($node_wrapper) {
      $this->node_wrapper = $node_wrapper;
    }
    $this->nid = $nid;
    $this->speedySettings = json_decode(variable_get('speedySettings', ''));
  }

  /**
   * Login to speedy, prepare job data and post it to speedy.
   *
   * @throws \Exception
   */
  public function execute() {
    if (empty($this->speedySettings->enable)) {
      return;
    }
    $login = $this->login();
    $token = self::getIdToken($login);
    $jobUrl = $this->preparePostJobUrl($token);
    $jobFields = $this->getJobFields();
    $responseSendJob = $this->sendJob($jobUrl, $jobFields);
    $prepared_response = self::prepareResponseSendJob($responseSendJob);
    $log_data = $this->prepareLogData($prepared_response);
    self::saveLog($this->nid, $log_data['title'], $log_data['body']);
  }

  /**
   * Prepare data for write log.
   *
   * @param mixed $prepared_response
   *   Response form Speedy soft.
   *
   * @return array
   *   Data for write log.
   */
  private function prepareLogData($prepared_response) :array {
    if (self::saveSpeedyJobIdToRequest($this, $prepared_response['job_id'])) {
      $result['title'] = 'New request created in Speedy';
      $result['body'] = "Request #{$this->nid} has been created in your Speedy account with id: {$prepared_response['job_id']}";
    }
    else {
      $result['title'] = 'New request not created in Speedy';
      $result['body'] = "Request #{$this->nid} has not been created, error:
       Data error: {$prepared_response['data_error']}; 
       Code: {$prepared_response['code']}; Error: {$prepared_response['error']}";
      watchdog("Speedy error", $result['body'], array(), WATCHDOG_ERROR);
    }
    return $result;
  }

  /**
   * Return unique job id from Speedy soft.
   *
   * @param mixed $response
   *   Response from Speedy soft.
   *
   * @return mixed
   *   String.
   */
  private static function prepareResponseSendJob($response) :array {
    $data = json_decode($response->data);
    return [
      'code' => !empty($response->code) ? $response->code : '',
      'job_id' => !empty($data->name) ? $data->name : '',
      'data_error' => !empty($data->error) ? $data->error : '',
      'error' => !empty($response->error) ? $response->error : '',
    ];
  }

  /**
   * Method authorization in Speedy soft.
   *
   * @return object
   *   Response from Speedy soft.
   */
  public function login() {
    $data = self::prepareDataForGetIdToken();
    $options = [
      'data' => json_encode($data),
      'method' => 'POST',
      'headers' => array(
        'Content-Type' => 'application/json',
        'accept' => 'application/json',
      ),
    ];
    $response = drupal_http_request($this->speedySettings->urlLogin, $options);

    return $response;
  }

  /**
   * Get id token from Speedy soft.
   *
   * @param object $response
   *   Response from Speedy soft.
   *
   * @return string
   *   Token.
   */
  private static function getIdToken($response) :string {
    if ($response->code == 200 && property_exists($response, 'data')) {
      return json_decode($response->data)->idToken;
    }
  }

  /**
   * Prepare data for get response from Speedy soft.
   *
   * @return array
   *   Data.
   */
  private function prepareDataForGetIdToken() :array {
    return [
      'email' => $this->speedySettings->email,
      'password' => $this->speedySettings->password,
      'returnSecureToken' => $this->speedySettings->returnSecureToken,
    ];
  }

  /**
   * Prepare url for post job to Speedy soft.
   *
   * @param string $idToken
   *   Token.
   *
   * @return string
   *   Url.
   */
  private function preparePostJobUrl(string $idToken) : string {
    return $this->speedySettings->urlPostJob . $this->speedySettings->companyKey . '/jobs.json?auth=' . $idToken;
  }

  /**
   * Prepare fields for set job to Speedy soft.
   *
   * @return array
   *   Job fields.
   *
   * @throws \Exception
   */
  private function getJobFields() {
    $node_wrapper = $this->node_wrapper;
    $request = new MoveRequestRetrieve($node_wrapper);
    $node = $request->nodeFields();

    $inst = new MoveRequest($node['nid']);
    $notes = $inst->get_notes(['foreman_notes']);

    return [
      'companyKey' => $this->speedySettings->companyKey,
      'createDateTime' => $node['created'] * 1000,
      'customerEmail' => $node['email'],
      'customerFirstName' => $node['first_name'],
      'customerLastName' => $node['last_name'],
      'customerPhone' => preg_replace('/[^0-9.]+/', '', $node['phone']),
      'deliveryEarliestDate' => $node['date']['raw'] * 1000,
      'deliveryLatestDate' => $node['date']['raw'] * 1000,
      'destinationAddress' => [
        'addressLine2' => $node['field_moving_to']['premise'] ?? '',
        'city' => $node['field_moving_to']['locality'] ?? '',
        'typeOfHouse' => $node['type_to']['value'] ?? '',
        'closeLocation' => '',
        'state' => $node['field_moving_to']['administrative_area'] ?? '',
        'street' => $node['field_moving_to']['thoroughfare'] ?? '',
        'zip' => $node['field_moving_to']['postal_code'] ?? '',
      ],
      'originAddress' => [
        'addressLine2' => $node['field_moving_from']['premise'] ?? '',
        'city' => $node['field_moving_from']['locality'] ?? '',
        'typeOfHouse' => $node['type_from']['value'] ?? '',
        'closeLocation' => '',
        'state' => $node['field_moving_from']['administrative_area'] ?? '',
        'street' => $node['field_moving_from']['thoroughfare'] ?? '',
        'zip' => $node['field_moving_from']['postal_code'] ?? '',
      ],
      'isCancelled' => FALSE,
      'isPriority' => FALSE,
      'jobNumber' => $node['nid'],
      'lifecycle' => 'New',
      'pickupDateTime' => $node['date']['raw'] * 1000,
      'storageInTransit' => TRUE,
      'uidOfCreatingUser' => $this->speedySettings->uid,
      'estimates' => [
        'totalCubicFeet' => $node['request_all_data']['invoice']['closing_weight']['value'] ?? 0,
      ],
      'entranceNotesOrigin' => '',
      'distanceDoorToParkingOrigin' => '',
      'entranceNotesDestination' => '',
      'distanceDoorToParkingDestination' => '',
      'extraPickupAddress' => self::prepareExtraAddressToString($node['field_extra_pickup']),
      'extraDropOffAddress' => self::prepareExtraAddressToString($node['field_extra_dropoff']),
      'pickupDateTimeEarliest' => $node['date']['raw'] * 1000,
      'pickupDateTimeLatest' => $node['date']['raw'] * 1000,
      'foremanNotes' => $notes->foreman_notes ?? '',
    ];
  }

  /**
   * Prepare string with extra pickup or drop off address.
   *
   * @param array $address
   *   Address data.
   *
   * @return string
   *   Extra address.
   */
  private static function prepareExtraAddressToString(array $address) {
    return $address['premise'] . ' ' . $address['thoroughfare'] . ' ' . $address['locality'] .
      ' ' . $address['administrative_area'] . ' ' . $address['postal_code'] . ' ' . $address['organisation_name'];
  }

  /**
   * Send job to Speedy soft.
   *
   * @param string $speedyPostJobUrl
   *   Url for request to Speedy.
   * @param array $jobFields
   *   Job fields.
   *
   * @return object
   *   Response.
   */
  private function sendJob(string $speedyPostJobUrl, array $jobFields) {
    $options = [
      'data' => json_encode($jobFields),
      'method' => 'POST',
      'headers' => array(
        'Content-Type' => 'application/json',
        'accept' => 'application/json',
      ),
    ];
    $response = drupal_http_request($speedyPostJobUrl, $options);
    return $response;
  }

  /**
   * Update move request and save unique field_speedy_id.
   *
   * @param Speedy $speedy
   *   Move request id.
   * @param string $job_id
   *   Unique job id from Speedy fost.
   *
   * @return array|bool
   *   Result update if success or false.
   */
  private static function saveSpeedyJobIdToRequest(Speedy $speedy, string $job_id) {
    if (empty($job_id)) {
      return FALSE;
    }
    $speedy->node_wrapper->field_speedy_id->set($job_id);
    $speedy->node_wrapper->save();
    return TRUE;
  }

  /**
   * Save log to db.
   *
   * @param int $nid
   *   Move request id.
   * @param string $log_title
   *   Title log.
   * @param string $log_body
   *   Log body.
   */
  private static function saveLog(int $nid, string $log_title, string $log_body) {
    $speedyLog = new Log($nid, EntityTypes::MOVEREQUEST);
    $log_data[] = array(
      'details' => array(
        [
          'activity' => 'System',
          'title' => $log_title,
          'text' => [['text' => $log_body]],
          'date' => time(),
        ],
      ),
      'source' => 'System',
    );
    $speedyLog->create($log_data);
  }

  /**
   * Check if you need to send request.
   *
   * @return bool
   *   True if need send.
   */
  public function needToSendRequest() {
    if ($this->isSpeedyDisabled()) {
      return FALSE;
    }

    if ($this->isMovingStorageRequest()
      || $this->isFlatRateRequest()
      || $this->isLongDistanceRequest()
      || $this->distanceCondition()) {
      $result = TRUE;
    }

    return $result ?? FALSE;
  }

  /**
   * Check speedy settings.
   *
   * @return bool
   *   False if disabled.
   */
  private function isSpeedyDisabled() {
    return empty($this->speedySettings->enable);
  }

  /**
   * Check move request service type is "moving and storage".
   *
   * @return bool
   *   True if it's true.
   */
  private function isMovingStorageRequest() {
    return $this->node_wrapper->field_move_service_type->value() == RequestServiceType::MOVING_AND_STORAGE;
  }

  /**
   * Check move request service type is "Flat Rate".
   *
   * @return bool
   *   True if it's true.
   */
  private function isFlatRateRequest() {
    return $this->node_wrapper->field_move_service_type->value() == RequestServiceType::FLAT_RATE;
  }

  /**
   * Check move request service type is "Long Distance".
   *
   * @return bool
   *   True if it's true.
   */
  private function isLongDistanceRequest() {
    return $this->node_wrapper->field_move_service_type->value() == RequestServiceType::LONG_DISTANCE;
  }

  /**
   * Check if move request distance is more than 200 miles.
   *
   * @return bool
   *   True if it's true.
   */
  private function distanceCondition() {
    return !empty($this->node_wrapper->field_distance->value()) && $this->node_wrapper->field_distance->value() > 200;
  }

}
