<?php

namespace Drupal\move_request_score\Models;

use PDO;

/**
 * Class MoveRequestScoreTotal. Model.
 *
 * @package Drupal\move_request_score\Models
 */
class MoveRequestScoreTotal {

  /**
   * DB table name.
   */
  const TABLE_NAME = 'move_request_score_total';

  /**
   * Selectable fields list.
   *
   * @var array
   */
  private static $fields = [
    'nid',
    'score',
    'updated',
  ];

  /**
   * Editable fields list.
   *
   * @var array
   */
  private static $fillable = [
    'nid',
    'score',
    'updated',
  ];

  private $data = [];

  /**
   * NotificationType constructor.
   *
   * @param array $data
   *   Processed NotificationType data(identifier, score, title).
   */
  public function __construct(array $data) {
    $this->data = $data;
    $this->data['updated'] = time();
  }

  /**
   * Returns NotificationType score.
   *
   * @return int
   *   Score points int value.
   */
  public function getScore(): int {
    return $this->data['score'];
  }

  /**
   * Save total score sum.
   *
   * @return mixed
   *   Result.
   *
   * @throws \Exception
   */
  public function saveState() {
    $data = array_intersect_key($this->data, array_flip(self::$fillable));
    $nid = $data['nid'];
    $score = $data['score'];
    $updated = $data['updated'];
    if (static::getByNid($nid)) {
      $result = db_merge(static::TABLE_NAME)
        ->key(array('nid' => $nid))
        ->fields(array(
          'updated' => $updated,
        ))
        ->expression('score', 'score + :score', array(':score' => $score))
        ->execute();
    }
    else {
      $result = $this->insertState();
    }
    return $result;
  }

  /**
   * Update total score value
   *
   * @return int
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function updateTotalScore() : int {
    $data = array_intersect_key($this->data, array_flip(self::$fillable));
    $nid = $data['nid'];
    $score = $data['score'];
    $updated = $data['updated'];

    return db_merge(static::TABLE_NAME)
      ->key(array('nid' => $nid))
      ->fields(array(
        'updated' => $updated,
        'score' => $score,
      ))
      ->execute();
  }

  /**
   * Insert data to db.
   *
   * @return \DatabaseStatementInterface|int
   * @throws \Exception
   */
  public function insertState() {
    $data = array_intersect_key($this->data, array_flip(self::$fillable));
    return db_insert(self::TABLE_NAME)
      ->fields($data)
      ->execute();
  }

  /**
   * Get score by request id.
   *
   * @param int $nid
   *   Request id.
   *
   * @return mixed
   *   Db select.
   */
  public static function getByNid($nid) {
    return db_select(static::TABLE_NAME, 'score_total')
      ->fields('score_total')
      ->condition('nid', $nid)
      ->execute()
      ->fetchAssoc();
  }

}
