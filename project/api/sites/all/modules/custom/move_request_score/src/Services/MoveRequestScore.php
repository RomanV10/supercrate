<?php

namespace Drupal\move_request_score\Services;

use Drupal\move_new_log\Services\Log;
use Drupal\move_request_score\Models\NotificationType;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_request_score\Models\MoveRequestScore as ScoreModel;
use Drupal\move_request_score\Models\MoveRequestScoreTotal as ScoreTotalModel;

/**
 * Class MoveRequestScore. Business logic layer.
 *
 * @package Drupal\move_request_score\Services
 */
class MoveRequestScore {

  static $scoreFiledName = 'field_total_score';
  /**
   * MoveRequest model instance.
   *
   * @var MoveRequest
   */
  public $model;

  /**
   * MoveRequestScore constructor.
   *
   * @param MoveRequest $model
   *   MoveRequest model instance.
   */
  public function __construct(MoveRequest $model) {
    $this->model = $model;
  }

  /**
   * Filter requests by score.
   *
   * @param $query
   *   DB object.
   * @param array $values
   *   Values "from number", "to number".
   */
  public static function filterRequestByScore(&$query, array $values) : void {
    $query->condition(MoveRequestScore::$scoreFiledName, array_values($values), 'BETWEEN');
  }

  /**
   * Adds Notification score to MoveRequest total_score field.
   *
   * @param NotificationType $type
   *   NotificationType model instance.
   */
  public function processNotificationType(NotificationType $type): void {
    if ($notification_type_score = $type->getScore()) {
      $data = $type->data;
      $data['score'] = $notification_type_score;
      $data['nid'] = $this->model->nid;
      $this->addScore($data);
      $this->addTotalScore($data);
      $this->logRequestScoreUpdating($notification_type_score, $type->getTitle());
      $this->addScoreToRequest($notification_type_score);
    }
  }

  /**
   * Log request score updating action.
   *
   * @param int $score
   *   Added score points.
   * @param string $action
   *   Action definition.
   */
  private function logRequestScoreUpdating($score, $action): void {
    (new Log($this->model->getKey(), EntityTypes::MOVEREQUEST))->create(
      Log::prepare_log_data('Request score updated', "{$score} points were added for \"{$action}\" action")
    );
  }

  /**
   * Insert score.
   *
   * @param array $data
   *    Data for save.
   *
   * @return int
   *   1 if added
   */
  public function addScore(array $data) {
    return (new ScoreModel($data))->insertState();
  }

  /**
   * Add/update total score.
   *
   * @param array $data
   *   Data for save.
   *
   * @return int
   *   1 if added or update.
   */
  public function addTotalScore(array $data) {
    return (new ScoreTotalModel($data))->saveState();
  }

  public function insertTotalScore(array $data) {
    return (new ScoreTotalModel($data))->updateTotalScore();
  }

  /**
   * Add score to drupal fields.
   *
   * @param $notification_type_score
   * @return array
   */
  public function addScoreToRequest($notification_type_score) {
    return $this->model->addScore($notification_type_score);
  }

  /**
   * Get total score for request.
   *
   * @param int $nid
   *   Id of request.
   *
   * @return int
   *   Score amount.
   */
  static function getRequestTotalScore($nid) : int{
    $total_all_info = ScoreTotalModel::getByNid($nid);
    return !empty($total_all_info) ? $total_all_info['score'] : 0;
  }

}
