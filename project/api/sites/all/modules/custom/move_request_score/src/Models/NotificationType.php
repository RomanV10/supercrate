<?php

namespace Drupal\move_request_score\Models;

use PDO;

/**
 * Class NotificationType. Model.
 *
 * @package Drupal\move_request_score\Models
 */
class NotificationType {

  /**
   * DB table name.
   */
  const TABLE_NAME = 'move_notification_types';

  /**
   * Primary key field name.
   */
  const PRIMARY_KEY_FIELD = 'ntid';

  /**
   * Selectable fields list.
   *
   * @var array
   */
  private static $fields = [
    'ntid',
    'title',
    'score',
  ];

  /**
   * Editable fields list.
   *
   * @var array
   */
  private static $editable = [
    'score',
  ];

  /**
   * NotificationType data.
   *
   * @var array
   */
  public $data;

  /**
   * NotificationType constructor.
   *
   * @param array $data
   *   Processed NotificationType data(identifier, score, title).
   */
  public function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * Retrieves NotificationType by identifier.
   *
   * @param int $id
   *   NotificationType identifier.
   *
   * @return NotificationType
   *   NotificationType model instance.
   */
  public static function retrieve($id): NotificationType {
    return new self(self::processRawData(self::retrieveRawData($id)));
  }

  /**
   * Returns NotificationType identifier.
   *
   * @return int
   *   Identifier int value.
   */
  public function getKey(): int {
    return $this->data[static::PRIMARY_KEY_FIELD];
  }

  /**
   * Returns NotificationType score.
   *
   * @return int
   *   Score points int value.
   */
  public function getScore(): int {
    return $this->data['score'];
  }

  /**
   * Returns NotificationType title(action definition).
   *
   * @return string
   *   Title string value.
   */
  public function getTitle(): string {
    return $this->data['title'];
  }

  /**
   * Updates NotificationType score in state.
   *
   * @param int $score
   *   Score points int value.
   */
  public function setScore($score): void {
    $this->data['score'] = $score;
  }

  /**
   * Updates NotificationType saved data.
   */
  public function saveState(): void {
    $data = array_intersect_key($this->data, array_flip(self::$editable));

    db_merge(self::TABLE_NAME)
      ->condition(self::PRIMARY_KEY_FIELD, $this->getKey())
      ->fields($data)
      ->execute();
  }

  /**
   * Retrieves raw saved NotificationType data.
   *
   * @param int $id
   *   NotificationType identifier.
   *
   * @return array
   *   NotificationType raw data.
   */
  private static function retrieveRawData($id): array {
    $table_alias = 'types';

    $select_result = db_select(self::TABLE_NAME, $table_alias)
      ->condition(self::PRIMARY_KEY_FIELD, $id)
      ->fields($table_alias, self::$fields)
      ->execute();

    return $select_result->fetchAssoc(PDO::FETCH_ASSOC);
  }

  /**
   * Processes raw NotificationType data(cast types).
   *
   * @param array $raw_data
   *   NotificationType raw data.
   *
   * @return array
   *   Processed NotificationType data(identifier, score, title).
   */
  private static function processRawData(array $raw_data): array {
    $raw_data[self::PRIMARY_KEY_FIELD] = (int) $raw_data[self::PRIMARY_KEY_FIELD];
    $raw_data['score'] = (int) $raw_data['score'];

    return $raw_data;
  }

}
