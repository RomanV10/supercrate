<?php

namespace Drupal\move_request_score\Cron\Tasks;

use Drupal\move_services_new\Cron\Tasks\CronInterface;
use Drupal\move_services_new\Services\move_request\MoveRequest;

/**
 * Class SynchronizeScoreTotal.
 *
 * @package Drupal\move_request_score\Cron\Tasks
 */
class SynchronizeScoreTotal implements CronInterface {

  /**
   * Execute task by cron.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public static function execute(): void {
    $query = db_select('move_request_score_total', 'score_total');
    $query->join('field_data_field_total_score', 'field_total_score', 'field_total_score.entity_id = score_total.nid AND score_total.score > field_total_score.field_total_score_value');
    $query->fields('score_total');
    $query->fields('field_total_score', ['field_total_score_value']);
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($result)) {
      foreach ($result as $item) {
        $data_for_update = array(
          'field_total_score' => array(
            'raw' => 'value',
            'value' => $item['score'],
            'data_to_update_in_cache' => array(
              'field_total_score' => $item['score'],
            ),
          ),
        );
        MoveRequest::updateNodeFieldsOnly($item['nid'], $data_for_update, 'move_request', FALSE);
      }
    }
  }

}
