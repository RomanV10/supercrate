<?php

namespace Drupal\move_request_score\Models;

use PDO;

/**
 * Class MoveRequestScore. Model.
 *
 * @package Drupal\move_request_score\Models
 */
class MoveRequestScore {

  /**
   * DB table name.
   */
  const TABLE_NAME = 'move_request_score';

  /**
   * Selectable fields list.
   *
   * @var array
   */
  private static $fields = [
    'nid',
    'score',
    'ntid',
    'created',
  ];

  /**
   * Editable fields list.
   *
   * @var array
   */
  private static $fillable = [
    'nid',
    'score',
    'ntid',
    'created',
  ];

  private $data = [];

  /**
   * NotificationType constructor.
   *
   * @param array $data
   *   Processed NotificationType data(identifier, score, title).
   */
  public function __construct(array $data) {
    $this->data = $data;
    $this->data['created'] = time();
  }

  /**
   * Retrieves NotificationType by identifier.
   *
   * @param int $id
   *   NotificationType identifier.
   *
   * @return NotificationType
   *   NotificationType model instance.
   */
  public static function retrieve($id): NotificationType {
    return new self(self::processRawData(self::retrieveRawData($id)));
  }

  /**
   * Returns NotificationType identifier.
   *
   * @return int
   *   Identifier int value.
   */
  public function getKey(): int {
    return $this->data[static::PRIMARY_KEY_FIELD];
  }

  /**
   * Returns NotificationType score.
   *
   * @return int
   *   Score points int value.
   */
  public function getScore(): int {
    return $this->data['score'];
  }

  public function insertState() : int {
    $data = array_intersect_key($this->data, array_flip(self::$fillable));
    return db_insert(self::TABLE_NAME)
      ->fields($data)
      ->execute();
  }

  public function prepareDataForSave($data) {

  }

}