<?php

namespace Drupal\move_request_score\Services;

use Drupal\move_request_score\Models\NotificationType;

/**
 * Class NotificationTypeScore. Business logic layer.
 *
 * @package Drupal\move_request_score\Services
 */
class NotificationTypeScore {

  /**
   * NotificationType model instance.
   *
   * @var NotificationType
   */
  private $model;

  /**
   * NotificationTypeScore constructor.
   *
   * @param NotificationType $model
   *   NotificationType model instance.
   */
  public function __construct(NotificationType $model) {
    $this->model = $model;
  }

  /**
   * Returns NotificationType score points.
   *
   * @return int
   *   Score points int value.
   */
  public function getScore(): int {
    return $this->model->getScore();
  }

  /**
   * Updates NotificationType score points.
   *
   * @param int $score
   *   Score points int value.
   */
  public function updateScore($score): void {
    $this->model->setScore($score);

    $this->model->saveState();
  }

}
