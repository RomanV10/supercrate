<?php

namespace Drupal\move_request_score\System;

use Drupal\move_request_score\Models\NotificationType;
use Drupal\move_request_score\Services\MoveRequestScore;
use Drupal\move_request_score\Services\NotificationTypeScore;
use Drupal\move_services_new\Services\move_request\MoveRequest;

/**
 * Class Service.
 *
 * @package Drupal\move_request_score\System
 */
class Service {

  /**
   * @return array
   */
  public function getResources(): array {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();

    return $resources;
  }

  /**
   * @return array
   */
  private static function definition(): array {
    // Unused resources.
    array(
      'request_score_groups' => array(
        'operations' => array(
          'index' => array(
            'callback' => 'Drupal\move_request_score\System\Service::allRequestScoreGroups',
            'file' => array(
              'type' => 'php',
              'module' => 'move_request_score',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );

    return array(
      'notification_types_score' => array(
        'operations' => array(
          'retrieve' => array(
            'callback' => 'Drupal\move_request_score\System\Service::retrieveNotificationTypeScore',
            'file' => array(
              'type' => 'php',
              'module' => 'move_request_score',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'ntid',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_request_score\System\Service::updateNotificationTypeScore',
            'file' => array(
              'type' => 'php',
              'module' => 'move_request_score',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'ntid',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'score',
                'type' => 'int',
                'source' => array('data' => 'score'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'add_score_to_request' => array(
            'help' => 'Add score points to request',
            'callback' => 'Drupal\move_request_score\System\Service::addScoreToRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_request_score',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'score',
                'type' => 'int',
                'source' => array('data' => 'score'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('add score'),
          ),
        ),
      ),
    );
  }

  /**
   * Retrieves notification type score points.
   *
   * @param int $ntid
   *   Notification type identifier.
   *
   * @return array
   *   Associative array with notification type score points.
   */
  public static function retrieveNotificationTypeScore($ntid): array {
    return array('score' => NotificationType::retrieve($ntid)->getScore());
  }

  /**
   * Updates notification type score points.
   *
   * @param int $ntid
   *   Notification type identifier.
   * @param int $score
   *   New notification score points.
   */
  public static function updateNotificationTypeScore($ntid, $score): void {
    (new NotificationTypeScore(NotificationType::retrieve($ntid)))->updateScore($score);
  }

  /**
   * Add scoring to request directly.
   *
   * @param int $nid
   *   Request id.
   * @param int $score
   *   Number of the score.
   *
   * @return array
   *   Array with name of the fields what was update.
   *   1 if update, 0 if not.
   */
  public static function addScoreToRequest(int $nid, int $score) : array {
    $request_inst = new MoveRequest($nid);
    $request_score = new MoveRequestScore($request_inst);
    $data['score'] = $score;
    $data['nid'] = $nid;
    $data['ntid'] = 0;
    $request_score->addScore($data);
    $request_score->insertTotalScore($data);
    return $request_inst->updateRequestScore($score);
  }

}
