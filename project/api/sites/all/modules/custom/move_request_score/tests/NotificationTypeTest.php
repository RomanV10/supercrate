<?php

use Drupal\move_request_score\Models\NotificationType;
use Drupal\move_services_new\Util\enum\NotificationTypes;
use PHPUnit\Framework\TestCase;

class NotificationTypeTest extends TestCase {

  public function testRetrieving() {
    $type = NotificationType::retrieve($this->getRandomNotificationTypeId());
    $this->assertInstanceOf(NotificationType::class, $type);

    return $type;
  }

  public function testSettingScore() {
    $notification_type = new NotificationType($this->retrieveTypeData());

    $notification_type->setScore($score = random_int(1, 100));
    $this->assertEquals($score, $notification_type->getScore());
  }

  /**
   * @depends testRetrieving
   *
   * @param NotificationType $type
   *   NotificationType model instance.
   */
  public function testSavingScore(NotificationType $type) {
    $previous_score = $type->getScore();

    $type->setScore($current_score = random_int(1, 100));
    $type->saveState();

    $type = NotificationType::retrieve($type->getKey());
    $this->assertEquals($current_score, $type->getScore());

    $type->setScore($current_score = $previous_score);
    $type->saveState();

    $type = NotificationType::retrieve($type->getKey());
    $this->assertEquals($current_score, $type->getScore());
  }

  private function retrieveTypeData(): array {
    return drupal_json_decode(file_get_contents(__DIR__ . '/data/notification_type.json'));
  }

  private function getRandomNotificationTypeId(): int {
    return array_rand(array_flip(NotificationTypes::getConstants()));
  }

}
