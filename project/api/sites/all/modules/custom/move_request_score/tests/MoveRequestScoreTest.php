<?php

use Drupal\move_request_score\Models\NotificationType;
use Drupal\move_request_score\Services\MoveRequestScore;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\NotificationTypes;
use PHPUnit\Framework\TestCase;

/**
 * Class MoveRequestScoreTest.
 */
class MoveRequestScoreTest extends TestCase {

  public function testCreateUser(): int {
    $user = drupal_json_decode(file_get_contents(dirname(__FILE__) . '/data/user.json'));

    $uid = (new Clients)->create($user)->uid;

    $this->assertNotFalse($uid);

    return $uid;
  }

  /**
   * @depends testCreateUser
   *
   * @param int $uid
   *   User identifier.
   *
   * @return MoveRequest
   *   MoveRequest instance.
   */
  public function testCreateRequest($uid): MoveRequest {
    global $user;

    $user = user_load($uid);

    $request = drupal_json_decode(file_get_contents(dirname(__FILE__) . '/data/request.json'));
    $encoded_extra = file_get_contents(dirname(__FILE__) . '/data/extra.json');

    $request['all_data']['invoice']['extraServices'] = $encoded_extra;
    $request['data']['uid'] = $uid;

    $nid = (new MoveRequest)->create($request, 1)['nid'];

    $this->assertGreaterThanOrEqual(1, $nid);

    return new MoveRequest($nid);
  }

  /**
   * @depends testCreateRequest
   *
   * @param MoveRequest $move_request
   *   MoveRequest instance.
   */
  public function testAddFirstNotificationTypeScoreToRequest(MoveRequest $move_request) {
    $this->assertEquals(0, $move_request->getScore());

    $notification_type = NotificationType::retrieve($this->getRandomNotificationTypeId());

    (new MoveRequestScore($move_request))->processNotificationType($notification_type);

    $this->assertEquals($notification_type->getScore(), $move_request->getScore());
  }

  /**
   * @depends testCreateRequest
   *
   * @param MoveRequest $move_request
   *   MoveRequest instance.
   *
   * @return MoveRequest
   *   MoveRequest instance with updated total score value.
   */
  public function testAddingMoveRequestTotalScore(MoveRequest $move_request): MoveRequest {
    $previous_score_points = $move_request->getScore();

    $move_request->addScore($added_score_points = random_int(1, 100));

    $current_score_points = $previous_score_points + $added_score_points;

    $this->assertEquals($current_score_points, $move_request->getScore());

    return $move_request;
  }

  /**
   * @depends testAddingMoveRequestTotalScore
   *
   * @param MoveRequest $move_request
   *   MoveRequest instance with updated total score value.
   */
  public function testSavingMoveRequestTotalScore(MoveRequest $move_request) {
    $this->assertEquals($move_request->getScore(), (new MoveRequest($move_request->getKey()))->getScore());
  }

  /**
   * @depends testCreateRequest
   *
   * @param MoveRequest $move_request
   *   MoveRequest instance.
   */
  public function testDeleteRequest(MoveRequest $move_request) {
    $nid = $move_request->getKey();

    $move_request->delete();

    $this->assertFalse(node_load($nid));
  }

  /**
   * @depends testCreateUser
   */
  public function testDeleteUser($uid) {
    (new Clients($uid))->delete();

    $this->assertFalse(user_load($uid));
  }

  private function getRandomNotificationTypeId(): int {
    return array_rand(array_flip(NotificationTypes::getConstants()));
  }

}
