<?php

use Drupal\move_request_score\Models\NotificationType;
use Drupal\move_request_score\Services\NotificationTypeScore;
use Drupal\move_services_new\Util\enum\NotificationTypes;
use PHPUnit\Framework\TestCase;

class NotificationTypeScoreTest extends TestCase {

  public function testRetrievingScore() {
    $notification_type = NotificationType::retrieve($this->getRandomNotificationTypeId());

    $this->assertInternalType('int', (new NotificationTypeScore($notification_type))->getScore());
  }

  public function testUpdatingScore() {
    $notification_type = NotificationType::retrieve($this->getRandomNotificationTypeId());
    $notification_type_score = new NotificationTypeScore($notification_type);
    $previous_score = $notification_type_score->getScore();

    $notification_type_score->updateScore($current_score = random_int(1, 100));
    $this->assertEquals($current_score, $notification_type_score->getScore());

    $notification_type_score->updateScore($current_score = $previous_score);
    $this->assertEquals($current_score, $notification_type_score->getScore());
  }

  private function getRandomNotificationTypeId(): int {
    return array_rand(array_flip(NotificationTypes::getConstants()));
  }

}
