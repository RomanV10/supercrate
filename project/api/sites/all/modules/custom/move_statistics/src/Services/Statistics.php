<?php

namespace Drupal\move_statistics\Services;

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_profit_loses\Services\ProfitLoses;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;
use Drupal\move_services_new\Util\enum\Roles;
use Drupal\move_services_new\Util\enum\TypeStatistics;
use PhpImap\Exception;

/**
 * Class Statistics.
 *
 * @package Drupal\move_statistics\Services
 */
class Statistics extends BaseService {
  public $timeZone = 'UTC';
  protected $fieldApprove = array();
  private $id = NULL;

  /**
   * Statistics constructor.
   *
   * @param mixed $id
   *   Is not used anywhere.
   */
  public function __construct($id = NULL) {
    // TODO make it more dynamically.
    // Fill field_approve by values from 1 to 16.
    $this->fieldApprove = range(1, 16);
    $this->id = $id;
    $this->timeZone = Extra::getTimezone();
    date_default_timezone_set($this->timeZone);
  }

  /**
   * Universal statistics by constants.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   * @param int $type
   *   Type statistics.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getUniversalStatisticsByConst(int $date_from, int $date_to, int $type) {
    $result = db_select('move_statistics', 'ms')
      ->fields('ms')
      ->condition('ms.type', array($type))
      ->condition('ms.created', array($date_from, $date_to), 'BETWEEN')
      ->countQuery()
      ->execute()
      ->fetchField();

    return $result;
  }

  /**
   * Get request field statistics.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   * @param string $field_name
   *   Field name.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getRequestFieldStatistics(int $date_from, int $date_to, string $field_name) {
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->condition('sadmr.created', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('sadmr', array($field_name));
    $query->groupBy($field_name);
    $result = $query->execute()->fetchAllKeyed();

    return $result;
  }

  /**
   * Return count published node from parser for time interval.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return int
   *   Source parser request count.
   */
  public static function getRequestSourceParser(int $date_from, int $date_to) {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('created', array($date_to, $date_from), 'BETWEEN')
      ->fieldCondition('field_from_parser', 'value', 1, '=');

    $result = $query->count()->execute();

    return $result;
  }

  /**
   * Return count published node from people for time interval.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return int
   *   Count source people.
   */
  public static function getRequestSourcePeople(int $date_from, int $date_to) {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('created', array($date_from, $date_to), 'BETWEEN')
      ->fieldCondition('field_from_parser', 'value', 0, '=');

    $result = $query->count()->execute();

    return $result;
  }

  /**
   * Cancelled request.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Count request.
   */
  public static function getCancelledRequest(int $date_from, int $date_to) {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyCondition('created', array($date_from, $date_to), 'BETWEEN')
      ->fieldCondition('field_approve', 'value', 5, '=');

    $result = $query->count()->execute();

    return $result;
  }

  /**
   * Get statistics confirmed requests for time interval by count movers.
   *
   * @param int $date_from
   *   Format Unix time.
   * @param int $date_to
   *   Format Unix time.
   *
   * @return array
   *   Count movers = count request.
   */
  public static function getCountRequestMovers(int $date_from, int $date_to) {
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->condition('sadmr.field_approve', 3);
    $query->condition('sadmr.created', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('sadmr', array('field_movers_count'));
    $query->groupBy('sadmr.field_movers_count');
    $result = $query->execute()->fetchAllKeyed();

    return $result;
  }

  /**
   * Count assign booked move date request.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   *
   * @return array
   *   To == assigned_booked.
   */
  public static function getCountAssignedBookedMoveDate($date_from, $date_to) {
    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->innerJoin('move_request_sales_person', 'mrsp', 'mrsp.nid=sadmr.nid');
    $query->condition('sadmr.field_approve', 3);
    $query->condition('sadmr.field_date_confirmed', array($date_from, $date_to), 'BETWEEN');
    $query->condition('sadmr.field_date', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('mrsp', array('uid'));
    $query->groupBy('mrsp.uid');
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $uid => $value) {
      $load_user = entity_metadata_wrapper('user', $uid);
      $username = "{$load_user->field_user_first_name->value()} {$load_user->field_user_last_name->value()}";
      $result[$uid] = array(
        "name" => $username,
        "to" => $value,
      );
    }

    return $result;
  }

  /**
   * Get receipt card.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getReceiptCreditCard(int $date_from, int $date_to) {
    $result = db_select('moveadmin_receipt_search', 'mrs')
      ->fields('mrs')
      ->condition('mrs.created', array($date_from, $date_to), 'BETWEEN')
      ->countQuery()
      ->execute()
      ->fetchField();

    return $result;
  }

  /**
   * Count confirm booked date.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function countConfirmBookedDate(int $date_from, int $date_to) {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldOrderBy('field_date_confirmed', 'value', 'DESC')
      ->fieldCondition('field_approve', 'value', 3, '=')
      ->fieldCondition('field_date_confirmed', 'value', array($date_from, $date_to), 'BETWEEN');
    $result = $query->count()->execute();

    return $result;
  }

  /**
   * Count reservation received.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function countReservationReceived(int $date_from, int $date_to) {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldOrderBy('field_date_confirmed', 'value', 'DESC')
      ->fieldCondition('field_approve', 'value', 3, '=')
      ->fieldCondition('field_reservation_received', 'value', 1, '=')
      ->fieldCondition('field_date_confirmed', 'value', array($date_from, $date_to), 'BETWEEN');
    $result = $query->count()->execute();

    return $result;
  }

  /**
   * Statistic for sales by interval day.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function statisticsForSalesByIntervalDay(string $date_from, string $date_to, $sales_id = NULL) {
    $result = array();
    $interval = static::getDays($date_from, $date_to);
    foreach ($interval as $day => $value) {
      $result[$value]['sales_by_assigned'] = static::assignCountManagersAndBooked($value, $value, $sales_id);
      $result[$value]['sales_payroll'] = static::getSalesPayroll($value, $value, $sales_id);
    }

    return $result;
  }

  /**
   * Helper method get days.
   *
   * @param string $start_date
   *   Date from.
   * @param string $end_date
   *   Date to.
   *
   * @return array
   *   Days.
   */
  public static function getDays(string $start_date, string $end_date) {
    $days = array();
    $timezone = Extra::getTimezone();
    $date_end = new \DateTime($end_date, new \DateTimeZone($timezone));
    $date_end->modify('+1 day');
    $start = new \DateTime($start_date, new \DateTimeZone($timezone));
    $interval = \DateInterval::createFromDateString('1 day');
    $period = new \DatePeriod($start, $interval, $date_end);
    /* @var \DateTime $dt */
    foreach ($period as $dt) {
      $days[] = $dt->format('Y-m-d');
    }

    return $days;
  }

  /**
   * Count assign and booked request for sales.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function assignCountManagersAndBooked(string $date_from, string $date_to, $sales_id = NULL) {
    $assign = static::getCountAssigned($date_from, $date_to, $sales_id);
    $assign_booked = static::getCountAssignedBooked($date_from, $date_to, $sales_id);
    $foreach_assign = !empty($assign) ? $assign : $assign_booked;

    return static::buildResultWithConversion($foreach_assign, $assign_booked);
  }

  /**
   * Count assign request for sales.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   From == assigned.
   */
  public static function getCountAssigned(string $date_from, string $date_to, $sales_id = NULL) {
    $timezone = Extra::getTimezone();
    $date_from = Extra::getDateFrom($date_from, $timezone);
    $date_to = Extra::getDateTo($date_to, $timezone);
    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->innerJoin('move_request_sales_person', 'mrsp', 'mrsp.nid=sadmr.nid');
    $query->condition('mrsp.created', array($date_from, $date_to), 'BETWEEN');
    $query->distinct();
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('mrsp', array('uid'));
    if (isset($sales_id)) {
      $query->condition('mrsp.uid', array($sales_id));
    }
    $query->groupBy('mrsp.uid');
    $result_query = $query->execute()->fetchAllKeyed();

    $sales_assign_second = static::getCountSalesAssignedSecond($date_from, $date_to, $sales_id);

    foreach ($result_query as $uid => $value) {
      $load_user = entity_metadata_wrapper('user', $uid);
      $username = "{$load_user->field_user_first_name->value()} {$load_user->field_user_last_name->value()}";
      $result[$uid] = array(
        "name" => $username,
        "from" => $value,
        "main" => $value - $sales_assign_second,
        "second" => $sales_assign_second,
      );
    }

    return $result;
  }

  /**
   * Get count requests where sales assigned as second manager.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param int|null $sales_id
   *   Manager id.
   *
   * @return int
   *   Count requests where manager assign as second.
   */
  private static function getCountSalesAssignedSecond(string $date_from, string $date_to, $sales_id = NULL) {
    if (!empty($sales_id)) {
      $query = db_select('search_api_db_move_request', 'sadmr');
      $query->innerJoin('move_request_sales_person', 'mrsp', 'mrsp.nid=sadmr.nid');
      $query->condition('mrsp.created', array($date_from, $date_to), 'BETWEEN');
      $query->fields('mrsp', array('uid'));
      $query->condition('mrsp.uid', array($sales_id));
      $query->condition('mrsp.main', 0);
      $result = $query->execute()->rowCount();
    }

    return $result ?? 0;
  }

  /**
   * Count assign booked request for sales.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   To == assigned_booked.
   */
  public static function getCountAssignedBooked(string $date_from, string $date_to, $sales_id = NULL) {
    $timezone = Extra::getTimezone();
    $date_from = Extra::getDateFrom($date_from, $timezone);
    $date_to = Extra::getDateTo($date_to, $timezone);
    $result = array();

    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->innerJoin('move_request_sales_person', 'mrsp', 'mrsp.nid=sadmr.nid');
    $query->condition('sadmr.field_approve', 3);
    $query->condition('sadmr.field_date_confirmed', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('mrsp', array('uid'));
    if (isset($sales_id)) {
      $query->condition('mrsp.uid', array($sales_id));
    }
    $query->groupBy('mrsp.uid');
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $uid => $value) {
      $load_user = entity_metadata_wrapper('user', $uid);
      $username = "{$load_user->field_user_first_name->value()} {$load_user->field_user_last_name->value()}";
      $result[$uid] = array(
        "name" => $username,
        "to" => $value,
      );
    }

    return $result;
  }

  /**
   * Get statistics 'sales' payroll for time interval.
   *
   * @param string $date_from_string
   *   Date from.
   * @param string $date_to_string
   *   Date to.
   * @param mixed $uid
   *   User id.
   *
   * @return array
   *   Workers with tips, total, jobs_count and etc.
   */
  public static function getSalesPayroll(string $date_from_string, string $date_to_string, $uid = NULL) {
    $payroll_instance = new Payroll();
    $result = $payroll_instance->cacheByRolePayroll($date_from_string, $date_to_string, Roles::SALES);

    if (isset($uid)) {
      $result = isset($result[$uid]) ? $result[$uid] : array();
    }
    else {
      foreach ($result as $uid => &$user) {
        unset($user['user_info']);
        $load_user = entity_metadata_wrapper('user', $uid);
        $user['username'] = "{$load_user->field_user_first_name->value()} {$load_user->field_user_last_name->value()}";
      }
    }

    return $result;
  }

  /**
   * Id setter, not used.
   *
   * @param int $id
   *   Is not used anywhere.
   */
  public function setId(int $id) {
    $this->id = $id;
  }

  /**
   * Get statistic timeZone.
   *
   * @return string
   *   Time zone string.
   */
  public function getTimeZone() : string {
    return $this->timeZone;
  }

  /**
   * Set timezone.
   *
   * @param string $timeZone
   *   Set timezone for date functions.
   *
   */
  public function setTimeZone(string $timeZone) {
    $this->timeZone = $timeZone;
    return $this;
  }

  /**
   * Insert statistics data in db.
   *
   * @param mixed $data
   *   Data.
   *
   * @return \DatabaseStatementInterface|int
   *   Id.
   *
   * @throws \Exception
   */
  public function create($data = array()) {
    try {
      $id = db_insert('move_statistics')
        ->fields(array(
          'type' => $data['type'] ?? 0,
          'value' => !empty($data['value']) ? serialize($data['value']) : '',
          'created' => time(),
          'form' => $data['form'] ?? NULL,
          'user_hash' => $this->calcUniqueUserHash() ?? '',
          'browser_type' => $data['browser_type'] ?? NULL,
          'browser_name' => $data['browser_name']['name'] ?? $data['browser_name'] ?? NULL,
          'page_url' => $data['page_url'] ?? NULL,
        ))
        ->execute();

      (new DelayLaunch())->createTask(TaskType::UPDATE_STATISTICS_FOR_DAY, ['date' => date('Y-m-d')]);

      return $id;
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("Move Statistics Create", $error_text, array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Calculate user hash.
   *
   * @return string
   *   Hash.
   */
  private function calcUniqueUserHash() {
    $nginx_uid = isset($_COOKIE['UID']) ? $_COOKIE['UID'] : $this->createUniqueUid();
    $language = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : 'en-US,en';
    $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    return hash('sha256', $nginx_uid . $language . $user_agent);
  }

  /**
   * Create unique uid.
   *
   * @return string
   *   Hex.
   */
  public function createUniqueUid() : string {
    $bytes = random_bytes(32);
    $hex = bin2hex($bytes);
    // 365 Days.
    $expire = time() + 86400 * 365;
    setcookie("UID", $hex, $expire, '/');
    return $hex;
  }

  /**
   * Retrieve.
   */
  public function retrieve() {}

  /**
   * Update.
   *
   * @param mixed $data
   *   Data.
   *
   * @return mixed
   *   Data.
   */
  public function update($data = array()) {
    return NULL;
  }

  /**
   * Delete.
   */
  public function delete() {}

  /**
   * Index.
   */
  public function index() {}

  /**
   * Get statistics for month by field approve.
   *
   * @param string $date_month
   *   Month for which statistics are collected.
   *
   * @return mixed
   *   Statistics for month.
   */
  public function getStatForMonthByApprove(string $date_month) {
    date_default_timezone_set($this->timeZone);
    $date_to = mktime(23, 59, 59, date('m', strtotime($date_month)), date('t', strtotime($date_month)), date('Y', strtotime($date_month)));
    $date_from = $this->dateToDayStart($date_month);
    return $result = $this->getCountOfRequestsForPeriodByApprove($date_from, $date_to);
  }

  /**
   * Get timestamp for UTC.
   *
   * @param string $date
   *   String date.
   *
   * @return int
   *   Unix timestamp.
   */
  private function dateToDayStart($date) {
    try {
      $new_date = new \DateTime((string) $date, new \DateTimeZone($this->timeZone));
      return $new_date->getTimestamp();
    }
    catch (\Throwable $e) {
      watchdog('Statistics', $e->getMessage(), $e->getCode(), WATCHDOG_ERROR);
    }
    return NULL;
  }

  /**
   * Get count of requests for period by field approve.
   *
   * @param string $date_from
   *   Start date.
   * @param string $date_to
   *   End date.
   *
   * @return mixed
   *   Count of requests for period by field approve.
   */
  private function getCountOfRequestsForPeriodByApprove($date_from, $date_to) {
    $result['count_by_approve'] = array();
    foreach ($this->fieldApprove as $value) {
      $query = $this->getQueryForPeriod($date_from, $date_to, $value);
      $this->getQueryForField($query, 'field_approve', $value);
      $this->getCountOfQuery($query);
      $result['count_by_approve'][$value] = $query->execute();
    }
    $count_query = $this->getQueryForPeriod($date_from, $date_to);
    $this->getCountOfQuery($count_query);

    $result['count_of_all'] = $count_query->execute();
    $result['confirmed'] = $this->getCountOfConfirmRequests($date_from, $date_to, RequestStatusTypes::CONFIRMED);
    $result['not_confirmed'] = $this->getCountOfRequests($date_from, $date_to, RequestStatusTypes::NOT_CONFIRMED);
    $start = (new \DateTime('now', new \DateTimeZone($this->timeZone)))->setTimestamp($date_from);
    $end = (new \DateTime('now', new \DateTimeZone($this->timeZone)))->setTimestamp($date_to);
    $result['unique_user'] = static::getCountUniqueUsers($start->format('Y-m-d'), $end->format('Y-m-d'), $type = TypeStatistics::USER_VISIT);

    return $result;
  }

  /**
   * Get query object for period.
   *
   * @param string $date_from
   *   Start date.
   * @param string $date_to
   *   End date.
   *
   * @return \EntityFieldQuery
   *   Query object for period.
   */
  private function getQueryForPeriod(string $date_from, string $date_to, $status = 0) {
    $query = $this->getEntityQueryForMoveRequest();
    $this->getMoveRequestForPeriod($query, $date_from, $date_to, $status);
    return $query;
  }

  /**
   * Create query object for requests.
   *
   * @return \EntityFieldQuery
   *   Query object for requests.
   */
  private function getEntityQueryForMoveRequest() {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');
    $query->entityCondition('bundle', 'move_request');
    return $query;
  }

  /**
   * Add to query object property condition for period.
   *
   * @param \EntityFieldQuery $query
   *   Query object to add.
   * @param string $date_from
   *   Start date.
   * @param string $date_to
   *   End date.
   */
  private function getMoveRequestForPeriod(\EntityFieldQuery &$query, $date_from, $date_to, int $status = 0) {
    if ($status == 3) {
      $query->fieldCondition('field_date_confirmed', 'value', array($date_from, $date_to), 'BETWEEN');
    }
    else {
      $query->propertyCondition('created', array($date_from, $date_to), 'BETWEEN');
    }
  }

  /**
   * Add field condition to query.
   *
   * @param \EntityFieldQuery $query
   *   Query object to add.
   * @param string $field_name
   *   Field name for field condition.
   * @param int $value
   *   Field value for field condition.
   */
  private function getQueryForField(\EntityFieldQuery &$query, $field_name, $value) {
    $query->fieldCondition($field_name, 'value', $value);
  }

  /**
   * Get count of records by entity query.
   *
   * @param \EntityFieldQuery $query
   *   Query object for count.
   */
  private function getCountOfQuery(\EntityFieldQuery &$query) {
    $query->count();
  }

  /**
   * Get count of confirm requests.
   *
   * @param int $date_from
   *   Start date.
   * @param int $date_to
   *   End date.
   * @param string $type_value
   *   Value of field type.
   * @param bool $period
   *   Flag need period.
   *
   * @return mixed
   *   Count of requests with this type.
   */
  private function getCountOfConfirmRequests(int $date_from, int $date_to, string $type_value, bool $period = TRUE) {
    $select = db_select('search_api_db_move_request', 'sadmr');
    if ($period) {
      $select->condition('sadmr.field_date_confirmed', array($date_from, $date_to), 'BETWEEN');
    }
    $count = $select->condition('sadmr.field_approve', $type_value)
      ->countQuery()
      ->execute()
      ->fetchField();

    return $count;
  }

  /**
   * Get count of requests by field type.
   *
   * @param int $date_from
   *   Start date.
   * @param int $date_to
   *   End date.
   * @param string $type_value
   *   Value of field type.
   * @param bool $period
   *   Flag need period.
   *
   * @return mixed
   *   Count of requests with this type.
   */
  private function getCountOfRequests(int $date_from, int $date_to, string $type_value, bool $period = TRUE) {
    $select = db_select('search_api_db_move_request', 'sadmr');
    if ($period) {
      $select->condition('sadmr.created', array($date_from, $date_to), 'BETWEEN');
    }
    $count = $select->condition('sadmr.field_approve', $type_value)
      ->countQuery()
      ->execute()
      ->fetchField();

    return $count;
  }

  /**
   * Count unique users.
   *
   * @param string $date_from_string
   *   Date from.
   * @param string $date_to_string
   *   Date to.
   * @param int $type
   *   Type.
   *
   * @return mixed
   *   Number count unique users.
   */
  public static function getCountUniqueUsers(string $date_from_string, string $date_to_string, int $type) {
    $timezone = Extra::getTimezone();

    $date_from = Extra::getDateFrom($date_from_string, $timezone);
    $date_to = Extra::getDateTo($date_to_string, $timezone);

    $query = db_select('move_statistics', 'ms');
    $query->condition('ms.type', array($type));
    $query->condition('ms.created', array($date_from, $date_to), 'BETWEEN');
    $query->fields('ms', array('user_hash'));
    $query->groupBy('ms.user_hash');
    $result = $query->countQuery()->execute()->fetchField();

    return $result;
  }

  /**
   * Get statistics for month.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   *
   * @return array
   *   Statistics.
   */
  public function getStatForMonths(string $date_from, string $date_to) {
    $result = array();
    $months = array();
    $start = new \DateTime($date_from, new \DateTimeZone($this->timeZone));
    $end = new \DateTime($date_to, new \DateTimeZone($this->timeZone));
    $diff = $end->diff($start);
    $months[] = array('start' => $date_from, 'end' => $start->modify('last day of this month')->format('Y-m-d'));
    $months_number = !empty($diff->y) ? $diff->y * 12 + $diff->m : $diff->m;
    for ($i = 1; $i <= $months_number; $i++) {
      // Last month.
      if ($i == $months_number) {
        $temp_start = $start->modify('+1 day');
        $months[] = array('start' => $temp_start->format('Y-m-d'), 'end' => $date_to);
      }
      else {
        $temp_start = clone $start->modify('+1 day');
        $temp_end = $start->modify('last day of this month');
        $months[] = array('start' => $temp_start->format('Y-m-d'), 'end' => $temp_end->format('Y-m-d'));
      }
    }

    foreach ($months as $key => $value) {
      $timestamp_from = Extra::getDateFrom($value['start'], $this->timeZone);
      $timestamp_to = Extra::getDateTo($value['end'], $this->timeZone);
      $result[$key] = $this->getCountOfRequestsForPeriodByApprove($timestamp_from, $timestamp_to);
      $temp = new \DateTime($value['start'], new \DateTimeZone($this->timeZone));
      $result[$key]['date'] = $temp->format('Y-m');
      unset($temp);
    }

    return $result;
  }

  /**
   * Get statistics for year by field approve.
   *
   * @param string $date_year
   *   Year for which statistics are collected.
   *
   * @return mixed
   *   Statistics for year.
   */
  public function getStatForYearByApprove(string $date_year) {
    date_default_timezone_set($this->timeZone);
    $date_from = mktime(00, 00, 00, 1, 1, $date_year);
    $date_to = mktime(23, 59, 59, 12, 31, $date_year);
    return $result = $this->getCountOfRequestsForPeriodByApprove($date_from, $date_to);
  }

  /**
   * Retrieve everyday statistic.
   *
   * @param string $date
   *   Date Y-m-d.
   *
   * @return mixed
   *   Statistics.
   */
  public function getTodayStat(string $date) {
    $date_from = Extra::getDateFrom($date, $this->timeZone);
    $date_to = Extra::getDateTo($date, $this->timeZone);
    $requests = $this->getCountOfRequestsForPeriodByApprove($date_from, $date_to);
    return $requests;
  }

  /**
   * Get statistics all.
   *
   * @return array
   *   Statistics.
   */
  public function getStatAll() {
    $result = array();
    $query = $this->getEntityQueryForMoveRequest();
    $this->getCountOfQuery($query);
    $result['all'] = $query->execute();
    $result['confirmed'] = $this->getCountOfRequests('', '', 3, FALSE);
    $result['unique_user_count'] = static::getCountUniqueUsersAllTime();
    return $result;
  }

  /**
   * Get count unique users for all time.
   *
   * @return mixed
   *   Count users.
   */
  public static function getCountUniqueUsersAllTime() {
    $query = db_select('move_statistics', 'ms');
    $query->condition('ms.type', array(0));
    $query->fields('ms', array('user_hash'));
    $query->groupBy('ms.user_hash');
    $result = $query->countQuery()->execute()->fetchField();

    return $result;
  }

  /**
   * Update statistics for days.
   *
   * @param string $date
   *   Date for update.
   */
  public function updateStatisticsForDays(string $date) {
    $date_from = Extra::getDateFrom($date, $this->timeZone);
    $date_to = Extra::getDateTo($date, $this->timeZone);
    $result = $this->getCountOfRequestsForPeriodByApprove($date_from, $date_to);
    $result['unique_user'] = static::getCountUniqueUsers($date, $date, $type = TypeStatistics::USER_VISIT);

    db_merge('move_request_cache_statistics')
      ->key(array('date' => $date))
      ->fields(array(
        'date' => $date,
        'count_of_all' => $result['count_of_all'],
        'confirmed' => $result['confirmed'],
        'unique_user' => $result['unique_user'],
      ))
      ->execute();
  }

  /**
   * Get array of dates from 2016-01-01 to current day for batch.
   *
   * @return array
   *   Array of all dates.
   */
  public function cacheRequestStatisticsPrepareBatch() {
    $operations = array();
    date_default_timezone_set($this->timeZone);
    db_truncate('move_request_cache_statistics')->execute();
    $date = '2016-01-01';
    $curDate = date('Y-m-d');
    while ($date <= $curDate) {
      $operations[] = array('move_statistics_move_request_batch_execute', array($date));
      $date_obj = new \DateTime($date);
      $date_obj = $date_obj->modify('+1 day');
      /* @var \DateTime $date_obj */
      $date = $date_obj->format('Y-m-d');
    }
    return $operations;
  }

  /**
   * Statistics all.
   *
   * @param string $date_from_string
   *   Date from.
   * @param string $date_to_string
   *   Date to.
   *
   * @return array
   *   Statistics.
   */
  public function statisticsAll($date_from_string, $date_to_string) {
    $result = array();
    $date_from = Extra::getDateFrom($date_from_string, $this->timeZone);
    $date_to = Extra::getDateTo($date_to_string, $this->timeZone);

    $result['count_site_visitors'] = static::getVisitorsCount($date_from, $date_to);
    $result['count_move_request_type_booked_all'] = static::getMoveRequestTypeCountAll($date_from_string, $date_to_string);
    $result['count_request_size_of_move_all'] = static::getCountRequestSizeOfMoveCountAndBooked($date_from_string, $date_to_string);
    $result['count_unique_site_visitors'] = static::getCountUniqueUsers($date_from_string, $date_to_string, TypeStatistics::USER_VISIT);
    $result['count_submit_form'] = static::getFormSubmitCount($date_from, $date_to);
    $result['get_statistics_for_days'] = static::getStatisticsForDays($date_from_string, $date_to_string);
    $result['get_stat_for_period'] = $this->getStatForPeriodByApprove($date_from, $date_to);
    $result['get_count_assigned_booked_all'] = static::assignCountManagersAndBooked($date_from_string, $date_to_string);
    $result['get_count_entrance_from'] = static::getEntranceAll($date_from_string, $date_to_string, 'field_type_of_entrance_from');
    $result['get_count_entrance_to'] = static::getEntranceAll($date_from_string, $date_to_string, 'field_type_of_entrance_to_');
    $result['get_sales_payroll'] = static::getSalesPayroll($date_from_string, $date_to_string, NULL);
    $result['statistics_by_browser_type'] = static::getStatisticsByBrowserType($date_from, $date_to);
    $result['statistics_by_browser_name'] = static::getStatisticsByBrowserName($date_from, $date_to);
    $result['stat_user_visit_conf_page'] = static::getStatUserVisitConfPage($date_from, $date_to);
    $result['stat_booked_by'] = static::getStatUserBookedBy($date_from, $date_to);
    $result['statistics_source_all'] = static::getStatisticsSourceBookedAll($date_from_string, $date_to_string);
    $result['stat_user_visit_account'] = static::getStatUserVisitAccount($date_from, $date_to);

    return $result;
  }

  /**
   * Get visitor count.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Visitor.
   */
  public static function getVisitorsCount(int $date_from, int $date_to) {
    $result = db_select('move_statistics', 'ms')
      ->fields('ms')
      ->condition('ms.type', array(0))
      ->condition('ms.created', array($date_from, $date_to), 'BETWEEN')
      ->countQuery()
      ->execute()
      ->fetchField();

    return $result;
  }

  /**
   * Move request type count.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   *
   * @return array
   *   Statistics.
   */
  public static function getMoveRequestTypeCountAll(string $date_from, string $date_to) : array {
    $result_all = array();
    $move_request_count = static::getMoveRequestTypeCount($date_from, $date_to);
    $move_request_count_booked = static::getMoveRequestTypeBookedCount($date_from, $date_to);
    $move_request_types_all = static::getFieldValue('field_move_service_type');

    foreach ($move_request_types_all as $key => $value) {
      if (array_key_exists($key, $move_request_count)) {
        $result_all[$key] = array_merge($move_request_types_all[$key], $move_request_count[$key]);
      }
      else {
        $result_all[$key] = $value;
        $result_all[$key]['from'] = 0;
      }
    }

    return static::buildResultWithConversion($result_all, $move_request_count_booked);
  }

  /**
   * Method return all request group by service type for time interval.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getMoveRequestTypeCount(string $date_from, string $date_to) {
    $timezone = Extra::getTimezone();

    $date_from = Extra::getDateFrom($date_from, $timezone);
    $date_to = Extra::getDateTo($date_to, $timezone);

    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->condition('sadmr.created', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('sadmr', array('field_move_service_type'));
    $query->groupBy('sadmr.field_move_service_type');
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'from' => $value,
      );
    }

    return $result;
  }

  /**
   * Method return all booked request group by service type for time interval.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getMoveRequestTypeBookedCount(string $date_from, string $date_to) {
    $timezone = Extra::getTimezone();

    $date_from = Extra::getDateFrom($date_from, $timezone);
    $date_to = Extra::getDateTo($date_to, $timezone);

    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->condition('sadmr.field_approve', array(3));
    $query->condition('sadmr.field_date_confirmed', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('sadmr', array('field_move_service_type'));
    $query->groupBy('sadmr.field_move_service_type');
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'to' => $value,
      );
    }

    return $result;
  }

  /**
   * Get field value.
   *
   * @param string $field
   *   Field name.
   *
   * @return array
   *   Value.
   */
  public static function getFieldValue(string $field) {
    $result = array();
    $field_info = field_info_field($field);
    $result_field[$field] = list_allowed_values($field_info);

    foreach ($result_field[$field] as $key => $value) {
      $result[$key] = array(
        'name' => trim($value),
      );
    }
    return $result;
  }

  /**
   * Count request size of move and count booked.
   *
   * @param string $date_from_string
   *   Date from.
   * @param string $date_to_string
   *   Date to.
   *
   * @return array
   *   Statistics.
   */
  public static function getCountRequestSizeOfMoveCountAndBooked(string $date_from_string, string $date_to_string) : array {
    $timezone = Extra::getTimezone();
    $date_from = Extra::getDateFrom($date_from_string, $timezone);
    $date_to = Extra::getDateTo($date_to_string, $timezone);
    $result_all = array();
    $size_of_move = static::getCountRequestSizeOfMove($date_from, $date_to);
    $size_of_move_booked = static::getCountRequestSizeOfMoveBooked($date_from, $date_to);
    $size_of_move_types_all = static::getFieldValue('field_size_of_move');

    foreach ($size_of_move_types_all as $key => $value) {
      if (array_key_exists($key, $size_of_move)) {
        $result_all[$key] = array_merge($size_of_move_types_all[$key], $size_of_move[$key]);
      }
      else {
        $result_all[$key] = $value;
        $result_all[$key]['from'] = 0;
        $result_all[$key]['size_of_move'] = 0;
      }
    }

    return static::buildResultWithConversion($result_all, $size_of_move_booked);
  }

  /**
   * Get statistics confirmed requests for time interval by size of move.
   *
   * @param int $date_from
   *   Format Unix time.
   * @param int $date_to
   *   Format Unix time.
   *
   * @return array
   *   Size of move = count request.
   */
  public static function getCountRequestSizeOfMove(int $date_from, int $date_to) {
    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->condition('sadmr.created', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('sadmr', array('field_size_of_move'));
    $query->groupBy('sadmr.field_size_of_move');
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'size_of_move' => $key,
        'from' => $value,
      );
    }

    return $result;
  }

  /**
   * Get statistics confirmed requests for time interval by size booked.
   *
   * @param int $date_from
   *   Format Unix time.
   * @param int $date_to
   *   Format Unix time.
   *
   * @return array
   *   Size of move = count request.
   */
  public static function getCountRequestSizeOfMoveBooked(int $date_from, int $date_to) {
    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->condition('sadmr.field_date_confirmed', array($date_from, $date_to), 'BETWEEN');
    $query->condition('sadmr.field_approve', 3);
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('sadmr', array('field_size_of_move'));
    $query->groupBy('sadmr.field_size_of_move');
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'size_of_move' => $key,
        'to' => $value,
      );
    }

    return $result;
  }

  /**
   * Get form submit count.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Count submit form.
   */
  public static function getFormSubmitCount(int $date_from, int $date_to) {
    $query = db_select('move_statistics', 'ms');
    $query->condition('ms.type', 12);
    $query->condition('ms.created', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('ms', array('form', 'page_url'));
    $query->isNotNull('form');
    $query->groupBy('form');
    $query->groupBy('page_url');
    $result = $query->execute()->fetchAll();

    return $result;
  }

  /**
   * Get statistics for days.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getStatisticsForDays(string $date_from, string $date_to) {
    return db_select('move_request_cache_statistics', 'mrcs')
      ->fields('mrcs', array('date', 'count_of_all', 'confirmed', 'unique_user'))
      ->condition('mrcs.date', array($date_from, $date_to), 'BETWEEN')
      ->execute()
      ->fetchAll();
  }

  /**
   * Get statistics for period by field approve.
   *
   * @param string $date_from
   *   Start date.
   * @param string $date_to
   *   End date.
   *
   * @return mixed
   *   Statistics for period.
   */
  public function getStatForPeriodByApprove($date_from, $date_to) {
    $result = array();
    // Check what date_to is more than date_from.
    if ((int) $date_to > (int) $date_from) {
      $result = $this->getCountOfRequestsForPeriodByApprove($date_from, $date_to);
    }

    return $result;
  }

  /**
   * Get entrance all.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param string $field_name
   *   Field name.
   *
   * @return array
   *   Statistics.
   */
  public static function getEntranceAll(string $date_from, string $date_to, string $field_name) {
    $result_all = array();
    $entrance_count = static::getEntranceCount($date_from, $date_to, $field_name);
    $entrance_all = static::getFieldValue($field_name);

    foreach ($entrance_all as $key => $value) {
      if (array_key_exists($key, $entrance_count)) {
        $result_all[$key] = array_merge($entrance_all[$key], $entrance_count[$key]);
      }
      else {
        $result_all[$key] = $value;
        $result_all[$key]['entrance_count'] = 0;
        $result_all[$key]['entrance_type'] = $key;
      }
    }

    return $result_all;
  }

  /**
   * Get entrance count.
   *
   * @param string $date_from_string
   *   Date from.
   * @param string $date_to_string
   *   Date to.
   * @param string $field_name
   *   Field name.
   *
   * @return array
   *   Statistics.
   */
  public static function getEntranceCount(string $date_from_string, string $date_to_string, string $field_name) {
    $timezone = Extra::getTimezone();

    $date_from = Extra::getDateFrom($date_from_string, $timezone);
    $date_to = Extra::getDateTo($date_to_string, $timezone);
    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->condition('sadmr.created', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('sadmr', array($field_name));
    $query->groupBy($field_name);
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'entrance_type' => $key,
        'entrance_count' => $value,
      );
    }

    return $result;
  }

  /**
   * Status by browser type.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getStatisticsByBrowserType(int $date_from, int $date_to) {
    $query = db_select('move_statistics', 'ms');
    $query->condition('ms.created', array($date_from, $date_to), 'BETWEEN');
    $query->fields('ms', array('browser_type'));
    $query->addExpression('COUNT(*)', 'count');
    $query->isNotNull('ms.browser_type');
    $query->groupBy('ms.browser_type');
    $result = $query->execute()->fetchAllKeyed();

    return $result;
  }

  /**
   * Status by browser name.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getStatisticsByBrowserName(int $date_from, int $date_to) {
    $query = db_select('move_statistics', 'ms');
    $query->condition('ms.created', array($date_from, $date_to), 'BETWEEN');
    $query->fields('ms', array('browser_name'));
    $query->addExpression('COUNT(*)', 'count');
    $query->isNotNull('ms.browser_name');
    $query->groupBy('ms.browser_name');
    $result = $query->execute()->fetchAllKeyed();

    return $result;
  }

  /**
   * Status user visit confirmed page.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getStatUserVisitConfPage(int $date_from, int $date_to) {
    $result = db_select('move_all_data_index', 'madi')
      ->fields('madi')
      ->condition('madi.user_visit_confirmation_page', 1)
      ->condition('madi.first_visit_confirmation_page', array($date_from, $date_to), 'BETWEEN')
      ->countQuery()
      ->execute()
      ->fetchField();

    return $result;
  }

  /**
   * Status user booked.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getStatUserBookedBy(int $date_from, int $date_to) {
    $query = db_select('move_all_data_index', 'madi');
    $query->condition('madi.booked_date', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('madi', array('booked_by'));
    $query->isNotNull('madi.booked_by');
    $query->groupBy('madi.booked_by');
    $result = $query->execute()->fetchAllKeyed();

    return $result;
  }

  /**
   * Get statistics by source booked all.
   *
   * @param string $date_from_string
   *   Date from.
   * @param string $date_to_string
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getStatisticsSourceBookedAll(string $date_from_string, string $date_to_string) : array {
    $timezone = Extra::getTimezone();
    $date_from = Extra::getDateFrom($date_from_string, $timezone);
    $date_to = Extra::getDateTo($date_to_string, $timezone);

    $providers_count = static::getStatisticsSource($date_from, $date_to);
    $providers_count_booked = static::getStatisticsSourceBooked($date_from, $date_to);
    return static::buildResultWithConversion($providers_count, $providers_count_booked);
  }

  /**
   * Build response with commission.
   *
   * @param array $result_with_count
   *   All counts.
   * @param array $result_with_booked_count
   *   Booked counts.
   *
   * @return array
   *   Array all, booked and commission.
   */
  public static function buildResultWithConversion(array $result_with_count, array $result_with_booked_count) : array {
    $result = array();

    $diff = array_diff_key($result_with_booked_count, $result_with_count);
    if (!empty($diff)) {
      foreach ($diff as $key => $val) {
        unset($diff[$key]['to']);
        $diff[$key]['from'] = 0;
      }
      $result_with_count = $result_with_count + $diff;
    }

    foreach ($result_with_count as $key => $value) {
      if (array_key_exists($key, $result_with_booked_count)) {
        $result[$key] = array_merge($result_with_count[$key], $result_with_booked_count[$key]);
        $all_count = !empty($result_with_count[$key]['from']) ? $result_with_count[$key]['from'] : 1;
        $result[$key]['conversion'] = round($result_with_booked_count[$key]['to'] / $all_count * 100, 3);
      }
      else {
        $result[$key] = $value;
        $result[$key]['to'] = 0;
        $result[$key]['conversion'] = 0;
      }
    }

    return $result;
  }

  /**
   * Get statistics by source.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getStatisticsSource(int $date_from, int $date_to) {
    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->condition('sadmr.created', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->distinct();
    $query->fields('sadmr', array('field_parser_provider_name'));
    $query->groupBy('field_parser_provider_name');
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'name' => $key,
        'from' => $value,
      );
    }
    return $result;
  }

  /**
   * Get statistics by source booked.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getStatisticsSourceBooked(int $date_from, int $date_to) {
    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->condition('sadmr.field_approve', array(3));
    $query->condition('sadmr.field_date_confirmed', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('sadmr', array('field_parser_provider_name'));
    $query->groupBy('field_parser_provider_name');
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'name' => $key,
        'to' => $value,
      );
    }
    return $result;
  }

  /**
   * Status user visit account.
   *
   * @param int $date_from
   *   Date from.
   * @param int $date_to
   *   Date to.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getStatUserVisitAccount(int $date_from, int $date_to) {
    $result = db_select('move_all_data_index', 'madi')
      ->fields('madi')
      ->condition('madi.user_visit_account', 1)
      ->condition('madi.first_date_visit_account', array($date_from, $date_to), 'BETWEEN')
      ->countQuery()
      ->execute()
      ->fetchField();

    return $result;
  }

  /**
   * Get entrance all period.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   *
   * @return array
   *   Statistics.
   */
  public function getEntranceAllPeriod(string $date_from, string $date_to) {
    $result = array();
    $result['get_count_entrance_from'] = static::getEntranceAll($date_from, $date_to, 'field_type_of_entrance_from');
    $result['get_count_entrance_to'] = static::getEntranceAll($date_from, $date_to, 'field_type_of_entrance_to_');

    return $result;
  }

  /**
   * Statistics fo 6 month.
   *
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public function getStatisticsForSixMonth($sales_id = NULL) {
    $result = array();
    $months = $this->getSixMonths();

    foreach ($months as $month => $value) {
      $result[$value] = $this->statisticsBySales(
        Extra::getFirstDayMonth($value, 'Y-m-d', $this->timeZone), Extra::getLastDaytMonth($value, 'Y-m-d', $this->timeZone), $sales_id
      );
      unset($result[$value]['sales_by_approve']);
      unset($result[$value]['sales_send_messages']);
      unset($result[$value]['sales_by_service_type']);
      unset($result[$value]['sales_by_source']);
    }

    return $result;
  }

  /**
   * Helper method get 6 months.
   *
   * @return array
   *   Months.
   */
  private function getSixMonths() {
    $months = array();
    $date = new \DateTime();
    $date->setTimezone(new \DateTimeZone($this->timeZone));
    $date->setTimestamp(time());
    $months['first_mounth'] = $date->modify('first day of last month')->format('Y-m');
    $months['second_month'] = $date->modify('first day of last month')->format('Y-m');
    $months['third_mounth'] = $date->modify('first day of last month')->format('Y-m');
    $months['fourth_mounth'] = $date->modify('first day of last month')->format('Y-m');
    $months['fifth_mounth'] = $date->modify('first day of last month')->format('Y-m');
    $months['sixth_mounth'] = $date->modify('first day of last month')->format('Y-m');

    return $months;
  }

  /**
   * Statistics by sales.
   *
   * @param string $date_from_string
   *   Date from.
   * @param string $date_to_string
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function statisticsBySales(string $date_from_string, string $date_to_string, $sales_id = NULL) {
    $result = array();
    // How many request he assigned (today, yesterday , this month , last month)
    // How many Confirmed.
    $result['sales_by_assigned'] = static::assignCountManagersAndBooked($date_from_string, $date_to_string, $sales_id);
    // Commission he make currently, last month, and this month,
    // future from estimated.
    $result['sales_payroll'] = static::getSalesPayroll($date_from_string, $date_to_string, $sales_id);
    // How many request by statuses, expired, cancelled, pending, not-confirmed.
    $result['sales_by_approve'] = static::countSalesAssignedByApprove($date_from_string, $date_to_string, $sales_id);
    // TODO this is hot fix to master. Need to think how to remove it and do in proper way.
    if (!empty($result['sales_by_approve'][3])) {
      $result['sales_by_approve'][3]['from'] = !empty($result['sales_by_assigned'][$sales_id]['to']) ? $result['sales_by_assigned'][$sales_id]['to'] : $result['sales_by_approve'][3]['from'];
    }
    // How many messages he received and send.
    $result['sales_send_messages'] = static::countSalesMessagesSend($date_from_string, $date_to_string, $sales_id);
    // Type of Move.
    $result['sales_by_service_type'] = static::countSalesAssignedByServiceType($date_from_string, $date_to_string, $sales_id);
    // Source.
    $result['sales_by_source'] = static::countSalesBySource($date_from_string, $date_to_string, $sales_id);
    // Income estimate only for owner.
    if (empty($sales_id)) {
      $result['income_estimate'] = (new ProfitLoses(NULL, $date_from_string, $date_to_string))->getEstimatedIncomeForPeriod();
    }

    return $result;
  }

  /**
   * Statistics by sales.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function countSalesAssignedByApprove(string $date_from, string $date_to, $sales_id = NULL) {
    $timezone = Extra::getTimezone();
    $date_from = Extra::getDateFrom($date_from, $timezone);
    $date_to = Extra::getDateTo($date_to, $timezone);
    $result = array();
    $result_all = array();
    $field_approve_all = static::getFieldValue('field_approve');

    $query = db_select('move_request_sales_person', 'mrsp');
    $query->innerJoin('search_api_db_move_request', 'sadmr', 'sadmr.nid=mrsp.nid');
    $query->condition('mrsp.created', array($date_from, $date_to), 'BETWEEN');
    $query->condition('mrsp.uid', array($sales_id));
    $query->addExpression('COUNT(*)', 'count');
    $query->groupBy('sadmr.field_approve');
    $query->fields('sadmr', array('field_approve'));
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'field_approve' => $key,
        'from' => $value,
      );
    }

    foreach ($field_approve_all as $key => $value) {
      if (array_key_exists($key, $result)) {
        $result_all[$key] = array_merge($field_approve_all[$key], $result[$key]);
      }
      else {
        $result_all[$key] = $value;
        $result_all[$key]['from'] = 0;
        $result_all[$key]['field_approve'] = $key;
      }
    }

    return $result_all;
  }

  /**
   * Count sales message send.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function countSalesMessagesSend(string $date_from, string $date_to, $sales_id = NULL) {
    $timezone = Extra::getTimezone();
    $date_from = Extra::getDateFrom($date_from, $timezone);
    $date_to = Extra::getDateTo($date_to, $timezone);

    $query = db_select('comment', 'c');
    $query->condition('c.created', array($date_from, $date_to), 'BETWEEN');
    $query->condition('c.uid', array($sales_id));
    $query->addExpression('COUNT(*)');
    $result_query = $query->execute()->fetchField();

    return $result_query;
  }

  /**
   * Count by sales assigned service type request.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function countSalesAssignedByServiceType(string $date_from, string $date_to, $sales_id = NULL) {
    $timezone = Extra::getTimezone();

    $date_from = Extra::getDateFrom($date_from, $timezone);
    $date_to = Extra::getDateTo($date_to, $timezone);
    $result = array();
    $result_all = array();
    $field_move_service_type = static::getFieldValue('field_move_service_type');

    $query = db_select('move_request_sales_person', 'mrsp');
    $query->condition('sadmr.created', array($date_from, $date_to), 'BETWEEN');
    $query->condition('mrsp.uid', array($sales_id));
    $query->innerJoin('search_api_db_move_request', 'sadmr', 'sadmr.nid=mrsp.nid');
    $query->addExpression('COUNT(*)', 'count');
    $query->groupBy('sadmr.field_move_service_type');
    $query->fields('sadmr', array('field_move_service_type'));
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'field_move_service_type' => $key,
        'from' => $value,
      );
    }

    foreach ($field_move_service_type as $key => $value) {
      if (array_key_exists($key, $result)) {
        $result_all[$key] = array_merge($field_move_service_type[$key], $result[$key]);
      }
      else {
        $result_all[$key] = $value;
        $result_all[$key]['from'] = 0;
        $result_all[$key]['field_move_service_type'] = $key;
      }
    }

    return $result_all;
  }

  /**
   * Count request sales by source.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function countSalesBySource(string $date_from, string $date_to, $sales_id = NULL) {
    $timezone = Extra::getTimezone();

    $date_from = Extra::getDateFrom($date_from, $timezone);
    $date_to = Extra::getDateTo($date_to, $timezone);
    $result = array();

    $query = db_select('move_request_sales_person', 'mrsp');
    $query->condition('sadmr.created', array($date_from, $date_to), 'BETWEEN');
    $query->condition('mrsp.uid', array($sales_id));
    $query->innerJoin('search_api_db_move_request', 'sadmr', 'sadmr.nid=mrsp.nid');
    $query->addExpression('COUNT(*)', 'count');
    $query->groupBy('sadmr.field_parser_provider_name');
    $query->fields('sadmr', array('field_parser_provider_name'));
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $key => $value) {
      $result[$key] = array(
        'name' => $key,
        'from' => $value,
      );
    }
    return $result;
  }

  /**
   * Get statistics for sales by day.
   *
   * @return array
   *   Statistics.
   */
  public function getStatisticsForSalesByDay() {
    $result = array();
    $days = $this->getSevenDays();

    foreach ($days as $day => $value) {
      $date_from = Extra::getDateFrom($value, $this->timeZone);
      $date_to = Extra::getDateTo($value, $this->timeZone);
      $assign_sales = static::getAssignSalesFieldDate($date_from, $date_to, NULL);

      if ($assign_sales) {
        foreach ($assign_sales as $uid => $sale) {
          $result[$value][$uid] = $this->statisticsBySales($value, $value, $uid);
        }
      }
      else {
        $result[$value] = array();
      }

    }

    return $result;
  }

  /**
   * Helper method get 7 days.
   *
   * @return array
   *   Days.
   */
  private function getSevenDays() {
    $days = array();
    $date = new \DateTime();
    $date->setTimezone(new \DateTimeZone($this->timeZone));
    $date->setTimestamp(time());
    $days['curent_day'] = $date->format('Y-m-d');
    $days['second_day'] = $date->modify('-1 day')->format('Y-m-d');
    $days['third_day'] = $date->modify('-1 day')->format('Y-m-d');
    $days['fourth_day'] = $date->modify('-1 day')->format('Y-m-d');
    $days['fifth_day'] = $date->modify('-1 day')->format('Y-m-d');
    $days['sixth_day'] = $date->modify('-1 day')->format('Y-m-d');
    $days['last_day'] = $date->modify('-1 day')->format('Y-m-d');

    return $days;
  }

  /**
   * Get assign sales field date.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   From == assigned.
   */
  public static function getAssignSalesFieldDate($date_from, $date_to, $sales_id = NULL) {
    $result = array();
    $query = db_select('search_api_db_move_request', 'sadmr');
    $query->innerJoin('move_request_sales_person', 'mrsp', 'mrsp.nid=sadmr.nid');
    $query->condition('mrsp.created', array($date_from, $date_to), 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('mrsp', array('uid'));
    if (isset($sales_id)) {
      $query->condition('mrsp.uid', array($sales_id));
    }
    $query->groupBy('mrsp.uid');
    $result_query = $query->execute()->fetchAllKeyed();

    foreach ($result_query as $uid => $value) {
      $load_user = entity_metadata_wrapper('user', $uid);
      $username = "{$load_user->field_user_first_name->value()} {$load_user->field_user_last_name->value()}";
      $result[$uid] = array(
        "name" => $username,
        "from" => $value,
      );
    }

    return $result;
  }

  /**
   * Statics for sales by month.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics
   */
  public function statisticsForSalesByIntervalMonth(string $date_from, string $date_to, $sales_id = NULL) {
    $result = array();
    $months = array();
    $timezone = new \DateTimeZone($this->timeZone);
    $start = (new \DateTime($date_from, $timezone))->modify('first day of this month');
    $end = (new \DateTime($date_to, $timezone))->modify('last day of next month');
    $interval = \DateInterval::createFromDateString('1 month');
    $period = new \DatePeriod($start, $interval, $end);

    /* @var \DateTime $dt */
    foreach ($period as $dt) {
      $months[] = array(
        'year'  => $dt->format("Y"),
        'month' => $dt->format("m"),
        'day'   => $dt->format("d"),
      );
    }
    unset($months[2]);
    foreach ($months as $value) {
      $date_from = (new \DateTime())->setTimezone($timezone)->setDate($value['year'], $value['month'], $value['day'])->modify('first day of this month');
      $date_to = (new \DateTime())->setTimezone($timezone)->setDate($value['year'], $value['month'], $value['day'])->modify('last day of this month');
      $result[$value['year'] . '-' . $value['month']]['sales_by_assigned'] = static::assignCountManagersAndBooked($date_from->format('Y-m-d'), $date_to->format('Y-m-d'), $sales_id);
      $result[$value['year'] . '-' . $value['month']]['sales_payroll'] = static::getSalesPayroll($date_from->format('Y-m-d'), $date_to->format('Y-m-d'), $sales_id);
    }
    return $result;
  }

  /**
   * Get first assigned date in db.
   *
   * If want wo get for user - need to pass sales id.
   *
   * @param int|null $sales_id
   *   Sales user id.
   *
   * @return string
   *   Date or empty string.
   */
  public function getFirstAssignedDate(?int $sales_id = NULL) : string {
    $query = db_select('search_api_db_move_request', 'sadmr');
    if ($sales_id) {
      $query->innerJoin('move_request_sales_person', 'mrsp', 'mrsp.nid=sadmr.nid');
      $query->condition('mrsp.uid', $sales_id);
    }
    $query->fields('sadmr', array('field_manager_assign_time'));
    $query->isNotNull('field_manager_assign_time');
    $query->range(0, 1);
    $query->orderBy('field_foreman_assign_time', 'ASC');
    $result = $query->execute()->fetchField();
    return !empty($result) ? Extra::date('Y-m-d', $result, $this->timeZone) : '';
  }

}
