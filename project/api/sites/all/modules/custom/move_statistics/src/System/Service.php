<?php

namespace Drupal\move_statistics\System;

use Drupal\move_statistics\Services\Statistics;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'move_statistics' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_statistics\System\Service::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_statistics\System\Service::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_statistics\System\Service::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_statistics\System\Service::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_statistics\System\Service::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'count_site_visitors' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getVisitorsCount',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'stat_user_visit_account' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getStatUserVisitAccount',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'stat_user_visit_conf_page' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getStatUserVisitConfPage',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'stat_booked_by' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getStatUserBookedBy',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'count_unique_site_visitors' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getCountUniqueUsers',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'string',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'universal_site_statistics_by_const' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getUniversalStatisticsByConst',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'string',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'statistics_by_browser_type' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getStatisticsByBrowserType',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'statistics_by_browser_name' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getStatisticsByBrowserName',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'count_move_request_type' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getMoveRequestCountType',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'count_move_request_type_booked' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getMoveRequestTypeBookedCount',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'count_move_request_type_booked_all' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getMoveRequestTypeCountAll',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'statistics_source_all' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getStatisticsSourceBookedAll',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'count_request_size_of_move_all' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getCountRequestSizeOfMoveCountAndBooked',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'count_submit_form' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getFormSubmitCount',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'count_request_from_parser' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getRequestSourceParser',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'count_request_by_people' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getRequestSourcePeople',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_cancelled_request' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getCancelledRequest',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_sales_payroll' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getSalesPayroll',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'string',
                'source' => array('data' => 'uid'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_reservation_received' => array(
            'callback' => 'Drupal\move_statistics\System\Service::countReservationReceived',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_confirm_booked' => array(
            'callback' => 'Drupal\move_statistics\System\Service::countConfirmBookedDate',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_count_assigned' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getCountAssigned',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_count_assigned_booked' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getCountAssignedBooked',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_count_assigned_booked_date' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getCountAssignedBookedMoveDate',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_count_assigned_booked_all' => array(
            'callback' => 'Drupal\move_statistics\System\Service::assignCountManagersAndBooked',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'statistics_by_sales_all' => array(
            'callback' => 'Drupal\move_statistics\System\Service::statisticsBySales',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'string',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'statistics_by_sales_interval' => array(
            'callback' => 'Drupal\move_statistics\System\Service::statisticsForSalesByIntervalDay',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'string',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'statistics_by_sales_interval_month' => array(
            'callback' => 'Drupal\move_statistics\System\Service::statisticsForSalesByIntervalMonth',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'string',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_statistics_by_field' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getRequestFieldStatistics',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'field',
                'type' => 'string',
                'source' => array('data' => 'field'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_statistics_for_days' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getStatisticsForDays',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_statistics_all' => array(
            'callback' => 'Drupal\move_statistics\System\Service::statisticsAll',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'statistics_for_six_months' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getStatisticsForSixMonth',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'string',
                'source' => array('data' => 'uid'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'statistics_for_sales_by_day' => array(
            'callback' => 'Drupal\move_statistics\System\Service::getStatisticsForSalesByDay',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'generate_unique_uid' => array(
            'help' => 'Generate unique',
            'callback' => 'Drupal\move_statistics\System\Service::genUniqueUid',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
          'get_first_assigned_date' => array(
            'help' => 'Get first assigned date for user on just first day',
            'callback' => 'Drupal\move_statistics\System\Service::getFirstAssignedDate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'default value' => NULL,
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
      'move_request_statistics' => array(
        'actions' => array(
          'get_stat_for_period' => array(
            'help' => 'Get statistics for period',
            'callback' => 'Drupal\move_statistics\System\Service::getStatForPeriod',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => array(),
                'source' => array('data' => 'datefrom'),
              ),
              array(
                'name' => 'dateto',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => array(),
                'source' => array('data' => 'dateto'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_stat_for_month' => array(
            'help' => 'Get statistics for month',
            'callback' => 'Drupal\move_statistics\System\Service::getStatForMonth',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'dateMonth',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => array(),
                'source' => array('data' => 'dateMonth'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_stat_for_months' => array(
            'help' => 'Get statistics for month',
            'callback' => 'Drupal\move_statistics\System\Service::getStatForMonths',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_stat_for_year' => array(
            'help' => 'Get statistics for year',
            'callback' => 'Drupal\move_statistics\System\Service::getStatForYear',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'dateYear',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => array(),
                'source' => array('data' => 'dateYear'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_today_stat' => array(
            'help' => 'Retrieve statistics for today',
            'callback' => 'Drupal\move_statistics\System\Service::getTodayStat',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'type' => 'string',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_total_stat' => array(
            'help' => 'Retrieve total statistics',
            'callback' => 'Drupal\move_statistics\System\Service::getTotalStat',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
          'get_count_entrance_from' => array(
            'help' => 'Retrieve statistics for today',
            'callback' => 'Drupal\move_statistics\System\Service::getCountEntranceFrom',
            'file' => array(
              'type' => 'php',
              'module' => 'move_statistics',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'string',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'string',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function create($data = array()) {
    $al_instance = new Statistics();
    return $al_instance->create($data);
  }

  public static function retrieve($id) {
    $al_instance = new Statistics($id);
    return $al_instance->retrieve();
  }

  public static function update($id, $data) {
    $al_instance = new Statistics($id);
    return $al_instance->update($data);
  }

  public static function delete(int $id) {
    $al_instance = new Statistics($id);
    return $al_instance->delete();
  }

  public static function index() {
    $al_instance = new Statistics();
    return $al_instance->index();
  }

  public static function getVisitorsCount($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getVisitorsCount($datefrom, $dateto);
  }

  public static function getStatUserVisitAccount($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getStatUserVisitAccount($datefrom, $dateto);
  }

  public static function getStatUserVisitConfPage($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getStatUserVisitConfPage($datefrom, $dateto);
  }

  public static function getStatUserBookedBy($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getStatUserBookedBy($datefrom, $dateto);
  }

  public static function getStatisticsByBrowserType($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getStatisticsByBrowserType($datefrom, $dateto);
  }

  /**
   * Statistics all.
   *
   * @param string $datefrom
   *   Date from.
   * @param string $dateto
   *   Date to.
   *
   * @return array
   *   Statistics.
   */
  public static function statisticsAll(string $datefrom, string $dateto) {
    $al_instance = new Statistics();
    return $al_instance->statisticsAll($datefrom, $dateto);
  }

  /**
   * Statistics fo 6 month.
   *
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function getStatisticsForSixMonth($sales_id) {
    $al_instance = new Statistics();
    return $al_instance->getStatisticsForSixMonth($sales_id);
  }

  /**
   * Get statistics for sales by day.
   *
   * @return array
   *   Statistics.
   */
  public static function getStatisticsForSalesByDay() {
    $al_instance = new Statistics();
    return $al_instance->getStatisticsForSalesByDay();
  }

  public static function getStatisticsByBrowserName($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getStatisticsByBrowserName($datefrom, $dateto);
  }

  public static function getUniversalStatisticsByConst($datefrom, $dateto, $type) {
    $al_instance = new Statistics();
    return $al_instance->getUniversalStatisticsByConst($datefrom, $dateto, $type);
  }

  /**
   * Count unique users.
   *
   * @param string $datefrom
   *   Date from.
   * @param string $dateto
   *   Date to.
   * @param int $type
   *   Type.
   *
   * @return mixed
   *   Number count unique users.
   */
  public static function getCountUniqueUsers(string $datefrom, string $dateto, int $type) {
    $al_instance = new Statistics();
    return $al_instance->getCountUniqueUsers($datefrom, $dateto, $type);
  }

  public static function getMoveRequestTypeCount($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getMoveRequestTypeCount($datefrom, $dateto);
  }

  public static function getMoveRequestTypeBookedCount($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getMoveRequestTypeBookedCount($datefrom, $dateto);
  }

  public static function getMoveRequestTypeCountAll($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getMoveRequestTypeCountAll($datefrom, $dateto);
  }

  public static function getStatisticsSourceBookedAll($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getStatisticsSourceBookedAll($datefrom, $dateto);
  }

  public static function getCountRequestSizeOfMoveCountAndBooked($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getCountRequestSizeOfMoveCountAndBooked($datefrom, $dateto);
  }

  /**
   * Get sales payroll.
   *
   * @param string $datefrom
   *   Date from.
   * @param string $dateto
   *   Date to.
   * @param mixed $uid
   *   User id.
   *
   * @return array
   *   Payroll.
   */
  public static function getSalesPayroll(string $datefrom, string $dateto, $uid) {
    $al_instance = new Statistics();
    return $al_instance->getSalesPayroll($datefrom, $dateto, $uid);
  }

  public static function countConfirmBookedDate($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->countConfirmBookedDate($datefrom, $dateto);
  }

  public static function countReservationReceived($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->countReservationReceived($datefrom, $dateto);
  }

  public static function getCountAssigned($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getCountAssigned($datefrom, $dateto);
  }

  public static function getCountAssignedBooked($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getCountAssignedBooked($datefrom, $dateto);
  }

  public static function getCountAssignedBookedMoveDate($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getCountAssignedBookedMoveDate($datefrom, $dateto);
  }

  /**
   * Assigned count manager adn booked.
   *
   * @param string $datefrom
   *   Date From.
   * @param string $dateto
   *   Date to.
   *
   * @return array
   *   Statistics.
   */
  public static function assignCountManagersAndBooked(string $datefrom, string $dateto) {
    $al_instance = new Statistics();
    return $al_instance->assignCountManagersAndBooked($datefrom, $dateto);
  }

  /**
   * Statistics by sales.
   *
   * @param string $datefrom
   *   Date from.
   * @param string $dateto
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function statisticsBySales(string $datefrom, string $dateto, $sales_id) {
    $al_instance = new Statistics();
    return $al_instance->statisticsBySales($datefrom, $dateto, $sales_id);
  }

  /**
   * Statistic for sales by interval day.
   *
   * @param string $datefrom
   *   Date from.
   * @param string $dateto
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics.
   */
  public static function statisticsForSalesByIntervalDay(string $datefrom, string $dateto, $sales_id) {
    $al_instance = new Statistics();
    return $al_instance->statisticsForSalesByIntervalDay($datefrom, $dateto, $sales_id);
  }

  /**
   * Statics for sales by month.
   *
   * @param string $datefrom
   *   Date from.
   * @param string $dateto
   *   Date to.
   * @param mixed $sales_id
   *   Sales id.
   *
   * @return array
   *   Statistics
   */
  public static function statisticsForSalesByIntervalMonth(string $datefrom, string $dateto, $sales_id) {
    $al_instance = new Statistics();
    return $al_instance->statisticsForSalesByIntervalMonth($datefrom, $dateto, $sales_id);
  }

  public static function getRequestFieldStatistics($datefrom, $dateto, $field) {
    $al_instance = new Statistics();
    return $al_instance->getRequestFieldStatistics($datefrom, $dateto, $field);
  }

  public static function getFormSubmitCount($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getFormSubmitCount($datefrom, $dateto);
  }

  public static function getRequestSourceParser($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getRequestSourceParser($datefrom, $dateto);
  }

  public static function getRequestSourcePeople($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getRequestSourcePeople($datefrom, $dateto);
  }

  public static function getCancelledRequest($datefrom, $dateto) {
    $al_instance = new Statistics();
    return $al_instance->getCancelledRequest($datefrom, $dateto);
  }

  public static function getStatisticsForDays($date_from, $date_to) {
    $statistics = new Statistics();
    return $statistics->getStatisticsForDays($date_from, $date_to);
  }

  /**
   * Callback function for get statistics for period from all requests.
   */
  public static function getStatForPeriod($date_from, $date_to) {
    $statistics = new Statistics();
    return $statistics->getStatForPeriodByApprove($date_from, $date_to);
  }

  /**
   * Callback function for get statistics for month from all requests.
   */
  public static function getStatForMonth($date_month) {
    $statistics = new Statistics();
    return $statistics->getStatForMonthByApprove($date_month);
  }

  /**
   * Get statistics for month.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   *
   * @return array
   *   Statistics.
   */
  public static function getStatForMonths(string $date_from, string $date_to) {
    $statistics = new Statistics();
    return $statistics->getStatForMonths($date_from, $date_to);
  }

  /**
   * Callback function for get statistics for year from all requests.
   */
  public static function getStatForYear($date_year) {
    $statistics = new Statistics();
    return $statistics->getStatForYearByApprove($date_year);
  }

  /**
   * Callback function for retrieve all statistics.
   *
   * @param string $date
   *   Date Y-m-d.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getTodayStat(string $date) {
    $request = new Statistics();
    return $request->getTodayStat($date);
  }

  /**
   * Callback function for count entrances.
   *
   * @return mixed
   *   Statistics.
   */
  public static function getCountEntranceFrom($date_from, $date_to) {
    $request = new Statistics();
    return $request->getEntranceAllPeriod($date_from, $date_to);
  }

  /**
   * Callback function for get total statistics.
   */
  public static function getTotalStat() {
    $stat = new Statistics();
    return $stat->getStatAll();
  }

  public static function genUniqueUid() {
    $stat = new Statistics();
    return $stat->createUniqueUid();
  }

  /**
   * Get first assigned date in db.
   *
   * If want wo get for user - need to pass sales id.
   *
   * @param int|null $uid
   *   Sales user id.
   *
   * @return int
   *   Timestamp or 0.
   */
  public static function getFirstAssignedDate(?int $uid = NULL) {
    return (new Statistics)->getFirstAssignedDate($uid);
  }

}
