<?php
/**
 * @file
 * move_request_stat.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function move_statistics_default_rules_configuration() {
  $items = array();
  $items['rules_move_request_stat_by_approve_confirmed_'] = entity_import('rules_config', '{ "rules_move_request_stat_by_approve_confirmed_" : {
      "LABEL" : "Move Request Stat by Approve - \\u0022Confirmed\\u0022",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "move_statistics" ],
      "ON" : { "node_update--move_request" : { "bundle" : "move_request" } },
      "IF" : [
        { "AND" : [
            { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_approve" } },
            { "NOT data_is" : {
                "data" : [ "node-unchanged:field-approve" ],
                "op" : "IN",
                "value" : { "value" : { "2" : "2", "3" : "3" } }
              }
            },
            { "data_is" : {
                "data" : [ "node:field-approve" ],
                "op" : "IN",
                "value" : { "value" : { "2" : "2", "3" : "3" } }
              }
            }
          ]
        }
      ],
      "DO" : [ { "move_request_stat_action" : { "obj" : [ "node" ] } } ]
    }
  }');
  return $items;
}
