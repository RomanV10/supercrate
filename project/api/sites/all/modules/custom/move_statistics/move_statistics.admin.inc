<?php

use Drupal\move_statistics\Services\Statistics;

function move_statistics_move_request_cache($form, &$form_state) {
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Start'),
  );

  return $form;
}

function move_statistics_move_request_cache_submit($form, &$form_state) {
  $instance = new Statistics();
  $operations = $instance->cacheRequestStatisticsPrepareBatch();
  $batch = array(
    'operations' => $operations,
    'finished' => 'move_statistics_move_request_batch_finished',
    'file' => drupal_get_path('module', 'move_statistics') . '/move_statistics.admin.inc',
  );

  batch_set($batch);
}

function move_statistics_move_request_batch_execute($date, &$context) {
  $instance = new Statistics();
  $instance->updateStatisticsForDays($date);
  $context['results']['titles'][] = $date;
  $context['message'] = 'Update cache move_request for <em>' . check_plain($date) . '</em>';
}

function move_statistics_move_request_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Cashed ' . count($results['titles']) . ' days.');
  }
  else {
    drupal_set_message('Ended with errors', 'error');
  }
}
