<?php

/**
 * Implements hook_rules_action_info().
 */
function move_statistics_rules_action_info() {
  $actions = array();
  $actions['move_request_stat_action'] = array(
    'label' => t('Move Request stat class based action'),
    'group' => t('Custom'),
    'parameter' => array(
      'obj' => array(
        'type' => array('node'),
        'label' => t('New entity'),
        'optional' => FALSE,
      ),
    ),
  );

  return $actions;
}

/**
 * Rules action for record to database.
 *
 * @param object $node
 *   The Node object.
 */
function move_request_stat_action($node) {
  date_default_timezone_set('UTC');
  $date = new DateTime('now');
  $emv_node = entity_metadata_wrapper('node', $node);
  $query = db_merge('move_request_stat')
    ->key(array('nid' => $node->nid))
    ->fields(array(
      'date' => $date->getTimestamp(),
      'type' => $emv_node->field_approve->raw(),
    ));
  $query->execute();
}
