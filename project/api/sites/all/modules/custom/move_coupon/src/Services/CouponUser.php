<?php

namespace Drupal\move_coupon\Services;

use Drupal\move_services_new\Services\BaseService;

/**
 * Class Coupon.
 *
 * @package Drupal\move_coupon\Services
 */
class CouponUser extends BaseService {
  private $id = NULL;

  public function __construct($id = NULL) {
    $this->id = $id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function create($data = array()) {}

  public function retrieve() {
    $result = db_select('move_coupon_user_buy', 'mcub')
      ->fields('mcub')
      ->condition('mcub.id', $this->id)
      ->execute()
      ->fetchAssoc();

    return $result;
  }

  public function update($data = array()) {
    $record = array(
      'id' => $this->id,
      'used' => $data['used'],
    );
    $result = drupal_write_record('move_coupon_user_buy', $record, 'id');

    return $result;
  }

  public function delete() {
    $result = db_delete('move_coupon')
      ->condition('id', $this->id)
      ->execute();

    return $result;
  }

  public function index() {
    $query = db_select('move_coupon_user_buy', 'mcub')
      ->fields('mcub')
      ->execute()
      ->fetchAll();

    foreach ($query as $key => &$coupon) {
      $coupon->user_name = $this->getUseNamebyUid($coupon->uid);
    }

    return $query;
  }

  public static function getUseNamebyUid(int $uid) {
    $user_first_name = 'Anonymous';
    $user_last_name = 'User';
    $user_wrapper = entity_metadata_wrapper('user', $uid);

    if ($user_wrapper->label()) {
      $user_first_name = $user_wrapper->field_user_first_name->value();
      $user_last_name = $user_wrapper->field_user_last_name->value();
    }

    return "$user_first_name $user_last_name";
  }

  // Create coupon for user.

  /**
   * Create coupon after user pay.
   *
   * @param int $entity_id
   *   Entity id.
   * @param array $data_payment
   *   Payment data.
   *
   * @return mixed
   *   Promo code.
   *
   * @throws \Exception
   */
  public static function createCouponAfterPayment(int $entity_id, array $data_payment) {
    global $user;

    try {
      $coupon_user = array(
        'uid' => $user->uid,
        'coupon_id' => $entity_id,
        'total' => $data_payment['amount'],
        'discount' => $data_payment['coupon']['discount'],
        'promo' => Coupon::generatePromoForCoupon(),
        'hash' => $data_payment['coupon']['hash'],
        'created' => time(),
      );

      $create = drupal_write_record('move_coupon_user_buy', $coupon_user);
      $result = $coupon_user['promo'];
      if ($create) {
        static::decrementCoupon($entity_id, 1);
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()}";
      watchdog('createCouponAfterPayment', $message, array(), WATCHDOG_ERROR);
      $error_msg = array(
        'status' => FALSE,
        'text' => $e->getMessage(),
      );
      throw new \ServicesException('', 400, $error_msg);
    }
    return $result;
  }

  public function getAllPurchasedCoupons() {
    $result = db_select('move_coupon_user_buy', 'mcub')
      ->fields('mcub')
      ->condition('mcub.used', 1)
      ->execute()
      ->fetchAssoc();

    return $result;
  }

  public function getAllNotUsedCouponsForUser() {
    global $user;
    $result = FALSE;
    if (!user_is_anonymous()) {
      $result = db_select('move_coupon_user_buy', 'mcub')
        ->fields('mcub')
        ->condition('mcub.used', '1', '!=')
        ->condition('mcub.uid', $user->uid)
        ->execute()
        ->fetchAll();
    }

    return $result;
  }

  public function getAllNotUsedCouponsForAdmin(int $uid) {
    $result = db_select('move_coupon_user_buy', 'mcub')
      ->fields('mcub')
      ->condition('mcub.used', '1', '!=')
      ->condition('mcub.uid', $uid)
      ->execute()
      ->fetchAll();

    return $result;
  }

  private static function decrementCoupon(int $id_admin_coupon, int $count_coupon) {
    $result = FAlSE;
    $al_instance = new Coupon($id_admin_coupon);
    $admin_coupon = $al_instance->retrieve();

    if (isset($admin_coupon['value']['coupons_left']) && ($admin_coupon['value']['coupons_left'] - $count_coupon >= 0)) {
      $data['coupons_left'] = $admin_coupon['value']['coupons_left'] - $count_coupon;
      $al_instance->update($data);
      $result = TRUE;
    }

    return $result;
  }

}
