<?php

namespace Drupal\move_coupon\Services;

use Drupal\move_services_new\Services\BaseService;

/**
 * Class Coupon.
 *
 * @package Drupal\move_coupon\Services
 */
class Coupon extends BaseService {
  private $id = NULL;

  public function __construct($id = NULL) {
    $this->id = $id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function create($data = array()) {
    $data['hash'] = static::generateHashForCoupon();

    $id = db_insert('move_coupon')
      ->fields(array(
        'value' => serialize($data),
        'hash' => $data['hash'],
      ))
      ->execute();

    return $id;
  }

  public function retrieve() {
    $result = db_select('move_coupon', 'mc')
      ->fields('mc')
      ->condition('mc.id', $this->id)
      ->execute()
      ->fetchAssoc();

    $result['value'] = unserialize($result['value']);

    return $result;
  }

  public function update($data = array()) {
    $merge_coupon = array();
    $retrieve_coupon = $this->retrieve();

    if (is_array($retrieve_coupon['value'])) {
      $merge_coupon = array_merge($retrieve_coupon['value'], $data);
    }

    $record = array(
      'id' => $this->id,
      'value' => $merge_coupon,
    );
    $result = drupal_write_record('move_coupon', $record, 'id');

    return $result;
  }

  public function delete() {
    $result = db_delete('move_coupon')
      ->condition('id', $this->id)
      ->execute();

    return $result;
  }

  public function index() {
    $result = array();
    $query = db_select('move_coupon', 'mc')
      ->fields('mc')
      ->execute()
      ->fetchAll();

    foreach ($query as $key => $value) {
      $unserialize = unserialize($value->value);

      $result[] = array(
        'id' => $value->id,
        'value' => $unserialize,
      );
    }

    return $result;
  }

  public static function generateHashForCoupon() {
    return $hash = user_password(52);
  }

  public static function generatePromoForCoupon() {
    $promo = user_password(6);
    if (static::checkPromoUnique($promo)) {
      return $promo;
    }
    else {
      static::generatePromoForCoupon();
    }
  }

  private static function checkPromoUnique(string $promo) {
    return static::getCouponByPromo($promo) ? FALSE : TRUE;
  }

//  public static function getHashByCouponId(int $coupon_id) {
//    $query = db_select('move_coupon', 'mc')
//      ->fields('mc', array('value'))
//      ->condition('mc.id', $coupon_id)
//      ->execute()
//      ->fetchField();
//
//    $coupon = unserialize($query);
//
//    return $coupon['hash'];
//  }

  public static function getCouponByPromo(string $promo) {
    $query = db_select('move_coupon_user_buy', 'mcub')
      ->fields('mcub')
      ->condition('mcub.promo', $promo)
      ->execute()
      ->fetchAll();

    return $query;
  }

  public static function getCouponByHash(string $hash) {
    $query = db_select('move_coupon', 'mcub')
      ->fields('mcub')
      ->condition('mcub.hash', $hash)
      ->execute()
      ->fetchObject();

    $result = unserialize($query->value);
    $result['id'] = $query->id;

    return $result;
  }

  public static function checkLimitCoupon(int $coupon_id) {
    $result = FALSE;
    $query = db_select('move_coupon', 'mc')
      ->fields('mc', array('value'))
      ->condition('mc.id', $coupon_id)
      ->execute()
      ->fetchField();

    $coupon = unserialize($query);
    if (isset($coupon['coupons_count']) && $coupon['coupons_count'] && $coupon['active'] && isset($coupon['active'])) {
      $result = TRUE;
    }

    return $result;
  }

}
