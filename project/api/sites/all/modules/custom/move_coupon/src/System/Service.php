<?php

namespace Drupal\move_coupon\System;

use Drupal\move_coupon\Services\Coupon;
use Drupal\move_coupon\Services\CouponUser;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'move_coupon' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_coupon\System\Service::createCoupon',
            'file' => array(
              'type' => 'php',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_coupon\System\Service::retrieveCoupon',
            'file' => array(
              'type' => 'php',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_coupon\System\Service::updateCoupon',
            'file' => array(
              'type' => 'php',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_coupon\System\Service::deleteCoupon',
            'file' => array(
              'type' => 'php',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_coupon\System\Service::indexCoupon',
            'file' => array(
              'type' => 'php',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'hash_coupon' => array(
            'callback' => 'Drupal\move_coupon\System\Service::hashCoupon',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'hash',
                'type' => 'string',
                'source' => array('data' => 'hash'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'promo_coupon' => array(
            'callback' => 'Drupal\move_coupon\System\Service::promoCoupon',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'promo',
                'type' => 'string',
                'source' => array('data' => 'promo'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
      'move_coupon_user' => array(
        'operations' => array(
          'retrieve' => array(
            'callback' => 'Drupal\move_coupon\System\Service::retrieveUserCoupon',
            'file' => array(
              'type' => 'php',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_coupon\System\Service::updateUserCoupon',
            'file' => array(
              'type' => 'php',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_coupon\System\Service::deleteUserCoupon',
            'file' => array(
              'type' => 'php',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_coupon\System\Service::indexUserCoupon',
            'file' => array(
              'type' => 'php',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'get_purchased_coupons' => array(
            'callback' => 'Drupal\move_coupon\System\Service::purchasedUserCoupon',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_not_used_coupons_for_user' => array(
            'callback' => 'Drupal\move_coupon\System\Service::notUsedCouponUser',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
          'get_not_used_coupons_for_admin' => array(
            'callback' => 'Drupal\move_coupon\System\Service::notUsedCouponAdmin',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_coupon',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'string',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  public static function createCoupon($data = array()) {
    $al_instance = new Coupon();
    return $al_instance->create($data);
  }

  public static function retrieveCoupon($id) {
    $al_instance = new Coupon($id);
    return $al_instance->retrieve();
  }

  public static function updateCoupon($id, $data) {
    $al_instance = new Coupon($id);
    return $al_instance->update($data);
  }

  public static function deleteCoupon(int $id) {
    $al_instance = new Coupon($id);
    return $al_instance->delete();
  }

  public static function indexCoupon() {
    $al_instance = new Coupon();
    return $al_instance->index();
  }

  public static function retrieveUserCoupon($id) {
    $al_instance = new CouponUser($id);
    return $al_instance->retrieve();
  }

  public static function updateUserCoupon($id, $data) {
    $al_instance = new CouponUser($id);
    return $al_instance->update($data);
  }

  public static function indexUserCoupon() {
    $al_instance = new CouponUser();
    return $al_instance->index();
  }

  public static function purchasedUserCoupon() {
    $al_instance = new CouponUser();
    return $al_instance->getAllPurchasedCoupons();
  }

  public static function notUsedCouponUser() {
    $al_instance = new CouponUser();
    return $al_instance->getAllNotUsedCouponsForUser();
  }

  public static function notUsedCouponAdmin(int $uid) {
    $al_instance = new CouponUser();
    return $al_instance->getAllNotUsedCouponsForAdmin($uid);
  }

  public static function hashCoupon($hash) {
    $al_instance = new Coupon();
    return $al_instance->getCouponByHash($hash);
  }

  public static function promoCoupon($hash) {
    $al_instance = new Coupon();
    return $al_instance->getCouponByPromo($hash);
  }
}