<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_coupon\Services\Coupon;

class MoveCouponTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new Coupon();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createCoupon() {
    $file_path = dirname(__FILE__) . '/data/coupon_admin.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('coupon_admin', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/coupon_admin_update.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('coupon_admin_update', $parse_json['data']);
  }

  public function testCreateCoupon() {
    self::createCoupon();
    $coupon = variable_get('coupon_admin');

    $coupon_id = $this->fixture->create($coupon);
    $this->assertGreaterThanOrEqual(1, $coupon);

    return (int) $coupon_id;
  }

  /**
   * @depends testCreateCoupon
   */
  public function testRetrieveCoupon($coupon_id) {
    $this->fixture->setId($coupon_id);
    $ret = $this->fixture->retrieve();
    $this->assertCount(2, $ret);
  }

  /**
   * @depends testCreateCoupon
   *
   */
  public function testUpdateCoupon($coupon_id) {
    $coupon_up = variable_get('request_storage_update');
    $coupon_update = new Coupon($coupon_id);
    $upd = $coupon_update->update($coupon_up, TRUE);
    $this->assertEquals(2, $upd);
  }

  /**
   * @depends testCreateCoupon
   *
   */
  public function testDeleteCoupon($coupon_id) {
    $coupon_delete = new Coupon($coupon_id);
    $del = $coupon_delete->delete();
    var_dump($del);
    $this->assertEquals(1, $del);
  }

}