<?php

namespace Drupal\move_profit_loses\System;

use Drupal\move_profit_loses\Services\ProfitLoses;

/**
 * Class Service.
 *
 * @package Drupal\move_profit_loses\System
 */
class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'profit_losses' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_profit_loses\System\Service::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_profit_loses',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'amount',
                'type' => 'int',
                'source' => array('data' => 'amount'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date',
                'type' => 'string',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'recurring',
                'type' => 'int',
                'source' => array('data' => 'recurring'),
                'default value' => 0,
                'optional' => TRUE,
              ),
              array(
                'name' => 'recurring_to',
                'type' => 'string',
                'source' => array('data' => 'recurring_to'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_profit_loses\System\Service::retrieve',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_profit_loses',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_profit_loses\System\Service::update',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_profit_loses',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_profit_loses\System\Service::delete',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_profit_loses',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_profit_loses\System\Service::index',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_profit_loses',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'date_from',
                'type' => 'string',
                'source' => array('param' => 'date_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_to',
                'type' => 'string',
                'source' => array('param' => 'date_to'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'show_all_profit_loss_info' => array(
            'callback' => 'Drupal\move_profit_loses\System\Service::showAllProfitLossInfo',
            'file' => array(
              'type' => 'php',
              'module' => 'move_profit_loses',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'date_from',
                'type' => 'string',
                'source' => array('data' => 'dateFrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_to',
                'type' => 'string',
                'source' => array('data' => 'dateTo'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'source' => array('data' => 'currentPage'),
                'type' => 'int',
                'default value' => 1,
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'source' => array('data' => 'perPage'),
                'type' => 'int',
                'default value' => 25,
              ),
              array(
                'name' => 'sorting',
                'optional' => TRUE,
                'source' => array('data' => 'sorting'),
                'type' => 'array',
                'default value' => ['direction' => "DESC", 'field' => "nid"],
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function create(float $amount, string $date, array $data, $recurring = 0, $recurring_to = 0) {
    $loses_instance = new ProfitLoses();
    $data = array(
      'amount' => $amount,
      'date' => $date,
      'recurring' => $recurring,
      'recurring_to' => $recurring_to,
      'data' => $data,
    );

    return $loses_instance->create($data);
  }

  /**
   * Get profit and loses by id.
   *
   * @param int $id
   *   ID of row.
   * @return mixed
   */
  public static function retrieve(int $id) {
    $loses_instance = new ProfitLoses($id);
    return $loses_instance->retrieve();
  }

  /**
   * Update profit and loses
   *
   * @param int $id
   *   Id of the row.
   * @param array $data
   *   Data for update.
   *
   * @return \DatabaseStatementInterface|mixed
   */
  public static function update(int $id, array $data) {
    $loses_instance = new ProfitLoses($id);
    return $loses_instance->update($data);
  }

  /**
   * Delete.
   * @param int $id
   *   Id of the row.
   *
   * @return int|mixed
   */
  public static function delete(int $id) {
    $loses_instance = new ProfitLoses($id);
    return $loses_instance->delete();
  }

  /**
   * Get all info with request by dates.
   *
   * @param string $date_from
   *   From date.
   * @param string $date_to
   *   To date.
   *
   * @return array
   *   Array with requests and payment totals.
   */
  public static function index(string $date_from, string $date_to) : array {
    $loses_instance = new ProfitLoses(NULL, $date_from, $date_to);
    return $loses_instance->index();
  }

  /**
   * Get all requests with estimate and actual payment income.
   *
   * @param string $date_from
   *   From date.
   * @param string $date_to
   *   To date.
   * @param int $current_page
   *   Start page.
   * @param int $per_page
   *   Items on page
   * @param array $sorting
   *   Array with fields for sorting.
   *
   * @return array
   *   Array with requests and other data.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \SearchApiException
   * @throws \ServicesException
   */
  public static function showAllProfitLossInfo(string $date_from, string $date_to, int $current_page = 1, int $per_page = 25, array $sorting = []) : array {
    return (new ProfitLoses(NULL, $date_from, $date_to))->showAllProfitLossInfo($current_page, $per_page, $sorting);
  }

}
