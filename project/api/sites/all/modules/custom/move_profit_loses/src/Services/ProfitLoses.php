<?php

namespace Drupal\move_profit_loses\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\move_request\MoveRequestSearch;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\FrequencyType;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\RequestSearchTypes;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class ProfitLoses.
 *
 * @package Drupal\move_profit_loses\Services
 */
class ProfitLoses extends BaseService {

  private $idExpenses;
  private $dateFrom;
  private $dateTo;
  public $timeZone = 'UTC';
  public $calculateGrandTotal;

  /**
   * ProfitLoses constructor.
   *
   * @param int $id
   *   Id of expenses if have.
   * @param string $date_from
   *   Date for search from date.
   * @param string $date_to
   *   Date for search to date.
   */
  public function __construct($id = NULL, $date_from = '', $date_to = '') {
    $this->timeZone = Extra::getTimezone();
    $this->idExpenses = $id;
    $this->dateFrom = $date_from ? Extra::getDateFrom($date_from, $this->timeZone) : '';
    $this->dateTo = $date_to ? Extra::getDateTo($date_to, $this->timeZone) : '';
  }

  /**
   * {@inheritdoc}
   */
  public function create($data = array()) {
    $result = db_insert('move_expenses')
      ->fields(array(
        'amount' => $data['amount'],
        'date' => Extra::convertDateToTimestamp($data['date'], $this->timeZone),
        'recurring' => $data['recurring'],
        'data' => serialize($data['data']),
      ))
      ->execute();

    if ($result && $data['recurring']) {
      $frequency = isset($data['data']['frequency']) ? $data['data']['frequency'] : FrequencyType::MONTHLY;
      $check_frequency = FrequencyType::isValidValue((int) $frequency);
      $recurring_to = $data['recurring_to'] ? strtotime($data['recurring_to']) : 0;
      if ($check_frequency) {
        db_insert('move_expenses_frequently')
          ->fields(array(
            'id_expenses' => $result,
            'frequency_type' => $frequency,
            'recurring_to' => $recurring_to,
            'created' => time(),
            'updated' => time(),
          ))
          ->execute();
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function retrieve() {
    $select = db_select('move_expenses', 'me');
    $select->addJoin('LEFT OUTER', 'move_expenses_frequently', 'mef', 'me.id = mef.id_expenses');
    $select->condition('me.id', $this->idExpenses)
      ->fields('me')
      ->fields('mef', array(
        'recurring_to',
        'created',
        'updated',
      ));

    $result = $select->execute()->fetchAssoc();
    if ($result) {
      $result['data'] = unserialize($result['data']);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function update($data = array()) {
    $expense = $this->retrieve();
    $data_update = array(
      'amount' => isset($data['amount']) ? $data['amount'] : $expense['amount'],
      'date' => isset($data['date']) ? strtotime($data['date']) : $expense['date'],
      'recurring' => isset($data['recurring']) ? (int) ($data['recurring']) : $expense['recurring'],
      'data' => isset($data['data']) ? serialize($data['data']) : serialize($expense['data']),
    );

    $result = db_update("move_expenses")
      ->fields($data_update)
      ->condition('id', $this->idExpenses)
      ->execute();

    if (isset($data['recurring']) && $data['recurring']) {
      $frequency = isset($data['data']['frequency']) ? $data['data']['frequency'] : FrequencyType::MONTHLY;
      $check_frequency = FrequencyType::isValidValue((int) $frequency);
      if ($check_frequency) {
        $current_date = time();
        $write = array(
          'frequency_type' => $frequency,
          'recurring_to' => isset($data['recurring_to']) ? strtotime($data['recurring_to']) : 0,
          'updated' => $current_date,
        );

        $merge = db_merge('move_expenses_frequently')
          ->key(array('id_expenses' => $this->idExpenses))
          ->fields($write)
          ->execute();

        // Add Date create if expenses was created.
        if ($merge == \MergeQuery::STATUS_INSERT) {
          db_update('move_expenses_frequently')
            ->fields(array('created' => $current_date))
            ->condition('id_expenses', $this->idExpenses)
            ->execute();
        }
      }
    }

    if (isset($data['recurring']) && !$data['recurring'] && $expense['recurring']) {
      db_delete('move_expenses_frequently')
        ->condition('id_expenses', $this->idExpenses)
        ->execute();
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    db_delete('move_expenses_frequently')
      ->condition('id_expenses', $this->idExpenses)
      ->execute();

    return db_delete('move_expenses')
      ->condition('id', $this->idExpenses)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function index() {
    $result = array();

    if ($this->dateFrom && $this->dateTo) {
      $arguments['condition'] = array('field_approve' => [RequestStatusTypes::CONFIRMED]);
      $arguments['filtering'] = array('field_date_from' => $this->dateFrom, 'field_date_to' => $this->dateTo);
      $arguments['page'] = 0;
      $arguments['pagesize'] = 999999;
      $arguments['display_type'] = RequestSearchTypes::PROFIT_LOSE;

      $result['requests'] = (new MoveRequestSearch($arguments))->search(FALSE);
      $result['etsimated_income_total'] = $this->getEstimatedIncomeForPeriod();
      $result['actual_income_total'] = $this->getActualIncomeForPeriod();
      $result['sum_paycheck'] = (new Payroll())->getCountPaycheckForPeriod($this->dateFrom, $this->dateTo);
      $result['sum_expenses'] = $this->countExpenses();
      $result['expenses'] = $this->getExpenses();
      $result['profit'] = $result['actual_income_total'] - $this->calculateTotalExpenses($result['sum_paycheck'], $result['sum_expenses']);
    }

    return $result;
  }

  /**
   * Show all info for profit and loss.
   *
   * @param int $current_page
   *   Current page.
   * @param int $per_page
   *   Number requests per page.
   * @param array $sorting
   *   Array with fields for sorting.
   *
   * @return array
   *   Array with info.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \SearchApiException
   * @throws \ServicesException
   */
  public function showAllProfitLossInfo(int $current_page = 1, int $per_page = 25, array $sorting = []) : array {
    $result['requests'] = $this->getRequests($current_page, $per_page, $sorting);
    $result['sum_paycheck'] = (new Payroll())->getCountPaycheckForPeriod($this->dateFrom, $this->dateTo);
    $result['sum_expenses'] = $this->countExpenses();
    $result['expenses'] = $this->getExpenses();
    $result['estimated_income_total'] = $this->getEstimatedIncomeForPeriod();
    $result['actual_income_total'] = $this->getActualIncomeForPeriod();
    $result['expenses_total'] = $this->calculateTotalExpenses($result['sum_paycheck'], $result['sum_expenses']);
    $result['profit'] = $result['actual_income_total'] - $result['expenses_total'];

    return $result;
  }

  /**
   * Calculate total expenses.
   *
   * @param float $sum_paycheck
   *   Sum all paychecks.
   * @param float $sum_expenses
   *   Sum all expenses.
   *
   * @return float
   *   Total sum
   */
  private function calculateTotalExpenses(float $sum_paycheck, float $sum_expenses) : float {
    return $sum_paycheck + $sum_expenses;
  }

  /**
   * Get requests by date.
   *
   * @param int $current_page
   *   Number of needed page.
   * @param int $per_page
   *   Number requests per page.
   * @param array $sorting
   *   Array with fields for sorting.
   *
   * @return array
   *   Array with all info.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \SearchApiException
   * @throws \ServicesException
   */
  private function getRequests(int $current_page, int $per_page, array $sorting = []) : array {
    $arguments['condition'] = array('field_approve' => [RequestStatusTypes::CONFIRMED]);
    $arguments['filtering'] = array('field_date_from' => $this->dateFrom, 'field_date_to' => $this->dateTo);
    $arguments['display_type'] = RequestSearchTypes::PROFIT_LOSE;
    $arguments['page'] = $current_page - 1;
    $arguments['pagesize'] = $per_page;
    $arguments['sorting'] = $sorting;
    $requests = (new MoveRequestSearch($arguments))->search();

    // Information about pages.
    $requests['meta'] = array(
      'currentPage' => (int) $current_page,
      'perPage' => (int) $per_page,
      'pageCount' => round($requests['count'] / $per_page),
      'totalCount' => (int) $requests['count'],
    );
    unset($requests['count']);

    return $requests;
  }

  /**
   * Get all receipts by requests move date.
   *
   * @return array
   *   Array with all receipts.
   */
  private function getAllReceiptByMoveDatePeriod() : array {
    $query = db_select('search_api_db_move_request', 'search');
    $query->join('moveadmin_receipt', 'receipt', 'receipt.entity_id = search.nid');
    $query->condition('search.field_approve', RequestStatusTypes::CONFIRMED);
    $query->condition('field_date', [$this->dateFrom, $this->dateTo], 'BETWEEN');
    $query->condition('receipt.entity_type', EntityTypes::MOVEREQUEST);
    $query->fields('receipt', array('receipt'));
    $result = $query->execute()->fetchCol();
    return $result ?? [];
  }

  /**
   * Calculate total form given receipts.
   *
   * @param array $receipts
   *   Array with receipts.
   *
   * @return float
   *   Total.
   */
  private function calculateReceiptsTotals(array $receipts) : float {
    $total = 0.0;
    foreach ($receipts as $receipt) {
      $receipt_array = unserialize($receipt);
      $total += $receipt_array['amount'] ?? 0;
    }
    return $total;
  }

  /**
   * Get actual income money for period (receipts).
   *
   * @return float
   *   Total of all incomes.
   */
  private function getActualIncomeForPeriod() : float {
    $receipts = $this->getAllReceiptByMoveDatePeriod();
    return !empty($receipts) ? $this->calculateReceiptsTotals($receipts) : 0.0;
  }

  /**
   * Get estimated income money(middle grand total).
   *
   * @return float
   *   Total of all incomes.
   */
  public function getEstimatedIncomeForPeriod() : float {
    $query = db_select('search_api_db_move_request', 'search');
    $query->join('field_data_field_grand_total_middle', 'grand_total_middle', 'grand_total_middle.entity_id = search.nid');
    $query->condition('search.field_approve', RequestStatusTypes::CONFIRMED)
      ->condition('field_date', [$this->dateFrom, $this->dateTo], 'BETWEEN')
      ->addExpression('SUM(field_grand_total_middle_value)', 'estimated_income');
    $result = $query->execute()->fetchField();
    return $result ?? 0.0;
  }

  /**
   * Get all expenses.
   *
   * @return array
   *   Array of expenses.
   */
  public function getExpenses() : array {
    $result = array();
    $select = db_select('move_expenses', 'me');
    $select->addJoin('LEFT OUTER', 'move_expenses_frequently', 'mef', 'me.id = mef.id_expenses');
    $select->condition('me.date', array($this->dateFrom, $this->dateTo), 'BETWEEN')
      ->fields('me')
      ->fields('mef', array(
        'recurring_to',
        'created',
        'updated',
      ));

    $execute = $select->execute()->fetchAllAssoc('id');

    foreach ($execute as $expense) {
      $expense->data = unserialize($expense->data);
      $result[] = $expense;
    }

    return $result;
  }

  /**
   * Sum of all expenses.
   *
   * @return float
   *   Total sum.
   */
  private function countExpenses() : float {
    $query = db_select('move_expenses', 'me')
      ->condition('me.date', array($this->dateFrom, $this->dateTo), 'BETWEEN');
    $query->addExpression('SUM(me.amount)');
    return round($query->execute()->fetchField(), 2);
  }

  /**
   * Get frequently.
   *
   * @return mixed
   *   False or frequently.
   */
  private function getFrequently() {
    return db_select('move_expenses_frequently', 'mef')
      ->fields('mef')
      ->execute()
      ->fetchAllAssoc('id');
  }

  /**
   * Add/update ExecFrequently.
   */
  public function execFrequently() : void {
    $new_date = new \DateTime('now', new \DateTimeZone($this->timeZone));
    $timestamp = $new_date->getTimestamp();
    $frequently = $this->getFrequently();
    $to_execute = array();
    $add_execute = function ($item, $diff) use (&$to_execute, $new_date, $timestamp) {
      $recurring_flag = $item->updated <= $timestamp - $diff;
      if (($item->recurring_to && $timestamp <= $item->recurring_to && $recurring_flag) || (!$item->recurring_to && $recurring_flag)) {
        $to_execute[] = $item;
      }
    };

    foreach ($frequently as $item) {
      switch ($item->frequency_type) {
        case FrequencyType::DAILY:
          $add_execute($item, 86400);
          break;

        case FrequencyType::WEEKLY:
          $add_execute($item, 604800);
          break;

        case FrequencyType::MONTHLY:
          $current_days = date("t");
          $add_execute($item, $current_days * 86400);
          break;
      }
    }

    foreach ($to_execute as $expense_f) {
      $this->idExpenses = $expense_f->id_expenses;
      $expense = $this->retrieve();
      $data = array(
        'amount' => $expense['amount'],
        'date' => date("Y-m-d H:i:s", $timestamp),
        'recurring' => 0,
        'data' => $expense['data'],
      );

      $create = $this->create($data);

      if ($create) {
        db_update("move_expenses_frequently")
          ->fields(array(
            'updated' => $timestamp,
            'id_expenses' => $create,
          ))
          ->condition('id', $expense_f->id)
          ->execute();

        db_update("move_expenses")
          ->fields(array(
            'recurring' => 0,
          ))
          ->condition('id', $expense_f->id_expenses)
          ->execute();

        db_update("move_expenses")
          ->fields(array(
            'recurring' => 1,
          ))
          ->condition('id', $create)
          ->execute();
      }
    }
  }

}
