<?php

namespace Drupal\move_profit_loses\Cron\Tasks;

use Drupal\move_profit_loses\Services\ProfitLoses;
use Drupal\move_services_new\Cron\Tasks\CronInterface;

/**
 * Class RecurringExpenses.
 *
 * @package Drupal\move_profit_loses\Cron\Tasks
 */
class RecurringExpenses implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    (new ProfitLoses())->execFrequently();
  }

}
