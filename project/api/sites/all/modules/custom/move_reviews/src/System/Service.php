<?php

namespace Drupal\move_reviews\System;

use Drupal\move_reviews\Services\Reviews;

/**
 * Class Service.
 *
 * @package Drupal\move_reviews\System
 */
class Service {

  /**
   * Function getResources.
   *
   * @return array
   *   Resources.
   */
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  /**
   * Private function definition.
   *
   * @return array
   *   Operations and actions.
   */
  private static function definition() {
    return array(
      'move_reviews' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_reviews\System\Service::createReviews',
            'file' => array(
              'type' => 'php',
              'module' => 'move_reviews',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'count_stars',
                'type' => 'int',
                'source' => array('data' => 'count_stars'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'text_review',
                'type' => 'string',
                'source' => array('data' => 'text_review'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'user_id',
                'type' => 'int',
                'source' => array('data' => 'user_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_reviews\System\Service::deleteReviews',
            'file' => array(
              'type' => 'php',
              'module' => 'move_reviews',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_reviews\System\Service::indexReviews',
            'file' => array(
              'type' => 'php',
              'module' => 'move_reviews',
              'name' => 'src\System\Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'retrieve' => array(
            'callback' => 'Drupal\move_reviews\System\Service::retrieveReviews',
            'file' => array(
              'type' => 'php',
              'module' => 'move_reviews',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'statistics_reviews' => array(
            'callback' => 'Drupal\move_reviews\System\Service::statisticsReviews',
            'file' => array(
              'type' => 'php',
              'module' => 'move_reviews',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'datefrom',
                'type' => 'int',
                'source' => array('data' => 'datefrom'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dateto',
                'type' => 'int',
                'source' => array('data' => 'dateto'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'sort_reviews' => array(
            'callback' => 'Drupal\move_reviews\System\Service::sortReviews',
            'file' => array(
              'type' => 'php',
              'module' => 'move_reviews',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'sort',
                'type' => 'string',
                'source' => array('data' => 'sort'),
                'optional' => TRUE,
                'default value' => 'created',
              ),
              array(
                'name' => 'direction',
                'type' => 'string',
                'source' => array('data' => 'direction'),
                'optional' => TRUE,
                'default value' => 'desc',
              ),
              array(
                'name' => 'page',
                'type' => 'int',
                'source' => array('data' => 'page'),
                'optional' => TRUE,
                'default value' => 0,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  /**
   * Callback function create review.
   *
   * @param int $entity_type
   *   Entity type.
   * @param int $entity_id
   *   Entity id.
   * @param int $count_stars
   *   Count stars.
   * @param string $text_review
   *   Review text.
   * @param int $user_id
   *   User id.
   *
   * @return int
   *   Integer value record.
   */
  public static function createReviews(int $entity_type, int $entity_id, int $count_stars, string $text_review, int $user_id) {
    $all_instance = new Reviews(NULL, $entity_type, $entity_id);
    $data = array(
      'count_stars' => $count_stars,
      'text_review' => $text_review,
      'user_id' => $user_id,
    );
    return $all_instance->create($data);
  }

  /**
   * Callback function for retrieve review.
   *
   * @param int $entity_type
   *   Entity type.
   * @param int $entity_id
   *   Entity id.
   *
   * @return array
   *   Array data of retrieve user review.
   */
  public static function retrieveReviews(int $entity_type, int $entity_id) {
    $all_instance = new Reviews(NULL, $entity_type, $entity_id);
    return $all_instance->retrieve();
  }

  /**
   * Callback function for delete review.
   *
   * @param int $id
   *   Unique id of review.
   *
   * @return int
   *   Result of delete from db.
   */
  public static function deleteReviews(int $id) {
    $all_instance = new Reviews($id);
    return $all_instance->delete();
  }

  /**
   * Callback function retrieve all reviews.
   *
   * @return array
   *   All reviews.
   */
  public static function indexReviews() {
    $all_instances = new Reviews();
    return $all_instances->index();
  }

  /**
   * Callback function for return review statistics.
   *
   * @param int $datefrom
   *   Unixtime from date.
   * @param int $dateto
   *   Unixtime to date.
   *
   * @return array
   *   Statistics review for time period group by count stars.
   */
  public static function statisticsReviews(int $datefrom, int $dateto) {
    $all_instance = new Reviews();
    return $all_instance->statisticsReviewAtStarsByTime($datefrom, $dateto);
  }

  /**
   * Callback function for sort reviews.
   *
   * @param string $sort
   *    Variable count_stars or created. Default created.
   * @param string $direction
   *    Variable directions: ASC or DESC.
   *
   * @return array
   *   Sorted array of reviews.
   */
  public static function sortReviews(string $sort, string $direction, int $page) {
    $all_instance = new Reviews();
    return $all_instance->sortReviews($sort, $direction, $page);
   }

}