<?php

namespace Drupal\move_reviews\Cron\Tasks;

use Drupal\move_reviews\Services\Reviews;
use Drupal\move_services_new\Cron\Tasks\CronInterface;

/**
 * Class GetNotSendReviews.
 *
 * @package Drupal\move_reviews\System
 */
class GetNotSendReviews implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $reviews_settings = json_decode(variable_get("reviews_settings"));
    if ($reviews_settings->reviews_autosend) {
      $queue = \DrupalQueue::get('move_reviews_send_reviews');
      $queue->createQueue();
      $reviews_not_send = (array) Reviews::reviewNotSend();
      $chunk = array_chunk($reviews_not_send, 1);
      foreach ($chunk as $review) {
        $review = reset($review);
        $queue->createItem($review);
      }
    }
  }

}
