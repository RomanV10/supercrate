<?php

namespace Drupal\move_reviews\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_template_builder\Services\EmailTemplate;
use Drupal\move_notification\Services\Notification;
use Drupal\move_services_new\Util\enum\NotificationTypes;

/**
 * Class Reviews.
 *
 * @package Drupal\move_reviews\Services
 */
class Reviews extends BaseService {
  private $id = NULL;
  private $entityType = NULL;
  private $entityId = NULL;

  /**
   * The number of page.
   *
   * @var int
   */
  private $page = 0;

  /**
   * The page size.
   *
   * @var int
   */
  private $pageSize = 25;

  /**
   * Reviews constructor.
   *
   * @param int $id
   *   Review id.
   * @param int $entity_type
   *   Entity type.
   * @param int $entity_id
   *   Entity id.
   */
  public function __construct($id = NULL, $entity_type = NULL, $entity_id = NULL) {
    $this->id = $id;
    $this->entityType = $entity_type;
    $this->entityId = $entity_id;
  }

  /**
   * Method create user review and save it in db.
   *
   * @param array $data
   *   Array data of user review.
   *
   * @return int
   *   Unique record id in 'move_reviews' db table.
   *
   * @throws \Exception
   */
  public function create($data = array()) {
    $review = db_insert('move_reviews')
      ->fields([
        'entity_type' => $this->entityType,
        'entity_id' => $this->entityId,
        'count_stars' => isset($data['count_stars']) ? $data['count_stars'] : NULL,
        'text_review' => isset($data['text_review']) ? $data['text_review'] : '',
        'user_id' => $data['user_id'],
        'created' => time(),
      ]
      )
      ->execute();

    if (isset($data['count_stars']) && $review) {
      self::setReviewSentFlag($this->entityId);
      $node = new \stdClass();
      $node->nid = $this->entityId;
      $node->type = 'move_request';
      $notification_text = ['text' => 'Number of stars is ' . $data['count_stars'] . '. Review text: ' . $data['text_review']];
      $notification_info = ['node' => $node, 'notification' => $notification_text];
      Notification::createNotification(NotificationTypes::CLIENT_ADD_REVIEW, $notification_info, $this->entityId, $this->entityType, $data['user_id']);
      static::sendReviewFeedbackEmail($this->entityId, $data['count_stars']);
    }

    return $review;
  }

  /**
   * Method retrieve array user review.
   *
   * @return array
   *   Retrieve review data.
   *
   * @throws \Exception
   */
  public function retrieve() {
    $result = db_select('move_reviews', 'mr')
      ->fields('mr')
      ->condition('mr.entity_type', $this->entityType)
      ->condition('mr.entity_id', $this->entityId)
      ->execute()
      ->fetchAssoc();

    if (!empty($result['user_id'])) {
      $user = (new Clients())->getUserFieldsData($result['user_id']);
      $result['user_name'] = isset($user['field_user_first_name']) ? $user['field_user_first_name'] : 'Anonymous';
      $result['user_last_name'] = isset($user['field_user_last_name']) ? $user['field_user_last_name'] : 'Anonymous';
    }

    return $result;
  }

  /**
   * Unusable method update review.
   *
   * @param array $data
   *   Data for update.
   */
  public function update($data = []) : void {}

  /**
   * Method delete review from db.
   *
   * @return int
   *   Result of db_delete operation.
   */
  public function delete() {
    $result = db_delete('move_reviews')
      ->condition('id', $this->id)
      ->execute();

    return $result;
  }

  /**
   * Method return all users reviews.
   *
   * @return array
   *   Return all users reviews.
   *
   * @throws \Exception
   */
  public function index() : array {
    $result = [];
    $reviews = db_select('move_reviews', 'mr')
      ->fields('mr')
      ->execute()
      ->fetchAll();

    foreach ($reviews as $key => $review) {
      if (!empty($review->user_id)) {
        $user = (new Clients())->getUserFieldsData($review->user_id);
      }
      $result[] = [
        'id' => $review->id,
        'entity_type' => $review->entity_type,
        'entity_id' => $review->entity_id,
        'count_stars' => $review->count_stars,
        'user_review' => $review->text_review,
        'user_id' => $review->user_id,
        'user_name' => !empty($user['field_user_first_name']) ? $user['field_user_first_name'] : 'Anonymous',
        'user_last_name' => !empty($user['field_user_last_name']) ? $user['field_user_last_name'] : 'Anonymous',
        'created' => $review->created,
      ];
    }

    return $result;
  }

  /**
   * Method return all reviews by count stars and created date.
   *
   * @param int $datefrom
   *   Date from.
   * @param int $dateto
   *   Date to.
   *
   * @return array
   *   Count reviews group by count stars for time period.
   */
  public function statisticsReviewAtStarsByTime(int $datefrom, int $dateto) {
    $query = db_select('move_reviews', 'mr');
    $query->condition('mr.created', [$datefrom, $dateto], 'BETWEEN');
    $query->addExpression('COUNT(*)', 'count');
    $query->fields('mr', ['count_stars']);
    $query->groupBy('mr.count_stars');
    $result_query = $query->execute()->fetchAllKeyed();

    return $result_query;
  }

  /**
   * Method return all reviews sort by created date or count stars asc or desc.
   *
   * @param string $sort
   *   Sort date created or count stars.
   * @param string $direction
   *   Direction asc or desc.
   * @param int $page
   *   Number page.
   *
   * @return array
   *   All sort reviews.
   *
   * @throws \Exception
   */
  public function sortReviews(string $sort, string $direction, int $page) {
    $result = [];
    if ($sort != 'created' && $sort != 'count_stars') {
      $sort = 'created';
    }
    if ($direction != 'DESC' && $direction != 'ASC') {
      $direction = 'DESC';
    }

    $query = db_select('move_reviews', 'mr');
    $query->fields('mr');
    $query->orderBy($sort, $direction);
    $query->range($page * $this->pageSize, $this->pageSize);
    $reviews = $query->execute()->fetchAll();

    foreach ($reviews as $key => $review) {
      if (!empty($review->user_id)) {
        $user = (new Clients())->getUserFieldsData($review->user_id);
      }

      $result[] = [
        'id' => $review->id,
        'entity_type' => $review->entity_type,
        'entity_id' => $review->entity_id,
        'count_stars' => isset($review->count_stars) ? $review->count_stars : 0,
        'user_review' => isset($review->text_review) ? $review->text_review : '',
        'user_id' => $review->user_id,
        'user_name' => !empty($user['field_user_first_name']) ? $user['field_user_first_name'] : 'Anonymous',
        'user_last_name' => !empty($user['field_user_last_name']) ? $user['field_user_last_name'] : 'Anonymous',
        'created' => $review->created,
      ];
    }

    return $result;
  }

  /**
   * Method get array requests_id if work is completed and review not send.
   *
   * @return array
   *   Key array it's nid, value - unix time completed date, user email.
   */
  public static function reviewNotSend() {
    $result = db_query("
      SELECT fdfcf.entity_id, fdfcd.field_completed_date_value, fdfem.field_e_mail_email
      FROM field_data_field_company_flags fdfcf
      INNER JOIN field_data_field_completed_date fdfcd ON fdfcf.entity_id = fdfcd.entity_id
      INNER JOIN field_data_field_e_mail fdfem ON fdfcf.entity_id = fdfem.entity_id
      WHERE fdfcf.field_company_flags_value = 3 AND fdfcf.entity_id NOT IN (
        SELECT entity_id
        FROM field_data_field_review_send
        WHERE field_review_send_value = 1)")
      ->fetchAll();

    return $result;
  }

  /**
   * Method return template by key name.
   *
   * @param string $review_template
   *   Key name template.
   *
   * @return array
   *   Template and subject.
   */
  public static function getReviewTemplate(string $review_template) {
    $data = [];
    $query_template = db_select('move_template_builder', 'mtb')
      ->fields('mtb')
      ->condition('mtb.key_name', '%' . db_like($review_template) . '%', 'LIKE')
      ->execute()
      ->fetchAll();

    if (isset($query_template) && $query_template) {
      $query_template = reset($query_template);
      $subject = json_decode($query_template->data);

      $data = [
        [
          'template' => $query_template->template,
          'subject'   => $subject->subject,
          'key_name'   => $query_template->key_name,
        ],
      ];
    }

    return $data;
  }

  /**
   * Send one review from queue.
   *
   * @param object $review
   *   Object of review data.
   *
   * @throws \Exception
   */
  public function cronSendOneReview($review) {
    $review_settings = json_decode(variable_get("reviews_settings"));
    $template = static::getReviewTemplate('review_reminder_send');
    if (!empty($template) && isset($review_settings->selectedDay->value) && !($review_settings->selectedDay->value === 6)) {
      $current_time = time();
      $date_auto_reviews = (int) $review->field_completed_date_value + (int) $review_settings->selectedDay->value * 86400;
      if (!empty($date_auto_reviews) && $current_time >= $date_auto_reviews) {
        $result = EmailTemplate::sendEmailTemplate($review->entity_id, $template);
        if ($result['send']) {
          Reviews::setReviewSentFlag($review->entity_id);
        }
      }
    }
  }

  /**
   * Identify feedback template by count stars and review settings.
   *
   * @param int $entity_id
   *   Entity id.
   * @param int $count_stars
   *   Count user set stars.
   */
  public static function sendReviewFeedbackEmail(int $entity_id, int $count_stars) {
    $review_settings = json_decode(variable_get("reviews_settings"));
    $count_stars_positive = $review_settings->current_stars_count;
    $current_negative_stars_count = $review_settings->current_negative_stars_count;

    if ($count_stars >= $count_stars_positive) {
      $template = static::getReviewTemplate('review_success_reminder');
      EmailTemplate::sendEmailTemplate($entity_id, $template);
    }
    elseif ($count_stars <= $current_negative_stars_count) {
      $template = static::getReviewTemplate('review_fail_reminder');
      EmailTemplate::sendEmailTemplate($entity_id, $template);
    }
  }

  /**
   * Set flag that review was sent.
   *
   * @param int $nid
   *   Request id.
   *
   * @throws \Exception
   */
  public static function setReviewSentFlag(int $nid) : void {
    $data_for_update = [
      'field_review_send' => [
        'raw' => 'value',
        'value' => 1,
      ],
    ];

    MoveRequest::updateNodeFieldsOnly($nid, $data_for_update, 'move_request', FALSE);
  }

}
