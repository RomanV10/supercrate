<?php

namespace Drupal\move_reviews\Cron\Queues;

use Drupal\move_reviews\Services\Reviews;
use Drupal\move_services_new\Cron\Queues\QueueInterface;

/**
 * Class SendReviews.
 *
 * @package Drupal\move_reviews\Cron\Queues
 */
class SendReviews implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute($data): void {
    (new Reviews())->cronSendOneReview($data);
  }

}
