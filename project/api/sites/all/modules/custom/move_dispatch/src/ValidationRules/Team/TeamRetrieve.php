<?php

namespace Drupal\move_dispatch\ValidationRules\Team;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class TeamRetrieve.
 *
 * @package Drupal\move_dispatch\ValidationRules\Team
 */
class TeamRetrieve implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'id' => ['required', 'int', 'dexist:move_dispatch_team,id'],
    ];
  }

}
