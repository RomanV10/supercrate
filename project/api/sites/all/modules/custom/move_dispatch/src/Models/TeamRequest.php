<?php

namespace Drupal\move_dispatch\Models;

/**
 * Class TeamRequest.
 *
 * @package Drupal\move_dispatch\Models
 */
class TeamRequest extends AbstractModel {
  protected $table = 'move_dispatch_team_requests';
  protected $alias = 'team_requests';

  /**
   * Add team id and request id to db.
   *
   * @param int $team_id
   *   Team id.
   * @param int $nid
   *   Request id.
   *
   * @return int
   *   Some number.
   *
   * @throws \InvalidMergeQueryException
   */
  public function assignTeamToRequest(int $team_id, int $nid) {
    return db_merge($this->table)
      ->key(array('team_id' => $team_id, 'nid' => $nid))
      ->fields(array(
        'team_id' => $team_id,
        'nid' => $nid,
      ))
      ->execute();
  }

  /**
   * Check if team is assigned.
   *
   * @param int $team_id
   *   Team id.
   *
   * @return mixed
   *   Array with teams.
   */
  public function isTeamAssigned(int $team_id) {
    return db_select($this->table, $this->alias)
      ->fields($this->alias, ['nid'])
      ->condition('team_id', $team_id)
      ->execute()
      ->fetchCol('nid');
  }

}
