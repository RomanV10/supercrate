<?php

namespace Drupal\move_dispatch\ValidationRules\Team;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class AddOneTeam.
 *
 * @package Drupal\move_dispatch\ValidationRules\Team
 */
class AddOneTeam implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'data' => ['array', 'required'],
      'data.foreman' => ['array', 'required'],
      'data.foreman.uid' => ['int', 'required'],
      'data.helpers' => ['array', 'present'],
      'data.helpers.*' => ['int', 'required'],
      'data.name' => ['string', 'required'],
      'data.team_id' => ['int', 'dexist:move_dispatch_team,id'],
    ];
  }

}
