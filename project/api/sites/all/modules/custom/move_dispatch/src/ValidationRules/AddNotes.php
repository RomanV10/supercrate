<?php

namespace Drupal\move_dispatch\ValidationRules;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class AddNotes.
 *
 * @package Drupal\move_dispatch\ValidationRules\Log
 */
class AddNotes implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'date' => ['required', 'string'],
      'note' => ['string'],
    ];
  }

}
