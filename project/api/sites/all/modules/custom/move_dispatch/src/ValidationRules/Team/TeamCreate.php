<?php

namespace Drupal\move_dispatch\ValidationRules\Team;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class TeamCreate.
 *
 * @package Drupal\move_dispatch\ValidationRules\Team
 */
class TeamCreate implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'name' => ['required', 'string'],
    ];
  }

}
