<?php

namespace Drupal\move_dispatch\ValidationRules\Log;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class LogAdd.
 *
 * @package Drupal\move_dispatch\ValidationRules\Log
 */
class Add implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'nid' => ['required', 'int'],
      'data' => ['required', 'min:3', 'json'],
    ];
  }

}
