<?php

namespace Drupal\move_dispatch\Routes;

use Drupal\move_dispatch\ValidationRules\Log\AddNotes;
use Drupal\move_dispatch\ValidationRules\Log\GetByDate;
use Drupal\move_dispatch\ValidationRules\Log\AddMulti;
use Drupal\move_dispatch\ValidationRules\Log\GetByNid;
use Drupal\move_dispatch\ValidationRules\Log\GetForCurrentDay;

/**
 * Class Service.
 *
 * @package Drupal\move_dispatch\Routes
 */
class LogRoutes {

  /**
   * Definition schema.
   *
   * @return array
   *   Array with routes.
   */
  public static function getRoutes() {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_dispatch',
      'name' => 'src/Controllers/Log',
    ];

    return array(
      'move_team_logs' => array(
        'actions' => array(
          'add_multi' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Log::addMulti',
            'file' => $controller_file,
            'rules' => AddMulti::class,
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_for_current_day' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Log::getForCurrentDay',
            'file' => $controller_file,
            'rules' => GetForCurrentDay::class,
            'args' => array(
              array(
                'name' => 'current_page',
                'type' => 'int',
                'source' => array('data' => 'current_page'),
                'optional' => TRUE,
                'default value' => 1,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_by_nid' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Log::getByNid',
            'file' => $controller_file,
            'rules' => GetByNid::class,
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'current_page',
                'type' => 'int',
                'source' => array('data' => 'current_page'),
                'optional' => TRUE,
                'default value' => 1,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_by_date' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Log::getByDate',
            'file' => $controller_file,
            'rules' => GetByDate::class,
            'args' => array(
              array(
                'name' => 'date_from',
                'type' => 'string',
                'source' => array('data' => 'date_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_to',
                'type' => 'string',
                'source' => array('data' => 'date_to'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'current_page',
                'type' => 'int',
                'source' => array('data' => 'current_page'),
                'optional' => TRUE,
                'default value' => 1,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

}
