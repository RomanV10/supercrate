<?php

namespace Drupal\move_dispatch\Models;

/**
 * Class TeamUser.
 *
 * @package Drupal\move_dispatch\Models
 */
class TeamUser extends AbstractModel {

  protected $table = 'move_dispatch_team_user';
  protected $alias = 'team_user';

  /**
   * Add users to team.
   *
   * @param array $users
   *   Users array.
   *
   * @return int
   *   Some number.
   */
  public function addUsers(array $users) {
    $query = db_insert($this->table);
    $query->fields(['team_id', 'uid', 'role', 'created']);
    foreach ($users as $user) {
      $query->values($user);
    }
    $result = $query->execute();

    return $result;
  }

  /**
   * Delete all users from team.
   *
   * @param int $team_id
   *   Team id.
   *
   * @return mixed
   *   FALSE or some number.
   */
  public function deleteAllUsers(int $team_id) {
    return db_delete($this->table)
      ->condition('team_id', $team_id)
      ->execute();
  }

  /**
   * Delete users from team by ids.
   *
   * @param int $team_id
   *   Team id.
   * @param array $users_ids
   *   Users ids.
   *
   * @return mixed
   *   Need to set good answer.
   */
  public function deleteUsers(int $team_id, array $users_ids) {
    return db_delete($this->table)
      ->condition('team_id', $team_id)
      ->condition('uid', $users_ids, 'IN')
      ->execute();
  }

}
