<?php

namespace Drupal\move_dispatch\Actions\SubActions;

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_dispatch\Tasks\Team as TeamTask;

/**
 * Class Team.
 *
 * @package Drupal\move_dispatch\Actions\SubActions
 */
class Team {

  private $teamTasks;

  /**
   * Team constructor.
   *
   * @param \Drupal\move_dispatch\Tasks\Team $team_tasks
   *   Team tasks.
   */
  public function __construct(TeamTask $team_tasks) {
    $this->teamTasks = $team_tasks;
  }

  /**
   * Send email after team was assigned.
   *
   * @param int $nid
   *   Request id.
   * @param array $data
   *   Crew data.
   */
  public function sendEmails(int $nid, array $data) : void {
    if (!empty($data['foremans']['added'])) {
      (new DelayLaunch())->createTask(TaskType::ASSIGN_TEAM_SEND_EMAIL_TO_FOREMANS, ['nid' => $nid, 'uids' => $data['foremans']['added']]);
    }

    if (!empty($data['helpers']['added'])) {
      (new DelayLaunch())->createTask(TaskType::ASSIGN_TEAM_SEND_EMAIL_TO_HELPERS, ['nid' => $nid, 'uids' => $data['helpers']['added']]);
    }
  }

  /**
   * Get team changes.
   *
   * @param array $new_team
   *   New crew array.
   * @param array $old_team
   *   Old crew array.
   *
   * @return array
   *   Array with team.
   */
  public function teamChanges(array $new_team, array $old_team) : array {
    $result = [];

    if ($this->teamTasks->isPickupCrewExist($new_team, $old_team)) {
      if ($pickup_crew_diff = $this->teamTasks->getPickupCrewChanges($new_team['pickedUpCrew'] ?? [], $old_team['pickedUpCrew'] ?? [])) {
        $result['pickup_crew_diff'] = $pickup_crew_diff;
      }
    }

    if ($this->teamTasks->isDeliveryCrewExists($new_team, $old_team)) {
      if ($delivery_crew_diff = $this->teamTasks->getDeliveryCrewChanges($new_team['deliveryCrew'] ?? [], $old_team['deliveryCrew'] ?? [])) {
        $result['delivery_crew_diff'] = $delivery_crew_diff;
      }
    }

    if ($this->teamTasks->isBaseCrewExist($new_team, $old_team)) {
      if ($base_crew_diff = $this->teamTasks->baseCrewChanges($new_team ?? [], $old_team ?? [])) {
        $result['base_crew_diff'] = $base_crew_diff;
      }
    }

    if ($this->teamTasks->isAdditionalCrewExist($new_team, $old_team)) {
      if ($additional_crew_diff = $this->teamTasks->additionalCrewChanges($new_team['additionalCrews'] ?? [], $old_team['additionalCrews'] ?? [])) {
        $result['additional_crew_diff'] = $additional_crew_diff;
      }
    }

    return $result;
  }

}
