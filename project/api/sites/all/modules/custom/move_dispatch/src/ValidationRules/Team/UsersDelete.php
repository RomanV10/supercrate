<?php

namespace Drupal\move_dispatch\ValidationRules\Team;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class UsersDelete.
 *
 * @package Drupal\move_dispatch\ValidationRules\Team
 */
class UsersDelete implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'team_id' => ['required', 'int', 'dexist:move_dispatch_team,id'],
      'users_ids' => ['required', 'array'],
    ];
  }

}
