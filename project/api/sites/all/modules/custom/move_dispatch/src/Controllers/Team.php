<?php

namespace Drupal\move_dispatch\Controllers;

use Drupal\move_dispatch\Actions\Team as TeamActions;

/**
 * Class Team.
 *
 * @package Drupal\move_dispatch\Controllers
 */
class Team {

  /**
   * Add new team.
   *
   * @param string $name
   *   Team name.
   *
   * @return array
   *   Array with full team info.
   */
  public static function create(string $name) {
    try {
      $result = static::response((new TeamActions())->saveTeam($name));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Create team', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Update team.
   *
   * @param int $id
   *   Team id.
   * @param string $name
   *   Team name.
   *
   * @return array
   *   Full team info.
   */
  public static function update(int $id, string $name) {
    try {
      $result = static::response((new TeamActions())->updateTeam($id, $name));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Update team', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Retrieve full team info.
   *
   * @param int $id
   *   Team id.
   *
   * @return array
   *   Array full team info.
   */
  public static function retrieve(int $id) {
    try {
      $result = static::response((new TeamActions())->retrieveTeam($id));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Retrieve team', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Get all teams.
   *
   * @return array
   *   Array with all teams.
   */
  public static function index() {
    try {
      $result = static::response((new TeamActions())->retrieveAllTeams());
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Get all teams', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Delete team.
   *
   * @param int $id
   *   Team id.
   *
   * @return array
   *   Array with bool answer.
   */
  public static function delete(int $id) {
    try {
      (new TeamActions())->deleteTeam($id);

      $result = static::response(TRUE);
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Delete team', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Add many team at once.
   *
   * @param array $data
   *   Data with all teams.
   *
   * @return array
   *   Array added teams.
   */
  public static function addAllTeamMulti(array $data) {
    try {
      $team_actions = new TeamActions();
      $team_actions->saveAllTeamMulti($data);
      $teams = $team_actions->retrieveAllTeams();

      $result = static::response($teams);
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Add many teams with users at once', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Add only one full team.
   *
   * @param array $data
   *   Data with team.
   *
   * @return array
   *   Array with full team info.
   */
  public static function addOneTeam(array $data) {
    try {
      $team_actions = new TeamActions();
      $saved_team = $team_actions->saveAllTeam($data);

      $team = $team_actions->retrieveTeam($saved_team['id']);
      $result = static::response($team);
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Add one teams with users', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Add only user to team. Remove all previous user.
   *
   * @param int $team_id
   *   Team id.
   * @param array $users
   *   Array with users info.
   *
   * @return array
   *   Array with full team info.
   */
  public static function addUsers(int $team_id, array $users) {
    try {
      $team_actions = new TeamActions();
      $team_actions->deleteAllUsers($team_id);
      $team_actions->addUsers($team_id, $users);
      $team = $team_actions->retrieveTeam($team_id);

      $result = static::response($team);
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Add users to team', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Update user in team.
   *
   * @param int $team_id
   *   Team id.
   * @param array $users
   *   Al users info.
   *
   * @return array
   *   Array with all team info.
   */
  public static function updateUsers(int $team_id, array $users) {
    try {
      $team_actions = new TeamActions();
      $team_actions->updateUsers($team_id, $users);
      $team = $team_actions->retrieveTeam($team_id);

      $result = static::response($team);
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Update users in team', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Delete all users from team.
   *
   * @param int $team_id
   *   Team id.
   *
   * @return array
   *   Array with full team info.
   */
  public static function deleteAllUsers(int $team_id) {
    try {
      $result = static::response((new TeamActions())->deleteAllUsers($team_id));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Remove all users from team', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Delete users from team by uids.
   *
   * @param int $team_id
   *   Team id.
   * @param array $users_ids
   *   Users ids.
   *
   * @return array
   *   Array with TRUE or FALSE.
   */
  public static function deleteUsers(int $team_id, array $users_ids) {
    try {
      (new TeamActions())->deleteUsers($team_id, $users_ids);

      $result = static::response(TRUE);
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Remove users from team by uids', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Check if team is busy.
   *
   * @param array $users_ids
   *   Users ids.
   * @param string $date
   *   Date string.
   * @param string $start_time
   *   Time string.
   * @param int $nid_to_exclude
   *   Nid what must be exclude from check.
   * @param int $type
   *   Delivery or Pickup.
   *
   * @return array
   *   Array with busy uids.
   */
  public static function isTeamAvailable(array $users_ids, string $date, string $start_time, int $nid_to_exclude, int $type) {
    try {
      $result = static::response((new TeamActions())->getNotAvailableUsers($users_ids, $date, $start_time, $nid_to_exclude, $type));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Check if team is available', $message, [], WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Get work time.
   *
   * @param string $date_from
   *   Date from: 2018-06-17.
   * @param string $date_to
   *   Date to: 2018-06-24.
   *
   * @return array
   *   Array with users and work time.
   */
  public static function getUsersWorkTime(string $date_from, string $date_to) {
    try {
      $result = static::response((new TeamActions())->getWorkTimeFromPayroll($date_from, $date_to));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Get users work time', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Assign team to request.
   *
   * @param int $nid
   *   Request id.
   * @param array $data
   *   Crew data.
   *
   * @return array
   *   Array with bool answer.
   */
  public static function assignTeamToRequest(int $nid, array $data) {
    try {
      $request = (new TeamActions())->assignTeamToRequest($nid, $data);
      $result = static::response($request);
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Assign team to request', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Check if team is assigned.
   *
   * @param int $team_id
   *   Team id.
   *
   * @return array
   *   Array with nodes.
   */
  public function isTeamAssigned(int $team_id) {
    try {
      $result = static::response((new TeamActions())->isTeamAssigned($team_id));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Check if team is assigned to request', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Response data.
   *
   * @param mixed $data
   *   Data for response.
   * @param int $status_code
   *   Code of response.
   * @param string $status_message
   *   Status message.
   *
   * @return array
   *   Array with response.
   */
  public static function response($data, $status_code = 200, $status_message = 'OK') {
    return [
      'data' => $data,
      'status_code' => $status_code,
      'status_message' => $status_message,
    ];
  }

}
