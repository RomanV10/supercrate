<?php

namespace Drupal\move_dispatch\Routes;

use Drupal\move_dispatch\ValidationRules\Team;

/**
 * Class Service.
 *
 * @package Drupal\move_dispatch\Routes
 */
class TeamRoutes {

  /**
   * Definition schema.
   *
   * @return array
   *   Array with routes.
   */
  public static function getRoutes() {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_dispatch',
      'name' => 'src/Controllers/Team',
    ];

    return array(
      'move_team' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::create',
            'file' => $controller_file,
            'rules' => Team\TeamCreate::class,
            'args' => array(
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::retrieve',
            'file' => $controller_file,
            'rules' => Team\TeamRetrieve::class,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::update',
            'file' => $controller_file,
            'rules' => Team\TeamUpdate::class,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::delete',
            'file' => $controller_file,
            'rules' => Team\TeamDelete::class,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::index',
            'file' => $controller_file,
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'add_all_teams' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::addAllTeamMulti',
            'file' => $controller_file,
            'rules' => Team\AddManyTeams::class,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'add_one_team' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::addOneTeam',
            'file' => $controller_file,
            'rules' => Team\AddOneTeam::class,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'add_users' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::addUsers',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'team_id',
                'type' => 'int',
                'source' => array('data' => 'team_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'users',
                'type' => 'array',
                'source' => array('data' => 'users'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update_users' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::updateUsers',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'team_id',
                'type' => 'int',
                'source' => array('data' => 'team_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'users',
                'type' => 'array',
                'source' => array('data' => 'users'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete_all_users' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::deleteAllUsers',
            'file' => $controller_file,
            'rules' => Team\UsersDelete::class,
            'args' => array(
              array(
                'name' => 'team_id',
                'type' => 'int',
                'source' => array('data' => 'team_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete_users' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::deleteUsers',
            'file' => $controller_file,
            'rules' => Team\UsersAllDelete::class,
            'args' => array(
              array(
                'name' => 'team_id',
                'type' => 'int',
                'source' => array('data' => 'team_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'users_ids',
                'type' => 'array',
                'source' => array('data' => 'users_ids'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'is_team_is_available' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::isTeamAvailable',
            'file' => $controller_file,
            'rules' => Team\IsTeamAvailable::class,
            'args' => array(
              array(
                'name' => 'users_ids',
                'type' => 'array',
                'source' => array('data' => 'users_ids'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date',
                'type' => 'string',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'start_time',
                'type' => 'string',
                'source' => array('data' => 'start_time'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'nid_to_exclude',
                'type' => 'int',
                'source' => array('data' => 'nid_to_exclude'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => TRUE,
                'default value' => 0,
              ),

            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_users_work_time' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::getUsersWorkTime',
            'file' => $controller_file,
            'rules' => Team\GetUsersWorkTime::class,
            'args' => array(
              array(
                'name' => 'date_from',
                'type' => 'string',
                'source' => array('data' => 'date_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_to',
                'type' => 'string',
                'source' => array('data' => 'date_to'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'assign_team_to_request' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::assignTeamToRequest',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'is_team_assigned' => array(
            'callback' => 'Drupal\move_dispatch\Controllers\Team::isTeamAssigned',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'team_id',
                'type' => 'int',
                'source' => array('data' => 'team_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

}
