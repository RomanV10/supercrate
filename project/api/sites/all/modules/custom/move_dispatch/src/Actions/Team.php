<?php

namespace Drupal\move_dispatch\Actions;

use Drupal\move_dispatch\Actions\SubActions\Team as TeamSubAction;
use Drupal\move_dispatch\Models\Team as TeamModel;
use Drupal\move_dispatch\Models\TeamRequest as TeamRequestModel;
use Drupal\move_dispatch\Models\TeamUser as TeamUserModel;
use Drupal\move_dispatch\Tasks\Team as TeamTasks;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class Team.
 *
 * @package Drupal\move_dispatch\Actions
 */
class Team {

  private $teamModel;
  private $teamUserModel;
  private $teamRequestModel;

  private $teamTasks;

  /**
   * TeamActions constructor.
   */
  public function __construct() {
    $this->teamModel = new TeamModel();
    $this->teamUserModel = new TeamUserModel();
    $this->teamRequestModel = new TeamRequestModel();

    $this->teamTasks = new TeamTasks();
  }

  /**
   * Save team.
   *
   * @param string $name
   *   Team name.
   *
   * @return mixed
   *   Team data.
   */
  public function saveTeam(string $name) {
    $result['name'] = $name;
    $result['created'] = time();
    $result['id'] = $this->teamModel->save($result);

    return $result;
  }

  /**
   * Update team info.
   *
   * @param int $id
   *   Team id.
   * @param string $name
   *   Team name.
   *
   * @return mixed
   *   Team info.
   */
  public function updateTeam(int $id, string $name) {
    $result['name'] = $name;
    $result['id'] = $id;
    $result['updated'] = $this->teamModel->update($id, $result);

    return $result;
  }

  /**
   * Delete team.
   *
   * @param int $id
   *   Team id.
   *
   * @return mixed
   *   Number if everything is ok.
   */
  public function deleteTeam(int $id) {
    return $this->teamModel->delete($id);
  }

  /**
   * Get full team info.
   *
   * @param int $id
   *   Team id.
   *
   * @return mixed
   *   Full team info.
   */
  public function retrieveTeam(int $id) {
    $result = $this->teamModel->retrieve($id);

    return $this->teamTasks->groupTeamResult($result)[0];
  }

  /**
   * Get all teams.
   *
   * @return array
   *   Array with all teams.
   */
  public function retrieveAllTeams() {
    $result = $this->teamModel->index();

    return $this->teamTasks->groupTeamResult($result);
  }

  /**
   * Add users to team.
   *
   * @param int $team_id
   *   Team id.
   * @param array $users
   *   Users to add.
   *
   * @return mixed
   *   Some number.
   */
  public function addUsers(int $team_id, array $users) {
    $data = $this->teamTasks->prepareUsersForSave($team_id, $users);
    return $this->teamUserModel->addUsers($data);
  }

  /**
   * Save many teams with users.
   *
   * @param array $data
   *   Array with teams data with users data.
   */
  public function saveAllTeamMulti(array $data) : void {
    foreach ($data as $team) {
      $this->saveAllTeam($team);
    }
  }

  /**
   * Save team with users.
   *
   * @param array $data
   *   Team data with users.
   *
   * @return mixed
   *   Team data.
   */
  public function saveAllTeam(array $data) {
    if (!empty($data['team_id'])) {
      $team_data = $this->updateTeam($data['team_id'], $data['name']);
      $this->deleteAllUsers($data['team_id']);
    }
    else {
      $team_data = $this->saveTeam($data['name']);
    }

    $this->addUsers($team_data['id'], $data);

    return $team_data;
  }

  /**
   * Update users in team. Remove previous users.
   *
   * @param int $team_id
   *   Team id.
   * @param array $users
   *   Users info.
   *
   * @return mixed
   *   Some data.
   */
  public function updateUsers(int $team_id, array $users) {
    $this->teamUserModel->deleteAllUsers($team_id);
    $data = $this->teamTasks->prepareUsersForSave($team_id, $users);

    return $this->teamUserModel->addUsers($data);
  }

  /**
   * Delete all users from team.
   *
   * @param int $team_id
   *   Team id.
   *
   * @return $this|bool|int
   *   Some number if deleted.
   */
  public function deleteAllUsers(int $team_id) {
    return $this->teamUserModel->deleteAllUsers($team_id);
  }

  /**
   * Delete users from team by id.
   *
   * @param int $team_id
   *   Team id.
   * @param array $users_ids
   *   Users ids.
   *
   * @return mixed
   *   Some number if deleted.
   */
  public function deleteUsers(int $team_id, array $users_ids) {
    return $this->teamUserModel->deleteUsers($team_id, $users_ids);
  }

  /**
   * Assign team to request.
   *
   * @param int $nid
   *   Id of the request.
   * @param array $data
   *   Array with team data.
   *
   * @return mixed
   *   TRUE or error.
   *
   * @throws \Exception
   */
  public function assignTeamToRequest(int $nid, array $data) {
    $move_request = new MoveRequest($nid);
    $saved_request = $move_request->update2($data);
    $new_crew = $data['request_data']['value']['crews'] ?? [];
    $old_crew = $move_request->retrieve()['request_data']['value']['crews'] ?? [];

    if ($saved_request) {
      $team_sub_actions = new TeamSubAction($this->teamTasks);
      $team_changes = $team_sub_actions->teamChanges($new_crew, $old_crew);

      if ($team_changes) {
        $grouped_uids = $this->teamTasks->groupAllUids($team_changes);
        $team_sub_actions->sendEmails($nid, $grouped_uids);
      }
    }

    return $saved_request;
  }

  /**
   * Check if team assigned to request.
   *
   * @param int $team_id
   *   Team id.
   *
   * @return mixed
   *   Must be array with requests id.
   */
  public function isTeamAssigned(int $team_id) {
    return $this->teamRequestModel->isTeamAssigned($team_id);
  }

  /**
   * Check if all users from team is available.
   *
   * @param array $uids
   *   Users ids.
   * @param string $date
   *   Date. 2018-02-20.
   * @param string $start_time
   *   Time. 08:00 PM.
   * @param int $nid_to_exclude
   *   Request if for exclude.
   * @param int $type
   *   Delivery or pickup.
   *
   * @return array
   *   Array with busy users ids.
   */
  public function getNotAvailableUsers(array $uids, string $date, string $start_time, int $nid_to_exclude, int $type) : array {
    $not_available_users = [];

    $date_from = $date . ' 00:00:00';
    $date_to = $date . ' 23:59:59';
    $start_time_array = $this->teamTasks::prepareStartTime($start_time);
    $users = $this->teamModel->getUsersByDateByTimeByUids($uids, $date_from, $date_to, $start_time_array, $nid_to_exclude);
    if ($users) {
      $users = $this->teamTasks->groupCompletedFlag($users);
      foreach ($users as $item) {
        if ($this->isFlatrate($item)) {
          $not_available_users = $this->getFlatrateBusyUsers($date, $start_time_array, $item, $uids);
        }
        else {
          if (!$this->teamTasks->isDateAndTimeAvailable($date, $start_time_array, $item, $type)) {
            if (in_array($item['field_helper_target_id'], $uids)) {
              $not_available_users[$item['field_helper_target_id']] = [
                'uid' => $item['field_helper_target_id'],
                'first_name' => $item['first_name_helper_field_user_first_name_value'],
                'last_name' => $item['last_name_helper_field_user_last_name_value'],
                'nid' => $item['nid'],
                'role' => 'helper',
              ];
            }

            if (in_array($item['field_foreman_target_id'], $uids)) {
              $not_available_users[$item['field_foreman_target_id']] = [
                'uid' => $item['field_foreman_target_id'],
                'first_name' => $item['field_user_first_name_value'],
                'last_name' => $item['field_user_last_name_value'],
                'nid' => $item['nid'],
                'role' => 'foreman',
              ];
            }
          }
        }
      }
    }

    return !empty($not_available_users) ? array_values($not_available_users) : $not_available_users;
  }

  /**
   * Get all work time for users by period.
   *
   * @param string $date_from
   *   Date string.
   * @param string $date_to
   *   Date string.
   *
   * @return array
   *   Array with users.
   */
  public function getWorkTimeFromPayroll(string $date_from, string $date_to) : array {
    $result = [];
    $payroll_instance = new Payroll();
    $cache = $payroll_instance->getPayrollCache($date_from, $date_to, 'cache_by_user');
    if ($cache) {
      foreach ($cache as $item) {
        $users_payroll = unserialize($item);
        foreach ($users_payroll as $uid => $user_payroll) {
          $result[$uid] = [
            'uid' => $uid,
            'work_time' => isset($result[$uid]) ? $result[$uid]['work_time'] + $user_payroll['totals']['hours'] : $user_payroll['totals']['hours'] ?? 0,
          ];
        }
      }
    }

    return !empty($result) ? array_values($result) : [];
  }

  /**
   * Check if request is flatrate.
   *
   * @param array $item
   *   $item.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isFlatrate(array $item) {

    return $item['field_move_service_type_value'] == RequestServiceType::FLAT_RATE;
  }

  /**
   * @param $date
   * @param $start_time_array
   * @param $item
   * @param $users
   */
  public function getFlatrateBusyUsers($date, $start_time_array, $item, $users) {
    $not_available_users = [];
    $pickup_foremans = $pickup_helpers = $delivery_foremans = $delivery_helpers = [];

    if (!$this->teamTasks->isPickupAvailable($date, $start_time_array, $item)) {
      $pickup_foremans = $this->teamModel->getFlatratePickupForemansFromUids($item['nid'], $users);
      $pickup_helpers = $this->teamModel->getFlatratePickupHelpersFromUids($item['nid'], $users);
    }

    if (!$this->teamTasks->isDeliveryAvailable($date, $start_time_array, $item)) {
      $delivery_foremans = $this->teamModel->getFlatrateDeliveryForemansFromUids($item['nid'], $users);
      $delivery_helpers = $this->teamModel->getFlatrateDeliveryHelpresFromUids($item['nid'], $users);
    }

    $users = array_merge($pickup_foremans, $pickup_helpers, $delivery_foremans, $delivery_helpers);

    if (!empty($users)) {
      foreach ($users as $user) {
        if ($item['field_helper_target_id'] == $user) {
          $not_available_users[$item['field_helper_target_id']] = [
            'uid' => $item['field_helper_target_id'],
            'first_name' => $item['first_name_helper_field_user_first_name_value'],
            'last_name' => $item['last_name_helper_field_user_last_name_value'],
            'nid' => $item['nid'],
            'role' => 'helper',
          ];
        }

        if ($item['field_foreman_target_id'] == $user) {
          $not_available_users[$item['field_foreman_target_id']] = [
            'uid' => $item['field_foreman_target_id'],
            'first_name' => $item['field_user_first_name_value'],
            'last_name' => $item['field_user_last_name_value'],
            'nid' => $item['nid'],
            'role' => 'foreman',
          ];
        }
      }
    }

    return $not_available_users;
  }

}
