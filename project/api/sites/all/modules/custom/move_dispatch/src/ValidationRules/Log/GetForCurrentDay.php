<?php

namespace Drupal\move_dispatch\ValidationRules\Log;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class GetForCurrentDay.
 *
 * @package Drupal\move_dispatch\ValidationRules\Log
 */
class GetForCurrentDay implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'current_page' => ['int'],
    ];
  }

}
