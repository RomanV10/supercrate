<?php

namespace Drupal\move_dispatch\ValidationRules\Team;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class UsersAddUpdate.
 *
 * @package Drupal\move_dispatch\ValidationRules\Team
 */
class UsersAddUpdate implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'team_id' => ['required', 'int'],
      'users' => ['array'],
      'users.foreman' => ['array'],
      'users.foreman.uid' => ['int'],
      'users.helpers' => ['present', 'array'],
      'users.helpers.*' => ['int', 'min:1', 'distinct'],
    ];
  }

}
