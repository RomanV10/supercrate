<?php

namespace Drupal\move_dispatch\ValidationRules\Team;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class IsTeamAvailable.
 *
 * @package Drupal\move_dispatch\ValidationRules\Team
 */
class IsTeamAvailable implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'users_ids' => ['required', 'array'],
      'users_ids.*' => ['required', 'int'],
      'date' => ['required', 'string'],
      'start_time' => ['required', 'string', 'min:8'],
      'nid_to_exclude' => ['required', 'int'],
      'type' => ['int'],
    ];
  }

}
