<?php

namespace Drupal\move_dispatch\ValidationRules\Log;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class GetByNid.
 *
 * @package Drupal\move_dispatch\ValidationRules\Log
 */
class GetByNid implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'nid' => ['required', 'int', 'dexist:move_dispatch_log,nid'],
      'current_page' => ['int'],
    ];
  }

}
