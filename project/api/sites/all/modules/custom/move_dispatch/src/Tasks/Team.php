<?php

namespace Drupal\move_dispatch\Tasks;

use Drupal\move_services_new\Util\enum\RequestCompanyFlags;
use Drupal\move_services_new\Util\enum\Roles;

/**
 * Class Team.
 *
 * @package Drupal\move_dispatch\Tasks
 */
class Team {
  const PICKUP = 0;
  const DELIVERY = 1;

  /**
   * Explode dates and compare what come from request with db dates.
   *
   * @param string $date
   *   Date.
   * @param array $item
   *   Item.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isDatesEquals(string $date, array $item) : bool {
    $result = FALSE;

    // Explode date and time to get only date.
    $date_array = explode(' ', $item['field_date_value']);
    $delivery_date = explode(' ', $item['field_delivery_date_value']);

    if ($date_array[0] == $date || $delivery_date[0] == $date) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Check if start time and company flags equals.
   *
   * @param string $date
   *   Date for check.
   * @param array $start_time_array
   *   Array with start time.
   * @param array $item
   *   Item with time.
   * @param int $type
   *   Delivery or pickup.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isDateAndTimeAvailable(string $date, array $start_time_array, array $item, int $type = 0) : bool {
    $result = TRUE;

    if (!$this->isPickupAvailable($date, $start_time_array, $item) || !$this->isDeliveryAvailable($date, $start_time_array, $item)) {
      $result = FALSE;
    }

    return $result;
  }

  /**
   * Check pickup available.
   *
   * @param string $date
   *   Date for check.
   * @param array $start_time_array
   *   Array with start time.
   * @param array $item
   *   Array with fields for check.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isPickupFlatrateUserAvailable(string $date, array $start_time_array, array $item) : bool {
    $result = TRUE;
    if (!$this->isPickupAvailable($date, $start_time_array, $item))

    return $result;
  }

  /**
   * Check delivery available.
   *
   * @param string $date
   *   Date for check.
   * @param array $start_time_array
   *   Array with start time.
   * @param array $item
   *   Array with fields for check.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isDeliveryFlatrateUserAvailable(string $date, array $start_time_array, array $item) : bool {
    $result = TRUE;

    $completed_flag = array_intersect($item['field_company_flags_value'], [RequestCompanyFlags::COMPLETED, RequestCompanyFlags::DELIVERY_COMPLETED]);
    $delivery_date = explode(' ', $item['field_delivery_date_value']);

    if (in_array($item['field_delivery_start_time1_value'], $start_time_array) && empty($completed_flag) && $delivery_date[0] == $date) {
      $result = FALSE;
    }

    return $result;
  }

  /**
   * Check pickup available.
   *
   * @param string $date
   *   Date for check.
   * @param array $start_time_array
   *   Array with start time.
   * @param array $item
   *   Array with fields for check.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isPickupAvailable(string $date, array $start_time_array, array $item) : bool {
    $result = TRUE;

    // Explode date and time to get only date.
    $date_array = explode(' ', $item['field_date_value']);
    $completed_flag = array_intersect($item['field_company_flags_value'], [RequestCompanyFlags::COMPLETED, RequestCompanyFlags::PICKUP_COMPLETED]);

    if (in_array($item['field_actual_start_time_value'], $start_time_array) && empty($completed_flag) && $date_array[0] == $date) {
      $result = FALSE;
    }

    return $result;
  }

  /**
   * Check delivery available.
   *
   * @param string $date
   *   Date for check.
   * @param array $start_time_array
   *   Array with start time.
   * @param array $item
   *   Array with fields for check.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isDeliveryAvailable(string $date, array $start_time_array, array $item) : bool {
    $result = TRUE;

    $completed_flag = array_intersect($item['field_company_flags_value'], [RequestCompanyFlags::COMPLETED, RequestCompanyFlags::DELIVERY_COMPLETED]);
    $delivery_date = explode(' ', $item['field_delivery_date_value']);

    if (in_array($item['field_delivery_start_time1_value'], $start_time_array) && empty($completed_flag) && $delivery_date[0] == $date) {
      $result = FALSE;
    }

    return $result;
  }

  /**
   * We need this spike. I database can be stored 8:00 AM or 08:00 AM.
   *
   * @param string $start_time
   *   Start time.
   *
   * @return array
   *   Array with start time.
   */
  public static function prepareStartTime(string $start_time) : array {
    $must_be_count = 8;
    $word_count = strlen($start_time);
    $result[] = $start_time;
    $first_letter = substr($start_time, 0, 1);
    if ($must_be_count == $word_count && $first_letter == '0') {
      $result[] = substr($start_time, 1);
    }

    return $result;
  }

  /**
   * Prepare users array for save.
   *
   * @param int $team_id
   *   Team id.
   * @param array $users
   *   Array with users.
   *
   * @return array
   *   Modified users.
   */
  public function prepareUsersForSave(int $team_id, array $users) : array {
    $result = [];

    if ($users['foreman']) {
      $result[] = [
        'uid' => $users['foreman']['uid'],
        'role' => Roles::FOREMAN,
        'team_id' => $team_id,
        'created' => time(),
      ];
    }

    if ($users['helpers']) {
      foreach ($users['helpers'] as $helper_uid) {
        $result[] = [
          'uid' => $helper_uid,
          'role' => Roles::HELPER,
          'team_id' => $team_id,
          'created' => time(),
        ];
      }
    }

    return $result;
  }

  /**
   * Group users fields by request id.
   *
   * @param array $users
   *   Users from query.
   *
   * @return array
   *   Array with users.
   */
  public function groupCompletedFlag(array $users) : array {

    foreach ($users as $user) {
      $company_flags[$user['nid']][$user['field_company_flags_value']] = $user['field_company_flags_value'];
    }

    if (!empty($company_flags)) {
      foreach ($users as $key => $user) {
        $users[$key]['field_company_flags_value'] = $company_flags[$user['nid']];
      }
    }

    return $users;
  }

  /**
   * Group result for response.
   *
   * @param array $teams
   *   Array with teams.
   *
   * @return array
   *   Grouped result.
   */
  public function groupTeamResult(array $teams) : array {

    $added_users = [];
    $result = [];

    foreach ($teams as $team) {
      if (!isset($added_users[$team->id][$team->uid])) {
        $user_full_name = $team->field_user_first_name_value . ' ' . $team->field_user_last_name_value;
        $result[$team->id]['team_id'] = $team->id;
        $result[$team->id]['name'] = $team->name;
        $result[$team->id]['created'] = $team->created;

        $user = [
          'uid' => $team->uid,
          'role' => $team->role,
          'name' => $user_full_name,
          'user_real_role' => $team->machine_name,
        ];

        switch ($team->role) {
          case Roles::FOREMAN:
            $result[$team->id]['foreman'] = $user;
            break;

          case Roles::HELPER:
            $result[$team->id]['helpers'][] = $user;
            break;
        }

        $added_users[$team->id][$team->uid] = $team->uid;
      }
    }

    return array_values($result);
  }

  /**
   * Check if pickup was exist in old or new crew.
   *
   * @param array $new_team
   *   New crew array.
   * @param array $old_team
   *   Old crew array.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isPickupCrewExist(array $new_team, array $old_team) : bool {
    return isset($new_team['pickedUpCrew']) || isset($old_team['pickedUpCrew']);
  }

  /**
   * Check if pickup was exist in old or new crew.
   *
   * @param array $new_team
   *   New crew array.
   * @param array $old_team
   *   Old crew array.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isDeliveryCrewExists(array $new_team, array $old_team) : bool {
    return isset($new_team['deliveryCrew']) || isset($old_team['deliveryCrew']);
  }

  /**
   * Check if base crew was exist in old and new crew.
   *
   * @param array $new_team
   *   New team.
   * @param array $old_team
   *   Old team.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isBaseCrewExist(array $new_team, array $old_team) : bool {
    return isset($new_team['baseCrew']) || isset($old_team['baseCrew']);
  }

  /**
   * Check if additional crew exist.
   *
   * @param array $new_team
   *   New team.
   * @param array $old_team
   *   Old team.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isAdditionalCrewExist(array $new_team, array $old_team) : bool {
    return isset($new_team['additionalCrews']) || isset($old_team['additionalCrews']);
  }

  /**
   * Get pickup crew changes.
   *
   * @param array $new_team
   *   New team.
   * @param array $old_team
   *   Old team.
   *
   * @return array
   *   Array with crew diff.
   */
  public function getPickupCrewChanges(array $new_team, array $old_team) : array {
    $result = [];

    $new_helpers = !empty($new_team['helpers']) ? $this->prepareHelpers($new_team['helpers']) : [];
    $old_helpers = !empty($old_team['helpers']) ? $this->prepareHelpers($old_team['helpers']) : [];

    if ($helpers_diff = $this->crewHelpersDiff($new_helpers, $old_helpers)) {
      $result['helpers'] = $helpers_diff;
    }

    if ($foreman_diff = $this->createForemanDiff($new_team['foreman'] ?? 0, $old_team['foreman'] ?? 0)) {
      $result['foreman'] = $foreman_diff;
    }

    return $result;
  }

  /**
   * Get delivery crew changes.
   *
   * @param array $new_team
   *   New team.
   * @param array $old_team
   *   Old team.
   *
   * @return array
   *   Array with crew diff.
   */
  public function getDeliveryCrewChanges(array $new_team, array $old_team) : array {
    $result = [];

    $new_helpers = !empty($new_team['helpers']) ? $this->prepareHelpers($new_team['helpers']) : [];
    $old_helpers = !empty($old_team['helpers']) ? $this->prepareHelpers($old_team['helpers']) : [];

    if ($helpers_diff = $this->crewHelpersDiff($new_helpers, $old_helpers)) {
      $result['helpers'] = $helpers_diff;
    }

    if ($foreman_diff = $this->createForemanDiff($new_team['foreman'] ?? 0, $old_team['foreman'] ?? 0)) {
      $result['foreman'] = $foreman_diff;
    }

    return $result;
  }

  /**
   * Base crew changes.
   *
   * @param array $new_team
   *   New team.
   * @param array $old_team
   *   Old team.
   *
   * @return array
   *   Array with crew diff.
   */
  public function baseCrewChanges(array $new_team, array $old_team) : array {
    $result = [];

    $new_helpers = !empty($new_team['baseCrew']['helpers']) ? $this->prepareHelpers($new_team['baseCrew']['helpers']) : [];
    $old_helpers = !empty($old_team['baseCrew']['helpers']) ? $this->prepareHelpers($old_team['baseCrew']['helpers']) : [];

    if ($helpers_diff = $this->crewHelpersDiff($new_helpers, $old_helpers)) {
      $result['helpers'] = $helpers_diff;
    }

    if ($foreman_diff = $this->createForemanDiff($new_team['foreman'] ?? 0, $old_team['foreman'] ?? 0)) {
      $result['foreman'] = $foreman_diff;
    }

    return $result;
  }

  /**
   * Additional crew changes.
   *
   * @param array $new_team
   *   New team.
   * @param array $old_team
   *   Old team.
   *
   * @return array
   *   Array with crew changes.
   */
  public function additionalCrewChanges(array $new_team, array $old_team) : array {
    $result = [];

    $all_new_helpers = [];
    $all_old_helpers = [];

    if (!empty($new_team)) {
      foreach ($new_team as $item) {
        if (!empty($item['helpers'])) {
          $all_new_helpers = array_merge($all_new_helpers, $item['helpers']);
        }
      }
    }

    if (!empty($old_team)) {
      foreach ($old_team as $item) {
        if (!empty($item['helpers'])) {
          $all_old_helpers = array_merge($all_old_helpers, $item['helpers']);
        }
      }
    }

    $new_helpers = !empty($all_new_helpers) ? $this->prepareHelpers($all_new_helpers) : [];
    $old_helpers = !empty($all_old_helpers) ? $this->prepareHelpers($all_old_helpers) : [];

    $helpers_diff = $this->crewHelpersDiff($new_helpers, $old_helpers);

    if ($helpers_diff) {
      $result['helpers'] = $helpers_diff;
    }

    return $result;
  }

  /**
   * Helpers diff.
   *
   * @param array $new_helpers
   *   New helpers array.
   * @param array $old_helpers
   *   Old helpers array.
   *
   * @return array
   *   Array with diff.
   */
  public function crewHelpersDiff(array $new_helpers, array $old_helpers) : array {
    $result = [];

    if (empty($old_helpers) && !empty($new_helpers)) {
      $result['added'] = array_values($new_helpers);
    }

    if (empty($new_helpers) && !empty($old_helpers)) {
      $result['removed'] = array_values($old_helpers);
    }

    if (!empty($old_helpers) && !empty($new_helpers)) {
      if ($added = array_diff($new_helpers, $old_helpers)) {
        $result['added'] = array_values($added);
      }

      if ($removed = array_diff($old_helpers, $new_helpers)) {
        $result['removed'] = array_values($removed);
      }
    }

    return $result;
  }

  /**
   * Filter empty helpers. Make unique.
   *
   * @param array $helpers
   *   Helpers array.
   *
   * @return array
   *   Array with helpers.
   */
  private function prepareHelpers(array $helpers) {
    $helpers = array_filter($helpers);

    if ($helpers) {
      $helpers = array_unique($helpers);
    }

    return $helpers;
  }

  /**
   * Foreman diff.
   *
   * @TODO $new_foreman and $old_foreman mast be int. Make this after front fix.
   * @param mixed $new_foreman
   *   New foreman uid.
   * @param mixed $old_foreman
   *   Old foreman uid.
   *
   * @return array
   *   Array with foreman diff.
   */
  public function createForemanDiff($new_foreman, $old_foreman) : array {
    $result = [];

    if (empty($old_foreman) && !empty($new_foreman)) {
      $result['added'] = $new_foreman;
    }

    if (empty($new_foreman) && !empty($old_foreman)) {
      $result['removed'] = $old_foreman;
    }

    if (!empty($old_foreman) && !empty($new_foreman) && $old_foreman != $new_foreman) {
      $result['added'] = $new_foreman;
      $result['removed'] = $old_foreman;
    }

    return $result;
  }

  /**
   * Group all uids from diff result.
   *
   * @param array $diff_result
   *   Array with diff result.
   *
   * @return array
   *   Array with uids.
   */
  public function groupAllUids(array $diff_result) : array {
    try {
      $result = [];

      $group_foremans = function ($key_name, $diff_item, &$result) {
        if (isset($diff_item['foreman'][$key_name])) {
          $result['foremans'][$key_name][] = $diff_item['foreman'][$key_name];
        }
      };

      $group_helpers = function ($key_name, $diff_item, &$result) {
        if (isset($diff_item['helpers'][$key_name])) {
          if (!isset($result['helpers'][$key_name])) {
            $result['helpers'][$key_name] = [];
          }

          $result['helpers'][$key_name] = array_merge($result['helpers'][$key_name], $diff_item['helpers'][$key_name]);
        }
      };

      foreach ($diff_result as $diff_item) {
        $group_foremans('added', $diff_item, $result);
        $group_foremans('removed', $diff_item, $result);

        $group_helpers('added', $diff_item, $result);
        $group_helpers('removed', $diff_item, $result);
      }
    }
    catch (\Exception $e) {
      watchdog('Assign manager. Diff. Group all id', $e->getMessage(), [], WATCHDOG_CRITICAL);
    }
    finally {
      return $result;
    }
  }

}
