<?php

namespace Drupal\move_dispatch\Models;

use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class Team.
 *
 * @package Drupal\move_dispatch\Models
 */
class Team extends AbstractModel {

  protected $table = 'move_dispatch_team';
  protected $alias = 'team';
  protected $fillable = ['name', 'created'];

  /**
   * Update team.
   *
   * @param int $id
   *   Id of the team.
   * @param array $data
   *   Data for save.
   *
   * @return mixed
   *   Some number.
   */
  public function update(int $id, array $data) {
    $data = $this->filterData($data);
    return db_update($this->table)
      ->fields($data)
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Delete team.
   *
   * @param int $id
   *   Team id.
   *
   * @return $this|bool|int
   *   Delete or not.
   */
  public function delete(int $id) {
    return db_delete($this->table)
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Retrieve team.
   *
   * @param int $id
   *   Id of the team.
   *
   * @return array
   *   Array with full team info.
   */
  public function retrieve(int $id) {
    $query = db_select($this->table, $this->alias);
    $query->leftJoin((new TeamUser())->getTable(), 'user', $this->alias . '.id = user.team_id');

    $query->leftJoin('field_data_field_user_first_name', 'first_name', 'first_name.entity_id = user.uid');
    $query->leftJoin('field_data_field_user_last_name', 'last_name', 'last_name.entity_id = user.uid');

    $query->leftJoin('users_roles', 'user_roles', 'user_roles.uid = user.uid');
    $query->leftJoin('role', 'role', 'user_roles.rid = role.rid');

    $query->fields($this->alias)
      ->fields('user', ['uid', 'role'])
      ->fields('first_name', ['field_user_first_name_value'])
      ->fields('last_name', ['field_user_last_name_value'])
      ->fields('role', ['machine_name'])
      ->condition('team.id', $id);

    return $query->execute()->fetchAll();
  }

  /**
   * Get all teams.
   *
   * @return array
   *   Array with all teams.
   */
  public function index() {
    $query = db_select($this->table, $this->alias);
    $query->leftJoin((new TeamUser())->getTable(), 'user', $this->alias . '.id = user.team_id');

    $query->leftJoin('field_data_field_user_first_name', 'first_name', 'first_name.entity_id = user.uid');
    $query->leftJoin('field_data_field_user_last_name', 'last_name', 'last_name.entity_id = user.uid');

    $query->leftJoin('users_roles', 'user_roles', 'user_roles.uid = user.uid');
    $query->leftJoin('role', 'role', 'user_roles.rid = role.rid');

    $query->fields('team')
      ->fields('user', ['uid', 'role'])
      ->fields('first_name', ['field_user_first_name_value'])
      ->fields('last_name', ['field_user_last_name_value'])
      ->fields('role', ['machine_name']);

    return $query->execute()->fetchAll();
  }

  /**
   * Get users by date and time.
   *
   * @param array $uids
   *   Users ids.
   * @param string $date_from
   *   Date. 2018-02-20 00:00:00.
   * @param string $date_to
   *   Date. 2018-02-20 23:59:59.
   * @param array $start_time_array
   *   Time array. 08:00 PM.
   * @param int $nid_to_exclude
   *   Request to exclude.
   *
   * @return array
   *   Array with busy users ids.
   */
  public function getUsersByDateByTimeByUids(array $uids, string $date_from, string $date_to, array $start_time_array, int $nid_to_exclude) {

    $query = db_select('node', 'n');
    $query->fields('n', ['nid']);
    $query->fields('start_time', ['field_actual_start_time_value']);
    $query->fields('delivery_start_time', ['field_delivery_start_time1_value']);

    $query->fields('move_date', ['field_date_value']);
    $query->fields('delivery_date', ['field_delivery_date_value']);

    $query->fields('helper', ['field_helper_target_id']);
    $query->fields('foreman', ['field_foreman_target_id']);

    $query->fields('company_flags', ['field_company_flags_value']);
    $query->fields('service_type', ['field_move_service_type_value']);

    $query->leftJoin('field_data_field_delivery_start_time1', 'delivery_start_time', 'delivery_start_time.entity_id = n.nid');
    $query->leftJoin('field_data_field_actual_start_time', 'start_time', 'start_time.entity_id = n.nid');
    $query->leftJoin('field_data_field_date', 'move_date', 'move_date.entity_id = n.nid');
    $query->leftJoin('field_data_field_delivery_date', 'delivery_date', 'delivery_date.entity_id = n.nid');
    $query->leftJoin('field_data_field_company_flags', 'company_flags', 'company_flags.entity_id = n.nid');
    $query->leftJoin('field_data_field_approve', 'status', 'status.entity_id = n.nid');
    $query->leftJoin('field_data_field_move_service_type', 'service_type', 'service_type.entity_id = n.nid');

    $query->leftJoin('field_data_field_foreman', 'foreman', 'foreman.entity_id = n.nid');
    $query->leftJoin('field_data_field_helper', 'helper', 'helper.entity_id = n.nid');

    $this->attachFullName($query, 'foreman', 'foreman.field_foreman_target_id');
    $this->attachFullName($query, 'helper', 'helper.field_helper_target_id');

    $time_or = db_or();
    $time_or->condition('field_actual_start_time_value', $start_time_array, 'IN');
    $time_or->condition('field_delivery_start_time1_value', $start_time_array, 'IN');
    $query->condition($time_or);

    $date_or = db_or();
    $date_or->condition('field_date_value', [$date_from, $date_to], 'BETWEEN');
    $date_or->condition('field_delivery_date_value', [$date_from, $date_to], 'BETWEEN');
    $query->condition($date_or);

    $query->condition('n.nid', $nid_to_exclude, '<>');

    $or_user = db_or();
    $or_user->condition('field_helper_target_id', $uids, 'IN');
    $or_user->condition('field_foreman_target_id', $uids, 'IN');

    $query->condition($or_user);
    $query->condition('status.field_approve_value', RequestStatusTypes::CONFIRMED);

    return $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Attach first and last name ro query.
   *
   * @param \SelectQuery $query
   *   SelectQuery.
   * @param string $part_alias_name
   *   Part of field alias.
   * @param string $filed_to_join
   *   Field with alias to join.
   */
  public function attachFullName(\SelectQuery &$query, string $part_alias_name, string $filed_to_join) {
    $first_name_alias = 'first_name_' . $part_alias_name;
    $last_name_alias = 'last_name_' . $part_alias_name;

    $query->leftJoin('field_data_field_user_first_name', $first_name_alias, $first_name_alias . '.entity_id = ' . $filed_to_join);
    $query->leftJoin('field_data_field_user_last_name', $last_name_alias, $last_name_alias . '.entity_id = ' . $filed_to_join);
    $query->fields($first_name_alias, ['field_user_first_name_value']);
    $query->fields($last_name_alias, ['field_user_last_name_value']);
  }


  /**
   * Get flatrate pickup foremans.
   *
   * @param int $nid
   *   Request id.
   * @param array $uids
   *   Users ids.
   *
   * @return mixed
   */
  public function getFlatratePickupForemansFromUids(int $nid, array $uids) {
    return db_select('field_data_field_foreman_pickup', 'foreman')
      ->fields('foreman', ['field_foreman_pickup_target_id'])
      ->condition('entity_id', $nid)
      ->condition('field_foreman_pickup_target_id', $uids)
      ->execute()
      ->fetchCol();
  }


  /**
   * Get flatrate pickup foremans.
   *
   * @param int $nid
   *   Request id.
   * @param array $uids
   *   Users ids.
   *
   * @return mixed
   */
  public function getFlatratePickupHelpersFromUids(int $nid, array $uids) {
    return db_select('field_data_field_helper_pickup', 'foreman')
      ->fields('foreman', ['field_helper_pickup_target_id'])
      ->condition('entity_id', $nid)
      ->condition('field_helper_pickup_target_id', $uids)
      ->execute()
      ->fetchCol();
  }

  /**
   * Get flatrate pickup foremans.
   *
   * @param int $nid
   *   Request id.
   * @param array $uids
   *   Users ids.
   *
   * @return mixed
   */
  public function getFlatrateDeliveryForemansFromUids(int $nid, array $uids) {
    return db_select('field_data_field_foreman_delivery', 'foreman')
      ->fields('foreman', ['field_foreman_delivery_target_id'])
      ->condition('entity_id', $nid)
      ->condition('field_foreman_delivery_target_id', $uids)
      ->execute()
      ->fetchCol();
  }

  /**
   * Get flatrate pickup foremans.
   *
   * @param int $nid
   *   Request id.
   * @param array $uids
   *   Users ids.
   *
   * @return mixed
   */
  public function getFlatrateDeliveryHelpresFromUids(int $nid, array $uids) {
    return db_select('field_data_field_helper_delivery', 'foreman')
      ->fields('foreman', ['field_helper_delivery_target_id'])
      ->condition('entity_id', $nid)
      ->condition('field_helper_delivery_target_id', $uids)
      ->execute()
      ->fetchCol();
  }

}
