<?php

namespace Drupal\move_dispatch\ValidationRules\Log;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class AddMulti.
 *
 * @package Drupal\move_dispatch\ValidationRules\Log
 */
class AddMulti implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'nid' => ['required', 'int'],
      'data' => ['required', 'array'],
      'data.*' => ['required', 'min:3', 'json'],
    ];
  }

}
