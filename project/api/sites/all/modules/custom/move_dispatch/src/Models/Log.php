<?php

namespace Drupal\move_dispatch\Models;

/**
 * Class Log.
 *
 * @package Drupal\move_dispatch\Models
 */
class Log extends AbstractModel {

  protected $table = 'move_dispatch_log';
  protected $alias = 'log';
  protected $fillable = [
    'nid',
    'uid',
    'data',
    'created',
    'created_string',
  ];
  protected $pageSize = 20;

  /**
   * Get all logs by nid.
   *
   * @param int $nid
   *   Node id.
   * @param int $current_page
   *   Current page number.
   *
   * @return array
   *   Array with logs.
   */
  public function getByNid(int $nid, $current_page = 0) {
    $query = db_select($this->table, $this->alias);
    $query->fields($this->alias);
    $query->condition('nid', $nid);

    $clone_query = clone $query;
    $total_count = $clone_query->execute()->rowCount();
    $pages_count = ceil($total_count / $this->pageSize);
    $start_from = ($current_page - 1) * $this->pageSize;

    $this->attachUserName($query);
    $query->range($start_from, $this->pageSize);
    $query->orderBy('created', 'DESC');
    $logs = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    $result['logs'] = $logs;
    $result['current_page'] = $current_page;
    $result['total_count'] = $total_count;
    $result['pages_count'] = $pages_count;

    return $result;
  }

  /**
   * Get all logs by date.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param int $current_page
   *   Current page number.
   *
   * @return array
   *   Array with logs.
   */
  public function getByDate(string $date_from, string $date_to, int $current_page = 1) {
    $query = db_select($this->table, $this->alias);
    $query->fields($this->alias);
    $query->condition('created_string', array($date_from, $date_to), 'BETWEEN');

    $clone_query = clone $query;
    $total_count = $clone_query->execute()->rowCount();
    $pages_count = ceil($total_count / $this->pageSize);
    $start_from = ($current_page - 1) * $this->pageSize;

    $this->attachUserName($query);
    $query->range($start_from, $this->pageSize);
    $query->orderBy('created', 'DESC');

    $logs = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    $result['logs'] = $logs;
    $result['current_page'] = $current_page;
    $result['total_count'] = $total_count;
    $result['pages_count'] = $pages_count;

    return $result;
  }

  /**
   * Attach user name to query.
   *
   * @param \SelectQuery $query
   *   SelectQuery implements.
   */
  private function attachUserName(\SelectQuery &$query) : void {
    $query->leftJoin('field_data_field_user_first_name', 'first_name', 'first_name.entity_id = ' . $this->alias . '.uid');
    $query->leftJoin('field_data_field_user_last_name', 'last_name', 'last_name.entity_id = ' . $this->alias . '.uid');
    $query->fields('first_name', ['field_user_first_name_value']);
    $query->fields('last_name', ['field_user_last_name_value']);
  }

}
