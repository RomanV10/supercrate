<?php

namespace Drupal\move_dispatch\Models;

/**
 * Class AbstractModel.
 */
class AbstractModel {

  protected $table = '';
  protected $alias = '';
  protected $fillable = [];

  /**
   * Save data.
   *
   * @param array $data
   *   Data for save.
   *
   * @return array
   *   Array with needed fields.
   */
  protected function filterData(array $data) {
    return array_intersect_key($data, array_flip($this->fillable));
  }

  /**
   * Save data.
   *
   * @param array $data
   *   Data for save.
   *
   * @return int
   *   Id of the row.
   */
  public function save(array $data) {
    $data = $this->filterData($data);
    return db_insert($this->table)
      ->fields($data)
      ->execute();
  }

  /**
   * Get table name.
   *
   * @return string
   *   Table name.
   */
  public function getTable() : string {
    return $this->table;
  }

}
