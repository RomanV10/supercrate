<?php

namespace Drupal\move_dispatch\ValidationRules\Team;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class UsersDelete.
 *
 * @package Drupal\move_dispatch\ValidationRules\Team
 */
class GetUsersWorkTime implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'date_from' => ['required', 'string', 'min:10', 'date_format:"Y-m-d"'],
      'date_to' => ['required', 'string', 'min:10', 'date_format:"Y-m-d"'],
    ];
  }

}
