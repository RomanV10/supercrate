<?php

namespace Drupal\move_dispatch\Actions;

use Carbon\Carbon;
use Drupal\move_dispatch\Models\Log as LogModel;

/**
 * Class LogActions.
 *
 * @package Drupal\move_dispatch\Actions
 */
class Log {

  private $model;
  private $dateFormat = 'Y-m-d H:i:s';
  private $user;

  /**
   * LogActions constructor.
   */
  public function __construct() {
    global $user;
    $this->model = new LogModel();
    $this->user = $user;
  }

  /**
   * Create log raw in db.
   *
   * @param int $nid
   *   Request id.
   * @param string $data
   *   Log json data.
   *
   * @return mixed
   *   Some number.
   */
  public function create(int $nid, string $data) {
    $date = time();
    $date_string = date($this->dateFormat, $date);

    $data_for_save = [
      'nid' => $nid,
      'data' => $data,
      'uid' => $this->user->uid,
      'created_string' => $date_string,
      'created' => $date,
    ];

    return $this->model->save($data_for_save);
  }

  /**
   * Create many logs in one time.
   *
   * @param int $nid
   *   Request id.
   * @param array $logs
   *   Logs data.
   *
   * @return array
   *   Array with created ids.
   */
  public function createMulti(int $nid, array $logs) : array {
    $result = [];

    foreach ($logs as $log_item) {
      $result[] = $this->create($nid, $log_item);
    }

    return $result;
  }

  /**
   * Get all logs for current day.
   *
   * @param int $current_page
   *   Current page.
   *
   * @return array
   *   Array with logs
   */
  public function getForCurrentDay(int $current_page) {
    $date_from = Carbon::today()->startOfDay()->format($this->dateFormat);
    $date_to = Carbon::today()->endOfDay()->format($this->dateFormat);

    return $this->model->getByDate($date_from, $date_to, $current_page);
  }

  /**
   * Get logs by date.
   *
   * @param string $date_from
   *   Date from log.
   * @param string $date_to
   *   Date to log.
   * @param int $current_page
   *   Current page.
   *
   * @return array
   *   Array with logs.
   */
  public function getByDate(string $date_from, string $date_to, int $current_page) {
    return $this->model->getByDate($date_from, $date_to, $current_page);
  }

  /**
   * Get all logs by nid.
   *
   * @param int $nid
   *   Node id.
   * @param int $current_page
   *   Current page.
   *
   * @return array
   *   Array wit logs.
   */
  public function getByNid(int $nid, $current_page) {
    return $this->model->getByNid($nid, $current_page);
  }

}
