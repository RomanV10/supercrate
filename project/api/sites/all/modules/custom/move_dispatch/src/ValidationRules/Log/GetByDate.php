<?php

namespace Drupal\move_dispatch\ValidationRules\Log;

use Drupal\move_services_new\Validation\ValidationRulesInterface;

/**
 * Class GetByDate.
 *
 * @package Drupal\move_dispatch\ValidationRules\Log
 */
class GetByDate implements ValidationRulesInterface {

  /**
   * Rules validation.
   *
   * @return array|mixed
   *   Array with rules.
   */
  public function rules() {
    return [
      'date_from' => ['required', 'string'],
      'date_to' => ['required', 'string'],
      'current_page' => ['int'],

    ];
  }

}
