<?php

namespace Drupal\move_dispatch\Controllers;

use Drupal\move_dispatch\Actions\Log as LogActions;

/**
 * Class Log.
 *
 * @package Drupal\move_dispatch\Controllers
 */
class Log {

  /**
   * Add many logs.
   *
   * @param int $nid
   *   Request id.
   * @param array $data
   *   Log data.
   *
   * @return array
   *   Array with logs ids.
   */
  public static function addMulti(int $nid, array $data) {
    try {
      $result = static::response((new LogActions())->createMulti($nid, $data));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Create log multi', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Get logs for current day.
   *
   * @param int $current_page
   *   Current page number.
   *
   * @return array
   *   Array with logs.
   */
  public static function getForCurrentDay(int $current_page) {
    try {
      $result = static::response((new LogActions())->getForCurrentDay($current_page));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Get log for current day', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Get all logs by nid.
   *
   * @param int $nid
   *   Request id.
   * @param int $current_page
   *   Current page.
   *
   * @return array
   *   Array with logs info.
   */
  public static function getByNid(int $nid, int $current_page) {
    try {
      $result = static::response((new LogActions())->getByNid($nid, $current_page));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Get all logs for request', $message, array(), WATCHDOG_CRITICAL);
      $result = static::response([], 500, $message);
    }
    finally {
      return $result;
    }
  }

  /**
   * Get logs by date.
   *
   * @param string $date_from
   *   Date from string.
   * @param string $date_to
   *   Date to string.
   * @param int $current_page
   *   Current page number.
   *
   * @return array
   *   Array with logs.
   */
  public static function getByDate(string $date_from, string $date_to, int $current_page) {
    try {
      return static::response((new LogActions())->getByDate($date_from, $date_to, $current_page));
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      watchdog('Get all logs by date', $message, array(), WATCHDOG_CRITICAL);
      return static::response([], 500, $message);
    }
  }

  /**
   * Good response.
   *
   * @param mixed $data
   *   Data for response.
   * @param int $status_code
   *   Status code number.
   * @param string $status_message
   *   Status message.
   *
   * @return array
   *   Array with info.
   */
  public static function response($data, $status_code = 200, $status_message = 'OK') {
    return [
      'status_code' => $status_code,
      'status_message' => $status_message,
      'data' => $data,
    ];
  }

}
