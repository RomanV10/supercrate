<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');

require_once DRUPAL_ROOT . 'includes/bootstrap.inc';

$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

use PHPUnit\Framework\TestCase;
use Drupal\move_dispatch\Tasks\Team as TeamTasks;
use Drupal\move_dispatch\Actions\SubActions\Team as TeamSubAction;

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

/**
 * Class MoveDispatchAssignCrewDeliveryCrew.
 */
class MoveDispatchAssignCrewDeliveryCrew extends TestCase {

  /**
   * Test for adding new crew.
   */
  public function testAddNewCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/delivery_crew/crewDiffAddNewDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews, []);
    $this->assertCount(1, $team_changes);

    $expected = [
      'delivery_crew_diff' =>
        [
          'helpers' => ['added' => ['5901', '5902']],
          'foreman' => ['added' => 5906],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test to remove crew.
   */
  public function testRemoveCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/delivery_crew/crewDiffRemoveDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges([], $crews);
    $this->assertCount(1, $team_changes);

    $expected = [
      'delivery_crew_diff' =>
        [
          'helpers' => ['removed' => ['5901', '5902']],
          'foreman' => ['removed' => 5906],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for change base crew.
   */
  public function testChangeCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/delivery_crew/crewDiffChangeDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'delivery_crew_diff' =>
        [
          'helpers' => [
            'added' => ['5903', '5902'],
            'removed' => ['5988', '5901'],
          ],
          'foreman' => [
            'added' => 5905,
            'removed' => 5904,
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for add new helpers.
   */
  public function testAddNewHelpersCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/delivery_crew/crewDiffAddNewHelpersDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'delivery_crew_diff' =>
        [
          'helpers' => [
            'added' => ['6865', '5908'],
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for remove helpers.
   */
  public function testRemoveHelpersCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/delivery_crew/crewDiffRemoveHelpersDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'delivery_crew_diff' =>
        [
          'helpers' => [
            'removed' => ['6865', '5908'],
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for add foreman.
   */
  public function testAddForemanCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/delivery_crew/crewDiffAddForemanDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'delivery_crew_diff' =>
        [
          'foreman' => [
            'added' => 5905,
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for remove foreman.
   */
  public function testRemoveForemanCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/delivery_crew/crewDiffRemoveForemanDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'delivery_crew_diff' =>
        [
          'foreman' => [
            'removed' => 5905,
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

}
