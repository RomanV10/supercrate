<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');

require_once DRUPAL_ROOT . 'includes/bootstrap.inc';

$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

use PHPUnit\Framework\TestCase;
use Drupal\move_dispatch\Tasks\Team as TeamTasks;
use Drupal\move_dispatch\Actions\SubActions\Team as TeamSubAction;

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

/**
 * Class MoveDispatchAssignPickupDeliveryCrew.
 */
class MoveDispatchAssignPickupDeliveryCrew extends TestCase {

  /**
   * Test to add delivery crew.
   */
  public function testAddDeliveryCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/pickup_delivery_crew/crewDiffAddDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'delivery_crew_diff' =>
        [
          'helpers' => ['added' => ['5901', '5902']],
          'foreman' => ['added' => 5906],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test to add pickup crew.
   */
  public function testAddPickupCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/pickup_delivery_crew/crewDiffAddPickupCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'pickup_crew_diff' =>
        [
          'helpers' => ['added' => ['5901', '5902']],
          'foreman' => ['added' => 5906],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for remove delivery crew.
   */
  public function testRemoveDeliveryCrew() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/pickup_delivery_crew/crewDiffRemoveDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'delivery_crew_diff' =>
        [
          'helpers' => [
            'removed' => ['5901', '5902'],
          ],
          'foreman' => [
            'removed' => 5906,
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for remove delivery crew.
   */
  public function testRemovePickupCrew() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/pickup_delivery_crew/crewDiffRemovePickupCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'pickup_crew_diff' =>
        [
          'helpers' => [
            'removed' => ['5903', '6865', '5908'],
          ],
          'foreman' => [
            'removed' => 5905,
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for remove delivery and pickup crew.
   */
  public function testRemovePickupDeliveryCrew() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/pickup_delivery_crew/crewDiffRemovePickupDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges([], $crews);
    $this->assertCount(2, $team_changes);

    $expected = [
      'pickup_crew_diff' =>
        [
          'helpers' => [
            'removed' => ['5903', '6865', '5908'],
          ],
          'foreman' => [
            'removed' => 5905,
          ],
        ],
      'delivery_crew_diff' =>
        [
          'helpers' => [
            'removed' => ['5901', '5902'],
          ],
          'foreman' => [
            'removed' => 5906,
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for adding delivery and pickup crews.
   */
  public function testAddPickupDeliveryCrew() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/pickup_delivery_crew/crewDiffAddPickupDeliveryCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews, []);
    $this->assertCount(2, $team_changes);

    $expected = [
      'pickup_crew_diff' =>
        [
          'helpers' => [
            'added' => ['5903', '6865', '5908'],
          ],
          'foreman' => [
            'added' => 5905,
          ],
        ],
      'delivery_crew_diff' =>
        [
          'helpers' => [
            'added' => ['5901', '5902'],
          ],
          'foreman' => [
            'added' => 5906,
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

}
