<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');

require_once DRUPAL_ROOT . 'includes/bootstrap.inc';

$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

use PHPUnit\Framework\TestCase;
use Drupal\move_dispatch\Tasks\Team as TeamTasks;
use Drupal\move_dispatch\Actions\SubActions\Team as TeamSubAction;

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

/**
 * Class MoveDispatchAssignAdditionalCrew.
 */
class MoveDispatchAssignAdditionalCrew extends TestCase {

  /**
   * Test for adding new crew.
   */
  public function testAddNewCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/additional_crew/crewDiffAddNewAdditionalCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews, []);
    $this->assertCount(1, $team_changes);

    $expected = [
      'additional_crew_diff' =>
        [
          'helpers' => ['added' => ['5987', '5901']],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test to remove crew.
   */
  public function testRemoveCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/additional_crew/crewDiffRemoveAdditionalCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges([], $crews);
    $this->assertCount(1, $team_changes);

    $expected = [
      'additional_crew_diff' =>
        [
          'helpers' => ['removed' => ['5987', '5901']],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for adding new crew item.
   */
  public function testAddNewCrewItemDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/additional_crew/crewDiffAddNewAdditionalCrewItem.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'additional_crew_diff' =>
        [
          'helpers' => ['added' => ['6001', '6002']],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for adding new crew item.
   */
  public function testRemoveCrewItemDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/additional_crew/crewDiffRemoveAdditionalCrewItem.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'additional_crew_diff' =>
        [
          'helpers' => ['removed' => ['6001', '6002']],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for change crew.
   */
  public function testChangeCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/additional_crew/crewDiffChangeAdditionalCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'additional_crew_diff' =>
        [
          'helpers' => [
            'added' => ['5987', '6001', '6002'],
            'removed' => ['5900', '6003', '6004'],
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for add new helpers.
   */
  public function testAddNewHelpersCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/additional_crew/crewDiffAddNewHelpersAdditionalCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'additional_crew_diff' =>
        [
          'helpers' => [
            'added' => ['5902', '6005'],
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for add helper in one crew.
   */
  public function testAddNewHelpersInOneCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/additional_crew/crewDiffAddNewHelpersInOneAdditionalCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'additional_crew_diff' =>
        [
          'helpers' => [
            'added' => ['5902'],
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test for remove helpers.
   */
  public function testRemoveHelpersCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/additional_crew/crewDiffRemoveHelpersAdditionalCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'additional_crew_diff' =>
        [
          'helpers' => [
            'removed' => ['5902', '6005'],
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

  /**
   * Test to remove helper from one item.
   */
  public function testRemoveHelperInOneItemCrewDiff() {
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_dispatch') . '/tests/data/additional_crew/crewDiffRemoveHelpersInOneAdditionalCrew.json';
    $file_content = file_get_contents($file_path);
    $crews = drupal_json_decode($file_content);

    $team_sub_actions = new TeamSubAction(new TeamTasks());
    $team_changes = $team_sub_actions->teamChanges($crews['new'], $crews['old']);
    $this->assertCount(1, $team_changes);

    $expected = [
      'additional_crew_diff' =>
        [
          'helpers' => [
            'removed' => ['5902'],
          ],
        ],
    ];
    $this->assertEquals($expected, $team_changes);
  }

}
