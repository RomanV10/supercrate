<?php

/**
 * @file
 * Contains install and update functions for this module.
 */

/**
 * Implements hook_schema().
 */
function move_dispatch_schema() {

  $schema['move_dispatch_team'] = array(
    'description' => 'Table to store team lead id',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique table record ID.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'created' => array(
        'type' => 'int',
        'description' => 'The Unix timestamp when the command was created.',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );

  $schema['move_dispatch_team_user'] = array(
    'description' => 'Table to store team lead id',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique table record ID.',
      ),
      'team_id' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Team id',
        'not null' => TRUE,
      ),
      'uid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'User ID',
        'not null' => TRUE,
      ),
      'role' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'User Role',
        'not null' => TRUE,
      ),
      'created' => array(
        'type' => 'int',
        'description' => 'The Unix timestamp when user was added.',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'team_id' => array('team_id'),
      'uid' => array('uid'),
      'role' => array('role'),
    ),
    'foreign keys' => array(
      'fk_team_id' => array(
        'table' => 'move_dispatch_team',
        'columns' => array('team_id' => 'id'),
      ),
    ),
  );

  $schema['move_dispatch_log'] = array(
    'description' => 'Table to store logs for dispatch',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique table record ID.',
      ),
      'nid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'nid',
        'not null' => TRUE,
      ),
      'uid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'User ID',
        'not null' => TRUE,
      ),
      'data' => array(
        'type' => 'blob',
        'description' => 'Logs info',
        'not null' => TRUE,
      ),
      'created_string' => array(
        'type' => 'datetime',
        'mysql_type' => 'datetime',
        'description' => 'The date when log was added.',
      ),
      'created' => array(
        'type' => 'int',
        'description' => 'The Unix when log was added.',
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'nid' => array('nid'),
      'uid' => array('uid'),
      'created' => array('created'),
      'created_string' => array('created_string'),
    ),
  );

  $schema['move_dispatch_team_requests'] = array(
    'description' => 'Table to store added team in requests',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique table record ID.',
      ),
      'team_id' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Team id',
        'not null' => TRUE,
      ),
      'nid' => array(
        'type' => 'int',
        'size' => 'normal',
        'description' => 'Request id',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'team_id' => array('team_id'),
      'nid' => array('nid'),
    ),
    'foreign keys' => array(
      'fk_team_requests' => array(
        'table' => 'move_dispatch_team',
        'columns' => array('id' => 'team_id'),
      ),
    ),
  );

  $schema['move_dispatch_notes'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Unique table record ID.',
      ),
      'date' => array(
        'mysql_type' => 'date',
        'description' => 'Date cache data',
        'not null' => TRUE,
      ),
      'note' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function move_dispatch_install() {
  db_query('
      ALTER TABLE {move_dispatch_team_user} 
      ADD CONSTRAINT {fk_team_id} 
      FOREIGN KEY (team_id) 
      REFERENCES {move_dispatch_team} (id) 
      ON UPDATE CASCADE ON DELETE CASCADE
  ');

  db_query(
    'ALTER TABLE {move_dispatch_team_requests} 
     ADD CONSTRAINT {fk_team_requests} 
     FOREIGN KEY (team_id) 
     REFERENCES {move_dispatch_team} (id) 
     ON UPDATE CASCADE ON DELETE CASCADE
  ');
}

/**
 * Implements hook_uninstall().
 */
function move_dispatch_uninstall() {
  db_query('
    ALTER TABLE {move_dispatch_team}
  ');

  db_query('
    ALTER TABLE {move_dispatch_team_user}
    DROP FOREIGN KEY {fk_team_id}
  ');
  db_query('
    ALTER TABLE {move_dispatch_log}
  ');

  db_query('
    ALTER TABLE {move_dispatch_team_requests}
    DROP FOREIGN KEY {fk_team_requests}
  ');
}

/**
 * Create a move_dispatch_notes table.
 */
function move_dispatch_update_7100() {
  $table_name = 'move_dispatch_notes';
  if (!db_table_exists($table_name)) {
    $schema = move_dispatch_schema();
    db_create_table($table_name, $schema[$table_name]);
  }
}
