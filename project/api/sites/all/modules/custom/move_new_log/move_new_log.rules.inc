<?php

/**
 * @file
 * Includes rules integration provided by the module.
 */

use Drupal\move_new_log\Services\Log;

/**
 * Implements hook_rules_action_info().
 */
function move_new_log_rules_action_info() {
  $actions = array();

  $actions['move_new_log_action'] = array(
    'label' => t('Move New Log class based action'),
    'group' => t('Custom'),
    'parameter' => array(
      'event_type' => array(
        'type' => 'list<text>',
        'label' => t('Event Type'),
        'options list' => 'move_new_log_action_event_list',
      ),
      'obj' => array(
        'type' => array('move_new_log_mail'),
        'label' => t('New entity'),
        'optional' => TRUE,
      ),
    ),
  );

  return $actions;
}

/**
 * Implements hook_rules_event_info().
 */
function move_new_log_rules_event_info() {
  return array(
    'send_mail_movecalc_notify' => array(
      'label' => t('Before sending mail of movecalc_notify'),
      'group' => t('Custom'),
      'variables' => array(
        'message' => array(
          'type' => 'move_new_log_mail',
          'label' => t('The subject message'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_data_info().
 */
function move_new_log_rules_data_info() {
  return array(
    'move_new_log_mail' => array(
      'label' => t('Message'),
      'wrap' => TRUE,
    ),
  );
}

/**
 * Custom rules action.
 *
 * @param string $id
 *   The type of event.
 * @param object $object
 *   The object from action rules.
 */
function move_new_log_action($id, $object) {
  $event = move_new_log_action_event_list();
  $current = $event[reset($id)];

  $find_cc = function () use ($object) {
    $result = '';
    if (!empty($object['params']['cc'])) {
      foreach ($object['params']['cc'] as $cc) {
        $result .= ', "' . $cc . '" ';
      }
    }

    return $result;
  };

  if ($current == 'mail') {
    $message = t('Mail was sent to "@user"@cc. From "@from". Subject: "@subject"',
      array(
        '@subject' => $object['subject'],
        '@from' => $object['from'],
        '@user' => $object['to'],
        '@cc' => $find_cc(),
      )
    );
    $entity_type = $object['params']['entity_type'];
    $data = Log::prepare_log_data('mail', $message);
    $instance = new Log($object['params']['nid'], $entity_type);
    $create_log = $instance->create($data);

    if ($create_log) {
      // Register mail body to new table.
      move_new_log_write_body_mail_log($object, reset($create_log));
    }
  }
}
