<?php
/**
 * @file
 * move_new_log.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function move_new_log_default_rules_configuration() {
  $items = array();
  $items['rules_send_mail_movecalc_notify'] = entity_import('rules_config', '{ "rules_send_mail_movecalc_notify" : {
      "LABEL" : "Send mail new movecalc_notify",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Custom" ],
      "REQUIRES" : [ "move_new_log" ],
      "ON" : { "send_mail_movecalc_notify" : [] },
      "DO" : [
        { "move_new_log_action" : { "event_type" : { "value" : [ "0" ] }, "obj" : [ "message" ] } }
      ]
    }
  }');
  return $items;
}
