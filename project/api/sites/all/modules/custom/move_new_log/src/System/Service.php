<?php

namespace Drupal\move_new_log\System;

use Drupal\move_new_log\Services\Log;

/**
 * Class Service.
 *
 * @package Drupal\move_new_log\System
 */
class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'new_log' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_new_log\System\Service::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_new_log',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_new_log\System\Service::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_new_log',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('retrieve log'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_new_log\System\Service::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_new_log',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('update log'),
            'index' => array(
              'callback' => 'Drupal\move_new_log\System\Service::index',
              'file' => array(
                'type' => 'php',
                'module' => 'move_new_log',
                'name' => 'src/System/Service',
              ),
              'access arguments' => array('administer site configuration'),
            ),
          ),
          'index' => array(
            'callback' => 'Drupal\move_new_log\System\Service::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_new_log',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('param' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('param' => 'entity_type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('retrieve log'),
          ),
        ),
      ),
    );
  }

  public static function create(int $id, int $type, $data = array()) {
    $instance = new Log($id, $type);
    return $instance->create($data);
  }

  public static function retrieve(int $id) {
    $instance = new Log();
    $instance->setId($id);
    return $instance->retrieve();
  }

  public static function update($id, $data) {
    $instance = new Log();
    $instance->setId($id);
    return $instance->update($data);
  }

  public static function index(int $id, int $type) {
    $instance = new Log($id, $type);
    return $instance->index();
  }

}
