<?php

namespace Drupal\move_new_log\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_coupon\Services\CouponUser;

/**
 * Class Log.
 *
 * @package Drupal\move_new_log\Services
 */
class Log extends BaseService {

  private $entityId = NULL;
  private $entityType = NULL;
  private $id = NULL;
  private $move_request = FALSE;

  /**
   * Log constructor.
   *
   * @param int $entity_id
   *   Id of existing entity.
   * @param int $entity_type
   *   Type from EntityTypes.
   */
  public function __construct(int $entity_id = NULL, int $entity_type = NULL) {
    $this->entityId = $entity_id;
    $this->entityType = $entity_type;
    if ($this->entityType !== NULL) {
      $this->checkEntityType();
      $move_request = array(EntityTypes::MOVEREQUEST, EntityTypes::LDREQUEST);
      if (in_array($this->entityType, $move_request) && $this->entityId) {
        $this->move_request = TRUE;
      }
    }
  }

  public function setId(int $id) {
    $this->id = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function create($data = array()) {
    $result = array();

    foreach ($data as $item) {
      $fill_details = function (array $details) : string {
        $this->addIpAddress($details);
        return serialize($details);
      };

      $details = (isset($item['details']) && is_array($item['details'])) ? $fill_details($item['details']) : serialize($item['details']);
      $source = (isset($item['source']) && $item['source']) ? serialize($item['source']) : '';

      $fields = array(
        'entity_id' => $this->entityId,
        'entity_type' => $this->entityType,
        'details' => $details,
        'source' => $source == 'Roman Halavach' ? 'System' : $source,
        'date' => time(),
      );

      $result[] = db_insert('move_logs')
        ->fields($fields)
        ->execute();
    }

    return $result;
  }

  /**
   * Find Real IP.
   *
   * @param array $details
   *   Details.
   */
  private function addIpAddress(array &$details) {
    $ip = $_SERVER['X-Real-IP'] ?? $_SERVER['X-Forwarded-For'] ?? '';

    if (isset($_SERVER['HTTP_USER_AGENT'])) {
      $browser = get_browser(NULL, TRUE);
    }
    foreach ($details as &$detail) {
      $detail['info']['browser'] = [
        'name' => $browser['browser'] ?? 'Undefined',
        'version' => $browser['version'] ?? 'Undefined',
      ];
      $detail['info']['ip'] = $ip;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieve() {
    $result = db_select('move_logs', 'ml')
      ->fields('ml')
      ->condition('ml.id', $this->id)
      ->execute()
      ->fetchAssoc();

    $result['details'] = $result['details'] ? unserialize($result['details']) : '';
    $result['source'] = $result['source'] ? unserialize($result['source']) : '';

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function update($data = array()) {
    $retrieve = $this->retrieve();
    $record = array(
      'id' => $this->id,
      'entity_id' => $retrieve['entity_id'],
      'entity_type' => $retrieve['entity_type'],
    );
    $merge_record = array_merge($record, $data);

    return drupal_write_record('move_logs', $merge_record, 'id');
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {}

  /**
   * Return all logs by entityId and entityType.
   *
   * @return mixed
   *   Array with logs.
   */
  public function index() {
    $types = array(EntityTypes::MOVEREQUEST);
    if ($this->move_request) {
      $types[] = EntityTypes::LDREQUEST;
      $types[] = EntityTypes::LDSIT;
    }
    if ($this->entityType == EntityTypes::STORAGEREQUEST) {
      $types = array();
      $types[] = EntityTypes::STORAGEREQUEST;
    }
    if ($this->entityType == EntityTypes::LDTRIP) {
      $types = array();
      $types[] = EntityTypes::LDTRIP;
    }
    if ($this->entityType == EntityTypes::LDCARRIER) {
      $types = array();
      $types[] = EntityTypes::LDCARRIER;
    }
    if ($this->entityType == EntityTypes::LDSIT) {
      $types = array();
      $types[] = EntityTypes::LDSIT;
    }
    if ($this->entityType == EntityTypes::STORAGE) {
      $types = array();
      $types[] = EntityTypes::STORAGE;
    }
    $result = db_select('move_logs', 'ml')
      ->fields('ml')
      ->condition('ml.entity_id', $this->entityId)
      ->condition('ml.entity_type', $types, 'IN')
      ->execute()
      ->fetchAll();

    foreach ($result as $key => &$value) {
      $value->details = $value->details ? unserialize($value->details) : '';
      $value->source = $value->source ? unserialize($value->source) : '';
      $value->id = (int) $value->id;
      if (isset($value->details[0]['event_type']) && $value->details[0]['event_type'] == 'mail') {
        $moveSalesLogMail = $this->getMoveSalesLogMail($value->id);
        $value->details[0]['body'] = $moveSalesLogMail['text'];
        $value->details[0]['from'] = $moveSalesLogMail['from_send'];
        $value->details[0]['visit'] = $moveSalesLogMail['visit'];
        $value->details[0]['ip'] = $moveSalesLogMail['ip'];
        $value->details[0]['date_viewed'] = $moveSalesLogMail['date_viewed'];
      }
      $value = (array) $value;
    }

    return $result;
  }

  /**
   * Validate entityType.
   */
  private function checkEntityType() {
    if (!EntityTypes::isValidValue($this->entityType)) {
      throw new LogCheckEntityType("Wrong Entity Type.");
    }
  }

  public static function prepare_log_data(string $mess_type, string $message) {
    global $user;
    $data[] = array(
      'details' => array([
        'event_type' => $mess_type,
        'activity' => 'system',
        'title' => ucfirst(preg_replace('/_/', ' ', $mess_type)),
        'text' => array(['text' => $message]),
        'date' => time(),
      ],
      ),
      'source' => CouponUser::getUseNamebyUid($user->uid),
    );

    return $data;
  }

  public function prepareLogOverbooking (string $message) : array {
    global $user;
    $data[] = array(
      'details' => array([
        'event_type' => "overbooking",
        'activity' => 'system',
        'title' => "Overbooking",
        'text' => array(['text' => $message]),
        'date' => time(),
      ],
      ),
      'source' => CouponUser::getUseNamebyUid($user->uid),
    );

    return $data;
  }

  public static function prepare_log_data_parser(string $title, string $message) {
    global $user;
    $data[] = array(
      'details' => array([
        'activity' => 'system',
        'title' => $title,
        'text' => array(['text' => $message]),
        'date' => time(),
      ],
      ),
      'source' => CouponUser::getUseNamebyUid($user->uid),
    );

    return $data;
  }

  /**
   * Get move sales log mail.
   *
   * @param int $id
   *   Movesales log id.
   *
   * @return mixed
   *   Row from movesales_log_mail.
   */
  public function getMoveSalesLogMail(int $id) {
    return db_select('movesales_log_mail', 'm')
      ->fields('m', [])
      ->condition('msl_id', $id)
      ->execute()
      ->fetchAssoc();
  }

  /**
   * Insert cron job info to db.
   *
   * @param $type
   * @param $job
   * @param $data
   */
  public static function addInfoToCronLog($name_cron, $type, $data) {
    try {
      db_insert('move_cron_log')
        ->fields(array(
          'name_cron' => $name_cron,
          'type' => $type,
          'data' => $data,
          'created' => time(),
        ))
        ->execute();
    }
    catch (\Throwable $e) {
      watchdog(WATCHDOG_CRITICAL, "Request not created!");
    }
  }

}

/**
 * Class LogException.
 *
 * @package Drupal\move_new_log\Services
 */
class LogException extends \RuntimeException {

  /**
   * Save message to db.
   */
  public function saveDataBase() {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()} </br> Trace: {$this->getTraceAsString()}";
    watchdog('Log system', $message, array(), WATCHDOG_ERROR);
  }

}

/**
 * Class LogCheckEntityType.
 *
 * @package Drupal\move_services_new\Services
 */
class LogCheckEntityType extends LogException {}
