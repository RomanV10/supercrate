<?php

namespace Drupal\move_centrifugo\Actions;

class Transport extends \phpcent\Transport {

  public function communicate($host, $data) {
    $ch = curl_init("https://$host/api/");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);

    if (self::$safety === Transport::UNSAFE) {
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    }

    $postData = http_build_query($data, '', '&');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    $response = curl_exec($ch);
    $headers = curl_getinfo($ch);
    curl_close($ch);

    if (empty($headers["http_code"]) || ($headers["http_code"] != 200)) {
      throw new \Exception("Response code: "
        . $headers["http_code"]
        . PHP_EOL
        . "Body: "
        . $response
      );
    }

    $answer = json_decode($response, TRUE);

    return $answer;
  }

}
