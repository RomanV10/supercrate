<?php
namespace Drupal\move_centrifugo\Actions;

use phpcent;

/**
 * Class Client.
 *
 * @package Drupal\move_reminders\System
 */
class Centrifugo extends  phpcent\Client{

  /**
   * @var string
   */
  private $namespace;

  /**
   * Centrifugo constructor.
   *
   * @param string $host
   *   Host name.
   */
  public function __construct(string $host = "") {
    $host = !empty($host) ? $host : variable_get('centrifugo_url', 'socket.themoveboard.com:9095');

    parent::__construct($host);

    $this->setSecret(variable_get('centrifugo_token', ''));
    $this->setTransport(new Transport());
    $this->namespace = variable_get('centrifugo_namespace', '');
  }

  /**
   * Publish data to centrifugo.
   *
   * @param string $channel
   *   Channel name.
   * @param $data
   *   Data for save.
   *
   * @return mixed
   */
  public function publish($channel, $data = []) {
    $channel =  $this->namespace . ':' . $channel;

    try {
      return $this->send("publish", ["channel" => $channel, "data" => $data]);
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("Centrifugo publish", $error_text, array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Get all users from channel.
   *
   * @param string $channel
   *   Chanel name.
   *
   * @return mixed
   */
  public function presence($channel)
  {
    $channel =  $this->namespace . ':' . $channel;

    try {
      return $this->send("presence", ["channel" => $channel]);
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("Centrifugo presence", $error_text, array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Send message into multiple channels. data is an actual information you want to send into channel.
   *
   * @param array $channels
   *   Channels.
   * @param array $data
   *   Data to send.
   *
   * @return mixed.
   */
  public function broadcast($channels, $data)
  {
    try {
      $channels = array_map(function($channel) { return $this->namespace . ':' . $channel; }, $channels);

      return
        $this->send(
          "broadcast",
          ["channels" => $channels, "data" => $data]
        );
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("Centrifugo broadcast", $error_text, array(), WATCHDOG_ERROR);
    }

  }

  /**
   * Unsubscribe user with certain ID from channel.
   *
   * @param string $channel
   *   Channel.
   * @param int $uid
   *   User id.
   *
   * @return mixed
   */
  public function unsubscribe($channel, $uid)
  {
    $channel =  $this->namespace . ':' . $channel;

    try {
      return $this->send("unsubscribe", ["channel" => $channel, "user" => $uid]);
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("Centrifugo unsubscribe", $error_text, array(), WATCHDOG_ERROR);
    }
  }

}
