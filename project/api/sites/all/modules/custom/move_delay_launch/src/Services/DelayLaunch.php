<?php

namespace Drupal\move_delay_launch\Services;

use Drupal\move_delay_launch\Services\Strategy\ArrivyAssignForeman;
use Drupal\move_delay_launch\Services\Strategy\ArrivyUnassignForeman;
use Drupal\move_delay_launch\Services\Strategy\AssignTeamSendEmailToForemans;
use Drupal\move_delay_launch\Services\Strategy\AssignTeamSendEmailToHelpers;
use Drupal\move_delay_launch\Services\Strategy\CommentsDelete;
use Drupal\move_delay_launch\Services\Strategy\CreateRequestSendMail;
use Drupal\move_delay_launch\Services\Strategy\GoogleCalendarMoveRequestSaveEvent;
use Drupal\move_delay_launch\Services\Strategy\GoogleCalendarMoveRequestUpdateEvent;
use Drupal\move_delay_launch\Services\Strategy\PayrollContractInfo;
use Drupal\move_delay_launch\Services\Strategy\ReviewSend;
use Drupal\move_delay_launch\Services\Strategy\SendArrivyForemanUpdate;
use Drupal\move_delay_launch\Services\Strategy\SendEmail;
use Drupal\move_delay_launch\Services\Strategy\SendReceiptsToQuickbooks;
use Drupal\move_delay_launch\Services\Strategy\SendReceiptToQbAfterPayment;
use Drupal\move_delay_launch\Services\Strategy\SmsActionCreateClient;
use Drupal\move_delay_launch\Services\Strategy\SmsActionJobConfirmed;
use Drupal\move_delay_launch\Services\Strategy\SmsActionJobDone;
use Drupal\move_delay_launch\Services\Strategy\SmsActionSend;
use Drupal\move_delay_launch\Services\Strategy\SpeedyCreate;
use Drupal\move_delay_launch\Services\Strategy\UpdateCache;
use Drupal\move_delay_launch\Services\Strategy\UpdatePayrollCacheByBlockUser;
use Drupal\move_delay_launch\Services\Strategy\UpdaterApi;
use Drupal\move_delay_launch\Services\Strategy\UpdateRequestSendMail;
use Drupal\move_delay_launch\Services\Strategy\UpdateStatisticsForDay;
use Drupal\move_delay_launch\Services\Strategy\SendEmailAfterContractSubmit;
use Drupal\move_delay_launch\Services\Strategy\SendRequestToArrivyIfRequestUpdated;
use Drupal\move_delay_launch\Services\Strategy\SmsActionNotConfirmed;
use Drupal\move_delay_launch\Services\Strategy\SmsActionCreateRequest;
use Drupal\move_delay_launch\Services\Strategy\SmsActionJobNotConfirmed;
use Drupal\move_delay_launch\Util\enum\TaskType;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request as GuzzleRequest;

/**
 * Class DelayLaunch.
 *
 * @package Drupal\move_delay_launch\Services
 */
class DelayLaunch {

  private const URI_CREATE_COMPANY = '/create_company';
  private const URI_SEND_TASK = '/execute_task';
  private const REMOTE_NAME = 'test';
  private const REMOTE_PASSWORD = '8LbQ2SDqhxAL9FVm';

  /**
   * DelayLaunch constructor.
   */
  public function __construct() {}

  /**
   * Execute task remotely.
   *
   * @param int $task_id
   *   Id of the task.
   *
   * @throws \Exception
   */
  public function executeTask(int $task_id) {
    try {
      $task = db_select('move_delay_task', 'mdt')
        ->fields('mdt')
        ->condition('task_id', $task_id)
        ->execute()
        ->fetchObject();

      if (isset($task->data)) {
        $data = unserialize($task->data);
      }
      else {
        return;
      }

      switch ($task->type) {
        case TaskType::SEND_EMAIL:
          SendEmail::execute($data);
          break;

        case TaskType::UPDATE_CACHE:
          UpdateCache::execute($data);
          break;

        case TaskType::REVIEW_SEND:
          ReviewSend::execute($data);
          break;

        case TaskType::CREATE_REQUEST_SEND_MAIL:
          CreateRequestSendMail::execute($data);
          break;

        case TaskType::UPDATE_REQUEST_SEND_MAIL:
          UpdateRequestSendMail::execute($data);
          break;

        case TaskType::COMMENTS_DELETE:
          CommentsDelete::execute($data);
          break;

        case TaskType::UPDATER_API:
          UpdaterApi::execute($data);
          break;

        case TaskType::UPDATE_PAYROLL_CACHE_BY_BLOCK_USER:
          UpdatePayrollCacheByBlockUser::execute($data);
          break;

        case TaskType::SEND_REQUEST_TO_ARRIVY_WHEN_FOREMAN_UPDATE:
          SendArrivyForemanUpdate::execute($data);
          break;

        case TaskType::GOOGLE_CALENDAR_MOVE_REQUEST_CREATE_EVENT:
          GoogleCalendarMoveRequestSaveEvent::execute($data);
          break;

        case TaskType::GOOGLE_CALENDAR_MOVE_REQUEST_UPDATE_EVENT:
          GoogleCalendarMoveRequestUpdateEvent::execute($data);
          break;

        case TaskType::ASSIGN_TEAM_SEND_EMAIL_TO_FOREMANS:
          AssignTeamSendEmailToForemans::execute($data);
          break;

        case TaskType::ASSIGN_TEAM_SEND_EMAIL_TO_HELPERS:
          AssignTeamSendEmailToHelpers::execute($data);
          break;

        case TaskType::UPDATE_STATISTICS_FOR_DAY:
          UpdateStatisticsForDay::execute($data);
          break;

        case TaskType::SEND_EMAIL_CONTRACT_SUBMIT:
          SendEmailAfterContractSubmit::execute($data);
          break;

        case TaskType::SEND_RECEIPTS_TO_QUICKBOOKS:
          SendReceiptsToQuickbooks::execute($data);
          break;

        case TaskType::ARRIVY_ASSIGN_FOREMAN:
          ArrivyAssignForeman::execute($data);
          break;

        case TaskType::ARRIVY_UNASSIGN_FOREMAN:
          ArrivyUnassignForeman::execute($data);
          break;

        case TaskType::SPEEDY_CREATE:
          SpeedyCreate::execute($data);
          break;

        case TaskType::SEND_RECEIPT_TO_QB_AFTER_PAYMENT:
          SendReceiptToQbAfterPayment::execute($data);
          break;

        case TaskType::SMS_ACTION_CREATE_REQUEST:
          SmsActionCreateRequest::execute($data);
          break;

        case TaskType::SEND_REQUEST_TO_ARRIVY_IF_REQUEST_UPDATED:
          SendRequestToArrivyIfRequestUpdated::execute($data);
          break;

        case TaskType::PAYROLL_CONTRACT_INFO:
          PayrollContractInfo::execute($data);
          break;

        case TaskType::SMS_ACTION_JOB_NOT_CONFIRMED:
          SmsActionJobNotConfirmed::execute($data);
          break;

        case TaskType::SMS_ACTION_JOB_CONFIRMED:
          SmsActionJobConfirmed::execute($data);
          break;

        case TaskType::SMS_ACTION_JOB_DONE:
          SmsActionJobDone::execute($data);
          break;

        case TaskType::SMS_NEW_CLIENT:
          SmsActionCreateClient::execute($data);
          break;

        case TaskType::SMS_SEND:
          SmsActionSend::execute($data);
          break;

        default:
          break;
      }
    }
    catch (\Throwable $e) {
      $message = "Error when create DL task {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Delay Launch', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      $this->deleteTask($task_id);
    }
  }

  /**
   * Delete task from database.
   *
   * @param int $task_id
   *   Task id in db.
   */
  private function deleteTask(int $task_id) {
    db_delete('move_delay_task')
      ->condition('task_id', $task_id)
      ->execute();
  }

  /**
   * Create task to execute and send asynchronously to realtime java server.
   *
   * @param int $taskType
   *   The enum TaskType.
   * @param array $data
   *   Data needed for execute task further.
   *
   * @throws \Throwable
   */
  public function createTask($taskType, $data) : void {
    try {
      $record = array(
        'data' => serialize($data),
        'type' => $taskType,
      );

      $task_id = db_insert('move_delay_task')
        ->fields($record)
        ->execute();

      if (!variable_get("java_token")) {
        $this->registerCompany();
      }

      $environments = (array) variable_get('environment', array());
      $allowed = array('delay_java', 'production');

      if (in_array($environments['default'], $allowed)) {
        $param = array(
          'companyKey' => variable_get('java_token'),
          'taskId' => $task_id,
          'taskType' => $taskType,
        );
        $this->curlPostAsync($param);
      }
      else {
        $this->sendToQueue($task_id);
      }
    }
    catch (\Throwable $e) {
      $message = $e->getMessage();
      $stack_trace = $e->getTraceAsString();
      watchdog('Create DelayLaunch task\'s', "Message: $message; StackTrace: $stack_trace", array(), WATCHDOG_CRITICAL);
    }
  }

  /**
   * Send data to java soft.
   *
   * @param array $data
   *   Array with params.
   */
  private function curlPostAsync(array $data) : void {
    $uri = $_ENV['JAVA_URL'] . self::URI_SEND_TASK;
    try {
      $client = new GuzzleClient();
      $headers = ['content-type' => 'application/json'];
      // Send an asynchronous request.
      $request = new GuzzleRequest('POST', $uri, $headers, json_encode($data));
      $promise = $client->sendAsync($request);
      $promise->wait();
    }
    catch (RequestException $e) {
      watchdog('delay_launch', $e->getMessage(), array(), WATCHDOG_ERROR);
      $this->sendToQueue($data['taskId']);
    }
  }

  /**
   * Write taskId for execute queue.
   *
   * @param int $taskId
   *   Id of the task.
   */
  private function sendToQueue(int $taskId) {
    $queue = \DrupalQueue::get('move_delay_launch_execute_task');
    $queue->createQueue();
    $queue->createItem($taskId);
  }

  /**
   * Register Company in Java Application.
   */
  public function registerCompany() : bool {
    $result = FALSE;
    $host = variable_get('drupal_url', 'http://api.movecalc.local');
    $port = variable_get('drupal_port', 80);

    $params = array(
      'url' => $_ENV['JAVA_URL'] . self::URI_CREATE_COMPANY,
      'post_fields' => array(
        'name' => self::REMOTE_NAME,
        'pass' => self::REMOTE_PASSWORD,
        'host' => $host,
        'port' => $port,
      ),
    );

    $data_string = json_encode($params['post_fields']);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
    curl_setopt($ch, CURLOPT_URL, $params['url']);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data_string),
    ));
    $response = curl_exec($ch);
    $error = curl_error($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($http_code == 200) {
      variable_set("java_token", $response);
      $result = TRUE;
    }
    else {
      watchdog('delay_launch', $error, array(), WATCHDOG_ERROR);
    }

    return $result;
  }

}
