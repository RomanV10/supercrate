<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class UpdateRequestSendMail.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class AssignTeamSendEmailToHelpers implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute(array $data): void {
    $template = db_select('move_template_builder', 'mtb')
      ->fields('mtb')
      ->condition('mtb.key_name', 'admin_assigned_work_helper')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($template)) {
      $node = node_load($data['nid']);

      if ($node) {
        $client = new Clients();
        $emails = [];
        $template[0]['data'] = (array) json_decode($template[0]['data']);
        $template[0]['subject'] = $template[0]['data']['subject'];

        foreach ($data['uids'] as $uid) {
          $user_fields = $client->getUserFieldsData($uid);
          $email = !empty($user_fields['settings']['field_notification_mail']) ? $user_fields['settings']['field_notification_mail'] : $user_fields['mail'];
          if (!empty($email)) {
            $emails[] = $email;
          }
          else {
            watchdog('Assign team', "User with id $uid not have email", [], WATCHDOG_CRITICAL);
          }
        }

        if ($emails) {
          $to = count($emails) > 1 ? $emails : $emails[0];
          (new TemplateBuilder())->moveTemplateBuilderSendTemplatesTo($node, $template, $to, EntityTypes::MOVEREQUEST);
        }
      }
      else {
        watchdog('Assign team', "Node {$data['nid']} dose not exist", [], WATCHDOG_CRITICAL);
      }
    }
  }

}
