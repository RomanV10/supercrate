<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_arrivy\Services\ArrivyRequest;


/**
 * Class ArrivyRemoveForeman.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class ArrivyUnassignForeman implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   */
  public static function execute(array $data): void {
    (new ArrivyRequest($data['nid']))->unassignForeman();
  }

}
