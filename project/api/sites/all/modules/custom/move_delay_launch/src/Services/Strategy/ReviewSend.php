<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_reviews\Services\Reviews;
use Drupal\move_template_builder\Services\EmailTemplate;

/**
 * Class ReviewSend.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class ReviewSend implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute(array $data) : void {
    $review_settings = json_decode(variable_get("reviews_settings"));
    if ($review_settings->reviews_autosend == 1 && $review_settings->selectedDay->value === 0) {
      $template = Reviews::getReviewTemplate('review_reminder_send');
      $result = EmailTemplate::sendEmailTemplate($data['nid'], $template);

      if ($result['send']) {
        Reviews::setReviewSentFlag($data['nid']);
      }
    }
  }

}
