<?php

namespace Drupal\move_delay_launch\Cron;

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_services_new\Cron\Tasks\CronInterface;

/**
 * Class CleanJobs.
 *
 * @package Drupal\move_delay_launch\Cron
 */
class CleanJobs implements CronInterface {

  /**
   * Execute task by cron.
   *
   * @throws \Exception
   */
  public static function execute(): void {
    $thisClass = new CleanJobs();
    $oldRecords1 = $thisClass->findVeryOldRecords();
    $oldRecords2 = $thisClass->findOldRecords();
    if ($oldRecords = array_merge($oldRecords1, $oldRecords2)) {
      $thisClass->deleteOldRecords($oldRecords);
    }
    $thisClass->executeTasks();
  }

  private function findVeryOldRecords() : array {
    $query = db_select('move_delay_task', 'mdt')
      ->fields('mdt', array('task_id'));
    $query->isNull('mdt.date');
    return $query->execute()->fetchCol();
  }

  private function findOldRecords() : array {
    $date = date('Y-m-d H:i:s', strtotime('-10 minutes'));
    $query = db_select('move_delay_task', 'mdt')
      ->fields('mdt', array('task_id'))
      ->condition('mdt.date', array($date), '<');
    return $query->execute()->fetchCol();
  }

  private function deleteOldRecords(array $nids) {
    db_delete('move_delay_task')
      ->condition('task_id', $nids)
      ->execute();
  }

  /**
   * @throws \Exception
   */
  private function executeTasks() {
    $date = date('Y-m-d H:i:s', strtotime('-10 minutes'));
    $current_date = date('Y-m-d H:i:s');
    $query = db_select('move_delay_task', 'mdt')
      ->fields('mdt', array('task_id'))
      ->condition('mdt.date', array($date, $current_date), 'BETWEEN');
    $tasks = $query->execute()->fetchCol();
    foreach ($tasks as $task) {
      (new DelayLaunch())->executeTask($task);
    }
  }

}
