<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_arrivy\Services\ArrivyRequest;


/**
 * Class ArrivyAssignForeman.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class ArrivyAssignForeman implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   */
  public static function execute(array $data): void {
    if (isset($data['foreman'][1])) {
      (new ArrivyRequest($data['nid']))->assignForeman($data['foreman'][0], $data['foreman'][1]);
    }
    else {
      (new ArrivyRequest($data['nid']))->assignForeman($data['foreman'][0], NULL);
    }
  }

}
