<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_statistics\Services\Statistics;

/**
 * Class updateStatisticsForDay.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class UpdateStatisticsForDay implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   */
  public static function execute(array $data): void {
    (new Statistics())->updateStatisticsForDays($data['date']);
  }

}
