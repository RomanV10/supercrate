<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_delay_launch\Services\Authorize;
use Drupal\move_sms\Services\Sms;

/**
 * Class SendSmsNotConfirmed.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class SmsActionSend extends Authorize implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Move request data.
   *
   * @throws \Exception
   */
  public static function execute(array $data) : void {
    if (!empty($data['user'])) {
      self::globalAuthorize($data['user']);
    }
    $node = isset($data['node']) ? $data['node'] : NULL;
    (new Sms($data['actionId']))->execute($data['nid'], $node, $data['uid']);
  }

}
