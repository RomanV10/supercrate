<?php

namespace Drupal\move_delay_launch\Services\Strategy;

/**
 * Class UpdaterApi.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class UpdaterApi implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   */
  public static function execute(array $data): void {
    // Send request on updater api and check if enable.
    $update_settings = json_decode(variable_get('updater_settings'));
    if (!empty($update_settings->updater->token) && $update_settings->updater->isOn) {
      updater_api($data['entity'], $update_settings->updater->token);
    }
    watchdog('delay_launch_execute', "UpdaterApi", array(), WATCHDOG_DEBUG);
  }

}
