<?php

namespace Drupal\move_delay_launch\Services\Strategy;

/**
 * Interface LaunchStrategy.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
interface LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   */
  public static function execute(array $data) : void;

}
