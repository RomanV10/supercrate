<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\Sales;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class CreateRequestSendMail.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class CreateRequestSendMail implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute(array $data): void {
    $template_builder = new TemplateBuilder();

    $node = node_load($data['nid']);
    $node_wrapper = entity_metadata_wrapper('node', $node);
    $approve = $node_wrapper->field_approve->value();
    $parser = $node_wrapper->field_from_parser->value();

    $user_created = $node_wrapper->field_user_created->raw();
    $user = user_load($user_created);
    $admin = (new Sales($data['nid'], $user))->checkAdminRoles();

    $new_user = $node_wrapper->field_new_user_flag->value();
    $service_type = $node_wrapper->field_move_service_type->value();

    if (!empty($new_user) && !$admin || !empty($data['changeDraftEmail'])) {
      $template_builder->moveTemplateBuilderSendTemplateRules('new_request_new_user', $node);
    }

    if (!$admin && !$parser && ($approve == RequestStatusTypes::PENDING || $approve == RequestStatusTypes::FLATE_RATE_INVENTORY_NEEDED)) {
      // IF LOCAL MOVE.
      if (in_array($service_type, [RequestStatusTypes::PENDING, RequestStatusTypes::DAY_N_A])) {
        $template_builder->moveTemplateBuilderSendTemplateRules('new_request_local', $node);
      }
      // For storage request send 1 email.
      elseif ($service_type == RequestServiceType::MOVING_AND_STORAGE
        && empty($node_wrapper->field_to_storage_move->value())
        && !empty($node_wrapper->field_from_storage_move->value())) {
        $template_builder->moveTemplateBuilderSendTemplateRules('new_request_storage', $node);
      }
      elseif ($service_type == RequestServiceType::LOADING_HELP) {
        $template_builder->moveTemplateBuilderSendTemplateRules('new_request_loading_help', $node);
      }
      elseif ($service_type == RequestServiceType::UNLOADING_HELP) {
        $template_builder->moveTemplateBuilderSendTemplateRules('new_request_unloading_help', $node);
      }
      elseif ($service_type == RequestServiceType::FLAT_RATE) {
        $template_builder->moveTemplateBuilderSendTemplateRules('new_request_flat_rate', $node);
      }
      elseif ($service_type == RequestServiceType::LONG_DISTANCE) {
        $template_builder->moveTemplateBuilderSendTemplateRules('new_request_ld', $node);
      }
    }
    elseif ($parser) {
      MoveRequest::getRequestAllData($node->nid);
      $template_builder->moveTemplateBuilderSendTemplateRules('no_quote_email', $node);
    }
    // Always send mails to admin.
    if (!$admin && !$parser) {
      $template_builder->moveTemplateBuilderSendTemplateRules('admin_new_request', $node);
    }

    (new MoveRequest())->addRequestToSearchApiIndex($node->nid);
  }

}
