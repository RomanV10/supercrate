<?php

namespace Drupal\move_delay_launch\Services\Strategy;

/**
 * Class SendEmail.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class SendEmail implements LaunchStrategyInterface {

  /**
   * @inheritdoc
   */
  public static function execute(array $data) : void {}

}
