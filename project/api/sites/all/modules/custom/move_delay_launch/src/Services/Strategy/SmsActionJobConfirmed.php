<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_delay_launch\Services\Authorize;
use Drupal\move_services_new\Util\enum\SmsActionTypes;
use Drupal\move_sms\Services\Sms;

class SmsActionJobConfirmed extends Authorize implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute(array $data): void {
    if (!empty($data['user'])) {
      self::globalAuthorize($data['user']);
    }
    (new Sms(SmsActionTypes::JOB_CONFIRMED))->execute($data['nid'], NULL, $data['uid']);
  }

}
