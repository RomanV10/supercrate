<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class SendEmailAfterContractSubmit.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class SendEmailAfterContractSubmit implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute(array $data): void {
    $template_builder = new TemplateBuilder();
    $node = node_load($data['nid']);
    $template_builder->moveTemplateBuilderSendTemplateRules('send_when_contract_submit', $node);
  }

}
