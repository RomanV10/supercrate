<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_services_new\Services\Cache;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Util\enum\ContractType;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class PayrollContractInfo.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class PayrollContractInfo implements LaunchStrategyInterface {

  /**
   * @inheritdoc
   * @throws \Throwable
   */
  public static function execute(array $data) : void {
    global $user;
    try {
      $result = FALSE;
      $payroll = new Payroll();
      $value = $data['value'];
      if (Payroll::getRequestServiceType($data['nid']) == RequestServiceType::FLAT_RATE) {
        $value['estimate_total'] = 0;
      }

      db_merge('move_contract_info')
        ->key(array('nid' => $data['nid'], 'flat_rate' => $data['contract_type']))
        ->fields(array(
          'value' => serialize($value),
        ))
        ->execute();

      // Setting company flag.
      Payroll::setCompletedFlag($data['nid']);
      _search_api_index_queued_items();

      // Create Payroll Records for all workers.
      $payroll->createPayrollWorkers($data['nid'], $value, $data['contract_type']);
      $payroll->createPayrollManager($data['nid'], $value, $data['contract_type']);

      Cache::updateCacheData($data['nid']);
      // Updating payroll cache.
      $payroll->cacheCreatePayrollAll($payroll->contractDate($data['nid'], $data['contract_type']));
      $result = TRUE;
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Payroll', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      if ($result && $data['contract_type'] != ContractType::PICKUP_PAYROLL) {
        (new DelayLaunch())->createTask(TaskType::SEND_EMAIL_CONTRACT_SUBMIT, array('nid' => $data['nid'], 'user' => $user->uid));
      }
    }
  }

}
