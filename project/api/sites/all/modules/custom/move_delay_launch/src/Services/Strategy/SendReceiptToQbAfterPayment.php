<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_quickbooks\Services\QBSalesReceipt;

/**
 * Class SendReceiptToQbAfterPayment.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class SendReceiptToQbAfterPayment implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute(array $data): void {
    /* If QuickBooks connection is present and post settings are set
    to closed then post on request close */
    if (!empty(variable_get("qbo_api_access_token", FALSE))
      && (variable_get("qbo_post_settings", FALSE) == "alltime")) {
      $post_payment_to_quickbooks = new QBSalesReceipt();
      $post_payment_to_quickbooks->createQBSalesReceipt($data['entity_id'], $data['receipt'], $data['receipt_id']);
    }
  }

}
