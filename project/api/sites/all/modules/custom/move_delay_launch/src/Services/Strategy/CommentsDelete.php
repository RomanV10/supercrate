<?php

namespace Drupal\move_delay_launch\Services\Strategy;

/**
 * Class CommentsDelete.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class CommentsDelete implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   */
  public static function execute(array $data): void {
    if ($data['type'] == 'move_request') {
      $cids = db_select('move_request_comments', 'mrc')
        ->fields('mrc')
        ->condition('mrc.nid', $data['nid'])
        ->execute()->fetchCol();
      if ($cids) {
        db_delete('move_request_comments')
          ->condition('cid', array((array) $cids))
          ->execute();
      }
    }
  }

}
