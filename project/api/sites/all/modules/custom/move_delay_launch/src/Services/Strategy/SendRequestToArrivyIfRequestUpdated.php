<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_arrivy\Services\ArrivyRequest;

/**
 * Class SendArrivyForemanUpdate.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class SendRequestToArrivyIfRequestUpdated implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   */
  public static function execute(array $data): void {
    (new ArrivyRequest($data['nid']))->create();
  }

}
