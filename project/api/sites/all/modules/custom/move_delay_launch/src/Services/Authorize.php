<?php

namespace Drupal\move_delay_launch\Services;

/**
 * Class Authorize.
 *
 * @package Drupal\move_delay_launch\Services
 */
abstract class Authorize {

  public static function globalAuthorize($uid) {
    global $user;
    if ($uid) {
      $user = user_load($uid);
    }
  }

}
