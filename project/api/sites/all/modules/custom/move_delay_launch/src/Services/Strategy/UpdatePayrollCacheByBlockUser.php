<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_services_new\Services\Payroll;

/**
 * Class UpdatePayrollCacheByBlockUser.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class UpdatePayrollCacheByBlockUser implements LaunchStrategyInterface {

  /**
   * @inheritdoc
   */
  public static function execute(array $data) : void {
    if (isset($data['uid'])) {
      $payroll_instance = new Payroll();
      $payroll_instance->updatePayrollCacheByBlockUser($data['uid']);
    }
  }

}
