<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_google_api\Google\Calendar\Services\CalendarsService;
use Drupal\move_services_new\Services\move_request\MoveRequest;

/**
 * Class GoogleCalendarMoveRequestUpdateEvent.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class GoogleCalendarMoveRequestUpdateEvent implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   *
   * @throws \Google_Service_Exception
   * @throws \Throwable
   */
  public static function execute(array $data): void {
    (new CalendarsService())->updateMoveRequestEvent(new MoveRequest($data['nid']));
  }

}
