<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_speedy\Services\Speedy;

/**
 * Class Speedy.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class SpeedyCreate implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute(array $data): void {
    (new Speedy($data['nid'], $data['node_wrapper']))->execute();
  }

}
