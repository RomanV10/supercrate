<?php

namespace Drupal\move_delay_launch\Cron\Queues;

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_services_new\Cron\Queues\QueueInterface;

/**
 * Class ExecuteTask.
 *
 * @package Drupal\move_delay_launch\Cron\Queues
 */
class ExecuteTask implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \Google_Service_Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   * @throws \Exception
   */
  public static function execute($data): void {
    (new DelayLaunch())->executeTask($data);
  }

}
