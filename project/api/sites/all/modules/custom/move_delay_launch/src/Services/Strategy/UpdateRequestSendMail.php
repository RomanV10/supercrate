<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_notification\Services\Notification;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\NotificationTypes;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class UpdateRequestSendMail.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class UpdateRequestSendMail implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   */
  public static function execute(array $data): void {
    $template_builder = new TemplateBuilder();
    try {
      if ($data['status_id'] == 3) {
        $moving_storage = array(2, 6);
        // Local move.
        if ($data['service_type'] == RequestServiceType::MOVING) {
          $template_builder->moveTemplateBuilderSendTemplateRules('status_confirmed', $data['entity']);
        }
        elseif (in_array($data['service_type'], $moving_storage)) {
          $template_builder->moveTemplateBuilderSendTemplateRules('status_confirmed_ms', $data['entity']);
        }
        // Loading.
        elseif ($data['service_type'] == RequestServiceType::LOADING_HELP) {
          $template_builder->moveTemplateBuilderSendTemplateRules('status_confirmed_loading_help', $data['entity']);
        }
        // Unloading.
        elseif ($data['service_type'] == RequestServiceType::UNLOADING_HELP) {
          $template_builder->moveTemplateBuilderSendTemplateRules('status_confirmed_unloading_help', $data['entity']);
        }
        // Flat Rate.
        elseif ($data['service_type'] == RequestServiceType::FLAT_RATE) {
          $template_builder->moveTemplateBuilderSendTemplateRules('status_confirmed_fr', $data['entity']);
        }
        // Long Distance.
        elseif ($data['service_type'] == RequestServiceType::LONG_DISTANCE) {
          $template_builder->moveTemplateBuilderSendTemplateRules('status_confirmed_ld', $data['entity']);
        }
        // Packing day.
        elseif ($data['service_type'] == RequestServiceType::PACKING_DAY) {
          $template_builder->moveTemplateBuilderSendTemplateRules('packing_day_confirmed', $data['entity']);
        }
        // Always send mails to admin.
        $template_builder->moveTemplateBuilderSendTemplateRules('admin_confirmed_request', $data['entity']);
      }
      elseif ($data['status_id'] == RequestStatusTypes::CANCELLED) {
        $template_builder->moveTemplateBuilderSendTemplateRules('status_cancelled', $data['entity']);
      }
      elseif ($data['status_id'] == RequestStatusTypes::EXPIRED) {
        $template_builder->moveTemplateBuilderSendTemplateRules('status_expired', $data['entity']);
      }
      elseif ($data['status_id'] == RequestStatusTypes::INHOME_ESTIMATE) {
        $template_builder->moveTemplateBuilderSendTemplateRules('inhome_estimate', $data['entity']);
      }
      $notification_info = array('node' => $data['entity'], 'user' => $data['user']);
      Notification::createNotification(NotificationTypes::JOB_EXPIRED_CANCELED, $notification_info, $data['entity']->nid, EntityTypes::MOVEREQUEST);
    }
    catch (\Throwable $e) {
      watchdog('Delay launch error UpdateRequestSendMail', $e->getMessage(), [], WATCHDOG_CRITICAL);
    }
  }

}
