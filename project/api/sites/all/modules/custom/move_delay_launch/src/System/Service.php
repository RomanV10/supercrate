<?php

namespace Drupal\move_delay_launch\System;

use Drupal\move_delay_launch\Services\DelayLaunch;

/**
 * Class definition.
 *
 * @package Drupal\move_delay_launch\System
 */
class Service {

  public function getDefinitions() {
    $resources = array(
      '#api_version' => 3002,
      'delay_launch' => array(
        'actions' => array(
          'execute_task' => array(
            'help' => 'Execute task Remotely',
            'callback' => 'Drupal\move_delay_launch\System\Service::executeTask',
            'file' => array(
              'type' => 'php',
              'module' => 'move_delay_launch',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'task_id',
                'type' => 'integer',
                'source' => array('data' => 'task_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );

    return $resources;
  }

  /**
   * Service execute task.
   *
   * @param int $task_id
   *   Delay launch Task id.
   *
   * @throws \Exception
   * @throws \Google_Service_Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public static function executeTask(int $task_id) {
    // @TODO Scratch!!!
    sleep(1);
    $instance = new DelayLaunch();
    $instance->executeTask($task_id);
  }

}
