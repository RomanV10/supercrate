<?php

namespace Drupal\move_delay_launch\Services\Strategy;

use Drupal\move_arrivy\Services\ArrivyForeman;

/**
 * Class SendArrivyForemanUpdate.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class SendArrivyForemanUpdate implements LaunchStrategyInterface {

  /**
   * Execute task by strategy.
   *
   * @param array $data
   *   Data needed for execute task.
   */
  public static function execute(array $data): void {
    (new ArrivyForeman($data['uid']))->update($data['first_name'], $data['last_name'], $data['phone']);
  }

}
