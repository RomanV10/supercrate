<?php

namespace Drupal\move_delay_launch\Services\Strategy;

/**
 * Class UpdateCache.
 *
 * @package Drupal\move_delay_launch\Services\Strategy
 */
class UpdateCache implements LaunchStrategyInterface {

  /**
   * @inheritdoc
   */
  public static function execute(array $data) : void {

  }

}
