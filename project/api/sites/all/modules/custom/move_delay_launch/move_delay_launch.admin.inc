<?php

use Drupal\move_delay_launch\Services\DelayLaunch;

function move_delay_launch_register_java() {
  $form = array();

  $form['drupal_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API Site URL'),
    '#default_value' => variable_get("drupal_url", 'http://api.movecalc.local'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['drupal_port'] = array(
    '#type' => 'textfield',
    '#title' => t('API Site port'),
    '#default_value' => variable_get("drupal_port", 80),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['token'] = array(
    '#type' => 'textfield',
    '#title' => t('Token'),
    '#default_value' => variable_get("java_token", ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
    '#disabled' => TRUE,
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

function move_delay_launch_register_java_submit($form, &$form_state) {
  variable_set("drupal_url", $form_state['values']['drupal_url']);
  variable_set("drupal_port", $form_state['values']['drupal_port']);
  $instance = new DelayLaunch();
  $instance->registerCompany();
}
