<?php

namespace Drupal\move_parser\Parser;

use Monolog\Formatter\LineFormatter;

/**
 * Class ParsePrintFormatter.
 *
 * @package Drupal\move_parser\Parser
 */
class ParsePrintFormatter extends LineFormatter {

  /**
   * {@inheritdoc}
   */
  public function format(array $record) {
    return json_encode($record);
  }

}
