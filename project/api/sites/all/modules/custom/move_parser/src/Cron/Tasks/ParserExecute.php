<?php

namespace Drupal\move_parser\Cron\Tasks;

use Drupal\move_parser\Parser\ParserAlgorithm;
use Drupal\move_services_new\Cron\Tasks\CronInterface;

/**
 * Class ParserExecute.
 *
 * @package Drupal\move_parser\Cron\Tasks
 */
class ParserExecute implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    (new ParserAlgorithm())->execute();
  }

}
