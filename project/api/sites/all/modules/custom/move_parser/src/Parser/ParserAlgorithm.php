<?php

namespace Drupal\move_parser\Parser;

use Drupal\move_parser\Parser\Providers\EquateMedia;
use Drupal\move_mail\System\Common;
use Drupal\move_mail\System\Read;
use Drupal\move_parser\Parser\Providers\Movers;
use Drupal\move_parser\Parser\Providers\Movers123;
use Drupal\move_parser\Parser\Providers\VanLines;
use Drupal\move_parser\Parser\Providers\Movingcom;
use Drupal\move_parser\Parser\Providers\HomeAdvisor;
use Drupal\move_parser\Parser\Providers\MyMovingLoads;
use Drupal\move_parser\Parser\Providers\Billycom;
use Drupal\move_parser\Parser\Providers\Webfixe;
use Drupal\move_parser\Parser\Providers\Irelocate;
use Drupal\move_parser\Parser\Providers\Homebulletin;
use Drupal\move_parser\Parser\Providers\QuoteRunnerLead;

/**
 * Class ParserAlgorithm.
 *
 * @package Drupal\move_parser\Parser
 */
class ParserAlgorithm {

  /**
   * Folder name.
   */
  private const FOLDER = 'INBOX';

  /**
   * @var array
   */
  private $mailUser = array();

  /**
   * @var int
   */
  private $mailUserUID = -1;

  /**
   * @var array
   */
  private $mails = array();

  /**
   * @var int
   */
  private $folderId;

  /**
   * @var \Drupal\move_parser\Parser\HandlerContainer
   */
  private $providersHandler;

  public function __construct() {
    // Create new folder if folder not exist.
    Common::saveNewFolders($this->mailUserUID, array(self::FOLDER));
    $this->folderId = Common::getFolderId($this->mailUserUID, self::FOLDER);
    $this->providersHandler = new HandlerContainer();
    // Register all providers.
    $this->getProviders();
  }

  /**
   * Entry point to class.
   *
   */
  public function execute() {
    try {
      // Check parser enabled.
      $parser_data = variable_get('parser_data');
      $parser_isset = isset($parser_data['enabled']);
      if (!$parser_isset || ($parser_isset && !$parser_data['enabled'])) {
        return;
      }
      $this->downloadMails();
      foreach ($this->mails as $mail) {
        $this->mailParsing($mail);
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Parser', $message, array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Download and save email from mail server.
   *
   * @throws ParserConnectToServer
   * @throws ParserDownloadMails
   */
  private function downloadMails() {
    $common_instance = $this->connect();
    $mail_inst = new Read($common_instance->getImapResource());
    $mails = $mail_inst->getNewMessages($this->mailUserUID, $this->folderId);
    if (!$mails) {
      return;
    }

    $mails = array_slice($mails, 0, 2);
    sort($mails);
    $error = NULL;
    foreach ($mails as $mid) {
      if (Common::checkMessageExist($this->mailUserUID, $mid, $this->folderId)) {
        $mail_inst->setSeenFlag($mid);
        continue;
      }

      $mail = $mail_inst->getMessage($mid, $this->mailUserUID, $this->folderId);
      $mail['message_body'] = check_markup(Common::replace4byte($mail['message_body']), 'plain_text');
      $mail['message_body'] = Common::clearHtml((string) $mail['message_body']);
      $mail['message_body'] = html_entity_decode($mail['message_body']);
      $mail['original'] = $mail['original'] ?? $mail['message_body'];

      if ($mail) {
        try {
          $record = move_mail_message_new();
          $record->user_id = $this->mailUserUID;
          $record->mid = $mid;
          $record->m_subject = $mail['message_subject'];
          $record->address_from = $mail['from_address'];
          $record->address_to = $mail['to_address'];
          $record->m_date = $mail['date'];
          $record->m_unread = FALSE;
          $record->m_answered = FALSE;
          $record->m_body = $mail['message_body'];
          $record->m_original = mb_strtolower($mail['original']);
          $record->folder_id = $this->folderId;
          $record->attachment_exists = isset($mail['attachment']) ? TRUE : FALSE;
          entity_save('move_mail_message', $record);
          $mail_inst->setSeenFlag($mid);
        }
        catch (\Throwable $exception) {
          // The first bypass the whole array.
          $error = $exception;
        }
      }

      $this->mails[] = $mail;
    }

    if ($error) {
      throw new ParserDownloadMails($error->getMessage());
    }
  }

  /**
   * @return \Drupal\move_mail\System\Common
   * @throws \Drupal\move_parser\Parser\ParserConnectToServer
   */
  private function connect() {
    $common_instance = new Common();
    $common_instance->connectToParseServer(self::FOLDER);
    if (!$common_instance->getImapResource()->getImapStream()) {
      watchdog("Parser", "An error occurred while trying to connect mail server", array(), WATCHDOG_INFO);
      throw new ParserConnectToServer("An error occurred while trying to connect mail server.");
    }

    return $common_instance;
  }

  /**
   * Implementing Pattern: Chain of Responsibility for parse mail through providers.
   *
   * @param array $mail
   */
  private function mailParsing(array $mail) {
    $this->providersHandler->execute($mail);
  }

  private function getProviders() {
    $this->providersHandler->addHandler(new Movers123());
    $this->providersHandler->addHandler(new VanLines());
    $this->providersHandler->addHandler(new Movingcom());
    $this->providersHandler->addHandler(new HomeAdvisor());
    $this->providersHandler->addHandler(new Movers());
    $this->providersHandler->addHandler(new EquateMedia());
    $this->providersHandler->addHandler(new MyMovingLoads());
    $this->providersHandler->addHandler(new Billycom());
    $this->providersHandler->addHandler(new Webfixe());
    $this->providersHandler->addHandler(new Irelocate());
    $this->providersHandler->addHandler(new Homebulletin());
    $this->providersHandler->addHandler(new QuoteRunnerLead());
  }

  public function checkProvider($id) {
    return $this->providersHandler->findProviderId($id);
  }

}

class ParserException extends \Exception {

  public function saveDataBase($type) {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()} </br> Trace: {$this->getTraceAsString()}";
    watchdog('Parser', $message, array(), $type);
  }

}

class ParserNotMailSettings extends ParserException {}
class ParserDownloadMails extends ParserException {

  /**
   * ParserDownloadMails constructor.
   *
   * @inheritdoc
   */
  public function __construct(string $message = "", int $code = 0, Throwable $previous = NULL) {
    parent::__construct($message, $code, $previous);
    $this->saveDataBase(WATCHDOG_ERROR);
  }

}
class ParserConnectToServer extends ParserException {}
