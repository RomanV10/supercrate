<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;

/**
 * Class Movers.
 *
 * @package Drupal\move_parser\Parser
 */
class Billycom extends ParserExtends implements ParseInterface {

  const PROVIDERID = 7;
  const PROVIDERNAME = 'Billy.com';

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool {
    parent::setProviderName($this->getProviderName());
    $result = FALSE;
    if ($this->findBelong($mail)) {
      $data = $this->parseMessage($mail['original']);
      $this->prepareName($data['data']);
      $this->prepareFieldDate($data['data']);
      $this->prepareSizeOfMove($data['data']);
      $this->prepareFieldAddress($data['data']);
      $this->setClientPhone($data['data']);
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail['original']);
      $result = TRUE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $subject = trim($mail['from_address']);
    $pattern = '/donotreply@billypartners.com/i';
    return preg_match($pattern, $subject);
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  private function parseMessage(string $body) : array {
    $data = array(
      'name' => '',
      'field_phone' => '',
      'field_phone2' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => '',
      'moving_from_state' => '',
      'moving_from_city' => '',
      'moving_from_zip' => '',
      'moving_to_zip' => '',
      'moving_to_city' => '',
      'moving_to_state' => '',
    );

    $patterns = array(
      'name' => array(
        'preg' => '/(?<=Customer Name:<\/b><\/span>|(?<=Customer Name:))\s*(\w+\s\w+)/i',
        'pos' => 1,
      ),
      'field_phone' => array(
        'preg' => '/(?<=Phone Number:<\/b><\/span>\s|(?<=Phone Number:))\s*([\d]*-[\d]*-[\d]*)/i',
        'pos' => 1,
      ),
      'field_phone2' => array(
        'preg' => '/(?<=Phone Number:<\/b><\/span>\s)\s*\<a[^>]+>(.*?)<\/a>/i',
        'pos' => 1,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=Email:\<\/b><\/span\>|(?<=Email:))\s+\<a[^>]+>(.*?)<\/a>/i',
        'pos' => 1,
      ),
      'moving_from_state' => array(
        'preg' => '/(?<=Moving From:<\/b><\/span>|(?<=Moving From:))\s*[\w\s]+\,\s+(\w+)/i',
        'pos' => 1,
      ),
      'moving_from_city' => array(
        'preg' => '/(?<=Moving From:<\/b><\/span>|(?<=Moving From:))\s*([\w\s]+)/i',
        'pos' => 1,
      ),
      'moving_from_zip' => array(
        'preg' => '/(?<=Moving From:<\/b><\/span>|(?<=Moving From:))\s*[\w\s]+\,\s+\w+\s+(\d+)/i',
        'pos' => 1,
      ),
      'field_date' => array(
        'preg' => '/(?<=Move Date:<\/b><\/span>|(?<=Move Date:))\s*(\d*\/\d*\/\d*)/i',
        'pos' => 1,
      ),
      'field_size_of_move' => array(
        'preg' => '/(?<=Weight:<\/b><\/span>|(?<=Weight:))\s*(\d*\,*\s*\d*\s*+lbs)/i',
        'pos' => 1,
      ),
      'moving_to_zip' => array(
        'preg' => '/(?<=Moving\sTo:<\/b><\/span>|(?<=Moving\sTo:))\s*[\w\s]+\,\s+\w+\s+(\d+)/i',
        'pos' => 1,
      ),
      'moving_to_city' => array(
        'preg' => '/(?<=Moving\sTo:<\/b><\/span>|(?<=Moving To:))\s*([\w\s]+)\,/i',
        'pos' => 1,
      ),
      'moving_to_state' => array(
        'preg' => '/(?<=Moving To:<\/b><\/span>|(?<=Moving To:))\s*[\w\s]+\,\s+(\w+)/i',
        'pos' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match($patterns[$id]['preg'], $body, $matches);
      if ($match && $matches) {
        $item = $matches[$patterns[$id]['pos']];
      }
    }

    return array('data' => $data, 'parse' => $body);
  }

  private function prepareName(array &$data) {
    if (isset($data['name']) && $data['name']) {
      list($first_name, $last_name) = preg_split("/[\s]+/", trim($data['name']));
      $data['field_first_name'] = $first_name;
      $data['field_last_name'] = $last_name;
    }
    unset($data['name']);
  }

  private function prepareFieldDate(&$data) {
    $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
  }

  private function prepareSizeOfMove(array &$data) {
    if (isset($data['field_size_of_move']) && $data['field_size_of_move']) {
      $data['field_size_of_move'] = str_replace(',', '', $data['field_size_of_move']);
    }
  }

  /**
   * Set client phone.
   *
   * @param array $data
   *   Parsing data for create request.
   */
  private function setClientPhone(array &$data) {
    $data['field_phone'] = !empty($data['field_phone']) ? $data['field_phone'] : $data['field_phone2'];
    unset($data['field_phone2']);
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_state',
        'moving_from_city',
        'moving_from_zip',
      ),
      'field_moving_to' => array(
        'moving_to_state',
        'moving_to_city',
        'moving_to_zip',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'country' => 'US',
        'locality' => '',
        'administrative_area' => '',
        'postal_code' => '',
      );
      if (isset($data[$item[0]])) {
        $data[$field_name]['administrative_area'] = strtoupper($this->getStateCode($data[$item[0]]));
        unset($data[$item[0]]);
      }
      if (isset($data[$item[1]])) {
        $data[$field_name]['locality'] = $data[$item[1]];
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['postal_code'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
    }
  }

}
