<?php

namespace Drupal\move_parser\Parser;

class VendorDetails {

  private $vendorId;

  public function __construct($id = NULL) {
    $this->vendorId = $id;
  }

  public static function registerVendor(array $data) {
    $result = FALSE;

    // Check provider.
    $parse_inst = new ParserAlgorithm();
    if ($parse_inst->checkProvider($data['provider_id'])) {
      $result = db_insert('move_vendor_details')
        ->fields(array(
          'parser_provider' => $data['provider_id'],
          'active' => $data['active'],
          'company_name' => $data['company_name'],
          'info' => serialize($data['info']),
          'created' => time(),
        ))
        ->execute();
    }

    return $result;
  }

}
