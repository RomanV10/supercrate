<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_mail\System\Common;
use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;
use Drupal\move_parser\Services\Parser;

/**
 * Class HomeAdvisor.
 *
 * @package Drupal\move_parser\Parser
 */
class HomeAdvisor extends ParserExtends implements ParseInterface {

  const PROVIDERID = 3;
  const PROVIDERNAME = 'Homeadvisor';

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function parse(array $mail) : bool {
    parent::setProviderName($this->getProviderName());
    $result = FALSE;

    if ($this->findBelong($mail)) {
      if ($mail['message_subject'] == 'New Exact Match Lead') {
        $data = $this->parseMessageVersion3($mail['original']);
      }
      else {
        $data = $this->parseMessageVersion2($mail['message_body']);
      }
      $this->prepareName($data['data']);
      $this->prepareFieldDate($data['data']);
      $this->prepareSizeOfMove($data['data']);
      $this->prepareFieldAddress($data['data']);
      if (Parser::emptyRequiredParseData($data['data'])) {
        return FALSE;
      }
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail['original']);
      $result = TRUE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $subject = trim($mail['from_address']);
    $pattern = '/newlead@homeadvisor.com/';
    return preg_match($pattern, $subject);
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  private function parseMessageVersion3(string $body) : array {
    $body = strip_tags($body);
    $body = Common::clean_string($body);

    $data = array(
      'name' => '',
      'field_phone' => '',
      'field_additional_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => '',
      'moving_from_thoroughfare' => '',
      'moving_from_city' => '',
      'moving_from_state' => '',
      'moving_from_zip' => '',
      'moving_to_zip' => '',
    );

    $patterns = array(
      'name' => array(
        'preg' => '/(?<=Customer\sInformation)<br>\s*(\w*\s*\w*)/i',
        'pos' => 1,
      ),
      'field_phone' => array(
        'preg' => '/(\(\d*\)\s*\d*-\d*)/i',
        'pos' => 1,
      ),
      'field_additional_phone' => array(
        'preg' => '/(?<=Customer\sInformation)<br>\s*\w*\s*\w*<br>\s*(\W+\d*\W\s*\d*-\d*)/i',
        'pos' => 1,
      ),
      'field_e_mail' => array(
        'preg' => '/([a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b)/i',
        'pos' => 1,
      ),
      'field_date' => array(
        'preg' => '/(?<=Date\sof\sMove)\s*:\s*(.*?)\</i',
        'pos' => 1,
      ),
      'field_size_of_move' => array(
        'preg' => '/(\d*\slbs)/i',
        'pos' => 1,
      ),
      'moving_from_thoroughfare' => array(
        'preg' => '/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b<br>\s*(.*?)\,/i',
        'pos' => 1,
      ),
      'moving_from_city' => array(
        'preg' => '/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b<br>\s*(.*?)\,\s*(.*?)\,/i',
        'pos' => 2,
      ),
      'moving_from_state' => array(
        'preg' => '/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b<br>\s*(.*?)\,.*?\,\s*(\w*)/i',
        'pos' => 2,
      ),
      'moving_from_zip' => array(
        'preg' => '/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b<br>\s*(.*?)\,.*?\,\s*\w*\s*(\d*)/i',
        'pos' => 2,
      ),
      'moving_to_zip' => array(
        'preg' => '/(?<=ZIP\s\Moving\sTo|(?<=ZIP\s\Moving\sPiano\sTo))\s*:\s*(\d*)/i',
        'pos' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match($patterns[$id]['preg'], $body, $matches);
      if ($match && $matches) {
        $item = $matches[$patterns[$id]['pos']];
      }
    }

    return array('data' => $data, 'parse' => $body);
  }

  private function parseMessageVersion2(string $body) : array {
    $data = array(
      'name' => '',
      'field_phone' => '',
      'field_additional_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => '',
      'moving_from_thoroughfare' => '',
      'moving_from_city' => '',
      'moving_from_state' => '',
      'moving_from_zip' => '',
      'moving_to_zip' => '',
    );

    $patterns = array(
      'name' => array(
        'preg' => '/(?<=Customer\sName)\s*\:\s*([\w\s\-]+)/i',
        'pos' => 1,
      ),
      'field_phone' => array(
        'preg' => '/(?<=Cellular\sPhone)\s*\:\s*([\(\)\d\ \-]+)/i',
        'pos' => 1,
      ),
      'field_additional_phone' => array(
        'preg' => '/(?<=Daytime\sPhone)\s*\:\s*([\(\)\d\ \-]+)/i',
        'pos' => 1,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=Email)\s*:\s*<a[^>]+>(.*?)<\/a>/i',
        'pos' => 1,
      ),
      'field_date' => array(
        'preg' => '/(?<=Date\sof\sMove)\s*:\s*(.*?)\</i',
        'pos' => 1,
      ),
      'field_size_of_move' => array(
        'preg' => '/(\d*\slbs)/i',
        'pos' => 1,
      ),
      'moving_from_thoroughfare' => array(
        'preg' => '/(?<=Address)\s*:\s*(.*?)\,/i',
        'pos' => 1,
      ),
      'moving_from_city' => array(
        'preg' => '/(?<=Address)\s*:\s*.*?\,\s*(.*?)\,/i',
        'pos' => 1,
      ),
      'moving_from_state' => array(
        'preg' => '/(?<=Address)\s*:\s*.*?\,.*?\,\s*(\w*)/i',
        'pos' => 1,
      ),
      'moving_from_zip' => array(
        'preg' => '/(?<=Address)\s*:\s*.*?\,.*?\,\s*\w*\s*(\d*)/i',
        'pos' => 1,
      ),
      'moving_to_zip' => array(
        'preg' => '/(?<=ZIP\s\Moving\sTo|(?<=ZIP\s\Moving\sPiano\sTo))\s*:\s*(\d*)/i',
        'pos' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match($patterns[$id]['preg'], $body, $matches);
      if ($match && $matches) {
        $item = $matches[$patterns[$id]['pos']];
      }
    }

    return array('data' => $data, 'parse' => $body);
  }

  private function parseMessage(string $parse_data) : array {
    $data = array(
      'name' => '',
      'field_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => '',
      'field_additional_comments' => '',
      'moving_from_state' => '',
      'moving_from_city' => '',
      'moving_from_zip' => '',
      'moving_from_thoroughfare' => '',
      'moving_to_zip' => '',
    );

    $patterns = array(
      'name' => array(
        'preg' => '/(?<=\<p\>Customer Information\<\/p\>\s)\<p\>([\s\w]+)/i',
        'pos' => 1,
      ),
      'field_phone' => array(
        'preg' => '/(?<=Phone)\s+([\(\)\d\ \-]+)/i',
        'pos' => 1,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=mailto:)(.*?)(?=\")/i',
        'pos' => 1,
      ),
      'moving_from_thoroughfare' => array(
        'preg' => '/(?<=Address\s{2})([\d\s\w\W]+)\,/Ui',
        'pos' => 1,
      ),
      'moving_from_state' => array(
        'preg' => '/(?<=Address)[\d\s\w\W]+\,\s[\w\s]+\,\s(\w+)\s[\d]+\<br \/\>/',
        'pos' => 1,
      ),
      'moving_from_city' => array(
        'preg' => '/(?<=Address)[\d\s\w\W]+\,\s([\w\s]+)\,\s(\w+)\s[\d]+\<br \/\>/',
        'pos' => 1,
      ),
      'moving_from_zip' => array(
        'preg' => '/(?<=Address)[\d\s\w\W]+\,\s[\w\s]+\,\s\w+\s([\d]+)\<br \/\>/',
        'pos' => 1,
      ),
      'field_date' => array(
        'preg' => '/\<p\>Date\ of\ Move\:\<\/p\>\s\<p\>([\S\s]*)\<\/p\>/Uix',
        'pos' => 1,
      ),
      'field_size_of_move' => array(
        'preg' => '/\<p\>\*Move\sWeight.*?\s\<p\>([\W\w]+)\<\/p\>/Uix',
        'pos' => 1,
      ),
      'field_additional_comments' => array(
        'preg' => '/\<p\>\*Comments:\*\<\/p\>\s\<p\>([\S\s]*)\<\/p\>/Uix',
        'pos' => 1,
      ),
      'moving_to_zip' => array(
        'preg' => '/\<p\>ZIP\ Moving\ To\:\<\/p\>\s\<p\>([\d]*)\<\/p\>/Uix',
        'pos' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match($patterns[$id]['preg'], $parse_data, $matches);
      if ($match && $matches) {
        $item = $matches[$patterns[$id]['pos']];
      }
    }

    return array('data' => $data, 'parse' => $parse_data);
  }

  /**
   * Prepare field date from parse data.
   *
   * @param array $data
   *   Data for create move request.
   */
  private function prepareFieldDate(array &$data) {
    if (preg_match('/[a-zA-Z]/', $data['field_date'])) {
      $format = "D M d Y";
      $dateobj = \DateTime::createFromFormat($format, $data['field_date']);
      if ($dateobj instanceof \DateTime) {
        $data['field_date'] = $dateobj->format("Y-m-d");
      }
      else {
        $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
      }
    }
    else {
      $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
    }

    // Set the future date if the date is not received or the date
    // format was set incorrectly.
    if (empty($data['field_date']) || (time() > strtotime($data['field_date']))) {
      $data['field_date'] = date('Y-m-d', strtotime("+10 day"));
    }
  }

  private function prepareName(array &$data) {
    if (isset($data['name']) && $data['name']) {
      list($first_name, $last_name) = preg_split("/[\s]+/", trim($data['name']));
      $data['field_first_name'] = trim($first_name);
      $data['field_last_name'] = trim($last_name);
    }

    unset($data['name']);
  }

  private function prepareSizeOfMove(array &$data) {
    if (isset($data['field_size_of_move']) && $data['field_size_of_move']) {
      $data['field_size_of_move'] = str_replace(',', '', $data['field_size_of_move']);
    }
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_zip',
        'moving_from_state',
        'moving_from_city',
        'moving_from_thoroughfare',
      ),
      'field_moving_to' => array(
        'moving_to_zip',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'country' => 'US',
        'locality' => '',
        'administrative_area' => '',
        'postal_code' => '',
        'thoroughfare' => '',
      );

      if (isset($item[0]) && isset($data[$item[0]])) {
        $data[$field_name]['postal_code'] = $data[$item[0]];
        unset($data[$item[0]]);
      }
      if (isset($item[1]) && isset($data[$item[1]])) {
        $data[$field_name]['administrative_area'] = strtoupper($data[$item[1]]);
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['locality'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
      if (isset($item[3]) && isset($data[$item[3]])) {
        $data[$field_name]['thoroughfare'] = $data[$item[3]];
        unset($data[$item[3]]);
      }
    }

  }

}
