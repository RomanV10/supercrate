<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;

/**
 * Class VanLines.
 *
 * @package Drupal\move_parser\Parser
 */
class VanLines extends ParserExtends implements ParseInterface {

  const PROVIDERID = 1;
  const PROVIDERNAME = 'Vanlines';

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool{
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    // TODO: Implement findBelong() method.
  }

}
