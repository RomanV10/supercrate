<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\CssSelector\CssSelectorConverter;

/**
 * Class Movers.
 *
 * @package Drupal\move_parser\Parser
 */
class Movers extends ParserExtends implements ParseInterface {

  const PROVIDERID = 5;
  const PROVIDERNAME = 'Movers.com';

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool {
    parent::setProviderName($this->getProviderName());
    $result = FALSE;
    if ($this->findBelong($mail)) {
      $data = $this->parseMessage($mail['message_body']);
      if (!$data['data']['field_e_mail']) {
        $data = $this->parseMessage2($mail['original']);
      }
      $this->prepareName($data['data']);
      $this->prepareFieldDate($data['data']);
      $this->prepareFieldAddress($data['data']);
      $this->prepareSizeOfMove($data['data']);
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail['original']);
      $result = TRUE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $subject = trim($mail['message_subject']);
    $pattern = '/movers.com/i';
    return preg_match($pattern, $subject);
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  private function parseMessage(string $parse_data) : array {
    $data = array(
      'name' => '',
      'field_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => 0,
      'field_additional_comments' => '',
      'moving_from_state' => '',
      'moving_from_city' => '',
      'moving_from_zip' => '',
      'moving_to_zip' => '',
      'moving_to_city' => '',
      'moving_to_state' => '',
    );

    $patterns = array(
      'name' => array(
        'preg' => '/(?<=Name:)\W\s*(\w+\s\w+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_phone' => array(
        'preg' => '/(?<=Contact number:)\W\s*HOME:([\d-\s]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'mobile_phone' => array(
        'preg' => '/(?<=Mobile Phone:\s)([\d-]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=Email:)\W\s*<a[^>]+>(.*?)<\/a>/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_date' => array(
        'preg' => '/(?<=Move\sDate:)\W\s*([\d]+\/[\d]+\/[\d]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_size_of_move' => array(
        'preg' => '/(\d*\s*lbs)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_additional_comments' => array(
        'preg' => '/(?<=Additional\sComments:\s)([\w\s]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_from_state' => array(
        'preg' => '/(?<=State:)\W\s*([\w\s]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_from_city' => array(
        'preg' => '/(?<=City:\s)\W\s*([\w\s]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_from_zip' => array(
        'preg' => '/(?<=ZipCode:)\W\s*([\w\s]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_to_state' => array(
        'preg' => '/(?<=State:)\W\s*([\w\s]+)/i',
        'pos' => 1,
        'id' => 1,
      ),
      'moving_to_city' => array(
        'preg' => '/(?<=City:\s)\W\s*([\w\s]+)/i',
        'pos' => 1,
        'id' => 1,
      ),
      'moving_to_zip' => array(
        'preg' => '/(?<=ZipCode:)\W\s*([\w\s]+)/i',
        'pos' => 1,
        'id' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match_all($patterns[$id]['preg'], $parse_data, $matches);
      if ($match && $matches) {
        $item = trim($matches[$patterns[$id]['pos']][$patterns[$id]['id']]);
      }
    }

    return array('data' => $data, 'parse' => $parse_data);
  }

  private function parseMessage2(string $parse_data) : array {
    $data = array(
      'name' => '',
      'field_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => 0,
      'field_additional_comments' => '',
      'moving_from_state' => '',
      'moving_from_city' => '',
      'moving_from_zip' => '',
      'moving_to_zip' => '',
      'moving_to_city' => '',
      'moving_to_state' => '',
    );

    $patterns = array(
      'name' => array(
        'preg' => '/(?<=NAME=)\s*(\w*\s*\w*)/i',
        'pos' => 1,
      ),
      'field_phone' => array(
        'preg' => '/(?<=CONTACT\sNO\.\(HOME\)\=)\s*(\d*\s*\d*\s*\d*)/i',
        'pos' => 1,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=Email\=)\W\s*<a[^>]+>(.*?)<\/a>/i',
        'pos' => 1,
      ),
      'field_date' => array(
        'preg' => '/(?<=MOVE\sDATE\=)(\d{2}\/\d{2}\/\d{4})/i',
        'pos' => 1,
      ),
      'field_size_of_move' => array(
        'preg' => '/(\d*\s*lbs)/i',
        'pos' => 1,
      ),
      'field_additional_comments' => array(
        'preg' => '/(?<=Additional\sComments:\s)([\w\s]+)/i',
        'pos' => 1,
      ),
      'moving_from_state' => array(
        'preg' => '/(?<=From_STATE\=)\s*(\w*)\</i',
        'pos' => 1,
      ),
      'moving_from_city' => array(
        'preg' => '/(?<=From_CITY\=)\s*(.*?)\</i',
        'pos' => 1,
      ),
      'moving_from_zip' => array(
        'preg' => '/(?<=From_ZIP\=)\s*(\d*)/i',
        'pos' => 1,
      ),
      'moving_to_state' => array(
        'preg' => '/(?<=To_STATE\=)\s*(\w*)/i',
        'pos' => 1,
      ),
      'moving_to_city' => array(
        'preg' => '/(?<=To_CITY\=)\s*(.*?)\</i',
        'pos' => 1,
      ),
      'moving_to_zip' => array(
        'preg' => '/(?<=To_ZIP\=)(\d*)/i',
        'pos' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match($patterns[$id]['preg'], $parse_data, $matches);
      if ($match && $matches) {
        $item = $matches[$patterns[$id]['pos']];
      }
    }

    return array('data' => $data, 'parse' => $parse_data);
  }

  private function prepareSizeOfMove(array &$data) {
    if (isset($data['field_size_of_move']) && $data['field_size_of_move']) {
      $matches = array();
      $pattern = '/([\d]+)\s*([a-zA-z]+)/i';
      preg_match($pattern, $data['field_size_of_move'], $matches);
      if (isset($matches[0]) && isset($matches[1]) && isset($matches[2])) {
        $size = ((int) $matches[0] + (int) $matches[1]) / 2;
        $data['field_size_of_move'] = "$size {$matches[2]}";
      }
    }
  }

  private function prepareName(array &$data) {
    if (isset($data['name']) && $data['name']) {
      list($first_name, $last_name) = preg_split("/[\s]+/", trim($data['name']));
      $data['field_first_name'] = $first_name;
      $data['field_last_name'] = $last_name;
    }
    unset($data['name']);
  }

  private function prepareFieldDate(&$data) {
    $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_zip',
        'moving_from_state',
        'moving_from_city',
      ),
      'field_moving_to' => array(
        'moving_to_zip',
        'moving_to_state',
        'moving_to_city',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'postal_code' => '',
        'administrative_area' => '',
        'locality' => '',
        'country' => '',
        'thoroughfare' => '',
      );

      if (isset($item[0]) && isset($data[$item[0]])) {
        $data[$field_name]['postal_code'] = $data[$item[0]];
        unset($data[$item[0]]);
      }
      if (isset($item[1]) && isset($data[$item[1]])) {
        $data[$field_name]['administrative_area'] = strtoupper($data[$item[1]]);
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['locality'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
      $data[$field_name]['country'] = 'US';

    }
  }

}
