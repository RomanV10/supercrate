<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Movers.
 *
 * @package Drupal\move_parser\Parser
 */
class MyMovingLoads extends ParserExtends implements ParseInterface {

  const PROVIDERID = 6;
  const PROVIDERNAME = 'Mymovingloads.com';

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool {
    parent::setProviderName($this->getProviderName());
    $result = FALSE;
    if ($this->findBelong($mail)) {
      $data = $this->parseMessage($mail['original']);
      $this->prepareFieldDate($data['data']);
      $this->prepareFieldAddress($data['data']);
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail['original']);
      $result = TRUE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $pattern = '/mymovingloads/i';
    $address = trim($mail['from_address']);
    $subject = trim($mail['message_subject']);
    $result = preg_match($pattern, $address);
    if (!$result) {
      $result = preg_match($pattern, $subject);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  private function parseMessage(string $body) : array {
    $crawler = new Crawler($body);
    $crawler = $crawler->filter("tbody > tr");

    $nodeValues = $crawler->each(
      function (Crawler $node, $i) {
        $first = $node->children()->first()->text();
        $last = $node->children()->last()->text();
        return array($first, $last);
      }
    );

    if (!isset($nodeValues[4][1]) && !$nodeValues[4][1]) {
      $crawler = new Crawler($body);
      $crawler = $crawler->filter("table > tr");

      $nodeValues = $crawler->each(
        function (Crawler $node, $i) {
          $first = $node->children()->first()->text();
          $last = $node->children()->last()->text();
          return array($first, $last);
        }
      );
    }

    $first_name = isset($nodeValues[1][1]) ? $nodeValues[1][1] : '';
    $last_name = $nodeValues[2][1] ? $nodeValues[2][1] : '';
    $phone = $nodeValues[3][1] ? $nodeValues[3][1] : '';
    $e_mail = $nodeValues[4][1] ? $nodeValues[4][1] : '';
    $field_date = $nodeValues[17][1] ? $nodeValues[17][1] : '';
    $from_city = $nodeValues[8][1] ? $nodeValues[8][1] : '';
    $from_state = $nodeValues[7][1] ? $nodeValues[7][1] : '';
    $from_zip = $nodeValues[6][1] ? $nodeValues[6][1] : '';
    $to_city = $nodeValues[13][1] ? $nodeValues[13][1] : '';
    $to_state = $nodeValues[12][1] ? $nodeValues[12][1] : '';
    $to_zip = $nodeValues[11][1] ? $nodeValues[11][1] : '';

    $data = array(
      'field_first_name' => $first_name,
      'field_last_name' => $last_name,
      'field_phone' => $phone,
      'field_e_mail' => $e_mail,
      'field_date' => $field_date,
      'moving_from_city' => $from_city,
      'moving_from_state' => $from_state,
      'moving_from_zip' => $from_zip,
      'moving_to_city' => $to_city,
      'moving_to_state' => $to_state,
      'moving_to_zip' => $to_zip,
      'field_size_of_move' => '',
      'field_additional_comments' => '',
    );

    return array('data' => $data, 'parse' => $body);
  }

  private function prepareFieldDate(&$data) {
    $format = "m/d/Y";
    if (isset($data['field_date']) && $data['field_date']) {
      $dateobj = \DateTime::createFromFormat($format, $data['field_date']);
      $data['field_date'] = $dateobj->format("Y-m-d");
    }
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_zip',
        'moving_from_state',
        'moving_from_city',
      ),
      'field_moving_to' => array(
        'moving_to_zip',
        'moving_to_state',
        'moving_to_city',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'postal_code' => '',
        'administrative_area' => '',
        'locality' => '',
        'country' => '',
        'thoroughfare' => '',
      );

      if (isset($item[0]) && isset($data[$item[0]])) {
        $data[$field_name]['postal_code'] = $data[$item[0]];
        unset($data[$item[0]]);
      }
      if (isset($item[1]) && isset($data[$item[1]])) {
        $data[$field_name]['administrative_area'] = strtoupper($data[$item[1]]);
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['locality'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
      $data[$field_name]['country'] = 'US';
    }
  }

}
