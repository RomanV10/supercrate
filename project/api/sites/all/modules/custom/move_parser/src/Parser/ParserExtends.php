<?php

namespace Drupal\move_parser\Parser;

use Drupal\move_branch\Services\Branch;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_new_log\Services\Log;
use Drupal\move_parser\Services\Parser;

/**
 * Class ParserExtends.
 *
 * @package Drupal\move_parser\Parser
 */
class ParserExtends {
  private $providerName = '';

  /**
   * Helper method for create request after parsing.
   *
   * @param array $data
   *   Array for new request.
   * @param int $provider_id
   *   The id of parser provider.
   * @param string $provider_name
   *   The name of parser provider.
   * @param array $parse_log
   *   The data for saving log information about email in DB.
   * @param string $email
   *   Original email for log.
   *
   * @throws \Exception
   */
  public function createRequest(array $data, int $provider_id, string $provider_name, array $parse_log, string $email) {
    global $user;
    $data['all_data'] = array();
    $this->setProviderName($provider_name);

    if ($this->checkUserFields($data)) {
      $data['account'] = array(
        'name' => $data['field_e_mail'],
        'mail' => $data['field_e_mail'],
        'fields' => array(
          'field_user_first_name' => (isset($data['field_first_name'])) ? $data['field_first_name'] : '',
          'field_user_last_name' => (isset($data['field_last_name'])) ? $data['field_last_name'] : '',
          'field_primary_phone' => (isset($data['field_phone']) && $data['field_phone']) ? $data['field_phone'] : '',
          'field_user_additional_phone' => (isset($data['field_additional_phone']) && $data['field_additional_phone']) ? $data['field_additional_phone'] : '',
        ),
      );
    }
    else {
      $user = user_load(1);
      $text = "Field email is empty! $this->providerName, $parse_log, $data";
      watchdog("Parser", $text, array(), WATCHDOG_INFO);
      $random_email = $this->prepareRandomEmail();
      $data['account'] = array(
        'name' => $random_email,
        'mail' => $random_email,
        'fields' => array(
          'field_user_first_name' => (isset($data['field_first_name'])) ? $data['field_first_name'] : '',
          'field_user_last_name' => (isset($data['field_last_name'])) ? $data['field_last_name'] : '',
          'field_primary_phone' => (isset($data['field_phone']) && $data['field_phone']) ? $data['field_phone'] : '',
          'field_user_additional_phone' => (isset($data['field_additional_phone']) && $data['field_additional_phone']) ? $data['field_additional_phone'] : '',
        ),
      );
    }

    $duplicate_request = Parser::checkDuplicateRequests($data['field_date'], $data['field_e_mail'], $data['field_moving_from']['postal_code'], $data['field_moving_to']['postal_code']);
    if (!empty($duplicate_request)) {
      return;
    }

    $instance = new MoveRequest();
    $data['title'] = 'Move Request';

    $data['field_date'] = array(
      'date' => $data['field_date'],
      'time' => '15:10:00',
    );

    $this->validateApt($data);
    $data['field_parser_provider_id'] = $provider_id;
    $data['field_parser_provider_name'] = $provider_name;
    $data['field_actual_start_time'] = "8:00 AM";
    $data['field_start_time'] = 1;
    $data['field_start_time_max'] = "8:00 PM";
    $data['field_minimum_move_time'] = 1;
    $data['field_maximum_move_time'] = 2;
    $data['field_from_parser'] = 1;
    $data['field_approve'] = 1;
    // TODO Hard coded service type field.
    $data['field_move_service_type'] = 1;
    if (isset($data['field_size_of_move']) && $data['field_size_of_move']) {
      $data['field_useweighttype'] = 3;
      $data['field_customweight'] = round((intval($data['field_size_of_move']) / 7), 2);
      $data['field_size_of_move'] = $this->calcMoveSize((string) $data['field_size_of_move']);
    }
    else {
      $data['field_size_of_move'] = 1;
    }
    $this->checkZips($data);
    DistanceCalculate::checkExistAreaAndState($data);
    $distance = new DistanceCalculate(TRUE);
    // TODO. Добавить расчитанный сервис тип, протестировать парсер.
    $distance->checkLongDistance($data, $provider_id);
    $travel_data = $distance->checkTravelTime($data['field_moving_from']['postal_code'], $data['field_moving_to']['postal_code'], $data['field_move_service_type'], TRUE);
    $data['field_travel_time'] = $travel_data['duration'];
    $data['field_distance'] = $travel_data['distances']['AB']['distance'] ?? 0;
    $data['field_duration'] = $travel_data['distances']['AB']['duration'] ?? 0;
    $basicSettings = json_decode(variable_get('basicsettings', array()));
    $data['field_request_settings'] = array('inventoryVersion' => 2, 'storage_rate' => $basicSettings->storage_rate);
    $data['all_data']['request_distance'] = $travel_data;
    $this->fillEntrance($data);

    // Adding double travel time.
    $data['field_double_travel_time'] = $distance->doubleTravelTime($data);
    $data['field_reservation_price'] = -1;
    $data['all_data']['parser'] = TRUE;
    $data['all_data']['localdiscount'] = $distance->getLocalDiscountByServiceType($data['field_move_service_type']);

    try {
      // Todo add logic for choose branche for create request.
      $branch_on = variable_get('same_parser_settings_for_all_branches');
      $branch = new Branch();
      if ($branch->checkBranchingIsActive() && !empty($branch_on)) {
        $temp = $branch->checkClosestBranch($data['field_moving_from']['postal_code'], $data['field_moving_to']['postal_code']);
        $urlToMigrate = $temp['api_url'] . "/server/move_request";
        $options = array(
          'method' => 'POST',
          'data' => json_encode($data),
          'headers' => array('Content-Type' => 'application/json', 'accept' => 'application/json'),
        );
        $request = drupal_http_request($urlToMigrate, $options);
        if ($request->code == '200' && !empty($request->data)) {
          $result = $request->data;
        }
      }
      else {
        $result = $instance->create2($data, 1);
      }
    }
    finally {
      $user = drupal_anonymous_user();

      if (!isset($result['nid'])) {
        watchdog("Parser", "Error on creating request at time parsing email", array(), WATCHDOG_INFO);
      }
      else {
        $this->writeLogMail($result['nid'], $email);
      }
    }
  }

  public function prepareRandomEmail() {
    $site_name = variable_get('site_name', "no_email");
    $no_space_name = str_replace(' ', '_', $site_name);
    $result_email = $no_space_name . "@" . time() . ".net";

    return $result_email;
  }

  private function checkUserFields(array $data) {
    return isset($data['field_e_mail']) && $data['field_e_mail'];
  }

  /**
   * Helper method for search code of state full name.
   *
   * @param string $full_name
   *   Full name state.
   *
   * @return string
   *   Found key of state otherwise FALSE.
   *
   * @throws \Exception
   */
  public function getStateCode(string $full_name) {
    $full_name = trim($full_name);
    $state_list = Extra::getStateList();
    $result = array_search($full_name, $state_list);
    if (!$result) {
      watchdog("Parser", "Error: Not found code of state by Full State name", array(), WATCHDOG_INFO);
    }
    return $result;
  }

  public function calcMoveSize(string $size) {
    $found = 1;
    $get_size = function (&$size) {
      $matches = array();
      $find = preg_match('/([\d]+)\s*([a-zA-z]+)$/', $size, $matches);
      if ($find && isset($matches[2]) && strtolower($matches[2]) == 'lbs') {
        $size = (int) $matches[1] / 7;
      }
      else {
        $size = (int) $size;
      }
    };

    $calc_settings = json_decode(variable_get('calcsettings', new \stdClass()));
    if (property_exists($calc_settings, 'size')) {
      $get_size($size);
      $calc_size_original = $calc_settings->size;

      // Unset commercial move weight type.
      if (isset($calc_size_original->{11})) {
        unset($calc_size_original->{11});
      }
      $calc_size = $calc_size_original = (array) $calc_size_original;

      $calc_size[] = $size;
      sort($calc_size);
      for ($i = 0, $return = $calc_size[1]; $i < count($calc_size) - 1; $i++) {
        if ($calc_size[$i + 1] == $size) {
          if ($i + 2 >= count($calc_size) || $size - $calc_size[$i] < $calc_size[$i + 2] - $size) {
            $return = $calc_size[$i];
          }
          else {
            $return = $calc_size[$i + 2];
          }
          break;
        }
      }
      $found = array_search($return, $calc_size_original);
      if (!$found) {
        $found = 1;
        watchdog("Parser", "Warning: Calculate Move Size is Wrong", array(), WATCHDOG_INFO);
      }
    }
    else {
      watchdog("Parser", "Error: No such property 'size' in 'calcsettings' variable", array(), WATCHDOG_INFO);
    }

    return $found;
  }

  public function writeLogMail(int $nid, string $mail) {
    $data_prepare = Log::prepare_log_data('mail', t('Request was created from parse system.'));
    $instance = new Log($nid, EntityTypes::MOVEREQUEST);
    $mes_id = $instance->create($data_prepare);
    if ($mes_id) {
      $body_mail['params']['body'] = $mail;
      move_new_log_write_body_mail_log($body_mail, reset($mes_id));
    }
  }

  public function setProviderName(string $name) {
    $this->providerName = $name;
  }

  /**
   * Initialize entrance. For default enter 1|No Stairs - Ground Floor.
   *
   * @param array $data
   *   Request array with filled fields.
   */
  private function fillEntrance(array &$data) {
    if (!empty($data['field_type_of_entrance_from'])) {
      $data['field_type_of_entrance_from'] = static::checkEntrance('field_type_of_entrance_from', $data['field_type_of_entrance_from']);
    }
    else {
      $data['field_type_of_entrance_from'] = 1;
    }

    if (!empty($data['field_type_of_entrance_to_'])) {
      $data['field_type_of_entrance_to_'] = static::checkEntrance('field_type_of_entrance_to_', $data['field_type_of_entrance_to_']);
    }
    else {
      $data['field_type_of_entrance_to_'] = 1;
    }

    $data['all_data']['entrance'] = FALSE;
  }

  /**
   * Set Quote if zips is not exists.
   *
   * @param array $data
   *   Request array with filled fields.
   */
  private function checkZips(array &$data) {
    // "Moving from" and "Moving to".
    if (!$data['field_moving_from']['postal_code'] || !$data['field_moving_to']['postal_code']) {
      $data['all_data'] = array('showQuote' => FALSE, 'isDisplayQuoteEye' => TRUE);
    }
  }

  /**
   * Get entrance allowed values.
   *
   * @return array
   *   Entrance values.
   */
  private static function getEntranceListsValue() {
    $result = array();

    $fields = array(
      'field_type_of_entrance_from',
      'field_type_of_entrance_to_',
    );

    foreach ($fields as $field) {
      $field_info = field_info_field($field);
      $list_values = list_allowed_values($field_info);
      $result[$field] = $list_values;
    }

    return $result;
  }

  /**
   * Check entrance.
   *
   * @param string $field
   *   Entrance field name.
   * @param mixed|null $entrance
   *   Entrance value.
   *
   * @return int|null
   *   Entrance value.
   */
  private static function checkEntrance($field, $entrance = NULL) {
    $entranceValues = static::getEntranceListsValue();
    if (!empty($entrance) && array_key_exists($entrance, $entranceValues[$field])) {
      $result = $entrance;
    }
    return $result ?? 1;
  }

  /**
   * Validate apt values. Cut if string longer 5 char.
   *
   * @param array $data
   *   Data for create request.
   */
  private function validateApt(array &$data) {
    if (!empty($data['field_apt_from']) && strlen($data['field_apt_from']) > 5) {
      $data['field_apt_from'] = substr($data['field_apt_from'], 0, 5);
    }
    if (!empty($data['field_apt_to']) && strlen($data['field_apt_to']) > 5) {
      $data['field_apt_to'] = substr($data['field_apt_to'], 0, 5);
    }
  }

}
