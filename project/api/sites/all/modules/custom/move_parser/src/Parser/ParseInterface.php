<?php

namespace Drupal\move_parser\Parser;

/**
 * Interface ParseInterface.
 *
 * @package Drupal\move_parser\Parser
 */
interface ParseInterface {

  /**
   * Parse email for the selected.
   *
   * @param array $mail
   *   Email to parsing.
   *
   * @return bool
   *   Result of operation.
   */
  public function parse(array $mail) : bool;

  /**
   * Return provider id.
   *
   * @return mixed
   */
  public function getProviderId();

  /**
   * Return provider name.
   *
   * @return mixed
   */
  public function getProviderName();

  /**
   * Find what email is belong to parse provider.
   *
   * @param array $mail
   *   The array with all email fields.
   *
   * @return bool
   *   TRUE if belong, FALSE otherwise.
   */
  public function findBelong(array $mail);

}
