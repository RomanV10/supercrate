<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;
use Symfony\Component\DomCrawler\Crawler;
use Drupal\move_distance_calculate\Services\DistanceCalculate;

/**
 * Class Movers123 provider.
 *
 * @package Drupal\move_parser\Parser
 */
class Movers123 extends ParserExtends implements ParseInterface {

  const PROVIDERID = 0;
  const PROVIDERNAME = '123movers.com';

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool {
    parent::setProviderName($this->getProviderName());
    $result = FALSE;
    if ($this->findBelong($mail)) {
      $data = $this->parseMessage($mail['message_body']);
      $mail_data = $mail['message_body'];
      if (!$data['data']['field_e_mail'] && !$data['data']['field_date'] && (!$data['data']['field_phone'] || !$data['data']['field_additional_phone'])) {
        $data = $this->parseMessageOrigin($mail['original']);
        $mail_data = $mail['original'];
        $this->prepareName($data['data']);
        $this->prepareFieldAddress($data['data']);
      }
      else {
        if ($data['data']['name']) {
          if (is_array($data['data']['name'])) {
            $name = trim(reset($data['data']['name']));
          }
          else {
            $name = trim($data['data']['name']);
          }
          list($first_name, $last_name) = preg_split("/[\s]+/", $name);
          $data['data']['field_first_name'] = $first_name;
          $data['data']['field_last_name'] = $last_name;
          unset($data['data']['name']);
        }
        $prepare_address_field = array(
          'field_moving_from',
          'field_moving_to',
        );
        foreach ($prepare_address_field as $item) {
          $this->prepareFieldAddress2($data['data'], $item);
        }
      }

      $this->prepareFieldDate($data['data']);
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail_data);
      $result = TRUE;
    }

    return $result;
  }

  private function prepareFieldDate(&$data) {
    $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
  }

  private function prepareFieldAddress2(&$data, $field_name) {
    $temp = $data[$field_name];
    unset($data[$field_name]);
    $data[$field_name]['country'] = 'US';
    $data[$field_name]['locality'] = isset($temp[1]) ? $temp[1] : '';
    $data[$field_name]['administrative_area'] = isset($temp[2]) ? $this->getStateCode($temp[2]) : '';
    $data[$field_name]['postal_code'] = isset($temp[3]) ? $temp[3] : '';
  }

  private function prepareName(array &$data) {
    if (isset($data['name']) && $data['name']) {
      list($first_name, $last_name) = explode(" ", trim($data['name']));
      $data['field_first_name'] = $first_name;
      $data['field_last_name'] = $last_name;
    }
    unset($data['name']);
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_zip',
        'moving_from_state',
        'moving_from_city',
      ),
      'field_moving_to' => array(
        'moving_to_zip',
        'moving_to_state',
        'moving_to_city',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'postal_code' => '',
        'administrative_area' => '',
        'locality' => '',
        'country' => '',
        'thoroughfare' => '',
      );

      if (isset($item[0]) && isset($data[$item[0]])) {
        $data[$field_name]['postal_code'] = $data[$item[0]];
        unset($data[$item[0]]);
      }
      if (isset($item[1]) && isset($data[$item[1]])) {
        $data[$field_name]['administrative_area'] = strtoupper($data[$item[1]]);
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['locality'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
      $data[$field_name]['country'] = 'US';

    }
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $subject = trim($mail['from_address']);
    $pattern = '/move@123movers.com/';
    return preg_match($pattern, $subject);
  }

  /**
   * Parse message by rules.
   *
   * @param string $body
   *   Body for parsing.
   *
   * @return array
   *
   * @throws \Exception
   */
  private function parseMessage(string $body) {
    $data = array(
      'name' => '',
      'field_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => '',
      'field_moving_from' => '',
      'field_moving_to' => '',
    );

    $patterns = array(
      'name' => array(
        'preg' => '/(?<=Name:\s)([a-zA-z0-9\s]+)?/i',
        'pos' => 1,
      ),
      'field_phone' => array(
        'preg' => '/(?<=Home\sPhone:\s)([\d-]+)/i',
        'pos' => 1,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=Email:\s)\<a[^>]*>(.*?)\<\/a\>/i',
        'pos' => 1,
      ),
      'field_date' => array(
        'preg' => '/(?<=Move\sDate:\s)([\d]+\/[\d]+\/[\d]+)/i',
        'pos' => 1,
      ),
      'field_size_of_move' => array(
        'preg' => '/(?<=Move\sWeight\:\s)((\d\s\w*)|(\w*))(?=\,\s([\d]+[\s]*[\w]*)?)/i',
        'pos' => 4,
      ),
      'field_moving_from' => array(
        'preg' => '/(?<=Origin:)\s*(\w+|\w+\s*\w+|\w+\s*\w+\s*\w+),\s*(\w+|\w+\s*\w+|\w+\s*\w+\s*\w+)\s*(\d*)\<br \/\>?/i',
      ),
      'field_moving_to' => array(
        'preg' => '/(?<=Destination:)\s*(\w+|\w+\s*\w+|\w+\s*\w+\s*\w+),\s*(\w+|\w+\s*\w+|\w+\s*\w+\s*\w+)\s*(\d*)\<br \/\>?/i',
      ),
    );

    $any = array(
      'name',
      'field_moving_from',
      'field_moving_to',
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      preg_match($patterns[$id]['preg'], $body, $matches);
      if ($matches && array_search($id, $any) !== FALSE) {
        $item = $matches;
      }
      elseif (!empty($patterns[$id]['pos']) && !empty($matches[$patterns[$id]['pos']])) {
        $item = $matches[$patterns[$id]['pos']];
      }
    }

    return array('data' => $data, 'parse' => $body);
  }

  private function parseMessageOrigin(string $body) {
    $crawler = new Crawler($body);
    $crawler = $crawler->filter("table > tr");

    $nodeValues = $crawler->each(
      function (Crawler $node, $i) {
        $first = trim($node->children()->first()->text());
        $last = trim($node->children()->last()->text());
        return array($first, $last);
      }
    );

    $name = $nodeValues[2][1];
    $phone = $nodeValues[4][1];
    $additional_phone = $nodeValues[5][1];
    $e_mail = $nodeValues[6][1];
    $field_date = $nodeValues[8][1];

    $from = $nodeValues[10][1];
    $from_city = array();
    preg_match('/([\w]*\s*[\w]*)/i', $from, $from_city);
//    $from_state = array();
//    preg_match('/(?<=[\w+])\s*\w*\,\s*(\w*)/i', $from, $from_state);
    $from_zip = array();
    preg_match('/([\d]+)/i', $from, $from_zip);
    $from_state = DistanceCalculate::getStateByZip($from_zip[1]);

    $to = $nodeValues[11][1];
    $to_city = array();
    preg_match('/([\w]*\s*[\w]*)/i', $to, $to_city);
//    $to_state = array();
//    preg_match('/(?<=[\w+])\s*\w*\,\s*(\w*)/i', $to, $to_state);
    $to_zip = array();
    preg_match('/([\d]+)/i', $to, $to_zip);
    $to_state = DistanceCalculate::getStateByZip($to_zip[1]);

    $size_of_move = $nodeValues[9][1];
    $field_size_of_move = array();
    preg_match('/(?<=[\w+])\s*\w*\,\s*(\w*)/i', $size_of_move, $field_size_of_move);

    $data = array(
      'name' => $name,
      'field_phone' => $phone,
      'field_additional_phone' => $additional_phone,
      'field_e_mail' => $e_mail,
      'field_date' => $field_date,
      'moving_from_city' => trim($from_city[1]),
      'moving_from_state' => strtoupper(trim($from_state)),
      'moving_from_zip' => trim($from_zip[1]),
      'moving_to_city' => trim($to_city[1]),
      'moving_to_state' => strtoupper(trim($to_state)),
      'moving_to_zip' => trim($to_zip[1]),
      'field_size_of_move' => isset($field_size_of_move[1]) ? $field_size_of_move[1] . 'lbs' : 0,
      'field_additional_comments' => '',
    );

    return array('data' => $data, 'parse' => $body);
  }

}
