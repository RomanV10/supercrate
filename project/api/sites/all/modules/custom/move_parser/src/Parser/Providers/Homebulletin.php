<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Movers.
 *
 * @package Drupal\move_parser\Parser
 */
class Homebulletin extends ParserExtends implements ParseInterface {

  const PROVIDERID = 10;
  const PROVIDERNAME = 'Homebulletin';

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool {parent::setProviderName($this->getProviderName());
    $result = FALSE;
    if ($this->findBelong($mail)) {
      $data = $this->parseMessage($mail['original']);
      $this->prepareName($data['data']);
      $this->prepareFieldDate($data['data']);
      $this->prepareFieldAddress($data['data']);
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail['original']);
      $result = TRUE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $subject = $mail['from_address'];
    $pattern = '/info@homebulletin.net/i';
    return preg_match($pattern, $subject);
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  private function parseMessage(string $body) : array {
    $body = preg_replace('/(windows-1251)/i', 'utf-8', $body);
    $crawler = new Crawler($body);
    $nodeValues = $crawler->filter("tbody > tr > td")->extract(['_text']);
    if (empty($nodeValues)) {
      $nodeValues = $crawler->filter("table > tr > td")->extract(['_text']);
    }

    $name = isset($nodeValues[5]) ? $nodeValues[5] : '';
    $last_name = isset($nodeValues[9]) ? $nodeValues[9] : '';
    $phone = isset($nodeValues[7]) ? $nodeValues[7] : '';
    $additional_phone = isset($nodeValues[11]) ? $nodeValues[11] : '';
    $e_mail = isset($nodeValues[13]) ? $nodeValues[13] : '';
    $field_date = isset($nodeValues[19]) ? $nodeValues[19] : '';
    $from_city = isset($nodeValues[23]) ? $nodeValues[23] : '';
    $from_state = isset($nodeValues[27]) ? $nodeValues[27] : '';
    $from_zip = isset($nodeValues[31]) ? $nodeValues[31] : '';
    $to_city = isset($nodeValues[25]) ? $nodeValues[25] : '';
    $to_state = isset($nodeValues[29]) ? $nodeValues[29] : '';
    $to_zip = isset($nodeValues[33]) ? $nodeValues[33] : '';
    $size_of_move = isset($nodeValues[17]) ? $nodeValues[17] : 0;
    $field_size_of_move = array();
    preg_match('/(\d*\s*lbs)/i', $size_of_move, $field_size_of_move);

    $data = array(
      'name' => trim($name),
      'last_name' => trim($last_name),
      'field_phone' => trim($phone),
      'field_additional_phone' => trim($additional_phone),
      'field_e_mail' => trim($e_mail),
      'field_date' => trim($field_date),
      'moving_from_city' => trim($from_city),
      'moving_from_state' => trim($from_state),
      'moving_from_zip' => trim($from_zip),
      'moving_to_city' => trim($to_city),
      'moving_to_state' => trim($to_state),
      'moving_to_zip' => trim($to_zip),
      'field_size_of_move' => trim($field_size_of_move[0]),
      'field_additional_comments' => '',
    );

    return array('data' => $data, 'parse' => $body);
  }

  private function prepareName(array &$data) {
    if (isset($data['name']) && $data['name']) {
      list($first_name, $last_name) = preg_split("/[\s]+/", trim($data['name']));
      $data['field_first_name'] = $first_name;
      $data['field_last_name'] = $last_name;
    }
    if (empty($data['field_last_name'])) {
      $data['field_last_name'] = $data['last_name'];
    }
    unset($data['name']);
    unset($data['last_name']);
  }

  private function prepareFieldDate(&$data) {
    $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_zip',
        'moving_from_state',
        'moving_from_city',
      ),
      'field_moving_to' => array(
        'moving_to_zip',
        'moving_to_state',
        'moving_to_city',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'postal_code' => '',
        'administrative_area' => '',
        'locality' => '',
        'country' => '',
        'thoroughfare' => '',
      );

      if (isset($item[0]) && isset($data[$item[0]])) {
        $data[$field_name]['postal_code'] = $data[$item[0]];
        unset($data[$item[0]]);
      }
      if (isset($item[1]) && isset($data[$item[1]])) {
        $data[$field_name]['administrative_area'] = strtoupper($data[$item[1]]);
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['locality'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
      $data[$field_name]['country'] = 'US';

    }
  }

}
