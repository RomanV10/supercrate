<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Movers.
 *
 * @package Drupal\move_parser\Parser
 */
class Webfixe extends ParserExtends implements ParseInterface {

  const PROVIDERID = 8;
  const PROVIDERNAME = 'Webfix';

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool {
    parent::setProviderName($this->getProviderName());
    $result = FALSE;
    if ($this->findBelong($mail)) {
      $data = $this->parseMessage($mail['original']);
      $this->prepareName($data['data']);
      $this->prepareFieldDate($data['data']);
      $this->prepareFieldAddress($data['data']);
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail['original']);
      $result = TRUE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $subject = $mail['from_address'];
    $pattern = '/leads@webfixe.com/';
    return preg_match($pattern, $subject);
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  private function parseMessage(string $body) : array {
    $crawler = new Crawler($body);
    $crawler = $crawler->filter("table > tr");

    $nodeValues = $crawler->each(
      function (Crawler $node, $i) {
        $first = trim($node->children()->first()->text());
        $last = trim($node->children()->last()->text());
        return array($first, $last);
      }
    );

    $name = $nodeValues[0][1];
    $phone = $nodeValues[2][1];
    $additional_phone = $nodeValues[3][1];
    $e_mail = $nodeValues[1][1];
    $field_date = $nodeValues[11][1];
    $from_city = $nodeValues[5][1];
    $from_state = $nodeValues[6][1];
    $from_zip = $nodeValues[4][1];
    $to_city = $nodeValues[8][1];
    $to_state = $nodeValues[9][1];
    $to_zip = $nodeValues[7][1];
    $field_size_of_move = $nodeValues[10][1];

    $data = array(
      'name' => $name,
      'field_phone' => $phone,
      'field_additional_phone' => $additional_phone,
      'field_e_mail' => $e_mail,
      'field_date' => $field_date,
      'moving_from_city' => $from_city,
      'moving_from_state' => $from_state,
      'moving_from_zip' => $from_zip,
      'moving_to_city' => $to_city,
      'moving_to_state' => $to_state,
      'moving_to_zip' => $to_zip,
      'field_size_of_move' => 0,
      'field_additional_comments' => '',
    );

    return array('data' => $data, 'parse' => $body);
  }

  private function prepareName(array &$data) {
    if (isset($data['name']) && $data['name']) {
      list($first_name, $last_name) = preg_split("/[\s]+/", trim($data['name']));
      $data['field_first_name'] = $first_name;
      $data['field_last_name'] = $last_name;
    }
    unset($data['name']);
  }

  private function prepareFieldDate(&$data) {
    $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_zip',
        'moving_from_state',
        'moving_from_city',
      ),
      'field_moving_to' => array(
        'moving_to_zip',
        'moving_to_state',
        'moving_to_city',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'postal_code' => '',
        'administrative_area' => '',
        'locality' => '',
        'country' => '',
        'thoroughfare' => '',
      );

      if (isset($item[0]) && isset($data[$item[0]])) {
        $data[$field_name]['postal_code'] = $data[$item[0]];
        unset($data[$item[0]]);
      }
      if (isset($item[1]) && isset($data[$item[1]])) {
        $data[$field_name]['administrative_area'] = strtoupper($data[$item[1]]);
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['locality'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
      $data[$field_name]['country'] = 'US';

    }
  }

}
