<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;

/**
 * Class Movers.
 *
 * @package Drupal\move_parser\Parser
 */
class QuoteRunnerLead extends ParserExtends implements ParseInterface {

  const PROVIDERID = 15;
  const PROVIDERNAME = 'QuoteRunnerLead';

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool {
    parent::setProviderName($this->getProviderName());
    $result = FALSE;
    if ($this->findBelong($mail)) {
      $data = $this->parseMessage($mail['original']);
      $this->prepareName($data['data']);
      $this->prepareFieldDate($data['data']);
      $this->prepareSizeOfMove($data['data']);
      $this->prepareFieldAddress($data['data']);
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail['original']);
      $result = TRUE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $subject = $mail['message_subject'];
    $pattern = '/Quote Runner Lead/i';
    return preg_match($pattern, $subject);
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  /**
   * Parse mail message.
   *
   * @param string $body
   *   Mail data.
   *
   * @return array
   *   Data.
   */
  private function parseMessage(string $body) : array {
    $data = array(
      'name' => '',
      'field_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => 0,
      'moving_from_state' => '',
      'moving_from_city' => '',
      'moving_from_zip' => '',
      'moving_to_zip' => '',
      'moving_to_city' => '',
      'moving_to_state' => '',
    );

    $patterns = array(
      'name' => array(
        'preg' => '/(?<=Customer Name)\s*:\s*(\w+\s\w+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_phone' => array(
        'preg' => '/(?<=Phone)\s*:\s*(\d*)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=Email)\s*:\s*\<a[^>]+>(.*?)<\/a>/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_date' => array(
        'preg' => '/(?<=Move Date)\s*:\s*(\d*-\d*-\d*)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_size_of_move' => array(
        'preg' => '/(\d*\s*lbs)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_from_state' => array(
        'preg' => '/(?<=State)\s*:\s*(\w*)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_from_city' => array(
        'preg' => '/(?<=City)\s*:\s*([\w\s]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_from_zip' => array(
        'preg' => '/(?<=Zip)\D*(\d*)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_to_zip' => array(
        'preg' => '/(?<=Zip)\D*(\d*)/i',
        'pos' => 1,
        'id' => 1,
      ),
      'moving_to_city' => array(
        'preg' => '/(?<=City)\s*:\s*([\w\s]+)/i',
        'pos' => 1,
        'id' => 1,
      ),
      'moving_to_state' => array(
        'preg' => '/(?<=State)\s*:\s*(\w*)/i',
        'pos' => 1,
        'id' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match_all($patterns[$id]['preg'], $body, $matches);
      if ($match && $matches) {
        $item = trim($matches[$patterns[$id]['pos']][$patterns[$id]['id']]);
      }
    }

    return array('data' => $data, 'parse' => $body);
  }

  /**
   * Prepare client name.
   *
   * @param array $data
   *   Parse data.
   */
  private function prepareName(array &$data) : void {
    if (isset($data['name']) && $data['name']) {
      list($first_name, $last_name) = preg_split("/[\s]+/", trim($data['name']));
      $data['field_first_name'] = $first_name;
      $data['field_last_name'] = $last_name;
    }
    unset($data['name']);
  }

  /**
   * Prepare field date.
   *
   * @param array $data
   *   Parse data.
   */
  private function prepareFieldDate(array &$data) : void {
    $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
  }

  /**
   * Prepare field size of move.
   *
   * @param array $data
   *   Parse data.
   */
  private function prepareSizeOfMove(array &$data) : void {
    if (isset($data['field_size_of_move']) && $data['field_size_of_move']) {
      $data['field_size_of_move'] = str_replace(',', '', $data['field_size_of_move']);
    }
  }

  /**
   * Prepare field address.
   *
   * @param array $data
   *   Parse data.
   */
  private function prepareFieldAddress(array &$data) : void {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_state',
        'moving_from_city',
        'moving_from_zip',
      ),
      'field_moving_to' => array(
        'moving_to_state',
        'moving_to_city',
        'moving_to_zip',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'country' => 'US',
        'locality' => '',
        'administrative_area' => '',
        'postal_code' => '',
      );
      if (isset($data[$item[0]])) {
        $data[$field_name]['administrative_area'] = strtoupper($data[$item[0]]);
        unset($data[$item[0]]);
      }
      if (isset($data[$item[1]])) {
        $data[$field_name]['locality'] = $data[$item[1]];
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['postal_code'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
    }
  }

}
