<?php

namespace Drupal\move_parser\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_parser\Parser\VendorDetails;
use Drupal\move_mail\System\Common;
use Drupal\move_parser\Parser\ParserExtends;

/**
 * Class Parser.
 *
 * @package Drupal\move_parser\Services
 */
class Parser extends BaseService {


  public function __construct() {}

  public function create($data = array()) {
    $data['password'] = Common::encryptPassword($data['password']);
    variable_set('parser_data', $data);
    return TRUE;
  }

  public function retrieve() {}

  public function update($data = array()) {}

  public function delete() {}

  public function index() {
    $data = variable_get('parser_data');
    $data['password'] = Common::decryptPassword($data['password']);
    return $data;
  }

  public function createVendor(int $provider_id, int $active = 0, string $company_name, $info = array()) {
    $data = array(
      'provider_id' => $provider_id,
      'active' => $active,
      'company_name' => $company_name,
      'info' => $info,
    );

    return VendorDetails::registerVendor($data);
  }

  /**
   * Create request from leads post action.
   *
   * @param array $lead_data
   *   Data for create request.
   *
   * @return bool|string
   *   True if request created.
   *
   * @throws \Exception
   */
  public function createLeadParsing(array $lead_data) {
    $result = FALSE;
    if ($lead_data['company_name']) {
      $data = $this->prepareLeadParsingData($lead_data);
      $this->prepareFieldAddress($data);

      if (empty($data['field_date']) || !$this->checkDateFormat($data['field_date'])) {
        return "Error. Received field_date: {$data['field_date']}, need format 'YYYY-MM-DD'";
      }

      if (empty($data['field_e_mail']) && empty($data['field_phone'])) {
        return "Error. Empty field_e_mail and field_phone";
      }

      $instance = new ParserExtends();
      $duplicate_request = static::checkDuplicateRequests($data['field_date'], $data['field_e_mail'], $data['field_moving_from']['postal_code'], $data['field_moving_to']['postal_code']);

      if (empty($duplicate_request)) {
        $log = $this->arrayToStringLog($lead_data);
        $instance->createRequest($data, $data['field_parser_provider_id'], $data['field_parser_provider_name'], $data, $log);
        $result = TRUE;
      }
      else {
        $result = 'Duplicate request';
      }
    }
    return $result;
  }

  /**
   * Check format date is "YYYY-MM-DD".
   *
   * @param string $date
   *   Date.
   *
   * @return bool
   *   True format or invalid.
   */
  public function checkDateFormat(string $date = '') {
    return (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date)) ? TRUE : FALSE;
  }

  /**
   * Prepare data for create move request.
   *
   * @param array $lead_data
   *   Array data.
   *
   * @return array
   *   Prepared data.
   */
  public function prepareLeadParsingData(array $lead_data) {
    $data = array(
      'field_parser_provider_id' => isset($lead_data['provider_id']) ? $lead_data['provider_id'] : FALSE,
      'field_parser_provider_name' => isset($lead_data['company_name']) ? $lead_data['company_name'] : '',
      'field_first_name' => isset($lead_data['field_first_name']) ? $lead_data['field_first_name'] : '',
      'field_last_name' => isset($lead_data['field_last_name']) ? $lead_data['field_last_name'] : '',
      'field_phone' => isset($lead_data['field_phone']) ? $lead_data['field_phone'] : '',
      'field_e_mail' => isset($lead_data['field_e_mail']) ? $lead_data['field_e_mail'] : FALSE,
      'field_date' => isset($lead_data['field_date']) ? $lead_data['field_date'] : FALSE,
      'field_size_of_move' => isset($lead_data['field_size_of_move']) ? $lead_data['field_size_of_move'] : '',
      'field_additional_comments' => isset($lead_data['field_additional_comments']) ? $lead_data['field_additional_comments'] : '',
      'moving_from_state' => isset($lead_data['moving_from_state']) ? $lead_data['moving_from_state'] : '',
      'moving_from_city' => isset($lead_data['moving_from_city']) ? $lead_data['moving_from_city'] : '',
      'moving_from_zip' => isset($lead_data['moving_from_zip']) ? $lead_data['moving_from_zip'] : '',
      'thoroughfare_from' => $lead_data['thoroughfare_from'] ?? '',
      'moving_to_state' => isset($lead_data['moving_to_state']) ? $lead_data['moving_to_state'] : '',
      'moving_to_city' => isset($lead_data['moving_to_city']) ? $lead_data['moving_to_city'] : '',
      'moving_to_zip' => isset($lead_data['moving_to_zip']) ? $lead_data['moving_to_zip'] : '',
      'thoroughfare_to' => $lead_data['thoroughfare_to'] ?? '',
      'field_apt_from' => $lead_data['field_apt_from'] ?? '',
      'field_apt_to' => $lead_data['field_apt_to'] ?? '',
      'field_type_of_entrance_from' => $lead_data['field_type_of_entrance_from'] ?? '',
      'field_type_of_entrance_to_' => $lead_data['field_type_of_entrance_to_'] ?? '',
    );

    return $data;
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_zip',
        'moving_from_state',
        'moving_from_city',
        'thoroughfare_from',
      ),
      'field_moving_to' => array(
        'moving_to_zip',
        'moving_to_state',
        'moving_to_city',
        'thoroughfare_to',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'postal_code' => '',
        'administrative_area' => '',
        'locality' => '',
        'country' => '',
        'thoroughfare' => '',
      );

      if (isset($item[0]) && isset($data[$item[0]])) {
        $data[$field_name]['postal_code'] = $data[$item[0]];
        unset($data[$item[0]]);
      }
      if (isset($item[1]) && isset($data[$item[1]])) {
        $data[$field_name]['administrative_area'] = $data[$item[1]];
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['locality'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
      if (isset($item[3]) && isset($data[$item[3]])) {
        $data[$field_name]['thoroughfare'] = $data[$item[3]];
        unset($data[$item[3]]);
      }
      $data[$field_name]['country'] = 'US';
    }
  }

  /**
   * Check exist duplicate request in system.
   *
   * @param string $move_date
   *   Moving date.
   * @param string $email
   *   User email.
   * @param string $zip_from
   *   Origin zip.
   * @param string $zip_to
   *   Destination zip.
   *
   * @return array
   *   Request nids.
   */
  public static function checkDuplicateRequests(string $move_date, string $email, string $zip_from, string $zip_to = '') {
    $result = array();
    if (!empty($move_date) && !empty($email) && !empty($zip_from)) {
      $result = db_query("
          SELECT nid
            FROM node n
            INNER JOIN field_data_field_date fdfd ON n.nid = fdfd.entity_id
            INNER JOIN field_data_field_e_mail fdfem ON n.nid = fdfem.entity_id
            INNER JOIN field_data_field_moving_from fdfmf ON n.nid = fdfmf.entity_id
            INNER JOIN field_data_field_moving_to fdfmt ON n.nid = fdfmt.entity_id
          WHERE fdfd.field_date_value
           BETWEEN :move_date_from AND :move_date_to
            AND fdfem.field_e_mail_email = :email 
            AND fdfmf.field_moving_from_postal_code = :zip_from 
            AND fdfmt.field_moving_to_postal_code = :zip_to",
        array(
          ':move_date_from' => "$move_date 00:00:00",
          ':move_date_to' => "$move_date 23:59:59",
          ':email' => $email,
          ':zip_from' => $zip_from,
          ':zip_to' => $zip_to,
        ))
        ->fetchAll();
    }
    return $result;
  }

  /**
   * Check required fields for create request.
   *
   * @param array $data
   *   Parse data.
   *
   * @return bool
   *   True if required data don't exist.
   */
  public static function emptyRequiredParseData(array $data) {
    if (empty($data['field_first_name']) &&
        empty($data['field_last_name']) &&
        empty($data['field_phone']) &&
        empty($data['field_e_mail']) &&
        empty($data['field_moving_from']['postal_code']) &&
        empty($data['field_moving_to']['postal_code'])) {
      return TRUE;
    }
  }

  /**
   * Prepare string log from lead company data.
   *
   * @param array $read_data
   *   Lead data.
   *
   * @return string
   *   Log data.
   */
  public function arrayToStringLog(array $read_data) :string {
    $log = $sep = '';
    foreach ($read_data as $key => $value) {
      if (!empty($key)) {
        $log .= $sep . $key . ' : ' . $value;
        $sep = '<br>';
      }
    }

    return $log;
  }

}
