<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;

/**
 * Class EquateMedia.
 *
 * @package Drupal\move_parser\Parser
 */
class EquateMedia extends ParserExtends implements ParseInterface {

  const PROVIDERID = 4;
  const PROVIDERNAME = 'Equatemedia.com';

  /**
   * Internal variable: 0 - xml type, 1 - text.
   *
   * @var int
   */
  private $type = 0;

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool {
    $result = FALSE;
    parent::setProviderName($this->getProviderName());
    if ($this->findBelong($mail)) {
      $data = $this->parseMessage($mail['message_body']);
      if (empty($data['data']['moving_from_zip']) && empty($data['data']['moving_to_zip'])) {
        $data = $this->parseTxt2($mail['message_body']);
      }
      $this->prepareFieldDate($data['data']);
      $this->prepareFieldAddress($data['data']);
      $this->prepareSizeOfMove($data['data']);
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail['original']);
      $result = TRUE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $subject = trim($mail['message_subject']);
    $pattern = '/Equate Media/i';
    return preg_match($pattern, $subject);
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  private function parseMessage(string $body) : array {
    $check_xml = simplexml_load_string($body, "SimpleXMLElement", LIBXML_NOERROR);
    return ($check_xml !== FALSE) ? $this->parseXml($body) : $this->parseTxt($body);
  }

  private function parseXml(string $body) : array {
    $data = array(
      'field_first_name' => '',
      'field_last_name' => '',
      'field_phone' => '',
      'field_additional_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'moving_from_city' => '',
      'moving_from_state' => '',
      'moving_from_zip' => '',
      'moving_from_country' => '',
      'moving_to_city' => '',
      'moving_to_state' => '',
      'moving_to_zip' => '',
      'moving_to_country' => '',
      'field_size_of_move' => '',
      'field_delivery_date_from' => '',
      'field_delivery_date_to' => '',
      'field_additional_comments' => '',
    );

    $patterns = array(
      'field_first_name' => array(
        'preg' => '#(?<=FirstName\>)(.*)(?=\<).*(?=FirstName\>)#i',
        'pos' => 1,
      ),
      'field_last_name' => array(
        'preg' => '#(?<=LastName\>)(.*)(?=\<).*(?=LastName\>)#i',
        'pos' => 1,
      ),
      'field_phone' => array(
        'preg' => '#(?<=(?:Phone_Day|HomePhone)\>)(?:\<a[^\>]*\>)?([^\<]*).*(?=Phone_Day|HomePhone)#i',
        'pos' => 1,
      ),
      'field_additional_phone' => array(
        'preg' => '#(?<=(?:Phone_Eve|WorkPhone|CellPhone)\>)(?:\<a[^\>]*\>)?([^\<]*).*(?=Phone_Eve|WorkPhone|CellPhone)#i',
        'pos' => 1,
      ),
      'field_e_mail' => array(
        'preg' => '#(?<=Email)[\w\s]*\>(?:\<a[^>]*\>)?(.*?)(?:\<\/a\>)?(?=\<\/).*(?=Email)[\w\s]*\>#i',
        'pos' => 1,
      ),
      'field_date' => array(
        'preg' => '#(?<=Date).*?(?=\d)([^\<]*)(?<=\d)(?=\<).*(?=Date\>)#i',
        'pos' => 1,
      ),
      'moving_from_city' => array(
        'preg' => '#(?:OriginCity|(?:From_?)?City)\>([^\<]*)(?=\<).*(?=OriginCity|(?:From_?)?City\>)#i',
        'pos' => 1,
      ),
      'moving_from_state' => array(
        'preg' => '#(?:OriginState|(?:From_?)?State)\>([^\<]*)(?=\<).*(?=OriginState|(?:From_?)?State\>)#i',
        'pos' => 1,
      ),
      'moving_from_zip' => array(
        'preg' => '#(?:OriginPostalCode|(?:From_?)?Zip)\>([^\<]*)(?=\<).*(?=OriginPostalCode|(?:From_?)?Zip\>)#i',
        'pos' => 1,
      ),
      'moving_from_country' => array(
        'preg' => '#(?:OriginCountry|(?:From_?)?Country)\>(.*)(?=\<).*(?=OriginCountry|(?:From_?)?Country\>)#i',
        'pos' => 1,
      ),

      'moving_to_city' => array(
        'preg' => '#(?:DestinationCity|To_?City)\>(.*)(?=\<).*(?=(?:DestinationCity|To_?City)\>)#i',
        'pos' => 1,
      ),
      'moving_to_state' => array(
        'preg' => '#(?:DestinationState|To_?State)\>(.*)(?=\<).*(?=(?:DestinationState|To_?State)\>)#i',
        'pos' => 1,
      ),
      'moving_to_zip' => array(
        'preg' => '#(?:DestinationPostalCode|To_?Zip|DeliveryZip)\>(.*)(?=\<).*(?=(?:DestinationPostalCode|To_?Zip|DeliveryZip)\>)#i',
        'pos' => 1,
      ),
      'moving_to_country' => array(
        'preg' => '#(?:DestinationCountry|To_?Country)\>(.*)(?=\<).*(?=DestinationCountry|To_?Country\>)#i',
        'pos' => 1,
      ),
      'field_size_of_move' => array(
        'preg' => '#(?:Weight|Size)\>(.*)(?=\<).*(?=Weight|Size\>)#i',
        'pos' => 1,
      ),
      'field_delivery_date_from' => array(
        'preg' => '#(?:RequestedLoadDate).*?(?=\d)(.*)(?<=\d)(?=\<).*(?=RequestedLoadDate\>)#i',
        'pos' => 1,
      ),
      'field_delivery_date_to' => array(
        'preg' => '#(?:LeadProviderReceivedDate)\>(.*)(?=\<).*(?=LeadProviderReceivedDate\>)#i',
        'pos' => 1,
      ),
      'field_additional_comments' => array(
        'preg' => '#(?:Comments?)\>(.*)(?=\<).*(?=Comments?\>)#i',
        'pos' => 1,
      ),


    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match($patterns[$id]['preg'], $body, $matches);
      if ($match && $matches) {
        $item = $matches[$patterns[$id]['pos']];
      }
    }

    return array('data' => $data, 'parse' => $body);
  }

  private function parseTxt(string $body) : array {
    $this->type = 1;

    $data = array(
      'name' => '',
      'field_phone' => '',
      'field_additional_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'moving_from_city' => '',
      'moving_from_state' => '',
      'moving_from_zip' => '',
      'moving_to_city' => '',
      'moving_to_state' => '',
      'moving_to_zip' => '',
    );

    $patterns = array(
      'name' => array(
        'preg' => '/(?<=Name)\s*\:\s*([\w\s\-]+)/i',
        'pos' => 1,
      ),
      'field_phone' => array(
        'preg' => '/(?<=Phone)\s*:\s*([\w ]+)/i',
        'pos' => 1,
      ),
      'field_additional_phone' => array(
        'preg' => '/(?<=EXT :)\s([\w ]+)\<\/p\>/i',
        'pos' => 1,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=mailto:)(.*?)(?=\")/i',
        'pos' => 1,
      ),
      'field_date' => array(
        'preg' => '/(?<=Move Date)\s*:\s*(\d*-\d*-\d*)/i',
        'pos' => 1,
      ),
      'moving_from_city' => array(
        'preg' => '/(?<=From City :)\s([\w ]+)/i',
        'pos' => 1,
      ),
      'moving_from_state' => array(
        'preg' => '/(?<=From State :)\s(\S{2})/i',
        'pos' => 1,
      ),
      'moving_from_zip' => array(
        'preg' => '/(?<=From Zip :)\s(\d+)/i',
        'pos' => 1,
      ),
      'moving_to_city' => array(
        'preg' => '/(?<=To City :)\s([\w ]+)/i',
        'pos' => 1,
      ),
      'moving_to_state' => array(
        'preg' => '/(?<=To State :)\s(\w{2})/i',
        'pos' => 1,
      ),
      'moving_to_zip' => array(
        'preg' => '/(?<=To Zip :)\s(\d+)/i',
        'pos' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match($patterns[$id]['preg'], $body, $matches);
      if ($match && $matches) {
        $item = $matches[$patterns[$id]['pos']];
      }
    }

    if (!empty($data['name'])) {
      $name = preg_split("/[\s]+/", trim($data['name']));
      $data['field_first_name'] = $name[0] ?? "";
      $data['field_last_name'] = $name[1] ?? "";
    }
    unset($data['name']);
    return array('data' => $data, 'parse' => $body);
  }

  private function parseTxt2(string $body) : array {
    $this->type = 1;

    $data = array(
      'field_first_name' => '',
      'field_last_name' => '',
      'field_phone' => '',
      'field_additional_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => 0,
      'moving_from_city' => '',
      'moving_from_state' => '',
      'moving_from_zip' => '',
      'moving_to_city' => '',
      'moving_to_state' => '',
      'moving_to_zip' => '',
    );

    $patterns = array(
      'field_first_name' => array(
        'preg' => '/(?<=First Name:)\s*([\w]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_last_name' => array(
        'preg' => '/(?<=Last Name:)\s*([\w]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_phone' => array(
        'preg' => '/(?<=Telephone)\s*:\s*([\w]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_additional_phone' => array(
        'preg' => '/(?<=Work)\s*:\s*([\w]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=mailto:)(.*?)(?=\")/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_date' => array(
        'preg' => '/(?<=Move Date)\s*:\s*(\d*-\d*-\d*)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'field_size_of_move' => array(
        'preg' => '/(?<=WEIGHT:)\s*([\w]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_from_city' => array(
        'preg' => '/(?<=City)\s*:\s*([\w]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_from_state' => array(
        'preg' => '/(?<=STATE)\s*:\s*([\w]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_from_zip' => array(
        'preg' => '/(?<=ZIP)\s*:\s*([\w]+)/i',
        'pos' => 1,
        'id' => 0,
      ),
      'moving_to_city' => array(
        'preg' => '/(?<=City)\s*:\s*([\w]+)/i',
        'pos' => 1,
        'id' => 1,
      ),
      'moving_to_state' => array(
        'preg' => '/(?<=STATE)\s*:\s*([\w]+)/i',
        'pos' => 1,
        'id' => 1,
      ),
      'moving_to_zip' => array(
        'preg' => '/(?<=ZIP)\s*:\s*([\w]+)/i',
        'pos' => 1,
        'id' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match_all($patterns[$id]['preg'], $body, $matches);
      if ($match && $matches) {
        $item = trim($matches[$patterns[$id]['pos']][$patterns[$id]['id']]);
      }
    }

    return array('data' => $data, 'parse' => $body);
  }

  private function prepareSizeOfMove(array &$data) {
    if (isset($data['field_size_of_move']) && $data['field_size_of_move']) {
      $matches = array();
      $pattern = '/([\d]+)[\D]+([\d]+)\s*([a-zA-z]+)$/i';
      preg_match($pattern, $data['field_size_of_move'], $matches);
      if (isset($matches[0]) && isset($matches[1]) && isset($matches[2])) {
        $size = ((int) $matches[0] + (int) $matches[1]) / 2;
        $data['field_size_of_move'] = "$size {$matches[3]}";
      }
    }
  }

  private function prepareFieldDate(&$data) {
    $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_zip',
        'moving_from_state',
        'moving_from_city',
        'moving_from_country',
      ),
      'field_moving_to' => array(
        'moving_to_zip',
        'moving_to_state',
        'moving_to_city',
        'moving_to_country',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'postal_code' => '',
        'administrative_area' => '',
        'locality' => '',
        'country' => '',
        'thoroughfare' => '',
      );

      if (isset($item[0]) && isset($data[$item[0]])) {
        $data[$field_name]['postal_code'] = $data[$item[0]];
        unset($data[$item[0]]);
      }
      if (isset($item[1]) && isset($data[$item[1]])) {
        $data[$field_name]['administrative_area'] = strtoupper($data[$item[1]]);
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['locality'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
      if (isset($item[3]) && isset($data[$item[3]])) {
        $data[$field_name]['country'] = 'US';
        unset($data[$item[3]]);
      }
    }
  }

}
