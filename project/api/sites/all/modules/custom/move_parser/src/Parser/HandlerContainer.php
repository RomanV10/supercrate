<?php

namespace Drupal\move_parser\Parser;

/**
 * Class HandlerContainer.
 *
 * @package Drupal\move_parser\Parser
 */
class HandlerContainer extends ParserExtends {

  /**
   * Array handlers.
   *
   * @var ParseInterface[]
   */
  protected $handlers = [];

  public function execute(array $mail) {
    $result = FALSE;

    foreach ($this->handlers as $handler) {
      $result = $handler->parse($mail);
      if ($result) {
        break;
      }
    }

    if (!$result) {
      watchdog("Parser", "Handler not found for downloaded email", array(), WATCHDOG_INFO);
    }

    return $result;
  }

  public function addHandler(ParseInterface $handler) {
    $this->handlers[] = $handler;
  }

  public function findProviderId($id) {
    $result = FALSE;

    foreach ($this->handlers as $handler) {
      $provider_id = $handler->getProviderId();
      if ($provider_id == $id) {
        $result = TRUE;
        break;
      }
    }

    return $result;
  }

}
