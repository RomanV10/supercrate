<?php

namespace Drupal\move_parser\Parser\Providers;

use Drupal\move_parser\Parser\ParseInterface;
use Drupal\move_parser\Parser\ParserExtends;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Moving.com.
 *
 * @package Drupal\move_parser\Parser
 */
class Irelocate extends ParserExtends implements ParseInterface {

  const PROVIDERID = 9;
  const PROVIDERNAME = 'Irelocate.net';

  /**
   * {@inheritdoc}
   */
  public function parse(array $mail) : bool {
    parent::setProviderName($this->getProviderName());
    $result = FALSE;

    if ($this->findBelong($mail)) {
      $data = $this->parseMessage($mail['original']);
      if (!$data['data']['field_e_mail'] && !$data['data']['field_date']) {
        $data = $this->parseMessageHtml($mail['original']);
      }
      $this->preparePhone($data['data']);
      $this->prepareFieldDate($data['data']);
      $this->prepareFieldAddress($data['data']);
      $this->prepareSizeOfMove($data['data']);
      $this->clearData($data['data']);
      $this->createRequest($data['data'], $this->getProviderId(), $this->getProviderName(), array($data['parse']), $mail['original']);
      $result = TRUE;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderId() {
    return self::PROVIDERID;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName() {
    return self::PROVIDERNAME;
  }

  private function prepareFieldDate(&$data) {
    $data['field_date'] = date('Y-m-d', strtotime($data['field_date']));
  }

  /**
   * {@inheritdoc}
   */
  public function findBelong(array $mail) {
    $subject = trim($mail['from_address']);
//    $subject = trim($mail['message_body']);
    $pattern = '/irelocate.net/i';
    return preg_match($pattern, $subject);
  }

  private function parseMessage(string $parse_data) : array {
    $data = array(
      'field_first_name' => '',
      'field_last_name' => '',
      'work_phone' => '',
      'home_phone' => '',
      'mobile_phone' => '',
      'field_e_mail' => '',
      'field_date' => '',
      'field_size_of_move' => '',
      'moving_from_state' => '',
      'moving_from_city' => '',
      'moving_from_zip' => '',
      'moving_to_state' => '',
      'moving_to_city' => '',
      'moving_to_zip' => '',
    );

    $patterns = array(
      'field_first_name' => array(
        'preg' => '/(?<=First Name:)\s*([\w]+)/i',
        'pos' => 1,
      ),
      'field_last_name' => array(
        'preg' => '/(?<=Last Name:)\s*([\w]+)/i',
        'pos' => 1,
      ),
      'work_phone' => array(
        'preg' => '/(?<=Work Phone:\s)([\d-]+)/i',
        'pos' => 1,
      ),
      'home_phone' => array(
        'preg' => '/(?<=Home Phone:\s)([\d-]+)/i',
        'pos' => 1,
      ),
      'field_e_mail' => array(
        'preg' => '/(?<=Email:)\s*(.*?)\s/i',
        'pos' => 1,
      ),
      'field_date' => array(
        'preg' => '/(?<=Approx[\s+]Move[\s+]Date:)\s*([\d]+\/[\d]+\/[\d]+)/i',
        'pos' => 1,
      ),
      'field_size_of_move' => array(
        'preg' => '/(?<=Approx[\s+]Move[\s+]Weight:)\s*(.*?)\D/i',
        'pos' => 1,
      ),
      'moving_from_state' => array(
        'preg' => '/(?<=From\sState:\s)([\w\s]+)/i',
        'pos' => 1,
      ),
      'moving_from_city' => array(
        'preg' => '/(?<=From\sCity:\s)([\w\s]+)/i',
        'pos' => 1,
      ),
      'moving_from_zip' => array(
        'preg' => '/(?<=From\sZip:\s)([\d]+)/i',
        'pos' => 1,
      ),
      'moving_to_state' => array(
        'preg' => '/(?<=To\sState:\s)([\w\s]+)/i',
        'pos' => 1,
      ),
      'moving_to_city' => array(
        'preg' => '/(?<=To\sCity:\s)([\w\s]+)/i',
        'pos' => 1,
      ),
      'moving_to_zip' => array(
        'preg' => '/(?<=To\sZip:\s)([\d]+)/i',
        'pos' => 1,
      ),
    );

    foreach ($data as $id => &$item) {
      $matches = array();
      $match = preg_match($patterns[$id]['preg'], $parse_data, $matches);
      if ($match && $matches) {
        $item = $matches[$patterns[$id]['pos']];
      }
    }

    return array('data' => $data, 'parse' => $parse_data);
  }

  private function parseMessageHtml(string $parse_data) : array {
    $crawler = new Crawler($parse_data);
    $crawler = $crawler->filter("table > tr");

    $nodeValues = $crawler->each(
      function (Crawler $node, $i) {
        $first = trim($node->children()->first()->text());
        $last = trim($node->children()->last()->text());
        return array($first, $last);
      }
    );

    $field_first_name = $nodeValues[8][1];
    $field_last_name = $nodeValues[9][1];
    $phone = $nodeValues[10][1];
    $additional_phone = $nodeValues[11][1];
    $e_mail = $nodeValues[12][1];
    $field_date = $nodeValues[1][1];
    $field_size_of_move = $nodeValues[3][1];

    $from = $nodeValues[5][1];
    $from_city = array();
    preg_match('/([\w]*\s*[\w]*)/i', $from, $from_city);
    $from_state = array();
    preg_match('/(?<=[\w+])\s*\w*\,\s*(\w*)/i', $from, $from_state);
    $from_zip = array();
    preg_match('/(?<=[\w+])\s*\w*\,\s*\w*\s*(\d*)/i', $from, $from_zip);

    $to = $nodeValues[6][1];
    $to_city = array();
    preg_match('/([\w]*\s*[\w]*)/i', $to, $to_city);
    $to_state = array();
    preg_match('/(?<=[\w+])\s*\w*\,\s*(\w*)/i', $to, $to_state);
    $to_zip = array();
    preg_match('/(?<=[\w+])\s*\w*\,\s*\w*\s*(\d*)/i', $to, $to_zip);

    $size_of_move = array();
    preg_match('/(\d*\s*+lbs)/i', $field_size_of_move, $size_of_move);

    $data = array(
      'field_first_name' => $field_first_name,
      'field_last_name' => $field_last_name,
      'field_phone' => $phone,
      'field_additional_phone' => $additional_phone,
      'field_e_mail' => $e_mail,
      'field_date' => $field_date,
      'moving_from_city' => trim($from_city[1]),
      'moving_from_state' => trim($from_state[1]),
      'moving_from_zip' => trim($from_zip[1]),
      'moving_to_city' => trim($to_city[1]),
      'moving_to_state' => trim($to_state[1]),
      'moving_to_zip' => trim($to_zip[1]),
      'field_size_of_move' => trim($size_of_move[1]),
      'field_additional_comments' => '',
    );

    return array('data' => $data, 'parse' => $parse_data);
  }

  private function prepareSizeOfMove(array &$data) {
    if (isset($data['field_size_of_move']) && $data['field_size_of_move']) {
      $data['field_size_of_move'] = str_replace(',', '', $data['field_size_of_move']);
    }
  }

  private function prepareFieldAddress(&$data) {
    $prepare_address_field = array(
      'field_moving_from' => array(
        'moving_from_state',
        'moving_from_city',
        'moving_from_zip',
      ),
      'field_moving_to' => array(
        'moving_to_state',
        'moving_to_city',
        'moving_to_zip',
      ),
    );

    foreach ($prepare_address_field as $field_name => $item) {
      $data[$field_name] = array(
        'country' => 'US',
        'locality' => '',
        'administrative_area' => '',
        'postal_code' => '',
      );
      if (isset($data[$item[0]])) {
        $data[$field_name]['administrative_area'] = strtoupper($this->getStateCode($data[$item[0]]));
        unset($data[$item[0]]);
      }
      if (isset($data[$item[1]])) {
        $data[$field_name]['locality'] = $data[$item[1]];
        unset($data[$item[1]]);
      }
      if (isset($item[2]) && isset($data[$item[2]])) {
        $data[$field_name]['postal_code'] = $data[$item[2]];
        unset($data[$item[2]]);
      }
    }
  }

  private function preparePhone(array &$data) {
    $field_phones = array('field_phone', 'field_additional_phone');
    $phones = array(
      $data['work_phone'] ?? '',
      $data['home_phone'] ?? '',
      $data['mobile_phone'] ?? '',
    );
    unset($data['work_phone']);
    unset($data['home_phone']);
    unset($data['mobile_phone']);

    arsort($phones, SORT_STRING);
    $sort_phones = array_slice($phones, 0, count($field_phones));
    $result = array_combine($field_phones, $sort_phones);
    $data += $result;
  }

  private function clearData(&$data) {
    $keys = array_keys($data, NULL, TRUE);
    foreach ($keys as $key) {
      unset($data[$key]);
    }
  }

}
