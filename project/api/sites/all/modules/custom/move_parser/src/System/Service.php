<?php

namespace Drupal\move_parser\System;

use Drupal\move_parser\Services\Parser;

class Service {
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'parser' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_parser\System\Service::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parser',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(),
          'update' => array(),
          'delete' => array(),
          'index' => array(
            'callback' => 'Drupal\move_parser\System\Service::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parser',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'create_vendor' => array(
            'help' => 'Create a new vendor',
            'callback' => 'Drupal\move_parser\System\Service::createVendor',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parser',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'parser_provider',
                'type' => 'int',
                'source' => array('data' => 'parser_provider'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'active',
                'type' => 'int',
                'source' => array('data' => 'active'),
                'optional' => TRUE,
                'default value' => 0,
              ),
              array(
                'name' => 'company_name',
                'type' => 'string',
                'source' => array('data' => 'company_name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'info',
                'type' => 'array',
                'source' => array('data' => 'info'),
                'optional' => TRUE,
                'default value' => array(),
              ),
            ),
            'access arguments' => array('create vendor'),
          ),
          'get_lead_parsing' => array(
            'help' => 'Create a new request from parser.',
            'callback' => 'Drupal\move_parser\System\Service::createLeadParsing',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parser',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function create($data = array()) {
    $al_instance = new Parser();
    return $al_instance->create($data);
  }

  public static function retrieve() {}

  public static function update() {}

  public static function delete() {}

  public static function index() {
    $al_instance = new Parser();
    return $al_instance->index();
  }

  public static function createVendor($provider_id, $active, $company_name, $info) {
    $instance = new Parser();
    return $instance->createVendor($provider_id, $active, $company_name, $info);
  }

  public static function createLeadParsing($data) {
    $instance = new Parser();
    return $instance->createLeadParsing($data);
  }

}
