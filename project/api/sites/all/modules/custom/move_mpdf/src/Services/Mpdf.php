<?php

namespace Drupal\move_mpdf\Services;

use Drupal\move_services_new\Services\BaseService;

/**
 * Class Mpdf.
 *
 * @package Drupal\move_mpdf\Services
 */
class Mpdf extends BaseService {
  private $id = NULL;
  private $entityId = NULL;
  private $entityType = NULL;
  private $html = '';
  private $page_id = '';
  private $allow_attach = NULL;
  private $is_full_path = TRUE;

  public function __construct($id = NULL, $page_id = NULL, $entity_id = NULL, $entity_type = NULL, int $allow_attach = 0, $html = '') {
    $this->id = $id;
    $this->entityId = $entity_id;
    $this->entityType = $entity_type;
    $this->page_id = $page_id;
    $this->html = $html;
    $this->allow_attach = $allow_attach;
  }

    /**
     * @param array $data
     * @return \DatabaseStatementInterface|int|mixed
     * @throws \Exception
     * @throws \ServicesException
     */
    public function create($data = array()) {
    $created = time();

    $filename = static::nameMpdfFile($this->entityType, $this->entityId, $created);
    static::writePdf((string) $data, $filename);

    $id = db_insert('move_mpdf')
      ->fields(array(
        'entity_type' => $this->entityType,
        'entity_id' => $this->entityId,
        'file_name' => "{$filename}.pdf",
        'created' => $created,
        'page_id' => $this->page_id,
        'allow_attach' => $this->allow_attach,
      ))
      ->execute();

    return $id;
  }

  public function retrieve() {
    $result = db_select('move_mpdf', 'mm')
      ->fields('mm')
      ->condition('mm.id', $this->id)
      ->execute()
      ->fetchAssoc();
    $result['url'] = file_create_url("public://pdf/{$result['file_name']}");
    return $result;
  }

  public function update($data = array()) {}

  public function delete() {}

  public function index() {
    $result = array();
    $query = db_select('move_mpdf', 'mm')
      ->fields('mm')
      ->condition('mm.entity_type', $this->entityType)
      ->condition('mm.entity_id', $this->entityId)
      ->execute()
      ->fetchAll();

    foreach ($query as $key => $value) {
      $url = $this->is_full_path ? file_create_url("public://pdf/{$value->file_name}") : drupal_realpath( "public://pdf/{$value->file_name}");

      $result[] = array(
        'id' => $value->id,
        'entity_type' => $value->entity_type,
        'entity_id' => $value->entity_id,
        'file_name' => $value->file_name,
        'created' => $value->created,
        'url' => $url,
        'page_id' => $value->page_id,
      );
    }

    return $result;
  }

  private static function nameMpdfFile(int $entity_type, int $entity_id, int $created) {
      return "{$entity_type}_{$entity_id}_{$created}";
  }

    /**
     * @param string $base_64
     * @param $filename
     * @return bool|mixed|\stdClass
     * @throws \ServicesException
     */
    public function writePdf(string $base_64, string $filename) {
    $directory = 'public://pdf';
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    $file_saved = $this->savePdf($base_64, $filename);


    return $file_saved;
  }

    /**
     * @param $data
     * @param string $filename
     * @return bool|mixed|\stdClass
     * @throws \ServicesException
     */
    public function savePdf($data, string $filename) {
    $destination = "public://pdf/$filename.pdf";

    $dir = drupal_dirname($destination);
    // Build the destination folder tree if it doesn't already exists.
    if (!file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
      return services_error(t("Could not create destination directory for file."), 500);
    }

    // Write the file.
    $file_saved = file_save_data(base64_decode($data), $destination);
    if ($file_saved) {
      $file_saved->filemime = 'pdf';
      file_save($file_saved);
      $file_saved->title = $filename;
    }

    return $file_saved;
  }

  /**
   * @param null $entityId
   */
  public function setEntityId($entityId): void {
    $this->entityId = $entityId;
  }

  /**
   * @param null $entityType
   */
  public function setEntityType($entityType): void {
    $this->entityType = $entityType;
  }

  /**
   * @param bool $is_full_path
   */
  public function setIsFullPath(bool $is_full_path): void {
    $this->is_full_path = $is_full_path;
  }

}
