<?php

namespace Drupal\move_mpdf\System;

use Drupal\move_mpdf\Services\Mpdf;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  public static function definition() {
    return array(
      'move_mpdf' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_mpdf\System\Service::createMpdf',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mpdf',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'html',
                'type' => 'string',
                'source' => array('data' => 'html'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'page_id',
                'type' => 'string',
                'source' => array('data' => 'page_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'allow_attach',
                'type' => 'int',
                'source' => array('data' => 'allow_attach'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit any move_request content'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_mpdf\System\Service::retrieveMpdf',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mpdf',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_mpdf\System\Service::indexMpdf',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mpdf',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('param' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('param' => 'entity_type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function createMpdf(string $html, string $page_id, int $entity_id, int $entity_type, int $allow_attach) {
    $al_instance = new Mpdf(NULL, $page_id, $entity_id, $entity_type, $allow_attach);
    return $al_instance->create($html);
  }

  public static function retrieveMpdf($id) {
    $al_instance = new Mpdf($id);
    return $al_instance->retrieve();
  }

  public static function indexMpdf(int $entity_id, int $entity_type) {
    $al_instance = new Mpdf(NULL, NULL, $entity_id, $entity_type);
    return $al_instance->index();
  }

}