<?php

namespace Drupal\move_long_distance\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class JobPaidType.
 *
 * @package Drupal\move_long_distance\Util
 */
class JobPaidType extends BaseEnum {
  const NONE = 0;
  const PAY = 1;
  const RECEIVE = 2;
}
