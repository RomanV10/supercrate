<?php

namespace Drupal\move_long_distance\Services;

use Drupal\move_long_distance\Entity\ForemanPayroll;
use Drupal\move_long_distance\Entity\HelperPayroll;
use Drupal\move_long_distance\Entity\PayrollExpensesReceived;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_long_distance\Util\enum\TripPayrollStatus;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use PDO;
use Drupal\move_long_distance\Controller\ControllerPayroll;

/**
 * Class LongDistancePayroll.
 *
 * @package Drupal\move_long_distance\Services
 */
class LongDistancePayroll extends BaseService {

  private $pid = NULL;
  private $tripId = NULL;
  private $encoders;
  private $normalizers;
  private $serializer;
  public $logText = [];

  public function __construct($trip_id = NULL, $pid = NULL) {
    $this->pid = $pid;
    $this->tripId = $trip_id;
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);
  }

  public function setId($pid) {
    $this->pid = $pid;
  }

  public function create($data = array()) {}

  public function retrieve($status = 0) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      $query = db_select('ld_trip_payroll_details', 'payroll_details');
      $query->leftJoin('ld_trip_details', 'trip_details', 'trip_details.ld_trip_id = payroll_details.ld_trip_id');
      $query->fields('payroll_details')
        ->condition('payroll_details.ld_trip_id', $this->tripId);
      $result['details'] = $query->execute()->fetchAssoc(PDO::FETCH_ASSOC);

      $result['received'] = db_select('ld_trip_payroll_received', 'received')
        ->fields('received')
        ->condition('ld_trip_id', $this->tripId)
        ->execute()
        ->fetchAll(PDO::FETCH_ASSOC);

      $result['expenses'] = db_select('ld_trip_payroll_expenses', 'expenses')
        ->fields('expenses')
        ->condition('ld_trip_id', $this->tripId)
        ->execute()
        ->fetchAll(PDO::FETCH_ASSOC);

      $foreman = db_select('ld_trip_foreman_payroll_closing', 'foreman');
      $foreman->leftJoin('field_data_field_user_first_name', 'first_name', 'first_name.entity_id = foreman.uid');
      $foreman->leftJoin('field_data_field_user_last_name', 'last_name', 'last_name.entity_id = foreman.uid');
      $foreman->fields('foreman')
        ->fields('last_name', array('field_user_last_name_value'))
        ->fields('first_name', array('field_user_first_name_value'))
        ->condition('ld_trip_id', $this->tripId);
      $result['foreman'] = $foreman->execute()->fetchAssoc(PDO::FETCH_ASSOC);

      $helper = db_select('ld_trip_helper_payroll_closing', 'helper');
      $helper->leftJoin('field_data_field_user_first_name', 'first_name', 'first_name.entity_id = helper.uid');
      $helper->leftJoin('field_data_field_user_last_name', 'last_name', 'last_name.entity_id = helper.uid');
      $helper->fields('helper')
        ->fields('last_name', array('field_user_last_name_value'))
        ->fields('first_name', array('field_user_first_name_value'))
        ->condition('ld_trip_id', $this->tripId);
      $result['helper'] = $helper->execute()->fetchAll(PDO::FETCH_ASSOC);

      $result['total_payroll_helpers'] = 0;
      if (!empty($result['helper'])) {
        foreach ($result['helper'] as $helper_item) {
          $result['total_payroll_helpers'] += $helper_item['total_payroll'];
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Retrieve Trip Payroll Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Trip Payroll', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Update payroll info.
   *
   * @param array $data
   *   Data for update.
   *
   * @return array|mixed
   *   Array or error message.
   *
   * @throws \ServicesException
   */
  public function update($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      $payroll_status = LongDistanceTrip::tripPayrollStatus($this->tripId);

      if ($payroll_status !== FALSE) {
        $old_payroll = $this->retrieve();
        if (!empty($old_payroll['details'])) {
          $old_payroll += $old_payroll['details'];
          unset($old_payroll['details']);
        }
      }

      self::payrollCalculation($data);
      db_merge('ld_trip_payroll_details')
        ->key(array('ld_trip_id' => $this->tripId))
        ->fields(array(
          'ld_trip_id' => $this->tripId,
          'total_payroll' => $data['total_payroll'],
          'date_start' => empty($data['date_start']) ? time() : $data['date_start'],
          'date_end' => empty($data['date_end']) ? time() : $data['date_end'],
          'created' => time(),
          'status' => 0,
        ))
        ->execute();
      $data['total_payroll_helpers'] = 0;
      // TODO need to remove switch and make it by one small method.
      foreach ($data as $field_name => $val) {
        switch ($field_name) {
          case 'received':
            db_delete('ld_trip_payroll_received')
              ->condition('ld_trip_id', $this->tripId)
              ->execute();
            if (!empty($val)) {
              foreach ($val as $key => $item) {
                $item['uid'] = $data['foreman']['uid'];
                $item['ld_trip_id'] = $this->tripId;
                $item['id'] = $key;
                $result[$field_name][] = $received = (array) $this->serializer->deserialize(json_encode($item), PayrollExpensesReceived::class, 'json');
                db_merge('ld_trip_payroll_received')
                  ->key(array('ld_trip_id' => $this->tripId, 'id' => $received['id']))
                  ->fields($received)
                  ->execute();
              }
            }
            break;

          case 'expenses':
            db_delete('ld_trip_payroll_expenses')
              ->condition('ld_trip_id', $this->tripId)
              ->execute();
            if (!empty($val)) {
              foreach ($val as $key => $item) {
                $item['uid'] = $data['foreman']['uid'];
                $item['ld_trip_id'] = $this->tripId;
                $item['id'] = $key;
                $result[$field_name][] = $expenses = (array) $this->serializer->deserialize(json_encode($item), PayrollExpensesReceived::class, 'json');
                db_merge('ld_trip_payroll_expenses')
                  ->key(array('ld_trip_id' => $this->tripId, 'id' => $expenses['id']))
                  ->fields($expenses)
                  ->execute();
              }
            }

            break;

          case 'foreman':
            $val['ld_trip_id'] = $this->tripId;
            $result[$field_name] = $foreman = (array) $this->serializer->deserialize(json_encode($val), ForemanPayroll::class, 'json');
            db_merge('ld_trip_foreman_payroll_closing')
              ->key(array('ld_trip_id' => $this->tripId))
              ->fields($foreman)
              ->execute();

            break;

          case 'helper':
            db_delete('ld_trip_helper_payroll_closing')
              ->condition('ld_trip_id', $this->tripId)
              ->execute();
            if (!empty($val)) {
              foreach ($val as $key => $item) {
                $item['ld_trip_id'] = $this->tripId;
                $result[$field_name] = $helper = (array) $this->serializer->deserialize(json_encode($item), HelperPayroll::class, 'json');
                db_merge('ld_trip_helper_payroll_closing')
                  ->key(array('ld_trip_id' => $this->tripId, 'uid' => $item['uid']))
                  ->fields($helper)
                  ->execute();
                $data['total_payroll_helpers'] += $helper['total_payroll'];
              }
            }
            break;

        }
      }
      if (!empty($old_payroll) && $diff = $this->arrayDiffAssocRecursive($data, $old_payroll)) {
        $this->prepareLogsFormDiff($diff);
        $log_title = 'Payroll was updated';
      }
      else {
        $log_title = 'Payroll was created';
      }

      if (!empty($log_title)) {
        (new LongDistanceTrip($this->tripId))->setLogs($log_title, $this->logText);
      }

    }
    catch (\Throwable $e) {
      $message = "Update Trip Payroll Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Trip Payroll', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
      $data['error'] = $error_message;
    }
    finally {

      return $error_flag ? services_error($error_message, 406) : $data;
    }
  }

  public function index() {}

  public function delete() {}

  /**
   * Get payroll foreman uid by trip id.
   *
   * @param int $trip_id
   *   Id of the trip.
   *
   * @return int
   *   UID of foreman.
   */
  public static function getPayrollForemanId(int $trip_id) : int {
    $uid = db_select('ld_trip_foreman_payroll_closing', 'ltfpc')
      ->fields('ltfpc', array('uid'))
      ->condition('ld_trip_id', $trip_id)
      ->execute()
      ->fetchField();
    return $uid;
  }

  /**
   * Submit payroll and make calculation for it.
   *
   * Change status to "Submit".
   */
  public function submitPayroll() {
    $payroll = $this->retrieve();
    self::changeTripPayrollStatus($this->tripId, TripPayrollStatus::SUBMIT);
    $calc_payroll = new ControllerPayroll($payroll);
    $calc_payroll->calculate();
    (new LongDistanceTrip($this->tripId))->setLogs('Payroll was submitted', array());
    return TRUE;

  }

  /**
   * Remove payroll. Change status to "Draft". Remove trip from payroll.
   */
  public function revokePayroll() {
    self::changeTripPayrollStatus($this->tripId, TripPayrollStatus::DRAFT);
    (new ControllerPayroll($this->retrieve()))->calculate();
    (new LongDistanceTrip($this->tripId))->setLogs('Payroll was revoked', array());
    return TRUE;
  }

  /**
   * Change trip payroll status.
   *
   * @param int $trip_id
   *   Trip id.
   * @param int $status
   *   Status to change.
   *
   * @return int
   *   If something was changed return 1. Else 0.
   */
  public static function changeTripPayrollStatus(int $trip_id, int $status) {
    $updated = db_update('ld_trip_payroll_details')
      ->fields(array('status' => $status))
      ->condition('ld_trip_id', $trip_id)
      ->execute();

    return $updated;
  }

  /**
   * Make calculation from payroll info.
   *
   * @param array $data
   *   Array with data for calculate.
   */
  public static function payrollCalculation(array &$data) : void {
    $data['foreman']['total_payroll'] = 0;
    $total_received = 0;
    $total_expenses = 0;
    $all_helpers_total = 0;
    // Calculate total for received cash.
    if (!empty($data['received'])) {
      foreach ($data['received'] as $fields) {
        $total_received += (float) $fields['amount'];
      }
      $data['foreman']['total_received'] = (float) $total_received;
    }
    else {
      $data['foreman']['total_received'] = 0;
    }

    // Calculate total for expenses cash.
    if (!empty($data['expenses'])) {
      foreach ($data['expenses'] as $fields) {
        $total_expenses += (float) $fields['amount'];
      }
      $data['foreman']['total_expenses'] = $total_expenses;
    }
    else {
      $data['foreman']['total_expenses'] = 0;
    }

    // Total for days.
    $data['foreman']['total_daily'] = (float) $data['foreman']['daily_amount'] * (float) $data['foreman']['total_days'];
    // Total for hours.
    $data['foreman']['total_hourly'] = (float) $data['foreman']['hourly_rate'] * (float) $data['foreman']['total_hours'];
    // Total for mileage.
    $data['foreman']['total_mileage'] = (float) $data['foreman']['mileage'] * (float) $data['foreman']['mileage_rate'];
    // Total for loading.
    $data['foreman']['total_loading'] = (float) $data['foreman']['loading_volume'] * (float) $data['foreman']['loading_rate'];
    // Total for unloading.
    $data['foreman']['total_unloading'] = (float) $data['foreman']['unloading_volume'] * (float) $data['foreman']['unloading_rate'];

    // Total foreman payroll.
    $exception_fields = array(
      'total_days',
      'total_hours',
      'total_payroll',
    );
    foreach ($data['foreman'] as $field_name => $val) {
      $is_total = strpos($field_name, 'total_');
      if ($is_total !== FALSE && !in_array($field_name, $exception_fields)) {
        if ($field_name == 'total_received' || $field_name == 'total_collected') {
          $data['foreman']['total_payroll'] -= (float) $val;
        }
        else {
          $data['foreman']['total_payroll'] += (float) $val;
        }
      }
    }

    // Calculate total for helpers.
    if (!empty($data['helper'])) {
      foreach ($data['helper'] as $key => $helper) {
        $data['helper'][$key]['total_daily'] = (float) $helper['total_days'] * (float) $helper['daily_amount'];
        $data['helper'][$key]['total_payroll'] = $data['helper'][$key]['total_daily'] + (float) $helper['other'];
        $all_helpers_total += $data['helper'][$key]['total_payroll'];
      }
    }

    $data['total_payroll'] = $data['foreman']['total_payroll'] + $all_helpers_total;
  }

  /**
   * Update foreman uid for payroll.
   *
   * @param int $uid
   *   Id of foreman.
   */
  public function updateForemanUid(int $uid) {
    db_update('ld_trip_foreman_payroll_closing')
      ->fields(array('uid' => $uid))
      ->condition('ld_trip_id', $this->tripId)
      ->execute();
  }

  /**
   * Sync payroll helpers with trip helpers.
   *
   * @param array $helpers
   *   Helpers array.
   */
  public function updateHelpers(array $helpers) {
    // If helpers exist get they ids for check.
    if (!empty($helpers)) {
      // Collect all helpers id in one array for compare.
      foreach ($helpers as $helper) {
        $trip_helpers[] = $helper['helper_id'];
      }
      $payroll_helpers_ids = db_select('ld_trip_helper_payroll_closing', 'p_helpers')
        ->fields('p_helpers', array('uid'))
        ->condition('ld_trip_id', $this->tripId)
        ->execute()
        ->fetchCol();

      $helpers_to_remove_from_payroll = array_diff($payroll_helpers_ids, $trip_helpers);
      // Remove helpres that dose not exist in trip.
      if (!empty($helpers_to_remove_from_payroll)) {
        $this->removeHelpersFromPayroll($helpers_to_remove_from_payroll);
      }
      // Add new helpers.
      foreach ($trip_helpers as $helper_id) {
        db_merge('ld_trip_helper_payroll_closing')
          ->key(array('uid' => $helper_id, 'ld_trip_id' => $this->tripId))
          ->fields(array('uid' => $helper_id, 'ld_trip_id' => $this->tripId))
          ->execute();
      }
    }
    // If all trip helpers are empty - remove them from payroll then.
    else {
      db_delete('ld_trip_helper_payroll_closing')
        ->condition('ld_trip_id', $this->tripId)
        ->execute();
    }
  }

  /**
   * Remove helpers from payroll by helpers ids.
   *
   * @param array $uids
   *   Helper ids.
   */
  public function removeHelpersFromPayroll(array $uids) {
    db_delete('ld_trip_helper_payroll_closing')
      ->condition('uid', $uids, 'IN')
      ->condition('ld_trip_id', $this->tripId)
      ->execute();
  }

  /**
   * Check diff on update and add diff to log var.
   *
   * @param array $new
   *   First array to check.
   * @param array $old
   *   Second array to check.
   * @param int $level
   *   Level of iteration.
   *
   * @return array
   *   Array with diff.
   */
  public function arrayDiffAssocRecursive(array $new, array $old, int $level = 1) : array {
    foreach ($new as $key => $value) {

      if (is_array($value)) {
        if (count($old[$key]) > count($new[$key])) {
          $temp_array = array_diff_assoc($old[$key], $new[$key]);
          $difference[$key] = $this->buildArrayForLog($temp_array);
        }

        if (!isset($old[$key])) {
          foreach ($value as $field_name => $val) {
            $difference[$key][$field_name]['new'] = $val;
            $difference[$key][$field_name]['old'] = NULL;
          }
        }
        elseif (!is_array($old[$key])) {
          $difference[$key]['new'] = $value;
          $difference[$key]['old'] = NULL;
        }
        else {
          $new_diff = $this->arrayDiffAssocRecursive($value, $old[$key], $level + 1);
          if ($new_diff != FALSE) {
            $difference[$key] = $new_diff;
          }
        }
      }
      elseif ((!isset($old[$key]) || $old[$key] != $value) && !($old[$key] === NULL && $value === NULL)) {
        $difference[$key]['new'] = $value;
        $difference[$key]['old'] = isset($old[$key]) ? $old[$key] : NULL;
      }
    }
    return !isset($difference) ? array() : $difference;
  }

  /**
   * Build old array for log.
   *
   * @param $array
   * @return mixed
   */
  public function buildArrayForLog($array) {
    foreach ($array as $key_name => $val) {
      if (is_array($val)) {
        $result[$key_name] = $this->buildArrayForLog($val);
      }
      else {
        $result[$key_name]['new'] = NULL;
        $result[$key_name]['old'] = $val;
      }
    }
    return $result;
  }

  /**
   * Set logs array from diff on update.
   *
   * @param array $diff
   *   Array with diff.
   * @param string $previous_field
   *   Previous field_name to build log string.
   * @param int $level
   *   Iteration level.
   */
  public function prepareLogsFormDiff(array $diff, string &$previous_field = '', int $level = 1, $temp_previous_field = '') : void {
    $excluded = ['id', 'ld_trip_id', 'uid'];
    foreach ($diff as $field_name => $val) {
      if (!in_array($field_name, $excluded, TRUE)) {
        if ($level === 1) {
          $previous_field = '';
          $temp_previous_field = ucfirst(str_replace('_', ' ', $field_name)) . '->';
        }
        if (is_array($val) && !isset($val['new']) && !isset($val['old'])) {
          $previous_field = $temp_previous_field . ucfirst(str_replace('_', ' ', $field_name)) . '->';
          $this->prepareLogsFormDiff($val, $previous_field, $level + 1, $temp_previous_field);
        }
        else {
          if (is_null($val['old'])) {
            $this->logConvertedValue($val['new'], $field_name);
            $this->logText[]['simpleText'] = '"' . $previous_field . ucfirst(str_replace('_', ' ', $field_name)) . '" was added. Field value is: "' . str_replace('.00', '', $val['new']) . '"';
          }
          elseif (is_null($val['new'])) {
            $this->logConvertedValue($val['old'], $field_name);
            $this->logText[]['simpleText'] = '"' . $previous_field . ucfirst(str_replace('_', ' ', $field_name)) . '" was removed. Field value was: "' . str_replace('.00', '', $val['old']) . '"';
          }
          elseif (!is_null($val['new']) && !is_null($val['old'])) {
            $this->logConvertedValue($val['old'], $field_name);
            $this->logConvertedValue($val['new'], $field_name);

            $this->logText[]['simpleText'] = '"' . $previous_field . ucfirst(str_replace('_', ' ', $field_name)) . '" was changed from "' . str_replace('.00', '', $val['old']) . '" to "' . str_replace('.00', '', $val['new']) . '"';
          }
        }
      }
    }
  }

  /**
   * Convert field to human read.
   *
   * @param $value
   *   Value to convert.
   * @param string $field_name
   *   Name of the field for check.
   */
  public function logConvertedValue(&$value, string $field_name) {
    switch ($field_name) {
      case 'date':
      case 'date_start':
      case 'date_end':
        $value = date('m/d/Y', $value);
        break;
    }
  }

}
