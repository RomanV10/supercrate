<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class Services.
 *
 * @package Drupal\move_long_distance\Entity
 */
class Services {

  /**
   * @var int
   */
  public $service_id = 0;


/**
   * @var int
   */
  public $job_id = 0;

  /**
   * @var float
   */
  public $total = 0.00;

  /**
   * @var string
   */
  public $name = '';


  // Getters.

  public function getServiceId() : int {
    return (int) $this->service_id;
  }
  public function getJobId() : int {
    return (int) $this->job_id;
  }
  public function getTotal() : float {
    return (float) $this->total;
  }
  public function getName() : string {
    return (string) $this->name;
  }

  // Setters.


  public function setServicesExtra($service_id) {
    $this->service_id = (int) $service_id;
  }

  public function setJobId($job_id) {
    $this->job_id = (int) $job_id;
  }

  public function setTotal($total) {
    $this->total = (float) $total;
  }

  public function setName($name) {
    $this->name = (string) $name;
  }

}
