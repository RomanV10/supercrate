<?php

namespace Drupal\move_long_distance\Services;

use Drupal\move_long_distance\Entity\TripClosing;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_long_distance\Entity\TPDelivery;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_long_distance\Entity\Services;
use Drupal\move_services_new\Util\enum\TripJobType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Drupal\move_services_new\Util\enum\TripStatus;
use Drupal\move_services_new\Util\enum\TripPaymentType;

use Drupal\move_new_log\Services\Log;

use PDO;
/**
 * Class LongDistanceTPDelivery.
 *
 * @package Drupal\move_long_distance\Services
 */
class LongDistanceTPDelivery extends BaseService {
  private $tpdId = NULL;
  private $tripId = NULL;
  private $encoders;
  private $normalizers;
  private $serializer;
  public $logText = [];

  public function __construct($tpd_id = NULL, $trip_id = NULL) {
    $this->tpdId = $tpd_id;
    $this->tripId = $trip_id;
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);
  }

  public function setId($tpd_id) {
    $this->tpdId = $tpd_id;
  }

  public function create($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $tp_delivery = FALSE;
    try {
      $tp_delivery = $this->serializer->deserialize(json_encode($data), TPDelivery::class, 'json');
      if (empty($tp_delivery->details->phones)) {
        throw new \Exception("Phones is empty");
      }
      else {
        // Unset phones from tp_delivery_details object.
        // We don't have this field in db.
        // If keep it will have error.
        $phones = $tp_delivery->details->phones;
        unset($tp_delivery->details->phones);

        // Filling the related tables.
        $fill_db = array(
          'delivery_job_id' => 'ld_tp_delivery_job',
          'details' => 'ld_tp_delivery_details',
        );
        $tp_delivery_id = db_insert('ld_tp_delivery')
          ->fields(array(
            'notes' => $tp_delivery->notes,
            'ld_trip_id' => $this->tripId,
            'created' => time(),
          )
          )
          ->execute();
        $tp_delivery->id = $tp_delivery_id;
        // Save values for related tables fields.
        foreach ($fill_db as $item_name => $table_name) {
          if ($item_name == 'delivery_job_id') {
            $fields = array(
              'job_id' => $tp_delivery_id,
              'delivery_job_id' => $tp_delivery->{$item_name},
              'ld_tp_delivery_id' => $tp_delivery_id,
            );
          }
          else {
            $tp_delivery->{$item_name}->ld_tp_delivery_id = $tp_delivery_id;
            $fields = $tp_delivery->{$item_name};
          }
          // Add last inserted id to array. Will use in future.
          $last_inserted_id[$item_name] = db_insert($table_name)
            ->fields((array) $fields)
            ->execute();
        }

        // Adding phones for Tp delivery.
        if (!empty($tp_delivery_id) && !empty($phones)) {
          $insert = db_insert('ld_tp_delivery_phones')
            ->fields(array('ld_tp_delivery_id', 'phone'));
          foreach ($phones as $phone) {
            if (!empty($phone)) {
              $insert->values(array('ld_tp_delivery_id' => $tp_delivery_id, 'phone' => $phone));
            }
          }
          $insert->execute();
        }

        // SIT.
        if (!empty($data['sit'])) {
          $sit_instance = new LongDistanceSit($tp_delivery_id, TripJobType::TP_DELIVERY);
          $sit = $sit_instance->update($data['sit']);
          $tp_delivery->sit = $sit;
        }



        // Build closing for save.
        $closing = (array) $this->serializer->deserialize(json_encode($data['closing']), TripClosing::class, 'json');
        $closing = LongDistanceJob::closingCalculation($closing, TRUE, EntityTypes::LDTDJOB);
        if (empty($closing) && empty($last_inserted_id['delivery_job_id'])) {
          throw new \Exception("TP deliver closing or insert TP deliver job error on create.");
        }
        else {
          $closing['ld_trip_id'] = $this->tripId;
          $closing['job_id'] = $last_inserted_id['delivery_job_id'];
          $closing['owner'] = $tp_delivery->details->customer;
          $job_instance = new LongDistanceJob($closing['job_id'], $this->tripId);
          $closing['status'] = TripStatus::PENDING;
          $closing['type'] = 11;
          $job_instance->addingClosingToTrip($closing, 'tp_delivery');
          // Services.
          if (!empty($data['services'])) {
            $service = new LongDistanceServices($this->tripId, $last_inserted_id['delivery_job_id']);
            $service->create($data, 'tp_delivery');
          }
          // Receipts.
          if (!empty($data['payments'])) {
            foreach ($data['payments'] as $receipt) {
              $receipt['tp_delivery_transaction'] = 1;
              $receipt['jobs'][] = $last_inserted_id['delivery_job_id'];
              MoveRequest::setReceipt($last_inserted_id['delivery_job_id'], $receipt, EntityTypes::LDTDJOB);
            }
          }
          $job_instance->updateClosingTotal($this->tripId, 1);
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Create TP Delivery Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('carrier', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : (array) $tp_delivery;
    }
  }

  /**
   * Get TP Delivery job info by it's id. Job has same id.
   * @return bool|mixed
   */
    public function retrieve() {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      $tp_delivery = db_select('ld_tp_delivery', 'tp_delivery')
        ->fields('tp_delivery')
        ->condition('ld_tp_delivery_id', $this->tpdId)
        ->execute()
        ->fetchAssoc(PDO::FETCH_ASSOC);


      // All details.
      $details = db_select('ld_tp_delivery_details', 'details')
        ->fields('details')
        ->condition('ld_tp_delivery_id', $this->tpdId)
        ->execute()
        ->fetchAssoc(PDO::FETCH_ASSOC);

      // Phones.
      $phones_query = db_select('ld_tp_delivery_phones', 'phones')
        ->fields('phones', array('phone'))
        ->condition('ld_tp_delivery_id', $this->tpdId)
        ->execute()
        ->fetchAll(PDO::FETCH_ASSOC);

      $phones = array();
      if (!empty($phones_query)) {
        foreach ($phones_query as $phone) {
          $phones[] = $phone['phone'];
        }
      }
      // Job info.
      $job = db_select('ld_tp_delivery_job', 'job')
        ->fields('job')
        ->condition('ld_tp_delivery_id', $this->tpdId)
        ->execute()
        ->fetchAssoc(PDO::FETCH_ASSOC);

      // Get SIT.
      $sit_instance = new LongDistanceSit($this->tpdId, TripJobType::TP_DELIVERY);
      $sit = $sit_instance->retrieve();

      // Get Receipts not from agent folio.
      $receipts_types = array(TripPaymentType::RECEIVED, TripPaymentType::TP_COLLECTED);
      $job_inst = new LongDistanceJob($this->tpdId);
      $receipt = $job_inst->getJobReceipt($receipts_types, EntityTypes::LDTDJOB, FALSE, TRUE);

      // Get closing.
      $closing = $this->getTPDeliveryClosing();

      $details['phones'] = $phones;
      $result = array(
        'id' => $this->tpdId,
        'details' => $details,
        'delivery_job_id' => $job['delivery_job_id'],
        'notes' => $tp_delivery['notes'],
        'closing' => $closing,
        'job' => $job,
        'sit' => $sit,
        'payments' => $receipt,
      );

    }
    catch (\Throwable $e) {
      $message = "Retrieve TP Delivery Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('TP delivery', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Update TP Delivery details, closing etc...
   *
   * @param array $data
   *   Array with information to update.
   *
   * @return bool|mixed
   */
  public function update($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $tp_delivery = array();
    try {
      $job_instance = new LongDistanceJob($this->tpdId);
      $job_instance->jobHasTransactionError();

      $old = $this->retrieve();

      $tp_delivery = $this->serializer->deserialize(json_encode($data), TPDelivery::class, 'json');
      $tp_delivery_real_job = $this->getTPDeliveryRealJob();
      if ($tp_delivery->delivery_job_id != $tp_delivery_real_job) {
        throw new \Exception("The company job id is different. You need to create new TP Delivery for this job.");
      }
      if (empty($tp_delivery->details->phones)) {
        throw new \Exception("Phones is empty");
      }
      else {
        // Unset phones from tp_delivery_details object.
        // We don't have this field in db.
        // If keep it will have error.
        $phones = $tp_delivery->details->phones;
        unset($tp_delivery->details->phones);

        // Filling the related tables.
        $fill_db = array(
          'details' => 'ld_tp_delivery_details',
        );
        db_update('ld_tp_delivery')
          ->fields(array(
            'notes' => $tp_delivery->notes,
            'ld_trip_id' => $this->tripId,
            'created' => time(),
          )
          )
          ->execute();

        // Save values for related tables fields.
        foreach ($fill_db as $item_name => $table_name) {
          if ($item_name == 'delivery_job_id') {
            $fields = array(
              'delivery_job_id' => $tp_delivery->{$item_name},
            );
          }
          else {
            $fields = $tp_delivery->{$item_name};
          }

          // Add last inserted id to array. Will use in future.
          db_update($table_name)
            ->fields((array) $fields)
            ->condition('ld_tp_delivery_id', $this->tpdId)
            ->execute();
        }

        // Adding phones for Tp delivery.
        if (!empty($phones)) {
          // First we need to delete all rows for tp_delivery phones.
          db_delete('ld_tp_delivery_phones')
            ->condition('ld_tp_delivery_id', $this->tpdId)
            ->execute();
          $insert = db_insert('ld_tp_delivery_phones')
            ->fields(array('ld_tp_delivery_id', 'phone'));
          foreach ($phones as $phone) {
            if (!empty($phone)) {
              $insert->values(array('ld_tp_delivery_id' => $this->tpdId, 'phone' => $phone));
            }
          }
          $insert->execute();
        }

        if (!empty($data['sit'])) {
          $sit_instance = new LongDistanceSit($this->tpdId, TripJobType::TP_DELIVERY);
          $sit = $sit_instance->update($data['sit']);
          $tp_delivery->sit = $sit;
        }

        // Build closing for save.
        $data['closing']['ld_trip_id'] = $this->tripId;
        $data['closing']['owner'] = $tp_delivery->details->customer;
        $closing = (array) $this->serializer->deserialize(json_encode($data['closing']), TripClosing::class, 'json');
        $closing = LongDistanceJob::closingCalculation($closing, TRUE, EntityTypes::LDTDJOB);
        // We don't need to update this field.
        unset($closing['closing']['job_id']);
        if (empty($closing)) {
          throw new \Exception("TP deliver closing or insert TP delivery job error on update.");
        }
        else {
          // Get already saved closing.
          $tp_delivery_closing = $this->getTPDeliveryClosing();
          $job_instance = new LongDistanceJob();

          if (!empty($tp_delivery_closing)) {
            unset($closing['job_id']);
            $job_instance::updateClosingById($tp_delivery_closing['id'], $closing);
          }

          // Services.
          if (!empty($data['services'])) {
            $service = new LongDistanceServices($this->tripId, $this->tpdId);
            $service->create($data, 'tp_delivery');
          }

          $job_instance->updateClosingTotal($this->tripId, 1);
          $data['closing'] = $closing;
          if ($diff = $this->arrayDiffAssocRecursive($data, $old)) {
            $this->prepareLogsFormDiff($diff);
          }

          if (!empty($this->logText)) {
            (new LongDistanceTrip($this->tripId))->setLogs('TP delivery was updated', $this->logText);
          }
        }
      }
    }

    catch (\Throwable $e) {
      $message = "Update TP Delivery Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('TP Delivery', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : (array) $tp_delivery;
    }
  }

  /**
   * Delete TP Delivery.
   */
  public function delete($update_closing_total = TRUE) {
    $tables_to_delete = array(
      'ld_tp_delivery_job',
      'ld_tp_delivery_details',
      'ld_tp_delivery',
      'ld_tp_delivery_phones',
    );

    // Remove closing.
    $closing = $this->getTPDeliveryClosing();
    db_delete('ld_trip_closing')
      ->condition('id', $closing['id'])
      ->execute();

    db_delete('ld_trip_jobs')
      ->condition('job_id', $closing['job_id'])
      ->execute();

    foreach ($tables_to_delete as $table_name) {
      db_delete($table_name)
        ->condition('ld_tp_delivery_id', $this->tpdId)
        ->execute();
    }
    // Delete SIT.
    db_delete('ld_request_sit')
      ->condition('ld_nid', $closing['job_id'])
      ->condition('entity_type', 1)
      ->execute();

    if ($update_closing_total) {
      $ld_job = new LongDistanceJob();
      $ld_job->updateClosingTotal($this->tripId, 1);
    }
  }

  /**
   * Delete one tp delivery job.
   *
   * @param int $job_id
   *  Job id.
   */
  public static function deleteTpDeliveryJob(int $job_id) {
    $tables = array(
      'ld_tp_delivery_job',
      'ld_tp_delivery_details',
      'ld_tp_delivery_phones',
      'ld_tp_delivery',
    );
    $tp_delivery_id = db_select('ld_tp_delivery_job', 'ltdj')
      ->fields('ltdj', array('ld_tp_delivery_id'))
      ->condition('job_id', $job_id)
      ->execute()
      ->fetchField();

    if (!empty($tp_delivery_id)) {
      foreach ($tables as $table_name) {
        db_delete($table_name)
          ->condition('ld_tp_delivery_id', $tp_delivery_id)
          ->execute();
      }
      // Delete SIT.
      db_delete('ld_request_sit')
        ->condition('ld_nid', $job_id)
        ->condition('entity_type', 1)
        ->execute();
    }
  }

  public function index() {}

  /**
   * All jobs for TP delivery company.
   *
   * @param int $company_id
   *   Id of the company.
   *
   * @return mixed
   */
  public static function getDeliveryJobsForCompany(int $company_id) {
    $query = db_select('ld_tp_delivery_details', 'ltdd');
    $query->leftJoin('ld_tp_delivery_job', 'ltdj', 'ltdd.ld_tp_delivery_id = ltdj.ld_tp_delivery_id');
    $query->condition('ltdd.delivery_for_company', $company_id);
    $query->fields('ltdj', array('job_id'));
    $result = $query->execute()->fetchCol();
    return $result;
  }

  /**
   * Get closing from db for tp delivery.
   *
   * @return mixed
   */
  public function getTPDeliveryClosing() {
    $query = db_select('ld_trip_closing', 'closing');
    $query->leftJoin('ld_tp_delivery_job', 'delivery_job', 'closing.job_id = delivery_job.job_id');
    $query->fields('closing')
      ->condition('ld_tp_delivery_id', $this->tpdId);
    $result = $query->execute()->fetchAssoc(PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Get TP delivery id from Third Party company.
   *
   * @return mixed
   */
  public function getTPDeliveryRealJob() {
    $job_id = db_select('ld_tp_delivery_job', 'delivery_job')
      ->fields('delivery_job', array('delivery_job_id'))
      ->condition('ld_tp_delivery_id', $this->tpdId)
      ->execute()
      ->fetchField();
    return $job_id;
  }

  public static function tpDeliveryReceiptNotFromAgentFolio($receipt_value) {
    if (!empty($receipt_value['tp_delivery_transaction'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Check diff on update and add diff to log var.
   *
   * @param array $new
   *   First array to check.
   * @param array $old
   *   Second array to check.
   *
   * @return array
   *   Array with diff.
   */
  public function arrayDiffAssocRecursive(array $new, array $old) : array {
    foreach ($new as $key => $value) {
      if (is_array($value)) {
        if (!isset($old[$key])) {
          $difference[$key] = $value;
        }
        elseif (!is_array($old[$key])) {
          $difference[$key] = $value;
        }
        else {
          $new_diff = $this->arrayDiffAssocRecursive($value, $old[$key]);
          if ($new_diff != FALSE) {
            $difference[$key] = $new_diff;
          }
        }
      }
      elseif ((!isset($old[$key]) || $old[$key] != $value) && !($old[$key] === NULL && $value === NULL)) {
        $difference[$key]['new'] = $value;
        $difference[$key]['old'] = isset($old[$key]) ? $old[$key] : NULL;
      }
    }
    return !isset($difference) ? array() : $difference;
  }

  /**
   * Set logs array from diff on update.
   *
   * @param array $diff
   *   Array with diff.
   * @param string $previous_field
   *   Previous field_name to build log string.
   * @param int $level
   *   Iteration level.
   */
  public function prepareLogsFormDiff(array $diff, string &$previous_field = '', int $level = 1) : void {
    $excluded_fields = ['owner', 'weight'];
    foreach ($diff as $field_name => $val) {
      if (!in_array($field_name, $excluded_fields)) {
        if ($level === 1) {
          $previous_field = '';
        }
        if (is_array($val) && !isset($val['new'])) {
          $previous_field .= ucfirst(str_replace('_', ' ', $field_name)) . '->';
          $this->prepareLogsFormDiff($val, $previous_field, $level + 1);
        }
        else {
          $this->logText[]['simpleText'] = '"' . $previous_field . ucfirst(str_replace('_', ' ', $field_name)) . '" was changed from "' . str_replace('.00', '', $val['old']) . '" to "' . str_replace('.00', '', $val['new']) . '"';
        }
      }
    }
  }

}
