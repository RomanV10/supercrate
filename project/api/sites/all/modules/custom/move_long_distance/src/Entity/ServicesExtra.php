<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class ServicesExtra.
 *
 * @package Drupal\move_long_distance\Entity
 */
class ServicesExtra {

  /**
   * @var int
   */
  public $extra_service_id = 0;

  /**
   * @var float
   */
  public $services_default_value = 0.00;

  /**
   * @var string
   */
  public $services_name = '';

  /**
   * @var int
   */
  public $services_read_only = 0;

  /**
   * @var string
   */
  public $services_type = '';

  /**
   * @var int
   */
  public $service_id = 0;


  // Getters.

  public function getExtraServiceId() : int {
    return (int) $this->extra_service_id;
  }

  public function getServicesDefaultValue() : float {
    return (float) $this->services_default_value;
  }

  public function getServicesName() : string {
    return (string) $this->services_name;
  }
  public function getServicesReadOnly() : int {
    return (string) $this->services_read_only;
  }

  public function getServicesType() : string {
    return (string) $this->services_type;
  }

  public function getServiceId() : int {
    return (int) $this->service_id;
  }

  // Setters.


  public function setExtraServiceId($extra_service_id) {
    $this->extra_service_id = (int) $extra_service_id;
  }

  public function setServicesDefaultValue($services_default_value) {
    $this->services_default_value = (float) $services_default_value;
  }

  public function setServicesName($services_name) {
    $this->services_name = (string) $services_name;
  }

  public function setServicesReadOnly($services_read_only) {
    $this->services_read_only = (int) $services_read_only;
  }

  public function ServicesType($services_type) {
    $this->services_type = (string) $services_type;
  }

  public function setServiceId($service_id) {
    $this->service_id = (int) $service_id;
  }

}
