<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class TripForeman.
 *
 * @package Drupal\move_long_distance\Entity
 */
class TripForeman {

  /**
   * @var int
   */
  public $foreman_id = 0;

  /**
   * @var string
   */
  public $foreman_name = "";



  // Getters.
  public function getForemanID() {
    return (int) $this->foreman_id;
  }

  public function getForemanName() {
    return (string) $this->foreman_name;
  }

  // Setters.
  public function setForemanId($foreman_id) {
    $this->foreman_id = (int) $foreman_id;
  }

  public function setForemanName($foreman_name) {
    $this->foreman_name = (string) $foreman_name;
  }

}
