<?php

namespace Drupal\move_long_distance\Services;

use Drupal\move_long_distance\Entity\Request;
use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\move_request\MoveRequestRetrieve;
use Drupal\move_services_new\Util\enum\TripJobType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class LongDistanceReuqest.
 *
 * @package Drupal\move_long_distance\Services
 */
class LongDistanceReuqest extends BaseService {
  private $nid = NULL;
  private $encoders;
  private $normalizers;
  private $serializer;

  public function __construct($nid = NULL) {
    $this->nid = $nid;
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);
  }

  public function setId($nid) {
    $this->nid = $nid;
  }

  public function create($data = array()) {}

  public function retrieve() {}

  public function update($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {

      $tables_to_save = array(
        'ld_request_address_from',
        'ld_request_address_to',
        'ld_request_sit',
      );
      $request = (array) $this->serializer->deserialize(json_encode($data), Request::class, 'json');
      foreach ($request as $field_name => $val) {
        if (in_array($field_name, $tables_to_save)) {
          $add_info[$field_name] = $val;
          unset($request[$field_name]);
        }
      }
      db_merge('ld_request')
        ->key(array('ld_nid' => $request['ld_nid']))
        ->fields($request)
        ->execute();

      if (!empty($add_info)) {
        foreach ($add_info as $table_name => $val) {
          $val = (array) $val;
          $key = array('ld_nid' => $request['ld_nid']);

          // For sit we need to add entity type.
          if ($table_name == 'ld_request_sit') {
            if (!empty($val['ld_nid'])) {
              $key = array('ld_nid' => $val['ld_nid'], 'entity_type' => TripJobType::REQUEST);
              $val['entity_type'] = TripJobType::REQUEST;
              db_merge($table_name)
                ->key($key)
                ->fields($val)
                ->execute();
            }
          }
          else {
            db_merge($table_name)
              ->key($key)
              ->fields($val)
              ->execute();
          }

        }
      }
    }
    catch (\Throwable $e) {
      $message = "Update LDRequest Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('LDRequest', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  public function index(int $page = 1, int $page_size = -1) {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    try {
      // Get total count of rows.
      $query = db_select('ld_storage', 'lds')
        ->fields('lds', array('storage_id'))
        ->execute();
      $total_count = $query->rowCount();
      // Calculate number of pages.
      $page_count = ($page_size > 0) ? ceil($total_count / $page_size) : $total_count;
      // Build pager info for front.
      $meta = array(
        'currentPage' => (int) $page,
        'perPage' => (int) $page_size,
        'pageCount' => (int) $page_count,
        'totalCount' => (int) $total_count,
      );
      // Number of row from we start our select.
      $start_from_page = ($page - 1) * $page_size;

      $result['items'] = array();
      $result['_meta'] = $meta;

      $query = db_select('ld_storage', 'lds');
      $query->fields('lds');
      if ($page_size > 0) {
        $query->range($start_from_page, $page_size);
      }
      $storages = $query->execute()->fetchAllAssoc('storage_id');
      if (!empty($storages)) {
        foreach ($storages as $storage_id => $val) {
          $phones_st = array();
          $phones = db_select('ld_storage_phones', 'lds')
            ->fields('lds', array('phone'))
            ->condition('storage_id', $this->storageId)
            ->execute();
          foreach ($phones as $val_p) {
            $phones_st[] = $val_p->phone;
          }
          $val->phones = $phones_st;
          $result['items'][$storage_id] = (array) $val;
        }
      }
      $result['items'] = array_values($result['items']);
    }
    catch (\Throwable $e) {
      $message = "Update Carrier Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('carrier', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  public static function writeRequestToLdRequest($nid) {
    $request_info_to_save = self::prepareRequestForSave($nid);
    if (!empty($request_info_to_save)) {
      $request_ins = new LongDistanceReuqest($nid);
      $request_ins->update($request_info_to_save);
    }
  }

  public function delete() {
    db_delete('ld_storage')
      ->condition('storage_id', $this->storageId)
      ->execute();

    db_delete('ld_storage_phones')
      ->condition('storage_id', $this->storageId)
      ->execute();
  }

  public static function prepareRequestForSave($nid) {
    $request_for_save = array();
    $address_field_to_change_keys = array(
      'locality' => 'city',
      'administrative_area' => 'state',
      'postal_code' => 'zip',
    );
    if (is_numeric($nid)) {
      $nid = node_load($nid);
    }
    $requestWrapper = entity_metadata_wrapper('node', $nid);
    if (!empty($requestWrapper)) {
      $request_retrieve = new MoveRequestRetrieve($requestWrapper);
      $request_entity = $request_retrieve->nodeFields();
      if ($request_entity['service_type']['raw'] == 7) {
        if ($request_entity['status']['raw'] == 3) {
          $request_for_save['ready_for_delivery'] = 0;
          $nid = $request_entity['nid'];
          $request_all_data = MoveRequest::getRequestAllData($nid);

          $distance_calculate = new DistanceCalculate();

          $request_for_save['ld_nid'] = $nid;
          $request_for_save['customer_name'] = $request_entity['name'];
          $request_for_save['pickup_date'] = (int) $request_entity['date']['raw'];
          $request_for_save['delivery_date'] = (int) $request_entity['ddate']['raw'];
          if (!empty($request_entity['ld_sit_delivery_dates']['value'])) {
            $request_for_save['delivery_date'] = $request_entity['ld_sit_delivery_dates']['value'];
          }
          else {
            $request_for_save['delivery_date'] = 0;
          }
          if (!empty($request_entity['ld_sit_delivery_dates']['value2'])) {
            $request_for_save['second_delivery_date'] = $request_entity['ld_sit_delivery_dates']['value2'];
          }
          else {
            $request_for_save['second_delivery_date'] = 0;
          }

          $request_for_save['distance_to_st'] = (float) ($request_all_data['request_distance']['distances']['AB']['distance'] ?? 0);
          $request_for_save['distance_from_st'] = (float) ($request_all_data['request_distance']['distances']['BC']['distance'] ?? 0);
          $request_for_save['distance'] = $request_for_save['distance_to_st'] + $request_for_save['distance_from_st'];

          $request_for_save['sales_weight'] = (float) $distance_calculate->getLongDistanceWeight($request_entity);
          $request_for_save['weight'] = (float) ($request_all_data['invoice']['closing_weight']['value'] ?? $request_for_save['sales_weight']);
          $request_for_save['ld_status'] = !empty($request_entity['ld_flag_status']['value']) ? $request_entity['ld_flag_status']['value'] : 0;
          $request_for_save['in_trip'] = (int) $requestWrapper->field_added_to_trip->value();
          $request_for_save['created'] = (int) $request_entity['created'];
          $request_for_save['address_from'] = $request_entity['adrfrom']['value'] . ', ' . $request_entity['apt_from']['value'];
          $request_for_save['address_to'] = $request_entity['adrto']['value'] . ', ' . $request_entity['apt_to']['value'];
          $request_for_save['request_flags'] = !empty($request_entity['field_company_flags']['value']) ? serialize($request_entity['field_company_flags']['value']) : '';

          if (!empty($request_for_save['delivery_date'])) {
            $request_for_save['ready_for_delivery'] = 1;
          }

          if (!empty($request_entity['field_company_flags']['value']) && isset($request_entity['field_company_flags']['value'][3])) {
            $request_for_save['ready_for_delivery'] = 1;
          }

          // schedule_delivery_date.
          $request_for_save['schedule_delivery_date'] = !empty($request_entity['schedule_delivery_date']['raw']) ? $request_entity['schedule_delivery_date']['raw'] : 0;

          // Change from dates.
          if (!empty($request_entity['field_moving_from'])) {
            foreach ($request_entity['field_moving_from'] as $request_field => $val) {
              if (isset($address_field_to_change_keys[$request_field])) {
                $request_for_save['ld_request_address_from'][$address_field_to_change_keys[$request_field]] = $val;
              }
            }
          }

          // Change to dates.
          if (!empty($request_entity['field_moving_to'])) {
            foreach ($request_entity['field_moving_to'] as $request_field => $val) {
              if (isset($address_field_to_change_keys[$request_field])) {
                $request_for_save['ld_request_address_to'][$address_field_to_change_keys[$request_field]] = $val;
              }
            }
          }
          $request_for_save['sit'] = 0;
          if (!empty($request_all_data['sit'])) {
            $request_for_save['ld_request_sit'] = $request_all_data['sit'];
            $request_for_save['ld_request_sit']['storage_id'] = (int) $request_all_data['sit']['storage_id'];
            $request_for_save['ld_request_sit']['date'] = $request_all_data['sit']['date'];
            $request_for_save['ld_request_sit']['foreman'] = (int) $request_all_data['sit']['foreman'];
            $request_for_save['ld_request_sit']['ld_nid'] = $nid;
            $request_for_save['sit'] = 1;
          }
        }
        else {
          $fields = array(
            'ld_request',
            'ld_request_address_to',
            'ld_request_address_from',
            'ld_request_sit',
          );
          // If request status was changed - remove it from search.
          foreach ($fields as $field_name) {
            $nid = $requestWrapper->getIdentifier();
            if (!empty($nid)) {
              db_delete($field_name)
                ->condition('ld_nid', $nid)
                ->execute();
            }
          }

        }
      }
    }
    return $request_for_save;
  }

  /**
   * Update LD request weght.
   *
   * @param int $nid
   *   Request id.
   * @param int|null $weight
   *   Weight or NULL.
   */
  public static function updateLdRequestWeight(int $nid, ?int $weight) : void {
    if (!is_null($weight)) {
      db_update('ld_request')
        ->fields(['weight' => $weight])
        ->condition('ld_nid', $nid)
        ->execute();
    }
  }

  /**
   * Search request in ld delivery page.
   *
   * @param array $condition
   *   Conditions.
   * @param array $sorting
   *   Sorting.
   * @param int $page
   *   Current page.
   * @param int $page_size
   *   Page size.
   *
   * @return array
   *   Requests.
   */
  public static function search(array $condition, array $sorting = array(), int $page = 1, int $page_size = -1) {
    $result = array();

    // Fields to select from db.
    $address_fields = array(
      'zip',
      'city',
      'state',
    );

    $sit_fields = array(
      'client_name',
      'storage_id',
      'foreman',
      'date',
      'blankets',
    );
    // This is to know which address field to use. Synonyms for tables.
    $address_type = array(
      1 => 'lraf',
      2 => 'lrat',
    );
    $query = db_select('ld_request', 'ld');
    $query->leftJoin('ld_request_address_from', $address_type[1], 'ld.ld_nid = ' . $address_type[1] . '.ld_nid');
    $query->leftJoin('ld_request_address_to', $address_type[2], 'ld.ld_nid = ' . $address_type[2] . '.ld_nid');
    $query->leftJoin('ld_request_sit', 'sit', 'ld.ld_nid = sit.ld_nid');
    $query->leftJoin('ld_storage', 'lds', 'sit.storage_id = lds.storage_id');
    // Attach trips tables.
    $query->leftJoin('ld_trip_jobs', 'ltj', 'ld.ld_nid = ltj.job_id');
    $query->leftJoin('ld_trip_details', 'ltd', 'ltj.trip_id = ltd.ld_trip_id');

    $query->leftJoin('users', 'u', 'sit.foreman = u.uid');
    $query->fields('ld');
    $query->fields('sit', $sit_fields);
    $query->addField('u', 'name', 'foreman_name');
    $query->addField('lds', 'name', 'storage_name');
    $query->addField('sit', 'status', 'sit_status');
    // We need this to get same fields with aliases.
    foreach ($address_fields as $field_name) {
      $query->addField($address_type[1], $field_name, 'from_' . $field_name);
    }
    foreach ($address_fields as $field_name) {
      $query->addField($address_type[2], $field_name, 'to_' . $field_name);
    }

    // Trip fields.
    $query->fields('ltd', array('ld_trip_id'));
    $query->addField('ltd', 'flag', 'trip_status');

    if (!empty($condition['filters'])) {
      $query_fields = $query->getFields();
      foreach ($condition['filters'] as $field_name => $val) {
        if (!empty($val) || $val === 0) {
          switch ($field_name) {
            case 'to_state':
            case 'from_state':
              $operator = 'IN';
              break;

            case 'date':
            case 'pickup_date':
            case 'delivery_date':
              if (!empty($val['from']) && !empty($val['to'])) {
                $date[0] = $val['from'];
                $date[1] = $val['to'];
                $val = $date;
                $operator = 'BETWEEN';
              }
              if (!empty($val['from']) && empty($val['to'])) {
                $val = $val['from'];
                $operator = '>';
              }
              if (empty($val['from']) && !empty($val['to'])) {
                $val = $val['to'];
                $operator = '<';
              }
              break;

            case 'to_city':
            case 'from_city':
              $val = db_like($val) . '%';
              $operator = 'LIKE';
              break;

            case 'job_id':
              $field_name = 'ld.ld_nid';
              $val = db_like($val) . '%';
              $operator = 'LIKE';
              break;

            default:
              $operator = '=';
          }
          // We need this yo use aliases for condition.
          // Drupal condition don't use alias.
          if (isset($query_fields[$field_name])) {
            $field_name = $query_fields[$field_name]['table'] . '.' . $query_fields[$field_name]['field'];
          }
          $query->condition($field_name, $val, $operator);
        }
      }
    }
    if (!empty($sorting['orderKey']) && $sorting['orderKey'] == 'r_type') {
      $order_r_type = $sorting['orderKey'];
      $order_val = $sorting['orderValue'];
    }
    else {
      $query->orderBy($sorting['orderKey'], $sorting['orderValue']);
    }

    $total_count = $query->execute()->rowCount();

    // Calculate number of pages.
    $page_count = ceil($total_count / $page_size);
    // Build pager info for front.
    $meta = array(
      'currentPage' => (int) $page,
      'perPage' => (int) $page_size,
      'pageCount' => (int) $page_count,
      'totalCount' => (int) $total_count,
    );
    // Number of row from we start our select.
    $start_from_page = ($page - 1) * $page_size;

    if ($page_size > 0) {
      $query->range($start_from_page, $page_size);
    }
    $result['_meta'] = $meta;

    $requests = $query->execute();

    // Request type(pickup or delivery)
    $current_date = time();
    foreach ($requests as $row) {
      $r_type = 'Pickup';
      if ($row->pickup_date < $current_date) {
        $r_type = 'Delivery';
      }
      if ($row->delivery_date > $current_date) {
        $r_type = 'Delivery';
      }
      if (!empty($row->ready_for_delivery)) {
        $r_type = 'Delivery';
      }
      $row->r_type = $r_type;
      $row->weight = number_format((float) $row->weight * 7, 2) . ' / ' . number_format($row->weight, 2);
      $row->sales_weight = number_format((float) $row->sales_weight * 7, 2) . ' / ' . number_format($row->sales_weight, 2);

      $result['items'][] = (array) $row;
    }
    if (!empty($order_r_type)) {
      $result['items'] = (array) $result['items'];
      uasort($result['items'], array('self', 'sortTripByRsort'));
      if ($order_val == "ASC") {
        $result['items'] = array_values($result['items']);
        $result['items'] = array_reverse($result['items']);
      }
      $result['items'] = array_values($result['items']);
    }

    return $result;
  }

  public static function updateLDDeliveryDates($nid, $delivery_dates) {
    $node = new \stdClass();
    $node->nid = $nid;
    $node->type = 'move_request';
    $update = FALSE;
    // Entity metadata wrapper not work here.
    if (!empty($delivery_dates['value'])) {
      $update = TRUE;
      $node->field_ld_sit_delivery_dates[LANGUAGE_NONE][0]['value'] = $delivery_dates['value'];
    }
    if (!empty($delivery_dates['value2'])) {
      $update = TRUE;
      $node->field_ld_sit_delivery_dates[LANGUAGE_NONE][0]['value2'] = $delivery_dates['value2'];
    }

    if ($update) {
      field_attach_presave('node', $node);
      field_attach_update('node', $node);
    }
  }

  /**
   * Add all LD request to ld search
   */
  public static function addRequestTOlDRequets() {
    $query = search_api_query('move_request');
    $query->condition('field_approve', 3, '=');
    $query->condition('field_move_service_type', 7);
    $data = $query->execute();
    $nids = array_keys($data['results']);
    if (!empty($nids)) {
      foreach ($nids as $nid) {
        self::writeRequestToLdRequest($nid);
      }
    }
    return TRUE;
  }

  /**
   * Get foreman name by his id.
   * @param $uid
   * @return string
   */
  public static function getFormenNameById($uid) {
    $result = '';
    if (!empty($uid)) {
      $result = db_select('users', 'u')
        ->fields('u', array('name'))
        ->condition('uid', $uid)
        ->execute()
        ->fetchField();
    }
    return $result;
  }

  public static function inSit($nid) {
    $result = FALSE;
    if (!empty($uid)) {
      $result = db_select('ld_request_sit', 'ldrs')
        ->fields('ldrs', array('ld_nid'))
        ->condition('ld_nid', $nid)
        ->execute()
        ->fetchField();
    }
    return $result;
  }

  /**
   * @param $a
   * @param $b
   * @return int
   */
  public static function sortTripByRsort($a, $b) {
    if ($a['r_type'] == $b['r_type']) {
      return 0;
    }
    return ($a['r_type'] < $b['r_type']) ? -1 : 1;
  }


}
