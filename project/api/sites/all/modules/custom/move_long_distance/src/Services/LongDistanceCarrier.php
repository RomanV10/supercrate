<?php

namespace Drupal\move_long_distance\Services;

use Drupal\move_long_distance\Services\Actions\SitInvoiceActions;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_long_distance\Entity\Carrier;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\TripPaymentType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Drupal\move_new_log\Services\Log;
use PDO;
/**
 * Class LongDistanceCarrier.
 *
 * @package Drupal\move_long_distance\Services
 */
class LongDistanceCarrier extends BaseService {
  private $carId = NULL;
  private $encoders;
  private $normalizers;
  private $serializer;

  public function __construct($carId = NULL) {
    $this->carId = $carId;
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);
  }

  public function setId($id) {
    $this->carId = $id;
  }

  /**
   * Create carrier.
   *
   * @param array $data
   *   Array of carrier info to save.
   * @return bool|\DatabaseStatementInterface|int|mixed
   */
  public function create($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      $carrier = $this->serializer->deserialize(json_encode($data), Carrier::class, 'json');
      $this->requireFieldValidation($carrier);
      // Check for required fields.
      if (!empty($carrier->error)) {
        $result = $carrier;
      }
      else {
        $carrier->created = ($carrier->created) ? $carrier->created : time();

        // Adding phones to separate variable.
        $carrier_phones = $carrier->phones;
        // Unset phones from carrier object. We don't have this field in db.
        // If keep it will have error.
        unset($carrier->phones);
        $carrier->created = time();
        $carrier_id = db_insert('ld_carrier')
          ->fields((array) $carrier)
          ->execute();

        // Adding phones for carrier.
        if (!empty($carrier_id) && !empty($carrier_phones)) {
          $insert = db_insert('ld_carrier_phones')
            ->fields(array('carrier_id', 'phone'));
          foreach ($carrier_phones as $phone) {
            if (!empty($phone)) {
              $insert->values(array('carrier_id' => $carrier_id, 'phone' => $phone));
            }
          }
          $insert->execute();
        }
        if (!empty($carrier_id)) {
          $log_title = 'Carrier "' . $carrier->name . '" was created.';
          $log_text = array();
          $carrier_inst = new LongDistanceCarrier($carrier_id);
          $carrier_inst->setLogs($log_title, $log_text, 'Carrier');
        }
        $result = $carrier_id;
      }
    }
    catch (\Throwable $e) {
      $message = "Create Carrier Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('carrier', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Retreive carrier info.
   *
   * @param int $active
   *   All active carriers. 0 or 1.
   * @param int $with_jobs
   *   If set to 1 show all carrier with his jobs. Default 0.
   * @param int $page
   *   NUmber of page to show.
   * @param int $page_size
   *   Number of carrier jobs on this page.
   * @param int $total
   *   Show carrier total closing.
   * @param int $hide_zero
   *   Hide carrier with zero balance.
   *
   * @return array|bool|mixed
   */
    public function retrieve(int $active = 1, int $with_jobs = 0, int $page = 1, int $page_size = -1, int $total = 1, int $hide_zero = 0) {
    $error_flag = FALSE;
    $error_message = "";
    $carrier = FALSE;
    try {
      $carrier_info = db_select('ld_carrier', 'lc')
        ->fields('lc')
        ->condition('ld_carrier_id', $this->carId)
        ->execute()
        ->fetchAssoc();

      $carrier_info['phones'] = array();
      $phones = db_select('ld_carrier_phones', 'lcp')
        ->fields('lcp', array('phone'))
        ->condition('carrier_id', $this->carId)
        ->execute();
      foreach ($phones as $val) {
        $carrier_info['phones'][] = $val->phone;
      }
      if (!empty($with_jobs)) {
        // Carrier jobs closing.
        $carrier = self::getCarrierJobsClosing($this->carId, $page, $page_size, $hide_zero, TRUE);
        if (!empty($total)) {
          // Total of carrier jobs closing.
          $carrier['total'] = $this->getCarrierTotal($this->carId, FALSE, $hide_zero);
          $carrier['info'] = $carrier_info;
        }
      }
      else {
        $carrier = $carrier_info;
      }
    }
    catch (\Throwable $e) {
      $message = "Retrieve Carrier Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('carrier', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $carrier;
    }
  }

  /**
   * Update carrier info.
   *
   * @param array $data
   *   Array of carrier info to update.
   *
   * @return bool|mixed
   */
  public function update($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      if (is_numeric($this->carId)) {
        $carrier = $this->serializer->deserialize(json_encode($data), Carrier::class, 'json');
        $carrier_phones = $carrier->phones;
        $this->requireFieldValidation($carrier);
        if (!empty($carrier->error)) {
          $result = $carrier;
        }
        else {
          // Unset created time. We no need to update it.
          unset($carrier->created);
          // Adding phones to separate variable.
          // Unset phones from carrier object. We don't have this field in db.
          // If keep it will have error.
          unset($carrier->phones);
          $carrier_id = db_update('ld_carrier')
            ->fields((array) $carrier)
            ->condition('ld_carrier_id', $this->carId)
            ->execute();

          // Update phones for carrier.
          if (!empty($carrier_phones)) {
            // First we need to delete all rows for carrier phones.
            db_delete('ld_carrier_phones')
              ->condition('carrier_id', $this->carId)
              ->execute();
            $insert = db_insert('ld_carrier_phones')
              ->fields(array('carrier_id', 'phone'));
            foreach ($carrier_phones as $phone) {
              // If phones is empty we no need add them to db.
              if (!empty($phone)) {
                $insert->values(array('carrier_id' => $this->carId, 'phone' => $phone));
              }
            }
            $insert->execute();
          }
          if (!empty($this->carId)) {
            $log_title = 'Carrier "' . $carrier->name . '" was updated.';
            $log_text = array();
            $carrier_inst = new LongDistanceCarrier($this->carId);
            $carrier_inst->setLogs($log_title, $log_text, 'Carrier');
          }
          $result = TRUE;
        }
      }
      else {
        $error_flag = TRUE;
        $message = "Carrier id must be numeric";
        watchdog('carrier update', $message, array(), WATCHDOG_CRITICAL);
        throw new \Exception($message, 406);
      }

    }

    catch (\Throwable $e) {
      $message = "Update Carrier Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('carrier update', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Delete carrier.
   */
  public function delete() {
    db_delete('ld_carrier')
      ->condition('ld_carrier_id', $this->carId)
      ->execute();

    db_delete('ld_carrier_phones')
      ->condition('carrier_id', $this->carId)
      ->execute();
    if (!empty($this->carId)) {
      $log_title = 'Carrier "' . $this->carId . '" was updated.';
      $log_text = array();
      $carrier_inst = new LongDistanceCarrier($this->carId);
      $carrier_inst->setLogs($log_title, $log_text, 'Carrier');
    }
  }

  public function index($current_page, $per_page, $active, $short_list, $total = FALSE, $hide_zero = 0, $sorting = array(), $condition = array(), $hide_no_trips = FALSE) {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    try {
      if (!empty($hide_zero)) {
        $carrier_with_zero_balance = array_keys($this->getCarrierWithBalanceOnly($active, $hide_zero));
      }
      // Get total count of rows.
      $query = db_select('ld_carrier', 'lc');
      $query->fields('lc', array('ld_carrier_id'));
      $query->condition('active', (int) $active);
      if (!empty($carrier_with_zero_balance)) {
        $query->condition('ld_carrier_id', $carrier_with_zero_balance, 'NOT IN');
      }
      // Filtering.
      if (!empty($condition['filters'])) {
        foreach ($condition['filters'] as $name => $val) {
          if ($val != '') {
            $operator = '=';
            switch ($name) {
              case 'name':
                $val = db_like($val) . '%';
                $operator = 'LIKE';
                break;
            }
            $query->condition($name, $val, $operator);
          }
        }
      }

      $total_count = $query->execute()->rowCount();
      // Calculate number of pages.
      $page_count = ceil($total_count / $per_page);
      // Build pager info for front.
      $meta = array(
        'currentPage' => (int) $current_page,
        'perPage' => (int) $per_page,
        'pageCount' => (int) $page_count,
        'totalCount' => (int) $total_count,
      );
      // Number of row from we start our select.
      $start_from_page = ($current_page - 1) * $per_page;

      $result['items'] = array();
      $result['_meta'] = $meta;

      if (!empty($short_list)) {
        $query = db_select('ld_carrier', 'lc');
        $query->fields('lc', array('ld_carrier_id', 'name'));
        if (!empty($carrier_with_zero_balance)) {
          $query->condition('ld_carrier_id', $carrier_with_zero_balance, 'NOT IN');
        }
        $query->condition('active', (int) $active);
        $query->range($start_from_page, $per_page);
        // Sorting.
        if (!empty($sorting)) {
          $query->orderBy($sorting['orderKey'], $sorting['orderValue']);
        }

        // Filtering.
        if (!empty($condition['filters'])) {
          foreach ($condition['filters'] as $name => $val) {
            $operator = '=';
            switch ($name) {
              case 'name':
                $val = db_like($val) . '%';
                $operator = 'LIKE';
                break;
            }
            $query->condition($name, $val, $operator);
          }
        }

        // Hide carrier without trips.
        if (!empty($hide_no_trips)) {
          $this->filterCarrieListDontShowWithoutJobs($query);
        }

        $carriers = $query->execute()->fetchAllAssoc('ld_carrier_id');
        if (!empty($carriers)) {
          $temp_carrier = array();
          foreach ($carriers as $carrier_id => $val) {
            $temp_carrier[$carrier_id] = $val;
            if (!empty($total)) {
              $temp_carrier[$carrier_id]->total = !empty($this->getCarrierTotal($carrier_id)) ? $this->getCarrierTotal($carrier_id) : FALSE;
            }
          }
          $result['items'] = array_values($temp_carrier);
        }
      }
      else {
        $fields_for_index_page = array(
          'ld_carrier_id',
          'name',
          'contact_person',
          'contact_person_phone',
          'city',
          'state',
          'email',
          'active',
        );
        if (!empty($total_count)) {
          // Getting carriers.
          $query = db_select('ld_carrier', 'lc');
          $query->fields('lc', $fields_for_index_page);
          $query->condition('active', (int) $active);
          // Sorting.
          if (!empty($sorting)) {
            $query->orderBy($sorting['orderKey'], $sorting['orderValue']);
          }

          // Filtering.
          if (!empty($condition['filters'])) {
            foreach ($condition['filters'] as $name => $val) {
              if ($val != '') {
                $operator = '=';
                switch ($name) {
                  case 'name':
                    $val = db_like($val) . '%';
                    $operator = 'LIKE';
                    break;
                }
                $query->condition($name, $val, $operator);
              }
            }
          }

          $query->range($start_from_page, $per_page);
          $query_result = $query->execute();
          if (!empty($query_result)) {
            foreach ($query_result as $value) {
              $carriers[$value->ld_carrier_id] = $value;
              // Id's for selecting phones.
              $carriers_ids[$value->ld_carrier_id] = $value->ld_carrier_id;
            }

            if (!empty($total)) {
              foreach ($carriers_ids as $carrier_id) {
                $carriers[$carrier_id]['total'] = $this->getCarrierTotal($carrier_id);
              }
            }

            // Select phone numbers for carrier.
            // TODO maybe there is a way to make other way.
            $query = db_select('ld_carrier_phones', 'lcp')
              ->fields('lcp')
              ->condition('carrier_id', $carriers_ids, 'IN');
            $query_phone_result = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($query_phone_result)) {
              foreach ($query_phone_result as $row) {
                $carrier_phones[$row['carrier_id']][] = $row['phone'];
              }
            }

            foreach ($carriers_ids as $carrier_id) {
              $carriers[$carrier_id]->phones = $carrier_phones[$carrier_id];
            }
            if (!empty($carriers)) {
              // Build result for the front.
              $result['items'] = array_values($carriers);
            }
          }
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Index Carrier Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('carrier', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  public static  function getCarrierList($active = TRUE) {
    $query = db_select('ld_carrier', 'lc');
    $query->fields('lc', array('ld_carrier_id', 'name'));
    if ($active) {
      $query->condition('active', 1);
    }
    $result  = $query->execute()->fetchAllAssoc('ld_carrier_id');

    return $result;
  }

  /**
   * Get carrier id with balance number only.
   *
   * @param bool $active
   * @param bool $hide_zero
   */
  public function getCarrierWithBalanceOnly($active = TRUE, $hide_zero = FALSE) {
    $carriers = array();
    $carrier_list = self::getCarrierList($active);
    if (!empty($carrier_list)) {
      foreach ($carrier_list as $list) {
        $total = $this->getCarrierTotal($list->ld_carrier_id);
        $balance = !empty($total['balance']) ? (float) round($total['balance']) : 0;
        if (!empty($hide_zero)) {
          if (empty($balance)) {
            $carriers[$list->ld_carrier_id] = $balance;
          }
        }
        else {
          $carriers[$list->ld_carrier_id] = $balance;
        }
      }
      return $carriers;
    }
  }

  /**
   * Filer carrier that doesn't add to trip.
   * @param $query
   */
  public function filterCarrieListDontShowWithoutJobs(&$query) {
    $query->leftJoin('ld_trip_carrier', 'trip_carrier', 'trip_carrier.carrier_id = lc.ld_carrier_id');
    $query->leftJoin('ld_tp_delivery_details', 'tp_delivery_trip_carrier', 'tp_delivery_trip_carrier.delivery_for_company = lc.ld_carrier_id');
    $db_or = db_or();
    $db_or->isNotNull('trip_carrier.carrier_id');
    $db_or->isNotNull('tp_delivery_trip_carrier.delivery_for_company');
    $query->condition($db_or);
  }


  /**
   *
   * @return array
   */
  public static function getCarriersWithTrips() {
    $result = array();
    $query = db_select('long_distance_carrier', 'ldc');
    $query->innerJoin('ld_trip_additional', 'lta', 'ldc.id=lta.carrier');
    $query->innerJoin('long_distance_trip', 'ldt', 'ldt.id=lta.trip_id');
    $query->fields('ldc', array('id'));
    $query->fields('ldc', array('value'));
    $query->fields('ldt', array('value'));
    $trips = $query->execute()->fetchAll();

    foreach ($trips as $key => $trip) {
      $cucumber = unserialize($trip->value);
      if (!isset($result[$trip->id]['carrier'])) {
        $result[$trip->id]['carrier'] = $cucumber;
      }
      $abc[$trip->id] = reset($result);
      if (array_key_exists($trip->id, $abc)) {
        $result[$trip->id]['carrier']['trip'][] = unserialize($trip->ldt_value);
      }
    }

    return $result;
  }

  /**
   * Get price per CF for Carrier.
   *
   * @param int $car_id
   *    The carrier id.
   *
   * @return mixed
   *    Return price number.
   */
  public static function getCarrierCFPrice($car_id) {
    $result = db_select('ld_carrier', 'lc')
      ->fields('lc', array('per_cf'))
      ->condition('ld_carrier_id', $car_id)
      ->execute()
      ->fetchField();

    return (float) $result;
  }

  /**
   * Get all jobs closing for carrier.
   *
   * @param int $carrier_id
   *    Id of carrier.
   * @param int $page
   *   If use page, then this in the number of start page.
   * @param int $page_size
   *   Number elements on page. If -1 then show all.
   *
   * @return array
   *   Array of jobs with meta pages.
   */
  public static function getCarrierJobsClosing($carrier_id, $page = 1, $page_size = -1, $hide_zero = 0, $with_payments = FALSE) {
    $result['items'] = array();
    $start_from_page = 0;


    // Get all carrier delivery jobs.
    $query = db_select('ld_tp_delivery_job', 'delivery_job');
    $query->leftJoin('ld_trip_closing', 'ltclo', 'delivery_job.job_id = ltclo.job_id');
    $query->leftJoin('ld_tp_delivery_details', 'delivery_details', 'delivery_job.ld_tp_delivery_id = delivery_details.ld_tp_delivery_id');
    $query->fields('delivery_job');
    $query->fields('ltclo');
    $query->condition('delivery_details.delivery_for_company', $carrier_id);
    if (!empty($hide_zero)) {
      $query->condition('ltclo.balance', 0, '<>');
    }
    $query->orderBy("ltclo.ld_trip_id", "ASC");
    $tp_delivery_jobs = $query->execute()->fetchAll(PDO::FETCH_ASSOC);


    $query = db_select('ld_trip_carrier', 'ltc');
    $query->leftJoin('ld_trip_closing', 'ltclo', 'ltc.ld_trip_id = ltclo.ld_trip_id');
    $query->fields('ltclo');
    $query->condition('ltc.carrier_id', $carrier_id);
    $query->isNotNull('ltclo.balance');
    $query->isNull('ltclo.is_total');
    if (!empty($hide_zero)) {
      $query->condition('ltclo.balance', 0, '<>');
    }
    $query->orderBy("ltclo.ld_trip_id", "ASC");
    $jobs = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
    $result['items'] = array_merge($tp_delivery_jobs, $jobs);
    // This is the check for using pages on not.
    // If $per_page = -1 - show all results.
    if ($page_size > 0) {
      $total_count = count($result['items']);

      // Calculate number of pages.
      $page_count = ceil($total_count / $page_size);
      // Build pager info for front.
      $meta = array(
        'currentPage' => (int) $page,
        'perPage' => (int) $page_size,
        'pageCount' => (int) $page_count,
        'totalCount' => (int) $total_count,
      );
      // Number of row from we start our select.
      $start_from_page = ($page - 1) * $page_size;
      $result['_meta'] = $meta;
      $result['items'] = array_slice($result['items'], $start_from_page, $page_size);
    }
    if ($with_payments) {
      $invoice_jobs_total = array();
      $job = new LongDistanceJob();
      foreach ($result['items'] as $key => $item) {
        $job->setId($item['job_id']);
        $entity_type = !empty($item['type']) ? EntityTypes::LDTDJOB : EntityTypes::MOVEREQUEST;
        $payment_types = array(TripPaymentType::PAID, TripPaymentType::RECEIVED);
        $receipt = $job->getJobReceipt($payment_types, $entity_type, TRUE);
        $receipt = reset($receipt);
        $invoice = $job->getJobInvoice();
        // Add receipts.
        if (!empty($receipt)) {
          $result['items'][$key]['receipt_id'] = (int) $receipt['receipt_id'];
          $result['items'][$key]['receipt_type'] = $receipt['type'];
          $result['items'][$key]['receipt'] = $receipt;
          // Get total for all jobs inside receipt.
          // Will do this only once for one receipt id.
          if (!empty($receipt['value']['jobs']) && empty($receipt_jobs_total[$receipt['receipt_id']])) {
            $total_receipts_jobs = SitInvoiceActions::getInfoForInvoiceOrPay($receipt['value']['jobs']);
            $receipt_jobs_total[$receipt['receipt_id']] = $total_receipts_jobs['total'];
          }
          if (!empty($receipt_jobs_total[$receipt['receipt_id']])) {
            $result['items'][$key]['receipt']['jobs_total'] = $receipt_jobs_total[$receipt['receipt_id']];
          }
        }
        else {
          $result['items'][$key]['receipt_id'] = 0;
        }

        // Add invoices.
        if ($invoice) {
          $result['items'][$key]['invoice_id'] = (int) $invoice['id'];
          $result['items'][$key]['invoice'] = $invoice;
          $result['items'][$key]['receipt_id'] = 0;
          // Get total for all jobs inside invoice.
          // Will do this only once for one invoice id.
          if (!empty($invoice['data']['jobs_id']) && empty($invoice_jobs_total[$invoice['id']])) {
            $total_invoice_jobs = SitInvoiceActions::getInfoForInvoiceOrPay($invoice['data']['jobs_id']);
            $invoice_jobs_total[$invoice['id']] = $total_invoice_jobs['total'];
          }
          if (!empty($invoice_jobs_total[$invoice['id']])) {
            $result['items'][$key]['invoice']['jobs_total'] = $invoice_jobs_total[$invoice['id']];
          }
        }
        else {
          $result['items'][$key]['invoice_id'] = 0;
        }
      }
    }
    return $result;
  }

  /**
   * Get total for carrier closing.
   *
   * @param bool $carrier_id
   *   Id of carrier. If there is no id - will use id from $this.
   * @param bool $fields_to_use
   *   Array of fields for calculation and response.
   *
   * @return array
   *   Array of carriers with total info.
   */
  public function getCarrierTotal($carrier_id = FALSE, $fields_to_use = FALSE, $hide_zero = 0) {
    $total = array();
    if (!$carrier_id) {
      $carrier_id = $this->carId;
    }
    if (!$fields_to_use) {
      $fields_to_use = array(
        'volume_cf',
        'job_cost',
        'services_total',
        'job_total',
        'tp_collected',
        'received',
        'paid',
        'to_receive',
        'to_pay',
        'balance',
        'order_balance',
      );
    }

    // Get total for jobs.
    $query = db_select('ld_trip_closing', 'ltc');
    $query->leftJoin('ld_trip_carrier', 'ltcar', 'ltc.ld_trip_id = ltcar.ld_trip_id');
    if (!empty($hide_zero)) {
      $query->condition('ltc.balance', 0, '<>');
    }
    $query->condition('ltcar.carrier_id', $carrier_id);
    $query->isNotNull('is_total');
    foreach ($fields_to_use as $field_name) {
      $query->addExpression('SUM({' . $field_name . '})', $field_name);
    }
    $result = $query->execute()->fetchAssoc();
    // This is the check if carrier not have jobs.
    // TODO need to think other check.
    if (!is_null($result['balance'])) {
      $total = LongDistanceJob::closingCalculation($result, TRUE);
    }


    $tp_delivery_jobs = LongDistanceTPDelivery::getDeliveryJobsForCompany($carrier_id);
    // Total for tp delivery jobs.
    if (!empty($tp_delivery_jobs)) {
      $query = db_select('ld_trip_closing', 'ltc');
      foreach ($fields_to_use as $field_name) {
        $query->addExpression('SUM({' . $field_name . '})', $field_name);
      }
      $query->condition('job_id', $tp_delivery_jobs, 'IN');
      $tp_total = $query->execute()->fetchAssoc();

      // Calculate total numbers.
      $tp_delivery_total = LongDistanceJob::closingCalculation($tp_total, TRUE, 'tp_delivery');
      if (!empty($total)) {
        foreach ($total as $field_name => $val) {
          $total[$field_name] += $tp_delivery_total[$field_name];
        }
      }
      else {
        $total = $tp_delivery_total;
      }
    }
    return $total;
  }

  /**
   * Get all carriers receipt with total.
   *
   * @return array|mixed
   *   Array with job_id, trip_id, receipt_id, total,
   */
  public function getCarrierReceipts($page = 1, int $page_size = -1) {
    $result = array();
    $error_flag = FALSE;
    $error_message = "";
    try {
      $start_from_page = 0;
      $result['items'] = array();
      // Receipts for request job.
      $query = db_select('ld_trip_receipts', 'ltr');
      $query->leftJoin('ld_trip_receipts_details', 'ltrd', 'ltr.receipt_id = ltrd.receipt_id');
      $query->leftJoin('ld_trip_carrier', 'ltc', 'ltrd.trip_id = ltc.ld_trip_id');
      $query->leftJoin('moveadmin_receipt', 'mar', 'ltr.receipt_id = mar.id');
      $query->fields('mar', array('receipt'));
      $query->fields('ltr', array('receipt_id', 'created', 'amount'));
      $query->condition('ltc.carrier_id', $this->carId)
        ->condition('mar.entity_type', EntityTypes::MOVEREQUEST, '<>')
        ->isNotNull('mar.created')
        ->groupBy('ltr.receipt_id');
      $job_receipts = $query->execute()->fetchAll();


      // Receipt for tp delivery job.
      $query = db_select('ld_trip_receipts', 'ltr');
      $query->leftJoin('ld_trip_receipts_details', 'ltrd', 'ltr.receipt_id = ltrd.receipt_id');
      $query->leftJoin('ld_tp_delivery', 'ltd', 'ltrd.trip_id = ltd.ld_trip_id');
      $query->leftJoin('ld_tp_delivery_details', 'ltdd', 'ltd.ld_tp_delivery_id = ltdd.ld_tp_delivery_id');
      $query->leftJoin('moveadmin_receipt', 'mar', 'ltr.receipt_id = mar.id');
      $query->fields('mar', array('receipt'));
      $query->fields('ltr', array('receipt_id', 'created', 'amount'));
      $query->condition('ltdd.delivery_for_company', $this->carId)
        ->condition('mar.entity_type', EntityTypes::MOVEREQUEST, '<>')
        ->isNotNull('mar.created')
        ->groupBy('ltr.receipt_id');
      $tp_job_receipts = $query->execute()->fetchAll();
      $receipts = array_merge($job_receipts, $tp_job_receipts);
      if ($page_size > 0) {
        // Calculate number of pages.
        $total_count = count($receipts);
        $page_count = ceil($total_count / $page_size);

        // Build pager info for front.
        $meta = array(
          'currentPage' => (int) $page,
          'perPage' => (int) $page_size,
          'pageCount' => (int) $page_count,
          'totalCount' => (int) $total_count,
        );

        // Number of row from we start our select.
        $start_from_page = ($page - 1) * $page_size;
        $result['_meta'] = $meta;
        $receipts = array_slice($receipts, $start_from_page, $page_size);
      }
      if (!empty($receipts)) {
        foreach ($receipts as $row) {
          $receipt_value = unserialize($row->receipt);
          if (empty($receipt_value['invoice_id'])) {
            // This check for receipts created form tp delivery.
            if (!LongDistanceTPDelivery::tpDeliveryReceiptNotFromAgentFolio($receipt_value)) {
              $jobs_total = SitInvoiceActions::getInfoForInvoiceOrPay($receipt_value['jobs']);
              $items[$row->receipt_id]['jobs_total'] = $jobs_total['total'];
              // Remove unwanted values.
              unset($row->receipt);

              // Get receipt items.
              $items[$row->receipt_id]['items'] = $jobs_total['items'];

              $items[$row->receipt_id]['total'] = $row->amount;
              $items[$row->receipt_id]['created'] = $row->created;
              $items[$row->receipt_id]['value'] = $receipt_value;
              $items[$row->receipt_id]['receipt_id'] = $row->receipt_id;
            }
            $result['items'] = array_values($items);
          }
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Get receipt for carrier Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Carrier get receipt', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Get all carrier invoices with jobs and total.
   *
   * @return array|mixed
   *   Jobs info with total
   */
  public function getCarrierInvoices($page = 1, int $page_size = -1) {
    $items = array();
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    try {
      $query = db_select('ld_invoices', 'li');
      $query->leftJoin('ld_invoices_details', 'lid', 'li.invoice_id = lid.invoice_id');
      $query->leftJoin('move_invoice', 'mi', 'lid.invoice_id = mi.id');
      $query->addField('li', 'amount', 'total_amount');
      $query->fields('lid')
        ->fields('mi', array())
        ->condition('lid.carrier_id', $this->carId)
        ->groupBy('li.invoice_id');

      $total_count = $query->execute()->rowCount();
      // Calculate number of pages.
      $page_count = ceil($total_count / $page_size);
      // Build pager info for front.
      $meta = array(
        'currentPage' => (int) $page,
        'perPage' => (int) $page_size,
        'pageCount' => (int) $page_count,
        'totalCount' => (int) $total_count,
      );
      // Number of row from we start our select.
      $start_from_page = ($page - 1) * $page_size;
      $result['_meta'] = $meta;


      $query = db_select('ld_invoices', 'li');
      $query->leftJoin('ld_invoices_details', 'lid', 'li.invoice_id = lid.invoice_id');
      $query->leftJoin('move_invoice', 'mi', 'lid.invoice_id = mi.id');
      $query->addField('li', 'amount', 'total_amount');
      $query->fields('li', array('created'));
      $query->fields('lid');
      $query->fields('mi');
      $query->condition('lid.carrier_id', $this->carId);
      $query->groupBy('li.invoice_id');
      if ($page_size > 0) {
        $query->range($start_from_page, $page_size);
      }
      $invoices = $query->execute();
      foreach ($invoices as $row) {
        $items[$row->invoice_id]['total'] = (float) $row->total_amount;
        unset($row->total_amount);
        $value = unserialize($row->data);
        unset($row->data);
        $jobs_total = SitInvoiceActions::getInfoForInvoiceOrPay($value['jobs_id']);
        $items[$row->invoice_id]['items'] = $jobs_total['items'];
        $items[$row->invoice_id]['jobs_total'] = $jobs_total['total'];
        $items[$row->invoice_id]['value'] = $value;
        $items[$row->invoice_id]['data'] = $value;
        $items[$row->invoice_id]['flag'] = $value['flag'];
        $items[$row->invoice_id]['created'] = $row->created;
        $items[$row->invoice_id]['invoice_id'] = $row->invoice_id;
        $items[$row->invoice_id]['hash'] = $row->hash;
        if (!isset($items[$row->invoice_id]['receipts'])) {
          $items[$row->invoice_id]['receipts'] = SitInvoiceActions::getInvoiceReceipts($row->invoice_id);
        }

      }
      $result['items'] = array_values($items);
    }
    catch (\Throwable $e) {
      $message = "Get invoices for carrier Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Carrier get invoices', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Check required fields.
   *
   * @param object $fields
   *   Fields for check.
   */
  public function requireFieldValidation(&$fields) {
    $required = array(
      'name',
      'contact_person',
      'contact_person_phone',
      'zip_code',
      'city',
      'state',
      'email',
      'phones' => array(0),
    );
    foreach ($required as $field_name => $field) {
      if (!is_array($field) && !is_object($field)) {
        if (property_exists($fields, $field)) {
          if (empty($fields->{$field})) {
            $fields->{$field} = 'error';
            $fields->error = TRUE;
          }
        }
      }
      else {
        foreach ($field as $key) {
          if (empty($fields->{$field_name}[$key])) {
            $fields->{$field_name}[$key] = 'error';
            $fields->error = TRUE;
          }
        }
      }
    }
  }

  /**
   * Logs for trip.
   * @param $title
   * @param $text
   * @param string $event_type
   */
  public function setLogs($title, $text, $event_type = 'Carrier') {
    global $user;
    $user_name = isset($user->name) ? $user->name : 'Anonymous';
    $body = '';
    if ($event_type == 'mail') {
      $body = $text[0]['text'];
    }
    $log_data[] = array(
      'details' => array([
        'event_type' => $event_type,
        'activity' => $user_name,
        'title' => ucfirst(preg_replace('/_/', ' ', $title)),
        'text' => $text,
        'date' => time(),
        'body' => $body,
      ],
      ),
      'source' => $user_name,
    );
    $log = new Log($this->carId, EntityTypes::LDCARRIER);
    $log->create($log_data);
  }

}
