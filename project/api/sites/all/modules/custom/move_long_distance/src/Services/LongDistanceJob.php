<?php

namespace Drupal\move_long_distance\Services;

use Drupal\move_calculator\Services\MoveRequestExtraService;
use Drupal\move_calculator\Services\MoveRequestGrandTotal;
use Drupal\move_long_distance\Services\Actions\SitInvoiceActions;
use Drupal\move_long_distance\Services\Actions\SitReceiptActions;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Cache;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\move_request\MoveRequestRetrieve;
use Drupal\move_services_new\Util\enum\TripPaymentType;
use Drupal\move_services_new\Util\enum\TripStatus;
use Drupal\move_services_new\Util\enum\TripType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class Invoice.
 *
 * @package Drupal\move_long_distance\Services
 */
class LongDistanceJob extends BaseService {

  private $jobId = NULL;
  private $encoders;
  private $normalizers;
  private $serializer;
  private $tripId;
  private $jobWrapper = NULL;
  private $logsText = array();
  private $entityType = EntityTypes::MOVEREQUEST;

  /**
   * LongDistanceJob constructor.
   *
   * @param null $jobId
   *   Job id.
   * @param null $tripId
   *   Trip id.
   * @param int $entity_type
   *   Entity type.
   */
  public function __construct($jobId = NULL, $tripId = NULL, $entity_type = EntityTypes::MOVEREQUEST) {
    $this->jobId = $jobId;
    $this->tripId = $tripId;
    $this->entityType = $entity_type;
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);

  }

  /**
   * Set job id and trip id.
   *
   * @param int $job_id
   *   Job id.
   * @param null $trip_id
   *   Trip id.
   */
  public function setId($job_id, $trip_id = NULL) {
    $this->jobId = $job_id;
    $this->tripId = $trip_id;
  }

  /**
   * Create.
   */
  public function create($data = array()) {}

  /**
   * Retrieve.
   */
  public function retrieve($with_closing = 0) {
    return self::getJobById($this->jobId, $with_closing);
  }

  /**
   * Update.
   */
  public function update($data = array()){}

  /**
   * Delete.
   */
  public function delete(){}

  /**
   * Index.
   */
  public function index() {}

  /**
   * Add calculated closing to trip. Write logs.
   *
   * @param mixed $closing
   *   Array with closing values.
   * @param string $type
   *   Type of job. Request|Tp delivery.
   *
   * @return array|mixed
   *   Closing.
   *
   * @throws \ServicesException
   */
  public function addingClosingToTrip($closing, $type = 'request') {
    $error_flag = FALSE;
    $error_message = "";
    try {
      $this->logsText = array();
      if (!is_array($closing)) {
        $closing = (array) $closing;
      }
      // Insert fields to closing.
      $closing_id = db_insert('ld_trip_closing')
        ->fields($closing)
        ->execute();

      $closing['id'] = $closing_id;
      // Insert job to trip.
      db_insert('ld_trip_jobs')
        ->fields(array(
          'trip_id' => $this->tripId,
          'job_id' => $this->jobId,
          'status' => $closing['status'],
          'created' => time(),
        )
        )
        ->execute();
      $this->updateClosingTotal($this->tripId);
      LongDistanceTrip::updateTripStatus($this->tripId, TripStatus::PENDING);

      $this->logsText[]['simpleText'] = 'Job price per cf = ' . $closing['rate_per_cf'];
      $this->logsText[]['simpleText'] = 'Tp collected = ' . (float) $closing['tp_collected'];
      $this->logsText[]['simpleText'] = 'Job type is ' . $type;
      $log_title = 'Job № . ' . $this->jobId . ' was added for trip';
      $trip_inst = new LongDistanceTrip($this->tripId);
      $trip_inst->setLogs($log_title, $this->logsText, 'Job added');
    }
    catch (\Throwable $e) {
      $message = "Save closing to trip Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('trip closing save', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $closing;
    }
  }

  /**
   * Save a new receipt for trip.
   *
   * @param int $job_id
   *   Id of the job.
   * @param int $entity_type
   *   Type of entity to add for receipt.
   * @param int $trip_id
   *   Id of the trip.
   * @param float $amount
   *   Amount of the receipt.
   * @param int $transaction_type
   *   Type of the transaction.
   * @param array $receipt
   *   Details for the receipt.
   *
   * @return \DatabaseStatementInterface|int
   *   Result db insert.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public static function createPaymentReceiptTrip(int $job_id, int $entity_type, int $trip_id, float $amount, $transaction_type = TripPaymentType::NEW_JOB, $receipt = array()) {
    $payment_flag = '';
    switch ($transaction_type) {
      case TripPaymentType::NEW_JOB:
        $type = 'custom';
        $carrier_id = LongDistanceTrip::getTripCarrier($trip_id);
        if (!empty($carrier_id)) {
          $carrier_inst = new LongDistanceCarrier($carrier_id);
          $carrier = $carrier_inst->retrieve();
          $payment_flag = 'T/P COLLECTED ' . $carrier['name'];
        }
        break;

      default:
        $type = 'cash';
        $payment_flag = 'regular';
        break;
    }

    // If receipt not exist we create it here.
    if (empty($receipt)) {
      $node_field = (new MoveRequest())->getNode($job_id);

      $receipt = array(
        'account_number' => '',
        'amount' => $amount,
        'auth_code' => '',
        'date' => date('d/m/Y'),
        'created' => time(),
        'description' => '',
        'invoice_id' => 'custom',
        'payment_method' => 'cash',
        'transaction_id' => 'Custom Payment',
        'pending' => FALSE,
        'type' => $type,
        'payment_flag' => $payment_flag,
        'id' => $job_id,
        'trip_id' => $trip_id,
        'jobs' => [$job_id],
        'transaction_type' => $transaction_type,
        'name' => $node_field['name'] ?? '',
        'phone' => $node_field['phone'] ?? 0,
        'email' => $node_field['email'] ?? '',
      );
    }

    $receipt_id = MoveRequest::setReceipt($job_id, $receipt, $entity_type);
    db_insert('ld_trip_receipts')
      ->fields(array(
        'receipt_id' => $receipt_id,
        'amount' => abs($receipt['amount']),
        'type' => $transaction_type,
        'created' => time(),
      ))
      ->execute();


    $result = db_insert('ld_trip_receipts_details')
      ->fields(array(
        'trip_id' => $trip_id,
        'receipt_id' => $receipt_id,
        'job_id' => $job_id,
        'amount' => abs($amount),
        'type' => $transaction_type,
        'entity_type' => $entity_type,
      ))
      ->execute();
    $log_title = 'Receipt was created for job #' . $job_id;

    $text[]['simpleText'] = 'Receipt #' . $receipt_id . ' was created';
    $text[]['simpleText'] = 'Receipt amount is ' . $amount;
    $text[]['simpleText'] = 'Receipt type is ' . $transaction_type;

    $trip_inst = new LongDistanceTrip($trip_id);
    $trip_inst->setLogs($log_title, $text, 'New Receipt');
    return $result;
  }

  /**
   * Prepare closing and add request to the trip.
   *
   * @param int $trip_id
   *   ID of trip.
   * @param array $jobs_id
   *   Array of jobs to add.
   *
   * @return array|mixed
   *   Result.
   *
   * @throws \ServicesException
   */
  public function processAddRequestToTrip(int $trip_id, array $jobs_id) {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    $this->tripId = $trip_id;
    try {
      $trip_inst = new LongDistanceTrip();
      // Trip type(Personally pr Carrier).
      $trip_type = $trip_inst::getTripType($trip_id);
      // Check if trip has carrier.
      $trip_has_carrier = $trip_inst::tripHasCarrier($trip_id);
      if (!$trip_has_carrier && $trip_type == TripType::CARRIER) {
        throw new \Exception("Trip dose not have carrier", 406);
      }
      else {
        foreach ($jobs_id as $job_id) {
          $this->jobId = $job_id;
          $this->logsText = array();
          $job_exist = self::isJobExist($job_id);
          if (!empty($job_exist)) {
            $result[$job_id]['error'] = TRUE;
            $result[$job_id]['text'] = t('Job is already exist');
          }
          else {
            $closing = $this->prepareJobEntityForSave();
            if (!empty($closing)) {
              $closing['status'] = TripStatus::PENDING;

              if ($trip_type == TripType::PERSONALLY) {
                $temp_closing = $closing;
                self::cleanJobClosing($closing);
              }

              $result[$job_id] = $this->addingClosingToTrip($closing);

              if (!empty($temp_closing)) {
                $temp_closing['id'] = $result[$job_id]['id'];
                self::insertTempClosingTable($temp_closing);
              }

              // Make first receipt only for carrier trip.
              if ($trip_type == TripType::CARRIER && !empty($closing['tp_collected'])) {
                self::createPaymentReceiptTrip($this->jobId, EntityTypes::MOVEREQUEST, $trip_id, (float) $closing['tp_collected']);
              }

              if ($trip_inst->tripHasStatus(TripStatus::ACTIVE, $trip_id)) {
                $this->changeJobSitStatus(LongDistanceSit::MOVE_OUT_STATUS);
              }

              self::updateJobInTripVal($this->jobWrapper, 1);
            }
            else {
              throw new \Exception("LD Closing for save was corrupted for job #" . $job_id, 406);
            }
          }
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Process add request to trip Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Request job', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Calculate closing total for trip.
   *
   * @param int $trip_id
   *   Id of the trip.
   * @param int $type
   *   Type of the job. Tp delivery or request.
   *
   * @return array|mixed
   *
   * @throws \ServicesException
   */
  public function updateClosingTotal(int $trip_id, $type = 0) {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    try {
      $trip_type = LongDistanceTrip::getTripType($trip_id);
      $fields_to_calculate = array(
        'volume_cf',
        'job_cost',
        'services_total',
        'job_total',
        'tp_collected',
        'received',
        'paid',
        'to_receive',
        'to_pay',
        'balance',
        'order_balance',
      );
      $query = db_select('ld_trip_closing', 'ltc');
      $query->condition('ld_trip_id', $trip_id);
      $query->isNull('is_total');
      foreach ($fields_to_calculate as $field_name) {
        $query->addExpression('SUM({' . $field_name . '})', $field_name);
      }

      if ($trip_type == TripType::PERSONALLY) {
        $query->condition('type', EntityTypes::MOVEREQUEST, '<>');

        $query_for_weight = db_select('ld_trip_closing', 'ltc');
        $query_for_weight->condition('ld_trip_id', $trip_id);
        $query_for_weight->isNull('is_total');
        foreach ($fields_to_calculate as $field_name) {
          $query_for_weight->addExpression('SUM({' . $field_name . '})', $field_name);
        }
        $query_for_weight->condition('type', EntityTypes::MOVEREQUEST);
        $request_only_weight = $query_for_weight->execute()->fetchAssoc();
        $type = EntityTypes::LDTDJOB;
      }

      $result = $query->execute()->fetchAssoc();
      if (!empty((array) $result)) {
        $result = self::closingCalculation($result, TRUE, $type);
        // Add this to total weight. We don't need to calculate this.
        if (!empty($request_only_weight['volume_cf'])) {
          $result['volume_cf'] += $request_only_weight['volume_cf'];
        }
        $result['is_total'] = 1;
        $result['job_id'] = 0;
        db_merge('ld_trip_closing')
          ->key(array('ld_trip_id' => $trip_id, 'is_total' => 1))
          ->fields($result)
          ->execute();
      }
    }
    catch (\Throwable $e) {
      $message = "Save job to trip Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('trip job', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Update job price per cf, weight, etc.
   *
   * @param int $trip_id
   *   ID of the trip.
   * @param bool $id
   *   Closing id.
   * @param array $fields_to_update
   *   Field which must be updated.
   *
   * @return array
   *   Job fields(updated), job total.
   *
   * @throws \Exception
   */
  public function updateJobPCfValueCf(int $trip_id, $id = FALSE, array $fields_to_update) {
    if ($id) {
      $this->setJobIdByClosingId($id);
    }
    $this->jobHasTransactionError();
    $closing = self::getJobsClosingByIds($this->jobId);

    $is_tp_delivery_job = self::is_tp_delivery_job($this->jobId);
    $job_type = $is_tp_delivery_job ? EntityTypes::LDTDJOB : EntityTypes::MOVEREQUEST;
    foreach ($fields_to_update as $field_name => $value) {
      $name = ucfirst(str_replace('_', ' ', $field_name));
      if ($closing[$this->jobId]->{$field_name} != $value) {
        $log_text[]['simpleText'] = 'Field "' . $name . '" was changed from "' . str_replace('.00', '', $closing[$this->jobId]->{$field_name}) . '" to "' . $value . '"';
      }
    }

    $result = $this->updateTripJobNumbers($trip_id, $id, $fields_to_update, $job_type);

    if (!empty($log_text)) {
      $trip_inst = new LongDistanceTrip($trip_id);
      $title = 'Job value was changed';
      $trip_inst->setLogs($title, $log_text, 'Job value updated');
    }
    return $result;
  }

  /**
   * Updating job number fields.
   *
   * @param int $trip_id
   *   Trip id.
   * @param bool $id
   *   Closing id.
   * @param array $fields_to_update
   *   Field to update.
   * @param bool $job_type
   *   Type of job(request job or tp delivery job).
   *
   * @return array
   *   Updated fields.
   *
   * @throws \ServicesException
   */
  public function updateTripJobNumbers(int $trip_id, $id = FALSE, array $fields_to_update, $job_type = FALSE) {
    $result = array();
    $closing = db_select('ld_trip_closing', 'ltc');
    $closing->fields('ltc', array(
      'rate_per_cf',
      'volume_cf',
      'job_cost',
      'services_total',
      'job_total',
      'tp_collected',
      'to_receive',
      'to_pay',
      'balance',
      'paid',
      'received',
      'order_balance',
    )
    );
    if ($id) {
      // Get job if by closing.
      $job_id = db_select('ld_trip_closing', 'ltc')
        ->fields('ltc', array('job_id'))
        ->condition('id', $id)
        ->execute()
        ->fetchField();

      $closing->condition('id', $id);
    }
    if (!empty($this->jobId)) {
      $closing->condition('job_id', $this->jobId);
    }
    $closing->condition('ld_trip_id', $trip_id);
    $closing_result = $closing->execute()->fetchAssoc();

    $closing_result = array_replace($closing_result, $fields_to_update);

    $result['fields'] = self::closingCalculation($closing_result, FALSE, $job_type);
    $updated = db_update('ld_trip_closing');
    $updated->fields($result['fields']);
    if ($id) {
      $updated->condition('id', $id);
    }
    if (!empty($this->jobId)) {
      $updated->condition('job_id', $this->jobId);
    }
    $updated_result = $updated->condition('ld_trip_id', $trip_id)->execute();

    if ($updated_result) {
      // Update temp closing fields.
      if (!empty($job_id)) {
        $this->jobId = $job_id;
      }
      self::updateTempClosingTable($this->jobId, $result['fields']);
      $result['total'] = $this->updateClosingTotal($trip_id, $job_type);
    }
    return $result;
  }

  /**
   * Update temp closing fields for job.
   *
   * We will restore from this fields all values
   * after trip will set to "Carrier".
   *
   * @param int $trip_id
   *   If of the trip.
   * @param array $fields
   *   Fields to update.
   */
  public function updateTempClosingTable(int $trip_id, array $fields) {
    $allowed_fields = array(
      'rate_per_cf',
      'job_cost',
      'services_total',
      'job_total',
      'tp_collected',
      'received',
      'paid',
      'to_receive',
      'to_pay',
      'balance',
    );
    // We need to get job from temp table. If it exist, we can update it.
    $query = db_select('ld_trip_temp_closing', 'temp_closing');
    $query->leftJoin('ld_trip_closing', 'closing', 'temp_closing.job_id = closing.job_id');
    $query->fields('temp_closing', array('job_id'))
      ->condition('closing.type', EntityTypes::MOVEREQUEST)
      ->condition('closing.ld_trip_id', $trip_id);
    $result = $query->execute()->fetchField();
    if (!empty($result)) {
      // Get need field from coming array.
      $allowed_fields = array_intersect_key($fields, array_flip($allowed_fields));
      if (!empty($allowed_fields)) {
        db_update('ld_trip_temp_closing')
          ->fields($allowed_fields)
          ->condition('job_id', $this->jobId)
          ->condition('ld_trip_id', $trip_id)
          ->execute();
      }
    }
  }

  /**
   * Synchronize Request with job.
   *
   * @param mixed $data
   *   Data.
   *
   * @throws \ServicesException
   */
  public function synchronizeReuqetsWithJob($data) {
    $job = $this->getJobById($this->jobId);
    if (!empty($data['invoice']['closing_weight']['value'])) {
      $field_to_update = array(
        'volume_cf' => $data['invoice']['closing_weight']['value'],
      );
    }

    if (!empty($field_to_update)) {
      $this->updateTripJobNumbers($job['trip_id'], FALSE, $field_to_update);
    }
  }

  /**
   * Update temp closing fields for job.
   *
   * We will restore from this fields all values
   * after trip will set to "Carrier".
   *
   * @param array $fields
   *   Fields to update.
   */
  public function insertTempClosingTable(array $fields) {
    $allowed_fields = array(
      'rate_per_cf',
      'job_cost',
      'services_total',
      'job_total',
      'tp_collected',
      'received',
      'paid',
      'to_receive',
      'to_pay',
      'balance',
      'ld_trip_id',
      'job_id',
      'id',
    );
    // Get need field from coming array.
    $allowed_fields = array_intersect_key($fields, array_flip($allowed_fields));
    if (!empty($allowed_fields)) {
      db_merge('ld_trip_temp_closing')
        ->key(array('job_id' => $this->jobId))
        ->fields($allowed_fields)
        ->execute();
    }
  }

  /**
   * Clean job closing.
   *
   * @param mixed $closing
   *   Closing data.
   */
  public static function cleanJobClosing(&$closing) {
    $fields = array(
      'rate_per_cf',
      'job_cost',
      'services_total',
      'job_total',
      'tp_collected',
      'received',
      'paid',
      'to_receive',
      'to_pay',
      'balance',
    );

    foreach ($fields as $field_name) {
      $closing[$field_name] = 0;
    }
  }

  /**
   * Update job status.
   *
   * Also must be updated cache of the request.
   *
   * @param int $job_id
   *   If of the job.
   * @param int $status
   *   Status id.
   *
   * @throws \Exception
   */
  public static function updateJobStatus(int $job_id, int $status) {
    db_update('ld_trip_jobs')
      ->fields(array('status' => $status))
      ->condition('job_id', $job_id)
      ->execute();

    db_update('ld_trip_closing')
      ->fields(array('status' => $status))
      ->condition('job_id', $job_id)
      ->execute();

    Cache::updateCacheData($job_id);
  }

  /**
   * Update "in_trip" field for request.
   *
   * @param mixed $jobWrapper
   *   Entity wrapper for job.
   * @param int $value
   *   VAlue for update.
   */
  public static function jobInTripFieldUpdate($jobWrapper, int $value) {
    $jobWrapper->field_added_to_trip->set($value);
    $jobWrapper->save();
    db_update('ld_request')
      ->fields(array('in_trip' => $value))
      ->condition('ld_nid', 1)
      ->execute();
  }

  /**
   * Pure closing values update. Without calculation. Only values.
   *
   * @param int $closing_id
   *   Id of the closing.
   * @param array|mixed $fields
   *   Array with fields name and values to update.
   *
   * @return \DatabaseStatementInterface
   *   Id something update - return number of updated fileds.
   */
  public static function updateClosingById(int $closing_id, $fields) {
    $updated = db_update('ld_trip_closing')
      ->fields($fields)
      ->condition('id', $closing_id)
      ->execute();
    return $updated;
  }

  /**
   * Get all services for job.
   *
   * @return array
   *   Array of job services
   */
  public function getJobServices() {
    $services = array();
    $query = db_select('ld_trip_services', 'lts')
      ->fields('lts')
      ->condition('job_id', $this->jobId)
      ->execute();
    foreach ($query as $row) {
      $services[$row->service_id] = (array) $row;
      $services_ids[$row->service_id] = $row->service_id;
    }
    if (!empty($services_ids)) {
      $query = db_select('ld_trip_extra_services', 'ltes')
        ->fields('ltes')
        ->condition('service_id', $services_ids, 'IN')
        ->execute();
      foreach ($query as $row) {
        $row->services_default_value = (int) $row->services_default_value;
        $services[$row->service_id]['extra_services'][] = (array) $row;
      }
    }
    return array_values($services);
  }

  public static function getJobById($job_id, $with_closing = 0) {
    $query = db_select('ld_trip_jobs', 'ltj');
    $query->fields('ltj');
    if (!empty($with_closing)) {
      $query->leftJoin('ld_trip_closing', 'ltc', 'ltj.job_id = ltc.job_id');
      $query->fields('ltc');
    }
    $query->condition('ltj.job_id', $job_id);
    $result = $query->execute()->fetchAssoc();
    return $result;
  }

  /**
   * Get node request entity.
   *
   * @return array
   *   Return requests node array.
   */
  public function getJobEntityAllInfo() {
    $job_entity = array();
    $node = node_load($this->jobId);
    $this->jobWrapper = entity_metadata_wrapper('node', $node);
    if (!empty($this->jobWrapper)) {
      $request_retrieve = new MoveRequestRetrieve($this->jobWrapper);
      $job_entity = $request_retrieve->nodeFields();
      $receipt_amount = 0;
      $receipt = MoveRequest::getReceipt($job_entity['nid']);
      $request_all_data = MoveRequest::getRequestAllData($this->jobId);
      $grand_total = (new MoveRequestGrandTotal())->setRequestRetrieveByWrapper($this->jobWrapper)->build()->calculate()->grandTotalClosing;
      foreach ($receipt as $receipt_item) {
        if (!empty($receipt_item) && empty($receipt_item['pending'])) {
          $receipt_amount = $receipt_amount + $receipt_item['amount'];
          $this->logsText[]['simpleText'] = 'Request payment ' . $receipt_item['amount'];
        }
      }
      $job_entity['balance'] = $grand_total - $receipt_amount;
      $this->logsText[]['simpleText'] = 'Job balance is ' . $job_entity['balance'];

      $job_entity['volume_cf'] = $request_all_data['invoice']['closing_weight']['value'];
      $extraServicesCalculator = (new MoveRequestExtraService())->setRequestRetrieve($job_entity)->build()->calculate();
      $services = !empty($extraServicesCalculator->extraServiceTotalClosing) ? $extraServicesCalculator->extraServiceTotalClosing : $extraServicesCalculator->extraServiceTotal;
      $job_entity['services'] = $services;
      $job_entity['status'] = !empty($request_all_data['sit']) ? (int) $request_all_data['sit']['status'] : 0;
    }
    return $job_entity;
  }

  /**
   * Get all jobs from closing by ids.
   *
   * @param  $ids.
   *    of job ids.
   *
   * @return mixed
   *    Object of closing fields.
   */
  public static function getJobsClosingByIds($ids) {
    if (!is_array($ids)) {
      $ids = array($ids);
    }
    $query = db_select('ld_trip_closing', 'ltc');
    $query->fields('ltc')
      ->condition('ltc.job_id', $ids, 'IN');
    $result = $query->execute()->fetchAllAssoc('job_id');
    return $result;
  }

  /**
   * Set job closing by it closing id.
   *
   * @param int $id
   *   Closing id.
   */
  public function setJobIdByClosingId(int $id) {
    $job_id = db_select('ld_trip_closing', 'ltc')
      ->fields('ltc', array('job_id'))
      ->condition('id', $id)
      ->execute()
      ->fetchField();
    if ($job_id) {
      $this->jobId = $job_id;
    }
  }

  /**
   * Get carrier by his jobs.
   *
   * @param array $job_ids
   *   Job ids.
   *
   * @return array|mixed
   */
  public static function getCarrierByJobsId($job_ids) {
    $carriers = array();
    if (!is_array($job_ids)) {
      $job_ids = array($job_ids);
    }
    $query = db_select('ld_trip_closing', 'ltc');
    $query->leftJoin('ld_trip_carrier', 'ltcar', 'ltc.ld_trip_id = ltcar.ld_trip_id');
    $query->leftJoin('ld_tp_delivery_job', 'ltj', 'ltc.job_id = ltj.job_id');
    $query->leftJoin('ld_tp_delivery_details', 'ltdd', 'ltj.ld_tp_delivery_id = ltdd.ld_tp_delivery_id');
    $query->fields('ltdd');
    $query->fields('ltcar');
    $query->condition('ltc.job_id', $job_ids, 'IN');

    $result = $query->execute();
    foreach ($result as $row) {
      $row->carrier_id = empty($row->carrier_id) ? $row->delivery_for_company : $row->carrier_id;
      if (!empty($row->carrier_id)) {
        $carriers_ids[$row->carrier_id] = $row->carrier_id;
      }

    }
    if (!empty($carriers_ids)) {
      foreach ($carriers_ids as $carrier_id) {
        $carrier = new LongDistanceCarrier($carrier_id);
        $carriers[$carrier_id]['carrier_info'] = $carrier->retrieve();
      }
    }
    if (count($carriers) < 2) {
      $carriers = reset($carriers);
    }
    return $carriers;
  }

  /**
   * Get trip id by job id.
   *
   * @return mixed
   *   Trip id or false.
   */
  public function getJobTripId() {
    return db_select('ld_trip_closing', 'ltc')
      ->fields('ltc', array('ld_trip_id'))
      ->condition('job_id', $this->jobId)
      ->execute()
      ->fetchField();
  }

  /**
   * Get all receipt for one job.
   *
   * @param array $types
   *   Types of receipt.
   * @param int $entity_type
   *   Entity type of job.
   *
   * @return array|mixed
   *   Array of job receipts
   */
  public function getJobReceipt($types = array(), int $entity_type = EntityTypes::MOVEREQUEST, bool $hide_tp_delivery = FALSE, bool $hide_tpd_agent_folio = FALSE) {
    $error_flag = FALSE;
    $error_message = "";
    $receipts = array();
    try {

      if (empty($types)) {
        $types = array(
          TripPaymentType::RECEIVED,
          TripPaymentType::PAID,
          TripPaymentType::TP_COLLECTED,
        );
      }

      $query = db_select('ld_trip_receipts_details', 'ltrd');
      $query->leftJoin('ld_trip_receipts', 'ltr', 'ltrd.receipt_id = ltr.receipt_id');
      $query->leftJoin('moveadmin_receipt', 'mar', 'ltrd.receipt_id = mar.id');
      $query->fields('ltrd');
      $query->fields('mar', array('receipt'));
      $query->fields('ltr', array('created'));
      $query->addField('ltr', 'amount', 'total');
      $query->condition('job_id', $this->jobId);
      $query->condition('ltrd.entity_type', $entity_type);
      $query->condition('ltr.type', $types, 'IN');
      $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
      $result = array_reverse($result);
      if (!empty($result)) {
        foreach ($result as $receipt) {
          $value = unserialize($receipt['receipt']);
          // This is the checks for tp_delivery type.
          // We need to hide receipts from agent folio.
          // Will be used on td delivery modal window.
          if ($hide_tpd_agent_folio && empty($value['tp_delivery_transaction'])) {
            continue;
          }
          // This is the checks for tp_delivery type.
          // If receipt was created from tp_delivery form - we no need to show it.
          if (!$hide_tp_delivery || ($hide_tp_delivery && empty($value['tp_delivery_transaction']))) {
            $receipt['value'] = $value;
            $receipts[] = $receipt;
          }


        }
      }
    }
    catch (\Throwable $e) {
      $message = "Get job receipt Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Get job receipt', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $receipts;
    }
  }

  /**
   * Get all receipt for one job.
   *
   * @param array $types
   *   Types of receipt.
   * @param int $entity_type
   *   Entity type of job.
   *
   * @return array|mixed
   *   Array of job receipts
   */
  public static function getJobsReceipt(array $jobs, $types = array(), array $entity_types, bool $hide_tp_delivery = FALSE, bool $hide_tpd_agent_folio = FALSE) {
    $error_flag = FALSE;
    $error_message = "";
    $receipts = array();
    try {

      if (empty($types)) {
        $types = array(
          TripPaymentType::RECEIVED,
          TripPaymentType::PAID,
          TripPaymentType::TP_COLLECTED,
        );
      }

      $query = db_select('ld_trip_receipts_details', 'ltrd');
      $query->leftJoin('ld_trip_receipts', 'ltr', 'ltrd.receipt_id = ltr.receipt_id');
      $query->leftJoin('moveadmin_receipt', 'mar', 'ltrd.receipt_id = mar.id');
      $query->fields('ltrd');
      $query->fields('mar', array('receipt'));
      $query->fields('ltr', array('created'));
      $query->addField('ltr', 'amount', 'total');
      $query->condition('job_id', $jobs, 'IN');
      $query->condition('ltrd.entity_type', $entity_types, 'IN');
      $query->condition('ltr.type', $types, 'IN');
      $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
      $result = array_reverse($result);
      if (!empty($result)) {
        foreach ($result as $receipt) {
          $value = unserialize($receipt['receipt']);
          // This is the checks for tp_delivery type.
          // We need to hide receipts from agent folio.
          // Will be used on td delivery modal window.
          if ($hide_tpd_agent_folio && empty($value['tp_delivery_transaction'])) {
            continue;
          }
          // This is the checks for tp_delivery type.
          // If receipt was created from tp_delivery form - we no need to show it.
          if ($hide_tp_delivery && !empty($value['tp_delivery_transaction'])) {
            continue;
          }
          $receipt['value'] = $value;
          if (empty($receipts[$receipt['receipt_id']])) {
            $receipts[$receipt['receipt_id']] = $receipt;
          }
          $receipts[$receipt['receipt_id']]['jobs'][$receipt['job_id']] = $receipt;


        }
      }
    }
    catch (\Throwable $e) {
      $message = "Get job receipt Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Get job receipt', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $receipts;
    }
  }

  public function getJobInvoice() {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    try {
      $invoice_id = db_select('ld_invoices_details', 'lid')
        ->fields('lid', array('invoice_id'))
        ->condition('job_id', $this->jobId)
        ->execute()
        ->fetchField();
      if (!empty($invoice_id)) {
        $result = (new SitInvoiceActions($invoice_id))->getInvoice($this->jobId);
      }
    }
    catch (\Throwable $e) {
      $message = "Get job invoice Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Get job invoice', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Remove job(request) from trip's.
   *
   * @param int $trip_id
   *   The node id trip's.
   *
   * @return mixed
   *   Result of operation.
   */
  public function removeJobLdTrip(int $trip_id) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;

    try {
      // Type of receipt payments to delete.
      $receipt_types = array(
        TripPaymentType::NEW_JOB,
        TripPaymentType::TP_COLLECTED,
        TripPaymentType::PAID,
        TripPaymentType::RECEIVED,
      );

      // Get all receipts by types to delete.
      $job_receipts = self::getJobReceipt($receipt_types, $this->entityType);
      if (!empty($job_receipts)) {
        foreach ($job_receipts as $receipt) {
          SitReceiptActions::helperDeleteRecipt($receipt['receipt_id']);
        }
      }

      // Remove all services.
      $this->removeAllJobServices();

      db_delete('ld_trip_jobs')
        ->condition('job_id', $this->jobId)
        ->condition('trip_id', $trip_id)
        ->execute();

      db_delete('ld_trip_closing')
        ->condition('job_id', $this->jobId)
        ->condition('ld_trip_id', $trip_id)
        ->execute();

      db_delete('ld_agent_folio_closing')
        ->condition('job_id', $this->jobId)
        ->condition('ld_trip_id', $trip_id)
        ->execute();

      $is_tp_delivery_job = self::is_tp_delivery_job($this->jobId);
      if (!$is_tp_delivery_job) {
        $jobWrapper = entity_metadata_wrapper('node', $this->jobId);
        // Update value for request.
        self::updateJobInTripVal($jobWrapper, 0);
        $this->updateClosingTotal($trip_id);
      }
      else {
        // Remove tp_delivery job and update closing total.
        LongDistanceTPDelivery::deleteTpDeliveryJob($this->jobId);
        $this->updateClosingTotal($trip_id, 1);
      }

      // Write logs.
      $log_title = 'Job #' . $this->jobId . ' was removed';
      $log_text[]['simpleText'] = 'Job #' . $this->jobId . ' was removed';
      $trip_inst = new LongDistanceTrip($trip_id);
      $trip_inst->setLogs($log_title, $log_text, 'Job');

      $result = TRUE;
    }
    catch (\Throwable $e) {
      $message = "Remove Job Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Remove Job', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Multiply remove jobs from trip closing.
   *
   * @param array $jobs_id
   *   Array of job ids.
   * @param int $trip_id
   *   Trip id.
   *
   * @throws \Exception
   *
   * @return bool
   *   TRUE if success.
   */
  public function removeJobLdTripMultiply(array $jobs_id, int $trip_id) {
    $result = FALSE;
    if (empty($jobs_id)) {
      throw new \Exception("Job ids is empty", 406);
    }
    else {
      foreach ($jobs_id as $job_id) {
        if (!empty($job_id)) {
          $is_tp_delivery = self::is_tp_delivery_job($job_id);
          if ($is_tp_delivery) {
            $this->entityType = EntityTypes::LDTDJOB;
          }
          $this->jobId = $job_id;
          $result = $this->removeJobLdTrip($trip_id);
        }
      }
    }
    return $result;
  }

  /**
   * Remove all services for job.
   */
  public function removeAllJobServices() {
    $services = $this->getJobServices();
    if (!empty($services)) {
      foreach ($services as $service) {
        db_delete('ld_trip_extra_services')
          ->condition('service_id', $service['service_id'])
          ->execute();
      }
      db_delete('ld_trip_services')
        ->condition('job_id', $this->jobId)
        ->execute();

    }
  }

  /**
   * Preparing job fields for saving. Make calculation etc.
   *
   * @return array
   *   Return array of fields to save in job and closing table.
   */
  public function prepareJobEntityForSave() {
    $job_entity = $this->getJobEntityAllInfo();
    $job_entity['rate_per_cf'] = 0;

    $this->logsText[]['simpleText'] = 'Price per cf = ' . $job_entity['rate_per_cf'];

    $job_entity['ld_trip_id'] = $this->tripId;

    $result['owner'] = $job_entity['name'];

    $result['tp_collected'] = (float) isset($job_entity['balance']) ? $job_entity['balance'] : 0;

    // 0 - value default.
    $result['services_total'] = 0;

    $result['rate_per_cf'] = $job_entity['rate_per_cf'];
    $result['volume_cf'] = (float) $job_entity['volume_cf'];
    $result['job_cost'] = (float) $result['rate_per_cf'] * $result['volume_cf'];
    $result['job_total'] = (float) $result['job_cost'] + $result['services_total'];
    $result['balance'] = (float) $result['tp_collected'] - $result['job_total'];

    if ($result['balance'] > 0) {
      $result['to_receive'] = (float) $result['balance'];
    }
    elseif ($result['balance'] < 0) {
      $result['to_pay'] = (float) abs($result['balance']);
    }
    else {
      $result['to_receive'] = 0;
      $result['to_pay'] = 0;

    }
    $result['job_id'] = $job_entity['nid'];
    $result['ld_trip_id'] = $job_entity['ld_trip_id'];
    return $result;
  }

  /**
   * If job in receipt or in invoice - call exception.
   *
   * @throws \Exception
   */
  public function jobHasTransactionError() {
    $is_job_in_receipt = FALSE;
    $is_job_in_invoice = FALSE;

    if (!empty($this->jobId)) {
      $is_job_in_invoice = self::isJobInInvoice($this->jobId);
      $is_job_in_receipt = self::isJobInReceipt($this->jobId);
    }

    if ($is_job_in_receipt) {
      throw new \Exception("Job is in receipt. Receipt id is " . implode($is_job_in_receipt), 406);
    }
    elseif ($is_job_in_invoice) {
      throw new \Exception("Job is in invoice. Invoice id is " . $is_job_in_invoice, 406);
    }
  }

  /**
   * Calculate job values for closing.
   *
   * @param array $fields
   *    Array of fields for calculate.
   *
   * @return array
   *   Array of calculated fields.
   */

  public static function closingCalculation($fields, $total = FALSE, $type = 0) {
    if (!is_array($fields)) {
      $fields = (array) $fields;
    }
    // If calculate total - no need calculate rates and jobs costs again.
    if (!$total) {
      $fields['job_cost'] = (float) $fields['rate_per_cf'] * $fields['volume_cf'];
      $fields['job_total'] = (float) $fields['job_cost'] + $fields['services_total'];
    }
    if (empty($type)) {
      $fields['balance'] = (float) $fields['tp_collected'] - $fields['job_total'] - $fields['received'] + $fields['paid'];
    }
    // For tp delivery calculation.
    else {
      $fields['balance'] = (float) $fields['job_total'] - $fields['tp_collected'] - $fields['received'] + $fields['paid'];
    }

    if ($fields['balance'] > 0) {
      $fields['to_pay'] = 0;
      $fields['to_receive'] = (float) abs($fields['balance']);
    }
    elseif ($fields['balance'] < 0) {
      $fields['to_pay'] = (float) abs($fields['balance']);
      $fields['to_receive'] = 0;
    }
    else {
      $fields['to_receive'] = 0;
      $fields['to_pay'] = 0;

    }

    // TODO this is the quick fix. Must find other way.
    // We need to make all values to zero.
    foreach ($fields as $field_name => $val) {
      if (is_null($val)) {
        $fields[$field_name] = 0;
      }
      else {
        $fields[$field_name] = round($val, 2);
      }
    }
    return $fields;
  }

  /**
   * Check if job was added to invoice.
   *
   * @param int $job_id
   *   Job id.
   *
   * @return mixed
   *   Invoice id or FALSE.
   */
  public static function isJobInInvoice(int $job_id) {
    $result = db_select('ld_invoices_details', 'lid')
      ->fields('lid', array('invoice_id'))
      ->condition('job_id', $job_id)
      ->execute()
      ->fetchField();
    return $result;
  }

  /**
   * Check if job in receipt.
   *
   * @param int $job_id
   *   Id of job.
   * @param int $job_type
   *   Type of the Job.
   *
   * @return mixed
   *   Receipt id or FALSE.
   */
  public static function isJobInReceipt(int $job_id) {
    $receipt_types = array(TripPaymentType::NEW_JOB, TripPaymentType::TP_COLLECTED);
    $query = db_select('ld_trip_receipts_details', 'ltr');
    $query->leftJoin('moveadmin_receipt', 'mr', 'ltr.receipt_id = mr.id');
    $query->fields('ltr', array('receipt_id'))
      ->fields('mr', array('receipt'))
      ->condition('job_id', $job_id)
      ->condition('type', array($receipt_types), 'NOT IN');
    $query_result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    $result = array();
    if (!empty($query_result)) {
      foreach ($query_result as $row) {
        $receipt = unserialize($row['receipt']);
        // This is the check for tp delivery job receipt.
        // This mean that receipt was created not from agent folio.
        if (empty($receipt['tp_delivery_transaction'])) {
          $result[] = $row['receipt_id'];
          break;
        }
      }
    }
    return $result;
  }

  /**
   * Check if job exist in closing.
   *
   * @param $job_id
   *
   * @return mixed
   */
  public static function isJobExist($job_id) {
    $result = db_select('ld_trip_closing', 'ltc')
      ->fields('ltc', array('job_id'))
      ->condition('job_id', $job_id)
      ->execute()
      ->fetchField();
    return $result;
  }

  /**
   * Check is job in TP Delivery.
   *
   * @param int $job_id
   *   Id of the job.
   *
   * @return mixed
   */
  public static function is_tp_delivery_job(int $job_id) {
    $result = db_select('ld_tp_delivery_job', 'delivery_job')
      ->fields('delivery_job', array('delivery_job_id'))
      ->condition('job_id', $job_id)
      ->execute()
      ->fetchField();
    return $result;
  }

  public static function getReceiptInfoByJob($job_id) {
    $result = db_select('ld_trip_receipts_details', 'ltr')
      ->fields('ltr', array('receipt_id', 'type'))
      ->condition('job_id', $job_id)
      ->condition("ltr.type", array(TripPaymentType::RECEIVED, TripPaymentType::PAID), 'IN')
      ->execute()
      ->fetchAssoc();
    return $result;
  }

  public static function getJobInInvoiceId(int $job_id) {
    $result = db_select('ld_invoices_details', 'lid')
      ->fields('lid', array('invoice_id'))
      ->condition('job_id', $job_id)
      ->execute()
      ->fetchField();
    return $result;
  }

  public static function updateJobInTripVal($wrapper, int $val) {
    $nid = $wrapper->nid->value();
    $wrapper->field_added_to_trip->set($val);
    $wrapper->save();
    db_update('ld_request')
      ->fields(array('in_trip' => $val))
      ->condition('ld_nid', $nid)
      ->execute();
  }

  /**
   * Change sit move in/move out status.
   *
   * Update ld sit table and request all data.
   *
   * @param int $status
   *   Status.
   *
   * @return array|bool|mixed
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function changeJobSitStatus(int $status) {
    $result = TRUE;
    if (!self::is_tp_delivery_job($this->jobId)) {
      $request_all_data = MoveRequest::getRequestAllData($this->jobId);
      if (!empty($request_all_data['sit']['status'])) {
        $request_all_data['sit']['status'] = $status;
        MoveRequest::setRequestAllData($this->jobId, $request_all_data);
        Cache::updateCacheData($this->jobId);
      }

      $ld_sit = new LongDistanceSit($this->jobId);
      $job_sit_data = $ld_sit->retrieve();
      $job_sit_data['status'] = $status;

      $result = $ld_sit->update($job_sit_data);
    }

    return $result;
  }

}
