<?php

namespace Drupal\move_long_distance\Services\Actions;

use Drupal\move_long_distance\Util\enum\RefundType;
use Drupal\move_services_new\Services\Cache;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_long_distance\Services\LongDistanceJob;
use Drupal\move_services_new\Services\Payment;
use Drupal\move_services_new\Services\Receipt;
use Drupal\move_services_new\Util\enum\TripPaymentType;
use Drupal\move_long_distance\Util\enum\JobPaidType;
use Drupal\move_services_new\Util\enum\EntityTypes;

class SitReceiptActions extends SitActions {
  public $receipt = [];
  private $receiptId = NULL;
  private $jobPaidType = JobPaidType::NONE;
  private $invoice = [];
  private $invoiceId = 0;
  private $responseInfo = [];
  public $logsFields = [
    'amount',
    'jobs',
    'payment_method',
    'pending',
    'date',
    'invoice_id',
    'payment_flag',
  ];

  /**
   * SitReceiptActions constructor.
   *
   * @param int $entity_id
   *   Receipt entity_id.
   * @param int $entity_type
   *   Receipt entity_type.
   * @param array $receipt
   *   Array with receipt data.
   */
  public function __construct(int $entity_id = 0, int $entity_type = 0, array $receipt = []) {
    $this->entityId = $entity_id;
    $this->entityType = $entity_type;
    $this->receipt = $receipt;
    $this->invoiceId = $receipt['invoice_id'] ?? 0;
    $this->receiptId = $this->receipt['receipt_id'] ?? $this->receipt['id'] ?? 0;
    $this->receipt['jobs'] = !empty($this->receipt['jobs']) ? $this->receipt['jobs'] : [$this->entityId];
  }

  public function build(){
    $this->invoiceId = $receipt['invoice_id'] ?? 0;
    $this->receiptId = $this->receipt['receipt_id'] ?? $this->receipt['id'] ?? 0;
    $this->receipt['jobs'] = !empty($this->receipt['jobs']) ? $this->receipt['jobs'] : [$this->entityId];
    $this->setEntityFromReceipt();
    return $this;
  }

  /**
   * Create receipt.
   * Will create move request or authorize receipt.
   * Recalculate job closing values in this receipt.
   *
   * @return $this
   *   Scope.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function addReceipt() {
    $this->receiptId = $this->isAuthorizeReceipt() ? $this->addAuthorizeReceipt() : $this->addMoveRequestReceipt();
    $this->getReceiptById();
    $this->saveLdReceipt();

    return $this;
  }

  /**
   * Crete authorize receipt.
   *
   * @return int
   * @throws \ServicesException
   */
  private function addAuthorizeReceipt() : int {
    $field_to_get_from_receipt = ['jobs', 'transaction_type', 'entity_id', 'entity_type'];
    $receipt = (new Payment($this->entityId, $this->entityType))->create($this->receipt, array_intersect_key($this->receipt, array_flip($field_to_get_from_receipt)));
    $this->setResponseInfo($receipt);

    return $receipt['status'] ? $receipt['receipt_id'] : services_error('Error authorize', 200, $this->getResponseInfo());
  }

  /**
   * Create move request receipt.
   *
   * @return int|null
   *   Receipt id or false.
   *
   * @throws \Exception
   */
  private function addMoveRequestReceipt() : ?int {
    $receipt_id = MoveRequest::setReceipt($this->entityId, $this->receipt, $this->entityType);
    $this->setResponseInfo(['receipt_id' => $receipt_id, 'status' => TRUE]);

    return $receipt_id;
  }

  /**
   * Check if receipt is authorize.
   *
   * @return bool
   *   TRUE of FALSE.
   */
  private function isAuthorizeReceipt() : bool {
    return (isset($this->receipt['type']) && $this->receipt['type'] == 'Authorized Credit Card') ? TRUE : FALSE;
  }

  /**
   * Update receipt value.
   *
   * @return bool
   *   TRUE if everything is good.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function updateReceipt() {
    $op = 'update';
    $old_receipt = Receipt::getReceiptById($this->receiptId);

    $this->removeCreditCardInfo();
    MoveRequest::updateReceipt($this->receiptId, $this->receipt);
    $this->getReceiptById();
    $this->setEntityFromReceipt();

    if ($this->isAgentFolioReceipt()) {
      $receipts_total = static::calculateTotalForJobsReceipts($this->receipt['jobs'], [TripPaymentType::RECEIVED, TripPaymentType::PAID]);
      $this->getInvoiceForReceipt();
      $this->recalculateAgentFolioReceiptJobsValues($receipts_total);
      if ($this->invoice) {
        $sit_invoice_action = new SitInvoiceActions($this->invoiceId, $this->receipt);
        $sit_invoice_action->linkInvoiceWithReceipt();
        $sit_invoice_action->rebuildInvoiceInfo();
      }
    }
    else {
      $this->recalculateNotAgentFolioReceiptsJobValue();
    }

    $this->writeLogs($op, $this->receipt, $old_receipt);

    return TRUE;
  }

  /**
   * Set receipt to pending, or remove pending.
   *
   * @param int $receipt_id
   *   Receipt id.
   * @param bool $value
   *   TRUE of FALSE.
   *
   * @return bool
   *   TRUE if everything is ok.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function pendingReceipt(int $receipt_id, bool $value) : bool {
    $this->setReceiptId($receipt_id);
    $this->getReceiptById();
    $this->receipt['pending'] = $value;
    return $this->build()->updateReceipt();
  }

  /**
   * Main method to create different receipts.
   *
   * @return array
   *   Array with response info.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function processAddReceipt() {
    $op = 'create';

    $this->addReceipt();

    if ($this->isAgentFolioReceipt()) {
      static::copyJobsToClosing($this->receipt['jobs']);
      static::addJobsToReceiptDetails($this->receipt['jobs'], $this->receipt);

      $receipts_total = static::calculateTotalForJobsReceipts($this->receipt['jobs'], [TripPaymentType::RECEIVED, TripPaymentType::PAID]);
      $this->recalculateAgentFolioReceiptJobsValues($receipts_total);
      if ($this->invoiceId) {
        $sit_invoice = new SitInvoiceActions($this->invoiceId, $this->receipt);
        $sit_invoice->linkInvoiceWithReceipt();
        $sit_invoice->rebuildInvoiceInfo();
      }
    }
    elseif ($this->isTpDeliveryReceipt()) {
      $this->recalculateTpDeliveryReceiptsJobValue();
    }
    else {
      $this->recalculateNotAgentFolioReceiptsJobValue();
    }

    $this->writeLogs($op, $this->receipt);

    return $this->getResponseInfo();
  }

  /**
   * Recalculate jobs values from for agent folio receipts.
   *
   * @param float $receipts_total
   *   Total of all receipts.
   * @param string $op
   *   Type of operation.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  private function recalculateAgentFolioReceiptJobsValues(float $receipts_total, $op = 'insert') {
    $jobs = static::getJobsFormAgentFolioClosing($this->receipt['jobs']);
    if ($jobs) {
      $ld_job = new LongDistanceJob();

      $total_to_receive = $this->calculateJobsTotalToReceive($jobs);
      $total_to_pay = $this->calculateJobsTotalToPay($jobs);

      $total_paid = $total_to_receive + $receipts_total;
      $total_received = $total_to_pay + $receipts_total;

      foreach ($jobs as $job) {
        $fields_to_update = array();
        $this->setPaidTypeFromJob($job);
        switch ($this->jobPaidType) {
          case JobPaidType::PAY:
            $job_amount = ($this->invoiceId) ? $this->calculateAmountForPaidJobInvoiceReceipt($job, $receipts_total) :
              $this->calculateAmountForPaidJobNotFromInvoiceReceipt($job, $total_paid, $receipts_total);
            break;

          case JobPaidType::RECEIVE:
            $job_amount = ($this->invoiceId) ? $this->calculateAmountForReceivedJobInvoiceReceipt($job, $total_received, $receipts_total) :
              $this->calculateAmountForReceivedJobNotFromInvoiceReceipt($job, $receipts_total);
            break;

          case JobPaidType::NONE:
          default:
            $job_amount = 0;
        }

        if ($op != 'delete') {
          $this->insertTripReceiptDetails($job, $job_amount);
        }

        if ($this->jobPaidType) {
          $paid_field = $this->getPaidFieldByPaidType();
          $job_amount = abs($job_amount);
          $fields_to_update[$paid_field] = $job_amount + $job->{$paid_field};
          // Update each job balance and paid balance.
          $ld_job->setId($job->job_id);
          $result[] = $ld_job->updateTripJobNumbers($this->getTripIdFromJob($job), FALSE, $fields_to_update, $job->type);
        }
      }
    }
  }

  /**
   * Calculate amount for receipts with paid type.
   *
   * @param $job
   *   Array with job info.
   * @param $receipts_total
   *   Receipts total.
   *
   * @return float
   *   Total amount.
   *
   */
  private function calculateAmountForPaidJobInvoiceReceipt($job, float $receipts_total) {
    $job_amount = $job->to_pay;
    if ($receipts_total <= 0) {
      $job_amount = 0;
    }
    return $job_amount;
  }

  /**
   * Calculate amount for receipts with received type.
   *
   * @param \stdClass $job
   *   Array with job info.
   * @param float $total_paid
   *   Array with job info.
   * @param float $receipts_total
   *   Receipts total.
   *
   * @return float
   *   Total amount.
   *
   */
  private function calculateAmountForReceivedJobInvoiceReceipt(\stdClass $job, float &$total_paid, float $receipts_total) : float {
    if ($total_paid > 0) {
      $job_amount = $total_paid;
      $total_paid = $total_paid - $job->to_receive;
      if ($total_paid > 0){
        $job_amount = $job->to_receive;
      }
    }else{
      $total_paid = 0;
      $job_amount = 0;
    }

    if ($receipts_total <= 0) {
      $job_amount = 0;
    }
    return $job_amount;
  }

  /**
   * Calculate amount for paid from not invoice with received type.
   *
   * @param \stdClass $job
   *   Array with job info.
   * @param float $total_received
   *   Receipts total.
   * @param float $receipts_total
   *   Receipts total.
   *
   * @return float
   *   Total amount.
   *
   */
  private function calculateAmountForPaidJobNotFromInvoiceReceipt(\stdClass $job, float &$total_received, float $receipts_total) : float {
    if ($total_received > 0) {
      $job_amount = $total_received;
      $total_paid = $total_received - $job->to_pay;
      if ($total_paid > 0){
        $job_amount = $job->to_pay;
      }
    }else{
      $total_received = 0;
      $job_amount = 0;
    }

    if ($receipts_total <= 0) {
      $job_amount = 0;
    }
    return $job_amount;
  }

  /**
   * Calculate amount for receipts from not invoice with received type.
   *
   * @param $job
   *   Array with job info.
   * @param $receipts_total
   *   Receipts total.
   *
   * @return float
   *   Total amount.
   *
   */
  private function calculateAmountForReceivedJobNotFromInvoiceReceipt($job, float $receipts_total) : float {
    $job_amount = $job->to_receive;
    if ($receipts_total <= 0) {
      $job_amount = 0;
    }
    return $job_amount;
  }

  /**
   * Recalculate job values not from agent folio receipts.
   *
   * @param string $op
   *   Type of operation.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  private function recalculateNotAgentFolioReceiptsJobValue($op = 'insert') {
    $ld_job = new LongDistanceJob();
    $closing_jobs = $ld_job::getJobsClosingByIds($this->receipt['jobs']);

    if ($op != 'delete') {
      $this->insertTripReceiptDetailsMultiplyJobs($closing_jobs, $this->receipt['amount']);
    }

    $receipts_total = static::calculateTotalForJobsReceipts($this->receipt['jobs'], [TripPaymentType::TP_COLLECTED, TripPaymentType::NEW_JOB]);

    if (!empty($closing_jobs)) {
      foreach ($closing_jobs as $job) {
        $paid_type = 'tp_collected';
        $fields_to_update[$paid_type] = $receipts_total;
        $ld_job->setId($job->job_id);
        // Update each job balance and paid balance.
        $result[] = $ld_job->updateTripJobNumbers($this->getTripIdFromJob($job), FALSE, $fields_to_update, $job->type);
      }
    }
  }

  /**
   * Recalculate tp delivery jobs value from receipt.
   *
   * @param string $op
   *   Type of operation.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  private function recalculateTpDeliveryReceiptsJobValue($op = 'insert') {
    $ld_job = new LongDistanceJob();
    $closing_jobs = $ld_job::getJobsClosingByIds($this->receipt['jobs']);

    if ($op != 'delete') {
      $this->insertTripReceiptDetailsMultiplyJobs($closing_jobs, $this->receipt['amount']);
    }

    $receipts_total = static::calculateTotalForJobsReceipts($this->receipt['jobs'], [TripPaymentType::RECEIVED], FALSE, TRUE);

    if (!empty($closing_jobs)) {
      foreach ($closing_jobs as $job) {
        $paid_type = 'received';
        $fields_to_update[$paid_type] = $receipts_total;
        $ld_job->setId($job->job_id);
        // Update each job balance and paid balance.
        $result[] = $ld_job->updateTripJobNumbers($this->getTripIdFromJob($job), FALSE, $fields_to_update, $job->type);
      }
    }
  }

  /**
   * Save long distance receipt links.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  private function saveLdReceipt() : void {
    db_merge('ld_trip_receipts')
      ->key(array('receipt_id' => $this->receipt['id']))
      ->fields(array(
        'receipt_id' => $this->receipt['id'],
        'amount' => $this->receipt['amount'],
        'type' => $this->receipt['transaction_type'],
        'created' => time(),
      ))
      ->execute();
  }

  /**
   * Calculate all total for receipt with type paid.
   *
   * @param $jobs
   * @return float
   */
  private function calculateJobsTotalToPay($jobs) : float {
    $total = 0;
    foreach ($jobs as $item) {
      $job_amount = !empty($item->balance) ? ($item->balance) : $item->amount;
      if ($item->to_pay > 0) {
        $total += abs($job_amount);
      }
    }

    return $total;
  }

  /**
   * Calculate all total for receipts with type received.
   *
   * @param $jobs
   * @return float
   */
  private function calculateJobsTotalToReceive($jobs) : float {
    $total = 0;
    foreach ($jobs as $item) {
      $job_amount = !empty($item->balance) ? ($item->balance) : $item->amount;
      if ($item->to_receive > 0) {
        $total += abs($job_amount);
      }
    }

    return $total;
  }

  /**
   * Insert link between receipt and his jobs.
   *
   * @param \stdClass $job
   *   Job with info.
   * @param float $job_amount
   *   Amount of job.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  private function insertTripReceiptDetails(\stdClass $job, float $job_amount) {
    $trip_id = $this->getTripIdFromJob($job);
    $job_entity_type = $this->getEntityTypeFromJob($job);

    // Insert receipt details.
    db_merge('ld_trip_receipts_details')
      ->key(array(
          'trip_id' => $trip_id,
          'receipt_id' => $this->receiptId,
          'job_id' => $job->job_id,
        )
      )
      ->fields(array(
        'trip_id' => $trip_id,
        'receipt_id' => $this->receiptId,
        'job_id' => $job->job_id,
        'amount' => (float) $job_amount,
        'type' => $this->receipt['transaction_type'],
        'entity_type' => $job_entity_type,
      ))
      ->execute();
  }

  /**
   * Inert links for many job and receipt.
   *
   * @param $jobs
   *   Array with jobs
   * @param float $job_amount
   *   Array receipt amount for job.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  private function insertTripReceiptDetailsMultiplyJobs(array $jobs, float $job_amount) : void {
    foreach ($jobs as $job) {
      $this->insertTripReceiptDetails($job, $job_amount);
    }
  }

  /**
   * Retrieve trip id form job stdClass.
   *
   * @param \stdClass $job
   *   Job with params.
   *
   * @return int
   *   Trip id.
   */
  private function getTripIdFromJob(\stdClass $job) : int {
    return isset($job->ld_trip_id) ? $job->ld_trip_id : $job->ld_trip_id->trip_id;
  }

  /**
   * Retrieve entity type form job stdClass.
   *
   * @param \stdClass $job
   *   Job with params.
   *
   * @return int
   *   Entity type.
   */
  private function getEntityTypeFromJob(\stdClass $job) : int {
    return !empty($job->type) ? EntityTypes::LDTDJOB : EntityTypes::MOVEREQUEST;
  }

  /**
   * Load receipt by his id.
   *
   * @return $this
   *   Scope.
   */
  public function getReceiptById() {
    $this->receipt = Receipt::getReceiptById($this->receiptId);

    return $this;
  }

  /**
   * Get LD invoice by his id.
   *
   * @param int $receipt_id
   *   Receipt id.
   * @param bool $job_id
   *   If not FALSE - in items will be only this job.
   *
   * @return mixed
   *   Receipt info with items.
   */
  public static function getReceipt(int $receipt_id, $job_id = FALSE) {
    $result = (array) Receipt::getReceiptById($receipt_id, TRUE);
    $result['value'] = unserialize($result['receipt']);
    $query = db_select('ld_trip_receipts_details', 'ltr');
    $query->fields('ltr');
    $query->condition('receipt_id', $receipt_id);
    if ($job_id) {
      $query->condition('job_id', $job_id);
    }
    $result['items'] = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Get invoice for receipt.
   *
   * @return array|mixed
   */
  private function getInvoiceForReceipt() {
    $invoice_id = db_select('ld_invoices_receipts', 'lir')
      ->fields('lir', array('invoice_id'))
      ->condition('receipt_id', $this->receiptId)
      ->execute()
      ->fetchField();
    if ($invoice_id) {
      $this->invoiceId = $invoice_id;
      return $this->invoice = (new SitInvoiceActions($invoice_id))->getInvoice();
    }
  }

  /**
   * Set receipt id.
   *
   * @param int $receipt_id
   *  Receipt id.
   */
  public function setReceiptId(int $receipt_id) : void {
    $this->receiptId = $receipt_id;
  }

  /**
   * Set paid type of job.
   *
   * @param \stdClass $job
   *   job params
   */
  private function setPaidTypeFromJob(\stdClass $job) : void {
    $paid_type = JobPaidType::NONE;

    if ($job->to_receive > 0) {
      $paid_type = JobPaidType::RECEIVE;
    }

    if ($job->to_pay > 0) {
      $paid_type = JobPaidType::PAY;
    }

    if ($job->to_receive == 0 && $job->to_pay == 0) {
      $paid_type = JobPaidType::NONE;
    }

    $this->jobPaidType = $paid_type;
  }

  /**
   * Get string version of paid type.
   *
   * @return string
   *   Paid type.
   */
  private function getPaidFieldByPaidType() : string {
    $fields[JobPaidType::PAY] = 'paid';
    $fields[JobPaidType::RECEIVE] = 'received';

    return $fields[$this->jobPaidType];
  }

  /**
   * Delete receipt and recalculate linked jobs.
   *
   * Also rebuild invoice info.
   *
   * @param bool $only_ld_receipt
   *  Only ld receipt or not.
   *
   * @return bool
   *   TRUE if everything is good.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function deleteReceipt(bool $only_ld_receipt = FALSE) {
    $op = 'delete';
    $this->getReceiptById();
    $this->setEntityFromReceipt();
    $this->deleteReceiptFromDb($only_ld_receipt);

    if (!$this->isTpDeliveryReceipt()) {
      $this->getInvoiceForReceipt();

      $receipts_total = static::calculateTotalForJobsReceipts($this->receipt['jobs'], [TripPaymentType::RECEIVED, TripPaymentType::PAID]);

      // TODO need to think how to remove "if else".
      if ($receipts_total) {
        $this->recalculateAgentFolioReceiptJobsValues($receipts_total, $op);
      }
      else {
        $this->recalculateNotAgentFolioReceiptsJobValue($op);
        $this->restoreClosingFromAgentFolioClosingByJobsIds();
        $this->deleteJobsFromAgentFolio();
      }

      if ($this->invoice) {
        (new SitInvoiceActions($this->invoice['id'], $this->receipt))->rebuildInvoiceInfo();
      }
    }
    else {
      $this->recalculateTpDeliveryReceiptsJobValue($op);
    }

    $this->writeLogs($op, $this->receipt);
    Cache::updateCacheData($this->receipt['entity_id']);

    return TRUE;
  }

  /**
   * Check if receipt is from agent folio.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  private function isAgentFolioReceipt() : bool {
    $result = FALSE;

    if (($this->receipt['transaction_type'] == TripPaymentType::PAID || $this->receipt['transaction_type'] == TripPaymentType::RECEIVED)
      && !$this->isTpDeliveryReceipt()) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Check if receipt is tp delivery.
   *
   * @return bool
   *   TRUE of FALSE.
   */
  private function isTpDeliveryReceipt() : bool {
    return (empty($this->receipt['tp_delivery_transaction'])) ? FALSE : TRUE;
  }

  /**
   * Delete receipt from db.
   *
   * @param bool $only_ld_receipt
   */
  public function deleteReceiptFromDb(bool $only_ld_receipt = FALSE) : void {
    if ($only_ld_receipt) {
      static::helperDeleteLdReceipt($this->receiptId);
    }
    else {
      static::helperDeleteRecipt($this->receiptId);
    }
  }

  /**
   * Restore closing job from agent folio closing jobs.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function restoreClosingFromAgentFolioClosing() {
    $agent_folio_jobs = db_select('ld_agent_folio_closing', 'agent_folio')
      ->fields('agent_folio')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
    if ($agent_folio_jobs) {
      foreach ($agent_folio_jobs as $job) {
        db_merge('ld_trip_closing')
          ->key(['job_id' => $job['job_id']])
          ->fields($job)
          ->execute();
      }
    }
  }

  /**
   * Restore closing job from agent folio closing jobs by they ids.
   */
  private function restoreClosingFromAgentFolioClosingByJobsIds() {
    $ld_job = new LongDistanceJob();
    $jobs = static::getJobsFormAgentFolioClosing($this->receipt['jobs']);

    if ($jobs) {
      // Update jobs numbers.
      foreach ($jobs as $item) {
        $ld_job->setId($item->job_id);
        $fields_to_update = array(
          'rate_per_cf' => $item->rate_per_cf,
          'volume_cf' => $item->volume_cf,
          'job_cost' => $item->job_cost,
          'services_total' => $item->services_total,
          'job_total' => $item->job_total,
          'tp_collected' => $item->tp_collected,
          'received' => $item->received,
          'paid' => $item->paid,
          'to_receive' => $item->to_receive,
          'to_pay' => $item->to_pay,
          'balance' => $item->balance,
        );
        // Update each job balance and paid balance.
        $ld_job->updateTripJobNumbers($item->ld_trip_id, FALSE, (array) $fields_to_update, $item->type);
      }
    }
  }

  /**
   * Delete jobs from agent folio closing.
   */
  private function deleteJobsFromAgentFolio() {
    if ($this->receipt['jobs']) {
      foreach ($this->receipt['jobs'] as $job) {
        // Remove agent folio closing job.
        db_delete('ld_agent_folio_closing')
          ->condition('job_id', $job)
          ->execute();
      }
    }
  }

  /**
   * @param array $jobs
   * @param array $receipt
   *
   * @throws \Exception
   *
   * @throws \InvalidMergeQueryException
   */
  public static function addJobsToReceiptDetails(array $jobs, array $receipt) {
    $closing_jobs = LongDistanceJob::getJobsClosingByIds($jobs);
    // Insert main information for trip.
    db_merge('ld_trip_receipts')
      ->key(array('receipt_id' => $receipt['id']))
      ->fields(array(
        'receipt_id' => $receipt['id'],
        'amount' => $receipt['amount'],
        'type' => $receipt['transaction_type'],
        'created' => time(),
      ))
      ->execute();

    if (!empty($closing_jobs)) {
      foreach ($closing_jobs as $job) {
        // Insert receipt details.
        db_merge('ld_trip_receipts_details')
          ->key(array(
              'trip_id' => $job->ld_trip_id,
              'receipt_id' => $receipt['id'],
              'job_id' => $job->job_id,
            )
          )
          ->fields(array(
            'trip_id' => $job->ld_trip_id,
            'receipt_id' => $receipt['id'],
            'job_id' => $job->job_id,
            'amount' => (float) $job->balance,
            'type' => $receipt['transaction_type'],
            'entity_type' => $job->type,
          ))
          ->execute();
      }
    }

  }

  /**
   * @param array $jobs
   * @throws \Exception
   */
  public static function copyJobsToClosing(array $jobs) {
    $agent_folio_closing_jobs = self::getJobsFormAgentFolioClosing($jobs);
    $closing_jobs = LongDistanceJob::getJobsClosingByIds($jobs);
    $diff_jobs = array_diff_key($closing_jobs, $agent_folio_closing_jobs);
    if (!empty($diff_jobs)) {
      foreach ($diff_jobs as $job) {
        db_insert('ld_agent_folio_closing')
          ->fields((array) $job)
          ->execute();
      }
    }
  }

  /**
   * @param $jobs
   * @return mixed
   */
  public static function getJobsFormAgentFolioClosing($jobs) {
    $query = db_select('ld_agent_folio_closing', 'afc');
    $query->fields('afc')
      ->condition('afc.job_id', $jobs, 'IN');
    $result = $query->execute()->fetchAllAssoc('job_id');
    return $result;
  }

  /**
   * Calculate total for receipt by his jobs.
   *
   * @param array $jobs
   *   Jobs array
   * @param array $receipt_types
   *   Receipts type.
   * @param bool $hide_tp_delivery
   *   Is hide tp delivery receipt?
   * @param bool $hide_tpd_agent_folio
   *   Is hide tp delivery agent folio receipt.
   *
   * @return float
   *   Receipt total.
   */
  public static function calculateTotalForJobsReceipts(array $jobs, array $receipt_types = [], bool $hide_tp_delivery = TRUE, bool $hide_tpd_agent_folio = FALSE) : float {
    $total = 0.0;
    $receipts = LongDistanceJob::getJobsReceipt($jobs, $receipt_types, array(EntityTypes::MOVEREQUEST, EntityTypes::LDTDJOB), $hide_tp_delivery, $hide_tpd_agent_folio);
    if (!empty($receipts)) {
      foreach ($receipts as $receipt) {

        // Skip pending status.
        if (!empty($receipt['value']['pending'])) {
          continue;
        }

        // Skip refund status. If type is CUSTOM we need to remove it from total.
        if (!empty($receipt['value']['refunds'])) {
          if ($receipt['value']['refund_type'] == RefundType::CUSTOM) {
            $receipt['value']['amount'] *= -1;
          }
          else {
            continue;
          }
        }

        $total += $receipt['value']['amount'];

      }
    }
    return $total;
  }

  /**
   * Refund receipt.
   *
   * @return array
   *   Array with response.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function refundReceipt() : array {
    if (!empty($this->receipt['credit_card']))  {
      $payment_inst = new Payment($this->entityId, $this->entityType);
      $data_to_merge = array(
        'refunds' => TRUE,
        'refund_notes' => $this->receipt['refund_notes'],
        'refund_date' => time(),
        'pending' => FALSE,
        'refund_type' => $this->receipt['refund_type'],
        'invoice_id' => $this->receipt['invoice_id'],
        'skip_new_receipt' => TRUE,
      );
      $result = $payment_inst->refunds($this->receipt, $data_to_merge);
      $this->setResponseInfo($result);
      if ($result['status']) {
        (new SitReceiptActions($this->entityId, $this->entityType, $this->receipt))->updateReceipt();
      }
    }

    return $this->getResponseInfo();
  }

  /**
   * Get response info.
   *
   * @return array
   *   Array with response info.
   */
  private function getResponseInfo() : array {
    return $this->responseInfo;
  }

  /**
   * Set response info.
   *
   * @param array $response
   *   Array with response info
   */
  private function setResponseInfo(array $response) {
    $this->responseInfo = $response;
  }

  /**
   * Set entity type and id from receipt.
   */
  private function setEntityFromReceipt() : void {
    $this->entityId = $this->receipt['entity_id'] ?? $this->entityId;
    $this->entityType = $this->receipt['entity_type'] ?? $this->entityType;
  }

  /**
   * Unset credit card indo from receipt.
   */
  private function removeCreditCardInfo() : void {
    if (isset($this->receipt['credit_card'])) {
      unset($this->receipt['credit_card']);
    }
  }

  /**
   * Write logs.
   *
   * @param string $op
   *   Operation type.
   *
   * @param array $new_data
   *   Current data for logs.
   * @param array $old_data
   *   Old data for logs.
   *
   * @return array
   */
  public function writeLogs(string $op, array $new_data, array $old_data = []) : array {
    $new_data = array_intersect_key($new_data, array_flip($this->logsFields));
    $old_data = array_intersect_key($old_data, array_flip($this->logsFields));
    $sit_logs = new SitLogActions($this->entityId, $this->entityType, 'Payment', $op, $this->getLogTitle($op), $new_data, $old_data);

    if ($this->isAgentFolioReceipt()) {
      $result = $sit_logs->processLog();
    }
    elseif ($this->isTpDeliveryReceipt()) {
      $entity = $this->getEntityForLog();
      $sit_logs->setEntity($entity['entity_id'], $entity['entity_type']);
      $result = $sit_logs->processLog();
    }
    else {
      // For tp collected, we need to send two logs. One for trip, second for request.
      $sit_logs->processLog();

      $entity = $this->getEntityForLog();
      $sit_logs->setEntity($entity['entity_id'], $entity['entity_type']);
      $result = $sit_logs->processLog();
    }

    return $result;
  }

  /**
   * Get entity for logs.
   *
   * @return array
   *   Array with entity_id and entity_type.
   */
  private function getEntityForLog() : array {
    if ($this->isAgentFolioReceipt()) {
      $result = [
        'entity_id' => $this->entityId,
        'entity_type' => $this->entityType,
      ];
    }
    else {
      $result = [
        'entity_id' => $this->receipt['trip_id'],
        'entity_type' => EntityTypes::LDTRIP,
      ];
    }
    return $result;
  }

  /**
   * @param string $op
   * @return string
   */
  private function getLogTitle(string $op) {
    $result = '';

    switch ($op) {
      case 'update':
        $result = 'Receipt #' . $this->receiptId . ' was updated';
        break;

      case 'create':
        $result = 'Receipt #' . $this->receiptId . ' was created';
        break;

      case 'delete':
        $result = 'Receipt #' . $this->receiptId . ' was deleted';
        break;
    }

    return $result;
  }

  /**
   * Helper function to delete receipt everywhere.
   *
   * @param int $receipt_id
   *   Receipt id.
   */
  public static function helperDeleteRecipt(int $receipt_id) {
    db_delete('ld_trip_receipts')
      ->condition('receipt_id', $receipt_id)
      ->execute();

    db_delete('ld_trip_receipts_details')
      ->condition('receipt_id', $receipt_id)
      ->execute();

    db_delete('moveadmin_receipt')
      ->condition('id', $receipt_id)
      ->execute();

    db_delete('moveadmin_receipt_search')
      ->condition('id', $receipt_id)
      ->execute();

    db_delete('ld_invoices_receipts')
      ->condition('receipt_id', $receipt_id)
      ->execute();

    move_global_search_solr_invoke('delete', EntityTypes::RECEIPT, $receipt_id);
  }

  /**
   * Helper function to delete only LD receipts.
   *
   * @param int $receipt_id
   *   Receipt id.
   */
  public static function helperDeleteLdReceipt(int $receipt_id) {
    db_delete('ld_trip_receipts')
      ->condition('receipt_id', $receipt_id)
      ->execute();

    db_delete('ld_trip_receipts_details')
      ->condition('receipt_id', $receipt_id)
      ->execute();

    db_delete('ld_invoices_receipts')
      ->condition('receipt_id', $receipt_id)
      ->execute();

  }

  /**
   * Check if receipt is exist.
   *
   * @param int $receipt_id
   *   Receipt id.
   *
   * @return mixed
   */
  public static function receiptExist($receipt_id) {
    $result = db_select('ld_trip_receipts', 'ltr')
      ->fields('ltr', array('receipt_id'))
      ->condition('receipt_id', $receipt_id)
      ->execute()
      ->fetchField();
    return $result;
  }

}


