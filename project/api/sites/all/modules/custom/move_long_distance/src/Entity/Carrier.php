<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class TripCarrier.
 *
 * @package Drupal\move_long_distance\Entity
 */
class Carrier {

  /**
   * @var string
   */
  public $name = "";

  /**
   * @var string
   */
  public $contact_person = "";
  /**
   * @var string
   */
  public $contact_person_phone = "";

  /**
   * @var string
   */
  public $zip_code = '';

  /**
   * @var string
   */
  public $state = '';

  /**
   * @var string
   */
  public $country = '';
  /**
   * @var string
   */
  public $city = '';
  /**
   * @var string
   */
  public $email;

  /**
   * @var string
   */
  public $address = '';

  /**
   * @var string
   */
  public $icc_mc_number;

  /**
   * @var string
   */
  public $usdot_number;

  /**
   * @var string
   */
  public $web_site = '';

  /**
   * @var integer
   */
  public $company_carrier = 0;

  /**
   * @var integer
   */
  public $active = 0;

  /**
   * @var integer
   */
  public $created;

  /**
   * @var array
   */
  public $phones = array();

  /**
   * @var float
   */
  public $per_cf = 0;

  // Getters.
  public function getName() : string {
    return (string) $this->name;
  }

  public function getContactPerson() : string {
    return (string) $this->contact_person;
  }

  public function getContactPersonPhone() : string {
    return (string) $this->contact_person_phone;
  }

  public function getZipCode() : string {
    return (string) $this->zip_code;
  }

  public function getState() : string {
    return (string) $this->state;
  }

  public function getCountry() : string {
    return (string) $this->country;
  }

  public function getCity() : string {
    return (string) $this->city;
  }

  public function getEmail() : string {
    return (string) $this->email;
  }

  public function getAddress() : string {
    return (string) $this->address;
  }

  public function getIccMcNumber() : string {
    return (string) $this->icc_mc_number;
  }

  public function getUsdotNumber() : string  {
    return (string) $this->usdot_number;
  }

  public function getWebSite() : string {
    return (string) $this->web_site;
  }

  public function getCompanyCarrier() : int {
    return (int) $this->company_carrier;
  }

  public function getActive() : int {
    return (int) $this->active;
  }

  public function getCreate() : int {
    return (int) $this->created;
  }

  public function getPhones() : array {
    return (array) $this->phones;
  }

  public function getPerCf() : float {
    return (float) $this->per_cf;
  }

  // Setters.
  public function setName($name) {
    $this->name = (string) $name;
  }

  public function setContactPerson($contact_person) {
    $this->contact_person = (string) $contact_person;
  }

  public function setContactPersonPhone($contact_person_phone) {
    $this->contact_person_phone = (string) $contact_person_phone;
  }

  public function setZipCode($zip) {
    $this->zip_code = (string) $zip;
  }

  public function setState($state) {
    $this->state = (string)  $state;
  }

  public function setCountry($country) {
    $this->country = (string) $country;
  }

  public function setCity($city) {
    $this->city = (string) $city;
  }

  public function setEmail($email) {
    $this->email = (string) $email;
  }

  public function setAddress($address){
    $this->address = (string) $address;
  }

  public function setIccMcNumber($icc_mc_number) {
    $this->icc_mc_number = (string) $icc_mc_number;
  }

  public function setUsdotNumber($usdot_number) {
    $this->usdot_number = (string) $usdot_number;
  }

  public function setWebSite($web_site) {
    $this->web_site = (string) $web_site;
  }

  public function setCompanyCarrier($company_carrier) {
    $this->company_carrier = (int) $company_carrier;
  }

  public function setActive($active) {
    $this->active = (int) $active;
  }

  public function setPhones($phones) {
    $this->phones = (array) $phones;
  }

  public function setCreated($created) {
    $this->created = (int) $created;
  }

  public function setPerCf($per_cf) {
    $this->per_cf = (float) $per_cf;
  }

}
