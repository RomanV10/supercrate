<?php

namespace Drupal\move_long_distance\System;

use Drupal\move_long_distance\Services\Actions\SitInvoiceActions;
use Drupal\move_long_distance\Services\Actions\SitReceiptActions;
use Drupal\move_long_distance\Services\LongDistanceCarrier;
use Drupal\move_long_distance\Services\LongDistanceJob;
use Drupal\move_long_distance\Services\LongDistanceServices;
use Drupal\move_long_distance\Services\LongDistanceStorage;
use Drupal\move_long_distance\Services\LongDistanceReuqest;
use Drupal\move_long_distance\Services\LongDistanceTPDelivery;
use Drupal\move_long_distance\Services\LongDistanceTrip;
use Drupal\move_long_distance\Services\LongDistanceSit;
use Drupal\move_long_distance\Services\LongDistancePayroll;
use Drupal\move_services_new\Util\enum\EntityTypes;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  public static function definition() {
    return array(
      'long_distance_carrier' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::createLdCarrier',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::retrieveLdCarrier',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'active',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Filter by active',
                'default value' => 1,
                'source' => array('param' => 'active'),
              ),
               array(
                 'name' => 'jobs_only',
                 'optional' => TRUE,
                 'type' => 'int',
                 'description' => 'If set to 1 will return only jobs',
                 'default value' => 0,
                 'source' => array('param' => 'jobs_only'),
               ),
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'page_size',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pageSize'),
              ),
              array(
                'name' => 'total',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'If set to 1 will return total, Will work with jobs_only param',
                'default value' => 1,
                'source' => array('param' => 'total'),
              ),
              array(
                'name' => 'hide_zero',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'If set to 1 will return total, Will work with jobs_only param',
                'default value' => 0,
                'source' => array('param' => 'hide_zero'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateLdCarrier',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::deleteLdCarrier',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::indexLdCarrier',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pageSize'),
              ),
              array(
                'name' => 'active',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Filter by active',
                'default value' => 1,
                'source' => array('param' => 'active'),
              ),
              array(
                'name' => 'short_list',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Show short list for select',
                'default value' => 0,
                'source' => array('param' => 'shortlist'),
              ),
              array(
                'name' => 'total',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Show short list for select',
                'default value' => 0,
                'source' => array('param' => 'total'),
              ),
              array(
                'name' => 'hide_zero',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Show short list for select',
                'default value' => 0,
                'source' => array('param' => 'hide_zero'),
              ),
              array(
                'name' => 'sorting',
                'optional' => TRUE,
                'type' => 'string',
                'source' => array('param' => 'sorting'),
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'type' => 'string',
                'source' => array('param' => 'condition'),
              ),
              array(
                'name' => 'hide_no_trips',
                'optional' => TRUE,
                'type' => 'int',
                'source' => array('param' => 'hide_no_trips'),
                'default value' => 0,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
        'actions' => array(
          'get_trip_driver' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getTripDriver',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_carrier_lis_for_new_trip' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getCarrierList',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer users'),
          ),
          'add_carrier_trip' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::addCarrierTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer users'),
          ),
          'get_carrier_receipt' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getCarrierReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'carrier_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pageSize'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_carrier_invoices' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getCarrierInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'carrier_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pageSize'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_price_per_cf' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getCarrierPricePerCf',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'carrier_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
      ),
      'long_distance_trip' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::createLdTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::retrieveLdTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateLdTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::deleteLdTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::indexLdTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pageSize'),
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'type' => 'string',
                'source' => array('param' => 'condition'),
              ),
              array(
                'name' => 'sorting',
                'optional' => TRUE,
                'type' => 'string',
                'source' => array('param' => 'sorting'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
        'actions' => array(
          'show_all_trips' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::indexLdTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('data' => 'page'),
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('data' => 'pageSize'),
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'type' => 'array',
                'source' => array('data' => 'condition'),
              ),
              array(
                'name' => 'sorting',
                'optional' => TRUE,
                'type' => 'array',
                'source' => array('data' => 'sorting'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'filter' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::ldtFilter',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'status',
                'type' => 'array',
                'source' => array('data' => 'status'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'job_id',
                'type' => 'int',
                'source' => array('data' => 'job_id'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'add_job_ld_trip' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::addJobLdTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('data' => 'trip_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'jobs_id',
                'type' => 'array',
                'source' => array('data' => 'jobs_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'del_job_ld_trip' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::delJobLdTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('data' => 'trip_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'job_id',
                'type' => 'int',
                'source' => array('data' => 'job_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'del_job_ld_trip_multiply' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::delJobLdTripMultiply',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('data' => 'trip_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'jobs_id',
                'type' => 'array',
                'source' => array('data' => 'jobs_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'calc_closing' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::calcClosing',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_trip_closing' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getClosingTrip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'total',
                'type' => 'int',
                'source' => array('data' => 'total'),
                'default value' => 1,
                'optional' => TRUE,
              ),
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('data' => 'page'),
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('data' => 'pageSize'),
              ),
              array(
                'name' => 'trip_details',
                'optional' => TRUE,
                'type' => 'int',
                'default value' => 0,
                'source' => array('data' => 'tripDetails'),
              ),
              array(
                'name' => 'sorting',
                'optional' => TRUE,
                'type' => 'array',
                'source' => array('data' => 'sorting'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'set_receipt' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::setReceiptForTripJob',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('data' => 'tip_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'job_id',
                'type' => 'int',
                'source' => array('data' => 'job_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'update_closing_values' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateJobPCfValueCf',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('data' => 'trip_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'fields_to_update',
                'type' => 'array',
                'source' => array('data' => 'fields_to_update'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'job_id',
                'type' => 'int',
                'source' => array('data' => 'job_id'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_job_services' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getJobServices',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'job_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_job_receipt' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getJobReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'job_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'default value' => EntityTypes::MOVEREQUEST,
                'optional' => TRUE,
              ),
              array(
                'name' => 'types',
                'type' => 'array',
                'source' => array('data' => 'types'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'hide_tp_delivery',
                'type' => 'int',
                'source' => array('data' => 'hide_tp_delivery'),
                'optional' => TRUE,
                'default value' => 0,

              ),
              array(
                'name' => 'hide_tpd_agent_folio',
                'type' => 'int',
                'source' => array('data' => 'hide_tpd_agent_folio'),
                'optional' => TRUE,
                'default value' => 0,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_job_invoice' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getJobInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'job_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'update_payroll' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updatePayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_payroll' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getPayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'status',
                'type' => 'int',
                'source' => array('data' => 'status'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'submit_payroll' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::submitPayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'revoke_payroll' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::revokePayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'clear_all' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::clearAll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer users'),
          ),
        ),
      ),
      'long_distance_service' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::createLdService',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::retrieveLdService',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateLdService',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::deleteLdService',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::indexLdService',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pageSize'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
      ),
      'long_distance_payments' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::createLdCarrier',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::retrieveTransaction',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
        'actions' => array(
          'collect_jobs_info' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getInfoForInvoiceOrPay',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'jobs_id',
                'type' => 'array',
                'source' => array('data' => 'jobs_id'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'build_invoice' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::buildInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'jobs_id',
                'type' => 'array',
                'source' => array('data' => 'jobs_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'carrier_id',
                'type' => 'int',
                'source' => array('data' => 'carrier_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'amount',
                'type' => 'int',
                'source' => array('data' => 'amount'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_receipt' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'receipt_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_invoice' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::getInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'invoice_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'send_invoice' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::sendInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'invoice_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'auto_send',
                'type' => 'int',
                'source' => array('data' => 'auto_send'),
                'optional' => TRUE,
                'default value' => 1,

              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'resend_invoice' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::resendInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'invoice_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'auto_send',
                'type' => 'int',
                'source' => array('data' => 'auto_send'),
                'optional' => TRUE,
                'default value' => 1,

              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'delete_receipt' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::deleteReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'receipt_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'delete_invoice' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::deleteInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'invoice_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'pending_receipt' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::pendingReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'receipt',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'pending',
                'type' => 'int',
                'source' => array('data' => 'pending'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'refund_receipt' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::refundReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'value',
                'type' => 'array',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'set_receipt' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::setReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update_receipt' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
      'long_distance_storage' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::createLdStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::retrieveLdStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateLdStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::deleteLdStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::indexLdStorage',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'page_size',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pageSize'),
              ),
              array(
                'name' => 'short',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Only name and id.',
                'default value' => 0,
                'source' => array('param' => 'short'),
              ),
              array(
                'name' => 'active',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Show active storage or not.',
                'default value' => 1,
                'source' => array('param' => 'active'),
              ),
              array(
                'name' => 'sorting',
                'optional' => TRUE,
                'type' => 'string',
                'source' => array('param' => 'sorting'),
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'type' => 'string',
                'source' => array('param' => 'condition'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
        'actions' => array(
          'update_active_or_not' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateActiveOrNot',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'storage_id',
                'type' => 'int',
                'source' => array('data' => 'storage_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'int',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
      ),
      'long_distance_request' => array(
        'operations' => array(),
        'actions' => array(
          'search' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::searchRequests',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'page',
                'type' => 'int',
                'source' => array('data' => 'page'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'pagesize',
                'type' => 'int',
                'source' => array('data' => 'pagesize'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'condition',
                'type' => 'array',
                'source' => array('data' => 'condition'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'sorting',
                'type' => 'array',
                'source' => array('data' => 'sorting'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'addRequestToLDSearch' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::addRequestToLdSearch',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer users'),
          ),
        ),
      ),
      'long_distance_sit' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::createSIT',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::retrieveSIT',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateSIT',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::deleteSIT',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::indexSIT',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 1.',
                'default value' => 1,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'page_size',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pageSize'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
      ),
      'long_distance_tp_delivery' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::createTpDelivery',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'trip_id',
                'type' => 'int',
                'source' => array('data' => 'trip_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::retrieveTpDelivery',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'tpd_id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateTpDelivery',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'tpd_id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'ld_trip_id',
                'type' => 'int',
                'source' => array('data' => 'ld_trip_id'),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::deleteTpDelivery',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'tpd_id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
      ),
      'long_distance_job' => array(
        'operations' => array(
          'retrieve' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::retrieveJob',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'with_closing',
                'type' => 'int',
                'source' => array('data' => 'withClosing'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
        'actions' => array(
          'update_job_status' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::updateJobStatus',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'job_id',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'status',
                'type' => 'int',
                'source' => array('data' => 'status'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'addRequestToLDSearch' => array(
            'callback' => 'Drupal\move_long_distance\System\Service::addRequestToLdSearch',
            'file' => array(
              'type' => 'php',
              'module' => 'move_long_distance',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer users'),
          ),
        ),
      ),
    );
  }

  // Trip.
  public static function createLdTrip($data = array()) {
    $al_instance = new LongDistanceTrip();
    return $al_instance->create($data);
  }

  public static function retrieveLdTrip($id) {
    $al_instance = new LongDistanceTrip($id);
    return $al_instance->retrieve();
  }

  public static function updateLdTrip($id, $data) {
    $al_instance = new LongDistanceTrip($id);
    return $al_instance->update($data);
  }

  public static function deleteLdTrip(int $id) {
    $al_instance = new LongDistanceTrip($id);
    return $al_instance->delete();
  }

  public static function indexLdTrip($current_page, $per_page, $condition, $sorting = array()) {
    $al_instance = new LongDistanceTrip();
    return $al_instance->index($current_page, $per_page, $condition, $sorting);
  }

  public static function ldtFilter($status, int $job_id = 0, int $type = 0) {
    $instance = new LongDistanceTrip();
    return $instance->filter($job_id, $type, $status);
  }

  public static function calcClosing(array $data, $type = 0) {
    return LongDistanceJob::closingCalculation($data, FALSE, $type);

  }

  public static function getClosingTrip($id, $total = 1, int $page, int $page_size = -1, bool $trip_details = FALSE, $sorting = array()) {
    $al_instance = new LongDistanceTrip($id);
    return $al_instance->getClosingTrip($total, $page, $page_size, $trip_details, $sorting);
  }

  public static function updatePayroll(int $trip_id, array $data) {
    $al_instance = new LongDistancePayroll($trip_id);
    return $al_instance->update($data);
  }

  public static function getPayroll(int $trip_id, $status = 0) {
    $al_instance = new LongDistancePayroll($trip_id);
    return $al_instance->retrieve($status);
  }

  public static function submitPayroll(int $trip_id) {
    $al_instance = new LongDistancePayroll($trip_id);
    return $al_instance->submitPayroll();
  }

  public static function revokePayroll(int $trip_id) {
    $al_instance = new LongDistancePayroll($trip_id);
    return $al_instance->revokePayroll();
  }

  public static function clearAll() {
    return LongDistanceTrip::cleanAll();
  }

  // TP Delivery.
  public static function createTpDelivery(int $trip_id, array $data) {
    $al_instance = new LongDistanceTPDelivery(NULL, $trip_id);
    return $al_instance->create($data);
  }

  public static function updateTpDelivery(int $tpd_id, int $trip_id,  array $data) {
    $al_instance = new LongDistanceTPDelivery($tpd_id, $trip_id);
    return $al_instance->update($data);
  }

  public static function retrieveTpDelivery(int $tpd_id) {
    $al_instance = new LongDistanceTPDelivery($tpd_id);
    return $al_instance->retrieve();
  }

  public static function deleteTpDelivery(int $tpd_id) {
    $al_instance = new LongDistanceTPDelivery($tpd_id);
    return $al_instance->delete();
  }

  // Carrier.
  public static function getTripDriver() {
    $instance = new LongDistanceCarrier();
    return $instance->getCarriersWithTrips();
  }

  public static function createLdCarrier($data = array()) {
    $al_instance = new LongDistanceCarrier();
    return $al_instance->create($data);
  }

  public static function retrieveLdCarrier(int $id, int $active = 1, int $jobs_only = 0, int $page = 1, int $page_size = -1, int $total = 0, int $hide_zero = 0) {
    $al_instance = new LongDistanceCarrier($id);
    return $al_instance->retrieve($active, $jobs_only, $page, $page_size, $total, $hide_zero);
  }

  public static function updateLdCarrier($id, $data) {
    $al_instance = new LongDistanceCarrier($id);
    return $al_instance->update($data);
  }

  public static function deleteLdCarrier(int $id) {
    $al_instance = new LongDistanceCarrier($id);
    return $al_instance->delete();
  }

  public static function indexLdCarrier($current_page, $per_page, $active, $short_list = 0, $total = 0, $hide_zero = 0, $sorting = FALSE, $condition = '',  $hide_no_trips = 0) {
    $sorting = !empty($sorting) ? drupal_json_decode($sorting) : array();
    $condition = !empty($condition) ? drupal_json_decode($condition) : array();
    $al_instance = new LongDistanceCarrier();
    return $al_instance->index($current_page, $per_page, $active, $short_list, $total, $hide_zero, $sorting, $condition, $hide_no_trips);
  }

  public static function getCarrierList() {
    return LongDistanceCarrier::getCarrierList();
  }

  public static function getCarrierPricePerCf($car_id) {
    return LongDistanceCarrier::getCarrierCFPrice($car_id);
  }

  /**
   * Get all receipts for carrier.
   *
   * @param int $carrier_id
   *   Id of the carrier.
   *
   * @return array|mixed
   *   Array with jobs and receipt info. And total.
   */
  public static function getCarrierReceipt(int $carrier_id, int $page = 1, int $page_size = -1) {
    $al_instance = new LongDistanceCarrier($carrier_id);
    return $al_instance->getCarrierReceipts($page, $page_size);
  }

  /**
   * Get all invoices for carrier.
   *
   * @param int $carrier_id
   *   Id of the carrier.
   *
   * @return array|mixed
   *   Array with jobs and invoice info. And total.
   */
  public static function getCarrierInvoice(int $carrier_id, int $page = 1, int $page_size = -1) {
    $al_instance = new LongDistanceCarrier($carrier_id);
    return $al_instance->getCarrierInvoices($page = 1, $page_size = -1);
  }

  // Jobs.
  public static function retrieveJob(int $job_id, int $with_closing = 0) {
    $instance = new LongDistanceJob($job_id);
    return $instance->retrieve($with_closing);
  }

  public static function delJobLdTrip($trip_id, $job_id) {
    $instance = new LongDistanceJob($job_id);
    return $instance->removeJobLdTrip($trip_id);
  }

  public static function delJobLdTripMultiply($trip_id, array $jobs_id) {
    $instance = new LongDistanceJob();
    return $instance->removeJobLdTripMultiply($jobs_id, $trip_id);
  }

  public static function addJobLdTrip($trip_id, array $jobs_id) {
    $instance = new LongDistanceJob();
    return $instance->processAddRequestToTrip($trip_id, $jobs_id);
  }

  public static function setReceiptForTripJob($trip_id, $job_id, $data) {
    //$instance = new LongDistanceJob($job_id);
    //return $instance->updateTripJobNumbers($trip_id, $data);
  }

  public static function updateJobPCfValueCf(int $trip_id, int $id, array $fields_to_update, $job_id = NULL) {
    $instance = new LongDistanceJob($job_id);
    return $instance->updateJobPCfValueCf($trip_id, $id, $fields_to_update);
  }

  public static function getJobServices($job_id, $trip_id = FALSE) {
    $instance = new LongDistanceJob($job_id);
    return $instance->getJobServices();
  }

  public static function getJobReceipt($job_id, $entity_type = EntityTypes::MOVEREQUEST, $types = array(), bool $hide_tp_delivery = FALSE, bool $hide_tpd_agent_folio = FALSE) {
    $instance = new LongDistanceJob($job_id);
    return $instance->getJobReceipt($types, $entity_type, $hide_tp_delivery, $hide_tpd_agent_folio);
  }

  public static function getJobInvoice($job_id) {
    $instance = new LongDistanceJob($job_id);
    return $instance->getJobInvoice();
  }

  public static function updateJobStatus(int $job_id, int $status) {
    return LongDistanceJob::updateJobStatus($job_id, $status);
  }

  // Services.
  public static function createLdService(array $data) {
    $al_instance = new LongDistanceServices($data['trip_id'], $data['job_id']);
    return $al_instance->create($data);
  }

  public static function updateLdService(array $data) {
    $al_instance = new LongDistanceServices($data['trip_id'], $data['job_id']);
    return $al_instance->create($data);
  }

  // Invoices.
  public static function getInfoForInvoiceOrPay(array $jobs_id) {
    return SitInvoiceActions::getInfoForInvoiceOrPay($jobs_id);
  }

  public static function sendInvoice(int $invoice_id, int $auto_send) {
    return (new SitInvoiceActions($invoice_id))->sendInvoiceEmail($auto_send);
  }

  public static function buildInvoice(array $jobs_id, int $carrier_id, float $amount) {
    return SitInvoiceActions::processInvoice($jobs_id, $carrier_id, $amount);
  }

  public static function getReceipt(int $receipt_id) {
    return SitReceiptActions::getReceipt($receipt_id);
  }

  public static function getInvoice(int $invoice_id) {
    return (new SitInvoiceActions($invoice_id))->getInvoice();
  }

  public static function resendInvoice(int $invoice_id, int $auto_send) {
    return (new SitInvoiceActions($invoice_id))->sendInvoiceEmail($auto_send);

  }

  public static function deleteReceipt(int $receipt_id) {
    $sit_receipt = new SitReceiptActions();
    $sit_receipt->setReceiptId($receipt_id);
    return $sit_receipt->deleteReceipt();
  }

  public static function deleteInvoice(int $invoice_id) {
    return (new SitInvoiceActions($invoice_id))->deleteInvoice();
  }

  public static function pendingReceipt(int $receipt_id, bool $pending) {
    return (new SitReceiptActions())->pendingReceipt($receipt_id, $pending);

  }
  public static function refundReceipt(array $value) {
    return (new SitReceiptActions($value['entity_id'], $value['entity_type'], $value))->refundReceipt();
  }

  public static function setReceipt(int $entity_id, int $entity_type, array $data) {
   return (new SitReceiptActions($entity_id, $entity_type, $data))->processAddReceipt();
  }

  public static function updateReceipt(int $id, array $data) {
    $sit_receipt = new SitReceiptActions(0, 0, $data);
    $sit_receipt->setReceiptId($id);
    return $sit_receipt->updateReceipt();
  }

  // Storages.
  public static function createLdStorage($data = array()) {
    $al_instance = new LongDistanceStorage();
    return $al_instance->create($data);
  }

  public static function retrieveLdStorage(int $id) {
    $al_instance = new LongDistanceStorage($id);
    return $al_instance->retrieve();
  }

  public static function updateLdStorage($id, $data) {
    $al_instance = new LongDistanceStorage($id);
    return $al_instance->update($data);
  }

  public static function deleteLdStorage(int $id) {
    $al_instance = new LongDistanceStorage($id);
    return $al_instance->delete();
  }

  public static function indexLdStorage($current_page, $per_page, $short = 0, $active = 1, $sorting = '', $condition = '') {
    $sorting = !empty($sorting) ? drupal_json_decode($sorting) : array();
    $condition = !empty($condition) ? drupal_json_decode($condition) : array();
    $al_instance = new LongDistanceStorage();
    return $al_instance->index($current_page, $per_page, $short, $active, $sorting, $condition);
  }
  public static function updateActiveOrNot(int $storage_id, int $value) {
    $al_instance = new LongDistanceStorage($storage_id);
    return $al_instance->updateStorageToActiveOrNot($value);
  }

  /**
   * Search request in ld delivery page.
   *
   * @param int $page
   *   Current page.
   * @param int $pageSize
   *   Page size.
   * @param array $condition
   *   Conditions.
   * @param array $sorting
   *   Sorting.
   *
   * @return array
   *   Requests.
   */
  public static function searchRequests(int $page, int $pageSize, array $condition, array $sorting = array()) {
    return LongDistanceReuqest::search($condition, $sorting, $page, $pageSize);
  }

  public static function addRequestToLdSearch() {
    return LongDistanceReuqest::addRequestTOlDRequets();
  }

  // SIT.
  public static function createSIT($data = array()) {
    $al_instance = new LongDistanceSit();
    return $al_instance->create($data);
  }

  public static function retrieveSIT(int $id, $entity_type) {
    $al_instance = new LongDistanceSit($id);
    return $al_instance->retrieve();
  }

  public static function updateSIT($id, $entity_type, $data) {
    $al_instance = new LongDistanceSit($id, $entity_type);
    return $al_instance->update($data);
  }

  public static function deleteSIT(int $id) {
    $al_instance = new LongDistanceSit($id);
    return $al_instance->delete();
  }

  public static function indexSIT($current_page, $per_page) {
    $al_instance = new LongDistanceSit();
    return $al_instance->index($current_page, $per_page);
  }

}
