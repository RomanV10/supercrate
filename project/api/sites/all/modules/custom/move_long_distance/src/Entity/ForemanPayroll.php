<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class ForemanPayroll.
 *
 * @package Drupal\move_long_distance\Entity
 */
class ForemanPayroll {

  /**
   * @var int
   */
  public $ld_trip_id = 0;

  /**
   * @var int
   */
  public $uid = 0;

  /**
   * @var float
   */
  public $total_received = 0;

  /**
   * @var float
   */
  public $total_expenses = 0;

  /**
   * @var float
   */
  public $daily_amount = 0;

  /**
   * @var int
   */
  public $total_days = 0;

  /**
   * @var float
   */
  public $total_daily = 0;

  /**
   * @var float
   */
  public $hourly_rate = 0;

  /**
   * @var int
   */
  public $total_hours = 0;

  /**
   * @var float
   */
  public $total_hourly = 0;

  /**
   * @var float
   */
  public $loading_rate = 0;

  /**
   * @var int
   */
  public $loading_volume = 0;

  /**
   * @var float
   */
  public $total_loading = 0;

  /**
   * @var float
   */
  public $unloading_rate = 0;

  /**
   * @var float
   */
  public $unloading_volume = 0;

  /**
   * @var float
   */
  public $total_unloading = 0;

  /**
   * @var int
   */
  public $mileage_start = 0;

  /**
   * @var int
   */
  public $mileage_end = 0;

  /**
   * @var float
   */
  public $mileage_rate = 0;

  /**
   * @var int
   */
  public $mileage = 0;

  /**
   * @var float
   */
  public $total_mileage = 0;

  /**
   * @var float
   */
  public $total_payroll = 0;

  /**
   * @var float
   */
  public $total_collected = 0;

  // Getters.
  public function getLdTripId() {
    return (int) $this->ld_trip_id;
  }

  public function getUid() {
    return (int) $this->uid;
  }

  public function getTotalReceived() {
    return (float) $this->total_received;
  }

  public function getTotalExpenses() {
    return (float) $this->total_expenses;
  }

  public function getDailyAmount() {
    return (float) $this->daily_amount;
  }
  
  public function getTotalDays() {
    return (int) $this->total_days;
  }
  
  public function getTotalDaily() {
    return (float) $this->total_daily;
  }
  
  public function getHourlyRate() {
    return (float) $this->hourly_rate;
  }
  
  public function getTotalHours() {
    return (int) $this->total_hours;
  }  
  
  public function getTotalHourly() {
    return (float) $this->total_hourly;
  }
  
  public function getLoadingRate() {
    return (float) $this->loading_rate;
  }
  
  public function getLoadingVolume() {
    return (int) $this->loading_volume;
  }
  
  public function getTotalLoading() {
    return (float) $this->total_loading;
  }
  public function getUnloadingRate() {
    return (float) $this->unloading_rate;
  }

  public function getUnloadingVolume() {
    return (int) $this->unloading_volume;
  }
  
  public function getTotalUnloading() {
    return (float) $this->total_unloading;
  }
  public function getMileageStart() {
    return (int) $this->mileage_start;
  }
  public function getMileageEnd() {
    return (int) $this->mileage_end;
  }
  public function getMileageRate() {
    return (float) $this->mileage_rate;
  }
  public function getMileage() {
    return (int) $this->mileage;
  }
  public function getTotalMileage() {
    return (float) $this->total_mileage;
  }
  public function getTotalPayroll() {
    return (float) $this->total_payroll;
  }
  public function getTotalCollected() {
    return (float) $this->total_collected;
  }



  // Setters.
  public function setLdTripId($ld_trip_id) {
    $this->ld_trip_id = (int) $ld_trip_id;
  }

  public function setUid($uid) {
    $this->uid = (int) $uid;
  }

  public function setTotalReceived($total_received) {
    $this->total_received = (float) $total_received;
  }

  public function setTotalExpenses($total_expenses) {
    $this->total_expenses = (float) $total_expenses;
  }

  public function setDailyAmount($daily_amount) {
    $this->daily_amount = (float) $daily_amount;
  }

  public function setTotalDays($total_days) {
    $this->total_days = (int) $total_days;
  }

  public function setTotalDaily($total_daily) {
    $this->total_daily = (float) $total_daily;
  }

  public function setHourlyRate($hourly_rate) {
    $this->hourly_rate = (float) $hourly_rate;
  }

  public function setTotalHours($total_hours) {
    $this->total_hours = (int) $total_hours;
  }

  public function setTotalHourly($total_hourly) {
    $this->total_hourly = (float) $total_hourly;
  }

  public function setLoadingRate($loading_rate) {
    $this->loading_rate = (float) $loading_rate;
  }

  public function setLoadingVolume($loading_volume) {
    $this->loading_volume = (int) $loading_volume;
  }

  public function setTotalLoading($total_loading) {
    $this->total_loading = (float) $total_loading;
  }

  public function setUnloadingRate($unloading_rate) {
    $this->unloading_rate = (float) $unloading_rate;
  }

  public function setUnloadingVolume($unloading_volume) {
    $this->unloading_volume = (int) $unloading_volume;
  }

  public function setTotalUnloading($total_unloading) {
    $this->total_unloading = (float) $total_unloading;
  }

  public function setMileageStart($mileage_start) {
    $this->mileage_start = (int) $mileage_start;
  }
  public function setMileageEnd($mileage_end) {
    $this->mileage_end = (int) $mileage_end;
  }

  public function setMileageRate($mileage_rate) {
    $this->mileage_rate = (float) $mileage_rate;
  }
  public function setMileage($mileage) {
    $this->mileage = (int) $mileage;
  }
  public function setTotalMileage($total_mileage) {
    $this->total_mileage = (float) $total_mileage;
  }
  public function setTotalPayroll($total_payroll) {
    $this->total_payroll = (float) $total_payroll;
  }

  public function setTotalCollected($total_collected) {
    $this->total_collected = (float) $total_collected;
  }
}
