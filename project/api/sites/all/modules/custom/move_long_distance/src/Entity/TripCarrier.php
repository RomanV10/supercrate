<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class TripCarrier.
 *
 * @package Drupal\move_long_distance\Entity
 */
class TripCarrier {

  /**
   * @var int
   */
  public $carrier_id = 0;

  /**
   * @var string
   */
  public $driver_name = "";

  /**
   * @var string
   */
  public $driver_phone = "";

  // Getters.
  public function getCarrierID() {
    return (int) $this->carrier_id;
  }

  public function getDriverName() {
    return (string) $this->driver_name;
  }

  public function getDriverPhone() {
    return (string) $this->driver_phone;
  }

  // Setters.
  public function setCarrier($carrier_id) {
    $this->carrier = (int) $carrier_id;
  }

  public function setDriverName($driver_name) {
    $this->driver_name = (string) $driver_name;
  }

  public function setDriverPhone($driver_phone) {
    $this->driver_phone = (string) $driver_phone;
  }

}
