<?php

namespace Drupal\move_long_distance\Services;

use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_long_distance\Entity\Services;
use Drupal\move_long_distance\Entity\ServicesExtra;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_new_log\Services\Log;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class Invoice.
 *
 * @package Drupal\move_long_distance\Services
 */
class LongDistanceServices extends BaseService {
  private $tripId = NULL;
  private $jobId = NULL;
  private $encoders;
  private $normalizers;
  private $serializer;
  public $logText = [];

  public function __construct($trip_id = NULL, $job_id = NULL) {
    $this->tripId = $trip_id;
    $this->jobId = $job_id;
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);
  }

  /**
   * Adding services to job. Before we need to remove all services from job.
   *
   * @param array $data
   *   Array must contain job and trip id.
   *
   * @return bool|mixed
   */
  public function create($data = array(), $job_type = FALSE) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    $log_text = array();
    try {
      $jobInstance = new LongDistanceJob($this->jobId);
      // If job in receipt or invoice call exception.
      $jobInstance->jobHasTransactionError();

      $old = $this->retrieve();
      $jobInstance->removeAllJobServices();


      $all_services_total = 0;
      $services = $data['services'];
      if (!empty($services)) {
        foreach ($services as $service_item) {
          // Add service to other variable.
          $extra_services = $service_item['extra_services'];
          unset($service_item['extra_services']);

          $total = 0;
          $temp_total = 1;
          $extra_services_items = array();
          // First we need to get all extra services and calculate they total.
          foreach ($extra_services as $key => $item) {
            $extra_services_items[$key] = (array) $this->serializer->deserialize(json_encode($item), ServicesExtra::class, 'json');
            $temp_total = $temp_total * $extra_services_items[$key]['services_default_value'];
          }
          $total += $temp_total;

          $new[] = array(
            'name' => $service_item['name'],
            'total' => $total,
          );

          $log_text[]['simpleText'] = "Service name is  " . $service_item['name'] . ". Service total is " . $total;

          // Now we adding total and job id to the main service and get values.
          $service_item['total'] = $total;
          $service_item['job_id'] = $this->jobId;
          $service_item = (array) $this->serializer->deserialize(json_encode($service_item), Services::class, 'json');
          // Unset service id. We don't need to use it in db.
          unset($service_item['service_id']);

          // Insert main extra service to db.
          $service_id = db_insert('ld_trip_services')
            ->fields($service_item)
            ->execute();

          if (!empty($service_id) && !empty($extra_services_items)) {
            // Insert each extra service to db.
            foreach ($extra_services_items as $item) {
              // Add main service id to fields for insert to db.
              $item['service_id'] = $service_id;
              $service_extra_id[] = db_insert('ld_trip_extra_services')
                ->fields($item)
                ->execute();
            }
          }
          // Calculate all totals for job.
          $all_services_total += $service_item['total'];
        }
        $title = 'Services was add for job #' . $this->jobId;

        $field_to_update['services_total'] = $all_services_total;
        // Update all job numbers and totals.
        $result = $jobInstance->updateTripJobNumbers($this->tripId, FALSE, $field_to_update, $job_type);
      }
      else {
        $field_to_update['services_total'] = 0;
        // Update all job numbers and totals.
        $result = $jobInstance->updateTripJobNumbers($this->tripId, FALSE, $field_to_update, $job_type);
        if (!empty($old)) {
          $title = 'Services was removed for job #' . $this->jobId;
        }
      }

      // TODO change this to diff logs.
      if (!empty($title)) {
        global $user;
        $user_name = isset($user->name) ? $user->name : 'Anonymous';
        $log_data[] = array(
          'details' => array([
            'event_type' => 'service',
            'activity' => $user_name,
            'title' => $title,
            'text' => $log_text,
            'date' => time(),
          ],
          ),
          'source' => $user_name,
        );
        $log = new Log($this->tripId, EntityTypes::LDTRIP);
        $log->create($log_data);
      }


    }
    catch (\Throwable $e) {
      $message = "Create Trip Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('service', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  public function retrieve() {
    return db_select('ld_trip_services', 'lts')
      ->fields('lts', array('name', 'total'))
      ->condition('job_id', $this->jobId)
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
  }

  public function update($data = array()) {}

  public function delete() {}

  public function index() {}

  /**
   * Check diff on update and add diff to log var.
   *
   * @param array $new
   *   First array to check.
   * @param array $old
   *   Second array to check.
   *
   * @return array
   *   Array with diff.
   */
  public function arrayDiffAssocRecursive(array $new, array $old) : array {
    foreach ($new as $key => $value) {
      if (is_array($value)) {
        if (!isset($old[$key])) {
          $difference[$key] = $value;
        }
        elseif (!is_array($old[$key])) {
          $difference[$key] = $value;
        }
        else {
          $new_diff = $this->arrayDiffAssocRecursive($value, $old[$key]);
          if ($new_diff != FALSE) {
            $difference[$key] = $new_diff;
          }
        }
      }
      elseif ((!isset($old[$key]) || $old[$key] != $value) && !($old[$key] === NULL && $value === NULL)) {
        $difference[$key]['new'] = $value;
        $difference[$key]['old'] = isset($old[$key]) ? $old[$key] : NULL;
      }
    }
    return !isset($difference) ? array() : $difference;
  }

  /**
   * Set logs array from diff on update.
   *
   * @param array $diff
   *   Array with diff.
   * @param string $previous_field
   *   Previous field_name to build log string.
   * @param int $level
   *   Iteration level.
   */
  public function prepareLogsFormDiff(array $diff, string &$previous_field = '', int $level = 1) : void {
    $excluded_fields = ['owner', 'weight'];
    foreach ($diff as $field_name => $val) {
      if (!in_array($field_name, $excluded_fields)) {
        if ($level === 1) {
          $previous_field = '';
        }
        if (is_array($val) && !isset($val['new'])) {
          $previous_field .= ucfirst(str_replace('_', ' ', $field_name)) . '->';
          $this->prepareLogsFormDiff($val, $previous_field, $level + 1);
        }
        else {
          $this->logText[]['simpleText'] = '"' . $previous_field . ucfirst(str_replace('_', ' ', $field_name)) . '" was changed from "' . str_replace('.00', '', $val['old']) . '" to "' . str_replace('.00', '', $val['new']) . '"';
        }
      }
    }
  }

}
