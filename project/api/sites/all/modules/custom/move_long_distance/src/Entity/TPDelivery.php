<?php

namespace Drupal\move_long_distance\Entity;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class TPDelivery.
 *
 * @package Drupal\move_long_distance\Entity
 */
class TPDelivery {

  /**
   * @var string
   */
  public $notes = '';

  /**
   * @var integer
   */
  public $delivery_job_id;

  /**
   * @var TPDeliveryDetails
   */
  public $details;

  /**
   * @var TripClosing
   */
  public $closing;

  /**
   * TPDelivery details constructor.
   */
  public function __construct() {
    $this->details = new TPDeliveryDetails();
    $this->closing = new TripClosing();
  }

  // Getters.
  public function getDetails() : TPDeliveryDetails {
    return $this->details;
  }

  public function getClosing() : TripClosing {
    return $this->closing;
  }

  public function getdDliveryJobId() {
    return (int) $this->delivery_job_id;
  }

  public function getNotes() {
    return (string) $this->notes;
  }

  // Setters.
  public function setDliveryJobId($delivery_job_id) {
    $this->delivery_job_id = (int) $delivery_job_id;
  }

  public function setNotes($notes) {
    $this->notes = (string) $notes;
  }

  public function setDetails($details) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->details = $serializer->deserialize(json_encode($details), TPDeliveryDetails::class, 'json');
  }
  public function setClosing($closing) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->closing = $serializer->deserialize(json_encode($closing), TripClosing::class, 'json');
  }

}
