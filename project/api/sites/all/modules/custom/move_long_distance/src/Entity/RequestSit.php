<?php

namespace Drupal\move_long_distance\Entity;


/**
 * Class RequestSit.
 *
 * @package Drupal\move_long_distance\Entity
 */
class RequestSit {

  /**
   * @var int
   */
  public $ld_nid;

  /**
   * @var int
   */
  public $entity_type = 0;

  /**
   * @var
   */
  public $date;


  /**
   * @var string
   */
  public $client_name = "";

  /**
   * @var string
   */
  public $storage_id;

  /**
   * @var string
   */
  public $rooms;

  /**
   * @var
   */
  public $foreman;

  /**
   * @var int
   */
  public $blankets;

  /**
   * @var int
   */
  public $status;

  /**
   * @var array
   */
  public $lots_info;

  /**
   * @var array
   */
  public $lot_number;

  // Getters.


  public function getClientName() : string {
    return (string) $this->client_name;
  }

  public function getLdNid() {
    return (int) $this->ld_nid;
  }
  public function getRooms() {
    return (string) $this->rooms;
  }
  public function getEntityType() {
    return (int) $this->entity_type;
  }

  public function getDate() {
    return (int) $this->date;
  }

  public function getStorageId(){
    return (int) $this->storage_id;
  }

  public function getForeman() {
    return (int) $this->foreman;
  }

  public function getBlankets() {
    return (int) $this->blankets;
  }

  public function getStatus() {
    return (int) $this->status;
  }
  public function getLotNumber()  {
    return $this->lot_number;
  }
  public function getLotsInfo()  {
    return $this->lots_info;
  }

  // Setters.
  public function setClientName($client_name) {
    $this->client_name = (string) $client_name;
  }

  public function setLdNid(int $ld_nid) {
    $this->ld_nid = (int) $ld_nid;
  }

  public function setEntityType(int $entity_type) {
    $this->entity_type = (int) $entity_type;
  }

  public function setDate( $date) {
    $this->date = (int) $date;
  }
  public function setStorageId($storage_id) {
    $this->storage_id = (int) $storage_id;
  }

  public function setForeman($foreman) {
    $this->foreman = (int) $foreman;
  }

  public function setBlankets($blankets) {
    $this->blankets = (int) $blankets;
  }
  public function setRooms($rooms) {
    $this->rooms = (string) $rooms;
  }

  public function setStatus(int $status) {
    $this->status = (int) $status;
  }
  public function setLotsInfo($lots_info) {
    $this->lots_info = drupal_json_encode($lots_info);
  }
  public function setLotNumber($lot_number) {
    $this->lot_number = drupal_json_encode($lot_number);
  }
}
