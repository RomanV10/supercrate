<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class TPDeliveryDetails.
 *
 * @package Drupal\move_long_distance\Entity
 */
class TPDeliveryDetails {

  /**
   * @var int
   */
  public $delivery_for_company = 0;

  /**
   * @var integer
   */
  public $actual_date_time = 0;

  /**
   * @var integer
   */
  public $date_from;
  /**
   * @var integer
   */
  public $date_to;

  /**
   * @var string
   */
  public $customer = "";
  /**
   * @var string
   */
  public $address = '';

  /**
   * @var string
   */
  public $zip = '';

  /**
   * @var string
   */
  public $state = '';

  /**
   * @var string
   */
  public $country = '';
  /**
   * @var string
   */
  public $city = '';
  /**
   * @var string
   */
  public $email;


  /**
   * @var array
   */
  public $phones = array();

  // Getters.
  public function getDeliveryForCompany() {
    return (int) $this->delivery_for_company;
  }

  public function getActualDateTime(){
    return (int) $this->actual_date_time;
  }

  public function getDateFrom() {
    return (int) $this->date_from;
  }

  public function getDateTo() {
    return (int) $this->date_to;
  }

  public function getCustomer() {
    return (string) $this->customer;
  }

  public function getAddress() {
    return (string) $this->address;
  }

  public function getZip() {
    return (string) $this->zip;
  }
  public function getState() {
    return (string) $this->state;
  }

  public function getCity() {
    return (string) $this->city;
  }
  public function getCountry() {
    return (string) $this->country;
  }

  public function getEmail() {
    return (string) $this->email;
  }


  public function getPhones() : array {
    return (array) $this->phones;
  }


  // Setters.
  public function setDeliveryForCompany($delivery_for_company) {
    $this->delivery_for_company = (int) $delivery_for_company;
  }

  public function setActualDateTime($actual_date_time) {
    $this->actual_date_time = (string) $actual_date_time;
  }

  public function setDateFrom($date_from) {
    $this->date_from = (int) $date_from;
  }

  public function setDateTo($date_to) {
    $this->date_to = (int) $date_to;
  }

  public function setCustomer($customer) {
    $this->customer = (string) $customer;
  }

  public function setAddress($address) {
    $this->address = (string) $address;
  }



  public function setZip($zip) {
    $this->zip = (string) $zip;
  }

  public function setState($state) {
    $this->state = (string) $state;
  }

  public function setCity($city) {
    $this->city = (string) $city;
  }

  public function setCountry($country) {
    $this->country = (string) $country;
  }

  public function setEmail($email) {
    $this->email = (string) $email;
  }

  public function setPhones($phones) {
    $this->phones = (array) $phones;
  }


}
