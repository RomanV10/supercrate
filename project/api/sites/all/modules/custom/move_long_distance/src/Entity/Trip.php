<?php

namespace Drupal\move_long_distance\Entity;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class Trip.
 *
 * @package Drupal\move_long_distance\Entity
 */
class Trip {

  /**
   * @var TripDetails
   */
  public $details;


  /**
   * @var TripAddress
   */
  public $start_from;

  /**
   * @var TripAddress
   */
  public $ends_in;

  /**
   * @var TripCarrier
   */
  public $carrier;

  /**
   * @var array
   */
  public $foreman;
  /**
   * @var array
   */
  public $helper;

  /**
   * @var string
   */
  public $note = "";

  /**
   * @var string
   */
  public $job_id = 0;

  /**
   * Trip constructor.
   */
  public function __construct() {
    $this->start_from = new TripAddress();
    $this->ends_in = new TripAddress();
    $this->carrier = new TripCarrier();
    $this->details = new TripDetails();
  }

  // Getters.
  public function getCarrier(): TripCarrier {
    return $this->carrier;
  }

  public function getForeman() {
    return (array) $this->foreman;
  }
  public function getHelper() {
    return (array) $this->helper;
  }
  public function getDetails() : TripDetails {
    return $this->details;
  }

  public function getStartFrom() : TripAddress {
    return $this->start_from;
  }

  public function getEndsIn() : TripAddress {
    return $this->ends_in;
  }

  public function getNote() : string {
    return (string) $this->note;
  }

  public function getJobId() : int {
    return (int) $this->job_id;
  }

  // Setters.
  public function setCarrier($carrier) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->carrier = $serializer->deserialize(json_encode($carrier), TripCarrier::class, 'json');
  }
  public function setForeman($foreman) {
    $this->foreman = (array) $foreman;
  }
  public function setHelper($helper) {
    $this->helper = (array) $helper;
  }
  public function setDetails($details) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->details = $serializer->deserialize(json_encode($details), TripDetails::class, 'json');
  }

  public function setStartFrom($start) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->start_from = $serializer->deserialize(json_encode($start), TripAddress::class, 'json');
  }

  public function setEndsIn($ends) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->ends_in = $serializer->deserialize(json_encode($ends), TripAddress::class, 'json');
  }

  public function setNote($note) {
    $this->note = $note;
  }

  public function setJobId($job_id) {
    $this->job_id = $job_id;
  }

}
