<?php

namespace Drupal\move_long_distance\Controller;

use Drupal\move_long_distance\Services\LongDistancePayroll;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\Roles;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Drupal\move_long_distance\Entity\ForemanPayroll;
use Drupal\move_long_distance\Entity\HelperPayroll;
use Drupal\move_long_distance\Entity\PayrollDetails;

/**
 * Class ControllerPayroll.
 *
 * Intermediate class for calculating payroll fot LongDistance.
 *
 * @package Drupal\move_long_distance\Controller
 */
class ControllerPayroll {

  /**
   * @var ForemanPayroll
   */
  private $LDForeman = array();

  /**
   * @var array<HelperPayroll>
   */
  private $LDHelpers = array();

  /**
   * @var PayrollDetails
   */
  private $LDDetails = array();

  /**
   * @var string
   */
  private $jobType = "Long Distance";

  /**
   * @var string
   */
  private $dateFrom = "";

  /**
   * @var string
   */
  private $dateTo = "";

  /**
   * @var array
   */
  private $encoders;

  /**
   * @var array
   */
  private $normalizers;

  /**
   * @var \Symfony\Component\Serializer\Serializer
   */
  private $serializer;

  function __construct(array $payroll, $date_from = "", $date_to = "") {
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);
    if (!empty($payroll['foreman'])) {
      $this->LDForeman = (array) $this->serializer->deserialize(json_encode($payroll['foreman']), ForemanPayroll::class, 'json');
    }
    if (!empty($payroll['helper'])) {
      foreach ($payroll['helper'] as $helper) {
        $this->LDHelpers[] = (array) $this->serializer->deserialize(json_encode($helper), HelperPayroll::class, 'json');
      }
    }
    if (!empty($payroll['details'])) {
      $this->LDDetails = $this->serializer->deserialize(json_encode($payroll['details']), PayrollDetails::class, 'json');
    }

    $this->dateTo = Extra::getDateTo($date_to);
    $this->dateFrom = Extra::getDateFrom($date_from);
  }

  public function calculate() {
    $payroll_time = $this->LDDetails->getDateEnd();
    $main_payroll = new Payroll();
    $main_payroll->cacheCreatePayrollAll(date("Y-m-d", $payroll_time));
  }

  /**
   * Returns filtered by end date long distance requests ids.
   *
   * @return array
   *   Array of ids.
   */
  public function getLdRequestsIdsForPayroll(): array {
    $condition = db_and()->condition('status', TRUE)
      ->condition('date_end', array($this->dateFrom, $this->dateTo), 'BETWEEN');

    $query = db_select('ld_trip_payroll_details', 'details')
      ->fields('details', array('ld_trip_id'))
      ->condition($condition);

    return $query->execute()->fetchCol();
  }

  /**
   * @deprecated
   */
  public function getAllLDRequestsForPeriod() : array {
    $query = db_select('ld_trip_details', 'trip_details')
      ->fields('trip_details')
      ->condition('date_end', array($this->dateFrom, $this->dateTo), 'BETWEEN');
    return $query->execute()->fetchCol(0);
  }

  /**
   * Filtration long distance request by crew.
   *
   * @param array $ids
   *   Lists of potentials requests for filtering.
   * @param int $uid
   *   User id.
   *
   * @return array
   *   Lists request ids.
   */
  public function filterLongDistance(array $ids, int $uid, int $role) : array {
    $result = array();
    $roles = array(Roles::FOREMAN, Roles::HELPER);
    if (!in_array($role, $roles)) {
      return array();
    }

    $user_role = ($role == Roles::FOREMAN) ? "foreman" : "helper";

    foreach ($ids as $ld_id) {
      $data = &drupal_static("LongDistance:$ld_id", FALSE);

      if (!$data) {
        $ld_instance = new LongDistancePayroll($ld_id);
        $data = $ld_instance->retrieve();
      }

      if (!empty($data[$user_role])) {
        $flag = FALSE;
        switch ($role) {
          case Roles::FOREMAN:
            $user = $data[$user_role];
            if ($user['uid'] == $uid) {
              $flag = TRUE;
            }
            break;

          case Roles::HELPER:
            foreach ($data[$user_role] as $user) {
              if ($user['uid'] == $uid) {
                $flag = TRUE;
                break;
              }
            }
            break;
        }

        if ($flag) {
          $result[] = $ld_id;
        }
      }
    }

    return $result;
  }

  /**
   * Get long distance requests by ids.
   *
   * @param array $ids
   *   Long Distance ids.
   *
   * @return array
   *   Requests.
   */
  public function getLDRequestsByIds(array $ids) : array {
    $result = array();

    foreach ($ids as $ld_id) {
      $data = &drupal_static("LongDistance:$ld_id", FALSE);

      if (!$data) {
        $ld_instance = new LongDistancePayroll($ld_id);
        $data = $ld_instance->retrieve();
      }

      $result[$ld_id] = $data;
    }

    return $result;
  }

}
