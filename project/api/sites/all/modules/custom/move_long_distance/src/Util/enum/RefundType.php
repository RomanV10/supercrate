<?php

namespace Drupal\move_long_distance\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RefundType.
 *
 * @package Drupal\move_long_distance\Util
 */
class RefundType extends BaseEnum {
  const CUSTOM = 0;
  const REVERT = 1;
}
