<?php

namespace Drupal\move_long_distance\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_long_distance\Entity\Storage;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use PDO;

/**
 * Class Invoice.
 *
 * @package Drupal\move_long_distance\Services
 */
class LongDistanceStorage extends BaseService {

  private $storageId = NULL;
  private $encoders;
  private $normalizers;
  private $serializer;

  /**
   * LongDistanceStorage constructor.
   *
   * @param null $storage_id
   *   Storage id.
   */
  public function __construct($storage_id = NULL) {
    $this->storageId = $storage_id;
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);
  }

  public function setId($storage_id) {
    $this->storageId = $storage_id;
  }

  public function create($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $storage_id = FALSE;
    try {
      $storage = $this->serializer->deserialize(json_encode($data), Storage::class, 'json');
      $storage->created = ($storage->created) ? $storage->created : time();

      // Adding phones to separate variable.
      $storage_phones = $storage->phones;
      if (empty($storage_phones)) {
        throw new \Exception("Phones is empty");
      }
      else {
        // Set all storages default value to zero.
        // If new default storage is set to 1.
        if (!empty($storage->default_storage)) {
          self::updateStorageFields(array('default_storage' => 0));
        }

        // Unset phones from storage object. We don't have this field in db.
        // If keep it will have error.
        unset($storage->phones);
        $storage_id = db_insert('ld_storage')
          ->fields((array) $storage)
          ->execute();

        // Adding phones for carrier.
        if (!empty($storage_id) && !empty($storage_phones)) {
          $insert = db_insert('ld_storage_phones')
            ->fields(array('storage_id', 'phone'));
          foreach ($storage_phones as $phone) {
            if (!empty($phone)) {
              $insert->values(array('storage_id' => $storage_id, 'phone' => $phone));
            }
          }
          $insert->execute();
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Create Storage Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('storage', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $storage_id;
    }
  }

  public function retrieve() {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      $result = db_select('ld_storage', 'lds')
        ->fields('lds')
        ->condition('storage_id', $this->storageId)
        ->execute()
        ->fetchAssoc(PDO::FETCH_ASSOC);

      $phones = db_select('ld_storage_phones', 'lds')
        ->fields('lds', array('phone'))
        ->condition('storage_id', $this->storageId)
        ->execute();
      foreach ($phones as $val) {
        $result['phones'][] = $val->phone;
      }
    }
    catch (\Throwable $e) {
      $message = "Get Storage Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Storage', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  public function update($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      $storage = $this->serializer->deserialize(json_encode($data), Storage::class, 'json');

      // Unset created time. We no need to update it.
      unset($storage->created);

      // Adding phones to separate variable.
      $storage_phones = $storage->phones;
      if (empty($storage_phones)) {
        throw new \Exception("Phones is empty");
      }
      else {

        // Set all storages default value to zero.
        // If new default storage is set to 1.
        if (!empty($storage->default_storage)) {
          self::updateStorageFields(array('default_storage' => 0));
        }

        // Unset phones from storage object. We don't have this field in db.
        // If keep it will have error.
        unset($storage->phones);
        db_update('ld_storage')
          ->fields((array) $storage)
          ->condition('storage_id', $this->storageId)
          ->execute();

        // Update phones for carrier.
        if (!empty($storage_phones)) {
          // First we need to delete all rows for carrier phones.
          db_delete('ld_storage_phones')
            ->condition('storage_id', $this->storageId)
            ->execute();
          $insert = db_insert('ld_storage_phones')
            ->fields(array('storage_id', 'phone'));
          foreach ($storage_phones as $phone) {
            // If phones is empty we no need add them to db.
            if (!empty($phone)) {
              $insert->values(array('storage_id' => $this->storageId, 'phone' => $phone));
            }
          }
          $insert->execute();
        }
        $result = TRUE;
      }
    }

    catch (\Throwable $e) {
      $message = "Update Storage Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Storage', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Update chosen storage fields.
   *
   * If $storage_id not empty fill upodate for storege id. Else for all storages.
   *
   * @param array $fields
   *   Array of field name with values.
   * @param bool $storage_id
   *   Id of storage.
   */
  public static function updateStorageFields(array $fields, $storage_id = FALSE) {
    $update = db_update('ld_storage');
    $update->fields($fields);
    if (!empty($storage_id)) {
      $update->condition('storage_id', $storage_id);
    }
    $update->execute();
  }

  public function delete() {
    db_delete('ld_storage')
      ->condition('storage_id', $this->storageId)
      ->execute();

    db_delete('ld_storage_phones')
      ->condition('storage_id', $this->storageId)
      ->execute();
  }

  public function index(int $page = 1, int $page_size = -1, $short = 0, $active = 1, $sorting = array(), $condition = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    try {
      // Get total count of rows.
      $query = db_select('ld_storage', 'lds');
      $query->fields('lds', array('storage_id'));
      if (!empty($active)) {
        $query->condition('lds.active', $active);
      }
      if (!empty($sorting)) {
        $query->orderBy($sorting['orderKey'], $sorting['orderValue']);
      }
      // Filtering.
      if (!empty($condition['filters'])) {
        foreach ($condition['filters'] as $name => $val) {
          if ($val != '') {
            $operator = '=';
            switch ($name) {
              case 'name':
                $val = db_like($val) . '%';
                $operator = 'LIKE';
                break;
            }
            $query->condition($name, $val, $operator);
          }
        }
      }

      $total_count = $query->execute()->rowCount();
      // Calculate number of pages.
      $page_count = ($page_size > 0) ? ceil($total_count / $page_size) : $total_count;
      // Build pager info for front.
      $meta = array(
        'currentPage' => (int) $page,
        'perPage' => (int) $page_size,
        'pageCount' => (int) $page_count,
        'totalCount' => (int) $total_count,
      );
      // Number of row from we start our select.
      $start_from_page = ($page - 1) * $page_size;

      $result['items'] = array();
      $result['_meta'] = $meta;

      $query = db_select('ld_storage', 'lds');
      if (empty($short)) {
        $query->fields('lds');
      }
      else {
        $query->fields('lds', array('storage_id', 'name'));
      }
      if (!empty($active)) {
        $query->condition('lds.active', $active);
      }
      if ($page_size > 0) {
        $query->range($start_from_page, $page_size);
      }
      // Sorting.
      if (!empty($sorting)) {
        $query->orderBy($sorting['orderKey'], $sorting['orderValue']);
      }
      // Filtering.
      if (!empty($condition['filters'])) {
        foreach ($condition['filters'] as $name => $val) {
          if ($val != '') {
            $operator = '=';
            switch ($name) {
              case 'name':
                $val = db_like($val) . '%';
                $operator = 'LIKE';
                break;
            }
            $query->condition($name, $val, $operator);
          }
        }
      }

      $storages = $query->execute()->fetchAllAssoc('storage_id');
      if (empty($short)) {
        if (!empty($storages)) {
          foreach ($storages as $storage_id => $val) {
            $phones_st = array();
            $phones = db_select('ld_storage_phones', 'lds')
              ->fields('lds', array('phone'))
              ->condition('storage_id', $storage_id)
              ->execute();
            foreach ($phones as $val_p) {
              $phones_st[] = $val_p->phone;
            }
            $val->phones = $phones_st;
            $result['items'][$storage_id] = (array) $val;
          }
        }
        $result['items'] = array_values($result['items']);
      }
      else {
        $result['items'] = array_values($storages);
      }
    }
    catch (\Throwable $e) {
      $message = "Update Carrier Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('carrier', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Set storage to active or not active.
   *
   * @param int $val
   *   Value 1 or 0.
   *
   * @return \DatabaseStatementInterface
   */
  public function updateStorageToActiveOrNot(int $val) {
    $updated = db_update('ld_storage')
      ->fields(array('active' => $val))
      ->condition('storage_id', $this->storageId)
      ->execute();
    return $updated;
  }

  public static function getStorageNameById($id) {
    $result = '';
    if (!empty($id)) {
      $result = db_select('ld_storage', 'lds')
        ->fields('lds', array('name'))
        ->condition('storage_id', $id)
        ->execute()
        ->fetchField();
    }
    return $result;
  }

}
