<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class TripClosing.
 *
 * @package Drupal\move_long_distance\Entity
 */
class TripClosing {

  /**
   * @var int
   */
  public $job_id = 0;

  /**
   * @var int
   */
  public $ld_trip_id = 0;

  /**
   * @var string
   */
  public $owner = "";

  /**
   * @var float
   */
  public $tp_collected = 0;

  /**
   * @var float
   */
  public $received = 0;
  /**
   * @var float
   */
  public $paid = 0;

  /**
   * @var float
   */
  public $to_receive = 0;

  /**
   * @var float
   */
  public $to_pay = 0;
  /**
   * @var int
   */
  public $volume_cf = 0;
  /**
   * @var float
   */
  public $job_cost = 0;

  /**
   * @var float
   */
  public $job_total = 0;

  /**
   * @var float
   */
  public $balance = 0;

  /**
   * @var float
   */
  public $rate_per_cf = 0;

  /**
   * @var float
   */
  public $services_total = 0;

  /**
   * @var float
   */
  public $order_balance = 0;

  // Getters.
  public function getJobId() : float {
    return (float) $this->job_id;
  }

  public function getLdTripId() : float {
    return (float) $this->ld_trip_id;
  }

  public function getOwner() : string {
    return (string) $this->owner;
  }

  public function getTpCollected() : float {
    return (float) $this->tp_collected;
  }

  public function getToReceive() : float {
    return (float) floatval($this->to_receive);
  }

  public function getToPay() : float {
    return (float) $this->to_pay;
  }

  public function getVolumeCf() : float {
    return (float) $this->volume_cf;
  }

  public function getJobCost() : float {
    return (float) $this->job_cost;
  }

  public function getJobTotal() : float {
    return (float) $this->job_total;
  }

  public function getBalance() : float  {
    return (float) $this->balance;
  }

  public function getRatePerCf() : float  {
    return (float) $this->rate_per_cf;
  }

  public function getServicesTotal() : float  {
    return (float) $this->services_total;
  }

  public function getOrderBalance() : float  {
    return (float) $this->order_balance;
  }
  public function getReceived() : float  {
    return (float) $this->received;
  }

  public function getPaid() : float  {
    return (float) $this->paid;
  }

  // Setters.
  public function setJobId($job_id) {
    $this->job_id = (float) $job_id;
  }

  public function setLdTripId($ld_trip_id) {
    $this->ld_trip_id = (float) $ld_trip_id;
  }

  public function setOwner($owner) {
    $this->owner = (string) $owner;
  }

  public function setTpCollected($tp_collected) {
    $this->tp_collected = (float) $tp_collected;
  }

  public function setToReceive($to_receive) {
    $this->to_receive = (float) $to_receive;
  }

  public function setToPay($to_pay) {
    $this->to_pay = (float) $to_pay;
  }

  public function setVolumeCf($volume_cf){
    $this->volume_cf = (float) $volume_cf;
  }

  public function setJobCost($job_cost) {
    $this->job_cost = (float) $job_cost;
  }

  public function setJobTotal($job_total) {
    $this->job_total = (float) $job_total;
  }

  public function setBalance($balance) {
    $this->balance = (float) $balance;
  }
  public function setRatePerCf($rate_per_cf) {
    $this->rate_per_cf = (float) $rate_per_cf;
  }
  public function setServicesTotal($services_total) {
    $this->services_total = (float) $services_total;
  }
  public function setOrderBalance($order_balance) {
    $this->order_balance = (float) $order_balance;
  }
  public function setReceived($received){
    $this->received = (float) $received;
  }

  public function setPaid($paid){
    $this->paid = (float) $paid;
  }
}
