<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class RequestAddress.
 *
 * @package Drupal\move_long_distance\Entity
 */
class RequestAddress {

  /**
   * @var string
   */
  public $zip = "";

  /**
   * @var string
   */
  public $city = "";

  /**
   * @var string
   */
  public $state = '';

  // Getters.
  public function getZip() {
    return $this->zip;
  }

  public function getCity() {
    return $this->city;
  }

  public function getState() {
    return $this->state;
  }

  // Setters.
  public function setZip($zip) {
    $this->zip = $zip;
  }

  public function setCity($city) {
    $this->city = $city;
  }

  public function setState($state) {
    $this->state = $state;
  }

}