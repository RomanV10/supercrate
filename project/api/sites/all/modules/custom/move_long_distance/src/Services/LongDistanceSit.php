<?php

namespace Drupal\move_long_distance\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\TripJobType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Drupal\move_long_distance\Entity\RequestSit;
use Drupal\move_new_log\Services\Log;

use PDO;
/**
 * Class Sit.
 *
 * @package Drupal\move_long_distance\Services
 */
class LongDistanceSit extends BaseService {
  private $ldNid = NULL;
  private $entityType = 0;
  private $encoders;
  private $normalizers;
  private $serializer;

  const MOVE_IN_STATUS = 1;
  const MOVE_OUT_STATUS = 2;

  public function __construct($ld_nid = NULL, $entity_type = 0) {
    $this->ldNid = $ld_nid;
    $this->entityType = $entity_type;
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);
  }

  public function setId($ld_nid) {
    $this->ldNid = $ld_nid;
  }

  public function create($data = array()) {}

    public function retrieve() {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {

      $query = db_select('ld_request_sit', 'ldrs');
      $query->leftJoin('ld_storage', 'lds', 'ldrs.storage_id = lds.storage_id');
      $query->leftJoin('field_data_field_user_first_name', 'first_name', 'ldrs.foreman = first_name.entity_id');
      $query->leftJoin('field_data_field_user_last_name', 'last_name', 'ldrs.foreman = last_name.entity_id');
      $query->fields('ldrs');
      $query->addField('first_name', 'field_user_first_name_value', 'first_name');
      $query->addField('last_name', 'field_user_last_name_value', 'last_name');
      $query->addField('lds', 'name', 'storage_name');
      $query->condition('ldrs.ld_nid', $this->ldNid)
        ->condition('ldrs.entity_type', $this->entityType);
      $temp_result = $query->execute()->fetchAll();
      foreach ($temp_result as $row) {
        $row->lots_info = !empty($row->lots_info) ? drupal_json_decode($row->lots_info) : FALSE;
        $row->lot_number = !empty($row->lot_number) ? drupal_json_decode($row->lot_number) : FALSE;
        $result = (array) $row;
      }
    }
    catch (\Throwable $e) {
      $message = "Get sit Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('sit', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      $result = !empty($result) ? (array) $result : $result;
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  public function update($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $sit = FALSE;
    $log_text = array();
    try {
      $title = 'SIT was added';
      $old_sit = $this->retrieve();

      $data['ld_nid'] = $this->ldNid;
      $data['entity_type'] = $this->entityType;

      $sit = (array) $this->serializer->deserialize(json_encode($data), RequestSit::class, 'json');
      $in_sit = self::inSit($this->ldNid, $this->entityType);
      if (!$in_sit) {
        //$sit->date = time();
      }
      else {
        $title = 'SIT was updated';
        $log_text = self::diffOnUpdate($old_sit, $sit);
      }
      db_merge('ld_request_sit')
        ->key(array('ld_nid' => $this->ldNid, 'entity_type' => $this->entityType))
        ->fields($sit)
        ->execute();

      $sit['lots_info'] = !empty($sit['lots_info']) ? drupal_json_decode($sit['lots_info']) : FALSE;
      $sit['lot_number'] = !empty($sit['lot_number']) ? drupal_json_decode($sit['lot_number']) : FALSE;
    }
    catch (\Throwable $e) {
      $message = "Update SIT Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('LD SIT', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      // TODO add method for this.
      if (!empty($log_text)) {
        global $user;
        $user_name = isset($user->name) ? $user->name : 'Anonymous';
        $log_data[] = array(
          'details' => array([
            'event_type' => 'SIT',
            'activity' => $user_name,
            'title' => ucfirst(preg_replace('/_/', ' ', $title)),
            'text' => $log_text,
            'date' => time(),
          ],
          ),
          'source' => $user_name,
        );
        $trip_id = (new LongDistanceJob($this->ldNid))->getJobTripId();

        // Write logs for Trip.
        if (!empty($trip_id)) {
          $log = new Log($trip_id, EntityTypes::LDTRIP);
          $log->create($log_data);
        }

        $log = new Log($this->ldNid, EntityTypes::LDSIT);
        $log->create($log_data);
      }


      return $error_flag ? services_error($error_message, 406) : $sit;

    }
  }

  public function delete() {}

  public function index() {}

  public static function inSit($nid, $entity_type) {
    $result = FALSE;
    if (!empty($nid)) {
      $result = db_select('ld_request_sit', 'ldrs')
        ->fields('ldrs', array('ld_nid'))
        ->condition('ld_nid', $nid)
        ->condition('entity_type', $entity_type)
        ->execute()
        ->fetchField();
    }
    return $result;
  }

  public static function diffOnUpdate($old, $new) {
    $new = (array) $new;
    $text = array();
    $excluded_fields = array(
      'lots_info',
      'lot_number',
      'last_name',
      'first_name',
      'storage_name',
      'id',
    );
    $sit_status = array(1 => 'Move in', 2 => 'Move out');
    foreach ($old as $field_name => $val) {
      // Work only with no excluded fields.
      if (!in_array($field_name, $excluded_fields)) {
        if (($old[$field_name] != $new[$field_name]) && !empty($new[$field_name])) {
          $name = ucfirst(str_replace('_', ' ', $field_name));
          $new_val = $new[$field_name];
          $old_val = $old[$field_name];
          switch ($field_name) {
            case 'status':
              $new_val = !empty($sit_status[$new[$field_name]]) ? $sit_status[$new[$field_name]] : NULL;
              $old_val = !empty($sit_status[$old[$field_name]]) ? $sit_status[$old[$field_name]] : NULL;
              break;

            case 'storage_id':
              $name = 'Storage';
              $new_val = !empty($new[$field_name]) ? LongDistanceStorage::getStorageNameById($new[$field_name]) : NULL;
              $old_val = !empty($old[$field_name]) ? LongDistanceStorage::getStorageNameById($old[$field_name]) : NULL;
              break;

            case 'foreman':
              $new_val = !empty($new[$field_name]) ? self::getUserFullNameById($new[$field_name]) : NULL;
              $old_val = !empty($old[$field_name]) ? self::getUserFullNameById($old[$field_name]) : NULL;
              break;

            case 'date':
              $new_val = !empty($new[$field_name]) ? date('M j, Y', $new[$field_name]) : NULL;
              $old_val = !empty($old[$field_name]) ? date('M j, Y', $old[$field_name]) : NULL;
              break;
          }
          if (empty($old[$field_name])) {
            $text[]['simpleText'] = $name . ' "' . $new_val . '" was added';
          }
          elseif (empty($new[$field_name])) {
            $text[]['simpleText'] = $name . ' "' . $old_val . '" was removed';
          }
          else {
            $text[]['simpleText'] = $name . ' was changed from "' . $old_val . '" to "' . $new_val . '"';
          }
        }
      }
    }
    return $text;
  }

  /**
   * Get full user name by his id.
   *
   * @param int $uid
   *   User id.
   *
   * @return null|string
   *  NULL or User name.
   */
  public static function getUserFullNameById(int $uid) {
    $full_name = NULL;
    $query = db_select('field_data_field_user_first_name', 'first_name');
    $query->leftJoin('field_data_field_user_last_name', 'last_name', 'first_name.entity_id = last_name.entity_id');
    $query->fields('first_name', array('field_user_first_name_value'))
      ->fields('last_name', array('field_user_last_name_value'))
      ->condition('first_name.entity_id', $uid);
    $full_name_result = $query->execute()->fetchAssoc(PDO::FETCH_ASSOC);
    if (!empty($full_name_result)) {
      $full_name = implode(' ', $full_name_result);
    }
    return $full_name;
  }

}
