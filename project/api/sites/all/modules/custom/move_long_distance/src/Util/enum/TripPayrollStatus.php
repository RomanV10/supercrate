<?php

namespace Drupal\move_long_distance\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TripPayrollStatus.
 *
 * @package Drupal\move_long_distance\Util
 */
class TripPayrollStatus extends BaseEnum {

  const DRAFT = 0;
  const SUBMIT = 1;

}
