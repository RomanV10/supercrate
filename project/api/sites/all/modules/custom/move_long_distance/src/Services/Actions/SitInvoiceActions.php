<?php

namespace Drupal\move_long_distance\Services\Actions;

use Drupal\move_long_distance\Services\LongDistanceCarrier;
use Drupal\move_invoice\Services\Invoice;
use Drupal\move_long_distance\Services\LongDistanceJob;
use Drupal\move_long_distance\Services\LongDistanceTPDelivery;
use Drupal\move_services_new\Util\enum\InvoiceFlags;
use Drupal\move_services_new\Util\enum\TripPaymentType;
use Drupal\move_template_builder\Services\EmailTemplate;

class SitInvoiceActions extends SitActions {
  public $invoiceId = 0;
  public $receipt = [];
  public $invoice = [];
  public $moveInvoice = [];
  public $receiptsTotal = 0.0;
  public $receiptId = 0;

  /**
   * SitInvoiceActions constructor.
   *
   * @param int $invoice_id
   *   Invoice id.
   * @param array $receipt
   *   Receipt data.
   */
  public function __construct(int $invoice_id, array $receipt = []) {
    $this->invoiceId = $invoice_id;
    $this->moveInvoice = new Invoice($this->invoiceId);
    $this->receipt = $receipt;
    $this->receiptId = $receipt['id'] ?? $receipt['receipt_id'] ?? 0;
  }

  /**
   * Rebuild invoice info. Status, paid types.
   */
  public function rebuildInvoiceInfo() : void {
    $this->getInvoice();
    $invoice_flag = InvoiceFlags::PAID;

    $this->receiptsTotal = SitReceiptActions::calculateTotalForJobsReceipts($this->receipt['jobs'], [TripPaymentType::RECEIVED, TripPaymentType::PAID]);
    $this->syncInvoiceOverpaidPartiallyPaid();

    if ($this->receiptsTotal <= 0) {
      $invoice_flag = InvoiceFlags::SENT;
    }

    $this->invoice['data']['flag'] = $invoice_flag;
    $this->moveInvoice->update($this->invoice['data']);
  }

  /**
   * Make link with receipt and invoice.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function linkInvoiceWithReceipt() {
    db_merge('ld_invoices_receipts')
      ->key(array('invoice_id' => $this->invoiceId, 'receipt_id' => $this->receiptId))
      ->fields(array('invoice_id' => $this->invoiceId, 'receipt_id' => $this->receiptId))
      ->execute();
  }

  /**
   * Get invoice info.
   *
   * @return array|mixed
   *   Invoice array.
   */
  public function getInvoice($job_id = FALSE) {
    $al_instance = new Invoice($this->invoiceId);
    $result = $al_instance->retrieve();

    // Remove charges. We need to get items form our db.
    if (isset($result['data']['charges'])) {
      unset($result['data']['charges']);
    }

    $query = db_select('ld_invoices_details', 'lid');
    $query->fields('lid');
    $query->condition('invoice_id', $this->invoiceId);

    if ($job_id) {
      $query->condition('job_id', $job_id);
    }

    $invoice = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    if (!empty($invoice)) {
      $result['receipts'] = static::getInvoiceReceipts($this->invoiceId);
    }
    $result['items'] = $invoice;
    $this->invoice = $result;
    return $this->invoice;
  }

  /**
   * Update overpaid or partially paid values.
   */
  private function syncInvoiceOverpaidPartiallyPaid() {
    $totals_diff = $this->calculateInvoiceTotalVSReceipts();
    $this->updateInvoiceOverPaid($totals_diff);
    $this->updateInvoicePartiallyPaid($totals_diff);
    $this->cleanOverpaidPartiallyPaid($totals_diff);
  }

  /**
   * Set invoice overpaid.
   *
   * @param float $total_diff
   *   Diff amount.
   */
  private function updateInvoiceOverPaid(float $total_diff) {
    if ($total_diff > 0) {
      $this->invoice['data']['partially_paid'] = $this->receiptsTotal;
      unset($this->invoice['data']['overpay']);
    }
  }

  /**
   * Set invoice partially paid.
   *
   * @param float $total_diff
   *   Diff amount.
   */
  private function updateInvoicePartiallyPaid(float $total_diff) {
    if ($total_diff < 0) {
      $this->invoice['data']['overpay'] = abs($total_diff);
      unset($this->invoice['data']['partially_paid']);
    }
  }

  /**
   * Remove overpaid and partially paid.
   *
   * @param float $total_diff
   *   Diff amount.
   */
  private function cleanOverpaidPartiallyPaid(float $total_diff) {
    if ($total_diff == 0) {
      unset($this->invoice['data']['overpay']);
      unset($this->invoice['data']['partially_paid']);
    }
  }

  /**
   * Calculate diff between invoice total and receipts total.
   *
   * @return float
   *   Diff amount.
   */
  private function calculateInvoiceTotalVSReceipts() {
    $total_invoice = $this->invoice['data']['amount'];
    return $total_invoice - $this->receiptsTotal;
  }

  /**
   * Set invoice receipt.
   *
   * @param array $receipt
   *   Receipt.
   */
  public function setReceipt(array $receipt) {
    $this->receipt = $receipt;
  }

  /**
   * Get receipts for invoice.
   *
   * @param int $invoice_id
   *   Invoice id.
   *
   * @return array
   *   Array with receipts.
   */
  public static function getInvoiceReceipts($invoice_id) {
    $receipts = db_select('ld_invoices_receipts', 'lir')
      ->fields('lir', array('receipt_id'))
      ->condition('invoice_id', $invoice_id)
      ->execute()
      ->fetchCol();
    if (!empty($receipts)) {
      foreach ($receipts as $key => $receipt_id) {
        $receipts[$key] = SitReceiptActions::getReceipt($receipt_id);
      }
    }
    return $receipts;
  }

  /**
   * @param $auto_send
   * @return array|bool
   */
  public function sendInvoiceEmail($auto_send) {
    if ($auto_send) {
      $data = static::getInvoiceTemplate('agent_folio_invoice_template');
      $send_email = EmailTemplate::sendEmailTemplateInvoice($this->invoiceId, $data);
      if ($send_email) {
        return $this->invoiceId;
      }
      return FALSE;
    }
  }


  /**
   * @param string $invoice_template
   * @return array
   */
  public static function getInvoiceTemplate(string $invoice_template) {
    $data = array();
    $query_template = db_select('move_template_builder', 'fuk')
      ->fields('fuk')
      ->condition('fuk.key_name', '%' . db_like($invoice_template) . '%', 'LIKE')
      ->execute()
      ->fetchAll();

    if (isset($query_template) && $query_template) {
      $query_template = reset($query_template);
      $subject = json_decode($query_template->data);

      $data = array(
        [
          'template' => $query_template->template,
          'subject'   => $subject->subject,
        ],
      );
    }

    return $data;
  }

  /**
   * @param $invoice_id
   */
  public static function changeInvoiceToStatus($invoice_id, $flag = FALSE, $entity_type) {
    $query = db_select('move_invoice', 'mi')
      ->fields('mi')
      ->condition('entity_type', $entity_type)
      ->condition('id', $invoice_id)
      ->execute()
      ->fetchObject();

    $unserialize = unserialize($query->data);
    if ($unserialize['flag'] == InvoiceFlags::DRAFT) {
      $unserialize['flag'] = InvoiceFlags::SENT;
    }
    if ($flag !== FALSE) {
      $unserialize['flag'] = $flag;
    }
    Invoice::changeInvoiceFlag($invoice_id, $unserialize);
  }

  /**
   * Delete invoice
   *
   * @return bool
   *   TRUE if was delete.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function deleteInvoice() {
    $log_text = array();
    $this->getInvoice();
    $sitReceipt = new SitReceiptActions();

    if (!empty($this->invoice['receipts'])) {
      foreach ($this->invoice['receipts'] as $receipt) {
        $log_text[]['simpleText'] = 'Receipt id is #' . $receipt['id'];
        $sitReceipt->setReceiptId($receipt['id']);
        $sitReceipt->deleteReceipt();
      }
    }

    $this->helperDeleteInvoice();
    $log_title = 'Invoice #' . $this->invoiceId . ' was deleted.';
    $carrier_inst = new LongDistanceCarrier($this->invoice['entity_id']);
    $carrier_inst->setLogs($log_title, $log_text);
    return TRUE;
  }

  /**
   * Get info for jobs.
   *
   * @param mixed $entity_id
   *   Entity id or array with entity ids.
   * @param  $paid_type
   *  Type of pay.
   *
   * @return array|mixed
   *   Array with info or error.
   *
   * @throws \ServicesException
   */
  public static function getInfoForInvoiceOrPay($entity_id, $paid_type = FALSE) {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    try {
      $result = array();
      $fields_to_use = array(
        'volume_cf',
        'job_cost',
        'services_total',
        'job_total',
        'tp_collected',
        'received',
        'paid',
        'to_receive',
        'to_pay',
        'order_balance',
        'balance',
      );
      if (!is_array($entity_id)) {
        $entity_id = array($entity_id);
      }

      $address_fields = array(
        'zip',
        'city',
        'state',
      );
      $address_tables = array(
        'from_a' => 'ld_request_address_from',
        'to_a' => 'ld_request_address_to',
      );


      // Get all carriers for job.
      $carriers = LongDistanceJob::getCarrierByJobsId($entity_id);

      // For job can be only one carrier.
      if (empty($carriers['carrier_info']) && count($carriers) > 1) {
        throw new \Exception("Job can't be belongs more the one carrier", 300);
      }

      if (!empty($carriers['carrier_info'])) {
        $carrier_info = $carriers['carrier_info'];
        $tp_delivery_customer_jobs = LongDistanceTPDelivery::getDeliveryJobsForCompany($carrier_info['ld_carrier_id']);
        $tp_delivery_jobs = array_intersect($entity_id, $tp_delivery_customer_jobs);
        $entity_id = array_diff($entity_id, $tp_delivery_customer_jobs);
      }

      // TODO refactor this later. Make method for this.
      if (!empty($entity_id)) {
        // Build query to get all total for this jobs.
        $query = db_select('ld_trip_closing', 'ltc');
        foreach ($fields_to_use as $field_name) {
          $query->addExpression('SUM({' . $field_name . '})', $field_name);
        }
        $query->condition('job_id', $entity_id, 'IN');
        $total = $query->execute()->fetchAssoc();

        // Calculate total numbers.
        $total = LongDistanceJob::closingCalculation($total, TRUE);
      }


      // Total for tp delivery jobs.
      if (!empty($tp_delivery_jobs)) {
        $entity_id = array_merge($tp_delivery_jobs, $entity_id);
        $query = db_select('ld_trip_closing', 'ltc');
        foreach ($fields_to_use as $field_name) {
          $query->addExpression('SUM({' . $field_name . '})', $field_name);
        }
        $query->condition('job_id', $tp_delivery_jobs, 'IN');
        $tp_total = $query->execute()->fetchAssoc();

        // Calculate total numbers.
        $tp_delivery_total = LongDistanceJob::closingCalculation($tp_total, TRUE, 'tp_delivery');
        if (!empty($total)) {
          foreach ($total as $field_name => $val) {
            $total[$field_name] += $tp_delivery_total[$field_name];
          }
        }
        else {
          $total = $tp_delivery_total;
        }
      }


      // Build main query to get all job items from closing.
      $main_query = db_select('ld_trip_closing', 'ltc');
      $main_query->leftJoin('ld_trip_carrier', 'ltcar', 'ltc.ld_trip_id = ltcar.ld_trip_id');
      $main_query->leftJoin('ld_tp_delivery_job', 'ltj', 'ltc.job_id = ltj.job_id');
      $main_query->leftJoin('ld_tp_delivery_details', 'ltdd', 'ltj.ld_tp_delivery_id = ltdd.ld_tp_delivery_id');
      $main_query->leftJoin('ld_carrier', 'lc', 'ltdd.delivery_for_company = lc.ld_carrier_id');
      foreach ($address_tables as $table_alias => $table_name) {
        $main_query->leftJoin($table_name, $table_alias, 'ltc.job_id = ' . $table_alias . '.ld_nid');
        foreach ($address_fields as $field_name) {
          $main_query->addField($table_alias, $field_name, $table_alias . '_' . $field_name);
        }
      }
      $main_query->fields('lc', array('name'));
      $main_query->fields('ltc');
      $main_query->fields('ltdd');
      $main_query->fields('ltj', array('delivery_job_id'));
      $main_query->fields('ltcar', array('carrier_id', 'driver_name', 'driver_phone'));
      $main_query->condition('ltc.job_id', $entity_id, 'IN');
      $main_query->isNull('ltc.is_total');
      $main_query_result = $main_query->execute()->fetchAll();

      // Send values back for action.
      if (!empty($main_query_result) && !empty($total) && !empty($carrier_info)) {
        foreach ($main_query_result as $key => $val) {
          $main_query_result[$key]->carrier_id = empty($val->carrier_id) ? $val->delivery_for_company : $val->carrier_id;
          $main_query_result[$key]->to_a_zip = empty($main_query_result[$key]->to_a_zip) ? $main_query_result[$key]->zip : $main_query_result[$key]->to_a_zip;
          $main_query_result[$key]->to_a_city = empty($main_query_result[$key]->to_a_city) ? $main_query_result[$key]->city : $main_query_result[$key]->to_a_city;
          $main_query_result[$key]->to_a_state = empty($main_query_result[$key]->to_a_state) ? $main_query_result[$key]->state : $main_query_result[$key]->to_a_state;
          $main_query_result[$key]->from_a_zip = empty($main_query_result[$key]->from_a_zip) ? $main_query_result[$key]->name : $main_query_result[$key]->from_a_zip;

        }

        switch ($paid_type) {
          // Simple payment for receipt.
          case  TripPaymentType::PAID:
            $result['items'] = $main_query_result;
            $result['total'] = $total;
            $result['carrier_info'] = $carrier_info;
            break;

          // Values for invoice.
          case TripPaymentType::RECEIVED:
            $result['carrier_info'] = $carrier_info;
            $result['items'] = $main_query_result;
            $result['total-closing'] = $total;
            $result['totalInvoice'] = (float) $total['balance'];
            break;

          // Values to show calculation for this job.
          case TripPaymentType::TP_COLLECTED:
          default:
            $result['items'] = $main_query_result;
            $result['total'] = $total;
            break;
        }

      }
    }
    catch (\Throwable $e) {
      $message = "Save job to trip Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('trip job', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Build information for invoice and send it.
   *
   * @param array $job_ids
   *   Array with jobs ids.
   * @param int $invoice_id
   *   Invoice id.
   * @param float $total
   *   Invoice total.
   *
   * @return int|mixed
   *   Invoice id or error.
   *
   * @throws \ServicesException
   */
  public static function processInvoice(array $job_ids, int $invoice_id, float $total) {
    $error_flag = FALSE;
    $error_message = "";
    try {
      $data = static::getInfoForInvoiceOrPay($job_ids, TripPaymentType::RECEIVED);
      static::saveInvoiceDetails($invoice_id, $total, $data['items']);
    }
    catch (\Throwable $e) {
      $message = "Make payment Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('LD payment', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $invoice_id;
    }
  }

  /**
   * Save details for invoice in long distance tables.
   *
   * @param int $invoice_id
   *   Id of invoice.
   * @param float $amount
   *   Total invoice amount.
   * @param array $data
   *   Invoice items for save.
   */
  public static function saveInvoiceDetails(int $invoice_id, float $amount, array $data) : void {
    db_insert('ld_invoices')
      ->fields(array(
          'invoice_id' => $invoice_id,
          'amount' => $amount,
          'created' => time(),
        )
      )
      ->execute();
    $log_text = array();
    $carrier_id = NULL;
    if (!empty($data)) {
      foreach ($data as $item) {
        $carrier_id = $item->carrier_id;
        // Array for logs.
        $data_for_log['ld_trip_id'][$item->ld_trip_id][] = $item;
        db_insert('ld_invoices_details')
          ->fields(array(
            'invoice_id' => $invoice_id,
            'amount' => $item->to_receive,
            'job_id' => $item->job_id,
            'trip_id' => $item->ld_trip_id,
            'carrier_id' => $carrier_id,
          ))
          ->execute();
        $log_text[]['simpleText'] = 'Trip  #' . $item->ld_trip_id;
        $log_text[]['simpleText'] = 'Job #' . $item->job_id . ' was added to invoice #' . $invoice_id . '. Job amount was "' . $item->balance . '"';
      }

      // Information for adding logs.
      $carrier_inst = new LongDistanceCarrier($carrier_id);
      $log_title = 'Invoice #' . $invoice_id . ' was created';
      $carrier_inst->setLogs($log_title, $log_text, 'Invoice');
    }
  }

  /**
   * Delete invoice from db.
   */
  public function helperDeleteInvoice() {
    $query = 'DELETE lid, ldid, mi FROM {ld_invoices} lid 
              INNER JOIN {ld_invoices_details} ldid ON lid.invoice_id = ldid.invoice_id
              INNER JOIN {move_invoice} mi ON ldid.invoice_id = mi.id 
              WHERE lid.invoice_id = :invoice_id';
    db_query($query, array(':invoice_id' => $this->invoiceId));
  }


}
