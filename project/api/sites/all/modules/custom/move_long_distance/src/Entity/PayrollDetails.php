<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class PayrollDetails.
 *
 * @package Drupal\move_long_distance\Entity
 */
class PayrollDetails {

  /**
   * @var int
   */
  private $pid = 0;

  /**
   * @var int
   */
  private $ld_trip_id = 0;

  /**
   * @var float
   */
  private $total_payroll = 0;

  /**
   * @var int
   */
  private $status = 0;

  /**
   * @var int
   */
  private $created = 0;

  /**
   * @var int
   */
  private $date_start = 0;

  /**
   * @var int
   */
  private $date_end = 0;

  // ############# Setters #############.
  /**
   * @param int $pid
   */
  public function setPid(int $pid) {
    $this->pid = $pid;
  }

  /**
   * @param int $date_end
   */
  public function setDateEnd(int $date_end) {
    $this->date_end = $date_end;
  }

  /**
   * @param int $created
   */
  public function setCreated(int $created) {
    $this->created = $created;
  }

  /**
   * @param int $date_start
   */
  public function setDateStart(int $date_start) {
    $this->date_start = $date_start;
  }

  /**
   * @param int $status
   */
  public function setStatus(int $status) {
    $this->status = $status;
  }

  /**
   * @param float $total_payroll
   */
  public function setTotalPayroll(float $total_payroll) {
    $this->total_payroll = $total_payroll;
  }

  /**
   * @param int $ld_trip_id
   */
  public function setLdTripId(int $ld_trip_id) {
    $this->ld_trip_id = $ld_trip_id;
  }

  // ############# Getters #############.
  /**
   * @return int
   */
  public function getPid(): int {
    return (int) $this->pid;
  }

  /**
   * @return int
   */
  public function getLdTripId(): int {
    return (int) $this->ld_trip_id;
  }

  /**
   * @return float
   */
  public function getTotalPayroll(): float {
    return (float) $this->total_payroll;
  }

  /**
   * @return int
   */
  public function getStatus(): int {
    return (int) $this->status;
  }

  /**
   * @return int
   */
  public function getCreated(): int {
    return (int) $this->created;
  }

  /**
   * @return int
   */
  public function getDateStart(): int {
    return (int) $this->date_start;
  }

  /**
   * @return int
   */
  public function getDateEnd(): int {
    return (int) $this->date_end;
  }

}
