<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class PayrollExpensesReceived.
 *
 * @package Drupal\move_long_distance\Entity
 */
class PayrollExpensesReceived {

  /**
   * @var int
   */
  public $ld_trip_id = 0;

  /**
   * @var int
   */
  public $id = 0;

  /**
   * @var int
   */
  public $uid = 0;

  /**
   * @var float
   */
  public $amount = 0;

  /**
   * @var int
   */
  public $date = 0;

  /**
   * @var string
   */
  public $description = '';

  // Getters.
  public function getLdTripId() {
    return (int) $this->ld_trip_id;
  }
  public function getId() {
    return (int) $this->id;
  }
  public function getUid() {
    return (int) $this->uid;
  }

  public function getAmount() {
    return (float) $this->amount;
  }

  public function getDate() {
    return (int) $this->date;
  }

  public function getDescription() {
    return (string) $this->description;
  }

  // Setters.
  public function setLdTripId($ld_trip_id) {
    $this->ld_trip_id = (int) $ld_trip_id;
  }

  public function setUid($uid) {
    $this->uid = (int) $uid;
  }

  public function setId($id) {
    $this->id = (int) $id;
  }

  public function setAmount($amount) {
    $this->amount = (float) $amount;
  }

  public function setDate($date) {
    $this->date = (int) $date;
  }

  public function setDescription($description) {
    $this->description = (string) $description;
  }
}
