<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class HelperPayroll.
 *
 * @package Drupal\move_long_distance\Entity
 */
class HelperPayroll {

  /**
   * @var int
   */
  public $ld_trip_id = 0;

  /**
   * @var int
   */
  public $uid = 0;

  /**
   * @var float
   */
  public $daily_amount = 0;

  /**
   * @var int
   */
  public $total_days = 0;

  /**
   * @var float
   */
  public $total_daily = 0;

  /**
   * @var float
   */
  public $other = 0;

  /**
   * @var float
   */
  public $total_payroll = 0;


  // Getters.
  public function getLdTripId() {
    return (int) $this->ld_trip_id;
  }

  public function getUid() {
    return (int) $this->uid;
  }

  public function getDailyAmount() {
    return (float) $this->daily_amount;
  }
  
  public function getTotalDays() {
    return (int) $this->total_days;
  }
  
  public function getTotalDaily() {
    return (float) $this->total_daily;
  }
  
  public function getOther() {
    return (float) $this->other;
  }
  
  public function getTotalPayroll() {
    return (float) $this->total_payroll;
  }  
  

  // Setters.
  public function setLdTripId($ld_trip_id) {
    $this->ld_trip_id = (int) $ld_trip_id;
  }

  public function setUid($uid) {
    $this->uid = (int) $uid;
  }

  public function setDailyAmount($daily_amount) {
    $this->daily_amount = (float) $daily_amount;
  }

  public function setTotalDays($total_days) {
    $this->total_days = (int) $total_days;
  }

  public function setTotalDaily($total_daily) {
    $this->total_daily = (float) $total_daily;
  }

  public function setOther($other) {
    $this->other = (float) $other;
  }

  public function setTotalPayroll($total_payroll) {
    $this->total_payroll = (float) $total_payroll;
  }

}
