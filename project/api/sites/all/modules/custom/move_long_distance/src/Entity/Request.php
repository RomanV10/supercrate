<?php

namespace Drupal\move_long_distance\Entity;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class Request.
 *
 * @package Drupal\move_long_distance\Entity
 */
class Request {

  /**
   * @var int
   */
  public $ld_nid;


  /**
   * @var RequestAddress
   */
  public $ld_request_address_from;

  /**
   * @var RequestAddress
   */
  public $ld_request_address_to;

  /**
   * @var RequestSit
   */
  public $ld_request_sit;

  /**
   * @var string
   */
  public $address_from = "";
  /**
   * @var string
   */
  public $address_to = "";
  /**
   * @var string
   */
  public $customer_name = "";

  /**
   * @var int
   */
  public $pickup_date;

  /**
   * @var int
   */
  public $delivery_date;

  /**
   * @var int
   */
  public $second_delivery_date;

  /**
   * @var int
   */
  public $created;
  /**
   * @var int
   */
  public $schedule_delivery_date;
  /**
   * @var int
   */
  public $sit;
  /**
   * @var float
   */
  public $distance_to_st;

  /**
   * @var float
   */
  public $distance_from_st;

  /**
   * @var float
   */
  public $distance;

  /**
   * @var int
   */
  public $weight;

  /**
   * @var int
   */
  public $sales_weight;

  /**
   * @var int
   */
  public $ld_status;

  /**
   * @var int
   */
  public $in_trip;

  /**
   * @var int
   */
  public $ready_for_delivery;

  /**
   * @var string
   */
  public $request_flags;

  /**
   * Trip constructor.
   */
  public function __construct() {
    $this->ld_request_address_from = new RequestAddress();
    $this->ld_request_address_to = new RequestAddress();
    $this->ld_request_sit = new RequestSit();
  }

  // Getters.

  public function getLdRequestAddressFrom() : RequestAddress {
    return $this->ld_request_address_from;
  }

  public function getLdRequestAddressTo() : RequestAddress {
    return $this->ld_request_address_to;
  }

  public function getLdRequestSit() : RequestSit {
    return $this->ld_request_sit;
  }

  public function getCustomerName() : string {
    return (string) $this->customer_name;
  }
  public function getAddressFrom() : string {
    return (string) $this->address_from;
  }
  public function getAddressTo() : string {
    return (string) $this->address_to;
  }

  public function getLdNid() {
    return (int) $this->ld_nid;
  }

  public function getPickupDate() {
    return (int) $this->pickup_date;
  }

  public function getCreated() {
    return (int) $this->created;
  }

  public function getDeliveryDate() : int {
    return (int) $this->delivery_date;
  }
  public function getSecondDeliveryDate() : int {
    return (int) $this->second_delivery_date;
  }

  public function getSit() : int {
    return (int) $this->sit;
  }
  public function getDistanceToSt() {
    return (float) $this->distance_to_st;
  }

  public function getDistanceFromSt() {
    return (float) $this->distance_from_st;
  }

  public function getWeight()  {
    return (int) $this->weight;
  }
  public function getSalesWeight()  {
    return (int) $this->sales_weight;
  }
  public function getScheduleDeliveryDate()  {
    return (int) $this->schedule_delivery_date;
  }
  public function getLdStatus()  {
    return (int) $this->ld_status;
  }
  public function getInTrip()  {
    return (int) $this->in_trip;
  }

  public function getReadyForDelivery() : int {
    return (int) $this->ready_for_delivery;
  }
  public function getDistance() {
    return (float) $this->distance;
  }
  public function getRequestFlags() {
    return (string) $this->request_flags;
  }
  // Setters.


  public function setLdRequestAddressFrom($ld_request_address_from) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->ld_request_address_from = (array) $serializer->deserialize(json_encode($ld_request_address_from), RequestAddress::class, 'json');
  }

  public function setLdRequestAddressTo($ld_request_address_to) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->ld_request_address_to = (array) $serializer->deserialize(json_encode($ld_request_address_to), RequestAddress::class, 'json');
  }

  public function setLdRequestSit($ld_request_sit) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->ld_request_sit = (array) $serializer->deserialize(json_encode($ld_request_sit), RequestSit::class, 'json');
  }

  public function setCustomerName($customer_name) {
    $this->customer_name = (string) $customer_name;
  }
  public function setAddressFrom($address_from) {
    $this->address_from = (string) $address_from;
  }

  public function setAddressTo($address_to) {
    $this->address_to = (string) $address_to;
  }

  public function setLdNid(int $ld_nid) {
    $this->ld_nid = (int) $ld_nid;
  }
  public function setSit(int $sit) {
    $this->sit = (int) $sit;
  }
  public function setCreated(int $created) {
    $this->created = (int) $created;
  }
  public function setPickupDate(int $pickup_date) {
    $this->pickup_date = (int) $pickup_date;
  }
  public function setScheduleDeliveryDate(int $schedule_delivery_date) {
    $this->schedule_delivery_date = (int) $schedule_delivery_date;
  }
  public function setDeliveryDate(int $delivery_date) {
    $this->delivery_date = (int) $delivery_date;
  }
  public function setSecondDeliveryDate(int $second_delivery_date) {
    $this->second_delivery_date = (int) $second_delivery_date;
  }
  public function setWeight(int $weight) {
    $this->weight = (int) $weight;
  }
  public function setSalesWeight(int $sales_weight) {
    $this->sales_weight = (int) $sales_weight;
  }

  public function stLdStatus(int $ld_status) {
    $this->ld_status = (int) $ld_status;
  }

  public function stInTrip(int $in_trip) {
    $this->in_trip = (int) $in_trip;
  }

  public function setDistanceToSt(int $distance_to_st) {
    $this->distance_to_st = (int) $distance_to_st;
  }

  public function setDistanceFromSt(int $distance_from_st) {
    $this->distance_from_st = (int) $distance_from_st;
  }

  public function setReadyForDelivery(int $ready_for_delivery) {
    $this->ready_for_delivery = (int) $ready_for_delivery;
  }

  public function setDelivery(float $delivery) {
    $this->distance = (float) $delivery;
  }

  public function setRequestFlags($request_flags) {
    $this->request_flags = (string) $request_flags;
  }
}
