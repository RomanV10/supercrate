<?php

namespace Drupal\move_long_distance\Entity;

/**
 * Class TripCarrier.
 *
 * @package Drupal\move_long_distance\Entity
 */
class Storage {

  /**
   * @var string
   */
  public $name = "";

  /**
   * @var string
   */
  public $notes = "";

  /**
   * @var string
   */
  public $zip_code = '';

  /**
   * @var string
   */
  public $state = '';

  /**
   * @var string
   */
  public $country = '';
  /**
   * @var string
   */
  public $city = '';
  /**
   * @var string
   */
  public $email;

  /**
   * @var string
   */
  public $address = '';


  /**
   * @var integer
   */
  public $created;

  /**
   * @var array
   */
  public $phones = array();

  /**
   * @var string
   */
  public $fax = '';

  /**
   * @var string
   */
  public $default_storage = 0;

  /**
   * @var int
   */
  public $active = 1;

  // Getters.
  public function getName() : string {
    return (string) $this->name;
  }

  public function getActive()  {
    return (int) $this->active;
  }

  public function getZipCode() : string {
    return (string) $this->zip_code;
  }

  public function getState() : string {
    return (string) $this->state;
  }

  public function getCountry() : string {
    return (string) $this->country;
  }

  public function getCity() : string {
    return (string) $this->city;
  }

  public function getEmail() : string {
    return (string) $this->email;
  }

  public function gtAddress() : string {
    return (string) $this->address;
  }


  public function getCreate() : int {
    return (int) $this->created;
  }

  public function getPhones() : array {
    return (array) $this->phones;
  }

  public function getFax() : string {
    return (string) $this->fax;
  }

  public function getNotes() : string {
    return (string) $this->notes;
  }

  public function getDefaultStorage() : int {
    return (string) $this->default_storage;
  }


  // Setters.
  public function setName($name) {
    $this->name = (string) $name;
  }
  public function setActive($active) {
    $this->active = (int) $active;
  }


  public function setZipCode($zip) {
    $this->zip_code = (string) $zip;
  }

  public function setState($state) {
    $this->state = (string)  $state;
  }

  public function setCountry($country) {
    $this->country = (string) $country;
  }

  public function setCity($city) {
    $this->city = (string) $city;
  }

  public function setEmail($email) {
    $this->email = (string) $email;
  }

  public function setAddress($address){
    $this->address = (string) $address;
  }
  public function setNotes($notes){
    $this->notes = (string) $notes;
  }
  public function setDefaultStorage($default_storage){
    $this->default_storage = (int) $default_storage;
  }

  public function setPhones($phones) {
    $this->phones = (array) $phones;
  }

  public function setFax($fax){
    $this->fax = (string) $fax;
  }

  public function setCreated($created) {
    $this->created = (int) $created;
  }

}
