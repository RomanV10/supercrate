<?php

namespace Drupal\move_long_distance\Entity;

use Drupal\move_services_new\Util\enum\TripStatus;
use Drupal\move_services_new\Util\enum\TripType;

/**
 * Class TripDetails.
 *
 * @package Drupal\move_long_distance\Entity
 */
class TripDetails {

  /**
   * @var string
   */
  public $internal_code = "";

  /**
   * @var int
   */
  public $type = TripType::CARRIER;

  /**
   * @var int
   */
  public $flag = TripStatus::DRAFT;

  /**
   * @var string
   */
  public $description = "";

  /**
   * @var int
   */
  public $date_start = 0;

  /**
   * @var int
   */
  public $date_end = 0;

  // Getters.
  public function getInternalCode() : string {
    return $this->internal_code;
  }

  public function getType() {
    return (int) $this->type;
  }

  public function getFlag() {
    return (int) $this->flag;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getDateStart() {
    return $this->date_start;
  }

  public function getDateEnd() {
    return $this->date_end;
  }

  // Setters.
  public function setInternalCode($code) {
    $this->internal_code = (string) $code;
  }

  public function setType(int $type) {
    if (TripType::isValidValue($type)) {
      $this->type = $type;
    }
  }

  public function setFlag(int $flag) {
    if (TripStatus::isValidValue($flag)) {
      $this->flag = $flag;
    }
  }

  public function setDescription($description) {
    $this->description = $description;
  }

  public function setDateStart($date_start) {
    $this->date_start = $date_start;
  }

  public function setDateEnd($date_end) {
    $this->date_end = $date_end;
  }

}
