<?php
namespace Drupal\move_long_distance\Services\Actions;

use Drupal\move_new_log\Services\Log;

class SitLogActions extends SitActions {
  public $oldData = [];
  private $logText = [];
  private $newData = [];

  private $op = '';
  private $title = '';
  private $event = '';
  public $methodName = '';
  protected $logEventType = '';

  public $entityId = 0;
  public $entityType = 0;

  public function __construct(int $entity_id, int $entity_type, string $event, string $op, string $title,  array $new_data, array $old_data = []) {
    $this->entityId = $entity_id;
    $this->entityType = $entity_type;
    $this->event = $event;
    $this->op = $op;
    $this->title = $title;
    $this->newData = $new_data;
    $this->oldData = $old_data;
    $this->methodName = 'prepareLogFor' . ucfirst($this->op);
  }

  public function processLog() : array {
    return (method_exists($this, $this->methodName)) ? $this->writeLogs($this->title, $this->{$this->methodName}(), $this->event) : [];
  }

  public function prepareLogForCreate() : array {
    return $this->prepareMainLogText($this->newData);
  }

  public function prepareLogForUpdate() : array {
    return  $this->buildDiffBetweenNewAndOld();
  }

  public function prepareLogForDelete() : array {
    return $this->prepareMainLogText($this->newData);
  }

  public function prepareMainLogText($data) : array {
    $logs_fields = [];
    foreach ($data as $field_name => $value) {
      $value = is_array($value) ? implode($value, ',') : $value;
      $value = is_bool($value) ? $this->boolToString($value) : $value;
      if ($value) {
        $field = '"' . ucfirst(str_replace('_', ' ' ,$field_name)) . '"';
        $logs_fields[]['simpleText'] = $field .': ' . $value;
      }
    }

    return $logs_fields;
  }

  /**
   * Logs for receipts.
   *
   * @param string $title
   *   Logs title.
   * @param array $text
   *   Logs text.
   * @param string $event_type
   *   Type of event.
   *
   * @return array
   *   Array with result.
   *
   */
  private function writeLogs(string $title, array $text, string $event_type) : array {
    global $user;
    $user_name = isset($user->name) ? $user->name : 'Anonymous';
    $body = '';
    if ($event_type == 'mail') {
      $body = $text[0]['text'];
    }
    $log_data[] = array(
      'details' => array([
        'event_type' => $event_type,
        'activity' => $user_name,
        'title' => ucfirst(preg_replace('/_/', ' ', $title)),
        'text' => $text,
        'date' => time(),
        'body' => $body,
      ],
      ),
      'source' => $user_name,
    );
    $log = new Log($this->entityId, $this->entityType);
    return $log->create($log_data);
  }

  private function buildDiffBetweenNewAndOld() {
    $diff = $this->arrayDiffAssocRecursive($this->newData, $this->oldData);
    $this->prepareLogsFormDiff($diff);
    return $this->logText;
  }

  public function arrayDiffAssocRecursive(array $new, array $old, int $level = 1) : array {
    foreach ($new as $key => $value) {

      if (is_array($value)) {
        if (count($old[$key]) > count($new[$key])) {
          $temp_array = array_diff_assoc($old[$key], $new[$key]);
          $difference[$key] = $this->buildArrayForLog($temp_array);
        }

        if (!isset($old[$key])) {
          foreach ($value as $field_name => $val) {
            $difference[$key][$field_name]['new'] = $val;
            $difference[$key][$field_name]['old'] = NULL;
          }
        }
        elseif (!is_array($old[$key])) {
          $difference[$key]['new'] = $value;
          $difference[$key]['old'] = NULL;
        }
        else {
          $new_diff = $this->arrayDiffAssocRecursive($value, $old[$key], $level + 1);
          if ($new_diff != FALSE) {
            $difference[$key] = $new_diff;
          }
        }
      }
      elseif ((!isset($old[$key]) || $old[$key] != $value) && !($old[$key] === NULL && $value === NULL)) {
        $difference[$key]['new'] = $value;
        $difference[$key]['old'] = isset($old[$key]) ? $old[$key] : NULL;
      }
    }
    return !isset($difference) ? array() : $difference;
  }

  /**
   * Build old array for log.
   *
   * @param $array
   * @return mixed
   */
  public function buildArrayForLog($array) : array {
    $result = [];

    foreach ($array as $key_name => $val) {
      if (is_array($val)) {
        $result[$key_name] = $this->buildArrayForLog($val);
      }
      else {
        $result[$key_name]['new'] = NULL;
        $result[$key_name]['old'] = $val;
      }
    }
    return $result;
  }

  /**
   * Set logs array from diff on update.
   *
   * @param array $diff
   *   Array with diff.
   * @param string $previous_field
   *   Previous field_name to build log string.
   * @param int $level
   *   Iteration level.
   */
  public function prepareLogsFormDiff(array $diff, string &$previous_field = '', int $level = 1) : void {
    $excluded = ['id', 'ld_trip_id', 'uid', 'receipt_id'];
    foreach ($diff as $field_name => $val) {
      if (!in_array($field_name, $excluded, TRUE)) {
        if ($level === 1) {
          $previous_field = '';
        }
        if (is_array($val) && !isset($val['new']) && !isset($val['old'])) {
          $previous_field .= ucfirst(str_replace('_', ' ', $field_name)) . '->';
          $this->prepareLogsFormDiff($val, $previous_field, $level + 1);
        }
        else {
          if (is_null($val['old'])) {
            $this->logConvertedValue($val['new'], $field_name);
            $this->logText[]['simpleText'] = '"' . $previous_field . ucfirst(str_replace('_', ' ', $field_name)) . '" was added. Field value is: "' . str_replace('.00', '', $val['new']) . '"';
          }
          elseif (is_null($val['new'])) {
            $this->logConvertedValue($val['old'], $field_name);
            $this->logText[]['simpleText'] = '"' . $previous_field . ucfirst(str_replace('_', ' ', $field_name)) . '" was removed. Field value was: "' . str_replace('.00', '', $val['old']) . '"';
          }
          elseif (!is_null($val['new']) && !is_null($val['old'])) {
            $this->logConvertedValue($val['old'], $field_name);
            $this->logConvertedValue($val['new'], $field_name);

            $this->logText[]['simpleText'] = '"' . $previous_field . ucfirst(str_replace('_', ' ', $field_name)) . '" was changed from "' . str_replace('.00', '', $val['old']) . '" to "' . str_replace('.00', '', $val['new']) . '"';
          }
        }
      }
    }
  }

  /**
   * Convert field to human read.
   *
   * @param $value
   *   Value to convert.
   * @param string $field_name
   *   Name of the field for check.
   */
  public function logConvertedValue(&$value, string $field_name) {
//    switch ($field_name) {
//      case 'date':
//        $value = date('Y-m-d', $value);
//        break;
//    }
  }

  /**
   * @param array $oldData
   */
  public function setOldData(array $oldData): void {
    $this->oldData = $oldData;
  }

  /**
   * @param $array1
   * @param $array2
   * @return array
   */
  private function diffRecursive($array1, $array2) {
    $difference=array();
    foreach($array1 as $key => $value) {
      if(is_array($value) && isset($array2[$key])){ // it's an array and both have the key
        $new_diff = $this->diffRecursive($value, $array2[$key]);
        if( !empty($new_diff) )
          $difference[$key] = $new_diff;
      }
      else if(is_string($value) && !in_array($value, $array2)) { // the value is a string and it's not in array B
        $difference[$key] = $value . " is missing from the second array";
      }
      else if(!is_numeric($key) && !array_key_exists($key, $array2)) { // the key is not numberic and is missing from array B
        $difference[$key] = "Missing from the second array";
      }
    }
    return $difference;
  }

  /**
   * @param int $entity_id
   * @param int $entity_type
   */
  public function setEntity(int $entity_id, int $entity_type) : void {
    $this->entityId = $entity_id;
    $this->entityType = $entity_type;
  }

}
