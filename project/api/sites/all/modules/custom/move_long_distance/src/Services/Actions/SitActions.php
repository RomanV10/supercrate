<?php

namespace Drupal\move_long_distance\Services\Actions;

abstract class SitActions {
  public $entityId = NULL;
  public $entityType = NULL;
  public $log = NULL;

  /**
   * Convert bool value to string version.
   *
   * @param bool $value
   *   TRUE or FALSE.
   *
   * @return string
   *   Yes or no.
   */
  public function boolToString(bool $value) : string {
    return ($value) ? 'Yes' : 'No';
  }


}