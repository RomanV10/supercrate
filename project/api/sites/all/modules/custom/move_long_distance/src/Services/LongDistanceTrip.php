<?php

namespace Drupal\move_long_distance\Services;

use Drupal\move_calculator\Services\MoveRequestGrandTotal;
use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_parking\Services\Parking;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Util\enum\TripPaymentType;
use Drupal\move_services_new\Util\enum\TripStatus;
use Drupal\move_services_new\Util\enum\TripType;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\move_request\MoveRequestRetrieve;
use Drupal\move_new_log\Services\Log;
use Drupal\move_long_distance\Entity\Trip;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Drupal\move_services_new\Services\Cache;

/**
 * Class Invoice.
 *
 * @package Drupal\move_long_distance\Services
 */
class LongDistanceTrip extends BaseService {

  private $tripId = NULL;
  private $encoders;
  private $normalizers;
  private $serializer;
  public $tripStatusesForAddTruckUnavailable = [TripStatus::ACTIVE, TripStatus::DELIVERED];

  /**
   * LongDistanceTrip constructor.
   *
   * @param int|null $tripId
   *   Trip id.
   */
  public function __construct($tripId = NULL) {
    $this->tripId = $tripId;
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);

  }

  /**
   * Set trip id.
   *
   * @param int|null $tripId
   *   Trip id.
   */
  public function setId($tripId) {
    $this->tripId = $tripId;
  }

  /**
   * Create new trip.
   *
   * @param array $data
   *   Array with data for trip creation.
   *
   * @return bool|mixed|object
   *   Result.
   *
   * @throws \ServicesException
   */
  public function create($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = FALSE;
    try {
      if (empty($data['foreman'])) {
        $data['foreman'] = array();
      }
      $trip = $this->serializer->deserialize(json_encode($data), Trip::class, 'json');
      $trip->details->date_start = empty($trip->details->date_start) ? time() : $trip->details->date_start;
      $foremans = (array) $trip->foreman;
      $helpers = (array) $trip->helper;
      $trip_id = db_insert('ld_trip')
        ->fields(array(
          'note' => $trip->note,
          'created' => time(),
        ))
        ->execute();
      if ($trip_id) {
        $trip->trip_id = $trip_id;
        $result = $trip;
        $this->tripId = $trip_id;
      }
      // Filling the related tables.
      $fill_db = array(
        'details' => 'ld_trip_details',
        'start_from' => 'ld_trip_start_from',
        'ends_in' => 'ld_trip_end_in',
        'carrier' => 'ld_trip_carrier',
      );

      foreach ($fill_db as $item_name => $table_name) {
        $trip->{$item_name}->ld_trip_id = $trip_id;
        db_insert($table_name)
          ->fields((array) $trip->{$item_name})
          ->execute();
      }

      if (!empty($foremans)) {
        foreach ($foremans as $foreman) {
          $foreman['ld_trip_id'] = $trip_id;
          db_insert('ld_trip_foreman')
            ->fields($foreman)
            ->execute();
        }
      }

      if (!empty($helpers)) {
        foreach ($helpers as $helper) {
          $foreman['ld_trip_id'] = $trip_id;
          db_insert('ld_trip_helper')
            ->fields($helper)
            ->execute();
        }
      }
      // Add default closing.
      db_merge('ld_trip_closing')
        ->key(array('ld_trip_id' => $trip_id, 'is_total' => 1))
        ->fields(array('ld_trip_id' => $trip_id, 'is_total' => 1, 'job_id' => 0))
        ->execute();

      $log_title = 'Trip №' . $trip_id . ' was created';
      $log_text[]['simpleText'] = 'Trip №' . $trip_id . ' was created';
      $this->setLogs($log_title, $log_text);
    }
    catch (\Throwable $e) {
      $message = "Create Trip Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('trip', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Retrieve LD trip.
   *
   * @return array|mixed
   *   Trip data.
   *
   * @throws \ServicesException
   */
  public function retrieve() {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();

    try {
      if (!$this->tripId) {
        throw new \Exception("Unknown Trip Id", 406);
      }
      // Base Trip.
      $select = db_select('ld_trip', 'lt')
        ->fields('lt', array('note', 'created'))
        ->condition('lt.id', $this->tripId);
      $result = $select->execute()->fetchAssoc();

      // Details.
      $select = db_select('ld_trip_details', 'ltd')
        ->fields('ltd', array(
          'internal_code',
          'type',
          'flag',
          'description',
          'date_start',
          'date_end',
        ))
        ->condition('ltd.ld_trip_id', $this->tripId);
      $result['details'] = $select->execute()->fetchAssoc();
      if (!empty($result['details'])) {
        $result['details']['date_start'] = empty($result['details']['date_start']) ? time() : $result['details']['date_start'];
      }

      // Start From.
      $select = db_select('ld_trip_start_from', 'ltsf')
        ->fields('ltsf', array('zip', 'city', 'state'))
        ->condition('ltsf.ld_trip_id', $this->tripId);
      $result['start_from'] = $select->execute()->fetchAssoc();

      // Start Ends.
      $select = db_select('ld_trip_end_in', 'ltei')
        ->fields('ltei', array('zip', 'city', 'state'))
        ->condition('ltei.ld_trip_id', $this->tripId);
      $result['ends_in'] = $select->execute()->fetchAssoc();

      // Carrier.
      $select = db_select('ld_trip_carrier', 'ltc')
        ->fields('ltc', array('carrier_id', 'driver_name', 'driver_phone'))
        ->condition('ltc.ld_trip_id', $this->tripId);
      $result['carrier'] = $select->execute()->fetchAssoc();
      if (!empty($result['carrier']['carrier_id'])) {
        $carrier_inst = new LongDistanceCarrier($result['carrier']['carrier_id']);
        $carrier = $carrier_inst->retrieve();
        if (!empty($carrier)) {
          $result['carrier'] = array_merge($result['carrier'], $carrier);
        }
      }

      // Foremans.
      $select = db_select('ld_trip_foreman', 'ltf');

      $select->join('field_data_field_user_first_name', 'first_name', 'first_name.entity_id = ltf.foreman_id');
      $select->join('field_data_field_user_last_name', 'last_name', 'last_name.entity_id = ltf.foreman_id');

      $select->fields('ltf', array('foreman_id'));
      $select->addExpression("CONCAT(first_name.field_user_first_name_value, ' ', last_name.field_user_last_name_value)", 'foreman_name');
      $select->condition('ltf.ld_trip_id', $this->tripId);
      $result['foreman'] = $select->execute()->fetchAssoc(\PDO::FETCH_ASSOC);

      // Helpers.
      $select = db_select('ld_trip_helper', 'lth')
        ->fields('lth', array('helper_id', 'helper_name'))
        ->condition('lth.ld_trip_id', $this->tripId);
      $result['helper'] = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);

      // Trucks.
      $select = db_select('move_parking', 'mp');
      $select->leftJoin('move_parking_chosen_trucks', 'mpct', 'mpct.pid = mp.pid');
      $select->leftJoin('taxonomy_term_data', 'ttd', 'mpct.truck_id = ttd.tid');
      $select->fields('mpct', array('truck_id'))
        ->fields('ttd', array('name'))
        ->condition('mp.entity_id', $this->tripId)
        ->condition('mp.entity_type', EntityTypes::LDTRIP);
      $trucks_query_result = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);
      if (!empty($trucks_query_result)) {
        foreach ($trucks_query_result as $truck) {
          if (!empty($truck['truck_id'])) {
            $truck_type = Parking::getTruckType($truck['truck_id']);
            if (!empty($truck_type)) {
              $result['trailers'][] = $truck;
            }
            else {
              $result['trucks'][] = $truck;
            }

          }
        }
      }

      // Payroll.
      $result['payroll_status'] = self::tripPayrollStatus($this->tripId);
      $payroll = new LongDistancePayroll($this->tripId);
      $result['payroll'] = $payroll->retrieve();
      $result['trip_id'] = $this->tripId;
      $result['jobs'] = $this->getClosingTrip(1, 1, -1);
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('trip retrieve', $message, array(), WATCHDOG_ERROR);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Update trip.
   *
   * @param array $data
   *   Trip data.
   *
   * @return bool|mixed|object
   *   Trip or error.
   *
   * @throws \ServicesException
   */
  public function update($data = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = TRUE;
    try {
      if (empty($data['foreman'])) {
        $data['foreman'] = array();
      }

      $trip = $this->serializer->deserialize(json_encode($data), Trip::class, 'json');
      $old_trip = $this->retrieve();
      $diff = self::diffOnUpdate($old_trip, $trip);

      // If trip type was changed - update jobs.
      if ($trip->details->type != $old_trip['details']['type']) {
        // Change trip carrier if type was changed. Remove actually.
        if ($trip->details->type == TripType::PERSONALLY) {
          $this->actionTripTypeWasChangedToForeman($trip);
        }

        if ($trip->details->type === TripType::CARRIER) {
          $this->actionTripTypeWasChangedToCarrier($trip);
        }
      }
      else {
        // If trip type is carrier and old carrier not equal new.
        if ($trip->details->type === TripType::CARRIER && !empty($trip->carrier) && $old_trip['carrier']['carrier_id'] != $trip->carrier->carrier_id) {
          $this->actionTripCarrierWasChanged($trip, $old_trip);
        }

        // If trip type is personally and old carrier not equal new.
        if ($trip->details->type === TripType::PERSONALLY && !empty($trip->foreman)) {
          $this->actionTripForemanWasChanged($trip, $old_trip);
        }

        // Check required fields.
        $this->requireFieldValidation($trip);
      }

      if (empty($trip->error)) {
        $this->actionsOnChangedStatus($trip, $old_trip);

        db_update('ld_trip')
          ->fields(array('note' => $trip->note))
          ->condition('id', $this->tripId)
          ->execute();

        // Filling the related tables.
        $fill_db = array(
          'details' => 'ld_trip_details',
          'start_from' => 'ld_trip_start_from',
          'ends_in' => 'ld_trip_end_in',
          'carrier' => 'ld_trip_carrier',
        );

        foreach ($fill_db as $item_name => $table_name) {
          db_merge($table_name)
            ->key(array('ld_trip_id' => $this->tripId))
            ->fields((array) $trip->{$item_name})
            ->condition('ld_trip_id', $this->tripId)
            ->execute();
        }

        $old_flag = (int) $old_trip['details']['flag'];

        $log_title = 'Trip was updated';
        if ($old_flag != $trip->details->flag && !empty($old_flag)) {
          $trip_jobs = self::getTripJobs($this->tripId);
          if (!empty($trip_jobs)) {
            foreach ($trip_jobs as $job_id) {
              LongDistanceJob::updateJobStatus($job_id['job_id'], $trip->details->flag);
            }
          }
        }
        if (!empty($diff)) {
          $this->setLogs($log_title, $diff);
          $job_inst = new LongDistanceJob();
          $job_inst->updateClosingTotal($this->tripId);
        }
      }
      else {
        $result = $trip;
      }

    }
    catch (\Throwable $e) {
      $message = "Updating Trip {$this->tripId}: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('trip update', $message, array(), WATCHDOG_ERROR);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Get jobs ids by trip id.
   *
   * @param int $trip_id
   *   Id of the trip.
   *
   * @return mixed
   *   False if nothing. Array of ids if exist.
   */
  public static function getTripJobs(int $trip_id) {
    $query = db_select('ld_trip_jobs', 'ltj');
    $query->fields('ltj', array('job_id'));
    $query->condition('trip_id', $trip_id);
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $result;

  }

  /**
   * Update only total cache value.
   *
   * @param int $trip_id
   *   Id of the trip.
   */
  public static function updateTripTotalInCache(int $trip_id) : void {
    $trip_total = self::getTripTotal($trip_id);
    if (!empty($trip_total)) {
      $trip_cache = db_select('ld_trip_index', 'lti')
        ->fields('lti', array('cache'))
        ->condition('trip_id', $trip_id)
        ->execute()
        ->fetchAssoc();
      if (!empty($trip_cache)) {
        $cache = unserialize($trip_cache['cache']);
        $new_cache = array_replace($cache, $trip_total);
      }
      else {
        $new_cache = $trip_total;
      }

      db_update('ld_trip_index')
        ->fields(array('cache' => serialize($new_cache)))
        ->condition('trip_id', $trip_id)
        ->execute();
    }
  }

  /**
   * Update trip status.
   *
   * @param int $ld_trip_id
   *   Trip id.
   * @param int $status
   *   New status.
   *
   * @return \DatabaseStatementInterface
   *   0 if not update or 1 if updated
   */
  public static function updateTripStatus(int $ld_trip_id, int $status) {
    $trip_inst = new LongDistanceTrip();
    $old_status = (int) $trip_inst->tripHasStatus($status, $ld_trip_id);
    $update = 0;
    if ($old_status != $status && !empty($old_status)) {
      $statuses = self::tripStatuses();
      $update = db_update('ld_trip_details')
        ->fields(array('flag' => $status))
        ->condition('ld_trip_id', $ld_trip_id)
        ->execute();

      $log_title = "Trip status was updated from " . $statuses[$old_status] . " to " . $statuses[$status];
      $text[]['simpleText'] = '';
      $trip_inst = new LongDistanceTrip($ld_trip_id);
      $trip_inst->setLogs($log_title, $text);
      $trip_jobs = self::getTripJobs($ld_trip_id);
      if (!empty($trip_jobs)) {
        foreach ($trip_jobs as $job_id) {
          LongDistanceJob::updateJobStatus($job_id['job_id'], $status);
        }
      }
    }

    return $update;
  }

  /**
   * Actions when trip status was changed.
   *
   * @param mixed $new_trip
   *   Object with trip values.
   * @param mixed $old_trip
   *   Array with old trip values.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function actionsOnChangedStatus($new_trip, $old_trip) {
    if ($this->isTripStatusWasChanged($new_trip, $old_trip)) {
      $this->statusWasChangedToActive($new_trip);
    }
  }

  /**
   * Check if trip status was changed.
   *
   * @param mixed $new_trip
   *   Object with new trip values.
   * @param array|mixed $old_trip
   *   Array with old trip values.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isTripStatusWasChanged($new_trip, $old_trip) {
    return ($new_trip->details->flag != $old_trip['details']['flag'] && !empty($new_trip->details->flag)) ? TRUE : FALSE;
  }

  /**
   * Action when status was changed to active.
   *
   * @param mixed $new_trip
   *   Trip values.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function statusWasChangedToActive($new_trip) {
    if ($new_trip->details->flag == TripStatus::ACTIVE) {
      $ld_job = new LongDistanceJob();
      $trip_jobs = self::getTripJobs($this->tripId);
      foreach ($trip_jobs as $job) {
        $ld_job->setId($job['job_id'], $this->tripId);
        $ld_job->changeJobSitStatus(LongDistanceSit::MOVE_OUT_STATUS);
      }
    }
  }

  /**
   * Get all total info for one trip.
   *
   * @param int $trip_id
   *   Id of  the trip.
   *
   * @return mixed
   *   Array of trip total fields.
   */
  public static function getTripTotal(int $trip_id) {
    $query = db_select('ld_trip_closing', 'ltc');
    $query->addField('ltc', 'job_total', 'jobs_total');
    $query->addField('ltc', 'volume_cf', 'volume');
    $query->addField('ltc', 'paid', 'total_payment');
    $query->addField('ltc', 'balance', 'balance');
    $query->condition('ld_trip_id', $trip_id);
    $query->condition('is_total', 1);
    $result = $query->execute()->fetchAssoc();
    return $result;
  }

  /**
   * Get the carrier of the trip.
   *
   * @param int $trip_id
   *   Id og the trip.
   *
   * @return mixed
   *   Array of carrier info.
   */
  public static function getTripCarrier($trip_id) {
    $result = db_select('ld_trip_carrier', 'ltc')
      ->fields('ltc', array('carrier_id'))
      ->condition('ld_trip_id', $trip_id)
      ->execute()
      ->fetchField();

    return $result;
  }

  /**
   * Get all trip closing with total or not.
   *
   * @param int $total
   *   Show total or not. 0 if not.
   * @param int $page
   *   Number page.
   * @param int $page_size
   *   Page size.
   * @param bool $trip_details
   *   Flag.
   * @param array $sorting
   *   Data for sorting.
   *
   * @return array|mixed
   *   All closing values.
   *
   * @throws \ServicesException
   */
  public function getClosingTrip(int $total = 1, int $page, int $page_size = -1, bool $trip_details = FALSE, $sorting = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = array();
    $start_from_page = 0;
    try {
      $trip_type = self::getTripType($this->tripId);
      if ($page_size > 0) {
        $result = array();
        // Get total count of rows.
        $query = db_select('ld_trip_closing', 'ltc');
        $query->fields('ltc');
        $query->condition('ld_trip_id', $this->tripId);
        if (empty($total)) {
          $query->isNotNull('is_total');
        }
        $total_count = $query->execute()->rowCount();
        // Calculate number of pages.
        $page_count = ceil($total_count / $page_size);
        // Build pager info for front.
        $meta = array(
          'currentPage' => (int) $page,
          'perPage' => (int) $page_size,
          'pageCount' => (int) $page_count,
          'totalCount' => (int) $total_count,
        );
        // Number of row from we start our select.
        $start_from_page = ($page - 1) * $page_size;
        $result['_meta'] = $meta;
      }

      // Request address fields.
      $request_fields = array(
        'pickup_date',
        'delivery_date',
        'second_delivery_date',
        'weight',
        'address_from',
        'address_to',
        'schedule_delivery_date',
        'ready_for_delivery',
      );
      $address_fields = array(
        'zip',
        'city',
        'state',
      );
      $address_tables = array(
        'from_a' => 'ld_request_address_from',
        'to_a' => 'ld_request_address_to',
      );
      $query = db_select('ld_trip_closing', 'ltc');
      $query->leftJoin('ld_request', 'ldr', 'ltc.job_id = ldr.ld_nid');

      foreach ($address_tables as $table_alias => $table_name) {
        $query->leftJoin($table_name, $table_alias, 'ltc.job_id = ' . $table_alias . '.ld_nid');
        foreach ($address_fields as $field_name) {
          $query->addField($table_alias, $field_name, $table_alias . '_' . $field_name);
        }
      }

      // TP Delivery tables.
      $query->leftJoin('ld_tp_delivery_job', 'tp_delivery_job', 'ltc.job_id = tp_delivery_job.job_id');
      $query->leftJoin('ld_tp_delivery_details', 'tp_delivery_details', 'tp_delivery_job.ld_tp_delivery_id = tp_delivery_details.ld_tp_delivery_id');
      $query->leftJoin('ld_carrier', 'carrier', 'tp_delivery_details.delivery_for_company = carrier.ld_carrier_id');
      $query->fields('tp_delivery_job', array('delivery_job_id'));
      $query->addField('carrier', 'name', 'carrier_name');
      $query->fields('tp_delivery_details');

      $query->fields('ltc');
      $query->fields('ldr', $request_fields);
      $query->condition('ld_trip_id', $this->tripId);
      if (empty($total)) {
        $query->isNotNull('is_total');
      }
      if ($page_size > 0) {
        $query->range($start_from_page, $page_size);
      }

      if (!empty($sorting)) {
        if ($sorting['orderKey'] == 'r_type') {
          $sorting_r_type = $sorting['orderValue'];
        }
        else {
          $query->orderBy($sorting['orderKey'], $sorting['orderValue']);
        }

      }
      $closing = $query->execute()->fetchAll();
      $current_date = time();

      $job_total_for_total = 0;
      $balance_for_total = 0;
      $total_paid_for_total = 0;
      $grand_total_calculator = new MoveRequestGrandTotal();
      if (!empty($closing)) {
        foreach ($closing as $row) {
          $row->request_balance = 0;
          $row->j_type = 0;
          $row->r_type = 'Pickup';
          // Payment part.
          $row->invoice = LongDistanceJob::isJobInInvoice($row->job_id);
          $row->receipt = LongDistanceJob::isJobInReceipt($row->job_id);

          // This is for Request jobs.
          if (!empty($row->job_id) && $row->type == '0') {
            $entity_type = EntityTypes::MOVEREQUEST;
            $total_paid = 0;
            try {
              $jobWrapper = entity_metadata_wrapper('node', $row->job_id);
              $request_retrieve = new MoveRequestRetrieve($jobWrapper);
              $job_entity = $request_retrieve->nodeFields();
            }
            catch (\Throwable $e) {
              continue;
            }
            $grand_total_all = $grand_total_calculator->setRequestRetrieve($job_entity)->build()->calculate();
            $grand_total = $grand_total_all->grandTotalClosing;
            $row->phone = !empty($job_entity['phone']) ? $job_entity['phone'] : '';
            $row->email = !empty($job_entity['email']) ? $job_entity['email'] : '';
            $row->owner = $job_entity['name'];
            $receipts = MoveRequest::getReceipt($row->job_id);
            $balance = $grand_total;
            if (!empty($receipts)) {
              foreach ($receipts as $receipt) {
                if (!$receipt['pending']) {
                  $total_paid += (float) $receipt['amount'];
                  $balance -= (float) $receipt['amount'];
                }
              }
            }
            // Only for carrier on trip details page.
            if ($trip_details && $trip_type == TripType::CARRIER) {
              $job_total_for_total += ($grand_total - $row->job_total);
              $balance_for_total += ($balance - $row->balance);
              $total_paid_for_total += ($total_paid - $row->tp_collected);
              $row->job_total = $grand_total;
              $row->tp_collected = $total_paid;
              $row->balance = $balance;

            }

            // If trip type is personally -
            // Get all numbers from request(not job closing).
            if ($trip_type == TripType::PERSONALLY) {
              // This is job total form request. We need to add it to total.
              $job_total_for_total += (float) $grand_total;
              // This balance will use for total.
              $balance_for_total += $balance;
              $total_paid_for_total += ($total_paid - $row->tp_collected);
              $row->job_total = $grand_total;
              $row->tp_collected = $total_paid;
              $row->balance = $balance;
            }

            if ($row->pickup_date < $current_date && !is_null($row->pickup_date)) {
              $row->r_type = 'Delivery';
              $row->j_type = 1;
            }

            if (($row->delivery_date > $current_date && !is_null($row->delivery_date)) || !empty($row->ready_for_delivery)) {
              $row->r_type = 'Delivery';
              $row->j_type = 1;
            }
          }
          // Check and replace fields for TP Delivery.
          if ($row->type > 0) {
            $tp_d_phones = db_select('ld_tp_delivery_phones', 'ltdp')
              ->fields('ltdp', array('phone'))
              ->condition('ld_tp_delivery_id', $row->job_id)
              ->execute()
              ->fetchField();
            $row->phone = !empty($tp_d_phones) ? $tp_d_phones : '';
            $row->owner = $row->owner ?: $row->customer;
            $entity_type = 1;
            $row->phone = !empty($job_entity['phone']) ? $job_entity['phone'] : '';
            $row->j_type = 2;
            $row->r_type = 'TP Delivery';
            $row->pickup_date = $row->date_from;
            $row->delivery_date = $row->date_to;
            // TODO make changes for address in db and entity.
            $row->address_to = $row->address;
            // Change city, state, zip to delivery fields.
            foreach ($address_fields as $field_name) {
              $row->{'to_a_' . $field_name} = $row->{$field_name};
            }
          }

          // SIT for print.
          if ($row->job_id > 0 && $trip_details) {
            $sit = new LongDistanceSit($row->job_id, $entity_type);
            $row->sit = $sit->retrieve();
          }

          $result['items'][] = $row;
        }

        foreach ($result['items'] as $item) {
          if (!empty($item->is_total)) {
            $item->job_total += $job_total_for_total;
            $item->balance += $balance_for_total;
            // Show this total only on trip details page and only for carrier.
            if ($trip_type == TripType::PERSONALLY || $trip_type == TripType::CARRIER && $trip_details) {
              $item->tp_collected += $total_paid_for_total;
            }
          }
        }

        if (isset($sorting_r_type)) {
          $this->sortClosingJobsByType($result['items'], $sorting_r_type);
          $result['items'] = array_values($result['items']);
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Updating Trip {$this->tripId}: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('trip update', $message, array(), WATCHDOG_ERROR);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Update all request jobs for trip when in personally. Set values to 0.
   *
   * Clone previuos values to temp table.
   */
  public function updateJobToPersonallyTrip() {
    $fields_to_get = array(
      'id',
      'job_id',
      'ld_trip_id',
      'rate_per_cf',
      'job_cost',
      'services_total',
      'job_total',
      'tp_collected',
      'received',
      'paid',
      'to_receive',
      'to_pay',
      'balance',
    );
    $fields_to_update = array(
      'rate_per_cf' => 0,
      'job_cost' => 0,
      'services_total' => 0,
      'job_total' => 0,
      'tp_collected' => 0,
      'received' => 0,
      'paid' => 0,
      'to_receive' => 0,
      'to_pay' => 0,
      'balance' => 0,
    );

    $closing = db_select('ld_trip_closing', 'ltc')
      ->fields('ltc', $fields_to_get)
      ->condition('ld_trip_id', $this->tripId)
      ->condition('type', 0)
      ->isNull('is_total')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($closing)) {
      // Copy job request closing to temp closing table.
      foreach ($closing as $item) {
        db_merge('ld_trip_temp_closing')
          ->key(array('id' => $item['id']))
          ->fields($item)
          ->execute();
        $ids[] = $item['id'];
      }
      // TODO user this method in future updateTripJobNumbers.
      // Update all values to 0.
      db_update('ld_trip_closing')
        ->fields($fields_to_update)
        ->condition('id', $ids, "IN")
        ->execute();
    }
  }

  /**
   * Update all request jobs for trip when in carrier.
   *
   * Copy jobs from temp table.
   */
  public function updateJobToCarrierTrip() {
    // Get all current jobs from closing.
    $closing_jobs = db_select('ld_trip_closing', 'ltc')
      ->fields('ltc', array('job_id'))
      ->condition('ld_trip_id', $this->tripId)
      ->condition('type', 0)
      ->isNull('is_total')
      ->execute()
      ->fetchCol();

    if (!empty($closing_jobs)) {
      // Get temp jobs closing.
      $temp_closing_jobs = db_select('ld_trip_temp_closing', 'ltc')
        ->fields('ltc')
        ->condition('ld_trip_id', $this->tripId)
        ->condition('job_id', $closing_jobs, 'IN')
        ->execute()
        ->fetchAll(\PDO::FETCH_ASSOC);
      if (!empty($temp_closing_jobs)) {
        $job_inst = new LongDistanceJob();
        // Update current job closing from temp closing.
        foreach ($temp_closing_jobs as $temp_job) {
          $job_id = $temp_job['job_id'];
          $trip_id = $temp_job['ld_trip_id'];

          unset($temp_job['id']);
          unset($temp_job['ld_trip_id']);

          $receipt = MoveRequest::getReceipt($job_id);

          // If job doesn't have first payment - add it there.
          if (!$this->isTransactionTypeExistInReceipt($receipt, TripPaymentType::NEW_JOB)) {
            $job_inst->setId($job_id, $trip_id);
            $closing = $job_inst->prepareJobEntityForSave();

            if (!empty($closing)) {
              unset($closing['volume_cf']);
              $temp_job = $closing;
              LongDistanceJob::createPaymentReceiptTrip($job_id, EntityTypes::MOVEREQUEST, $trip_id, (float) $closing['tp_collected']);
            }
          }

          db_update('ld_trip_closing')
            ->fields($temp_job)
            ->condition('job_id', $temp_job['job_id'])
            ->execute();

          // Remove job from temp closing.
          db_delete('ld_trip_temp_closing')
            ->condition('job_id', $temp_job['job_id'])
            ->execute();
        }

      }
    }
  }

  /**
   * Remove trip.
   *
   * Remove trip with all dependencies.
   *
   * @return \DatabaseStatementInterface
   *   TRUE of FALSE.
   */
  public function delete() {
    $result = array();
    $is_draft = $this->tripHasStatus(TripStatus::DRAFT, $this->tripId);
    if ($is_draft) {
      $result = db_delete('ld_trip')->condition('id', $this->tripId)->execute();

      $tables = array(
        'details' => 'ld_trip_details',
        'start_from' => 'ld_trip_start_from',
        'ends_in' => 'ld_trip_end_in',
        'carrier' => 'ld_trip_carrier',
        'helper' => 'ld_trip_helper',
        'foreman' => 'ld_trip_foreman',
      );
      foreach ($tables as $table) {
        db_delete($table)->condition('ld_trip_id', $this->tripId)->execute();
      }

      // Jobs.
      $this->removeAllJobsFromTrip();

      // Parking.
      $this->deleteTripParking();

      // TP Delivery.
      self::deleteAllTpDeliveryInTrip($this->tripId);

      // Clear Index.
      db_delete('ld_trip_index')->condition('trip_id', $this->tripId)->execute();
    }
    else {
      $result['error'] = t('Only Draft can be removed');
    }
    return $result;
  }

  /**
   * Check if trip has needed status.
   *
   * @param int $status
   *   Status of the trip.
   * @param int $trip_id
   *   Id of the trip.
   *
   * @return mixed
   *   Flag number or FALSE.
   */
  public function tripHasStatus(int $status, int $trip_id) {
    $query = db_select('ld_trip_details', 'ltd')
      ->fields('ltd', array('flag'))
      ->condition('flag', $status)
      ->condition('ld_trip_id', $trip_id)
      ->execute()
      ->fetchField();
    return $query;
  }

  /**
   * Get trip status by trip id.
   *
   * @param int $trip_id
   *   Trip id.
   *
   * @return mixed
   *   Return trip status or false.
   */
  public static function getTripStatus(int $trip_id) {
    $status = db_select('ld_trip_details', 'ltd')
      ->fields('ltd', array('flag'))
      ->condition('ld_trip_id', $trip_id)
      ->execute()
      ->fetchField();
    return $status;
  }

  /**
   * Recieved trip type(Personally or Carrier).
   *
   * @param int $trip_id
   *   Id of the trip.
   *
   * @return mixed
   *   False or type number.
   */
  public static function getTripType(int $trip_id) {
    $result = db_select('ld_trip_details', 'ltd')
      ->fields('ltd', array('type'))
      ->condition('ld_trip_id', $trip_id)
      ->execute()
      ->fetchField();
    return $result;
  }

  /**
   * TODO fix pages argument. Set default to null.
   *
   * Get all trips with pager.
   *
   * @param $current_page
   *
   * @param $per_page
   *
   * @return mixed
   */
  public function index($current_page = 1, $per_page = 20, $condition = array(), $sorting = array()) {
    // Sub query for job count.
    $trip_jobs = db_select('ld_trip_jobs', 'ltj');
    $trip_jobs->leftJoin('ld_trip_closing', 'ltc', 'ltj.job_id = ltc.job_id');
    $trip_jobs->fields('ltj', array('trip_id'));
    $trip_jobs->addExpression('COUNT(ltj.job_id)', 'jobs_count');
    $trip_jobs->groupBy('ltj.trip_id');

    $query = db_select('ld_trip', 'ld');
    $query->leftJoin('ld_trip_closing', 'ltc', 'ld.id = ltc.ld_trip_id');
    $query->leftJoin('ld_trip_carrier', 'ltca', 'ld.id = ltca.ld_trip_id');
    $query->leftJoin('ld_trip_foreman', 'ltf', 'ld.id = ltf.ld_trip_id');
    $query->leftJoin('ld_trip_details', 'ltd', 'ld.id = ltd.ld_trip_id');
    $query->addJoin('LEFT OUTER', $trip_jobs, 'ltj', 'ltj.trip_id = ld.id');

    $query->fields('ltc');
    $query->fields('ltca');
    $query->fields('ltd');
    $query->fields('ld');
    $query->fields('ltj');
    $query->fields('ltf', array('foreman_name'));
    if (!empty($condition['filters']['job_id'])) {
      $query->condition('ltc.job_id', db_like($condition['filters']['job_id']) . '%', 'LIKE');
      $query->groupBy('ltc.ld_trip_id');
      unset($condition['filters']['job_id']);
    }
    else {
      $query->isNotNull('is_total');
    }

    $result['items'] = array();

    if (!empty($condition['filters'])) {
      $query_fields = $query->getFields();
      foreach ($condition['filters'] as $field_name => $val) {
        if (!empty($val) || $val === 0) {
          switch ($field_name) {
            case 'to_state':
            case 'from_state':
              $operator = 'IN';
              break;

            case 'date':
            case 'pickup_date':
            case 'delivery_date':
              if (!empty($val['from']) && !empty($val['to'])) {
                $date[0] = $val['from'];
                $date[1] = $val['to'];
                $val = $date;
                $operator = 'BETWEEN';
              }
              if (!empty($val['from']) && empty($val['to'])) {
                $val = $val['from'];
                $operator = '>';
              }
              if (empty($val['from']) && !empty($val['to'])) {
                $val = $val['to'];
                $operator = '<';
              }
              break;

            case 'to_city':
            case 'from_city':
              $val = db_like($val) . '%';
              $operator = 'LIKE';
              break;

            default:
              if (is_array($val)) {
                $operator = 'IN';
              }
              else {
                $operator = '=';
              }
          }
          // We need this yo use aliases for condition.
          // Drupal condition don't use alias.
          if (isset($query_fields[$field_name])) {
            $field_name = $query_fields[$field_name]['table'] . '.' . $query_fields[$field_name]['field'];
          }
          $query->condition($field_name, $val, $operator);
        }
      }
    }
    // Get total count of rows.
    $total_count = $query->execute()->rowCount();

    // Calculate number of pages.
    $page_count = ceil($total_count / $per_page);

    // Build pager info for front.
    $meta = array(
      'currentPage' => (int) $current_page,
      'perPage' => (int) $per_page,
      'pageCount' => (int) $page_count,
      'totalCount' => (int) $total_count,
    );

    // Number of row from we start our select.
    $start_from_page = ($current_page - 1) * $per_page;
    $query->range($start_from_page, $per_page);
    $result['_meta'] = $meta;
    if (!empty($sorting)) {
      switch ($sorting['orderKey']) {
        case 'ld_trip_id':
          $sorting['orderKey'] = 'ltc.ld_trip_id';
          break;

        case 'type':
          $sorting['orderKey'] = 'ltd.type';
          break;

        case 'foreman':
          $sorting['orderKey'] = 'ltca.driver_name';
          break;
      }
      $query->orderBy($sorting['orderKey'], $sorting['orderValue']);
    }
    $query_result = $query->execute();
    // Get total count of rows.
    // Get values for total.
    $total_fields = array(
      'jobs_count' => 0,
      'volume_cf' => 0,
      'job_total' => 0,
      'balance' => 0,
    );

    foreach ($query_result as $row) {
      if ($row->type == TripType::PERSONALLY) {
        $row->driver_name = $row->foreman_name;
      }
      // Get closing values for the trip.
      $this->setId($row->ld_trip_id);
      $trip_jobs = $this->getClosingTrip(TRUE, 1, -1);
      if (!empty($trip_jobs['items'])) {
        foreach ($trip_jobs['items'] as $item) {
          if (!empty($item->is_total)) {
            $total_closing = $item;
            unset($total_closing->type);
          }
        }
      }
      if (!empty($total_closing)) {
        $row = array_merge((array) $row, (array) $total_closing);
      }
      unset($row['id']);
      foreach ($total_fields as $field_name => $val) {
        $total_fields[$field_name] += $row[$field_name];
      }
      $weight = (float) $row['volume_cf'] * 7;
      $row['volume_cf'] = $row['volume_cf'] . '/' . $weight;
      $result['items'][] = (array) $row;
    }

    if (!empty($total_fields['volume_cf'])) {
      $total_fields['weight'] = $total_fields['volume_cf'] * 7;
    }
    $total_fields['trip_count'] = count($result['items']);
    $result['total'] = $total_fields;
    return $result;
  }

  /**
   * Trip Planer filter. Filtering Trips.
   *
   * @param int $job_id
   *   Id move_request related with trip.
   * @param int $type
   *   Trip type: 1 - Carrier/Agent, 2 - Personally.
   * @param array $status
   *   Trip status: 1 - Draft, 2 - Pending, 3 - Active, 4 - Delivered.
   *
   * @return array
   *   Loaded trips.
   */
  public function filter(int $job_id = 0, int $type = 0, array $status = array()) {
    $result = array();
    $trips = array();
    $job_id = $job_id ? $job_id : FALSE;
    $type = TripType::isValidValue($type) ? $type : FALSE;

    // Checking legitimate status.
    $check_status = function ($status) {
      $result = array();
      foreach ((array) $status as $stat) {
        if (TripStatus::isValidValue($stat)) {
          $result[] = $stat;
        }
      }
      return $result;
    };
    $checked_status = $check_status($status);

    // Filter by trip jobs.
    $trip_jobs = function (int $job_id) use (&$trips) {
      $select = db_select('ld_trip_jobs', 'tj')
        ->distinct()
        ->fields('tj', array('trip_id'))
        ->condition('tj.job_id', $job_id)
        ->orderBy('tj.trip_id', 'ASC')
        ->execute();
      $trips = $select->fetchCol(0);
    };
    $trip_jobs($job_id);

    // Filter by status and type.
    $trip_additional = function (int $type, array $status) use (&$trips) {
      $select = db_select('ld_trip_additional', 'lt')
        ->distinct()
        ->fields('lt', array('trip_id'));
      if ($type) {
        $select->condition('lt.trip_type', $type);
      }
      if ($status) {
        $select->condition('lt.trip_status', $status, 'IN');
      }
      $select->orderBy('lt.trip_id', 'ASC');
      $result = $select->execute()->fetchCol(0);
      $trips = array_merge($trips, $result);
    };
    $trip_additional($type, $checked_status);

    // Load filtered trips.
    foreach ($trips as $trip_id) {
      $this->tripId = $trip_id;
      $result[$trip_id] = $this->retrieve();
    }

    return $result;
  }

  /**
   * Remove all TP Delivery for trip.
   *
   * @param int $trip_id
   *   Id of the trip.
   */
  public static function deleteAllTpDeliveryInTrip(int $trip_id) {
    $tp_delivery = db_select('ld_tp_delivery', 'tp_delivery')
      ->fields('tp_delivery', array('ld_tp_delivery_id'))
      ->condition('ld_trip_id', $trip_id)
      ->execute()
      ->fetchCol();
    if (!empty($tp_delivery)) {
      $job_inst = new LongDistanceJob(NULL, NULL, EntityTypes::LDTDJOB);
      foreach ($tp_delivery as $id) {
        $job_inst->setId($id);
        $job_inst->removeJobLdTrip($trip_id);
      }
    }
  }

  /**
   * Remove all jobs from trip.
   */
  public function removeAllJobsFromTrip() {
    $trip_jobs = self::getTripJobs($this->tripId);
    if (!empty($trip_jobs)) {
      $job_inst = new LongDistanceJob();
      foreach ($trip_jobs as $job_id) {
        $job_inst->setId($job_id['job_id']);
        $job_inst->removeJobLdTrip($this->tripId);
      }
    }
  }

  /**
   * Remove parking for trip.
   */
  public function deleteTripParking() {
    $select = db_select('move_parking', 'mp')
      ->fields('mp', array('pid'))
      ->condition('mp.entity_id', $this->tripId)
      ->condition('mp.entity_type', EntityTypes::LDTRIP);
    $result = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);
    if (!empty($result)) {
      $parking = new Parking();
      foreach ($result as $row) {
        $parking->setId($row['pid']);
        $parking->delete();
      }
    }
  }

  /**
   * Short list of fields of the new reuqest for trip.
   *
   * @param array $nids
   *   Array of nids of Requests.
   *
   * @return array
   *   Fiedls of the requests fields.
   */
  public static function getNewRequestTrips(array $nids) :array {
    $results = array();
    $get_fields = function (array $node_fields) {
      $move_request = array();
      $fields = array(
        'nid',
        'date',
        'ddate',
        'status',
        'first_name',
        'last_name',
        'field_moving_from',
        'field_moving_to',
        'storage_id',
        'field_ld_status',
      );

      foreach ($fields as $field_name) {
        $move_request[$field_name] = $node_fields[$field_name];
      }
      return $move_request;
    };

    $current_time = time();
    $distance_calculate = new DistanceCalculate();
    foreach ($nids as $nid) {
      $obj = new MoveRequest($nid);
      $request = $obj->getNode($nid, 1);

      $results[$nid] = $get_fields($request);
      if (!empty($results[$nid]['date']['raw']) &&  $current_time < $results[$nid]['date']['raw']) {
        $status = 1;
      }

      if (!empty($results[$nid]['ddate']['raw']) && $current_time > $results[$nid]['ddate']['raw']) {
        $status = 2;
      }
      $results[$nid]['r_type'] = $status;
      $results[$nid]['to_storage'] = $request['request_all_data']['request_distance']['distances']['AB']['distance'];
      $results[$nid]['from_storage'] = $request['request_all_data']['request_distance']['distances']['BC']['distance'];
      $results[$nid]['weight'] = $distance_calculate->getLongDistanceWeight($request);
      $results[$nid]['sit'] = (!empty($request['request_all_data']['sit'])) ? TRUE : FALSE;
    }

    return array_values($results);
  }

  /**
   * Run this action when trip type was changed from Carrier to Foreman.
   *
   * @param $trip
   *   Object new trip data.
   */
  public function actionTripTypeWasChangedToForeman($trip) : void {
    $trip->carrier->carrier_id = 0;
    $this->updateJobToPersonallyTrip();
    $job_inst = new LongDistanceJob();
    // We need to update trip total.
    $job_inst->updateClosingTotal($this->tripId, 1);
  }

  /**
   * Run this action when trip type was changed from Foreman to Carrier.
   *
   * @param $trip
   *   Object new trip data.
   */
  public function actionTripTypeWasChangedToCarrier($trip) : void {
    // Remove parking for trip when type is personally.
    $this->deleteTripParking();

    // Remove all TP Delivery jobs for trip.
    self::deleteAllTpDeliveryInTrip($this->tripId);

    // Remove all foremans.
    db_delete('ld_trip_foreman')
      ->condition('ld_trip_id', $this->tripId)
      ->execute();

    // Remove all helpers.
    db_delete('ld_trip_helper')
      ->condition('ld_trip_id', $this->tripId)
      ->execute();

    // Update all request jobs for trip by carrier.
    if (!empty($trip->carrier->carrier_id)) {
      $this->updateJobToCarrierTrip();
      $job_inst = new LongDistanceJob();
      // We need to update trip total.
      $job_inst->updateClosingTotal($this->tripId);
    }
  }

  /**
   * @param $trip
   * @param $old_trip
   */
  public function actionTripCarrierWasChanged($trip, $old_trip) : void {
    // If old carrier is empty.
    // Update all request jobs for trip by carrier.
    if (!empty($trip->carrier->carrier_id) && empty($old_trip['carrier']['carrier_id'])) {
      $this->updateJobToCarrierTrip();
      $job_inst = new LongDistanceJob();
      // We need to update trip total.
      $job_inst->updateClosingTotal($this->tripId);
    }
    // Old and new carrier not empty.
    elseif (!empty($trip->carrier->carrier_id) && !empty($old_trip['carrier']['carrier_id'])) {
      // Remove all jobs.
      $this->removeAllJobsFromTrip();
    }
  }

  /**
   * @param $trip
   * @param $old_trip
   */
  public function actionTripForemanWasChanged($trip, $old_trip) : void{

    $payroll = new LongDistancePayroll($this->tripId);
    $payroll_foreman = LongDistancePayroll::getPayrollForemanId($this->tripId);

    if (!empty($payroll_foreman)) {
      // Sync foreman for payroll.
      if ($old_trip['foreman']['foreman_id'] != $trip->foreman['foreman_id']) {
        $payroll->updateForemanUid($trip->foreman['foreman_id']);
      }
      // Sync helper for payroll.
      $payroll = new LongDistancePayroll($this->tripId);
      $payroll->updateHelpers($trip->helper);
    }


    // Update foreman.
    db_delete('ld_trip_foreman')
      ->condition('ld_trip_id', $this->tripId)
      ->execute();

    $foreman['ld_trip_id'] = $this->tripId;
    db_merge('ld_trip_foreman')
      ->key(array('ld_trip_id' => $this->tripId, 'foreman_id' => $trip->foreman['foreman_id']))
      ->fields($foreman)
      ->execute();

    // Update helpers.
    db_delete('ld_trip_helper')
      ->condition('ld_trip_id', $this->tripId)
      ->execute();
    if (!empty($trip->helper)) {
      foreach ($trip->helper as $helper) {
        $helper['ld_trip_id'] = $this->tripId;
        db_merge('ld_trip_helper')
          ->key(array('ld_trip_id' => $this->tripId, 'helper_id' => $helper['helper_id']))
          ->fields($helper)
          ->execute();
      }
    }

    // Add or remove trucks unavailable for trip.
    if ($old_trip['details']['flag'] != $trip->details->flag && !empty($old_trip['details']['flag'])) {
      // Add truck from Unavailable.
      if (in_array($trip->details->flag, $this->tripStatusesForAddTruckUnavailable)) {
        $this->addTruckUnavailable();
      }
      else {
        // Remove truck from Unavailable.
        $this->removeTruckUnavailable();
      }
    }
  }

  /**
   * Check if trip has carrier.
   *
   * @param int $trip_id
   *   Id of the trip.
   *
   * @return mixed
   *   Carrier or FALSE.
   */
  public static function tripHasCarrier($trip_id) {
    $carrier = self::getTripCarrier($trip_id);
    return $carrier;
  }

  /**
   * Check if receipts array exist.
   *
   * @param array $receipts
   *   Array with receipts.
   * @param int $transaction_type
   *   Transaction type to search.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isTransactionTypeExistInReceipt(array $receipts, int $transaction_type) : bool {
    if (!empty($receipts)) {
      $transactions = array_column($receipts, 'transaction_type');
      if (in_array($transaction_type, $transactions)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get payroll status.
   *
   * @param int $trip_id
   *   Id of the trip.
   *
   * @return mixed
   *   If exist return payroll status. If not false.
   */
  public static function tripPayrollStatus(int $trip_id) {
    $payroll = db_select('ld_trip_payroll_details', 'payroll_details')
      ->fields('payroll_details', array('status'))
      ->condition('ld_trip_id', $trip_id)
      ->execute()
      ->fetchField();

    return $payroll;
  }

  /**
   * @param $title
   * @param $text
   * @param string $event_type
   */
  public function setLogs($title, $text, $event_type = 'Trip') {
    global $user;
    $user_name = isset($user->name) ? $user->name : 'Anonymous';
    $log_data[] = array(
      'details' => array([
        'event_type' => $event_type,
        'activity' => $user_name,
        'title' => ucfirst(preg_replace('/_/', ' ', $title)),
        'text' => $text,
        'date' => time(),
      ],
      ),
      'source' => $user_name,
    );
    $log = new Log($this->tripId, EntityTypes::LDTRIP);
    $log->create($log_data);
  }


  /**
   * Check required fields.
   *
   * @param object $fields
   *   Fields for check.
   */
  public function requireFieldValidation(&$fields) {
    switch ($fields->details->type) {
      case TripType::CARRIER:
        $required = array(
          'details' => array('type', 'flag'),
          'carrier' => array(
            'carrier_id',
            'driver_name',
            'driver_phone',
          ),
        );
        break;

      case TripType::PERSONALLY:
        $required = array();
        break;
    }
    if (!empty($required)) {
      foreach ($required as $field_name => $field) {
        if (!is_array($field) && !is_object($field)) {
          if (property_exists($fields, $field)) {
            if (empty($fields->{$field})) {
              $fields->{$field} = 'error';
              $fields->error = TRUE;
            }
          }
        }
        else {
          foreach ($field as $key) {
            // For array.
            if (is_array($fields->{$field_name})) {
              if (empty($fields->{$field_name}[$key])) {
                $fields->{$field_name}[$key] = 'error';
                $fields->error = TRUE;
              }
            }
            // For object.
            elseif (is_object($fields->{$field_name})) {
              if (empty($fields->{$field_name}->{$key})) {
                $fields->{$field_name}->{$key} = 'error';
                $fields->error = TRUE;
              }
            }
          }
        }
      }
    }
  }

  /**
   * Remove all trucks Unavailable for trip.
   */
  public function removeTruckUnavailable() : void {
    $parking_inst = new Parking();
    $parking = $parking_inst->getEntityParking($this->tripId, EntityTypes::LDTRIP);
    if (!empty($parking)) {
      foreach ($parking as $row) {
        $dates = $parking_inst->dateDiffInDateArray($row['from_date'], $row['to_date']);
        $parking_inst->removeTruckUnavailableForTrip($row['truck_id'], $dates);
      }
    }
  }

  /**
   * Add Truck Unavailable for trip bu date.
   */
  public function addTruckUnavailable() : void {
    $parking_inst = new Parking();
    $parking = $parking_inst->getEntityParking($this->tripId, EntityTypes::LDTRIP);
    if (!empty($parking)) {
      foreach ($parking as $row) {
        $dates = $parking_inst->dateDiffInDateArray($row['from_date'], $row['to_date']);
        $parking_inst->addTruckUnavailableForTrip($row['truck_id'], $dates);
      }
    }
  }

  /**
   * Get diff between old and new trip changes.
   *
   * @param array $old
   *   Old values.
   * @param object $new
   *   New values.
   *
   * @return array
   *   Differenc between values.
   */
  public static function diffOnUpdate($old, $new) {
    $new = (array) $new;
    $text = array();
    $statuses = self::tripStatuses();
    $excluded_fields = array(
      'foreman_id',
      'job_id',
      'payroll',
      'trip_id',
      'jobs',
      'payroll_status',
      'created',
      'trucks',
      'trailers',
    );
    $trip_types = array(1 => 'Carrier/Agent', 2 => 'Foreman/Helper');

    if ($old['foreman'] === FALSE) {
      $old['foreman'] = array();
    }
    foreach ($old as $field_name => $val) {
      if (!in_array($field_name, $excluded_fields)) {
        if ($field_name == 'helper') {
          $helper_text = self::helperDiff($new['helper'], $old['helper']);
          $text = !empty($text) ? array_merge($text, $helper_text) : $helper_text;
        }
        else {
          if ((is_array($val) || is_object($val))) {
            $new[$field_name] = (array) $new[$field_name];
            $diff = array_diff_assoc($new[$field_name], $old[$field_name]);
            if (!empty($diff)) {
              foreach ($diff as $diff_name => $diff_val) {
                if (!in_array($diff_name, $excluded_fields)) {
                  $old_val = !empty($old[$field_name][$diff_name]) ? $old[$field_name][$diff_name] : FALSE;
                  $new_val = !empty($new[$field_name][$diff_name]) ? $new[$field_name][$diff_name] : FALSE;
                  $name = ucfirst(str_replace('_', ' ', $diff_name));
                  switch ($diff_name) {
                    case 'flag':
                      $name = 'Status';
                      $old_val = !empty($old_val) ? $statuses[$old_val] : FALSE;
                      $new_val = !empty($new_val) ? $statuses[$new_val] : FALSE;
                      break;

                    case 'date_start':
                    case 'date_end':
                      $old_val = date('m/d/Y', $old_val);
                      $new_val = date('m/d/Y', $new_val);
                      break;

                    case 'type':
                      $old_val = !empty($old_val) ? $trip_types[$old_val] : FALSE;
                      $new_val = !empty($new_val) ? $trip_types[$new_val] : FALSE;
                      break;
                  }
                  if (empty($old_val)) {
                    $text[]['simpleText'] = $name . ' "' . $new_val . '"" was added';
                  }
                  elseif (empty($new_val)) {
                    $text[]['simpleText'] = $name . ' "' . $old_val . '"" was removed';
                  }
                  else {
                    $text[]['simpleText'] = $name . ' was changed from "' . $old_val . '"" to "' . $new_val . '"';
                  }
                }
              }
            }
          }
          else {
            $name = ucfirst(str_replace('_', ' ', $field_name));
            if (($old[$field_name] != $new[$field_name]) && !empty($new[$field_name])) {
              if (empty($old[$field_name])) {
                $text[]['simpleText'] = $name . ' "' . $new[$field_name] . '" was added';
              }
              elseif (empty($new[$field_name])) {
                $text[]['simpleText'] = $name . ' "' . $old[$field_name] . '" was removed';
              }
              else {
                $text[]['simpleText'] = $name . ' was changed from "' . $old[$field_name] . '" to "' . $new[$field_name] . '"';
              }
            }
          }
        }
      }

    }
    return $text;
  }

  public static function helperDiff($new, $old) {
    $text = array();
    $new_helpers = array();
    $old_helpers = array();
    if (!empty($new)) {
      foreach ($new as $val) {
        $new_helpers[$val['helper_id']] = $val['helper_name'];
      }
    }
    if (!empty($old)) {
      foreach ($old as $val) {
        $old_helpers[$val['helper_id']] = $val['helper_name'];
      }
    }

    $diff_added = array_diff_assoc($new_helpers, $old_helpers);
    if (!empty($diff_added)) {
      foreach ($diff_added as $item) {
        $text[]['simpleText'] = 'Helper "' . $item . '"" was added';
      }
    }
    $diff_removed = array_diff_assoc($old_helpers, $new_helpers);
    if (!empty($diff_removed)) {
      foreach ($diff_removed as $item) {
        $text[]['simpleText'] = 'Helper "' . $item . '"" was removed';
      }
    }
    return $text;
  }

  /**
   * Clean all.
   *
   * @return string
   *   Deleted.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public static function cleanAll() {
    $tables_to_clear = array(
      'ld_trip_closing',
      'ld_invoices',
      'ld_invoices_details',
      'ld_trip',
      'ld_carrier',
      'ld_trip_carrier',
      'ld_trip_closing',
      'ld_trip_temp_closing',
      'ld_trip_details',
      'ld_trip_foreman',
      'ld_trip_helper',
      'ld_trip_end_in',
      'ld_trip_extra_services',
      'ld_trip_index',
      'ld_trip_jobs',
      'ld_trip_receipts',
      'ld_trip_receipts_details',
      'ld_trip_services',
      'ld_trip_start_from',
      'ld_carrier_phones',
      'ld_request_address_from',
      'ld_request_address_to',
      'ld_invoices_receipts',
      'ld_request_sit',
      'ld_request',
      'ld_tp_delivery',
      'ld_tp_delivery_details',
      'ld_tp_delivery_phones',
      'ld_tp_delivery_job',
      'ld_trip_payroll_details',
      'ld_trip_payroll_expenses',
      'ld_trip_payroll_received',
      'ld_trip_foreman_payroll_closing',
      'ld_trip_helper_payroll_closing',
      'ld_agent_folio_closing',
    );
    $all_receipts = db_select('ld_trip_receipts', 'ltr')
      ->fields('ltr', array('receipt_id'))
      ->execute()
      ->fetchCol();
    // Remove all receipts.
    if ($all_receipts) {
      db_delete('moveadmin_receipt')
        ->condition('id', $all_receipts, 'IN')
        ->execute();
    }

    $all_invoices = db_select('ld_invoices', 'li')
      ->fields('li', array('invoice_id'))
      ->execute()
      ->fetchCol();
    // Remove all invoices.
    if ($all_invoices) {
      db_delete('move_invoice')
        ->condition('id', $all_invoices, 'IN')
        ->execute();
    }
    foreach ($tables_to_clear as $table_name) {
      db_delete($table_name)
        ->execute();
    }

    // Set all request to not in trip.
    db_update('field_data_field_added_to_trip')
      ->fields(array('field_added_to_trip_value' => 0))
      ->execute();
    db_update('field_revision_field_added_to_trip')
      ->fields(array('field_added_to_trip_value' => 0))
      ->execute();

    $ld_jobs = db_select('field_data_field_move_service_type', 'service')
      ->fields('service', array('entity_id'))
      ->condition('field_move_service_type_value', 7)
      ->execute()
      ->fetchCol();

    if (!empty($ld_jobs)) {
      db_delete('moveadmin_receipt')
        ->condition('entity_id', $ld_jobs, 'IN')
        ->condition('entity_type', EntityTypes::MOVEREQUEST)
        ->execute();
      foreach ($ld_jobs as $nid) {
        $cache_data = Cache::getCacheData($nid, 'move_services_cache');
        unset($cache_data['ld_job']);
        Cache::setCacheData($nid, $cache_data);
      }
    }
    db_delete('moveadmin_receipt')
      ->condition('entity_type', array(7, 8, 9, 10, 11))
      ->execute();

    db_delete('move_invoice')
      ->condition('entity_type', array(7, 8, 9, 10, 11))
      ->execute();
    drupal_flush_all_caches();
    return 'Deleted';
  }

  public static function dateFiltering($condition, \SearchApiQueryInterface &$query) {

    if (!empty($condition['field_date'])) {
      if (empty($condition['field_date']['from'])) {
        $query->condition('field_date', $condition['field_date']['to'], '>');

      }
      else {
        $query->condition('field_date', $condition['field_date'], 'BETWEEN');
      }
    }

    if (!empty($condition['field_delivery_date'])) {
      if (empty($condition['field_delivery_date']['from'])) {
        $query->condition('field_delivery_date', $condition['field_date']['to'], '>');

      }
      else {
        $query->condition('field_delivery_date', $condition['field_delivery_date'], 'BETWEEN');
      }
    }

    return $query;
  }

  /**
   * Statuses in string format.
   *
   * @return array
   */
  public static function tripStatuses() {
    $statuses = array(
      1 => 'DRAFT',
      2 => 'PENDING',
      3 => 'ACTIVE',
      4 => 'DELIVERED',
    );
    return $statuses;
  }

  /**
   * Sort by type closing jobs for trip
   * @param $jobs
   * @param $direction
   */
  private function sortClosingJobsByType(&$jobs, $direction) : void {
    $function_name = $direction == 'ASC' ? 'sort_by_type_acs' : 'sort_by_type_desc';
    uasort($jobs, $function_name);
  }
}
