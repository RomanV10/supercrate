<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_long_distance\Services\LongDistanceSit;
class LongDistanceSitTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new LongDistanceSit();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function sit() {

    $file_path = dirname(__FILE__) . '/data/sit.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('sit_update', $parse_json['data']);
  }


  public function testRetriveSit() {
    $rsid = 5314;
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertGreaterThanOrEqual(3, $ret);
  }


  public function testUpdateSit() {
    self::sit();
    $rs_up = variable_get('sit_update');
    $rsid = $rs_up['ld_nid'];
    $rs_update = new LongDistanceSit($rsid);
    $upd = $rs_update->update($rs_up);
    $this->assertEquals(2, $upd);
  }

}