<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_long_distance\Services\LongDistanceStorage;
class LongDistanceCarrierTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new LongDistanceStorage();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createStorage() {
    $file_path = dirname(__FILE__) . '/data/storage.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('storage', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/storageUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('storage_update', $parse_json['data']);
  }

  public function testCreateStorage() {
    self::createStorage();
    $rsp = variable_get('storage');
    $rsid = $this->fixture->create($rsp);
    $this->assertGreaterThanOrEqual(1, $rsid);

    return (int) $rsid;
  }

  /**
   * @depends testCreateStorage
   */
  public function testRetriveStorage($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertGreaterThanOrEqual(3, $ret);
  }

  /**
   * @depends testCreateStorage
   */

  public function testIndexStorage() {
    $ret = $this->fixture->index();
    $this->assertGreaterThanOrEqual(1, $ret);
  }
  /**
   * @depends testCreateStorage
   */
  public function testUpdateStorage($rsid) {
    $rs_up = variable_get('storage_update');
    $rs_update = new LongDistanceStorage($rsid);
    $upd = $rs_update->update($rs_up, TRUE);
    $this->assertEquals(2, $upd);
  }

}