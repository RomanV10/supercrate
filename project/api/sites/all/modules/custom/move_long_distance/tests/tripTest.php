<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_long_distance\Services\LongDistanceTrip;

class LongDistanceTripTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new LongDistanceTrip();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createTrip() {
    $file_path = dirname(__FILE__) . '/data/trip.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('trip', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/tripUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('trip_update', $parse_json['data']);
  }

  public function testCreateTrip() {
    self::createTrip();
    $rsp = variable_get('trip');
    $rsid = $this->fixture->create($rsp);
    $this->assertGreaterThanOrEqual(1, $rsid);

    return (int) $rsid;
  }

  /**
   * @depends testCreateTrip
   */
  public function testRetriveTrip($rsid) {
    var_dump($rsid);

    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertCount(8, $ret);
  }

  /**
   * @depends testCreateTrip
   */
  public function testUpdateTrip($rsid) {
    $rs_up = variable_get('trip_update');
    $rs_update = new LongDistanceTrip($rsid);
    $upd = $rs_update->update($rs_up, TRUE);
    $this->assertEquals(2, $upd);
  }
//
//  /**
//   * @depends testCreateTrip
//   */
//  public function testDeleteTrip($rsid) {
//    $rsc_delete = new LongDistanceTrip($rsid);
//    $del = $rsc_delete->delete();
//    $this->assertEquals(1, $del);
//  }

}