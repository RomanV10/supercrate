<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_long_distance\Services\LongDistanceCarrier;
class LongDistanceCarrierTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new LongDistanceCarrier();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createCarrier() {
    $file_path = dirname(__FILE__) . '/data/carrier.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('carrier', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/carrierUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('carrier_update', $parse_json['data']);
  }

  public function testCreateCarrier() {
    self::createCarrier();
    $rsp = variable_get('carrier');
    $rsid = $this->fixture->create($rsp);
    $this->assertGreaterThanOrEqual(1, $rsid);

    return (int) $rsid;
  }

  /**
   * @depends testCreateCarrier
   */
  public function testRetriveCarrier($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertGreaterThanOrEqual(3, $ret);
  }

  /**
   * @depends testCreateCarrier
   */
  public function testUpdateCarrier($rsid) {
    $rs_up = variable_get('carrier_update');
    $rs_update = new LongDistanceCarrier($rsid);
    $upd = $rs_update->update($rs_up, TRUE);
    $this->assertEquals(2, $upd);
  }
//
//  /**
//   * @depends testCreateCarrier
//   */
//  public function testDeleteCarrier($rsid) {
//    $rsc_delete = new LongDistanceCarrier($rsid);
//    $del = $rsc_delete->delete();
//    $this->assertEquals(1, $del);
//  }

}