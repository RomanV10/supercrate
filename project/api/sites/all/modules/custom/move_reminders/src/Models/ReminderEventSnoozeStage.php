<?php

namespace Drupal\move_reminders\Models;

use Illuminate\Support\Collection;

/**
 * Class ReminderEventSnoozeStage.
 *
 * @package Drupal\move_reminders\Models
 *
 * @property int $id
 * @property int $event_id
 * @property int $snoozed_for
 * @property int $snoozed_to
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class ReminderEventSnoozeStage extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'move_reminders_events_stages';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'event_id',
    'snoozed_for',
    'snoozed_to',
  ];

  /**
   * Retrieves snooze stages for many events by events identifiers.
   *
   * @param array $events_ids
   *   Reminders events identifiers.
   *
   * @return Collection|self[]
   *   Events snooze stages collection.
   */
  public static function allByEventsIds(array $events_ids): Collection {
    if (!empty($events_ids)) {
      $raw_select_result = db_select(static::$table, static::$table)
        ->condition('event_id', $events_ids, 'IN')
        ->fields(static::$table)->execute();


      if ($raw_select_result !== NULL && $raw_select_result->rowCount()) {
        $items = $raw_select_result->fetchAll(\PDO::FETCH_ASSOC);
      }
    }

    return static::newCollection($items ?? [], TRUE);
  }

}
