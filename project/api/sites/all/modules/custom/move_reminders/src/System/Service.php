<?php

namespace Drupal\move_reminders\System;

/**
 * Class Service.
 *
 * @package Drupal\move_reminders\System
 */
class Service {

  /**
   * Default resources API version.
   *
   * @var int
   */
  protected static $apiVersion = 3002;

  /**
   * Module resources definition array.
   *
   * @return array
   *   Resources definition(routes and API version).
   */
  public function resources(): array {
    return ['#api_version' => self::$apiVersion] + static::routes();
  }

  /**
   * Module routes definition array.
   *
   * @return array
   *   Resources routes(operations and actions).
   */
  protected static function routes(): array {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_reminders',
      'name' => 'src/System/Controller',
    ];

    return [
      'move_reminders' => [
        'operations' => [
          'create' => [
            'callback' => Controller::class . '::store',
            'file' => $controller_file,
            'args' => [
              [
                'name' => 'attributes',
                'type' => 'array',
                'source' => 'data',
                'optional' => FALSE,
              ],
            ],
            'access arguments' => ['access content'],
          ],
          'update' => [
            'callback' => Controller::class . '::update',
            'file' => $controller_file,
            'args' => [
              [
                'name' => 'reminder_id',
                'type' => 'int',
                'source' => ['path' => 0],
                'default value' => NULL,
                'optional' => FALSE,
              ],
              [
                'name' => 'attributes',
                'type' => 'array',
                'source' => 'data',
                'optional' => FALSE,
              ],
            ],
            'access arguments' => ['access content'],
          ],
        ],
        'actions' => [
          'get_for_user' => [
            'callback' => Controller::class . '::getForUser',
            'file' => $controller_file,
            'args' => [
              [
                'name' => 'user_id',
                'type' => 'int',
                'source' => ['data' => 'user_id'],
                'optional' => FALSE,
              ],
              [
                'name' => 'from',
                'type' => 'int',
                'source' => ['data' => 'from'],
                'optional' => FALSE,
              ],
              [
                'name' => 'to',
                'type' => 'int',
                'source' => ['data' => 'to'],
                'optional' => FALSE,
              ],
            ],
            'access arguments' => ['access content'],
          ],
          'all_for_node' => [
            'callback' => Controller::class . '::allForNode',
            'file' => $controller_file,
            'args' => [
              [
                'name' => 'node_id',
                'type' => 'int',
                'source' => ['data' => 'node_id'],
                'optional' => FALSE,
              ],
            ],
            'access arguments' => ['access content'],
          ],
          'snooze' => [
            'callback' => Controller::class . '::snooze',
            'file' => $controller_file,
            'args' => [
              [
                'name' => 'reminder_id',
                'type' => 'int',
                'source' => ['data' => 'reminder_id'],
                'optional' => FALSE,
              ],
              [
                'name' => 'snoozed_for',
                'type' => 'int',
                'source' => ['data' => 'snoozed_for'],
                'optional' => FALSE,
              ],
              [
                'name' => 'snoozed_to',
                'type' => 'int',
                'source' => ['data' => 'snoozed_to'],
                'optional' => FALSE,
              ],
            ],
            'access arguments' => ['access content'],
          ],
          'dismiss' => [
            'callback' => Controller::class . '::dismiss',
            'file' => $controller_file,
            'args' => [
              [
                'name' => 'reminder_id',
                'type' => 'int',
                'source' => ['data' => 'reminder_id'],
                'optional' => FALSE,
              ],
              [
                'name' => 'dismissed_at',
                'type' => 'int',
                'source' => ['data' => 'dismissed_at'],
                'optional' => FALSE,
              ],
            ],
            'access arguments' => ['access content'],
          ],
        ],
      ],
    ];
  }

}
