<?php

namespace Drupal\move_reminders\Models;

use Illuminate\Support\Collection;
use JsonSerializable;

/**
 * Class AbstractModel.
 *
 * @package Drupal\move_reminders\Models
 *
 * @property int $created_at
 * @property int $updated_at
 */
abstract class AbstractModel implements JsonSerializable {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table;

  /**
   * The primary key for the model.
   *
   * @var array|string
   */
  public static $primaryKey = 'id';

  /**
   * The model's attributes.
   *
   * @var array
   */
  protected $attributes = [];

  /**
   * The model attribute's original state.
   *
   * @var array
   */
  protected $original = [];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];

  /**
   * Indicates if the instance exists in DB.
   *
   * @var bool
   */
  public $exists = FALSE;

  /**
   * Find a model's instance by its primary key or throw an exception.
   *
   * @param mixed $id
   *   Primary key value.
   *
   * @return $this
   *
   * @throws ModelNotFoundException
   */
  public static function find($id) {
    $raw_select_result = db_select(static::$table, 'alias')
      ->condition(static::$primaryKey, $id)
      ->fields('alias')->execute();

    if (NULL !== $raw_select_result && $raw_select_result->rowCount()) {
      $attributes = $raw_select_result->fetchAssoc(\PDO::FETCH_ASSOC);

      return static::newInstance($attributes, TRUE);
    }

    throw (new ModelNotFoundException)->setModel(static::class);
  }

  /**
   * Creates a new model's instance and saves its into database.
   *
   * @param array $attributes
   *   Instance attributes.
   *
   * @return $this
   */
  public static function create(array $attributes) {
    return static::newInstance($attributes)->save();
  }

  /**
   * Creates a new model's instance.
   *
   * @param array $attributes
   *   Instance attributes.
   * @param bool $exists
   *   Is instance saved to database.
   *
   * @return $this
   */
  public static function newInstance(array $attributes = [], $exists = FALSE) {
    if ($exists) {
      $instance = new static();

      $instance->attributes = $instance->original = $attributes;
    }
    else {
      $instance = new static($attributes);
    }

    $instance->exists = $exists;

    return $instance;
  }

  /**
   * Creates a new model's instances collection.
   *
   * @param array $items
   *   Model's instances attributes.
   * @param bool $exists
   *   Is instance exists in DB.
   *
   * @return Collection
   *   Model's instances collection.
   */
  public static function newCollection(array $items, $exists = FALSE): Collection {
    return new Collection(array_map(function ($attributes) use ($exists) {
      return static::newInstance($attributes, $exists);
    }, $items));
  }

  /**
   * AbstractModel constructor.
   *
   * @param array $attributes
   *    Instance attributes.
   */
  protected function __construct(array $attributes = []) {
    $this->fill($attributes);
  }

  /**
   * Get the value of the relation's primary key.
   *
   * @return string|int
   */
  public function getKey() {
    if ($this->exists && isset($this->original, static::$primaryKey)) {
      return $this->original[static::$primaryKey];
    }

    return $this->getAttribute(static::$primaryKey);
  }

  /**
   * Set a primary key value.
   *
   * @param $id
   */
  public function setKey($id) {
    $this->setAttribute(self::$primaryKey, $id);
  }

  /**
   * Prepares drupal database condition for primary key.
   *
   * @return \DatabaseCondition|\QueryConditionInterface
   */
  protected function getKeyCondition() {
    $condition = db_and();

    $condition->condition(static::$primaryKey, $this->getKey());

    return $condition;
  }

  /**
   * Get an attribute from the model.
   *
   * @param  string $key
   * @return mixed
   */
  public function getAttribute($key) {
    if (array_key_exists($key, $this->attributes)) {
      return $this->attributes[$key];
    }

    return NULL;
  }

  /**
   * Set a given attribute on the model.
   *
   * @param  string $key
   * @param  mixed $value
   * @return $this
   */
  public function setAttribute($key, $value) {
    $this->attributes[$key] = $value;

    return $this;
  }

  /**
   * Get a selection of the current attributes on the model.
   *
   * @param array $keys
   *
   * @return array
   */
  public function getAttributes(array $keys = []) {
    if ($keys === []) {
      return $this->attributes;
    }

    $keys = is_array($keys) ? $keys : func_get_args();

    $result = [];

    foreach ($keys as $key) {
      $value = $this->getAttribute($key);

      if (!is_null($value)) {
        $result[$key] = $value;
      }
    }

    return $result;
  }

  /**
   * Fill the model with an array of attributes.
   *
   * @param  array $attributes
   * @return $this
   */
  public function fill(array $attributes) {
    $attributes = array_intersect_key($attributes, array_flip($this->fillable));

    foreach ($attributes as $key => $value) {
      $this->setAttribute($key, $value);
    }

    return $this;
  }

  /**
   * Determine if the model or given attribute(s) have been modified.
   *
   * @param  array|string|null $attributes
   * @return bool
   */
  public function isDirty($attributes = NULL) {
    $dirty = $this->getDirty();

    if (is_null($attributes)) {
      return count($dirty) > 0;
    }

    if (!is_array($attributes)) {
      $attributes = func_get_args();
    }

    foreach ($attributes as $attribute) {
      if (array_key_exists($attribute, $dirty)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Get the attributes that have been changed since last save.
   *
   * @return array
   */
  public function getDirty() {
    $dirty = [];

    foreach ($this->attributes as $key => $value) {
      if (!array_key_exists($key, $this->original)) {
        $dirty[$key] = $value;
      }
      elseif ($value !== $this->original[$key] &&
        !is_numbers_equals($this->attributes[$key], $this->original[$key])
      ) {
        $dirty[$key] = $value;
      }
    }

    return $dirty;
  }

  /**
   * Saves the model attributes to the database.
   */
  public function save() {
    if ($this->isDirty()) {
      if (!$this->exists) {
        if (!$this->isDirty('created_at')) {
          $this->created_at = time();
        }

        $this->setKey(db_insert(static::$table)->fields($this->getDirty())->execute());
      }
      else {
        if (!$this->isDirty('updated_at')) {
          $this->updated_at = time();
        }

        db_update(static::$table)->condition($this->getKeyCondition())
          ->fields($this->getDirty())->execute();
      }

      $this->original = $this->attributes;
    }

    return $this;
  }

  public function fresh() {
    if ($this->exists) {
      $fresh_instance = static::find($this->getKey());

      $this->attributes = $fresh_instance->attributes;
      $this->original = $fresh_instance->original;
    }
  }

  /**
   * Removes model attributes from the database.
   */
  public function delete() {
    db_delete(static::$table)->condition($this->getKeyCondition())->execute();
  }

  /**
   * Dynamically retrieve attributes on the model.
   *
   * @param  string $key
   * @return mixed
   */
  public function __get($key) {
    return $this->getAttribute($key);
  }

  /**
   * Dynamically set attributes on the model.
   *
   * @param  string $key
   * @param  mixed $value
   * @return void
   */
  public function __set($key, $value) {
    $this->setAttribute($key, $value);
  }

  /**
   * Determine if an attribute exists on the model.
   *
   * @param  string $key
   * @return bool
   */
  public function __isset($key) {
    return !is_null($this->getAttribute($key));
  }

  /**
   * Unset an attribute on the model.
   *
   * @param  string $key
   * @return void
   */
  public function __unset($key) {
    unset($this->attributes[$key]);
  }

  /**
   * Convert the model to its string representation.
   *
   * @return string
   */
  public function __toString() {
    return json_encode($this->jsonSerialize());
  }

  /**
   * Convert the object into something JSON serializable.
   *
   * @return array
   */
  public function jsonSerialize() {
    return $this->getAttributes();
  }

}
