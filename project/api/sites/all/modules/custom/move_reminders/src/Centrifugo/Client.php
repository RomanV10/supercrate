<?php

namespace Drupal\move_reminders\Centrifugo;

/**
 * Class Client.
 *
 * @package Drupal\move_reminders\System
 */
class Client extends \phpcent\Client {

  /**
   * @var static
   */
  protected static $instance;

  /**
   * @return static
   */
  public static function instance() {
    if (!is_a(static::$instance, static::class)) {
      $client = new Client(variable_get('centrifugo_url', 'socket.themoveboard.com:9095'));

      $client->setSecret(variable_get('centrifugo_token', ''));
      $client->setTransport(new Transport());

      static::$instance = $client;
    }

    return static::$instance;
  }

}
