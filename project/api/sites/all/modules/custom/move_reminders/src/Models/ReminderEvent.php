<?php

namespace Drupal\move_reminders\Models;

use Drupal\move_reminders\Models\ReminderEventSnoozeStage as SnoozeStage;
use Illuminate\Support\Collection;

/**
 * Class AbstractEvent.
 *
 * @package Drupal\move_reminders\Models
 *
 * @property int $id
 * @property int $user_id
 * @property string $subject
 * @property string $description
 * @property string|null $color
 * @property int $starts_at
 * @property int $ends_at
 * @property int|null $remind_for
 * @property int|null $remind_at
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $dismissed_at
 */
class ReminderEvent extends AbstractModel {

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'move_reminders_events';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'user_id',
    'subject',
    'description',
    'color',
    'starts_at',
    'ends_at',
    'remind_for',
    'remind_at',
    'created_at',
    'updated_at',
  ];

  /**
   * Last reminder event snooze stage model instance.
   *
   * @var SnoozeStage|null
   */
  protected $lastSnooze;

  /**
   * Creates a new reminder event's instances collection.
   *
   * Loads actual clients data. Adds snooze stages to reminders events.
   *
   * @inheritdoc
   */
  public static function newCollection(array $items, $exists = FALSE): Collection {
    $events = parent::newCollection($items, $exists);

    $events_ids = $events->pluck('id')->toArray();

    $snooze_stages = SnoozeStage::allByEventsIds($events_ids)->groupBy('event_id');

    return $events->each(function (self $event) use ($snooze_stages) {
      /** @var Collection|SnoozeStage[] $event_snooze_stages */
      $event_snooze_stages = $snooze_stages->get($event->getKey(), new Collection());

      $event->lastSnooze = $event_snooze_stages->sortBy('id')->last();
    });
  }

  /**
   * Retrieves reminders events for user by timestamps range.
   *
   * @param int $user_id
   *   User identifier.
   * @param int $from
   *   From date timestamp.
   * @param int $to
   *   To date timestamp.
   * @param int|null $skip_to_id
   *   Last requested identifier.
   *
   * @return Collection|ReminderEvent[]
   *   Collection of reminders events.
   */
  public static function getForUser(int $user_id, int $from = NULL, int $to = NULL, int $skip_to_id = NULL) {
    $conditions = "events.user_id = $user_id ";

    if ($from !== NULL && $to !== NULL) {
      $conditions .= " AND ((events.starts_at BETWEEN $from AND $to) OR ((events2.remind_at > events.starts_at) AND (events2.remind_at BETWEEN $from AND $to))) ";
    }

    if ($skip_to_id !== NULL) {
      $primary_key = static::$primaryKey;
      $conditions .= " AND (events.$primary_key >= $skip_to_id)";
    }

    $table_name = static::$table;
    $raw_select_result = db_query("SELECT DISTINCT events.* FROM $table_name AS events, $table_name AS events2 WHERE $conditions");
    $select_result = $raw_select_result->fetchAll(\PDO::FETCH_ASSOC);

    if ($select_result) {
      $events = static::newCollection($select_result, TRUE);
    }

    return $events ?? new Collection();
  }

  /**
   * Get a selection of the current attributes on the model.
   *
   * Overrides remind_at attribute with last snooze stage data. Overrides
   * user_name attribute with actual user data.
   *
   * @inheritdoc
   */
  public function getAttributes(array $keys = []): array {
    $attributes = parent::getAttributes($keys);

    if (empty($keys) || in_array('snoozed_for', $keys, TRUE)) {
      $attributes['snoozed_for'] = is_a($this->lastSnooze, SnoozeStage::class)
        ? $this->lastSnooze->snoozed_for : NULL;
    }

    if ((empty($keys) || in_array('remind_at', $keys, TRUE))
      && is_a($this->lastSnooze, SnoozeStage::class)
    ) {
      $attributes['remind_at'] = $this->lastSnooze->snoozed_to;
    }

    return $attributes;
  }

  /**
   * Snoozes reminder event to date.
   *
   * @param int $snooze_to
   *   Snooze to date timestamp.
   *
   * @return $this
   */
  public function snooze(int $snoozed_for, int $snoozed_to) {
    $attributes = [
      'snoozed_for' => $snoozed_for,
      'snoozed_to' => $snoozed_to,
    ];

    if ($this->lastSnooze !== NULL && !$this->lastSnooze->exists) {
      $this->lastSnooze->fill($attributes);
    }
    else {
      $this->lastSnooze = new SnoozeStage($attributes);
    }

    return $this;
  }

  /**
   * Dismisses reminder event for date.
   *
   * @param int $dismissed_at
   *   Dismissing date timestamp.
   *
   * @return $this
   */
  public function dismiss(int $dismissed_at) {
    $this->setAttribute('dismissed_at', $dismissed_at);

    return $this;
  }

  /**
   * Saves the reminder event attributes to the database.
   */
  public function save() {
    $transaction = db_transaction();

    parent::save();

    try {
      if (is_a($this->lastSnooze, SnoozeStage::class) && !$this->lastSnooze->exists) {
        $this->lastSnooze->setAttribute('event_id', $this->getKey())->save();
      }
    }
    catch (\Throwable $exception) {
      $transaction->rollback();

      throw $exception;
    }

    return $this;
  }

}
