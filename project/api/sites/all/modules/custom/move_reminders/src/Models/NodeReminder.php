<?php

namespace Drupal\move_reminders\Models;

use Drupal\move_reminders\Models\NodeReminderRelation as Relation;
use Illuminate\Support\Collection;

/**
 * Class NodeReminder.
 *
 * @package Drupal\move_reminders\Models
 *
 * @property-read int|null $node_id
 */
class NodeReminder extends ReminderEvent {

  /**
   * Reminder node relation model instance.
   *
   * @var Relation
   */
  private $nodeRelation;

  /**
   * Creates a new reminder event's instance.
   *
   * Appends relation data.
   *
   * @inheritdoc
   */
  public static function newInstance(array $attributes = [], $exists = FALSE) {
    /** @var self $event */
    $event = parent::newInstance($attributes, $exists);

    if ($exists) {
      $event->nodeRelation = Relation::lastByEventId($event->getKey());
    }

    return $event;
  }

  /**
   * Creates a new reminder event's instances collection.
   *
   * Appends nodes relations to reminders events.
   *
   * @inheritdoc
   *
   * @return Collection|self[]
   *   Collection of reminders events instances with nodes relations.
   */
  public static function newCollection(array $items, $exists = FALSE): Collection {
    $events = parent::newCollection($items, $exists);

    $events_ids = $events->pluck(static::$primaryKey)->toArray();

    $relations = Relation::allByEventsIds($events_ids)->keyBy('event_id');

    return $events->each(function (self $event) use ($relations) {
      $relation = $relations->get($event->getKey(), function () {
        throw (new ModelNotFoundException)->setModel(Relation::class);
      });

      $event->nodeRelation = $relation;

      return $event;
    });
  }

  /**
   * Retrieves all reminders events for node by identifier.
   *
   * @param int $node_id
   *   Node identifier.
   *
   * @return Collection|self[]
   *   Collection of reminders events.
   */
  public static function allForNode(int $node_id): Collection {
    $join_query = db_select(static::$table, 'events');

    $join_query->join(Relation::$table, 'relations', 'events.id = relations.event_id');

    $conditions = db_and()->condition('relation_id', $node_id)
      ->condition('relation_type', Relation::RELATION_TYPE_NODE);

    $raw_select_result = $join_query->condition($conditions)
      ->fields('events')->execute();

    if (NULL !== $raw_select_result && $raw_select_result->rowCount()) {
      $select_result = $raw_select_result->fetchAll(\PDO::FETCH_ASSOC);

      $events = static::newCollection($select_result, TRUE);
    }

    return $events ?? new Collection();
  }

  /**
   * Creates a new reminder event model instance.
   *
   * @param array $attributes
   *   Associative array of model's attributes.
   */
  public function __construct(array $attributes = []) {
    $this->fillable[] = 'node_id';

    $this->nodeRelation = Relation::newInstance([
      'relation_type' => Relation::RELATION_TYPE_NODE,
    ]);

    parent::__construct($attributes);
  }

  /**
   * @inheritdoc
   */
  public function getAttributes(array $keys = []): array {
    $attributes = parent::getAttributes($keys);

    if (empty($keys) || in_array('node_id', $keys, TRUE)) {
      $attributes['node_id'] = $this->nodeRelation->relation_id;

    }

    if (empty($keys) || in_array('customer_name', $keys, TRUE)) {
      $attributes['customer_name'] = $this->nodeRelation->customer_name;
    }

    return $attributes;
  }

  /**
   * Set a given attribute on the model.
   *
   * @param  string $key
   * @param  mixed $value
   * @return $this
   */
  public function setAttribute($key, $value) {
    if ($key === 'node_id') {
      $this->nodeRelation->relation_id = $value;
    }
    else {
      parent::setAttribute($key, $value);

    }

    return $this;
  }

  /**
   * Saves the model attributes to the database.
   */
  public function save() {
    $recently_created = !$this->exists;
    $transaction = db_transaction();

    parent::save();

    if ($recently_created) {
      $this->nodeRelation->event_id = $this->getKey();
    }

    try {
      $this->nodeRelation->save()->fresh();
    }
    catch (\Throwable $exception) {
      $transaction->rollback();

      throw $exception;
    }

    return $this;
  }

}
