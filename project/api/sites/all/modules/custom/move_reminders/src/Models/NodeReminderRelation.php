<?php

namespace Drupal\move_reminders\Models;

use Illuminate\Support\Collection;

/**
 * Class NodeReminderRelation.
 *
 * @package Drupal\move_reminders\Models
 *
 * @property int $event_id
 * @property int $relation_id
 * @property string $relation_type
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class NodeReminderRelation extends AbstractModel {

  /**
   * Currently available relation type.
   *
   * @var string
   */
  const RELATION_TYPE_NODE = 'node';

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public static $table = 'move_reminders_events_relations';

  /**
   * The primary key for the model.
   *
   * @var array
   */
  public static $primaryKey = [
    'event_id',
    'relation_id',
    'relation_type',
  ];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'event_id',
    'relation_id',
    'relation_type',
  ];

  protected static function getBaseQuery() {
    $query = db_select(static::$table, 'relations')->fields('relations');

    $query->leftJoin('node', 'nodes', 'nodes.nid = relations.relation_id');
    $query->leftJoin('field_data_field_user_first_name', 'first_name', 'nodes.uid = first_name.entity_id');
    $query->leftJoin('field_data_field_user_last_name', 'last_name', 'nodes.uid = last_name.entity_id');

    $query->addExpression("CONCAT(first_name.field_user_first_name_value, ' ', last_name.field_user_last_name_value)", 'customer_name');

    return $query;
  }

  /**
   * Finds a relation by its composite primary key or throw an exception.
   *
   * @param $id
   *   Associative array of primary key values.
   *
   * @return $this
   *
   * @throws ModelNotFoundException
   */
  public static function find($id) {
    $conditions = db_and();

    foreach ($id as $field => $value) {
      $conditions = $conditions->condition($field, $value);
    }

    $raw_select_result = self::getBaseQuery()->condition($conditions)->execute();

    if (NULL !== $raw_select_result && $raw_select_result->rowCount()) {
      $attributes = $raw_select_result->fetchAssoc(\PDO::FETCH_ASSOC);

      return static::newInstance($attributes, TRUE);
    }

    throw (new ModelNotFoundException)->setModel(static::class);
  }

  /**
   * Retrieves first relation of specified type for reminder event.
   *
   * @param int $event_id
   *    Reminders event identifier.
   *
   * @return $this
   *
   * @throws ModelNotFoundException
   */
  public static function lastByEventId(int $event_id) {
    $conditions = db_and()->condition('event_id', $event_id)
      ->condition('relation_type', static::RELATION_TYPE_NODE);

    $raw_select_result = self::getBaseQuery()->condition($conditions)
      ->orderBy('event_id', 'DESC')->range(0, 1)->execute();

    if ($raw_select_result !== NULL && $raw_select_result->rowCount()) {
      return static::newInstance($raw_select_result->fetchAssoc(), TRUE);
    }

    throw (new ModelNotFoundException)->setModel(static::class);
  }

  /**
   * Retrieves relations for many events by events identifiers.
   *
   * @param array $events_ids
   *    Reminders events identifiers.
   *
   * @return Collection|self[]
   *   Events relations collection.
   */
  public static function allByEventsIds(array $events_ids): Collection {
    if (!empty($events_ids)) {

      $conditions = db_and()->condition('event_id', $events_ids, 'IN')
        ->condition('relation_type', static::RELATION_TYPE_NODE);

      $raw_select_result = self::getBaseQuery()->condition($conditions)->execute();

      if ($raw_select_result !== NULL && $raw_select_result->rowCount()) {
        $items = $raw_select_result->fetchAll(\PDO::FETCH_ASSOC);
      }
    }

    return static::newCollection($items ?? [], TRUE);
  }

  /**
   * Get the value of the relation's primary key.
   *
   * @return array
   */
  public function getKey(): array {
    foreach (static::$primaryKey as $attribute) {
      if (isset($this->original[$attribute])) {
        $primary_key[$attribute] = $this->original[$attribute];
      }
      else {
        $primary_key[$attribute] = $this->getAttribute($attribute);

      }
    }

    return $primary_key ?? [];
  }

  /**
   * Prepares drupal database condition for primary key.
   *
   * @return \DatabaseCondition|\QueryConditionInterface
   */
  protected function getKeyCondition() {
    $condition = db_and();

    foreach ($this->getKey() as $field => $value) {
      $condition = $condition->condition($field, $value);
    }

    return $condition;
  }

  /**
   * Set a primary key value.
   *
   * @param array $id
   */
  public function setKey($id) {
    if (is_array($id)) {
      foreach ($id as $field => $value) {
        $this->setAttribute($field, $value);
      }
    }
  }

}
