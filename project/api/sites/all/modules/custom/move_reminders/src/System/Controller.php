<?php

namespace Drupal\move_reminders\System;

use Drupal\move_centrifugo\Actions\Centrifugo;
use Drupal\move_reminders\Models\ModelNotFoundException;
use Drupal\move_reminders\Models\NodeReminder as Reminder;

/**
 * Class Controller.
 *
 * @package Drupal\move_reminders\System
 */
class Controller {

  /**
   * Retrieves reminders list for User by event timestamps range.
   *
   * @param int $user_id
   *   User identifier.
   * @param int|null $from
   *   From date timestamp.
   * @param int|null $to
   *   To date timestamp.
   *
   * @return array
   *   Array with formatted response data.
   */
  public static function getForUser(int $user_id, int $from = NULL, int $to = NULL): array {
    return response_success(Reminder::getForUser($user_id, $from, $to)->values());
  }

  /**
   * Retrieves reminders list for MoveRequest.
   *
   * @param int $node_id
   *   Node identifier.
   *
   * @return array
   *   Array of reminders objects.
   */
  public static function allForNode(int $node_id): array {
    return response_success(Reminder::allForNode($node_id)->values());
  }

  /**
   * Stores new reminder.
   *
   * @param array $attributes
   *   Associative array of reminder attributes.
   *
   * @return array
   *   Array with formatted response data.
   */
  public static function store(array $attributes): array {
    $reminder = Reminder::create($attributes);

    self::sendWithCentrifugo($reminder);

    return response_success($reminder);
  }

  /**
   * Updates reminder.
   *
   * @param int $reminder_id
   *   Existing reminder identifier.
   * @param array $attributes
   *   Associative array of new reminder attributes.
   *
   * @return array
   *   Array with formatted response data.
   *
   * @throws \ServicesException.
   */
  public static function update(int $reminder_id, array $attributes): array {
    $reminder = self::findOrFail($reminder_id)->fill($attributes)->save();
    return response_success($reminder);
  }

  /**
   * Snoozes reminder for date.
   *
   * @param int $reminder_id
   *   Existing reminder identifier.
   *
   * @return array
   *   Array with formatted response data.
   *
   * @throws \ServicesException.
   */
  public static function snooze(int $reminder_id, int $snoozed_for, int $snoozed_to): array {
    $reminder = self::findOrFail($reminder_id)->snooze($snoozed_for, $snoozed_to)->save();
    return response_success($reminder);
  }

  /**
   * Dismisses reminder for date.
   *
   * @param int $reminder_id
   *   Existing reminder identifier.
   * @param int $dismissed_at
   *   Dismissing date timestamp.
   *
   * @return array
   *   Array with formatted response data.
   *
   * @throws \ServicesException.
   */
  public static function dismiss(int $reminder_id, int $dismissed_at): array {
    $reminder = self::findOrFail($reminder_id)->dismiss($dismissed_at)->save();
    return response_success($reminder);
  }

  /**
   * Find a reminder by its identifier or throw an exception.
   *
   * @param int $reminder_id
   *   Existing reminder identifier.
   *
   * @return Reminder
   *   Reminder model instance.
   *
   * @throws \ServicesException.
   */
  private static function findOrFail(int $reminder_id): Reminder {
    try {
      return Reminder::find($reminder_id);
    }
    catch (\Throwable $exception) {
      return response_error(['error' => 'Reminder not found by id=' . $reminder_id]);
    }
  }

  /**
   * @param Reminder $reminder
   */
  private static function sendWithCentrifugo(Reminder $reminder) {
    global $user;

    $channel = 'reminders#' . $reminder->user_id;

    try {
      if ($user->uid != $reminder->user_id) {
        (new Centrifugo())->publish($channel, $reminder);
      }
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("sendWithCentrifugo", $error_text, array(), WATCHDOG_ERROR);
    }

  }

}
