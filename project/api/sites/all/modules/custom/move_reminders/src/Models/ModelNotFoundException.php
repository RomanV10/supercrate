<?php

namespace Drupal\move_reminders\Models;

/**
 * Class ModelNotFoundException.
 *
 * @package Drupal\move_reminders\Exceptions
 */
class ModelNotFoundException extends \RuntimeException {

  /**
   * Name of the affected model.
   *
   * @var string|null
   */
  protected $model;

  /**
   * Set the affected model.
   *
   * @param string $model
   *
   * @return $this
   */
  public function setModel($model) {
    $this->model = $model;

    $this->message = "No query results for model [{$model}].";

    return $this;
  }

  /**
   * Get the affected model.
   *
   * @return string|null
   *   Name of the affected model.
   */
  public function getModel(): ?string {
    return $this->model;
  }

}
