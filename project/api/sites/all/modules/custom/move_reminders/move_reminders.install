<?php

/**
 * @file
 * Contains install and update functions for this module.
 */

/**
 * Implements hook_schema().
 */
function move_reminders_schema() {

  $schema['move_reminders_events'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'user_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'subject' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'description' => array(
        'type' => 'text',
        'not null' => TRUE,
      ),
      'color' => array(
        'type' => 'varchar',
        'length' => 6,
        'not null' => FALSE,
      ),
      'starts_at' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'ends_at' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'remind_for' => array(
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
      'remind_at' => array(
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
      'dismissed_at' => array(
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
      'created_at' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'updated_at' => array(
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'starts_at' => array('starts_at'),
      'remind_at' => array('remind_at'),
    ),
    'foreign keys' => array(
      'fk_move_reminders_events_user_id' => array(
        'table' => 'users',
        'columns' => array('user_id' => 'uid'),
      ),
    ),
  );

  $schema['move_reminders_events_relations'] = array(
    'fields' => array(
      'event_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'relation_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'relation_type' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'created_at' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'updated_at' => array(
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
    ),
    'primary key' => array('event_id', 'relation_id', 'relation_type'),
    'indexes' => array(
      'stage_event_id' => array('event_id'),
      'relation_id' => array('relation_id'),
    ),
    'foreign keys' => array(
      'fk_move_reminders_events_relations_event_id' => array(
        'table' => 'move_reminders_events',
        'columns' => array('event_id' => 'id'),
      ),
    ),
  );

  $schema['move_reminders_events_stages'] = array(
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'event_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'snoozed_for' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'snoozed_to' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'created_at' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'updated_at' => array(
        'type' => 'int',
        'not null' => FALSE,
        'unsigned' => TRUE,
      ),
    ),

    'primary key' => array('id'),

    'foreign keys' => array(
      'fk_move_reminders_events_stages_event_id' => array(
        'table' => 'move_reminders_events',
        'columns' => array('event_id' => 'id'),
      ),

      'indexes' => array(
        'stage_event_id' => array('event_id'),
      ),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function move_reminders_install() {
  db_query('
    ALTER TABLE {move_reminders_events}
    ADD CONSTRAINT {fk_move_reminders_events_user_id}
    FOREIGN KEY (user_id) REFERENCES {users} (uid)
    ON DELETE CASCADE
  ');

  db_query('
    ALTER TABLE {move_reminders_events_relations}
    ADD CONSTRAINT {fk_move_reminders_events_relations_event_id}
    FOREIGN KEY (event_id) REFERENCES {move_reminders_events} (id)
    ON DELETE CASCADE
  ');

  db_query('
    ALTER TABLE {move_reminders_events_stages}
    ADD CONSTRAINT {fk_move_reminders_events_stages_event_id}
    FOREIGN KEY (event_id) REFERENCES {move_reminders_events} (id)
    ON DELETE CASCADE
  ');
}

/**
 * Update field move_reminders_events table.
 */
function move_reminders_update_7101() {
  if (db_table_exists('move_reminders_events')) {
    $schema = move_reminders_schema();
    db_add_index('move_reminders_events', 'starts_at', $schema['move_reminders_events']['indexes']['starts_at']);
    db_add_index('move_reminders_events', 'remind_at', $schema['move_reminders_events']['indexes']['remind_at']);
  }
}

/**
 * Update field move_reminders_events_relations table.
 */
function move_reminders_update_7102() {
  if (db_table_exists('move_reminders_events_relations')) {
    $schema = move_reminders_schema();
    db_add_index('move_reminders_events_relations', 'relation_id', $schema['move_reminders_events_relations']['indexes']['relation_id']);
  }
}

/**
 * Implements hook_uninstall().
 */
function move_reminders_uninstall() {
  db_query('
    ALTER TABLE {move_reminders_events}
    DROP FOREIGN KEY {fk_move_reminders_events_user_id}
  ');

  db_query('
    ALTER TABLE {move_reminders_events_relations}
    DROP FOREIGN KEY {fk_move_reminders_events_relations_event_id}
  ');

  db_query('
    ALTER TABLE {move_reminders_events_stages}
    DROP FOREIGN KEY {fk_move_reminders_events_stages_event_id}
  ');
}
