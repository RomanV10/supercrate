<?php

namespace Drupal\move_timer;

use DateTime;

class ExecutionTime {

  public static function Start(String $name) {
    static::timerStart($name);
  }

  public static function End(String $name) {
    $timer = static::timerStop($name);
    static::write($timer);
  }

  private static function write(array $timer) {
    $previously = $timer['backtrace'][1];
    $pre_previously = $timer['backtrace'][2];
    $file = $previously['file'];
    $line = $previously['line'];
    $function = $pre_previously['function'];
    $class = !empty($pre_previously['class']) ? $pre_previously['class'] : "";
    $now = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
    $date = $now->format('Y-m-d H:i:s.u');
    $execution_time = $timer['time'];
    $memory = (memory_get_peak_usage(TRUE) / 1024 / 1024);
    db_insert('move_timers')
      ->fields(array(
          'file_name' => $file,
          'method_name' => $function,
          'line' => $line,
          'class' => $class,
          'time' => $execution_time,
          'date' => $date,
          'memory' => $memory,
        ))
      ->execute();
  }

  private static function timerStart($name) {
    global $timers;
    $timers[$name]['start'] = microtime(TRUE);
    $timers[$name]['count'] = isset($timers[$name]['count']) ? ++$timers[$name]['count'] : 1;
    $timers[$name]['backtrace'] = debug_backtrace();
  }

  private static function timerStop($name) {
    global $timers;

    if (isset($timers[$name]['start'])) {
      $stop = microtime(TRUE);
      $diff = round(($stop - $timers[$name]['start']) * 1000, 5);
      if (isset($timers[$name]['time'])) {
        $timers[$name]['time'] += $diff;
      }
      else {
        $timers[$name]['time'] = $diff;
      }
      unset($timers[$name]['start']);
    }

    return $timers[$name];
  }

}
