<?php

/**
 * Admin settings form.
 */
function move_quickbooks_admin_settings($form, &$form_state) {
  module_load_install('move_quickbooks');

  // Requirements.
  $form['requirements'] = array(
    '#type' => 'fieldset',
    '#title' => t('Requirements'),
  );
  $form['requirements']['report'] = array(
    '#markup' => theme('status_report', array('requirements' => move_quickbooks_requirements('runtime'))),
  );

  // Configuration.
  $qbo = new QBO();
  $qbo->configForm($form, $form_state);

  return $form;
}
