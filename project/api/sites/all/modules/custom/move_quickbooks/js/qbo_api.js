/**
 * Added by mlevasseur on 2/25/2016.
 *
 * @see https://github.com/IntuitDeveloper/oauth-php/blob/master/PHPOAuthSample/index.php
 */

(function ($) {

  Drupal.behaviors.intuitOAuthConnect = {
    attach: function (context, settings) {
      var parser = document.createElement('a');
      parser.href = document.url;

      intuit.ipp.anywhere.setup({
        menuProxy: '',
        grantUrl: 'http://' + parser.hostname + '/' + Drupal.settings.qboApi.oauthUrl
        // outside runnable you can point directly to the oauth.php page
      });
    }
  }

})(jQuery);
