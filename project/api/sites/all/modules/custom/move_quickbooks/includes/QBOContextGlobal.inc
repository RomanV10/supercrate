<?php

/**
 * QBO contextual for global site-wide connections.
 */
class QBOContextGlobal extends QBOContext {

  /**
   * {@inheritdoc}
   */
  public function id() {
    return 'global';
  }

  /**
   * {@inheritdoc}
   */
  public function keys() {
    // See loadKeys() to understand why we need a separate function.
    return $this->loadKeys();
  }

  /**
   * {@inheritdoc}
   */
  public static function allKeys() {
    $data = array();
    if ($keys = self::loadKeys()) {
      $data[] = array(
        'args' => array(),
        'keys' => $keys,
      );
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function save($values) {
    foreach ($this->filterKeyValues($values) as $name => $value) {
      variable_set("qbo_api_{$name}", $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function disconnect() {
    // Delete OAuth data.
    variable_del('qbo_api_access_token');
    variable_del('qbo_api_access_secret');
    variable_del('qbo_api_auth_date');
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    return user_access('administer qbo api');
  }

  /**
   * Wrapper for parent::keys().
   *
   * We need this separate so that it can be static, so that it can
   * be called statically from allKeys().
   */
  public static function loadKeys() {
    $keys = array();

    foreach (self::keyNames() as $name) {
      $keys[$name] = variable_get("qbo_api_{$name}", NULL);
    }

    return $keys;
  }
}
