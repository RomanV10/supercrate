<?php

namespace Drupal\move_quickbooks\Cron\Queues;

use Drupal\move_quickbooks\Services\QBSalesReceipt;
use Drupal\move_services_new\Cron\Queues\QueueInterface;

/**
 * Class BulkPost.
 *
 * @package Drupal\move_quickbooks\Cron\Queues
 */
class BulkPost implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   */
  public static function execute($data): void {
    (new QBSalesReceipt())->createQBSalesReceipts($data);
  }

}
