<?php

namespace Drupal\move_quickbooks\System;

use Drupal\move_quickbooks\Services\QBSalesReceipt;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_quickbooks\Services\Cron;
use QBO;

/**
 * Class Service.
 *
 * @package Drupal\move_quickbooks\System
 */
class Service {

  /**
   * Function getResources.
   *
   * @return array
   *   Resources.
   */
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  /**
   * Private function definition.
   *
   * @return array
   *   Operations and actions.
   */
  private static function definition() {
    return array(
      'move_quickbooks' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_quickbooks\System\Service::createQBSalesReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
                'description' => t('The request ID for which the sales transaction is posted to QuickBooks'),
              ),
              array(
                'name' => 'receipt',
                'type' => 'array',
                'source' => array('data' => 'receipt'),
                'optional' => FALSE,
                'description' => t('The moveadmin_receipt array from get_receipt'),
              ),
              array(
                'name' => 'receipt_id',
                'type' => 'int',
                'source' => array('data' => 'receipt_id'),
                'optional' => FALSE,
                'description' => t('The ID of the moveadmin_receipt'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_quickbooks\System\Service::retrieveQBSalesReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
                'description' => t('The request ID for which the Quickbooks post status is returned'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'post_company_id' => array(
            'file' => [
              'type' => 'inc',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ],
            'callback' => 'Drupal\move_quickbooks\System\Service::saveQBCompanyID',
            'access arguments' => array('access content'),
            'args' => array(
              array(
                'name' => 'compid',
                'optional' => FALSE,
                'source' => array('data' => 'compid'),
                'description' => t('Company ID for Quickbooks authorization'),
                'type' => 'int',
              ),
            ),
          ),
          'get_company_id' => array(
            'file' => [
              'type' => 'inc',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ],
            'callback' => 'Drupal\move_quickbooks\System\Service::getQBCompanyID',
            'access arguments' => array('access content'),
          ),
          'check_auth_date_reconnect_button' => array(
            'file' => [
              'type' => 'inc',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ],
            'callback' => 'Drupal\move_quickbooks\System\Service::checkQBReconnectAllowed',
            'access arguments' => array('access content'),
          ),
          'check_token_disconnect_button' => array(
            'file' => [
              'type' => 'inc',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ],
            'callback' => 'Drupal\move_quickbooks\System\Service::checkAccessTokenSet',
            'access arguments' => array('access content'),
          ),
          'qbo_reconnect' => array(
            'file' => [
              'type' => 'inc',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ],
            'callback' => 'Drupal\move_quickbooks\System\Service::reconnectQB',
            'access arguments' => array('access content'),
          ),
          'qbo_disconnect' => array(
            'file' => [
              'type' => 'inc',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ],
            'callback' => 'Drupal\move_quickbooks\System\Service::disconnectQB',
            'access arguments' => array('access content'),
          ),
          'set_quickbooks_settings' => array(
            'file' => [
              'type' => 'inc',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ],
            'callback' => 'Drupal\move_quickbooks\System\Service::saveQBPostSettings',
            'access arguments' => array('access content'),
            'args' => array(
              array(
                'name' => 'qb_post_settings',
                'optional' => FALSE,
                'source' => array('data' => 'qb_post_settings'),
                'description' => t('Quickbooks post settings such as when closed, all the time or off'),
                'type' => 'string',
              ),
            ),
          ),
          'export_all_qbo' => array(
            'file' => [
              'type' => 'inc',
              'module' => 'move_quickbooks',
              'name' => 'src/System/Service',
            ],
            'callback' => 'Drupal\move_quickbooks\System\Service::exportConfirmedRequests',
            'access arguments' => array('access content'),
            'args' => array(
              array(
                'name' => 'date_filter_from',
                'optional' => FALSE,
                'source' => array('data' => 'date_filter_from'),
                'description' => t('The from date filer for confirmed requests'),
                'type' => 'int',
              ),
              array(
                'name' => 'date_filter_to',
                'optional' => FALSE,
                'source' => array('data' => 'date_filter_to'),
                'description' => t('The to date filer for confirmed requests'),
                'type' => 'int',
              ),
            ),
          ),
        ),
      ),
    );
  }

  /**
   * Creates a single Sales Receipt in QuickBooks right after.
   *
   * The payment is made and then posts results into move_quickbooks table.
   *
   * @param int $entity_id
   *   The request ids for posting.
   * @param array $receipt
   *   The receipt object from moveadmin_receipt.
   * @param int $receipt_id
   *   The receipt id from moveadmin_receipt.
   *
   * @return mixed
   *   Result db_insert.
   */
  public static function createQBSalesReceipt($entity_id, array $receipt = array(), $receipt_id) {
    $instance = new QBSalesReceipt();
    return $instance->createQBSalesReceipt($entity_id, $receipt, $receipt_id);
  }

  /**
   * Retrieves receipts are posted to Quickbooks account of the company or not.
   *
   * @param int $entity_id
   *   The request ID.
   *
   * @return int
   *   Returns 0 if request sales receipts are not posted,
   *   else returns 1 if all sales receipts of the request are posted
   *   to QuickBooks, returns 2 if only some of the request sales receipts
   *   posted to QuickBooks, also, returns false
   *   if no request with such ID present.
   */
  public static function retrieveQBSalesReceipt($entity_id) {
    if (MoveRequest::checkNode($entity_id)) {
      $instance = new QBSalesReceipt();
      return $instance->getMQReceiptRecord($entity_id);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Save company id to drupal db from front.
   *
   * Update consumer key and secrets when saving company id.
   *
   * @param int $comid
   *   The QuickBooks company ID.
   *
   * @return bool
   *   Returns true if saving company id successful, else returns false.
   */
  public static function saveQBCompanyID($comid) {
    variable_set("qbo_api_company_id", $comid);
    if (variable_get("qbo_api_company_id", FALSE) != $comid) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Returns the saved company id and post settings to front.
   *
   * @return array
   *   Returns an array containing company Id and quickbooks post settings.
   */
  public static function getQBCompanyID() {
    $temp_company_id = variable_get("qbo_api_company_id", FALSE);
    $temp_qbo_post_settings = variable_get("qbo_post_settings", FALSE);
    if (!empty($temp_company_id)) {
      return array($temp_company_id, $temp_qbo_post_settings);
    }
    else {
      return array(FALSE, $temp_qbo_post_settings);
    }
  }

  /**
   * Reconnect to QuickBooks from front.
   *
   * @throws \QBOException
   */
  public static function reconnectQB() {
    $qborecon = new QBO();
    $qborecon->reconnect();
  }

  /**
   * Disconnect from QuickBooks from front.
   *
   * @throws \QBOException
   */
  public static function disconnectQB() {
    $qbodiscon = new QBO();
    $qbodiscon->disconnect();
    variable_del("qbo_post_settings");
  }

  /**
   * Checks whether the reconnect to QuickBooks is allowed.
   *
   * @return bool
   *   Returns true if reconnect allowed, else returns false.
   */
  public static function checkQBReconnectAllowed() {
    $temp_auth_date = variable_get("qbo_api_auth_date", FALSE);
    if (!empty($temp_auth_date) and !empty(variable_get("qbo_api_acess_token", FALSE))
      and move_quickbooks_token_needs_refresh($temp_auth_date)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Checks Quickbooks Oauth authorization status.
   *
   * @return bool
   *   Returns true if QuickBooks authorized, else returns false.
   */
  public static function checkAccessTokenSet() {
    if (!empty(variable_get("qbo_api_access_token", FALSE))) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Save QuickBooks post settings from front.
   *
   * @param mixed $qb_post_settings
   *   Quick Books settings.
   *
   * @return bool
   *   Returns true when post settings saved.
   */
  public static function saveQBPostSettings($qb_post_settings) {
    if ($qb_post_settings == "qboff") {
      variable_del("qbo_post_settings");
      return TRUE;
    }
    else {
      variable_set("qbo_post_settings", $qb_post_settings);
      return TRUE;
    }
  }

  /**
   * Bulk export to QuickBooks in profit and loss.
   *
   * @param mixed $date_filter_from
   *   Date from.
   * @param mixed $date_filter_to
   *   Date to.
   *
   * @throws \Exception
   */
  public static function exportConfirmedRequests($date_filter_from, $date_filter_to) {
    Cron::prepareDataForCron($date_filter_from, $date_filter_to);
  }

}
