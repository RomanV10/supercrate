<?php

namespace Drupal\move_quickbooks\Services;

use QBO;
use IPPCustomer;
use IPPTelephoneNumber;
use IPPEmailAddress;
use IPPSalesReceipt;
use IPPRefundReceipt;
use IPPLine;
use IPPSalesItemLineDetail;
use IPPAccount;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_new_log\Services\Log;

/**
 * Class QBSalesReceipt.
 *
 * @package Drupal\move_quickbooks\Services
 */
class QBSalesReceipt {

  /**
   * Creates multiple Sales Receipts in QuickBooks.
   *
   * When job is closed and then saves results into move_quickbooks table.
   * The functions is mostly used by MoveRequest.php's update function.
   *
   * @param int $entity_id
   *   The request id for posting request payments on closing.
   *
   * @throws \IdsException
   * @throws \QBOException
   * @throws \Exception
   */
  public function createQBSalesReceipts($entity_id) {
    // Check if we have MoveBoard account id, if not create it.
    if (!empty($qbo_post_account_id = self::createMoveBoardAccount())) {
      // Check if the request has receipts.
      if (!empty($request_receipts = MoveRequest::getReceipt($entity_id))) {
        // Check if we have a customer id, if not create customer.
        if (!empty($qb_customer_id = self::getQBCustomerID($entity_id))) {
          // Compare move_quickbooks receipts with move_admin_receipts
          // to identify missing receipts to be posted on close.
          $qb_receipt_ids_mq_db = self::checkMQReceiptRecords($entity_id);
          foreach ($request_receipts as $key => $value) {
            if (in_array($value['id'], $qb_receipt_ids_mq_db)) {
              continue;
            }
            else {
              if (array_key_exists('refunds', $value) && $value['refunds'] == TRUE) {
                $request_single_receipt = -$value['amount'];
              }
              else {
                $request_single_receipt = $value['amount'];
              }
              // Finally create a sales receipt and create logs.
              if (!empty($qb_receipt_id = self::postQBSalesReceipt($request_single_receipt, $entity_id, $qb_customer_id, $value, $qbo_post_account_id))) {
                self::prepareQuickbooksLogs($qb_customer_id, $qb_receipt_id, $request_single_receipt, $entity_id, 0);

                // Save to move_quickbooks table.
                $data = array(
                  'entityId' => $entity_id,
                  'receiptId' => $value['id'],
                  'post_status' => 1,
                  'customer_id' => $qb_customer_id,
                  'receipt_id' => $qb_receipt_id,
                  'request_single_receipt' => $request_single_receipt,
                );
                self::insertDrupalDB($data);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Checks if we have MoveBoard Account ID.
   *
   * If not creates the Quickbooks account named MoveBoard of type Checking.
   *
   * @throws \QBOException
   * @throws \IdsException
   */
  public static function createMoveBoardAccount() {
    $qbo_moveboard_account_id = variable_get("qbo_post_account", FALSE);
    if (!empty($qbo_moveboard_account_id)) {
      return $qbo_moveboard_account_id;
    }
    else {
      $qbo = new QBO();
      $account = new IPPAccount();
      $account->Name = 'MoveBoard';
      $account->SubAccount = 'false';
      $account->Active = 'true';
      $account->Classification = 'Asset';
      $account->AccountType = 'Bank';
      $account->AccountSubType = 'Checking';

      $account = $qbo->dataService()->Add($account);
      $error = $qbo->dataService()->getLastError();

      // If errors with creating MoveBoard Account
      // go inside this if else statement.
      if ($error != NULL) {
        // If error is because the MoveBoard Account already exists,
        // then just query for MoveBoard Account ID.
        if (preg_match('/Duplicate Name Exists Error/', $error->getResponseBody())) {
          $entities = $qbo->query("SELECT Name FROM Account WHERE Name='MoveBoard'");
          if (count($entities) == 0) {
            watchdog('move_quickbooks', 'Could not find the Account named MoveBoard in your Quickbooks Account', array(), WATCHDOG_ERROR);
            return 0;
          }
          else {
            variable_set('qbo_post_account', (int) $entities[0]->Id);
            return (int) $entities[0]->Id;
          }
        }
        else {
          watchdog('move_quickbooks', $error->getResponseBody(), array(), WATCHDOG_ERROR);
          return 0;
        }
      }
      else {
        variable_set('qbo_post_account', (int) $account->Id);
        return (int) $account->Id;
      }
    }
  }

  /**
   * Retrieves the customer info from MoveBoard for a request.
   *
   * Then queries external Quickbooks searching for customer.
   *
   * By his unique email address from MoveBoard.
   *
   * If the client is not found, create a new customer in Quickbooks.
   *
   * @param int|mixed $entity_id
   *   The request ids for which to get the customer info.
   *
   * @return int
   *   Returns the customer ID from external Quickbooks
   *   company account (the same for create and query).
   *
   * @throws \QBOException
   * @throws \IdsException
   */
  public static function getQBCustomerID($entity_id) {
    // Query drupal database to check if we already have
    // qb_customer_id for this request.
    $qb_customer_id = db_select('move_quickbooks', 'mq')
      ->fields('mq', array('quickbooks_customer_id'))
      ->condition('mq.entity_id', $entity_id)
      ->execute()
      ->fetchCol();
    if (!empty($qb_customer_id)) {
      return $qb_customer_id[0];
    }
    else {
      $user_info = self::getClientInfoForRequest($entity_id);
      $firstName = $user_info['field_user_first_name_value'] ?? '';
      $lastName = $user_info['field_user_last_name_value'] ?? '';
      $mail = $user_info['mail'] ?? '';
      $phone = $user_info['field_primary_phone_value'] ?? '';

      if (!empty($user_info)) {
        $qbo = new QBO();
        $entities = $qbo->query("SELECT Id FROM Customer WHERE PrimaryEmailAddr='{$mail}'");
        if (empty($entities)) {
          return self::createQBCustomer($firstName, $lastName, $phone, $mail);
        }
        else {
          return (int) $entities[0]->Id;
        }
      }
      else {
        watchdog('move_quickbooks', "getQBCustomerID empty user data for request $entity_id", array(), WATCHDOG_ERROR);
      }
    }
  }

  public static function getClientInfoForRequest(int $nid) {
    $query = db_select('node', 'n');
    $query->leftJoin('users', 'u', 'u.uid = n.uid');
    $query->leftJoin('field_data_field_user_first_name', 'first_name', 'first_name.entity_id = u.uid');
    $query->leftJoin('field_data_field_user_last_name', 'last_name', 'last_name.entity_id = u.uid');
    $query->leftJoin('field_data_field_primary_phone', 'phone', 'phone.entity_id = u.uid');
    $query->fields('u', array('mail'))
      ->fields('first_name', array('field_user_first_name_value'))
      ->fields('last_name', array('field_user_last_name_value'))
      ->fields('phone', array('field_primary_phone_value'))
      ->condition('n.nid', $nid);
    $result = $query->execute()->fetchAssoc();
    return $result;
  }

  /**
   * Creates a new customer in the company Quickbooks account.
   *
   * @param string $first_name
   *   The first name of the customer.
   * @param string $last_name
   *   The last name of the customer.
   * @param string $phone
   *   The phone number of the customer.
   * @param string $email
   *   The email address of the customer.
   *
   * @return int
   *   Returns the customer ID of a newly created QuickBooks customer
   *   if creation is successful, else returns 0
   *
   * @throws \QBOException
   * @throws \IdsException
   */
  public static function createQBCustomer($first_name, $last_name, $phone, $email) {
    $qbo = new QBO();
    $customerObj = new IPPCustomer();
    $customerObj->GivenName = $first_name;
    $customerObj->FamilyName = $last_name;
    $customerObj->CurrencyRef = 'USD';
    $customerObj->PrimaryPhone = new IPPTelephoneNumber();
    $customerObj->PrimaryPhone->FreeFormNumber = $phone;
    $customerObj->PrimaryEmailAddr = new IPPEmailAddress();
    $customerObj->PrimaryEmailAddr->Address = $email;
    $resultingCustomerObj = $qbo->dataService()->Add($customerObj);
    $error = $qbo->dataService()->getLastError();
    if ($error != NULL) {
      watchdog('move_quickbooks', $error->getResponseBody(), array(), WATCHDOG_ERROR);
      return 0;
    }
    else {
      return (int) $resultingCustomerObj->Id;
    }
  }

  /**
   * Query move_quickbooks table by request id.
   *
   * @param int $entity_id
   *   The request ID.
   *
   * @return array
   *   Returns array of receipt ids which belong to specific request if found.
   */
  public static function checkMQReceiptRecords($entity_id) {
    return db_select('move_quickbooks', 'mq')
      ->fields('mq', array('moveadmin_receipt_id'))
      ->condition('mq.entity_id', $entity_id)
      ->execute()
      ->fetchCol();
  }

  /**
   * Posts receipt information to company Quickbooks account.
   *
   * @param int $payms
   *   The payment amount to post to Quickbooks.
   * @param int $reqnum
   *   The MoveBoard request ID.
   * @param int $customref
   *   The customer ID from QuickBooks account.
   * @param array $receiptMoveBoard
   *   The payment note from MoveBoard.
   * @param mixed $qbo_post_account_id
   *   QBO post account id.
   *
   * @return int
   *   Returns the Sales Receipt ID of a newly created Quickbooks Sales Receipt
   *   if creation is successful, else returns 0.
   *
   * @throws \IdsException
   * @throws \QBOException
   */
  public static function postQBSalesReceipt($payms, $reqnum, $customref, $receiptMoveBoard, $qbo_post_account_id) {
    $qbo = new QBO();
    $receiptCreated = REQUEST_TIME;
    $receiptDescription = '';
    if (!empty($receiptMoveBoard)) {
      $receiptCreated = $receiptMoveBoard['created'];
      $receiptDescription = $receiptMoveBoard['description'];
    }
    if ($payms < 0) {
      $receipt = new IPPRefundReceipt();
      $receipt->TxnDate = format_date($receiptCreated, 'custom', 'Y-m-d');
      $receipt->CustomerRef = $customref;
      $receipt->DepositToAccountRef = $qbo_post_account_id;

      $receipt->Line = array();
      $receipt_line = new IPPLine();
      $receipt_line->DetailType = 'SalesItemLineDetail';
      $receipt_line->Description = $receiptDescription;
      $receipt_line->Amount = $payms * -1;
      $receipt_line->SalesItemLineDetail = new IPPSalesItemLineDetail();
      $receipt_line->SalesItemLineDetail->Qty = 1;
      $receipt_line->SalesItemLineDetail->UnitPrice = $payms * -1;
      $receipt_line->SalesItemLineDetail->TaxCodeRef = 'NON';
      $receipt->Line[] = $receipt_line;
    }
    elseif ($payms > 0) {
      $receipt = new IPPSalesReceipt();
      $receipt->TxnDate = format_date($receiptCreated, 'custom', 'Y-m-d');
      $receipt->PaymentRefNum = 'MoveBoard #' . $reqnum;
      $receipt->CustomerRef = $customref;
      $receipt->DepositToAccountRef = $qbo_post_account_id;

      $receipt->Line = array();
      $receipt_line = new IPPLine();
      $receipt_line->DetailType = 'SalesItemLineDetail';
      $receipt_line->Description = $receiptDescription;
      $receipt_line->Amount = $payms;
      $receipt_line->SalesItemLineDetail = new IPPSalesItemLineDetail();

      $receipt_line->SalesItemLineDetail->Qty = 1;
      $receipt_line->SalesItemLineDetail->UnitPrice = $payms;
      $receipt_line->SalesItemLineDetail->TaxCodeRef = 'NON';
      $receipt->Line[] = $receipt_line;
    }

    $receipt = $qbo->dataService()->Add($receipt);
    $error = $qbo->dataService()->getLastError();
    if ($error != NULL) {
      watchdog('move_quickbooks', $error->getResponseBody(), array(), WATCHDOG_ERROR);
      return 0;
    }
    else {
      return (int) $receipt->DocNumber;
    }
  }

  /**
   * Create MoveBoard request logs for Quickbooks Sales Receipts.
   *
   * @param int $qb_customer_id
   *   The customer ID in the Quickbooks.
   * @param int $qb_sales_receipt_id
   *   The sales receipt ID in the Quickbooks.
   * @param int $qb_sales_receipt_amount
   *   The sales receipt amount.
   * @param int $entity_id
   *   The MoveBoard request ID.
   * @param int $entity_type
   *   The entity type ID for the request.
   */
  public static function prepareQuickbooksLogs($qb_customer_id, $qb_sales_receipt_id, $qb_sales_receipt_amount, $entity_id, $entity_type = NULL) {
    $qbLog = new Log($entity_id, $entity_type);
    if ($qb_sales_receipt_amount > 0) {
      $log_data[] = array(
        'details' => array(
          [
            'activity' => 'System',
            'title' => 'Sales Receipt was posted to Quickbooks',
            'text' => array(
              [
                'text' => 'Sales Receipt #' . $qb_sales_receipt_id . ' in amount of $' . $qb_sales_receipt_amount . ' has been created for Customer #' . $qb_customer_id . ' in your Quickbooks account',
              ],
            ),
            'date' => time(),
          ],
        ),
        'source' => 'System',
      );
    }
    else {
      $log_data[] = array(
        'details' => array(
          [
            'activity' => 'System',
            'title' => 'Refund Receipt was posted to Quickbooks',
            'text' => array(
              [
                'text' => 'Refund Receipt #' . $qb_sales_receipt_id . ' in amount of $' . $qb_sales_receipt_amount . ' has been created for Customer #' . $qb_customer_id . ' in your Quickbooks account',
              ],
            ),
            'date' => time(),
          ],
        ),
        'source' => 'System',
      );
    }

    $qbLog->create($log_data);
  }

  /**
   * Write results of posting Sales Receipt into drupal move_quickbooks table.
   *
   * @param array $data
   *   The array of Sales Receipt information to record into the database.
   *
   * @return bool
   *   Returns true if recording into move_quickbooks table successful,
   *   else returns false.
   *
   * @throws \Exception
   */
  public static function insertDrupalDB($data = array()) {
    return db_insert('move_quickbooks')
      ->fields(array(
        'entity_id' => $data['entityId'],
        'moveadmin_receipt_id' => $data['receiptId'],
        'quickbooks_post_status' => $data['post_status'],
        'quickbooks_customer_id' => $data['customer_id'],
        'quickbooks_receipt_id' => $data['receipt_id'],
        'quickbooks_payment_amount' => $data['request_single_receipt'],
        'created' => time(),
      ))
      ->execute();
  }

  /**
   * Creates a single Sales Receipt in QuickBooks after the payment is made.
   *
   * Then posts results into move_quickbooks table.
   *
   * Receives all parameters from MoveRequest.php's setReceipt function.
   *
   * @param int $entity_id
   *   The request ids for posting.
   * @param array $receipt
   *   The receipt object from moveadmin_receipt.
   * @param int $receipt_id
   *   The receipt id from moveadmin_receipt.
   *
   * @return int
   *   Returns result db_insert.
   *
   * @throws \IdsException
   * @throws \QBOException
   * @throws \Exception
   */
  public function createQBSalesReceipt($entity_id, array $receipt = array(), $receipt_id) {
    // Check if we have MoveBoard account id, if not create it.
    if (!empty($qbo_post_account_id = self::createMoveBoardAccount())) {

      // Check if the receipt id already exists in
      // our database to avoid duplicate transactions.
      if (empty(self::receiptRecordExists($receipt_id))) {
        // Check if we have a customer id, if not create customer.
        if (!empty($qb_customer_id = self::getQBCustomerID($entity_id))) {
          $receipt_amount = $receipt['amount'];

          // If it is a refund transaction, save with minus sign.
          if (array_key_exists('refunds', $receipt) && $receipt['refunds'] == TRUE) {
            $receipt_amount = -$receipt_amount;
          }

          // Finally create a salesreceipt and create logs.
          if (!empty($qb_receipt_id = self::postQBSalesReceipt($receipt_amount, $entity_id, $qb_customer_id, $receipt, $qbo_post_account_id))) {
            self::prepareQuickbooksLogs($qb_customer_id, $qb_receipt_id, $receipt_amount, $entity_id, 0);

            // Save to move_quickbooks table.
            $data = array(
              'entityId' => $entity_id,
              'receiptId' => $receipt_id,
              'post_status' => 1,
              'customer_id' => $qb_customer_id,
              'receipt_id' => $qb_receipt_id,
              'request_single_receipt' => $receipt_amount,
            );
            return self::insertDrupalDB($data);
          }
        }
      }
    }
  }

  /**
   * Query move_quickbooks table by receipt id.
   *
   * @param int $receipt_id
   *   The receipt ID from moveadmin_receipt table.
   *
   * @return int
   *   Returns number of rows that have specified argument moveadmin receipt id.
   */
  public static function receiptRecordExists($receipt_id) {
    return db_select('move_quickbooks', 'mq')
      ->fields('mq', array('moveadmin_receipt_id'))
      ->condition('mq.moveadmin_receipt_id', $receipt_id)
      ->execute()
      ->rowCount();
  }

  /**
   * Compares the number of receipts in moveadmin_receipt.
   *
   * With move_quickbooks table to determine whether sales.
   *
   * Receipts are posted to QuickBooks account of the company or not.
   *
   * @param int $entity_id
   *   The request ID.
   *
   * @return int
   *   Returns 0 if request sales receipts are not posted,
   *   else returns 1 if all sales receipts of the request are posted
   *   to Quickbooks, finally returns 2 if only some
   *   of the request sales receipts posted to Quickbooks.
   */
  public function getMQReceiptRecord($entity_id) {
    $qb_receipt_ids_drupal_db = QBSalesReceipt::checkMQReceiptRecords($entity_id);
    $request_receipts = MoveRequest::getReceipt($entity_id);
    $in_array_counter = 0;
    foreach ($request_receipts as $key => $value) {
      if (in_array($value['id'], $qb_receipt_ids_drupal_db)) {
        $in_array_counter += 1;
      }
    }

    if ($in_array_counter == 0) {
      $result = 0;
    }
    elseif (count($request_receipts) == $in_array_counter) {
      $result = 1;
    }
    else {
      $result = 2;
    }

    return $result;
  }

}
