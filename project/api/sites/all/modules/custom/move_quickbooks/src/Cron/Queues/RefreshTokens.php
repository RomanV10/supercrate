<?php

namespace Drupal\move_quickbooks\Cron\Queues;

use Drupal\move_services_new\Cron\Queues\QueueInterface;
use QBO;

/**
 * Class RefreshTokens.
 *
 * @package Drupal\move_quickbooks\Cron\Queues
 */
class RefreshTokens implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \QBOException
   * @throws \Exception
   */
  public static function execute($data): void {
    // Create a QBO object for the context that needs refreshing.
    $qbo = new QBO($data['context'], $data['args']);

    // Check that these keys still need to be refreshed.
    if ($qbo->keys() && move_quickbooks_token_needs_refresh($qbo->keys['auth_date'])) {
      // Refresh the tokens.
      if (!$qbo->reconnect()) {
        throw new \Exception("Unable to refresh tokens for QBO context: " . $qbo->context->id());
      }
    }
  }

}
