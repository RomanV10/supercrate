<?php

namespace Drupal\move_quickbooks\Services;

use Drupal\move_services_new\Services\move_request\MoveRequestSearch;
use Drupal\move_services_new\Util\enum\RequestSearchTypes;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class Cron.
 *
 * @package Drupal\move_quickbooks\Services
 */
class Cron {

  /**
   * Prepare array of request ids.
   *
   * For bulk posting receipts to QuickBooks and execute cron queue.
   *
   * @param int $date_filter_from
   *   Unix time to filter requests - date from.
   * @param int $date_filter_to
   *   Unix time to filter requests - date to.
   *
   * @throws \Exception
   */
  public static function prepareDataForCron($date_filter_from, $date_filter_to) {
    $queue = \DrupalQueue::get('move_quickbooks_bulk_post_queue');
    $queue->createQueue();

    $query = new MoveRequestSearch(array(
      'condition' => array('field_approve' => array(RequestStatusTypes::CONFIRMED)),
      'filtering' => array('field_date_from' => $date_filter_from, 'field_date_to' => $date_filter_to),
      'display_type' => RequestSearchTypes::QUICK_BOOKS,
    ));
    $result = $query->search(FALSE);

    foreach ($result['nodes'] as $value) {
      $queue->createItem($value);
    }
  }

}
