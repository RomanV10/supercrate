<?php
/**
 * @file
 * move_mail.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function move_mail_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access move_mail'.
  $permissions['access move_mail'] = array(
    'name' => 'access move_mail',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'move_mail',
  );

  // Exported permission: 'administer move_mail'.
  $permissions['administer move_mail'] = array(
    'name' => 'administer move_mail',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'move_mail',
  );

  // Exported permission: 'create new move_mail'.
  $permissions['create new move_mail'] = array(
    'name' => 'create new move_mail',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'move_mail',
  );

  // Exported permission: 'delete own move_mail'.
  $permissions['delete own move_mail'] = array(
    'name' => 'delete own move_mail',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'move_mail',
  );

  // Exported permission: 'write move_mail'.
  $permissions['write move_mail'] = array(
    'name' => 'write move_mail',
    'roles' => array(
      'administrator' => 'administrator',
      'manager' => 'manager',
    ),
    'module' => 'move_mail',
  );

  return $permissions;
}
