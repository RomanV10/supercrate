<?php

namespace Drupal\move_mail\Cron\Queues;

use Drupal\move_services_new\Cron\Queues\QueueInterface;

/**
 * Class EmailQueueProcess.
 *
 * @package Drupal\move_mail\Queues
 */
class EmailQueueProcess implements QueueInterface {

  /**
   * Process email queue.
   *
   * @param mixed $data
   *   Data.
   */
  public static function execute($data):void {
    /* @var \Swift_Mailer $mailer */
    try {
      // variable_set("locked_queue", TRUE);.
      $item = db_query('SELECT * FROM {queue} WHERE name = :name ORDER BY item_id ASC LIMIT 1', [':name' => 'move_email_queue'])->fetchAssoc();
      foreach ($data as $key => $datum) {
        try {
          $mailer = $datum['mailer'];
          $message = $datum['message'];
          $result = $mailer->send($message);

          if ($result) {
            // Delete message if sending successful.
            unset($data[$key]);
            $writen_data = serialize($data);
            db_merge('queue')
              ->key(['item_id' => $item['item_id']])
              ->fields(['data' => $writen_data])
              ->execute();
          }
        }
        catch (\Throwable $e) {
          // Add log about error to db.
          $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
          watchdog("Smtp error mail", $error_text, array(), WATCHDOG_ERROR);
        }
      }
    }
    finally {
      variable_del("locked_queue");
    }
  }

}
