<?php

namespace Drupal\move_mail\Services;

use Drupal\move_mail\System\Common;
use Drupal\move_mail\System\Compose;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Users;


/**
 * Class Mail.
 *
 * @package Drupal\move_mail\Services
 */
class Mail extends BaseService {
  public $user = NULL;
  private $id;
  private $settings = array();

  public function __construct($id = NULL) {
    global $user;
    $this->user = $user;
    $this->id = $id;
    date_default_timezone_set('UTC');
    $this->settings = Users::getUserSettings($this->user->uid);
  }

  public function update($data = array()) {
    // TODO: Implement update() method.
  }

  public function delete() {
    $wrapper = entity_metadata_wrapper('move_mail_message', $this->id);
    $folder = $wrapper->folder_id->value();
    $mid = $wrapper->mid->value();
    $imap_resource = Common::connectToServer($this->user->uid, $folder->folder_name);
    imap_delete($imap_resource, $mid);
    imap_expunge($imap_resource);
    imap_close($imap_resource);
    entity_delete('move_mail_message', $this->id);
  }

  public function create($data = array()) {
    $result = FALSE;
    if (isset($data['subject']) && isset($data['body']) && isset($data['to'])) {
      $compose_instance = new Compose($data);
      $compose_instance->send();
      $result = $compose_instance->getStatus();
    }

    return $result;
  }

  public function retrieve() {
    $entity_message = entity_load('move_mail_message', array($this->id), array(
      "user_id" => $this->user->uid,
    ));
    if ($entity_message) {
      $entity_message = reset($entity_message);
      // Attachments files.
      $attachments = array();
      $fids = db_select('move_mail_files', 'mm')
        ->fields('mm', array('file_id'))
        ->condition('mm.mid', $entity_message->mid)
        ->condition('mm.uid', $this->user->uid)
        ->execute()->fetchCol();

      if ($fids) {
        foreach ($fids as $fid) {
          $file = file_load($fid);
          $attachments[] = file_create_url($file->uri);
        }

        if ($attachments) {
          $entity_message->attachments = $attachments;
        }
      }
    }

    return $entity_message ? $entity_message : array();
  }

  public function index($page, $page_size, $folder) {
    $result = array();
    $table_alias = 'm';
    $mail_select = db_select('move_mail_messages', $table_alias)
      ->orderBy('id', 'DESC');
    $this->buildIndexQuery($mail_select, $page, $page_size, 'id', 'mail', $table_alias, array(
      'folder_id' => $folder,
      'user_id' => $this->user->uid,
    ));
    $ids = $this->buildIndexList($this->executeIndexQuery($mail_select));
    foreach ($ids as $id) {
      $this->id = $id->id;
      $result[] = $this->retrieve();
    }

    return $result;
  }

  public function getFolders() {
    $folders = array();

    $entity_folders = entity_load('move_mail_folders', FALSE, array(
      "uid" => $this->user->uid,
    ));
    foreach ($entity_folders as $entity_folder) {
      $folders[$entity_folder->fid] = array(
        'name' => $entity_folder->folder_name,
        'unread' => $this->getUnreadCount($entity_folder->fid),
      );
    }

    return $folders;
  }

  public function changeRead($id) {
    $wrapper = entity_metadata_wrapper('move_mail_message', $id);
    $wrapper->m_unread->set(1);
    $wrapper->save();
    return TRUE;
  }

  public function getUnreadCount($folder_id) {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'move_mail_message')
      ->propertyCondition('m_unread', 0)
      ->propertyCondition('folder_id', $folder_id)
      ->addMetaData('account', $this->user);

    $count = $query->count()->execute();
    if ($count !== FALSE) {
      return $count;
    }
    else {
      throw new \ServicesException(t('An error occurred.'), 406);
    }
  }

  public function createFolder($name) {
    $imap_resource = Common::connectToServer($this->user->uid, OP_HALFOPEN);
    $ss = Common::getServerString($this->settings['email_account']['incoming']);
    $status = @imap_createmailbox($imap_resource, $ss . $name);
    if ($status) {
      $value = array(
        'uid' => $this->user->uid,
        'folder_name' => $name,
      );
      entity_create('move_mail_folders', $value)->save();
    }

    imap_close($imap_resource);
    return $status;
  }

  public function deleteFolder($fid) {
    $status = FALSE;
    $entity_folder = entity_load('move_mail_folders', array($fid), array(
      "uid" => $this->user->uid,
    ));
    if ($entity_folder) {
      $entity_folder = reset($entity_folder);
      $imap_resource = Common::connectToServer($this->user->uid, OP_HALFOPEN);
      $ss = Common::getServerString($this->settings['email_account']['incoming']);
      $status = @imap_deletemailbox($imap_resource, $ss . $entity_folder->folder_name);
      if ($status) {
        entity_delete('move_mail_folders', $fid);
      }
      imap_close($imap_resource);
    }

    return $status;
  }

  public function renameFolder($fid, $f_name) {
    $status = FALSE;
    $entity_folder = entity_load('move_mail_folders', array($fid), array(
      "uid" => $this->user->uid,
    ));
    if ($entity_folder) {
      $entity_folder = reset($entity_folder);
      $old_name = $entity_folder->folder_name;
      if ($old_name != $f_name) {
        $imap_resource = Common::connectToServer($this->user->uid, OP_HALFOPEN);
        $ss = Common::getServerString($this->settings['email_account']['incoming']);
        $status = @imap_renamemailbox($imap_resource, $ss . $old_name, $ss . $f_name);
        if ($status) {
          $entity_folder = entity_metadata_wrapper('move_mail_folders', $entity_folder);
          $entity_folder->folder_name->set($f_name);
          $entity_folder->save();
        }
      }
    }

    return $status;
  }

}
