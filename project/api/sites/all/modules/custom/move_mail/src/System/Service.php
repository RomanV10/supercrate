<?php

namespace Drupal\move_mail\System;

use Drupal\move_mail\Services\Mail;
use Drupal\move_mail\System\Common;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'mail' => array(
        'operations' => array(
          'retrieve' => array(
            'help' => 'Retrieve mail',
            'callback' => 'Drupal\move_mail\System\Service::mailRetrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'optional' => FALSE,
                'source' => array('path' => 0),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'create' => array(
            'help' => 'Compose new mail',
            'callback' => 'Drupal\move_mail\System\Service::mailCreate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'optional' => FALSE,
                'source' => array('data' => 'data'),
                'type' => 'array',
              ),
            ),
            'access arguments' => array('write move_mail'),
          ),
          'delete' => array(
            'help' => 'Delete mail with delete in mail server',
            'callback' => 'Drupal\move_mail\System\Service::mailDelete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'optional' => FALSE,
                'source' => array('path' => 0),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('delete own move_mail'),
          ),
          'index' => array(
            'help' => 'All message from folder',
            'callback' => 'Drupal\move_mail\System\Service::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'page',
                'optional' => FALSE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 0.',
                'default value' => 0,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'pagesize',
                'optional' => FALSE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pagesize'),
              ),
              array(
                'name' => 'folder',
                'optional' => FALSE,
                'type' => 'int',
                'source' => array('param' => 'folder'),
              ),
            ),
            'access arguments' => array('access move_mail'),
          ),
        ),
        'actions' => array(
          'get_folders' => array(
            'help' => 'Get all mail folders for current user',
            'callback' => 'Drupal\move_mail\System\Service::getFolders',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access move_mail'),
          ),
          'change_read' => array(
            'help' => 'Change message to Read.',
            'callback' => 'Drupal\move_mail\System\Service::changeRead',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'optional' => FALSE,
                'source' => array('path' => 1),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access move_mail'),
          ),
          'create_folder' => array(
            'help' => 'Create folder',
            'callback' => 'Drupal\move_mail\System\Service::createFolder',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'name',
                'optional' => FALSE,
                'source' => array('data' => 'name'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access move_mail'),
          ),
          'delete_folder' => array(
            'help' => 'Delete folder',
            'callback' => 'Drupal\move_mail\System\Service::deleteFolder',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'fid',
                'optional' => FALSE,
                'source' => array('data' => 'fid'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access move_mail'),
          ),
          'rename_folder' => array(
            'help' => 'Rename an old mailbox to new mailbox',
            'callback' => 'Drupal\move_mail\System\Service::renameFolder',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'fid',
                'optional' => FALSE,
                'source' => array('data' => 'fid'),
                'type' => 'int',
              ),
              array(
                'name' => 'new_name',
                'optional' => FALSE,
                'source' => array('data' => 'new_name'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access move_mail'),
          ),
          'get_unread_count' => array(
            'help' => 'Get count unread message',
            'callback' => 'Drupal\move_mail\System\Service::getUnreadCount',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'optional' => FALSE,
                'source' => array('data' => 'folder_id'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access move_mail'),
          ),
          'test_parser_connection' => array(
            'help' => 'Test parser connection to server',
            'callback' => 'Drupal\move_mail\System\Service::testParserConnection',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access move_mail'),
          ),
          'test_mail_connection' => array(
            'help' => 'Test mail connection to server',
            'callback' => 'Drupal\move_mail\System\Service::testMailConnection',
            'file' => array(
              'type' => 'php',
              'module' => 'move_mail',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'name',
                'optional' => FALSE,
                'type' => 'string',
                'source' => array('data' => 'name'),
              ),
              array(
                'name' => 'outgoing',
                'optional' => FALSE,
                'type' => 'array',
                'source' => array('data' => 'outgoing'),
              ),
              array(
                'name' => 'password',
                'optional' => FALSE,
                'type' => 'string',
                'source' => array('data' => 'password'),
              ),
            ),
            'access arguments' => array('access move_mail'),
          ),
        ),
      ),
    );
  }

  public static function index($page, $pagesize, $folder) {
    $mail_instance = new Mail();
    return $mail_instance->index($page, $pagesize, $folder);
  }

  public static function mailRetrieve($id) {
    $mail_instance = new Mail($id);
    return $mail_instance->retrieve();
  }

  public static function mailCreate($data = array()) {
    $mail_instance = new Mail();
    return $mail_instance->create($data);
  }

  public static function mailDelete($id) {
    $mail_instance = new Mail($id);
    $mail_instance->delete();
  }

  public static function getFolders() {
    $mail_instance = new Mail();
    return $mail_instance->getFolders();
  }

  public static function changeRead($id) {
    $mail_instance = new Mail();
    return $mail_instance->changeRead($id);
  }

  public static function getUnreadCount($folder_id) {
    $mail_instance = new Mail();
    return $mail_instance->getUnreadCount($folder_id);
  }

  public static function createFolder($name) {
    $mail_instance = new Mail();
    return $mail_instance->createFolder($name);
  }

  public static function deleteFolder($fid) {
    $mail_instance = new Mail();
    return $mail_instance->deleteFolder($fid);
  }

  public static function renameFolder($fid, $f_name) {
    $mail_instance = new Mail();
    return $mail_instance->renameFolder($fid, $f_name);
  }

  public static function testParserConnection() {
    $common_instance = new Common();
    return $common_instance->testParserConnection();
  }

  public static function testMailConnection($name, $outgoing, $password) {
    $common_instance = new Common();
    return $common_instance->testMailConnection($name, $outgoing, $password);
  }

}
