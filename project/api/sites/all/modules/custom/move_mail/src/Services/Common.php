<?php

namespace Drupal\move_mail\Services;

use Drupal\move_services_new\Services\Users;

/**
 * Class Common.
 *
 * @package Drupal\move_mail\Services
 */
class Common {

  public static function getMailUsers() {
    $mail_users = array();
    $roles = array('manager', 'administrator');
    $uids = Users::getUserByRole($roles, TRUE, TRUE);

    foreach ($uids as $uid) {
      $user = user_load($uid);
      if (!empty($user->mail) && !empty($user->data['move_mail_password'])) {
        $mail_users[] = $uid;
      }
    }

    return $mail_users;
  }

}
