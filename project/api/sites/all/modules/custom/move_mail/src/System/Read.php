<?php

namespace Drupal\move_mail\System;

use PhpImap\Mailbox as ImapMailbox;

/**
 * Class Read.
 *
 * @package Drupal\move_mail\System
 */
class Read {

  /**
   * @var	ImapMailbox
   */
  private $imapMailbox;

  public function __construct(ImapMailbox $imapMailbox) {
    date_default_timezone_set('UTC');
    if (!$imapMailbox) {
      return FALSE;
    }

    $this->imapMailbox = $imapMailbox;
  }

  public function getNewMessages(int $uid, int $folder_id) : array {
    $result = array();
    // Check for first execute.
    $mails = entity_load('move_mail_message', FALSE, array(
      "user_id" => $uid,
      "folder_id" => $folder_id,
    ));

    // For get mails only one from.
    // $message_uids = $this->imapMailbox->searchMailbox("ALL FROM info@mymovingloads.com");
    $message_uids = $this->imapMailbox->sortMails(SORTDATE);
    // There are have no saved messages.
    if (!$mails) {
      $result = array(end($message_uids));
    }
    else {
      // @TODO Provide reset all indexes and download all emails.
      // Finding last downloaded mail.
      $last_d_mail = end($mails);
      $last_muid = (int) $last_d_mail->mid;
      $message_uids[] = $last_muid;
      sort($message_uids);

      for ($i = 0, $find_key = 1; $i < count($message_uids) - 1; $i++) {
        if ($message_uids[$i + 1] == $last_muid) {
          if ($i + 2 >= count($message_uids) || $last_muid - $message_uids[$i] < $message_uids[$i + 2] - $last_muid) {
            $find_key = $i;
          }
          else {
            $find_key = $i + 2;
          }
          break;
        }
      }
      $result = array_slice($message_uids, $find_key + 1);
    }

    return $result;
  }

  public function getMessage($muid, $uid, $fid) {
    $result = array();
    $mail = $this->imapMailbox->getMail($muid);
    $result['message_subject'] = $mail->subject;
    $result['to_address'] = $mail->toString;
    $result['from_address'] = $mail->fromAddress;
    $result['date'] = $mail->date;
    $result['message_body'] = $mail->textPlain;
    $result['original'] = $mail->textHtml;
    return $result;
  }

  public function setSeenFlag($mid) : void {
    $this->imapMailbox->markMailAsRead($mid);
  }

}
