<?php

namespace Drupal\move_mail\System;

use Drupal\move_services_new\Services\Users;
use PhpImap\Mailbox as ImapMailbox;

/**
 * Class Common.
 *
 * @package Drupal\move_mail\System
 */
class Common {

  /**
   * @var	ImapMailbox
   */
  private $imap;

  public function connectToServer($uid = NULL, $folder = 'INBOX') : void {
    if ($uid === NULL) {
      self::reportError("Wrong use connectToServer method.");
    }

    // Retrieve the user object for the user accessing the mail server.
    if ($uid) {
      $user = user_load($uid);
      $settings = Users::getUserSettings($uid);
      if (!isset($settings['email_account']) || !isset($settings['email_account']['name'])) {
        $message = '!name has not configured their !user_settings_link.';
        $message_parameters = array(
          '!name' => $user->name,
          '!user_settings_link' => l(t('mail settings'), 'user/' . $user->uid . '/edit'),
        );
        self::reportError($message, $message_parameters);
      }
    }
    else {
      $settings['email_account'] = Users::getUserParser();
    }

    // Get the users email login and password.
    $username = $settings['email_account']['name'];
    $password = self::decryptPassword($settings['email_account']['password']);
    $mailbox = self::getServerString($settings['email_account']['incoming']) . $folder;
    $this->imap = new ImapMailbox($mailbox, $username, $password);
  }

  public function connectToParseServer($folder = 'INBOX') : void {
    $parser_data = variable_get('parser_data');
    if (!isset($parser_data) || !isset($parser_data['name'])) {
      $message = 'Parser has not configured';
      self::reportError($message);
    }
    // Get the users email login and password.
    $username = $parser_data['name'];
    $password = self::decryptPassword($parser_data['password']);
    $mailbox = self::getServerString($parser_data['incoming']) . $folder;
    $this->imap = new ImapMailbox($mailbox, $username, $password);
  }

  public function testParserConnection() {
    $response = array('status' => '200', 'statusText' => 'OK');
    try {
      $this->connectToParseServer();
      $this->imap->getImapStream();
    }
    catch (\Throwable $e) {
      $response['status'] = $e->getCode() != 0 ? $e->getCode() : 500;
      $response['statusText'] = $e->getMessage();
    }
    finally {
      return $response;
    }
  }

  /**
   * Test mail connection for department users.
   *
   * @param string $name
   *   The User name.
   * @param array $outgoing
   *   Array with settings for connection.
   * @param string $password
   *   The password of user's.
   *
   * @return int|string
   *   Result of test.
   */
  public function testMailConnection(string $name, array $outgoing, string $password) {
    try {
      $host = $outgoing['server'];
      $port = $outgoing['port'];

      $encryption = NULL;
      if ($outgoing['tls']) {
        $encryption = 'tls';
      }
      elseif ($outgoing['ssl']) {
        $encryption = 'ssl';
      }
      $transport = \Swift_SmtpTransport::newInstance($host, $port, $encryption);

      $transport->setUsername($name);
      $transport->setPassword($password);

      $mailer = \Swift_Mailer::newInstance($transport);
      $mailer->getTransport()->start();
      return 1;
    }
    catch (\Throwable $e) {
      return $e->getMessage();
    }
  }

  /**
   * Test mail connection for department users.
   *
   * @param string $name
   *   The User name.
   * @param array $outgoing
   *   Array with settings for connection.
   * @param string $password
   *   The password of user's.
   *
   * @return int|string
   *   Result of test.
   */
  public function testMailConnectionForCron(string $name, array $outgoing, string $password) {
    try {
      $host = $outgoing['server'];
      $port = $outgoing['port'];

      $encryption = NULL;
      if ($outgoing['tls']) {
        $encryption = 'tls';
      }
      elseif ($outgoing['ssl']) {
        $encryption = 'ssl';
      }
      $transport = \Swift_SmtpTransport::newInstance($host, $port, $encryption);

      $transport->setUsername($name);
      $transport->setPassword($password);

      $mailer = \Swift_Mailer::newInstance($transport);
      $mailer->getTransport()->start();
      return 1;
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("testMailConnectionForCron", $error_text, array(), WATCHDOG_ERROR);
      // Ignoring error without code.
      if (strripos($e->getMessage(), 'but got code ""') !== FALSE) {
        return 1;
      }
      // Ignoring error "timed out".
      if (strripos($e->getMessage(), 'Timed Out') !== FALSE) {
        return 1;
      }
      // Ignoring error "Connection could not be established with host".
      if (strripos($e->getMessage(), 'Connection could not be established with host') !== FALSE) {
        return 1;
      }
      return $e->getMessage();
    }
  }

  public function disconnect() : void {
    $this->disconnect();
  }

  /**
   * @return \PhpImap\Mailbox
   */
  public function getImapResource() {
    return $this->imap;
  }

  /**
   * Encrypts a user's password.
   *
   * If neither of the Encryption (http://drupal.org/project/encrypt) module are
   * installed, this function will simply return the value passed in.
   *
   * @param string $password
   *   The password to encrypt.
   *
   * @return bool|string
   *   The encrypted password, if the option to encrypt is enabled and an
   *   encryption module is installed, or the passed in value, if not.
   */
  public static function encryptPassword($password) {
    if (variable_get('move_mail_use_encryption', 0) == 1) {
      $password = aes_encrypt($password);
    }

    return $password;
  }

  /**
   * Decrypts a user's password.
   *
   * If neither of the Encryption (http://drupal.org/project/encrypt)
   * modules are installed, this function will simply return the value passed in.
   *
   * @param string $password
   *   The password to decrypt.
   *
   * @return bool|string
   *   The decrypted password, if the option to encrypt is enabled and an
   *   encryption module is installed, or the passed in value, if not.
   */
  public static function decryptPassword($password) {
    if (variable_get('move_mail_use_encryption', 0) == 1) {
      // Decrypt the password.
      $password = aes_decrypt($password);
    }

    return $password;
  }

  public static function getServerString($settings) {
    // Initialize variable.
    $server_address = $settings['server'];

    // Sanity check -- make sure the admin has done the configuration.
    if (!isset($settings['server']) || !isset($settings['port'])) {
      $message = 'The User email account have not yet been configured. Please configure email settings before checking your email.';
      self::reportError($message);
      return FALSE;
    }

    // Prepare the flags for connecting to the server with.
    $imap_flags = '/novalidate-cert';
    if (isset($settings['ssl']) && $settings['ssl'] == 1) {
      $imap_flags .= '/ssl';
    }
    elseif (isset($settings['tls']) && $settings['tls'] == 1) {
      $imap_flags .= '/tls';
    }

    // Return the configured settings and create a connection string.
    $result = '{' . $server_address . ':' . $settings['port'] . $imap_flags . '}';

    return $result;
  }

  /**
   * Reports error messages.
   *
   * @param string $message
   *   The additional text to be saved and/or displayed.
   * @param int $severity
   *   The severity of the message, as per RFC 3164. See
   *   http://api.drupal.org/api/drupal/includes--bootstrap.inc/group/logging_severity_levels/7
   *   for more information.
   */
  public static function reportError($message, $message_parameters = array(), $severity = WATCHDOG_ERROR) {
    if ($severity != WATCHDOG_INFO && $severity != WATCHDOG_NOTICE) {
      // Get the most recent IMAP error message.
      $last_error = imap_last_error();
      if ($last_error === FALSE) {
        // There was no recent IMAP error message, so add a generic message.
        $message_parameters['!last_error'] = t('There was no additional information provided by the system.');
      }
      else {
        $message_parameters['!last_error'] = $last_error;
      }
    }

    // The message is to be saved in the system logs.
    watchdog('move_mail', $message, $message_parameters, $severity);
  }

  public function getListFolders() {
    $folder_names = array();
    $folders = $this->imap->getListingFolders();

    if (!empty($folders)) {
      foreach ($folders as $folder_name) {

      }
    }

    return $folder_names;
  }

  public static function saveNewFolders($uid, $folders = array()) {
    foreach ($folders as $folder_name) {
      $entity_folder = entity_load_multiple_by_name('move_mail_folders', FALSE, array(
        "uid" => $uid,
        "folder_name" => $folder_name,
      ));

      if (!$entity_folder) {
        $record = move_mail_folders_new();
        $record->uid = $uid;
        $record->folder_name = $folder_name;
        entity_save('move_mail_folders', $record);
      }
    }
  }

  public static function checkMessageExist($uid, $muid, $folder_id) {
    $entity_message = entity_load_multiple_by_name('move_mail_message', FALSE, array(
      "user_id" => $uid,
      "mid" => $muid,
      "folder_id" => $folder_id,
    ));

    return $entity_message ? TRUE : FALSE;
  }

  public static function getFolderName($uid, $folder_id) {
    $entity_folder = entity_load('move_mail_folders', FALSE, array(
      "uid" => $uid,
      "fid" => $folder_id,
    ));

    if ($entity_folder) {
      $entity_folder = reset($entity_folder);
    }

    return $entity_folder ? $entity_folder->folder_name : '';
  }

  public static function getFolderId($uid, $folder_name) {
    $entity_folder = entity_load('move_mail_folders', FALSE, array(
      "uid" => $uid,
      "folder_name" => $folder_name,
    ));

    if ($entity_folder) {
      $entity_folder = reset($entity_folder);
    }

    return $entity_folder ? $entity_folder->fid : 0;
  }

  public static function replace4byte($string) {
    return preg_replace('%(?:
          \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
    )%xs', '', (string) $string);
  }

  public static function clean_string($string) {
    // Drop all non utf-8 characters.
    $s = Encoding::fixUTF8((string) $string);
    // Reduce all multiple whitespace to a single space.
    $s = nl2br($s, FALSE);
    $s = preg_replace('/[\r\n]/', '', $s);
    $s = preg_replace('/[\n]/', '', $s);
    $s = preg_replace('/(<br[^>]*>)(?:\s*\1)+/', '$1', $s);
    // Just in case convert any html entities to UTF-8 characters.
    $s = html_entity_decode($s);
    return $s;
  }

  public static function clearHtml($str) {
    $purifierConfig = \HTMLPurifier_Config::createDefault();
    $purifier = new \HTMLPurifier($purifierConfig);
    $clean_html = $purifier->purify($str);
    $clean_html = htmlspecialchars_decode($clean_html);
    $patterns = array('/\&lt\;/', '/\&gt\;/');
    $clean_html = preg_replace($patterns, '', $clean_html);
    return $clean_html ? $clean_html : "";
  }

}
