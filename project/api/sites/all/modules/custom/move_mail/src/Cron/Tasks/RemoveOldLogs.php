<?php

namespace Drupal\move_mail\Cron\Tasks;

use Drupal\move_services_new\Cron\Tasks\CronInterface;

/**
 * Class RemoveOldLogs.
 *
 * @package Drupal\move_mail\Cron
 */
class RemoveOldLogs implements CronInterface {

  /**
   * Delete old log.
   */
  public static function execute():void {
    db_delete("move_mail_email_queue_messages")
      ->condition("created", time() - 2592000, "<=")
      ->execute();
  }

}
