<?php

namespace Drupal\move_mail\System;

use Drupal\move_services_new\Services\Users;
use Swift_Message;
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Attachment;

class Compose {
  private $attachments = array();
  private $savedFiles = array();
  private $message;
  private $resource;
  private $userName;
  private $userSettings;
  private $password;
  private $host;
  private $port;
  private $mailbox;
  private $folderName = 'Sent';
  private $folderId;
  private $mime;
  private $status = FALSE;

  public function __construct($data = array()) {
    include_once DRUPAL_ROOT . '/includes/file.mimetypes.inc';
    global $user;
    $this->user = $user;
    $this->mime = file_mimetype_mapping();
    $this->message = Swift_Message::newInstance();
    $this->folderId = Common::getFolderId($this->user->uid, $this->folderName);
    $this->userSettings = Users::getUserSettings($this->user->uid);

    $this->message->setTo(array($data['to'] => $data['to']));
    $this->message->setSubject($data['subject']);
    $this->message->setBody($data['body']);

    if (isset($data['cc'])) {
      $this->message->setCc($data['cc']);
    }
    if (isset($data['bcc'])) {
      $this->message->setBcc($data['bcc']);
    }

    $this->attachments = isset($data['attachments']) ? $data['attachments'] : array();
    $this->resource = Common::connectToServer($user->uid, $this->folderName);
    $this->mailbox = Common::getServerString($this->userSettings['email_account']['outgoing']) . $this->folderName;
    $this->userName = $user->mail;
    $this->password = Common::decryptPassword($user->data['move_mail_password']);
    $this->host = variable_get('move_mail_server_address', 'localhost');
    $this->port = variable_get('move_mail_server_port_smtp', '25');
  }

  public function send() {
    if (!isset($this->userSettings['email_account'])) {
      return FALSE;
    }

    $this->message->setFrom(array($this->userName => $this->userName));

    $transport = Swift_SmtpTransport::newInstance($this->host, $this->port)
      ->setPassword($this->password)
      ->setUsername($this->userName);

    // Create the Mailer.
    $mailer = Swift_Mailer::newInstance($transport);

    // @todo Check for errors.
    // Attach files to message.
    if ($this->attachments) {
      foreach ($this->attachments as $attachment) {
        if (isset($attachment['data']) && isset($attachment['name'])) {
          $file_exp = explode(',', $attachment['data']);
          $exp_code = explode(';', $file_exp[0]);
          $exp_mimetype = explode(':', $exp_code[0]);
          $extension = $this->getExtension($exp_mimetype[1]);
          $uploaded_file = $this->file($file_exp[1], $attachment['name'] . '.' . $extension, $exp_mimetype);

          $this->savedFiles[] = $uploaded_file;
          if ($uploaded_file) {
            $swift_attachment = Swift_Attachment::fromPath($uploaded_file->uri);
            $this->message->attach($swift_attachment);
          }
        }
      }
    }

    // Send the message.
    $mailer->send($this->message);

    // Append message to 'Send' mailbox.
    $msg = $this->message->toString();
    imap_append($this->resource, $this->mailbox, $msg . "\r\n", "\\Seen");
    $check = imap_check($this->resource);
    if ($this->savedFiles) {
      foreach ($this->savedFiles as $saved_file) {
        $this->systemMessage($saved_file->uri, $check->Nmsgs);
      }
    }

    // Save message to local "Sent" folder.
    $record = move_mail_message_new();
    $record->user_id = $this->user->uid;
    $record->mid = $check->Nmsgs;
    $record->m_subject = $this->message->getSubject();
    $record->address_from = $this->userName;
    $record->m_date = $this->message->getDate();
    $record->m_unread = TRUE;
    $record->m_answered = FALSE;
    $record->m_body = $this->message->getBody();
    $record->folder_id = $this->folderId;
    $record->attachment_exists = $this->attachments ? TRUE : FALSE;
    entity_save('move_mail_message', $record);

    imap_close($this->resource);
    $this->status = TRUE;
  }

  private function file($file, $file_name, $exp_mimetype) {
    $file_data = @base64_decode((string) $file);

    // Create the destination path variable.
    $file_dest = variable_get('move_mail_attachment_location', file_default_scheme() . '://attachments');

    // Add the user's login ID to the path.
    $file_dest .= "/{$this->user->name}/$file_name";

    // Write the file.
    $file_saved = file_save_data($file_data, $file_dest);
    if ($file_saved) {
      file_usage_add($file_saved, 'move_mail', 'files', $file_saved->fid);
      $file_saved->filemime = $exp_mimetype[1];
      file_save($file_saved);
    }

    return $file_saved;
  }

  private function systemMessage($file_uri, $mess_num) {
    $files = file_load_multiple(array(), array('uri' => $file_uri));
    $file = reset($files);

    $record = array(
      'mid' => $mess_num,
      'uid' => $this->user->uid,
      'file_id' => $file->fid,
      'folder_id' => $this->folderId,
    );
    drupal_write_record('move_mail_files', $record);
  }

  public function getExtension($mimetype) {
    $extension = 'txt';
    $key = array_search($mimetype, $this->mime['mimetypes']);
    if ($key !== FALSE) {
      $find = array_search($key, $this->mime['extensions']);
      if ($find !== FALSE) {
        $extension = $find;
      }
    }

    return $extension;
  }

  public function getStatus() {
    return $this->status;
  }

}
