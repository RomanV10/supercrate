<?php
/**
 * @file
 * This file handles the administrative settings and configuration.
 */

/**
 * Administration settings form.
 *
 * @return array
 *   The completed form definition.
 */
function move_mail_admin_settings($form, &$form_state) {
  $form = array();
  // The user set of fields for the form.
  $form['move_mail_user_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('User settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['move_mail_user_settings']['move_mail_attachment_location'] = array(
    '#type' => 'textfield',
    '#title' => t('Attachment location'),
    '#default_value' => variable_get('move_mail_attachment_location', file_default_scheme() . '://attachments'),
    '#description' => t('The file path where to save message attachments.'),
  );

  $disabled = TRUE;
  if (module_exists('aes')) {
    $disabled = FALSE;
  }

  $form['move_mail_user_settings']['move_mail_use_encryption'] = array(
    '#type' => 'checkbox',
    '#title' => t("Use encryption when saving the user's password."),
    '#disabled' => $disabled,
    '#default_value' => variable_get('move_mail_use_encryption', 0),
    '#description' => t("By default, the login information is saved in clear text in the data field of the user table. Check this box to enable encrypting the passwords before saving them. This option requires either the !aes or !encrypt modules.",
      array(
        '!aes' => l(t('AES encryption'), 'http://drupal.org/project/aes'),
      )
    ),
  );

  return system_settings_form($form);
}

/**
 * Additional validation for the administration settings form.
 *
 * @param array $form
 *   The form definition.
 * @param array $form_state
 *   The form values of the passed form.
 */
function move_mail_admin_settings_validate($form, &$form_state) {
  if (!file_prepare_directory($form_state['values']['move_mail_attachment_location'], FILE_CREATE_DIRECTORY)) {
    form_set_error('move_mail_attachment_location', t('The directory does not exist or is not writable, and there was a problem creating the path.'));
  }
}
