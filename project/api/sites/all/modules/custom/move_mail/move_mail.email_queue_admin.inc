<?php

function move_email_queue_page() {
  $header = array(
    array('data' => 'From', 'field' => 'message_from'),
    array('data' => 'To', 'field' => 'message_to'),
    array('data' => 'Error', 'field' => 'error'),
  );
  $rows = array();

  $data = move_email_queue_get_all();

  foreach ($data as $obj) {
    $rows[] = array(
      'data' => array(
        'from' => $obj->message_from,
        'to' => $obj->message_to,
        'error' => l('exception', "admin/config/custom/email_queue/dpm/$obj->error"),
      ),
    );
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output;
}

function move_email_queue_get_all() {
  $result = db_select('move_mail_email_queue_messages', 'mmeqm')
    ->fields('mmeqm')
    ->execute()
    ->fetchAll();
  return $result;
}

function move_email_queue_dpm_log($error) {
  dpm($error);
  return '<h1>Log</h1>';
}

