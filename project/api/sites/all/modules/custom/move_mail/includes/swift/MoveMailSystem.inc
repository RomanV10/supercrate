<?php

use Drupal\move_services_new\Services\Clients;

/**
 * Class MoveMailSystem.
 */
class MoveMailSystem extends \SWIFTMailSystem {

  public function format(array $message) {
    return parent::format($message);
  }

  public function mail(array $message) {
    // Include required files.
    module_load_include('inc', 'swiftmailer', '/includes/helpers/conversion');
    module_load_include('inc', 'swiftmailer', '/includes/helpers/utilities');


    // Validate whether the Swift Mailer module has been configured.
    // Include the Swift Mailer library.
    require_once DRUPAL_ROOT . '/' . variable_get('swiftmailer_path', SWIFTMAILER_VARIABLE_PATH_DEFAULT) . '/lib/swift_required.php';

    try {

      // Create a new message.
      $m = Swift_Message::newInstance();

      // Not all Drupal headers should be added to the e-mail message.
      // Some headers must be supressed in order for Swift Mailer to
      // do its work properly.
      $suppressable_headers = swiftmailer_get_supressable_headers();

      // Keep track of whether we need to respect the provided e-mail
      // format or not.
      $respect_format = variable_get('swiftmailer_respect_format', SWIFTMAILER_VARIABLE_RESPECT_FORMAT_DEFAULT);

      $manager_uid = (isset($message['params']['manager'])) ? $message['params']['manager'] : 0;

      // Process headers provided by Drupal. We want to add all headers which
      // are provided by Drupal to be added to the message. For each header we
      // first have to find out what type of header it is, and then add it to
      // the message as the particular header type.
      if (!empty($message['headers']) && is_array($message['headers'])) {
        foreach ($message['headers'] as $header_key => $header_value) {

          // Check wether the current header key is empty or represents
          // a header that should be supressed. If yes, then skip header.
          if (empty($header_key) || in_array($header_key, $suppressable_headers)) {
            continue;
          }

          // Skip 'Content-Type' header if the message to be sent will be a
          // multipart message or the provided format is not to be respected.
          if ($header_key == 'Content-Type' && (!$respect_format || swiftmailer_is_multipart($message))) {
            continue;
          }

          // Get header type.
          $header_type = swiftmailer_get_headertype($header_key, $header_value);

          // Add the current header to the e-mail message.
          switch ($header_type) {
            case SWIFTMAILER_HEADER_ID:
              swiftmailer_add_id_header($m, $header_key, $header_value);
              break;

            case SWIFTMAILER_HEADER_PATH:
              swiftmailer_add_path_header($m, $header_key, $header_value);
              break;

            case SWIFTMAILER_HEADER_MAILBOX:
              swiftmailer_add_mailbox_header($m, $header_key, $header_value);
              break;

            case SWIFTMAILER_HEADER_DATE:
              swiftmailer_add_date_header($m, $header_key, $header_value);
              break;

            case SWIFTMAILER_HEADER_PARAMETERIZED:
              swiftmailer_add_parameterized_header($m, $header_key, $header_value);
              break;

            default:
              swiftmailer_add_text_header($m, $header_key, $header_value);
              break;

          }
        }
      }

      // Set basic message details.
      swiftmailer_remove_header($m, 'From');
      swiftmailer_remove_header($m, 'To');
      swiftmailer_remove_header($m, 'Subject');

      // Parse 'from' and 'to' mailboxes.
      $from = swiftmailer_parse_mailboxes($message['from']);
      $to = swiftmailer_parse_mailboxes($message['to']);

      // Set 'from', 'to' and 'subject' headers.
      $m->setFrom($from);
      $m->setTo($to);
      $m->setSubject($message['subject']);

      // Get applicable format.
      $applicable_format = $this->getApplicableFormat($message);

      // Get applicable character set.
      $applicable_charset = $this->getApplicableCharset($message);

      // Set body.
      $m->setBody($message['body'], $applicable_format, $applicable_charset);

      // Add alternative plain text version if format is HTML and plain text
      // version is available.
      if ($applicable_format == SWIFTMAILER_FORMAT_HTML && !empty($message['plain'])) {
        $m->addPart($message['plain'], SWIFTMAILER_FORMAT_PLAIN, $applicable_charset);
      }

      // Validate that $message['params']['files'] is an array.
      if (empty($message['params']['files']) || !is_array($message['params']['files'])) {
        $message['params']['files'] = array();
      }

      // Let other modules get the chance to add attachable files.
      $files = module_invoke_all('swiftmailer_attach', $message['key']);
      if (!empty($files) && is_array($files)) {
        $message['params']['files'] = array_merge(array_values($message['params']['files']), array_values($files));
      }

      // Attach files.
      if (!empty($message['params']['files']) && is_array($message['params']['files'])) {
        $this->attach($m, $message['params']['files']);
      }

      // Embed images.
      if (!empty($message['params']['images']) && is_array($message['params']['images'])) {
        $this->embed($m, $message['params']['images']);
      }

      static $mailer;

      // If required, create a mailer which will be used to send the message.
      if (empty($mailer)) {

        // Get the configured transport type.
        $transport_type = variable_get('swiftmailer_transport', SWIFTMAILER_VARIABLE_TRANSPORT_DEFAULT);

        // Configure the mailer based on the configured transport type.
        if ($transport_type == SWIFTMAILER_TRANSPORT_SMTP) {
          $manager_fields = (new Clients())->getUserFieldsData($manager_uid, FALSE);
          // Check that the user does not send himself mail.
          if ($manager_uid && !empty($manager_fields['settings']['email_account']['outgoing']['active']) && key($from) == $manager_fields['settings']['email_account']['name']) {
            $host = $manager_fields['settings']['email_account']['outgoing']['server'];
            $port = $manager_fields['settings']['email_account']['outgoing']['port'];
            $encryption = '';
            if ($manager_fields['settings']['email_account']['outgoing']['tls']) {
              $encryption = 'tls';
            }
            elseif ($manager_fields['settings']['email_account']['outgoing']['ssl']) {
              $encryption = 'ssl';
            }

            $username = $manager_fields['settings']['email_account']['name'];
            $password = $manager_fields['settings']['email_account']['password'];
          }
          else {
            // Get transport configuration.
            $host = variable_get('swiftmailer_smtp_host', SWIFTMAILER_VARIABLE_SMTP_HOST_DEFAULT);
            $port = variable_get('swiftmailer_smtp_port', SWIFTMAILER_VARIABLE_SMTP_PORT_DEFAULT);
            $encryption = variable_get('swiftmailer_smtp_encryption', SWIFTMAILER_VARIABLE_SMTP_ENCRYPTION_DEFAULT);
            $username = variable_get('swiftmailer_smtp_username', SWIFTMAILER_VARIABLE_SMTP_USERNAME_DEFAULT);
            $password = variable_get('swiftmailer_smtp_password', SWIFTMAILER_VARIABLE_SMTP_PASSWORD_DEFAULT);
          }

          // Instantiate transport.
          $transport = Swift_SmtpTransport::newInstance($host, $port);
          // @TODO Warning, may be set real domain name instead 127.0.0.1
          $transport->setLocalDomain('[127.0.0.1]');

          // Set encryption (if any).
          if (!empty($encryption)) {
            $transport->setEncryption($encryption);
          }

          // Set username (if any).
          if (!empty($username)) {
            $transport->setUsername($username);
          }

          // Set password (if any).
          if (!empty($password)) {
            $transport->setPassword($password);
          }


          $mailer = Swift_Mailer::newInstance($transport);
        }
      }
      drupal_alter('swiftmailer', $mailer, $m);

      $this->setBlocker();
      $queue = DrupalQueue::get('move_mail_send_email');

      // Additional recipients.
      if (!empty($message['params']['cc'])) {
        $m->setCc((array) $message['params']['cc']);
      }

      // Attach files.
      if (!empty($message['params']['files'])) {
        foreach ($message['params']['files'] as $file) {
          $m->attach(Swift_Attachment::fromPath($file['url'])->setFilename($file['name']));
        }
      }

      $metadata_message = array();
      $metadata_message['mailer'] = clone $mailer;
      $metadata_message['message'] = clone $m;
      $metadata_message['metadata'] = $message;

      // get the last item and check count of elements
      $item = db_query('SELECT * FROM {queue} WHERE name = :name ORDER BY item_id DESC LIMIT 1', array(':name' => 'move_email_queue'))->fetchAssoc();
      if (!$item) {
        // if we get nothing
        $queue->createItem(array($metadata_message));
      }
      else {
        $unserialized_items = unserialize($item['data']);
        // if count == 5, we create new item and add new element
        if (count($unserialized_items) == 5) {
          $queue->createItem(array($metadata_message));
        }
        else {
          // else we add new element to this item
          $unserialized_items = array_merge($unserialized_items, array($metadata_message));
          $data = serialize($unserialized_items);
          db_merge('queue')
            ->key(array('item_id' => $item['item_id']))
            ->fields(array('data' => $data))
            ->execute();
        }
      }
    }
    catch (\Throwable $e) {
      $headers = !empty($m) ? $m->getHeaders() : '';
      $headers = !empty($headers) ? nl2br($headers->toString()) : 'No headers were found.';
      watchdog('swiftmailer',
        'An attempt to send an e-mail message failed, and the following error
        message was returned : @exception_message<br /><br />The e-mail carried
        the following headers:<br /><br />!headers',
        array('@exception_message' => $e->getMessage(), '!headers' => $headers),
        WATCHDOG_ERROR);
      drupal_set_message(t('An attempt to send an e-mail message failed.'), 'error');
    }
    finally {
      $this->removeBlocker();
      return 1;
    }
  }

  /**
   * Helper method for block multiple edit request.
   *
   */
  private function setBlocker() : void {
    $endless_protect = 0;
    $lock = variable_get("locked_queue", FALSE);
    while ($lock) {
      usleep(2000);
      $lock = FALSE;

      // Don't use variable_get as it cached.
      $query = db_query("SELECT name, value FROM {variable} WHERE name = 'locked_queue'")->fetchAllKeyed();
      if ($query) {
        $lock = unserialize($query["locked_queue"]);
      }
      // Protect from loop endless.
      if ($endless_protect++ >= 500) {
        $lock = FALSE;
      }

    }
    variable_set("locked_queue", TRUE);
  }

  private function removeBlocker() : void {
    variable_del("locked_queue");
  }

}
