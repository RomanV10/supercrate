<?php

/**
 * @SWG\Definition(required={"field_e_mail"})
 */
class MoveRequestModel {

  /**
   * The email user.
   * @SWG\Property()
   * @var string
   */
  public $field_e_mail;

  /**
   * Node id.
   * @SWG\Property(example="70000")
   * @var integer
   */
  public $nid;

  /**
   * The request status.
   * @SWG\Property(example="1")
   * @var integer
   */
  public $status;

  /**
   * The user id.
   * @SWG\Property(example="1155")
   * @var integer
   */
  public $uid;

  /**
   * The request title.
   * @SWG\Property(example="Move Request")
   * @var string
   */
  public $title;

  /**
   * @SWG\Property()
   * @var MoveRequestPostalField[]
   */
  public $field_moving_from;

  /**
   * @SWG\Property()
   * @var MoveRequestPostalField[]
   */
  public $field_moving_to;

  /**
   * The date field.
   * @SWG\Property()
   * @var MoveRequestDateField[]
   */
  public $field_date;

  /**
   * Approve flag.
   * @SWG\Property(
   *   description="
   *     1| Pending
   *     2| Not Confirmed
   *     3| Confirmed
   *     4| Inhome Estimate
   *     5| Cancelled
   *     6| Day N/A
   *     7| Date Pending
   *     8| Complete
   *     9| Flat Rate Step 1
   *     10| Flat Rate Step 2
   *     11| Flat Rate Step 3
   *     12| Expired
   *     13| We are not available
   *     14| Spam
   *     15| Not submitted
   *     16| Pre-pending
   *     17| Pending-info
   *     18| Not Interested
   *     19| From Parser",
   *   enum={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19})
   * @var integer[]
   */
  public $field_approve;

  /**
   * Size of Move field.
   * @SWG\Property(description="
   *     1| Room
   *     2| Studio
   *     3| Small 1 Bedroom
   *     4| Large 1 Bedroom
   *     5| Small 2 Bedroom
   *     6| Large 2 Bedroom
   *     7| 3 Bedroom
   *     8| 2 bedroom house/townhouse
   *     9| 3 bedroom house/townhouse
   *     10| 4 bedroom house/townhouse",
   *   enum={1,2,3,4,5,6,7,8,9,10})
   * @var string
   */
  public $field_size_of_move;

  /**
   * The entrance from field.
   * @SWG\Property(description="
   *     1| No Stairs - Ground Floor
   *     2| Stairs - 2nd Floor
   *     3| Stairs - 3nd Floor
   *     4| Stairs - 4nd Floor
   *     5| Stairs - 5nd Floor
   *     6| Elevator
   *     7| Private House",
   *   enum={1,2,3,4,5,6,7})
   * @var string
   */
  public $field_type_of_entrance_from;

  /**
   * The entrance to field.
   * @SWG\Property(description="
   *     1| No Stairs - Ground Floor
   *     2| Stairs - 2nd Floor
   *     3| Stairs - 3nd Floor
   *     4| Stairs - 4nd Floor
   *     5| Stairs - 5nd Floor
   *     6| Elevator
   *     7| Private House",
   *   enum={1,2,3,4,5,6,7})
   * @var string
   */
  public $field_type_of_entrance_to_;

  /**
   * The start time field.
   * @SWG\Property(description="
   *     1| Any Time
   *     2| Morning 8AM - 10AM
   *     3| Noon 11AM - 2PM
   *     4| Afternoon 1PM - 4PM
   *     5| Afternoon 3PM - 7PM",
   *   enum={1,2,3,4,5})
   * @var string
   */
  public $field_start_time;

  /**
   * The move_service_type field.
   * @SWG\Property(
   *   example=1,
   *   description="
   *     1| Moving
   *     2| Moving & Storage
   *     3| Loading Help
   *     4| Unloading Help
   *     5| Flat Rate
   *     6| Overnight
   *     7| Long Distance",
   *   enum={1,2,3,4,5,6,7})
   * @var string
   */
  public $field_move_service_type;

  /**
   * The extra furnished rooms.
   * @SWG\Property(
   *   example={1,2},
   *   description="
   *   1| Moving
   *   2| Moving & Storage
   *   3| Loading Help
   *   4| Unloading Help
   *   5| Flat Rate
   *   6| Overnight
   *   7| Long Distance")
   * )
   * @var integer[]
   */
  public $field_extra_furnished_rooms;

  /**
   * The pool field.
   * @SWG\Property(
   *   example={1,3},
   *   description="
   *   1| Yelp
   *   2| Angie's List
   *   3| Google search
   *   4| Word of mouth")
   * )
   * @var integer[]
   */
  public $field_poll;

  /**
   * The delivery_date_from field.
   * @SWG\Property(example="2016-10-12")
   * @var MoveRequestDateField[]
   */
  public $field_delivery_date_from;

  /**
   * The delivery_date_to field.
   * @SWG\Property(example="2016-10-12")
   * @var MoveRequestDateField[]
   */
  public $field_delivery_date_to;

  /**
   * The delivery_date field.
   * @SWG\Property(example="2016-10-12")
   * @var MoveRequestDateField[]
   */
  public $field_delivery_date;

  /**
   * The price_per_hour field.
   * @SWG\Property(example="100")
   * @var integer
   */
  public $field_price_per_hour;

  /**
   * The movers_count field.
   * @SWG\Property(example="2")
   * @var integer
   */
  public $field_movers_count;

  /**
   * The request_completeness.
   * @SWG\Property(example="Some comments")
   * @var string
   */
  public $field_request_completeness;

  /**
   * The delivery_crew_size field.
   * @SWG\Property(example="2")
   * @var integer
   */
  public $field_delivery_crew_size;

  /**
   * The has_alert flag.
   * @SWG\Property(example=0)
   * @var boolean
   */
  public $field_has_alert;

  /**
   * The new_request flag.
   * @SWG\Property(example=0)
   * @var boolean
   */
  public $field_new_request;
  public $field_crew;
  public $field_from_storage_move;
  public $field_list_truck;

  /**
   * The request title.
   * @SWG\Property(
   *    type="number",
   *    format="double"
   * )
   */
  public $field_distance;

}

/**
 * @SWG\Definition()
 */
class Account {

  /**
   * The name of user.
   * @SWG\Property()
   * @var string
   */
  public $name;

  /**
   * The email of user.
   * @SWG\Property()
   * @var string
   */
  public $mail;

  /**
   * @SWG\Property()
   * @var AccountFields
   */
  public $fields;

}

/**
 * @SWG\Definition()
 */
class MoveRequestModelAdditional {}

/**
 * @SWG\Definition(required={"date"})
 */
class MoveRequestDateField {

  /**
   * The full-date field.
   * @SWG\Property(
   *   type="string",
   *   format="date"
   * )
   * @var string
   */
  public $date;

  /**
   * The time field. Example: 12:59:59
   * @SWG\Property(example="12:59:59")
   * @var string
   */
  public $time;

}

/**
 * @SWG\Definition()
 */
class MoveRequestPostalField {

  /**
   * The short country name field. Example: US.
   * @SWG\Property(example="US")
   * @var string
   */
  public $country;

  /**
   * The short administrative area name field. Example: MA.
   * @SWG\Property(example="MA")
   * @var string
   */
  public $administrative_area;

  /**
   * The locality field. Example: Allston.
   * @SWG\Property(example="Allston")
   * @var string
   */
  public $locality;

  /**
   * The postal code field. Example: 01854.
   * @SWG\Property(example="01854")
   * @var string
   */
  public $postal_code;

  /**
   * The thoroughfare field. Example: Linkoln.
   * @SWG\Property(example="Linkoln")
   * @var string
   */
  public $thoroughfare;

  /**
   * The premise field. Example: 45.
   * @SWG\Property(example="45")
   * @var string
   */
  public $premise;

}

/**
 * @SWG\Definition()
 */
class AccountFields {

  /**
   * The user first name.
   * @SWG\Property()
   * @var string
   */
  public $field_user_first_name;

  /**
   * The user last name.
   * @SWG\Property()
   * @var string
   */
  public $field_user_last_name;

  /**
   * The user additional phone.
   * @SWG\Property()
   * @var string
   */
  public $field_user_additional_phone;

  /**
   * The primary phone field.
   * @SWG\Property()
   * @var string
   */
  public $field_primary_phone;

}
