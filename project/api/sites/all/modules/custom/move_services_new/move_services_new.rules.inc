<?php

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_branch\Services\Trucks;
use Drupal\move_services_new\Services\Sales;

/**
 * Implements hook_rules_action_info().
 */
function move_services_new_rules_action_info() {
  $actions = array(
    'move_services_new_confirmed_date' => array(
      'label' => t('Register data where request is confirmed status.'),
      'group' => t('Custom'),
      'parameter' => array(
        'obj' => array(
          'type' => array('node'),
          'label' => t('Request'),
          'optional' => TRUE,
        ),
      ),
    ),
    'move_services_new_updated_user' => array(
      'label' => t('Add user to Sales Person array'),
      'group' => t('Custom'),
    ),
    'move_services_new_blocked_user' => array(
      'label' => t('Update payroll cache for blocked user.'),
      'parameter' => array(
        'obj' => array(
          'type' => array('integer'),
          'label' => t('User id.'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('Custom'),
    ),
    'move_services_new_custom_send_mail' => array(
      'label' => t('Movecalc send mail'),
      'group' => t('Custom'),
      'parameter' => array(
        'param' => array(
          'type' => 'text',
          'label' => t('First parameter for movecalc_send_mail()'),
          'restriction' => 'input',
          'optional' => FALSE,
        ),
        'node' => array(
          'type' => array('node'),
          'label' => t('Request'),
          'optional' => FALSE,
        ),
      ),
    ),
    'move_services_new_delete_sales_user' => array(
      'label' => t('Delete sales user from request when drupal user deleted'),
      'group' => t('Custom'),
      'arguments' => array(
        'sales' => array('type' => 'user', 'label' => t('Sales user')),
      ),
    ),
  );

  return $actions;
}

/**
 * Implements hook_rules_event_info().
 */
function move_services_new_rules_event_info() {
  return array(
    'move_services_save_new_account' => array(
      'label' => t('After save a new user account'),
      'group' => t('Custom'),
      'variables' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('registered user'),
        ),
      ),
    ),
    'move_services_user_update' => array(
      'label' => t('After updating an existing user account'),
      'group' => t('Custom'),
      'variables' => array(
        'account' => array('type' => 'user', 'label' => t('updated user')),
        'account_unchanged' => array(
          'type' => 'user',
          'label' => t('unchanged user'),
          'handler' => 'rules_events_entity_unchanged',
        ),
      ),
    ),
  );
}

/**
 * Callback function.
 *
 * @param object $node
 *   Loaded node.
 */
function move_services_new_confirmed_date($node) {
  $node_wrapper = entity_metadata_wrapper('node', $node->nid);
  $author = $node_wrapper->author->value();
  $GLOBALS['user'] = user_load(1);
  drupal_session_regenerate();
  // Change another request for same day.
  if (variable_get('is_common_branch_truck', FALSE)) {
    (new Trucks())->handlerConflictRequest($node_wrapper);
  }
  else {
    _movecalc_get_conflict_requests($node->nid, $node_wrapper);
  }
  $GLOBALS['user'] = $author;
  drupal_session_regenerate();
}

/**
 * Callback function.
 */
function move_services_new_updated_user() {
  Sales::setListSalesPersons(TRUE);
}

// @TODO Remove unused rules and actions! Moved this functional to move_services_new_entity_presave
function move_services_new_custom_send_mail($param, $node) {}

/**
 * User of company is blocked.
 *
 * @param int $uid
 */
function move_services_new_blocked_user(int $uid) {
  (new DelayLaunch())->createTask(TaskType::UPDATE_PAYROLL_CACHE_BY_BLOCK_USER, array('uid' => $uid));
}

/**
 * Rools action when for deleting user. Removed sales/manager from request.
 * @param object $sales_user - object of user.
 */
function move_services_new_delete_sales_user($sales_user) {
  $move_request_inst = new MoveRequest();
  $move_request_inst->removeSalesUserFromRequest($sales_user);
}
