<?php

namespace Drupal\move_services_new\Traits;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Trait SymfonySerializer.
 */
trait SymfonySerializer {

  /**
   * JsonEncoder.
   *
   * @var object
   */
  protected $encoders;

  /**
   * ObjectNormalizer.
   *
   * @var object
   */
  protected $normalizers;

  /**
   * Serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * Init serializers, encoders, normalizers.
   */
  protected function initSerializer() {
    $this->encoders = array(new JsonEncoder());
    $this->normalizers = array(new ObjectNormalizer());
    $this->serializer = new Serializer($this->normalizers, $this->encoders);
  }

}
