<?php

namespace Drupal\move_services_new\Services\move_request;

use Drupal\move_branch\Services\Trucks;
use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_calculator\Services\CheckRecalculateGrandTotal;
use Drupal\move_calculator\Services\MoveRequestExtraService;
use Drupal\move_inventory\Actions\RoomActions;
use Drupal\move_inventory\Models\RequestInventoryModel;
use Drupal\move_calculator\Services\MoveRequestGrandTotal;
use Drupal\move_calculator\Services\MoveRequestRate;
use Drupal\move_invoice\Services\Invoice;
use Drupal\move_long_distance\Services\LongDistanceReuqest;
use Drupal\move_services_new\Exceptions\ValidationException;
use Drupal\move_services_new\Services\Cache;
use Drupal\move_services_new\Services\ExtraServices;
use Drupal\move_services_new\Services\FieldStrategy\FieldBoolStrategy;
use Drupal\move_services_new\Services\FieldStrategy\FieldDateStrategy;
use Drupal\move_services_new\Services\FieldStrategy\FieldEmailStrategy;
use Drupal\move_services_new\Services\FieldStrategy\FieldEntityRefStrategy;
use Drupal\move_services_new\Services\FieldStrategy\FieldImageFileStrategy;
use Drupal\move_services_new\Services\FieldStrategy\FieldOptionsStrategy;
use Drupal\move_services_new\Services\FieldStrategy\FieldTaxonomyRefStrategy;
use Drupal\move_services_new\Services\FieldStrategy\FieldTextStrategy;
use Drupal\move_services_new\Services\FieldStrategy\FieldAddressStrategy;
use Drupal\move_services_new\Services\Parklot;
use Drupal\move_services_new\Services\Receipt;
use Drupal\move_inventory\Actions\ItemActions;
use Drupal\move_services_new\Services\Sales;
use Drupal\move_services_new\Util\enum\InvoiceFlags;
use Drupal\move_services_new\Util\enum\RequestSearchTypes;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Util\enum\ContractType;
use Drupal\move_services_new\Util\enum\SmsActionTypes;
use EntityFieldQuery;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\Users;
use Drupal\move_services_new\Services\Comments;
use Drupal\move_services_new\Services\Settings;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Services\Inventory;
use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_template_builder\Services\TemplateBuilder;
use Drupal\move_template_builder\Services\EmailTemplate;
use Drupal\move_services_new\System\Extra;
use Drupal\move_notification\Services\Notification;
use Drupal\move_services_new\Util\enum\NotificationTypes;
use Drupal\move_reviews\Services\Reviews;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;
use Drupal\move_new_log\Services\Log;
use stdClass;
use Drupal\move_arrivy\Services\ArrivyRequest;
use Drupal\move_calculator\Services\CalculateReservationPrice;
use Drupal\move_calculator\Services\CalculateWorkTime;
use Drupal\move_services_new\Services\UserPermissions;
use Drupal\move_services_new\Services\FieldStrategy\FieldArrayContext;

/**
 * Class MoveRequest.
 *
 * @package Drupal\move_services_new\Services\move_request
 */
class MoveRequest extends BaseService {

  protected $data = [];
  public $nid = NULL;
  protected $user = NULL;
  public static $files = [];
  protected $isManager = FALSE;
  public $searchApiIndex = NULL;
  public $timeZone = 'UTC';
  protected $clients = NULL;
  public $draft = FALSE;
  public static $needRecalculateGrandTotal = FALSE;

  /**
   * Constructor of MoveRequest service.
   *
   * @param int $nid
   *   Node id.
   *
   * @throws \Exception
   */
  public function __construct($nid = NULL) {
    global $user;
    $this->user = $user;
    $this->clients = new Clients($user->uid);
    $this->isManager = $this->clients->checkUserRoles(['manager', 'sales']);
    $this->timeZone = Extra::getTimezone();
    if ($nid) {
      $this->nid = (int) $nid;
      // Puts data to cache table.
      $this->getNode($this->nid);
    }
  }

  /**
   * Create a new move request.
   *
   * @param mixed $request
   *   Data of requests.
   * @param int $type
   *   Type of request: 0 - Default, 1 - Programmatically.
   *
   * @return array|mixed
   *   Node id.
   *
   * @throws \Exception
   */
  public function create($request = [], int $type = 0) {
    $result = [];

    try {
      $node = new \stdClass();
      $node->type = 'move_request';
      $node->status = 1;
      $node->language = LANGUAGE_NONE;

      node_object_prepare($node);
      $old_request = $request;
      // Duplication because have auto-login with global $user.
      $user_who_creating = $this->user;

      if (isset($request['all_data'])) {
        $request_all_data = $request['all_data'];
        unset($request['all_data']);
      }

      if (isset($request['data'])) {
        $request = $request['data'];
        if (isset($old_request['account'])) {
          $request['account'] = $old_request['account'];
        }
      }
      if (!empty($request['field_request_settings'])) {
        $request['field_request_settings'] = serialize($request['field_request_settings']);
      }

      if (!empty($request['field_custom_commercial_item'])) {
        $request['field_custom_commercial_item'] = serialize($request['field_custom_commercial_item']);
      }

      $exist_user_flag = FALSE;
      if (isset($old_request['account'])) {
        $clients = new Clients();
        $exist_user_flag = $clients->checkExistUser($old_request['account']);
        // If user is exist then new_user_flag = FALSE and otherwise TRUE.
        $request['field_new_user_flag'] = !$exist_user_flag;
      }

      // Have to be careful to check if it's account array in $request.
      $this->user($request);

      if (((isset($this->user->token) && !$this->checkSavedToken($this->user->token)) || !$exist_user_flag) && !$type) {
        $this->clearRequest($request);
        $result['uid'] = $this->user->uid;
        if (user_is_anonymous()) {
          $result['token'] = $this->autoLogin($this->user->uid);
        }
        // Only if new just created user.
        if (isset($this->user->autologin)) {
          $result['autologin'] = $this->user->autologin;
        }
      }
      else {
        if (isset($request['account'])) {
          unset($request['account']);
        }
      }
      // If client already have requests - get manager from previous request.
      $previous_client_request_manger = FALSE;
      if ($exist_user_flag) {
        $previous_client_request_manger = $this->getSalesForExistingClient($this->user);
      }

      $node->uid = $this->user->uid;
      $request['uid'] = $this->user->uid;
      $request['field_user_created'] = $user_who_creating->uid;
      // Check inventory exist.
      $data_inventory = NULL;
      if (isset($request['inventory'])) {
        $data_inventory = $request['inventory'];
        unset($request['inventory']);
      }

      if (isset($request['field_move_service_type']) && $request['field_move_service_type'] == 5) {
        $request['field_usecalculator'] = FALSE;
      }

      // Title field is required.
      $this->checkRequiredTitle($node, $request);
      if (is_array($validation_errors = $this->formValidate($node, $request))) {
        if ($type) {
          return t('Form validation error. Try again.');
        }
        else {
          throw new ValidationException($validation_errors);
        }
      }
      else {
        $request = $this->setAllowPendingInfo($request);
        $nid = $this->createNode($request);
        $result['nid'] = $nid;
        $service_type = $request['field_move_service_type'];
        (new Sales($nid))->setSalesPersonAppointed($user_who_creating, $previous_client_request_manger, $service_type);
      }


      if (isset($data_inventory)) {
        $this->setInventory($nid, $data_inventory);
      }

      if (isset($request_all_data)) {
        MoveRequest::setRequestAllData($result['nid'], $request_all_data);
        Cache::updateCacheData($nid);
        MoveRequest::setRequestAllDataIndex($result['nid'], $request_all_data);
      }


      // Notification.
      $node->nid = $nid;
      $notification_info = ['node' => $node, 'user' => $this->user];
      Notification::createNotification(NotificationTypes::NEW_REQUEST, $notification_info, $nid, EntityTypes::MOVEREQUEST);
    }
    catch (\Throwable $e) {
      watchdog(WATCHDOG_CRITICAL, "Request not created!");
    }
    finally {
      return $result;
    }
  }

  /**
   * Create move request.
   *
   * @param array $request
   *   Data for create move request.
   * @param int $type
   *   Type of request: 0 - Default, 1 - Programmatically.
   *
   * @return array|mixed
   *   Node id.
   *
   * @throws \Exception
   */
  public function create2(array $request = [], int $type = 0) {
    $update_cache = FALSE;
    $result = [];

    $request_account = isset($request['account']) ? $request['account'] : [];
    $request_data = isset($request['data']) ? $request['data'] : $request;
    $request_all_data = isset($request['all_data']) ? $request['all_data'] : [];

    $user = $this->createRequestUser($request_data, $request_account);

    unset($request_data['uid']);
    unset($request_data['all_data']);
    unset($request_data['account']);

    $values = $this->prepareEntityData($user->uid);
    $entity = entity_create('node', $values);
    $node_wrapper = entity_metadata_wrapper('node', $entity);

    $request_data['field_new_user_flag'] = $user->new;

    if (!empty($request_data['field_request_settings'])) {
      $request_data['field_request_settings'] = serialize($request_data['field_request_settings']);
    }

    if (!empty($request_data['field_custom_commercial_item'])) {
      $request_data['field_custom_commercial_item'] = serialize($request_data['field_custom_commercial_item']);
    }

    if (((isset($user->token) && !$this->checkSavedToken($user->token)) || $user->new) && !$type) {
      $result['uid'] = $user->uid;
      if (user_is_anonymous()) {
        $result['token'] = $this->autoLogin($user->uid);
      }
      // Only if new just created user.
      if (isset($user->autologin)) {
        $result['autologin'] = $user->autologin;
      }
    }

    $request_data['field_user_created'] = $this->user->uid;

    if (isset($request_data['field_move_service_type']) && $request_data['field_move_service_type'] == 5) {
      $request_data['field_usecalculator'] = FALSE;
    }

    // If client already have requests - get manager from previous request.
    $previous_sales = FALSE;
    if (!$user->new) {
      $previous_sales_uid = $this->getSalesForExistingClient($user);
      $previous_sales = !empty($previous_sales_uid) ? user_load($previous_sales_uid) : FALSE;
    }
    elseif ($this->clients->checkUserRoles(['sales', 'manager', 'administrator'])) {
      $previous_sales = $this->user;
    }

    $request_data['field_allow_to_pending_info'] = variable_get('allowToPendingInfo', 1);

    // Check inventory exist.
    $inventory = [];
    if (isset($request_data['inventory'])) {
      $inventory = $request_data['inventory'];
      unset($request_data['inventory']);
    }

    $date = "{$request_data['field_date']['date']} {$request_data['field_date']['time']}";

    foreach ($request_data as $key => $field) {
      if ($key == 'field_date') {
        $entity->{$key}[LANGUAGE_NONE][0] = [
          'value' => format_date(strtotime($date), 'custom', 'Y-m-d H:i:s', 'UTC'),
          'timezone' => 'UTC',
          'timezone_db' => 'UTC',
        ];
      }
      else {
        $node_wrapper->{$key}->set($field);
      }
    }

    $node_wrapper->save();

    $result['nid'] = $node_wrapper->getIdentifier();
    if ($inventory) {
      $this->setInventory($result['nid'], $inventory);
    }

    $service_type = $request_data['field_move_service_type'];
    (new Sales($result['nid']))->setSalesPersonAppointed($previous_sales, $service_type);

    if (isset($request_all_data)) {
      MoveRequest::setRequestAllData($result['nid'], $request_all_data);
      MoveRequest::setRequestAllDataIndex($result['nid'], $request_all_data);
      $update_cache = TRUE;
    }

    if ($result['nid']) {
      (new ExtraServices($this->user, $node_wrapper, $request_all_data))->setExtraServicesOnRequestCreate($this);
      $update_cache = TRUE;
      // Notification.
      $entity->nid = $result['nid'];
      $notification_info = ['node' => $entity, 'user' => $this->user];
      Notification::createNotification(NotificationTypes::NEW_REQUEST, $notification_info, $result['nid'], EntityTypes::MOVEREQUEST);
    }

    if ($update_cache) {
      Cache::updateCacheData($result['nid'], [], 'move_services_cache', $node_wrapper, TRUE);
    }

    return $result;
  }

  /**
   * Prepare data and create a new entity object.
   *
   * @return array
   *   Prepared data.
   */
  private function prepareEntityData($uid) {
    return [
      'type' => 'move_request',
      'uid' => $uid,
      'status' => 1,
      'comment' => 1,
      'promote' => 0,
      'language' => LANGUAGE_NONE,
      'title' => 'Move Request',
    ];
  }

  /**
   * Create user or load when create request.
   *
   * @param array $request_data
   *   Move request data.
   * @param array $request_account
   *   User account data.
   *
   * @return mixed|null
   *   User data.
   *
   * @throws \Exception
   */
  private function createRequestUser(array $request_data, array $request_account = []) {
    if (((new Sales())->checkAdminRoles() || user_is_anonymous()) && $request_account && empty($request_data['field_from_storage_move'])) {
      // @TODO. Refactoring global with front end.
      $request_account['account'] = $request_account;
      if ($uid = $this->clients->checkExistUser(['mail' => $request_account['mail']])) {
        $user = user_load($uid);
        $user->new = 0;
      }
      else {
        $user = $this->clients->create($request_account);
        $user->autologin = $this->clients->oneTimeHash($user->mail);
        $user->new = 1;
      }
    }
    elseif (!empty($request_data['uid'])) {
      $user = user_load($request_data['uid']);
      $user->new = 0;
    }
    else {
      $user = $this->user;
      $user->new = 0;
    }

    return $user;
  }

  /**
   * Set inventory.
   *
   * @param int $nid
   *   Node id.
   * @param array $inventory
   *   Inventory.
   */
  private function setInventory(int $nid, array $inventory) {
    $inventory_instance = new Inventory($nid);
    $inventory_instance->create($inventory);
  }

  /**
   * Get request weight.
   *
   * @param array $request
   *   Request data.
   *
   * @return int
   *   Weight.
   */
  public function getRequestWeight(array $request = []) {
    $distanceCalculate = new DistanceCalculate();
    if ($request['service_type']['raw'] == RequestServiceType::LONG_DISTANCE) {
      $weight = $distanceCalculate->getLongDistanceWeight($request);
    }
    else {
      $weight = $distanceCalculate->getWeightByWeightType($request);
      if (empty($weight)) {
        $move_size = 0;
        if (isset($request['move_size']['raw']) && $request['move_size']['raw'] !== '') {
          $req_move_size = $request['move_size']['raw'];
          $move_size = (int) $distanceCalculate->getCalculateSettings()['size'][$req_move_size];
        }
        $total_room_weight = 0;
        if (!empty($request['rooms']['raw'])) {
          foreach ($request['rooms']['raw'] as $room) {
            $total_room_weight += (int) $distanceCalculate->getCalculateSettings()['room_size'][$room];
          }
        }

        $weight = $total_room_weight + $move_size + (int) $distanceCalculate->getCalculateSettings()['room_kitchen'];
      }
    }

    return $weight;
  }

  /**
   * Helper method for check what user has saved session.
   */
  private function checkSavedToken($token) {
    $auth = db_select('move_acrossdomain_auth', 'm');
    $auth->join('sessions', 's', 'm.sid = s.sid');
    $auth->condition('m.token', $token);
    $auth->addExpression('COUNT(*)');
    return $auth->execute()->fetchField();
  }

  /**
   * Create storage request.
   *
   * @param mixed $request
   *   Id of storage1 node.
   *
   * @return array|bool
   *   Nid`s or false.
   *
   * @throws \Exception
   */
  public function storageRequestCreate($request = array()) {
    if (isset($request['storage1']) && isset($request['storage2'])) {
      $is_overnight = isset($request['storage1']['data'])
        ? $request['storage1']['data']['field_move_service_type'] == 6
        : $request['storage1']['field_move_service_type'] == 6;

      if (!isset($request['storage1']['all_data'])) {
        $request['storage1']['all_data'] = array();
      }

      if (!isset($request['storage2']['all_data'])) {
        $request['storage2']['all_data'] = array();
      }

      // Create storage 1.
      // To Storage.
      $st1 = $this->create2($request['storage1'], 0);

      $request['storage2']['data']['field_to_storage_move'] = $st1['nid'];
      $request['storage2']['data']['uid'] = $st1['uid'];

      // Create storage 2.
      // From Storage.
      $st2 = $this->create2($request['storage2'], 1);

      if ($is_overnight) {
        $this->saveOvernightExtraServices($st1['nid']);
        // Add overnight storage only for first overnight request.
        // $this->saveOvernightExtraServices($st2['nid']);.
      }

      // Update storage 2.
      $storage2 = array(
        'nid' => $st1['nid'],
        'field_from_storage_move' => $st2['nid'],
      );
      (new MoveRequest($st1['nid']))->update2($storage2);

      return array($st1, $st2);
    }

    return FALSE;
  }

  /**
   * Save overnight extra services.
   *
   * @param int $nid
   *   Node id.
   *
   * @throws \Exception
   */
  public function saveOvernightExtraServices(int $nid) {
    $basicSettings = json_decode(variable_get('basicsettings', array()));

    if ($basicSettings->overnight->rate) {
      $part_one = new \stdClass();
      $part_one->services_default_value = $basicSettings->overnight->rate;
      $part_one->services_name = "Cost";
      $part_one->services_read_only = FALSE;
      $part_one->services_type = "Amount";

      $part_two = new \stdClass();
      $part_two->services_default_value = 1;
      $part_two->services_name = "Value";
      $part_two->services_read_only = FALSE;
      $part_two->services_type = "Number";

      $overnight_extra = new \stdClass();
      $overnight_extra->name = "Overnight Storage";
      $overnight_extra->index = 1;
      $overnight_extra->edit = TRUE;
      $overnight_extra->extra_services = array($part_one, $part_two);

      $extraServices = ExtraServices::getExtraServices($nid);
      $extraServices[] = $overnight_extra;
      ExtraServices::setExtraServices($nid, $extraServices, FALSE, FALSE);
    }
  }

  /**
   * Create a user before create new node.
   *
   * @param array $request
   *   Request data.
   *
   * @throws \Exception
   */
  protected function user(array &$request = array()) {
    // Check if user exist.
    if (!$this->user->uid || (new Sales())->checkAdminRoles()) {
      if (isset($request['uid']) && $request['uid']) {
        $this->user = user_load($request['uid']);
      }
      elseif (!$request['field_new_user_flag']) {
        $this->user = user_load_by_mail($request['field_e_mail']);
      }
      elseif (isset($request['account'])) {
        $this->user = $this->clients->create($request);
        $this->user->autologin = $this->clients->oneTimeHash($this->user->mail);
        unset($request['account']);
      }
    }
  }

  /**
   * Retrieve a move_request.
   *
   * @return array|mixed
   *   Request data.
   */
  public function retrieve() {
    return $this->data;
  }

  /**
   * Auto Login for user's.
   *
   * @param int $uid
   *   User id.
   *
   * @return string
   *   Token of authorized user.
   */
  private function autoLogin($uid) {
    global $user;
    $user = user_load($uid);
    $edit = array();
    $_COOKIE = array();
    drupal_save_session(TRUE);
    drupal_session_started(FALSE);
    drupal_session_regenerate();
    drupal_session_commit();
    user_module_invoke('login', $edit, $user);
    $token = drupal_get_token('services');
    return $token;
  }

  /**
   * Get list of move_requests.
   *
   * @param int $page
   *   Number of current page.
   * @param int $page_size
   *   Count elements on page.
   *
   * @return array
   *   List of nodes with loaded fields
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public function index(int $page, int $page_size) {
    $table_alias = 'n';
    $node_select = db_select('node', $table_alias)
      ->orderBy('nid', 'DESC');
    $this->buildIndexQuery($node_select, $page, $page_size, 'nid', 'node', $table_alias, array('type' => 'move_request'));
    $db_nids = $this->buildIndexList($this->executeIndexQuery($node_select));
    $db_nids = $this->getIdNodes($db_nids);
    $result = array();
    foreach ($db_nids as $nid) {
      array_push($result, $this->getNode($nid));
    }

    return $result;
  }

  /**
   * Update move request.
   *
   * @param array $request
   *   Data for update.
   *
   * @return bool
   *   Result update.
   *
   * @throws \Exception
   */
  public function update2(array $request = []) {
    global $user;

    try {
      $result = [];
      $needUpdatePayroll = FALSE;
      self::setBlocker($this->nid);
      set_time_limit(120);
      $request_data = [];

      // Lock updating request if another process already is updated.
      $nid = $this->nid;

      // Check if current user has permission to update request.
      $user_permissions = new UserPermissions($this->user, $request, $this->data);
      $is_user_can_update_request = $user_permissions->isUserCanUpdateRequest();

      if (!$is_user_can_update_request) {
        // Manager has't permissions for edit request.
        throw new \Exception("Permission denied.");
      }

      if (isset($request['nid'])) {
        $nid = $request['nid'];
        unset($request['nid']);
      }

      // Quick fix.
      if (isset($request['dontShowDelirery'])) {
        unset($request['dontShowDelirery']);
      }
      // We do not allow clients to save this empty field values.
      if (array_key_exists('field_movers_count', $request) && empty($request['field_movers_count'])) {
        $request['field_movers_count'] = 0;
      }
      if (array_key_exists('field_price_per_hour', $request) && empty($request['field_price_per_hour'])) {
        $request['field_price_per_hour'] = 0;
      }

      if (!empty($request['field_request_settings'])) {
        $request['field_request_settings'] = serialize($request['field_request_settings']);
      }

      if (!empty($request['field_custom_commercial_item'])) {
        $request['field_custom_commercial_item'] = serialize($request['field_custom_commercial_item']);
      }

      if (isset($request['global_user_uid'])) {
        unset($request['global_user_uid']);
      }

      if (isset($request['request_data'])) {
        $request_data = $request['request_data'];
        unset($request['request_data']);
      }
      if (isset($request['all_data'])) {
        $request_all_data = $request['all_data'];
        static::$needRecalculateGrandTotal = CheckRecalculateGrandTotal::checkAllData($request_all_data, $this->data);
        unset($request['all_data']);
      }

      // We recalculate packing because we can't trust frontend.
      if (isset($request_all_data['invoice']['request_data']['value']['packings'])) {
        $this->recalculateSummPacking($request_all_data['invoice']['request_data']['value']['packings'], $nid);
      }

      if (!empty($request_all_data['sit']['type'])) {
        $request['field_sit_type'] = $request_all_data['sit']['type'];
      }

      if (isset($request['uid'])) {
        unset($request['nid']);
        unset($request['uid']);
      }

      // If work completed save date time.
      if (!empty($request['field_company_flags'])) {
        if (in_array(3, $request['field_company_flags'])) {
          $request['field_completed_date'] = time();
          /* If quickbooks connection is present and post settings are set
          to closed then post on request close */
          (new DelayLaunch())->createTask(TaskType::SEND_RECEIPTS_TO_QUICKBOOKS, ['nid' => $nid]);

          $review_settings = json_decode(variable_get("reviews_settings"));
          if ($review_settings->reviews_autosend == 1 && $review_settings->selectedDay->value === 6) {
            $template = Reviews::getReviewTemplate('review_reminder_send');
            $email_was_send = EmailTemplate::sendEmailTemplate($nid, $template);
            if ($email_was_send['send']) {
              $request['field_review_send'] = 1;
            }
          }
          (new DelayLaunch())->createTask(TaskType::SMS_SEND, [
            'actionId' => SmsActionTypes::REVIEW,
            'nid' => $nid,
            'uid' => NULL,
            'user' => $user->uid,
          ]);
        }

        $needUpdatePayroll = TRUE;
      }

      if ($request) {
        static::$needRecalculateGrandTotal = CheckRecalculateGrandTotal::checkRequest($nid, $request) || static::$needRecalculateGrandTotal;
        $nodeSave = $this->saveNode($nid, $request);
        $result['saveNode'] = (bool) $nodeSave;
      }

      if (isset($request_all_data)) {
        MoveRequest::setRequestAllData($nid, $request_all_data);
        MoveRequest::setRequestAllDataIndex($nid, $request_all_data);
        LongDistanceReuqest::updateLdRequestWeight($nid, $request_all_data['invoice']['closing_weight']['value'] ?? NULL);
        $result['saveRequestAllData'] = TRUE;
      }

      if ($request_data) {
        MoveRequest::setRequestData($nid, $request_data);
        $result['saveRequestData'] = TRUE;
      }

      if (!empty($result['saveNode'])) {
        module_invoke_all('move_request_fields_update', new self($nid));
      }

      // Assign foreman to request in arrivy.
      if (isset($request['field_foreman'])) {
        if (isset($request['field_foreman']['del'])) {
          (new DelayLaunch())->createTask(TaskType::ARRIVY_UNASSIGN_FOREMAN, ['nid' => $nid]);
        }
        else {
          (new DelayLaunch())->createTask(TaskType::ARRIVY_ASSIGN_FOREMAN, ['nid' => $nid, 'foreman' => $request['field_foreman']]);
          (new DelayLaunch())->createTask(TaskType::SMS_SEND, [
            'actionId' => SmsActionTypes::FOREMAN_ASSIGN,
            'nid' => $nid,
            'uid' => $request['field_foreman'][0],
            'user' => $user->uid,
          ]);
          (new DelayLaunch())->createTask(TaskType::SMS_SEND, ['actionId' => SmsActionTypes::FOREMAN_ASSIGN_TO_FOREMAN, 'nid' => $nid, 'uid' => $request['field_foreman'][0]]);
        }
      }

      Cache::updateCacheData($nid);

      if ($needUpdatePayroll) {
        // Update payroll cache when field_company_flags is changed.
        $this->updatePayrollCache($nid);
      }

      return (!empty($result['saveExtraServices']) || !empty($result['saveRequestAllData']) || !empty($result['saveNode']) || !empty($result['saveRequestData'])) ?? FALSE;
    }
    catch (\Throwable $e) {
      $message = "Error when request updated. {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('request_update', $message, array(), WATCHDOG_ERROR);
      services_error($e->getMessage(), 500);
    }
    finally{
      self::removeBlocker($nid);
    }
  }

  /**
   * Update existing move request.
   *
   * @param mixed $request
   *   Data with fields to update.
   *
   * @return bool|mixed
   *   Status of update operation.
   *
   * @throws \Drupal\move_services_new\Exceptions\ValidationException
   * @throws \ServicesException
   */
  public function update($request = array()) {
    $nid = $this->nid;
    $request_data = array();
    $error_flag = FALSE;
    $something_updated = FALSE;
    try {
      set_time_limit(120);
      if (!$nid && isset($request['nid'])) {
        $nid = $request['nid'];
        unset($request['nid']);
      }

      if (!empty($request['field_request_settings'])) {
        $request['field_request_settings'] = serialize($request['field_request_settings']);
      }

      if (!empty($request['field_custom_commercial_item'])) {
        $request['field_custom_commercial_item'] = serialize($request['field_custom_commercial_item']);
      }

      // If work completed save date time.
      if (!empty($request['field_company_flags'])) {
        if (in_array(3, $request['field_company_flags'])) {
          $request['field_completed_date'] = time();
          (new DelayLaunch())->createTask(TaskType::SEND_RECEIPTS_TO_QUICKBOOKS, array('nid' => $nid));

          $review_settings = json_decode(variable_get("reviews_settings"));
          if ($review_settings->reviews_autosend == 1 && $review_settings->selectedDay->value === 6) {
            $template = Reviews::getReviewTemplate('review_reminder_send');
            EmailTemplate::sendEmailTemplate($nid, $template);
          }
        }

        // Update payroll cache when field_company_flags is changed.
        $this->updatePayrollCache($nid);
      }

      // Assign foreman to request in arrivy.
      if (isset($request['field_foreman'])) {
        if (isset($request['field_foreman']['del'])) {
          (new DelayLaunch())->createTask(TaskType::ARRIVY_UNASSIGN_FOREMAN, ['nid' => $nid]);
        }
        else {
          (new DelayLaunch())->createTask(TaskType::ARRIVY_ASSIGN_FOREMAN, ['nid' => $nid, 'foreman' => $request['field_foreman']]);
        }
      }


      // Lock updating request if another process already is updated.
      self::setBlocker($nid);

      // Check if current user has permission to update request.
      $user_permissions = new UserPermissions($this->user, $request, $this->data);
      $is_user_can_update_request = $user_permissions->isUserCanUpdateRequest();

      if (!$is_user_can_update_request) {
        // Manager has't permissions for edit request.
        throw new \Exception("Permission denied.");
      }

      if (isset($request['global_user_uid'])) {
        unset($request['global_user_uid']);
      }

      if (isset($request['request_data'])) {
        $request_data = $request['request_data'];
        unset($request['request_data']);
      }
      if (isset($request['all_data'])) {
        $request_all_data = $request['all_data'];
        unset($request['all_data']);
      }

      // We recalculate packing because we can't trust frontend.
      if (isset($request_all_data['invoice']['request_data']['value']['packings'])) {
        $this->recalculateSummPacking($request_all_data['invoice']['request_data']['value']['packings'], $nid);
      }

      $node = $this->getRealNode($nid);

      if (!empty($request_all_data['sit']['type'])) {
        $request['field_sit_type'] = $request_all_data['sit']['type'];
      }

      // Todo Refactor this later.
      if ($this->isDoubleRequest($node, $request)) {
        $node_fields = $this->getNode($nid);

        if (!empty($node_fields["storage_id"])) {
          $paired_node_fields = $this->getNode($node_fields["storage_id"]);
          $node_date = strtotime($request["field_date"]["date"]);
          $paired_node_date = $paired_node_fields["date"]["raw"];

          $count_day = abs($node_date - $paired_node_date) / (60 * 60 * 24);
          $paired_request = [];

          if ($count_day == 1) {
            if ($paired_node_fields["service_type"]["raw"] != "6") {
              $paired_request["field_move_service_type"] = "6";
              $request["field_move_service_type"] = "6";
            }
          }
          else {
            if ($paired_node_fields["service_type"]["raw"] != "2") {
              $paired_request["field_move_service_type"] = "2";
              $request["field_move_service_type"] = "2";
            }
          }

          $this->saveNode($paired_node_fields["nid"], $paired_request);
        }
      }

      $node->changed = time();

      if (is_array($validation_errors = $this->formValidate($node, $request))) {
        $error_flag = TRUE;
      }
      else {
        if (isset($request['uid'])) {
          unset($request['nid']);
          unset($request['uid']);
        }

        if ($request_data) {
          MoveRequest::setRequestData($nid, $request_data);
          $something_updated = TRUE;
        }

        if (isset($request_all_data)) {
          MoveRequest::setRequestAllData($nid, $request_all_data);
          Cache::updateCacheData($nid);
          MoveRequest::setRequestAllDataIndex($nid, $request_all_data);
          $something_updated = TRUE;
        }

        if ($request) {
          $something_updated = $this->saveNode($nid, $request);
          Cache::updateCacheData($nid);
        }

        if ($something_updated) {
          module_invoke_all('move_request_fields_update', new self($nid));
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Error when request updated. {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('request_update', $message, array(), WATCHDOG_DEBUG);
      $error_flag = TRUE;
      $error_msg = $e->getMessage();
    }
    finally {
      module_load_include('inc', 'services', 'includes/services.runtime');
      self::removeBlocker($nid);
      if ($error_flag) {
        if (isset($validation_errors) && is_array($validation_errors)) {
          throw new ValidationException($validation_errors);
        }
        services_error($error_msg ?? 'Something went wrong');
      }
      else {
        return $something_updated;
      }
    }
  }

  /**
   * Update payroll cache by nid when field_company_flags is changed.
   *
   * @param int $nid
   *   Move Request id.
   */
  private function updatePayrollCache(int $nid) {
    $payroll_instance = new Payroll();
    if ($payroll_instance->isFlatRate($nid)) {
      $payroll_instance->cacheUpdatePayrollRequest($nid, ContractType::PICKUP_PAYROLL);
      $payroll_instance->cacheUpdatePayrollRequest($nid, ContractType::DELIVERY_PAYROLL);
    }
    else {
      $payroll_instance->cacheUpdatePayrollRequest($nid, ContractType::PAYROLL);
    }
  }

  /**
   * Update only node fields.
   *
   * Update request cache and drupal cache for this fields.
   *
   * @param int $nid
   *   Id of the node.
   * @param array $fields
   *   Array with data to update.
   *   Example.
   *   $data_for_update = array(
   *     'field_list_truck' => array(
   *       'raw' => 'tid',
   *       'value' => 'remove', - if value is "remove" all values be deleted.
   *       'data_to_update_in_cache' => array(
   *       'trucks' => array(
   *         'value' => array(),
   *         'label' => 'Truck',
   *         'raw' => array(),
   *         'old' => array(),
   *         'field' => 'field_list_truck',
   *       ),
   *     ),
   *      ),
   *     'field_approve' => array(
   *       'raw' => 'value',
   *       'value' => '7',
   *       'data_to_update_in_cache' => array(
   *         'status' => array(
   *           'value' => 'Date Pending',
   *           'label' => 'Status',
   *           'raw' => '7',
   *           'old' => '7',
   *           'field' => 'field_approve',
   *         ),
   *       ),
   *       ),
   *       );.
   * @param string $bundle
   *   Bundle in db fields to update.
   * @param bool $use_hook
   *   Use hook or not.
   * @param bool $update_drupal_cache
   *   Update drupal cache or not.
   *
   * @return array
   *   Return array with field name and "1" if was updated.
   *
   * @throws \Exception
   */
  public static function updateNodeFieldsOnly(int $nid, array $fields, string $bundle = 'move_request', $use_hook = TRUE, $update_drupal_cache = TRUE) {
    $updated = array();

    if (!$fields) {
      return array();
    }

    $data_to_update_in_cache = [];

    foreach ($fields as $field_name => $data) {
      $field_name_in_db = 'field_data_' . $field_name;
      $field_revision_in_db = 'field_revision_' . $field_name;
      $raw_in_db = $field_name . '_' . $data['raw'];
      $value_to_update = $data['value'];

      if (!empty($data['data_to_update_in_cache'])) {
        $data_to_update_in_cache += $data['data_to_update_in_cache'];
      }

      if ($value_to_update == 'remove') {
        // Update main field.
        db_delete($field_name_in_db)
          ->condition('entity_id', $nid)
          ->condition('bundle', $bundle)
          ->execute();
        // Update revision field.
        db_delete($field_revision_in_db)
          ->condition('entity_id', $nid)
          ->condition('bundle', $bundle)
          ->execute();
        $updated[$field_name] = 1;
      }
      else {
        if (!static::isFieldValueExist($nid, $field_name_in_db)) {
          // Insert field value.
          static::updateNodeFieldAttachUpdate($nid, $bundle, $field_name, $data['raw'], $value_to_update);
          $updated[$field_name] = 1;
        }
        else {
          // Update main field.
          $updated[$field_name] = db_update($field_name_in_db)
            ->fields(array($raw_in_db => $value_to_update))
            ->condition('entity_id', $nid)
            ->condition('bundle', $bundle)
            ->execute();
          // Update revision field.
          $updated[$field_name] = db_update($field_revision_in_db)
            ->fields(array($raw_in_db => $value_to_update))
            ->condition('entity_id', $nid)
            ->condition('bundle', $bundle)
            ->execute();
        }
      }
    }

    // Update request field cache.
    if (!empty($data_to_update_in_cache)) {
      $move_request = new self($nid);
      $cache_data = $move_request->retrieve();
      $small_cached_data = Cache::getCacheData($nid, 'move_services_cache', 0);
      $cache_data = array_merge($cache_data, $data_to_update_in_cache);
      // Update small cache.
      $key_exist = array_intersect_key($small_cached_data, $data_to_update_in_cache);
      if ($key_exist) {
        $small_cached_data = array_merge($small_cached_data, $data_to_update_in_cache);
      }

      Cache::setCacheData($nid, $cache_data, 'move_services_cache', $small_cached_data);
    }

    if ($use_hook) {
      module_invoke_all('move_request_fields_update', $move_request, $fields);
    }

    if ($update_drupal_cache) {
      // Clean all caches for node fields.
      cache_clear_all('field:node:' . $nid, 'cache_field');
      entity_get_controller('node')->resetCache(array($nid));

      $move_request = $move_request ?? new self($nid);
      $move_request->addRequestToSearchApiIndex($nid);
    }

    return $updated;
  }

  /**
   * Check if exist field value for request.
   *
   * IF entity id exist - that's mean that we have value for field.
   *
   * @param int $entity_id
   *   Id of the request.
   * @param string $table_name
   *   Name of the table field.
   *
   * @return bool
   *   TRUE if exist, FALSE if not.
   */
  public static function isFieldValueExist(int $entity_id, string $table_name) : bool {
    $result = db_select($table_name, 'alias')
      ->fields('alias', array('entity_id'))
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetchField();

    return !empty($result) ? TRUE : FALSE;
  }

  /**
   * Wrapper to insert field value with "field_attach_update".
   *
   * @param int $nid
   *   Request id.
   * @param string $bundle
   *   Bundle name. move_request for example.
   * @param string $field_name
   *   Name of the field to save.
   * @param string $raw
   *   Name of the raw. "value", "tid", etc...
   * @param string $value
   *   Value to save.
   */
  public static function updateNodeFieldAttachUpdate(int $nid, string $bundle, string $field_name, string $raw, string $value) : void {
    $node = new stdClass();
    $node->nid = $nid;
    $node->type = $bundle;
    $node->{$field_name}[LANGUAGE_NONE][0][$raw] = $value;
    field_attach_presave('node', $node);
    field_attach_update('node', $node);
  }

  /**
   * Helper method for block multiple edit request.
   *
   * @param int $nid
   *   The node id.
   */
  public static function setBlocker(int $nid) : void {
    $endless_protect = 0;
    $lock = variable_get("locked_{$nid}", FALSE);
    while ($lock) {
      usleep(2000);
      $lock = FALSE;
      // Don't use variable_get as it cached.
      $query = db_query("SELECT name, value FROM {variable} WHERE name = 'locked_{$nid}'")->fetchAllKeyed();
      if ($query) {
        $lock = unserialize($query["locked_{$nid}"]);
      }
      // Protect from loop endless.
      if ($endless_protect++ >= 500) {
        break;
      }
    }
    variable_set("locked_{$nid}", TRUE);
  }

  /**
   * Remove move request block.
   *
   * @param int $nid
   *   Move request id.
   */
  public static function removeBlocker(int $nid) : void {
    variable_del("locked_{$nid}");
  }

  /**
   * Check service type. @TODO Refactor it!
   *
   * @param mixed $node
   *   Node object.
   * @param mixed $request
   *   Request data.
   *
   * @return bool
   *   Result check.
   */
  private function isDoubleRequest($node, $request) {
    $result = FALSE;
    if ($this->existDateAndServiceType($request)) {
      $nodeServiceType = $node->field_move_service_type["und"]["0"]["value"];
      $result = in_array($nodeServiceType, [2, 6]) || in_array($request["field_move_service_type"], [2, 6]);
    }

    return $result;
  }

  /**
   * Exist date, service type in move request.
   *
   * @param mixed $request
   *   Request data.
   *
   * @return bool
   *   True if exist.
   */
  private function existDateAndServiceType($request) {
    return isset($request["field_date"]) && isset($request["field_move_service_type"]);
  }

  /**
   * Calculate packing sum.
   *
   * @param array $packings
   *   Packing data.
   * @param int $nid
   *   Node id.
   */
  public function recalculateSummPacking(array &$packings, int $nid) {
    $request = entity_metadata_wrapper('node', $nid);
    if ($request->field_move_service_type->value() == 5 || $request->field_move_service_type->value() == 7) {
      $settings = json_decode(variable_get('basicsettings'));
      $checkShowLaborRate = $settings->packing_settings->packingLabor;
      if ($checkShowLaborRate) {
        $properties = array('LDRate', 'quantity');
        foreach ($packings as $packing) {
          $packing_obj = (object) $packing;
          if (Extra::findProperties($packing_obj, $properties)) {
            $labor_rate = property_exists($packing_obj, 'laborRate') ? $packing_obj->laborRate : 0;
            $packing['total'] = ($packing_obj->LDRate + $labor_rate) * $packing_obj->quantity;
          }
        }
      }
      else {
        $properties = array('LDRate', 'quantity');
        foreach ($packings as $packing) {
          $packing_obj = (object) $packing;
          if (Extra::findProperties($packing_obj, $properties)) {
            $packing['total'] = $packing_obj->LDRate * $packing_obj->quantity;
          }
        }
      }
    }
    else {
      $properties = array('rate', 'quantity');
      foreach ($packings as $key => &$packing) {
        $packing_obj = (object) $packing;
        if (Extra::findProperties($packing_obj, $properties)) {
          $packing['total'] = $packing_obj->rate * $packing_obj->quantity;
        }
      }
    }
  }

  /**
   * Delete node and their cache.
   */
  public function delete() : void {
    node_delete($this->nid);
    Cache::removeCacheData($this->nid);
  }

  /**
   * Checking exists of node.
   *
   * @param int $nid
   *   Node id.
   *
   * @return mixed
   *   DB query result.
   */
  public static function checkNode($nid) {
    return db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('n.nid', $nid)
      ->condition('n.type', 'move_request')
      ->execute()
      ->fetchField();
  }

  protected function clearRequest(&$request) {
    foreach ($request as $id => $field) {
      if ((is_array($field) && !$field) || !$field && $field != 0) {
        unset($request[$id]);
      }
    }

    if (isset($request['account'])) {
      unset($request['account']);
    }
  }

  /**
   * Set data field to $form_state. Only for formValidate().
   *
   * @param string $field_name
   *   Name of field.
   * @param string $type
   *   Field type.
   * @param array $form_state
   *   A keyed array containing the current state of the form.
   * @param array $value
   *   Data with value.
   */
  protected function setValueField($field_name, $type, &$form_state = array(), $value = array(), \stdClass &$node, $widget_type) {
    $field_info = field_info_field($field_name);
    $field_value = $this->getFieldArray($value, $type, $widget_type, $field_info);
    $form_state['values'][$field_name] = $field_value;
    $node->{$field_name} = $field_value;
    $form_state['node'] = $node;
  }

  /**
   * Get fields of node from cache or loaded of node and save in cache.
   *
   * @param int $nid
   *   Node id.
   * @param int $type
   *   Flag: 1 - Normal cache; 0 - Small Cache.
   *
   * @return mixed
   *   Cache data.
   *
   * @throws \Exception
   */
  public function getNode(int $nid, $type = 1) {
    // Check data in cache.
    $cache_data = Cache::getCacheData($nid, 'move_services_cache', $type);
    if ($cache_data) {
      $this->data = $cache_data;
    }
    else {
      if ($this->checkNode($nid)) {
        $node = entity_metadata_wrapper('node', $nid);
        $request = new MoveRequestRetrieve($node);
        $nodeFields = $request->nodeFields();
        $small_data = $request->smallNodeFields($nodeFields);

        // Set new cache data.
        Cache::setCacheData($nid, $nodeFields, 'move_services_cache', $small_data);
        $this->data = $nodeFields;
      }
      else {
        return services_error(t('Unknown node'), 406);
      }
    }

    return $this->data;
  }

  /**
   * Get last client request node.
   *
   * @param object $user
   *   User object of client.
   *
   * @return int
   *   Return nid of request
   */
  public function getLastClientRequest($user) {
    $result = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->orderBy('created', 'DESC')
      ->range(0, 1)
      ->condition('uid', $user->uid)
      ->execute()
      ->fetchField();
    return $result;
  }

  /**
   * Get client sales/manger who want create the request. Check sales/manger if active.
   *
   * @param object $user
   *   User object of client.
   *
   * @return bool|int
   *   Return sales/manger uid or FALSE.
   */
  public function getSalesForExistingClient($user) {
    $request_manager = FALSE;
    $last_client_request_id = $this->getLastClientRequest($user);
    if ($last_client_request_id) {
      $request_manager = self::getRequestManager($last_client_request_id, 1);
      if (!empty($request_manager)) {
        $user_setting = Users::getUserSettings($request_manager);
        if (!empty($user_setting['employee_is_active'])) {
          return $request_manager;
        }
      }
    }
    return $request_manager;
  }

  /**
   * Get real node.
   *
   * @param int $nid
   *   Node id.
   *
   * @return bool|mixed
   *   Loaded node object or FALSE.
   */
  public static function getRealNode($nid) {
    $node = node_load($nid);
    $node->revision = FALSE;
    return $node;
  }

  /**
   * Helper function to get lists of node ids.
   *
   * @param array $nodes
   *   Ids from DB.
   *
   * @return array
   *   lists of nids.
   */
  protected function getIdNodes($nodes = array()) {
    $list = array();

    foreach ($nodes as $node) {
      $list[] = $node->nid;
    }

    return $list;
  }

  /**
   * Check and set title to node.
   *
   * @param \stdClass $node
   *   Node object.
   * @param array $fields
   *   Field with title.
   */
  protected function checkRequiredTitle(\stdClass &$node, $fields = array()) : void {
    if (isset($fields['title'])) {
      $node->title = $fields['title'];
    }
    else {
      $node->title = "Move Request";
    }
  }

  /**
   * Helper function.
   *
   * @param array $values
   *   Value os field.
   * @param string $type
   *   Field Type.
   * @param string $widget_type
   *   The widget type.
   * @param array $field_info
   *   Information about field.
   *
   * @return array
   *   Result.
   */
  protected function getFieldArray($values = array(), string $type, string $widget_type, array $field_info) {
    switch ($type) {
      case 'list_boolean':
        $context = new fieldArrayContext(new fieldBoolStrategy());
        break;

      case 'email':
        $context = new fieldArrayContext(new fieldEmailStrategy());
        break;

      case 'addressfield':
        $context = new fieldArrayContext(new fieldAddressStrategy());
        break;

      case 'list_text':
        $context = new fieldArrayContext(new fieldOptionsStrategy());
        break;

      case 'datetime':
        $context = new fieldArrayContext(new fieldDateStrategy());
        break;

      case 'entityreference':
        $context = new fieldArrayContext(new fieldEntityRefStrategy());
        break;

      case 'taxonomy_term_reference':
        $context = new fieldArrayContext(new fieldTaxonomyRefStrategy());
        break;

      case 'image':
        $context = new fieldArrayContext(new fieldImageFileStrategy());
        break;

      default:
        // text, date_popup, number_integer, number_decimal, text_long.
        $context = new fieldArrayContext(new fieldTextStrategy());
        break;
    }

    return $context->execute($values, $widget_type, $field_info);
  }

  /**
   * Validate form elements.
   *
   * @param \stdClass $node
   *   Node.
   * @param array $data
   *   Array data.
   *
   * @return array|null
   *   Result.
   */
  protected function formValidate(stdClass $node, array $data = array()): ?array {
    module_load_include('inc', 'node', 'node.pages');

    $form_id = 'move_request_node_form';
    $form_state = array();
    $form_state['values']['op'] = t('Save');
    $field_list = field_info_instances('node', 'move_request');

    foreach ($data as $field_name => $value) {
      // Check field.
      if (array_key_exists($field_name, $field_list)) {
        if (is_array($value)) {
          if (isset($value['add'])) {
            $value = $value['add'];
          }
          if (isset($value['del'])) {
            $value = $value['del'];
          }
        }

        $widget_type = $field_list[$field_name]['widget']['type'];
        $field_info = field_info_field($field_name);
        $field_type = $field_info['type'];
        $this->setValueField($field_name, $field_type, $form_state, $value, $node, $widget_type);
      }
    }

    $form_state['build_info']['args'][] = $node;
    $form_state += form_state_defaults();
    $form_state['input'] = $form_state['values'];
    $form_state['programmed'] = TRUE;
    $form = drupal_retrieve_form($form_id, $form_state);
    $form_state['submitted'] = TRUE;
    $form_state['must_validate'] = TRUE;
    drupal_prepare_form($form_id, $form, $form_state);

    $form = form_builder($form_id, $form, $form_state);
    $form_state['triggering_element']['#parents'] = array();
    _form_validate($form, $form_state, $form_id);

    $counter = 0;

    if ($errors = form_get_errors()) {
      // Check for empty addressfield.
      foreach ($errors as $key => $error) {
        // Ignore several fields.
        if ((strpos($key, 'field_moving_from') === FALSE) && (strpos($key, 'field_moving_to') === FALSE)
        && (strpos($key, 'field_helper') === FALSE) && (strpos($key, 'field_foreman') === FALSE)) {
          $counter++;
        }
        else {
          unset($errors[$key]);
        }
      }

      if ($counter) {
        watchdog("Validate errors", print_r($errors, TRUE), array(), WATCHDOG_ERROR);
      }
    }
    // Clean error message.
    if (isset($_SESSION['messages']['error'])) {
      unset($_SESSION['messages']['error']);
    }

    if (!empty($errors)) {
      return self::processDrupalValidationErrors($errors);
    }

    return NULL;
  }

  /**
   * Processes Drupal validation errors for response.
   *
   * Removes Drupal metadata from keys. Stripes HTML tags from messages.
   *
   * @param array $drupal_validation_errors
   *
   * @return array
   */
  protected static function processDrupalValidationErrors(array $drupal_validation_errors) :array {
    $validation_errors = array();

    foreach ($drupal_validation_errors as $key => $message) {
      // preg_match('/(.+?)]/', $key, $matches); $key = $matches[0];
      $validation_errors[substr($key, 0, strpos($key, ']'))] = strip_tags($message);
    }

    return $validation_errors;
  }

  /**
   * Create new node.
   *
   * @param array $request
   *   Request data.
   *
   * @return int
   *   Node id of new created node.
   */
  protected function createNode(array $request = array()) : int {
    $node = entity_create('node', array('type' => 'move_request'));
    if (isset($this->user->uid)) {
      $node->uid = $this->user->uid;
      if (isset($request['uid'])) {
        $node->uid = $request['uid'];
        unset($request['uid']);
      }
    }

    return $this->saveNode(NULL, $request);
  }

  /**
   * Save or Update node.
   *
   * @param int $nid
   *   The Node id.
   * @param array|null $request
   *   Request array.
   *
   * @return mixed
   *   Result.
   *
   * @throws \ServicesException
   */
  protected function saveNode($nid = NULL, ?array $request = array()) {
    if (!$request) {
      return $nid;
    }
    try {
      $emw_node = entity_metadata_wrapper('node', $nid, array('bundle' => 'move_request'));
      $prop_info = $emw_node->getPropertyInfo();

      foreach ($request as $field_name => $value) {
        $multi = FALSE;
        $field_info = field_info_field($field_name);
        // Correct data for multi field value.
        if (isset($field_info['cardinality']) && ($field_info['cardinality'] == -1 || $field_info['cardinality'] >= 2)) {
          $multi = TRUE;
          if (!empty($value) && (!is_array($value) || (!array_key_exists(0, $value) && !isset($value['add']) && !isset($value['del'])))) {
            $value = array($value);
          }
        }

        // Correct this value if need to add new to exists value.
        $exist_value = $emw_node->{$field_name}->raw();
        if ((isset($prop_info[$field_name]['property info']) && $prop_info[$field_name]['type'] != 'list<field_item_image>')
          || (isset($prop_info[$field_name]['type'])
            && ($prop_info[$field_name]['type'] == 'list<user>' || $prop_info[$field_name]['type'] == 'user'))) {

          if (isset($value['add'])) {
            $interim = $value['add'];
            if ($multi) {
              $value = array_merge($interim, $exist_value);
              $value = array_unique($value);
            }
            else {
              $value = is_array($interim) ? $interim[0] : $interim;
            }
          }

          if (isset($value['del'])) {
            if ($multi && is_array($value['del'])) {
              foreach ($value['del'] as $element) {
                $key = array_search($element, $exist_value);
                if ($key !== FALSE) {
                  unset($exist_value[$key]);
                }
              }
              $value = $exist_value;
            }
            else {
              $value = NULL;
            }
          }
        }

        if (isset($prop_info[$field_name]['property info'])) {
          if ($multi && $prop_info[$field_name]['type'] != 'list<field_item_image>') {
            foreach ($value as $id => $item) {
              $data = array();
              foreach ($prop_info[$field_name]['property info'] as $key2 => $value2) {
                if (isset($item[$key2])) {
                  $data[$key2] = $item[$key2];
                }
              }
              $emw_node->{$field_name}[$id]->set($data);
            }
          }
          elseif ($prop_info[$field_name]['type'] == 'list<field_item_image>') {
            if (isset($value['add'])) {
              $count = count($value['add']);
              (new FieldImageFileStrategy())->createData($value['add'], 'image', $count, $multi, $field_info);
              self::$files = array_merge(self::$files, $exist_value);
              $this->emwFileSet($emw_node, $field_name);
            }
            elseif (!isset($value['del'])) {
              $emw_node->{$field_name} = NULL;
              $this->emwFileSet($emw_node, $field_name);
            }

            if (isset($value['del'])) {
              foreach ($exist_value as $key => $item) {
                foreach ($value['del'] as $item2) {
                  if (isset($item2['fid']) && $item2['fid'] == $item['fid']) {
                    unset($emw_node->{$field_name}{$key});
                  }
                }
              }
            }
          }
          else {
            $data = array();
            foreach ($prop_info[$field_name]['property info'] as $key2 => $value2) {
              if (isset($value[$key2])) {
                $data[$key2] = $value[$key2];
              }
            }
            $emw_node->{$field_name}->set($data);
          }
        }
        elseif (isset($prop_info[$field_name]['type']) &&
          ($prop_info[$field_name]['type'] == 'date' || $prop_info[$field_name]['type'] == 'list<date>')) {

          if (!is_array($value)) {
            $value = array('date' => $value);
          }
          else {
            // Correct data if have multi values in it.
            if (!array_key_exists('date', $value) && !array_key_exists('time', $value)) {
              $correct_value = array();
              foreach ($value as $item) {
                if (!is_array($item)) {
                  $correct_value[] = array('date' => $item);
                }
              }
              if ($correct_value) {
                $value = &$correct_value;
              }
            }
          }

          if ($multi) {
            foreach ($value as $id => $item) {
              $emw_node->{$field_name}[$id] = $this->getTimestamp($item);
            }
          }
          else {
            // Correct data if not multi value.
            if (!array_key_exists('date', $value) && !array_key_exists('time', $value)
              && (count($value) == 1)) {
              $value = reset($value);
            }

            // This check for empty date.
            if (empty($value['date'])) {
              $emw_node->{$field_name} = NULL;
            }
            else {
              $emw_node->{$field_name}->set($this->getTimestamp($value));
            }
          }
        }
        else {
          // Additional check of field list_boolean type.
          // Drupal doesn't give error on validate this field type.
          if (isset($prop_info[$field_name]['type']) && $prop_info[$field_name]['type'] == 'boolean') {
            if (!array_key_exists((int) $value, $field_info['settings']['allowed_values'])) {
              // Set $value at first element from allowed values.
              $value = reset($field_info['settings']['allowed_values']);
            }
          }
          elseif (isset($prop_info[$field_name]['type']) && $prop_info[$field_name]['type'] == 'node') {
            $value = (is_array($value)) ? reset($value) : $value;
          }

          // Clear value from empty data.
          if (is_array($value)) {
            foreach ($value as $id => $item) {
              if ($item === '') {
                unset($value[$id]);
              }
            }
          }

          $emw_node->{$field_name} = $value;
        }
      }

      $emw_node->save();
      $result = $emw_node->getIdentifier();

      return $result;
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()}";
      watchdog("Move Request: saveNode", "<pre>$message</pre>", array(), WATCHDOG_CRITICAL);
      services_error($e->getMessage(), 500);
    }
  }

  /**
   * Helper method to save data for file field type.
   *
   * @param \EntityMetadataWrapper $emw_node
   *   Loaded Entity Metadata Wrapper object.
   * @param string $field_name
   *   The name of field.
   */
  protected function emwFileSet(\EntityMetadataWrapper &$emw_node, $field_name) {
    foreach (self::$files as $key => $file) {
      $file = (object) $file;
      $title = $file->title ?? '';
      if (isset($file->title)) {
        unset($file->title);
      }
      unset(self::$files[$key]);
      $emw_node->{$field_name}{$key}->file = $file;
      $emw_node->{$field_name}{$key}->title = $title;
    }
  }

  /**
   * Helper function to get timestamp value from date-time field.
   *
   * @param array $value
   * @return int
   */
  protected function getTimestamp(array $value) {
    $date = isset($value['date']) ? $value['date'] : '';
    $time = isset($value['time']) ? $value['time'] : '';
    $acceptance_date = new \DateTime();
    $acceptance_date->setTimezone(new \DateTimeZone('UTC'));
    $acceptance_date->setTimestamp(strtotime("$date $time"));
    return $acceptance_date->getTimestamp();
  }

  /**
   * Get manager request's.
   *
   * @param int $nid
   *   Node id.
   * @param int $exclude_users
   *   User id for exclude.
   *
   * @return mixed
   *   User object or NULL.
   */
  public static function getRequestManager(int $nid, int $exclude_users = 0) {
    $query = db_select('move_request_sales_person', 'm');
    $query->fields('m', array('uid'));
    if (!empty($exclude_users)) {
      $query->condition('m.uid', $exclude_users, '<>');
    }
    $query->condition('m.nid', $nid);
    $query->condition('m.main', 1);
    $uid = $query->execute()->fetchField();

    return $uid ? (int) $uid : NULL;
  }

  /**
   * Get all managers/sales from request.
   *
   * @param int $nid
   *   Request id.
   *
   * @return array
   *   Array with user ids, or empty array.
   */
  public static function getRequestAllManagers(int $nid) : array {
    $query = db_select('move_request_sales_person', 'm');
    $query->fields('m', array('uid', 'main'));
    $query->condition('m.nid', $nid);
    $uids = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return $uids ? $uids : array();
  }

  /**
   * Get last name and first name manager's from request.
   *
   * @param int $nid
   *   The Node id.
   *
   * @return array
   *   Manager data.
   */
  public static function getRequestManagerInfo($nid) {
    $result = array();
    $uid = MoveRequest::getRequestManager($nid);
    if ($uid && user_load($uid)) {
      $user = entity_metadata_wrapper('user', $uid);
      $result['last_name'] = $user->field_user_last_name->value();
      $result['first_name'] = $user->field_user_first_name->value();
    }

    return $result;
  }

  /*
   * Search only delivery flatrate requests
   */
  public function getFlatrateRequests($date) {
    $parklot = array();
    $results = array();
    $obj_date = new \DateTime($date);
    $last_day = $obj_date->modify('last day of this month 23:59:59')->format('d');
    $obj_date->modify('first day of this month 00:00:00');
    for ($i = 1; $i <= $last_day; $i++) {
      $query = new \EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'move_request')
        ->fieldCondition('field_delivery_date_from', 'value', $obj_date->format('Y-m-d'), '<=')
        ->fieldCondition('field_delivery_date_to', 'value', $obj_date->format('Y-m-d'), '>=')
        ->fieldCondition('field_move_service_type', 'value', 5, '=')
        ->addMetaData('account', user_load(1));
      $results[$i] = $query->execute();
      $obj_date->modify('+1 day');
    }

    foreach ($results as $day => $result) {
      if (isset($result['node'])) {
        foreach (array_keys($result['node']) as $key => $nid) {
          $parklot[$day][$nid] = $this->getParklotRequest($nid);
        }
      }
      else {
        $parklot[$day] = array();
      }
    }

    return $parklot;
  }

  /**
   * Get parklot request.
   *
   * @param int $nid
   *   Node id.
   *
   * @return array
   *   Array node.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function getParklotRequest(int $nid) {
    $move_request = array();
    $obj = new MoveRequest($nid);
    $request = $obj->getNode($nid, 1);

    $needed_fields = array(
      'nid',
      'trucks',
      'delivery_trucks',
      'service_type',
      'delivery_date_from',
      'delivery_date_to',
      'field_double_travel_time',
      'travel_time',
      'status',
      'crew',
      'move_size',
      'minimum_time',
      'maximum_time',
      'start_time1',
      'start_time2',
      'zip_from',
      'zip_to',
      'field_reservation_received',
      'from',
      'to',
      'date',
      'name',
      'field_company_flags',
      'ddate',
      'delivery_start_time',
      'phone',
      'email',
      'link',
      'rate',
      'type_from',
      'type_to',
      'distance',
      'adrfrom',
      'request_all_data',
      'field_delivery_crew_size',
      'request_data',
      'field_foreman_assign_time',
      'field_helper',
      'field_foreman',
      'field_flat_rate_local_move',
      'apt_from',
      'apt_to',
      'field_moving_from',
      'field_moving_to',
      'inventory',
      'field_useweighttype',
      'inventory_weight',
      'custom_weight',
      'commercial_extra_rooms',
      'field_custom_commercial_item',
      'rooms',
    );

    foreach ($needed_fields as $field_name) {
      $move_request[$field_name] = isset($request[$field_name]) ? $request[$field_name] : '';
    }

    return $move_request;
  }

  public function getOptionsForPickup($options, $request_id) {
    $array_keys = array(
      'rate',
      'picktime1',
      'picktime2',
      'deltime1',
      'deltime1',
      'pickup',
      'delivery',
    );

    foreach ($array_keys as $key) {
      if (!array_key_exists($key, $options)) {
        return;
      }
    }

    $rate = $options['rate'];
    $pick_time1 = $this->getTimeOfDay($options['picktime1']);
    $pick_time2 = $this->getTimeOfDay($options['picktime2']);
    $del_time1 = $this->getTimeOfDay($options['deltime1']);
    $del_time2 = $this->getTimeOfDay($options['deltime2']);

    // Prepare date format.
    $pickupdate = date("Y-n-j", strtotime($options['pickup']));
    $deliverydate = date("Y-n-j", strtotime($options['delivery']));

    $node_wrapper = entity_metadata_wrapper('node', $request_id);
    $node_wrapper->field_date->set(strtotime($pickupdate));
    $node_wrapper->field_delivery_date->set(strtotime($deliverydate));
    $node_wrapper->field_approve->set(1);
    $node_wrapper->field_actual_start_time->set($pick_time1);

    if ($options['picktime2'] != 0) {
      $node_wrapper->field_start_time_max->set($pick_time2);
    }

    $node_wrapper->field_estimated_prise->set($rate);
    // Update Delivery.
    $node_wrapper->field_delivery_start_time1->set($del_time1);
    if ($options['deltime2'] != 0) {
      $node_wrapper->field_delvery_start_time2->set($del_time2);
    }
    $node_wrapper->save();
  }

  private function getTimeOfDay($time) {
    $time = (int) $time;
    if ($time > 12) {
      $amtime = $time - 12;
      $result = ($amtime == 12) ? "12:00 AM" : $amtime . ':00 PM';
    }
    else {
      $result = ($time == 12) ? "12:00 PM" : $time . ':00 AM';
    }

    return $result;
  }

  /**
   * Function to get contract by node.
   *
   * @param int $nid
   *   Node id.
   *
   * @return array|bool
   *   Unserialazed array.
   */
  public static function getContract($nid) {
    // Check user permissions.
    if (!(new Sales($nid))->checkUserPermRequest($nid)) {
      return FALSE;
    }

    $result = array();
    $value = db_select('move_contracts', 'mc')
      ->fields('mc', array('value'))
      ->condition('mc.nid', $nid, '=')
      ->execute()
      ->fetchField();

    if ($value) {
      $result = unserialize($value);
    }

    return $result;
  }

  /**
   * Save contract to database.
   *
   * @param int $nid
   *   Node Id.
   * @param array $value
   *   Array to save.
   *
   * @return int
   *   Result of operation.
   *
   * @throws \Exception
   */
  public static function setContract(int $nid, array $value = array()) {
    global $user;
    $request = (new MoveRequest($nid))->retrieve();
    $is_from_storage = isset($request['from_storage']) && $request['from_storage'] == 1;

    if ($is_from_storage) {
      // Check user permissions.
      if (!(new Sales($nid))->checkUserPermRequest($nid) && !UserPermissions::foremanCanUpdateOtherRequest($request, $user->uid)) {
        return FALSE;
      }
    }

    $write = array(
      'nid' => (int) $nid,
      'value' => serialize($value),
    );

    $result = db_merge('move_contracts')
      ->key(array('nid' => (int) $nid))
      ->fields($write)
      ->execute();

    return $result;
  }

  /**
   * Get all receipts relation with node.
   *
   * @param int $entity_id
   *   The id of entity relation with receipt.
   *
   * @return array
   *   Receipts.
   */
  public static function getReceipt(int $entity_id, int $entity_type = EntityTypes::MOVEREQUEST) : array {
    return (new Receipt($entity_id, $entity_type))->retrieve();
  }

  /**
   * Save receipt to database.
   *
   * @param int $entity_id
   *   The id of entity.
   * @param array $receipt
   *   The data receipt's for save.
   * @param int $entity_type
   *   The type of entity.
   *
   * @return \DatabaseStatementInterface|int
   *   Id new insert receipt.
   *
   * @throws \Exception
   */
  public static function setReceipt(int $entity_id, array $receipt = array(), int $entity_type = EntityTypes::MOVEREQUEST) : int {
    return (new Receipt($entity_id, $entity_type))->create($receipt);
  }

  /**
   * Update receipt to database.
   *
   * @param int $id
   *   The unique id of record.
   * @param array $receipt
   *   The data receipt's for save.
   *
   * @return int
   *   Result of operation.
   *
   * @throws \ServicesException
   */
  public static function updateReceipt(int $id, $receipt = array()) {
    return (new Receipt(NULL, NULL, $id))->update($receipt);
  }

  /**
   * Delete receipt.
   *
   * @param int $receipt_id
   *   Receipt id.
   *
   * @return int
   *   Result db_delete.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public static function deleteReceipt(int $receipt_id) : int {
    return (new Receipt(NULL, NULL, $receipt_id))->delete();
  }

  /**
   * Send email when request.
   *
   * @param int $nid
   *   Node id.
   */
  public static function mailReservation($nid) {
    $template_builder = new TemplateBuilder();
    $node = node_load($nid);
    $template_builder->moveTemplateBuilderSendTemplateRules('reminder_finish_reservation', $node);
  }

  public static function createEditable($nid, $uid) {
    $write = array(
      'nid' => $nid,
      'uid' => $uid,
      'time' => time(),
    );

    $query = db_merge('move_edit_request')
      ->key(array('nid' => $nid))
      ->fields((array)$write);

    return $query->execute();
  }

  public static function deleteEditable($nid) {
    return db_delete('move_edit_request')
      ->condition('nid', $nid)
      ->execute();
  }

  public static function checkEditable($nid) {
    global $user;

    $check = db_select('move_edit_request', 'mer')
      ->fields('mer', array('uid'))
      ->condition('mer.nid', $nid)
      ->condition('mer.time', time() - 3600, '>')
      ->execute()
      ->fetchField();

    $clients = new Clients($user->uid);
    $isManager = $clients->checkUserRoles(array('manager', 'sales'));

    if ($isManager) {
      $instance = new Sales($nid);
      $is_manager_of_request = $instance->checkIsManagerOfRequest($nid);
      $is_not_have_enough_permission = $isManager && (!$instance->checkUserPermRequest($nid, array('administrator'), 'canEditOtherLeads') && !$is_manager_of_request);

      if ($is_not_have_enough_permission) {
        $result = array('status' => FALSE, 'uid' => -1);

        return $result;
      }
    }

    if ($check) {
      $clients = new Clients($check);
      $clients = $clients->retrieve();
      $result = array(
        'status' => $isManager,
        'uid' => $clients,
      );
    }
    else {
      self::createEditable($nid, $user->uid);
      $result = array(
        'status' => TRUE,
        'uid' => $user,
      );
    }

    return $result;
  }

  public function getForHelperRequest($type = 'foreman', $uid) {
    global $user;
    $result = array();

    // If uid is not defined then set it for current logged user.
    if (!$uid) {
      $uid = $user->uid;
    }

    $field_query = new \EntityFieldQuery();
    $field_query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request');

    switch ($type) {
      case 'foreman':
        $field_query->fieldCondition('field_foreman', 'target_id', $uid);
        break;

      case 'helper':
        $field_query->fieldCondition('field_helper', 'target_id', $uid);
        break;

      default:
        return $result;
    }

    $field_query->addMetaData('account', user_load(1));
    $query = $field_query->execute();

    if (isset($query['node'])) {
      $nids = array_keys($query['node']);
      foreach ($nids as $nid) {
        $result[$nid] = $this->getNode($nid);
      }
    }

    return $result;
  }

  public function createFlag(array $values, string $type = 'flags') {
    $id = FALSE;
    foreach ($values as $item) {
      switch ($type) {
        case 'flags':
          $record = move_service_flag_new();
          $record->value = (string)$item;
          entity_save('move_service_flag', $record);
          $id = (int)$record->identifier();
          break;
      }
    }

    return $id;
  }

  /**
   * Get allowed values from flag.
   *
   * @return array
   *   Flags.
   */
  public static function getAllowedValueFlag() : array {
    $return = array();
    $flags = entity_load('move_service_flag');
    $field_company = field_info_field('field_company_flags');
    $company_flags = list_allowed_values($field_company);

    foreach ($flags as $id => $item) {
      $return['flags'][$id] = $item->value;
    }

    foreach ($company_flags as $id => $item) {
      $return['company_flags'][$id] = $item;
    }

    return $return;
  }

  public function removeFlag($id, $type = 'flags') {
    $result = NULL;

    switch ($type) {
      case 'flags':
        $result = entity_delete('move_service_flag', $id);
        break;
    }

    return $result;
  }

  public function editFlag($id, $new_value, $type = 'flags') {
    if ($new_value) {
      switch ($type) {
        case 'flags':
          $flag = entity_load('move_service_flag', array($id));
          if ($flag) {
            $node_wrap = entity_metadata_wrapper('move_service_flag', $id);
            $node_wrap->value->set((string) $new_value);
            $node_wrap->save();
          }
          break;
      }
    }
  }

  /**
   * Save "Request All Data" to database.
   *
   * @param int $nid
   *   Node id.
   * @param array $request_all_data
   *   Data to save in DB.
   *
   * @throws \Exception
   */
  public static function setRequestAllData(int $nid, array $request_all_data) {
    db_merge('move_all_data')
      ->key(array('nid' => $nid))
      ->fields(array('value' => serialize($request_all_data)))
      ->execute();
  }

  public static function setRequestAllDataIndex(int $nid, array $request_all_data) {
    if (isset($request_all_data['statistic'])) {
      try {
        db_merge('move_all_data_index')
          ->key(array('nid' => $nid))
          ->fields(array(
            'user_visit_account' => isset($request_all_data['statistic']['user_visit_account']) ? $request_all_data['statistic']['user_visit_account'] : NULL,
            'first_date_visit_account' => isset($request_all_data['statistic']['first_date_visit_account']) ? $request_all_data['statistic']['first_date_visit_account'] : NULL,
            'user_visit_confirmation_page' => isset($request_all_data['statistic']['user_visit_confirmation_page']) ? $request_all_data['statistic']['user_visit_confirmation_page'] : NULL,
            'first_visit_confirmation_page' => isset($request_all_data['statistic']['first_visit_confirmation_page']) ? $request_all_data['statistic']['first_visit_confirmation_page'] : NULL,
            'booked_by' => isset($request_all_data['statistic']['booked_by']) ? $request_all_data['statistic']['booked_by'] : NULL,
            'booked_date' => isset($request_all_data['statistic']['booked_date']) ? $request_all_data['statistic']['booked_date'] : NULL,
          ))
          ->execute();
      }
      catch (\Throwable $e) {
        $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
        watchdog("setRequestAllDataIndex", $error_text, array(), WATCHDOG_ERROR);
      }
    }
  }

  public static function getRequestAllData($nid) {
    $value = db_select('move_all_data', 'ad')
      ->fields('ad', array('value'))
      ->condition('ad.nid', $nid)
      ->execute()
      ->fetchField();

    return $value ? unserialize($value) : [];
  }

  public static function setRequestData($nid, $value) {
    db_merge('move_request_data')
      ->key(['nid' => $nid])
      ->fields(['value' => serialize($value)])
      ->execute();
  }

  public static function getRequestData($nid) {
    $value = db_select('move_request_data', 'mrd')
      ->fields('mrd', ['value'])
      ->condition('mrd.nid', $nid)
      ->execute()
      ->fetchField();

    return $value ? unserialize($value) : [];
  }

  /**
   * Get card from DB.
   *
   * @param int $nid
   *   The node id.
   *
   * @return array
   *   All cards by nid.
   */
  public static function getCard(int $nid) : array {
    $result = array();
    $current_path = drupal_get_path("module", "move_services_new");
    $file = "$current_path/keys/public.pem";
    $fpp1 = fopen($file,"r");
    $public_key = fread($fpp1,8192);
    fclose($fpp1);

    $db_cards = db_select('move_contract_credit_card', 'mccc')
      ->fields('mccc')
      ->condition('mccc.nid', $nid)
      ->execute()->fetchAll();

    foreach ($db_cards as $db_card) {
      $result[] = Extra::decrypt_RSA($public_key, $db_card->value);
    }

    return $result;
  }

  /**
   * Save card to DB.
   *
   * @param int $nid
   *   Node id.
   * @param array $data
   *   Array with Base64 images.
   *
   * @return array
   *   Results of operations.
   *
   * @throws \Exception
   */
  public static function setCard(int $nid, array $data) : array {
    $result = array();
    $current_path = drupal_get_path("module", "move_services_new");
    $file = "$current_path/keys/private.pem";
    $fpp1 = fopen($file,"r");
    $private_key = fread($fpp1,8192);
    fclose($fpp1);

    foreach ($data as $item) {
      $text = Extra::encrypt_RSA($item, $private_key);
      $result[] = db_insert('move_contract_credit_card')
        ->fields(array('nid' => $nid, 'value' => $text))
        ->execute();
    }

    return $result;
  }

  /**
   * Get data for page dashboard.
   *
   * @return array|mixed
   *   Data: requests, invoices etc.
   *
   * @throws \InvalidMergeQueryException
   * @throws \SearchApiException
   * @throws \ServicesException
   */
  public function getDashBoard() {
    $result = &drupal_static(__METHOD__, array());

    if (!$result) {
      $user_settings = Users::getUserSettings($this->clients->getUid());
      // Pending requests.
      $search = new MoveRequestSearch(array(
        'display_type' => RequestSearchTypes::SHORT,
        'condition' => array('field_approve' => [1, 7, 9, 10, 16, 17]),
        'sorting' => array(
          'direction' => 'DESC',
          'field' => 'nid',
        ),
      ));

      $search->setAllDatesByUserSetting($user_settings);

      $result['pending'] = $search->search(TRUE);
      // Calculate unconfirmed.
      $result['unconfirmed'] = $search->unconfirmedRequests(0);
      // Calculate reservation.
      $result['unconfirmed_reserved'] = $search->unconfirmedRequests(1);

      // Confirmed Booked Date.
      $confirmed_booked_date = $search->confirmBookedDate();
      $confirmed_moved_date = $search->confirmMoveDate();

      $result['confirmed_total_dates'] = $this->mergeConfirmedAllDates($confirmed_booked_date, $confirmed_moved_date);

      $confirmed_booked_date['nodes'] = array_values($confirmed_booked_date['nodes']);
      $result['confirmed_booked_date'] = $confirmed_booked_date;

      // Confirmed Move Date.
      $confirmed_moved_date['nodes'] = array_values($confirmed_moved_date['nodes']);
      $result['confirmed_move_date'] = $confirmed_moved_date;

      // Get home estimate requests.
      $result['home_estimate'] = $search->homeEstimateRequests();
      // Unread Comments.
      $comments = new Comments();
      $comments_count = $comments->getUnreadCountUser();
      $new_comments = $comments->getAllNewComments(TRUE);
      $result['comments'] = array(
        'count' => $comments_count ? $comments_count : 0,
        'new_comments' => $new_comments,
      );
      $instance = new Invoice();
      $result['invoices']['all'] = $instance->getCountInvoicesForPeriod();
      $result['invoices']['paid'] = $instance->getCountInvoicesForPeriod(EntityTypes::MOVEREQUEST, NULL, NULL, InvoiceFlags::PAID);
    }

    return $result;
  }

  /**
   * Merge all confirmed request days.
   *
   * @param array $confirmed_booked_date
   *   Array with booked requests.
   * @param array $confirmed_moved_date
   *   Array with moved requests.
   *
   * @return mixed
   *   Result
   */
  private function mergeConfirmedAllDates(array $confirmed_booked_date, array $confirmed_moved_date) {
    $result['nodes'] = array_merge($confirmed_booked_date['nodes'], $confirmed_moved_date['nodes']);
    $result['nodes'] = array_unique($result['nodes'], SORT_REGULAR);
    $result['nodes'] = array_slice($result['nodes'], 0, 25);
    $result['total'] = count(array_unique(array_merge($confirmed_booked_date['nids'], $confirmed_moved_date['nids'])));

    return $result;
  }

  /**
   * Get data for modal window request.
   *
   * @return array
   *   Request data, trucks, managers etc.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function getSuperRequest() {
    $result = array();
    $result['request'] = $this->data;
    $move_date = $this->data['date']['value'];
    $get_home_estimate = function ($estimate_date) {
      $result = array();
      if (!empty($estimate_date)) {
        $home_estimate_search = new MoveRequestSearch(array(
          'filtering' => array('field_home_estimate_date' => strtotime($estimate_date)),
          'display_type' => RequestSearchTypes::HOME_ESTIMATE,
        ));
        $result = $home_estimate_search->search(FALSE);
      }
      return $result;
    };
    (new MoveRequestExtraService())->setRequestRetrieve($this->data)->build()->calculate();

    $condition = array('field_approve' => [2, 3]);
    // If enable branches parklot.
    if (variable_get('is_common_branch_truck', FALSE)) {
      $instance = new Trucks();
      $result['requests_day'] = $instance->getAllParklot($this->data['date']['raw'], $condition);
    }
    else {
      $result['requests_day'] = (new Parklot($this))->get($this->data['date']['raw'], $condition);
    }
    $result['trucks'] = $this->getSuperRequestTrucks((string) $move_date);
    $result['manager'] = self::getRequestManager($this->nid);
    $result['check_editable'] = self::checkEditable($this->nid);
    $result['managers'] = Sales::getManagers();
    $result['home_estimate'] = $get_home_estimate($this->data['home_estimate_date']['value']);
    $result['user_for_estimate'] = $this->getUserForEstimate();
    // Returns tracking arrivy url if connected else returns false (remake only for confirmed requests)
    $arrivy_task_url = (new ArrivyRequest($this->nid))->retrieve();
    if (!empty($arrivy_task_url)) {
      $result['arrivy_request_url'] = $arrivy_task_url[0];
      if (!empty($arrivy_task_url[1])) {
        $result['arrivy_delivery_request_url'] = $arrivy_task_url[1];
      }
    }
    else {
      $result['arrivy_request_url'] = $arrivy_task_url;
    }

    return $result;
  }

  private function getSuperRequestTrucks(string $move_date) : array {
    $obj_date = new \DateTime($move_date);
    $transform_date = $obj_date->format("Y-m-d");
    $trucksAvailability = array();
    $settings = new Settings();
    $field_condition = array(
      'field_ld_only' => 0,
    );
    $trucks = $settings->getTrucks($field_condition);
    // If enable branches parklot.
    if (variable_get('is_common_branch_truck', FALSE)) {
      $instance = new Trucks();
      $branch_trucks = $instance->index();
      $trucks += $branch_trucks['data'];
    }
    foreach ($trucks as $tid => $truck) {
      $flag = 1;

      if (isset($truck['unavailable'])) {
        foreach ($truck['unavailable'] as $unavailable_date) {
          if ($transform_date == date("Y-m-d", $unavailable_date)) {
            $flag = 0;
            break;
          }
        }
      }

      $trucksAvailability[$tid] = array(
        'name' => $truck['name'],
        'available' => $flag,
      );
    }

    return $trucksAvailability;
  }

  /**
   * Get user for estimate.
   *
   * @return array
   */
  public function getUserForEstimate() {
    $result = array();
    $role = array('manager', 'sales', 'customer service', 'administrator');
    $all_user = Users::getUserByRole($role, TRUE);

    foreach ($all_user as $role_users) {
      foreach ($role_users as $user) {
        $settings = $user['settings'];
        if (!empty($settings['homeEstimator'])) {
          $result[$user['uid']] = array(
            'first_name' => $user['field_user_first_name'],
            'last_name' => $user['field_user_last_name'],
            'uid' => $user['uid'],
          );
        }
      }
    }

    return $result;
  }

  /**
   * Get author(drupal node author) of request.
   *
   * @param int $nid
   *   Node id.
   *
   * @return mixed
   *   Uid or false.
   */
  public static function getRequestUserId(int $nid) {
    if (!empty($nid)) {
      $uid = db_select('node', 'n')
        ->fields('n', ['uid'])
        ->condition('n.nid', $nid)
        ->execute()->fetchField();

      if ($uid) {
        return $uid;
      }
    }
    else {
      throw new \RuntimeException('Nid is empty');
    }

    return FALSE;
  }

  /**
   * Remove sales/manger user from request.
   * If second argument is TRUE then we add new sales/manger to request.
   *
   * @param object $sales_user - user object
   * @param bool $add_new_manager - if true we add new sales/manger to request
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function removeSalesUserFromRequest($sales_user, $add_new_manager = FALSE) {
    try{
      if(!empty($sales_user->uid)) { // here we got all manager requests.
        $manager_nids = db_select('move_request_sales_person', 'sp')
          ->fields('sp', array('nid'))
          ->condition('sp.uid', $sales_user->uid)
          ->execute()
          ->fetchCol();
        if (!empty($manager_nids)) {
          if ($add_new_manager) {

          }
          else {
            // Delete all sales/manager uids and nids from table.
            db_delete('move_request_sales_person')
              ->condition('uid', $sales_user->uid)
              ->execute();

            // Updating cache.
            foreach ($manager_nids as $nid) {
              $was_updated = FALSE;
              $cached_data = Cache::getCacheData($nid);

              // Remove sales/manager from cache.
              if (isset($cached_data['manager'])) {
                unset($cached_data['manager']);
                $was_updated = TRUE;
              }
              // Remove sales/manager from cache.
              if (isset($cached_data['current_manager'])) {
                unset($cached_data['current_manager']);
                $was_updated = TRUE;
              }
              if ($was_updated) {
                Cache::setCacheData($nid, $cached_data);
              }
            }
          }
        }
      }
    }
    catch (\Throwable $e) {
      watchdog("Remove sales(manager) user", "<pre>$e</pre>", array(), WATCHDOG_CRITICAL);
    }
  }

  /**
   * Update sales/manger in request node
   * @param $uid
   */
  public static function updateRequestNodeFieldForSales($uid) {
    $sales_request = self::getSalesFromRequestNodeField($uid);
    if ($sales_request) {
      foreach ($sales_request as $nid) {
        $node = node_load($nid);
        $wrapper = entity_metadata_wrapper('node', $node);
        $wrapper->field_request_manager->set(NULL);
        $wrapper->save();
      }
    }
  }

  /**
   * Remove sales/manger from request.
   *
   * @param int $uid
   *    User id.
   * @param $nid
   *    Request id.
   */
  public static function deleteManagerFromRequestById(int $uid, int $nid) : void {
    db_delete('move_request_sales_person')
      ->condition('nid', $nid)
      ->condition('uid', $uid)
      ->execute();
    $all_managers = MoveRequest::getRequestAllManagers($nid);
    $managers_count = count($all_managers);
    // We need to have main sales.
    // If we have only one manager update it to main.
    if ($managers_count === 1) {
      db_update('move_request_sales_person')
        ->fields(['main' => 1])
        ->condition('nid', $nid)
        ->execute();
    }
    elseif($managers_count > 1) {
      $array_with_main_val = array_column($all_managers, 'main');
      // If we don't have main manager - set first to be main.
      if (!in_array('1', $array_with_main_val)) {
        db_update('move_request_sales_person')
          ->fields(['main' => 1])
          ->condition('nid', $nid)
          ->condition('uid', $all_managers[0]['uid'])
          ->execute();
      }
    }
  }

  /**
   * Getting all request id by sales/manager field from node.
   * @param $uid
   * @return array|bool|mixed
   */
  public static function getSalesFromRequestNodeField($uid) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition('field_request_manager', 'target_id', $uid);
    $result = $query->execute();
    if (!empty($result['node'])) {
      $result = array_keys($result['node']);
      return $result;
    }
    return FALSE;
  }

  /**
   * Added email mark when it was sent.
   * @param $node
   * @param string $mark - this can be interval day when it's was sent. Or something else.
   * @throws \ServicesException
   */
  public static function markSentEmailByInterval($node, string $mark = '') {
    if (!empty($node)) {
      try {
        db_insert('move_services_email_was_sent')
          ->fields(array(
            'nid' => $node->nid,
            'mark' => $mark,
            'time' => time()
          ))
          ->execute();
      }
      catch (\Throwable $e) {
        watchdog(WATCHDOG_WARNING, "Email type is <strong> @type </strong>. Email was not marked when sent(Emails which are sending by cron with days interval). ", array('@$type' => $mark));
      }
    }
    else {
      throw new \ServicesException(t('NOde object is empty'), 406);
    }
  }

  /**
   * This is the check was this email sent or not. Return true(if was sent) or false(was not sent).
   * @param $nid - the id on node request.
   * @param $mark - the mark when email was sent.
   * @return bool
   */
  public static function intervalEmailWasSent($nid, $mark) {
    $result = db_select('move_services_email_was_sent', 'mse')
      ->fields('mse')
      ->condition('nid', $nid)
      ->condition('mark', $mark)
      ->execute()
      ->fetchAllAssoc('nid');
    if (empty($result)) {
      return FALSE;
    }
    return $result;
  }

  /**
   * Update request delivery date field from details.
   * @param $nid
   * @param $date
   */
  public static function updateDeliveryDate($nid, $date) {
    $dates = explode(',', $date);
    if (is_numeric($dates[0])) {
      $date_len = strlen($dates[0]);
      //TODO на фронте домножают на 1000.
      if ($date_len > 10) {
        $dates[0] = $dates[0] / 1000;
      }
      $newDate = date('Y-m-d', $dates[0]);

    }
    else {
      if(!is_numeric($dates[0])) {
        $date = date_create_from_format('F j, Y', $date);
        if (!$date) {
          return 1;
        }
        $newDate = $date->format('Y-m-d');
      }
    }

    $node = new \stdClass();
    $node->nid = $nid;
    $node->type = 'move_request';
    // Entity metadatawrapper not work here.
    $node->field_delivery_date[LANGUAGE_NONE][0]['value'] = $newDate . ' 15:10:00';
    field_attach_presave('node', $node);
    field_attach_update('node', $node);
  }

  /**
   * Returns MoveRequest node identifier.
   *
   * @return int|null
   *   Current node identifier or null if not initialized.
   */
  public function getKey(): ?int {
    return $this->nid;
  }

  /**
   * Returns MoveRequest score points value.
   *
   * @return int
   */
  public function getScore(): int {
    $score = db_select('field_data_field_total_score', 'score')
      ->fields('score', array('field_total_score_value'))
      ->condition('entity_id', $this->nid)
      ->execute()
      ->fetchField();
    return empty($score) ? 0 : $score;
  }

  /**
   * Adds score points to MoveRequest state and updates total_score field in DB.
   *
   * @param int $score
   *   Score.
   *
   * @return array
   *   Result.
   */
  public function addScore(int $score): array {
    $score = $this->getScore() + $score;
    return $this->updateRequestScore($score);
  }

  /**
   * Update request score.
   *
   * @param int $score
   *   Score number.
   *
   * @return array
   *   Array with fields names.
   */
  public function updateRequestScore(int $score) : array {
    $date = date('Y-m-d H:i:s');
    $data_for_update = array(
      'field_total_score' => array(
        'raw' => 'value',
        'value' => $score,
        'data_to_update_in_cache' => array(
          'field_total_score' => $score
        ),
      ),
      'field_total_score_updated' => array(
        'raw' => 'value',
        'value' => $date,
        'data_to_update_in_cache' => array(
          'field_total_score_updated' => $date
        ),
      ),
    );
    return MoveRequest::updateNodeFieldsOnly($this->nid, $data_for_update, 'move_request', FALSE);
  }

  /**
   * Write notes in db.
   *
   * @param array $notes
   *   Body notes.
   *
   * @return \DatabaseStatementInterface|int
   *   bool.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \Throwable
   */
  public function set_notes(array $notes) {
    $result = db_merge('users_notes')
      ->key(array('nid' => $this->nid))
      ->fields(array(
        'sales_notes',
        'foreman_notes',
        'client_notes',
        'dispatch_notes',
      ), $notes)
      ->execute();

    if (!empty($result)) {
      (new DelayLaunch())->createTask(TaskType::GOOGLE_CALENDAR_MOVE_REQUEST_UPDATE_EVENT, ['nid' => $this->nid]);
      $result = TRUE;
    }
    return $result ?? FALSE;
  }

  /**
   * Get note from bd.
   *
   * @param array $role_note
   *   Ex. foreman_notes, or sales_notes.
   *
   * @return object
   *   Object with necessary note.
   */
  public function get_notes(array $role_note) {
    $result = db_select('users_notes', 'un')
      ->fields('un', array(
        'client_notes',
        'sales_notes',
        'foreman_notes',
        'dispatch_notes',
      ))
      ->condition('un.nid', $this->nid)
      ->execute()
      ->fetchAll();

    if (!empty($result[0])) {
      $result = $result[0];
    }
    else {
      $result = (object) $result;
      $result->client_notes = NULL;
      $result->sales_notes = NULL;
      $result->foreman_notes = NULL;
      $result->dispatch_notes = NULL;
      return $result;
    }
    if (!in_array('client_notes', $role_note)) {
      unset($result->client_notes);
    }
    if (!in_array('sales_notes', $role_note)) {
      unset($result->sales_notes);
    }
    if (!in_array('foreman_notes', $role_note)) {
      unset($result->foreman_notes);
    }
    if (!in_array('dispatch_notes', $role_note)) {
      unset($result->dispatch_notes);
    }
    return $result;
  }

  /**
   *  Check what fields was changed.
   *
   * @EntityMetadataWrapper $new_entity
   *   Entity with new fields values.
   * @EntityMetadataWrapper $old_entity
   *   Entity with old fields values.
   * @param array $fields
   *   Fields for check.
   *   Example
   *     $fields_to_check = array(
   *      'field_actual_start_time' => array(),
   *      'field_approve' => array(),
   *      'field_list_truck' => array('multi' => TRUE),
   *     );
   *
   * @return array
   *   If field was changed return this field with new and old value.
   */
  public static function changedFields(\EntityMetadataWrapper $new_metadata_wrapper, \EntityMetadataWrapper $old_metadata_wrapper, array $fields) {
    $result = array();
    foreach ($fields as $field_name => $settings) {
      if ($new_metadata_wrapper->__isset($field_name)) {
        // TODO make check for non existing fields. And make changes for field type.
        $new_value = $new_metadata_wrapper->{$field_name}->value();
        $old_value = $old_metadata_wrapper->{$field_name}->value();
        if (!empty($settings['multi'])) {
          if ((empty($new_value) && !empty($old_value)) || (!empty($new_value) && empty($old_value))) {
            $result[$field_name]['new'] = $new_value;
            $result[$field_name]['old'] = $old_value;
          }
          elseif (empty($new_value) && empty($new_value)) {
            continue;
          }
          else {
            $old_count = count($old_metadata_wrapper->{$field_name}->value());
            $new_count = count($new_metadata_wrapper->{$field_name}->value());
            if ($old_count != $new_count) {
              $result[$field_name]['new'] = $new_value;
              $result[$field_name]['old'] = $old_value;
            }
            else {
              foreach ($new_value as $item) {
                if (isset($item->tid)) {
                  $search_result = array_search($item->tid, array_column($old_value, 'tid'));
                  if ($search_result === FALSE) {
                    $result[$field_name]['new'] = $new_value;
                    $result[$field_name]['old'] = $old_value;
                  }
                }
              }
            }
          }
        }
        else {
          if ($new_value != $old_value) {
            $result[$field_name]['new'] = $new_value;
            $result[$field_name]['old'] = $old_value;
          }
        }
      }
    }
    return $result;
  }

  /**
   * Check overbooking.
   *
   * @TODO Этот метод по большей части скопирован из _movecalc_get_conflict_requests. Нужно подумать как не копировать код, а повторно его использовать.
   *
   * @param $nid
   * @param array $data .
   *  Example:
   *  field_date:1503760200
   *  field_duration:44
   *  field_maximum_move_time:2.25
   *  field_travel_time:1
   *  field_actual_start_time:8:00 AM
   *  field_list_truck:[12,34]
   *
   * @return bool
   */
  public static function checkOverbooking($nid, $data = array()) {
    // Common trucks.
    $common_trucks_sett = variable_get('is_common_branch_truck', FALSE);
    if ($common_trucks_sett) {
      return (new Trucks())->handlerConflictRequest($nid, $data);
    }
    try {
      $calculate_work_time_inst = new CalculateWorkTime();
      $overbooking = FALSE;
      if (empty($nid) && !empty($data)) {
        $request_date = strtotime($data['field_date']);
        $maximum_move_time = $data['field_maximum_move_time'] ?? $calculate_work_time_inst->buildWorkTimeByNid($nid)['max'];
        $travel_time = $data['field_travel_time'];
        $start_time = $data['field_actual_start_time'];
        $nid = $data['nid'];
        if (!empty($data['field_list_truck'])) {
          $list_truck = array_unique($data['field_list_truck']);
        }
      }
      elseif (!empty($nid) && empty($data)) {
        $wrapper = entity_metadata_wrapper('node', $nid);
        $request_date = $wrapper->field_date->raw();
        $maximum_move_time = $wrapper->field_maximum_move_time->value() ?? $calculate_work_time_inst->buildWorkTimeByNid($nid)['max'];
        $travel_time = $wrapper->field_travel_time->value();
        $start_time = $wrapper->field_actual_start_time->value();
        $list_truck = $wrapper->field_list_truck->value();
        if (!empty($list_truck)) {
          $list_truck = array_column($list_truck, 'tid');
        }

        if ($wrapper->field_double_travel_time->value()) {
          $travel_time += (float)$wrapper->field_double_travel_time->value() / 2;
        }
      }
      else {
        throw new \Exception("Input data is empty.");
      }
      // Get Park_lot array of unconfirmed requests.
      $park_lot = move_admin_get_same_day_request($request_date);
      if ((json_decode(variable_get('basicsettings', '')))->withTravelTime) {
        $work_time = ($maximum_move_time + $travel_time) * 60;
      }
      else {
        $work_time = $maximum_move_time * 60;
      }
      $work_time = round($work_time, 1, PHP_ROUND_HALF_UP);
      if (!$work_time) {
        throw new \ActualWorkTimeException("For request $nid can't be calculated work_time: $work_time ");
      }

      if (!$start_time) {
        throw new \ActualStartTimeException("For request $nid field `field_actual_start_time` got wrong data: $start_time ");
      }

      // Lambda function.
      // Add time to request move_date.
      $time_odd = function (string $time, int $date): int {
        $time_date = new \DateTime();
        $time_date->setDate(date("Y", $date), date("m", $date), date("d", $date));
        $time_date->setTime(date("H", strtotime($time)), date("i", strtotime($time)));
        return $time_date->getTimestamp();
      };

      // Lambda function.
      // To the $time will add $add_time hours.
      // Example: if $time "09:00AM" and $add_time is "2" - result will be "11:00AM".
      $time_even = function (int $time, int $add_time): int {
        $time_date = new \DateTime();
        $time_date->setTimestamp($time);
        $time_date->add(new \DateInterval("PT{$add_time}M"));
        return $time_date->getTimestamp();
      };

      $time_1_time = $start_time ? $start_time : "05:00 AM";
      // Get date with time when pickup will start.
      $time_1 = $time_odd($time_1_time, $request_date);
      // Get time when delivery will end.
      $time_2 = $time_even($time_1, $work_time);
      if (!$time_1) {
        throw new \Exception("\$time_1 of nid: $nid is not calculated!");
      }
      if (!$time_2) {
        throw new \Exception("\$time_2 of nid: $nid is not calculated!");
      }

      if (empty($list_truck)) {
        throw new \Exception("Trucks is empty.");
      }

      foreach ($list_truck as $truck) {
        if (!empty($park_lot[$truck])) {
          foreach ($park_lot[$truck] as $request) {
            try {
              // $request have only status "Not Confirmed".
              if ($request['nid'] == $nid) {
                continue;
              }

              if (empty($request['sT1'])) {
                throw new \Exception("'\$request[\'sT1\']\' of nid: {$request['nid']} is empty!");
              }

              $request['work_time'] = $request['work_time'] * 60;
              $request['work_time'] = round($request['work_time'], 1, PHP_ROUND_HALF_UP);

              if (!$request['maximum_time']) {
                throw new \Exception("maximum_time of nid: {$request['nid']} is empty!");
              }

              // Get date with time when pickup will start.
              $time_3 = $time_odd($request['sT1'], $request_date);
              // Get time when delivery will end.
              $time_4 = $time_even($time_3, $request['work_time']);
              if (!$time_3) {
                throw new \Exception("\$time_3 of nid: {$request['nid']} is not calculated!");
              }
              if (!$time_4) {
                throw new \Exception("\$time_4 of nid: {$request['nid']} is not calculated!");
              }

              $conflict = _move_is_intersect($time_1, $time_2, $time_3, $time_4);
              if ($conflict && $request['approve'] == RequestStatusTypes::CONFIRMED) {
                $overbooking = TRUE;
              }

              if ($conflict && ($request['field_reservation_received'] && $request['approve'] != RequestStatusTypes::PENDING_INFO)) {
                $overbooking = TRUE;
              }
            }
            catch (\Throwable $e) {
              $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
              watchdog("overbooking", $error_text, array(), WATCHDOG_ERROR);
            }
          }
        }
      }
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("overbooking check", $error_text, array(), WATCHDOG_ERROR);
    }
    finally {
      return $overbooking;
    }
  }

  public static function allowResetTruckAndStatusWhenOverbooking(bool $conflict, array $request): bool {
    $allow = FALSE;
    if ($conflict) {
      if ($request['approve'] != RequestStatusTypes::CONFIRMED && !$request['field_reservation_received']) {
        $allow = TRUE;
      }

      if ($request['approve'] == RequestStatusTypes::PENDING_INFO && $request['field_reservation_received']) {
        $allow = TRUE;
      }

    }
    return $allow;
  }

  /**
   * Check if we can to allow reservation.
   *
   * @param \EntityMetadataWrapper $wrapper
   * @return bool
   */
  public static function allowToOverbooking(\EntityMetadataWrapper $wrapper): bool {
    $allow_overbooking = FALSE;
    $error_flag = FALSE;
    $error_message = "";
    if (!empty($wrapper)) {
      try {
        $status = $wrapper->field_approve->value();
        $reservation = $wrapper->field_reservation_received->value();

        if ($status == RequestStatusTypes::CONFIRMED) {
          $allow_overbooking = TRUE;
        }
        // If not confirmed and reservation - allow overbooking.
        if ($status == RequestStatusTypes::NOT_CONFIRMED && !empty($reservation)) {
          $allow_overbooking = TRUE;
        }
      }
      catch (\Throwable $e) {
        $message = "Check if allow overbooking when request saved: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
        watchdog('Overbooking allow', $message, array(), WATCHDOG_CRITICAL);
        $error_flag = TRUE;
        $error_message = $e->getMessage();
      }
      finally {
        return $error_flag ? services_error($error_message, 406) : $allow_overbooking;
      }
    }
    return $allow_overbooking;
  }

  /**
   * Add/update one request to search api index.
   *
   * @param int $entity_id
   *   Id of request.
   */
  public function addRequestToSearchApiIndex(int $entity_id): void {
    $this->setSearchApiIndexByMachineName();
    if (!empty($this->searchApiIndex)) {
      search_api_index_specific_items($this->searchApiIndex, array($entity_id));
    }
  }

  /**
   * Set search api index by his machine_name.
   *
   * @param string $name
   *   Machine name of index.
   */
  public function setSearchApiIndexByMachineName(string $name = 'move_request'): void {
    $this->searchApiIndex = &drupal_static(__METHOD__, NULL);
    if (!$this->searchApiIndex) {
      $index_id = db_select('search_api_index', 'sai')
        ->fields('sai', array('id'))
        ->condition('machine_name', $name)
        ->execute()
        ->fetchField();
      if (!empty($index_id)) {
        $this->searchApiIndex = search_api_index_load($index_id);
      }
    }
  }

  /**
   * Create draft move request.
   *
   * @return int
   *   Node id.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function createDraftMoveRequest() {
    $this->draft = TRUE;
    if (!empty(variable_get('draft_move_request'))) {
      $request = variable_get('draft_move_request');
    } else {
      $file_path = dirname(__FILE__, 4) . '/tests/data/draft_move_request.json';
      $file_content = file_get_contents($file_path);
      $request = drupal_json_decode($file_content);
    }

    $basicSettings = json_decode(variable_get('basicsettings', ''));
    $calculateSettings = json_decode(variable_get('calcsettings', ''));

    // Prepare data for create draft move request.
    $move_date = $this->prepareDraftMoveDate();
    $request['data']['field_move_service_type'] = $this->setAvailableRequestServiceType($basicSettings);

    $request['data']['field_date']['date'] = $move_date;
    $user_email = static::generateUserData();
    $request['data']['field_e_mail'] = $user_email;
    $request['account']['name'] = $user_email;
    $request['account']['mail'] = $user_email;

    $company_parking_zip = $this->getCompanyParkingZip();
    $request['data']['field_moving_to']['postal_code'] = $company_parking_zip['company_parking_zip'];
    $request['data']['field_moving_to']['locality'] = $company_parking_zip['storage_city'];
    $request['data']['field_moving_to']['administrative_area'] = $company_parking_zip['main_state'];
    $request['data']['field_moving_from']['postal_code'] = $company_parking_zip['company_parking_zip'];
    $request['data']['field_moving_from']['locality'] = $company_parking_zip['storage_city'];
    $request['data']['field_moving_from']['administrative_area'] = $company_parking_zip['main_state'];
    $request['data']['field_reservation_price'] = (new CalculateReservationPrice($move_date, $request['data']['field_move_service_type']))->buildReservationPriceForRequest();

    $distance = new DistanceCalculate(TRUE);
    $travel_data = $distance->checkTravelTime($company_parking_zip['company_parking_zip'], $company_parking_zip['company_parking_zip'], $request['data']['field_move_service_type'], TRUE);
    $request['data']['field_travel_time'] = $travel_data['duration'];
    $request['data']['field_distance'] = $travel_data['distances']['AB']['distance'];
    $request['data']['field_duration'] = $travel_data['distances']['AB']['duration'];
    $request['data']['field_request_settings'] = array('inventoryVersion' => 2, 'storage_rate' => $basicSettings->storage_rate);
    $request['all_data']['request_distance'] = $travel_data;
    $request['all_data']['localdiscount'] = $distance->getLocalDiscountByServiceType($request['data']['field_move_service_type']);
    $request['all_data']['travelTime'] = $calculateSettings->travelTime ?? TRUE;
    $request['all_data']['surcharge_fuel'] = 0;
    $request['data']['field_price_per_hour'] = (new MoveRequestRate())->getRateWithoutRequest($move_date);

    if (isset($request['data']['field_approve']) && isset($basicSettings->showQuote->{$request['data']['field_approve']})) {
      $request['all_data']['isDisplayQuoteEye'] = !$basicSettings->showQuote->{$request['data']['field_approve']};
      $request['all_data']['showQuote'] = $basicSettings->showQuote->{$request['data']['field_approve']};
    }

    $request['data']['field_double_travel_time'] = $distance->doubleTravelTime($request['data']);

    if (empty(variable_get('draft_move_request'))) {
      variable_set('draft_move_request', $request);
    }
    $nid = $this->create2($request, 1);

    return $nid['nid'];
  }

  /**
   * Check available service type from company settings.
   *
   * @param object $basicSettings
   *   Company basic settings.
   *
   * @return int
   *   Service type.
   */
  private function setAvailableRequestServiceType($basicSettings) : int {
    if (!empty($basicSettings->services->localMoveOn)) {
      $service_type = 1;
    }
    elseif(!empty($basicSettings->isflat_rate_miles)) {
      $service_type = 5;
    }
    elseif(!empty($basicSettings->islong_distance_miles)) {
      $service_type = 7;
    }
    elseif(!empty($basicSettings->services->loadingHelpOn)) {
      $service_type = 3;
    }
    elseif(!empty($basicSettings->services->unloadingHelpOn)) {
      $service_type = 4;
    }
    elseif(!empty($basicSettings->services->overnightStorageOn)) {
      $service_type = 6;
    }
    elseif(!empty($basicSettings->services->packingDayOn)) {
      $service_type = 8;
    }

    return $service_type ?? 1;
  }

  /**
   * Prepare date for draft move request.
   *
   * @return string
   *   Move date.
   *
   * @throws \Exception
   */
  private function prepareDraftMoveDate() : string {
    $date = new \DateTime(date('Y-m-d'));
    $date->add(new \DateInterval('P10D'));
    $move_date = $date->format('Y-m-d');
    $blocked_day = TRUE;
    $i = 0;
    // Loop to get first available date.
    while ($blocked_day) {
      // Protect from never ended loop.
      if ($i == 100) {
        break;
      }
      // Will add plus one day until find available.
      if ($blocked_day = $this->checkBlockedMoveDate($move_date)) {
        $date->modify('+1 day');
        $move_date = $date->format('Y-m-d');
      }
      $i++;
    }

    return $move_date;
  }

  public static function generateUserData() {
    return rand(1, 100) . 'user_' . time() . '@draft.www';
  }

  private function getCompanyParkingZip() {
    $basic_settings = json_decode(variable_get('basicsettings'));
    $company_address = array(
      'company_parking_zip' => $basic_settings->parking_address,
      'main_state' => $basic_settings->main_state,
      'storage_city' => $basic_settings->storage_city,
    );

    return $company_address;
  }

  /**
   * Check if move date is blocked.
   *
   * @param string $date
   *   Date string "2017-12-31".
   *
   * @return bool
   *   TRUE if blocked, FALSE if not.
   */
  function checkBlockedMoveDate(string $date) : bool {
    $timestamp = Extra::convertDateToTimestamp($date);
    $result = db_select('price_calendar_new', 'pcn')
      ->fields('pcn', array('type'))
      ->condition('date', $timestamp)
      // Type 5 - is blocked day in calendar.
      ->condition('type', 5)
      ->execute()
      ->rowCount();
    return ($result) ? TRUE : FALSE;
  }

  /**
   * Method for change move request client.
   *
   * @param int $uid
   *   User id.
   *
   * @return int|array
   *   Request id or error.
   *
   * @throws \Exception
   */
  public function changeRequestClient(int $uid) {
    $change = FALSE;
    $result = array(
      'status_message' => "User id is empty",
    );
    if (!empty($uid)) {
      $user_data = user_load($uid);
      if ($user_data && property_exists($user_data, 'mail') && $user_data->mail) {
        $data = array(
          'author' => $uid,
          'field_e_mail' => $user_data->mail,
          'field_first_name' => $user_data->field_user_first_name['und'][0]['value'] ?? '',
          'field_last_name' => $user_data->field_user_last_name['und'][0]['value'] ?? '',
          'field_phone' => $user_data->field_primary_phone['und'][0]['value'] ?? '',
          'field_additional_phone' => $user_data->field_user_additional_phone['und'][0]['value'] ?? '',
        );

        $result = $this->update2($data);
        $change = TRUE;
      }
      else {
        $result = array(
          'status' => FALSE,
          'status_code' => 406,
          'status_message' => "User {$uid} does not exist or user email is empty.",
        );
      }

      $this->createChangeUserLog($user_data, $change);
    }

    return $result;
  }

  /**
   * Prepare data log with old user and new user.
   *
   * @param $user_data
   *   User data.
   * @return array
   *   Prepared array users data.
   */
  private function prepareClientDataForLog($user_data) {
    $client_data = array(
      'old' => array(
        'uid' => $this->data['uid']['uid'],
        'first_name' => $this->data['first_name'],
        'last_name' => $this->data['last_name'],
        'email' => $this->data['email'],
        'phone' => $this->data['phone'],
      ),
      'new' => array(
        'uid' => $user_data->uid,
        'first_name' => $user_data->field_user_first_name['und'][0]['value'],
        'last_name' => $user_data->field_user_last_name['und'][0]['value'],
        'email' => $user_data->mail,
        'phone' => $user_data->field_primary_phone['und'][0]['value'],
      )
    );

    return $client_data;
  }

  /**
   * Method for write user log in request about change users.
   *
   * @param object $user_data
   *   User data.
   * @param bool $change
   *   Change user TRUE or FALSE.
   */
  private function createChangeUserLog($user_data, $change) {
    global $user;
    $title = $change ? 'Request client was updated' : 'Update Request client error.';
    $client = $this->prepareClientDataForLog($user_data);
    $log_text = array();
    $log_text[]['simpleText'] = "Client {$client['old']['uid']} was changed from {$client['old']['first_name']} {$client['old']['last_name']} {$client['old']['email']} {$client['old']['phone']}";
    $log_text[]['simpleText'] = "To {$client['new']['uid']} {$client['new']['first_name']} {$client['new']['last_name']} {$client['new']['email']} {$client['new']['phone']}";

    $user_name = isset($user->name) ? $user->name : 'System';

    $log_data[] = array(
      'details' => array([
        'event_type' => 'Change client',
        'activity' => $user_name,
        'title' => ucfirst(preg_replace('/_/', ' ', $title)),
        'text' => $log_text,
        'date' => time(),
      ],
      ),
      'source' => $user_name,
    );
    $log = new Log($this->nid, EntityTypes::MOVEREQUEST);
    $log->create($log_data);
  }

  /**
   * Method for set additional user to move request.
   */
  public function setAdditionalUser($account) : array {
    $result = array('nid' => $this->nid, 'uid' => NULL, 'node_update' => FALSE);
    $data['account'] = $account;
    $user = (new Clients())->create($data);
    if (!empty($user)) {
      $request['field_additional_user'][] = $user->uid;
      $node_update = $this->update2($request);
      $result['uid'] = $user->uid;
      $result['node_update'] = $node_update;
    }

    return $result;
  }

  public function getAdditionalUser() : array {
    $result = array();
    if (!empty($this->data['field_additional_user']) && !empty($this->data['send_additional_email'])) {
      $result = $this->data['field_additional_user'];
    }

    return $result;
  }

  // TODO. Change hardcode 11
  /**
   * Update "CommercialMoveSettings".
   *
   * Update "field_size_of_move" default values. Update variable.
   *
   * @param array $data
   * @return bool
   */
  public function setCommercialMoveSettings($data) {
    if (!empty($data)) {
      try {
        $values = drupal_json_decode($data);
        if (!empty($values['name'])) {
          $field_info = field_info_field('field_size_of_move');
          $list_values = list_allowed_values($field_info);
          if (empty($list_values[11]) || $list_values[11] != $values['name']) {
            $list_values[11] = $values['name'];
            $field_info['settings']['allowed_values'] = $list_values;
            field_update_field($field_info);
          }
        }
      }
      catch (\Throwable $e) {
        $message = "setCommercialMoveSettings Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
        watchdog('setCommercialMoveSettings', $message, array(), WATCHDOG_CRITICAL);
      }
    }

    variable_set("commercial_move_size_setting", $data);
    return TRUE;
  }

  /**
   * Create commercial extra room.
   *
   * @param string $name
   *   Name commercial extra room.
   *
   * @param int $cubic_feet
   *   Weight commercial extra room in c.f.
   *
   * @return bool
   *   If success TRUE or Exception.
   */
  public function createCommercialExtraRoom($name, $cubic_feet) {
    try{
      $record = db_insert('commercial_extra_room')
        ->fields(array(
          'name' => $name,
          'cubic_feet' => $cubic_feet,
        ))
        ->execute();

      if ($record) {
        $field_info = field_info_field('field_commercial_extra_rooms');
        $list_values = list_allowed_values($field_info);
        if (empty($list_values[$record])) {
          $list_values[$record] = $name;
          $field_info['settings']['allowed_values'] = $list_values;
          field_update_field($field_info);
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Create commercial extra room error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('createCommercialExtraRoom', $message, array(), WATCHDOG_CRITICAL);
    }

    return TRUE;
  }

  /**
   * Get all commercials extra rooms.
   *
   * @return mixed
   *   All commercial extra rooms.
   */
  public static function getCommercialExtraRooms() {
    $query = db_select('commercial_extra_room', 'cer')
      ->fields('cer')
      ->execute()
      ->fetchAll();

    return $query;
  }

  /**
   * Update commercial extra room.
   *
   * @return bool
   *   Result db_update.
   */
  public function updateCommercialExtraRoom($room_id, $name, $cubic_feet) {
    try {
      $field_info = field_info_field('field_commercial_extra_rooms');
      $list_values = list_allowed_values($field_info);
      if (!empty($list_values[$room_id])) {
        $list_values[$room_id] = $name;
        $field_info['settings']['allowed_values'] = $list_values;
        $update_field_exception = field_update_field($field_info);
      }
      if (!isset($update_field_exception)) {
        db_update('commercial_extra_room')
          ->fields(array(
            'name' => $name,
            'cubic_feet' => $cubic_feet,
          ))
          ->condition('id', $room_id)
          ->execute();
      }
    }
    catch (\Throwable $e) {
      $message = "Update commercial extra room error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('updateCommercialExtraRoom', $message, array(), WATCHDOG_CRITICAL);
    }

    return TRUE;
  }

  /**
   * Get commercial item cubic feet.
   *
   * @param int $commercial_item_id
   *
   * @return int
   *   Item cubic feet.
   */
  public function getCommercialItemCubicFeet(int $commercial_item_id) : int {
    $commercial_item_cubic_feet = 0;
    $commercial_extra_rooms = MoveRequest::getCommercialExtraRooms();

    foreach ($commercial_extra_rooms as $commercial_extra_room) {
      if ($commercial_extra_room->id == $commercial_item_id) {
        $commercial_item_cubic_feet = $commercial_extra_room->cubic_feet;
      }
    }
    return (int) $commercial_item_cubic_feet;
  }

  /**
   * Delete commercial extra room.
   *
   * @param int $room_id
   *   Room id.
   *
   * @return bool
   *    Result db_delete.
   */
  public function deleteCommercialExtraRoom(int $room_id) {
    try{
      $field_info = field_info_field('field_commercial_extra_rooms');
      $list_values = list_allowed_values($field_info);
      if (!empty($list_values[$room_id])) {
        unset($list_values[$room_id]);
        $field_info['settings']['allowed_values'] = $list_values;
        $update_field_exception = field_update_field($field_info);
      }
      if (!isset($update_field_exception)) {
        db_delete('commercial_extra_room')
          ->condition('id', $room_id)
          ->execute();
      }
    }
    catch (\Throwable $e) {
      $message = "Delete commercial extra room error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('deleteCommercialExtraRoom', $message, array(), WATCHDOG_CRITICAL);
    }

    return TRUE;
  }

  /**
   * Calculate work time, movers count and save it.
   *
   * @param \EntityMetadataWrapper $node_wrapper
   */
  public static function calculateRequestData(\EntityMetadataWrapper $node_wrapper) : void {
    $request_retrieve_fields = (new MoveRequestRetrieve($node_wrapper))->nodeFields();
    $nid = $node_wrapper->nid->value();

    $calculate_work_time_instance = new CalculateWorkTime();
    $work_time = $calculate_work_time_instance->buildWorkTimeFromRequest($request_retrieve_fields);

    $movers_count = $calculate_work_time_instance->movers_count;

    $max_time = MoveRequestRetrieve::getReadableTimeHelper($work_time['max'], TRUE);
    $min_time = MoveRequestRetrieve::getReadableTimeHelper($work_time['min'], TRUE);

    $data_for_update = array(
      'field_minimum_move_time' => array(
        'raw' => 'value',
        'value' => $work_time['min'],
        'data_to_update_in_cache' => array(
          'minimum_time' => array(
            'value' => $min_time,
            'label' => 'Minimum Move Time',
            'field' => 'field_minimum_move_time',
            'raw' => $work_time['min'],
            'old' => $min_time,
          ),
        ),
      ),
      'field_maximum_move_time' => array(
        'raw' => 'value',
        'value' => $work_time['max'],
        'data_to_update_in_cache' => array(
          'maximum_time' => array(
            'value' => $max_time,
            'label' => 'Maximum Move time',
            'field' => 'field_maximum_move_time',
            'raw' => $work_time['max'],
            'old' => $max_time,
          ),
        ),
      ),
      'field_movers_count' => array(
        'raw' => 'value',
        'value' => $movers_count,
        'data_to_update_in_cache' => array(
          'crew' => array(
            'value' => (string) $movers_count,
            'label' => 'Movers Count',
            'field' => 'field_movers_count',
            'old' => (string) $movers_count,
          ),
        ),
      ),
    );

    static::updateNodeFieldsOnly($nid, $data_for_update, 'move_request', FALSE);
  }


  /**
   * Clone request.
   *
   * @return mixed
   *   Custom answer with new nid.
   *
   * @throws \Exception
   */
  public function cloneRequest() {
    $get_room = function ($room_id, $custom_rooms) {
      foreach ($custom_rooms as $rooms) {
        if ($rooms['old_room'] == $room_id) {
          return $rooms['new_room'];
        }
      }
      return NULL;
    };
    // Copy node.
    $node = node_load($this->nid);
    $new_node = $node;
    $new_node->clone = TRUE;
    unset($new_node->nid);
    unset($new_node->vid);
    // Delete list truck.
    /* @var \EntityDrupalWrapper $emw */
    $emw = entity_metadata_wrapper('node', $new_node);
    $emw->field_list_truck->set(NULL);
    $emw->field_approve->set(RequestStatusTypes::PENDING);
    // Unset team for request.
    $emw->field_company_flags->set(NULL);
    $emw->field_foreman->set(NULL);
    $emw->field_foreman_delivery->set(NULL);
    $emw->field_foreman_pickup->set(NULL);
    $emw->field_helper->set(NULL);
    $emw->field_helper_delivery->set(NULL);
    $emw->field_helper_pickup->set(NULL);
    $emw->field_reservation_received->set(0);
    $emw->save();
    unset($emw);
    // Request all data copy.
    $allDataOriginal = static::getRequestAllData($this->nid);
    $allDataOriginal['req_tips'] = 0;
    $allDataOriginal['reservation_receipt'] = 0;

    static::setRequestAllData($new_node->nid, $allDataOriginal);
    Cache::updateCacheData($new_node->nid);

    // Copy old inventory.
    $inventory_retrieve = (new Inventory($this->nid))->retrieve();
    $inventory_retrieve['items'] = $inventory_retrieve['inventory_list'] ??  NULL;
    unset($inventory_retrieve['inventory_list']);
    $inventory_retrieve['stats'] = $inventory_retrieve['statistics'] ?? NULL;
    unset($inventory_retrieve['statistics']);
    $inventory_retrieve['details'] = $inventory_retrieve['move_details'] ?? NULL;
    unset($inventory_retrieve['move_details']);
    (new Inventory($new_node->nid))->create($inventory_retrieve);
    // Copy new inventory.
    $custom_rooms = (new RoomActions())->cloneRoomToOtherRequest($this->nid, $new_node->nid);
    $new_inventory = RequestInventoryModel::getInventoryItems(['entity_id' => $this->nid]);
    if (!empty($new_inventory)) {
      $itemActions = new ItemActions();
      foreach ($new_inventory as $item) {
        $item['entity_id'] = $new_node->nid;
        $item['room_id'] = $get_room($item['room_id'], $custom_rooms) ?? $item['room_id'];
        $data = array_intersect_key($item, array(
          'entity_id' => '',
          'entity_type' => '',
          'item_id' => '',
          'filter_id' => '',
          'room_id' => '',
          'package_id' => '',
          'pac_rel_id' => '',
          'name' => '',
          'count' => '',
          'price' => '',
          'cf' => '',
          'type' => '',
          'type_inventory' => '',
        ));
        $itemActions->addItemToRequest($data);
      }
    }
    // Copy notes.
    $notes = $this->get_notes(array('sales_notes', 'foreman_notes', 'client_notes', 'dispatch_notes'));
    $move_request = new MoveRequest($new_node->nid);
    $move_request->set_notes(array($notes->sales_notes, $notes->foreman_notes, $notes->client_notes, $notes->dispatch_notes));
    unset($move_request);
    // Copy extra service.
    $copy_data = ExtraServices::getExtraServices((int) $this->nid);
    db_insert('move_extra_services')
      ->fields(array(
        'nid' => (int)$new_node->nid,
        'value' => serialize($copy_data),
      ))
      ->execute();

    // Copy request_data.
    $request_data = static::getRequestData($this->nid);
    if (!empty($request_data['value']['crews'])) {
      unset($request_data['value']['crews']);
    }
    static::setRequestData($new_node->nid, $request_data);
    Cache::updateCacheData($new_node->nid, []);
    static::addRequestToSearchApiIndex($new_node->nid);

    return $new_node->nid;
  }

  /**
   * Create packing day request.
   *
   * @return array
   */
  public function createPackingDay() {
    try {
      $new_nid = $this->cloneRequest();
      if (!empty($new_nid)) {
        // Set packing_request_id for new node.
        $request_all_data = static::getRequestAllData($new_nid);
        $request_all_data['additionalInfo'] = array(
          'serviceType' => $this->data['service_type']['value'],
          'status' => $this->data['status']['value'],
          'moveDate' => date('M d, Y' ,$this->data['date']['raw']),
        );
        $request_all_data['packing_request_id'] = $this->nid;

        static::setRequestAllData($new_nid, $request_all_data);
        Cache::updateCacheData($new_nid);

        // Get date.
        $date = db_select('field_data_field_date', 'fdate')
          ->fields('fdate', array('field_date_value'))
          ->condition('fdate.entity_id', $new_nid, '=')
          ->execute()->fetchField();
        $dateTime = new \DateTime($date);
        $dateTime->modify('-1 day');

        // Get row from move_request_sales_person.
        $manager_row = db_select('move_request_sales_person', 'mrsp')
          ->fields('mrsp', array())
          ->condition('mrsp.nid', $this->nid, '=')
          ->execute()->fetchAll(\PDO::FETCH_ASSOC);
        db_insert('move_request_sales_person')
          ->fields(array(
            'nid' => $new_nid,
            'uid' => $manager_row[0]['uid'] ?? 0,
            'main'=> $manager_row[0]['main'] ?? 1,
            'created' => $manager_row[0]['created'] ?? 0,
          ))
          ->execute();
        $data_for_update = array(
          'field_move_service_type' => array(
            'raw' => 'value',
            'value' => RequestServiceType::PACKING_DAY,
            'data_to_update_in_cache' => array(
              'service_type' => array(
                'value' => 'Packing day',
                'label' => 'Service Type',
                'field' => 'field_move_service_type',
                'raw' => (string) RequestServiceType::PACKING_DAY,
                'old' => (string) RequestServiceType::PACKING_DAY,
              ),
            ),
          ),
          'field_date' => array(
            'raw' => 'value',
            'value' => $dateTime->format('Y-m-d H:i:s'),
            'data_to_update_in_cache' => array(
              'date' => array(
                'value' => (string) $dateTime->format('m/d/Y'),
                'label' => 'Date',
                'field' => 'field_date',
                'old' => (string) $dateTime->format('m/d/Y'),
                'raw' => $dateTime->getTimestamp(),
              ),
            ),
          ),
        );
        // Service Type.
        $this->updateNodeFieldsOnly($new_nid, $data_for_update, 'move_request', FALSE);
        Cache::updateCacheData($new_nid);
      }
    }
    catch (\Throwable $exception) {
      $message = "{$exception->getMessage()} in {$exception->getFile()}: {$exception->getLine()}";
      watchdog('Create packing day', $message, array(), WATCHDOG_ERROR);
      return array(
        'status' => 500,
        'statusText' => 'Cannot crate packing day request!',
        'data' => NULL,
      );
    }
    return array(
      'status' => 200,
      'statusText' => 'OK',
      'data' => $new_nid,
    );
  }

  /**
   * Untie packing day.
   *
   * @param int $parentNid
   *   Parent nid.
   * @param int $childNid
   *   Child nid.
   *
   * @return array
   *   Custom answer.
   *
   * @throws \ServicesException
   */
  public function untiePackingDay(int $parentNid, int $childNid) {
    try {
      $request_all_data = static::getRequestAllData($parentNid);
      $request_all_data['packing_request_id'] = NULL;
      static::setRequestAllData($parentNid, $request_all_data);
      Cache::updateCacheData($parentNid);

      $request_all_data = static::getRequestAllData($childNid);
      $request_all_data['packing_request_id'] = NULL;
      static::setRequestAllData($childNid, $request_all_data);
      Cache::updateCacheData($childNid);
    }
    catch (\Throwable $exception) {
      $message = "{$exception->getMessage()} in {$exception->getFile()}: {$exception->getLine()}";
      watchdog('Untie Packing day', $message, array(), WATCHDOG_WARNING);
      services_error('Error message here', 406);
    }

    return array(
      'status' => 200,
      'statusText' => 'OK',
      'data' => NULL,
    );
  }

  /**
   * Function create move request for crate and invoice for them.
   *
   * @param array $data
   * @return array
   *   Invoice hash.
   *
   * @throws \Drupal\move_services_new\Exceptions\ValidationException
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function createCrateMoveRequest($data = array()) {
    $crate_sales_notes = $data['crate_sales_notes'];
    $crate_client_notes = $data['crate_client_notes'];
    $crate_invoice = $data['crate_invoice'];
    unset($data['crate_sales_notes']);
    unset($data['crate_client_notes']);
    unset($data['crate_invoice']);

    $result_create = $this->storageRequestCreate($data);

    $move_request = new self($result_create[0]['nid']);
    $request_data = $move_request->retrieve();

    if ($result_create[0]['nid']) {
      $this->prepareInvoiceDataMoveRequest($request_data, $crate_invoice);
      $invoice = new Invoice(NULL, $result_create[0]['nid'], EntityTypes::MOVEREQUEST);
      $invoice_id = $invoice->create($crate_invoice);

      $move_request = new MoveRequest($result_create[0]['nid']);
      $move_request->set_notes(array($crate_sales_notes, '', $crate_client_notes));

      $move_request = new MoveRequest($result_create[1]['nid']);
      $move_request->set_notes(array($crate_sales_notes, '', $crate_client_notes));

      $invoice = (new Invoice($invoice_id, $result_create[0]['nid'], EntityTypes::MOVEREQUEST))->retrieve();
    }

    return array('hash' => $invoice['hash'] ?? FALSE);
  }

  /**
   * Prepare invoice data after create request for create crate invoice.
   *
   * @param array $request_data
   *   Request retrieve.
   * @param array $crate_invoice
   *   Invoice data.
   */
  private function prepareInvoiceDataMoveRequest($request_data, &$crate_invoice) {
    $crate_invoice['entity_id'] = $crate_invoice['id'] = $request_data['nid'];
    $crate_invoice['entity_type'] = EntityTypes::MOVEREQUEST;
    $crate_invoice['uid'] = $request_data['uid']['uid'];
  }

  /**
   * Add quote and grand total values on hooke presave.
   *
   * @param \stdClass $node
   *   Drupal node object.
   */
  public static function addQuoteAndGrandTotalOnHookPreSave(stdClass &$node) : void {
    $quote_and_grand_total = (new MoveRequestGrandTotal())->setRequestRetrieveByNode($node)->build()->calculate();

    $node->field_grand_total_min[LANGUAGE_NONE][0]['value'] = $quote_and_grand_total->grandTotalMin;
    $node->field_grand_total_max[LANGUAGE_NONE][0]['value'] = $quote_and_grand_total->grandTotalMax;
    $node->field_grand_total_middle[LANGUAGE_NONE][0]['value'] = $quote_and_grand_total->grandTotalMiddle;
    $node->field_grand_total_closing[LANGUAGE_NONE][0]['value'] = $quote_and_grand_total->grandTotalClosing;

    $node->field_quote_min[LANGUAGE_NONE][0]['value'] = $quote_and_grand_total->quoteMin;
    $node->field_quote_max[LANGUAGE_NONE][0]['value'] = $quote_and_grand_total->quoteMax;
    $node->field_quote_closing[LANGUAGE_NONE][0]['value'] = $quote_and_grand_total->quoteClosing;
    $node->field_quote_middle[LANGUAGE_NONE][0]['value'] = $quote_and_grand_total->quoteMiddle;
  }

  /**
   * Get ip and browser.
   *
   * @return array
   */
  public static function getCurrentBrowserAndIp() : array {
    $result['ip'] = $_SERVER['X-Real-IP'] ?? $_SERVER['X-Forwarded-For'] ?? '';

    if (isset($_SERVER['HTTP_USER_AGENT'])) {
      $browser = get_browser(NULL, TRUE);
    }
    $result['browser']['name'] = $browser['browser'] ?? 'Undefined';
    $result['browser']['version'] = $browser['version'] ?? 'Undefined';
    $result['browser']['platform'] = $browser['platform'] ?? 'Undefined';
    $result['browser']['platform_bits'] = $browser['platform_bits'] ?? 'Undefined';
    $result['browser']['browser_bits'] = $browser['browser_bits'] ?? 'Undefined';

    return $result;
  }


  /**
   * Set move request 'field_allow_to_pending_info' value from settings.
   *
   * @param array $request
   * @return array
   */
  public function setAllowPendingInfo(array $request = array()) {
    $request['field_allow_to_pending_info'] = variable_get('allowToPendingInfo', 1);
    return $request;
  }

  /**
   * This is test function to get diff between 'field_approve' in database in field 'status' in cache.
   *
   * TODO remove it after DEBUG.
   *
   * @param $nid
   * @param $cache_data
   */
  public static function testDiffFieldStatusFromDrupalFieldAndCache($nid, $cache_data) {
    $db_status_field = db_select('field_data_field_approve', 'status')
      ->fields('status', ['field_approve_value'])
      ->condition('entity_id', $nid)
      ->execute()
      ->fetchField();

    $cache_status_field = $cache_data['status']['raw'];

    if ($db_status_field != $cache_status_field) {
      watchdog('Cache field status not equal db field approve',  "<pre>" . print_r(debug_backtrace(), TRUE) . "</pre>", [], WATCHDOG_CRITICAL);
    }
  }

}
