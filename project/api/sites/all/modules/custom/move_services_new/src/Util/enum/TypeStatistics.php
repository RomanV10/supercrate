<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TypeStatistics.
 *
 * @package Drupal\move_services_new\Util
 */
class TypeStatistics extends BaseEnum {

  const USER_VISIT = 0;
  const REQUEST_COUNT = 1;
  const GRAND_TOTAL = 2;
  const PROFIT_AND_LOSE = 3;
  const SALARY = 4;
  const PAYMENT_METHOD = 5;
  const SOURCE = 6;
  const USER_CREATE_REQUEST = 7;
  const USER_IN_ACCOUNT = 8;
  const RESERVATION_TYPE = 9;
  const USER_TIME_ON_SITE = 10;
  const USER_VIEW_PAGE_COUNT = 11;
  const USER_CLICK_FORM = 12;

}
