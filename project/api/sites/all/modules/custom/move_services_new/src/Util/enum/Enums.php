<?php

namespace Drupal\move_services_new\Util\enum;

/**
 * Class Enums.
 *
 * @package Drupal\move_services_new\Util
 */
class Enums {

  public static function getEnums() {
    $enums = &drupal_static(__CLASS__, array());

    if (!$enums) {
      $spls = array(
        array('entities' => new EntityTypes()),
        array('roles' => new Roles()),
        array('payroll_rate_commision' => new RateCommission()),
        array('payroll_manager_rate' => new ManagerRateCommission()),
        array('payroll_form_payment' => new FormPayment()),
        array('payroll_type_payment' => new TypePayment()),
        array('payroll_amount_options' => new AmountOption()),
        array('payroll_jobs_type' => new PayCheckJobsType()),
        array('payroll_type_commission' => new TypeCommission()),
        array('trip_type' => new TripType()),
        array('trip_status' => new TripStatus()),
        array('invoice_flags' => new InvoiceFlags()),
        array('profit_lose_frequency' => new FrequencyType()),
        array('request_storage_flags' => new RequestStorageFlags()),
        array('request_storage_time_period' => new RequestStorageTimePeriod()),
        array('request_storage_type_charge' => new RequestStorageTypeCharge()),
        array('template_type' => new TemplateType()),
        array('template_block_type' => new TemplateBlockType()),
        array('parser_providers' => new Providers()),
        array('type_statistics' => new TypeStatistics()),
        array('form_names' => new FormNames()),
        array('browser_types' => new BrowserType()),
        array('request_service_types' => new RequestServiceType()),
        array('sending_request_branch_setting' => new SendingRequestBranchSettings()),
        array('contract_type' => new ContractType()),
        array('notification_types' => new NotificationTypes()),
        array('credit_card' => new CreditCard()),
        array('payment_flag' => new PaymentFlag()),
        array('payment_method' => new PaymentMethod()),
        array('roles' => new Roles()),
        array('valuation_types' => new ValuationTypes()),
        array('sms_notification_types' => new SmsActionTypes()),
        array('sms_notification_folders' => new SmsNotificationFolder()),
      );

      // For some reason loop foreach does not work.
      for ($i = 0; $i < count($spls); $i++) {
        $key = key($spls[$i]);
        $enums[$key] = $spls[$i][$key]->getConstants();
      }
    }

    return $enums;
  }

}
