<?php

namespace Drupal\move_services_new\Cron\Queues;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class SendReminderNotChanged2Days.
 *
 * @package Drupal\move_services_new\Cron\Queues
 */
class SendReminderNotChanged2Days implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \ServicesException
   */
  public static function execute($data): void {
    $type = 'reminder_notconfirmed';
    $template_builder = new TemplateBuilder();
    foreach ($data as $key => $nid) {
      $node = node_load($nid);
      $wrapper = entity_metadata_wrapper('node', $node);
      $move_field_to_storage_move = $wrapper->field_to_storage_move->raw();
      // Check if this email with this mark was sent.
      $was_sent = MoveRequest::intervalEmailWasSent($nid, $type);
      // Flat Rate Reminder.
      if (!isset($move_field_to_storage_move) && empty($was_sent)) {
        $is_send = $template_builder->moveTemplateBuilderSendTemplateRules($type, $node);
      }
      if (!$was_sent && !empty($is_send[$type]['send'])) {
        MoveRequest::markSentEmailByInterval($node, $type);
      }
    }
  }

}
