<?php

namespace Drupal\move_services_new\System\Resources;

use Drupal\move_services_new\Services\Settings;

class SettingsDefinition {

  public static function getDefinition() {
    return array(
      'settings' => array(
        'operations' => array(),
        'actions' => array(
          'price_table_get' => array(
            'help' => 'Get price table',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::priceTableGet',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'price_table_set' => array(
            'help' => 'Set price table',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::priceTableSet',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of price table',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'storage_table_set' => array(
            'help' => 'Set price storage table',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::storageTableSet',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of price table',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'storage_table_get' => array(
            'help' => 'Get price storage table',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::storageTableGet',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'trucks_get' => array(
            'help' => 'Get trucks',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::trucksGet',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'conditions',
                'type' => 'array',
                'description' => 'The conditions to select only trucks or trailers, or both.',
                'source' => 'data',
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'truck_edit' => array(
            'help' => 'Edit truck',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::trucksEdit',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of taxonomy trucks term.',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'truck_remove' => array(
            'help' => 'Remove truck',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::truckRemove',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'tid',
                'optional' => FALSE,
                'source' => array('path' => 1),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'truck_insert' => array(
            'help' => 'Insert new truck',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::truckInsert',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of new taxonomy trucks term.',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'truck_unavailable' => array(
            'help' => 'Add unavailable date for truck',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::truckUnavailable',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'tid',
                'type' => 'int',
                'source' => array('data' => 'tid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date',
                'type' => 'array',
                'description' => 'Date unavailable truck.',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'price_calendar_types' => array(
            'help' => 'Get Price Calendar types.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::priceCalendarTypes',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'price_calendar' => array(
            'help' => 'Get Price Calendar.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::priceCalendar',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update_price_calendar' => array(
            'help' => 'Update existing data.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::updatePriceCalendar',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'set_settings' => array(
            'help' => 'Set base settings.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::setSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_settings' => array(
            'help' => 'Get base settings.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::getSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'access arguments' => array('access content'),
          ),
          'get_default_settings' => array(
            'help' => 'Get base settings.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::getDefaultSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'restore_default_settings' => array(
            'help' => 'Get base settings.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::restoreDefaultSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_available_variable' => array(
            'help'   => 'Returns the value of a system variable using variable_get().',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::getAvailableVariable',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'name',
                'optional' => FALSE,
                'source' => array('data' => 'name'),
                'description' => t('The name of the variable to return.'),
                'type' => 'string',
              ),
              array(
                'name' => 'default',
                'optional' => TRUE,
                'source' => array('data' => 'default'),
                'description' => t('The default value to use if this variable has never been set.'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('get a system variable'),
            'access arguments append' => FALSE,
          ),
          'get_transport_smtp_settings' => array(
            'help' => 'Get Transport smtp mail settings.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::getTransportSmtpSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'set_variable' => array(
            'help'   => 'Sets the value of a system variable using variable_set().',
            'file' => array('type' => 'inc', 'module' => 'services', 'name' => 'resources/system_resource'),
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::variableSet',
            'access arguments' => array('set a system variable'),
            'access arguments append' => FALSE,
            'args' => array(
              array(
                'name' => 'name',
                'optional' => FALSE,
                'source' => array('data' => 'name'),
                'description' => t('The name of the variable to set.'),
                'type' => 'string',
              ),
              array(
                'name' => 'value',
                'optional' => FALSE,
                'source' => array('data' => 'value'),
                'description' => t('The value to set.'),
                'type' => 'string',
              ),
            ),
          ),
          'set_valuation_plan' => array(
            'help' => 'Save valuation plan.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::setValuationPlan',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_valuation_plan' => array(
            'help' => 'Get valuation plan.',
            'callback' => 'Drupal\move_services_new\System\Resources\SettingsDefinition::getValuationPlan',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/SettingsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function priceTableGet() {
    $table = new Settings();
    return $table->getPriceTable();
  }

  public static function priceTableSet($data = array()) {
    $table = new Settings();
    $table->setPriceTable($data);
  }

  public static function storageTableGet() {
    $table = new Settings();
  //  return $table->getStorageTable();
  }

  public static function storageTableSet($data = array()) {
    $table = new Settings();
    $table->setStorageTable($data);
  }

  public static function trucksGet($conditions = array()) {
    $table = new Settings();
    return $table->getTrucks($conditions);
  }

  public static function trucksEdit($data = array()) {
    if (isset($data['tid']) && isset($data['name'])) {
      $settings = new Settings();
      return $settings->editTermTruck($data);
    }
  }

  public static function truckUnavailable($tid, $date) {
    if ($tid && $date) {
      return Settings::dateUnavailableTruck((int) $tid, (array) $date);
    }
  }

  public static function truckRemove($tid) {
    if ($tid) {
      $settings = new Settings();
      return $settings->removeTruck($tid);
    }
  }

  public static function truckInsert($data) {
    if (isset($data['name'])) {
      $settings = new Settings();
      return $settings->insertNewTruck($data);
    }
  }

  public static function priceCalendar() {
    return Settings::getPriceCalendar();
  }

  public static function priceCalendarTypes() {
    return Settings::getPriceTypes();
  }

  public static function updatePriceCalendar($data) {
    return Settings::setPriceCalendarData($data);
  }

  public static function setSettings($name, $data) {
    return Settings::setSettings($name, $data);
  }

  public static function getSettings() {
    return Settings::getSettings();
  }

  public static function getDefaultSettings() {
    return Settings::getDefaultSettings();
  }

  public static function restoreDefaultSettings() {
    Settings::restoreSetting();
  }

  public static function getAvailableVariable($name, $default) {
    $variable = new Settings();
    return $variable->getAvailableVariable($name, $default);
  }

  public static function getTransportSmtpSettings() {
    $variable = new Settings();
    return $variable->getTransportSmtpSettings();
  }

  /**
   * Custom service for set variable.
   *
   * @param string $name
   *   Variable name.
   * @param string $value
   *   Variable value.
   *
   * @return \DatabaseStatementInterface|int
   *   Result db_merge.
   *
   * @throws \Exception
   */
  public static function variableSet(string $name, string $value) {
    $variable = new Settings();
    return $variable->variableSet($name, $value);
  }

  /**
   * Save valuation plan.
   *
   * @param array $data
   *   Data for save.
   *
   * @return array
   *   Custom answer.
   *
   * @throws \ServicesException
   */
  public static function setValuationPlan(array $data) {
    try {
      (new Settings())->saveValuationPlan($data);
      return [
        'status_code' => 200,
        'status_message' => 'OK',
        'data' => TRUE,
      ];
    }
    catch (\Throwable $exception) {
      $message = "valuationPlanSave: {$exception->getMessage()} in {$exception->getFile()}: {$exception->getLine()} </br> Trace: {$exception->getTraceAsString()}";
      watchdog('Valuation plan save', $message, array(), WATCHDOG_CRITICAL);
      return services_error($exception->getMessage(), $exception->getCode());
    }
  }

  /**
   * Get valuation plan.
   *
   * @param array $data
   *   Who plan needed.
   *
   * @return array|mixed
   *   Plan.
   *
   * @throws \ServicesException
   */
  public static function getValuationPlan(array $data) {
    try {
      return [
        'status_code' => 200,
        'status_message' => 'OK',
        'data' => (new Settings())->getValuationPlan($data),
      ];
    }
    catch (\Throwable $exception) {
      $message = "valuationPlanSave: {$exception->getMessage()} in {$exception->getFile()}: {$exception->getLine()} </br> Trace: {$exception->getTraceAsString()}";
      watchdog('Valuation plan save', $message, array(), WATCHDOG_CRITICAL);
      return services_error($exception->getMessage(), $exception->getCode());
    }
  }

}
