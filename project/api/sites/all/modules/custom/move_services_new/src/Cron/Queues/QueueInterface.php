<?php

namespace Drupal\move_services_new\Cron\Queues;

/**
 * Interface QueueInterface.
 *
 * @package Drupal\move_services_new\Cron\Queues
 */
interface QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   */
  public static function execute($data) : void;

}
