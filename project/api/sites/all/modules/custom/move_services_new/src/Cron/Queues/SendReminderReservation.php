<?php

namespace Drupal\move_services_new\Cron\Queues;

use Drupal\move_services_new\Util\enum\SmsActionTypes;
use Drupal\move_sms\Services\Sms;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class SendReminderReservation.
 *
 * @package Drupal\move_services_new\Cron\Queues
 */
class SendReminderReservation implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute($data): void {
    $type = 'please_confirm';
    $template_builder = new TemplateBuilder();
    foreach ($data as $key => $nid) {
      $node = node_load($nid);
      if (!empty($node)) {
        $template_builder->moveTemplateBuilderSendTemplateRules($type, $node);
        (new Sms(SmsActionTypes::REMINDER_RESERVATION))->execute($node->nid, $node, $node->uid);
      }
    }
  }

}
