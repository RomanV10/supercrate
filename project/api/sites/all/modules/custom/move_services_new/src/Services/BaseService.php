<?php

namespace Drupal\move_services_new\Services;

/**
 * Class BaseService.
 */
abstract class BaseService {

  /**
   * Create service.
   *
   * @param array $data
   *   Data for create entity.
   *
   * @return mixed
   *   Result of operation.
   */
  abstract protected function create($data = array());

  /**
   * Retrieve service.
   *
   * @return mixed
   *   Result of operation.
   */
  abstract protected function retrieve();

  /**
   * Service for update entity.
   *
   * @param array $data
   *   Data for update entity.
   *
   * @return mixed
   *   Result of operation.
   */
  abstract protected function update($data = array());

  /**
   * Service for remove entity.
   *
   * @return mixed
   *   Result of operation.
   */
  abstract protected function delete();

  /**
   * Helper function to build index queries.
   *
   * @param object $query
   *   SelectQuery object.
   * @param int $page
   *   Page number.
   * @param int $page_size
   *   Page size.
   * @param string $fields
   *   List of fields comma separated.
   * @param string $resource
   *   The name of resource.
   * @param string $alias
   *   Table alias.
   * @param array $conditions
   *   Lists of conditions. Important format: $key => $value
   *   where $key = name of field.
   */
  static protected function buildIndexQuery($query, $page, $page_size, $fields, $resource, $alias, $conditions = array()) {
    $default_limit = variable_get("services_{$resource}_index_page_size", 20);
    if (!user_access('perform unlimited index queries') && $page_size > $default_limit) {
      $page_size = $default_limit;
    }
    $query->range($page * $page_size, $page_size);
    if ($fields == '*') {
      $query->fields($alias);
    }
    else {
      $query->fields($alias, explode(',', $fields));
    }
    if ($conditions) {
      foreach ($conditions as $key => $value) {
        $query->condition($alias . '.' . $key, $value);
      }
    }
  }

  /**
   * Helper function to execute a index query.
   *
   * @param \SelectQuery $query
   *   Object of SelectQuery type.
   *
   * @return mixed
   *   Executed query object.
   *
   * @throws \ServicesException
   */
  static protected function executeIndexQuery(\SelectQuery $query) {
    try {
      return $query->execute();
    }
    catch (\Throwable $e) {
      return services_error(t('Invalid query provided, double check that the fields and parameters you defined are correct and exist. @text', array('@text' => $e->getMessage())), 406);
    }
  }

  /**
   * Helper function to build a list of items satisfying the index query.
   */
  static protected function buildIndexList($results) {
    // Put together array of matching items to return.
    $items = array();
    foreach ($results as $result) {
      $items[] = $result;
    }

    return $items;
  }

}
