<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class EntityTypes.
 *
 * @package Drupal\move_services_new\Util
 */
class EntityTypes extends BaseEnum {

  const MOVEREQUEST = 0;
  const STORAGEREQUEST = 1;
  const LDREQUEST = 2;
  const STORAGE = 3;
  const RECEIPT = 4;
  const COUPON = 5;
  const SYSTEM = 6;
  const LDCARRIER = 7;
  const LDTRIP = 8;
  const LDJOB = 9;
  const LDSIT = 10;
  const LDTDJOB = 11;
  const NOTIFICATION = 20;

}
