<?php

namespace Drupal\move_services_new\Services\FieldStrategy;

/**
 * Class FieldEmailStrategy.
 *
 * @package Drupal\move_services_new\Services\FieldStrategy
 */
class FieldEmailStrategy implements FieldArrayStrategyInterface {

  /**
   * Create data.
   *
   * @param mixed $values
   *   Value os field.
   * @param string $widget_type
   *   The widget type.
   * @param int $count_values
   *   Count values.
   * @param bool $multi
   *   Indicated that field is multi.
   * @param array $field_info
   *   Information about field.
   *
   * @return mixed
   *   Result
   */
  public function createData($values, string $widget_type, int $count_values, bool $multi = FALSE, array $field_info = array()) {
    $data = array();
    if ($multi) {
      for ($i = 0; $i < $count_values; $i++) {
        $data[LANGUAGE_NONE][$i]['email'] = $values[$i];
      }
    }
    else {
      $data[LANGUAGE_NONE][0]['email'] = $values;
    }

    return $data;
  }

}
