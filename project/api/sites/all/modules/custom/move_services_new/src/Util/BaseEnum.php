<?php

namespace Drupal\move_services_new\Util;

/**
 * Class BaseEnum.
 *
 * @package Drupal\move_services_new\Util
 */
abstract class BaseEnum {

  /**
   * Cache reflection request.
   *
   * @var array
   */
  protected static $constCache = [];
  public static $available = [];

  /**
   * Get class constant array. key = constant name, value = value.
   *
   * @return array
   */
  public static function getConstants() {
    if (empty(static::$constCache[get_called_class()])) {
      $reflect = new \ReflectionClass(get_called_class());
      static::$constCache[get_called_class()] = $reflect->getConstants();
    }

    return static::$constCache[get_called_class()];
  }

  public static function getNameByValue($name) {
    $result = FALSE;
    $constants = static::getConstants();
    $check = static::isValidName($name);
    if ($check) {
      $keys = array_map('strtolower', array_keys($constants));
      $result = array_search($name, $keys);
    }

    return $result;
  }

  public static function getKeyByValue($constant_value) {
    $constants = static::getConstants();
    $result = array_search($constant_value, $constants);

    return strtolower($result);
  }

  /**
   * Is there constant with name.
   *
   * @param string $name
   * @param boolean $strict
   *
   * @return boolean
   */
  public static function isValidName($name, $strict = FALSE) {
    $constants = static::getConstants();

    if ($strict) {
      return array_key_exists($name, $constants);
    }

    $keys = array_map('strtolower', array_keys($constants));
    return in_array(strtolower($name), $keys);
  }

  /**
   * Is there cinstant with this value.
   *
   * @param string $value
   *
   * @return boolean
   */
  public static function isValidValue($value) {
    $values = array_values(static::getConstants());
    return in_array($value, $values, $strict = TRUE);
  }

  public static function isAvailable($name) {
    return in_array($name, static::$available);
  }

  public static function getAvailable() {
    return static::$available;
  }

  public static function setAvailable($available) {
    static::$available = $available;
  }

}
