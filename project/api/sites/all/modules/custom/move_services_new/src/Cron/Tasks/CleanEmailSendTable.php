<?php

namespace Drupal\move_services_new\Cron\Tasks;

/**
 * Class CleanEmailSendTable.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
class CleanEmailSendTable implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $date = strtotime("-7 day");
    db_delete('move_services_email_was_sent')
      ->condition('time', $date, '<=  ')
      ->execute();
  }

}
