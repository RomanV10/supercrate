<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class Roles.
 *
 * @package Drupal\move_services_new\Util
 */
class Roles extends BaseEnum {

  const DEFAULT = 0;
  const MANAGER = 1;
  const HELPER = 2;
  const FOREMAN = 3;
  const DRIVER = 4;
  const SALES = 5;
  const OWNER = 6;
  const CUSTOMER_SERVICE = 7;
  const ANONYMOUS = 8;
  const CUSTOMER = 9;
  const ADMINISTRATOR = 10;

}
