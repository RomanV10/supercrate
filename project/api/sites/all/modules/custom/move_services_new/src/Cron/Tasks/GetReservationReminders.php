<?php

namespace Drupal\move_services_new\Cron\Tasks;

use Drupal\move_services_new\Services\move_request\MoveRequestSearch;

/**
 * Class GetReservationReminders.
 *
 * @package Drupal\move_services_new\Cron
 */
class GetReservationReminders implements CronInterface {

  /**
   * Execute task by cron.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public static function execute(): void {
    $requests = (new MoveRequestSearch())->reservationRequests();

    if (!empty($requests)) {
      $queue = \DrupalQueue::get('move_services_new_send_reminder_reservation');
      $queue->createQueue();
      $chunk = array_chunk($requests, 3);
      foreach ($chunk as $item) {
        $queue->createItem($item);
      }
    }
  }

}
