<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RequestSearchTypes.
 *
 * @package Drupal\move_storage\Services
 */
class RequestSearchTypes extends BaseEnum {

  const FULL = 1;
  const SHORT = 2;
  const TRIP = 3;
  const HOME_ESTIMATE = 4;
  const QUICK_BOOKS = 5;
  const PROFIT_LOSE = 6;

}
