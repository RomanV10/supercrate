<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_branch\Services\Trucks;

/**
 * Class Settings.
 *
 * @package Drupal\move_services_new\Services
 */
class Settings extends BaseService {
  public function create($data = array()) {}

  public function update($data = array()) {}

  public function delete() {}

  public function retrieve() {}

  public function getPriceTable() {
    $trucks = (array) variable_get('price_table_trucs', array());
    $types = (array) variable_get('price_table_types', array());
    $result = (array) variable_get('price_table', array());

    if (!$result) {
      foreach ($types as $type_name) {
        foreach ($trucks as $truck_key => $truck_name) {
          $result[$type_name][$truck_name] = '';
        }
      }
    }

    return $result;
  }

  public static function getPriceTableArray() {
    $result = array();
    $price = variable_get('price_table', array());

    foreach ($price as $price_key => $price_array) {
      $result[] = $price[$price_key];
    }

    return $result;
  }

  public function setPriceTable($data = array()) {
    $trucks = (array) variable_get('price_table_trucs', array());
    $types = (array) variable_get('price_table_types', array());
    $price = (array) variable_get('price_table', array());

    foreach ($types as $type_name) {
      if (isset($data[$type_name])) {
        foreach ($trucks as $truck_key => $truck_name) {
          if (isset($data[$type_name][$truck_key])) {
            $price[$type_name][$truck_key] = (int) $data[$type_name][$truck_key];
          }
        }
      }
    }

    variable_set('price_table', $price);
  }

  public function setStorageTable($data = array()) {
    $storage_size = (array) variable_get('price_table_storage_size', array());
    $storage = (array) variable_get('price_table_storage', array());
    $storage_price = (array) variable_get('price_storage', array());

    foreach ($storage_size as $storage_size_name) {
      if (isset($data[$storage_size_name])) {
        foreach ($storage as $storage_key => $storage_name) {
          if (isset($data[$storage_size_name][$storage_key])) {
            $storage_price[$storage_size_name][$storage_key] = (int) $data[$storage_size_name][$storage_key];
          }
        }
      }
    }

    variable_set('price_storage', $storage_price);
  }

  public function getStorageTable() {
    $storage_size = (array) variable_get('price_table_storage_size', array());
    $storage = (array) variable_get('price_table_storage', array());
    $storage_price = (array) variable_get('price_storage', array());
    $result = array();

    foreach ($storage_size as $size_name) {
      foreach ($storage as $storage_key => $storage_name) {
        $result[$size_name][$storage_name] = $storage_price[$size_name][$storage_key];
      }
    }

    return $result;
  }

  public function getTrucks($field_conditions = array()) {
    $trucks = array();
    $entity_type = 'taxonomy_term';
    $vocabulary = taxonomy_vocabulary_machine_name_load('trucks');
    $condition['vid'] = $vocabulary->vid;
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', $entity_type);
    $query->propertyCondition('vid', $vocabulary->vid);
    $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');
    if (!empty($field_conditions)) {
      foreach ($field_conditions as $field_name => $val) {
        if (!empty($field_name)) {
          $query->fieldCondition($field_name, 'value', $val);
        }
      }
    }
    $result = $query->execute();
    if (!empty($result)) {
      $terms = entity_load($entity_type, array_keys($result[$entity_type]));
      $terms_unavailble_db = db_select('truck_unavailable', 'tu')
        ->fields('tu')
        ->condition('tu.tid', array_keys($terms))
        ->execute()
        ->fetchAll(\PDO::FETCH_ASSOC);

      foreach ($terms_unavailble_db as $un) {
        $type = (int) $un['type'];
        if ($type === 0) {
          $terms_unavailble_manually[$un['tid']][] = $un['date'];
        }
        else {
          $terms_unavailble_from_request[$un['tid']][] = $un['date'];
        }
      }

      $trucksBranches = Trucks::getTrucksBranches();
      foreach ($terms as $term) {
        // You can`t see branch truck.
        if (array_search($term->tid, $trucksBranches) !== FALSE) {
          continue;
        }
        $un_date = (isset($terms_unavailble_manually[$term->tid])) ? $terms_unavailble_manually[$term->tid] : NULL;
        $un_date_from_request = (isset($terms_unavailble_from_request[$term->tid])) ? $terms_unavailble_from_request[$term->tid] : NULL;

        // Remove value for unavailable days if they in unavailable in request.
        if (is_array($un_date) && is_array($un_date_from_request)) {
          $un_date = array_diff($un_date, $un_date_from_request);
          $un_date = empty($un_date) ? NULL : $un_date;
        }

        $trucks[$term->tid] = array(
          'name' => $term->name,
          'field_ld_only' => isset($term->field_ld_only[LANGUAGE_NONE][0]['value']) ? (int) $term->field_ld_only[LANGUAGE_NONE][0]['value'] : 0,
          'unavailable' => $un_date,
          'unavailable_from_request' => $un_date_from_request,
        );
      }
    }


    return $trucks;
  }

  // TODO add for this symphony decoder.
  public function editTermTruck($data = array()) {
    $term = $this->termLoad($data['tid']);
    unset($data['unavailable']);
    unset($data['tid']);
    // Check that term belongs for vocabulary Truck.
    if ($this->termBelongs($term, 'trucks')) {
      $data['name'] = check_plain($data['name']);
      $entity = entity_metadata_wrapper('taxonomy_term', $term);
      foreach ($data as $field_name => $value) {
        $entity->{$field_name} = $value;
      }
      $entity->save();
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function removeTruck($tid) {
    $result = FALSE;
    $term = $this->termLoad($tid);
    // Check that term belongs for vocabulary Truck.
    if ($this->termBelongs($term, 'trucks') && $this->checkPeggingTruck($tid)) {
      $result = taxonomy_term_delete((int) $tid);
    }

    return $result;
  }

  public function checkPeggingTruck($tid) {
    $query = search_api_query('move_request');
    $query->condition("field_list_truck", (int) $tid);
    $data = $query->execute();
    return (isset($data['results']) && count($data['results'])) ? FALSE : TRUE;
  }

  public function insertNewTruck($data) {
    $vocab = taxonomy_vocabulary_machine_name_load('trucks');
    $term = (object) array(
      'name' => $data['name'],
      'vid' => $vocab->vid,
      'type' => 'trucks',
    );
    taxonomy_term_save($term);
    $entity = entity_metadata_wrapper('taxonomy_term', $term);
    foreach ($data as $field_name => $value) {
      $entity->{$field_name} = $value;
    }
    $entity->save();
    return $term->tid;
  }

  public static function dateUnavailableTruck($tid, $dates = array()) {
    foreach ($dates as $key => $date) {
      if (is_array($date)) {
        switch ($key) {
          case "add":
            foreach ($date as $date_new) {
              $date_converted = self::priceCalendarDateUnix($date_new);
              if (!$date_converted) {
                continue;
              }

              $record = array(
                'tid' => $tid,
                'date' => $date_converted,
              );

              db_merge('truck_unavailable')
                ->key($record)
                ->fields($record)
                ->execute();
            }
            break;

          case "remove":
            $unix_dates = array();
            foreach ($date as $date_new) {
              $unix_dates[] = self::priceCalendarDateUnix($date_new);
            }

            if ($unix_dates) {
              db_delete('truck_unavailable')
                ->condition('tid', $tid)
                ->condition('date', $unix_dates)
                ->execute();
            }
            break;
        }
      }
    }
  }

  /**
   * Helper method to load one taxonomy term.
   *
   * @param int $tid
   *   Term Id.
   *
   * @return mixed
   *   Loaded term.
   */
  public function termLoad($tid) {
    $term = entity_load('taxonomy_term', array((int) $tid));
    return reset($term);
  }

  /**
   * Checked what taxonomy term is belong to vocabulary.
   *
   * @param object $term
   *   Object of loaded terms.
   * @param string $vocabulary
   *   Name of vocabulary.
   *
   * @return bool
   *   Status: FALSE - Not belong, TRUE - Belong.
   */
  protected function termBelongs($term, $vocabulary) {
    return $term->vocabulary_machine_name == $vocabulary;
  }

  public static function getPriceCalendar() {
    $result = array();
    $current = date("Y");
    $next_year = date('Y', strtotime('+1 year'));
    $last_year = date('Y', strtotime('-1 year'));
    $result[$last_year] = self::getPriceCalendarByYear($last_year);
    $result[$current] = self::getPriceCalendarByYear($current);
    $result[$next_year] = self::getPriceCalendarByYear($next_year);

    return $result;
  }

  protected static function getPriceCalendarByYear($year) {
    $calendar = variable_get('price_calendar_' . $year, array());

    if (!count($calendar)) {
      self::createNewPriceCalendar($year);
    }
    if (!$calendar) {
      $calendar = self::setPriceCalendar($year);
    }

    return $calendar;
  }

  protected static function setPriceCalendar($year) {
    $result = array();
    $date_from = self::priceCalendarDateUnix($year . '-1-1');
    $date_to = self::priceCalendarDateUnix($year . '-12-31');
    $calendar = db_select('price_calendar_new', 'pc')
      ->fields('pc')
      ->condition('pc.date', array($date_from, $date_to), 'BETWEEN')
      ->orderBy('pc.date', 'ASC')
      ->execute()
      ->fetchAll();

    foreach ($calendar as $date_new) {
      $result[self::priceCalendarDateNormal($date_new->date)] = $date_new->type;
    }

    variable_set("price_calendar_$year", $result);
    return $result;
  }

  protected static function createNewPriceCalendar($year) {
    for ($month = 1; $month <= 12; $month++) {
      $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
      for ($day = 1; $day <= $days_in_month; $day++) {
        $date = $year . '-' . $month . '-' . $day;
        $unix_date = self::priceCalendarDateUnix($date);

        $record = new \stdClass();
        $record->date = $unix_date;
        drupal_write_record('price_calendar_new', $record);
      }
    }
  }

  public static function priceCalendarDateUnix($date) {
    if (strtotime($date)) {
      $new_date = new \DateTime((string) $date, new \DateTimeZone('UTC'));
      return $new_date->getTimestamp();
    }
  }

  public static function priceCalendarDateNormal($date) {
    $new_date = new \DateTime('@' . (int) $date);
    return $new_date->format('Y-m-d');
  }

  protected static function getPriceTipeName($id) {
    $type = '';
    $types = (array) variable_get('calendar_types', array());
    if (isset($types[(int) $id])) {
      $type = $types[(int) $id];
    }

    return $type;
  }

  public static function getPriceTypes() {
    return (array) variable_get('calendar_types');
  }

  public static function setPriceCalendarData($data) {
    $result = array();
    $price_types = self::getPriceTypes();
    $years = array();

    foreach ($data as $date => $type) {
      // Check format of date.
      $d = \DateTime::createFromFormat('Y-m-d', $date);
      if ($d && $d->format('Y-m-d') == $date) {
        $year = $d->format('Y');
        if (self::getPriceCalendarByYear($year) && array_key_exists($type, $price_types)) {
          $record = new \stdClass();
          $record->date = self::priceCalendarDateUnix($date);
          $record->type = $type;
          $result_operation = drupal_write_record('price_calendar_new', $record, 'date');
          $result[$date] = $result_operation == 2 ? TRUE : FALSE;
          if (!isset($years[$year])) {
            $years[] = $year;
          }
        }
      }
    }

    foreach ($years as $year) {
      self::setPriceCalendar($year);
    }

    return $result;
  }

  public static function setSettings($name, $data) {
    $vars = self::availableVars();
    if (in_array($name, $vars)) {
      variable_set($name, $data);
    }

    return TRUE;
  }

  public static function availableVars() {
    return array('basicsettings', 'longdistance', 'calcsettings', 'price_table');
  }

  /**
   * @return array
   */
  public static function getSettings() {
    $settings = array();
    $settings_name = self::availableVars();
    foreach ($settings_name as $name) {
      $settings[$name] = variable_get($name, '');
    }

    return $settings;
  }

  /**
   * Get all default settings from json file.
   *
   * @return array
   */
  public static function getDefaultSettings() {
    $data = array();
    $destination = file_default_scheme() . '://';
    $file_uri = $destination . '/settings/default.json';
    if (file_exists($file_uri)) {
      $filepath = drupal_realpath($file_uri);
      $file_data = file_get_contents($filepath);
      $data = json_decode($file_data);
      if ($data === NULL && json_last_error() !== JSON_ERROR_NONE) {
        watchdog('setDefaultSettings', 'Could\'t correct decode json string from file!', WATCHDOG_WARNING);
        return FALSE;
      }
    }

    return $data;
  }

  /**
   * @return bool
   */
  public static function setDefaultSettings() {
    $result = TRUE;
    $settings = self::getSettings();
    foreach ($settings as $name => $data) {
      $set = static::setDefaultSetting($name, $data);
      if (!$set) {
        $result = FALSE;
      }
    }

    return $result;
  }

  /**
   * Write default settings to json file.
   *
   * @param string $name
   *   The name of setting.
   * @param array $data
   *   Data for setting.
   *
   * @return bool
   *   Result of executed operation.
   */
  protected static function setDefaultSetting($name, $data = array()) {
    $result = FALSE;
    $destination = file_default_scheme() . '://';
    $directory = file_stream_wrapper_uri_normalize($destination . 'settings');
    $file_uri = $directory . '/default.json';
    $data_save = array($name => $data);

    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      if (file_exists($file_uri)) {
        $file_data = file_get_contents($file_uri);
        $decode = json_decode($file_data);
        if ($decode === NULL && json_last_error() !== JSON_ERROR_NONE) {
          watchdog('setDefaultSettings', 'Could\'t correct decode json string from file!', WATCHDOG_WARNING);
          return FALSE;
        }

        $data_save = array_merge((array) $decode, $data_save);
      }

      $result = file_unmanaged_save_data(json_encode($data_save), $file_uri, FILE_EXISTS_REPLACE);
      $result = ($result) ? TRUE : FALSE;
    }

    return $result;
  }

  public static function restoreSetting() {
    $data = self::getDefaultSettings();

    foreach ($data as $name => $value) {
      self::setSettings($name, $value);
    }
  }

  public function getAvailableVariable($name, $default) {
    $result = FALSE;
    $available_vars = self::listPrivateVariable();
    $is_available = array_search($name, $available_vars);

    if ($is_available !== FALSE) {
      $result = variable_get($name);
    }

    return $result;
  }

  private static function listPrivateVariable() {
    $list = array(
      'orderTemplatesBuilder',
      'templateBuilderFolders',
      'calcsettings',
      'basicsettings',
      'longdistance',
      'scheduleSettings',
      'additional_settings',
      'packing_settings',
    );

    return $list;
  }

  /**
   * Method get site transport smtp settings.
   */
  public static function getTransportSmtpSettings() {
    return array(
      'swiftmailer_smtp_host' => variable_get('swiftmailer_smtp_host'),
      'swiftmailer_smtp_port' => variable_get('swiftmailer_smtp_port'),
      'swiftmailer_smtp_encryption' => variable_get('swiftmailer_smtp_encryption'),
      'swiftmailer_smtp_username' => variable_get('swiftmailer_smtp_username'),
      'swiftmailer_smtp_password' => variable_get('swiftmailer_smtp_password'),
    );
  }

  /**
   * Set variable and write log.
   *
   * @param string $name
   *   Variable name.
   * @param string $value
   *   Variable value.
   *
   * @return \DatabaseStatementInterface|int
   *   Result db_merge.
   *
   * @throws \Exception
   */
  public function variableSet(string $name, string $value) {
    global $conf;

    $result = db_merge('variable')->key(array('name' => $name))->fields(array('value' => serialize($value)))->execute();

    cache_clear_all('variables', 'cache_bootstrap');

    $conf[$name] = $value;
    // Write logs: who update settings, time, value.
    $this->createSystemLog($name, $value);

    return $result;
  }

  /**
   * Create system logs.
   *
   * @param string $name
   *   Variable name.
   * @param string $value
   *   Variable value.
   *
   * @return \DatabaseStatementInterface|int
   *   Result db_insert.
   *
   * @throws \Exception
   */
  public function createSystemLog(string $name, string $value) {
    global $user;

    $fields = array(
      'uid' => $user->uid,
      'created' => time(),
      'name' => $name,
      'value' => is_string($value) ? $value : serialize($value),
      'details' => '',
      'source' => 'system',
    );

    $result = db_insert('system_logs')
      ->fields($fields)
      ->execute();

    return $result;
  }

  /**
   * Save valuation plan.
   *
   * @param array $data
   *   Save dat.
   *
   * @throws \Exception
   */
  public function saveValuationPlan(array $data) {
    // Form a two-dimensional array.
    $matrix = [
      'percentTable' => [0 => [0 => NULL]],
      'fixedPriceTable' => [0 => [0 => NULL]],
      'rateTable' => [0 => [0 => NULL]],
    ];
    foreach ($data as $namePlan => $table) {
      $rowIterator = 0;
      foreach ($table['header'] as $firstRowValue) {
        $matrix[$namePlan][$rowIterator][] = $firstRowValue;
      }
      foreach ($table['body'] as $row) {
        $matrix[$namePlan][++$rowIterator][] = $row['name'];
        foreach ($row['value'] as $value) {
          $matrix[$namePlan][$rowIterator][] = $value;
        }
      }
    }
    $transaction = db_transaction();
    try {
      db_truncate('valuation_plan')->execute();
      foreach ($matrix as $namePlan => $table) {
        foreach ($table as $row_id => $row_value) {
          foreach ($row_value as $column_id => $value) {
            $mergeQuery = db_merge('valuation_plan');
            $mergeQuery->key(
              [
                'id_plan' => $namePlan,
                'id_column' => $column_id,
                'id_row' => $row_id,
              ]
            );
            $mergeQuery->fields(array('value' => $value));
            $mergeQuery->execute();
          }
        }
      }
    }
    catch (\Throwable $exception) {
      $transaction->rollback();
      services_error($exception->getMessage(), $exception->getCode());
    }
  }

  /**
   * Get valuation plan.
   *
   * @param array $data
   *   Plan name.
   *
   * @return array
   *   Plan.
   */
  public function getValuationPlan(array $data) {
    $result = [];
    foreach ($data as $idPlan) {
      $select = db_select('valuation_plan', 'vp');
      $select->condition('vp.id_plan', $idPlan);
      $select->isNotNull('vp.value');
      $select->fields('vp');
      $select->orderBy('vp.id_row');
      $select->orderBy('vp.id_column');
      $matrix = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);
      $header = []; $body = [];
      foreach ($matrix as $cell) {
        if ($cell['id_row'] == 0) {
          $header[] = $cell['value'];
        }
        else {
          $rowIndex = $cell['id_row'] - 1;
          $columnIndex = $cell['id_column'];
          if ($columnIndex == 0) {
            $body[$rowIndex]['name'] = $cell['value'];
          }
          else {
            $body[$rowIndex]['value'][$columnIndex - 1] = $cell['value'];
          }

        }
      }
      $result[$idPlan] = [
        'header' => $header,
        'body' => $body,
      ];
    }

    return $result;
  }

}
