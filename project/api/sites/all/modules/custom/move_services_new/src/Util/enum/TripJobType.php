<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TripJobType.
 *
 * @package Drupal\move_services_new\Util
 */
class TripJobType extends BaseEnum {
  const REQUEST = 0;
  const TP_DELIVERY = 1;

}
