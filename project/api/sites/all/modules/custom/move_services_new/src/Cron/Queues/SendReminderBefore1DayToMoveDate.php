<?php

namespace Drupal\move_services_new\Cron\Queues;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\SmsActionTypes;
use Drupal\move_sms\Services\Sms;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class SendReminderBefore1DayToMoveDate.
 *
 * @package Drupal\move_services_new\Cron\Queues
 */
class SendReminderBefore1DayToMoveDate implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \ServicesException
   * @throws \Exception
   */
  public static function execute($data): void {
    $type = 'move_reminder_1';
    $template_builder = new TemplateBuilder();
    foreach ($data as $key => $nid) {
      $node = node_load($nid);
      $was_sent = MoveRequest::intervalEmailWasSent($nid, $type);
      if (empty($was_sent)) {
        $is_send = $template_builder->moveTemplateBuilderSendTemplateRules($type, $node);
      }
      if (!$was_sent && !empty($is_send[$type]['send'])) {
        MoveRequest::markSentEmailByInterval($node, $type);
        (new Sms(SmsActionTypes::REMINDER_TOMORROW))->execute($node->nid, $node, $node->uid);
      }
    }
  }

}
