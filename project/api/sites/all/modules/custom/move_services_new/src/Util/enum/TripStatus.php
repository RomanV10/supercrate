<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TripStatus.
 *
 * @package Drupal\move_services_new\Util
 */
class TripStatus extends BaseEnum {

  const DRAFT = 1;
  const PENDING = 2;
  const ACTIVE = 3;
  const DELIVERED = 4;

}
