<?php

namespace Drupal\move_services_new\Services\move_request;

use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\HomeEstimateStatus;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class MoveRequestHomeEstimatorSearch.
 *
 * @package Drupal\move_services_new\move_request
 */
class MoveRequestHomeEstimatorSearch {
  const NEW_JOBS = 1;
  const PAST_JOBS = 2;
  const PER_PAGE_NUM = 25;

  public $moveRequestSearch;

  private $date = '';

  /**
   * MoveRequestHomeEstimatorSearch constructor.
   */
  public function __construct() {
    $this->moveRequestSearch = new MoveRequestSearch();
  }

  /**
   * Search for home estimator jobs.
   *
   * @param int $type
   *   Type of the jobs(NEW or PAST).
   * @param array $sorting
   *   Array with field sorting.
   * @param int $current_page
   *   Current page to show.
   * @param int $per_page
   *   Number items on page.
   *
   * @return array
   *   Array with requests and page info.
   */
  public function search(int $type = MoveRequestHomeEstimatorSearch::NEW_JOBS, array $sorting = array(), int $current_page = 1, int $per_page = MoveRequestHomeEstimatorSearch::PER_PAGE_NUM) : array {
    $sorting = !empty($sorting) ? $sorting : ['field_home_estimate_date' => 'ASC'];

    $this->date = Extra::date('Y-m-d', NULL, $this->moveRequestSearch->timeZone) . ' 00:00:00';

    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_home_estimator', 'field_home_estimator', 'field_home_estimator.entity_id = n.nid');
    $query->leftJoin('field_data_field_home_estimate_date_created', 'field_home_estimate_date_created', 'field_home_estimate_date_created.entity_id = n.nid');
    $query->leftJoin('field_data_field_home_estimate_date', 'field_home_estimate_date', 'field_home_estimate_date.entity_id = n.nid');
    $query->leftJoin('field_data_field_home_estimate_status', 'field_home_estimate_status', 'field_home_estimate_status.entity_id = n.nid');
    $query->leftJoin('field_data_field_approve', 'field_approve', 'field_approve.entity_id = n.nid');

    $query->fields('n', array('nid'));
    $query->condition('field_home_estimator_value', $this->moveRequestSearch->getUserId());
    $query->condition('n.type', 'move_request');

    if ($type == static::NEW_JOBS) {
      $this->newJobsConditions($query);
    }
    else {
      $this->pastJobsConditions($query);
    }

    if (!empty($sorting)) {
      foreach ($sorting as $field_name => $direction) {
        $query->orderBy($field_name . '_value', $direction);
      }
    }

    $query_for_pager = clone $query;
    $total_count = $query_for_pager->execute()->rowCount();
    $page_count = ceil($total_count / $per_page);
    $start_from = ($current_page - 1) * $per_page;

    $query->range($start_from, $per_page);
    $nids = $query->execute()->fetchCol();

    $result['meta'] = array(
      'currentPage' => (int) $current_page,
      'perPage' => (int) $per_page,
      'pageCount' => (int) $page_count,
      'totalCount' => (int) $total_count,
    );

    $result['items'] = !empty($nids) ? $this->moveRequestSearch->getRequestsMultiply($nids) : [];

    return $result;
  }

  /**
   * Conditions for new jobs.
   *
   * @param $query
   */
  private function newJobsConditions(&$query) {
    $query->condition('field_home_estimate_date_value', $this->date, '>=');
    $query->condition('field_home_estimate_status_value', HomeEstimateStatus::SCHEDULE);
    $query->condition('field_approve_value', RequestStatusTypes::INHOME_ESTIMATE);
  }

  /**
   * Conditions for past jobs.
   *
   * @param $query
   */
  private function pastJobsConditions(&$query) {
    $db_or = db_or();
    $db_or->condition('field_home_estimate_status_value', HomeEstimateStatus::SCHEDULE, '<>');
    $db_or->condition('field_approve_value', RequestStatusTypes::INHOME_ESTIMATE, '<>');
    $db_or->condition('field_home_estimate_date_value', $this->date, '<');
    $query->condition($db_or);
  }

}
