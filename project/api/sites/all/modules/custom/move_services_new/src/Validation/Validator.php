<?php

namespace Drupal\move_services_new\Validation;

use Illuminate\Container\Container;
use Illuminate\Validation\Factory as ValidatorFactory;
use Symfony\Component\Translation\Translator;

/**
 * Validation service.
 */
class Validator
{
  /**
   * @var
   */
  private $validation;

  /**
   * Method to validate incoming info.
   *
   * @param array $controller
   *   Controller definition.
   * @param array $sources
   *   Array arguments incoming data.
   *
   * @throws \ServicesException
   */
  public function validateService(array $controller, array $sources) : void {
    if (empty($controller['rules'])) {
      return;
    }

    $rules = $this->getRules($controller);
    $arguments = $this->getDataFromArguments($controller, $sources);

    if ($rules && $arguments) {
      $validation = $this->validate($rules, $arguments);
      if ($validation->fails()) {
        services_error($this->prepareErrorString($validation->messages()->toArray()), 400);
      }
    }
  }

  /**
   * Get arguments data and names from controller.
   *
   * @param array $controller
   *   Controller definition.
   * @param array $sources
   *   Arguments data.
   *
   * @return array
   *   Data with arguments.
   */
  private function getDataFromArguments(array $controller, array $sources) : array {
    $arguments = [];

    foreach ($controller['args'] as $argument_number => $arg) {
      $argument_source = $arg['source'];

      if (is_array($argument_source)) {
        $argument_source_keys = array_keys($argument_source);
        $source_name = reset($argument_source_keys);
        $argument_name = $argument_source[$source_name];
        if ($source_name == 'path') {
          $argument_name = (int) $argument_name;
        }
        if (isset($sources[$source_name][$argument_name])) {
          $arguments[$arg['name']] = $sources[$source_name][$argument_name];
        }
      }
      else {
        if (isset($sources[$argument_source])) {
          $arguments[$arg['name']] = $sources[$argument_source];
        }
      }
    }

    return $arguments;
  }

  /**
   * Validate data by rules.
   *
   * @param array $rules
   *   Array with rules.
   * @param array $arguments
   *   Array with arguments.
   *
   * @return mixed
   *   Return errors.
   *
   * @throws \ServicesException
   */
  public function validate(array $rules, array $arguments) {
    try {
      $this->setValidator();

      return $this->validation->make($arguments, $rules, $this->messageFormat());
    }
    catch (\Throwable $e) {
      services_error($e->getMessage(), 500);
    }
  }

  /**
   * Set validator.
   */
  private function setValidator() : void {
    $app = new Container();
    $app->singleton('translator', function () {
      return new Translator('en');
    });
    $app->singleton('validator', function ($app) {
      return new ValidatorFactory($app['translator']);
    });

    $this->validation = $app->make('validator');

    $this->addExtends();
  }

  /**
   * Build message format.
   *
   * @return array
   *   Array with messages info.
   */
  private function messageFormat() : array {
    return array(
      'required' => ':attribute is required',
      'min' => ':attribute must be at least :min characters',
      'max' => ':attribute must be no more than :max characters',
      'email' => ':attribute must email',
      'dexist' => ':attribute dose not exist',
      'dunique' => ':attribute is not unique',
      'array' => ':attribute must be array',
      'json' => ':attribute must be json string',
      'string' => ':attribute must be string',
      'integer' => 'must be integer',
      'date_format' => 'date format is wrong',
    );
  }

  /**
   * Prepare error message for front.
   *
   * @param array $errors
   *   Array with errors.
   *
   * @return string
   *   Error string.
   */
  private function prepareErrorString(array $errors) : string {
    $output = '';
    foreach ($errors as $arg_name => $arg_error) {
      $output .= "$arg_name : \r\n";
      $output .= implode("\r\n", $arg_error);
      $output .= "\r\n";
    }

    return $output;
  }

  /**
   * Get rules from controller.
   *
   * @param array $controller
   *   Argument info.
   *
   * @return array
   *   Array with rules.
   */
  private function getRules(array $controller) : array {
    return class_exists($controller['rules']) ? (new $controller['rules']())->rules() : [];
  }

  /**
   * Extends for rules.
   */
  public function addExtends() {
    $this->validation->extend('dexist', function ($attribute, $value, $parameters) {
      return db_select($parameters[0], 't')
        ->fields('t', [$parameters[1]])
        ->condition($parameters[1], $value)
        ->execute()
        ->rowCount() > 0;
    });

    $this->validation->extend('dunique', function ($attribute, $value, $parameters) {
      return db_select($parameters[0], 't')
        ->fields('t', [$parameters[1]])
        ->condition($parameters[1], $value)
        ->execute()
        ->rowCount() < 1;
    });

    $this->validation->extend('json', function ($attribute, $value, $parameters) {
      json_decode($value);
      return (json_last_error() == JSON_ERROR_NONE) ? TRUE : FALSE;
    });
  }

}
