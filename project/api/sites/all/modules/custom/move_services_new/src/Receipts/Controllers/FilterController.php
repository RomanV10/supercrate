<?php

namespace Drupal\move_services_new\Recipts\Controllers;

/**
 * Class FilterController.
 *
 * @package Drupal\move_services_new\Recipts\Controllers
 */
class FilterController {

  public function filterReceiptByDate() {

  }

  public function filterReceiptByPayMethod() {

  }

  public function filterReceiptByRole() {

  }

  public function filterReceiptByStorageAgreement() {

  }

  public function filterReceiptThirdPartyPayment() {

  }

  public function filterReceiptThirdPartyCollected() {

  }

  public function filterReceiptByStorageTenant() {

  }

  public function filterReceiptByCreditCardType() {

  }

  public function filterReceiptByPendingFlag() {

  }

  public function filterReceiptByRefundFlag() {

  }

}