<?php

namespace Drupal\move_services_new\Services\FieldStrategy;

use Drupal\move_services_new\Services\move_request\MoveRequest;

/**
 * Class FieldImageFileStrategy.
 *
 * @package Drupal\move_services_new\Services\FieldStrategy
 */
class FieldImageFileStrategy implements FieldArrayStrategyInterface {
  private $fileTypes = array(
    'image/jpeg' => 'jpg',
    'image/png' => 'png',
    'image/gif' => 'gif',
  );

  /**
   * Create data.
   *
   * @param mixed $values
   *   Value os field.
   * @param string $widget_type
   *   The widget type.
   * @param int $count_values
   *   Count values.
   * @param bool $multi
   *   Indicated that field is multi.
   * @param array $field_info
   *   Information about field.
   *
   * @return mixed
   *   Result
   *
   * @throws \ServicesException
   */
  public function createData($values, string $widget_type, int $count_values, bool $multi = FALSE, array $field_info = array()) {
    $data = array();
    $values = (array_key_exists(0, $values)) ? $values : array($values);
    for ($i = 0; $i < $count_values; $i++) {
      if (isset($values[$i]['uri'])) {
        $file_saved = $this->saveFile($values[$i], $field_info['field_name']);
        if ($file_saved) {
          MoveRequest::$files[$i] = (array) $file_saved;
          $data[LANGUAGE_NONE][] = (array) $file_saved;
        }
      }
    }
    return $data;
  }

  /**
   * Save file.
   *
   * @param array $value
   *   File array.
   * @param string $field_name
   *   Field name.
   *
   * @return bool|mixed|\stdClass
   *   Save result.
   *
   * @throws \ServicesException
   */
  protected function saveFile(array $value, string $field_name) {
    $field_instance = field_info_instance('node', $field_name, 'move_request');
    $file_exp = explode(',', $value['uri']);
    $file_data = array(
      'title' => (isset($value['title'])) ? $value['title'] : '',
      'data' => $file_exp[1],
    );

    $exp_code = explode(';', $file_exp[0]);
    $exp_mimetype = explode(':', $exp_code[0]);
    $destination = file_default_scheme() . '://' . $field_instance['settings']['file_directory'] . '/' . user_password(8) . '.' . $this->fileTypes[$exp_mimetype[1]];

    $dir = drupal_dirname($destination);
    // Build the destination folder tree if it doesn't already exists.
    if (!file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
      return services_error(t("Could not create destination directory for file."), 500);
    }

    // Write the file.
    $file_saved = file_save_data(base64_decode($file_data['data']), $destination);
    if ($file_saved) {
      file_usage_add($file_saved, 'move_services_new', 'files', $file_saved->fid);
      $file_saved->filemime = $exp_mimetype[1];
      file_save($file_saved);
      if ($file_data['title']) {
        $file_saved->title = $file_data['title'];
      }
    }

    return $file_saved;
  }

}
