<?php

namespace Drupal\move_services_new\Cron\Queues;

use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class SendEmailWhenStatusChangeToDatePending.
 *
 * @package Drupal\move_services_new\Queues
 */
class SendEmailWhenStatusChangeToDatePending implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   */
  public static function execute($data): void {
    try {
      if (!empty($data['nid'])) {
        $node = node_load($data['nid']);
        if ($node) {
          (new TemplateBuilder())->moveTemplateBuilderSendTemplateRules('status_we_are_not_available', $node);
        }
        else {
          throw  new \Exception("Node {$data['nid']} is empty");
        }
      }
    }
    catch (\Throwable $e) {
      $message = "Sending template to client when status was changed to 'Date pending': {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Action:Request status changed', $message, array(), WATCHDOG_CRITICAL);
    }
  }

}
