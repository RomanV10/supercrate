<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class InvoiceFlags.
 *
 * @package Drupal\move_invoice\Services
 */
class InvoiceFlags extends BaseEnum {

  const DRAFT = 0;
  const SENT = 1;
  const VIEWED = 2;
  const PAID = 3;

}
