<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RequestsPermissions.
 *
 * @package Drupal\move_storage\Services
 */
class RequestsPermissions extends BaseEnum {

  const CAN_SEE_OTHER_SALES_LEADS = 1;
  const CAN_SEARCH_OTHER_SALES_LEADS = 2;
  const CAN_EDIT_OTHER_LEADS = 3;
  const CAN_SEE_UNSIGNED_LEADS = 4;
  const CAN_ASSIGN_LEADS_TO_OTHER_SALES = 5;
  const CAN_ASSIGN_UNASSIGNED_LEADS = 6;

}
