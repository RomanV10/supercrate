<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_long_distance\Services\LongDistanceReuqest;

/**
 * Class Invertory.
 */
class Inventory extends BaseService {
  protected $nid = NULL;

  /**
   * Default froom.
   *
   * Taken from front.
   * Path:'project/front/moveBoard/app/inventories/services/inventories.service.js'
   *
   * @var array
   */
  public $rooms = [
    1 => 'Studio',
    2 => 'Kitchen',
    3 => 'Bedroom',
    4 => 'Bedroom 2',
    5 => 'Bedroom 3',
    6 => 'Bedroom 4',
    7 => 'Living Room',
    8 => 'Dining Room',
    9 => 'Office',
    10 => 'Extra Rooms',
    11 => 'Basement',
    12 => 'Garage',
    13 => 'Patio',
    14 => 'Play Room',
    15 => 'All',
    16 => 'Other',
    17 => 'Boxes',
  ];

  /**
   * Constructor of Invertory service.
   */
  public function __construct($nid = NULL) {
    if ($nid) {
      $query = db_select('node');
      $query->addExpression('COUNT(*)');
      $query->condition('nid', (int) $nid)
        ->execute()->fetchField();
      if ($query) {
        $this->nid = (int) $nid;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function create($data = array()) {
    return $this->update($data);
  }

  /**
   * {@inheritdoc}
   */
  public function update($data = array()) {
    if ($this->nid) {
      $inventory = $this->setInvertoryData($data);

      $query = db_merge('move_inventory')
        ->key(array('id' => $inventory->id))
        ->fields((array) $inventory);
      $query->execute();

      if (property_exists($inventory, 'items') && $inventory->items) {
        $items = new \stdClass();
        $items->id = $inventory->id;
        $items->items = $inventory->items;
        if (property_exists($inventory, 'stats') && $inventory->stats) {
          $items->stats = $inventory->stats;
        }
        rules_invoke_event('inventory_save', $items);
      }

      if (property_exists($inventory, 'details') && $inventory->details) {
        $details = new \stdClass();
        $details->id = $inventory->id;
        $details->details = $inventory->details;
        rules_invoke_event('details_save', $details);
      }
      if (isset($data['details']['delivery'])) {
        MoveRequest::updateDeliveryDate($this->nid, $data['details']['delivery']);
      }
      if (isset($data['field_ld_sit_delivery_dates'])) {
        LongDistanceReuqest::updateLDDeliveryDates($this->nid, $data['field_ld_sit_delivery_dates']);
      }
      LongDistanceReuqest::writeRequestToLdRequest($this->nid);
      Cache::updateCacheData($this->nid);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Helper method to set invertory data to save in db.
   *
   * @param array $data
   *   Data to save.
   *
   * @return \stdClass
   *   Object to save in to db.
   */
  protected function setInvertoryData($data = array()) {
    $inventory = new \stdClass();
    $inventory->id = $this->nid;

    if (isset($data['items'])) {
      $inventory->items = json_encode($data['items']);
    }

    if (isset($data['stats'])) {
      $inventory->stats = json_encode($data['stats']);
    }

    if (isset($data['details'])) {
      $inventory->details = json_encode($data['details']);
    }

    return $inventory;
  }

  /**
   * {@inheritdoc}
   */
  public function retrieve() {
    $result = array();
    if ($this->nid) {
      $query = db_select('move_inventory', 'p')
        ->fields('p', array('id', 'items', 'stats', 'details'))
        ->condition('id', $this->nid);

      if ($inv = $query->execute()->fetch()) {
        $result['inventory_list'] = json_decode($inv->items, TRUE);
        $result['statistics'] = json_decode($inv->stats, TRUE);
        $result['move_details'] = json_decode($inv->details, TRUE);
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {}

  /**
   * Get invertory objects.
   */
  public function getInvertoryObjects() {
    $data = &drupal_static(__METHOD__, NULL);

    if (!isset($data)) {
      if ($cache = cache_get("move_invertory_cached_data")) {
        $data = $cache->data;
      }
      else {
        $data = $this->getInvertoryData();
        // Create cache on 20 days.
        cache_set("move_invertory_cached_data", $data, 'cache', REQUEST_TIME + 1728000);
      }
    }

    return $data;
  }

  /**
   * Helper method to get all invertory nodes for cache.
   */
  protected function getInvertoryData() {
    $data = array();
    $query = new \EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'inventory_item')
      ->execute();
    $nids = array_keys($result['node']);

    foreach ($nids as $nid) {
      $wrapper = entity_metadata_wrapper('node', $nid);
      array_push($data, array(
        'title' => $wrapper->title->value(),
        'photo' => $this->getItemPhotoInfo($wrapper),
        'id' => $nid,
        'fid' => $this->getItemFilterInfo($wrapper),
        'cubicFeet' => $wrapper->field_weight_lb_->value(),
        'title_invertory_list' => $wrapper->field_title_inventory_list_->value(),
      ));
    }

    return $data;
  }

  /**
   * Helper method for get data about photo field.
   *
   * @param \EntityMetadataWrapper $wrapper
   *
   * @return array
   *   Data from photo field.
   */
  protected function getItemPhotoInfo(\EntityMetadataWrapper $wrapper) {
    $value = $wrapper->field_inventory_item_photo->value();

    return array(
      'fid' => $value['fid'],
      'name' => $value['filename'],
      'path' => file_create_url($value['uri']),
    );
  }

  /**
   * Helper method for get data about filter field.
   *
   * @param \EntityMetadataWrapper $wrapper
   *
   * @return string
   *   Taxonomy title name.
   */
  protected function getItemFilterInfo(\EntityMetadataWrapper $wrapper) {
    $value = $wrapper->field_inventory_item_filter->value();
    return $value->tid;
  }
}
