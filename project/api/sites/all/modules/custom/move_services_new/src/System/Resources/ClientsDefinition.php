<?php

namespace Drupal\move_services_new\System\Resources;

use Drupal\move_dispatch\Controllers\Team;
use Drupal\move_services_new\Services\Clients;

/**
 * Class ClientsDefinition.
 *
 * @package Drupal\move_services_new\System\Resources
 */
class ClientsDefinition {

  public static function getDefinition() {
    return array(
      'clients' => array(
        'operations' => array(
          'retrieve' => array(
            'help' => 'Retrieve a user',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'description' => 'The uid of the user to retrieve.',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access user profiles'),
          ),
          'create' => array(
            'help' => 'Create a user',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'account',
                'type' => 'array',
                'description' => 'The user object',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::userAccess',
            'access arguments' => array('create'),
            'access arguments append' => FALSE,
          ),
          'update' => array(
            'help' => 'Update a user',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'description' => 'Unique identifier for this user',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The user object with updated information',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::userAccess',
            'access arguments' => array('update'),
            'access arguments append' => TRUE,
          ),
          'delete' => array(
            'help' => 'Delete a user',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'description' => 'The id of the user to delete',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::userAccess',
            'access arguments' => array('delete'),
            'access arguments append' => TRUE,
          ),
          'index' => array(
            'help' => 'List all users',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 0.',
                'default value' => 0,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'pagesize',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pagesize'),
              ),
              array(
                'name' => 'fields',
                'optional' => TRUE,
                'type' => 'string',
                'description' => 'The fields to get.',
                'default value' => '*',
                'source' => array('param' => 'fields'),
              ),
            ),
            'access arguments' => array('access user profiles'),
          ),
        ),
        'actions' => array(
          'checkunique' => array(
            'help' => 'Check unique email',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::checkunique',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'email',
                'type' => 'string',
                'description' => 'Email string',
                'source' => array('data' => 'email'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'getcurrent' => array(
            'help' => 'Retrieve a user',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::getcurrent',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'access arguments' => array('access content'),
          ),
          'find_client' => array(
            'help' => 'Find client by email or name',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::findClient',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'roles' => array(
            'help' => 'Get User roles',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::roles',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'access arguments' => array('access content'),
          ),
          'link_restore_password' => array(
            'help' => 'Generating link for password reset.',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::linkRestorePassword',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'email',
                'type' => 'string',
                'source' => array('data' => 'email'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'link_restore' => array(
            'help' => 'Automatic authorization, generating new token(session) for client.',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::linkRestore',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'timestamp',
                'type' => 'int',
                'source' => array('data' => 'timestamp'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'hashed_pass',
                'type' => 'string',
                'source' => array('data' => 'hashed_pass'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'restore_password' => array(
            'help' => 'Request for restore password.',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::restorePassword',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'access arguments' => array('change own password'),
          ),
          'email_account' => array(
            'help' => 'Update User Email account',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::emailAccount',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::UserAccess',
            'access arguments' => array('update'),
            'access arguments append' => TRUE,
          ),
          'update_user_settings' => array(
            'help' => 'Update User Email account',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::updateUserSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::UserAccess',
            'access arguments' => array('update'),
            'access arguments append' => TRUE,
          ),
          'update_client_all_requests' => array(
            'help' => 'Update the client and all his requests',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::updateClientAllRequests',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'description' => 'Unique identifier for this user',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The user object with updated information',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::userAccess',
            'access arguments' => array('update'),
            'access arguments append' => TRUE,
          ),
          'create_user_update_request' => array(
            'help' => 'Update the client and all his requests',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::createUserUpdateRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'description' => 'Unique identifier for this user',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'User data',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'check_login' => array(
            'help' => 'Check login',
            'callback' => 'Drupal\move_services_new\System\Resources\ClientsDefinition::checkLogin',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/ClientsDefinition',
            ),
            'args' => array(
              array(
                'name' => 'login',
                'type' => 'string',
                'source' => array('data' => 'login'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  /**
   * Get user details.
   *
   * @param int $uid
   *   UID of the user to be loaded.
   *
   * @return object
   *   A user object.
   *
   * @see user_load()
   *
   * @throws \Exception
   */
  public static function retrieve($uid) {
    $clients = new Clients($uid);
    return $clients->retrieve();
  }

  /**
   * Create a new user.
   *
   * @param object $account
   *   User object.
   *
   * @return bool|int
   *   Return user ID.
   *
   * @throws \Exception
   */
  public static function create($account) {
    $clients = new Clients();
    return $clients->create($account);
  }

  /**
   * Update an existing user.
   *
   * @param int $uid
   *   User id.
   * @param $account
   *   User object.
   *
   * @return array
   *   Result.
   *
   * @throws \Exception
   */
  public static function update($uid, $account) {
    $clients = new Clients($uid);
    return $clients->update($account);
  }

  /**
   * Delete a user.
   *
   * @param int $uid
   *   UID of the user to be deleted.
   *
   * @see user_delete()
   *
   * @return mixed
   *   If user deleted - TRUE.
   *
   * @throws \Exception
   */
  public static function delete($uid) {
    $clients = new Clients($uid);
    return $clients->delete();
  }

  /**
   * @param $page
   * @param $pagesize
   * @param $fields
   * @return array
   *
   * @throws \Exception
   */
  public static function index($page, $pagesize, $fields) {
    $clients = new Clients();
    return $clients->index($page, $pagesize, $fields);
  }

  /**
   * @return array|bool
   *
   * @throws \Exception
   */
  public static function getcurrent() {
    global $user;

    if ($user->uid) {
      $clients = new Clients($user->uid);
      return $clients->getCurrentUser();
    }
    else {
      return FALSE;
    }
  }

  /**
   * @param $email
   * @return bool
   */
  public static function checkunique($email) {
    $result = FALSE;

    if ($user = user_load_by_mail($email)) {
      return $user->uid ?? FALSE;
    }

    return $result;
  }

  /**
   * @param $data
   * @return array
   * @throws \Exception
   */
  public static function findClient($data) {
    $clients = new Clients();
    return $clients->findUser($data);
  }

  public static function roles() {
    return user_roles();
  }

  /**
   * @param $email
   * @return array
   * @throws \Exception
   */
  public static function linkRestorePassword($email) {
    return (new Clients())->linkRestorePassword($email);
  }

  /**
   * Create link restore.
   *
   * @param int $uid
   *   User id.
   * @param int $timestamp
   *   Timestamp.
   * @param string $hashed_pass
   *   Hashed user password.
   *
   * @return mixed
   *   Token or false.
   *
   * @throws \Exception
   */
  public static function linkRestore($uid, $timestamp, $hashed_pass) {
    if (empty($uid)) {
      return Team::response([], 500, 'Uid does not set or incorrect');
    }
    return (new Clients())->linkRestore($uid, $timestamp, $hashed_pass);
  }

  public static function restorePassword() {
    (new Clients())->restorePassword();
  }

  public static function emailAccount($uid, $data) {
    $clients = new Clients($uid);
    return $clients->updateEmailSettings($data);
  }

  public static function updateUserSettings($uid, $data) {
    return Clients::updateUserSettings($uid, $data);
  }

  /**
   * Access check callback for user resource.
   *
   * @param string $op
   * @param array $args
   * @return bool
   * @throws \ServicesException
   */
  public static function userAccess($op = 'view', $args = array()) {
    global $user;
    $user_register = variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL);

    if (isset($args[0])) {
      $args[0] = _services_access_value($args[0], array('account', 'data'));
    }

    // Check if the user exists if appropriate.
    if ($op != 'create' && $op != 'register') {
      $account = user_load($args[0]);
      if (!$account) {
        return services_error(t('There is no user with ID @uid.', array('@uid' => $args[0])), 406);
      }
    }

    switch ($op) {
      case 'view':
        return user_view_access($account);

      case 'update':
        return ($user->uid == $account->uid || user_access('administer users'));

      case 'create':
      case 'register':
        if (!$user->uid &&  $user_register != USER_REGISTER_ADMINISTRATORS_ONLY) {
          return TRUE;
        }
        else {
          return user_access('administer users');
        }
      case 'password_reset':
        return TRUE;

      case 'delete':
      case 'cancel':
        return user_access('administer users');
    }
  }

  /**
   * Update the user and all his requests.
   *
   * @param int $uid
   *   Unique user id.
   * @param array $client_data
   *   Data to update user and his requests.
   *
   * @return array
   *   Updated nids.
   *
   * @throws \Exception
   */
  public static function updateClientAllRequests($uid, array $client_data) {
    return (new Clients($uid))->updateClientAllRequests($client_data);
  }

  /**
   * Create user and set in request.
   *
   * @param int $nid
   *   Node id.
   * @param array $client_data
   *   Client data for create.
   *
   * @return array
   *   Request id.
   *
   * @throws \Exception
   */
  public static function createUserUpdateRequest(int $nid, array $client_data) {
    return (new Clients())->createUserUpdateRequest($nid, $client_data);
  }

  /**
   * Check login exist.
   *
   * @param string $login
   *   Name.
   *
   * @return array
   *   Custom answer.
   */
  public static function checkLogin(string $login) {
    return array(
      'status_code' => 200,
      'status_message' => 'OK',
      'data' => (new Clients())->checkLoginExist($login),
    );
  }

}
