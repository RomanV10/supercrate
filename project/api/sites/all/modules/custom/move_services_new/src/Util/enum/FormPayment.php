<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class FormPayment.
 *
 * @package Drupal\move_services_new\Util
 */
class FormPayment extends BaseEnum {

  const CHECK = 0;
  const CASH = 1;

}
