<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class Roles.
 *
 * @package Drupal\move_services_new\Util
 */
class CreditCard extends BaseEnum {

  const VISA = 1;
  const MASTERCARD = 2;
  const DISCOUVER = 3;
  const AMEX = 4;

}