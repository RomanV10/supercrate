<?php

namespace Drupal\move_services_new\System\Resources;

use Drupal\move_services_new\Services\Payment;

class PaymentDefinition {

  public static function getDefinition() {
    return array(
      'payment' => array(
        'operations' => array(
          'create' => array(
            'help' => 'Create payment',
            'callback' => 'Drupal\move_services_new\System\Resources\PaymentDefinition::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PaymentDefinition',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'phone',
                'type' => 'string',
                'source' => array('data' => 'phone'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'email',
                'type' => 'string',
                'source' => array('data' => 'email'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'zip_code',
                'type' => 'string',
                'source' => array('data' => 'zip_code'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'card_type',
                'type' => 'string',
                'source' => array('data' => 'card_type'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'description',
                'type' => 'string',
                'source' => array('data' => 'description'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'credit_card',
                'type' => 'array',
                'description' => 'The data of new content.',
                'source' => array('data' => 'credit_card'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'amount',
                'type' => 'string',
                'source' => array('data' => 'amount'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'payment_method',
                'type' => 'string',
                'source' => array('data' => 'payment_method'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'payment_flag',
                'type' => 'string',
                'source' => array('data' => 'payment_flag'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'invoice_id',
                'type' => 'int',
                'source' => array('data' => 'invoice_id'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'coupon',
                'type' => 'array',
                'description' => 'The coupon data.',
                'source' => array('data' => 'coupon'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'pending',
                'type' => 'int',
                'description' => 'The pending status.',
                'source' => array('data' => 'pending'),
                'optional' => TRUE,
                'default value' => 0,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'set_authorizenet' => array(
            'help' => 'Save authorize.net data',
            'callback' => 'Drupal\move_services_new\System\Resources\PaymentDefinition::setAuthorizenet',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PaymentDefinition',
            ),
            'args' => array(
              array(
                'name' => 'loginid',
                'type' => 'string',
                'source' => array('data' => 'loginid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'transactionkey',
                'type' => 'string',
                'source' => array('data' => 'transactionkey'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_authorizenet' => array(
            'help' => 'Get authorize.net data',
            'callback' => 'Drupal\move_services_new\System\Resources\PaymentDefinition::getAuthorizenet',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PaymentDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'refund' => array(
            'help' => 'Refund transaction',
            'callback' => 'Drupal\move_services_new\System\Resources\PaymentDefinition::refund',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PaymentDefinition',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'credit_card',
                'type' => 'array',
                'source' => array('data' => 'credit_card'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'phone',
                'type' => 'string',
                'source' => array('data' => 'phone'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'email',
                'type' => 'string',
                'source' => array('data' => 'email'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'zip_code',
                'type' => 'string',
                'source' => array('data' => 'zip_code'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'amount',
                'type' => 'string',
                'source' => array('data' => 'amount'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'receipt_id',
                'type' => 'int',
                'source' => array('data' => 'receipt_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'description',
                'type' => 'string',
                'source' => array('data' => 'description'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'trans_id',
                'type' => 'int',
                'source' => array('data' => 'trans_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  public static function create($entity_id, $entity_type, $name, $phone, $email, $zip_code, $card_type = '', $description = '', $credit_card, $amount, $payment_method = '', $payment_flag = '', $invoice_id = NULL, $coupon = array(), bool $pending = FALSE) {
    $payment_instance = new Payment($entity_id, $entity_type);
    $data = array(
      'name' => $name,
      'phone' => $phone,
      'email' => $email,
      'zip_code' => $zip_code,
      'invoice_id' => $invoice_id,
      'card_type' => $card_type,
      'description' => $description,
      'credit_card' => $credit_card,
      'payment_flag' => $payment_flag,
      'payment_method' => $payment_method,
      'amount' => $amount,
      'coupon' => $coupon,
      'pending' => $pending,
    );

    return $payment_instance->create($data);
  }

  public static function setAuthorizenet($loginid, $transkey) {
    Payment::setAuthorizenet($loginid, $transkey);
  }

  public static function getAuthorizenet() {
    return Payment::getAuthorizenet();
  }

  public static function refund($entity_id, $entity_type, $credit_card, $name, $phone, $email, $zip_code, $amount, $receipt_id, $description = '', $trans_id) {
    $payment_instance = new Payment($entity_id, $entity_type);
    $data = array(
      'amount' => $amount,
      'credit_card' => $credit_card,
      'description' => $description,
      'trans_id' => $trans_id,
      'name' => $name,
      'phone' => $phone,
      'email' => $email,
      'zip_code' => $zip_code,
      'receipt_id' => $receipt_id,
    );

    return $payment_instance->refunds($data);
  }

}
