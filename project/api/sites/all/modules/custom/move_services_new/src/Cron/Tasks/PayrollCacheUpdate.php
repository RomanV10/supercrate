<?php

namespace Drupal\move_services_new\Cron\Tasks;

use Drupal\move_services_new\Services\Payroll;

/**
 * Class PayrollCacheUpdate.
 *
 * @package Drupal\move_services_new\Cron
 */
class PayrollCacheUpdate implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $dates = array();
    $now_date_instance = new \DateTime();
    $dates[] = $now_date_instance->format("Y-m-d");
    $now_date_instance->modify("-1 day");
    $dates[] = $now_date_instance->format("Y-m-d");
    $payroll_instance = new Payroll();
    foreach ($dates as $date) {
      $payroll_instance->cacheCreatePayrollAll($date);
    }
  }

}
