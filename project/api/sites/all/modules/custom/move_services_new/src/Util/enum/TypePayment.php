<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TypePayment.
 *
 * @package Drupal\move_services_new\Util
 */
class TypePayment extends BaseEnum {

  const HOURLYRATE = 0;
  const AMOUNT = 1;

}
