<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class FormNames.
 *
 * @package Drupal\move_services_new\Util
 */
class FormNames extends BaseEnum {

  const FORM_QUOTE_1 = 1;
  const FORM_QUOTE_2 = 2;
  const FORM_QUOTE_3 = 3;
  const FORM_CALCULATE_1 = 4;
  const FORM_CALCULATE_2 = 5;
  const FORM_CALCULATE_3 = 6;

}
