<?php

namespace Drupal\move_services_new\Cron\Queues;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class SendReminderOneDayReservationDone.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
class SendReminderOneDayReservationDone implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Tasks id.
   *
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public static function execute($data): void {
    $type = 'reminder_finish_reservation';
    $template_builder = new TemplateBuilder();

    foreach ($data as $key => $nid) {
      $node = node_load($nid);
      if (!empty($node)) {
        $is_send = $template_builder->moveTemplateBuilderSendTemplateRules($type, $node);
        if (!empty($is_send[$type]['send'])) {
          MoveRequest::markSentEmailByInterval($node, $type);
        }
      }
    }
  }

}
