<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RateCommission.
 *
 * @package Drupal\move_services_new\Util
 */
class RateCommission extends BaseEnum {

  const ADVANCEDSERVICE = 0;
  const COMMISSIONFROMTOTAL = 1;
  const EXTRASCOMMISSION = 2;
  const DAILYRATE = 3;
  const HOURLYRATE = 4;
  const INSURANCE = 5;
  const LOADINGPEROUFT = 6;
  const MATERIALSCOMMISSION = 7;
  const UNLOADINGPEROUFT = 8;
  const HOURLYRATEHELPER = 9;
  const TIPS = 10;
  const BONUS = 11;

}
