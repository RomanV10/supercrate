<?php

namespace Drupal\move_services_new\Cron\Tasks;

use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class GetRemindersBefore1DaysMoveDate.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
class GetRemindersBefore1DaysMoveDate implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $today = date("Y-m-d 00:00:01");
    $plus_five_days = date('Y-m-d 23:59:59', strtotime("+1 days"));

    $query = db_select('field_data_field_date', 'fd');
    $query->leftJoin('node', 'n', 'n.nid = fd.entity_id');
    $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id');
    $query->fields('fd', array('entity_id'));
    $query->condition('fd.bundle', 'move_request');
    $query->condition('fd.field_date_value', [$today, $plus_five_days], 'BETWEEN');
    $query->condition('fa.field_approve_value', RequestStatusTypes::CONFIRMED, '=');
    $result = $query->execute()->fetchCol();
    if (!empty($result)) {
      $queue = \DrupalQueue::get('move_services_new_send_reminder_before_1day_move_date');
      $queue->createQueue();
      $chunk = array_chunk($result, 3);
      foreach ($chunk as $item) {
        $queue->createItem($item);
      }
    }
  }

}
