<?php

namespace Drupal\move_services_new\Services\FieldStrategy;

/**
 * Class FieldArrayContext.
 *
 * @package Drupal\move_services_new\Services\FieldStrategy
 */
class FieldArrayContext {
  private $strategyName;
  private $data = array();
  private $countValues = 1;

  /**
   * FieldArrayContext constructor.
   *
   * @param \Drupal\move_services_new\Services\FieldStrategy\FieldArrayStrategyInterface $strategy
   *   Strategy.
   */
  public function __construct(FieldArrayStrategyInterface $strategy) {
    $this->strategyName = $strategy;
  }

  /**
   * Create data for strategy.
   *
   * @param mixed $values
   *   Value os field.
   * @param string $widget_type
   *   The widget type.
   * @param array $field_info
   *   Information about field.
   *
   * @return array|mixed
   *   Result.
   */
  public function execute($values, string $widget_type, array $field_info = array()) {
    $multi = (isset($field_info['cardinality']) && ($field_info['cardinality'] == -1 || $field_info['cardinality'] >= 2)) ? TRUE : FALSE;
    if (is_array($values)) {
      $this->countValues = count($values);
    }
    elseif (($multi && !is_array($values)) || (!$multi && $field_info['cardinality'] >= 2 && !is_array($values))) {
      $values = array($values);
    }

    $this->data = $this->strategyName->createData($values, $widget_type, $this->countValues, $multi, $field_info);
    return $this->data;
  }

}
