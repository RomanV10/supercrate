<?php

namespace Drupal\move_services_new\Cron\Queues;

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\SmsActionTypes;
use Drupal\move_sms\Services\Sms;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class SendReminderHomeEstimate.
 *
 * @package Drupal\move_storage\Queues
 */
class SendReminderHomeEstimate implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \Throwable
   */
  public static function execute($data = []): void {
    global $user;

    $template = db_select('move_template_builder', 'mtb')
      ->fields('mtb')
      ->condition('mtb.key_name', 'inhome_estimate_reminder', '=')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($template)) {
      $template[0]['data'] = (array) json_decode($template[0]['data']);
      $template[0]['subject'] = $template[0]['data']['subject'];
      $template_builder = new TemplateBuilder();

      foreach ($data as $item) {
        $mails = [];
        $node = node_load($item['nid']);
        if (!empty($node)) {
          $query = db_select('node', 'n');
          $query->leftJoin('field_data_field_home_estimator', 'h_estimator', 'h_estimator.entity_id = n.nid');
          $query->leftJoin('field_data_field_request_manager', 'r_manager', 'r_manager.entity_id = n.nid');
          $query->leftJoin('field_revision_field_e_mail', 'f_email', 'f_email.entity_id = n.nid');
          $query->fields('h_estimator', array('field_home_estimator_value'));
          $query->fields('r_manager', array('field_request_manager_target_id'));
          $query->fields('f_email', array('field_e_mail_email'));
          $query->condition('n.nid', $item['nid'], '=');
          $users = $query->execute()->fetchAssoc();
          // Home estimator.
          if (!empty($users['field_home_estimator_value'])) {
            $estimator_mail = db_select('users', 'u');
            $estimator_mail->fields('u', ['mail']);
            $estimator_mail->condition('u.uid', $users['field_home_estimator_value'], '=');
            $mails[] = $estimator_mail->execute()->fetchField();

          }
          // Manager.
          if (!empty($users['field_request_manager_target_id'])) {
            $manager_mail = db_select('users', 'u');
            $manager_mail->fields('u', ['mail']);
            $manager_mail->condition('u.uid', $users['field_request_manager_target_id'], '=');
            $mails[] = $manager_mail->execute()->fetchField();
          }
          // Client.
          if (!empty($users['field_e_mail_email'])) {
            $mails[] = $users['field_e_mail_email'];
          }
          // Send sms to client.
          if (!empty($node->uid)) {
            (new Sms(SmsActionTypes::HOME_ESTIMATE_REMINDER))->execute($node->nid, $node, $node->uid);
            (new DelayLaunch())->createTask(TaskType::SMS_SEND, [
              'actionId' => SmsActionTypes::HOME_ESTIMATE_REMINDER,
              'nid' => $node->nid,
              'node' => $node,
              'uid' => $node->uid,
              'user' => $user->uid,
            ]);
          }
          if ($mails) {
            $template_builder->moveTemplateBuilderSendTemplatesTo($node, $template, $mails, EntityTypes::MOVEREQUEST);
          }
        }
      }
    }

  }

}
