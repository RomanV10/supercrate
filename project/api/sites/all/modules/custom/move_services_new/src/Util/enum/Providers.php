<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class AmountOption.
 *
 * @package Drupal\move_services_new\Util
 */
class Providers extends BaseEnum {

  const MOVERS123 = 0;
  const VANLINES = 1;
  const MOVING_COM = 2;
  const HOMEADVISOR = 3;
  const EQUATEMEDIA = 4;
  const MOVERS_COM = 5;
  const MY_MOVING_LOADS = 6;
  const BILLY_COM = 7;
  const WEBFIXE = 8;
  const IRELOCATE = 9;
  const HOMEBULLETIN = 10;
  const IRELOCATE_POST = 11;
  const EQUATEMEDIA_POST = 12;
  const MOVING_COM_POST = 13;
  const TOP_MOVERS_QUOTE_POST = 14;
  const QUOTE_RUNNER_LEAD = 15;
  const NETWORK_MOVING_COM_POST = 16;
  const GLOBAL_VANLINES_POST = 17;
  const MOVING_RELOCATION_POST = 18;
  const GOODMOVERS_POST = 19;
  const TOPMOVINGCOMPANY4U_POST = 21;
  const SMARTSWIFTLEAD_POST = 22;
  const MOVINGCOMPANYREVIEWS_POST = 23;

}
