<?php

namespace Drupal\move_services_new\Exceptions;

/**
 * Class ValidationException
 *
 * @package Drupal\move_services_new\Exceptions
 */
class ValidationException extends \ServicesException {

  private static $statusMessage = 'Bad Request';
  private static $statusCode = 422;

  /**
   * ValidationException constructor.
   *
   * @param array $validation_errors
   */
  public function __construct(array $validation_errors) {
    $response_data = self::prepareJsonResponseData(
      t('Form validation error. Try again.'),
      $validation_errors
    );

    parent::__construct(self::$statusMessage, self::$statusCode, $response_data);
  }

  /**
   * TODO: Use global template.
   *
   * @param $error_message
   * @param array $validation_errors
   *
   * @return array
   */
  private static function prepareJsonResponseData($error_message, array $validation_errors): array {
    return array(
      'status' => FALSE,
      'status_code' => self::$statusCode,
      'status_message' => self::$statusMessage,
      'response' => array(
        'error_message' => $error_message,
        'validation_errors' => $validation_errors,
      ),
    );
  }

}
