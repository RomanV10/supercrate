<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_services_new\Services\move_request\MoveRequestRetrieve;

/**
 * Class Cache.
 *
 * @package Drupal\move_services_new\Services
 */
class Cache {

  /**
   * Update cache data of "move request" service.
   *
   * @param int $nid
   *   Node id.
   * @param array|null $data
   *   Data to set cache.
   * @param string $db_name
   *   Table name.
   * @param bool $node_wrapper
   *   Node wrapper.
   * @param bool $init
   *   Flag: init request(create) or update.
   *
   * @throws \Exception
   */
  public static function updateCacheData($nid, ?array $data = array(), $db_name = 'move_services_cache', $node_wrapper = FALSE, bool $init = FALSE) {
    // Otherwise will have notice when foreman assign in dispatch.
    if (!$node_wrapper) {
      $node_wrapper = entity_metadata_wrapper('node', $nid);
    }

    $retrieve = new MoveRequestRetrieve($node_wrapper, $init);
    if (empty($data)) {
      $data = $retrieve->nodeFields();
    }

    $record = array(
      'nid' => $nid,
      'data' => serialize($data),
    );

    if ($db_name == 'move_services_cache') {
      $record['small_data'] = serialize($retrieve->smallNodeFields($data));
    }

    drupal_write_record($db_name, $record, 'nid');
  }

  /**
   * Get cache of "move request" from DB.
   *
   * @param int $nid
   *   Node id.
   * @param string $db_name
   *   Table name.
   * @param int $type
   *   Flag: 1 - Normal cache; 0 - Small Cache.
   *
   * @return array|mixed
   *   Unserialized data from DB.
   */
  public static function getCacheData(int $nid, $db_name = 'move_services_cache', $type = 1) {
    $data = array();

    $cache = db_select($db_name, 'msc')
      ->fields('msc')
      ->condition('msc.nid', $nid, '=')
      ->execute()
      ->fetchObject();

    if ($type && $cache && $cache->data) {
      $data = unserialize($cache->data);
    }
    elseif (!$type && $cache && $cache->small_data) {
      $data = unserialize($cache->small_data);
    }
    return $data;
  }

  /**
   * Set cache of "move request" service.
   *
   * @param int $nid
   *   Node id.
   * @param array $data
   *   Data to set cache.
   * @param string $db_name
   *   Table name.
   * @param array $small_data
   *   Small data.
   *
   * @throws \Exception
   */
  public static function setCacheData(int $nid, array $data = array(), string $db_name = 'move_services_cache', array $small_data = array()) {
    $fields = array('nid' => $nid, 'data' => serialize($data));
    if ($small_data) {
      $fields['small_data'] = serialize($small_data);
    }

    db_merge($db_name)
      ->key(array('nid' => $nid))
      ->fields($fields)
      ->execute();
  }

  /**
   * Remove data from cache table.
   *
   * @param int $nid
   *   Node id.
   * @param string $db_name
   *   Table name.
   */
  public static function removeCacheData($nid, $db_name = 'move_services_cache') {
    db_delete($db_name)
      ->condition('nid', $nid)
      ->execute();
  }

}
