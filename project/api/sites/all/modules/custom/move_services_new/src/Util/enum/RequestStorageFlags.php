<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RequestStorageFlags.
 *
 * @package Drupal\move_storage\Services
 */
class RequestStorageFlags extends BaseEnum {

  const PENDING = 1;
  const MOVEIN = 2;
  const MOVEOUT = 3;
  const MOVEOUT_PENDING = 4;

}
