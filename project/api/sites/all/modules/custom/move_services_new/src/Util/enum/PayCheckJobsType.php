<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class PayCheckJobsType.
 *
 * @package Drupal\move_services_new\Util
 */
class PayCheckJobsType extends BaseEnum {

  const NORMALJOB = 0;
  const HOURLYMISC = 1;

}
