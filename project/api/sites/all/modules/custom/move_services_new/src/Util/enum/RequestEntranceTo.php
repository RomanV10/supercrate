<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RequestEntranceTo.
 *
 * @package Drupal\move_services_new\Util
 */
class RequestEntranceTo extends BaseEnum {
  const NO_STAIRS_GROUND_FLOOR = 1;
  const STAIRS_2ND_FLOOR = 2;
  const STAIRS_3ND_FLOOR = 3;
  const STAIRS_4ND_FLOOR = 4;
  const STAIRS_5ND_FLOOR = 5;
  const ELEVATOR = 6;
  const PRIVATE_HOUSE = 7;

}
