<?php

namespace Drupal\move_services_new\Cron\Tasks;

use Drupal\move_services_new\Util\enum\HomeEstimateStatus;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class GetHomeEstimateReminders.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
class GetHomeEstimateReminders implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $today = date("Y-m-d 00:00:01");
    $plus_one_days = date('Y-m-d 23:59:59', strtotime("+1 days"));

    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_home_estimate_date', 'hed', 'n.nid = hed.entity_id');
    $query->leftJoin('field_data_field_approve', 'f_approve', 'f_approve.entity_id = n.nid');
    $query->leftJoin('field_data_field_home_estimate_status', 'he_status', 'he_status.entity_id = n.nid');
    $query->fields('n', array('nid'));
    $query->condition('f_approve.bundle', 'move_request');
    $query->condition('hed.field_home_estimate_date_value', [$today, $plus_one_days], 'BETWEEN');
    $query->condition('f_approve.field_approve_value', RequestStatusTypes::INHOME_ESTIMATE, '=');
    $query->condition('he_status.field_home_estimate_status_value', HomeEstimateStatus::SCHEDULE, '=');
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($result)) {
      $queue = \DrupalQueue::get('move_services_new_send_home_estimate_reminders');
      $queue->createQueue();
      $chunk = array_chunk($result, 2);
      foreach ($chunk as $item) {
        $queue->createItem($item);
      }
    }
  }

}
