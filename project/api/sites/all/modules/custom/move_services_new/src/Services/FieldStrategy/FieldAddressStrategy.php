<?php

namespace Drupal\move_services_new\Services\FieldStrategy;

/**
 * Class fieldAddressStrategy.
 *
 * @package Drupal\move_services_new\Services\FieldStrategy
 */
class FieldAddressStrategy implements FieldArrayStrategyInterface {

  /**
   * Create data.
   *
   * @param mixed $values
   *   Value os field.
   * @param string $widget_type
   *   The widget type.
   * @param int $count_values
   *   Count values.
   * @param bool $multi
   *   Indicated that field is multi.
   * @param array $field_info
   *   Information about field.
   *
   * @return mixed
   *   Result
   */
  public function createData($values, string $widget_type, int $count_values, bool $multi = FALSE, array $field_info = array()) {
    // Recalculating count_values.
    if (!array_key_exists(0, $values)) {
      $count_values = 1;
    }

    $data = array();
    if ($multi && $count_values >= 2) {
      for ($i = 0; $i < $count_values; $i++) {
        $data[LANGUAGE_NONE][$i] = $values[$i];
      }
    }
    else {
      $data[LANGUAGE_NONE][0] = $values;
    }

    return $data;
  }

}
