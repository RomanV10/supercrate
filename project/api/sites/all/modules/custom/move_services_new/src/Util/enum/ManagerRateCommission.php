<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class ManagerRateCommision.
 *
 * @package Drupal\move_services_new\Util
 */
class ManagerRateCommission extends BaseEnum {

  const OFFICECOMMISSION = 0;
  const OFFICEWITHVISUAL = 1;
  const VISUALESTIMATE = 2;

}
