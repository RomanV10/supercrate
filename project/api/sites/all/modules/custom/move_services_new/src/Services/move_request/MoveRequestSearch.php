<?php

namespace Drupal\move_services_new\Services\move_request;

use Drupal\move_request_score\Services\MoveRequestScore;
use Drupal\move_services_new\Services\Cache;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Services\Sales;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\RequestSearchTypes;
use Drupal\move_long_distance\Services\LongDistanceTrip;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class MoveRequestSearch.
 *
 * @package Drupal\move_services_new\move_request
 */
class MoveRequestSearch extends MoveRequest {

  const FILTER_CONFIRMED_BY_CONFIRMED_DATE = 1;
  const FILTER_CONFIRMED_BY_MOVE_DATE = 2;
  const FILTER_CONFIRMED_BY_ALL_DATE_FROM_BEGINNING = 3;
  const FILTER_CONFIRMED_BY_ALL_DATE_TO_THE_END = 4;

  /**
   * The condition for operation.
   *
   * @var array
   */
  protected $conditions = array();
  /**
   * The type of operation. 0 - Default, for search operation.
   *
   * 1 - For requests operation.
   *
   * @var int
   */
  protected $type;
  /**
   * The number of page.
   *
   * @var int
   */
  private $page = 0;
  /**
   * The page size.
   *
   * @var int
   */
  private $pageSize = 25;
  /**
   * Filtering options.
   *
   * @var array
   */
  private $filtering = array();
  /**
   * Sequentially sorting.
   *
   * @var array
   */
  private $sorting = array();
  /**
   * Flag: 0 - Disable; 1 - Confirmed booked date; 2 - Confirmed move date.
   *
   * @var int
   */
  private $confirmed = 0;
  /**
   * Display result type.
   *
   * @var int
   */
  private $displayType;
  /**
   * Is user home estimator.
   *
   * @var bool
   */
  private $isHomeEstimator = FALSE;

  /**
   * Admin flag for authenticated user.
   *
   * @var bool
   */
  private $isAdmin = FALSE;

  /**
   * User id.
   *
   * @var int
   */
  private $uid;

  /**
   * Use this date for all fields.
   *
   * @var array
   */
  private $allDates = [];

  /**
   * Constructor of MoveRequestSearch service.
   *
   * @param array $arguments
   *   Array with arguments.
   * @param int $type
   *   The type of operation.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function __construct(array $arguments = array(), int $type = 0) {
    parent::__construct();

    $this->isAdmin = $this->clients->checkUserRoles(array('administrator'));
    $this->isHomeEstimator = $this->clients->checkUserRoles(array('home-estimator'));
    $this->uid = $this->user->uid;
    $this->type = $type;

    $this->displayType = RequestSearchTypes::FULL;
    if (isset($arguments['display_type'])) {
      $this->displayType = (int) $arguments['display_type'];
    }

    if (isset($arguments['page'])) {
      $this->page = (int) $arguments['page'];
    }

    if (isset($arguments['pagesize'])) {
      $this->pageSize = (int) $arguments['pagesize'];
    }

    if (isset($arguments['filtering'])) {
      $this->filtering = (array) $arguments['filtering'];
    }

    // Check fields of conditions.
    if (isset($arguments['condition'])) {
      $this->conditions = (array) $arguments['condition'];
    }

    if (isset($arguments['confirmed'])) {
      $this->confirmed = (int) $arguments['confirmed'];
    }

    if (isset($arguments['sorting'])) {
      $this->sorting = (array) $arguments['sorting'];
    }

    if (isset($arguments['all_dates']) && is_array($arguments['all_dates'])) {
      $this->allDates = $arguments['all_dates'];
    }
  }

  /**
   * Getter for uid.
   *
   * @return int
   *   User id.
   */
  public function getUserId() : int {
    return $this->uid;
  }

  /**
   * Get nodes of move_request type by conditions.
   *
   * @param bool $pagination
   *   Flag  indicates that needed usage pagination.
   *
   * @return array
   *   Array with founded nodes.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \SearchApiException
   * @throws \ServicesException
   */
  public function search(bool $pagination = TRUE) : array {
    $results = array();
    $results['nodes'] = array();
    $this->checkPageSize('node');
    $query = search_api_query('move_request');

    // Filter by score.
    if (!empty($this->conditions['field_total_score'])) {
      MoveRequestScore::filterRequestByScore($query, $this->conditions['field_total_score']);
      unset($this->conditions['field_total_score']);
    }

    if ($this->confirmed) {
      $this->filterConfirmedByDate($query);
    }

    if ($this->displayType == RequestSearchTypes::TRIP) {
      if (empty($this->conditions['field_date']) && empty($this->conditions['field_delivery_date'])) {
        $query->condition('field_added_to_trip', 0);
      }
      else {
        LongDistanceTrip::dateFiltering($this->conditions, $query);
        unset($this->conditions['field_date']);
        unset($this->conditions['field_delivery_date']);
      }

      $query->condition('field_move_service_type', RequestServiceType::LONG_DISTANCE);
    }

    if ($this->conditions) {
      foreach ($this->conditions as $field_name => $value) {
        $this->conditions($query, $field_name, $value);
      }
    }

    // Manager permissions.
    if ($this->isManager && !$this->isHomeEstimate()) {
      $manager_condition = array();
      $check_perm = function (string $perm) {
        return (new Sales())->checkManagerPermissions($perm);
      };

      // Manager can see own and can see unsigned.
      if (!$check_perm('canSeeOtherLeads') && $check_perm('canSeeUnsignedLeads')) {
        $manager_condition[] = array($this->uid, '=');
        $manager_condition[] = array(NULL, '=');
      }
      // Manager can see own and theirs but do not see unsigned.
      elseif ($check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
        $manager_condition[] = array(NULL, '<>');
      }
      // Manager can see only own requests.
      elseif (!$check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
        $manager_condition[] = array($this->uid, '=');
      }

      // If manager have all permission for view all request then
      // do not apply conditions.
      if ($manager_condition) {
        $this->conditions($query, 'field_request_manager', $manager_condition);
      }
    }

    // If user was login from home estimate and has role estimate.
    // Use conditions.
    if ($this->isHomeEstimate()) {
      $this->conditions($query, 'field_home_estimator', $this->uid);
    }

    // Date filtering if necessary.
    $this->dateFiltering($query);

    if ($this->sorting) {
      $direction = 'DESC';
      if (isset($this->sorting['direction']) && in_array($this->sorting['direction'], array('ASC', 'DESC'))) {
        $direction = (string) $this->sorting['direction'];
      }
      $field = isset($this->sorting['field']) ? (string) $this->sorting['field'] : 'nid';
      $query->sort($field, $direction);
    }
    else {
      $query->sort('created', 'DESC');
    }

    $data = $query->execute();
    $results['count'] = &$data['result count'];

    // Filtration by foreman.
    if (array_key_exists('field_foreman', $this->conditions)) {
      $close = array_key_exists('field_company_flags', $this->conditions) && in_array(3, $this->conditions['field_company_flags']) ? TRUE : FALSE;
      $foreman_uid = (int) $this->conditions['field_foreman'];
      /* TODO need to remove this check after front will fix
      search query after logout.*/
      if (!empty($foreman_uid)) {
        $nids = array_keys($data['results']);
        $approve_conditions = !empty($this->conditions['field_approve']) ? $this->conditions['field_approve'] : array();
        $this->filterByForeman($foreman_uid, $nids, $close, '', '', array($approve_conditions));
        $data['results'] = $nids;
      }
      $data['results'] = array_flip($data['results']);
      $data['result count'] = count($data['results']);
    }

    // Custom pagination.
    if ($pagination) {
      $data['results'] = array_slice($data['results'], $this->page * $this->pageSize, $this->pageSize, TRUE);
    }

    /*
     * Nodes with all fields(full version).
     *
     * @param array $data
     */
    $nodes = function (array $data) use (&$results) {
      $nids = array_keys($data['results']);
      foreach ($nids as $nid) {
        $obj = new MoveRequest($nid);
        $results['nodes'][] = $obj->retrieve();
      }
    };

    if (isset($data['results'])) {
      switch ($this->displayType) {
        case RequestSearchTypes::TRIP:
          $results['nodes'] = LongDistanceTrip::getNewRequestTrips(array_keys($data['results']));
          break;

        case RequestSearchTypes::HOME_ESTIMATE:
          $results['nodes'] = $this->getHomeEstimateRequest(array_keys($data['results']));
          break;

        case RequestSearchTypes::SHORT:
          $results['nodes'] = $this->getShortRequests(array_keys($data['results']), FALSE);
          break;

        case RequestSearchTypes::FULL:
          $nodes($data);
          break;

        case RequestSearchTypes::QUICK_BOOKS:
          $results['nodes'] = array_keys($data['results']);
          break;

        case RequestSearchTypes::PROFIT_LOSE:
          $results['items'] = $this->getRequestForProfitLoss(array_keys($data['results']));
          unset($results['nodes']);
          break;
      }
    }

    return $results;
  }

  /**
   * Helper function to check pageSize for current user.
   *
   * @param string $name_res
   *   Resource name.
   */
  private function checkPageSize($name_res) {
    $default_limit = variable_get("services_{$name_res}_index_page_size", 20);
    if (!user_access('perform unlimited index queries') && $this->pageSize > $default_limit) {
      $this->pageSize = $default_limit;
    }
  }

  /**
   * Return first and last time of current month.
   *
   * @param bool $timestamp
   *   Flag: FALSE - return string of date; TRUE - return timestamp.
   *
   * @return array
   *   Result of operation.
   */
  public function firstLastCurrentMonthTime(bool $timestamp = FALSE) {
    $first = Extra::getFirstDayCurrentMonth($this->timeZone);
    $last = Extra::getLastDayCurrentMonth($this->timeZone);

    return array(
      'first' => $timestamp ? $first : date('Y-m-d H:i:s', $first),
      'last' => $timestamp ? $last : date('Y-m-d H:i:s', $last),
    );
  }

  /**
   * Add conditions for each field.
   *
   * @param \SearchApiQueryInterface $query
   *   Search Api query.
   * @param string $field_name
   *   Name of field.
   * @param string|int|array $value
   *   Value of search fields.
   * @param string $operator
   *   Operator condition.
   * @param string $filter2
   *   Filter for conditions.
   */
  private function conditions(\SearchApiQueryInterface &$query, $field_name, $value, $operator = '=', $filter2 = "OR") {
    // @todo Check what field exist in index.
    if (is_array($value)) {
      $filter = $query->createFilter($filter2);
      foreach ($value as $val) {
        if (is_array($val)) {
          list($val2, $operator2) = $val;
          $filter->condition($field_name, $val2, $operator2);
        }
        else {
          $filter->condition($field_name, $val, $operator);
        }

      }
      $query->filter($filter);
    }
    else {
      $query->condition($field_name, $value, $operator);
    }
  }

  /**
   * Additional condition for filter date.
   *
   * @param \SearchApiQueryInterface $query
   *   Search Api query.
   */
  private function dateFiltering(\SearchApiQueryInterface &$query) {
    $is_from = isset($this->filtering['date_from']);
    $is_to = isset($this->filtering['date_to']);
    $is_create = isset($this->filtering['created']);
    $is_date = isset($this->filtering['field_date']);
    $is_date_from = isset($this->filtering['field_date_from']);
    $is_date_to = isset($this->filtering['field_date_to']);
    $is_home_estimate = isset($this->filtering['field_home_estimate_date']);
    $is_home_estimate_from = isset($this->filtering['home_estimate_date_from']);
    $is_home_estimate_to = isset($this->filtering['home_estimate_date_to']);

    if ($is_from || $is_to || $is_create || $is_date || $is_date_from || $is_date_to || $is_home_estimate || $is_home_estimate_from || $is_home_estimate_to) {
      $date_from = isset($this->filtering['date_from']) ? $this->filtering['date_from'] : '';
      $date_to = isset($this->filtering['date_to']) ? $this->filtering['date_to'] : '';
      $created = isset($this->filtering['created']) ? $this->filtering['created'] : '';
      $field_date = isset($this->filtering['field_date']) ? $this->filtering['field_date'] : '';
      $field_date_from = isset($this->filtering['field_date_from']) ? $this->filtering['field_date_from'] : '';
      $field_date_to = isset($this->filtering['field_date_to']) ? $this->filtering['field_date_to'] : '';
      $date_home_estimate = !empty($this->filtering['field_home_estimate_date']) ? $this->filtering['field_home_estimate_date'] : '';
      $date_home_estimate_from = !empty($this->filtering['home_estimate_date_from']) ? $this->filtering['home_estimate_date_from'] : '';
      $date_home_estimate_to = !empty($this->filtering['home_estimate_date_to']) ? $this->filtering['home_estimate_date_to'] : '';

      if ($date_from && !$date_to) {
        $query->condition('created', $date_from, '>=');
      }
      elseif (!$date_from && $date_to) {
        $query->condition('created', $date_to, '<=');
      }
      elseif ($date_from && $date_to) {
        $query->condition('created', array($date_from, $date_to), 'BETWEEN');
      }
      elseif ($created && $field_date) {
        $created_tomorrow = $this->filtering['created'];
        $query->condition('created', array($created, $created_tomorrow), 'BETWEEN');

        $field_date_tomorrow = $this->filtering['field_date'];
        $query->condition('field_date', array($field_date, $field_date_tomorrow), 'BETWEEN');
      }
      elseif ($created || $field_date) {
        $date_var = ($created) ? $created : $field_date;
        $date_symb = ($created) ? 'created' : 'field_date';
        $tomorrow = $this->filtering[$date_symb] + 86399;
        $query->condition($date_symb, array($date_var, $tomorrow), 'BETWEEN');
      }
      elseif ($field_date_from && !$field_date_to) {
        $query->condition('field_date', $field_date_from, '>=');
      }
      elseif (!$field_date_from && $field_date_to) {
        $query->condition('field_date', $field_date_to, '<=');
      }
      elseif ($field_date_from && $field_date_to) {
        $tomorrow = $this->filtering['field_date_to'];
        $query->condition('field_date', array($field_date_from, $tomorrow), 'BETWEEN');
      }
      elseif ($date_home_estimate) {
        $tomorrow = $date_home_estimate + 86399;
        $query->condition('field_home_estimate_date', array($date_home_estimate, $tomorrow), 'BETWEEN');
      }
      elseif ($date_home_estimate_from && $date_home_estimate_to) {
        $query->condition('field_home_estimate_date', array($date_home_estimate_from, $date_home_estimate_to), 'BETWEEN');
      }
    }
  }

  /**
   * TODO need to use validation rules. Validation code in dev branch now.
   *
   * Filter search result by dates and confirm type.
   *
   * @param \SearchApiQueryInterface $query
   *
   */
  public function filterConfirmedByDate(\SearchApiQueryInterface &$query) {

    $this->conditions = [];
    $query->condition('field_approve', RequestStatusTypes::CONFIRMED, '=');

    switch ($this->confirmed) {
      case self::FILTER_CONFIRMED_BY_CONFIRMED_DATE:
        $date = $this->firstLastCurrentMonthTime(TRUE);

        $query->condition('field_date_confirmed', [$date['first'], $date['last']], 'BETWEEN');
        break;

      case self::FILTER_CONFIRMED_BY_MOVE_DATE:
        $date = $this->firstLastCurrentMonthTime(TRUE);

        $query->condition('field_date', [$date['first'], $date['last']], 'BETWEEN');
        break;

      case self::FILTER_CONFIRMED_BY_ALL_DATE_FROM_BEGINNING:
        $date = $this->firstLastCurrentMonthTime(TRUE);

        $date['first'] = 0;
        $or = $query->createFilter('OR');
        $or->condition('field_date_confirmed', [$date['first'], $date['last']], 'BETWEEN');
        $or->condition('field_date', [$date['first'], $date['last']], 'BETWEEN');

        $query->filter($or);
        break;

      case self::FILTER_CONFIRMED_BY_ALL_DATE_TO_THE_END:
        $date['first'] = Extra::getDateFrom('now');
        $date['last'] = 9999999999;

        $query->condition('field_date', [$date['first'], $date['last']], 'BETWEEN');
        break;
    }
  }

  /**
   * Global filter requests by foreman.
   *
   * @param int $uid
   *   Foreman id.
   * @param array $nids
   *   Array with nids.
   * @param bool $close_flag
   *   Close/Open contract.
   */
  public function filterByForeman(int $uid, array &$nids, bool $close_flag = FALSE, $date_from = '', $date_to = '', $status_to_show = array()) : void {
    if (!$nids) {
      return;
    }

    // Find All requests.
    $select = db_select('node', 'n')
      ->fields('n', array('nid'));
    $select->distinct('n.nid');
    $select->leftJoin('field_data_field_foreman', 'ff', 'ff.entity_id = n.nid');
    $select->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('n.nid', $nids, 'IN')
      ->condition('ff.field_foreman_target_id', $uid);
    $all_requests_nodes = $select->execute()->fetchCol();

    $filter_flatrate = $this->requestFilterFlatRateForeman($uid, $close_flag, $all_requests_nodes, $date_from, $date_to, $status_to_show);
    $filter_not_flatrate = $this->requestFilterNotFlatRateForeman($uid, $nids, $close_flag, $status_to_show);
    $filter_local_flatrate = $this->requestFilterLocalFlatRateForeman($uid, $nids, $close_flag, $status_to_show);
    $filter_foreman_as_helper = $this->requestFilterForemanAsHelper($uid, $nids, $close_flag, $status_to_show);
    $result = array_merge($filter_flatrate, $filter_not_flatrate, $filter_local_flatrate, $filter_foreman_as_helper);
    $nids = array_unique($result);
  }

  private function requestFilterFlatRateForeman($uid, $close_flag, $all_requests_nodes, $date_from = '', $date_to = '', $status_to_show = array()) {
    $flatrate_requests = $this->requestFlatRateForemanBelong($uid, $all_requests_nodes);
    // Find all flatrate by foreman or flatrate with closed flags by foreman.
    $select = db_select('node', 'n')
      ->fields('n', array('nid'));
    $select->distinct('n.nid');
    $select->leftJoin('field_data_field_foreman', 'ff', 'ff.entity_id = n.nid');
    $select->leftJoin('field_data_field_move_service_type', 'fms', 'fms.entity_id = n.nid');
    $select->leftJoin('field_data_field_approve', 'fdfa', 'fdfa.entity_id = n.nid');
    $select->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('ff.field_foreman_target_id', $uid)
      ->condition('fms.field_move_service_type_value', 5)
      ->condition('fdfa.field_approve_value', 22, '<>');
    if ($close_flag) {
      $select->leftJoin('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select->condition('fcf.field_company_flags_value	', array(5, 6), 'IN');
    }
    if (!empty($status_to_show)) {
      $select->condition('fdfa.field_approve_value', $status_to_show, 'IN');
    }
    $for_filters = $select->execute()->fetchCol();

    if ($for_filters) {
      $filter_select = function (array $for_filters) use ($uid, $date_from, $date_to) {
        $select2 = db_select('node', 'n')
          ->fields('n', array('nid'))
          ->fields('fcf', array('field_company_flags_value'));
        $select2->join('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
        $select2->condition('n.nid', $for_filters, 'IN');
        $array2 = array();
        $select3 = clone $select2;
        if ($date_from || $date_to) {
          $prepare_date_from = date("Y-m-d H:i:s", $date_from);
          $prepare_date_to = date("Y-m-d H:i:s", $date_to);

          $select2->leftJoin('field_data_field_foreman_pickup', 'fp', 'fp.entity_id = n.nid');
          $select2->condition('fp.field_foreman_pickup_target_id', $uid);
          $select2->leftJoin('field_data_field_date', 'fd', 'fd.entity_id = n.nid');
          $select2->condition('fd.field_date_value', array($prepare_date_from, $prepare_date_to), 'BETWEEN');

          $select3->leftJoin('field_data_field_foreman_delivery', 'fd', 'fd.entity_id = n.nid');
          $select3->condition('fd.field_foreman_delivery_target_id', $uid);
          $select3->leftJoin('field_data_field_delivery_date', 'fdd', 'fdd.entity_id = n.nid');
          $select3->condition('fdd.field_delivery_date_value', array($prepare_date_from, $prepare_date_to), 'BETWEEN');
          $array2 = $select3->execute()->fetchAll();
        }

        $array = $select2->execute()->fetchAll();

        $array_merged = array_merge($array, $array2);
        $temp = array();
        foreach ($array_merged as $flags) {
          // Cache local flatrate.
          $is_flat_rate = &drupal_static(__METHOD__ . "tmp:" . $flags->nid, NULL);

          // Cache initialize.
          if ($is_flat_rate == NULL) {
            $is_flat_rate = Payroll::isFlatRateLocal($flags->nid);
          }

          // Besides local flatrate.
          if ($is_flat_rate == FALSE) {
            $temp[$flags->nid][] = $flags->field_company_flags_value;
          }
        }

        return $temp;
      };

      if (!$close_flag) {
        $temp = $filter_select($for_filters);

        foreach ($temp as $nid_key => $flags) {
          // Search in pickup data.
          $in_pickup = in_array($nid_key, $flatrate_requests['pickup']) && in_array(5, $flags);

          // Search in delivery data.
          $in_delivery = in_array($nid_key, $flatrate_requests['delivery']) && in_array(6, $flags);

          if ($in_pickup || $in_delivery) {
            unset($temp[$nid_key]);
          }
        }

        $for_filters = array_keys($temp);
      }
      else {
        $temp = $filter_select($for_filters);

        foreach ($temp as $nid_key => $flags) {
          // Search in pickup data.
          $in_pickup = in_array($nid_key, $flatrate_requests['pickup']) && in_array(5, $flags);

          // Search in delivery data.
          $in_delivery = in_array($nid_key, $flatrate_requests['delivery']) && in_array(6, $flags);

          if (!$in_pickup && !$in_delivery) {
            unset($temp[$nid_key]);
          }
        }

        $for_filters = array_keys($temp);
      }
    }

    return $for_filters;
  }

  private function requestFlatRateForemanBelong($uid, array $nids) : array {
    $data = array('pickup' => array(), 'delivery' => array());

    if ($nids) {
      $data['pickup'] = db_select('field_data_field_foreman_pickup', 'fd')
        ->fields('fd', array('entity_id'))
        ->condition('fd.field_foreman_pickup_target_id', $uid)
        ->condition('fd.entity_id', $nids, 'IN')
        ->execute()
        ->fetchCol();

      $data['delivery'] = db_select('field_data_field_foreman_delivery', 'fd')
        ->fields('fd', array('entity_id'))
        ->condition('fd.field_foreman_delivery_target_id', $uid)
        ->condition('fd.entity_id', $nids, 'IN')
        ->execute()
        ->fetchCol();
    }

    return $data;
  }

  private function requestFilterNotFlatRateForeman($uid, array $nids, $close_flag = FALSE, array $status_to_show = array()) {
    $types = array(1, 2, 3, 4, 6, 7);

    // Find All requests.
    $select = db_select('node', 'n')
      ->fields('n', array('nid'));
    $select->distinct('n.nid');
    $select->leftJoin('field_data_field_foreman', 'ff', 'ff.entity_id = n.nid');
    $select->leftJoin('field_data_field_move_service_type', 'fms', 'fms.entity_id = n.nid');
    $select->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('n.nid', $nids, 'IN')
      ->condition('ff.field_foreman_target_id', $uid)
      ->condition('fms.field_move_service_type_value', $types, 'IN');
    if ($close_flag) {
      $select->leftJoin('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select->condition('fcf.field_company_flags_value	', array(3), 'IN');
    }

    // Add filtering by status(CONFIRMED, PENDING etc...).
    if (!empty($status_to_show)) {
      $select->leftJoin('field_data_field_approve', 'fdfa', 'fdfa.entity_id = n.nid');
      $select->condition('fdfa.field_approve_value', $status_to_show, 'IN');
    }

    $all_requests_nodes = $select->execute()->fetchCol();

    if (!$close_flag && $all_requests_nodes) {
      $select2 = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->fields('fcf', array('field_company_flags_value'));
      $select2->join('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select2->condition('n.nid', $all_requests_nodes, 'IN');
      $results2 = $select2->execute()->fetchAll();
      $temp = array();
      foreach ($results2 as $flags) {
        $temp[$flags->nid][] = $flags->field_company_flags_value;
      }

      foreach ($temp as $nid_key => $flags) {
        $in = in_array(3, $flags);
        if ($in) {
          $key = array_search($nid_key, $all_requests_nodes);
          unset($all_requests_nodes[$key]);
        }
      }
    }

    return $all_requests_nodes;
  }

  private function requestFilterLocalFlatRateForeman($uid, array $nids, $close_flag = FALSE, array $status_to_show = array()) {
    // Find All requests.
    $select = db_select('node', 'n')
      ->fields('n', array('nid'));
    $select->distinct('n.nid');
    $select->leftJoin('field_data_field_foreman', 'ff', 'ff.entity_id = n.nid');
    $select->leftJoin('field_data_field_move_service_type', 'fms', 'fms.entity_id = n.nid');
    $select->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('n.nid', $nids, 'IN')
      ->condition('ff.field_foreman_target_id', $uid)
      ->condition('fms.field_move_service_type_value', 5);
    if ($close_flag) {
      $select->leftJoin('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select->condition('fcf.field_company_flags_value	', array(3), 'IN');
    }
    // Add filtering by status(CONFIRMED, PENDING etc...).
    if (!empty($status_to_show)) {
      $select->leftJoin('field_data_field_approve', 'fdfa', 'fdfa.entity_id = n.nid');
      $select->condition('fdfa.field_approve_value', $status_to_show, 'IN');
    }

    $result_nids = $select->execute()->fetchCol();

    $all_requests_nodes = array();

    foreach ($result_nids as $nid) {
      if (Payroll::isFlatRateLocal($nid)) {
        $all_requests_nodes[] = $nid;
      }
    }

    if (!$close_flag && $all_requests_nodes) {
      $select2 = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->fields('fcf', array('field_company_flags_value'));
      $select2->join('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select2->condition('n.nid', $all_requests_nodes, 'IN');
      $results2 = $select2->execute()->fetchAll();
      $temp = array();
      foreach ($results2 as $flags) {
        $temp[$flags->nid][] = $flags->field_company_flags_value;
      }

      foreach ($temp as $nid_key => $flags) {
        $in = in_array(3, $flags);
        if ($in) {
          $key = array_search($nid_key, $all_requests_nodes);
          unset($all_requests_nodes[$key]);
        }
      }
    }

    return $all_requests_nodes;
  }

  private function requestFilterForemanAsHelper($uid, $nids, $close_flag, array $status_to_show = array()) {
    $select = db_select('node', 'n')
      ->fields('n', array('nid'));
    $select->distinct('n.nid');
    $select->leftJoin('field_data_field_helper', 'ff', 'ff.entity_id = n.nid');
    $select->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('n.nid', $nids, 'IN')
      ->condition('ff.field_helper_target_id', $uid);
    if ($close_flag) {
      $select->leftJoin('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select->condition('fcf.field_company_flags_value	', array(3), 'IN');
    }

    // Add filtering by status(CONFIRMED, PENDING etc...).
    if (!empty($status_to_show)) {
      $select->leftJoin('field_data_field_approve', 'fdfa', 'fdfa.entity_id = n.nid');
      $select->condition('fdfa.field_approve_value', $status_to_show, 'IN');
    }

    $result_nids = $select->execute()->fetchCol();

    if (!$close_flag && $result_nids) {
      $select2 = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->fields('fcf', array('field_company_flags_value'));
      $select2->join('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select2->condition('n.nid', $result_nids, 'IN');
      $results2 = $select2->execute()->fetchAll();
      $temp = array();
      foreach ($results2 as $flags) {
        $temp[$flags->nid][] = $flags->field_company_flags_value;
      }

      foreach ($temp as $nid_key => $flags) {
        $in = in_array(3, $flags);
        if ($in) {
          $key = array_search($nid_key, $result_nids);
          unset($result_nids[$key]);
        }
      }
    }

    return $result_nids;
  }

  public function getShortRequests(array $nids, $short = TRUE, $reset_keys = TRUE) {
    $results = array();
    if ($short) {
      $nids = array_slice($nids, 0, 25);
    }

    foreach ($nids as $nid) {
      $obj = new MoveRequest($nid);
      $request = $obj->getNode($nid, 0);
      $request['request_all_data'] = MoveRequest::getRequestAllData($nid);
      if ($reset_keys) {
        $results[] = $request;
      }
      else {
        $results[$nid] = $request;
      }
    }

    return $results;
  }

  /**
   * Get home estimate data for each node.
   *
   * @param array $nids
   *   Nodes array.
   *
   * @return array
   *   Short version nodes.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function getHomeEstimateRequest(array $nids) {
    $results = array();
    $needed_fields = array(
      'home_estimate_date',
      'home_estimate_actual_start',
      'home_estimate_start_time',
      'home_estimate_time_duration',
      'home_estimate_assigned',
      'home_estimate_status',
      'home_estimator',
      'home_estimate_date_created',
      'service_type',
      'field_moving_from',
      'type_from',
      'field_moving_to',
      'type_to',
      'distance',
    );
    foreach ($nids as $nid) {
      $obj = new MoveRequest($nid);
      $request = $obj->getNode($nid, 1);
      $move_request = array('nid' => $nid);
      foreach ($needed_fields as $field_name) {
        $move_request[$field_name] = $request[$field_name];
      }
      $results[$nid] = $move_request;
    }

    return $results;
  }

  /**
   * Get response for profit lose search.
   *
   * @param array $nids
   *   Array nid.
   *
   * @return array
   *   Needed field for each nid.
   */
  public function getRequestForProfitLoss(array $nids) {
    $result = [];
    $neededRequestsFields = [
      'nid' => '',
      'date' => [],
      'field_date_confirmed' => [],
      'service_type' => [],
      'status' => [],
      'receipts' => [],
      'receipts_total' => 0.0,
      'field_grand_total_middle' => 0.0,
    ];
    foreach ($nids as $key => $nid) {
      $obj = new MoveRequest($nid);
      $request = $obj->retrieve();
      $result[$key] = array_intersect_key($request, $neededRequestsFields);
    }

    return $result;
  }

  public function openJobs() {
    $results = array();

    // Analog query.
    //$db_query = db_query("select nid from node LEFT JOIN field_data_field_company_flags as fd ON node.nid=fd.entity_id where (type = 'move_request') AND ((fd.entity_id IS NULL) OR (fd.entity_id NOT IN (select entity_id from field_data_field_company_flags where field_company_flags_value = 3 ))) GROUP BY nid;");

    $condition = db_select('field_data_field_company_flags', 'fcf2')
      ->fields('fcf2', array('entity_id'))
      ->condition('fcf2.field_company_flags_value', '3', '=');

    $db_select = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('n.type', 'move_request');
    $db_select->leftJoin('field_data_field_approve', 'fa', 'fa.entity_id = n.nid');
    $db_select->condition('fa.field_approve_value', '3', '=');
    $db_select->leftJoin('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
    $db_select->condition(db_or()
      ->condition('fcf.entity_id', '', 'IS NULL')
      ->condition('fcf.entity_id', $condition, 'NOT IN')
    );

    if ($this->filtering && !empty($this->filtering['date_from']) && !empty($this->filtering['date_to'])) {
      $date_from_str = date('Y-m-d H:i:s', $this->filtering['date_from']);
      $date_from_extra = Extra::getDateFrom($date_from_str);
      $date_from = date('Y-m-d H:i:s', $date_from_extra);
      $date_to_str = date('Y-m-d H:i:s', $this->filtering['date_to']);
      $date_to_extra = Extra::getDateFrom($date_to_str);
      $date_to = date('Y-m-d H:i:s', $date_to_extra);

      $db_select->leftJoin('field_data_field_date', 'fd', 'fd.entity_id = n.nid');
      $db_select->condition('fd.field_date_value', array($date_from, $date_to), 'BETWEEN');
    }
    $db_select->groupBy('n.nid');
    $db_select->range($this->page * $this->pageSize, $this->pageSize);

    // For total count data.
    $db_select_total = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('n.type', 'move_request');
    $db_select_total->leftJoin('field_data_field_approve', 'fa', 'fa.entity_id = n.nid');
    $db_select_total->condition('fa.field_approve_value', '3', '=');
    $db_select_total->leftJoin('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
    $db_select_total->condition(db_or()
      ->condition('fcf.entity_id', '', 'IS NULL')
      ->condition('fcf.entity_id', $condition, 'NOT IN')
    );

    if (!empty($date_from) && !empty($date_to)) {
      $db_select_total->leftJoin('field_data_field_date', 'fd', 'fd.entity_id = n.nid');
      $db_select_total->condition('fd.field_date_value', array($date_from, $date_to), 'BETWEEN');
    }
    $db_select_total->groupBy('n.nid');
    $results['count'] = $db_select_total->countQuery()->execute()->fetchField();
    $res = $db_select->execute()->fetchCol();
    $results['nodes'] = $this->getShortRequests($res, FALSE);

    return $results;
  }

  /**
   * Global filter requests by helper.
   *
   * @param int $uid
   *   Foreman id.
   * @param array $nids
   *   Array with nids.
   * @param bool $close_flag
   *   Close/Open contract.
   */
  public function filterByHelper(int $uid, array &$nids, bool $close_flag = FALSE, $date_from = '', $date_to = '') : void {
    // Find All requests.
    $select = db_select('node', 'n')
      ->fields('n', array('nid'));
    $select->distinct('n.nid');
    $select->leftJoin('field_data_field_helper', 'ff', 'ff.entity_id = n.nid');
    $select->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('n.nid', $nids, 'IN')
      ->condition('ff.field_helper_target_id', $uid);
    $all_requests_nodes = $select->execute()->fetchCol();

    $filter_flatrate = $this->requestFilterFlatRateHelper($uid, $close_flag, $all_requests_nodes, $date_from, $date_to);
    $filter_not_flatrate = $this->requestFilterNotFlatRateHelper($uid, $nids, $close_flag);
    $filter_local_flatrate = $this->requestFilterLocalFlatRateHelper($uid, $nids, $close_flag);
    $result = array_merge($filter_flatrate, $filter_local_flatrate, $filter_not_flatrate);
    $nids = array_unique($result);
  }

  private function requestFilterFlatRateHelper($uid, $close_flag, $all_requests_nodes,  $date_from = '', $date_to = '') {
    $flatrate_requests = $this->requestFlatRateHelperBelong($uid, $all_requests_nodes);
    // Find all flatrate by helper or flatrate with closed flags by helper.
    $select = db_select('node', 'n')
      ->fields('n', array('nid'));
    $select->distinct('n.nid');
    $select->leftJoin('field_data_field_helper', 'ff', 'ff.entity_id = n.nid');
    $select->leftJoin('field_data_field_move_service_type', 'fms', 'fms.entity_id = n.nid');
    $select->leftJoin('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
    $select->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('ff.field_helper_target_id', $uid)
      ->condition('fms.field_move_service_type_value', 5);
    if ($close_flag) {
      $select->condition('fcf.field_company_flags_value', array(5, 6), 'IN');
    }
    $for_filters = $select->execute()->fetchCol();

    if ($for_filters) {
      $filter_select = function (array $for_filters) use ($uid, $date_from, $date_to) {
        $select2 = db_select('node', 'n')
          ->fields('n', array('nid'))
          ->fields('fcf', array('field_company_flags_value'));
        $select2->join('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
        $select2->condition('n.nid', $for_filters, 'IN');
        $select3 = clone $select2;
        if ($date_from || $date_to) {
          $prepare_date_from = date("Y-m-d H:i:s", $date_from);
          $prepare_date_to = date("Y-m-d H:i:s", $date_to);

          $select2->leftJoin('field_data_field_helper_pickup', 'hp', 'hp.entity_id = n.nid');
          $select2->condition('hp.field_helper_pickup_target_id', $uid);
          $select2->leftJoin('field_data_field_date', 'fd', 'fd.entity_id = n.nid');
          $select2->condition('fd.field_date_value', array($prepare_date_from, $prepare_date_to), 'BETWEEN');

          $select3->leftJoin('field_data_field_helper_delivery', 'hd', 'hd.entity_id = n.nid');
          $select3->condition('hd.field_helper_delivery_target_id', $uid);
          $select3->leftJoin('field_data_field_delivery_date', 'fdd', 'fdd.entity_id = n.nid');
          $select3->condition('fdd.field_delivery_date_value', array($prepare_date_from, $prepare_date_to), 'BETWEEN');
        }

        $array = $select2->execute()->fetchAll();
        $array2 = $select3->execute()->fetchAll();

        $array_merged = array_merge($array, $array2);
        $temp = array();
        foreach ($array_merged as $flags) {
          $temp[$flags->nid][] = $flags->field_company_flags_value;
        }

        return $temp;
      };

      if (!$close_flag) {
        $temp = $filter_select($for_filters);

        foreach ($temp as $nid_key => $flags) {
          // Search in pickup data.
          $in_pickup = in_array($nid_key, $flatrate_requests['pickup']) && in_array(5, $flags);

          // Search in delivery data.
          $in_delivery = in_array($nid_key, $flatrate_requests['delivery']) && in_array(6, $flags);

          if ($in_pickup || $in_delivery) {
            unset($temp[$nid_key]);
          }
        }

        $for_filters = array_keys($temp);
      }
      else {
        $temp = $filter_select($for_filters);

        foreach ($temp as $nid_key => $flags) {
          // Search in pickup data.
          $in_pickup = in_array($nid_key, $flatrate_requests['pickup']) && in_array(5, $flags);

          // Search in delivery data.
          $in_delivery = in_array($nid_key, $flatrate_requests['delivery']) && in_array(6, $flags);

          if (!$in_pickup && !$in_delivery) {
            unset($temp[$nid_key]);
          }
        }

        $for_filters = array_keys($temp);
      }
    }

    return $for_filters;
  }

  private function requestFlatRateHelperBelong($uid, array $nids) : array {
    $data = array('pickup' => array(), 'delivery' => array());

    if ($nids) {
      $data['pickup'] = db_select('field_data_field_helper_pickup', 'fd')
        ->fields('fd', array('entity_id'))
        ->condition('fd.field_helper_pickup_target_id', $uid)
        ->condition('fd.entity_id', $nids, 'IN')
        ->execute()
        ->fetchCol();

      $data['delivery'] = db_select('field_data_field_helper_delivery', 'fd')
        ->fields('fd', array('entity_id'))
        ->condition('fd.field_helper_delivery_target_id', $uid)
        ->condition('fd.entity_id', $nids, 'IN')
        ->execute()
        ->fetchCol();
    }

    return $data;
  }

  private function requestFilterNotFlatRateHelper($uid, array $nids, $close_flag = FALSE) {
    $types = array(1, 2, 3, 4, 6, 7);

    // Find All requests.
    $select = db_select('node', 'n')
      ->fields('n', array('nid'));
    $select->distinct('n.nid');
    $select->leftJoin('field_data_field_helper', 'ff', 'ff.entity_id = n.nid');
    $select->leftJoin('field_data_field_move_service_type', 'fms', 'fms.entity_id = n.nid');
    $select->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('n.nid', $nids, 'IN')
      ->condition('ff.field_helper_target_id', $uid)
      ->condition('fms.field_move_service_type_value', $types, 'IN');
    if ($close_flag) {
      $select->leftJoin('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select->condition('fcf.field_company_flags_value	', array(3), 'IN');
    }
    $all_requests_nodes = $select->execute()->fetchCol();

    if (!$close_flag && $all_requests_nodes) {
      $select2 = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->fields('fcf', array('field_company_flags_value'));
      $select2->join('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select2->condition('n.nid', $all_requests_nodes, 'IN');
      $results2 = $select2->execute()->fetchAll();
      $temp = array();
      foreach ($results2 as $flags) {
        $temp[$flags->nid][] = $flags->field_company_flags_value;
      }

      foreach ($temp as $nid_key => $flags) {
        $in = in_array(3, $flags);
        if ($in) {
          $key = array_search($nid_key, $all_requests_nodes);
          unset($all_requests_nodes[$key]);
        }
      }
    }

    return $all_requests_nodes;
  }

  private function requestFilterLocalFlatRateHelper($uid, array $nids, $close_flag = FALSE) {
    // Find All requests.
    $select = db_select('node', 'n')
      ->fields('n', array('nid'));
    $select->distinct('n.nid');
    $select->leftJoin('field_data_field_helper', 'ff', 'ff.entity_id = n.nid');
    $select->leftJoin('field_data_field_move_service_type', 'fms', 'fms.entity_id = n.nid');
    $select->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('n.nid', $nids, 'IN')
      ->condition('ff.field_helper_target_id', $uid)
      ->condition('fms.field_move_service_type_value', 5);
    if ($close_flag) {
      $select->leftJoin('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select->condition('fcf.field_company_flags_value	', array(3), 'IN');
    }
    $nids = $select->execute()->fetchCol();

    $all_requests_nodes = array();

    foreach ($nids as $nid) {
      if (Payroll::isFlatRateLocal($nid)) {
        $all_requests_nodes[] = $nid;
      }
    }

    if (!$close_flag && $all_requests_nodes) {
      $select2 = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->fields('fcf', array('field_company_flags_value'));
      $select2->join('field_data_field_company_flags', 'fcf', 'fcf.entity_id = n.nid');
      $select2->condition('n.nid', $all_requests_nodes, 'IN');
      $results2 = $select2->execute()->fetchAll();
      $temp = array();
      foreach ($results2 as $flags) {
        $temp[$flags->nid][] = $flags->field_company_flags_value;
      }

      foreach ($temp as $nid_key => $flags) {
        $in = in_array(3, $flags);
        if ($in) {
          $key = array_search($nid_key, $all_requests_nodes);
          unset($all_requests_nodes[$key]);
        }
      }
    }

    return $all_requests_nodes;
  }

  /**
   * Helper function for prepare dates for using.
   *
   * @param string $date
   *   Date.
   * @param array $options
   *   Additional options.
   *
   * @return int
   *   Unix Time.
   */
  public function prepareDate($date = '', $options = array()) {
    if ($date) {
      date_default_timezone_set('UTC');

      $new_date = new \DateTime($date);
      $new_date->modify('today');

      if ($options) {
        if (in_array('tomorrow', $options)) {
          $new_date->modify('tomorrow');
        }
        if (in_array('midnight', $options)) {
          $new_date->setTime(23, 59, 59);
        }
      }

      return $new_date->getTimestamp();
    }
  }

  /**
   * Get requests for admin page.
   *
   * @return array $result
   *   Result with fields of node requests.
   */
  public function requests() {
    $result = array();
    $result['confirmed'] = $this->getDataNids($this->confirmedRequests());
    $result['action_required'] = $this->getDataNids($this->actionRequiredRequests());

    return $result;
  }

  /**
   * Get data fields of node.
   *
   * @param array $nids
   *   Lists of node ids.
   *
   * @return array
   *   Array of node fields.
   */
  protected function getDataNids($nids = array()) {
    $data = array();
    foreach ($nids as $nid) {
      $data[$nid] = $this->getRequestsNode($nid);
    }

    return $data;
  }

  /**
   * Get fields of node from cache or loaded of node and save in cache.
   *
   * @param int $nid
   *   Node id.
   *
   * @return object $node
   *   Object of node.
   */
  protected function getRequestsNode($nid) {
    $node = NULL;
    $table = 'move_services_search_admin_cache';
    $cache_data = Cache::getCacheData($nid, $table);
    if ($cache_data) {
      $node = $cache_data;
    }
    else {
      if (parent::checkNode($nid)) {
        $node = $this->updateAdminCacheData($nid, TRUE);
      }
      else {
        return services_error(t('Unknown node'), 406);
      }
    }

    return $node;
  }

  /**
   * Update cache data of "move_request" node type for search action.
   *
   * @param int $nid
   *   Node id.
   * @param bool|FALSE $return
   *   Flag pointing at to return or not to return result.
   *
   * @return array
   *   Array with fields of node.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  protected function updateAdminCacheData($nid, $return = FALSE) {
    $table = 'move_services_search_admin_cache';
    $node = entity_metadata_wrapper('node', $nid);
    $result = $this->getRequestsNodeFields($node);
    $cache = Cache::getCacheData($nid, $table);

    // Set new cache data.
    if ($cache) {
      Cache::updateCacheData($nid, $result, $table);
    }
    else {
      Cache::setCacheData($nid, $result, $table);
    }

    return ($return) ? $result : TRUE;
  }

  /**
   * Get fields of node "move_request" type for admin page.
   *
   * @param \EntityMetadataWrapper $node
   *   Node object of EMW type.
   *
   * @return array
   *   Fields of node for admin request.
   */
  protected function getRequestsNodeFields(\EntityMetadataWrapper $node) {
    $date = ($node->field_date->value()) ? format_date($node->field_date->value(), 'rm_standard') : NULL;
    $created = ($node->created->value()) ? format_date($node->created->value(), 'super_short_with_time') : NULL;
    $changed = ($node->changed->value()) ? format_date($node->changed->value(), 'super_short_with_time') : NULL;

    $result = array(
      'nid' => $node->nid->value(),
      'status' => $node->field_approve->label(),
      'service_type' => $node->field_move_service_type->label(),
      'date' => $date,
      'customer' => $this->getRequestsNodeCustomerField($node),
      'moving_from' => $this->getAddress($node, 'field_moving_from'),
      'moving_to' => $this->getAddress($node, 'field_moving_to'),
      'size_of_move' => $node->field_size_of_move->label(),
      'crew' => $node->field_movers_count->value(),
      'post_date' => $created,
      'update_date' => $changed,
      'from' => $node->field_poll->label(),
      'user_status' => $node->status->label(),
    );

    return $result;
  }

  /**
   * Customer fields.
   *
   * @param \EntityMetadataWrapper $node
   *   Node object of EMW type.
   *
   * @return string
   *   String of customer data fields.
   */
  protected function getRequestsNodeCustomerField(\EntityMetadataWrapper $node) {
    $string[] = $node->field_first_name->value();
    $string[] = $node->field_last_name->value();
    $string[] = $node->field_phone->value();
    $string[] = $node->field_e_mail->value();

    return implode(' ', $string);
  }

  /**
   * Get data from address field.
   *
   * @param \EntityMetadataWrapper $node
   *   Node object of EMW type.
   * @param string $field_name
   *   The name of field.
   *
   * @return string
   *   Concatenated string of address field data.
   */
  protected function getAddress(\EntityMetadataWrapper $node, $field_name) {
    $string[] = $this->requestsNodeAddressField($node, $field_name, 'locality');
    $string[] = $this->requestsNodeAddressField($node, $field_name, 'administrative_area');
    $string[] = $this->requestsNodeAddressField($node, $field_name, 'postal_code');

    return implode(' ', $string);
  }

  /**
   * Get data of address field.
   *
   * @param \EntityMetadataWrapper $node
   *   Node object of EMW type.
   * @param string $field_name
   *   The name of field.
   * @param string $field
   *   The name of variant.
   *
   * @return null|string
   *   Field value or nothing.
   */
  protected function requestsNodeAddressField(\EntityMetadataWrapper $node, $field_name, $field) {
    try {
      return $node->{$field_name}->{$field}->value();
    }
    catch (\Throwable $exc) {
      return NULL;
    }
  }

  /**
   * Node ids of confirm requests.
   *
   * @return array
   *   List of node ids.
   */
  protected function confirmedRequests() {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyOrderBy('created', 'DESC')
      ->fieldCondition('field_approve', 'value', 3, '=')
      ->fieldCondition('field_confirmed_added', 'value', 0, '=');

    return $this->requestsExecute($query);
  }

  /**
   * Execute EntityFieldQuery.
   *
   * @param \EntityFieldQuery $query
   *   Query object.
   *
   * @return array
   *   Array of keys.
   */
  protected function requestsExecute(\EntityFieldQuery $query) {
    $nodes = $query->execute();
    return isset($nodes['node']) ? array_keys($nodes['node']) : array();
  }

  /**
   * Action required requests.
   *
   * @return array
   */
  protected function actionRequiredRequests() {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyOrderBy('created', 'DESC')
      ->fieldCondition('field_approve', 'value', array(1, 7, 10), 'IN');

    return $this->requestsExecute($query);
  }

  /**
   * Unconfirmed requests.
   *
   * @return array|int
   */
  public function unconfirmedRequests(int $reservation = 0) {
    $result = &drupal_static(__METHOD__ . $reservation, array());

    if (!$result) {
      $query = db_select('node', 'n');
      $query->leftJoin('field_data_field_request_manager', 'frm', 'n.nid = frm.entity_id AND frm.bundle = :type', array(':type' => 'move_request'));
      $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id AND fa.bundle = :type', array(':type' => 'move_request'));
      $query->leftJoin('field_data_field_reservation_received', 'fdr', 'n.nid = fdr.entity_id AND fdr.bundle = :type', array(':type' => 'move_request'));

      $query->fields('n', array('nid'))
        ->condition('n.type', 'move_request')
        ->condition('n.status', NODE_PUBLISHED)
        ->condition('fa.field_approve_value', [RequestStatusTypes::NOT_CONFIRMED, RequestStatusTypes::FLATE_RATE_WAIT_OPTOINS], 'IN')
        ->condition('fdr.field_reservation_received_value', $reservation);

      // Manager permissions.
      if ($this->isManager) {
        $manager_condition = array();
        $check_perm = function (string $perm) {
          return (new Sales())->checkManagerPermissions($perm);
        };

        // Manager can see own and can see unsigned.
        if (!$check_perm('canSeeOtherLeads') && $check_perm('canSeeUnsignedLeads')) {
          $manager_condition[] = array($this->uid, '=');
          $manager_condition[] = array('', '=');
          $query->condition(db_or()
            ->condition('frm.field_request_manager_target_id', $this->uid, '=')
            ->isNull('frm.field_request_manager_target_id'));
        }
        // Manager can see own and theirs but do not see unsigned.
        elseif ($check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->isNotNull('frm.field_request_manager_target_id');
        }
        // Manager can see only own requests.
        elseif (!$check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->condition('frm.field_request_manager_target_id', $this->uid, '=');
        }
      }

      $query->orderBy('created', 'DESC');
      $requests = $query->execute()->fetchCol();
      $result['nodes'] = $this->getShortRequests($requests);
      $result['total'] = count($requests);
    }

    return $result;
  }

  /**
   * Get request with status 'home estimate'.
   *
   * @return array|int
   *   Array with requests.
   */
  public function homeEstimateRequests() {
    $result = &drupal_static(__METHOD__, array());

    if (!$result) {
      $query = db_select('node', 'n');
      $query->leftJoin('field_data_field_request_manager', 'frm', 'n.nid = frm.entity_id AND frm.bundle = :type', array(':type' => 'move_request'));
      $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id AND fa.bundle = :type', array(':type' => 'move_request'));
      $query->leftJoin('field_data_field_reservation_received', 'fdr', 'n.nid = fdr.entity_id AND fdr.bundle = :type', array(':type' => 'move_request'));

      $query->fields('n', array('nid'))
        ->condition('n.type', 'move_request')
        ->condition('n.status', NODE_PUBLISHED)
        ->condition('fa.field_approve_value', RequestStatusTypes::INHOME_ESTIMATE);

      // TODO same logic with permissions used in other query. We need to move it to one method.
      // Manager permissions.
      if ($this->isManager) {
        $manager_condition = array();
        $check_perm = function (string $perm) {
          return (new Sales())->checkManagerPermissions($perm);
        };

        // Manager can see own and can see unsigned.
        if (!$check_perm('canSeeOtherLeads') && $check_perm('canSeeUnsignedLeads')) {
          $manager_condition[] = array($this->uid, '=');
          $manager_condition[] = array('', '=');
          $query->condition(db_or()
            ->condition('frm.field_request_manager_target_id', $this->uid, '=')
            ->isNull('frm.field_request_manager_target_id'));
        }
        // Manager can see own and theirs but do not see unsigned.
        elseif ($check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->isNotNull('frm.field_request_manager_target_id');
        }
        // Manager can see only own requests.
        elseif (!$check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->condition('frm.field_request_manager_target_id', $this->uid, '=');
        }
      }

      $query->orderBy('created', 'DESC');
      $requests = $query->execute()->fetchCol();
      $result['nodes'] = $this->getShortRequests($requests);
      $result['total'] = count($requests);
    }

    return $result;
  }

  public function reservationRequests($reservation = 1) {
    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id AND fa.bundle = :type', array(':type' => 'move_request'));
    $query->leftJoin('field_data_field_reservation_received', 'fdr', 'n.nid = fdr.entity_id AND fdr.bundle = :type', array(':type' => 'move_request'));
    $query->fields('n', array('nid'))
      ->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('fa.field_approve_value', 2)
      ->condition('fdr.field_reservation_received_value', $reservation);

    $requests = $query->execute()->fetchCol();

    return $requests;
  }

  /**
   * Get request by confirmed move day and between dates.
   *
   * @return array|mixed
   *   Array with nodes.
   */
  public function confirmMoveDate() {
    $result = &drupal_static(__METHOD__ . $this->allDates['first'] . $this->allDates['last'], array());
    if (!$result) {
      $date_from = date('Y-m-d', $this->allDates['first']) . ' 00:00:01';
      $date_to = date('Y-m-d', $this->allDates['last']) . ' 00:00:01';

      $query = db_select('node', 'n');
      $query->leftJoin('field_data_field_request_manager', 'frm', 'n.nid = frm.entity_id AND frm.bundle = :type', array(':type' => 'move_request'));
      $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id AND fa.bundle = :type', array(':type' => 'move_request'));
      $query->leftJoin(' field_data_field_date', 'fdfd', 'n.nid = fdfd.entity_id AND fdfd.bundle = :type', array(':type' => 'move_request'));
      $query->leftJoin('field_data_field_date_confirmed', 'fdc', 'n.nid = fdc.entity_id AND fdc.bundle = :type', array(':type' => 'move_request'));
      $query->fields('n', array('nid'))
        ->condition('n.type', 'move_request')
        ->condition('n.status', NODE_PUBLISHED)
        ->orderBy('fdc.field_date_confirmed_value', 'DESC')
        ->condition('fa.field_approve_value', 3)
        ->condition('fdfd.field_date_value', array($date_from, $date_to), 'BETWEEN');

      if ($this->isManager) {
        $manager_condition = array();
        $check_perm = function (string $perm) {
          return (new Sales())->checkManagerPermissions($perm);
        };

        // Manager can see own and can see unsigned.
        if (!$check_perm('canSeeOtherLeads') && $check_perm('canSeeUnsignedLeads')) {
          $manager_condition[] = array($this->uid, '=');
          $manager_condition[] = array('', '=');
          $query->condition(db_or()
            ->condition('frm.field_request_manager_target_id', $this->uid, '=')
            ->isNull('frm.field_request_manager_target_id'));
        }
        // Manager can see own and theirs but do not see unsigned.
        elseif ($check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->isNotNull('frm.field_request_manager_target_id');
        }
        // Manager can see only own requests.
        elseif (!$check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->condition('frm.field_request_manager_target_id', $this->uid, '=');
        }
      }

      $requests = $query->execute()->fetchCol();
      $result['nodes'] = $this->getShortRequests($requests, TRUE, FALSE);
      $result['nids'] = $requests;
      $result['total'] = count($requests);
    }

    return $result;
  }

  /**
   * Get request by confirmed booked day and between dates.
   *
   * @return array|mixed
   *   Array with nodes.
   */
  public function confirmBookedDate() {
    $result = &drupal_static(__METHOD__ . $this->allDates['first'] . $this->allDates['last'], array());
    if (!$result) {
      $query = db_select('node', 'n');
      $query->leftJoin('field_data_field_request_manager', 'frm', 'n.nid = frm.entity_id AND frm.bundle = :type', array(':type' => 'move_request'));
      $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id AND fa.bundle = :type', array(':type' => 'move_request'));
      $query->leftJoin('field_data_field_date_confirmed', 'fdc', 'n.nid = fdc.entity_id AND fdc.bundle = :type', array(':type' => 'move_request'));
      $query->fields('n', array('nid'))
        ->condition('n.type', 'move_request')
        ->condition('n.status', NODE_PUBLISHED)
        ->orderBy('fdc.field_date_confirmed_value', 'DESC')
        ->condition('fa.field_approve_value', 3)
        ->condition('fdc.field_date_confirmed_value', array($this->allDates['first'], $this->allDates['last']), 'BETWEEN');

      // Manager permissions.
      if ($this->isManager) {
        $manager_condition = array();
        $check_perm = function (string $perm) {
          return (new Sales())->checkManagerPermissions($perm);
        };

        // Manager can see own and can see unsigned.
        if (!$check_perm('canSeeOtherLeads') && $check_perm('canSeeUnsignedLeads')) {
          $manager_condition[] = array($this->uid, '=');
          $manager_condition[] = array('', '=');
          $query->condition(db_or()
            ->condition('frm.field_request_manager_target_id', $this->uid, '=')
            ->isNull('frm.field_request_manager_target_id'));
        }
        // Manager can see own and theirs but do not see unsigned.
        elseif ($check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->isNotNull('frm.field_request_manager_target_id');
        }
        // Manager can see only own requests.
        elseif (!$check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->condition('frm.field_request_manager_target_id', $this->uid, '=');
        }
      }

      $requests = $query->execute()->fetchCol();
      $result['nodes'] = $this->getShortRequests($requests, TRUE, FALSE);
      $result['nids'] = $requests;
      $result['total'] = count($requests);
    }

    return $result;
  }

  public function getAllRequestsManagerPermission() {
    $result = &drupal_static(__METHOD__, array());

    if (!$result) {
      $query = db_select('node', 'n');
      $query->leftJoin('field_data_field_request_manager', 'frm', 'n.nid = frm.entity_id AND frm.bundle = :type', array(':type' => 'move_request'));
      $query->fields('n', array('nid'))
        ->condition('n.type', 'move_request')
        ->condition('n.status', 1);

      // Manager permissions.
      if ($this->isManager) {
        $check_perm = function (string $perm) {
          return (new Sales())->checkManagerPermissions($perm);
        };

        // Manager can see own and can see unsigned.
        if (!$check_perm('canSeeOtherLeads') && $check_perm('canSeeUnsignedLeads')) {
          $query->condition(db_or()
            ->condition('frm.field_request_manager_target_id', $this->uid, '=')
            ->isNull('frm.field_request_manager_target_id'));
        }
        // Manager can see own and theirs but do not see unsigned.
        elseif ($check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->isNotNull('frm.field_request_manager_target_id');
        }
        // Manager can see only own requests.
        elseif (!$check_perm('canSeeOtherLeads') && !$check_perm('canSeeUnsignedLeads')) {
          $query->condition('frm.field_request_manager_target_id', $this->uid, '=');
        }
      }

      $result = $query->execute()->fetchCol();
    }

    return $result;
  }

  /**
   * Get count home estimate request with status schedule.
   *
   * @return mixed
   *   Int.
   */
  public function countEstimateSchedule() {
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'move_request')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->propertyOrderBy('created', 'DESC')
      ->fieldCondition('field_approve', 'value', 4, '=')
      ->addMetaData('account', user_load(1))
      ->fieldCondition('field_home_estimate_status', 'value', 1, '=');

    $count = $query->count()->execute();
    return $count;
  }

  public function getFlatRateLocalMove() {
    $nids = db_query("
      SELECT nid
        FROM node n
        INNER JOIN field_data_field_move_service_type fdfmst ON n.nid = fdfmst.entity_id
        WHERE fdfmst.field_move_service_type_value = :service_type", array(':service_type' => RequestServiceType::FLAT_RATE))
      ->fetchCol();

    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $request_all_data = MoveRequest::getRequestAllData($nid);
        if (!empty($request_all_data) && isset($request_all_data['localMove'])) {
          $data_for_update = array(
            'field_flat_rate_local_move' => array(
              'raw' => 'value',
              'value' => (int) $request_all_data['localMove'],
              'data_to_update_in_cache' => array(
                'field_flat_rate_local_move' => array(
                  'value' => (int) $request_all_data['localMove'],
                ),
              ),
            ),
          );
          $result[] = array(
            'nid' => $nid,
            'localMove' => $request_all_data['localMove'],
            'data' => MoveRequest::updateNodeFieldsOnly($nid, $data_for_update, 'move_request', FALSE, FALSE),
          );
        }
      }
    }

    return !empty($result) ? $result : array();
  }

  /**
   * Get foreman jobs. New and old.
   *
   * Can be sorted by move_date.
   *
   * @param int $uid
   *   Foreman user id.
   * @param array $conditions
   *   Conditions show jobs.
   * @param array $sorting
   *   Array with fields to sort.
   * @param string $type
   *   Type to show jobs.
   * @param int $current_page
   *   Number of page to show.
   * @param int $per_page
   *   Number request on the page. -1 to show all.
   *
   * @return array
   *   Array of nids with page info or empty array
   */
  public function searchForemanRequests(int $uid, array $conditions = array(), array $sorting = array('field_date_value' => 'DESC'), $type = 'new', int $current_page = 1, int $per_page = 15) {
    $result = $this->getAllForemanRequestsNids($uid, $conditions, $sorting, $type, $current_page, $per_page);
    if (!empty($result['items'])) {
      $result['items'] = $this->getRequestsMultiply($result['items']);
    }
    return $result;
  }

  /**
   * Get foreman jobs. New and old.
   *
   * Can be sorted by move_date.
   *
   * @param int $uid
   *   Foreman user id.
   * @param array $conditions
   *   Conditions show jobs.
   * @param array $sorting
   *   Array with fields to sort.
   * @param string $type
   *   Type to show jobs.
   * @param int $current_page
   *   Number of page to show.
   * @param int $per_page
   *   Number request on the page. -1 to show all.
   *
   * @return array
   *   Array of nids with page info or empty array
   */
  public function getAllForemanRequestsNids(int $uid, array $conditions = array(), array $sorting = array('field_date_value' => 'DESC'), $type = 'new', int $current_page = 1, int $per_page = 15) : array {
    $current_date = Extra::date('Y-m-d', NULL, $this->timeZone) . ' 00:00:00';

    $field_for_sorting = array(
      'field_actual_start_time',
      'field_moving_from',
      'field_moving_to',
      'field_size_of_move',
      'field_movers_count',
      'field_user_first_name',
      'field_first_name',
      'field_last_name',
    );

    $query = db_select('node', 'n');
    $query->fields('n', array('nid'));

    // Add user request author name.
    $query->leftJoin('field_data_field_user_first_name', 'user_first_name', 'user_first_name.entity_id = n.uid');
    $query->leftJoin('field_data_field_user_last_name', 'user_last_name', 'user_last_name.entity_id = n.uid');

    $query->leftJoin('field_data_field_foreman', 'foreman', 'foreman.entity_id = n.nid');
    $query->leftJoin('field_data_field_foreman_delivery', 'foreman_delivery', 'foreman_delivery.entity_id = n.nid');
    $query->leftJoin('field_data_field_foreman_pickup', 'foreman_pickup', 'foreman_pickup.entity_id = n.nid');

    $query->leftJoin('field_data_field_date', 'field_date', 'field_date.entity_id = n.nid');
    $query->leftJoin('field_data_field_delivery_date', 'delivery_date', 'delivery_date.entity_id = n.nid');
    $query->leftJoin('field_data_field_delivery_date_second', 'second_delivery_date', 'second_delivery_date.entity_id = n.nid');
    $query->leftJoin('field_data_field_delivery_date_from', 'delivery_date_from', 'delivery_date_from.entity_id = n.nid');
    $query->leftJoin('field_data_field_delivery_date_to', 'delivery_date_to', 'delivery_date_to.entity_id = n.nid');

    // For sort fields.
    foreach ($field_for_sorting as $field_name) {
      $query->leftJoin('field_data_' . $field_name, $field_name, $field_name . '.entity_id = n.nid');
    }

    $query->leftJoin('field_data_field_company_flags', 'company_flags', 'company_flags.entity_id = n.nid');

    $query->leftJoin('field_data_field_approve', 'field_approve', 'field_approve.entity_id = n.nid');

    $query->leftJoin('field_data_field_foreman_assign_time', 'field_foreman_assign_time', 'field_foreman_assign_time.entity_id = n.nid');

    $query->leftJoin('field_data_field_move_service_type', 'move_service_type', 'move_service_type.entity_id = n.nid');

    $query->leftJoin('field_data_field_flat_rate_local_move', 'flat_rate_local_move', 'flat_rate_local_move.entity_id = n.nid');

    $query->condition('field_approve.field_approve_value', RequestStatusTypes::CONFIRMED);

    if ($type == 'new') {
      $this->foremanNewJobs($query, $uid, $current_date);
    }

    if ($type == 'old') {
      $this->foremanDoneJobs($query, $uid, $current_date);
      $new_jobs_nids = $this->getAllForemanRequestsNids($uid, $conditions, $sorting, 'new');
      if (!empty($new_jobs_nids)) {
        $query->condition('n.nid', $new_jobs_nids['items'], 'NOT IN');
      }
    }


    $query->groupBy('n.nid');

    if (!empty($sorting)) {
      foreach ($sorting as $field_name => $direction) {
        $query->orderBy($field_name, $direction);
      }
    }

    // Filtering.
    if (!empty($conditions)) {
      foreach ($conditions as $field_name => $value) {
        $operator = '=';
        if ($field_name == 'nid') {
          $value = db_like($value) . '%';
          $operator = 'LIKE';
        }
        $query->condition($field_name, $value, $operator);
      }
    }

    $count_query = clone  $query;
    $total_count = $count_query->execute()->rowCount();
    $page_count = ceil($total_count / $per_page);

    // Information about pages.
    $meta = array(
      'currentPage' => (int) $current_page,
      'perPage' => (int) $per_page,
      'pageCount' => (int) $page_count,
      'totalCount' => (int) $total_count,
    );

    // Apply pagination for query.
    $this->pagerForForemanJobs($query, $current_page, $per_page);

    return empty($result = $query->execute()->fetchCol()) ? array() : ['items' => $result, 'meta' => $meta];

  }

  /**
   * Filter all foreman new jobs.
   *
   * @param $query
   *   Drupal db_query object.
   * @param int $uid
   *   Foreman uid.
   * @param string $current_date
   *   Current day.
   */
  public function foremanNewJobs(&$query, int $uid, string $current_date) : void {
    $db_or = db_or();

    // Not flatrate jobs.
    $db_and_not_flatrate = db_and();
    $db_or_not_flatrate = db_or();
    $db_or_local_flat_rate = db_or();

    $db_or_local_flat_rate->condition('move_service_type.field_move_service_type_value', RequestServiceType::FLAT_RATE, '<>');
    $db_or_local_flat_rate->condition(
      db_and()
        ->condition('move_service_type.field_move_service_type_value', RequestServiceType::FLAT_RATE)
        ->condition('flat_rate_local_move.field_flat_rate_local_move_value', 1)
    );

    $db_and_not_flatrate->condition($db_or_local_flat_rate);

    $db_and_not_flatrate->condition('foreman.field_foreman_target_id', $uid);
    $db_and_not_flatrate->condition('field_date.field_date_value', $current_date, '>=');
    $db_and_not_flatrate->condition('company_flags.field_company_flags_value', 3, '<>');

    $not_flatrate_jobs = $this->foremanDoneJobsByFlagsOnly($query, $uid, [3]);

    if (!empty($not_flatrate_jobs)) {
      $db_and_not_flatrate->condition('n.nid', $not_flatrate_jobs, 'NOT IN');
    }


    $db_or_not_flatrate->condition($db_and_not_flatrate);

    $db_or->condition($db_and_not_flatrate);

    // Pickup flatrate.
    $db_and_flatrate_pickup = db_and();
    $db_and_flatrate_pickup->condition('move_service_type.field_move_service_type_value', RequestServiceType::FLAT_RATE);
    $db_and_flatrate_pickup->condition('foreman_pickup.field_foreman_pickup_target_id', $uid);

    $db_and_flatrate_pickup->condition('field_date.field_date_value', $current_date, '>=');

    $pickup_jobs = $this->foremanDoneJobsByFlagsOnly($query, $uid, [5]);
    if (!empty($pickup_jobs)) {
      $db_and_flatrate_pickup->condition('n.nid', $pickup_jobs, 'NOT IN');
    }

    $db_or->condition($db_and_flatrate_pickup);

    // Delivery flatrate.
    $db_and_flatrate_delivery = db_and();
    $db_and_flatrate_delivery->condition('move_service_type.field_move_service_type_value', RequestServiceType::FLAT_RATE);
    $db_and_flatrate_delivery->condition('foreman_delivery.field_foreman_delivery_target_id', $uid);
    // Show delivery faltrate only for not local move flatrate.
    $db_and_flatrate_delivery->condition('flat_rate_local_move.field_flat_rate_local_move_value', 1, '<>');
    $db_and_flatrate_delivery->condition($this->foremanJobsDatesFilter($current_date, '>='));

    $dilvery_jobs = $this->foremanDoneJobsByFlagsOnly($query, $uid, [6]);

    if (!empty($dilvery_jobs)) {
      $db_and_flatrate_delivery->condition('n.nid', $dilvery_jobs, 'NOT IN');
    }

    $db_or->condition($db_and_flatrate_delivery);

    $query->condition($db_or);

  }

  /**
   * Get all foreman done jobs by flags.
   *
   * @param $query
   * @param int $uid
   * @return mixed
   */
  public function foremanDoneJobsByFlagsOnly($query, int $uid, $flags = [3, 5, 6]) {
    $db_or = db_or();
    $clone_query = clone $query;

    $db_or->condition('foreman.field_foreman_target_id', $uid);
    $db_or->condition('foreman_pickup.field_foreman_pickup_target_id', $uid);
    $db_or->condition('foreman_delivery.field_foreman_delivery_target_id', $uid);

    $clone_query->condition('company_flags.field_company_flags_value', $flags, 'IN');
    $clone_query->condition($db_or);

    return $clone_query->execute()->fetchCol();
  }

  /**
   * Filter foreman delivery dates.
   *
   * @param string $current_date
   *   Current day.
   * @param string $sign
   *   Sign to check  "less" or "bigger" then current date.
   *
   * @return \DatabaseCondition
   *   Conditions for select.
   */
  public function foremanJobsDatesFilter(string $current_date, string $sign) {
    $db_or = db_or();
    $db_and1 = db_and();
    $db_or_1 = db_or();

    // Only delivery date.
    $db_or_1->condition('second_delivery_date.field_delivery_date_second_value', '');
    $db_or_1->isNull('second_delivery_date.field_delivery_date_second_value');
    $db_and1->condition('delivery_date.field_delivery_date_value', $current_date, $sign);
    $db_and1->condition($db_or_1);

    // Second delivery date.
    $db_and_2 = db_and();
    $db_and_2->condition('second_delivery_date.field_delivery_date_second_value', '', '<>');
    $db_and_2->isNotNull('second_delivery_date.field_delivery_date_second_value');
    $db_and_2->condition('second_delivery_date.field_delivery_date_second_value', $current_date, $sign);

    $db_or->condition($db_and1);
    $db_or->condition($db_and_2);

    return $db_or;
  }

  /**
   * Filter all foreman done jobs.
   *
   * @param $query
   *   Drupal db_query object.
   * @param int $uid
   *   Foreman uid.
   * @param string $current_date
   *   Current day.
   */
  public function foremanDoneJobs(&$query, int $uid, $current_date) : void {
    $db_or = db_or();

    // Not flatrate jobs.
    $db_and_not_flatrate = db_and();
    $db_or_not_flatrate = db_or();
    $db_or_local_flat_rate = db_or();

    $db_or_local_flat_rate->condition('move_service_type.field_move_service_type_value', RequestServiceType::FLAT_RATE, '<>');
    $db_or_local_flat_rate->condition(
      db_and()
        ->condition('move_service_type.field_move_service_type_value', RequestServiceType::FLAT_RATE)
        ->condition('flat_rate_local_move.field_flat_rate_local_move_value', 1)
    );
    $db_and_not_flatrate->condition($db_or_local_flat_rate);
    $db_and_not_flatrate->condition('foreman.field_foreman_target_id', $uid);

    $db_or_not_flatrate->condition('field_date.field_date_value', $current_date, '<');
    $db_or_not_flatrate->condition('company_flags.field_company_flags_value', 3);

    $db_and_not_flatrate->condition($db_or_not_flatrate);

    $db_or->condition($db_and_not_flatrate);

    // Pickup flatrate.
    $db_and_flatrate_pickup = db_and();
    $db_or_flatrate_pickup = db_or();
    $db_and_flatrate_pickup->condition('move_service_type.field_move_service_type_value', RequestServiceType::FLAT_RATE);
    $db_and_flatrate_pickup->condition('foreman_pickup.field_foreman_pickup_target_id', $uid);

    $db_or_flatrate_pickup->condition('field_date.field_date_value', $current_date, '<');
    $db_or_flatrate_pickup->condition('company_flags.field_company_flags_value', 5);

    $db_and_flatrate_pickup->condition($db_or_flatrate_pickup);

    $db_or->condition($db_and_flatrate_pickup);

    // Delivery flatrate.
    $db_and_flatrate_delivery = db_and();
    $db_or_flatrate_delivery = db_or();
    $db_and_flatrate_delivery->condition('move_service_type.field_move_service_type_value', RequestServiceType::FLAT_RATE);
    $db_and_flatrate_delivery->condition('foreman_delivery.field_foreman_delivery_target_id', $uid);
    // Show delivery faltrate only for not local move flatrate.
    $db_and_flatrate_delivery->condition('flat_rate_local_move.field_flat_rate_local_move_value', 1, '<>');

    // Select delivery date or second delivery date.
    $db_or_flatrate_delivery->condition($this->foremanJobsDatesFilter($current_date, '<'));
    $db_or_flatrate_delivery->condition('company_flags.field_company_flags_value', 6);

    $db_and_flatrate_delivery->condition($db_or_flatrate_delivery);
    $db_or->condition($db_and_flatrate_delivery);


    $query->condition($db_or);
  }

  /**
   * Limit nodes by pages.
   *
   * @param $query
   *   Database select object.
   * @param int $current_page
   *   Page for select.
   * @param int $per_page
   *   Number requests on the page.
   */
  public function pagerForForemanJobs(&$query, int $current_page, int $per_page) : void {
    $start_from = ($current_page - 1) * $per_page;
    $query->range($start_from, $per_page);
  }

  /**
   * Get request by requests ids.
   *
   * @param array $nids
   *   Requests ids.
   *
   * @return array
   *   Array with nodes, or empty array.
   */
  public function getRequestsMultiply(array $nids) : array {
    $results = array();
    if (!empty($nids)) {
      foreach ($nids as $nid) {
        $obj = new MoveRequest($nid);
        $results[] = $obj->retrieve();
      }
    }
    return $results;
  }

  /**
   * Check if user has role home estimator and login from home estimate.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isHomeEstimate() : bool {
    return ($this->isHomeEstimator && $this->clients->isUserLoginFromHomeEstimate()) ? TRUE : FALSE;
  }

  /**
   * Set allDates param by user settings.
   *
   * Set time for get request from beginning of time to end of current month.
   * Or set time from start of month to the end of time.
   *
   * @param array $settings
   *   User settings.
   */
  public function setAllDatesByUserSetting(array $settings) : void {
    $all_dates = $this->firstLastCurrentMonthTime(TRUE);

    if (!empty($settings['all_time'])) {
      switch ($settings['all_time']) {
        case self::FILTER_CONFIRMED_BY_ALL_DATE_FROM_BEGINNING:
          $all_dates['first'] = 0;
          break;

        case self::FILTER_CONFIRMED_BY_ALL_DATE_TO_THE_END:
          $all_dates['first'] = Extra::getDateFrom('now');
          $all_dates['last'] = 99999999999;
          break;
      }
    }

    $this->allDates = $all_dates;
  }

}
