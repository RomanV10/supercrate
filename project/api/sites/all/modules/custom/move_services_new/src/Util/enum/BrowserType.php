<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TripStatus.
 *
 * @package Drupal\long_distance\Util
 */
class BrowserType extends BaseEnum {

  const DESKTOP = 1;
  const MOBILE = 2;
  const TABLET = 3;

}
