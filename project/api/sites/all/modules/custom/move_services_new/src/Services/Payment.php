<?php

namespace Drupal\move_services_new\Services;

use net\authorize\api\contract\v1\CustomerDataType;
use net\authorize\api\controller\CreateTransactionController;
use net\authorize\api\contract\v1\PaymentType;
use net\authorize\api\contract\v1\MerchantAuthenticationType;
use net\authorize\api\contract\v1\CreditCardType;
use net\authorize\api\contract\v1\AnetApiResponseType;
use net\authorize\api\contract\v1\CreateTransactionRequest;
use net\authorize\api\contract\v1\OrderType;
use net\authorize\api\contract\v1\TransactionRequestType;
use net\authorize\api\contract\v1\CustomerAddressType;
use net\authorize\api\constants\ANetEnvironment;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\InvoiceFlags;
use Drupal\move_invoice\Services\Invoice;
use Drupal\move_coupon\Services\CouponUser;
use Drupal\move_new_log\Services\Log;

/**
 * Class Payment.
 */
class Payment extends BaseService {
  private $apiLoginId = '';
  private $transactionKey = '';
  private $entity = array();
  private $entityId = NULL;

  /**
   * Additional field. Needed for trips.
   *
   * @var int null
   */
  private $nid = NULL;

  /**
   * Flag indicated for invoices.
   *
   * @var bool null
   */
  private $isInvoice = NULL;
  private $entityType = NULL;
  private $nameCard = NULL;
  private $cardNumber = NULL;
  private $cardCode = NULL;
  private $expDate = NULL;
  private $transactionType = 'authCaptureTransaction';
  private $amount = 0;
  private $description = '';
  private $environment = ANetEnvironment::SANDBOX;
  private $phone = '';
  private $email = '';
  private $saveCardNumber = '';
  private $zipCode = NULL;
  private $cardType = 'creditcard';
  private $transactTime = '';
  private $transactId = '';
  private $method = '';
  private $paymentFlag = FALSE;
  private $lastName = '';
  private $firstName = '';
  private $receiptId = NULL;
  private $pending = FALSE;
  private $invoiceId = NULL;

  /**
   * Payment constructor.
   *
   * @param null|int $entity_id
   *   Entity id.
   * @param null|int $entity_type
   *   The type of entity.
   */
  public function __construct(int $entity_id = NULL, int $entity_type = NULL) {
    $this->setEnvironment();
    $this->setLogFile();
    $secure_data = self::getAuthorizenet();
    $this->apiLoginId = $secure_data['loginid'];
    $this->transactionKey = $secure_data['transactionkey'];

    $this->entityId = $entity_id;
    $this->entityType = $entity_type;

    if ($entity_type == EntityTypes::MOVEREQUEST) {
      $move = new MoveRequest($entity_id);
      if ($this->entity = $move->getNode($entity_id)) {
        $this->getUserInfo();
      }
    }
  }

  public function retrieve() {}

  public function update($data = array()) {}

  public function delete() {}

  public function create($data = array(), $data_to_merge = []) {
    $result = array();

    try {
      set_time_limit(180);
      if (!$this->entityId) {
        throw new \ServicesException("Request ID is NULL.", 406);
      }

      $this->nameCard = $data['name'];
      $this->prepareUserName();
      $this->paymentFlag = $data['payment_flag'];
      $this->email = $data['email'];
      $this->phone = $data['phone'];
      $this->description = $data['description'];
      $this->method = $data['payment_method'];
      $this->nid = isset($data['nid']) ? (int) $data['nid'] : NULL;
      $this->isInvoice = isset($data['is_invoice']) ? (bool) $data['is_invoice'] : NULL;
      $this->pending = (bool) $data['pending'];
      if (isset($data['zip_code']) && $data['zip_code']) {
        $this->zipCode = $data['zip_code'];
      }

      if (isset($data['card_type']) && $data['card_type']) {
        $this->cardType = $data['card_type'];
      }

      // Common setup for API credentials.
      $merchant_authentication = $this->commonApiCredential();
      $ref_id = time();

      // Get Payment details.
      $this->cardNumber = $data['credit_card']['card_number'];
      $this->cardCode = $data['credit_card']['card_code'];
      $this->expDate = $data['credit_card']['exp_date'];
      $this->amount = $data['amount'];
      $this->saveCardNumber = substr($this->cardNumber, -4);

      // Create the payment data for a credit card.
      $credit_card = new CreditCardType();
      $credit_card->setCardNumber($this->cardNumber);
      $credit_card->setExpirationDate($this->expDate);
      $credit_card->setCardCode($this->cardCode);
      $payment_credit_card = new PaymentType();
      $payment_credit_card->setCreditCard($credit_card);

      // Create a transaction.
      $transaction_request_type = new TransactionRequestType();
      $transaction_request_type->setTransactionType($this->transactionType);
      $transaction_request_type->setAmount($this->amount);
      $transaction_request_type->setPayment($payment_credit_card);

      if ($this->description) {
        $order = new OrderType();
        $order->setDescription($this->description);
        $transaction_request_type->setOrder($order);
      }

      // Customer information.
      $billto = new CustomerAddressType();
      $billto->setFirstName($this->firstName);
      $billto->setLastName($this->lastName);
      $billto->setZip($this->zipCode);
      $billto->setPhoneNumber($this->phone);
      $billto->setEmail($this->email);
      $transaction_request_type->setBillTo($billto);
      $customerdata = new CustomerDataType();
      $customerdata->setEmail($this->email);
      $transaction_request_type->setCustomer($customerdata);

      $request = new CreateTransactionRequest();
      $request->setMerchantAuthentication($merchant_authentication);
      $request->setRefId($ref_id);
      $request->setTransactionRequest($transaction_request_type);

      $response = $this->executeResponse($request);
      $tresponse = $this->getTransactResponse($response);
      $hash = $tresponse->getTransHash();
      $this->transactTime = time();
      $instance = new Log($this->entityId, $this->entityType);

      if ($this->paymentFlag == "Reservation by client") {
        $this->method = "Reservation";
      }

      $receipt = array(
        'amount' => (float) $this->amount,
        'checkN' => '',
        'transaction_id' => $tresponse->getTransId(),
        'type' => $this->cardType,
        'ccardN' => $this->saveCardNumber,
        'name' => $this->nameCard,
        'zip_code' => $this->zipCode,
        'phone' => $this->phone,
        'email' => $this->email,
        'description' => $this->description,
        'created' => $this->transactTime,
        'card_type' => $tresponse->getAccountType(),
        'payment_method' => $this->method,
        'pending' => $this->pending ? $this->pending : FALSE,
        'trans_hash' => $hash,
        'payment_flag' => $this->paymentFlag,
        'auth_code' => $tresponse->getAuthCode(),
        'is_invoice' => $this->isInvoice,
        'invoice_id' => $data['invoice_id'],
        'nid' => $this->nid,
      );

      $receipt = array_merge($receipt, $data_to_merge);

      $receipt_id = MoveRequest::setReceipt($this->entityId, $receipt, $this->entityType);
      if ($receipt_id) {
        $log_receipt_data = $this->prepareReceiptLog($tresponse, $receipt_id);
        $instance->create($log_receipt_data);
      }

      if (isset($data['invoice_id']) && $data['invoice_id']) {
        $query = db_select('move_invoice', 'mi')
          ->fields('mi')
          ->condition('id', $data['invoice_id'])
          ->execute()
          ->fetchObject();

        $unserialize = unserialize($query->data);
        $unserialize['flag'] = InvoiceFlags::PAID;
        Invoice::changeInvoiceFlag($data['invoice_id'], $unserialize);
      }

      if ($this->entityType == EntityTypes::COUPON) {
        $result['promo'] = CouponUser::createCouponAfterPayment($this->entityId, $data);
      }

      $result['status'] = TRUE;
      $result['receipt_id'] = $receipt_id;
      $result['transaction_id'] = $tresponse->getTransId();
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()}";
      watchdog('Create Payment', $message, array(), WATCHDOG_ERROR);
      $result['status'] = FALSE;
      $result['text'] = $e->getMessage();
    }
    finally {
      return $result;
    }
  }

  public function logAuthorizeResponse($response) {
    $instance = new Log($this->entityId, $this->entityType);
    // Check response from authorize.
    if ($response != NULL) {
      if ($response->getMessages()->getResultCode() == 'Ok') {
        $tresponse = $response->getTransactionResponse();
        if ($tresponse != NULL && $tresponse->getMessages() != NULL) {
          $tresponse = $response->getTransactionResponse();
          $log_payment_data = $this->preparePaymentLog($this->amount, $tresponse, $status = 'Success', $title = 'Online payment', TRUE);
          $instance->create($log_payment_data);
        }
        else {
          if ($tresponse->getErrors() != NULL) {
            $log_payment_data = $this->preparePaymentLog($this->amount, $tresponse, $status = 'Failed', $title = 'Online payment', TRUE);
            $instance->create($log_payment_data);
          }
        }
      }
      else {
        $tresponse = $response->getTransactionResponse();
        if ($tresponse != NULL && $tresponse->getErrors() != NULL) {
          $log_payment_data = $this->preparePaymentLog($this->amount, $tresponse, $status = 'Failed', $title = 'Online payment', TRUE);
          $instance->create($log_payment_data);
        }
        else {
          $log_payment_data = $this->preparePaymentLog($this->amount, NULL, $status = 'Failed', $title = 'Online payment', NULL);
          $instance->create($log_payment_data);
        }
      }
    }
    else {
      $log_payment_data = $this->preparePaymentLog($this->amount, NULL, $status = 'Failed', $title = 'Online payment', FALSE);
      $instance->create($log_payment_data);
    }
  }

  #######################REFUND################################################

  public function refunds($data = array(), array $data_to_merge = []) {
    $result = array();

    try {
      if (!$this->entityId) {
        throw new RefundReceipt("Entity id was not set.");
      }
      $this->nid = isset($data['nid']) ? (int) $data['nid'] : NULL;
      $this->isInvoice = isset($data['is_invoice']) ? (bool) $data['is_invoice'] : NULL;
      $this->invoiceId = $data['invoice_id'] ?? NULL;
      $this->refundReceipt($data);
      $this->nameCard = $data['name'];
      $this->prepareUserName();
      $this->email = $data['email'];
      $this->phone = $data['phone'];
      $this->zipCode = $data['zip_code'];

      // Get Payment details.
      $this->cardNumber = $data['credit_card']['card_number'];
      $this->cardCode = $data['credit_card']['card_code'];
      $this->expDate = $data['credit_card']['exp_date'];
      $this->amount = $data['amount'];
      $this->transactId = $data['trans_id'];
      $this->description = $data['description'];

      // Common setup for API credentials.
      $merchant_authentication = $this->commonApiCredential();
      $ref_id = 'ref' . time();

      // Create the payment data for a credit card.
      $credit_card = new CreditCardType();
      $credit_card->setCardNumber($this->cardNumber);
      $credit_card->setExpirationDate($this->expDate);
      $credit_card->setCardCode($this->cardCode);
      $payment_one = new PaymentType();
      $payment_one->setCreditCard($credit_card);

      // Create a transaction.
      $transaction_request = new TransactionRequestType();
      $transaction_request->setTransactionType("refundTransaction");
      $transaction_request->setAmount($this->amount);
      $transaction_request->setPayment($payment_one);
      $transaction_request->setRefTransId($this->transactId);

      // Customer information.
      $billto = new CustomerAddressType();
      $billto->setFirstName($this->firstName);
      $billto->setLastName($this->lastName);
      $billto->setZip($this->zipCode);
      $billto->setPhoneNumber($this->phone);
      $billto->setEmail($this->email);
      $transaction_request->setBillTo($billto);
      $customerdata = new CustomerDataType();
      $customerdata->setEmail($this->email);
      $transaction_request->setCustomer($customerdata);

      if ($this->description) {
        $order = new OrderType();
        $order->setDescription($this->description);
        $transaction_request->setOrder($order);
      }

      $request = new CreateTransactionRequest();
      $request->setMerchantAuthentication($merchant_authentication);
      $request->setRefId($ref_id);
      $request->setTransactionRequest($transaction_request);

      $response = $this->executeResponse($request);
      $tresponse = $this->getTransactResponse($response);
      $hash = $tresponse->getTransHash();
      $this->transactTime = time();
      $receipt = array(
        'amount' => (float) $this->amount,
        'checkN' => $tresponse->getAuthCode(),
        'transaction_id' => $tresponse->getTransId(),
        'type' => $this->cardType,
        'ccardN' => $this->saveCardNumber,
        'name' => $this->nameCard,
        'phone' => $this->phone,
        'email' => $this->email,
        'description' => $this->description,
        'created' => $this->transactTime,
        'card_type' => $tresponse->getAccountType(),
        'refunds' => TRUE,
        'receipt_id' => $this->receiptId,
        'pending' => FALSE,
        'is_invoice' => $this->isInvoice,
        'invoice_id' => $this->invoiceId,
        'nid' => $this->nid,
        'trans_hash' => $hash,
        'refund_notes' => $data['refund_notes'] ?? '',
      );

      $receipt = array_merge($receipt, $data_to_merge);

      if (empty($receipt['skip_new_receipt'])) {
        $receipt_id = MoveRequest::setReceipt($this->entityId, $receipt, $this->entityType);
      }
      else {
        $receipt_id = $data['receipt_id'];
      }
      $result['status'] = TRUE;
      $result['receipt_id'] = $receipt_id;
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()}";
      watchdog('Refunds Payment', $message, array(), WATCHDOG_ERROR);
      $result['status'] = FALSE;
      $result['text'] = $e->getMessage();
    }
    finally {
      return $result;
    }
  }

  /**
   * Method for create logs for payment.
   *
   * @param $amount
   * @param $tresponse
   * @param $status
   * @param $response
   * @param $payment_flag
   *
   * @return array
   *   Prepared log data.
   */
  public function preparePaymentLog($amount, $tresponse, $status, $title, $response, $payment_flag = 'regular') {
    if ($response && $status == 'Success') {
      $log_data[] = array(
        'details' => array([
          'activity' => 'System',
          'title' => $title,
          'text' => array([
            'text' => "$status<br>Amount: $$amount<br>Card type: {$tresponse->getAccountType()}<br>Card number: {$tresponse->getAccountNumber()}<br>Payment flag: {$payment_flag}<br>Transaction id: {$tresponse->getTransId()}",
          ]),
          'date' => time(),
        ]),
        'source' => 'System',
      );
    }
    elseif ($response && $status == 'Failed') {
      $log_data[] = array(
        'details' => array([
          'activity' => 'System',
          'title' => $title,
          'text' => array([
            'text' => "Transaction Failed.<br>Error code:{$tresponse->getErrors()[0]->getErrorCode()}.<br>Error message:{$tresponse->getErrors()[0]->getErrorText()}",
          ]),
          'date' => time(),
        ]),
        'source' => 'System',
      );
    }
    elseif ($response && $status == NULL) {
      $log_data[] = array(
        'details' => array([
          'activity' => 'System',
          'title' => $title,
          'text' => array([
            'text' => "Transaction Failed.<br>Error code:{$tresponse->getErrors()[0]->getErrorCode()}.<br>Error message:{$tresponse->getErrors()[0]->getErrorText()}",
          ]),
          'date' => time(),
        ]),
        'source' => 'System',
      );
    }
    else {
      $log_data[] = array(
        'details' => array([
          'activity' => 'System',
          'title' => $title,
          'text' => array([
            'text' => "Transaction Failed. Authorize response empty.",
          ]),
          'date' => time(),
        ]),
        'source' => 'System',
      );
    }

    return $log_data;
  }

  /**
   * Method for create logs for receipt.
   *
   * @param $receipt_id
   * @param $tresponse
   *
   * @return array
   *   Prepared log data.
   */
  public function prepareReceiptLog($tresponse, $receipt_id) {
    $log_data[] = array(
      'details' => array([
        'activity' => 'System',
        'title' => 'Online payment',
        'text' => array([
          'text' => "Receipt for transaction id: {$tresponse->getTransId()} was created with id $receipt_id",
        ]),
        'date' => time(),
      ]),
      'source' => 'System',
    );

    return $log_data;
  }

  private function commonApiCredential() {
    $merchant_authentication = new MerchantAuthenticationType();
    $merchant_authentication->setName($this->apiLoginId);
    $merchant_authentication->setTransactionKey($this->transactionKey);
    return $merchant_authentication;
  }

  /**
   * Execute transaction and get result of operation.
   *
   * @param CreateTransactionRequest $request
   *
   * @throws ResponsePaymentException
   *
   * @return CreateTransactionResponse
   */
  private function executeResponse(CreateTransactionRequest $request) {
    $controller = new CreateTransactionController($request);
    $response = $controller->executeWithApiResponse($this->environment);
    $transaction_response = $response->getTransactionResponse();
    $this->logAuthorizeResponse($response);
    if ($transaction_response->getResponseCode() != 1) {
      $errors = "Response : </br>";
      foreach ($response->getMessages()->getMessage() as $message) {
        $errors .= "{$message->getCode()} {$message->getText()} </br>";
      }

      foreach ($transaction_response->getErrors() as $error) {
        $errors .= "Error: {$error->getErrorCode()} {$error->getErrorText()} </br>";
      }
      throw new ResponsePaymentException("Charge Credit Card ERROR, $errors </br>");
    }

    return $response;
  }

  /**
   * Helper method to get transaction response.
   *
   * @param AnetApiResponseType $response
   *
   * @throws TResponseException
   *
   * @return TransactionResponseType
   */
  private function getTransactResponse(AnetApiResponseType $response) {
    $tresponse = $response->getTransactionResponse();
    if (($tresponse == NULL) || (is_a($response, 'TransactionResponseType') && $tresponse->getResponseCode() != 1)) {
      throw new TResponseException("Charge Credit Card ERROR:  Invalid response");
    }

    return $tresponse;
  }

  private function setEnvironment() {
    $environments = (array) variable_get('environment', array());

    if ($environments['default'] == 'production') {
      $this->environment = ANetEnvironment::PRODUCTION;
    }
    else {
      $this->environment = ANetEnvironment::SANDBOX;
    }
  }

  private function getUserInfo() {
    if ($this->entityType == EntityTypes::MOVEREQUEST) {
      $this->phone = $this->entity['phone'];
      $this->email = $this->entity['email'];
    }
  }

  public static function setAuthorizenet($loginid, $transkey) {
    variable_set('authnet_login_id', static::encryptString($loginid));
    variable_set('authnet_transaction_key', static::encryptString($transkey));
  }

  public static function getAuthorizenet() {
    return array(
      'loginid' => static::decryptString(variable_get('authnet_login_id')),
      'transactionkey' => static::decryptString(variable_get('authnet_transaction_key')),
    );
  }

  public static function encryptString(string $data) : string {
    $current_path = drupal_get_path("module", "move_services_new");
    $file = "$current_path/keys/public.pem";
    $fpp1 = fopen($file,"r");
    $pub_key = fread($fpp1,8192);
    fclose($fpp1);
    openssl_get_publickey($pub_key);
    openssl_public_encrypt($data, $result, $pub_key);
    return base64_encode($result);
  }

  public static function decryptString(string $data) : string {
    $data = base64_decode($data);
    $current_path = drupal_get_path("module", "move_services_new");
    $file = "$current_path/keys/private.pem";
    $fpp1 = fopen($file,"r");
    $private_key = fread($fpp1,8192);
    fclose($fpp1);
    openssl_get_privatekey($private_key);
    openssl_private_decrypt($data, $result, $private_key);
    return $result;
  }

  /**
   * Update receipt info for search.
   *
   * @param array $data
   *   Array data for save.
   */
  public static function updateReceiptSearch($data) {
    $data = (array) $data;
    $needed_fields = array(
      'entity_id',
      'amount',
      'cart_type',
      'credit_card',
      'customer_name',
      'entity_type',
      'phone',
    );
    foreach ($needed_fields as $field) {
      if (isset($data[$field])) {
        $fields_for_update[$field] = $data[$field];
      }
    }
    if (isset($fields_for_update['phone'])) {
      $fields_for_update['phone'] = preg_replace('/[^0-9.]+/', '', $fields_for_update['phone']);
    }
    if (!empty($fields_for_update)) {
      db_update('moveadmin_receipt_search')
        ->fields($fields_for_update)
        ->condition('id', $data['id'])
        ->execute();
    }

  }

  private function refundReceipt(array $data) {
    if (isset($data['receipt_id'])) {
      $this->receiptId = (int) $data['receipt_id'];
    }
    else {
      throw new RefundReceipt("Receipt was not set.");
    }
  }

  /**
   * Prepare First Name and Last Name from name field in card.
   */
  private function prepareUserName() {
    $name = explode(" ", $this->nameCard);
    $this->firstName = isset($name[0]) ? $name[0] : '';
    $this->lastName = isset($name[1]) ? $name[1] : '';
  }

  private function setLogFile() {
    // Create the destination path variable.
    $file_dest = 'private://authorize.net';
    // Create any necessary directories.
    file_prepare_directory($file_dest, FILE_CREATE_DIRECTORY);
    define("AUTHORIZENET_LOG_FILE", "$file_dest/autorize.log");
  }

  public function saveDataBase($e) {
    $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()}";
    watchdog('Payment', $message, array(), WATCHDOG_WARNING);
  }

}

/**
 * Class PaymentException.
 *
 * @package Drupal\move_services_new\Services
 */
class PaymentException extends \Exception {}

/**
 * Class ResponsePaymentException.
 *
 * @package Drupal\move_services_new\Services
 */
class ResponsePaymentException extends PaymentException {}

/**
 * Class TResponseException.
 *
 * @package Drupal\move_services_new\Services
 */
class TResponseException extends PaymentException {}

/**
 * Class RefundReceipt.
 *
 * @package Drupal\move_services_new\Services
 */
class RefundReceipt extends PaymentException {}
