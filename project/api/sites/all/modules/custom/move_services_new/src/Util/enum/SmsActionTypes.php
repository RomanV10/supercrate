<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class SmsActionTypes.
 *
 * @package Drupal\move_services_new\Util
 */
class SmsActionTypes extends BaseEnum {

  const NEW_CLIENT = 1;
  const NEW_REQUEST = 2;
  const REMINDER_TOMORROW = 3;
  const REMINDER_5DAYS = 4;
  const FOREMAN_ASSIGN = 5;
  const REMINDER_RESERVATION = 6;
  const HOME_ESTIMATE_REMINDER = 7;
  const JOB_DONE = 8;
  const JOB_NOT_CONFIRMED = 9;
  const JOB_CONFIRMED = 10;
  const COMPLETE_RESERVATION = 11;
  const STORAGE_INVOICE = 12;
  const FOREMAN_ASSIGN_TO_FOREMAN = 13;
  const REVIEW = 14;

}
