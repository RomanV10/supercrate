<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RequestStatusTypes.
 *
 * @package Drupal\move_services_new\Util
 */
class RequestStatusTypes extends BaseEnum {
  const PENDING = 1;
  const NOT_CONFIRMED = 2;
  const CONFIRMED = 3;
  const INHOME_ESTIMATE = 4;
  const CANCELLED = 5;
  const DAY_N_A = 6;
  const DATE_PENDING = 7;
  const COMPLETE = 8;
  const FLATE_RATE_INVENTORY_NEEDED = 9;
  const FLATE_RATE_PROVIDE_OPTIONS = 10;
  const FLATE_RATE_WAIT_OPTOINS = 11;
  const EXPIRED = 12;
  const WE_ARE_NOT_AVAILABLE = 13;
  const SPAM = 14;
  const NOT_SUBMITTED = 15;
  const PRE_PENDING = 16;
  const PENDING_INFO = 17;
  const NOT_INTERESTED = 18;
  const FROM_PARSER = 19;
  const COMPLETE_SECOND = 20;
  const DEAD_LEAD = 21;
  const ARCHIVE = 22;
  const DRAFT = 23;
}
