<?php

namespace Drupal\move_services_new\Cron\Queues;

use Drupal\move_services_new\Services\Cache;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class ChangeStatusNonActiveToExpired.
 *
 * @package Drupal\move_services_new\Cron\Queues
 */
class ChangeStatusNonActiveToExpired implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \EntityMetadataWrapperException
   */
  public static function execute($data): void {
    /* @var \EntityDrupalWrapper $node_wrapper */
    foreach ($data as $key => $nid) {
      $node_wrapper = entity_metadata_wrapper('node', $nid);
      $node_wrapper->field_approve->set(RequestStatusTypes::EXPIRED);
      $node_wrapper->save();
      Cache::updateCacheData($nid);
    }
  }

}
