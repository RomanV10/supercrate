<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_invoice\Services\Invoice;
use Drupal\move_mail\System\Common;
use Drupal\move_notification\Services\Notification;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_template_builder\Services\TemplateBuilder;
use Drupal\move_new_log\Services\Log;
use Drupal\move_services_new\Util\enum\EntityTypes;

/**
 * Class Clients.
 */
class Clients extends BaseService {
  private $user;

  /**
   * Get user.
   *
   * @return mixed|null
   *   User.
   */
  public function getUser(): \stdClass {
    return $this->user;
  }

  /**
   * Set User.
   *
   * @param mixed|null $user
   *   User.
   */
  public function setUser(\stdClass $user): void {
    $this->user = $user;
  }

  /**
   * Constructor of Clients service.
   *
   * @param int $uid
   *   User id.
   */
  public function __construct($uid = NULL) {
    if ($uid) {
      $this->user = user_load((int) $uid);
    }
  }

  /**
   * Create new user.
   *
   * @param mixed $account
   *   Object containing account information. The $account object should
   *   contain, at minimum, the following properties:
   *     - name (user name)
   *     - mail (email address)
   *     - pass (plain text unencrypted password).
   *
   * @return mixed
   *   Return user ID.
   *
   * @throws \Exception
   */
  public function create($account = array()) {
    $result = FALSE;
    $admin_create = FALSE;
    if (isset($account['account']) && $userData = $account['account']) {
      if (isset($userData['name']) && isset($userData['mail'])) {
        // Check if user name or user email already exist.
        if (!$this->checkExistUser($userData, TRUE)) {
          try {
            // Check roles of user.
            $userData['roles'] = isset($userData['roles']) ? $this->findRoles($userData['roles']) : [];
            // Active user for default.
            $userData['status'] = $userData['status'] ?? 1;
            // Generate password if not exist.
            isset($userData['pass']) ? $admin_create = TRUE : $userData['pass'] = $this->generatePassword();
            // Unset settings.
            if (isset($userData['settings'])) {
              $settings = $userData['settings'];
              unset($userData['settings']);
            }
            // Unset notification.
            if (!empty($userData['notifications'])) {
              foreach ($userData['notifications'] as $item) {
                if (!empty($item['selected'])) {
                  $notifications[] = $item['ntid'];
                }
              }
              unset($userData['notifications']);
            }
            /* @var \EntityDrupalWrapper $user */
            $user = entity_metadata_wrapper('user', entity_create('user', []));
            foreach ($userData as $field_name => $field_data) {
              if (is_array($field_data)) {
                if ($field_name == 'roles') {
                  $user->{$field_name} = $field_data;
                  continue;
                }
                foreach ($field_data as $name => $value) {
                  $user->{$name} = $value;
                }
              }
              else {
                $user->{$field_name} = $field_data;
              }
            }
            $user->save();
            if (!empty($notifications)) {
              (new Notification())->addUserToManyNotificationTypes($user->uid->value(), $notifications);
            }
            // Save settings.
            if (!empty($settings)) {
              $this->prepareEmailSettings($settings);
              Users::writeUserSettings($user->uid->value(), $settings);
            }

            if (!$admin_create) {
              variable_set($userData['mail'], $userData['pass']);
            }
            $result = $user->value();
          }
          catch (\Throwable $e) {
            $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
            watchdog('Create user', $error_text, [], WATCHDOG_ERROR);
          }
        }
        else {
          services_error('This login already use', 406);
        }
      }
    }

    return $result;
  }

  /**
   * Prepare email settings.
   *
   * @param array $settings
   *   User settings.
   */
  private function prepareEmailSettings(array &$settings) {
    if (isset($settings['email_account']['name']) && isset($settings['email_account']['password'])) {
      $settings['email_account']['name'] = trim(strtolower($settings['email_account']['name']));
      $settings['email_account']['password'] = Common::encryptPassword($settings['email_account']['password']);
    }
  }

  /**
   * Generate password.
   *
   * @param int $length
   *   Length.
   *
   * @return string
   *   Password.
   */
  public function generatePassword(int $length = 5) {
    $pass = '';
    $allowable_characters = '12345678qwertyuiopasdfghjklzxcvbnm';

    // Zero-based count of characters in the allowable list:
    $len = strlen($allowable_characters) - 1;

    // Loop the number of times specified by $length.
    for ($i = 0; $i < $length; $i++) {
      // Each iteration, pick a random character from the
      // allowable string and append it to the password:
      $pass .= $allowable_characters[mt_rand(0, $len)];
    }

    return $pass;
  }

  /**
   * Check email host.
   *
   * @param string $email
   *   Email.
   * @param string $host
   *   Host.
   *
   * @return array
   *   Email data.
   */
  protected function checkEmailHost(string $email, string $host) {
    $new_email = array();
    if (filter_var($email, FILTER_VALIDATE_EMAIL) && $parse = imap_rfc822_parse_adrlist($email, '')) {
      $parse = reset($parse);
      $new_email['name'] = strtolower($parse->mailbox);
      $new_email['email'] = "{$new_email['name']}@{$host}";
    }

    return $new_email;
  }

  /**
   * Retrieve operation.
   *
   * @return mixed|null
   *   User object.
   *
   * @throws \ServicesException
   * @throws \Exception
   */
  public function retrieve() {
    if ($this->user) {
      return $this->getUserFieldsData($this->getUid());
    }
    else {
      return services_error(t('There is no user with ID @uid.', array('@uid' => $this->getUid())), 406);
    }
  }

  /**
   * Update user.
   *
   * @param mixed $user_data
   *   User data.
   *
   * @return array|mixed
   *   Result update.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public function update($user_data = array()) {
    $error_flag = FALSE;
    $error_message = '';
    $result = array(
      'update' => FALSE,
    );
    /* @var \EntityDrupalWrapper $wrapper */
    $wrapper = entity_metadata_wrapper('user', $this->user);
    $fields = $user_data['data'];
    $fields += $user_data['fields'] ?? [];
    try {
      $fields_not_in_entity = ['data', 'metadata'];
      // Check field name and email for unique.
      if ($this->checkUniqueDataUser($fields)['unique']) {
        foreach ($fields as $field_name => $field_data) {
          if (!in_array($field_name, $fields_not_in_entity)) {
            if ($field_name == 'roles') {
              $wrapper->{$field_name}->set($this->findRoles($field_data));
              continue;
            }
            if ($field_name == 'status') {
              $field_data = (int) $field_data;
            }
            $wrapper->{$field_name}->set($field_data);
          }
        }
        $wrapper->save();
        // Save settings.
        if (isset($user_data['settings'])) {
          $this->prepareEmailSettings($user_data['settings']);
          Users::writeUserSettings($this->getUid(), $user_data['settings']);
        }
        $result = array(
          'update' => TRUE,
          'user' => $this->getUserFieldsData($this->user->uid, FALSE),
        );
      }
      else {
        throw new CheckLoginException('This login already use', 406);
      }

      module_invoke_all('move_user_update', $user_data, $wrapper);
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()}";
      watchdog('Clients Update', $message, array(), WATCHDOG_ERROR);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      if ($error_flag) {
        return services_error('Client Update Error', 406, array(
          'response' => array(
            'error_message' => $error_message,
            'validation_errors' => $validation_errors ?? array(),
          ),
        ));
      }
      else {
        return $result;
      }
    }
  }

  /**
   * Check unique data user.
   *
   * @param array $fields
   *   Fields.
   *
   * @return array
   *   True or false.
   */
  private function checkUniqueDataUser(array $fields) {
    $result = array('unique' => TRUE);
    $original_user = entity_load_unchanged('user', $this->getUid());

    if (isset($fields['name']) && $original_user->name != $fields['name']) {
      if (user_load_by_name((string) $fields['name'])) {
        $result = array(
          'unique' => FALSE,
          'user_data' => user_load_by_name((string) $fields['name']),
        );
      }
    }
    elseif (isset($fields['mail']) && $original_user->mail != $fields['mail']) {
      if (user_load_by_mail((string) $fields['mail'])) {
        $result = array(
          'unique' => FALSE,
          'user_data' => user_load_by_mail((string) $fields['mail']),
        );
      }
    }

    return $result;
  }

  /**
   * Write user email settings to user_settings.
   *
   * @param array $data
   *   Data for write.
   *
   * @return bool
   *   Result of operation.
   */
  public function updateEmailSettings(array $data) {
    $result = FALSE;
    if ($data) {
      $settings = Users::getUserSettings($this->getUid());
      $settings['email_account'] = $data;
      $this->prepareEmailSettings($settings);
      $result = Users::writeUserSettings($this->getUid(), $settings);
    }

    return $result;
  }

  /**
   * Write user settings to user_settings.
   *
   * @param int $uid
   *   User id.
   * @param array $data
   *   Data for write.
   *
   * @return bool
   *   Result of operation.
   */
  public static function updateUserSettings(int $uid, array $data) {
    $result = FALSE;
    if ($data) {
      $settings = Users::getUserSettings($uid);
      $settings = array_merge($settings, $data);

      $result = Users::writeUserSettings($uid, $settings);
    }

    return $result;
  }

  /**
   * Get list of users.
   *
   * @param int $page
   *   Number of current page.
   * @param int $pagesize
   *   Count elements on page.
   * @param string $fields
   *   List fields for extract.
   *
   * @return array
   *   List of users.
   *
   * @throws \Exception
   */
  public function index($page, $pagesize, $fields) {
    return $this->getUsers($page, $pagesize, $fields);
  }

  /**
   * Remove user.
   *
   * If client have confirmed request don't deleting client impossible.
   * If in request have a payment need warn.
   *
   * @return mixed
   *   Exception.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public function delete() {
    $uid = $this->getUid();
    $query = new \EntityFieldQuery();

    $query->entityCondition('entity_type', 'node')
      ->propertyCondition('status', 1)
      ->fieldCondition('field_approve', 'value', 3, '=')
      ->propertyCondition('uid', $uid);
    $result = $query->execute();

    if ($result) {
      return services_error(t('This user have confirmed request. Delete impossible.'), 406);
    }
    else {
      $query_all = new \EntityFieldQuery();
      $query_all->entityCondition('entity_type', 'node')
        ->propertyCondition('uid', $uid);
      $requests = $query_all->execute();

      foreach ($requests['node'] as $nid => $value) {
        node_delete($nid);
        watchdog('node', 'Move request: %nid deleted', array('%nid' => $nid), WATCHDOG_NOTICE, $link = NULL);
        Cache::removeCacheData($nid);
      }

      user_delete($uid);
      watchdog('user', 'Deleted user: %userid, %name, %email.',
        array(
          '%userid' => $this->user->uid,
          '%name' => $this->user->name,
          '%email' => '<' . $this->user->mail . '>',
        ),
        WATCHDOG_NOTICE, $link = NULL);

      return TRUE;
    }
  }

  /**
   * Current loaded user.
   *
   * @throws \Exception
   */
  public function getCurrentUser() {
    $fields = $this->getUserFieldsData($this->getUid());
    $fields['credentials'] = MoveRequest::getCurrentBrowserAndIp();

    return $fields;
  }

  /**
   * Get user id.
   *
   * @return mixed
   *   Uid or exception.
   */
  public function getUid() {
    if (!$this->user || !property_exists((Object) $this->user, 'uid')) {
      throw new ClientsUIDException("User does not defined");
    }

    return $this->user->uid;
  }

  /**
   * Helper function to get list of users.
   *
   * @param int $page
   *   Page.
   * @param int $pageSize
   *   Page size.
   * @param mixed $fields
   *   Fields.
   *
   * @return array
   *   Users.
   *
   * @throws \ServicesException
   * @throws \Exception
   */
  protected function getUsers(int $page, int $pageSize, $fields) {
    $table_alias = 't';
    $fields = ($fields == '*') ? 'uid' : $fields;
    $user_select = db_select('users', $table_alias)
      ->orderBy('created', 'DESC');
    $this->buildIndexQuery($user_select, $page, $pageSize, $fields, 'user', $table_alias);
    $users = $this->buildIndexList($this->executeIndexQuery($user_select));
    $data = array();
    foreach ($users as $user) {
      $data[$user->uid] = $this->getUserFieldsData($user->uid);
    }

    return $data;
  }

  /**
   * Helper function to check if user already existing.
   *
   * @param array $user
   *   Array of user data.
   * @param bool $check_by_name
   *   Id we need to check by name.
   *
   * @return mixed
   *   UID if exist.
   */
  public function checkExistUser(array $user = array(), bool $check_by_name = FALSE) {
    $result = FALSE;
    if (isset($user['mail']) && !empty($user_new = user_load_by_mail($user['mail']))) {
      $result = $user_new->uid;
    }
    elseif (isset($user['name']) && $check_by_name && !empty($user_new = user_load_by_name($user['name']))) {
      $result = $user_new->uid;
    }

    return $result;
  }

  /**
   * Helper function to check roles in user roles.
   *
   * @param array $roles
   *   List of roles.
   *
   * @return bool
   *   Result of check.
   */
  public function checkUserRoles(array $roles = array()) {
    $intersect = FALSE;
    if (isset($this->user->roles)) {
      $intersect = array_intersect((array) $this->user->roles, $roles);
    }

    return ($intersect) ? TRUE : FALSE;
  }

  /**
   * Get all nodes of user.
   *
   * @param int $uid
   *   User ID.
   *
   * @return array
   *   Nodes of user.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function getUserNodes(int $uid) {
    $nodes = array();
    $query = new \EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');
    $query->entityCondition('bundle', 'move_request');
    $query->propertyCondition('uid', $uid);
    $result = $query->execute();
    if (isset($result['node'])) {
      $move_request = new MoveRequest();
      foreach ($result['node'] as $node) {
        /* Check that the user does not see in the account
        request with the status of the archive.*/
        $temp_node = $move_request->getNode($node->nid);
        if ($temp_node['status']['raw'] != 22) {
          $nodes[$node->nid] = $temp_node;
        }
      }
    }

    return $nodes;
  }

  /**
   * Helper function to get all roles of users.
   *
   * @return array
   *   List all roles.
   */
  protected function getAllRoles() {
    return user_roles();
  }

  /**
   * Helper function to find roles.
   *
   * @param array $roles
   *   List roles to find.
   *
   * @return array
   *   Found roles.
   */
  protected function findRoles(array $roles) {
    $all_roles = $this->getAllRoles();
    $result_roles = array();
    foreach ($roles as $role) {
      if ($id = array_search($role, $all_roles)) {
        $result_roles[$all_roles[$id]] = $id;
      }
    }

    return $result_roles;
  }

  /**
   * Find user.
   *
   * @param array $data
   *   Data.
   *
   * @return array
   *   User fields.
   *
   * @throws \Exception
   */
  public function findUser(array $data = array()) {
    if (isset($data['email'])) {
      $user = user_load_by_mail($data['email']);
    }
    elseif (isset($data['name'])) {
      $user = user_load_by_name($data['name']);
    }

    return $this->getUserFieldsData($user->uid ?? NULL);
  }

  /**
   * Get user data.
   *
   * @param int|null $uid
   *   User id.
   * @param bool $nodes
   *   With node.
   *
   * @return array
   *   List of user data.
   *
   * @throws \Exception
   */
  public function getUserFieldsData(?int $uid = NULL, bool $nodes = TRUE) {
    $fields = &drupal_static(__METHOD__ . ":$uid", array());

    if (!$fields) {
      $uid = ($uid !== NULL) ? (int) $uid : $this->getUid();
      if (!db_query("SELECT COUNT(*) FROM {users} WHERE uid = :uid", array(':uid' => $uid))->fetchField()) {
        $fields['field_user_first_name'] = 'Deleted';
        $fields['field_user_last_name'] = 'user';
        return $fields;
      }
      $emw = entity_metadata_wrapper('user', $uid);
      if (isset($emw->value()->picture)) {
        $fields['user_picture'] = $emw->value()->picture;
        $fields['user_picture']->url = file_create_url($fields['user_picture']->uri);
      }
      foreach ($emw->getPropertyInfo() as $key => $prop) {
        if ($key == 'roles') {
          $roles = array();
          $rids = $emw->{$key}->value();
          foreach ($rids as $rid) {
            $role = user_role_load($rid);
            $roles[] = $role->name;
          }

          $fields[$key] = $roles;
        }
        else {
          $fields[$key] = $emw->{$key}->value();
        }
      }

      // Get nodes related with user.
      if ($nodes) {
        $fields['nodes'] = $this->getUserNodes($uid);
      }

      // Add settings.
      $fields['settings'] = Users::getUserSettings($uid);
    }

    return $fields;
  }

  /**
   * Checking what manager have mail settings.
   *
   * @param array $user_data
   *   Result getUserFieldsData.
   *
   * @return bool
   *   Result of operation.
   *
   * @throws \Exception
   */
  public function managerHaveMailSettings(array $user_data) : bool {
    $result = FALSE;
    if ($this->checkUserRoles(array('manager', 'sales'))) {
      $name = $user_data['settings']['email_account']['name'] ?? '';
      $password = $user_data['settings']['email_account']['password'] ?? '';
      $host = $user_data['settings']['email_account']['outgoing']['server'] ?? '';
      $port = $user_data['settings']['email_account']['outgoing']['port'] ?? '';
      $result = ($name && $password && $host && $port) ? TRUE : FALSE;
    }

    return $result;
  }

  /**
   * Generating link for restore password and send mail.
   *
   * @param string $email
   *   Email for send restore link.
   *
   * @return array
   *   Mail send.
   *
   * @throws \Exception
   */
  public function linkRestorePassword(string $email) {

    try {
      $this->findUser(array('email' => $email));
    }
    catch (\Throwable $exception) {
      return array('send' => FALSE);
    }

    return (new TemplateBuilder)->moveTemplateBuilderSendTemplateRestorePassword($email);
  }

  /**
   * Link restore.
   *
   * @param int $uid
   *   User id.
   * @param int $timestamp
   *   Timestamp.
   * @param string $hashed_pass
   *   Hash password.
   *
   * @return bool|int|string
   *   Token.
   *
   * @throws \Exception
   */
  public function linkRestore(int $uid, int $timestamp, string $hashed_pass) {
    global $user;
    $token = FALSE;

    $timeout = variable_get('user_password_reset_timeout', 604800);
    $current = REQUEST_TIME;
    $users = user_load_multiple(array($uid), array('status' => '1'));
    if ($timestamp <= $current && $account = reset($users)) {
      // No time out for first time login.
      if ($account->login && $current - $timestamp > $timeout) {
        $this->linkRestorePassword($account->mail);
        $token = 3;
      }
      elseif ($account->uid && $timestamp >= $account->login && $timestamp <= $current && $hashed_pass == user_pass_rehash($account->pass, $timestamp, $account->login, $account->uid)) {
        $user = $account;

        // user_login_finalize();
        $edit = array();
        $_COOKIE = array();
        drupal_save_session(TRUE);
        drupal_session_started(FALSE);
        drupal_session_regenerate();
        drupal_session_commit();
        user_module_invoke('login', $edit, $user);
        $token = drupal_get_token('services');
      }
    }

    return $token;
  }

  /**
   * Restore password.
   */
  public function restorePassword() {
    global $user;

    if ($user->uid) {
      $hashthepass = user_password(8);
      require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
      $hashedpass = user_hash_password(trim($hashthepass), 9);

      db_update('users')
        ->fields(array('pass' => $hashedpass))
        ->condition('uid', $user->uid)
        ->execute();

      drupal_mail('system', 'mail', $user->email, language_default(), array(
        'context' => array(
          'subject' => 'Your password has been restored!',
          'message' => "Your new password: $hashthepass",
        ),
      ));
    }
  }

  /**
   * Hash for uid.
   *
   * @param string|null $email
   *   Email.
   *
   * @return array
   *   Result.
   */
  public function oneTimeHash(?string $email) {
    $result = [];

    try {
      if ($email) {
        // Check what email is exist.
        $user = user_load_by_mail($email);
        if ($user) {
          $result = static::prepareOneTimeHashParams($user);
        }
      }
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog('Get one time hash', $error_text, [], WATCHDOG_CRITICAL);
    }
    finally {
      return $result;
    }
  }

  /**
   * Prepare data for create one time hash link.
   *
   * @param mixed $user
   *   User data.
   *
   * @return array
   *   One time hash data.
   */
  public static function prepareOneTimeHashParams($user) : array {
    $result = array();

    if ($user) {
      $timestamp = REQUEST_TIME;
      $hash = user_pass_rehash($user->pass, $timestamp, $user->login, $user->uid);
      $result = array(
        'uid' => $user->uid,
        'hash' => $hash,
        'timestamp' => $timestamp,
      );
    }

    return $result;
  }

  /**
   * Method for update user and all requests.
   *
   * @param array $client_data
   *   User data for update.
   *
   * @return array
   *   Updated nids.
   *
   * @throws \Exception
   * @throws \Throwable
   */
  public function updateClientAllRequests(array $client_data) {
    if (isset($client_data['data']['nid'])) {
      $clientDataNid = $client_data['data']['nid'];
      (new DelayLaunch())->createTask(TaskType::CREATE_REQUEST_SEND_MAIL, ['nid' => $clientDataNid, 'changeDraftEmail' => TRUE]);
      unset($client_data['data']['nid']);
    }
    $result = array();
    $client_data_old_obj = entity_metadata_wrapper('user', $this->getUid());
    $client_data_old = array(
      'first_name' => $client_data_old_obj->field_user_first_name->value(),
      'last_name' => $client_data_old_obj->field_user_last_name->value(),
      'email' => $client_data_old_obj->mail->value(),
      'phone' => $client_data_old_obj->field_primary_phone->value(),
      'field_additional_phone' => $client_data_old_obj->field_user_additional_phone->value(),
    );

    if ($client_data_old['email'] != $client_data['data']['mail']) {
      if ($new_uid = $this->checkExistUser($client_data['data'])) {
        return services_error('This mail already exist', 500, $new_uid);
      }
    }
    // Update user.
    $client_update = $this->update($client_data);

    // If user update true, search request.
    if (!empty($client_update['update'])) {
      $user_nodes = $this->getUserNodes($this->getUid());

      // If we found requests update its user info.
      if (!empty($user_nodes)) {
        $invoiceInstance = new Invoice();
        foreach ($user_nodes as $nid => $node) {
          // Update user in invoice.
          $invoices = $invoiceInstance->getInvoiceForEntity($nid, EntityTypes::MOVEREQUEST);
          if (!empty($invoices)) {
            foreach ($invoices as $invoice) {
              $invoice['data']['client_name'] = $client_data['data']['field_user_first_name'] . ' ' . $client_data['data']['field_user_last_name'];
              $invoice['data']['email'] = $client_data['data']['mail'];
              $invoice['data']['phone'] = $client_data['data']['field_primary_phone'];
              Invoice::updateInvoiceData($invoice['id'], $invoice['data']);
            }
          }
          $client_data_for_up_request = array(
            'field_e_mail' => $client_data['data']['mail'],
            'field_first_name' => $client_data['data']['field_user_first_name'],
            'field_last_name' => $client_data['data']['field_user_last_name'],
            'field_phone' => $client_data['data']['field_primary_phone'] ?? '',
            'field_additional_phone' => $client_data['data']['field_user_additional_phone'] ?? '',
          );

          $result[$nid] = (new MoveRequest($nid))->update2($client_data_for_up_request);
          if (!empty($result[$nid])) {
            $this->createClientRequestUpdateLog($nid, $client_data, $client_data_old);
          }
        }
      }
    }

    return $result;
  }

  /**
   * Method for write user log in request about change users.
   *
   * @param int $nid
   *   Unique node id.
   * @param array $client_data
   *   Updated User data.
   * @param array $client_data_old
   *   Old client data.
   */
  public function createClientRequestUpdateLog(int $nid, array $client_data, array $client_data_old) {
    global $user;
    $user_name = $user->name ?? 'System';

    $client_data_all = $this->prepareClientDataForLog($client_data, $client_data_old);
    $log_text = array();
    $log_text[]['simpleText'] = "Client info was updated from {$client_data_all['old']['first_name']} {$client_data_all['old']['last_name']} {$client_data_all['old']['email']} {$client_data_all['old']['phone']}";
    $log_text[]['simpleText'] = "To {$client_data_all['new']['first_name']} {$client_data_all['new']['last_name']} {$client_data_all['new']['email']} {$client_data_all['new']['phone']}";

    $log_data[] = array(
      'details' => array([
        'event_type' => 'Change client',
        'activity' => $user_name,
        'title' => 'Request client was updated',
        'text' => $log_text,
        'date' => time(),
      ],
      ),
      'source' => $user_name,
    );

    (new Log($nid, EntityTypes::MOVEREQUEST))->create($log_data);
  }

  /**
   * Prepare data log with old user and new user.
   *
   * @param array $client_data
   *   New user data.
   * @param array $client_data_old
   *   Old user data.
   *
   * @return array
   *   Prepared array users data.
   */
  private function prepareClientDataForLog(array $client_data, array $client_data_old) {
    $client_data_all = array(
      'old' => array(
        'first_name' => $client_data_old['first_name'],
        'last_name' => $client_data_old['last_name'],
        'email' => $client_data_old['email'],
        'phone' => $client_data_old['phone'],
        'field_additional_phone' => $client_data_old['field_additional_phone'] ?? '',
      ),
      'new' => array(
        'first_name' => $client_data['data']['field_user_first_name'],
        'last_name' => $client_data['data']['field_user_last_name'],
        'email' => $client_data['data']['mail'],
        'phone' => $client_data['data']['field_primary_phone'] ?? '',
        'field_additional_phone' => $client_data['data']['field_user_additional_phone'] ?? '',
      ),
    );

    return $client_data_all;
  }

  /**
   * Create user and set in request.
   *
   * @param int $nid
   *   Node id.
   * @param array $client_data
   *   Client data for create.
   *
   * @return array
   *   Request id.
   *
   * @throws \Exception
   */
  public function createUserUpdateRequest(int $nid, array $client_data) : array {
    $new_user = $this->create($client_data);
    $new_user_wrap = entity_metadata_wrapper('user', $new_user->uid);

    $client_data_for_up_request = array(
      'author' => $new_user_wrap->getIdentifier(),
      'field_first_name' => $new_user_wrap->field_user_first_name->value(),
      'field_last_name' => $new_user_wrap->field_user_last_name->value(),
      'field_e_mail' => $new_user_wrap->mail->value(),
      'field_phone' => $new_user_wrap->field_primary_phone->value(),
      'field_additional_phone' => $new_user_wrap->field_user_additional_phone->value(),
    );

    if ($new_user) {
      $result = array(
        'user' => $new_user_wrap->getIdentifier(),
        'nid' => (new MoveRequest($nid))->update2($client_data_for_up_request),
      );
    }

    return isset($result) ? $result : array('user' => NULL, 'nid' => NULL);
  }

  /**
   * Check if user was login from home estimate.
   *
   * @return bool
   *   TRUE if yes. FALSE if NOT.
   */
  public function isUserLoginFromHomeEstimate() : bool {
    return empty($this->user->field_is_home_estimate_login[LANGUAGE_NONE][0]['value']) ? FALSE : TRUE;
  }

  /**
   * Check if login exist.
   *
   * @param string $name
   *   Name.
   *
   * @return bool
   *   Bool.
   */
  public function checkLoginExist(string $name) {
    $result = FALSE;
    if (!empty(user_load_by_name($name))) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Get user fields from table users.
   *
   * @param int $uid
   *   User id.
   * @param array $fields
   *   Needed fields.
   *
   * @return mixed
   *   Fields.
   */
  public static function getUserFields(int $uid, array $fields) {
    return db_select('users', 'u')
      ->fields('u', $fields)
      ->condition('u.uid', $uid)
      ->execute()->fetchAssoc();
  }

  /**
   * Get move request author phone number.
   *
   * @param int $nid
   *   Move request id.
   *
   * @return mixed
   *   Phone number.
   */
  public static function getAuthorPhoneNumberByNid(int $nid) {
    return db_select('field_data_field_phone', 'fp')
      ->fields('fp', ['field_phone_value'])
      ->condition('fp.entity_id', $nid)
      ->execute()
      ->fetchField();
  }

  /**
   * Get user primary phone number.
   *
   * @param int $uid
   *   User id.
   *
   * @return mixed
   *   Phone number.
   */
  public static function getUserPrimaryPhoneNumber(int $uid) {
    return db_select('field_data_field_primary_phone', 'fpp')
      ->fields('fpp', ['field_primary_phone_value'])
      ->condition('fpp.entity_id', $uid)
      ->execute()
      ->fetchField();
  }

  /**
   * Get move request author phone number.
   *
   * @param int $nid
   *   Move request id.
   *
   * @return mixed
   *   Phone number.
   */
  public static function getAuthorEmailByNid(int $nid) {
    return db_select('field_data_field_e_mail', 'fem')
      ->fields('fem', ['field_e_mail_email'])
      ->condition('fem.entity_id', $nid)
      ->execute()
      ->fetchField();
  }

}

/**
 * Class ClientsException.
 *
 * @package Drupal\move_services_new\Services
 */
class ClientsException extends \RuntimeException {

  /**
   * Save information about error to database.
   *
   * @param int $type
   *   A watchdog severity of the message.
   */
  public function saveDataBase($type) {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()} </br> Trace: {$this->getTraceAsString()}";
    watchdog('Clients', $message, array(), $type);
  }

}

/**
 * Class CommentsRetrieve.
 *
 * @package Drupal\move_services_new\Services
 */
class ClientsUIDException extends ClientsException {}

/**
 * Class CheckLoginException.
 *
 * @package Drupal\move_services_new\Services
 */
class CheckLoginException extends ClientsException {
  /**
   * CheckLoginException constructor.
   *
   * @param string $message
   *   Message.
   * @param int $code
   *   Code.
   * @param \Throwable|null $previous
   *   Error interface.
   */
  public function __construct(string $message = "", int $code = 0, \Throwable $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }

}
