<?php

namespace Drupal\move_services_new\System;

use Drupal\move_services_new\Receipts\Routes\ReceiptRoute;
use Drupal\move_services_new\System\Resources\CommentDefinition;
use Drupal\move_services_new\System\Resources\ClientsDefinition;
use Drupal\move_services_new\System\Resources\FrontDefinition;
use Drupal\move_services_new\System\Resources\MoveInvertoryDefinition;
use Drupal\move_services_new\System\Resources\MoveRequestDefinition;
use Drupal\move_services_new\System\Resources\MoveUsersDefinition;
use Drupal\move_services_new\System\Resources\PaymentDefinition;
use Drupal\move_services_new\System\Resources\PayrollDefinition;
use Drupal\move_services_new\System\Resources\SettingsDefinition;

/**
 * Class definition.
 *
 * @package Drupal\move_services_new\System
 */
class Services {

  public function getDefinitions() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += CommentDefinition::getDefinition();
    $resources += ClientsDefinition::getDefinition();
    $resources += FrontDefinition::getDefinition();
    $resources += MoveInvertoryDefinition::getDefinition();
    $resources += MoveRequestDefinition::getDefinition();
    $resources += MoveUsersDefinition::getDefinition();
    $resources += PaymentDefinition::getDefinition();
    $resources += PayrollDefinition::getDefinition();
    $resources += SettingsDefinition::getDefinition();
    $resources += ReceiptRoute::getRoutes();

    return $resources;
  }

}
