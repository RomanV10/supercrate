<?php

namespace Drupal\move_services_new\Services\move_request;

use Drupal\move_request_score\Services\MoveRequestScore;
use Drupal\move_services_new\Services\ExtraServices;
use Drupal\move_services_new\Services\Receipt;
use Drupal\move_services_new\System\Extra;
use Drupal\move_statistics\Services\Statistics;
use EntityMetadataWrapper as Wrapper;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Services\Inventory;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_long_distance\Services\LongDistanceJob;

/**
 * Class MoveRequestRetrieve.
 */
class MoveRequestRetrieve {

  public $timeZone = 'UTC';
  protected $node;
  private $init;

  /**
   * Constructor of MoveRequest service.
   *
   * @param \EntityMetadataWrapper $node
   *   Node object.
   * @param bool $init
   *   If request create init true.
   */
  public function __construct(Wrapper $node, bool $init = FALSE) {
    $this->node = $node;
    $this->timeZone = Extra::getTimezone();
    $this->init = $init;
  }

  /**
   * Get short node array fields.
   *
   * @param array $nodeFields
   *   Retrieve move request fields.
   *
   * @return array
   *   Node fields.
   *
   * @throws \Exception
   */
  public function smallNodeFields($nodeFields = array()) : array {
    $move_request = array();

    if (empty($nodeFields)) {
      $nodeFields = $this->nodeFields();
    }

    $fields = array(
      'field_date_confirmed',
      'date',
      'move_size',
      'name',
      'first_name',
      'last_name',
      'from',
      'to',
      'created',
      'changed',
      'field_parser_provider_name',
      'manager',
      'field_flags',
      'status',
      'nid',
      'service_type',
      'trucks',
      'crew',
      'maximum_time',
      'start_time1',
      'start_time2',
      'field_reservation_received',
      'travel_time',
      'zip_from',
      'zip_to',
      'nid',
      'type_from',
      'type_to',
      'distance',
      'phone',
      'email',
      'field_company_flags',
      // Need for correct work when foreman login.
      'field_foreman_assign_time',
      'field_total_score',
      'field_total_score_updated',
    );

    foreach ($fields as $field_name) {
      $move_request[$field_name] = $nodeFields[$field_name];
    }

    return $move_request;
  }

  /**
   * Get fields of node.
   *
   * @return array
   *   Array of node fields.
   *
   * @throws \Exception
   */
  public function nodeFields() {
    global $base_path;
    $move_request = [];
    $nid = $this->getFieldData('nid') ?? 0;

    $client_inst = new Clients();

    // Field_date Move Date  07/31/2015.
    $move_request['date']['value'] = $this->dateConvert($this->getFieldData('field_date'), "m/d/Y");
    $move_request['date']['label'] = $this->getFieldInfo('field_date');
    $move_request['date']['raw'] = strtotime($this->dateConvert($this->getFieldData('field_date')));
    $move_request['date']['field'] = 'field_date';
    $move_request['date']['old'] = $move_request['date']['value'];

    // Field_approve.
    $move_request['status'] = array();
    $move_request['status']['value'] = $this->fieldList('field_approve');
    $move_request['status']['label'] = $this->getFieldInfo('field_approve');
    $move_request['status']['field'] = 'field_approve';
    $move_request['status']['raw'] = $this->fieldList('field_approve', TRUE);
    $move_request['status']['old'] = $move_request['status']['raw'];

    // Minimum Work Time.
    $move_request['minimum_time'] = array();
    $move_request['minimum_time']['value'] = $this->getReadableTime($this->getFieldData('field_minimum_move_time'), TRUE);
    $move_request['minimum_time']['label'] = $this->getFieldInfo('field_minimum_move_time');
    $move_request['minimum_time']['field'] = 'field_minimum_move_time';
    $move_request['minimum_time']['raw'] = floatval($this->getFieldData('field_minimum_move_time'));
    $move_request['minimum_time']['old'] = $move_request['minimum_time']['value'];

    // Maximum work time.
    $move_request['maximum_time'] = array();
    $move_request['maximum_time']['value'] = $this->getReadableTime($this->getFieldData('field_maximum_move_time'), TRUE);
    $move_request['maximum_time']['label'] = $this->getFieldInfo('field_maximum_move_time');
    $move_request['maximum_time']['field'] = 'field_maximum_move_time';
    $move_request['maximum_time']['raw'] = floatval($this->getFieldData('field_maximum_move_time'));
    $move_request['maximum_time']['old'] = $move_request['maximum_time']['value'];

    // Travel TIme.
    $move_request['travel_time'] = array();
    $move_request['travel_time']['value'] = $this->getReadableTime($this->getFieldData('field_travel_time'), TRUE);
    $move_request['travel_time']['label'] = $this->getFieldInfo('field_travel_time');
    $move_request['travel_time']['field'] = 'field_travel_time';
    $move_request['travel_time']['old'] = $move_request['travel_time']['value'];
    $move_request['travel_time']['raw'] = floatval($this->getFieldData('field_travel_time'));

    // Move Size.
    $move_request['move_size']['value'] = $this->fieldList('field_size_of_move');
    $move_request['move_size']['label'] = $this->getFieldInfo('field_size_of_move');
    $move_request['move_size']['field'] = 'field_size_of_move';
    $move_request['move_size']['raw'] = $this->fieldList('field_size_of_move', TRUE);
    $move_request['move_size']['old'] = $move_request['move_size']['raw'];

    // Service Type.
    $move_request['service_type']['value'] = $this->fieldList('field_move_service_type');
    $move_request['service_type']['label'] = $this->getFieldInfo('field_move_service_type');
    $move_request['service_type']['field'] = 'field_move_service_type';
    $move_request['service_type']['raw'] = $this->getFieldData('field_move_service_type');
    $move_request['service_type']['old'] = $move_request['service_type']['raw'];

    // FIELD CREW_COUNT.
    $move_request['crew'] = array();
    $move_request['crew']['value'] = $this->getFieldData('field_movers_count');
    $move_request['crew']['label'] = $this->getFieldInfo('field_movers_count');
    $move_request['crew']['field'] = 'field_movers_count';
    $move_request['crew']['old'] = $move_request['crew']['value'];

    $move_request['field_delivery_crew_size']['value'] = $this->getFieldData('field_delivery_crew_size');
    $move_request['field_delivery_crew_size']['label'] = $this->getFieldInfo('field_delivery_crew_size');
    $move_request['field_delivery_crew_size']['raw'] = $move_request['field_delivery_crew_size']['value'];
    $move_request['field_delivery_crew_size']['field'] = 'field_delivery_crew_size';

    // FIELD RATE.
    $move_request['rate']['value'] = $this->getFieldData('field_price_per_hour');
    $move_request['rate']['label'] = $this->getFieldInfo('field_price_per_hour');
    $move_request['rate']['field'] = 'field_price_per_hour';
    $move_request['rate']['old'] = $move_request['rate']['value'];

    // ADDRESS FROM.
    $move_request['field_moving_from']['locality'] = $this->addressField('field_moving_from', 'locality');
    $move_request['field_moving_from']['administrative_area'] = $this->addressField('field_moving_from', 'administrative_area');
    $move_request['field_moving_from']['thoroughfare'] = $this->addressField('field_moving_from', 'thoroughfare');
    $move_request['field_moving_from']['postal_code'] = $this->addressField('field_moving_from', 'postal_code');
    $move_request['field_moving_from']['premise'] = $this->addressField('field_moving_from', 'premise');

    $move_request['adrfrom']['value'] = $this->addressField('field_moving_from', 'thoroughfare');
    $move_request['adrfrom']['label'] = $this->getFieldInfo('field_moving_from');
    $move_request['adrfrom']['field'] = 'field_moving_from';
    $move_request['adrfrom']['subfield'] = 'thoroughfare';
    $move_request['adrfrom']['old'] = $move_request['adrfrom']['value'];

    // CITY FROM.
    $move_request['city_from']['value'] = $this->addressField('field_moving_from', 'locality');
    $move_request['city_from']['label'] = 'Origin city';
    $move_request['city_from']['field'] = 'field_moving_from';
    $move_request['city_from']['subfield'] = 'locality';
    $move_request['city_from']['old'] = $move_request['city_from']['value'];

    // ZIP FROM.
    $move_request['zip_from']['value'] = $this->addressField('field_moving_from', 'postal_code');
    $move_request['zip_from']['label'] = 'Origin zip zode';
    $move_request['zip_from']['field'] = 'field_moving_from';
    $move_request['zip_from']['subfield'] = 'postal_code';
    $move_request['zip_from']['old'] = $move_request['zip_from']['value'];

    // STATE FROM.
    $move_request['state_from']['value'] = $this->addressField('field_moving_from', 'administrative_area');
    $move_request['state_from']['label'] = 'Origin state';
    $move_request['state_from']['field'] = 'field_moving_from';
    $move_request['state_from']['subfield'] = 'administrative_area';
    $move_request['state_from']['old'] = $move_request['state_from']['value'];

    // APT FROM.
    $move_request['apt_from']['value'] = $this->getFieldData('field_apt_from');
    $move_request['apt_from']['label'] = $this->getFieldInfo('field_apt_from');
    $move_request['apt_from']['field'] = 'field_apt_from';
    $move_request['apt_from']['old'] = $move_request['apt_from']['value'];

    // ENTARANCE FROM.
    $move_request['type_from']['value'] = $this->fieldList('field_type_of_entrance_from');
    $move_request['type_from']['label'] = $this->getFieldInfo('field_type_of_entrance_from');
    $move_request['type_from']['field'] = 'field_type_of_entrance_from';
    $move_request['type_from']['raw'] = $this->fieldList('field_type_of_entrance_from', TRUE);
    $move_request['type_from']['old'] = $move_request['type_from']['raw'];

    // ADDRESS FROM.
    $move_request['field_moving_to']['locality'] = $this->addressField('field_moving_to', 'locality');
    $move_request['field_moving_to']['administrative_area'] = $this->addressField('field_moving_to', 'administrative_area');
    $move_request['field_moving_to']['thoroughfare'] = $this->addressField('field_moving_to', 'thoroughfare');
    $move_request['field_moving_to']['postal_code'] = $this->addressField('field_moving_to', 'postal_code');
    $move_request['field_moving_to']['premise'] = $this->addressField('field_moving_to', 'premise');

    $move_request['adrto']['value'] = $this->addressField('field_moving_to', 'thoroughfare');
    $move_request['adrto']['label'] = 'Destination address';
    $move_request['adrto']['field'] = 'field_moving_to';
    $move_request['adrto']['subfield'] = 'thoroughfare';
    $move_request['adrto']['old'] = $move_request['adrto']['value'];

    // CITY TO.
    $move_request['city_to']['value'] = $this->addressField('field_moving_to', 'locality');
    $move_request['city_to']['label'] = 'Destination city';
    $move_request['city_to']['field'] = 'field_moving_to';
    $move_request['city_to']['subfield'] = 'locality';
    $move_request['city_to']['old'] = $move_request['city_to']['value'];

    // ZIP FROM.
    $move_request['zip_to']['value'] = $this->addressField('field_moving_to', 'postal_code');
    $move_request['zip_to']['label'] = 'Destination zip zode';
    $move_request['zip_to']['field'] = 'field_moving_to';
    $move_request['zip_to']['subfield'] = 'postal_code';
    $move_request['zip_to']['old'] = $move_request['zip_to']['value'];

    // STATE FROM.
    $move_request['state_to']['value'] = $this->addressField('field_moving_to', 'administrative_area');
    $move_request['state_to']['label'] = 'Destination state';
    $move_request['state_to']['field'] = 'field_moving_to';
    $move_request['state_to']['subfield'] = 'administrative_area';
    $move_request['state_to']['old'] = $move_request['state_to']['value'];

    // ENTARANCE TO.
    $move_request['type_to']['value'] = $this->fieldList('field_type_of_entrance_to_');
    $move_request['type_to']['label'] = $this->getFieldInfo('field_type_of_entrance_to_');
    $move_request['type_to']['field'] = 'field_type_of_entrance_to_';
    $move_request['type_to']['raw'] = $this->fieldList('field_type_of_entrance_to_', TRUE);
    $move_request['type_to']['old'] = $move_request['type_to']['raw'];

    // EXTRA DROP OFF AND EXTRA PICKUP.
    $move_request['field_extra_pickup']['locality'] = $this->addressField('field_extra_pickup', 'locality');
    $move_request['field_extra_pickup']['premise'] = $this->addressField('field_extra_pickup', 'premise');
    $move_request['field_extra_pickup']['administrative_area'] = $this->addressField('field_extra_pickup', 'administrative_area');
    $move_request['field_extra_pickup']['thoroughfare'] = $this->addressField('field_extra_pickup', 'thoroughfare');
    $move_request['field_extra_pickup']['postal_code'] = $this->addressField('field_extra_pickup', 'postal_code');
    $move_request['field_extra_pickup']['organisation_name'] = $this->addressField('field_extra_pickup', 'organisation_name');
    if (!empty($move_request['field_extra_pickup']['organisation_name'])) {
      $pickupEntrance = $move_request['field_extra_pickup']['organisation_name'];
      $move_request['field_extra_pickup']['entrance'] = Statistics::getFieldValue("field_type_of_entrance_to_")[$pickupEntrance]['name'];
    }
    else {
      $move_request['field_extra_pickup']['entrance'] = Statistics::getFieldValue("field_type_of_entrance_to_")[1]['name'];
    }

    $move_request['field_extra_dropoff']['locality'] = $this->addressField('field_extra_dropoff', 'locality');
    $move_request['field_extra_dropoff']['premise'] = $this->addressField('field_extra_dropoff', 'premise');
    $move_request['field_extra_dropoff']['administrative_area'] = $this->addressField('field_extra_dropoff', 'administrative_area');
    $move_request['field_extra_dropoff']['thoroughfare'] = $this->addressField('field_extra_dropoff', 'thoroughfare');
    $move_request['field_extra_dropoff']['postal_code'] = $this->addressField('field_extra_dropoff', 'postal_code');
    $move_request['field_extra_dropoff']['organisation_name'] = $this->addressField('field_extra_dropoff', 'organisation_name');
    if (!empty($move_request['field_extra_dropoff']['organisation_name'])) {
      $dropoffEntrance = $move_request['field_extra_dropoff']['organisation_name'];
      $move_request['field_extra_dropoff']['entrance'] = Statistics::getFieldValue("field_type_of_entrance_to_")[$dropoffEntrance]['name'];
    }
    else {
      $move_request['field_extra_dropoff']['entrance'] = Statistics::getFieldValue("field_type_of_entrance_to_")[1]['name'];
    }

    // FIELD IMAGES.
    $fields_images = array(
      'field_move_images',
      'field_contract_images',
    );

    foreach ($fields_images as $fields_image) {
      $move_request[$fields_image] = array();
      $files = $this->getFieldData($fields_image, 'value', FALSE);
      foreach ($files as $key => $file) {
        if ($file && isset($file['uri'])) {
          $move_request[$fields_image][$key]['fid'] = $file['fid'];
          $move_request[$fields_image][$key]['title'] = $file['title'];
          $move_request[$fields_image][$key]['url'] = file_create_url($file['uri']);
        }
      }
    }

    // Flat Rate Local Move.
    $move_request['field_flat_rate_local_move']['value'] = !empty($local_move_flatrate = $this->getFieldData('field_flat_rate_local_move')) ? $local_move_flatrate : FALSE;
    $move_request['field_flat_rate_local_move']['old'] = $move_request['field_flat_rate_local_move']['value'];

    // APT To.
    $move_request['apt_to']['value'] = $this->getFieldData('field_apt_to');
    $move_request['apt_to']['label'] = $this->getFieldInfo('field_apt_to');
    $move_request['apt_to']['field'] = 'field_apt_to';
    $move_request['apt_to']['old'] = $move_request['apt_to']['value'];

    $address_from = $move_request['city_from']['value'] . ', ' . $move_request['state_from']['value'] . ', ' . $move_request['zip_from']['value'];
    $address_to = $move_request['city_to']['value'] . ', ' . $move_request['state_to']['value'] . ', ' . $move_request['zip_to']['value'];

    $move_request['from'] = $address_from;
    $move_request['to'] = $address_to;

    // @TODO SEPARATE IN CALCULATOR WORK TIME AND TRAVEL TIME.
    // STORAGE PRICE.
    $storage_rate = $this->getFieldData('field_storage_price');

    $move_request['storage_rate']['value'] = ($storage_rate) ? $storage_rate : 100;
    $move_request['storage_rate']['label'] = $this->getFieldInfo('field_storage_price');
    $move_request['storage_rate']['field'] = 'field_storage_price';
    $move_request['storage_rate']['old'] = $move_request['storage_rate']['value'];

    // RESERVATION RATE.
    $reservation_rate = $this->getFieldData('field_reservation_price');
    $move_request['reservation_rate']['value'] = ($reservation_rate) ? $reservation_rate : 0;
    $move_request['reservation_rate']['label'] = $this->getFieldInfo('field_reservation_price');
    $move_request['reservation_rate']['field'] = 'field_reservation_price';
    $move_request['reservation_rate']['old'] = $move_request['reservation_rate']['value'];

    // RESERVATION received.
    $move_request['field_reservation_received']['value'] = $this->getFieldData('field_reservation_received');
    $move_request['field_reservation_received']['label'] = $this->getFieldInfo('field_reservation_received');
    $move_request['field_reservation_received']['field'] = 'field_reservation_received';
    $move_request['field_reservation_received']['old'] = $move_request['field_reservation_received']['value'];
    $move_request['field_reservation_received']['text'] = !empty($move_request['field_reservation_received']['value']) ? 'Yes' : 'No';

    // Weight Type.
    $move_request['field_cubic_feet']['value'] = $this->getFieldData('field_cubic_feet');
    $move_request['field_cubic_feet']['label'] = $this->getFieldInfo('field_cubic_feet');
    $move_request['field_cubic_feet']['field'] = 'field_cubic_feet';
    $move_request['field_cubic_feet']['old'] = $move_request['field_cubic_feet']['value'];

    // Long Distance Rate.
    $move_request['field_long_distance_rate']['value'] = $this->getFieldData('field_long_distance_rate');
    $move_request['field_long_distance_rate']['label'] = $this->getFieldInfo('field_long_distance_rate');
    $move_request['field_long_distance_rate']['field'] = 'field_long_distance_rate';
    $move_request['field_long_distance_rate']['old'] = $move_request['field_long_distance_rate']['value'];

    // ROOMS.
    $move_request['rooms']['value'] = $this->getFieldData('field_extra_furnished_rooms', 'value', FALSE);
    $move_request['rooms']['label'] = $this->getFieldInfo('field_extra_furnished_rooms');
    $move_request['rooms']['field'] = 'field_extra_furnished_rooms';
    $move_request['rooms']['raw'] = $this->fieldList('field_extra_furnished_rooms', TRUE, FALSE);
    $move_request['rooms']['old'] = $move_request['rooms']['raw'];

    // Commercial extra rooms.
    $move_request['commercial_extra_rooms']['value'] = $this->getFieldData('field_commercial_extra_rooms', 'value', FALSE);
    $move_request['commercial_extra_rooms']['label'] = $this->getFieldInfo('field_commercial_extra_rooms');
    $move_request['commercial_extra_rooms']['field'] = 'field_commercial_extra_rooms';
    $move_request['commercial_extra_rooms']['raw'] = $this->fieldList('field_commercial_extra_rooms', TRUE, FALSE);
    $move_request['commercial_extra_rooms']['old'] = $move_request['commercial_extra_rooms']['raw'];

    $move_request['name'] = $this->getName();
    $move_request['first_name'] = $this->getFieldData('field_first_name');
    $move_request['last_name'] = $this->getFieldData('field_last_name');
    $author = $this->getFieldData('author');

    if (isset($author->uid)) {
      $move_request['uid'] = $client_inst->getUserFieldsData($author->uid, FALSE);
    }

    $additional_user = $this->getFieldData('field_additional_user');
    if (!empty($additional_user)) {
      $additional_user_wrap = $client_inst->getUserFieldsData($additional_user->uid, FALSE);
      $move_request['field_additional_user'] = array(
        'uid' => $additional_user_wrap['uid'],
        'first_name' => $additional_user_wrap['field_user_first_name'],
        'last_name' => $additional_user_wrap['field_user_last_name'],
        'mail' => $additional_user_wrap['mail'],
        'phone' => $additional_user_wrap['field_primary_phone'],
      );
    }

    $move_request['send_additional_email'] = $this->getFieldData('field_send_additional_email');

    $move_request['email'] = $this->getFieldData('field_e_mail');
    $move_request['phone'] = $this->getFieldData('field_phone');
    $move_request['field_additional_phone'] = $this->getFieldData('field_additional_phone');
    $move_request['field_parser_provider_name'] = $this->getFieldData('field_parser_provider_name');
    $move_request['field_parser_provider_id'] = $this->getFieldData('field_parser_provider_id');

    // Trucks.
    $move_request['trucks']['value'] = $this->getFieldData('field_list_truck');
    $move_request['trucks']['label'] = $this->getFieldInfo('field_list_truck');
    $move_request['trucks']['raw'] = $this->getFieldData('field_list_truck', 'raw', FALSE);
    $move_request['trucks']['old'] = $move_request['trucks']['raw'];
    $move_request['trucks']['field'] = 'field_list_truck';
    $move_request['suggested_trucks'] = $this->getFieldData('field_suggested_truck');

    // Delivery Trucks.
    $move_request['delivery_trucks']['value'] = $this->getFieldData('field_flat_rate_delivery_trucks');
    $move_request['delivery_trucks']['label'] = $this->getFieldInfo('field_flat_rate_delivery_trucks');
    $move_request['delivery_trucks']['raw'] = $this->getFieldData('field_flat_rate_delivery_trucks', 'raw', FALSE);
    $move_request['delivery_trucks']['old'] = $move_request['delivery_trucks']['raw'];
    $move_request['delivery_trucks']['field'] = 'field_flat_rate_delivery_trucks';

    $request_data = MoveRequest::getRequestData($nid);
    $move_request['request_data']['value'] = isset($request_data['value']) ? $request_data['value'] : '';

    $request_all_data = MoveRequest::getRequestAllData($nid);
    $move_request['request_all_data'] = isset($request_all_data) ? $request_all_data : [];

    $move_request['delivery_time'] = $this->getFieldData('field_delivery_time');
    $move_request['pref_time'] = '';

    // START TIME INIT.
    $move_request['start_time1']['value'] = '';
    $move_request['start_time1']['label'] = $this->getFieldInfo('field_actual_start_time');
    $move_request['start_time1']['field'] = 'field_actual_start_time';

    $move_request['start_time2']['value'] = '';
    $move_request['start_time2']['label'] = $this->getFieldInfo('field_start_time_max');
    $move_request['start_time2']['field'] = 'field_start_time_max';

    if (!$move_request['delivery_time']) {
      // Prefered Time.
      $s_time = $this->getFieldData('field_start_time');
      if ($s_time == 1) {
        $move_request['start_time1']['value'] = '8:00 AM';
        $move_request['start_time2']['value'] = '8:00 PM';
        $move_request['pref_time'] = 'Any Time';
      }
      if ($s_time == 2) {
        $move_request['start_time1']['value'] = '9:00 AM';
        $move_request['start_time2']['value'] = '12:00 PM';
        $move_request['pref_time'] = '9AM-12PM';
      }
      if ($s_time == 3) {
        $move_request['start_time1']['value'] = '12:00 PM';
        $move_request['start_time2']['value'] = '3:00 PM';
        $move_request['pref_time'] = '12:00 PM - 3:00 PM';
      }
      if ($s_time == 4) {
        $move_request['start_time1']['value'] = '3:00 PM';
        $move_request['start_time2']['value'] = '6:00 PM';
        $move_request['pref_time'] = '3:00 PM - 6:00 PM';
      }

      if (isset($move_request['start_time2'])) {
        $move_request['delivery_time'] = $move_request['start_time1']['label'] . ' - ' . $move_request['start_time2']['label'];
      }
      else {
        $move_request['delivery_time'] = $move_request['start_time1']['label'];
      }
    }

    // IF start Time min and max is empty - take from prefered.
    $s_time = 1;
    // Preferred Time.
    if ($this->getFieldData('field_start_time')) {
      $s_time = $this->getFieldData('field_start_time');
    }
    if ($s_time == 1) {
      $move_request['start_time1']['value'] = '8:00 AM';
      $move_request['start_time2']['value'] = '8:00 PM';
      $move_request['pref_time'] = 'Any Time';
    }
    if ($s_time == 2) {
      $move_request['start_time1']['value'] = '8:00 AM';
      $move_request['start_time2']['value'] = '10:00 AM';
      $move_request['pref_time'] = '8:00 AM - 10:00 AM';
    }
    if ($s_time == 3) {
      $move_request['start_time1']['value'] = '11:00 AM';
      $move_request['start_time2']['value'] = '2:00 PM';
      $move_request['pref_time'] = '11:00 AM - 2:00 PM';
    }
    if ($s_time == 4) {
      $move_request['start_time1']['value'] = '1:00 PM';
      $move_request['start_time2']['value'] = '4:00 PM';
      $move_request['pref_time'] = '1:00 PM - 4:00 PM';
    }
    if ($s_time == 5) {
      $move_request['start_time1']['value'] = '3:00 PM';
      $move_request['start_time2']['value'] = '7:00 PM';
      $move_request['pref_time'] = '3:00 PM - 7:00 PM';
    }

    $move_request['start_time']['value'] = $this->fieldList('field_start_time');
    $move_request['start_time']['field'] = 'field_start_time';
    $move_request['start_time']['raw'] = $this->fieldList('field_start_time', TRUE);

    if ($this->getFieldData('field_actual_start_time')) {
      $move_request['start_time1']['value'] = $this->getFieldData('field_actual_start_time');
      $move_request['start_time1']['label'] = $this->getFieldInfo('field_actual_start_time');
      $move_request['start_time1']['raw'] = $this->convertTime($this->getFieldInfo('field_actual_start_time'));
      $move_request['start_time1']['old'] = $move_request['start_time1']['value'];
      $move_request['start_time1']['field'] = 'field_actual_start_time';
    }

    if ($this->getFieldData('field_start_time_max')) {
      $move_request['start_time2']['value'] = $this->getFieldData('field_start_time_max');
      $move_request['start_time2']['label'] = $this->getFieldInfo('field_start_time_max');
      $move_request['start_time2']['raw'] = $this->convertTime($this->getFieldInfo('field_start_time_max'));
      $move_request['start_time2']['old'] = $move_request['start_time2']['value'];
      $move_request['start_time2']['field'] = 'field_start_time_max';
    }

    // FIELD DISTANCE.
    $move_request['distance']['value'] = $this->getFieldData('field_distance');
    $move_request['distance']['label'] = $this->getFieldInfo('field_distance');
    $move_request['distance']['field'] = 'field_distance';
    $move_request['distance']['old'] = $move_request['distance']['value'];

    // Duration.
    $move_request['duration']['value'] = $this->getFieldData('field_duration');
    $move_request['duration']['label'] = $this->getFieldInfo('field_duration');
    $move_request['duration']['field'] = 'field_duration';
    $move_request['duration']['old'] = $move_request['duration']['value'];

    // FLAT RATE PRICE.
    // TODO refactor.
    $move_request['flat_rate_quote']['value'] = $this->getFieldData('field_estimated_prise');
    $move_request['flat_rate_quote']['label'] = $this->getFieldInfo('field_estimated_prise');
    $move_request['flat_rate_quote']['field'] = 'field_estimated_prise';
    $move_request['flat_rate_quote']['old'] = $move_request['flat_rate_quote']['value'];

    // Use calculator or Not.
    $move_request['field_usecalculator']['value'] = $this->getFieldData('field_usecalculator');
    $move_request['field_usecalculator']['label'] = $this->getFieldInfo('field_usecalculator');
    $move_request['field_usecalculator']['field'] = 'field_usecalculator';
    $move_request['field_usecalculator']['old'] = $move_request['field_usecalculator']['value'];

    // Use weight type.
    $move_request['field_useweighttype']['value'] = $this->getFieldData('field_useweighttype');
    $move_request['field_useweighttype']['label'] = $this->getFieldInfo('field_useweighttype');
    $move_request['field_useweighttype']['field'] = 'field_useweighttype';
    $move_request['field_useweighttype']['old'] = $move_request['field_useweighttype']['value'];

    // Custom Weight.
    $move_request['custom_weight']['value'] = $this->getFieldData('field_customweight');
    $move_request['custom_weight']['label'] = $this->getFieldInfo('field_customweight');
    $move_request['custom_weight']['field'] = 'field_customweight';
    $move_request['custom_weight']['old'] = $move_request['custom_weight']['value'];

    $move_request['field_double_travel_time']['value'] = $this->getReadableTime($this->getFieldData('field_double_travel_time'), TRUE);
    $move_request['field_double_travel_time']['label'] = $this->getFieldInfo('field_double_travel_time');
    $move_request['field_double_travel_time']['field'] = 'field_double_travel_time';
    $move_request['field_double_travel_time']['old'] = $this->getReadableTime($this->getFieldData('field_double_travel_time'), TRUE);
    $raw_field_double_travel_time = $this->getFieldData('field_double_travel_time', 'raw', FALSE);
    $move_request['field_double_travel_time']['raw'] = $raw_field_double_travel_time ? floatval($raw_field_double_travel_time) : NULL;

    // Foreman's.
    $move_request['field_foreman']['value'] = $this->getFieldData('field_foreman', 'raw');
    $move_request['field_foreman']['label'] = $this->getFieldInfo('field_foreman');
    $move_request['field_foreman']['field'] = 'field_foreman';
    $move_request['field_foreman']['old'] = $move_request['field_foreman']['value'];

    // Helpers List.
    $move_request['field_helper']['value'] = $this->getFieldData('field_helper', 'raw', FALSE);
    $move_request['field_helper']['label'] = $this->getFieldInfo('field_helper');
    $move_request['field_helper']['field'] = 'field_helper';
    $move_request['field_helper']['old'] = $move_request['field_helper']['value'];

    // Confirmed Date.
    $move_request['field_date_confirmed']['value'] = $this->getFieldData('field_date_confirmed');
    $move_request['field_date_confirmed']['label'] = $this->getFieldInfo('field_date_confirmed');
    $move_request['field_date_confirmed']['field'] = 'field_date_confirmed';
    $move_request['field_date_confirmed']['raw'] = Extra::date('m/d/Y h:i A', $move_request['field_date_confirmed']['value'], Extra::getTimezone());
    $move_request['field_date_confirmed']['old'] = $move_request['field_date_confirmed']['value'];

    // Field Flags.
    $move_request['field_flags']['value'] = $this->flags($this->getFieldData('field_flags', 'value', FALSE));
    $move_request['field_flags']['label'] = $this->getFieldInfo('field_flags');
    $move_request['field_flags']['field'] = 'field_flags';
    $move_request['field_flags']['old'] = $move_request['field_flags']['value'];

    // Field Company Flags.
    $move_request['field_company_flags']['value'] = $this->getCompanyFlags($this->getFieldData('field_company_flags', 'value', FALSE));
    $move_request['field_company_flags']['label'] = $this->getFieldInfo('field_company_flags');
    $move_request['field_company_flags']['field'] = 'field_company_flags';

    // Field Foreman Assign Time.
    $move_request['field_foreman_assign_time']['value'] = $this->getFieldData('field_foreman_assign_time', 'raw', FALSE);
    $move_request['field_foreman_assign_time']['label'] = $this->getFieldInfo('field_foreman_assign_time');
    $move_request['field_foreman_assign_time']['field'] = 'field_foreman_assign_time';
    $move_request['field_foreman_assign_time']['old'] = $move_request['field_foreman_assign_time']['value'];

    // Additional comments.
    $move_request['additional_comments']['value'] = $this->getFieldData('field_additional_comments', 'raw');
    $move_request['additional_comments']['label'] = $this->getFieldInfo('field_additional_comments');
    $move_request['additional_comments']['field'] = 'field_additional_comments';
    $move_request['additional_comments']['old'] = $move_request['additional_comments']['value'];

    // Quote Explanation.
    $move_request['field_quote_explanationn'] = $this->getFieldData('field_quote_explanationn');

    // Field Home Estimate Date 07/31/2015.
    $move_request['home_estimate_date']['value'] = $this->dateConvert($this->getFieldData('field_home_estimate_date'), "m/d/Y");
    $move_request['home_estimate_date']['label'] = $this->getFieldInfo('field_home_estimate_date');
    $move_request['home_estimate_date']['raw'] = strtotime($this->dateConvert($this->getFieldData('field_home_estimate_date')));
    $move_request['home_estimate_date']['field'] = 'field_home_estimate_date';
    $move_request['home_estimate_date']['old'] = $this->dateConvert($this->getFieldData('field_home_estimate_date'), "m/d/Y");

    // Home Estimate Actual Start Time.
    $move_request['home_estimate_actual_start'] = array();
    $move_request['home_estimate_actual_start']['value'] = $this->getFieldData('field_home_estimate_actual_start');
    $move_request['home_estimate_actual_start']['label'] = $this->getFieldInfo('field_home_estimate_actual_start');
    $move_request['home_estimate_actual_start']['field'] = 'field_home_estimate_actual_start';
    $move_request['home_estimate_actual_start']['old'] = $move_request['home_estimate_actual_start']['value'];
    $move_request['home_estimate_actual_start']['raw'] = floatval($this->getFieldData('field_home_estimate_actual_start'));

    // Home Estimate Start Time.
    $move_request['home_estimate_start_time'] = array();
    $move_request['home_estimate_start_time']['value'] = $this->getFieldData('field_home_estimate_start_time');
    $move_request['home_estimate_start_time']['label'] = $this->getFieldInfo('field_home_estimate_start_time');
    $move_request['home_estimate_start_time']['field'] = 'field_home_estimate_start_time';
    $move_request['home_estimate_start_time']['old'] = $move_request['home_estimate_start_time']['value'];
    $move_request['home_estimate_start_time']['raw'] = floatval($this->getFieldData('field_home_estimate_start_time'));

    // Home Estimate Time Duration.
    $move_request['home_estimate_time_duration'] = array();
    $move_request['home_estimate_time_duration']['value'] = $this->getReadableTime($this->getFieldData('field_home_estimate_time_duratio'), TRUE);
    $move_request['home_estimate_time_duration']['label'] = $this->getFieldInfo('field_home_estimate_time_duratio');
    $move_request['home_estimate_time_duration']['field'] = 'field_home_estimate_time_duratio';
    $move_request['home_estimate_time_duration']['old'] = $move_request['home_estimate_time_duration']['value'];
    $move_request['home_estimate_time_duration']['raw'] = floatval($this->getFieldData('field_home_estimate_time_duratio'));

    // Home Estimate user who created.
    $move_request['home_estimate_assigned']['value'] = $this->getFieldData('field_home_estimate_assignedn', 'raw');
    $move_request['home_estimate_assigned']['label'] = $this->getFieldInfo('field_home_estimate_assignedn');
    $move_request['home_estimate_assigned']['field'] = 'field_home_estimate_assignedn';
    $move_request['home_estimate_assigned']['old'] = $move_request['home_estimate_assigned']['value'];
    if (!empty($move_request['home_estimate_assigned']['value'])) {
      $user = $client_inst->getUserFieldsData($move_request['home_estimate_assigned']['value'], FALSE);
      $move_request['home_estimate_assigned']['value'] = $user['field_user_first_name'] . " " . $user['field_user_last_name'];
    }

    // Home Estimate Status.
    $move_request['home_estimate_status'] = array();
    $move_request['home_estimate_status']['value'] = $this->fieldList('field_home_estimate_status', TRUE);
    $move_request['home_estimate_status']['label'] = $this->getFieldInfo('field_home_estimate_status');
    $move_request['home_estimate_status']['field'] = 'field_home_estimate_status';
    $move_request['home_estimate_status']['raw'] = $this->fieldList('field_home_estimate_status');
    $move_request['home_estimate_status']['old'] = $move_request['home_estimate_status']['value'];

    // Home Estimator.
    $move_request['home_estimator'] = array();
    $move_request['home_estimator']['value'] = $this->getFieldData('field_home_estimator');
    $move_request['home_estimator']['label'] = $this->getFieldInfo('field_home_estimator');
    $move_request['home_estimator']['field'] = 'field_home_estimator';
    $move_request['home_estimator']['old'] = $move_request['home_estimator']['value'];
    $move_request['home_estimator']['first_name'] = '';
    $move_request['home_estimator']['last_name'] = '';
    if (!empty($move_request['home_estimator']['value'])) {
      $user = $client_inst->getUserFieldsData($move_request['home_estimator']['value'], FALSE);
      $move_request['home_estimator']['first_name'] = $user['field_user_first_name'];
      $move_request['home_estimator']['last_name'] = $user['field_user_last_name'];
    }

    // Home Estimate Date Created.
    $move_request['home_estimate_date_created']['value'] = $this->dateConvert($this->getFieldData('field_home_estimate_date_created'), "m/d/Y");
    $move_request['home_estimate_date_created']['label'] = $this->getFieldInfo('field_home_estimate_date_created');
    $move_request['home_estimate_date_created']['raw'] = strtotime($this->dateConvert($this->getFieldData('field_home_estimate_date_created')));
    $move_request['home_estimate_date_created']['field'] = 'field_home_estimate_date_created';
    $move_request['home_estimate_date_created']['old'] = $move_request['home_estimate_date_created']['value'];

    // Grand total.
    $move_request['field_grand_total_min'] = $this->getFieldData('field_grand_total_min') ?? 0;
    $move_request['field_grand_total_max'] = $this->getFieldData('field_grand_total_max') ?? 0;
    $move_request['field_grand_total_middle'] = $this->getFieldData('field_grand_total_middle') ?? 0;
    $move_request['field_grand_total_closing'] = $this->getFieldData('field_grand_total_closing') ?? 0;

    // Grand total.
    $move_request['field_quote_min'] = $this->getFieldData('field_quote_min') ?? 0;
    $move_request['field_quote_max'] = $this->getFieldData('field_quote_max') ?? 0;
    $move_request['field_quote_middle'] = $this->getFieldData('field_quote_middle') ?? 0;
    $move_request['field_quote_closing'] = $this->getFieldData('field_quote_closing') ?? 0;

    $move_request['field_estimate_signatures']['value'] = $this->getFieldData('field_estimate_signatures');
    $move_request['field_estimate_signatures']['label'] = $this->getFieldInfo('field_estimate_signatures');
    $move_request['field_estimate_signatures']['raw'] = json_decode($this->getFieldData('field_estimate_signatures'));
    $move_request['field_estimate_signatures']['field'] = 'field_estimate_signatures';
    $move_request['field_estimate_signatures']['old'] = $move_request['field_estimate_signatures']['value'];

    // Allow To Pending Info field.
    $move_request['field_allow_to_pending_info'] = $this->getFieldData('field_allow_to_pending_info');

    // IF MOVING AND STORAGE THE CREATE REQ LINK.
    $move_request['storage_id'] = 0;
    if ($this->getFieldData('field_move_service_type') == 2 || $this->getFieldData('field_move_service_type') == 6) {
      if ($this->getFieldData('field_to_storage_move', 'raw')) {
        $move_request['storage_id'] = $this->getFieldData('field_to_storage_move', 'raw');
        $move_request['toStorage'] = TRUE;
      }
      else {
        if ($this->getFieldData('field_from_storage_move', 'raw')) {
          $move_request['storage_id'] = $this->getFieldData('field_from_storage_move', 'raw');
          $move_request['toStorage'] = FALSE;
        }
      }
    }

    if ($request_settings = $this->getFieldData('field_request_settings')) {
      $move_request['field_request_settings'] = unserialize($request_settings);
    }

    $move_request['from_storage'] = 0;
    if ($this->getFieldData('field_from_storage_move', 'raw')) {
      // From storage.
      $move_request['from_storage'] = 1;
    }
    if ($this->getFieldData('field_to_storage_move', 'raw')) {
      // To storage.
      $move_request['from_storage'] = 2;
    }

    $move_request['request_link'] = "<i class=\"fa fa-external-link\"></i>http://{$_SERVER["HTTP_HOST"]}{$base_path}request/{$nid}";

    // HELPER VALUES FOR MOVE REQUEST ARRAY.
    $move_request['day'] = $this->dateConvert(strtotime($move_request['date']['value']), "j");
    $move_request['month'] = $this->dateConvert(strtotime($move_request['date']['value']), "m");
    $move_request['year'] = $this->dateConvert(strtotime($move_request['date']['value']), "Y");
    $move_request['created'] = $this->getFieldData('created');
    $move_request['changed'] = $this->getFieldData('changed');
    $move_request['manager'] = MoveRequest::getRequestManagerInfo((int) $nid);
    $move_request['user_created'] = $this->getFieldData('field_user_created', 'raw');

    // Labor time.
    $move_request['labor_time'] = array();
    $move_request['labor_time']['value'] = $this->getReadableTime($this->getFieldData('field_labor_time'), TRUE);
    $move_request['labor_time']['label'] = $this->getFieldInfo('field_labor_time');
    $move_request['labor_time']['field'] = 'field_labor_time';
    $move_request['labor_time']['old'] = $move_request['labor_time']['value'];
    $move_request['labor_time']['raw'] = floatval($this->getFieldData('field_labor_time'));

    // Commercial company name.
    $move_request['field_commercial_company_name']['value'] = !empty($this->getFieldData('field_commercial_company_name')) ? $this->getFieldData('field_commercial_company_name') : '';
    $move_request['field_commercial_company_name']['label'] = $this->getFieldInfo('field_commercial_company_name');
    $move_request['field_commercial_company_name']['field'] = 'field_commercial_company_name';
    $move_request['field_commercial_company_name']['old'] = $move_request['field_commercial_company_name']['value'];

    // Schedule Delivery date.
    $move_request['schedule_delivery_date']['value'] = $this->dateConvert($this->getFieldData('field_schedule_delivery_date'), "m/d/Y");
    $move_request['schedule_delivery_date']['label'] = $this->getFieldInfo('field_schedule_delivery_date');
    $move_request['schedule_delivery_date']['raw'] = strtotime($this->dateConvert($this->getFieldData('field_schedule_delivery_date')));
    $move_request['schedule_delivery_date']['field'] = 'field_schedule_delivery_date';

    $move_request['ld_sit_delivery_dates'] = $this->getFieldData('field_ld_sit_delivery_dates');
    $move_request['ld_status'] = $this->getFieldData('field_ld_status', 'raw');
    $move_request['ld_job'] = LongDistanceJob::getJobById($nid, TRUE);
    $move_request['field_total_score_updated'] = !empty($score_update_date = $this->getFieldData('field_total_score_updated')) ? Extra::date('m/d/y g:i A', $score_update_date, $this->timeZone) : '';
    $move_request['field_custom_commercial_item'] = !empty($commercial_item = $this->getFieldData('field_custom_commercial_item')) ? unserialize($commercial_item) : '';

    // Speedy soft unique job id.
    $move_request['field_speedy_id']['value'] = !empty($this->getFieldData('field_speedy_id', TRUE)) ? $this->getFieldData('field_speedy_id', TRUE) : '';
    $move_request['field_speedy_id']['label'] = $this->getFieldInfo('field_speedy_id');
    $move_request['field_speedy_id']['field'] = 'field_speedy_id';
    $move_request['field_speedy_id']['raw'] = $this->getFieldData('field_speedy_id');
    $move_request['field_speedy_id']['old'] = $this->getFieldData('field_speedy_id', TRUE);

    // INIT DELIVERY DATES.
    $move_request = $this->deliveryDate($move_request);
    // Init delivery date second.
    $move_request = $this->deliveryDateSecond($move_request);

    $move_request['nid'] = $nid;

    // Add payroll for edit request directive.
    $move_request['payroll'] = [];
    $move_request['extraServices'] = [];
    $move_request['receipts'] = [];
    $move_request['receipts_total'] = 0;
    $move_request['contract_info'] = [];
    $move_request['field_total_score'] = 0;
    $move_request['inventory'] = [];

    if (!$this->init) {
      $move_request['extraServices'] = ExtraServices::getExtraServices((int) $nid);
      $move_request['receipts'] = MoveRequest::getReceipt((int) $nid);
      $move_request['receipts_total'] = !empty($move_request['receipts']) ? Receipt::calculateReceiptsTotal($move_request['receipts']) : 0.0;
      $move_request['contract_info'] = Payroll::getContractInfo((int) $nid);
      // Somewhere drupal field override.
      $move_request['field_total_score'] = MoveRequestScore::getRequestTotalScore($nid);
      $move_request['inventory'] = (new Inventory($nid))->retrieve();
    }

    // TODO Quick fix. Need only for display manager data on account.
    $manager_uid = MoveRequest::getRequestManager($nid);
    if ($manager_uid) {
      // TODO if manager is deleted then returned ERROR!!
      $move_request['current_manager'] = $client_inst->getUserFieldsData($manager_uid, FALSE);
      if (isset($move_request['current_manager']['settings']['password'])) {
        unset($move_request['current_manager']['settings']['password']);
      }
    }

    /* Ld flag status(field_ld_flag_status) do not miss with ld status(field_ld_status). Ld status(field_ld_status) it`s status for trip. */
    $move_request['ld_flag_status']['value'] = $this->getFieldData('field_ld_flag_status', 'value');
    $move_request['ld_flag_status']['label'] = $this->getFieldData('field_ld_flag_status', 'label');
    $move_request['ld_flag_status']['field'] = 'field_ld_flag_status';

    return $move_request;
  }

  /**
   * Get data from field.
   *
   * @param string $field_name
   *   Name of field.
   * @param string $type
   *   Type data from field.
   * @param bool $reset
   *   Reset result if count element more than one.
   *
   * @return mixed
   *   Value of field.
   */
  public function getFieldData($field_name, $type = 'value', $reset = TRUE) {
    $result = '';

    switch ($type) {
      case 'value':
        $result = $this->node->{$field_name}->value();
        break;

      case 'raw':
        $result = $this->node->{$field_name}->raw();
        break;

      case 'label':
        $result = $this->node->{$field_name}->label();
        break;
    }

    if ($reset && is_array($result) && count($result) == 1) {
      $result = reset($result);
    }

    return $result;
  }

  /**
   * Helper function to get date in specified format.
   *
   * @param string $date
   *   String of data.
   * @param string $format
   *   Data format.
   *
   * @return string
   */
  public function dateConvert($date, $format = "d-n-Y") {
    $result = 0;
    if ($this->isValidTimeStamp($date)) {
      $result = date($format, $date);
    }
    else {
      $result = '';
    }

    return $result;
  }

  /**
   * Check that string is valid unix time stamp.
   *
   * @param string $str_time_stamp
   *   A unix time stamp.
   *
   * @return bool
   *   TRUE if valid, FALSE otherwise.
   */
  private function isValidTimeStamp($str_time_stamp) {
    return ((string) (int) $str_time_stamp === $str_time_stamp)
    && ($str_time_stamp <= PHP_INT_MAX)
    && ($str_time_stamp >= ~PHP_INT_MAX);
  }

  /**
   * Git info about field.
   *
   * @param string $field_name
   *   Name of field.
   *
   * @return mixed
   *   Field label.
   */
  public function getFieldInfo($field_name) {
    $entity_type = 'node';
    $bundle_name = 'move_request';
    $instance = field_info_instance($entity_type, $field_name, $bundle_name);
    return $instance['label'];
  }

  private static function getFieldLabel() {

  }

  protected function fieldList($field_name, $option = FALSE, $reset = TRUE) {
    $value = $this->getFieldData($field_name, 'label');

    if ($option) {
      $value = $this->getFieldData($field_name, 'value', $reset);
    }

    return $value;
  }

  /**
   * Old function. Remove me!!!
   *
   * @todo Remove this function or convert her to normal view.
   *
   * @param int $number
   * @param bool|FALSE $euformat
   * @return string
   */
  protected function getReadableTime($number, $euformat = FALSE) {
    return static::getReadableTimeHelper($number, $euformat);
  }

  /**
   * Wrapper for getReadableTime method.
   *
   * @param mixed $number
   *   Number to convert.
   * @param bool $euformat
   *   Format.
   *
   * @return string
   *   Formatted time.
   */
  public static function getReadableTimeHelper($number, $euformat = FALSE) {
    if (is_array($number)) {
      $number = reset($number);
    }
    $zero = '0';
    $zero2 = '0';
    $minutes = 0;
    $hour = floor($number);
    $fraction = $number - $hour;

    // 0, 15min - 0.25 , 30min - 0.5 , 45 min - 0.75.
    if ($fraction > 0.15 && $fraction < 0.36) {
      $minutes = 15;
    }
    if ($fraction >= 0.36 && $fraction < 0.64) {
      $minutes = 30;
    }
    if ($fraction >= 0.64 && $fraction < 0.87) {
      $minutes = 45;
    }
    if ($fraction >= 0.87) {
      $minutes = 0;
      $hour++;
    }

    if (!$euformat) {
      if ($minutes == 0 && $hour != 1) {
        return $hour . ' hrs';
      }
      elseif ($hour == 1 && $minutes == 0) {
        return $hour . ' hr';
      }
      elseif ($hour == 1 && $minutes != 0) {
        return $hour . ' hr ' . $minutes . ' min';
      }
      elseif ($hour == 0) {
        return $minutes . ' min';
      }
      else {
        return $hour . ' hrs ' . $minutes . ' min';
      }
    }
    else {
      if ($hour > 9) {
        $zero = '';
      }
      if ($minutes > 9) {
        $zero2 = '';
      }
      if ($minutes == 0) {
        return $zero . $hour . ':00';
      }
      elseif ($hour == 0) {
        return '00:' . $minutes;
      }
      else {
        return $zero . $hour . ':' . $zero2 . $minutes;
      }
    }
  }

  protected function addressField($field_name, $field) {
    try {
      $field_value = $this->node->{$field_name};
      $field_info = field_info_field($field_name);

      if (isset($field_info['cardinality']) && $field_info['cardinality'] == -1) {
        if (!is_array($field_value) || !array_key_exists(0, $field_value)) {
          // @TODO Now returned is one first element from array.
          $this->node->{$field_name}[0]->{$field}->value();
        }
      }
      else {
        return $this->node->{$field_name}->{$field}->value();
      }
    }
    catch (\Throwable $exc) {
      return NULL;
    }
  }

  /**
   * Get user name.
   *
   * @return string
   *   Name of user.
   */
  public function getName() {
    return $this->getFieldData('field_first_name') . ' ' . $this->getFieldData('field_last_name');
  }

  /**
   * Custom method to convert time.
   *
   * @param $time
   * @return int $hours
   */
  private function convertTime($time = '') {
    $hours = 0;

    if ($time) {
      $timestamp = strtotime($time);
      $hours = intval(date('G', $timestamp));
      $minutes = intval(date('i', $timestamp));
      $hours = $hours * 2;
      if ($minutes == 30) {
        $hours++;
      }
    }

    return $hours;
  }

  private function flags($flags) {
    $new_flags = array();

    foreach ($flags as $flag) {
      if ($flag) {
        $new_flags[$flag->id] = $flag->value;
      }
    }

    return $new_flags;
  }

  private function getCompanyFlags(array $flags) {
    $new_flags = array();
    $allowed_values = list_allowed_values(field_info_field('field_company_flags'));
    foreach ($flags as $flag) {
      $new_flags[$flag] = $allowed_values[$flag];
    }

    return $new_flags;
  }

  public function deliveryDate($result) {
    $result['ddate']['value'] = $this->dateConvert($this->getFieldData('field_delivery_date'), "m/d/Y");
    $result['ddate']['label'] = $this->getFieldInfo('field_delivery_date');
    $result['ddate']['raw'] = strtotime($this->dateConvert($this->getFieldData('field_delivery_date')));
    $result['ddate']['field'] = 'field_delivery_date';

    $result['dstart_time1']['value'] = $this->getFieldData('field_delivery_start_time1');
    $result['dstart_time1']['label'] = $this->getFieldInfo('field_delivery_start_time1');
    $result['dstart_time1']['raw'] = $this->getFieldInfo('field_delivery_start_time1');
    $result['dstart_time1']['field'] = 'field_delivery_start_time1';

    $result['dstart_time2']['value'] = $this->getFieldData('field_delvery_start_time2');
    $result['dstart_time2']['label'] = $this->getFieldInfo('field_delvery_start_time2');
    $result['dstart_time2']['raw'] = $this->getFieldInfo('field_delvery_start_time2');
    $result['dstart_time2']['field'] = 'field_delvery_start_time2';

    $result['delivery_date_to']['value'] = $this->dateConvert($this->getFieldData('field_delivery_date_to'));
    $result['delivery_date_to']['label'] = $this->getFieldInfo('field_delivery_date_to');
    $result['delivery_date_to']['raw'] = strtotime($this->dateConvert($this->getFieldData('field_delivery_date_to')));
    $result['delivery_date_to']['field'] = 'field_delivery_date_to';

    $result['delivery_start_time']['value'] = $this->getFieldData('field_delivery_start');
    $result['delivery_start_time']['label'] = $this->getFieldInfo('field_delivery_start');
    $result['delivery_start_time']['raw'] = strtotime($this->dateConvert($this->getFieldData('field_delivery_start')));
    $result['delivery_start_time']['field'] = 'field_delivery_start';


    $result['delivery_date_from']['value'] = $this->dateConvert($this->getFieldData('field_delivery_date_from'));
    $result['delivery_date_from']['label'] = $this->getFieldInfo('field_delivery_date_from');
    $result['delivery_date_from']['raw'] = strtotime($this->dateConvert($this->getFieldData('field_delivery_date_from')));
    $result['delivery_date_from']['field'] = 'field_delivery_date_from';

    return $result;
  }

  public function deliveryDateSecond($result) {
    if ($this->getFieldData('field_delivery_date_second')) {
      $result['ddate_second']['value'] = $this->dateConvert($this->getFieldData('field_delivery_date_second'), "m/d/Y");
      $result['ddate_second']['label'] = $this->getFieldInfo('field_delivery_date_second');
      $result['ddate_second']['raw'] = strtotime($this->dateConvert($this->getFieldData('field_delivery_date_second')));
      $result['ddate_second']['field'] = 'field_delivery_date_second';
    }
    else {
      $result['ddate_second']['value'] = "";
      $result['ddate_second']['label'] = "Delivery date second";
      $result['ddate_second']['raw'] = FALSE;
      $result['ddate_second']['field'] = 'field_delivery_date_second';
    }

    return $result;
  }

}
