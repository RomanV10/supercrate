<?php

namespace Drupal\move_services_new\Validation;

/**
 * Interface ValidationRulesInterface.
 *
 * @package Drupal\move_services_new\Validation
 */
interface ValidationRulesInterface {

  /**
   * Rules for validation.
   *
   * @return mixed
   *   Array with rules.
   */
  public function rules();

}
