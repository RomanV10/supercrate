<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class PayCheckJobsType.
 *
 * @package Drupal\move_services_new\Util
 */
class SmsNotificationFolder extends BaseEnum {

  const CLIENT = 1;
  const FOREMAN = 2;
  const MANAGER = 3;
  const OWNER = 4;

}
