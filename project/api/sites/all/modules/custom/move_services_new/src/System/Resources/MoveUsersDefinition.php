<?php

namespace Drupal\move_services_new\System\Resources;

use Drupal\move_services_new\Services\Users;
use PetstoreIO\User;

class MoveUsersDefinition {

  public static function getDefinition() {
    return array(
      'move_users_resource' => array(
        'operations' => array(
          'delete' => array(
            'help' => 'Delete user',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'description' => 'The user id for delete',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer users'),
          ),
        ),
        'actions' => array(
          'get_user_by_role' => array(
            'help' => 'Get user by role',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::getUserByRole',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'role',
                'optional' => FALSE,
                'type' => 'data',
                'description' => 'Role name\'s',
                'source' => array('data' => 'role'),
              ),
              array(
                'name' => 'active',
                'optional' => TRUE,
                'type' => 'bool',
                'default value' => TRUE,
                'source' => array('data' => 'active'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_count_users_by_role' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::getCountUsersByRole',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'role',
                'optional' => FALSE,
                'type' => 'string',
                'description' => 'Role name\'s',
                'default value' => 'authenticated user',
                'source' => array('data' => 'role'),
              ),
              array(
                'name' => 'active',
                'optional' => TRUE,
                'type' => 'bool',
                'default value' => TRUE,
                'source' => array('data' => 'active'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'write_user_settings' => array(
            'help' => 'Write custom user settings',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::writeUserSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'description' => 'Unique identifier for user',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'settings',
                'optional' => FALSE,
                'type' => 'data',
                'source' => array('data' => 'settings'),
              ),
            ),
            'access arguments' => array('administer users'),
          ),
          'get_user_settings' => array(
            'help' => 'Get user settings',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::getUserSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'description' => 'Unique identifier for user',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access user profiles'),
          ),
          'login' => array(
            'help' => 'Login a user for a new session',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::login',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'username',
                'type' => 'string',
                'description' => 'A valid username',
                'source' => array('data' => 'username'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'password',
                'type' => 'string',
                'description' => 'A valid password',
                'source' => array('data' => 'password'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'from_home_estimate',
                'type' => 'int',
                'description' => 'Dose user login from home estimate',
                'source' => array('data' => 'from_home_estimate'),
                'optional' => TRUE,
                'default value' => 0,
              ),
            ),
            'access callback' => 'services_access_menu',
          ),
          'set_user_parser' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::setUserParser',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'set_user_picture' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::savePictureUser',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'string',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'customer_view_request_add' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::customerViewRequestAdd',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'client',
                'type' => 'string',
                'source' => array('data' => 'client'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'customer_view_request_delete' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveUsersDefinition::customerViewRequestDelete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveUsersDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
        ),
      ),
    );
  }

  public static function delete($uid) {
    return Users::deleteUser($uid);
  }

  public static function getUserByRole($role, $active_user = TRUE) {
    return Users::getUserByRole($role, $active_user);
  }

  public static function getCountUsersByRole($role, $active_user = TRUE) {
    return Users::getCountUsersByRole($role, $active_user);
  }

  public static function writeUserSettings($uid, $settings) {
    return Users::writeUserSettings((int) $uid, $settings);
  }

  public static function getUserSettings($uid) {
    return Users::getUserSettings((int) $uid);
  }

  public static function login($username, $password, $from_home_estimate) {
    return Users::login($username, $password, $from_home_estimate);
  }

  public static function setUserParser(int $data) {
    Users::setUserParser($data);
  }

  public static function savePictureUser($data, $uid) {
    Users::savePictureUser($data, $uid);
  }

  /**
   * Add raw when customer view request.
   *
   * @param int $uid
   *   User id.
   * @param int $nid
   *   Node id.
   *
   * @return int
   *   If 0 - not added. Other number - added.
   */
  public static function customerViewRequestAdd(string $client, int $uid, int $nid, $data) {
    return Users::customerViewRequestAdd($client, $uid, $nid, json_encode($data));
  }

  /**
   * Remove raw when customer stop view request.
   *
   * @param int $uid
   *   User id.
   *
   * @return mixed
   */
  public static function customerViewRequestDelete(int $uid) {
    return Users::customerViewRequestDelete($uid);
  }

  /**
   * Get all customers what are viewed requests now.
   *
   * @param array $uids
   *   Users ids.
   *
   * @return array
   *   Array with user and requests.
   */
  public static function whatCustomersViewRequestsByClients(array $uids) {
    return Users::whatCustomersViewRequestsByClients($uids);
  }

}
