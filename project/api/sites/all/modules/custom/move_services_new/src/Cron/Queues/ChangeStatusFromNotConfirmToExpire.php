<?php

namespace Drupal\move_services_new\Cron\Queues;

use Carbon\Carbon;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class ChangeStatusFromNotConfirmToExpire.
 *
 * @package Drupal\move_services_new\Queues
 */
class ChangeStatusFromNotConfirmToExpire implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \EntityMetadataWrapperException
   */
  public static function execute($data): void {
    /* @var \EntityDrupalWrapper $entity_wrapper */
    $scheduleSettings = json_decode(variable_get('scheduleSettings', ''));
    $beforeHours = $scheduleSettings->ExperationBeforeTime ?? 0;
    $expirationReqType = $scheduleSettings->ExperationReqType ?? 'all';
    $timezone = Extra::getTimezone();
    foreach ($data as $request) {
      $toExpire = FALSE;
      $moveDate = Carbon::createFromFormat('Y-m-d H:i:s', $request['field_date_value'], $timezone);
      if ($expirationReqType == 'morning') {
        $startTime = Carbon::createFromFormat('g:ia', $request['field_actual_start_time_value'], $timezone)->hour;
        $isMorningTimeRequest = $startTime >= 7  && $startTime <= 10;
        if ($isMorningTimeRequest && ($moveDate->isYesterday() || Carbon::now($timezone)->diffInHours($moveDate, FALSE) <= 0) && $beforeHours <= Carbon::now($timezone)->hour) {
          $toExpire = TRUE;
        }
      }
      else {
        if ($beforeHours <= Carbon::now($timezone)->hour && ($moveDate->isTomorrow() || Carbon::now($timezone)->diffInHours($moveDate, FALSE) <= 0)) {
          $toExpire = TRUE;
        }
      }
      if ($toExpire) {
        $entity_wrapper = entity_metadata_wrapper('node', node_load($request['nid']));
        $entity_wrapper->field_approve->set(RequestStatusTypes::EXPIRED);
        $entity_wrapper->save();
      }
    }
  }

}
