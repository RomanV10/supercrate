<?php

namespace Drupal\move_services_new\Services\FieldStrategy;

/**
 * Interface FieldArrayStrategyInterface.
 *
 * @package Drupal\move_services_new\Services
 */
interface FieldArrayStrategyInterface {

  /**
   * Method for create structured array.
   *
   * @param mixed $values
   *   Value os field.
   * @param string $widget_type
   *   The widget type.
   * @param int $count_values
   *   Count values.
   * @param bool $multi
   *   Indicated that field is multi.
   * @param array $field_info
   *   Information about field.
   *
   * @return mixed
   *   Result
   */
  public function createData($values, string $widget_type, int $count_values, bool $multi = FALSE, array $field_info = array());

}
