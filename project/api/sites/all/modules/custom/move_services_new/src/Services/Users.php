<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_arrivy\Services\ArrivyForeman;

/**
 * Class Users.
 */
class Users extends BaseService {
  public static $user = NULL;

  /**
   * Constructor of Clients service.
   *
   * @param int $uid
   *   User id.
   */
  public function __construct($uid = NULL) {
    if ($uid) {
      self::$user = user_load((int) $uid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function create($user = array()) {}

  /**
   * {@inheritdoc}
   */
  public function retrieve() {}

  /**
   * {@inheritdoc}
   */
  public function update($user = array()) {}

  /**
   * {@inheritdoc}
   */
  public function delete() {}

  /**
   * Return all users who have the given role.
   *
   * @param array $roles
   *   Array with name of role or role ID.
   * @param bool $active_user
   *   Determine, if only the active users should be returned.
   * @param bool $uid_key
   *   Return only uid of user.
   *
   * @return array
   *   Array of user objects.
   *
   * @throws \Exception
   */
  public static function getUserByRole(array $roles = [], $active_user = TRUE, $uid_key = FALSE) {
    $users = array();
    $clients = new Clients();

    foreach ($roles as $role) {
      // TODO hot fix for role customer service.
      $is_customer_service = $role == 'customer service';
      if ($is_customer_service) {
        $role = 'sales';
      }
      $role = (!is_int($role)) ? user_role_load_by_name($role) : user_role_load($role);
      $uids = db_select('users_roles', 'ur')
        ->fields('ur', array('uid'))
        ->condition('ur.rid', $role->rid)
        ->execute()->fetchCol();
      if (!empty($uids)) {
        if (!$uid_key) {
          $query = new \EntityFieldQuery();
          $query->entityCondition('entity_type', 'user');
          $query->propertyCondition('uid', $uids, 'IN');
          $query->propertyCondition('status', (string) ((int) $active_user));
          $entities = $query->execute();

          if (!empty($entities)) {
            $tmp_users = array();
            $tmp_uids = array_keys($entities['user']);
            foreach ($tmp_uids as $uid) {
              $user = $clients->getUserFieldsData($uid, FALSE);

              // This setting must the same as status.
              $user['settings']['employee_is_active'] = (int) $user['status'];

              // Hard code names admins for exclude from result.
              if ($role->name == 'administrator' && ($user['name'] == 'roma4ke' || $user['name'] == 'eli' || $user['name'] == 'dimka' || $user['name'] == 'gabrielblare')) {
                unset($user);
                unset($uid);
                continue;
              }
              if ($is_customer_service) {
                if (isset($user['settings']['isCustomerService']) && $user['settings']['isCustomerService']) {
                  $tmp_users[$uid] = $user;
                }
              }
              else {
                if (!isset($user['settings']['isCustomerService']) || !$user['settings']['isCustomerService']) {
                  $tmp_users[$uid] = $user;
                }
              }
            }

            if ($is_customer_service) {
              $users['customer service'] = $tmp_users;
            }
            else {
              $users[$role->name] = $tmp_users;
            }
          }
        }
        else {
          $users = array_merge($uids, $users);
        }
      }

    }

    if ($uid_key) {
      $users = array_unique($users);
    }

    return $users;
  }

  public static function getCountUsersByRole($roles, $active_user = TRUE) {
    $count = 0;
    $users = Users::getUserByRole($roles, $active_user);
    foreach ($users as $array_users) {
      $count += count($array_users);
    }

    return $count;
  }

  public static function writeUserSettings($uid, $settings) {
    try {
      $write = array(
        'uid' => $uid,
        'settings' => serialize($settings),
      );

      $query = db_merge('user_settings')
        ->key(array('uid' => $uid))
        ->fields((array) $write);

      return $query->execute();
    }
    catch (\Throwable $e) {
      watchdog('Save settings', $e->getMessage(), WATCHDOG_ERROR);
    }
  }

  public static function getUserSettings($uid) {
    $result = array();

    $settings = db_select('user_settings', 'us')
      ->fields('us', array('settings'))
      ->condition('us.uid', $uid)
      ->execute()
      ->fetchAssoc();

    if (isset($settings['settings'])) {
      $result = unserialize($settings['settings']);
    }

    return $result;
  }

  public static function deleteUser($uid) {
    if ($uid == 1) {
      return services_error(t('The admin user cannot be deleted.'), 403);
    }

    $account = user_load($uid);
    if (empty($account)) {
      return services_error(t('There is no user with ID @uid.', array('@uid' => $uid)), 404);
    }
    // Delete foreman in arrivy remake within function to work
    $user_foreman = (new Clients($account->uid))->checkUserRoles(array('foreman'));
    if ($user_foreman) {
      (new ArrivyForeman($uid))->delete();
    }
    user_delete($uid);

    // Everything went right.
    return TRUE;
  }

  public static function login($username, $password, int $from_home_estimate = 0) {
    global $user;

    // Check if account is active.
    if (user_is_blocked($username)) {
      return services_error(t('The username %name has not been activated or is blocked.', array('%name' => $username)), 403);
    }

    // Emulate drupal native flood control: check for flood condition.
    $flood_state = array();
    if (variable_get('services_flood_control_enabled', TRUE)) {
      $flood_state = _user_resource_flood_control_precheck($username);
    }

    // Only authenticate if a flood condition was not detected.
    if (empty($flood_state['flood_control_triggered'])) {
      $uid = user_authenticate($username, $password);
    }
    else {
      $uid = FALSE;
    }

    // Emulate drupal native flood control: register flood event, and throw error
    // if a flood condition was previously detected.
    if (variable_get('services_flood_control_enabled', TRUE)) {
      $flood_state['uid'] = $uid;
      _user_resource_flood_control_postcheck($flood_state);
    }

    if ($uid) {
      $user = user_load($uid);
      if ($user->uid) {
        $user_metadata_wrapper = entity_metadata_wrapper('user', $user);
        $user_metadata_wrapper->field_previous_login = $user->login;
        $user_metadata_wrapper->field_is_home_estimate_login = (int) $from_home_estimate;
        $user_metadata_wrapper->save();

        user_login_finalize();

        $return = new \stdClass();
        $return->sessid = session_id();
        $return->session_name = session_name();
        $return->token = drupal_get_token('services');
        $account = clone $user;
        services_remove_user_data($account);
        $clients = new Clients($user->uid);
        $return->user = $clients->getCurrentUser();
        return $return;
      }
    }
    watchdog('user', 'Invalid login attempt for %username.', array('%username' => $username));
    return services_error(t('Wrong username or password.'), 401);
  }

  public static function setUserParser(int $uid) {
    variable_set('user_parser', $uid);
  }

  public static function getUserParser() {
    return variable_get('user_parser', 0);
  }

  /**
   * Check active/block user.
   *
   * @param int $uid
   *   The user id.
   *
   * @return bool
   *   Result of operation.
   */
  public static function checkUserIsActive(int $uid) {
    $select = db_select('users')
      ->fields('users', array('name'))
      ->condition('uid', $uid)
      ->condition('status', 1)
      ->execute()->fetchObject();
    return $select ? TRUE : FALSE;
  }

  public static function savePictureUser($value, $uid) {
    $directory = variable_get('user_picture_path');
    $file_saved = static::savePicture($value, $directory);

    $account = user_load($uid);
    // Standard Drupal validators for user pictures.
    $validators = array(
      'file_validate_is_image' => array(),
      'file_validate_image_resolution' => array(variable_get('user_picture_dimensions', '85x85')),
      'file_validate_size' => array(variable_get('user_picture_file_size', '30') * 1024),
    );

    $errors = file_validate($file_saved, $validators);
    if (empty($errors)) {
      $edit['picture'] = $file_saved;
      user_save($account, $edit);
    }
    return $file_saved;
  }

  public static function savePicture(string $value, string $directory) {
    $file_types = array(
      'image/jpeg' => 'jpg',
      'image/png' => 'png',
      'image/gif' => 'gif',
    );

    $file_exp = explode(',', $value);
    $file_data = array(
      'title' => (isset($value['title'])) ? $value['title'] : '',
      'data' => $file_exp[1],
    );

    $exp_code = explode(';', $file_exp[0]);
    $exp_mimetype = explode(':', $exp_code[0]);
    $picture_name = user_password(8);
    $destination = file_default_scheme() . '://' . $directory . '/' . $picture_name . '.' . $file_types[$exp_mimetype[1]];

    $dir = drupal_dirname($destination);
    // Build the destination folder tree if it doesn't already exists.
    if (!file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
      return services_error(t("Could not create destination directory for file."), 500);
    }

    // Write the file.
    $file_saved = file_save_data(base64_decode($file_data['data']), $destination);
    if ($file_saved) {
      $file_saved->filemime = $exp_mimetype[1];
      file_save($file_saved);
      if ($file_data['title']) {
        $file_saved->title = $file_data['title'];
      }
    }

    return $file_saved;
  }

  /**
   * Add raw when customer viewed request.
   *
   * @param int $uid
   *   User id.
   * @param int $nid
   *   Node id.
   *
   * @return int
   *   If 0 - not added. Other number - added.
   */
  public static function customerViewRequestAdd(string $client, int $uid, int $nid, $data) : int {
    return db_merge('move_view_request')
            ->key(['uid' => $uid, 'nid' => $nid])
            ->fields([
              'client'=> $client,
              'uid' => $uid,
              'nid' => $nid,
              'data' => $data,
              'time' => time()
            ])
            ->execute();
  }

  /**
   * Delete raw when client stop viewed request.
   *
   * @param int $uid
   *   User id.
   *
   * @return mixed
   */
  public static function customerViewRequestDelete(int $uid) {
    return db_delete('move_view_request')
            ->condition('uid', $uid)
            ->execute();
  }

  /**
   * Get online request by uid.
   *
   * @param array $uids
   *   User ids.
   *
   * @return mixed
   *  Array with results.
   */
  public static function whatCustomersViewRequestsByClients(array $uids) {
    $result = db_select('move_view_request', 'mvr')
            ->fields('mvr')
            ->condition('uid', $uids, 'IN')
            ->orderBy('mvr.time', 'DESC')
            ->execute()
            ->fetchAll(\PDO::FETCH_ASSOC);

    if ($result) {
      foreach ($result as $key => $value) {
        $result[$key]['data'] = json_decode($value['data']);
      }
    }

    return $result;
  }

  /**
   * Get user roles by sql query.
   *
   * @param int $uid
   *   User id.
   *
   * @return array
   *   Roles names.
   */
  public static function getUserRolesSql(int $uid) {
    $query = db_select('users_roles', 'ur');
    $query->innerJoin('role', 'r', 'r.rid=ur.rid');
    $query->condition('ur.uid', $uid);
    $query->fields('r', array('rid', 'name'));
    $result = $query->execute()->fetchAllKeyed();

    return $result;
  }

  /**
   * Get user first name.
   *
   * @param int $uid
   *   User id.
   *
   * @return string|null
   *   First name.
   */
  public static function getUserFirstNameByUid(int $uid) :?string {
    return db_select('field_data_field_user_first_name', 'fn')
      ->fields('fn', array('field_user_first_name_value'))
      ->condition('fn.entity_id', $uid)
      ->execute()
      ->fetchField();
  }

  /**
   * Get user last name.
   *
   * @param int $uid
   *   User id.
   *
   * @return string|null
   *   First name.
   */
  public static function getUserLastNameByUid(int $uid) :?string {
    return db_select('field_data_field_user_last_name', 'fn')
      ->fields('fn', array('field_user_last_name_value'))
      ->condition('fn.entity_id', $uid)
      ->execute()
      ->fetchField();
  }

}
