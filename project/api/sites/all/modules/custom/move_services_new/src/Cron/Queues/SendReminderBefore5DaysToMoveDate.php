<?php

namespace Drupal\move_services_new\Cron\Queues;

use Drupal\move_delay_launch\Services\Authorize;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\SmsActionTypes;
use Drupal\move_sms\Services\Sms;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class SendReminderBefore5DaysToMoveDate.
 *
 * @package Drupal\move_services_new\Cron\Queues
 */
class SendReminderBefore5DaysToMoveDate extends Authorize implements QueueInterface {

  /**
   * Execute task by queue.
   *
   * @param mixed $data
   *   Data needed for execute task.
   *
   * @throws \Exception
   */
  public static function execute($data): void {
    $type = 'move_reminder_5';
    $template_builder = new TemplateBuilder();
    foreach ($data as $nid => $uid) {
      $node = node_load($nid);
      self::globalAuthorize($uid);
      // Send mail to client.
      if ($node) {
        $is_send = $template_builder->moveTemplateBuilderSendTemplateRules($type, $node);
        if (!empty($is_send[$type]['send'])) {
          MoveRequest::markSentEmailByInterval($node, $type);
          (new Sms(SmsActionTypes::REMINDER_5DAYS))->execute($node->nid, $node, $node->uid);
        }
      }
    }
  }

}
