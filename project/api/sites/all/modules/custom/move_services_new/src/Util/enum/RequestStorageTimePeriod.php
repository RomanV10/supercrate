<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RequestStorageTimePeriod.
 *
 * @package Drupal\move_services_new\Util
 */
class RequestStorageTimePeriod extends BaseEnum {

  const WEEK = 0;
  const EVERY_TEN_MINUTE = 1;
  const THREE_WEEK = 2;
  const MOUNTH = 3;
  const TWO_MOUNTH = 4;
  const THREE_MOUNTH = 5;
  const FOUR_MOUNTH = 6;
  const HALF_YEAR = 7;

}
