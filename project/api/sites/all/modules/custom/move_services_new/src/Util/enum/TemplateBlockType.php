<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TemplateBlockType.
 *
 * @package Drupal\move_services_new\Util
 */
class TemplateBlockType extends BaseEnum {

  const HEADER = 0;
  const CONTENT = 1;
  const FOOTER = 2;

}
