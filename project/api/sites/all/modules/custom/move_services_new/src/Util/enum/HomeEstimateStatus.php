<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TypePayment.
 *
 * @package Drupal\move_services_new\Util
 */
class HomeEstimateStatus extends BaseEnum {

  const SCHEDULE = 1;
  const DONE = 2;
  const CANCELED = 3;

}
