<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_long_distance\Services\LongDistancePayroll;
use Drupal\move_services_new\Services\move_request\MoveRequestSearch;
use Drupal\move_services_new\Util\enum\RequestCompanyFlags;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_services_new\Util\enum\Roles;
use Drupal\move_services_new\Util\enum\RateCommission;
use Drupal\move_services_new\Util\enum\PayCheckJobsType;
use Drupal\move_services_new\Util\enum\ManagerRateCommission;
use Drupal\move_services_new\Util\enum\FormPayment;
use Drupal\move_services_new\Util\enum\AmountOption;
use Drupal\move_services_new\Util\enum\TypeCommission;
use Drupal\move_services_new\Util\enum\TypePayment;
use Drupal\move_services_new\Util\enum\ContractType;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_long_distance\Controller\ControllerPayroll as LDControllerPayroll;

/**
 * Class Payroll.
 */
class Payroll extends BaseService {

  private $dateFrom = '';
  private $dateTo = '';

  /**
   * Constructor of Payroll service.
   *
   * @param string $date_from
   *   Unix time.
   * @param string $date_to
   *   Unix time.
   */
  public function __construct(string $date_from = '', string $date_to = '') {
    if ($date_from && $date_to) {
      $this->dateFrom = Extra::getDateFrom($date_from);
      $this->dateTo = Extra::getDateTo($date_to);

      // Check what dateFrom less than dateTo.
      if ($this->dateFrom > $this->dateTo) {
        $this->dateTo = time();
      }
    }
  }

  /**
   * Override.
   */
  public function create($data = array()) {}

  /**
   * Override.
   */
  public function update($data = array()) {}

  /**
   * Override.
   */
  public function delete() {}

  /**
   * Override.
   */
  public function retrieve() {}

  /**
   * Get payroll details by role.
   *
   * @param int $role
   *   Id role.
   *
   * @return array
   *   All users payroll by role.
   *
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function detailsByRole(int $role) {
    $result = &drupal_static(__METHOD__ . ":$role:$this->dateFrom:$this->dateTo", []);
    if (!$result) {
      // Check role.
      if ($this->checkRole($role)) {
        $users = $this->getUsersByRole($role);
        foreach ($users as $uid) {
          $user_payment = $this->getUserPayroll($uid);
          if (isset($user_payment['totals'])) {

            $to_paid_misc = 0;
            foreach ($user_payment['hourly_misc'] as $item) {
              if ($item->amount_options == 1) {
                $to_paid_misc = $item->amount;
              }
            }

            $result[$uid]['jobs_count'] = isset($user_payment['jobs']) ? $this->calcCountJobs($user_payment['jobs'], $user_payment['trip_jobs']) : 0;
            $result[$uid]['hours'] = $user_payment['totals']['hours'];
            $result[$uid]['hours_pay'] = $user_payment['totals']['hours_pay'];
            $result[$uid]['grand_total'] = $user_payment['totals']['grand_total'];
            $result[$uid]['materials'] = $user_payment['totals']['materials'];
            $result[$uid]['extra'] = $user_payment['totals']['extra'];
            $result[$uid]['tip'] = $user_payment['totals']['tip'];
            $result[$uid]['total'] = $user_payment['totals']['total'];
            $result[$uid]['paid'] = $this->calcPaid($user_payment['paychecks']) + $to_paid_misc;
            $result[$uid]['uid'] = $uid;
          }
        }
      }
    }

    return $result;
  }

  /**
   * Calculate count jobs.
   *
   * @param array $jobs
   *   Jobs.
   * @param array $trip_jobs
   *   Trip jobs.
   *
   * @return int
   *   Count.
   */
  private function calcCountJobs(array $jobs, array $trip_jobs) {
    $count = 0;
    foreach ($jobs as $job) {
      $count_jobs = count($job);
      $flat_count_jobs = array(1, 2);
      if (in_array($count_jobs, $flat_count_jobs)) {
        $count += $count_jobs;
      }
      else {
        $count++;
      }
    }

    $trip_count = count($trip_jobs);
    $count += $trip_count;

    return $count;
  }

  /**
   * Get payroll details.
   *
   * @return array
   *   All details.
   *
   * @throws \SearchApiException
   */
  /**
   * @return array
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function detailsAll() {
    $result = array();
    $roles = Roles::getConstants();
    unset($roles['DEFAULT']);
    unset($roles['ANONYMOUS']);
    unset($roles['CUSTOMER']);
    unset($roles['ADMINISTRATOR']);

    foreach ($roles as $role) {
      $result[$role] = array(
        'role_name' => $this->getRoleName($role),
        'jobs_count' => 0,
        'hours' => 0,
        'hours_pay' => 0,
        'grand_total' => 0,
        'materials' => 0,
        'extra' => 0,
        'tip' => 0,
        'total' => 0,
        'paid' => 0,
      );

      $users = $this->detailsByRole($role);
      foreach ($users as $user) {
        $result[$role]['jobs_count'] += $user['jobs_count'];
        $result[$role]['hours'] += $user['hours'];
        $result[$role]['hours_pay'] += $user['hours_pay'];
        $result[$role]['grand_total'] += $user['grand_total'];
        $result[$role]['materials'] += $user['materials'];
        $result[$role]['extra'] += $user['extra'];
        $result[$role]['tip'] += $user['tip'];
        $result[$role]['paid'] += $user['paid'];
        $result[$role]['total'] += $user['total'];
      }
    }

    return $result;
  }

  /**
   * All workers who worked on request.
   *
   * @param int $nid
   *   The node id.
   * @param int $contract_type
   *   Flat Rate contract type: 0 - Pickup, 1 - Delivery.
   *
   * @return array
   *   Array with workers who worked on request.
   */
  public function getAllPayrollInfo(int $nid, int $contract_type = ContractType::PAYROLL) : array {
    $result = array();
    $workers = $this->getWorkersByRole();
    foreach ($workers as $uid => $role_name) {
      $user_role = $this->userRole(user_load($uid));
      $filter = $this->filterRequests(array($nid), $user_role, $uid, $contract_type);
      if ($filter) {
        $result[$role_name][$uid] = $this->getJobUser($nid, $uid, TRUE, $contract_type);
      }
    }

    return $result;
  }

  private function setDate($date_from, $date_to) {
    $this->dateFrom = Extra::getDateFrom($date_from);
    $this->dateTo = Extra::getDateTo($date_to);
  }

  /**
   * Calculate the sum of all payments.
   *
   * @param array $paychecks
   *   All paychecks.
   *
   * @return float
   *   Total sum of all payments.
   */
  private function calcPaid(array $paychecks = array()): float {
    $sum = 0;

    foreach ($paychecks as $paycheck) {
      if ((int) $paycheck->type === FormPayment::CASH) {
        $sum -= $paycheck->amount;
      }
      else {
        $sum += $paycheck->amount;
      }
    }

    return $sum;
  }

  private function getWorkersByRole() {
    $result = array();
    $num_roles = new Roles();
    $roles = $num_roles->getConstants();

    $roles_func = function ($role) use (&$result) {
      $role_name = $this->getRoleName($role);
      $users = $this->getUsersByRole($role);
      foreach ($users as $uid) {
        $result[$uid] = $role_name;
      }
    };

    array_walk($roles, $roles_func);
    return $result;
  }

  /**
   * Create a new paycheck for worker.
   *
   * @param int $uid
   *   User id.
   * @param array $data
   *   Array with data for paycheck.
   *
   * @return bool
   *   Status of operation.
   */
  public function applyPaycheck(int $uid, array $data) {
    $result = FALSE;
    $user = user_load($uid);
    $role = $this->userRole($user);
    if ($role && (isset($data['jobs']) || isset($data['hourly_misc']))) {
      // Check data.
      $paycheck = array();
      $paycheck['date'] = strtotime($data['date']);
      $paycheck['form'] = (int) $data['form'];
      $paycheck['amount'] = floatval($data['amount']);
      $paycheck['pending'] = isset($data['pending']) ? 1 : 0;
      $paycheck['check_numb'] = 0;
      if ($paycheck['form'] == FormPayment::CASH && isset($data['check_numb'])) {
        $paycheck['check_numb'] = (int) $data['check_numb'];
      }
      $paycheck['note'] = isset($data['note']) ? $data['note'] : '';
      $paycheck['uid'] = $uid;
      $paycheck['created'] = time();

      // Write to DB.
      $id = db_insert('move_paycheck')
        ->fields($paycheck)
        ->execute();

      if (isset($data['jobs']) && $data['jobs']) {
        $nids = array_column((array) $data['jobs'], 'nid');
        $types = array_column((array) $data['jobs'], 'contract_type');
        $combined = array_combine($nids, $types);

        $trips = $this->extractTrips($nids, $data['jobs']);
        $trips = $this->filterTrips($trips, $uid);

        // Check what user have relationship to jobs.
        $jobs = $this->filterRequests((array) $nids, $role, $uid);

        $jobs = array_merge($jobs, $trips);
        foreach ($jobs as $nid) {
          db_insert('move_paycheck_jobs')
            ->fields(array(
              'id' => $nid,
              'payment_id' => $id,
              'type' => PayCheckJobsType::NORMALJOB,
              'flat_rate' => $combined[$nid],
            ))
            ->execute();
        }
      }

      if (isset($data['hourly_misc']) && $data['hourly_misc']) {
        // Check what user have relationship to hourly/misc.
        $hourly_misc = $this->filterHourlyMisc($uid, array_values((array) $data['hourly_misc']));
        foreach ($hourly_misc as $item) {
          db_insert('move_paycheck_jobs')
            ->fields(array(
              'id' => $item->id,
              'payment_id' => $id,
              'type' => PayCheckJobsType::HOURLYMISC,
            ))
            ->execute();
        }
      }

      $result = $id;
      // Updating cache payroll for date.
      if ($result) {
        $this->updateCachePayrollPaycheck($id, 1);
      }
    }

    return $result;
  }

  /**
   * Get payroll for concrete user.
   *
   * @param int $uid
   *   User id.
   *
   * @return array
   *   User payroll.
   *
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function getUserPayroll(int $uid) : array {
    $result = &drupal_static(__METHOD__ . ":$uid:$this->dateFrom:$this->dateTo", []);
    if (!$result) {
      set_time_limit(120);
      if ($this->dateFrom && $this->dateTo && Users::checkUserIsActive($uid)) {
        $userRoles = Users::getUserRolesSql($uid);
        $role = $this->userRolePayroll($userRoles);
        $result['jobs'] = array();
        $result['jobs_info'] = array();

        if ($nids = $this->getAllRequestsForPeriod()) {
          $filter_requests = $this->filterRequests((array) $nids, $role, $uid);
          $search = new MoveRequestSearch();
          if ($filter_requests && ($role == Roles::FOREMAN || $role == Roles::DRIVER)) {
            $search->filterByForeman($uid, $filter_requests, TRUE, $this->dateFrom, $this->dateTo);
          }
          elseif ($filter_requests && $role == Roles::HELPER) {
            $search->filterByHelper($uid, $filter_requests, TRUE, $this->dateFrom, $this->dateTo);
          }

          $jobs = $this->getPayrollByNids($filter_requests, $uid);
          if (isset($jobs['jobs']) && $jobs['jobs']) {
            $result['jobs'] = $jobs['jobs'];
            $result['jobs_info'] = $this->getPayrollInfoUser($result['jobs'], $uid);
          }
        }

        // For trips.
        $ld_instance = new LDControllerPayroll(array(), date("Y-m-d", $this->dateFrom), date("Y-m-d", $this->dateTo));
        $trip_jobs = array();
        if ($ldIds = $ld_instance->getLdRequestsIdsForPayroll()) {
          $filter_requests = $ld_instance->filterLongDistance($ldIds, $uid, $role);
          $jobs = $ld_instance->getLDRequestsByIds($filter_requests);

          foreach ($jobs as $job) {
            $trip_id = $job['details']['ld_trip_id'];
            $trip_jobs[$trip_id] = array(
              'contract_type' => ContractType::LONGDISTANCE,
              'grand_total' => (float) $job['details']['total_payroll'],
              'hours' => 0,
              'hours_pay' => 0,
              'job_type' => 'Long Distance',
              'move_date' => $job['details']['date_end'],
              'total' => 0,
            );

            switch ($role) {
              case Roles::FOREMAN:
                $user_data = $job['foreman'];
                $trip_jobs[$trip_id]['hours'] = $user_data['total_hours'];
                $trip_jobs[$trip_id]['hours_pay'] = $user_data['total_hourly'];
                $trip_jobs[$trip_id]['total'] = (float) $user_data['total_payroll'];
                break;

              case Roles::HELPER:
                foreach ($job['helper'] as $helper) {
                  if ($helper['uid'] != $uid) {
                    continue;
                  }
                  $trip_jobs[$trip_id]['total'] = (float) $helper['total_payroll'];
                }

                break;
            }
          }

        }

        $result['hourly_misc'] = $this->getHourlyMisc($uid);
        $result['paychecks'] = $this->viewPaychecks($this->paycheckUserForPeriod($uid));
        $result['trip_jobs'] = $trip_jobs;
        $result_jobs = (isset($result['jobs']) && $result['jobs']) ? $result['jobs'] : array();
        $result['totals'] = $this->calculateTotalUser($result['paychecks'], $result_jobs, $result['hourly_misc'], $trip_jobs);
      }
    }

    return $result;
  }

  private function viewPaychecks($paychecks) :array {
    $result = array();
    foreach ($paychecks as $paycheck) {
      $fill = function ($type) use (&$result, $paycheck) {
        if (array_key_exists($paycheck->payment_id, $result)) {
          $result[$paycheck->payment_id]->{$type}[] = $paycheck->job_id;
        }
        else {
          $result[$paycheck->payment_id] = $paycheck;
          $result[$paycheck->payment_id]->{$type} = array($paycheck->job_id);
          unset($result[$paycheck->payment_id]->job_id);
        }
      };

      $paycheck->type ? $fill('misc') : $fill('jobs');
    }

    return $result;
  }

  private function getPayrollByNids(array $nids, int $uid) : array {
    $result = array();

    foreach ($nids as $nid) {
      // If request have flat_rate type.
      $is_flat_rate = $this->isFlatRate($nid);

      // Anonymous function for get total job info.
      $get_job_data = function (array $job, int $contract_type = ContractType::PAYROLL) use ($is_flat_rate, $uid, $nid) : array {
        $total = 0;
        if (isset($job['total'])) {
          $total = $job['total'];
        }
        $req_data = $this->requestData($nid);

        $extract_hours = array('total' => 0);
        $solve_commission = array(RateCommission::HOURLYRATE, RateCommission::HOURLYRATEHELPER);
        if (isset($job['commissions'])) {
          foreach ($solve_commission as $commission) {
            if (array_key_exists($commission, $job['commissions'])) {
              $extract_hours['hours'] = $job['commissions'][$commission]['for_commission'];
              $extract_hours['total'] = $job['commissions'][$commission]['total'];
              break;
            }
          }
        }

        $contract = $this->getContract($nid, $contract_type);
        $workers_hours = $this->getWorkersHours($contract);
        $worker_hours = !empty($workers_hours[$uid]) ? $workers_hours[$uid] : 0;

        // How many worked hours from contract.
        if (!isset($extract_hours['hours'])) {
          $extract_hours['hours'] = $worker_hours;
        }

        $grand_total = isset($contract['grand_total']) ? $contract['grand_total'] : 0;
        $tip = isset($job['commissions']) ? $this->getWorkerHandTip($job['commissions']) : 0;
        $bonus = isset($job['commissions']) ? $this->getWorkerBonus($job['commissions']) : 0;

        $custom_payroll = $this->getCustomPayroll($nid, $uid);

        // Call this here to calculate job total before.
        $custom_total = 0;

        foreach ($custom_payroll as $item) {
          $item = (array) $item;
          $custom_total += $item['amount'];
        }
        // Add custom payroll total to job total.
        $total += $custom_total;
        $custom_payroll_total = $custom_total;

        return array(
          'nid' => $nid,
          'advanced' => NULL,
          'delivery' => $req_data['delivery'],
          'extra' => $this->getPayrollExtra($nid, $uid, $contract_type),
          'grand_total' => $grand_total,
          'hours' => $extract_hours['hours'],
          'hours_pay' => $extract_hours['total'],
          'ins' => NULL,
          'job_type' => $req_data['job_type'],
          'move_date' => $req_data['move_date'],
          'materials' => $this->getPayrollMaterials($nid, $uid, $contract_type),
          'pickup' => $req_data['pickup'],
          'tip' => $tip,
          'bonus' => $bonus,
          'custom_payroll' => $custom_payroll_total,
          'is_flat_rate' => $is_flat_rate,
          'contract_type' => $contract_type,
          'total' => $total,
        );
      };


      if ($is_flat_rate) {
        // If flat rate request have 1 job then it PICKUP else 2 then it DELIVERY.
        for ($job_type = 0; $job_type <= 1; $job_type++) {
          $dates = array($this->dateFrom, $this->dateTo);
          if ($job_info = $this->getJobUser($nid, $uid, TRUE, $job_type, $dates)) {
            $result['jobs'][$nid][] = $get_job_data($job_info, $job_type);
          }
        }
      }
      else {
        $job_info = $this->getJobUser($nid, $uid);
        $result['jobs'][$nid] = $get_job_data($job_info);
      }
    }

    return $result;
  }

  /**
   * Get materials for payroll by user.
   *
   * @param int $nid
   *   Node id.
   * @param int $uid
   *   User id.
   * @param int $contract_type
   *
   * @return float
   *   Sum materials payroll.
   */
  private function getPayrollMaterials(int $nid, int $uid, int $contract_type = ContractType::PAYROLL) : float {
    // If user already have saved payroll.
    $saved_commission = $this->savedPayrollCommission($nid, $uid, $contract_type, RateCommission::MATERIALSCOMMISSION);
    if ($saved_commission !== FALSE) {
      return $saved_commission;
    }

    $sum = $this->getSumMaterials($nid);
    return $this->calculateMaterials($uid, $nid, $sum, $contract_type);
  }

  /**
   * Helper method for detect already saved payroll commission.
   *
   * @param $nid
   * @param $uid
   * @param $contract_type
   * @param $rate_type
   *
   * @return bool|int
   */
  private function savedPayrollCommission($nid, $uid, $contract_type, $rate_type) {
    $job = $this->getJobUser($nid, $uid, FALSE, $contract_type);
    if ($job && isset($job['commissions'][$rate_type])) {
      return $job['commissions'][$rate_type]['total'];
    }
    elseif ($job && !isset($job['commissions'][$rate_type])) {
      return 0;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Calculating sum materials for request.
   *
   * @param int $nid
   *   Node id.
   *
   * @return float
   *   Calculated materials sum.
   */
  private function getSumMaterials(int $nid) {
    $total = 0;
    $request = entity_metadata_wrapper('node', $nid);
    $all_data = MoveRequest::getRequestAllData($nid);
    if (isset($all_data['invoice']['request_data']['value'])) {
      $decode_data = (object) $all_data['invoice']['request_data']['value'];
      if (is_object($decode_data) && property_exists($decode_data, 'packings')) {
        // For Flat Rate and Long Distance service type.
        if (in_array($request->field_move_service_type->value(), array(5, 7))) {
          $settings = json_decode(variable_get('basicsettings'));
          $properties = array('LDRate', 'quantity');
          $checkShowLaborRate = $settings->packing_settings->packingLabor;
          foreach ($decode_data->packings as $packing) {
            $packing_obj = (object) $packing;
            if (Extra::findProperties($packing_obj, $properties)) {
              $labor_rate = ($checkShowLaborRate && property_exists($packing_obj, 'laborRate')) ? $packing_obj->laborRate : 0;
              $total += ($packing_obj->LDRate + $labor_rate) * $packing_obj->quantity;
            }
          }
        }
        else {
          $properties = array('rate', 'quantity');
          foreach ($decode_data->packings as $packing) {
            $packing_obj = (object) $packing;
            if (Extra::findProperties($packing_obj, $properties)) {
              $packing_obj->rate = self::validateFloatValue($packing_obj->rate);
              $total += $packing_obj->rate * $packing_obj->quantity;
            }
          }
        }
      }
    }

    return (float) $total;
  }

  /**
   * Calculate rate materials for worker.
   *
   * @param int $uid
   *   User id.
   * @param int $nid
   *   Node id.
   * @param float $sum
   *   Sum all materials.
   * @param int $contract_type
   *   The type of contract.
   *
   * @return float
   *   Calculated rate materials for worker.
   */
  private function calculateMaterials(int $uid, int $nid, float $sum, int $contract_type = ContractType::PAYROLL) : float {
    $rate = 0;

    // Foreman as Helper have not this commissions.
    if ($this->detectHourlyRateAsHelper($nid, $uid, $contract_type)) {
      return 0;
    }

    $payroll_info = $this->getJobUser($nid, $uid, FALSE, $contract_type);
    if (isset($payroll_info['commissions']) && array_key_exists(RateCommission::MATERIALSCOMMISSION, $payroll_info['commissions'])) {
      $payroll_commission = $payroll_info['commissions'][RateCommission::MATERIALSCOMMISSION]['rate'];
      $payroll_extra_total = $payroll_info['commissions'][RateCommission::MATERIALSCOMMISSION]['for_commission'];
      if ($payroll_commission && $payroll_extra_total) {
        $rate = ($payroll_commission / 100) * $payroll_extra_total;
      }
    }
    elseif ($materials_commission = $this->getUserCommission($uid, RateCommission::MATERIALSCOMMISSION)) {
      $sum = $contract_type != ContractType::PAYROLL ? $sum / 2 : $sum;
      $rate = ($materials_commission / 100) * $sum;
    }

    return round($rate, 2);
  }

  /**
   * Get extra payroll.
   *
   * @param int $nid
   *   Node id.
   * @param int $uid
   *   User id.
   * @param int $contract_type
   *
   * @return float
   *   Calculate result.
   */
  private function getPayrollExtra(int $nid, int $uid, int $contract_type = ContractType::PAYROLL) : float {
    // If user already have saved payroll.
    $saved_commission = $this->savedPayrollCommission($nid, $uid, $contract_type, RateCommission::EXTRASCOMMISSION);
    if ($saved_commission !== FALSE) {
      return $saved_commission;
    }

    $sum = $this->sumExtra($nid);
    return $this->calculateExtra($uid, $nid, $sum, $contract_type);
  }

  /**
   * Calculate extra sum.
   *
   * @param int $nid
   *   Node id.
   * @param array $item_names
   *   Specific name items. Items, that included to sum.
   *
   * @return float
   *   Sum of all extra services.
   */
  private function sumExtra(int $nid, array $item_names = array()) : float {
    $result = 0;
    $extra = $this->getRequestAddServices($nid);

    foreach ($extra as $item) {
      $item = (object) $item;
      $item_property = property_exists($item, 'name');
      // If item_name must be included to sum, we skip it item
      if ($item_names && (($item_property && in_array($item->name, $item_names)) || !$item_property)) {
        continue;
      }

      if (property_exists($item, 'extra_services')) {
        $items = array();

        if (is_array($item->extra_services)) {
          foreach ($item->extra_services as $extra_service) {
            $extra_service = (object) $extra_service;
            if (property_exists($extra_service, 'services_default_value')) {
              $items[] = $extra_service->services_default_value;
            }
          }
        }
        elseif (isset($item->extra_services->services_default_value)) {
          $items[] = $item->extra_services->services_default_value;
        }

        $result += array_product($items);
      }
    }

    return (float) $result;
  }

  private function calculateExtra($uid, $nid, $total, int $contract_type = ContractType::PAYROLL) : float {
    $rate = 0;

    // Foreman as Helper have not this commissions.
    if ($this->detectHourlyRateAsHelper($nid, $uid, $contract_type)) {
      return 0;
    }

    $extra_commission = $this->getUserCommission($uid, RateCommission::EXTRASCOMMISSION);
    $payroll_info = $this->getJobUser($nid, $uid, FALSE, $contract_type);
    if (isset($payroll_info['commissions']) && array_key_exists(RateCommission::EXTRASCOMMISSION, $payroll_info['commissions'])) {
      $payroll_commission = $payroll_info['commissions'][RateCommission::EXTRASCOMMISSION]['rate'];
      $payroll_extra_total = $payroll_info['commissions'][RateCommission::EXTRASCOMMISSION]['for_commission'];
      if ($payroll_commission && $payroll_extra_total) {
        $rate = ($payroll_commission / 100) * $payroll_extra_total;
      }
    }
    elseif ($extra_commission) {
      $rate = ($extra_commission / 100) * $total;
    }

    return $rate;
  }

  private function getUserCommission($uid, $rate_commission) {
    $commission = 0;
    $user_rate = $this->rateWorkUser($uid);
    if (array_key_exists($rate_commission, $user_rate)) {
      $commission = $user_rate[$rate_commission]['rate'];
    }

    return $commission;
  }

  private function getPayrollInfoUser($jobs, $uid) {
    $result = array();

    $calc_result = function (array $job, int $job_type, int $nid) use (&$result, $uid) {
      $contract_type = isset($job['contract_type']) ? (int) $job['contract_type'] : ContractType::PAYROLL;
      $payroll_info = $this->getJobUser($nid, $uid, TRUE, $contract_type);
      $flat_rate_types = array(ContractType::PICKUP_PAYROLL, ContractType::DELIVERY_PAYROLL);
      if ($payroll_info && in_array($contract_type, $flat_rate_types)) {
        $result[$nid][$contract_type] = $payroll_info;
      }
      elseif ($payroll_info) {
        $result[$nid] = $payroll_info;
      }
    };

    foreach ($jobs as $nid => $job) {
      if (count($job) <= 2) {
        array_walk($job, $calc_result, $nid);
      }
      else {
        $calc_result($job, 0, $nid);
      }
    }

    return $result;
  }

  /**
   * Get Payroll of user by job(node).
   *
   * @param int $nid
   *   Node id.
   * @param int $uid
   *   User id.
   * @param bool $add
   *   Additional data about user.
   * @param int $contract_type
   *   The type of contract.
   *
   * @return array
   *   Job of user.
   */
  private function getJobUser(int $nid, int $uid, bool $add = TRUE, int $contract_type = ContractType::PAYROLL, $date = array()) : array {
    $query = db_select('move_payroll_info', 'mpi');
    if (!empty($date) && is_array($date) && (count($date) == 2)) {
      $form_date = date('Y-m-d H:i:s', $date[0]);
      $to_date = date('Y-m-d H:i:s', $date[1]);
      $date = array($form_date, $to_date);
      switch ($contract_type) {
        case ContractType::PICKUP_PAYROLL:
          $query->leftJoin('field_data_field_date', 'pickup_date', 'mpi.nid = pickup_date.entity_id');
          $query->condition('pickup_date.field_date_value', $date, 'BETWEEN');
          break;

        case  ContractType::DELIVERY_PAYROLL:
          $query->leftJoin('field_data_field_delivery_date', 'delivery_date', 'mpi.nid = delivery_date.entity_id');
          $query->condition('delivery_date.field_delivery_date_value', $date, 'BETWEEN');

          break;
      }
    }
    $query->fields('mpi');
    $query->condition('mpi.nid', $nid);
    $query->condition('mpi.uid', $uid);
    $query->condition('mpi.flat_rate', $contract_type);
    $payroll = $query->execute()->fetchCol(2);

    $payroll = reset($payroll);
    $payroll = ($payroll) ? unserialize($payroll) : array();
    if ($add && !empty($payroll)) {
      $this->helperData($payroll, $uid, $nid);
    }

    return $payroll;
  }

  /**
   * Get helper user data.
   *
   * @param array $job_data
   *   Job data.
   * @param int $uid
   *   User id.
   * @param int $nid
   *   Node id.
   */
  private function helperData(array &$job_data, int $uid, int $nid) : void {
    $userRoles = Users::getUserRolesSql($uid);
    $role = $this->userRolePayroll($userRoles);

    if ($role == Roles::MANAGER) {
      $rate = $this->allManagerRates();
    }
    else {
      $rate = $this->allWorkerRates();
    }

    $firstName = Users::getUserFirstNameByUid($uid) ?? '';
    $lastName = Users::getUserLastNameByUid($uid) ?? '';
    $job_data['name'] = "{$firstName} {$lastName}";

    if (isset($job_data['commissions'])) {
      foreach ($job_data['commissions'] as $id => &$commission) {
        if (!isset($rate[$id])) {
          unset($job_data['commissions'][$id]);
          watchdog("Helper Data", "Unknown type of Rate \"{$id}\": Uid: {$uid}; Nid: {$nid}", array(), WATCHDOG_WARNING);
        }
        else {
          $commission['editable'] = $rate[$id]['editable'];
          $commission['type'] = $rate[$id]['type'];
        }
      }
    }
  }

  /**
   * Get some fields of move request for payroll.
   *
   * @param int $nid
   *   Move request id.
   *
   * @return array
   *   Fields data.
   */
  private function requestData(int $nid) : array {
    $result = &drupal_static(__METHOD__ . ":$nid", array());
    if (!$result) {
      $result['move_date'] = self::getMoveDate($nid);
      $result['job_type'] = self::getRequestServiceType($nid);
      $result['pickup'] = self::getMovingFromAdmArea($nid);
      $result['delivery'] = self::getMovingToAdmArea($nid);
    }

    return $result;
  }

  private function getHourlyMisc(int $uid) {
    $select = db_select('move_hourly_misc_payment', 'mhm');
    $select->fields('mhm')
      ->condition('mhm.payee', $uid)
      ->condition('mhm.date', array($this->dateFrom, $this->dateTo), 'BETWEEN');
    return $select->execute()->fetchAllAssoc('id');
  }

  private function getHourlyMiscById(int $mid) {
    return db_select('move_hourly_misc_payment', 'mhm')
      ->fields('mhm')
      ->condition('mhm.id', $mid)
      ->execute()->fetchAssoc();
  }

  /**
   * Get enum const for user role.
   *
   * @param array|null $userRole
   *   User roles.
   *
   * @return int
   *   Enum const.
   */
  private function userRolePayroll(?array $userRole) {
    $role = Roles::DEFAULT;

    if (array_search('manager', $userRole)) {
      $role = Roles::MANAGER;
    }
    elseif (array_search('helper', $userRole)) {
      $role = Roles::HELPER;
    }
    elseif (array_search('foreman', $userRole)) {
      $role = Roles::FOREMAN;
    }
    elseif (array_search('driver', $userRole)) {
      $role = Roles::DRIVER;
    }
    elseif (array_search('sales', $userRole)) {
      $role = Roles::SALES;
    }
    elseif (array_search('customer service', $userRole)) {
      $role = Roles::CUSTOMER_SERVICE;
    }

    return $role;
  }

  public function userRole(\stdClass $user, bool $allRoles = FALSE) : int {
    $role = Roles::DEFAULT;

    if (array_search('manager', $user->roles)) {
      $role = Roles::MANAGER;
    }
    elseif (array_search('helper', $user->roles)) {
      $role = Roles::HELPER;
    }
    elseif (array_search('foreman', $user->roles)) {
      $role = Roles::FOREMAN;
    }
    elseif (array_search('driver', $user->roles)) {
      $role = Roles::DRIVER;
    }
    elseif (array_search('sales', $user->roles)) {
      $role = Roles::SALES;
    }
    elseif (array_search('customer service', $user->roles)) {
      $role = Roles::CUSTOMER_SERVICE;
    }

    if ($allRoles) {
      if (array_search('owner', $user->roles)) {
        $role = Roles::OWNER;
      }
      elseif (array_search('administrator', $user->roles)) {
        $role = Roles::ADMINISTRATOR;
      }
      elseif (array_search('anonymous', $user->roles)) {
        $role = Roles::ANONYMOUS;
      }
      elseif (array_search('authenticated user', $user->roles)) {
        $role = Roles::CUSTOMER;
      }
    }

    return $role;
  }

  private function checkRole($role) {
    $check = FALSE;

    try {
      $check = new Roles($role);
    }
    catch (\Throwable $uve) {
      watchdog("Payroll", "Check role exception: {$uve->getMessage()}", array(), WATCHDOG_WARNING);
    }

    return $check;
  }

  private function filterRequests(array $nids, int $role, int $uid, int $contract_type = ContractType::PAYROLL) : array {
    try {
      $result = array();

      if (empty($nids)) {
        return $result;
      }

      if ($role == Roles::MANAGER || $role == Roles::SALES) {
        $manager_nids = $this->filterRequestByManager($uid, $nids);
        if (!empty($manager_nids)) {
          $result = $this->payrollManagerFilterRequests($manager_nids, $uid, $contract_type);
        }
      }
      else {
        $query_s = db_select('node', 'n')
          ->fields('n', array('nid'))
          ->condition('n.type', 'move_request')
          ->condition('n.status', NODE_PUBLISHED)
          ->condition('n.nid', $nids, 'IN');
        if ($contract_type == ContractType::PAYROLL) {
          $query_s->leftJoin('field_data_field_foreman', 'ff', 'n.nid = ff.entity_id');
          $query_s->leftJoin('field_data_field_helper', 'fh', 'n.nid = fh.entity_id');
          $query_s->condition(
            db_or()
              ->condition('ff.field_foreman_target_id', $uid)
              ->condition('fh.field_helper_target_id', $uid)
          );
        }
        elseif ($contract_type == ContractType::DELIVERY_PAYROLL) {
          $query_s->leftJoin('field_data_field_foreman_delivery', 'ff', 'n.nid = ff.entity_id');
          $query_s->leftJoin('field_data_field_helper_delivery', 'fh', 'n.nid = fh.entity_id');
          $query_s->condition(
            db_or()
              ->condition('ff.field_foreman_delivery_target_id', $uid)
              ->condition('fh.field_helper_delivery_target_id', $uid)
          );
        }
        elseif ($contract_type == ContractType::PICKUP_PAYROLL) {
          $query_s->leftJoin('field_data_field_foreman_pickup', 'ff', 'n.nid = ff.entity_id');
          $query_s->leftJoin('field_data_field_helper_pickup', 'fh', 'n.nid = fh.entity_id');
          $query_s->condition(
            db_or()
              ->condition('ff.field_foreman_pickup_target_id', $uid)
              ->condition('fh.field_helper_pickup_target_id', $uid)
          );
        }
        $query_s->groupBy('n.nid');
        $result = $query_s->execute()->fetchCol();
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Payroll filterRequests', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      return $result;
    }
  }

  /**
   * Filter trips by user id.
   *
   * @param array $trips
   *   Trips ids.
   * @param int $uid
   *   User id.
   *
   * @return array
   *   Empty array or array with trips id.
   */
  private function filterTrips(array $trips, int $uid) : array {
    $result = array();

    try {
      if (!empty($trips)) {
        $query = db_select('ld_trip', 'lt');
        $query->leftJoin('ld_trip_foreman', 'ldf', 'ldf.ld_trip_id = lt.id');
        $query->leftJoin('ld_trip_helper', 'ldh', 'ldh.ld_trip_id = lt.id');
        $query->condition(
          db_or()
            ->condition('ldf.foreman_id', $uid)
            ->condition('ldh.helper_id', $uid)
        );
        $query->condition('lt.id', $trips, 'IN');
        $query->groupBy('lt.id');
        $query->fields('lt', array('id'));
        $result = $query->execute()->fetchCol();
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Payroll', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      return $result;
    }
  }

  /**
   * Extract trips from nids array.
   *
   * Update nids array by link. Return trips array with trips ids.
   *
   * @param array $nids
   *   Array of nids.
   * @param array $jobs
   *   Array with nids and conract type to know where is trip.
   *
   * @return array
   *   Empty array or trips ids.
   */
  private function extractTrips(array &$nids, array $jobs) {
    $trips = array();
    foreach ($jobs as $job) {
      if (in_array($job['nid'], $nids) && $job['contract_type'] == ContractType::LONGDISTANCE) {
        unset($nids[array_search($job['nid'], $nids)]);
        $trips[] = $job['nid'];
      }
    }
    return $trips;
  }

  /**
   * @param array $nids
   * @param int $uid
   * @param null $contract_type
   * @return array
   */
  private function payrollManagerFilterRequests(array $nids, int $uid, $contract_type = NULL) {
    $result_nids = array();
    $date_from = $this->dateFrom;
    $date_to = $this->dateTo;

    if ($contract_type === NULL) {
      $query = function (int $nid, $contract_type = ContractType::PAYROLL) use ($uid) : array {
        return db_select('move_payroll_info', 'mpi')
          ->fields('mpi')
          ->condition('mpi.nid', $nid)
          ->condition('mpi.uid', $uid)
          ->condition('mpi.flat_rate', $contract_type)
          ->execute()
          ->fetchCol(0);
      };

      $date_belong = function (int $date) use ($date_from, $date_to) : bool {
        return ($date >= $date_from && $date <= $date_to) ? TRUE : FALSE;
      };

      foreach ($nids as $nid) {
        if ($this->isFlatRate($nid)) {
          // View by date for pickup.
          $emw = entity_metadata_wrapper('node', $nid);
          $pickup_date = $emw->field_date->value();
          $check_pickup = $date_belong($pickup_date);

          // View by date for delivery.
          $delivery_date = $emw->field_delivery_date->value();
          $check_delivery = $date_belong($delivery_date);

          if ($check_pickup) {
            $select = $query($nid, ContractType::PICKUP_PAYROLL);
            if ($select) {
              $result_nids[] = $nid;
            }
          }
          elseif ($check_delivery) {
            $select = $query($nid, ContractType::DELIVERY_PAYROLL);
            if ($select) {
              $result_nids[] = $nid;
            }
          }
        }
        elseif ($query($nid)) {
          $result_nids[] = $nid;
        }
      }

      $result_nids = array_unique($result_nids);
    }
    else {
      $result_nids = db_select('move_payroll_info', 'mpi')
        ->fields('mpi')
        ->condition('mpi.nid', $nids, 'IN')
        ->condition('mpi.uid', $uid)
        ->condition('mpi.flat_rate', $contract_type)
        ->execute()
        ->fetchCol(0);
    }

    return $result_nids;
  }

  public function fillPickupDelivery($nid) {
    if ($this->isFlatRate($nid)) {
      $request = entity_metadata_wrapper('node', $nid);
      $request_all_data = MoveRequest::getRequestData($nid);
      $data = array(
        'field_foreman_delivery' => array('deliveryCrew' => 'foreman'),
        'field_helper_delivery' => array('deliveryCrew' => 'helpers'),
        'field_foreman_pickup' => array('pickedUpCrew' => 'foreman'),
        'field_helper_pickup' => array('pickedUpCrew' => 'helpers'),
      );

      foreach ($data as $field_name => $value) {
        $type = key($value);
        $name = current($value);
        if (!empty($request_all_data['value']['crews'][$type][$name])) {
          $data = array();
          $name = $request_all_data['value']['crews'][$type][$name];

          // For many Helpers.
          if (is_array($name) && $name) {
            foreach ($name as $item_name) {
              if (!empty($item_name)) {
                $data[] = $item_name;
              }
            }
          }
          // If a one foreman.
          elseif ($name) {
            $data = $name;
          }

          if ($data) {
            $request->{$field_name} = $data;
          }
        }
      }

      $request->save();
    }
  }

  public function eraseDeliveryDateField($nid) {
    if (!$this->isFlatRate($nid)) {
      $entity = entity_load_single('node', $nid);
      // Reset the array to zero-based sequential keys.
      $entity->field_delivery_date = array();

      // Save the entity.
      entity_save('node', $entity);
    }
  }

  private function filterRequestByManager($uid, $nids) {
    $select = db_select('move_request_sales_person', 'mr')
      ->fields('mr')
      ->condition('mr.uid', $uid)
      ->condition('mr.nid', $nids, 'IN')
      ->execute()
      ->fetchAllAssoc('nid');

    return array_keys($select);
  }

  public function updatePayrollCacheByBlockUser(int $uid) {
    $nids = $this->getAllRequestForWorker($uid);

    $execute = function (int $nid, int $contract_type) {
      $date = $this->getDateContract($nid, $contract_type);
      $this->cacheCreatePayrollAll($date);
    };

    foreach ($nids as $nid) {
      if ($this->isFlatRate($nid)) {
        $execute($nid, ContractType::PICKUP_PAYROLL);
        $execute($nid, ContractType::DELIVERY_PAYROLL);
      }
      else {
        $execute($nid, ContractType::PAYROLL);
      }
    }
  }

  /**
   * Return all nids worker's.
   *
   * @param int $uid
   *   The user id.
   *
   * @return array $result
   *   All found nids or empty array.
   */
  private function getAllRequestForWorker(int $uid) {
    $result = array();

    try {
      $user_role = $this->userRole(user_load($uid));
      $manager_roles = array(Roles::MANAGER, Roles::SALES);
      if (in_array($user_role, $manager_roles)) {
        $select = db_select('move_request_sales_person', 'mr')
          ->fields('mr', array('nid'))
          ->condition('mr.uid', $uid)
          ->execute()
          ->fetchAllAssoc('nid');
        $result = array_keys($select);
      }
      else {
        $query = new \EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'move_request')
          ->addTag('payroll_filter_or_operator')
          ->addMetaData('account', user_load(1));
        $query->fieldCondition('field_foreman', 'target_id', $uid);
        $query->fieldCondition('field_helper', 'target_id', $uid);
        $results = $query->execute();
        if (isset($results['node']) && $results['node']) {
          $result = array_keys($results['node']);
        }
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Payroll', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      return $result;
    }
  }

  /**
   * Filtration hourly misc payment.
   *
   * @param int $payee
   *   User who created misc payment.
   * @param array $hourly_misc
   *   Array of hourly misc payment.
   *
   * @return mixed
   *   Ids of filtered misc payments.
   */
  private function filterHourlyMisc(int $payee, array $hourly_misc) {
    $select = db_select('move_hourly_misc_payment', 'mhm')
      ->fields('mhm', array())
      ->condition('mhm.payee', $payee)
      ->condition('mhm.id', array($hourly_misc), 'IN');
    return $select->execute()->fetchAllAssoc('id');
  }

  /**
   * Calculate total data for payroll.
   *
   * @param array $paychecks
   *   Paychecks of user.
   * @param array $jobs
   *   Jobs of user.
   * @param array $hourly_misc
   *   Custom hourly misc of user.
   * @param array $trip_jobs
   *   Long distance jobs.
   *
   * @return array
   *   Calculated data.
   */
  private function calculateTotalUser($paychecks = array(), $jobs = array(), $hourly_misc = array(), $trip_jobs = array()) :array {
    $jobs_calc = array(
      'total' => 0,
      'hours' => 0,
      'grand_total' => 0,
      'hours_pay' => 0,
      'materials' => 0,
      'advanced' => 0,
      'extra' => 0,
      'ins' => 0,
      'tip' => 0,
      'bonus' => 0,
      'misc' => 0,
      'paid' => 0,
    );
    $receipt_calc = 0;

    $job_calc = function (array $job) use (&$jobs_calc) {
      $jobs_calc['total'] += isset($job['total']) ? (float) $job['total'] : 0;
      $jobs_calc['hours'] += isset($job['hours']) ? (float) $job['hours'] : 0;
      $jobs_calc['hours_pay'] += isset($job['hours_pay']) ? (float) $job['hours_pay'] : 0;
      $jobs_calc['grand_total'] += isset($job['grand_total']) ? (float) $job['grand_total'] : 0;
      $jobs_calc['materials'] += isset($job['materials']) ? (float) $job['materials'] : 0;
      $jobs_calc['advanced'] += isset($job['advanced']) ? (float) $job['advanced'] : 0;
      $jobs_calc['extra'] += isset($job['extra']) ? (float) $job['extra'] : 0;
      $jobs_calc['ins'] += isset($job['ins']) ? (float) $job['ins'] : 0;
      $jobs_calc['tip'] += isset($job['tip']) ? (float) $job['tip'] : 0;
      $jobs_calc['bonus'] += isset($job['bonus']) ? (float) $job['bonus'] : 0;
    };

    foreach ($jobs as $nid => $job) {
      $flat_count_jobs = array(1, 2);
      if (in_array(count($job), $flat_count_jobs)) {
        foreach ($job as $item) {
          $job_calc((array) $item);
        }
      }
      else {
        $job_calc((array) $job);
      }
    }

    foreach ($hourly_misc as $misc) {
      $sum_to_paid = 0;
      if ($misc->type) {
        $sum = $misc->amount;
        // This is for "paid".
        if ($misc->amount_options == 1) {
          $sum = ($misc->amount) > 0 ? $misc->amount * -1 : $misc->amount;
          $sum_to_paid = $misc->amount;
        }
        // This is for "deduct".
        if ($misc->amount_options == 2) {
          $sum = ($misc->amount) > 0 ? $misc->amount * -1 : $misc->amount;
        }
      }
      else {
        $sum = ($misc->hours / 60) * ($misc->hourly_rate * 60);
      }

      $jobs_calc['hours'] += (float) $misc->hours;
      $jobs_calc['total'] += (float) $sum;
      $jobs_calc['misc'] += (float) $sum;
      $jobs_calc['paid'] += (float) $sum_to_paid;
    }

    $unique_id = array();
    foreach ($paychecks as $paycheck) {
      if (array_search($paycheck->payment_id, $unique_id) === FALSE) {
        $unique_id[] = $paycheck->payment_id;
        $receipt_calc += $paycheck->amount;
        $jobs_calc['paid'] += $paycheck->amount;
      }
    }

    foreach ($trip_jobs as $trip_id => $trip_job) {
      $jobs_calc['total'] += isset($trip_job['total']) ? (float) $trip_job['total'] : 0;
      $jobs_calc['grand_total'] += isset($trip_job['grand_total']) ? (float) $trip_job['grand_total'] : 0;
      $jobs_calc['hours'] += isset($trip_job['hours']) ? (float) $trip_job['hours'] : 0;
      $jobs_calc['hours_pay'] += isset($trip_job['hours_pay']) ? (float) $trip_job['hours_pay'] : 0;
    }

    $result = array(
      'total' => round(($jobs_calc['total'] - $receipt_calc), 2),
      'hours' => $jobs_calc['hours'],
      'grand_total' => $jobs_calc['grand_total'],
      'hours_pay' => $jobs_calc['hours_pay'],
      'materials' => $jobs_calc['materials'],
      'advanced' => $jobs_calc['advanced'],
      'extra' => $jobs_calc['extra'],
      'ins' => $jobs_calc['ins'],
      'tip' => $jobs_calc['tip'],
      'bonus' => $jobs_calc['bonus'],
      'misc' => $jobs_calc['misc'],
      'paid' => $jobs_calc['paid'],
    );

    return $result;
  }

  /**
   * Return paychecks of user for 'date create' period.
   *
   * @param int $uid
   *   User id.
   *
   * @return mixed
   *   Receipts.
   */
  public function paycheckUserForPeriod(int $uid) {
    $select = db_select('move_paycheck', 'mpp');
    $select->innerJoin('move_paycheck_jobs', 'mppj', 'mpp.id = mppj.payment_id');
    $select->addField('mpp', 'id', 'payment_id');
    $select->addField('mppj', 'id', 'job_id');
    $select->addField('mppj', 'flat_rate', 'contract_type');
    $select->fields('mpp', array(
      'date',
      'pending',
      'form',
      'amount',
      'check_numb',
      'note',
      'uid',
      'created',
    ))
      ->fields('mppj', array('type'))
      ->condition('mpp.uid', $uid)
      ->condition('mpp.date', array($this->dateFrom, $this->dateTo), 'BETWEEN');
    $receipts = $select->execute()->fetchAll();

    return $receipts;
  }

  /**
   * Method to update paycheck.
   *
   * @param int $rid
   *   Paycheck(receipt) id.
   * @param array $data
   *   Data for update.
   *
   * @return bool
   *   Result code of operation.
   */
  public function editPaycheck(int $rid, $data = array()) {
    $result = FALSE;

    $paycheck = $this->getPaycheck($rid);
    if ($paycheck) {
      $update_data = array(
        'date' => (isset($data['date']) && $data['date'] !== '') ? strtotime($data['date']) : $paycheck['date'],
        'pending' => (isset($data['pending']) && $data['pending'] !== '') ? (int) $data['pending'] : $paycheck['pending'],
        'form' => (isset($data['form']) && $data['form'] !== '') ? (int) $data['form'] : $paycheck['form'],
        'amount' => (isset($data['amount']) && $data['amount'] !== '') ? (float) $data['amount'] : $paycheck['amount'],
        'check_numb' => (isset($data['check_numb']) && $data['check_numb'] !== '') ? (int) $data['check_numb'] : $paycheck['check_numb'],
        'note' => (isset($data['note']) && $data['note'] !== '') ? $data['note'] : $paycheck['note'],
        'uid' => (isset($data['uid']) && $data['uid'] !== '') ? $data['uid'] : $paycheck['uid'],
      );

      // Remove paycheck from cache for old paycheck date.
      // Important: remove from cache do need before update in DB.
      if ($paycheck['date'] != date("Y-m-d", $update_data['date'])) {
        $this->updateCachePayrollPaycheck($rid, 0);
      }

      $result = db_merge('move_paycheck')
        ->key(array('id' => $rid))
        ->fields($update_data)
        ->execute();

      $this->updateCachePayrollPaycheck($rid, 1);
    }

    return $result;
  }

  /**
   * Delete paycheck from DB.
   *
   * @param int $rid
   *   Paycheck id.
   *
   * @return bool
   *   Result code of operation.
   */
  public function deletePaycheck(int $rid) {
    $result = FALSE;

    $paycheck = $this->getPaycheck($rid);
    if ($paycheck) {
      $this->updateCachePayrollPaycheck($rid, 0);
      db_delete('move_paycheck')
        ->condition('id', $rid)
        ->execute();
      db_delete('move_paycheck_jobs')
        ->condition('payment_id', $rid)
        ->execute();
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Delete Misc/Payment from DB.
   *
   * @param int $rid
   *   Record id.
   *
   * @return bool|\DatabaseStatementInterface
   *   Result code of operation.
   */
  public function deleteMiscPayment(int $rid, $date) {
    $result = FALSE;

    $misc = $this->getHourlyMiscById($rid);
    if ($misc) {
      $result = db_delete('move_hourly_misc_payment')
        ->condition('id', $rid)
        ->execute();
      $date = $this->cacheDeletePayrollHourlyMisc($rid, $misc);
    }

    $this->tryRemoveMiscPaymentFromCache($date, $rid);

    return $result;
  }

  // TODO this is quick fix. Потому что криво апдейтился кеш для миск пеймента. (Когда менялась дата то старый миск на старой дате сохранялся и новый миск добавлялся на новую дату)
  function tryRemoveMiscPaymentFromCache($date, $rid) {
    if (isset($date)) {
      $date = date('Y-m-d', $date);
      $uid = $this->searchUserIdContainsMiscPaymentInCache($rid, $date);

      if ($uid) {
        $this->updateCachePayrollHourlyMisc($rid, $date, $uid, 0);
      }
    }
  }

  function searchUserIdContainsMiscPaymentInCache($rid, $date) {
    $result = FALSE;

    $cache = $this->getPayrolDayCache($date);

    if (!$cache) {
      $this->cacheCreatePayrollAll($date);
      // Override cache variable again.
      $cache = $this->getPayrolDayCache($date);
    }

    $by_user = unserialize($cache->cache_by_user);

    $user_uid = FALSE;
    $is_found_misc = FALSE;

    foreach ($by_user as $uid => $value) {
      if (isset($value['hourly_misc']) && $value['hourly_misc']) {
        $user_uid = $uid;
        if (isset($value['hourly_misc'][$rid])) {
          $is_found_misc = TRUE;
          break;
        }
      }
    }

    if ($user_uid && $is_found_misc) {
      $result = $user_uid;
    }

    return $result;
  }

  /**
   * Helper method for get paycheck by id.
   *
   * @param int $rid
   *   Paycheck(receipt) id.
   *
   * @return mixed
   *   Result code of operation.
   */
  private function getPaycheck(int $rid) {
    return db_select('move_paycheck', 'mpp')
      ->fields('mpp')
      ->condition('mpp.id', $rid)
      ->execute()->fetchAssoc();
  }

  private function getPaycheckAll(int $rid) {
    $select = db_select('move_paycheck', 'mpp');
    $select->innerJoin('move_paycheck_jobs', 'mppj', 'mpp.id = mppj.payment_id');
    $select->addField('mpp', 'id', 'payment_id');
    $select->addField('mppj', 'id', 'job_id');
    $select->addField('mppj', 'flat_rate', 'contract_type');
    $select->fields('mpp', array(
      'date',
      'pending',
      'form',
      'amount',
      'check_numb',
      'note',
      'uid',
      'created',
    ))
      ->fields('mppj', array('type'))
      ->condition('mpp.id', $rid);
    return $select->execute()->fetchAll();
  }

  /**
   * Returned paycheck count for created date period.
   *
   * @param int $date_from
   *   Date FROM period.
   * @param int $date_to
   *   Date TO period.
   *
   * @return float
   *   Count paychecks.
   */
  public function getCountPaycheckForPeriod(int $date_from, int $date_to) {
    $convert_from = $date_from;
    $convert_to = $date_to;
    $query = db_select('move_paycheck', 'mpp')
      ->condition('mpp.date', array($convert_from, $convert_to), 'BETWEEN');
    $query->addExpression('SUM(mpp.amount)');
    $result = $query->execute()->fetchField();

    return round($result, 2);
  }

  /**
   * Returned all id requests for period 'move date'.
   *
   * @return array|bool
   *   array ids of requests or FALSE otherwise.
   */
  private function getAllRequestsForPeriod() {
    $result = &drupal_static(__METHOD__ . ":$this->dateFrom:$this->dateTo", array());
    if (!$result) {
      $comp_flag = RequestCompanyFlags::COMPLETED;

      $result = db_query("
      SELECT mr.item_id AS item_id
      FROM {search_api_db_move_request} mr
      INNER JOIN {search_api_db_move_request_field_company_flags} fcf ON fcf.item_id = mr.item_id
      WHERE  (fcf.value = {$comp_flag}) 
      AND (mr.field_date BETWEEN {$this->dateFrom} 
      AND {$this->dateTo}) 
      AND (mr.field_move_service_type <> '3') 
    ")->fetchCol();
    }

    return $result;
  }

  /**
   * Return id of 'completed' flag.
   *
   * @return bool|mixed
   *   Id of flag or FALSE otherwise.
   */
  public static function getCompletedFlag() {
    $have_flag = FALSE;
    $id = FALSE;
    $flags = MoveRequest::getAllowedValueFlag();
    if (isset($flags['company_flags'])) {
      $id = array_search("Completed", $flags['company_flags'], TRUE);
      if ($id !== FALSE) {
        $have_flag = TRUE;
      }
    }

    // If flag "Completed" does not exist.
    if (!$have_flag) {
      watchdog("Payroll", "Completed flag does not exist!", array(), WATCHDOG_WARNING);
    }

    return $id;
  }

  /**
   * Forcibly set company flag to "Completed".
   *
   * @param int $nid
   *   Move request id.
   */
  public static function setCompletedFlag(int $nid) {
    $blocked = 0;
    try {
      if (!self::checkNodeExistCompletedFlag($nid)) {
        MoveRequest::setBlocker($nid);
        $blocked = 1;
        $complete_flag = RequestCompanyFlags::COMPLETED;

        $emw = entity_metadata_wrapper('node', $nid);
        $company_flag = $emw->field_company_flags->raw();
        $company_flag[] = $complete_flag;
        $emw->field_company_flags->set($company_flag);
        // TODO Использовать enum.
        if (in_array(5, $company_flag) && in_array(6, $company_flag)) {
          $emw->field_completed_date->set(time());
        }
        $emw->save();
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('setCompletedFlag', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      if ($blocked) {
        MoveRequest::removeBlocker($nid);
      }
    }
  }

  /**
   * Check exist completed flag for move request.
   *
   * @param int $nid
   *   Move request id.
   *
   * @return int
   *   If exist 1, 0 if not exist.
   */
  public static function checkNodeExistCompletedFlag(int $nid) {
    $comp_flag = RequestCompanyFlags::COMPLETED;
    return db_query("
      SELECT 1 
      FROM {search_api_db_move_request_field_company_flags} fcf 
      WHERE (fcf.item_id = {$nid})
      AND (fcf.value = {$comp_flag})
      ")
      ->rowCount();
  }

  /**
   * Unserialized contract.
   *
   * @param int $nid
   *   Request(node) id.
   * @param int $contract_type
   *   The contract type.
   *
   * @return array
   *   Contract information or empty array otherwise.
   */
  private function getContract(int $nid, int $contract_type = ContractType::PAYROLL) : array {
    $result = array();
    $contract = db_select('move_contract_info', 'mc')
      ->fields('mc')
      ->condition('mc.nid', $nid)
      ->execute()
      ->fetchCol(1);

    if ($contract) {
      switch ($contract_type) {
        case ContractType::PAYROLL:
        case ContractType::PICKUP_PAYROLL:
          $result = unserialize(reset($contract));
          break;

        case ContractType::DELIVERY_PAYROLL:
          $result = unserialize(end($contract));
          break;
      }
    }

    return $result;
  }

  /**
   * Array of worker rates.
   *
   * @param int $uid
   *   User id.
   *
   * @return array
   *   Worker rates.
   */
  private function rateWorkUser(int $uid) {
    $rate = array();
    $settings = Users::getUserSettings($uid);
    $all_rates = $this->allWorkerRates();

    if (isset($settings['rateCommission'])) {
      foreach ($settings['rateCommission'] as $setting) {
        $flag = FALSE;

        switch ($setting['option']) {
          case 'Hourly Rate':
            $flag = RateCommission::HOURLYRATE;
            break;

          case 'Advanced Service':
            $flag = RateCommission::ADVANCEDSERVICE;
            break;

          case 'Extras Commission':
            $flag = RateCommission::EXTRASCOMMISSION;
            break;

          case 'Daily Rate':
            $flag = RateCommission::DAILYRATE;
            break;

          case 'Commission from total':
            $flag = RateCommission::COMMISSIONFROMTOTAL;
            break;

          case 'Insurance':
            $flag = RateCommission::INSURANCE;
            break;

          case 'Loading per ou.ft':
            $flag = RateCommission::LOADINGPEROUFT;
            break;

          case 'Packing Commission':
            $flag = RateCommission::MATERIALSCOMMISSION;
            break;

          case 'Unloading per ou.ft':
            $flag = RateCommission::UNLOADINGPEROUFT;
            break;

          case 'Hourly Rate (as helper)':
            $flag = RateCommission::HOURLYRATEHELPER;
            break;

          case 'Bonus':
            $flag = RateCommission::BONUS;
            break;
        }

        if ($flag !== FALSE) {
          $rate[$flag] = array(
            'rate' => $setting['input'],
            'editable' => $all_rates[$flag]['editable'],
            'type' => $all_rates[$flag]['type'],
          );
        }
      }
    }

    return $rate;
  }

  /**
   * Array worker's rate for view table.
   *
   * @return array
   *   Array of rates.
   */
  private function allWorkerRates() {
    return array(
      RateCommission::HOURLYRATE => array(
        'editable' => TRUE,
        'type' => TypeCommission::CACHE,
      ),
      RateCommission::ADVANCEDSERVICE => array(
        'editable' => FALSE,
        'type' => TypeCommission::PERCENTAGE,
      ),
      RateCommission::EXTRASCOMMISSION => array(
        'editable' => FALSE,
        'type' => TypeCommission::PERCENTAGE,
      ),
      RateCommission::DAILYRATE => array(
        'editable' => FALSE,
        'type' => TypeCommission::PERCENTAGE,
      ),
      RateCommission::COMMISSIONFROMTOTAL => array(
        'editable' => FALSE,
        'type' => TypeCommission::PERCENTAGE,
      ),
      RateCommission::INSURANCE => array(
        'editable' => FALSE,
        'type' => TypeCommission::PERCENTAGE,
      ),
      RateCommission::LOADINGPEROUFT => array(
        'editable' => TRUE,
        'type' => TypeCommission::CACHE,
      ),
      RateCommission::MATERIALSCOMMISSION => array(
        'editable' => FALSE,
        'type' => TypeCommission::PERCENTAGE,
      ),
      RateCommission::UNLOADINGPEROUFT => array(
        'editable' => FALSE,
        'type' => TypeCommission::PERCENTAGE,
      ),
      RateCommission::HOURLYRATEHELPER => array(
        'editable' => TRUE,
        'type' => TypeCommission::CACHE,
      ),
      RateCommission::BONUS => array(
        'editable' => FALSE,
        'type' => TypeCommission::CACHE,
      ),
      RateCommission::TIPS => array(
        'editable' => TRUE,
        'type' => TypeCommission::CACHE,
      ),
    );
  }

  /**
   * Rate manager's from user settings.
   *
   * @param int $uid
   *   User id.
   *
   * @return array
   *   Array of rates.
   */
  private function rateManager($uid) {
    $rate = array();
    $settings = Users::getUserSettings($uid);
    $all_rates = $this->allManagerRates();

    if (isset($settings['rateCommission'])) {
      $settings = array_slice($settings['rateCommission'], 0, 6);
      foreach ($settings as $setting) {
        $flag = FALSE;

        switch ($setting['option']) {
          case 'Office Commission':
            $flag = ManagerRateCommission::OFFICECOMMISSION;
            break;

          case 'Office With visual':
            $flag = ManagerRateCommission::OFFICEWITHVISUAL;
            break;

          case 'Visual Estimate':
            $flag = ManagerRateCommission::VISUALESTIMATE;
            break;
        }

        if ($flag !== FALSE) {
          $input = !empty($setting['input']) ? $setting['input'] : 0;
          $editable = !empty($all_rates[$flag]['editable']) ? $all_rates[$flag]['editable'] : FALSE;
          $type = !empty($all_rates[$flag]['type']) ? $all_rates[$flag]['type'] : '';

          $rate[] = array(
            'flag' => $flag,
            'rate' => $input,
            'editable' => $editable,
            'type' => $type,
          );
        }
      }
    }

    return $rate;
  }

  /**
   * Flags for exclude from calculation manager rates.
   *
   * @param int $uid
   *   User id.
   *
   * @return array
   *   Excluded flags.
   */
  private function excludeRateUser(int $uid) : array {
    $exclude = array();
    $settings = Users::getUserSettings($uid);
    $exclude['fuel'] = (!empty($settings['exclude_fuel'])) ? TRUE : FALSE;
    $exclude['valuation'] = (!empty($settings['exclude_valuation'])) ? TRUE : FALSE;
    $exclude['packing'] = (!empty($settings['exclude_packing'])) ? TRUE : FALSE;
    $exclude['additional'] = (!empty($settings['exclude_additional'])) ? TRUE : FALSE;
    // Additional Services.
    $exclude['additional_services'] = (!empty($settings['excluded_additional_services'])) ? $settings['excluded_additional_services'] : array();
    return $exclude;
  }

  private function getRequestAddServices($nid) {
    $all_data = MoveRequest::getRequestAllData($nid);
    $extra = array();

    if (!empty($all_data['invoice']['extraServices'])) {
      if (is_array($all_data['invoice']['extraServices'])) {
        $extra = $all_data['invoice']['extraServices'];
      }
      else {
        $extra = json_decode((string) $all_data['invoice']['extraServices']);
      }
    }

    return $extra;
  }

  /**
   * Array manager's rate for view table.
   *
   * @return array
   *   Array of rates.
   */
  private function allManagerRates() {
    return array(
      ManagerRateCommission::OFFICECOMMISSION => array(
        'editable' => TRUE,
        'type' => TypeCommission::PERCENTAGE,
      ),
      ManagerRateCommission::OFFICEWITHVISUAL => array(
        'editable' => TRUE,
        'type' => TypeCommission::PERCENTAGE,
      ),
      ManagerRateCommission::VISUALESTIMATE => array(
        'editable' => TRUE,
        'type' => TypeCommission::PERCENTAGE,
      ),
    );
  }

  /**
   * Return move date for request.
   *
   * @param int $nid
   *   Node id.
   *
   * @return mixed
   *   Move date.
   */
  public static function getMoveDate(int $nid) : int {
    return db_select('search_api_db_move_request', 'mr')
      ->fields('mr', array('field_date'))
      ->condition('mr.nid', $nid)
      ->execute()
      ->fetchField();
  }

  /**
   * Return field moving from administrative area.
   *
   * @param int $nid
   *   Node id.
   *
   * @return mixed
   *   Administrative area.
   */
  public static function getMovingFromAdmArea(int $nid) {
    return db_select('field_data_field_moving_from', 'mf')
      ->fields('mf', array('field_moving_from_administrative_area'))
      ->condition('mf.entity_id', $nid)
      ->execute()
      ->fetchField();
  }

  /**
   * Return field moving to administrative area.
   *
   * @param int $nid
   *   Node id.
   *
   * @return mixed
   *   Move date.
   */
  public static function getMovingToAdmArea(int $nid) {
    return db_select('field_data_field_moving_to', 'mt')
      ->fields('mt', array('field_moving_to_administrative_area'))
      ->condition('mt.entity_id', $nid)
      ->execute()
      ->fetchField();
  }

  /**
   * Calculate tips for worker.
   *
   * @param int $uid
   *   User id.
   * @param int $nid
   *   Request(node) id.
   * @param array $contract_info
   *   Array with Contract.
   *
   * @return float $tips
   *   Tips worker's.
   */
  private function getWorkerTip(int $uid, int $nid, array $contract_info) {
    $tips = 0;
    if (!isset($contract_info['tips_total']) || !$contract_info['tips_total']) {
      return $tips;
    }

    // Find Switcher and Additional Workers.
    $switch = $this->findSwitchers($contract_info);
    $additional = $this->findSwitchers($contract_info, 'additional');

    // Equally.
    if (!count($switch) && !count($additional)) {
      $workers = $this->getWorkersRequest($nid, $contract_info);
      $tips = $this->equallyWorkersTips($workers, $contract_info['tips_total']);
    }
    else {
      $tips = $this->getWorkersTips($uid, $contract_info);
    }

    return round($tips, 2);
  }

  /**
   * Calculate tips.
   *
   * @param array $workers
   *   Array with workers.
   * @param int $all_tip
   *   Tips for all workers.
   *
   * @return float
   *   Tips for worker.
   */
  private function equallyWorkersTips($workers, $all_tip) : float {
    return round($all_tip / (count($workers)), 2);
  }

  /**
   * Get worker tips.
   *
   * @param array $commissions
   *   Array of commissions.
   *
   * @return float
   *   Tips.
   */
  private function getWorkerHandTip($commissions = array()) : float {
    $tips = 0;
    if (isset($commissions[RateCommission::TIPS])) {
      $tips = $commissions[RateCommission::TIPS]['total'];
    }

    return (float) $tips;
  }

  private function getWorkerBonus($commissions = array()) {
    $bonus = 0;
    if (isset($commissions[RateCommission::BONUS])) {
      $bonus = $commissions[RateCommission::BONUS]['total'];
    }

    return (float) $bonus;
  }

  /**
   * Calculate tips for worker.
   *
   * @param int $uid
   *   User id.
   * @param array $contract_info
   *   Array info about contract.
   *
   * @return float
   *   Calculated tips for worker.
   */
  private function getWorkersTips(int $uid, array $contract_info) {
    $tips_total = (float) $contract_info['tips_total'];
    $workers_hours = $this->getWorkersHours($contract_info);
    $count_worker_hours = array_sum($workers_hours);
    $tips_hour = $tips_total / $count_worker_hours;
    $workers = $this->getWorkersContract($contract_info);
    $worker = $this->findWorkerContract($uid, $workers);
    return round(($tips_hour / 60) * ($worker['hours_to_pay'] * 60), 2);
  }

  private function getWorkersHours(array $contract_info) {
    $result = array();
    $finder = array('foreman', 'helper');
    foreach ($finder as $role) {
      if (isset($contract_info[$role])) {
        foreach ($contract_info[$role] as $uid => $worker) {
          $result[$uid] = $worker['hours_to_pay'];
        }
      }
    }

    return $result;
  }

  /**
   * Get workers for request.
   *
   * @param array $contract_info
   *   Information about contract.
   *
   * @return array
   *   Worker's array.
   */
  private function getWorkersRequest(int $nid, array $contract_info) : array {
    $result = array();
    $workers_contract = $this->getWorkersContract($contract_info);
    if ($workers_contract) {
      foreach ($workers_contract as $role_name => $role) {
        foreach ($role as $uid => $user) {
          $result[$uid] = array('fields' => user_load($uid), 'role' => $role_name);
        }
      }
    }
    else {
      $node_wrapper = entity_metadata_wrapper('node', $nid);
      $foremans = $node_wrapper->field_foreman->value();
      $helpers = $node_wrapper->field_helper->value();
      foreach ($foremans as $foreman) {
        $result[$foreman->uid] = array('fields' => $foreman, 'role' => 'foreman');
      }

      foreach ($helpers as $helper) {
        $result[$helper->uid] = array('fields' => $helper, 'role' => 'helper');
      }
    }


    return $result;
  }

  private function getWorkersFlatRateRequest(int $nid, int $contract_type = ContractType::PICKUP_PAYROLL) : array {
    $result = array();
    $extra_service = MoveRequest::getRequestData($nid);
    if (isset($extra_service['value']) && $extra_service['value']) {
      $decode = $extra_service['value'];
      if (isset($decode['crews'])) {
        $field_name = ($contract_type == ContractType::DELIVERY_PAYROLL) ? 'deliveryCrew' : 'pickedUpCrew';
        $crew = isset($decode['crews'][$field_name]) ? $decode['crews'][$field_name] : array();
        $foreman_uid = isset($crew['foreman']) ? (int) $crew['foreman'] : NULL;
        $helpers = isset($crew['helpers']) ? $crew['helpers'] : [];

        if ($foreman_uid) {
          $result[$foreman_uid] = array('fields' => user_load($foreman_uid), 'role' => 'foreman');
        }
        foreach ($helpers as $helper_uid) {
          $result[$helper_uid] = array('fields' => user_load($helper_uid), 'role' => 'helper');
        }
      }
    }

    return $result;
  }

  private function findSwitchers($contract_info, $type = 'switcher') {
    $switchers = array();
    $workers = $this->getWorkersContract($contract_info);
    foreach ($workers as $worker) {
      foreach ($worker as $uid => $user) {
        if (isset($user[$type]) && $user[$type]) {
          $switchers[] = $uid;
        }
      }
    }

    return $switchers;
  }

  private function getWorkersContract(array $contract_info) : array {
    $workers = array();
    if (isset($contract_info['foreman'])) {
      $workers['foreman'] = $contract_info['foreman'];
    }
    if (isset($contract_info['helper'])) {
      $workers['helper'] = $contract_info['helper'];
    }

    return $workers;
  }

  private function findWorkerContract($uid, $workers) {
    $result = array();
    $finder = array('foreman', 'helper');
    foreach ($finder as $role) {
      if (isset($workers[$role])) {
        if (array_key_exists($uid, $workers[$role])) {
          $result = $workers[$role][$uid];
        }
      }
    }

    return $result;
  }

  public function getUsersByRole(int $real_role) {
    $result = array();
    $role_name = $this->getRoleName($real_role);
    $real_role = user_role_load_by_name($role_name);
    if ($real_role) {
      $select = db_select('users_roles', 'u')
        ->fields('u', array('uid'))
        ->condition('u.rid', $real_role->rid);
      $result = $select->execute()->fetchCol();
      // Checking what user is active.
      foreach ($result as $key => $uid) {
        if (!Users::checkUserIsActive($uid)) {
          unset($result[$key]);
        }
      }
    }

    return $result;
  }

  private function getRoleName($role) {
    $role_name = '';

    switch ($role) {
      case Roles::FOREMAN:
        $role_name = 'foreman';
        break;

      case Roles::HELPER:
        $role_name = 'helper';
        break;

      case Roles::MANAGER:
        $role_name = 'manager';
        break;

      case Roles::DRIVER:
        $role_name = 'driver';
        break;

      case Roles::SALES:
        $role_name = 'sales';
        break;

      case Roles::OWNER:
        $role_name = 'owner';
        break;

      case Roles::CUSTOMER_SERVICE:
        $role_name = 'customer_service';
        break;
    }

    return $role_name;
  }

  /**
   * Saving contract information to DB.
   *
   * @param int $nid
   *   Node id.
   * @param array $value
   *   Contract data.
   * @param int $contract_type
   *   The type of contract.
   *
   * @return bool
   *   Result: Set contract info or false.
   *
   * @throws \Exception
   */
  public function setContractInfo(int $nid, array $value, int $contract_type = ContractType::PAYROLL) {
    $data = array(
      'nid' => $nid,
      'value' => $value,
      'contract_type' => $contract_type
    );
    (new DelayLaunch())->createTask(TaskType::PAYROLL_CONTRACT_INFO, $data);
    return TRUE;
  }

  /**
   * Get move request service type.
   *
   * @param int $nid
   *   Node id.
   *
   * @return int
   *   Move request service type.
   */
  public static function getRequestServiceType(int $nid) {
    return db_select('field_data_field_move_service_type', 'fdfmst')
      ->fields('fdfmst', array('field_move_service_type_value'))
      ->condition('entity_id', $nid)
      ->execute()
      ->fetchField();
  }

  /**
   * Get worker's contract.
   *
   * @param int $nid
   *   Node id.
   *
   * @return array
   *   Ready to view worker's contract.
   */
  public static function getContractInfo(int $nid) : array {
    $result = array();
    $contracts = db_select('move_contract_info', 'mc')
      ->fields('mc', array('value'))
      ->condition('mc.nid', $nid, '=')
      ->execute()
      ->fetchAll();

    foreach ($contracts as $contract) {
      if ($contract->value) {
        $result[] = unserialize($contract->value);
      }
    }

    return $result;
  }

  /**
   * Create payroll.
   *
   * @param int $nid
   *   Node id.
   * @param array $data
   *   Workers payroll.
   * @param int $contract_type
   *   The type of contract.
   *
   * @throws \Exception
   */
  public function setPayrollInfo(int $nid, array $data = array(), int $contract_type = ContractType::PAYROLL) : void {
    $save_wrapper = FALSE;

    // @TODO validate $contract_type
    foreach ($data as $worker) {
      if (isset($worker['uid']) && isset($worker['value'])) {
        $uid = (int) $worker['uid'];

        if (isset($worker['new']) && $worker['new']) {
          try {
            // This contract type is default. So we need to set it always.
            $field = $this->workerRequestField($uid, ContractType::PAYROLL);

            if ($field == 'manager') {
              $this->addManager($nid, $uid);
            }
            else {
              $node_wrapper = entity_metadata_wrapper('node', $nid);
              $node_wrapper = $this->setPayRollUserFields($node_wrapper, $uid, $field);

              if ($contract_type == ContractType::PICKUP_PAYROLL || $contract_type == ContractType::DELIVERY_PAYROLL) {
                $field = $this->workerRequestField($uid, $contract_type);
                $node_wrapper = $this->setPayRollUserFields($node_wrapper, $uid, $field);
              }
            }

            // Flag to check in need to save node wrapper.
            $save_wrapper = TRUE;
          }
          catch (\Throwable $e) {
            $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
            watchdog('Payroll', $message, array(), WATCHDOG_ERROR);
          }
        }

        try {
          $write = array(
            'nid' => $nid,
            'uid' => $uid,
            'value' => serialize($worker['value']),
            'flat_rate' => $contract_type,
          );

          db_merge('move_payroll_info')
            ->key(array(
              'nid' => $nid,
              'uid' => $uid,
              'flat_rate' => $contract_type,
            ))
            ->fields($write)
            ->execute();

          // Save user fields to request.
          if ($save_wrapper && isset($node_wrapper)) {
            $node_wrapper->save();
          }

          $this->cacheUpdatePayrollRequest($nid, $contract_type);
        }
        catch (\Throwable $e) {
          $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
          watchdog('Set Payroll Info', $message, array(), WATCHDOG_ERROR);
        }
      }
    }
  }

  /**
   * Add users to drupal fields for payroll.
   *
   * @param \EntityMetadataWrapper $node_wrapper
   *   Node object.
   * @param int $uid
   *   User id.
   * @param string $field
   *   Field to update.
   *
   * @return mixed
   *   Node object.
   */
  private function setPayRollUserFields(\EntityMetadataWrapper $node_wrapper, int $uid, string $field) : \EntityMetadataWrapper {
    $users = $node_wrapper->{$field}->raw();
    $count_u = count($users);
    $node_wrapper->{$field}[++$count_u]->set($uid);
    return $node_wrapper;
  }

  /**
   * Remove users from drupal fields for payroll.
   *
   * @param \EntityMetadataWrapper $node_wrapper
   *   Node object.
   * @param int $uid
   *   User id.
   * @param string $field
   *   Field to update.
   *
   * @return mixed
   *   Node object.
   */
  private function unsetPayRollUserFields(\EntityMetadataWrapper $node_wrapper, int $uid, string $field) : \EntityMetadataWrapper {
    foreach ($node_wrapper->{$field} as $delta => $item) {
      if ($item->raw() == $uid) {
        unset($node_wrapper->{$field}[$delta]);
      }
    }
    return $node_wrapper;
  }

  /**
   * Update users from drupal fields for payroll.
   *
   * Not save it.
   *
   * @param \EntityMetadataWrapper $node_wrapper
   *   Node object
   * @param int $uid
   *   User id.
   * @param string $field
   *   Field to update.
   * @return mixed
   *  Node object
   */
  private function updatePayRollUserFields(\EntityMetadataWrapper $node_wrapper, int $new_uid, int $old_uid,  string $field) : \EntityMetadataWrapper {
    $users = $node_wrapper->{$field}->raw();
    $key = $this->searchUserInField($old_uid, $users);
    $node_wrapper->{$field}[$key]->set($new_uid);
    return $node_wrapper;
  }

  public function contractDate($nid, int $contract_type = ContractType::PAYROLL) : string {
    if ($contract_type == ContractType::LONGDISTANCE) {
      $payroll_inst = new LongDistancePayroll($nid);
      $payroll = $payroll_inst->retrieve();
      $date = date("Y-m-d", $payroll['details']['date_end']);
    }
    else {
      $node_wrapper = entity_metadata_wrapper('node', $nid);
      $field = 'field_date';

      if (($this->isFlatRate($nid) && $contract_type === ContractType::DELIVERY_PAYROLL)) {
        $field = 'field_delivery_date';
      }
      $date = date("Y-m-d", $node_wrapper->{$field}->raw());
    }

    return $date;
  }

  /**
   * Update payroll user's to another user.
   *
   * @param int $nid
   *   Request (node) id.
   * @param int $old_uid
   *   Old uid user payroll's.
   * @param int $new_uid
   *   New uid user payroll's.
   * @param int $contract_type
   *   Contract type.
   * @return bool|int
   *   result of operation.
   */
  public function updateUserPayroll(int $nid, int $old_uid, int $new_uid, int $contract_type = ContractType::PAYROLL) {
    $result = FALSE;

    try {
      $field = $this->workerRequestField($new_uid, ContractType::PAYROLL);
      if ($field == 'manager') {
        $this->managerRequest($nid, $new_uid);
        $result = $this->writeChangeUserPayroll($nid, $old_uid, $new_uid);
      }
      else {
        $node_wrapper = entity_metadata_wrapper('node', $nid);
        $node_wrapper = $this->updatePayRollUserFields($node_wrapper, $new_uid, $old_uid, $field);

        if ($contract_type == ContractType::PICKUP_PAYROLL || $contract_type == ContractType::DELIVERY_PAYROLL) {
          $field = $this->workerRequestField($new_uid, $contract_type);
          $node_wrapper = $this->updatePayRollUserFields($node_wrapper, $new_uid, $old_uid, $field);
        }

        $node_wrapper->save();
        $result = $this->writeChangeUserPayroll($nid, $old_uid, $new_uid);
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Payroll', $message, array(), WATCHDOG_ERROR);
    }

    return $result;
  }

  /**
   * Remove payroll worker's for request.
   *
   * @param int $nid
   *   Node id.
   * @param array $uids
   *   User id.
   * @param int $contract_type
   *   The type of contract.
   *
   * @return array
   *   Result of operation.
   */
  public function removeUserPayroll(int $nid, array $uids, int $contract_type = ContractType::PAYROLL) : array {
    $result = array();
    $save_wrapper = FALSE;
    foreach ($uids as $uid) {
      if ($contract_type == ContractType::LONGDISTANCE) {
        LongDistancePayroll::changeTripPayrollStatus($nid, 0);
      }
      else {
        // This contract type is default. So we need to set it always.
        $field = $this->workerRequestField($uid, ContractType::PAYROLL);
        if ($field != 'manager') {
          $node_wrapper = entity_metadata_wrapper('node', $nid);
          if ($this->detectHourlyRateAsHelper($nid, $uid, $contract_type)) {
            $field = 'field_helper';
          }

          $node_wrapper = $this->unsetPayRollUserFields($node_wrapper, $uid, $field);

          if ($contract_type == ContractType::PICKUP_PAYROLL || $contract_type == ContractType::DELIVERY_PAYROLL) {
            $field = $this->workerRequestField($uid, $contract_type);
            $node_wrapper = $this->unsetPayRollUserFields($node_wrapper, $uid, $field);
          }

          $save_wrapper = TRUE;
        }
        else {
          MoveRequest::deleteManagerFromRequestById($uid, $nid);
        }
      }

      if ($contract_type == ContractType::LONGDISTANCE) {
        $result[$uid] = 1;
      }
      else {
        $result[$uid] = db_delete('move_payroll_info')
          ->condition('nid', $nid)
          ->condition('uid', $uid)
          ->condition('flat_rate', $contract_type)
          ->execute();
      }

      if ($save_wrapper && isset($node_wrapper)) {
        $node_wrapper->save();
      }

      $this->cacheDeleteUserPayroll($nid, $uid, $contract_type);
    }

    return $result;
  }

  private function searchUserInField($old_uid, $users) {
    $key = array_search($old_uid, $users);
    if ($key === FALSE) {
      throw new PayrollFindUserField("User not found in request.");
    }

    return $key;
  }

  private function addManager(int $nid, int $uid) {
    // Check that the assigned manager.
    $assign_user = MoveRequest::getRequestManager($nid);
    if (!$assign_user) {
      // Assign manager.
      $this->managerRequest($nid, $uid, TRUE);
    }
    elseif ($assign_user != $uid) {
      // Additional manager.
      $this->managerRequest($nid, $uid, FALSE);
    }
  }

  private function managerRequest($nid, $uid, bool $new = TRUE) {
    (new Sales($nid))->assingManager($uid, $new, FALSE, FALSE);
  }

  private function writeChangeUserPayroll($nid, $old_uid, $new_uid) {
    $result = FALSE;
    try {
      $result = db_merge('move_payroll_info')
        ->key(array('nid' => $nid, 'uid' => $old_uid))
        ->fields(array('uid' => $new_uid))
        ->execute();
      $this->cacheDeleteUserPayroll($nid, $old_uid);
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('writeChangeUserPayroll', $message, array(), WATCHDOG_ERROR);
      throw new PayrollChangeUser("Change worker for payroll is wrong.");
    }
    finally {
      return $result;
    }
  }

  // Can write twice, check at Flat Rate
  private function workerRequestField($uid, int $contract_type = ContractType::PAYROLL) : string {
    $all_fields = array(
      Roles::DRIVER => 'field_foreman',
      Roles::MANAGER => 'manager',
      Roles::SALES => 'manager',
    );

    $fields = [
      ContractType::PAYROLL => [
        Roles::HELPER => 'field_helper',
        Roles::FOREMAN => 'field_foreman',
      ] + $all_fields,
      ContractType::PICKUP_PAYROLL => [
        Roles::HELPER => 'field_helper_pickup',
        Roles::FOREMAN => 'field_foreman_pickup',
      ] + $all_fields ,
      ContractType::DELIVERY_PAYROLL => [
        Roles::HELPER => 'field_helper_delivery',
        Roles::FOREMAN => 'field_foreman_delivery',
      ] + $all_fields,
    ];

    $rid = $this->userRole(user_load($uid));
    $field = $fields[$contract_type][$rid];

    if (!$field) {
      throw new PayrollWorkerRequestsField("User have not necessary role.");
    }
    return $field ?? '';
  }

  /**
   * Create new Hourly/Misc.
   *
   * @param array $data
   *    Records for Hourly/Misc.
   *
   * @return bool|\DatabaseStatementInterface|int
   *   Result of operation.
   */
  public function setHourlyMiscPayment($data = array()) {
    $result = FALSE;

    try {
      $this->checkPaymentType($data['type']);
      $this->checkHourlyMiscPaymentDate($data['date']);
      // @todo Create check Payee user.
      $fields = array(
        'type' => (int) $data['type'],
        'payee' => (int) $data['payee'],
        'note' => (string) $data['note'],
        'date' => strtotime($data['date']),
      );

      if ($data['type'] == TypePayment::AMOUNT) {
        $this->checkHourlyMiscAmount($data);
        $this->checkMiscAmountOption($data['amount']['amount_options']);
        $fields['amount'] = (float) $data['amount']['sum'];
        $fields['amount_options'] = (int) $data['amount']['amount_options'];
      }
      elseif ($data['type'] == TypePayment::HOURLYRATE) {
        $this->checkHourlyMiscHourly($data);
        $fields['hourly_rate'] = (float) $data['hourly']['hourly_rate'];
        $fields['hours'] = floatval($data['hourly']['hours']);
      }

      $result = db_insert('move_hourly_misc_payment')
        ->fields($fields)
        ->execute();

      if ($result) {
        $this->cacheCreatePayrollHourlyMisc($result);
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Payroll', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      return $result;
    }
  }

  private function checkPaymentType($type) {
    if (!TypePayment::isValidValue($type)) {
      throw new PayrollPaymentType();
    }
  }

  private function checkMiscAmountOption($option) {
    if (!AmountOption::isValidValue($option)) {
      throw new PayrollMiscAmountOption();
    }
  }

  private function checkHourlyMiscPaymentDate($date) {
    if (!strtotime($date)) {
      throw new PayrollMiscPaymentDate();
    }
  }

  private function checkHourlyMiscHourly(array $data) {
    if (!isset($data['hourly']['hourly_rate']) || !isset($data['hourly']['hours'])) {
      throw new PayrollException();
    }
  }

  private function checkHourlyMiscAmount(array $data) {
    if (!isset($data['amount']['sum']) || !isset($data['amount']['amount_options'])) {
      throw new PayrollException();
    }
  }

  /**
   * Edit Hourly/Misc by id.
   *
   * @param int $id
   *   Record id.
   * @param array $data
   *   Data for updated.
   *
   * @return bool|int
   *   Result code of operation.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function editHourlyMisc(int $id, array $data) {
    $result = FALSE;

    $misc = $this->getHourlyMiscById($id);
    if ($data) {
      $update_data = array(
        'type' => (isset($data['type']) && $data['type'] !== '') ? (int) $data['type'] : $misc['type'],
        'hourly_rate' => (isset($data['hourly_rate']) && $data['hourly_rate'] !== '') ? (float) $data['hourly_rate'] : 0,
        'hours' => (isset($data['hours']) && $data['hours'] !== '') ? (float) $data['hours'] : 0,
        'amount' => (isset($data['amount']) && $data['amount'] !== '') ? (float) $data['amount'] : $misc['amount'],
        'amount_options' => (isset($data['amount_options'])) ? (int) $data['amount_options'] : $misc['amount_options'],
        'payee' => (isset($data['payee']) && $data['payee'] !== '') ? (int) $data['payee'] : $misc['payee'],
        'note' => (isset($data['note']) && $data['note'] !== '') ? $data['note'] : $misc['note'],
        'date' => (isset($data['date']) && $data['date'] !== '') ? strtotime($data['date']) : $misc['date'],
      );

      $result = db_merge('move_hourly_misc_payment')
        ->key(array('id' => $id))
        ->fields($update_data)
        ->execute();
      $this->tryRemoveMiscPaymentFromCache($misc['date'], $id);
      $this->cacheCreatePayrollHourlyMisc($id);
    }

    return $result;
  }

  /**
   * Calculate and save payroll for manager.
   *
   * @param int $nid
   *   Node id.
   * @param array $contract_info
   *   Data with contract information.
   * @param int $contract_type
   *   The type of contract.
   *
   * @throws \Exception
   */
  public function createPayrollManager(int $nid, array $contract_info, int $contract_type = ContractType::PAYROLL) {
    // Calculate payroll for manager is one time
    // if request have Flat Rate service type.
    $flat_rate_condition = $this->isFlatRate($nid) && ($contract_type === ContractType::PICKUP_PAYROLL);
    // If request service type haven't
    // Flat Rate necessarily calculate payroll for manager.
    if ($flat_rate_condition || !$this->isFlatRate($nid)) {
      $manager_uid = $this->getManagerContract($nid);
      if ($manager_uid) {
        $data = array();
        $data['uid'] = (int) $manager_uid;
        $data['value'] = $this->calcManagerCommissions($nid, $data['uid'], $contract_info);
        $this->setPayrollInfo($nid, array($data), $contract_type);
      }
    }
  }

  /**
   * Calculate commissions for manager.
   *
   * @param int $nid
   *   Node(request) id is associated with the contract.
   * @param int $uid
   *   User(manager) id.
   * @param array $contract_info
   *   Array with information about contract.
   * @param int $contract_type
   *   The type of contract.
   *
   * @return array
   *   Calculated commissions.
   */
  private function calcManagerCommissions($nid, int $uid, array $contract_info, int $contract_type = ContractType::PAYROLL) {
    $result = array();
    $result['total'] = 0;
    $rates = $this->rateManager($uid);
    $request = entity_metadata_wrapper('node', $nid);

    // For Long Distance.
    $slice_offset = ($request->field_move_service_type->value() == 7) ? 3 : 0;
    $rates_slice = array_slice($rates, $slice_offset, 3, TRUE);

    foreach ($rates_slice as $rate) {
      switch ($rate['flag']) {
        case ManagerRateCommission::OFFICECOMMISSION:
          $calc_commissions = $this->calcWithExcludeManageCommissions($uid, $nid, (float) $rate['rate'], $contract_info, $contract_type);
          $result['commissions'][$rate['flag']] = array(
            'for_commission' => $calc_commissions['grand_total'],
            'rate' => $rate['rate'],
            'total' => $calc_commissions['total'],
          );
          break;
      }
    }

    if (isset($result['commissions'])) {
      $result['total'] = $this->calcTotalCommissions($result['commissions']);
    }

    return $result;
  }

  /**
   * Get commission from total with exclude.
   *
   * @param int $uid
   *   User id.
   * @param int $nid
   *   Node(contract) id.
   * @param int $contract_type
   *   The type of contract.
   * @param array $contract_info
   *   The array with contract data.
   *
   * @return float
   *   Total commission.
   */
  public function getSumCommissionFromTotal(int $uid, int $nid, int $contract_type = ContractType::PAYROLL, array $contract_info = array()) : float {
    $total = array();

    $user_role = $this->userRole(user_load($uid));
    $manager_roles = array(Roles::MANAGER, Roles::SALES);

    $user_exclude = $this->excludeRateUser($uid);

    if (empty($contract_info)) {
      $contract_info = self::getContractInfo($nid);
      $contract_info = reset($contract_info);
    }

    if (in_array($user_role, $manager_roles)) {
      $grand_total = isset($contract_info['manager_total']) ? $contract_info['manager_total'] : 0;
    }
    else {
      $grand_total = isset($contract_info['grand_total']) ? $contract_info['grand_total'] : 0;
    }

    $total['packing'] = isset($contract_info['materials']) ? $contract_info['materials'] : 0;
    $total['valuation'] = isset($contract_info['valuation']) ? $contract_info['valuation'] : 0;
    $total['fuel'] = isset($contract_info['fuel']) ? $contract_info['fuel'] : 0;
    $additional_contract_total = isset($contract_info['extra_service_total']) ? $contract_info['extra_service_total'] : 0;
    // Flag TRUE or FALSE.
    $user_exclude_additional = $user_exclude['additional'];
    unset($user_exclude['additional']);

    // Names of services.
    $user_additional_services = $user_exclude['additional_services'];
    unset($user_exclude['additional_services']);

    if ($user_exclude_additional && is_array($user_additional_services) && !empty($user_additional_services)) {
      $sum_for_exclude = $this->sumExtra($nid, $user_additional_services);
      if ($sum_for_exclude) {
        $grand_total -= $sum_for_exclude;
      }
    }
    elseif ($user_exclude_additional && $additional_contract_total > 0) {
      $grand_total -= $additional_contract_total;
    }

    foreach ($user_exclude as $name => $item) {
      if ($item) {
        $grand_total -= $total[$name];
      }
    }

    $custom_payroll = $this->getCustomPayroll($nid, $uid, $contract_type);
    foreach ($custom_payroll as $key => $value) {
      $grand_total += $value['amount'];
    }

    $grand_total = (float) $grand_total;
    return round($grand_total, 2);
  }

  /**
   * Calculate manager commission.
   *
   * @param int $uid
   *   The user id.
   * @param int $nid
   *   The node id.
   * @param float $rate
   *   Manager Rate.
   * @param array $contract_info
   *   Array with contract.
   * @param int $contract_type
   *   The type of contract.
   *
   * @return array
   *   Manager commissions with grand total.
   */
  private function calcWithExcludeManageCommissions(int $uid, int $nid, float $rate, array $contract_info = array(), int $contract_type = ContractType::PAYROLL) : array {
    $grand_total = $this->getSumCommissionFromTotal($uid, $nid, $contract_type, $contract_info);

    $result = array(
      'total' => round($grand_total * ($rate / 100), 2),
      'grand_total' => $grand_total,
    );

    return $result;
  }

  /**
   * Calculate worker commissions from contract.
   *
   * @param int $nid
   *   The node id.
   * @param int $uid
   *   The user id.
   * @param string $role_name
   *   The name of role.
   * @param array $contract_info
   *   Information about contract.
   * @param int $type
   *   The type of operation:
   *    0 - Create payroll from contract;
   *    1 - Update Payroll.
   * @param int $contract_type
   *   Flat Rate contract type: 0 - Pickup, 1 - Delivery.
   *
   * @return array
   *   Worker commissions.
   */
  private function calcContractWorkerCommissions(int $nid, int $uid, string $role_name, array $contract_info, int $type = 1, int $contract_type = ContractType::PAYROLL) : array {
    $result = array();
    $result['additional'] = isset($contract_info[$role_name][$uid]['additional']) ? $contract_info[$role_name][$uid]['additional'] : 0;
    $result['switcher'] = isset($contract_info[$role_name][$uid]['switcher']) ? $contract_info[$role_name][$uid]['switcher'] : 0;
    $result['commissions'] = array();
    $user_rate = $this->rateWorkUser($uid);

    $calc_hourly_rate = function ($hours_pay, $rate) {
      $rate = self::validateFloatValue($rate);
      return array(
        'for_commission' => $hours_pay,
        'rate' => $rate,
        'total' => round(($hours_pay * $rate), 2),
      );
    };

    $hours_pay = 0;
    if (isset($contract_info[$role_name][$uid]['hours_to_pay'])) {
      $hours_pay = $contract_info[$role_name][$uid]['hours_to_pay'];
    }
    elseif (isset($contract_info['hours'])) {
      $hours_pay = $contract_info['hours'];
    }

    foreach ($user_rate as $id => $rate) {
      switch ($id) {
        case RateCommission::HOURLYRATE:
          $result['commissions'][$id] = $calc_hourly_rate($hours_pay, $rate['rate']);
          break;

        case RateCommission::EXTRASCOMMISSION:
          $result['commissions'][$id] = array(
            'for_commission' => $this->sumExtra($nid),
            'rate' => $rate['rate'],
            'total' => $this->getPayrollExtra($nid, $uid, $contract_type),
          );
          break;

        case RateCommission::DAILYRATE:
          $rate = (float) $rate['rate'];
          $result['commissions'][$id] = array(
            'for_commission' => $rate,
            'rate' => 100,
            'total' => $rate,
          );
          break;

        case RateCommission::MATERIALSCOMMISSION:
          $for_commission = $this->getSumMaterials($nid);
          $result['commissions'][$id] = array(
            'for_commission' => $contract_type != ContractType::PAYROLL ? $for_commission / 2 : $for_commission,
            'rate' => $rate['rate'],
            'total' => $this->getPayrollMaterials($nid, $uid, $contract_type),
          );
          break;

        case RateCommission::HOURLYRATEHELPER:
          if ($this->detectHourlyRateAsHelper($nid, $uid, $contract_type)) {
            $result['commissions'][$id] = $calc_hourly_rate($hours_pay, $rate['rate']);
          }
          break;

        case RateCommission::BONUS:
          $result['commissions'][$id] = array(
            'for_commission' => $hours_pay,
            'rate' => $rate['rate'],
            'total' => $hours_pay * $rate['rate'],
          );
          break;

        case RateCommission::COMMISSIONFROMTOTAL:
          $for_commission = $this->getSumCommissionFromTotal($uid, $nid, $contract_type, $contract_info);
          $result['commissions'][$id] = array(
            'for_commission' => $for_commission,
            'rate' => $rate['rate'],
            'total' => ((float) $rate['rate'] / 100) * $for_commission,
          );
          break;
      }
    }

    // Calculate Tips.
    $tips = $this->getWorkerTip($uid, $nid, $contract_info);
    $result['commissions'][RateCommission::TIPS] = array(
      'for_commission' => $tips,
      'rate' => 100,
      'total' => $tips,
    );

    if (!$type && $payroll = $this->getJobUser($nid, $uid, FALSE)) {
      if (isset($payroll['commissions']) && $payroll['commissions']) {
        foreach ($payroll['commissions'] as $key => $commission) {
          if ($commission['for_commission']) {
            $result['commissions'][$key] = $commission;
          }
        }
      }
    }

    // If worker have role Foreman and he saved as field_helper then calculate
    // only HOURLYRATEHELPER rate and TIPS.
    if ($this->detectHourlyRateAsHelper($nid, $uid, $contract_type)) {
      $solve_commission = array(
        RateCommission::HOURLYRATEHELPER,
        RateCommission::TIPS,
        RateCommission::BONUS,
        RateCommission::DAILYRATE,
      );
      $rates_walk = function ($commission_id) use (&$result, $solve_commission, $uid) {
        if (!in_array($commission_id, $solve_commission)) {
          unset($result['commissions'][$commission_id]);
        }
      };

      foreach (array_keys($result['commissions']) as $commission_id) {
        $rates_walk($commission_id);
      }
    }

    $result['total'] = $this->calcTotalCommissions($result['commissions']);

    return $result;
  }

  /**
   * Check if value is string prepare value to float.
   *
   * @param mixed $value
   *   Value for validate.
   *
   * @return mixed
   *   Value.
   */
  private static function validateFloatValue($value) {
    return (float) filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
  }

  private function calcTotalCommissions($commissions) {
    $total = 0;
    if (count($commissions)) {
      foreach ($commissions as $item) {
        $total += $item['total'];
      }
    }

    return round($total, 2);
  }

  /**
   * Helper method for detect that user has foreman role in helper field.
   *
   * @param int $nid
   *   The node id.
   * @param int $uid
   *   The user id.
   * @param int $contract_type
   *   Flat Rate contract type: 0 - Pickup, 1 - Delivery.
   *
   * @return bool
   *   The result of operation.
   */
  private function detectHourlyRateAsHelper(int $nid, int $uid, int $contract_type = ContractType::PAYROLL) : bool {
    // @TODO Check additionalCrews!
    $result = FALSE;
    $request_data = MoveRequest::getRequestData($nid);
    $request_data_decode = isset($request_data['value']) ? (array) $request_data['value'] : FALSE;
    $user_role = $this->userRole(user_load($uid));
    if ($request_data_decode && $user_role == Roles::FOREMAN && isset($request_data_decode['crews'])) {
      // Default behavior.
      $crews = (object) $request_data_decode['crews'];
      $worker_collection = $crews->workersCollection;

      switch ($contract_type) {
        case ContractType::PICKUP_PAYROLL:
          $worker_collection = $crews->pickedUpCrew['helpers'];
          break;

        case ContractType::DELIVERY_PAYROLL:
          $worker_collection = $crews->deliveryCrew['helpers'];
          break;
      }

      $result = in_array($uid, $worker_collection);
    }

    return $result;
  }

  public function createPayrollWorkers(int $nid, array $contract_info, int $contract_type = ContractType::PAYROLL) : void {
    $workers = $this->isFlatRate($nid) ? $this->getWorkersFlatRateRequest($nid, $contract_type) : $this->getWorkersRequest($nid, $contract_info);

    foreach ($workers as $uid => $worker) {
      if (!is_numeric($uid)) {
        continue;
      }

      $data = array();
      $data['uid'] = $uid;
      $data['value'] = $this->calcContractWorkerCommissions($nid, $uid, $worker['role'], $contract_info, 0, $contract_type);
      $this->setPayrollInfo($nid, array($data), $contract_type);
    }
  }

  private function getManagerContract($nid) {
    $select = db_select('move_request_sales_person', 'mr')
      ->fields('mr', array('uid'))
      ->condition('mr.nid', $nid)
      ->execute()
      ->fetchCol();

    return $select ? reset($select) : NULL;
  }

  public static function getWorkersInfo() {
    $result = array();
    $payroll_instance = new Payroll();
    $all_workers = $payroll_instance->getWorkersByRole();
    foreach ($all_workers as $uid => $role_name) {
      $user_info = (new Clients())->getUserFieldsData($uid, FALSE);
      if ($role_name == 'manager' || $role_name == 'sales') {
        $result[$role_name][$uid] = array(
          'info' => $user_info,
          'rates' => $payroll_instance->rateManager($uid),
        );
      }
      else {
        $result[$role_name][$uid] = array(
          'info' => $user_info,
          'rates' => $payroll_instance->rateWorkUser($uid),
        );
      }
    }

    return $result;
  }

  /**
   * Save custom payroll to database.
   *
   * @param int $uid
   *   The user id who creating payroll.
   * @param int $nid
   *   The node id.
   * @param string $name
   *   Name of custom payroll.
   * @param float $amount
   *   Amount of custom payroll.
   * @param array $photo
   *   Photo of receipts.
   * @param int $contract_type
   *   The type of contract.
   * @return \DatabaseStatementInterface|int Id of new inserted to DB custom payroll.
   * Id of new inserted to DB custom payroll.
   */
  public function createCustomPayroll(int $uid, int $nid, string $name, float $amount, array $photo, int $contract_type = ContractType::PAYROLL) {
    $payroll_id = db_insert('move_payroll_custom')
      ->fields(array(
        'uid' => $uid,
        'nid' => $nid,
        'name' => $name,
        'amount' => $amount,
        'flat_rate' => $contract_type,
      ))
      ->execute();

    foreach ($photo as $item) {
      db_insert('move_payroll_custom_photo')
        ->fields(array(
          'pcid' => $payroll_id,
          'value' => $item,
        ))
        ->execute();
    }

    return $payroll_id;
  }

  /**
   * Delete concrete custom payroll.
   *
   * @param int $id
   *   The id of custom payroll.
   *
   * @return \DatabaseStatementInterface
   *   Result of operation.
   */
  public function deleteCustomPayroll(int $id) {
    $result = db_delete('move_payroll_custom')
      ->condition('id', $id)
      ->execute();

    db_delete('move_payroll_custom_photo')
      ->condition('pcid', $id)
      ->execute();

    return $result;
  }

  /**
   * Update concrete custom payroll.
   *
   * @param int $id
   *   Id of custom payroll.
   * @param string $name
   *   Name of custom payroll.
   * @param float $amount
   *   Amount of custom payroll.
   * @param array $photo
   *   Update field photo.
   *
   * @return int
   *   Status operation.
   */
  public function updateCustomPayroll(int $id, string $name, float $amount, array $photo) {
    $result = db_update('move_payroll_custom')
      ->fields(array(
        'amount' => $amount,
        'name' => $name,
      ))
      ->condition('id', $id)
      ->execute();

    foreach ($photo as $key => $item) {
      if ($item) {
        switch ($key) {
          case 'add':
            $addPhoto = function ($data) use ($id) {
              db_insert('move_payroll_custom_photo')
                ->fields(array(
                  'pcid' => $id,
                  'value' => (string) $data,
                ))
                ->execute();
            };
            array_map($addPhoto, (array) $item);
            break;

          case 'del':
            $delPhoto = function ($custom_photo_id) {
              db_delete('move_payroll_custom_photo')
                ->condition('id', (int) $custom_photo_id)
                ->execute();
            };
            array_map($delPhoto, (array) $item);
            break;
        }
      }
    }

    return $result;
  }

  /**
   * Custom payroll (receipts) for Foreman.
   *
   * @param int $nid
   *   Node id.
   * @param int $uid
   *   User id.
   * @param int $contract_type
   *   The type of contract.
   *
   * @return array
   *   Custom receipts.
   */
  public function getCustomPayroll(int $nid, int $uid, int $contract_type = ContractType::PAYROLL) {
    $result = array();
    $select = db_select('move_payroll_custom', 'mpc');
    $select->leftJoin('move_payroll_custom_photo', 'mpcp', 'mpc.id = mpcp.pcid');
    $select->addField('mpc', 'id', 'payroll_id');
    $select->addField('mpcp', 'id', 'photo_id');
    $select->condition('mpc.nid', $nid)
      ->condition('mpc.flat_rate', $contract_type)
      ->condition('mpc.uid', $uid)
      ->fields('mpc', array('name', 'amount', 'nid', 'uid'))
      ->fields('mpcp', array('value'));
    $data = $select->execute()->fetchAll();

    /*
     * Anonymous function for adding photo to payroll.
     *
     * @param $pid
     *   Payroll id.
     * @param $photo_id
     *   Photo id.
     * @param $photo
     *   The base64 photo.
     */
    $add_photo = function ($pid, $photo_id, $photo) use (&$result) {
      $result[$pid]['photo'][] = array(
        'id' => $photo_id,
        'value' => $photo,
      );
    };

    foreach ($data as $item) {
      if (isset($result[$item->payroll_id]) && $item->value && $item->photo_id) {
        $add_photo($item->payroll_id, $item->photo_id, $item->value);
      }
      else {
        $result[$item->payroll_id] = array(
          'name' => $item->name,
          'amount' => $item->amount,
          'nid' => $item->nid,
          'uid' => $item->uid,
        );
        if ($item->value && $item->photo_id) {
          $add_photo($item->payroll_id, $item->photo_id, $item->value);
        }
      }
    }

    return $result;
  }

  /**
   * Get All cached payroll.
   *
   * @param string $date_from
   *   Interval date "from".
   * @param string $date_to
   *   Interval date "to".
   *
   * @return array
   *   All cache for period.
   */
  public function cacheAllPayroll(string $date_from, string $date_to) {
    $result = array();
    $cache = $this->getPayrollCache($date_from, $date_to, 'value');
    foreach ($cache as $value) {
      $result_query = unserialize($value);
      foreach ($result_query as $item) {
        $role_id = Roles::getNameByValue($item['role_name']);
        // Initialization array.
        if (!isset($result[$role_id])) {
          $result[$role_id] = array(
            'role_name' => 0,
            'jobs_count' => 0,
            'hours' => 0,
            'hours_pay' => 0,
            'grand_total' => 0,
            'materials' => 0,
            'extra' => 0,
            'tip' => 0,
            'paid' => 0,
            'total' => 0,
          );
        }

        $result[$role_id]['role_name'] = $item['role_name'];
        $result[$role_id]['jobs_count'] += $item['jobs_count'];
        $result[$role_id]['hours'] += $item['hours'];
        $result[$role_id]['hours_pay'] += $item['hours_pay'];
        $result[$role_id]['grand_total'] += $item['grand_total'];
        $result[$role_id]['materials'] += $item['materials'];
        $result[$role_id]['extra'] += $item['extra'];
        $result[$role_id]['tip'] += $item['tip'];
        $result[$role_id]['paid'] += $item['paid'];
        $result[$role_id]['total'] += $item['total'];
      }
    }

    return $result;
  }

  /**
   * Get cached payroll by Role.
   *
   * @param string $date_from
   *   Interval date "from".
   * @param string $date_to
   *   Interval date "to".
   * @param int $rid
   *   Role id.
   *
   * @return array
   *   Cache by Role for period.
   */
  public function cacheByRolePayroll(string $date_from, string $date_to, int $rid) {
    $result = array();
    $cache = $this->getPayrollCache($date_from, $date_to, 'cache_by_role');

    $resulting = function ($user, $uid) use (&$result) {
      // Initialization array.
      if (!isset($result[$uid])) {
        $result[$uid] = array(
          'jobs_count' => 0,
          'hours' => 0,
          'hours_pay' => 0,
          'grand_total' => 0,
          'materials' => 0,
          'extra' => 0,
          'tip' => 0,
          'total' => 0,
          'paid' => 0,
        );
      }

      $result[$uid]['jobs_count'] += $user['jobs_count'];
      $result[$uid]['hours'] += $user['hours'];
      $result[$uid]['hours_pay'] += $user['hours_pay'];
      $result[$uid]['grand_total'] += $user['grand_total'];
      $result[$uid]['materials'] += $user['materials'];
      $result[$uid]['extra'] += $user['extra'];
      $result[$uid]['tip'] += $user['tip'];
      $result[$uid]['total'] += $user['total'];
      $result[$uid]['paid'] += $user['paid'];
      if (isset($user['uid'])) {
        $result[$uid]['user_info'] = user_load($user['uid']);
      }
      else {
        $result[$uid]['user_info'] = user_load($user['user_info']->uid);
      }
    };

    foreach ($cache as $value) {
      $unserialized = unserialize($value);
      if (isset($unserialized[$rid])) {
        array_walk($unserialized[$rid], $resulting);
      }
    }

    // Clear empty elements.
    foreach ($result as $uid => $user) {
      unset($user['user_info']);
      unset($user['total']);
      $user['paid'] = $user['paid'] * -1;

      $sum = array_sum($user);
      if ($sum == 0) {
        unset($result[$uid]);
      }
    }

    return $result;
  }

  /**
   * Get cached payroll for user.
   *
   * @param string $date_from
   *   Interval date "from".
   * @param string $date_to
   *   Interval date "to".
   * @param int $uid
   *   User id.
   *
   * @return array
   *   User cache for period.
   */
  public function cacheByUserPayroll(string $date_from, string $date_to, int $uid) {
    ini_set('memory_limit', '512M');
    $result = array(
      'jobs' => array(),
      'jobs_info' => array(),
      'hourly_misc' => array(),
      'paychecks' => array(),
      'trip_jobs' => array(),
      'totals' => array(
        'total' => 0,
        'hours' => 0,
        'grand_total' => 0,
        'hours_pay' => 0,
        'materials' => 0,
        'advanced' => 0,
        'extra' => 0,
        'ins' => 0,
        'tip' => 0,
        'bonus' => 0,
        'misc' => 0,
        'paid' => 0,
        'custom_payroll' => 0,
      ),
    );
    $cache = $this->getPayrollCache($date_from, $date_to, 'cache_by_user');

    $calc_total = function (array $totals) use (&$result) {
      $result['totals']['total'] += $totals['total'];
      $result['totals']['hours'] += $totals['hours'];
      $result['totals']['grand_total'] += $totals['grand_total'];
      $result['totals']['hours_pay'] += $totals['hours_pay'];
      $result['totals']['materials'] += $totals['materials'];
      $result['totals']['advanced'] += $totals['advanced'];
      $result['totals']['extra'] += $totals['extra'];
      $result['totals']['ins'] += $totals['ins'];
      $result['totals']['tip'] += $totals['tip'];
      $result['totals']['bonus'] += $totals['bonus'];
      $result['totals']['misc'] += $totals['misc'];
      $result['totals']['paid'] += $totals['paid'];
    };

    // Adding retrieve request data dynamically.
    $get_retrieve = function (&$item, $nid) {
      if (count($item) <= 2) {
        foreach ($item as $key => $value) {
          if (empty($item[$key]['retrieve'])) {
            $item[$key]['retrieve'] = (new MoveRequest())->getNode($value['nid'], 1);
          }
        }
      }
      elseif (empty($item['retrieve'])) {
        $item['retrieve'] = (new MoveRequest())->getNode($nid, 1);
      }
    };

    foreach ($cache as $value) {
      $unserialized = unserialize($value);
      if (isset($unserialized[$uid])) {
        // TODO refactor this.
        // IF the nid is already exist we need to add it to key item.
        $same_jobs = array_intersect_key($result['jobs'], $unserialized[$uid]['jobs']);
        if (!empty($same_jobs)) {
          // For Flatrate requests.
          foreach ($same_jobs as $key_nid => $val) {
            $result['jobs'][$key_nid] = array_merge($result['jobs'][$key_nid], $unserialized[$uid]['jobs'][$key_nid]);
          }

          // For other requests.
          $result['jobs'] += array_diff_key($unserialized[$uid]['jobs'], $result['jobs']);
        }
        else {
          $result['jobs'] += $unserialized[$uid]['jobs'];
        }

        array_walk($result['jobs'], $get_retrieve);

        $result['trip_jobs'] += $unserialized[$uid]['trip_jobs'] ?? [];
        $result['jobs_info'] += $unserialized[$uid]['jobs_info'];
        $result['hourly_misc'] += $unserialized[$uid]['hourly_misc'];
        $result['paychecks'] += $unserialized[$uid]['paychecks'];
        $calc_total($unserialized[$uid]['totals']);
      }
    }

    $custom_payroll_total = 0;
    foreach ($result['jobs'] as $job_nid => $job) {
      $custom_payroll_total += $this->getCustomPayrollTotal($uid, $job_nid);
    }
    $result['totals']['custom_payroll'] = $custom_payroll_total;

    return $result;
  }

  private function getCustomPayrollTotal(int $uid, int $job) : int {
    $total = 0;
    $move_payroll_custom = db_select('move_payroll_custom', 'mpc')
      ->fields('mpc', array('amount'))
      ->condition('mpc.uid', $uid)
      ->condition('mpc.nid', $job)
      ->execute()
      ->fetchCol();

    foreach ($move_payroll_custom as $item) {
      $total += $item;
    }

    return $total;
  }

  /**
   * Helper method for cache functions.
   *
   * @param string $date_from
   *   Interval date "from".
   * @param string $date_to
   *   Interval date "to".
   * @param string $field
   *   The type of payroll.
   *
   * @return mixed
   *   All found record.
   */
  public function getPayrollCache(string $date_from, string $date_to, string $field) {
    return db_select('move_payroll_day_cache', 'mpdc')
      ->fields('mpdc', array($field))
      ->condition('mpdc.date', array($date_from, $date_to), 'BETWEEN')
      ->execute()
      ->fetchCol();
  }

  /**
   * Get array of dates from 2016-01-01 to current day for batch.
   *
   * @return array
   *   Array of all dates.
   */
  public function cachePayrollPrepareBatch() {
    $operations = array();
    $date = '2016-01-01';
    $curDate = date('Y-m-d');
    while ($date <= $curDate) {
      $operations[] = array('move_service_new_payroll_batch_execute', array($date));
      $date_obj = new \DateTime($date);
      $date_obj = $date_obj->modify('+1 day');
      $date = $date_obj->format('Y-m-d');
    }
    return $operations;
  }

  /**
   * Updating cache payroll by node id.
   *
   * @param int $nid
   *   Node id.
   * @param int $contract_type
   *   The type of contract.
   * @param int $op Operation
   *    0 - Delete workers from payroll cache.
   *    1 - Add workers from payroll cache.
   *
   * @return int
   *   If date for contract type is not set.
   */
  public function cacheUpdatePayrollRequest(int $nid, int $contract_type = ContractType::PAYROLL, int $op = 1) {
    $date = $this->getDateContract($nid, $contract_type);
    // If request move date is not set return 0.
    // @TODO Crutch!
    if ($date == '1970-01-01') {
      return 0;
    }
    $cache = $this->getCacheByDay($date);

    // Worker List.
    $move_payroll_info = db_select('move_payroll_info', 'mpi')
      ->fields('mpi', array('uid'))
      ->condition('mpi.nid', $nid)
      ->condition('mpi.flat_rate', $contract_type)
      ->execute()
      ->fetchCol();

    $this->cachePayrollRequest($cache['all_department'], $cache['by_role'], $cache['by_user'], $date, $move_payroll_info, $nid, $op);
  }

  private function cacheDeleteUserPayroll(int $nid, int $uid, int $contract_type = ContractType::PAYROLL) : void {
    $date = $this->getDateContract($nid, $contract_type);
    $cache = $this->getCacheByDay($date);
    $this->cachePayrollRequest($cache['all_department'], $cache['by_role'], $cache['by_user'], $date, array($uid), $nid, 0, $contract_type);
  }

  private function getDateContract(int $nid, int $contract_type = ContractType::PAYROLL) : string {
    $date = $this->contractDate($nid, $contract_type);
    $this->setDate($date, $date);
    return $date;
  }

  private function getCacheByDay(string $date) {
    $result = array();
    $cache = $this->getPayrolDayCache($date);

    if (!$cache) {
      $this->cacheCreatePayrollAll($date);
      // Override cache variable again.
      $cache = $this->getPayrolDayCache($date);
    }

    $result['all_department'] = unserialize($cache->value);
    $result['by_role'] = unserialize($cache->cache_by_role);
    $result['by_user'] = unserialize($cache->cache_by_user);
    return $result;
  }

  /**
   * Get payrol cache for day.
   *
   * @param string $date
   *   The date of payrol cache.
   *
   * @return mixed
   */
  private function getPayrolDayCache(string $date) {
    return db_select('move_payroll_day_cache', 'mpdc')
      ->fields('mpdc')
      ->condition('mpdc.date', $date)
      ->execute()
      ->fetchObject();
  }

  /**
   * Updating payroll cache for request.
   *
   * @param array $all_department_cache
   *   Cache all departments for request contract date.
   * @param array $by_role_cache
   *   Cache all users all roles for request contract date.
   * @param array $by_user_cache
   *   Cache users(workers) for request contract date.
   * @param string $date
   *   Date when contract filled.
   * @param array $uids
   *   Worker List.
   * @param int $nid
   *   Contract id.
   * @param int $op
   *   Operation:
   *    0 - Delete workers from payroll cache.
   *    1 - Add workers from payroll cache.
   */
  private function cachePayrollRequest(array $all_department_cache, array $by_role_cache, array $by_user_cache, string $date, array $uids, int $nid, int $op = 1, int $contract_type = ContractType::PAYROLL) {
    foreach ($uids as $uid) {
      $user = user_load($uid);
      if (!$user) {
        continue;
      }

      $rid = $this->userRole($user);
      $original_rid_uid = array();
      // Cached payroll.
      if (isset($by_user_cache[$uid])) {
        $original_rid_uid = $by_user_cache[$uid];
      }

      // Fill if user have not elements.
      $fill_array = array(
        'jobs',
        'jobs_info',
        'hourly_misc',
        'paychecks',
        'totals',
      );
      foreach ($fill_array as $item) {
        if (!isset($original_rid_uid[$item])) {
          $original_rid_uid[$item] = array();
        }
        if (!isset($by_user_cache[$uid][$item])) {
          $by_user_cache[$uid][$item] = array();
        }
      }

      if ($op) {
        // New payroll.
        $payroll_info = $this->getPayrollByNids(array($nid), $uid);
        $by_user_cache[$uid]['jobs'][$nid] = $payroll_info['jobs'][$nid];
        $by_user_cache[$uid]['jobs_info'][$nid] = $this->getJobUser($nid, $uid, FALSE);
        $original_rid_uid['jobs'][$nid] = $by_user_cache[$uid]['jobs'][$nid];
        $original_rid_uid['jobs_info'][$nid] = $original_rid_uid['jobs'][$nid];
      }
      else {
        // Remove User from payroll.
        if ($contract_type == ContractType::LONGDISTANCE) {
          unset($by_user_cache[$uid]['trip_jobs'][$nid]);
          unset($original_rid_uid['trip_jobs'][$nid]);
        }
        else {
          unset($by_user_cache[$uid]['jobs'][$nid]);
          unset($by_user_cache[$uid]['jobs_info'][$nid]);
          unset($original_rid_uid['jobs'][$nid]);
          unset($original_rid_uid['jobs_info'][$nid]);
        }
      }
      $original_rid_uid['totals'] = $this->calculateTotalUser($original_rid_uid['paychecks'], $original_rid_uid['jobs'], $original_rid_uid['hourly_misc']);
      $by_user_cache[$uid]['totals'] = $original_rid_uid['totals'];
      $user_by_role = array();
      $user_by_role['jobs_count'] = isset($original_rid_uid['jobs']) ? count($original_rid_uid['jobs']) : 0;
      $user_by_role['hours'] = $original_rid_uid['totals']['hours'];
      $user_by_role['hours_pay'] = $original_rid_uid['totals']['hours_pay'];
      $user_by_role['grand_total'] = $original_rid_uid['totals']['grand_total'];
      $user_by_role['materials'] = $original_rid_uid['totals']['materials'];
      $user_by_role['extra'] = $original_rid_uid['totals']['extra'];
      $user_by_role['total'] = $original_rid_uid['totals']['total'];
      $user_by_role['tip'] = $original_rid_uid['totals']['tip'];
      $user_by_role['bonus'] = $original_rid_uid['totals']['bonus'];
      $user_by_role['misc'] = $original_rid_uid['totals']['misc'];
      $user_by_role['paid'] = $original_rid_uid['totals']['paid'];
      // Sometimes user data in by_role array haven't filled.
      if (isset($by_role_cache[$rid][$uid]) && is_array($by_role_cache[$rid][$uid])) {
        $by_role_cache[$rid][$uid] = array_merge($by_role_cache[$rid][$uid], $user_by_role);
      }
      else {
        $new_data = array(
          'jobs_count' => 0,
          'hours' => 0,
          'hours_pay' => 0,
          'grand_total' => 0,
          'materials' => 0,
          'extra' => 0,
          'tip' => 0,
          'total' => 0,
          'paid' => 0,
          'uid' => $uid,
        );
        $by_role_cache[$rid][$uid] = array_merge($new_data, $user_by_role);
      }
    }

    // Updated All Department cache.
    foreach ($all_department_cache as $rid => $role) {
      $new_department = array(
        'jobs_count' => 0,
        'hours' => 0,
        'hours_pay' => 0,
        'grand_total' => 0,
        'materials' => 0,
        'extra' => 0,
        'tip' => 0,
        'total' => 0,
        'paid' => 0,
      );

      foreach ($by_role_cache[$rid] as $user) {
        if (is_array($user)) {
          $new_department['jobs_count'] += $user['jobs_count'];
          $new_department['hours'] += $user['hours'];
          $new_department['hours_pay'] += $user['hours_pay'];
          $new_department['grand_total'] += $user['grand_total'];
          $new_department['materials'] += $user['materials'];
          $new_department['extra'] += $user['extra'];
          $new_department['tip'] += $user['tip'];
          $new_department['paid'] += $user['paid'];
          $new_department['total'] += $user['total'];
        }
      }

      $all_department_cache[$rid] = array_merge($all_department_cache[$rid], $new_department);
    }

    // Updating payroll cache.
    db_merge('move_payroll_day_cache')
      ->key(array('date' => $date))
      ->fields(array(
        'value' => serialize($all_department_cache),
        'cache_by_role' => serialize($by_role_cache),
        'cache_by_user' => serialize($by_user_cache),
      ))
      ->execute();
  }

  /**
   * Add a new hourly/misc to payroll cache by user.
   *
   * @param int $record_id
   *   Hourly/Misc record id in DB.
   */
  private function cacheCreatePayrollHourlyMisc(int $record_id) {
    $new_hourly_misc = db_select('move_hourly_misc_payment', 'mhm')
      ->fields('mhm')
      ->condition('mhm.id', $record_id)
      ->execute()
      ->fetchObject();

    $uid = $new_hourly_misc->payee;
    $date = date('Y-m-d', $new_hourly_misc->date);
    $this->setDate($date, $date);
    $this->updateCachePayrollHourlyMisc($record_id, $date, $uid, 1, $new_hourly_misc);
  }

  private function cacheDeletePayrollHourlyMisc(int $record_id, array $old_misc) {
    $uid = $old_misc['payee'];
    $date = date('Y-m-d', $old_misc['date']);
    $this->setDate($date, $date);
    $this->updateCachePayrollHourlyMisc($record_id, $date, $uid, 0);
    return $old_misc['date'];
  }

  /**
   * Updating cache payroll for Hourly/Misc.
   *
   * @param int $record_id
   *   Id of Hourly/Misc.
   * @param string $date
   *   Date of created cache.
   * @param int $uid
   *   User id.
   * @param int $op
   *   Operation: 1 - Create; 0 - Delete.
   * @param bool $new_hourly_misc
   *   Flag new hourly misc.
   */
  private function updateCachePayrollHourlyMisc(int $record_id, string $date, int $uid, int $op = 0, $new_hourly_misc = NULL) : void {

    $cache = $this->getPayrolDayCache($date);
    if (!$cache) {
      $this->cacheCreatePayrollAll($date);
      // Override cache variable again.
      $cache = $this->getPayrolDayCache($date);
    }

    $all_department = unserialize($cache->value);
    $by_role = unserialize($cache->cache_by_role);
    $by_user = unserialize($cache->cache_by_user);

    // Fill if user have not elements.
    $fill_array = array('jobs', 'jobs_info', 'hourly_misc', 'paychecks',
      'totals',
    );
    foreach ($fill_array as $item) {
      if (!isset($by_user[$uid][$item])) {
        $by_user[$uid][$item] = array();
      }
    }

    if ($op) {
      $by_user[$uid]['hourly_misc'][$record_id] = $new_hourly_misc;
    }
    else {
      unset($by_user[$uid]['hourly_misc'][$record_id]);
    }
    $by_user[$uid]['totals'] = $this->calculateTotalUser($by_user[$uid]['paychecks'], $by_user[$uid]['jobs'], $by_user[$uid]['hourly_misc']);
    $rid = $this->userRole(user_load($uid));
    $original_rid_uid = $by_user[$uid];
    $user_by_role = array();
    $user_by_role['jobs_count'] = isset($original_rid_uid['jobs']) ? count($original_rid_uid['jobs']) : 0;
    $user_by_role['hours'] = $original_rid_uid['totals']['hours'];
    $user_by_role['hours_pay'] = $original_rid_uid['totals']['hours_pay'];
    $user_by_role['grand_total'] = $original_rid_uid['totals']['grand_total'];
    $user_by_role['materials'] = $original_rid_uid['totals']['materials'];
    $user_by_role['extra'] = $original_rid_uid['totals']['extra'];
    $user_by_role['total'] = $original_rid_uid['totals']['total'];
    $user_by_role['tip'] = $original_rid_uid['totals']['tip'];
    $user_by_role['bonus'] = $original_rid_uid['totals']['bonus'];
    $user_by_role['misc'] = $original_rid_uid['totals']['misc'];
    $user_by_role['paid'] = $original_rid_uid['totals']['paid'];
    // Sometimes user data in by_role array haven't filled.
    if (isset($by_role[$rid][$uid]) && is_array($by_role[$rid][$uid])) {
      $by_role[$rid][$uid] = array_merge($by_role[$rid][$uid], $user_by_role);
    }
    else {
      $new_data = array(
        'jobs_count' => 0,
        'hours' => 0,
        'hours_pay' => 0,
        'grand_total' => 0,
        'materials' => 0,
        'extra' => 0,
        'tip' => 0,
        'total' => 0,
        'paid' => 0,
        'uid' => $uid,
      );
      $by_role[$rid][$uid] = array_merge($new_data, $user_by_role);
    }

    // Updated All Department cache.
    foreach ($all_department as $rid => $role) {
      $new_department = array(
        'jobs_count' => 0,
        'hours' => 0,
        'hours_pay' => 0,
        'grand_total' => 0,
        'materials' => 0,
        'extra' => 0,
        'tip' => 0,
        'total' => 0,
        'paid' => 0,
      );

      foreach ($by_role[$rid] as $user) {
        if (is_array($user)) {
          $new_department['jobs_count'] += $user['jobs_count'];
          $new_department['hours'] += $user['hours'];
          $new_department['hours_pay'] += $user['hours_pay'];
          $new_department['grand_total'] += $user['grand_total'];
          $new_department['materials'] += $user['materials'];
          $new_department['extra'] += $user['extra'];
          $new_department['tip'] += $user['tip'];
          $new_department['paid'] += $user['paid'];
          $new_department['total'] += $user['total'];
        }
      }

      $all_department[$rid] = array_merge($all_department[$rid], $new_department);
    }

    // Updating payroll cache.
    db_merge('move_payroll_day_cache')
      ->key(array('date' => $date))
      ->fields(array(
        'value' => serialize($all_department),
        'cache_by_role' => serialize($by_role),
        'cache_by_user' => serialize($by_user),
      ))
      ->execute();
  }

  /**
   * Updating cache payroll for paycheck.
   *
   * @param int $paycheck_id
   *   Id of paycheck.
   * @param int $op
   *   Operation with paycheck. 1 - Create; 0 - Delete.
   */
  public function updateCachePayrollPaycheck(int $paycheck_id, int $op = 0) {
    $paycheck = $this->viewPaychecks($this->getPaycheckAll($paycheck_id));
    $paycheck = reset($paycheck);

    $date = new \DateTime();
    $date->setTimezone(new \DateTimeZone('UTC'));
    $date->setTimestamp($paycheck->date);

    $cache = $this->getPayrolDayCache($date->format("Y-m-d"));
    if (!$cache) {
      $this->cacheCreatePayrollAll($date->format("Y-m-d"));
      // Override cache variable again.
      $cache = $this->getPayrolDayCache($date->format("Y-m-d"));
    }

    $all_department = unserialize($cache->value);
    $by_role = unserialize($cache->cache_by_role);
    $by_user = unserialize($cache->cache_by_user);
    $uid = $paycheck->uid;

    // Fill if user have not elements.
    $fill_array = array('jobs', 'jobs_info', 'hourly_misc', 'paychecks', 'totals');
    foreach ($fill_array as $item) {
      if (!isset($by_user[$uid][$item])) {
        $by_user[$uid][$item] = array();
      }
    }

    if ($op) {
      $by_user[$uid]['paychecks'][$paycheck_id] = $paycheck;
    }
    else {
      unset($by_user[$uid]['paychecks'][$paycheck_id]);
    }
    $by_user[$uid]['totals'] = $this->calculateTotalUser($by_user[$uid]['paychecks'], $by_user[$uid]['jobs'], $by_user[$uid]['hourly_misc']);

    $rid = $this->userRole(user_load($uid));
    $original_rid_uid = $by_user[$uid];
    $user_by_role = array();
    $user_by_role['jobs_count'] = isset($original_rid_uid['jobs']) ? count($original_rid_uid['jobs']) : 0;
    $user_by_role['hours'] = $original_rid_uid['totals']['hours'];
    $user_by_role['hours_pay'] = $original_rid_uid['totals']['hours_pay'];
    $user_by_role['grand_total'] = $original_rid_uid['totals']['grand_total'];
    $user_by_role['materials'] = $original_rid_uid['totals']['materials'];
    $user_by_role['extra'] = $original_rid_uid['totals']['extra'];
    $user_by_role['total'] = $original_rid_uid['totals']['total'];
    $user_by_role['tip'] = $original_rid_uid['totals']['tip'];
    $user_by_role['bonus'] = $original_rid_uid['totals']['bonus'];
    $user_by_role['misc'] = $original_rid_uid['totals']['misc'];
    $user_by_role['paid'] = $original_rid_uid['totals']['paid'];
    // Sometimes user data in by_role array haven't filled.
    if (isset($by_role[$rid][$uid]) && is_array($by_role[$rid][$uid])) {
      $by_role[$rid][$uid] = array_merge($by_role[$rid][$uid], $user_by_role);
    }
    else {
      $new_data = array(
        'jobs_count' => 0,
        'hours' => 0,
        'hours_pay' => 0,
        'grand_total' => 0,
        'materials' => 0,
        'extra' => 0,
        'tip' => 0,
        'total' => 0,
        'paid' => 0,
        'uid' => $uid,
      );
      $by_role[$rid][$uid] = array_merge($new_data, $user_by_role);
    }

    // Updated All Department cache.
    foreach ($all_department as $rid => $role) {
      $new_department = array(
        'jobs_count' => 0,
        'hours' => 0,
        'hours_pay' => 0,
        'grand_total' => 0,
        'materials' => 0,
        'extra' => 0,
        'tip' => 0,
        'total' => 0,
        'paid' => 0,
      );

      foreach ($by_role[$rid] as $user) {
        if (is_array($user)) {
          $new_department['jobs_count'] += $user['jobs_count'];
          $new_department['hours'] += $user['hours'];
          $new_department['hours_pay'] += $user['hours_pay'];
          $new_department['grand_total'] += $user['grand_total'];
          $new_department['materials'] += $user['materials'];
          $new_department['extra'] += $user['extra'];
          $new_department['tip'] += $user['tip'];
          $new_department['paid'] += $user['paid'];
          $new_department['total'] += $user['total'];
        }
      }

      $all_department[$rid] = array_merge($all_department[$rid], $new_department);
    }

    $date_format = $date->format("Y-m-d");
    // Updating payroll cache.
    db_merge('move_payroll_day_cache')
      ->key(array('date' => $date_format))
      ->fields(array(
        'value' => serialize($all_department),
        'cache_by_role' => serialize($by_role),
        'cache_by_user' => serialize($by_user),
      ))
      ->execute();
  }

  /**
   * Update Payroll Cache for period.
   *
   * @param string $date_from
   *   The date from.
   * @param string $date_to
   *   The Date to.
   *
   * @return bool
   *   Result of operation. TRUE or FALSE.
   */
  public function updateAllPayrollCache(string $date_from, string $date_to) : bool {
    try {
      $result = FALSE;
      // No time limit execution for update payroll cache.
      set_time_limit(120);
      ini_set('memory_limit', '512M');

      $execute = function ($date) {
        $this->cacheCreatePayrollAll($date);
      };

      if ($date_from == $date_to) {
        $execute($date_from);
      }
      else {
        while ($date_from <= $date_to) {
          $execute($date_from);
          $date_obj = new \DateTime($date_from);
          $date_obj = $date_obj->modify('+1 day');
          $date_from = $date_obj->format('Y-m-d');
        }
      }

      $result = TRUE;
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Update Payroll Cache', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      return $result;
    }
  }

  /**
   * Create All cache and save to DB.
   *
   * @param string $date
   *   Date for cache.
   */
  public function cacheCreatePayrollAll(string $date) {
    ini_set('memory_limit', '512M');
    set_time_limit(120);
    try {
    $this->dateFrom = Extra::getDateFrom($date);
    $this->dateTo = Extra::getDateTo($date);

    $day_result = $this->detailsAll();
    $byroll_result = $this->getPayrollByRole($day_result);

    $byuser_result = $this->getPayrollByUser($byroll_result);

    db_merge('move_payroll_day_cache')
      ->key(array('date' => $date))
      ->fields(array(
        'value' => serialize($day_result),
        'cache_by_role' => serialize($byroll_result),
        'cache_by_user' => serialize($byuser_result),
      ))
      ->execute();
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('cacheCreatePayrollAll', $message, array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Get payroll for roles.
   *
   * @param array $roles
   *   User roles.
   *
   * @return array
   *   Payroll.
   */
  private function getPayrollByRole(array $roles) : array {
    $result = array();
    foreach ($roles as $rid => $role) {
      $result[$rid] = $this->detailsByRole($rid);
    }
    return $result;
  }

  /**
   * Get payroll by user.
   *
   * @param array $byroll_result
   *   Payroll.
   *
   * @return array
   *   Payroll.
   */
  private function getPayrollByUser(array $byroll_result) : array {
    $result = array();

    foreach ($byroll_result as $rid => $users) {
      foreach ($users as $uid => $data) {
        $user_payroll = $this->getUserPayroll($uid);
        if ($user_payroll) {
          $result[$uid] = $user_payroll;
        }
      }
    }

    return $result;
  }

  /**
   * Determining flat rate move request.
   *
   * @param int $nid
   *   The node id.
   *
   * @return bool
   */
  public function isFlatRate(int $nid) : bool {
    $wrapper = entity_metadata_wrapper('node', $nid);
    $service_type = $wrapper->field_move_service_type->value();
    return $service_type == 5 && !self::isFlatRateLocal($nid);
  }

  /**
   * Determining long distance request.
   *
   * @param int $nid
   *   The node id.
   *
   * @return bool
   */
  public function isLDRequest(int $nid) : bool {
    $wrapper = entity_metadata_wrapper('node', $nid);
    $service_type = $wrapper->field_move_service_type->value();
    return $service_type == 7;
  }

  /**
   * Contract Type if request have FlatRate service type.
   *
   * @param int $nid
   *   The node id.
   *
   * @return int
   */
  private function getFlatRateContractType(int $nid) : int {
    // @TODO Get contract type by contract data.
    $contracts = self::getContractInfo($nid);
    return (count($contracts) == 2) ? ContractType::DELIVERY_PAYROLL : ContractType::PICKUP_PAYROLL;
  }

  public static function isFlatRateLocal(int $nid) : bool {
    $wrapper = entity_metadata_wrapper('node', $nid);
    $service_type = $wrapper->field_flat_rate_local_move->value();
    return !empty($service_type) ? TRUE : FALSE;
  }

}

/**
 * Class PayrollException.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollException extends \RuntimeException {

  /**
   * Save message to db.
   *
   * @param int $type
   *   A watchdog severity of the message.
   */
  public function saveDataBase($type) {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()} </br> Trace: {$this->getTraceAsString()}";
    watchdog('Payroll', $message, array(), $type);
  }

}

/**
 * Class PayrollWorkerRequestsField.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollWorkerRequestsField extends PayrollException {}

/**
 * Class PayrollChangeUser.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollChangeUser extends PayrollException {}

/**
 * Class PayrollFindUserField.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollFindUserField extends PayrollException {}

/**
 * Class PayrollCheckPaymentType.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollPaymentType extends PayrollException {}

/**
 * Class PayrollCheckMiscPaymentDate.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollMiscPaymentDate extends PayrollException {}

/**
 * Class PayrollCheckMiscPaymentDate.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollMiscAmountOption extends PayrollException {}

/**
 * Class PayrollMiscType.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollMiscType extends PayrollException {}

/**
 * Class PayrollMergeError.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollMergeError extends \InvalidMergeQueryException {

  /**
   * Save message to db.
   */
  public function saveDataBase() {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()} </br> Trace: {$this->getTraceAsString()}";
    watchdog('Payroll', $message, array(), WATCHDOG_ERROR);
  }

}

/**
 * Class PayrollFilterRequest.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollFilterRequest extends \EntityFieldQueryException {

  /**
   * Save message to db.
   */
  public function saveDataBase() {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()} </br> Trace: {$this->getTraceAsString()}";
    watchdog('Payroll', $message, array(), WATCHDOG_ERROR);
  }

}

/**
 * Class PayrollMainException.
 *
 * @package Drupal\move_services_new\Services
 */
class PayrollMainException extends \Exception {

  /**
   * Save message to db.
   */
  public function saveDataBase() {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()} </br> Trace: {$this->getTraceAsString()}";
    watchdog('Payroll', $message, array(), WATCHDOG_ERROR);
  }

}
