<?php

namespace Drupal\move_services_new\System\Resources;

use Drupal\move_services_new\Services\Comments;

/**
 * Class CommentDefinition.
 *
 * @package Drupal\move_services_new\System
 */
class CommentDefinition {

  public static function getDefinition() {
    return array(
      'move_request_comments' => array(
        'operations' => array(
          'retrieve' => array(
            'help' => 'Retrieve a comments',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => FALSE,
                'source' => array('path' => 0),
                'type' => 'int',
                'description' => 'The nid of the node to retrieve',
              ),
            ),
            'access arguments' => array('access comments'),
          ),
          'create' => array(
            'help' => 'Create a comment',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of new comment',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('post comments'),
          ),
          'delete' => array(
            'help' => 'Delete a comment',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'args' => array(
              array(
                'name' => 'cid',
                'type' => 'int',
                'description' => 'The comment id.',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own comments'),
          ),
          'update' => array(
            'help' => 'Comment update',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'args' => array(
              array(
                'name' => 'cid',
                'type' => 'int',
                'description' => 'Comment id',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'Message for update.',
                'source' => 'data',
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer comments'),
          ),
        ),
        'actions' => array(
          'get_all_comments' => array(
            'help' => 'Get all comments from all requests by user',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::getAllComments',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'access arguments' => array('access content'),
          ),
          'get_new_comments' => array(
            'help' => 'Get all new comments from all requests by uid',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::getAllNewComments',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'access arguments' => array('access comments'),
          ),
          'set_read_comment' => array(
            'help' => 'Set comment status to Read',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::setReadComment',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'args' => array(
              array(
                'name' => 'cid',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => array(),
                'source' => array('path' => 1),
              ),
            ),
            'access arguments' => array('access comments'),
          ),
          'unread_count' => array(
            'help' => 'Get count of new comment by nid',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::unreadCount',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'access arguments' => array('access comments'),
          ),
          'admin_all_comments' => array(
            'help' => 'Get count of new comment by nid',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::adminAllComments',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'access arguments' => array('access comments'),
          ),
          'set_read_comment_all' => array(
            'help' => 'Change all users comments to read for admin.',
            'callback' => 'Drupal\move_services_new\System\Resources\CommentDefinition::setReadCommentAll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/CommentDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  /**
   * Callback function for retrieve comments by node.
   *
   * @param int $nid
   *   Node id.
   *
   * @return mixed
   *   Lists of comments.
   */
  public static function retrieve($nid) {
    $request = new Comments($nid);
    return $request->retrieve();
  }

  /**
   * Callback function for create a log form node.
   *
   * @param array $data
   *   The nid of the node to retrieve log.
   *
   * @return int
   *   Comment id.
   */
  public static function create($data = array()) {
    if (isset($data['data']['nid'])) {
      $comment = new Comments($data['data']['nid']);
      return $comment->create($data['data']);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Callback function for get all comments from all requests by uid.
   */
  public static function getAllComments() {
    $comments = new Comments();
    return $comments->getAllCommentsByUid();
  }

  /**
   * Callback function to get all new comments by uid.
   *
   * @return array|bool
   */
  public static function getAllNewComments() {
    $comments = new Comments();
    return $comments->getAllNewComments();
  }

  /**
   * Callback function to set comment status to read.
   *
   * @param int $cid
   *   Comment IDs.
   *
   * @return bool
   *   Status operation: TRUE - comment status is set to read, FALSE - otherwise.
   */
  public static function setReadComment($cid) {
    $result = FALSE;

    $comment = entity_load('comment', array((int) $cid));
    if ($comment) {
      $comments = new Comments($comment[(int) $cid]->nid);
      $result = $comments->setReadComment($comment[(int) $cid]);
    }

    return $result;
  }

  /**
   * Change all users comments to read for admin.
   *
   * @return bool
   */
  public static function setReadCommentAll() {
    $comments = new Comments();
    return $comments->setReadCommentAll();
  }

  /**
   * Callback function to get count unread messages by user.
   *
   * @return int
   *   Count unread messages.
   */
  public static function unreadCount() {
    $comments = new Comments();
    return $comments->getUnreadCountUser();
  }

  /**
   * Callback function for delete comment.
   *
   * @param int $cid
   *   Comment id.
   */
  public static function delete($cid) {
    if ((int) $cid) {
      $comment = new Comments(NULL, (int) $cid);
      $comment->delete();
    }
  }

  /**
   * Callback function to update comment.
   *
   * @param array $data
   */
  public static function update($cid, $data = array()) {
    if (isset($data['data']['message'])) {
      $comment = new Comments(NULL, (int) $cid);
      $comment->update($data['data']);
    }
  }

  /**
   * Callback function to get last 100 requests(nids) with comments.
   */
  public static function adminAllComments() {
    $comment = new Comments();
    return $comment->getAdminAllNodesWithComments();
  }

}
