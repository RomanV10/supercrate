<?php

namespace Drupal\move_services_new\System\Resources;

use Drupal\move_branch\Services\Trucks;
use Drupal\move_dispatch\ValidationRules\AddNotes;
use Drupal\move_services_new\Receipts\Tasks\ReceiptTasks;
use Drupal\move_services_new\Services\ExtraServices;
use Drupal\move_services_new\Services\move_request\MoveRequestSearch;
use Drupal\move_services_new\Services\move_request\MoveRequestHomeEstimatorSearch;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\Parklot;
use Drupal\move_services_new\Services\Receipt;
use Drupal\move_services_new\Services\Sales;

/**
 * Class MoveRequestDefinition.
 *
 * @package Drupal\move_services_new\System\Resources
 */
class MoveRequestDefinition {

  /**
   * Schema definition.
   *
   * @return array
   *   Schema.
   */
  public static function getDefinition() {
    return array(
      'move_request' => array(
        'operations' => array(
          'retrieve' => array(
            'help' => 'Retrieve a request info',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => FALSE,
                'source' => array('path' => 0),
                'type' => 'int',
                'description' => 'The nid of the node to retrieve',
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'index' => array(
            'help' => 'Fields of all nodes of move_request type',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 0.',
                'default value' => 0,
                'source' => array('param' => 'page'),
              ),
              array(
                'name' => 'pagesize',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('param' => 'pagesize'),
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'delete' => array(
            'help' => 'Delete a node of move_request type.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'description' => 'The id of the user to delete',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('delete any move_request content'),
          ),
          'create' => array(
            'help' => 'Create a new move_request',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of new content.',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('create move_request content'),
          ),
          'update' => array(
            'help' => 'Update a move_request node type',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'description' => 'Node id',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The node object with updated information',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('create move_request content'),
          ),
        ),
        'actions' => array(
          'search' => array(
            'help' => 'Search move requests',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::search',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'optional' => FALSE,
                'type' => 'array',
                'description' => 'Conditions for search of nodes',
                'default value' => array(),
                'source' => 'data',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'open_jobs' => array(
            'help' => 'Search move requests',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::openJobs',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'optional' => FALSE,
                'type' => 'array',
                'description' => 'Conditions for search of nodes',
                'default value' => array(),
                'source' => 'data',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'requests' => array(
            'help' => 'Get requests for admin',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::requests',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'optional' => FALSE,
                'type' => 'array',
                'description' => 'Conditions for search of nodes',
                'default value' => array(),
                'source' => 'data',
              ),
            ),
            'access arguments' => array('edit any move_request content'),
          ),
          'storage_request' => array(
            'help' => 'Get storage request',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::storageRequests',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'optional' => TRUE,
                'type' => 'array',
                'description' => 'Create two move_request',
                'source' => 'data',
              ),
            ),
            'access arguments' => array('create move_request content'),
          ),
          'managers' => array(
            'help' => 'Get list managers',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::managers',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'access arguments' => array('edit any move_request content'),
          ),
          'request_manager' => array(
            'help' => 'Get manager from request',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::requestManager',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'description' => 'Node id',
                'source' => array('path' => 1),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('edit any move_request content'),
          ),
          'assign_manager' => array(
            'help' => 'Assign manager to request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::requestAssignManager',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => '',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit any move_request content'),
          ),
          'flatrate' => array(
            'help' => 'Get flatrate requests.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::requestFlatrate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'type' => 'array',
                'description' => '',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_options_for_pickup' => array(
            'help' => 'Get option for pickup',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getOptionsForPickup',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'options',
                'type' => 'int',
                'description' => 'Options for pickup',
                'source' => array('data' => 'options'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'requestId',
                'type' => 'int',
                'description' => 'Request id',
                'source' => array('data' => 'requestId'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'get_contract' => array(
            'help' => 'Get contract for request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getContract',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'description' => 'Request id',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'get_card' => array(
            'help' => 'Get Credit card.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getCard',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('get credit card slip'),
          ),
          'set_card' => array(
            'help' => 'Set Credit card.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::setCard',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('path' => 1),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'set_contract' => array(
            'help' => 'Set contract for request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::setContract',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'description' => 'Request id',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'data',
                'description' => 'Json array',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'set_payment' => array(
            'help' => 'Set payment data for request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::setExtraServices',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('set_payment request'),
          ),
          'get_payment' => array(
            'help' => 'Get payment data for request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getPayment',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'set_receipt' => array(
            'help' => 'Set receipt\'s data for request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::setReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => 0,
                'source' => array('data' => 'entity_type'),
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update_receipt' => array(
            'help' => 'Set receipt\'s data for request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::updateReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_receipt' => array(
            'help' => 'Get receipt data for request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => 0,
                'source' => array('data' => 'entity_type'),
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete_receipt' => array(
            'help' => 'Get receipt data for request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::deleteReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'rid',
                'description' => 'Receipt id.',
                'type' => 'int',
                'source' => array('data' => 'rid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'create_editable' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::createEditable',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'delete_editable' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::deleteEditable',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'check_editable' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::checkEditable',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'get_foreman_helper_requests' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getForemanHelperRequests',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'type' => 'string',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'create_flag' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::createFlag',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'value',
                'type' => 'array',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'string',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'remove_flag' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::removeFlag',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'string',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'edit_flag' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::editFlag',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'string',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'string',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_flags' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getFlags',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'access arguments' => array('access content'),
          ),
          'search_receipt' => array(
            'help' => 'Search move requests by receipt',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::searchReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'optional' => FALSE,
                'type' => 'array',
                'description' => 'Conditions for search of nodes',
                'default value' => array(),
                'source' => array('data' => 'data'),
              ),
              array(
                'name' => 'entity_type',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => 0,
                'source' => array('data' => 'entity_type'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'filter_receipts' => array(
            'help' => 'Return receipts with filters and sort',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::filterReceipts',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'filters',
                'optional' => TRUE,
                'type' => 'array',
                'description' => 'Conditions for filtering receipts',
                'default value' => array(),
                'source' => array('data' => 'filters'),
              ),
              array(
                'name' => 'sort',
                'optional' => TRUE,
                'type' => 'array',
                'description' => 'Conditions for sort receipts',
                'default value' => array(),
                'source' => array('data' => 'sort'),
              ),
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Conditions for sort receipts',
                'default value' => 0,
                'source' => array('data' => 'page'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_dashboard' => array(
            'help' => 'Get all data for dashboard',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getDashboard',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_super_request' => array(
            'help' => 'Get all request data for dashboard',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getSuperRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => FALSE,
                'source' => array('path' => 1),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_parklot' => array(
            'help' => 'Get parklot',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getParklot',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'optional' => FALSE,
                'source' => array('data' => 'date'),
                'type' => 'string',
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'source' => array('data' => 'condition'),
                'type' => 'array',
                'default value' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'set_parklot_note' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::addParklotNote',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'rules' => AddNotes::class,
            'args' => array(
              array(
                'name' => 'date',
                'type' => 'string',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'note',
                'type' => 'int',
                'source' => array('data' => 'note'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_parklot_note' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getParklotNote',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'rules' => AddNotes::class,
            'args' => array(
              array(
                'name' => 'date',
                'type' => 'string',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_all_branch_parklot' => array(
            'help' => 'Get all branch parklot',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getAllBranchParklot',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'optional' => FALSE,
                'source' => array('data' => 'date'),
                'type' => 'string',
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'source' => array('data' => 'condition'),
                'type' => 'array',
                'default value' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_month_parklot' => array(
            'help' => 'Get parklot',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getMonthParklot',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'optional' => FALSE,
                'source' => array('data' => 'date'),
                'type' => 'string',
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'source' => array('data' => 'condition'),
                'type' => 'array',
                'default value' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_count_estimate_schedule' => array(
            'help' => 'Get count home estimate request with status schedule',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getCountEstimateSchedule',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'access arguments' => array('access content'),
          ),
          'get_user_for_estimate' => array(
            'help' => 'Get data about home estimate users',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getUserEstimate',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'access arguments' => array('access content'),
          ),
          'set_notes' => array(
            'help' => 'Write note in db.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::setNotes',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => FALSE,
                'source' => array('path' => 1),
                'type' => 'int',
              ),
              array(
                'name' => 'notes',
                'optional' => FALSE,
                'source' => array('data' => 'notes'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_notes' => array(
            'help' => 'Get note in db.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getNotes',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => FALSE,
                'source' => array('path' => 1),
                'type' => 'int',
              ),
              array(
                'name' => 'role_note',
                'optional' => FALSE,
                'source' => array('data' => 'role_note'),
                'type' => 'array',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'check_overbooking' => array(
            'help' => 'Check overbooking.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::checkOverbooking',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => TRUE,
                'source' => array('data' => 'nid'),
                'type' => 'int',
              ),
              array(
                'name' => 'fields',
                'optional' => TRUE,
                'source' => array('data' => 'fields'),
                'type' => 'array',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'clone_request' => array(
            'help' => 'Clone request.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::cloneRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => TRUE,
                'source' => array('data' => 'nid'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'create_draft_move_request' => array(
            'help' => 'Create move request with draft status.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::createDraftMoveRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'change_request_client' => array(
            'help' => 'Change request client.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::changeRequestClient',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'optional' => FALSE,
                'source' => array('data' => 'uid'),
                'type' => 'int',
              ),
              array(
                'name' => 'nid',
                'optional' => FALSE,
                'source' => array('data' => 'nid'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'set_additional_user' => array(
            'help' => 'Change request client.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::setAdditionalUser',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => FALSE,
                'source' => array('data' => 'nid'),
                'type' => 'int',
              ),
              array(
                'name' => 'account',
                'optional' => FALSE,
                'source' => array('data' => 'account'),
                'type' => 'array',
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_foreman_request' => array(
            'help' => 'Get foreman requests.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getForemanRequests',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'optional' => FALSE,
                'source' => array('data' => 'uid'),
                'type' => 'int',
              ),
              array(
                'name' => 'conditions',
                'optional' => TRUE,
                'source' => array('data' => 'conditions'),
                'type' => 'array',
                'default value' => array(),
              ),
              array(
                'name' => 'sorting',
                'optional' => TRUE,
                'source' => array('data' => 'sorting'),
                'type' => 'array',
                'default value' => array('field_date_value' => 'DESC'),
              ),
              array(
                'name' => 'type',
                'optional' => TRUE,
                'source' => array('data' => 'type'),
                'type' => 'string',
                'default value' => 'new',
              ),
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'source' => array('data' => 'currentPage'),
                'type' => 'int',
                'default value' => 1,
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'source' => array('data' => 'perPage'),
                'type' => 'int',
                'default value' => 15,
              ),

            ),
            'access arguments' => array('access content'),
          ),
          'get_home_estimator_request' => array(
            'help' => 'Get home estimator requests.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getHomeEstimatorRequests',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'type',
                'optional' => TRUE,
                'source' => array('data' => 'type'),
                'type' => 'int',
                'default value' => MoveRequestHomeEstimatorSearch::NEW_JOBS,
              ),
              array(
                'name' => 'sorting',
                'optional' => TRUE,
                'source' => array('data' => 'sorting'),
                'type' => 'array',
                'default value' => [],
              ),
              array(
                'name' => 'current_page',
                'optional' => TRUE,
                'source' => array('data' => 'currentPage'),
                'type' => 'int',
                'default value' => 1,
              ),
              array(
                'name' => 'per_page',
                'optional' => TRUE,
                'source' => array('data' => 'perPage'),
                'type' => 'int',
                'default value' => MoveRequestHomeEstimatorSearch::PER_PAGE_NUM,
              ),

            ),
            'access arguments' => array('access content'),
          ),
          'set_commercial_move_settings' => array(
            'help' => 'Set Commercial move settings.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::setCommercialMoveSettings',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'value',
                'optional' => FALSE,
                'source' => array('data' => 'value'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'create_commercial_extra_room' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::createCommercialExtraRoom',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'cubic_feet',
                'type' => 'int',
                'source' => array('data' => 'cubic_feet'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_commercial_extra_room' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::getCommercialExtraRoom',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'access arguments' => array('access content'),
          ),
          'update_commercial_extra_room' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::updateCommercialExtraRoom',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'room_id',
                'type' => 'string',
                'source' => array('data' => 'room_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'cubic_feet',
                'type' => 'int',
                'source' => array('data' => 'cubic_feet'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete_commercial_extra_room' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::deleteCommercialExtraRoom',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'room_id',
                'type' => 'string',
                'source' => array('data' => 'room_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'create_packing_day' => array(
            'help' => 'Create packing day.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::createPackingDay',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => TRUE,
                'source' => array('data' => 'nid'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'untie_packing_day' => array(
            'help' => 'Untie packing day.',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::untiePackingDay',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'parent_nid',
                'optional' => FALSE,
                'source' => array('data' => 'parent_nid'),
                'type' => 'int',
              ),
              array(
                'name' => 'child_nid',
                'optional' => FALSE,
                'source' => array('data' => 'child_nid'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'create_crate_move_request' => array(
            'help' => 'Create a new move_request',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveRequestDefinition::createCrateMoveRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveRequestDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of new content.',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('create move_request content'),
          ),
        ),
      ),
    );
  }

  public static function getOptionsForPickup($options, $request_id) {
    if ($options && $request_id) {
      $instance = new MoveRequest();
      $instance->getOptionsForPickup((array) $options, (int) $request_id);
    }
  }

  public static function retrieve($nid) {
    $instance = new MoveRequest($nid);
    return $instance->retrieve();
  }

  public static function index($page, $page_size) {
    $instance = new MoveRequest();
    return $instance->index($page, $page_size);
  }

  public static function delete($nid) {
    $instance = new MoveRequest($nid);
    $instance->delete();
    return TRUE;
  }

  public static function create($request = array()) {
    $instance = new MoveRequest();
    // Fix to make CREATE user.
    return $instance->create2($request, 0);
  }

  public static function update($nid, $request = array()) {
    $instance = new MoveRequest($nid);
    return $instance->update2($request);
  }

  /**
   * Search request.
   *
   * @param array $arguments
   *   Arguments.
   *
   * @return array
   *   Nodes.
   *
   * @throws \SearchApiException
   */
  public static function search(array $arguments) {
    $instance = new MoveRequestSearch($arguments);
    return $instance->search(TRUE);
  }

  public static function openJobs($arguments) {
    $instance = new MoveRequestSearch($arguments);
    return $instance->openJobs();
  }

  public static function requests($arguments) {
    $instance = new MoveRequestSearch($arguments, 1);
    return $instance->requests();
  }

  public static function storageRequests($request = array()) {
    $instance = new MoveRequest();
    return $instance->storageRequestCreate($request);
  }

  public static function managers() {
    return Sales::getManagers();
  }

  public static function requestManager($nid) {
    return MoveRequest::getRequestManager((int) $nid);
  }

  public static function requestAssignManager($data = array()) {
    if (isset($data['nid']) && isset($data['uid'])) {
      return (new Sales($data['nid']))->assingManager($data['uid']);
    }
  }

  public static function requestFlatrate($data = array()) {
    if (isset($data['date'])) {
      $instance = new MoveRequest();
      return $instance->getFlatrateRequests($data['date']);
    }
  }

  public static function getContract($nid) {
    return MoveRequest::getContract($nid);
  }

  public static function setContract($nid, $value) {
    return MoveRequest::setContract($nid, $value);
  }

  public static function setExtraServices($nid, $data) {
    return ExtraServices::setExtraServices($nid, $data);
  }

  public static function getPayment($nid) {
    return ExtraServices::getExtraServices($nid);
  }

  public static function setReceipt($entity_id, $entity_type, $data = array()) {
    return MoveRequest::setReceipt($entity_id, $data, $entity_type);
  }

  public static function updateReceipt($id, $data = array()) {
    return MoveRequest::updateReceipt($id, $data);
  }

  public static function getReceipt($entity_id, $entity_type) {
    return MoveRequest::getReceipt($entity_id, $entity_type);
  }

  public static function deleteReceipt($rid) {
    return MoveRequest::deleteReceipt($rid);
  }

  public static function createEditable($nid, $uid) {
    return MoveRequest::createEditable($nid, $uid);
  }

  public static function deleteEditable($nid) {
    return MoveRequest::deleteEditable($nid);
  }

  public static function checkEditable($nid) {
    return MoveRequest::checkEditable($nid);
  }

  public static function getForemanHelperRequests($type = 'foreman', $uid) {
    $instance = new MoveRequest();
    return $instance->getForHelperRequest($type, $uid);
  }

  public static function createFlag(array $value, string $type = 'flags') {
    $instance = new MoveRequest();
    return $instance->createFlag($value, $type);
  }

  public static function removeFlag($id, $type = 'flags') {
    $instance = new MoveRequest();
    return $instance->removeFlag($id, $type);
  }

  public static function editFlag($id, $value, $type = 'flags') {
    $instance = new MoveRequest();
    $instance->editFlag($id, $value, $type);
  }

  public static function getFlags() {
    return MoveRequest::getAllowedValueFlag();
  }

  public static function searchReceipt($data, $entity_type) {
    return Receipt::receiptSearch($data, $entity_type);
  }

  public static function getCard($nid) {
    return MoveRequest::getCard($nid);
  }

  public static function setCard($nid, $data) {
    return MoveRequest::setCard($nid, $data);
  }

  /**
   * Get data for page dashboard.
   *
   * @return array|mixed
   *   Data: requests, invoices etc.
   *
   * @throws \InvalidMergeQueryException
   * @throws \SearchApiException
   * @throws \ServicesException
   * @throws \Exception
   */
  public static function getDashboard() {
    $instance = new MoveRequest();
    return $instance->getDashBoard();
  }

  public static function getSuperRequest($nid) {
    $instance = new MoveRequest($nid);
    return $instance->getSuperRequest();
  }

  public static function getParklot($date, $condition = array()) {
    return (new Parklot(new MoveRequest()))->get($date, $condition);
  }

  /**
   * @throws \InvalidMergeQueryException
   */
  public static function addParklotNote($date, $note) {
    return Parklot::setNote($date, $note);
  }

  public static function getParklotNote($date) {
    return Parklot::getNote($date);
  }

  public static function getAllBranchParklot($date, $condition = array()) {
    $instance = new Trucks();
    return $instance->getAllParklot($date, $condition);
  }

  public static function getMonthParklot($date, $condition = array()) {
    return (new Parklot(new MoveRequest()))->getByMonth($date, $condition);
  }

  /**
   * Get count request with status estimate.
   *
   * @return mixed
   *   Integer.
   */
  public static function getCountEstimateSchedule() {
    $instance = new MoveRequestSearch();
    return $instance->countEstimateSchedule();
  }

  /**
   * Get information about user who make estimate.
   *
   * @return array
   *   Data about users without settings.
   */
  public static function getUserEstimate() {
    $instance = new MoveRequest();
    return $instance->getUserForEstimate();
  }

  /**
   * Set notes.
   *
   * @param int $nid
   *   Node id.
   * @param array $notes
   *   Body note.
   *
   * @return \DatabaseStatementInterface|int
   *   result.
   */
  public static function setNotes(int $nid, array $notes) {
    $instance = new MoveRequest($nid);
    return $instance->set_notes($notes);
  }

  /**
   * Get note.
   *
   * @param int $nid
   *   Node id.
   * @param mixed $role_note
   *   Ex. sales_note, client_notes.
   *
   * @return object
   *   Result.
   */
  public static function getNotes(int $nid, $role_note = array()) {
    $instance = new MoveRequest($nid);
    return $instance->get_notes($role_note);
  }

  /**
   * @param int $nid
   * @return bool
   */
  public static function checkOverbooking($nid, $fields = array()) {
    return MoveRequest::checkOverbooking($nid, $fields);
  }

  /**
   * Create draft move request.
   *
   * @return int
   *   Request id.
   */
  public static function createDraftMoveRequest() {
    $instance = new MoveRequest();
    return $instance->createDraftMoveRequest();
  }

  /**
   * Method for change request client.
   *
   * @param int $uid
   *   User id.
   * @param int $nid
   *   Node id.
   *
   * @return bool
   */
  public static function changeRequestClient(int $uid, int $nid) {
    $instance = new MoveRequest($nid);
    return $instance->changeRequestClient($uid);
  }

  /**
   * Method for set additional user for move request.
   *
   * @param int $nid
   *   Move request id.
   * @param array $account
   *   User data.
   *
   * @return array
   *   Result set additional user.
   */
  public static function setAdditionalUser(int $nid, array $account) {
    return (new MoveRequest($nid))->setAdditionalUser($account);
  }

  /**
   * Clone request.
   *
   * @param int $nid
   *   Node id.
   *
   * @return array
   *   Custom answer with new nid.
   *
   * @throws \Exception
   */
  public static function cloneRequest(int $nid) {
    $instance = new MoveRequest($nid);
    try {
      $result = $instance->cloneRequest();
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog('Clone Request', $message, array(), WATCHDOG_ERROR);
      return array(
        'status' => 500,
        'statusText' => 'Cannot clone request!',
        'data' => NULL,
      );
    }
    return array(
      'status' => 200,
      'statusText' => 'OK',
      'data' => $result,
    );
  }

  /**
   * Get foreman jobs. New and old.
   *
   * Can be sorted by move_date.
   *
   * @param int $uid
   *   Foreman user id.
   * @param array $conditions
   *   Array with fields to filter.
   * @param array $sorting
   *   Array with fields to sort.
   * @param string $type
   *   Type to show jobs.
   * @param int $current_page
   *   Number of page to show.
   * @param int $per_page
   *   Number request on the page. -1 to show all.
   *
   * @return array
   *   Array of nids with page info or empty array
   */
  public static function getForemanRequests(int $uid, array $conditions = array(),  array $sorting, $type, int $current_page, int $per_page) {
    return (new MoveRequestSearch())->searchForemanRequests($uid, $conditions, $sorting, $type, $current_page, $per_page);
  }


  public static function getHomeEstimatorRequests(int $type, array $sorting, int $current_page, int $per_page) {
    return (new MoveRequestHomeEstimatorSearch())->search($type, $sorting, $current_page, $per_page);
  }


  /**
   * Method for add commercial size of move.
   *
   * @param $value
   * @return bool
   */
  public static function setCommercialMoveSettings($value) {
    return (new MoveRequest())->setCommercialMoveSettings($value);
  }

  /**
   * Create commercial extra room.
   */
  public static function createCommercialExtraRoom(string $name, int $cubic_feet) {
    $instance = new MoveRequest();
    return $instance->createCommercialExtraRoom($name, $cubic_feet);
  }

  /**
   * Get all commercial extra rooms.
   */
  public static function getCommercialExtraRoom() {
    $instance = new MoveRequest();
    return $instance->getCommercialExtraRooms();
  }

  /**
   * Update commercial extra room.
   */
  public static function updateCommercialExtraRoom(int $room_id, string $name, int $cubic_feet) {
    $instance = new MoveRequest();
    return $instance->updateCommercialExtraRoom($room_id, $name, $cubic_feet);
  }

  /**
   * Delete commercial extra room.
   */
  public static function deleteCommercialExtraRoom(int $room_id) {
    $instance = new MoveRequest();
    return $instance->deleteCommercialExtraRoom($room_id);
  }

  /**
   * Create packing day.
   *
   * @param int $nid
   *   Node id.
   *
   * @return array
   *   result.
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public static function createPackingDay(int $nid) {
    return (new MoveRequest($nid))->createPackingDay();
  }

  /**
   * Untie packing day.
   *
   * @param int $parentNid
   *   Parent nid.
   * @param int $childNid
   *   Child nid.
   *
   * @return array
   *   Custom answer.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public static function untiePackingDay(int $parentNid, int $childNid) {
    return (new MoveRequest())->untiePackingDay($parentNid, $childNid);
  }

  /**
   * Create crate move request method.
   *
   * @param array $request
   *   Request data + crate data.
   *
   * @return mixed
   *   Invoice data.
   *
   * @throws \Drupal\move_services_new\Exceptions\ValidationException
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public static function createCrateMoveRequest($request = array()) {
    $instance = new MoveRequest();
    // Fix to make CREATE user.
    return $instance->createCrateMoveRequest($request, 0);
  }

  /**
   * Method get receipts with filters and sort.
   *
   * @param array $filters
   *   Data for filtering.
   *
   * @param array $sort
   *   Data for sorting.
   *
   * @return array
   *   Receipts.
   */
  public static function filterReceipts(array $filters = array(), array $sort = array(), int $page = 0) {
    return (new ReceiptTasks())->filterReceipts($filters, $sort, $page);
  }

}
