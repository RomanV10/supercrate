<?php

namespace Drupal\move_services_new\System\Resources;

use Drupal\move_services_new\Services\Inventory;

class MoveInvertoryDefinition {

  public static function getDefinition() {
    return array(
      'move_invertory' => array(
        'operations' => array(
          'retrieve' => array(
            'help' => 'Retrieve invertory',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveInvertoryDefinition::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveInvertoryDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => FALSE,
                'source' => array('path' => 0),
                'type' => 'int',
                'description' => 'The nid of the node to retrieve',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'create' => array(
            'help' => 'Create invertory',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveInvertoryDefinition::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveInvertoryDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'description' => 'Node(request) id',
                'source' => array('path' => 0),
                'optional' => TRUE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'Invertory in request for update.',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
          'update' => array(
            'help' => 'Update invertory',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveInvertoryDefinition::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveInvertoryDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'description' => 'Node(request) id',
                'source' => array('path' => 0),
                'optional' => TRUE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'Invertory in request for update.',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('edit own move_request content'),
          ),
        ),
        'actions' => array(
          'get_invertory' => array(
            'help' => 'Get all invertory objects',
            'callback' => 'Drupal\move_services_new\System\Resources\MoveInvertoryDefinition::getInventory',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/MoveInvertoryDefinition',
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  /**
   * Move invertory service: retrieve operation.
   *
   * @param int $nid
   *   Node ID.
   *
   * @return array
   */
  public static function retrieve($nid) {
    $invertory = new Inventory($nid);
    return $invertory->retrieve();
  }

  /**
   * Move invertory service: create operation.
   *
   * @param int $nid
   *   Node id.
   * @param array $data
   *
   * @return bool
   *   Status of operation.
   */
  public static function create($nid, $data = array()) {
    return self::update($nid, $data);
  }

  /**
   * Move invertory service: update operation.
   *
   * @param int $nid
   *   Node Id.
   * @param array $data
   *
   * @return bool
   *    Result of operation.
   */
  public static function update($nid, $data = array()) {
    if ($nid && $data) {
      $invertory = new Inventory((int) $nid);
      return $invertory->update($data['data']);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Callback to get invertory objects.
   */
  public static function getInventory() {
    $invertory = new Inventory();
    return $invertory->getInvertoryObjects();
  }

}
