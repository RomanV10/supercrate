<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TripStatus.
 *
 * @package Drupal\long_distance\Util
 */
class RequestServiceType extends BaseEnum {

  const MOVING = 1;
  const MOVING_AND_STORAGE = 2;
  const LOADING_HELP = 3;
  const UNLOADING_HELP = 4;
  const FLAT_RATE = 5;
  const OVERNIGHT = 6;
  const LONG_DISTANCE = 7;
  const PACKING_DAY = 8;
}