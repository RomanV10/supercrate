<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class FrequencyType.
 *
 * @package Drupal\move_services_new\Util
 */
class FrequencyType extends BaseEnum {

  const DAILY = 0;
  const WEEKLY = 1;
  const MONTHLY = 2;

}
