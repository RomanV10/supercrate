<?php

namespace Drupal\move_services_new\System;

/**
 * Class for of additional actions for manipulating of data.
 */
class Extra {

  // Block size for encryption block cipher.
  private static $ENCRYPT_BLOCK_SIZE = 200;

  // Block size for decryption block cipher.
  private static $DECRYPT_BLOCK_SIZE = 256;

  /**
   * Set date to first time of select day.
   *
   * @param string $date
   *   English textual datetime description.
   *
   * @return int|string
   */
  public static function getDateFrom($date = '', $time_zone = 'UTC') {
    $result = '';

    if ($date) {
      $new_date = new \DateTime((string) $date, new \DateTimeZone($time_zone));
      $new_date->setTime(0, 0, 0);
      $result = $new_date->getTimestamp();
    }

    return $result;
  }

  /**
   * Set date to last minute of select day.
   *
   * @param string $date
   *   English textual datetime description.
   *
   * @return false|int|string
   */
  public static function getDateTo($date = '', $time_zone = 'UTC') {
    $result = '';

    if ($date) {
      date_default_timezone_set($time_zone);
      $result = mktime(23, 59, 59, date('m', strtotime($date)), date('d', strtotime($date)), date('Y', strtotime($date)));
    }

    return $result;
  }

  /**
   * Convert date from string to timestamp.
   *
   * @param string $date
   *   Date string.
   * @param string $time_zone
   *   Time zone.
   *
   * @return int
   *   Timestamp.
   */
  public static function convertDateToTimestamp(string $date, $time_zone = 'UTC') : int {
    $date = new \DateTime($date, new \DateTimeZone($time_zone));
    return $date->getTimestamp();
  }

  public static function getFirstDayCurrentMonth($time_zone = 'UTC') {
    $date = new \DateTime('first day of this month', new \DateTimeZone($time_zone));
    $date->setTime(0, 0, 0);
    return $date->getTimestamp();
  }

  public static function getLastDayCurrentMonth($time_zone = 'UTC') {
    $date = new \DateTime('last day of this month', new \DateTimeZone($time_zone));
    $date->setTime(23, 59, 59);
    return $date->getTimestamp();
  }

  public static function date($format, $time = NULL, $time_zone = 'UTC') {
    $date = new \DateTime();
    $time = is_null($time) ? time() : $time;
    $date->setTimezone(new \DateTimeZone($time_zone))->setTimestamp($time);
    return $date->format($format);
  }

  public static function convertDecimalToDate(float $decimal, string $format = 'm/d/Y', $time_zone = 'UTC') {
    $hours = floor($decimal);
    $minutes = ($decimal - $hours) * 60;
    $date = new \DateTime();
    $date->setTime($hours, $minutes);
    $date->setTimezone(new \DateTimeZone($time_zone));
    return $date->format($format);
  }

  /**
   * Get interval with start date and end date.
   *
   * @param mixed $start_date
   *   Unix time.
   * @param mixed $end_date
   *   Date to compare.
   * @param string $format
   *   Format date.
   * @param string $time
   *   Time: hours:minutes:seconds.
   * @param string $timezone
   *   Time zone.
   *
   * @return bool|\DateInterval
   *   Days count.
   */
  public function getDiffDays($start_date, $end_date, string $format = 'F m, Y', string $time = '23:59:59', string $timezone = 'UTC') {
    $end_date_obj = new \DateTime("$end_date . $time", new \DateTimeZone($timezone));
    $current_date_obj = new \DateTime($start_date, new \DateTimeZone($timezone));
    $diff = $current_date_obj->diff($end_date_obj);
    return $diff;
  }

  /**
   * Check end date more then current date.
   *
   * @param \DateInterval $diff
   *   Diff object without current and end dates.
   *
   * @return bool
   *   True if more.
   */
  public function checkDiffDaysMoreZeroPositive(\DateInterval $diff) {
    return !$diff->invert && $diff->days >= 0;
  }

  /**
   * Get first day of month.
   *
   * @param mixed $date
   *   Date.
   * @param string $format
   *   Return format.
   * @param string $time_zone
   *   Time zone.
   *
   * @return string
   *   Date.
   */
  public static function getFirstDayMonth($date, string $format = 'Y-m', string $time_zone = 'UTC') {
    $date = new \DateTime($date, new \DateTimeZone($time_zone));
    $date->modify('first day of this month');
    $date->setTime(0, 0, 0);

    return $date->format($format);
  }

  /**
   * Get last day of month.
   *
   * @param mixed $date
   *   Date.
   * @param string $format
   *   Return format.
   * @param string $time_zone
   *   Time zone.
   *
   * @return string
   *   Date.
   */
  public static function getLastDaytMonth($date, string $format = 'Y-m', string $time_zone = 'UTC') {
    $date = new \DateTime($date, new \DateTimeZone($time_zone));
    $date->modify('last day of this month');
    $date->setTime(23, 59, 59);

    return $date->format($format);
  }

  /**
   * Check string is json format.
   *
   * @param mixed $string
   *   String.
   *
   * @return bool|mixed
   *   True.
   */
  public static function jsonValidate($string) {
    $result = json_decode($string);

    // Switch and check possible JSON errors.
    switch (json_last_error()) {
      case JSON_ERROR_NONE:
        $error = '';
        break;

      case JSON_ERROR_DEPTH:
        $error = 'The maximum stack depth has been exceeded.';
        break;

      case JSON_ERROR_STATE_MISMATCH:
        $error = 'Invalid or malformed JSON.';
        break;

      case JSON_ERROR_CTRL_CHAR:
        $error = 'Control character error, possibly incorrectly encoded.';
        break;

      case JSON_ERROR_SYNTAX:
        $error = 'Syntax error, malformed JSON.';
        break;

      case JSON_ERROR_UTF8:
        $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
        break;

      case JSON_ERROR_RECURSION:
        $error = 'One or more recursive references in the value to be encoded.';
        break;

      case JSON_ERROR_INF_OR_NAN:
        $error = 'One or more NAN or INF values in the value to be encoded.';
        break;

      case JSON_ERROR_UNSUPPORTED_TYPE:
        $error = 'A value of a type that cannot be encoded was given.';
        break;

      default:
        $error = 'Unknown JSON error occured.';
        break;
    }

    if ($error !== '') {
      watchdog('Json validate', $error, [], WATCHDOG_WARNING);
      $result = FALSE;
    }

    return $result;
  }

  public static function getStateList() {
    return array(
      'AL' => "Alabama",
      'AK' => "Alaska",
      'AZ' => "Arizona",
      'AR' => "Arkansas",
      'CA' => "California",
      'CO' => "Colorado",
      'CT' => "Connecticut",
      'DE' => "Delaware",
      'DC' => "District Of Columbia",
      'FL' => "Florida",
      'GA' => "Georgia",
      'HI' => "Hawaii",
      'ID' => "Idaho",
      'IL' => "Illinois",
      'IN' => "Indiana",
      'IA' => "Iowa",
      'KS' => "Kansas",
      'KY' => "Kentucky",
      'LA' => "Louisiana",
      'ME' => "Maine",
      'MD' => "Maryland",
      'MA' => "Massachusetts",
      'MI' => "Michigan",
      'MN' => "Minnesota",
      'MS' => "Mississippi",
      'MO' => "Missouri",
      'MT' => "Montana",
      'NE' => "Nebraska",
      'NV' => "Nevada",
      'NH' => "New Hampshire",
      'NJ' => "New Jersey",
      'NM' => "New Mexico",
      'NY' => "New York",
      'NC' => "North Carolina",
      'ND' => "North Dakota",
      'OH' => "Ohio",
      'OK' => "Oklahoma",
      'OR' => "Oregon",
      'PA' => "Pennsylvania",
      'RI' => "Rhode Island",
      'SC' => "South Carolina",
      'SD' => "South Dakota",
      'TN' => "Tennessee",
      'TX' => "Texas",
      'UT' => "Utah",
      'VT' => "Vermont",
      'VA' => "Virginia",
      'WA' => "Washington",
      'WV' => "West Virginia",
      'WI' => "Wisconsin",
      'WY' => "Wyoming",
    );
  }

  public static function allowedStates() {
    return array(
      'AL' => 'AL',
      'AK' => 'AK',
      'AZ' => 'AZ',
      'AR' => 'AR',
      'CA' => 'CA',
      'CO' => 'CO',
      'CT' => 'CT',
      'DE' => 'DE',
      'FL' => 'FL',
      'GA' => 'GA',
      'HI' => 'HI',
      'ID' => 'ID',
      'IL' => 'IL',
      'IN' => 'IN',
      'IA' => 'IA',
      'KS' => 'KS',
      'KY' => 'KY',
      'LA' => 'LA',
      'ME' => 'ME',
      'MD' => 'MD',
      'MA' => 'MA',
      'MI' => 'MI',
      'MN' => 'MN',
      'MS' => 'MS',
      'MO' => 'MO',
      'MT' => 'MT',
      'NE' => 'NE',
      'NV' => 'NV',
      'NH' => 'NH',
      'NJ' => 'NJ',
      'NM' => 'NM',
      'NY' => 'NY',
      'NC' => 'NC',
      'ND' => 'ND',
      'OH' => 'OH',
      'OK' => 'OK',
      'OR' => 'OR',
      'PA' => 'PA',
      'RI' => 'RI',
      'SC' => 'SC',
      'SD' => 'SD',
      'TN' => 'TN',
      'TX' => 'TX',
      'UT' => 'UT',
      'VT' => 'VT',
      'VA' => 'VA',
      'WA' => 'WA',
      'WV' => 'WV',
      'WI' => 'WI',
      'WY' => 'WY',
      'DC' => 'DC',
    );
  }

  public static function base64Detect(string $data) : bool {
    $result = FALSE;
    if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Find all needed properties on object.
   *
   * @param $object
   *   Object where will be find properties.
   * @param array $properties
   *   Lists of properties.
   *
   * @return bool
   *   Result of operation.
   */
  public static function findProperties($object, array $properties = array()) : bool {
    $result = TRUE;
    foreach ($properties as $property_name) {
      if (!property_exists($object, $property_name)) {
        $result = FALSE;
      }
    }

    return $result;
  }

  /**
   * @param $plain_data
   * @param $private_pem_key
   * @return string
   */
  public static function encrypt_RSA($plain_data, $private_pem_key) : string {
    $encrypted = '';
    $plain_data = str_split($plain_data, static::$ENCRYPT_BLOCK_SIZE);
    foreach($plain_data as $chunk) {
      $partialEncrypted = '';
      // Using for example OPENSSL_PKCS1_PADDING as padding.
      $encryptionOk = openssl_private_encrypt($chunk, $partialEncrypted, $private_pem_key, OPENSSL_PKCS1_PADDING);

      // Also you can return and error. If too big this will be false.
      if($encryptionOk === false) {
        return false;
      }

      $encrypted .= $partialEncrypted;
    }

    return base64_encode($encrypted);
  }


  /**
   * @param $public_pem_key
   * @param $data
   * @return string
   */
  public static function decrypt_RSA($public_pem_key, $data) : string {
    $decrypted = '';
    // Decode must be done before spliting for getting the binary String.
    $data = str_split(base64_decode($data), static::$DECRYPT_BLOCK_SIZE);

    foreach($data as $chunk) {
      $partial = '';

      // Be sure to match padding.
      $decryptionOK = openssl_public_decrypt($chunk, $partial, $public_pem_key, OPENSSL_PKCS1_PADDING);

      // Here also processed errors in decryption. If too big this will be false.
      if($decryptionOK === false) {
        return false;
      }

      $decrypted .= $partial;
    }

    return $decrypted;
  }

  /**
   * Get time zone.
   *
   * @return string
   *   Time zone.
   */
  public static function getTimezone() {
    return empty($_ENV['MOVE_GOOGLE_CALENDAR_TIMEZONE']) ? 'UTC' : $_ENV['MOVE_GOOGLE_CALENDAR_TIMEZONE'];
  }

  /**
   * Get custom modules schema version.
   *
   * @return array
   *   Modules with their versions.
   */
  private function getCustomModulesSchemaVersion() {
    $versions = [];
    $query = db_select('system', 's')
      ->fields('s', ['name', 'schema_version'])
      ->condition('s.type', 'module')
      ->condition('s.name', '%' . db_like('move') . '%', 'LIKE')
      ->execute();
    foreach ($query as $row) {
      $versions[$row->name] = $row->schema_version;
    }

    return $versions;
  }

  /**
   * Set hard code version of  last update from install file.
   *
   * @return array
   *   Modules with their version.
   */
  private function getCustomModuleLastUpdateVersion() {
    return [
      'movecalc_site_settings' => 7001,
      'move_acrossdomain_auth' => 7101,
      'move_admin_logs' => 0,
      'move_arrivy' => 7101,
      'move_branch' => 7101,
      'move_calculator' => 0,
      'move_centrifugo' => 0,
      'move_coupon' => 7101,
      'move_delay_launch' => 0,
      'move_dispatch' => 0,
      'move_distance_calculate' => 7102,
      'move_dotenv' => 0,
      'move_faq' => 7101,
      'move_features' => 0,
      'move_global_search' => 0,
      'move_google_api' => 7102,
      'move_granot' => 0,
      'move_inventory' => 7116,
      'move_invoice' => 7101,
      'move_long_distance' => 7153,
      'move_mail' => 7105,
      'move_misc' => 0,
      'move_mpdf' => 7103,
      'move_new_log' => 7108,
      'move_notification' => 7116,
      'move_parking' => 7109,
      'move_parser' => 7102,
      'move_profit_loses' => 7103,
      'move_quickbooks' => 7101,
      'move_reminders' => 7102,
      'move_request_score' => 7105,
      'move_reviews' => 7101,
      'move_sales_log' => 0,
      'move_services_new' => 7240,
      'move_speedy' => 7101,
      'move_statistics' => 7103,
      'move_storage' => 7114,
      'move_template_builder' => 7103,
      'move_timer' => 7001,
      'move_zip_codes' => 7100,
    ];
  }

  /**
   * Compare module last update version with their schema version.
   *
   * @return array
   *   Module name.
   */
  public function compareUpdateAndSchemaVersions() {
    $result = [];
    $schemaVersion = $this->getCustomModulesSchemaVersion();
    $updateVersion = $this->getCustomModuleLastUpdateVersion();

    foreach ($schemaVersion as $module => $version) {
      if ($updateVersion[$module] != $version) {
        $result[$module] = "Update: {$updateVersion[$module]} vs Schema: {$version}";
      }
      if ($version == '-1') {
        drupal_set_installed_schema_version($module, 0);
      }
    }

    return $result;
  }

  /**
   * Convert phone number to needed format.
   *
   * Keep only numbers.
   *
   * @param string $phone_number
   *   Phone number.
   *
   * @return null|string|string[]
   *   Converted phone.
   */
  public static function phoneNumberConvert(string $phone_number) {
    $phone_number = preg_replace('/[^0-9.]+/', '', $phone_number);
    if (mb_strlen($phone_number) > 10) {
      $phone_number = mb_substr($phone_number, 1);
    }

    return $phone_number;
  }

}
