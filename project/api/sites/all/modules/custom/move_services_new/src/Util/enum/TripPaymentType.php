<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TemplateType.
 *
 * @package Drupal\move_services_new\Util
 */
class TripPaymentType extends BaseEnum {

  const RECEIVED = 1;
  const PAID = 2;
  const NEW_JOB = 3;
  const TP_COLLECTED = 4;

}
