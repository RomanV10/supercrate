<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class Roles.
 *
 * @package Drupal\move_services_new\Util
 */
class PaymentMethod extends BaseEnum {

  const CREDIT_CARD = 1;
  const CASH = 2;
  const CHECK = 3;
  const CUSTOM = 4;

}