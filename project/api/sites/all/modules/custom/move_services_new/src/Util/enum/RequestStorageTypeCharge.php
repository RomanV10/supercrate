<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RequestStorageTypeCharge.
 *
 * @package Drupal\move_storage\Services
 */
class RequestStorageTypeCharge extends BaseEnum {

  const CHARGE = 0;
  const RECURRING_CHARGE = 1;
  const FEE_CHARGE = 2;

}
