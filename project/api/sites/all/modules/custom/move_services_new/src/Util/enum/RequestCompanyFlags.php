<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RequestCompanyFlags.
 *
 * @package Drupal\move_services_new\Util
 */
class RequestCompanyFlags extends BaseEnum {

  const TEAM_ASSIGNED = 1;
  const CONFIRMED_BY_CUSTOMER = 2;
  const COMPLETED = 3;
  const PAYROLL_DONE = 4;
  const PICKUP_COMPLETED = 5;
  const DELIVERY_COMPLETED = 6;
  const PICKUP_PAYROLL_DONE = 7;
  const DELIVERY_PAYROLL_DONE = 8;

}
