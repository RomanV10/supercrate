<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_branch\Services\Branch;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\Enums;
use Drupal\move_branch\Services\Trucks;

/**
 * Class for Front page.
 *
 * @SWG\Info(title="Boston Best Rate API", version="0.9")
 * @SWG\Definition(@SWG\Xml(name="Front"))
 */
class Front extends BaseService {
  /**
   * {@inheritdoc}
   */
  public function create($data = array()) {}

  public function retrieve() {}

  /**
   * {@inheritdoc}
   */
  public function update($data = array()) {}

  /**
   * {@inheritdoc}
   */
  public function delete() {}

  /**
   * {@inheritdoc}
   */
  public function index() {}

  /**
   * @SWG\Post(
   * path="/server/front/frontpage",
   * tags={"Front"},
   * description="Get data array from frontpage.",
   * @SWG\Response(response="200", description="successful operation")
   * )
   */
  public function frontpage() {
    $table = new Settings();
    $branch = new Branch();

    $front_page = array();
    $front_page['calcsettings'] = variable_get('calcsettings', array());
    $settings = json_decode(variable_get('basicsettings', array()));
    $settings->mail_domain = variable_get('exim_domain', '');
    $front_page['contract_page'] = variable_get('contract_page', array());
    $front_page['basicsettings'] = json_encode($settings);
    $front_page['longdistance'] = variable_get('longdistance', array());
    $front_page['reviews_settings'] = variable_get('reviews_settings', array());
    $front_page['customized_text'] = variable_get('customized_text', array());
    $front_page['settingsclosestbranch'] = variable_get('settingsclosestbranch', 0);
    $front_page['updater_settings'] = variable_get('updater_settings', array());
    $front_page['inventorySetting'] = json_decode(variable_get('inventorySetting', ''));
    $front_page['calendartype'] = array(
      '0' => 'Discount',
      '1' => 'Regular',
      '2' => 'SubPeak',
      '3' => 'Peak',
      '4' => 'HighPeak',
      '5' => 'Block this day',
    );

    $front_page['packing_settings'] = variable_get('packing_settings', array());
    $front_page['schedulesettings'] = variable_get('scheduleSettings', array());

    $front_page['is_common_branch_truck'] = variable_get('is_common_branch_truck', FALSE);

    $front_page['allowedstate'] = Extra::allowedStates();
    $front_page['calendar'] = Settings::getPriceCalendar();
    $front_page['prices_array'] = $table->getPriceTable();
    $front_page['branches'] = $branch->index();
    $front_page['current_branch'] = variable_get('current_branch');

    $query = "SELECT COUNT(*) amount FROM {node} n WHERE n.type = :type";
    $result = db_query($query, array(':type' => 'move_request'))->fetch();
    $front_page['move_requests_total'] = 55555 + $result->amount;
    $front_page['field_lists'] = static::getFieldListsValue();
    $front_page['taxonomy'] = $this->getTaxonomyTerms();

    $front_page['email_rules'] = array(
      'new_request_local',
      'new_request_ld',
      'new_request_storage',
      'new_request_flat_rate',
      'new_request_new_user',
      'restore_password',
      'new_message',
      'reminder_notconfirmed',
      'move_reminder_1',
      'move_reminder_5',
      'flat_rate_submitted',
      'choose_options',
    );
    $front_page['enums'] = Enums::getEnums();
    $front_page['commercial_move_size_setting'] = variable_get('commercial_move_size_setting', NULL);
    $front_page['commercial_extra_rooms'] = MoveRequest::getCommercialExtraRooms();
    $front_page['leadscoringsettings'] = variable_get('leadscoringsettings', "{}");
    $front_page['centrifugo_settings'] = static::getCentrifugoSettings();
    $front_page['timeZone'] = $this->getTimeZones();
    $front_page['progressbarsettings'] = variable_get('progressbarsettings', json_encode(move_settings_load_from_file('progressBarSettings.json')));
    $front_page['allowToPendingInfo'] = variable_get('allowToPendingInfo', '1');
    $front_page['stateList'] = Extra::getStateList();
    $front_page['valuationSettings'] = json_decode(variable_get('valuationSettings', ''));
    $front_page['valuation_plan_tables'] = $table->getValuationPlan([
      'percentTable',
      'fixedPriceTable',
      'rateTable',
    ]);

    $front_page['localDispatchSettings'] = json_decode(variable_get('localDispatchSettings', '{}'));
    $front_page['extraSignaturesSettings'] = json_decode(variable_get('extraSignaturesSettings', '{}'));
    $front_page['smsEnable'] = variable_get('smsEnable');
    $front_page['stripeCustomerEmail'] = variable_get('stripeCustomerEmail', '');
    $front_page['stripeCustomerId'] = variable_get('stripeCustomerId', '');
    $front_page['stripeAutoCharge'] = variable_get('stripeAutoCharge', 0);
    $front_page['stripeCustomerCardLast4'] = variable_get('stripeCustomerCardLast4', '');
    $front_page['stripeCustomerCardBrand'] = variable_get('stripeCustomerCardBrand', '');

    return $front_page;
  }

  public static function getCentrifugoSettings() : array {
    return array(
      'centrifugo_url' => variable_get('centrifugo_url', 'socket.themoveboard.com:9095'),
      'centrifugo_namespace' => variable_get('centrifugo_namespace', ''),
      'centrifugo_token' => variable_get('centrifugo_token', ''),
    );
  }

  /**
   * Helper function to get values of field type list.
   *
   * @return array $result
   *   Lists of fields.
   */
  public static function getFieldListsValue() {
    $result = array();

    $fields = array(
      'field_approve',
      'field_size_of_move',
      'field_type_of_entrance_from',
      'field_type_of_entrance_to_',
      'field_start_time',
      'field_move_service_type',
      'field_extra_furnished_rooms',
      'field_poll',
      'field_company_flags',
      'field_commercial_extra_rooms',
    );

    foreach ($fields as $field) {
      $field_info = field_info_field($field);
      $list_values = list_allowed_values($field_info);
      $result[$field] = $list_values;
    }

    return $result;
  }

  private function getTaxonomyTerms() {
    $result = array();
    $taxonomy = array(
      'trucks',
      'inventory_rooms',
      'inventory_items_filter',
    );
    $settings = new Settings();
    foreach ($taxonomy as $dict) {
      $vocabulary = taxonomy_vocabulary_machine_name_load($dict);
      if ($dict == 'trucks') {
        $field_condition = array(
          'field_ld_only' => 0,
        );
        $trucks = $settings->getTrucks($field_condition);
        // If enable branches parklot.
        if (variable_get('is_common_branch_truck', FALSE)) {
          $instance = new Trucks();
          $branch_trucks = $instance->index();
          $trucks += $branch_trucks['data'];
        }

        if (!empty($trucks)) {
          foreach ($trucks as $tid => $val) {
            $result[$dict][$tid] = $val['name'];
          }
        }
      }
      else {
        $terms = taxonomy_get_tree($vocabulary->vid);
        foreach ($terms as $term) {
          $result[$dict][$term->tid] = $term->name;
        }
      }

    }

    return $result;
  }

  /**
   * Get geo names.
   *
   * @param string $params
   *   Params for ex. Boston MA.
   *
   * @return object|\stdClass
   *   Json data.
   */
  public function getGeoNames(string $params) {
    $response_data = new \stdClass();
    $url = 'http://api.geonames.org/postalCodeSearchJSON?placename=/' . urlencode(utf8_encode($params)) . '/&country=USA&country=CAN&maxRows=100&username=roma4ke';
    $options = array(
      'method' => 'GET',
      'headers' => array('Content-Type' => 'application/json', 'accept' => 'application/json'),
    );
    $result = drupal_http_request($url, $options);
    if ($result->code == 200) {
      $response_data = $result->data;
    }
    return $response_data;
  }

  /**
   * Get needed timezone for front from server.
   *
   * For front we need to take timezone with it's wetting from file.
   *
   * @return array
   *   Array with zone name and initial settings.
   */
  public function getTimeZones() : array {
    $result = array();
    // Timezone from server.
    $timeZone = Extra::getTimezone();
    // List of timezones from filefor front.
    $file_path = dirname(__FILE__, 3) . '/tests/data/time_zones.json';
    $file_content = file_get_contents($file_path);
    $zones_from_file = drupal_json_decode($file_content);
    if (!empty($zones_from_file)) {
      foreach ($zones_from_file['zones'] as $zone) {
        $zone_array = explode('|', $zone);
        // Check timezone from server with zone from file.
        // First element from array  - zone name.
        if (!empty($zone_array[0]) && $zone_array[0] == $timeZone) {
          $result['name'] = $timeZone;
          $result['initial'] = $zone;
        }
      }

    }
    return $result;
  }

}
