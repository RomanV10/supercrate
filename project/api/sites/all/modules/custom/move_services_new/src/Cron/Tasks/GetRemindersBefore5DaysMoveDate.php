<?php

namespace Drupal\move_services_new\Cron\Tasks;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class GetRemindersBefore5DaysMoveDate.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
class GetRemindersBefore5DaysMoveDate implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $today_plus_five_days_start_day = date("Y-m-d 00:00:01", strtotime("+5 days"));
    $today_plus_five_days_end_day = date('Y-m-d 23:59:59', strtotime("+5 days"));

    $query = db_select('field_data_field_date', 'fd');
    $query->leftJoin('node', 'n', 'n.nid = fd.entity_id');
    $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id');
    $query->leftJoin('move_services_email_was_sent', 'email_was_sent', 'email_was_sent.nid = fd.entity_id AND email_was_sent.mark = :type', [':type' => 'move_reminder_5']);
    $query->fields('fd', ['entity_id']);
    $query->condition('fd.bundle', 'move_request');
    $query->condition('fd.field_date_value', [$today_plus_five_days_start_day, $today_plus_five_days_end_day], 'BETWEEN');
    $query->condition('fa.field_approve_value', RequestStatusTypes::CONFIRMED, '=');
    $query->isNull('email_was_sent.id');
    $query->groupBy('n.nid');
    $result = $query->execute()->fetchCol();
    if (!empty($result)) {
      $queue = \DrupalQueue::get('move_services_new_send_reminder_before_5days_move_date');
      $queue->createQueue();
      $chunk = array_chunk($result, 3);
      foreach ($chunk as $nids) {
        $data = array();
        foreach ($nids as $nid) {
          $manager_uid = MoveRequest::getRequestManager($nid);
          if ($manager_uid == NULL) {
            $manager_uid = 1;
          }
          $data[$nid] = $manager_uid;
        }

        $queue->createItem($data);
      }
    }
  }

}
