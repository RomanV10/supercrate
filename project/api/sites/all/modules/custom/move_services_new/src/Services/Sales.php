<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_parking\Services\Parking;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\RequestsPermissions;

/**
 * Class Sales.
 *
 * @package Drupal\move_services_new\Services
 */
class Sales {

  private $nid = NULL;
  private $user = NULL;
  private $isManager = FALSE;
  private $isAdmin = FALSE;
  private $timeZone = 'UTC';
  private static $adminRoles = array('sales', 'manager', 'administrator');

  /**
   * Sales constructor.
   *
   * @param int|null $nid
   *   Node id.
   * @param \stdClass|null $user
   *   User object.
   */
  public function __construct(?int $nid = NULL, ?\stdClass $user = NULL) {
    // @TODO need to think other way how to make it work better.
    if (!empty($user)) {
      $this->user = $user;
    }
    else {
      global  $user;
      $this->user = $user;
    }

    $this->nid = (int) $nid;
    $this->timeZone = empty($_ENV['MOVE_GOOGLE_CALENDAR_TIMEZONE']) ? $this->timeZone : $_ENV['MOVE_GOOGLE_CALENDAR_TIMEZONE'];

    // @TODO remove clients class.
    $clients = new Clients($user->uid);
    $this->isManager = $clients->checkUserRoles(array('manager', 'sales'));
    $this->isAdmin = $clients->checkUserRoles(array('administrator', 'owner'));
  }

  /**
   * Helper function to get users by name of role.
   *
   * @param array|string $role_names
   *   Role name.
   * @param bool $flag_active
   *   Active user or Blocked.
   * @param bool $reset_cache
   *   Reset cache.
   *
   * @return array
   *   Lists of users.
   */
  public static function getUsersByRoleName($role_names, bool $flag_active = FALSE, bool $reset_cache = FALSE) {
    $static_name = is_array($role_names) ? reset($role_names) : (string) $role_names;
    $users = &drupal_static($static_name, array(), $reset_cache);

    if (!$users) {
      $role_ids = array();
      if (is_array($role_names)) {
        foreach ($role_names as $role_name) {
          $role = user_role_load_by_name($role_name);
          $role_ids[] = $role->rid;
        }
      }
      elseif ((string) $role_names) {
        $role = user_role_load_by_name($role_names);
        $role_ids[] = $role->rid;
      }

      $query = new \EntityFieldQuery();
      $query->entityCondition('entity_type', 'user');
      $roles_subquery = db_select('users_roles', 'ur');
      $roles_subquery->fields('ur', array('uid'));
      $roles_subquery->condition('rid', $role_ids);
      $query->propertyCondition('uid', $roles_subquery, 'IN');
      if ($flag_active) {
        $query->propertyCondition('status', 1);
      }
      $result = $query->execute();

      if (isset($result['user'])) {
        foreach ($result['user'] as $user) {
          $users[] = $user->uid;
        }
      }
      else {
        $users = array();
      }
    }

    return $users;
  }

  /**
   * Helper function to set lists of all sales person.
   *
   * @param bool $flag_active
   *   Status users: active or all.
   */
  public static function setListSalesPersons($flag_active = FALSE) : void {
    $users_sales = static::getUsersByRoleName(array('sales'), $flag_active);
    $users_manager = static::getUsersByRoleName(array('manager'), $flag_active);
    $users_owner = static::getUsersByRoleName(array('administrator'), $flag_active);
    $list_users = array_merge($users_sales, $users_manager, $users_owner);
    variable_set('list_sales_person', $list_users);
    $auto_sales = self::checkAutoAssignSales($list_users);
    $sales_stack = self::checkSalesTypeService($auto_sales);

    $list = array();
    foreach ($sales_stack as $key => $value) {
      $list[$key] = $value;
    }

    variable_set('list_sales_person_types', $list);
  }

  /**
   * Get list sales persons.
   *
   * @param int $service_type
   *   Service type.
   * @param bool $flag_active
   *   Status users: active or all.
   *
   * @return array
   *   Sales uid.
   */
  public static function getListSalesPersonsCur(int $service_type, bool $flag_active = FALSE) {
    static::setListSalesPersons($flag_active);
    $value = variable_get('list_sales_person_types', array());
    $result = isset($value[$service_type]) ? $value[$service_type] : array();
    return $result;
  }

  /**
   * Check sales option: auto assignment.
   *
   * @param array $sales
   *   User with sales, managers roles.
   *
   * @return array
   *   Sales users.
   */
  public static function checkAutoAssignSales(array $sales) {
    $result = array();
    $auto_assign_sales = array();

    foreach ($sales as $sale) {
      $result[$sale] = Users::getUserSettings($sale);
      if (isset($result[$sale]['autoassignment'])
        && !empty($result[$sale]['autoassignment']['isAutoAssign'])
        && !empty($result[$sale]['employee_is_active'])) {
        $auto_assign_sales[] = $sale;
      }
    }

    return $auto_assign_sales;
  }

  /**
   * Check sales type by services.
   *
   * @param array $sales
   *   Users with role 'sales'.
   *
   * @return array
   *   Users id.
   */
  public static function checkSalesTypeService(array $sales) {
    $sales_settings = array();
    $result = array();
    $move_service_type = field_info_field('field_move_service_type');
    $service_type = list_allowed_values($move_service_type);

    $user_settings = function (int $uid) use (&$sales_settings) {
      $settings = Users::getUserSettings($uid);
      if (isset($settings['autoassignment']) && isset($settings['autoassignment']['services'])) {
        $sales_settings[$uid] = (array) $settings['autoassignment']['services'];
      }
    };

    array_walk($sales, $user_settings);

    foreach ($service_type as $id_flag => $flag) {
      foreach ($sales_settings as $uid => $sales_setting) {
        if (in_array($id_flag, $sales_setting)) {
          $result[$id_flag][] = $uid;
        }
      }
    }

    return $result;
  }

  /**
   * Helper function to get lists of all sales person.
   *
   * @param bool $flag_active
   *   Status users: active or all.
   *
   * @return array|mixed
   *   Users id.
   */
  public static function getListSalesPersons($flag_active = FALSE) {
    if (!variable_get('list_sales_person', array())) {
      static::setListSalesPersons($flag_active);
    }

    return variable_get('list_sales_person');
  }

  /**
   * Get managers list.
   *
   * @param bool $flag_active
   *   Get users with role 'manager'.
   *
   * @return array
   *   Users id.
   */
  public static function getListManagers($flag_active = FALSE) {
    return static::getUsersByRoleName('manager', $flag_active);
  }

  /**
   * Get list owner.
   *
   * @param bool $flag_active
   *   Get users with role 'administrator'.
   *
   * @return array
   *   Users id.
   */
  public static function getListOwners($flag_active = FALSE) {
    return static::getUsersByRoleName('administrator', $flag_active);
  }

  /**
   * Get managers list.
   *
   * @return array
   *   List of managers.
   *
   * @throws \Exception
   */
  public static function getManagers() {
    $managers = array();
    $list_sales = static::getListSalesPersons(TRUE);
    $list_managers = static::getListManagers(TRUE);
    $list_owners = static::getListOwners(TRUE);
    $list_users = array_merge($list_sales, $list_managers, $list_owners);
    $list_users = array_unique($list_users);

    foreach ($list_users as $uid) {
      $user = user_load($uid);

      if ($user) {
        $employee = (new Clients())->getUserFieldsData($uid, FALSE);
        if (isset($employee['settings']['employee_is_active']) && (bool) $employee['settings']['employee_is_active']) {
          $managers[$uid] = $employee;
        }
      }
    }

    return $managers;
  }

  /**
   * Method to assign another manager to request.
   *
   * @param int $uid
   *   Manager's id.
   * @param bool $main
   *   Flag that manager must be assign.
   * @param bool $auto_assign
   *   Flag: auto assign.
   * @param bool $update_cache
   *   Flag: update cache.
   *
   * @return bool
   *   Status of operation.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function assingManager(int $uid, bool $main = TRUE, $auto_assign = FALSE, $update_cache = TRUE) {
    $result = FALSE;
    $nid = $this->nid;

    $is_sales_in_request = db_select('move_request_sales_person', 'mrsp')
      ->fields('mrsp', array('created'))
      ->condition('uid', $uid)
      ->condition('nid', $nid)
      ->execute()
      ->fetchField();

    if ($this->checkAdminRoles()) {
      $request_perm = $this->checkRequestPermission($nid, 'canSignedSales');
      $request_perm2 = $this->checkRequestPermission($nid, 'canAssignUnassignedLeads');

      // Administrator has all permission.
      // Manager need to check permission.
      // @TODO add correct method for check permission.
      if (($this->isAdmin || ($this->isManager && ($request_perm || $request_perm2)) || $auto_assign)) {
        $key = array(
          'nid' => $nid,
          'uid' => $uid,
        );

        if ($main) {
          $key = array(
            'nid' => $nid,
            'main' => 1,
          );
        }

        $write = array(
          'nid' => $nid,
          'uid' => $uid,
          'main' => $main ? 1 : 0,
        );

        if (empty($is_sales_in_request)) {
          $write['created'] = time();
        }

        $result = db_merge('move_request_sales_person')
          ->key($key)
          ->fields($write)
          ->execute();

        $this->updateNodeFieldAssignManager($uid, $update_cache);
        // @TODO. We have different fields on small cache and big cache.
        // So now need to rebuild everything.
        if ($update_cache) {
          Cache::updateCacheData($nid);
        }
      }
    }

    return $result;
  }

  /**
   * Get the last appointed Sales Person.
   *
   * @param int $service_type
   *   Service type.
   *
   * @return array
   *   Current sales person.
   */
  public static function getLastSalesPerson(int $service_type) {
    $result = array();
    $result['current'] = variable_get("current_sales_person_{$service_type}");
    if (!$result['current']) {
      $value = variable_get('list_sales_person_types', array());
      $persons = isset($value[$service_type]) ? $value[$service_type] : array();
      $result['current'] = array_shift($persons);
      $result['first'] = TRUE;
    }

    return $result;
  }

  /**
   * Helper function to set next appointed Sales Person.
   *
   * @param int $service_type
   *   Move request service type.
   *
   * @return int|null
   *   Sales person.
   */
  protected function setNextSalesPerson(int $service_type) {
    $next_sales = NULL;
    $sales = static::getListSalesPersonsCur($service_type);
    $current_sales = static::getLastSalesPerson($service_type);
    if (isset($current_sales['first'])) {
      $next_sales = $current_sales['current'];
    }
    else {
      $next_sales_key = array_search($current_sales['current'], $sales) + 1;
      if (array_key_exists($next_sales_key, $sales)) {
        $next_sales = $sales[$next_sales_key];
      }
      else {
        $next_sales = reset($sales);
      }
    }
    variable_set("old_sales_person_{$service_type}", $current_sales);
    variable_set("current_sales_person_{$service_type}", $next_sales);

    return $next_sales;
  }

  /**
   * Helper function's to set "Appoint Sales Person".
   *
   * @param mixed $previous_sales
   *   Previous manager user id.
   * @param int $service_type
   *   Request service type.
   *
   * @throws \Exception
   */
  public function setSalesPersonAppointed($previous_sales = FALSE, int $service_type = 1) : void {
    if ($previous_sales) {
      $this->user = $previous_sales;
      $this->assingManager($previous_sales->uid, TRUE, TRUE, FALSE);
    }
    else {
      $next_sales_uid = $this->setNextSalesPerson($service_type);
      if ($next_sales_uid) {
        $record = [
          'nid' => $this->nid,
          'uid' => $next_sales_uid,
          'created' => time(),
        ];
        drupal_write_record('move_request_sales_person', $record);

        // Scratch this shit.
        $this->updateNodeFieldAssignManager($next_sales_uid);
      }
    }
  }

  /**
   * Custom update node fields: field_request_manager,field_manager_assign_time.
   *
   * @param int $next_sales_person
   *   Id sales person.
   * @param bool $update_cache
   *   Flag: use update cache or not.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function updateNodeFieldAssignManager(int $next_sales_person, $update_cache = TRUE) {
    $data_for_update = array(
      'field_request_manager' => array(
        'raw' => 'target_id',
        'value' => $next_sales_person,
      ),
      'field_manager_assign_time' => array(
        'raw' => 'value',
        'value' => time(),
      ),
    );

    MoveRequest::updateNodeFieldsOnly($this->nid, $data_for_update, 'move_request', FALSE, $update_cache);
  }

  /**
   * Helper method to detect what user role is admin.
   *
   * @param array|null $roles
   *   Roles list.
   *
   * @return bool
   *   Result of check.
   */
  public function checkAdminRoles(?array $roles = array()) {
    $roles = empty($roles) ? static::$adminRoles : $roles;
    $intersect = FALSE;
    if (is_object($this->user)) {
      $intersect = array_intersect((array) $this->user->roles, $roles);
    }
    return $intersect;
  }

  /**
   * Check request permission.
   *
   * @param int $nid
   *   Node id.
   * @param string $sales_perm
   *   Permission name.
   *
   * @return bool
   *   True if user have permission.
   */
  public function checkRequestPermission(int $nid, string $sales_perm = '') : bool {
    $permission = FALSE;
    $manager_perm = $this->checkManagerPermissions($sales_perm);
    $prog_perm = $this->getProgrammManagerPermission($sales_perm);
    $sales_person = MoveRequest::getRequestManager($nid);

    switch ($prog_perm) {
      case RequestsPermissions::CAN_SEE_OTHER_SALES_LEADS:
      case RequestsPermissions::CAN_SEARCH_OTHER_SALES_LEADS:
        // Can see all leads or see own leads.
        // Can search all leads or search own leads.
        if ($manager_perm || ($sales_person == $this->user->uid)) {
          $permission = TRUE;
        }
        break;

      case RequestsPermissions::CAN_ASSIGN_LEADS_TO_OTHER_SALES:
        // Can assign leads to other sales.
        if ($manager_perm) {
          $permission = TRUE;
        }
        break;

      case RequestsPermissions::CAN_SEE_UNSIGNED_LEADS:
      case RequestsPermissions::CAN_ASSIGN_UNASSIGNED_LEADS:
        // Manager has permission and request have't assigned manager.
        if ($manager_perm && !$sales_person) {
          $permission = TRUE;
        }
        break;

      case RequestsPermissions::CAN_EDIT_OTHER_LEADS:
        // Manager has edit permission or is an manager request's.
        if ($manager_perm || (!$manager_perm && $sales_person == $this->user->uid)) {
          $permission = TRUE;
        }
        break;
    }

    return $permission;
  }

  /**
   * Get manager permission number.
   *
   * @param string $sales_perm
   *   Permission name.
   *
   * @return int
   *   Permission number.
   */
  private function getProgrammManagerPermission(string $sales_perm = '') {
    $permission = 0;
    switch ($sales_perm) {
      case 'canSeeOtherLeads':
        $permission = RequestsPermissions::CAN_SEE_OTHER_SALES_LEADS;
        break;

      case 'canSearchOtherLeads':
        $permission = RequestsPermissions::CAN_SEARCH_OTHER_SALES_LEADS;
        break;

      case 'canEditOtherLeads':
        $permission = RequestsPermissions::CAN_EDIT_OTHER_LEADS;
        break;

      case 'canSeeUnsignedLeads':
        $permission = RequestsPermissions::CAN_SEE_UNSIGNED_LEADS;
        break;

      case 'canSignedSales':
        $permission = RequestsPermissions::CAN_ASSIGN_LEADS_TO_OTHER_SALES;
        break;

      case 'canAssignUnassignedLeads':
        $permission = RequestsPermissions::CAN_ASSIGN_UNASSIGNED_LEADS;
        break;
    }

    return $permission;
  }

  /**
   * Check is user have permission.
   *
   * @param string $sales_perm
   *   Name permission.
   *
   * @return bool
   *   True if user have permission, another false.
   */
  public function checkManagerPermissions(string $sales_perm = '') {
    $result = FALSE;
    $settings = Users::getUserSettings($this->user->uid);
    if ($sales_perm && isset($settings['permissions'][$sales_perm]) && $settings['permissions'][$sales_perm]) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Check what user is manager.
   *
   * @param int $uid
   *   User ID.
   *
   * @return bool
   *   Result of the search in array.
   */
  public static function isManager($uid) {
    $persons = static::getListSalesPersons();
    $search = array_search((int) $uid, $persons);
    return ($search === FALSE) ? 0 : 1;
  }

  /**
   * Check is global user manager of request.
   *
   * @param int $nid
   *   Node id.
   *
   * @return bool
   *   True if manager.
   */
  public function checkIsManagerOfRequest(int $nid) {
    $sales_person = MoveRequest::getRequestManager($nid);
    $result = ($sales_person == $this->user->uid);

    return $result;
  }

  /**
   * Check user access to request.
   *
   * @param int $nid
   *   Node Id.
   * @param array $admin_roles
   *   Admin roles list.
   * @param string $sales_perm
   *   Permission name.
   *
   * @return bool
   *   Result of check.
   */
  public function checkUserPermRequest(int $nid, array $admin_roles = array(), string $sales_perm = '') : bool {
    $admin_roles = empty($roles) ? static::$adminRoles : $admin_roles;
    $result = FALSE;

    try {
      // If user have admin permissions.
      $admin = $this->checkAdminRoles($admin_roles);

      if (!property_exists($this->user, 'uid')) {
        throw new CheckUserPermToRequest("User does not defined");
      }
      if (!empty(Parking::isNodeExist($nid))) {
        // Check foreman and helper.
        $wrapper = entity_metadata_wrapper('node', $nid);
        $foremans = $wrapper->field_foreman->value();
        $foreman_result = FALSE;
        foreach ($foremans as $foreman) {
          if ($foreman && $this->user->uid == $foreman->uid && in_array('foreman', $foreman->roles)) {
            $foreman_result = TRUE;
            break;
          }
        }

        $helper = $wrapper->field_helper->value();
        $helper_result = FALSE;
        foreach ($helper as $helper_u) {
          if ($helper_u && $this->user->uid == $helper_u->uid) {
            $helper_result = TRUE;
            break;
          }
        }

        // If user has sales person roles for this request.
        $sales_person = MoveRequest::getRequestManager($nid);
        $sales = ($sales_person == $this->user->uid);

        // Checking unsigned lead.
        if (!$sales_person) {
          $sales = $this->checkRequestPermission($nid, 'canSeeUnsignedLeads');
        }
        // Checking sales lead permission.
        elseif ($this->isManager && $sales_perm) {
          $sales = $this->checkRequestPermission($nid, $sales_perm);
        }

        // If user is author of this request.
        $user = $this->checkUserIsAuthorRequest($nid);

        $result = $admin || $sales || $user || $foreman_result || $helper_result;
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Comments', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      return $result;
    }
  }

  /**
   * Check author request.
   *
   * @param int $nid
   *   Move request id.
   *
   * @return int
   *   If global user author request return 1.
   */
  private function checkUserIsAuthorRequest(int $nid) {
    return db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('uid', $this->user->uid)
      ->condition('nid', $nid)
      ->range(0, 1)
      ->execute()
      ->rowCount();
  }

}
