<?php

namespace Drupal\move_services_new\Cron\Tasks;

use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class GetRemindersNotChanged2Day.
 *
 * @package Drupal\move_services_new\Cron
 */
class GetRemindersNotChanged2Day implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $today = time();
    $minus_two_days = strtotime("-2 days");

    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id');
    $query->fields('n', array('nid'));
    $query->condition('n.type', 'move_request');
    $query->condition('fa.field_approve_value', [RequestStatusTypes::NOT_CONFIRMED, RequestStatusTypes::FLATE_RATE_WAIT_OPTOINS], 'IN');
    $query->condition('n.changed', [$minus_two_days, $today], 'BETWEEN');
    $result = $query->execute()->fetchCol();
    if (!empty($result)) {
      $queue = \DrupalQueue::get('move_services_new_send_reminder_not_changed_2days');
      $queue->createQueue();
      $chunk = array_chunk($result, 3);
      foreach ($chunk as $item) {
        $queue->createItem($item);
      }
    }
  }

}
