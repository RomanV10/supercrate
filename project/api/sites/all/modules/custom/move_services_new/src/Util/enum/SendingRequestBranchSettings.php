<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class SendingRequestBranchSettings.
 *
 * @package Drupal\move_branch\Services
 */
class SendingRequestBranchSettings extends BaseEnum {

  const BY_CLOSEST_DISTANCE_FROM_PARKING_ZIP_BASE_ON_DRIVING_MILES = 1;
  const BY_CLOSEST_TO_ORIGIN = 2;
  const BY_CLOSEST_TO_DESTINATION = 3;
  const BY_CLOSEST_TO_DESTINATION_AND_ORIGIN = 4;
  const BY_SMALLEST_TRAVEL_TIME = 5;

}