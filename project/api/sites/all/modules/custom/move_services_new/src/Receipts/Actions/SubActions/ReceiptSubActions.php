<?php

namespace Drupal\move_services_new\Receipts\Actions\SubActions;

use Drupal\move_branch\Services\Trucks;
use Drupal\move_inventory\Actions\SubActions\SubActionsInterface;
use Drupal\move_long_distance\Services\LongDistanceJob;
use Drupal\move_services_new\Receipts\Entity\Receipt;
use Drupal\move_services_new\Services\Cache;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\PaymentFlag;
use Drupal\move_services_new\Util\enum\PaymentMethod;
use Drupal\move_services_new\Util\enum\Roles;
use Drupal\move_storage\Services\RequestStorage;
use Drupal\move_services_new\Receipts\Tasks\ReceiptTasks;
use Drupal\move_services_new\Traits\SymfonySerializer;

/**
 * Class ReceiptSubActions.
 *
 * @package Drupal\move_services_new\Receipts\Actions\SubActions
 */
class ReceiptSubActions implements SubActionsInterface {
  use SymfonySerializer;

  private $entityId;
  private $entityType;
  private $id;

  /**
   * Get entity type.
   *
   * @return mixed
   *   Entity type.
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * Get entity id.
   *
   * @return mixed
   *   Entity id.
   */
  public function getEntityId() {
    return $this->entityId;
  }

  /**
   * Set entity id.
   *
   * @param mixed $entityId
   *   Entity id.
   */
  public function setEntityId($entityId): void {
    $this->entityId = $entityId;
  }

  /**
   * Set entity type.
   *
   * @param mixed $entityType
   *   Entity type.
   */
  public function setEntityType($entityType): void {
    $this->entityType = $entityType;
  }

  /**
   * ReceiptSubActions constructor.
   *
   * @param int|null $id
   *   Receipt id.
   */
  public function __construct(?int $id = NULL) {
    $this->id = $id;
    $this->initSerializer();
  }

  /**
   * Create receipt.
   *
   * @param array $data
   *   Receipt data.
   *
   * @return $this|bool|\DatabaseStatementEmpty|\DatabaseStatementInterface|int|mixed
   *   Receipt id.
   *
   * @throws \Exception
   * @throws \ServicesException
   */
  public function create(array $data = []) {
    $receipt = (array) $this->serializer->deserialize(json_encode($data), Receipt::class, 'json');
    $receipt_id = ReceiptTasks::insertReceipt($receipt);

    $receipt['id'] = $receipt_id;
    ReceiptTasks::receiptSolarOperation('create', $receipt);

    switch ($receipt['entity_type']) {
      case EntityTypes::MOVEREQUEST:
        /* If quickbooks connection is present and qbo settings are set to alltime then post every transaction one by one.*/
        ReceiptTasks::sendReceiptToQuickBooks($receipt);

        if ($receipt['payment_flag'] == PaymentFlag::RESERVATION) {
          $node_wrapper = entity_metadata_wrapper('node', $receipt['entity_id']);
          $node_wrapper->field_reservation_price->set(0);
          $node_wrapper->field_reservation_received->set(1);
          if ($receipt['role'] == Roles::CUSTOMER) {
            $node_wrapper->field_approve->set(3);
          }
          else {
            ReceiptTasks::mailReservation($receipt['entity_id']);
          }
          $node_wrapper->save();
          Cache::updateCacheData($receipt['entity_id']);
          // Check overbooking.
          if (variable_get('is_common_branch_truck', FALSE)) {
            (new Trucks())->handlerConflictRequest($node_wrapper);
          }
          else {
            _movecalc_get_conflict_requests($receipt['entity_id']);
          }
        }
        break;

      case EntityTypes::STORAGEREQUEST:
        (new RequestStorage())->insertReqStorBalance($receipt['entity_id']);
        break;
    }

    return $receipt_id;
  }

  /**
   * Retrieve receipts.
   *
   * @return array|mixed
   *   Receipts.
   *
   * @throws \ServicesException
   */
  public function retrieve() {
    return ReceiptTasks::retrieveReceipt($this->getEntityId(), $this->getEntityType()) ?? [];
  }

  /**
   * Update Receipt.
   *
   * @param array $data
   *   Data for update.
   *
   * @return \DatabaseStatementEmpty|\DatabaseStatementInterface|mixed
   *   Update rows.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function update(array $data = []) {
    $receipt = ReceiptTasks::getReceiptById($this->id);
    if (!empty($receipt)) {
      $result = ReceiptTasks::updateReceipt($this->id, $data);
      ReceiptTasks::receiptSolarOperation('update', $receipt[0]);
      Cache::updateCacheData($receipt[0]['entity_id']);
    }

    return $result ?? 0;
  }

  /**
   * Delete receipt.
   *
   * @return $this|bool|int|mixed
   *   Deleted rows.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function delete() {
    $receipt = ReceiptTasks::getReceiptById($this->id);
    if (!empty($receipt)) {
      $nid = $receipt[0]['entity_id'];
      $result = ReceiptTasks::deleteReceipt($this->id);
      ReceiptTasks::deleteReceiptInSit($this->id);
      ReceiptTasks::receiptSolarOperation('delete', $this->id);
      // Todo тот же вопрос если пройтись по удалению в ресиптах не нашел мест где обновлялись филды реквеста. Сообственно зачем тогда кеш дергать лишний раз?
      Cache::updateCupdateCacheDataache($nid);
    }

    return $result ?? 1;
  }

  /**
   * TODO add conversion for check number(checkM). Keep only numbers.
   * Deserialize $receipt.
   *
   * @param array $receipt
   *   Serialize data.
   *
   * @return array
   *   Receipt data.
   *
   * @throws \Exception
   */
  public function prepareDataForUpdate(array $receipt) {
    global $user;
    if (isset($receipt['receipt']) && is_string($receipt['receipt'])) {
      $unserialize_data = unserialize($receipt['receipt']);
    }
    else {
      $unserialize_data = $receipt;
    }

    $unserialize_data['amount'] = abs($unserialize_data['amount']);

    if (!empty($unserialize_data['transaction_id']) && $unserialize_data['transaction_id'] == 'Custom Payment') {
      $unserialize_data['custom'] = 1;
      $unserialize_data['transaction_id'] = '';
    }

    if (!empty($unserialize_data['payment_method']) && $unserialize_data['payment_method'] == 'customrefund') {
      $unserialize_data['refunds'] = 1;
      $unserialize_data['custom'] = 1;
    }

    if (!empty($unserialize_data['invoice_id'])) {
      if (strtolower($unserialize_data['invoice_id']) == 'custom' || $unserialize_data['invoice_id'] == $receipt['id']) {
        $unserialize_data['invoice_id'] = NULL;
      }
      else {
        $unserialize_data['invoice_id'] = (int) $unserialize_data['invoice_id'];
      }
    }
    else {
      $unserialize_data['invoice_id'] = NULL;
    }

    if (!empty($unserialize_data['card_type'])) {
      $unserialize_data['card_type'] = ReceiptTasks::getCardType($unserialize_data['card_type']);
    }
    elseif (!empty($unserialize_data['cctype'])) {
      $unserialize_data['card_type'] = ReceiptTasks::getCardType($unserialize_data['cctype']);
    }
    else {
      $unserialize_data['card_type'] = NULL;
    }

    if (!empty($unserialize_data['payment_method']) && !empty($unserialize_data['card_type'])) {
      $unserialize_data['payment_method'] = PaymentMethod::CREDIT_CARD;
    }
    elseif (!empty($unserialize_data['payment_method'])) {
      $temp_payment_method = $unserialize_data['payment_method'];
      $unserialize_data['payment_method'] = ReceiptTasks::getPaymentMethod($unserialize_data['payment_method']);
      if ($unserialize_data['payment_method'] == PaymentMethod::CUSTOM) {
        $unserialize_data['custom_payment_method'] = $temp_payment_method;
      }
    }
    elseif (!empty($unserialize_data['type']) && $unserialize_data['type'] == 'creditcard') {
      $unserialize_data['payment_method'] = PaymentMethod::CREDIT_CARD;
    }
    else {
      $unserialize_data['payment_method'] = NULL;
    }

    if (!empty($unserialize_data['ccardN'])) {
      $unserialize_data['card_number'] = (string) substr($unserialize_data['ccardN'], -4);
    }
    elseif (empty($unserialize_data['ccardN']) && !empty($unserialize_data['card_type']) && !empty($unserialize_data['account_number'])) {
      $unserialize_data['card_number'] = (string) substr($unserialize_data['account_number'], -4);
    }

    if (empty($unserialize_data['pending'])) {
      $unserialize_data['pending'] = 0;
    }

    if (empty($unserialize_data['name']) && !empty($unserialize_data['first_name']) && !empty($unserialize_data['last_name'])) {
      $unserialize_data['customer_name'] = $unserialize_data['first_name'] . ' ' . $unserialize_data['last_name'];
    }
    elseif (!empty($unserialize_data['name'])) {
      $unserialize_data['customer_name'] = $unserialize_data['name'];
    }
    elseif ($receipt['entity_type'] == EntityTypes::STORAGEREQUEST) {
      $query = db_select('request_storage', 'rs')
        ->fields('rs')
        ->condition('rs.id', $receipt['entity_id'])
        ->execute()
        ->fetchAssoc();

      $result = unserialize($query['value']);
      if (!empty($result['user_info']['uid'])) {
        $additional_user_wrap = (new Clients())->getUserFieldsData($result['user_info']['uid'], FALSE);
      }
      else {
        $clients = new Clients();
        $uid = $clients->checkExistUser(['mail' => $result['user_info']['email'], 'name' => $result['user_info']['name']], TRUE);
        $additional_user_wrap = $clients->getUserFieldsData($uid, FALSE);
      }
      $unserialize_data['customer_name'] = $additional_user_wrap['field_user_first_name'] . ' ' . $additional_user_wrap['field_user_last_name'];
      $unserialize_data['phone'] = $additional_user_wrap['field_primary_phone'];
      $unserialize_data['email'] = $additional_user_wrap['mail'];
      $unserialize_data['uid'] = $additional_user_wrap['uid'];
      $unserialize_data['role'] = Roles::CUSTOMER;
      if (!empty($unserialize_data['cctype'])) {
        $unserialize_data['card_type'] = ReceiptTasks::getCardType($unserialize_data['cctype']);
      }
    }

    if ($receipt['entity_type'] == EntityTypes::MOVEREQUEST && !empty($unserialize_data['transaction_type'])) {
      if (empty($unserialize_data['jobs'])) {
        $unserialize_data['jobs'] = [$receipt['entity_id']];
      }

      if (empty($unserialize_data['customer_name'])) {
        $uid = MoveRequest::getRequestUserId($receipt['entity_id']);
        $user_wrap = entity_metadata_wrapper('user', user_load($uid));

        $unserialize_data['customer_name'] = $user_wrap->field_user_first_name->value() . ' ' . $user_wrap->field_user_last_name->value();
        $unserialize_data['uid'] = $user_wrap->uid->value();
        $unserialize_data['phone'] = $user_wrap->field_primary_phone->value();
        $unserialize_data['email'] = $user_wrap->mail->value();
        $unserialize_data['role'] = Roles::CUSTOMER;

      }


    }

    if ($receipt['entity_type'] == EntityTypes::LDCARRIER) {
      $carrier_info = db_select('ld_carrier', 'lc')
        ->fields('lc')
        ->condition('ld_carrier_id', $receipt['entity_id'])
        ->execute()
        ->fetchAssoc();

      if (!empty($carrier_info['zip_code'])) {
        $unserialize_data['zip_code'] = $carrier_info['zip_code'];
      }
    }

    if ($receipt['entity_type'] == EntityTypes::LDTDJOB) {
      $ld_tp_delivery = db_select('ld_tp_delivery_details', 'ltd')
        ->fields('ltd')
        ->condition('ld_tp_delivery_id', $receipt['entity_id'])
        ->execute()
        ->fetchAssoc();
      $unserialize_data['customer_name'] = $ld_tp_delivery['customer'];
      $unserialize_data['zip_code'] = $ld_tp_delivery['zip'];
      if (!empty($unserialize_data['cctype'])) {
        $unserialize_data['card_type'] = ReceiptTasks::getCardType($unserialize_data['cctype']);
      }
    }

    if (!empty($unserialize_data['payment_flag'])) {
      $payment_flag = strtolower($unserialize_data['payment_flag']);
      if ($payment_flag == 'reservation by client' || $payment_flag == 'payment for invoice by client') {
        $unserialize_data['role'] = Roles::CUSTOMER;
      }
      elseif ($payment_flag == 'payment for invoice by admin') {
        $unserialize_data['role'] = Roles::OWNER;
      }
    }

    if (empty($unserialize_data['role'])) {
      $unserialize_data['role'] = (new Payroll())->userRole($user, FALSE);
    }

    if (!empty($unserialize_data['payment_flag'])) {
      $unserialize_data['payment_flag'] = ReceiptTasks::getPaymentFlag($unserialize_data['payment_flag']);
    }
    else {
      $unserialize_data['payment_flag'] = PaymentFlag::REGULAR;
    }

    if (!empty($unserialize_data['invoice_id'])) {
      $unserialize_data['payment_flag'] = PaymentFlag::INVOICE;
    }

    if (!empty($unserialize_data['email'])) {
      $unserialize_data['email'] = strtolower($unserialize_data['email']);
      if (empty($unserialize_data['customer_name']) && $user_load_email = user_load_by_mail($unserialize_data['email'])) {
        $user_wrap = entity_metadata_wrapper('user', $user_load_email);
        $unserialize_data['customer_name'] = $user_wrap->field_user_first_name->value() . ' ' . $user_wrap->field_user_last_name->value();
        $unserialize_data['uid'] = $user_wrap->uid->value();
      }
    }

    if (empty($unserialize_data['uid'])) {
      $unserialize_data['uid'] = $user->uid;
    }

    if (!empty($unserialize_data['phone'])) {
      $unserialize_data['phone'] = preg_replace('/[^0-9.]+/', '', $unserialize_data['phone']);
    }

    if (!empty($unserialize_data['jobs'])) {
      $trips = array();
      foreach ($unserialize_data['jobs'] as $job) {
        $full_job = LongDistanceJob::getJobById($job);
        $trips[] = $full_job['trip_id'];
      }

      $unserialize_data['trips_id'] = serialize(array_unique($trips));
      $unserialize_data['jobs'] = serialize($unserialize_data['jobs']);
    }

    $unserialize_data['created'] = !empty($receipt['created']) ? $receipt['created'] : strtotime($unserialize_data['date']);
    $unserialize_data['entity_id'] = (int) $receipt['entity_id'];
    $unserialize_data['entity_type'] = (int) $receipt['entity_type'];
    $unserialize_data['id'] = (int) $receipt['id'];

    $receipt_fields = ReceiptTasks::prepareReceiptAttributes();

    $diff = array_diff_key($unserialize_data, $receipt_fields);
    foreach ($diff as $receipt_field => $value) {
      unset($unserialize_data[$receipt_field]);
    }

    return $unserialize_data;
  }

}
