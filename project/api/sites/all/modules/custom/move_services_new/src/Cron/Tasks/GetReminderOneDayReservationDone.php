<?php

namespace Drupal\move_services_new\Cron\Tasks;

use Carbon\Carbon;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\PaymentFlag;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class GetReminderOneDayReservationDone.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
class GetReminderOneDayReservationDone implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $yesterday = Carbon::now()->subDays(1)->timestamp;
    $day_before_yesterday = Carbon::now()->subDays(2)->timestamp;

    $query = db_select('node', 'n');
    $query->fields('n', ['nid']);
    $query->leftJoin('temp_moveadmin_receipt_search', 'receipt', 'receipt.entity_id = n.nid AND receipt.entity_type = :entity_type', [':entity_type' => EntityTypes::MOVEREQUEST]);
    $query->leftJoin('field_data_field_approve', 'status', 'status.entity_id = receipt.entity_id');
    $query->leftJoin('field_data_field_reservation_received', 'reservation_received', 'reservation_received.entity_id = receipt.entity_id');
    $query->leftJoin('move_services_email_was_sent', 'email_was_sent', 'email_was_sent.nid = receipt.entity_id AND email_was_sent.mark = :type', [':type' => 'reminder_finish_reservation']);

    $query->condition('receipt.payment_flag', PaymentFlag::RESERVATION);
    $query->condition('field_approve_value', RequestStatusTypes::NOT_CONFIRMED);
    $query->condition('field_reservation_received_value', '1');
    $query->condition('receipt.created', [$day_before_yesterday, $yesterday], 'BETWEEN');
    $query->isNull('email_was_sent.id');

    $query->groupBy('n.nid');

    $nids = $query->execute()->fetchCol();

    if ($nids) {
      $queue = \DrupalQueue::get('move_services_new_send_reminder_one_day_reservation_done');
      $queue->createQueue();
      $chunk = array_chunk($nids, 3);
      foreach ($chunk as $item) {
        $queue->createItem($item);
      }
    }
  }

}
