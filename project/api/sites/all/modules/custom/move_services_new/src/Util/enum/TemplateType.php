<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TemplateType.
 *
 * @package Drupal\move_services_new\Util
 */
class TemplateType extends BaseEnum {

  const REQUEST = 0;
  const USER = 1;
  const COMMON = 2;

}
