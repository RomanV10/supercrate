<?php

namespace Drupal\move_services_new\Receipts\Controllers;

use Drupal\move_inventory\Controllers\AbstractController;
use Drupal\move_services_new\Receipts\Actions\ReceiptActions;

/**
 * Class ReceiptController.
 *
 * @package Drupal\move_services_new\Receipts\Controllers
 */
class ReceiptController extends AbstractController {

  /**
   * Create receipt.
   *
   * @param array $data
   *   Data.
   *
   * @return array
   *   Result.
   *
   * @throws \ServicesException
   */
  public static function createReceipt(array $data = array()) {
    return self::run(ReceiptActions::class, 'create', [$data]);
  }

  /**
   * Update receipt.
   *
   * @param int $id
   *   Id receipt.
   * @param array $data
   *   Receipt data.
   *
   * @return array
   *   Update rows.
   *
   * @throws \ServicesException
   */
  public static function updateReceipt(int $id, array $data = array()) {
    return self::run(new ReceiptActions($id), 'update', [$data]);
  }

  /**
   * Get receipt.
   *
   * @param int $entity_id
   *   Entity id.
   * @param int $entity_type
   *   Entity type.
   *
   * @return array
   *   Receipt.
   *
   * @throws \ServicesException
   */
  public static function getReceipt(int $entity_id, int $entity_type) {
    $receiptActions = new ReceiptActions();
    $receiptActions->setEntityId($entity_id);
    $receiptActions->setEntityType($entity_type);

    return self::run($receiptActions, 'retrieve', []);
  }

  /**
   * Delete receipt.
   *
   * @param int $rid
   *   Receipt id.
   *
   * @return array
   *   Delete row.
   *
   * @throws \ServicesException
   */
  public static function deleteReceipt(int $rid) {
    return self::run(new ReceiptActions($rid), 'delete', []);
  }

  public static function searchReceipt($data, $entity_type) {
   // return Receipt::receiptSearch($data, $entity_type);
  }

}
