<?php

namespace Drupal\move_services_new\System\Resources;

use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Util\enum\ContractType;

/**
 * Class PayrollDefinition.
 *
 * @package Drupal\move_services_new\System\Resources
 */
class PayrollDefinition {

  public static function getDefinition() {
    return array(
      'payroll' => array(
        'operations' => array(),
        'actions' => array(
          'set_contract_info' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::setContractInfo',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'value',
                'type' => 'array',
                'source' => array('data' => 'value'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'contract_type',
                'type' => 'int',
                'source' => array('data' => 'contract_type'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_contract_info' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::getContractInfo',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'payroll_by_role' => array(
            'help' => 'Payroll by role',
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::payrollByRole',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date_from',
                'type' => 'string',
                'source' => array('data' => 'date_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_to',
                'type' => 'string',
                'source' => array('data' => 'date_to'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'role',
                'type' => 'int',
                'source' => array('data' => 'role'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'payroll_all' => array(
            'help' => 'Payroll by role',
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::payrollAll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date_from',
                'type' => 'string',
                'source' => array('data' => 'date_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_to',
                'type' => 'string',
                'source' => array('data' => 'date_to'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'user_paycheck' => array(
            'help' => 'Apply Payment for worker.',
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::userPaycheck',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_receipt_period' => array(
            'help' => 'Get receipt',
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::getReceiptPeriod',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_from',
                'type' => 'string',
                'source' => array('data' => 'date_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_to',
                'type' => 'string',
                'source' => array('data' => 'date_to'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'edit_receipt' => array(
            'help' => 'Edit receipt information',
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::editReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'rid',
                'type' => 'int',
                'source' => array('data' => 'rid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete_receipt' => array(
            'help' => 'Edit receipt information',
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::deleteReceipt',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'rid',
                'type' => 'int',
                'source' => array('data' => 'rid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_payroll' => array(
            'help' => 'Worker payroll for period.',
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::getPayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_from',
                'type' => 'string',
                'source' => array('data' => 'date_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_to',
                'type' => 'string',
                'source' => array('data' => 'date_to'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'set_payroll_info' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::setPayrollInfo',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'contract_type',
                'type' => 'int',
                'source' => array('data' => 'contract_type'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'hourly_misc_payment' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::hourlyMiscPayment',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'type' => 'int',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'payee',
                'type' => 'int',
                'source' => array('data' => 'payee'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => FALSE,
                'default value' => 0,
              ),
              array(
                'name' => 'hourly',
                'type' => 'array',
                'source' => array('data' => 'hourly'),
                'default value' => array(),
                'optional' => TRUE,
              ),
              array(
                'name' => 'amount',
                'type' => 'array',
                'source' => array('data' => 'amount'),
                'default value' => array(),
                'optional' => TRUE,
              ),
              array(
                'name' => 'note',
                'type' => 'string',
                'source' => array('data' => 'note'),
                'default value' => '',
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'edit_hourly_misc' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::editHourlyMisc',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete_hourly_misc' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::deleteHourlyMisc',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date',
                'type' => 'string',
                'source' => array('data' => 'date'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_all_payroll_info' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::getAllPayrollInfo',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'contract_type',
                'type' => 'int',
                'source' => array('data' => 'contract_type'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'get_workers_info' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::getWorkersInfo',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update_user_payroll' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::updateUserPayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'old_uid',
                'type' => 'int',
                'source' => array('data' => 'old_uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'new_uid',
                'type' => 'int',
                'source' => array('data' => 'new_uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'contract_type',
                'type' => 'int',
                'source' => array('data' => 'contract_type'),
                'optional' => FALSE,
                'default' => ContractType::PAYROLL,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'remove_user_payroll' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::removeUserPayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'array',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'contract_type',
                'type' => 'int',
                'source' => array('data' => 'contract_type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'create_custom_payroll' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::createCustomPayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'amount',
                'type' => 'string',
                'source' => array('data' => 'amount'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'photo',
                'type' => 'array',
                'source' => array('data' => 'photo'),
                'optional' => TRUE,
              ),
              array(
                'name' => 'contract_type',
                'type' => 'int',
                'source' => array('data' => 'contract_type'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('create custom payroll'),
          ),
          'delete_custom_payroll' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::deleteCustomPayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('create custom payroll'),
          ),
          'update_custom_payroll' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::updateCustomPayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'name',
                'type' => 'string',
                'source' => array('data' => 'name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'amount',
                'type' => 'string',
                'source' => array('data' => 'amount'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'photo',
                'type' => 'array',
                'source' => array('data' => 'photo'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('update custom payroll'),
          ),
          'get_custom_payroll' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::getCustomPayroll',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'contract_type',
                'type' => 'int',
                'source' => array('data' => 'contract_type'),
                'optional' => TRUE,
              ),
            ),
            'access arguments' => array('get custom payroll'),
          ),
          'update_payroll_cache' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::updatePayrollCache',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date_from',
                'type' => 'string',
                'source' => array('data' => 'date_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date_to',
                'type' => 'string',
                'source' => array('data' => 'date_to'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'sum_commission_from_total' => array(
            'callback' => 'Drupal\move_services_new\System\Resources\PayrollDefinition::sumCommissionFromTotal',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/PayrollDefinition',
            ),
            'args' => array(
              array(
                'name' => 'uid',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'contract_type',
                'type' => 'int',
                'source' => array('data' => 'contract_type'),
                'optional' => TRUE,
                'default' => ContractType::PAYROLL,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  /**
   * Service callback: Set contract info.
   *
   * @param int $nid
   *   Move request id.
   * @param array $value
   *   Data.
   * @param int $contract_type
   *   Contract type.
   *
   * @return bool
   *   True when payroll update.
   *
   * @throws \Exception
   */
  public static function setContractInfo(int $nid, $value = array(), $contract_type = ContractType::PAYROLL) {
    $result = FALSE;

    if ($value) {
      $payroll_instance = new Payroll();
      $result = $payroll_instance->setContractInfo($nid, $value, $contract_type);
    }

    return $result;
  }

  public static function getContractInfo(int $nid) {
    return Payroll::getContractInfo($nid);
  }

  public static function payrollByRole($date_from, $date_to, int $role) {
    $payroll_instance = new Payroll();
    return $payroll_instance->cacheByRolePayroll($date_from, $date_to, $role);
  }

  public static function setPayrollInfo(int $nid, array $data, $contract_type = ContractType::PAYROLL) {
    $payroll_instance = new Payroll();
    $payroll_instance->setPayrollInfo($nid, $data, $contract_type);
    return TRUE;
  }

  public static function getPayroll(int $uid, $date_from, $date_to) {
    $payroll_instance = new Payroll();
    return $payroll_instance->cacheByUserPayroll($date_from, $date_to, $uid);
  }

  public static function updateUserPayroll(int $nid, int $old_uid, int $new_uid, int $contract_type) {
    $payroll_instance = new Payroll();
    return $payroll_instance->updateUserPayroll($nid, $old_uid, $new_uid, $contract_type);
  }

  public static function removeUserPayroll(int $nid, array $uids, $contract_type = ContractType::PAYROLL) {
    $payroll_instance = new Payroll();
    return $payroll_instance->removeUserPayroll($nid, $uids, $contract_type);
  }

  public static function payrollAll($date_from, $date_to) {
    $payroll_instance = new Payroll();
    return $payroll_instance->cacheAllPayroll($date_from, $date_to);
  }

  public static function getAllPayrollInfo($nid, $contract_type) {
    if (is_null($contract_type)) {
      $contract_type = ContractType::PAYROLL;
    }
    $payroll_instance = new Payroll();
    return $payroll_instance->getAllPayrollInfo($nid, $contract_type);
  }

  public static function getWorkersInfo() {
    return Payroll::getWorkersInfo();
  }

  public static function getReceiptPeriod(int $uid, string $date_from, string $date_to) {
    $payroll_instance = new Payroll($date_from, $date_to);
    return $payroll_instance->paycheckUserForPeriod($uid);
  }

  public static function userPaycheck(int $uid, array $data) {
    $result = NULL;
    $payroll_instance = new Payroll();
    if (isset($data['date']) && isset($data['form']) && isset($data['amount'])) {
      $result = $payroll_instance->applyPaycheck($uid, $data);
    }

    return $result;
  }

  public static function editReceipt(int $rid, array $data) {
    $payroll_instance = new Payroll();
    return $payroll_instance->editPaycheck($rid, $data);
  }

  public static function deleteReceipt(int $rid) {
    $payroll_instance = new Payroll();
    return $payroll_instance->deletePaycheck($rid);
  }

  public static function hourlyMiscPayment($date, $payee, $type = 0, $hourly = array(), $amount = array(), $note = '') {
    $payroll_instance = new Payroll();
    $data = array(
      'date' => $date,
      'payee' => $payee,
      'type' => $type,
      'hourly' => $hourly,
      'amount' => $amount,
      'note' => $note,
    );
    return $payroll_instance->setHourlyMiscPayment($data);
  }

  public static function editHourlyMisc(int $id, array $data) {
    $payroll_instance = new Payroll();
    return $payroll_instance->editHourlyMisc($id, $data);
  }

  public static function deleteHourlyMisc(int $id, $date) {
    $payroll_instance = new Payroll();
    return $payroll_instance->deleteMiscPayment($id, $date);
  }

  public static function createCustomPayroll($uid, $nid, $name, $amount, $photo, $contract_type = ContractType::PAYROLL) {
    $payroll_instance = new Payroll();
    return $payroll_instance->createCustomPayroll($uid, $nid, $name, $amount, $photo, $contract_type);
  }

  public static function updateCustomPayroll($id, $name, $amount, $photo) {
    $payroll_instance = new Payroll();
    return $payroll_instance->updateCustomPayroll($id, $name, $amount, $photo);
  }

  public static function deleteCustomPayroll($id) {
    $payroll_instance = new Payroll();
    return $payroll_instance->deleteCustomPayroll($id);
  }

  public static function getCustomPayroll($nid, $uid, $contract_type = ContractType::PAYROLL) {
    $payroll_instance = new Payroll();
    return $payroll_instance->getCustomPayroll($nid, $uid, $contract_type);
  }

  public static function updatePayrollCache($date_from, $date_to) {
    $payroll_instance = new Payroll();
    return $payroll_instance->updateAllPayrollCache($date_from, $date_to);
  }

  public static function sumCommissionFromTotal($uid, $nid, $contract_type) {
    $payroll_instance = new Payroll();
    return $payroll_instance->getSumCommissionFromTotal($uid, $nid, $contract_type);
  }

}
