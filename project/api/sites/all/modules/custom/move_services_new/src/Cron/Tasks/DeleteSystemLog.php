<?php

namespace Drupal\move_services_new\Cron\Tasks;

/**
 * Class DeleteSystemLog.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
class DeleteSystemLog implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    module_load_include('inc', 'move_services_new', 'move_services_new.admin');
    deleteSystemSettingsLogs(0, 750);
  }

}
