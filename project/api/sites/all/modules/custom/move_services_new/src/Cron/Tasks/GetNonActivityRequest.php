<?php

namespace Drupal\move_services_new\Cron\Tasks;

use DateTime;

/**
 * Class GetNonActivityRequest.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
class GetNonActivityRequest implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    if (!$days_expired = self::getExpiredDaysOption()) {
      return;
    }

    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_request_manager', 'frm', 'n.nid = frm.entity_id AND frm.bundle = :type', array(':type' => 'move_request'));
    $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id AND fa.bundle = :type', array(':type' => 'move_request'));
    $query->leftJoin('field_data_field_reservation_received', 'fdr', 'n.nid = fdr.entity_id AND fdr.bundle = :type', array(':type' => 'move_request'));

    $query->fields('n', array('nid'))
      ->condition('n.type', 'move_request')
      ->condition('n.status', NODE_PUBLISHED)
      ->condition('fa.field_approve_value', 2)
      ->condition('fdr.field_reservation_received_value', 0);

    $query->orderBy('created', 'ASC');
    $requests = $query->execute()->fetchCol();

    if (!empty($requests)) {
      $result = array();

      foreach ($requests as $key => $nid) {
        $wrapper = entity_metadata_wrapper('node', $nid);
        $move_field_to_storage_move = $wrapper->field_to_storage_move->raw();
        $move_field_from_storage_move = $wrapper->field_from_storage_move->raw();

        $date1 = new DateTime('now');
        $date2 = new DateTime();
        $date2->setTimestamp($wrapper->changed->value());
        $interval_diff = $date1->diff($date2);
        $interval = $interval_diff->days;

        if ($interval >= $days_expired) {
          // Don't expired connected moves (From storage and overnight.
          if (!isset($move_field_to_storage_move)) {
            // Change on canceled.
            $result[] = $nid;
            // If it's overnight or storage
            // first move expired connected move to.
            if (isset($move_field_from_storage_move)) {
              $result[] = $move_field_from_storage_move;
            }
          }
        }
      }

      if ($result) {
        $queue = \DrupalQueue::get('move_services_new_change_status_non_active_to_expired');
        $queue->createQueue();
        $chunk = array_chunk($result, 3);
        foreach ($chunk as $item) {
          $queue->createItem($item);
        }
      }
    }
  }

  /**
   * Get expired days option.
   *
   * @return int
   *   Days count.
   */
  public static function getExpiredDaysOption() {
    $schedule_settings = json_decode(variable_get("scheduleSettings", ''));
    return isset($schedule_settings->daysExpired) ? intval($schedule_settings->daysExpired) : 0;
  }

}
