<?php

namespace Drupal\move_services_new\Receipts\Tasks;

use Drupal\move_inventory\Exceptions\SQLException;
use Drupal\move_long_distance\Services\Actions\SitReceiptActions;
use Drupal\move_quickbooks\Services\QBSalesReceipt;
use Drupal\move_services_new\Util\enum\CreditCard;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\PaymentFlag;
use Drupal\move_services_new\Util\enum\PaymentMethod;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class ReceiptTasks.
 *
 * @package Drupal\move_services_new\Receipts\Tasks
 */
class ReceiptTasks {
  private $page = 0;
  private $pageSize = 50;

  /**
   * Get enum value.
   *
   * @param null|string $cardType
   *   Card type.
   *
   * @return int|null
   *   Enum card value.
   */
  public static function getCardType(?string $cardType) {
    $cardTypeLowCase = strtolower($cardType);
    if ($cardTypeLowCase == 'visa') {
      return CreditCard::VISA;
    }
    if ($cardTypeLowCase == 'mastercard' || $cardTypeLowCase == 'master') {
      return CreditCard::MASTERCARD;
    }
    if ($cardTypeLowCase == 'discouver' || $cardTypeLowCase == 'discover') {
      return CreditCard::DISCOUVER;
    }
    if ($cardTypeLowCase == 'amex' || $cardTypeLowCase == 'american express' || $cardTypeLowCase == 'americanexpress') {
      return CreditCard::AMEX;
    }

    return NULL;
  }

  /**
   * Get receipts from moveadmin_receipt.
   *
   * @return array
   *   Receipts.
   */
  public static function getMoveAdminReceipts() {
    $select = db_select('moveadmin_receipt', 'mr');
    $select->fields('mr', []);
    return $select->execute()->fetchAll(\PDO::FETCH_ASSOC) ?? [];
  }

  /**
   * Get payment method.
   *
   * @param null|string $payMethod
   *   Payment method.
   *
   * @return int|null
   *   Enum payment method.
   */
  public static function getPaymentMethod(?string $payMethod): ?int {
    $payMethodLowCase = strtolower($payMethod);
    if ($payMethodLowCase == 'creditcard' || $payMethodLowCase == 'cc') {
      return PaymentMethod::CREDIT_CARD;
    }
    if ($payMethodLowCase == 'cash') {
      return PaymentMethod::CASH;
    }
    if ($payMethodLowCase == 'check') {
      return PaymentMethod::CHECK;
    }

    return PaymentMethod::CUSTOM;
  }

  /**
   * Get payment flag.
   *
   * @param string $payFlag
   *   Payment flag.
   *
   * @return int|null
   *   Enum pay flag.
   */
  public static function getPaymentFlag(?string $payFlag) {
    $payFlagLowCase = strtolower($payFlag);
    if ($payFlagLowCase == 'regular') {
      return PaymentFlag::REGULAR;
    }
    if ($payFlagLowCase == 'reservation' || $payFlagLowCase == 'reservation by client') {
      return PaymentFlag::RESERVATION;
    }
    if ($payFlagLowCase == 'contract') {
      return PaymentFlag::CONTRACT;
    }
    if (static::checkInvoicePayFlag($payFlagLowCase)) {
      return PaymentFlag::INVOICE;
    }

    return PaymentFlag::REGULAR;
  }

  /**
   * Check receipt for invoice or not.
   *
   * @param null|string $payFlag
   *   Payment flag.
   *
   * @return false|int
   *   Int if matches given.
   */
  private static function checkInvoicePayFlag(?string $payFlag) {
    return preg_match('/payment for invoice/', $payFlag);
  }

  /**
   * Index receipt.
   *
   * @param string $operation_type
   *   Type operation.
   * @param array|int $receipt
   *   Receipt data.
   */
  public static function receiptSolarOperation(string $operation_type, $receipt = array()) {
    move_global_search_solr_invoke($operation_type, EntityTypes::RECEIPT, $receipt);
  }

  /**
   * Insert receipts task.
   *
   * @param array $receipt
   *   Receipt.
   *
   * @return $this|bool|\DatabaseStatementEmpty|\DatabaseStatementInterface|int
   *   Id.
   *
   * @throws \ServicesException
   */
  public static function insertReceipt(array $receipt) {
    try {
      return db_insert('temp_moveadmin_receipt_search')
        ->fields($receipt)
        ->execute();
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

  /**
   * Send receipt to quickbooks.
   *
   * @param array $receipt
   *   Receipt data.
   */
  public static function sendReceiptToQuickBooks(array $receipt) {
    if (!empty(variable_get("qbo_api_access_token", FALSE)) && (variable_get("qbo_post_settings", FALSE) == "alltime")) {
      $post_payment_to_quickbooks = new QBSalesReceipt();
      $post_payment_to_quickbooks->createQBSalesReceipt($receipt['entity_id'], $receipt, $receipt['id']);
    }
  }

  /**
   * Send email when request.
   *
   * @param int $nid
   *   Node id.
   */
  public static function mailReservation($nid) {
    $template_builder = new TemplateBuilder();
    $node = node_load($nid);
    $template_builder->moveTemplateBuilderSendTemplateRules('reminder_finish_reservation', $node);
  }

  /**
   * Update receipt.
   *
   * @param int $id
   *   Receipt id.
   * @param array $receipt
   *   Receipt data.
   *
   * @return \DatabaseStatementEmpty|\DatabaseStatementInterface
   *   Update rows.
   *
   * @throws \ServicesException
   */
  public static function updateReceipt(int $id, array $receipt) {
    try {
      return db_update('temp_moveadmin_receipt_search')
        ->fields($receipt)
        ->condition('id', $id)
        ->execute();
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

  /**
   * Retrieve receipts.
   *
   * @param int $entityId
   *   Entity id.
   * @param int $entityType
   *   Entity type.
   *
   * @return mixed
   *   Receipts.
   *
   * @throws \ServicesException
   */
  public static function retrieveReceipt(int $entityId, int $entityType) {
    try {
      return db_select('temp_moveadmin_receipt_search', 'mr')
        ->fields('mr')
        ->condition('entity_id', $entityId)
        ->condition('entity_type', $entityType)
        ->execute()->fetchAll();
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

  /**
   * Get receipt by id.
   *
   * @param int $id
   *   Receipt id.
   *
   * @return mixed
   *   Receipt.
   *
   * @throws \ServicesException
   */
  public static function getReceiptById(int $id) {
    try {
      return db_select('temp_moveadmin_receipt_search', 'mr')
        ->fields('mr')
        ->condition('id', $id)
        ->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

  /**
   * Delete receipt.
   *
   * @param int $id
   *   Receipt id.
   *
   * @return $this|bool|int
   *   Deleted row.
   *
   * @throws \ServicesException
   */
  public static function deleteReceipt(int $id) {
    try {
      return db_delete('temp_moveadmin_receipt_search')
        ->condition('id', $id)
        ->execute();
    }
    catch (\Throwable $exception) {
      throw new SQLException($exception);
    }
  }

  /**
   * Delete receipt.
   *
   * @param int $id
   *   Receipt id.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public static function deleteReceiptInSit(int $id) {
    // If receipt exist in CIT, we delete it too.
    $sit_receipt = new SitReceiptActions();
    $is_ld_receipt = $sit_receipt::receiptExist($id);
    if ($is_ld_receipt) {
      $sit_receipt->setReceiptId($id);
      $sit_receipt->deleteReceipt(TRUE);
    }
  }

  /**
   * Get filtered payment receipts.
   *
   * @param array $filters
   *   Filters params for receipts.
   * @param array $sort
   *   Sort params for receipt.
   * @param int|null $page
   *   Page number.
   *
   * @return array
   *   Receipts.
   */
  public function filterReceipts(array $filters = array(), array $sort = array(), ?int $page = 0) {
    $result = array();
    $query = $this->prepareReceiptQuery($filters, $sort);

    $query->fields('tmrs');
    $result['meta'] = $this->getIndexReceiptMetaData($query, $page);
    $query->range($page * $this->pageSize, $this->pageSize);
    $result['items'] = $query->execute()->fetchAll();

    $result['sum']['all'] = $this->sumReceipts($filters);
    $result['sum']['pending'] = $this->sumPendingReceipts($filters);
    $result['sum']['refunds'] = $this->sumPendingRefunds($filters);
    $result['sum']['pending_refunds'] = $this->sumPendingRefundsReceipt($filters);
    $result['sum']['total'] = $result['sum']['all'][0] - $result['sum']['pending'][0] - $result['sum']['refunds'][0];

    return $result;
  }

  /**
   * Prepare receipt select query.
   *
   * @param array $filters
   *   Filters data.
   * @param array $sort
   *   Sort data.
   *
   * @return mixed|\SelectQuery
   *   SelectQuery.
   */
  private function prepareReceiptQuery(array $filters = array(), array $sort = array()) {
    $query = db_select('temp_moveadmin_receipt_search', 'tmrs');

    if (!empty($filters)) {
      $query = static::filterIndexReceipt($query, $filters);
    }
    if (!empty($sort)) {
      $query = static::sortIndexReceipt($query, $sort);
    }

    return $query;
  }

  /**
   * Prepare receipt select query.
   *
   * @param array $filters
   *   Filters data.
   *
   * @return mixed|\SelectQuery
   *   SelectQuery.
   */
  private function sumReceipts(array $filters = array()) {
    $query = $this->prepareReceiptQuery($filters, []);

    $query->condition('pending', 1, '!=');
    $query->condition('refunds', 1, '!=');
    $query->addExpression('SUM(tmrs.amount)', 'total_amount');

    return $query->execute()->fetchCol();
  }

  /**
   * Prepare receipt select query.
   *
   * @param array $filters
   *   Filters data.
   *
   * @return mixed|\SelectQuery
   *   SelectQuery.
   */
  private function sumPendingReceipts(array $filters = array()) {
    $query = $this->prepareReceiptQuery($filters, []);

    $query->condition('pending', 1, '=');
    $query->condition('refunds', 1, '!=');
    $query->addExpression('SUM(tmrs.amount)', 'total_amount');

    return $query->execute()->fetchCol();
  }

  /**
   * Prepare receipt select query.
   *
   * @param array $filters
   *   Filters data.
   *
   * @return mixed|\SelectQuery
   *   SelectQuery.
   */
  private function sumPendingRefunds(array $filters = array()) {
    $query = $this->prepareReceiptQuery($filters, []);

    $query->condition('pending', 1, '!=');
    $query->condition('refunds', 1, '=');
    $query->addExpression('SUM(tmrs.amount)', 'total_amount');

    return $query->execute()->fetchCol();
  }

  /**
   * Prepare receipt select query.
   *
   * @param array $filters
   *   Filters data.
   *
   * @return mixed|\SelectQuery
   *   SelectQuery.
   */
  private function sumPendingRefundsReceipt(array $filters = array()) {
    $query = $this->prepareReceiptQuery($filters, []);

    $query->condition('pending', 1, '=');
    $query->condition('refunds', 1, '=');
    $query->addExpression('SUM(tmrs.amount)', 'total_amount');

    return $query->execute()->fetchCol();
  }

  /**
   * Prepare condition for filter receipt.
   *
   * @param \SelectQuery $query
   *   Db query object.
   * @param array $filters
   *   User filter data.
   *
   * @return mixed
   *   Query object.
   */
  private static function filterIndexReceipt(\SelectQuery $query, array $filters = array()) {
    if (!empty($filters['entity_id'])) {
      $query->condition('entity_id', $filters['entity_id']);
    }

    if (isset($filters['entity_type'])) {
      $query->condition('entity_type', $filters['entity_type'], 'IN');
    }

    if (!empty($filters['amount']) && !empty($filters['amount']['min']) && !empty($filters['amount']['max'])) {
      $query->condition('amount', array($filters['amount']['min'], $filters['amount']['max']), 'BETWEEN');
    }

    if (!empty($filters['card_type'])) {
      $query->condition('card_type', $filters['card_type'], 'IN');
    }

    if (!empty($filters['card_number'])) {
      $query->condition('card_number', $filters['card_number']);
    }

    if (!empty($filters['custom'])) {
      $query->condition('custom', $filters['custom']);
    }

    if (!empty($filters['customer_name'])) {
      $query->condition('customer_name', $filters['customer_name']);
    }

    if (!empty($filters['phone'])) {
      $query->condition('phone', $filters['phone']);
    }

    if (!empty($filters['created'])) {
      if (!empty($filters['created']['from']) && !empty($filters['created']['to'])) {
        $query->condition('created', array($filters['created']['from'], $filters['created']['to']), 'BETWEEN');
      }
    }

    if (!empty($filters['checkN'])) {
      $query->condition('checkN', $filters['checkN']);
    }

    if (!empty($filters['description'])) {
      $query->condition('description', $filters['description']);
    }

    if (!empty($filters['email'])) {
      $query->condition('email', $filters['email']);
    }

    if (!empty($filters['invoice_id'])) {
      $query->condition('invoice_id', $filters['invoice_id']);
    }

    if (!empty($filters['payment_method'])) {
      $query->condition('payment_method', $filters['payment_method']);
    }

    if (isset($filters['pending'])) {
      $query->condition('pending', $filters['pending']);
    }

    if (!empty($filters['payment_flag'])) {
      $query->condition('payment_flag', $filters['payment_flag'], 'IN');
    }

    if (isset($filters['refunds'])) {
      $query->condition('refunds', $filters['refunds']);
    }

    if (isset($filters['refund_type'])) {
      $query->condition('refund_type', $filters['refund_type']);
    }

    if (!empty($filters['transaction_id'])) {
      $query->condition('transaction_id', $filters['transaction_id']);
    }

    if (isset($filters['transaction_type'])) {
      $query->condition('transaction_type', $filters['transaction_type']);
    }
    else {
      $db_or = db_or();
      $db_or->condition('transaction_type', array(1, 2, 4), 'IN');
      $db_or->isNULL('transaction_type');
      $query->condition($db_or);
    }

    if (!empty($filters['total_amount']) && isset($filters['total_amount']['min']) && isset($filters['total_amount']['max'])) {
      $query->condition('total_amount', array($filters['total_amount']['min'], $filters['total_amount']['max']), 'BETWEEN');
    }

    if (!empty($filters['trip_id'])) {
      $query->condition('trip_id', $filters['trip_id']);
    }

    if (!empty($filters['zip_code'])) {
      $query->condition('zip_code', $filters['zip_code']);
    }

    if (!empty($filters['role'])) {
      $query->condition('role', $filters['role']);
    }

    if (!empty($filters['uid'])) {
      $query->condition('uid', $filters['uid']);
    }

    if (!empty($filters['storage_agreement'])) {
    }

    if (!empty($filters['third_party_payment'])) {
    }

    return $query;
  }

  /**
   * Sort receipt.
   *
   * @param \SelectQuery $query
   *   Select query object.
   * @param array|null $sort
   *   Sort receipt options.
   *
   * @return mixed
   *   Query object.
   */
  private static function sortIndexReceipt(\SelectQuery $query, ?array $sort = array()) {
    if (!empty($sort) && !empty($sort['field']) && !empty($sort['direction'])) {
      $query->orderBy($sort['field'], $sort['direction']);
    }
    else {
      $query->orderBy('created', 'DESC');
    }

    return $query;
  }

  /**
   * Metadata for receipts query.
   *
   * @param \SelectQuery $query
   *   Receipts select query.
   *
   * @return array
   *   Query metadata.
   */
  private function getIndexReceiptMetaData(\SelectQuery $query, int $page = 0) : array {
    $receipts_count = $query->execute()->rowCount();
    $pages_count = !empty($receipts_count) ? ceil($receipts_count / $this->pageSize) : 0;

    $result = [
      'currentPage' => $page,
      'pageCount' => $pages_count,
      'perPage' => $this->pageSize,
      'totalCount' => $receipts_count,
    ];

    return $result;
  }

  /**
   * Prepare available receipt attributes.
   *
   * @return array
   *   Receipt attributes.
   */
  public static function prepareReceiptAttributes() {
    return array(
      'id' => NULL,
      'entity_id' => NULL,
      'entity_type' => NULL,
      'amount' => NULL,
      'card_type' => NULL,
      'card_number' => NULL,
      'custom' => NULL,
      'customer_name' => NULL,
      'phone' => NULL,
      'auth_code' => NULL,
      'created' => NULL,
      'checkN' => NULL,
      'description' => NULL,
      'email' => NULL,
      'invoice_id' => NULL,
      'jobs' => NULL,
      'payment_method' => NULL,
      'custom_payment_method' => NULL,
      'pending' => NULL,
      'payment_flag' => NULL,
      'refunds' => NULL,
      'refund_type' => NULL,
      'transaction_id' => NULL,
      'trans_hash' => NULL,
      'transaction_type' => NULL,
      'total_amount' => NULL,
      'trips_id' => NULL,
      'zip_code' => NULL,
      'role' => NULL,
      'uid' => NULL,
      'refund_notes' => NULL,
    );
  }

}
