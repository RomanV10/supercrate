<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class AmountOption.
 *
 * @package Drupal\move_services_new\Util
 */
class AmountOption extends BaseEnum {

  const PAYABLE = 0;
  const RECEIVABLE = 1;
  const DEDUCT = 2;

}
