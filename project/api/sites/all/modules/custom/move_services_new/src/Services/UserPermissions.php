<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class UserPermissions.
 *
 * @package Drupal\move_services_new\Services
 */
class UserPermissions {

  private $nid = NULL;
  private $request = [];
  private $requestRetrieve = [];
  private $user = NULL;
  private $clients = NULL;

  /**
   * UserPermissions constructor.
   *
   * @param \stdClass $user
   *   User object.
   * @param array $request
   *   Request user.
   * @param array $request_retrieve
   *   Request retrieve.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function __construct(\stdClass $user, array $request, array $request_retrieve) {
    $this->request = $request;
    $this->requestRetrieve = $request_retrieve;
    $this->nid = $request_retrieve['nid'];

    $this->user = user_load($user->uid);
    $this->clients = new Clients();
    $this->clients->setUser($this->user);
  }

  /**
   * One method to collect if user can update request.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  public function isUserCanUpdateRequest() : bool {
    $can_update = FALSE;

    if ($this->isManagerOrSales()) {
      $can_update = $this->isManagerCanUpdateRequest();
    }

    if ($this->isHomeEstimate()) {
      $can_update = TRUE;
    }

    if ($this->isAdmin()) {
      $can_update = $this->isAdminCanUpdateRequest();
    }

    if ($this->isAuthor()) {
      $can_update = $this->isAuthorCanUpdateRequest();
    }

    if ($this->isForeman()) {
      $can_update = $this->isForemanCanUpdateRequest();
      $can_update = $can_update || $this->foremanCanUpdateOtherRequest($this->requestRetrieve, $this->user->uid);
    }

    if ($this->isHelper()) {
      $can_update = $this->isHelperCanUpdateRequest();
    }

    if (!empty($this->request['global_user_uid'])) {
      $can_update = TRUE;
    }

    return $can_update;
  }

  /**
   * Check if manager or sales can edit request.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  public function isManagerCanUpdateRequest() : bool {
    $result = FALSE;
    $is_manager_of_request = (new Sales($this->nid))->checkIsManagerOfRequest($this->nid);
    $can_edit_other_leads = (new Sales($this->nid))->checkUserPermRequest($this->nid, array('administrator'), 'canEditOtherLeads');

    if ($can_edit_other_leads || $is_manager_of_request || !empty($this->request['global_user_uid'])) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Check if home estimator can update his request.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  public function isHomeEstimatorCanUpdateRequest() : bool {
    return $this->isHomeEstimatorOfRequest();
  }

  /**
   * Check if admin can update request.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  public function isAdminCanUpdateRequest() : bool {
    return (new Sales($this->nid))->checkUserPermRequest($this->nid, array('administrator'), 'canEditOtherLeads');
  }

  /**
   * Check if author of request can update his request.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  public function isAuthorCanUpdateRequest() : bool {
    return TRUE;
  }

  /**
   * Check if foreman of request can update his request.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  public function isForemanCanUpdateRequest() : bool {
    return $this->isForemanOfRequest();
  }

  /**
   * Check if helper of request can update his request.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  public function isHelperCanUpdateRequest() : bool {
    return $this->isHelperOfRequest();
  }

  /**
   * Check if user if home estimator for this request.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  public function isHomeEstimatorOfRequest() : bool {
    $home_estimator = $this->requestRetrieve['home_estimator']['value'] ?? NULL;
    return $home_estimator == $this->user->uid ? TRUE : FALSE;
  }

  /**
   * Is this foreman is request foreman.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  private function isForemanOfRequest() {
    $request_foremans = $this->requestRetrieve['field_foreman']['value'] ?? [];
    $request_foremans = (is_array($request_foremans)) ? $request_foremans : [$request_foremans];

    $result = FALSE;
    foreach ($request_foremans as $foreman) {
      if ($foreman == $this->user->uid) {
        $result = TRUE;
      }
    }
    return $result;
  }

  /**
   * Is this helper is request helper.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  private function isHelperOfRequest() {
    $request_helpers = $this->requestRetrieve['field_helper']['value'] ?? [];
    $request_helpers = (is_array($request_helpers)) ? $request_helpers : [$request_helpers];

    $result = FALSE;
    foreach ($request_helpers as $helper) {
      if ($helper == $this->user->uid) {
        $result = TRUE;
      }
    }
    return $result;
  }

  /**
   * Is user have role home-estimator.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  private function isHomeEstimator() : bool {
    return $this->clients->checkUserRoles(array('home-estimator'));
  }

  /**
   * Check if user has role home estimator and login from home estimate.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isHomeEstimate() : bool {
    return ($this->isHomeEstimator() && $this->clients->isUserLoginFromHomeEstimate()) ? TRUE : FALSE;
  }

  /**
   * Is user have role sales or manager.
   *
   * @return bool
   *   TRUE or FAlSE.
   */
  private function isManagerOrSales() : bool {
    return $this->clients->checkUserRoles(array('manager', 'sales'));
  }

  /**
   * Is user is administrator.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  private function isAdmin() {
    return $this->clients->checkUserRoles(array('administrator'));
  }

  /**
   * Is user is foreman.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  private function isForeman() {
    return $this->clients->checkUserRoles(array('foreman'));
  }

  /**
   * Is user is helper.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  private function isHelper() {
    return $this->clients->checkUserRoles(array('helper'));
  }

  /**
   * Is user is author of request.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  private function isAuthor() {
    return $this->requestRetrieve['uid']['uid'] == $this->user->uid ? TRUE : FALSE;
  }

  /**
   * Check if user can see what client is view request.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isUserCanSeeOnlineCustomers() : bool {
    $result = FALSE;

    if ($this->isAdmin() || $this->user->id == '1') {
      $result = TRUE;
    }

    if ($this->isManagerOrSales()) {
      $settings = Users::getUserSettings($this->user->uid);
      if (!empty($settings['permissions']['canSeeWhatCustomerViewRequest'])) {
        $result = TRUE;
      }
    }

    return $result;
  }

  public static function isUserCanUpdateExtraService(int $uid, int $nid) {
    $clientsInstance = new Clients($uid);
    $can_update = $clientsInstance->checkUserRoles(array(
      'administrator',
      'manager',
      'sales',
      'authenticated user',
    ));

    // If user have foreman role.
    if ($clientsInstance->checkUserRoles(array('foreman'))) {
      $requests = (new MoveRequest())->getForHelperRequest('foreman', $uid);
      $requests_keys = array_keys($requests);
      if (in_array($nid, $requests_keys)) {
        $can_update = TRUE;
      }
    }

    return $can_update;
  }

  /**
   * Check if foreman can update other "moving storage" or "overnight" request.
   *
   * @param array $requestRetrieve
   *   Request.
   * @param int $uid
   *   User id.
   *
   * @return bool
   *   Can update or not.
   */
  public static function foremanCanUpdateOtherRequest(array $requestRetrieve, int $uid) {
    $can_update = FALSE;
    if ($requestRetrieve['service_type']['raw'] == RequestServiceType::MOVING_AND_STORAGE ||
      $requestRetrieve['service_type']['raw'] == RequestServiceType::OVERNIGHT) {
      if (!empty($requestRetrieve['storage_id'])) {
        $select = db_select('field_data_field_foreman', 'ff');
        $select->fields('ff', ['field_foreman_target_id']);
        $select->condition('ff.entity_id', $requestRetrieve['storage_id']);
        $foreman = $select->execute()->fetchField();

        if ($foreman == $uid) {
          $can_update = TRUE;
        }
      }
    }

    return $can_update;
  }

}
