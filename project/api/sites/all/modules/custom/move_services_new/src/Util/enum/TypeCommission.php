<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TypeCommission.
 *
 * @package Drupal\move_services_new\Util
 */
class TypeCommission extends BaseEnum {

  const CACHE = 0;
  const PERCENTAGE = 1;

}
