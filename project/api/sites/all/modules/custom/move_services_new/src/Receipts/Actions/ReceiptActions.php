<?php

namespace Drupal\move_services_new\Receipts\Actions;

use Drupal\move_inventory\Actions\BaseCRUDInterface;
use Drupal\move_services_new\Receipts\Actions\SubActions\ReceiptSubActions;

/**
 * Class ReceiptActions.
 *
 * @package Drupal\move_services_new\Receipts\Actions
 */
class ReceiptActions implements BaseCRUDInterface {

  private $id;
  private $entityId;
  private $entityType;

  /**
   * Get entity type.
   *
   * @return mixed
   *   Entity type.
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * Get entity id.
   *
   * @return mixed
   *   Entity id.
   */
  public function getEntityId() {
    return $this->entityId;
  }

  /**
   * Set entity id.
   *
   * @param mixed $entityId
   *   Entity id.
   */
  public function setEntityId($entityId): void {
    $this->entityId = $entityId;
  }

  /**
   * Set entity type.
   *
   * @param mixed $entityType
   *   Entity type.
   */
  public function setEntityType($entityType): void {
    $this->entityType = $entityType;
  }

  /**
   * ReceiptActions constructor.
   *
   * @param int|null $id
   *   Receipt id.
   */
  public function __construct(?int $id = NULL) {
    $this->id = $id;
  }

  /**
   * Create receipt and save it in db.
   *
   * @param array $data
   *   Receipt data.
   *
   * @return \DatabaseStatementInterface|int|mixed
   *   Receipt id.
   *
   * @throws \Exception
   */
  public function create(array $data = []) {
    return (new ReceiptSubActions())->create($data);
  }

  /**
   * Retrieve receipt.
   *
   * @return array|mixed
   *   Receipt.
   *
   * @throws \ServicesException
   */
  public function retrieve() {
    $receiptSubActions = new ReceiptSubActions();
    $receiptSubActions->setEntityType($this->getEntityType());
    $receiptSubActions->setEntityId($this->getEntityId());

    return $receiptSubActions->retrieve();
  }

  /**
   * Update receipt.
   *
   * @param array $data
   *   Data for update.
   *
   * @return \DatabaseStatementEmpty|\DatabaseStatementInterface|mixed
   *   Update rows.
   *
   * @throws \ServicesException
   */
  public function update(array $data = []) {
    return (new ReceiptSubActions($this->id))->update($data);
  }

  /**
   * Delete receipt.
   *
   * @return $this|bool|int|mixed
   *   Deleted row.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function delete() {
    return (new ReceiptSubActions($this->id))->delete();
  }

}
