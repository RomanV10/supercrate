<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_branch\Services\Trucks;
use Drupal\move_delay_launch\Services\DelayLaunch;
use Drupal\move_delay_launch\Util\enum\TaskType;
use Drupal\move_long_distance\Services\Actions\SitReceiptActions;
use Drupal\move_services_new\Receipts\Actions\SubActions\ReceiptSubActions;
use Drupal\move_services_new\Receipts\Tasks\ReceiptTasks;
use Drupal\move_services_new\Services\move_request\MoveRequestSearch;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Util\enum\SmsActionTypes;
use Drupal\move_sms\Services\Sms;
use Drupal\move_storage\Services\RequestStorage;

/**
 * Class Receipt.
 *
 * @package Drupal\move_services_new\Services
 */
class Receipt extends BaseService {

  public $entityId = NULL;
  public $entityType = NULL;
  public $receiptId = NULL;

  /**
   * Receipt constructor.
   *
   * @param int|null $entity_id
   *   Id entity.
   * @param int|null $entity_type
   *   Type entity. Check EntityTypes.php class.
   */
  public function __construct(?int $entity_id = NULL, ?int $entity_type = NULL, ?int $receipt_id = NULL) {
    $this->entityId = $entity_id;
    $this->entityType = $entity_type;
    $this->receiptId = $receipt_id;
  }

  /**
   * Create receipt and save it in db.
   *
   * @param array $receipt
   *   Receipt data.
   *
   * @return mixed
   *   Receipt id.
   *
   * @throws \Exception
   */
  public function create($receipt = array()) {
    global $user;

    try {
      $reservation_flag = FALSE;
      $node_wrapper = NULL;
      $receipt_id = 0;

      if ($this->entityId && $receipt && EntityTypes::isValidValue($this->entityType)) {
        $receipt_id = db_insert('moveadmin_receipt')
          ->fields(array(
            'entity_id' => $this->entityId,
            'receipt' => serialize($receipt),
            'entity_type' => $this->entityType,
            'created' => time(),
          ))
          ->execute();


        /*
         * TODO Temp solution.
         */
        $receipt['id'] = $receipt_id;
        $receipt['entity_id'] = $this->entityId;
        $receipt['entity_type'] = $this->entityType;
        $receipt['created'] = time();
        $prepare_receipt_data = (new ReceiptSubActions())->prepareDataForUpdate($receipt);
        ReceiptTasks::insertReceipt($prepare_receipt_data);

        $invoke = $receipt;
        $invoke['id'] = $receipt_id;
        $invoke['entity_id'] = $this->entityId;
        $invoke['entity_type'] = $this->entityType;
        self::receiptInvoke('create', $invoke);
        static::insertReceiptSearch($invoke);

        if ($this->entityType == EntityTypes::MOVEREQUEST) {
          // If quickbooks connection is present and qbo settings are set
          // to alltime then post every transaction one by one.
          if (!empty(variable_get("qbo_api_access_token", FALSE))
            && (variable_get("qbo_post_settings", FALSE) == "alltime")) {

            $dataForQb = [
              'entity_id' => $this->entityId,
              'receipt' => $receipt,
              'receipt_id' => $receipt_id,
            ];

            (new DelayLaunch())->createTask(TaskType::SEND_RECEIPT_TO_QB_AFTER_PAYMENT, $dataForQb);
          }

          if (isset($receipt['payment_flag']) && $receipt['payment_flag'] == "Reservation") {
            $node_wrapper = entity_metadata_wrapper('node', $this->entityId);
            $node_wrapper->field_reservation_price->set(0);
            $node_wrapper->field_reservation_received->set(1);
            $node_wrapper->save();
            $reservation_flag = TRUE;
            (new DelayLaunch())->createTask(TaskType::SMS_SEND, [
              'actionId' => SmsActionTypes::COMPLETE_RESERVATION,
              'nid' => $this->entityId,
              'uid' => NULL,
              'user' => $user->uid,
            ]);
          }
          elseif (isset($receipt['payment_flag']) && $receipt['payment_flag'] == "Reservation by client") {
            $node_wrapper = entity_metadata_wrapper('node', $this->entityId);
            $node_wrapper->field_reservation_price->set(0);
            $node_wrapper->field_reservation_received->set(1);
            $node_wrapper->field_approve->set(3);
            $node_wrapper->save();
            $reservation_flag = TRUE;
          }

          Cache::updateCacheData($this->entityId);

          if (!empty($node_wrapper) && variable_get('is_common_branch_truck', FALSE)) {
            (new Trucks())->handlerConflictRequest($node_wrapper);
          }
          else {
            if ($reservation_flag) {
              _movecalc_get_conflict_requests($this->entityId);
            }
          }
        }

        if ($this->entityType == EntityTypes::STORAGEREQUEST) {
          $balance = new RequestStorage();
          $balance->insertReqStorBalance($this->entityId);
        }
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Receipt', $message, array(), WATCHDOG_WARNING);
    }
    finally {
      return $receipt_id;
    }
  }

  public function retrieve() : array {
    $result = array();
    $records = db_select('moveadmin_receipt', 'mr')
      ->fields('mr')
      ->condition('entity_id', $this->entityId)
      ->condition('entity_type', $this->entityType)
      ->execute()->fetchAll();


    foreach ($records as $item) {
      $receipt = unserialize($item->receipt);
      $receipt['id'] = $item->id;
      $result[] = $receipt;
    }

    return $result;
  }

  /**
   * Update receipt to database.
   *
   * @param array $receipt
   *   The data receipt's for save.
   *
   * @return int
   *   Result of operation.
   *
   * @throws \ServicesException
   */
  public function update($receipt = array()) {
    $error_flag = FALSE;
    $error_message = "";
    $result = 0;
    try {
      $result = db_update('moveadmin_receipt')
        ->fields(array('receipt' => serialize($receipt)))
        ->condition('id', $this->receiptId)
        ->execute();
      $invoke = $receipt;
      $invoke['id'] = $this->receiptId;
      $receipt['entity_id'] = $invoke['entity_id'] = static::getReceiptEntityId($this->receiptId);
      $receipt['entity_type'] = $invoke['entity_type'] = (int) static::getReceiptType($this->receiptId);

      $prepare_receipt_data = (new ReceiptSubActions())->prepareDataForUpdate($receipt);
      ReceiptTasks::updateReceipt($this->receiptId, $prepare_receipt_data);
      self::receiptInvoke('update', $invoke);

      if ($receipt['entity_type'] != EntityTypes::STORAGEREQUEST) {
        Cache::updateCacheData($receipt['entity_id']);
      }

      Payment::updateReceiptSearch($invoke);
    }
    catch (\Throwable $e) {
      $message = "Update Receipt Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Move request Receipt', $message, array(), WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Delete payment receipt.
   *
   * @return int|mixed
   *   Result db_delete receipt.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   */
  public function delete() : int {
    // TODO: Implement delete() method.
    $reciptData = db_select('moveadmin_receipt', 'mr')
      ->fields('mr', array('entity_id', 'entity_type'))
      ->condition('id', $this->receiptId)
      ->execute()->fetchAssoc();

    $result = db_delete('moveadmin_receipt')
      ->condition('id', $this->receiptId)
      ->execute();

    // If receipt exist in CIT, we delete it too.
    $sit_receipt = new SitReceiptActions();
    $is_ld_receipt = $sit_receipt::receiptExist($this->receiptId);
    if ($is_ld_receipt) {
      $sit_receipt->setReceiptId($this->receiptId);
      $sit_receipt->deleteReceipt(TRUE);
    }

    ReceiptTasks::deleteReceipt($this->receiptId);
    self::receiptInvoke('delete', $this->receiptId);
    if ($reciptData['entity_type'] != 1) {
      Cache::updateCacheData($reciptData['entity_id']);
    }
    return $result;
  }

  /**
   * Index receipt.
   *
   * @param string $operation_type
   *   Type operation.
   * @param array|int $receipt
   *   Receipt data.
   */
  private static function receiptInvoke(string $operation_type, $receipt = array()) {
    move_global_search_solr_invoke($operation_type, EntityTypes::RECEIPT, $receipt);
  }

  /**
   * Get receipt payment type.
   *
   * @param int $receipt_id
   *   Receipt id.
   *
   * @return mixed
   *   Receipt payment type.
   */
  public static function getReceiptType(int $receipt_id) {
    $select = db_select('moveadmin_receipt', 'mr')
      ->fields('mr', array('entity_type'))
      ->condition('id', $receipt_id);
    return $select->execute()->fetchField();
  }

  /**
   * Get entity id for receipt.
   *
   * @param int $receipt_id
   *   Receipt id.
   *
   * @return mixed
   *   Entity id.
   */
  public static function getReceiptEntityId(int $receipt_id) {
    $select = db_select('moveadmin_receipt', 'mr')
      ->fields('mr', array('entity_id'))
      ->condition('id', $receipt_id);
    return $select->execute()->fetchField();
  }

  /**
   * Get payment receipt data by id.
   *
   * @param int $id
   *   Receipt id.
   * @param bool $full
   *   Flag.
   *
   * @return mixed
   *   Payment receipt data.
   */
  public static function getReceiptById(int $id, $full = FALSE) {
    $select = db_select('moveadmin_receipt', 'mr')
      ->fields('mr')
      ->condition('id', $id)
      ->execute()
      ->fetchObject();

    if ($full) {
      $result = $select;
    }
    else {
      $result = unserialize($select->receipt);
      $result['id'] = $id;
      $result['entity_id'] = $select->entity_id;
      $result['entity_type'] = $select->entity_type;
    }

    return $result;
  }

  /**
   * Calculate total from receipts.
   *
   * @param array $receipts
   *   Array with receipts.
   *
   * @return float
   *   Receipts total.
   */
  public static function calculateReceiptsTotal(array $receipts) : float {
    $total = 0.0;
    foreach ($receipts as $receipt) {
      if (!is_array($receipt)) {
        $receipt = (array) $receipt;
      }
      $total += abs($receipt['amount']) ?? 0;
    }
    return $total;
  }

  /**
   * Search receipt.
   *
   * @param $data
   * @param int $entity_type
   *
   * @return mixed
   */
  public static function receiptSearch($data, $entity_type = EntityTypes::MOVEREQUEST) {
    $select = db_select('moveadmin_receipt_search', 'mrs')
      ->fields('mrs', array('entity_id'))
      ->condition('mrs.entity_type', $entity_type);

    $check_field = function ($field_name) {
      $result = FALSE;

      switch ($field_name) {
        case 'date':
        case 'amount':
        case 'cart_type':
        case 'credit_card':
        case 'customer_name':
        case 'phone':
          $result = TRUE;
          break;
      }

      return $result;
    };

    $date_interval = function ($date) {
      $search_instance = new MoveRequestSearch();
      return array(
        'date_from' => $search_instance->prepareDate($date),
        'date_to' => $search_instance->prepareDate($date, array('midnight')),
      );
    };

    foreach ($data as $name => $value) {
      if (!$check_field($name)) {
        continue;
      }

      switch ($name) {
        case 'date':
          $interval = $date_interval($value);
          $select->condition("mrs.$name", array($interval['date_from'], $interval['date_to']), 'BETWEEN');
          break;

        case 'customer_name':
        case 'phone':
          $select->condition("mrs.$name", '%' . db_like($value) . '%', 'LIKE');
          break;

        default:
          $select->condition("mrs.$name", $value);
      }
    }

    $select->groupBy('entity_id');
    $nids = $select->execute()->fetchCol();
    return $nids;
  }

  /**
   * Get all receipts needed for solr indexation.
   *
   * @return array
   */
  public static function getAllIndexReceipt() : array {
    return db_select('moveadmin_receipt', 'mr')
      ->fields('mr', array('id'))
      ->execute()->fetchCol();
  }

  /**
   * Insert receipt data for search.
   *
   * @param array $data
   *   Array data for save.
   *
   * @throws \Exception
   */
  public static function insertReceiptSearch(array $data) {
    $data = (array) $data;
    $phone = '';
    $ccardN = '';
    $card_type = '';

    // TODO need to remove this and keep only one key after check with front.
    if (!empty($data['cctype'])) {
      $card_type = $data['cctype'];
    }

    if (!empty($data['card_type'])) {
      $card_type = $data['card_type'];
    }

    if (isset($data['phone'])) {
      $phone = (int) preg_replace('/[^0-9.]+/', '', $data['phone']);
    }
    if (isset($data['ccardN'])) {
      $ccardN = preg_replace('/[^0-9.]+/', '', $data['ccardN']);
      $count_number = strlen($data['ccardN']);
      $start_from = $count_number - 4;
      if ($start_from > 0) {
        $ccardN = substr($ccardN, $start_from, 4);
      }
    }
    db_insert('moveadmin_receipt_search')
      ->fields(array(
        'id' => $data['id'],
        'entity_id' => $data['entity_id'],
        'entity_type' => $data['entity_type'],
        'date' => !empty($data['created']) ? (int) $data['created'] : time(),
        'amount' => (float) $data['amount'],
        'cart_type' => $card_type,
        'credit_card' => $ccardN,
        'customer_name' => !empty($data['name']) ? $data['name'] : '' ,
        'phone' => $phone,
      ))
      ->execute();
  }

  public function checkReceiptAmount() {
    $entityIdAll = $this->getReceiptEntityIdAll();

    foreach ($entityIdAll as $entity_id) {
      $data = [];
      $rec = new Receipt($entity_id['entity_id'], $entity_id['entity_type']);
      $old_receipts = $rec->retrieve();
      $new_receipts = $rec->getReceiptsFromTemp();

      $old_receipts_total = static::calculateReceiptsTotal($old_receipts);
      $new_receipts_total = static::calculateReceiptsTotal($new_receipts);
      $data['old']['total'] = $old_receipts_total;
      $data['new']['total'] = $old_receipts_total;
      foreach ($old_receipts as $receipt) {
        if (!is_array($receipt)) {
          $receipt = (array) $receipt;
        }
        $data['old']['receipts'][] = $receipt['amount'];
      }

      foreach ($new_receipts as $receipt) {
        if (!is_array($receipt)) {
          $receipt = (array) $receipt;
        }
        $data['new']['receipts'][] = $receipt['amount'];
      }

      $this->write(['entity_id' => $entity_id['entity_id'], 'entity_type' => $entity_id['entity_type'], 'result' => $new_receipts_total == $old_receipts_total ? 1 : 0, 'data' => serialize($data)]);
    }
  }

  private function getReceiptsFromTemp() {
    $receipts = db_select('temp_moveadmin_receipt_search', 'tmrs')
      ->fields('tmrs')
      ->condition('entity_id', $this->entityId)
      ->condition('entity_type', $this->entityType)
      ->execute()->fetchAll();

    return $receipts;
  }

  private function getReceiptEntityIdAll() {
    $receipts = db_select('moveadmin_receipt', 'tmrs')
      ->fields('tmrs', array('entity_id', 'entity_type'))
      ->groupBy('entity_id')
      ->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return $receipts;
  }

  public function write($write) {
    $message = fopen("receiptlog.txt", 'a');
    fwrite($message, 'Entity id: ' . $write['entity_id'] .' Entity type: ' . $write['entity_type'] . '      Result: ' . $write['result'] . '  Data: ' . $write['data'] . "\r\n");
  }

}

/**
 * Class ReceiptException.
 *
 * @package Drupal\move_services_new\Services
 */
class ReceiptException extends \Exception {

  public function saveDataBase() {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()}";
    watchdog('Receipt', $message, array(), WATCHDOG_WARNING);
  }

}
