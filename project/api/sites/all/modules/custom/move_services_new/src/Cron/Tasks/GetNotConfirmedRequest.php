<?php

namespace Drupal\move_services_new\Cron\Tasks;

use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class GetNotConfirmedRequest.
 *
 * @package Drupal\move_services_new\Cron
 */
class GetNotConfirmedRequest implements CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute(): void {
    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_approve', 'fa', 'n.nid = fa.entity_id AND fa.bundle = :type', array(':type' => 'move_request'));
    $query->leftJoin('field_data_field_date', 'fd', 'n.nid = fd.entity_id AND fd.bundle = :type', array(':type' => 'move_request'));
    $query->leftJoin('field_data_field_actual_start_time', 'fdast', 'n.nid = fdast.entity_id AND fdast.bundle = :type', array(':type' => 'move_request'));
    $query->leftJoin('field_data_field_reservation_received', 'fr', 'n.nid = fr.entity_id AND fr.bundle = :type', array(':type' => 'move_request'));

    $query->fields('n', array('nid'));
    $query->fields('fd', array('field_date_value'));
    $query->fields('fdast', array('field_actual_start_time_value'));
    $query->condition('n.type', 'move_request');
    $query->condition('n.status', NODE_PUBLISHED);
    $query->condition('fa.field_approve_value', RequestStatusTypes::NOT_CONFIRMED);
    $query->condition('fr.field_reservation_received_value', 0);
    $query->orderBy('created', 'ASC');
    $requests = $query->execute()->fetchAll(\PDO::FETCH_ASSOC) ?? FALSE;

    if ($requests) {
      $queue = \DrupalQueue::get('move_services_new_change_from_not_confirmed_to_expired');
      $queue->createQueue();
      $chunk = array_chunk($requests, 3);
      foreach ($chunk as $item) {
        $queue->createItem($item);
      }
    }
  }

}
