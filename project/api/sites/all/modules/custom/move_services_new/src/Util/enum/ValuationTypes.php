<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TypeStatistics.
 *
 * @package Drupal\move_services_new\Util
 */
class ValuationTypes extends BaseEnum {

  const PER_POUND = 1;
  const FULL_VALUE = 2;

}