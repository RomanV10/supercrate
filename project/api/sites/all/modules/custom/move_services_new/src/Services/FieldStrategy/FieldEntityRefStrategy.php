<?php

namespace Drupal\move_services_new\Services\FieldStrategy;

/**
 * Class fieldEntityRefStrategy.
 *
 * @package Drupal\move_services_new\Services\FieldStrategy
 */
class FieldEntityRefStrategy implements FieldArrayStrategyInterface {

  /**
   * Create data.
   *
   * @param mixed $values
   *   Value os field.
   * @param string $widget_type
   *   The widget type.
   * @param int $count_values
   *   Count values.
   * @param bool $multi
   *   Indicated that field is multi.
   * @param array $field_info
   *   Information about field.
   *
   * @return mixed
   *   Result
   */
  public function createData($values, string $widget_type, int $count_values, bool $multi = FALSE, array $field_info = array()) {
    $data = array();
    if ($widget_type == 'options_select' || $widget_type == 'options_buttons') {
      for ($i = 0; $i < $count_values; $i++) {
        $data[LANGUAGE_NONE][] = $multi ? $values[$i] : $values;
      }
    }
    if ($widget_type == 'entityreference_autocomplete') {
      for ($i = 0; $i < $count_values; $i++) {
        $value = $multi ? $values[$i] : $values;
        switch ($field_info['settings']['target_type']) {
          case 'node':
            $data[LANGUAGE_NONE][$i]['target_id'] = "Move request ($value)";
            break;

          case 'user':
            $user = user_load((int) $value);
            if ($user) {
              $mail = $user->mail;
              $data[LANGUAGE_NONE][$i]['target_id'] = "$mail ($value)";
            }
            break;
        }
      }
    }

    return $data;
  }

}
