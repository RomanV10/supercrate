<?php

namespace Drupal\move_services_new\Receipts\Routes;

use Drupal\move_services_new\Receipts\Controllers\ReceiptController;

/**
 * Class ReceiptRoute.
 *
 * @package Drupal\move_services_new\Receipts\Routes
 */
class ReceiptRoute {

  /**
   * Receipt route.
   *
   * @return array
   *   Definitions.
   */
  public static function getRoutes() {
    $controller_file = [
      'type' => 'php',
      'module' => 'move_services_new',
      'name' => 'src/Receipts/Controllers/ReceiptController',
    ];

    return array(
      'move_receipts' => array(
        'operations' => array(
          'create' => array(
            'help' => 'Set receipt\'s data for request.',
            'callback' => ReceiptController::class . '::createReceipt',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'help' => 'Update receipt\'s data for request.',
            'callback' => ReceiptController::class . '::updateReceipt',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'help' => 'Get receipt data for request.',
            'callback' => ReceiptController::class . '::getReceipt',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => 0,
                'source' => array('path' => 1),
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'help' => 'Delete receipt data for request.',
            'callback' => ReceiptController::class . '::deleteReceipt',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'rid',
                'description' => 'Receipt id.',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'search_receipt' => array(
            'help' => 'Search move requests by receipt',
            'callback' => ReceiptController::class . '::searchReceipt',
            'file' => $controller_file,
            'args' => array(
              array(
                'name' => 'data',
                'optional' => FALSE,
                'type' => 'array',
                'description' => 'Conditions for search of nodes',
                'default value' => array(),
                'source' => array('data' => 'data'),
              ),
              array(
                'name' => 'entity_type',
                'optional' => FALSE,
                'type' => 'int',
                'default value' => 0,
                'source' => array('data' => 'entity_type'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );

  }

}
