<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class Roles.
 *
 * @package Drupal\move_services_new\Util
 */
class PaymentFlag extends BaseEnum {

  const REGULAR = 1;
  const RESERVATION = 2;
  const CONTRACT = 3;
  const INVOICE = 4;

}
