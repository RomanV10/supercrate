<?php

namespace Drupal\move_services_new\Cron\Tasks;

/**
 * Interface CronInterface.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
interface CronInterface {

  /**
   * Execute task by cron.
   */
  public static function execute() : void;

}
