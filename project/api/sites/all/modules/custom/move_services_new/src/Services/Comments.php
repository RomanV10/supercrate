<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\move_request\MoveRequestSearch;
use Drupal\move_services_new\System\Extra;

/**
 * Class CRUDCommentsResource.
 */
class Comments extends BaseService {
  protected $nid = NULL;
  protected $cid = NULL;
  protected $node = NULL;
  protected $nodeWrapper = NULL;
  protected $user = NULL;
  protected static $commentsRoles = array('sales', 'manager', 'administrator');
  private $isManager = FALSE;
  private $isAdmin = FALSE;
  public $timeZone = 'UTC';

  /**
   * Constructor of Comments service.
   *
   * @param int $nid
   *   Node id.
   * @param int $cid
   *   Comment id.
   */
  public function __construct($nid = NULL, $cid = NULL) {
    global $user;
    $this->user = $user;
    $this->timeZone = Extra::getTimezone();

    $clients = new Clients($user->uid);
    $this->isManager = $clients->checkUserRoles(array('manager', 'sales'));
    $this->isAdmin = $clients->checkUserRoles(array('administrator', 'owner'));

    if ($nid && $node = node_load((int) $nid)) {
      $this->node = $node;
      $this->nid = (int) $nid;
      $this->nodeWrapper = entity_metadata_wrapper('node', $this->node);
    }
    if ($cid) {
      $this->cid = (int) $cid;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function retrieve() : array {
    $result = array();
    try {
      if ($this->node && (new Sales())->checkUserPermRequest((int) $this->node->nid)) {
        $result = $this->getCommentsByNode();
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Comments', $message, array(), WATCHDOG_ERROR);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @param array $data
   *   Data with message of comment's.
   *
   * @return int|bool
   *   Comment id if comment is saved or FALSE otherwise.
   *
   * @throws \Exception
   */
  public function create($data = array()) {
    $result = FALSE;
    if (isset($data['message']) && trim($data['message']) && $this->nid && (!empty($data['sms']) || (new Sales())->checkUserPermRequest($this->nid))) {

      // Check user format.
      $write_mess = $this->clearMessFormat($data);

      $comment = array(
        'nid' => $this->nid,
        'uid' => $this->user->uid,
        'cid' => 0,
        'pid' => 0,
        'mail' => '',
        'subject' => '',
        'is_anonymous' => FALSE,
        'status' => COMMENT_PUBLISHED,
        'language' => LANGUAGE_NONE,
        'comment_body' => array(
          LANGUAGE_NONE => array(
            0 => array(
              'value' => $write_mess['value'],
              'format' => $write_mess['format'],
            ),
          ),
        ),
        'field_comment_user_time_zone' => array(
          LANGUAGE_NONE => array(
            0 => array(
              'value' => $data['time_zone'] ?? $this->timeZone,
            ),
          ),
        ),
        'field_sms' => [
          LANGUAGE_NONE => array(
            0 => array(
              'value' => $data['sms'] ?? NULL,
            ),
          ),
        ],
        'field_sms_sid' => [
          LANGUAGE_NONE => array(
            0 => array(
              'value' => $data['sms_sid'] ?? '',
            ),
          ),
        ],
        'field_sms_status' => [
          LANGUAGE_NONE => array(
            0 => array(
              'value' => $data['sms_status'] ?? '',
            ),
          ),
        ],
        'field_sms_to' => [
          LANGUAGE_NONE => array(
            0 => array(
              'value' => $data['sms_to'] ?? '',
            ),
          ),
        ],
        'field_sms_from' => [
          LANGUAGE_NONE => array(
            0 => array(
              'value' => $data['sms_from'] ?? '',
            ),
          ),
        ],
      );
      comment_save((object) $comment);
      $cids = $this->getCids();
      $result = end($cids);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function update($data = array()) {
    if ($this->cid && $data['message']) {
      try {
        $text_format = (isset($data['format'])) ? $data['format'] : 'full_html';
        $write_mess = $this->clearMessFormat(
          array(
            'message' => $data['message'],
            'format' => $text_format,
          )
        );
        $wrapper = entity_metadata_wrapper('comment', $this->cid);
        $wrapper->comment_body->set($write_mess);
        $wrapper->save();
      }
      catch (\Throwable $exc) {
        watchdog('Update Comment Error: ', '<pre>' . print_r($exc, TRUE) . '</pre>');
        return FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    if ($this->cid) {
      comment_delete_multiple(array($this->cid));
      db_delete('move_request_comments')
        ->condition('cid', $this->cid)
        ->execute();
    }
  }

  /**
   * Helper method for clear text if needed.
   *
   * @param array|null $data
   *   Array with message and name of text format.
   *
   * @return array
   *   Text format and message.
   */
  protected function clearMessFormat(?array $data = array()) {
    $text_format = 'full_html';

    if (isset($data['message'])) {
      if (isset($data['format']) && $this->checkFormat($data['format'])) {
        $text_format = $data['format'];
      }

      return array('format' => $text_format, 'value' => $data['message']);
    }
  }

  /**
   * Get all comments by uid.
   *
   * @return array
   *   All comments.
   */
  public function getAllCommentsByUid() {
    $all_comments = NULL;

    if ($this->checkUser()) {
      $all_comments = $this->getCommentsNodesByManager();
    }
    elseif ($this->checkUserClient()) {
      $all_comments = $this->getCommentsNodesByUser();
    }

    return $all_comments;
  }

  /**
   * Helper method to check user permissions.
   *
   * @return bool
   *   Status.
   */
  protected function checkUser() {
    $check_user = new Clients($this->user->uid);
    $exist_user = $check_user->checkExistUser((array) $this->user);
    $check_roles = $check_user->checkUserRoles(static::$commentsRoles);

    return $check_roles ? $exist_user : FALSE;
  }

  /**
   * Checking what user have own requests.
   *
   * @return bool
   *
   * @throws \SearchApiException
   */
  protected function checkUserClient() {
    return $this->getUserRequests() ? TRUE : FALSE;
  }

  /**
   * Get NIDs requests by author.
   *
   * @return mixed
   * @throws \SearchApiException
   */
  protected function getUserRequests() {
    // Check exist requests at user.
    return db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('uid', $this->user->uid)
      ->execute()
      ->fetchCol();
  }

  /**
   * Helper method to check what comment wrote admin(manager).
   *
   * @param int $cid
   *   Comment id.
   *
   * @return int
   *   Flag.
   */
  public function checkAdmin($cid) {
    $comment = db_select('move_request_comments', 'mrc')
      ->fields('mrc', array('manager_flag'))
      ->condition('mrc.cid', $cid)
      ->execute()
      ->fetchCol();
    return (int) reset($comment);
  }

  /**
   * Get comments by nodes by sales person.
   */
  protected function getCommentsNodesByManager($fields = TRUE) {
    $all_comments = array();
    if ($fields) {
      $cs = db_select('move_request_comments', 'mrc')
        ->fields('mrc', array('cid', 'nid'))
        ->condition('mrc.manager_flag', 0)
        ->condition('mrc.manager_uid', $this->user->uid)
        ->execute()
        ->fetchAll();

      foreach ($cs as $comment) {
        $this->node = node_load($comment->nid);
        $all_comments[$comment->nid] = $this->getCommentsByNode($fields);
      }
    }
    else {
      $move_search = new MoveRequestSearch();
      $manager_nids = $move_search->getAllRequestsManagerPermission();
      if ($manager_nids) {
        $all_comments = db_select('move_request_comments', 'mrc')
          ->fields('mrc', array('cid'))
          ->condition('mrc.manager_flag', 0)
          ->condition('mrc.nid', $manager_nids, 'IN')
          ->execute()
          ->fetchCol();
      }
    }

    return $all_comments;
  }

  /**
   * Get comments by nodes by normal user.
   */
  protected function getCommentsNodesByUser($fields = TRUE) {
    $all_comments = array();
    $nids = $this->getUserRequests();
    if ($nids) {
      if ($fields) {
        foreach ($nids as $nid) {
          $this->node = node_load($nid);
          if ((new Sales())->checkUserPermRequest($nid) && $this->getCommentsByNode($fields)) {
            $all_comments[$nid] = $this->getCommentsByNode($fields);
          }
        }
      }
      else {
        $all_comments = db_select('move_request_comments', 'mrc')
          ->fields('mrc', array('cid'))
          ->condition('mrc.manager_flag', 0)
          ->condition('mrc.nid', $nids, 'IN')
          ->execute()
          ->fetchCol();
      }
    }

    return $all_comments;
  }

  /**
   * Get all new comments for administration.
   */
  protected function getAdminAllNewComments() {
    $select = db_select('move_request_comments', 'mrc')
      ->fields('mrc', array('nid'))
      ->distinct()
      ->condition('mrc.read_status', 0)
      ->condition('mrc.manager_flag', 0)
      ->orderBy('mrc.changed', 'DESC');
    return $select->execute()->fetchCol();
  }

  /**
   * Checking what comment still unread.
   *
   * @param int $cid
   *   Comment id.
   *
   * @return mixed
   *   Comment: read status.
   */
  protected function checkCommentReadStatus($cid) {
    return db_select('move_request_comments', 'mrc')
      ->fields('mrc', array('read_status'))
      ->condition('mrc.cid', (int) $cid)
      ->execute()
      ->fetchField();
  }

  /**
   * Helper method to detect what user role is admin.
   *
   * @return bool
   *   Result of check.
   */
  public function checkAdminRoles($roles = array('administrator', 'manager', 'sales')) {
    $intersect = FALSE;
    if (is_object($this->user)) {
      $intersect = array_intersect((array) $this->user->roles, $roles);
    }
    return $intersect;
  }

  /**
   * Method for action "admin_all_comments".
   *
   * @return array
   *   Array of NIDs.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function getAdminAllNodesWithComments() {
    $result = array();

    if ((new Sales())->checkAdminRoles()) {
      $query = db_select('move_request_comments', 'mrc');
      $query->innerJoin('node', 'n', 'n.nid = mrc.nid');
      $query->fields('mrc', array('nid'));
      $query->orderBy('mrc.created', 'DESC');
      $query->range(0, 50);
      $result = $query->execute()->fetchCol();
    }
    elseif ($this->checkUser()) {
      $result = db_select('move_request_sales_person', 'sp')
        ->fields('sp', array('nid'))
        ->condition('sp.uid', $this->user->uid)
        ->range(0, 50)
        ->execute()
        ->fetchCol();
    }

    $result = array_unique($result);
    $requests = array();
    // Load all request object by nid.
    foreach ($result as $nid) {
      $obj = new MoveRequest($nid);
      array_push($requests, $obj->retrieve());
    }

    return $requests;
  }

  /**
   * Get all comments by node.
   *
   * @param bool $fields
   *   Flag to add comment fields to result array.
   *
   * @return array
   *   All comments from node.
   *
   * @throws \Exception
   */
  public function getCommentsByNode($fields = TRUE) : array {
    $all_comments = array();

    try {
      $cids = $this->getCids();
      $comments = comment_load_multiple($cids);
      if ($comments) {
        comment_prepare_thread($comments);

        foreach ($comments as $comment) {
          if ($fields) {
            $all_comments[$comment->cid] = $this->getFieldsComment($comment);
          }
          else {
            $all_comments[] = $comment->cid;
          }
        }
      }
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Comments', $message, array(), WATCHDOG_ERROR);
    }
    finally {
      return $all_comments;
    }
  }

  /**
   * Get fields for comments.
   *
   * @param object $comment
   *   Object of comment.
   *
   * @return mixed|array
   *   Comment data.
   *
   * @throws \Exception
   */
  protected function getFieldsComment($comment) {
    $result['cid'] = (int) $comment->cid;
    $result['uid'] = (int) $comment->uid;
    $result['name'] = $comment->name;
    $result['admin'] = $this->checkAdmin($comment->cid);
    $result['read'] = property_exists($comment, 'custom_new') ? (int) $comment->custom_new : 0;
    $result['created'] = (int) $comment->created;
    $result['sms'] = $comment->field_sms['und'][0]['value'] ?? 0;
    $result['sms_sid'] = $comment->field_sms_sid['und'][0]['value'] ?? '';
    $result['sms_status'] = $comment->field_sms_status['und'][0]['value'] ?? '';
    $result['sms_to'] = $comment->field_sms_to['und'][0]['value'] ?? '';
    $result['sms_from'] = $comment->field_sms_from['und'][0]['value'] ?? '';
    $client = (new Clients())->getUserFieldsData($comment->uid, FALSE);

    // Comment Body.
    $field = field_info_field('comment_body');
    $langcode = field_is_translatable('comment', $field) ? entity_language('comment', $comment) : LANGUAGE_NONE;
    $comment_body = $comment->comment_body[$langcode][0];
    $comment_text = '';
    if (isset($comment_body['format']) && $comment_body['format'] != 'plain_text') {
      $comment_text = trim(check_markup($comment_body['value'], $comment_body['format']));
    }
    elseif (isset($comment_body['format']) && $comment_body['format'] == 'plain_text') {
      $comment_text = trim(drupal_html_to_text($comment_body['value']));
    }
    $result['comment_body'] = $comment_text;
    $result['user_first_name'] = isset($client['field_user_first_name']) ? $client['field_user_first_name'] : '';
    $result['user_last_name'] = isset($client['field_user_last_name']) ? $client['field_user_last_name'] : '';
    $result['time_zone'] = !empty($comment->field_comment_user_time_zone) ? $comment->field_comment_user_time_zone[$langcode][0]['value'] : $this->timeZone;
    $result['created_date_string'] = Extra::date('F j, Y, g:i A', $result['created'], $result['time_zone']);
    // Comment Subject.
    $subject = truncate_utf8(trim(decode_entities(strip_tags($comment_text))), 29, TRUE);
    // Edge cases where the comment body is populated only by HTML tags will
    // require a default subject.
    if ($subject == '') {
      $subject = t('(No subject)');
    }
    $result['subject'] = $subject;

    return $result;
  }

  /**
   * Change comment status to Read.
   *
   * @param object $comment
   *   Comment object.
   *
   * @return bool
   *   Result of operation.
   */
  public function setReadComment($comment) {
    $result = FALSE;

    if ((new Sales())->checkUserPermRequest($comment->nid) && !$this->checkCommentReadStatus($comment->cid)) {
      $record = array(
        'cid' => $comment->cid,
        'read_status' => 1,
      );
      drupal_write_record('move_request_comments', $record, 'cid');
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Change all users comments to read for admin.
   *
   * @return bool
   *   Result db update.
   */
  public static function setReadCommentAll() {
    $result = db_update('move_request_comments')
      ->fields(array('read_status' => 1))
      ->condition('manager_flag', 0)
      ->condition('read_status', 0)
      ->execute();

    return $result;
  }

  /**
   * Count of unread comments for user.
   *
   * @return int|null
   *   Count unread messages of NULL.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \SearchApiException
   * @throws \ServicesException
   */
  public function getUnreadCountUser() {
    $results = 0;
    $uid = $this->user->uid;
    $check_client = $this->checkUserClient();
    $check_admin = (new Sales())->checkAdminRoles(array('administrator'));

    if (($this->isManager || $check_client) && !$check_admin) {
      $nids = array();

      if ($this->isManager) {
        $move_search = new MoveRequestSearch();
        $nids = $move_search->getAllRequestsManagerPermission();
      }
      elseif ($check_client) {
        $nids = $this->getUserRequests();
      }

      if ($nids) {
        // If wrote comment is manager then $manager flag set to 0.
        // If wrote comment is user then $manager flag set to 1.
        $manager = Sales::isManager($uid) ? 0 : 1;

        $select = db_select('move_request_comments', 'mrc')
          ->condition('mrc.nid', $nids, 'IN')
          ->condition('mrc.manager_flag', $manager)
          ->condition('mrc.read_status', 0)
          ->countQuery();

        $results = $select->execute()->fetchField();
      }
    }
    elseif ($check_admin) {
      $select = db_select('move_request_comments', 'mrc')
        ->condition('mrc.manager_flag', 0)
        ->condition('mrc.read_status', 0)
        ->countQuery();
      $results = $select->execute()->fetchField();
    }

    return ($results) ? $results : NULL;
  }

  /**
   * Get all new comments by uid.
   *
   * @param bool $partly_request
   *   Flag.
   *
   * @return array
   *   New comments.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function getAllNewComments(bool $partly_request = FALSE) {
    $new_comments = array();
    if ($this->user) {
      // Check user permissions.
      if ((new Sales())->checkAdminRoles(array('administrator'))) {
        $new_comments = $this->getAdminAllNewComments();
      }
      else {
        $cids = $this->getCommentsNodesByManager(FALSE);

        // Normal user.
        if (!$cids) {
          $cids = $this->getCommentsNodesByUser(FALSE);
        }

        $manager = Sales::isManager($this->user->uid) ? 0 : 1;
        if ($cids) {
          $new_comments = db_select('move_request_comments', 'mrc')
            ->fields('mrc', array('nid'))
            ->distinct()
            ->condition('mrc.cid', $cids, 'IN')
            ->condition('mrc.manager_flag', $manager)
            ->condition('mrc.read_status', 0, '=')
            ->orderBy('mrc.changed', 'DESC')
            ->execute()
            ->fetchCol();
        }
      }
    }

    $requests = array();

    // Load all request object by nid.
    foreach ($new_comments as $nid) {
      $obj = new MoveRequest($nid);
      $type = $partly_request ? 1 : 0;
      $requests[] = $obj->getNode($nid, $type);
    }

    return $requests;
  }

  /**
   * Helper function to check using text format.
   *
   * @param string $format_name
   *   Format name.
   *
   * @return bool
   *   True if user can use this text format.
   */
  protected function checkFormat($format_name = '') {
    if ($format_name) {
      $known_formats = filter_formats();
      if (array_key_exists((string) $format_name, $known_formats)) {
        return filter_access($known_formats[$format_name]);
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Retrieve comments for a thread.
   *
   * @return mixed|array
   *   Comments.
   */
  protected function getCids() {
    if (!property_exists($this->node, 'type')) {
      throw new CommentsRetrieve("Node object does not defined");
    }
    $mode = variable_get('comment_default_mode_' . $this->node->type, 1);
    $comments_per_page = variable_get('comment_default_per_page_' . $this->node->type, 50);
    return comment_get_thread($this->node, $mode, $comments_per_page);
  }

  /**
   * Get timezone for comment.
   *
   * @param int $cid
   *   Comment id.
   *
   * @return mixed
   *   Time zone or false.
   */
  public static function getCommentTimeZone(int $cid) {
    $time_zone = db_select('field_data_field_comment_user_time_zone', 'time_zone')
      ->fields('time_zone', ['field_comment_user_time_zone_value'])
      ->condition('entity_id', $cid)
      ->condition('entity_type', 'comment')
      ->execute()
      ->fetchField();

    return $time_zone;
  }

}

/**
 * Class CommentsException.
 *
 * @package Drupal\move_services_new\Services
 */
class CommentsException extends \RuntimeException {

  /**
   * Save message to db.
   *
   * @param int $type
   *   A watchdog severity of the message.
   */
  public function saveDataBase($type) {
    $message = "{$this->getMessage()} in {$this->getFile()}: {$this->getLine()} </br> Trace: {$this->getTraceAsString()}";
    watchdog('Comments', $message, array(), $type);
  }

}

/**
 * Class CommentsRetrieve.
 *
 * @package Drupal\move_services_new\Services
 */
class CommentsRetrieve extends CommentsException {}

/**
 * Class CheckUserPermToRequest.
 *
 * @package Drupal\move_services_new\Services
 */
class CheckUserPermToRequest extends CommentsException {}
