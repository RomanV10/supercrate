<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_branch\Services\Branch;
use Drupal\move_parking\Services\Parking;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class Parklot.
 *
 * @package Drupal\move_services_new\Services
 */
class Parklot {

  /**
   * Move request object.
   *
   * @var \Drupal\move_services_new\Services\move_request\MoveRequest
   */
  private $moveRequest;

  /**
   * Table name.
   *
   * @var string
   */
  const REQUEST_CACHE_TABLE = 'move_services_cache';

  /**
   * Needed fields.
   *
   * @var array
   */
  private $neededFields = [
    'nid',
    'trucks',
    'delivery_trucks',
    'service_type',
    'delivery_date_from',
    'delivery_date_to',
    'field_double_travel_time',
    'travel_time',
    'status',
    'crew',
    'move_size',
    'minimum_time',
    'maximum_time',
    'start_time1',
    'start_time2',
    'zip_from',
    'zip_to',
    'field_reservation_received',
    'from',
    'to',
    'date',
    'name',
    'field_company_flags',
    'ddate',
    'delivery_start_time',
    'phone',
    'email',
    'link',
    'rate',
    'type_from',
    'type_to',
    'distance',
    'adrfrom',
    'request_all_data',
    'field_delivery_crew_size',
    'request_data',
    'field_foreman_assign_time',
    'field_helper',
    'field_foreman',
    'field_flat_rate_local_move',
    'apt_from',
    'apt_to',
    'field_moving_from',
    'field_moving_to',
    'inventory',
    'field_useweighttype',
    'inventory_weight',
    'custom_weight',
    'commercial_extra_rooms',
    'field_custom_commercial_item',
    'rooms',
  ];

  /**
   * Parklot constructor.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   Move request.
   */
  public function __construct(MoveRequest $move_request) {
    $this->moveRequest = $move_request;
  }

  /**
   * Get nodes by days for parklot.
   *
   * @param int $date
   *   Date.
   * @param array $condition
   *   Conditions.
   *
   * @return array
   *   Parklot.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function get(int $date, array $condition = array()) {
    $datetime = new \DateTime();
    $datetime->setTimezone(new \DateTimeZone('GMT'));
    $datetime->setTimestamp($date);
    $date_from = $datetime->format('Y-m-d') . ' 00:00:00';
    $date_to = $datetime->format('Y-m-d') . ' 23:59:59';

    return $this->getByDate($date_from, $date_to, $condition);
  }

  /**
   * Get month parklot.
   *
   * @param int $date
   *   Date.
   * @param array $condition
   *   Conditions.
   *
   * @return array
   *   Parklot.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public function getByMonth(int $date, array $condition) {
    $datetime = new \DateTime();
    $datetime->setTimezone(new \DateTimeZone('GMT'));
    $datetime->setTimestamp($date);

    $first_day_of_month = clone $datetime->modify('first day of this month 00:00:00');
    $last_day_of_month = clone $datetime->modify('last day of this month 23:59:59');

    $date_from = $first_day_of_month->format('Y-m-d') . ' 00:00:00';
    $date_to = $last_day_of_month->format('Y-m-d') . ' 23:59:59';

    return $this->getByDate($date_from, $date_to, $condition);
  }

  /**
   * Get requests and trips by dates.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param array $condition
   *   Conditions.
   *
   * @return array
   *   Request.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  private function getByDate(string $date_from, string $date_to, array $condition): array {
    $result = $this->getRequests($date_from, $date_to, $condition);

    $trips = Parking::getTripParkingForGeneralParklotByDate($date_from, $date_to);
    if (!empty($trips)) {
      $result = array_merge($result, $trips);
    }

    return array_values($result);
  }

  /**
   * Get Request by date.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param array $condition
   *   Conditions.
   *
   * @return array
   *   Nodes.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  private function getRequests(string $date_from, string $date_to, array $condition) {
    $nids = static::getNidsByDate($date_from, $date_to, $condition);
    $nodes = [];
    if ($nids) {
      $nodes = $this->getCachedRequests($nids);
      $nids = array_flip($nids);
      if (count($nids) > count($nodes)) {
        $nids = array_keys(array_diff_key($nids, $nodes));
        $nodes = array_merge($nodes, $this->getNotCachedRequests($nids));
      }
    }

    return $nodes;
  }

  /**
   * Get nids by date.
   *
   * @param string $date_from
   *   Date from.
   * @param string $date_to
   *   Date to.
   * @param array $condition
   *   Conditions.
   *
   * @return array
   *   Nids.
   */
  public static function getNidsByDate(string $date_from, string $date_to, array $condition) : array {
    $flatrate_query = db_select('node', 'n');
    $flatrate_query->leftJoin('field_data_field_delivery_date_from', 'delivery_from', 'n.nid = delivery_from.entity_id');
    $flatrate_query->leftJoin('field_data_field_delivery_date_to', 'delivery_to', 'n.nid = delivery_to.entity_id');
    $flatrate_query->leftJoin('field_data_field_move_service_type', 'service_type', 'n.nid = service_type.entity_id');
    $flatrate_query->fields('n', array('nid'));
    $db_or = db_or();
    $db_and = db_and();
    $db_and2 = db_and();
    $db_and->condition('delivery_from.field_delivery_date_from_value', $date_from, '>=');
    $db_and->condition('delivery_to.field_delivery_date_to_value', $date_to, '<=');

    $db_and2->condition('delivery_from.field_delivery_date_from_value', $date_from, '<=');
    $db_and2->condition('delivery_to.field_delivery_date_to_value', $date_from, '>=');

    $db_or->condition($db_and);
    $db_or->condition($db_and2);

    $flatrate_query->condition($db_or);
    $flatrate_query->condition('service_type.field_move_service_type_value', RequestServiceType::FLAT_RATE);
    if (!empty($condition)) {
      foreach ($condition as $field_name => $val) {
        $db_field_name = 'field_data_' . $field_name;
        $flatrate_query->leftJoin($db_field_name, $field_name, 'n.nid = ' . $field_name . '.entity_id');

        $db_field_value_name = $field_name . '_value';
        $operator = '';
        if (is_array($val)) {
          $operator = 'IN';
        }
        $flatrate_query->condition($db_field_value_name, $val, $operator);
      }
    }
    $flatrate_nids = $flatrate_query->execute()->fetchCol();

    $request_query = db_select('node', 'n');
    $request_query->leftJoin('field_data_field_date', 'date', 'n.nid = date.entity_id');
    $request_query->fields('n', array('nid'));

    $request_query->condition('date.field_date_value', array($date_from, $date_to), 'BETWEEN');
    if (!empty($condition)) {
      foreach ($condition as $field_name => $val) {
        $db_field_name = 'field_data_' . $field_name;
        $request_query->leftJoin($db_field_name, $field_name, 'n.nid = ' . $field_name . '.entity_id');
        $db_field_value_name = $field_name . '_value';
        $operator = '';
        if (is_array($val)) {
          $operator = 'IN';
        }
        $request_query->condition($db_field_value_name, $val, $operator);
      }
    }
    $request_nids = $request_query->execute()->fetchCol();

    return array_merge($flatrate_nids, $request_nids);
  }

  /**
   * Get cached request by nids.
   *
   * @param array $nids
   *   Requests nids.
   *
   * @return array
   *   Requests array.
   */
  private function getCachedRequests(array $nids) {
    $result = db_select(self::REQUEST_CACHE_TABLE, 'cache')
      ->fields('cache', ['data', 'nid'])
      ->condition('nid', $nids, 'IN')
      ->execute();

    $nodes = [];
    foreach ($result as $item) {
      $nodes[$item->nid] = $this->filterRequestFields(unserialize($item->data));
    }

    return $nodes;
  }

  /**
   * Get not cached requests by nids.
   *
   * @param array $nids
   *   Requests nids.
   *
   * @return array
   *   Requests array.
   *
   * @throws \Exception
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  private function getNotCachedRequests(array $nids) {
    $node = [];
    foreach ($nids as $nid) {
      $node[] = $this->filterRequestFields($this->moveRequest->getNode($nid));
    }

    return $node;
  }

  /**
   * Filter requests fields.
   *
   * @param array $request
   *   Request with fields.
   *
   * @return array
   *   Filtered request fields.
   */
  private function filterRequestFields(array $request) : array {
    return array_intersect_key($request, array_flip($this->neededFields));
  }

  /**
   * Save dispatch notes to db.
   *
   * @param string $date
   *   Human readable date.
   * @param string $note
   *   Text with notes.
   *
   * @return int
   *   Result of operations.
   *
   * @throws \InvalidMergeQueryException
   */
  public static function setNote(string $date, string $note) {
    $merge = db_merge('move_dispatch_notes')
      ->insertFields(array(
        'date' => $date,
        'note' => $note,
      ))
      ->updateFields(array(
        'note' => $note,
      ))
      ->key(array('date' => $date))
      ->execute();

    // If enabled Branches when send note to each branch on $date
    (new Branch())->updateDispatchNotes($date, $note);
    return $merge;
  }

  public static function getNote($date) {
    return db_select('move_dispatch_notes', 'mdn')
      ->fields('mdn', array('note'))
      ->condition('mdn.date', $date)
      ->execute()
      ->fetchObject();
  }

}
