<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TripType.
 *
 * @package Drupal\move_services_new\Util
 */
class TripType extends BaseEnum {

  const CARRIER = 1;
  const PERSONALLY = 2;

}
