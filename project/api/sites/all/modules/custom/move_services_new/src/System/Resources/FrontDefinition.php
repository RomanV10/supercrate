<?php

namespace Drupal\move_services_new\System\Resources;

use Drupal\move_services_new\Services\Front;

/**
 * Class frontDefinition.
 *
 * @package Drupal\move_services_new\System
 */
class FrontDefinition {

  public static function getDefinition() {
    return array(
      'front' => array(
        'operations' => array(),
        'actions' => array(
          'frontpage' => array(
            'help' => 'Retrieve a request info',
            'callback' => 'Drupal\move_services_new\System\Resources\FrontDefinition::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/FrontDefinition',
            ),
            'access arguments' => array('access content'),
          ),
          'get_geo_names' => array(
            'help' => 'Get geo names',
            'callback' => 'Drupal\move_services_new\System\Resources\FrontDefinition::getGeoNames',
            'file' => array(
              'type' => 'php',
              'module' => 'move_services_new',
              'name' => 'src/System/Resources/FrontDefinition',
            ),
            'args' => array(
              array(
                'name' => 'params',
                'optional' => FALSE,
                'source' => array('data' => 'params'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function retrieve() {
    $front = new Front();
    return $front->frontpage();
  }

  /**
   * Get geo names.
   *
   * @param string $params
   *   Params.
   *
   * @return object|\stdClass
   *   Json.
   */
  public static function getGeoNames(string $params) {
    $front = new Front();
    return $front->getGeoNames($params);
  }
}
