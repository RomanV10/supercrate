<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class RequestSizeOfMove.
 *
 * @package Drupal\move_services_new\Util
 */
class RequestSizeOfMove extends BaseEnum {
  const ROOM_OR_LESS = 1;
  const STUDIO = 2;
  const SMALL_1_BEDROOM_CONDO_APRT = 3;
  const LARGE_1_BEDROOM_CONDO_APRT = 4;
  const SMALL_2_BEDROOM_CONDO_APRT = 5;
  const LARGE_2_BEDROOM_CONDO_APRT = 6;
  const BEDROOM_3_CONDO_APRT = 7;
  const BEDROOM_2_HOUSE_TOWNHOUSE = 8;
  const BEDROOM_3_HOUSE_TOWNHOUSE = 9;
  const BEDROOM_4_HOUSE_TOWNHOUSE = 10;
  const COMMERCIAL_MOVE = 11;
}
