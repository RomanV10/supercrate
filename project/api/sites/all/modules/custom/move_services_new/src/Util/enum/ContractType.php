<?php

namespace Drupal\move_services_new\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;


/**
 * Class AmountOption.
 *
 * @package Drupal\move_services_new\Util
 */
class ContractType extends BaseEnum {

  const PICKUP_PAYROLL = 0;
  const DELIVERY_PAYROLL = 1;
  const PAYROLL = 2;
  const LONGDISTANCE = 3;

}
