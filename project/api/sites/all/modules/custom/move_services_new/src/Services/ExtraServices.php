<?php

namespace Drupal\move_services_new\Services;

use Drupal\move_calculator\Services\MoveRequestExtraService;
use Drupal\move_inventory\Actions\SubActions\InventorySubActions;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\move_request\MoveRequestRetrieve;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class ExtraServices.
 *
 * @package Drupal\move_services_new\Services
 */
class ExtraServices {

  private $extraServicesCalculate;
  public $requestRetrieve;
  public $user;

  /**
   * ExtraServices constructor.
   *
   * @param \stdClass $user
   *   User object.
   * @param \EntityMetadataWrapper $request_wrapper
   *   Request wrapper.
   * @param array $request_all_data
   *   Request all data.
   *
   * @throws \Exception
   */
  public function __construct(\stdClass $user, \EntityMetadataWrapper $request_wrapper, array $request_all_data = []) {
    $this->user = $user;
    $this->getFieldsForCalculation($request_wrapper, $request_all_data);


    $this->extraServicesCalculate = (new MoveRequestExtraService());
    $this->extraServicesCalculate->setRequestRetrieve($this->requestRetrieve)->build();
  }

  /**
   * This is fields what we need for extra services calculation.
   *
   * THe structure is like from request retrieve. But only needed fields.
   *
   * @param \EntityMetadataWrapper $request_wrapper
   *   Request Wrapper.
   * @param array $request_all_data
   *   Request all data.
   */
  private function getFieldsForCalculation(\EntityMetadataWrapper $request_wrapper, array $request_all_data) {
    $this->requestRetrieve['nid'] = $request_wrapper->nid->value();
    $this->requestRetrieve['type_from']['raw'] = $request_wrapper->field_type_of_entrance_from->value();
    $this->requestRetrieve['type_to']['raw'] = $request_wrapper->field_type_of_entrance_to_->value();
    $this->requestRetrieve['field_moving_from'] = $request_wrapper->field_moving_from->value();
    $this->requestRetrieve['field_moving_to'] = $request_wrapper->field_moving_to->value();
    $this->requestRetrieve['service_type']['raw'] = $request_wrapper->field_move_service_type->value();
    $this->requestRetrieve["field_useweighttype"]['value'] = $request_wrapper->field_useweighttype->value();
    $this->requestRetrieve["field_cubic_feet"]["value"] = $request_wrapper->field_useweighttype->value();
    $this->requestRetrieve['rooms']['value'] = $this->requestRetrieve['rooms']['raw'] = $request_wrapper->field_extra_furnished_rooms->value();
    $this->requestRetrieve['custom_weight']['value'] = $request_wrapper->field_customweight->value();
    $this->requestRetrieve['commercial_extra_rooms']['value'] = $request_wrapper->field_commercial_extra_rooms->value();
    $this->requestRetrieve['move_size']['raw'] = $request_wrapper->field_size_of_move->value();

    $this->requestRetrieve['request_all_data'] = $request_all_data;
  }

  /**
   * @TODO refactor this after Alex permission imp.
   *
   * Set all extra service on request create.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $moveRequest
   *   Move Request.
   *
   * @throws \Exception
   */
  public function setExtraServicesOnRequestCreate(MoveRequest $moveRequest) {
    $extra_services = [];

    if ($this->requestRetrieve['service_type']['raw'] == RequestServiceType::LONG_DISTANCE) {
      $extra_services = $this->extraServicesCalculate->longDistanceCalculation();
    }

    $equipment_fee = $this->getEquipmentFee($moveRequest);
    if ($equipment_fee) {
      $extra_services[] = $equipment_fee;
    }

    if (!empty($extra_services)) {
      self::setExtraServices($this->requestRetrieve['nid'], $extra_services, $this->user, FALSE);
    }
  }

  /**
   * Set payment's data for request.
   *
   * @param int $nid
   *   Nid.
   * @param array $data
   *   Data to save in database.
   * @param mixed $user
   *   Bool.
   * @param bool $update_cache
   *   Update cache or not.
   *
   * @return int
   *   Result of db_merge operation.
   *
   * @throws \Exception
   */
  public static function setExtraServices(int $nid, array $data = [], $user = FALSE, bool $update_cache = TRUE) {
    $result = 0;

    if ($nid) {
      if (empty($user) || empty($user->uid)) {
        global $user;
      }

      if (UserPermissions::isUserCanUpdateExtraService($user->uid, $nid)) {

        $result = db_merge('move_extra_services')
          ->key(['nid' => $nid])
          ->fields([
            'nid' => $nid,
            'value' => serialize($data),
          ])
          ->execute();

        self::addExtraServiceForStorageRequest($nid, $data);

        if ($update_cache) {
          Cache::updateCacheData($nid);
        }
      }
    }

    return $result;
  }

  /**
   * Add extra services.
   *
   * @param int $nid
   *   Request id.
   * @param array $data
   *   Data with services.
   *
   * @throws \Exception
   */
  private static function addExtraServiceForStorageRequest(int $nid, array $data) {
    $storageRelation = (new InventorySubActions())->getEntityIdStoragePacking($nid);

    if (!empty($storageRelation['to']) || !empty($storageRelation['from'])) {
      $storage_nid = $storageRelation['to'] == $nid ? $storageRelation['from'] : $storageRelation['to'];
      $writeForStorage = self::copyExtraServiceItemInStorageRequest($storage_nid, $data);

      db_merge('move_extra_services')
        ->key(['nid' => $writeForStorage['nid']])
        ->fields($writeForStorage)
        ->execute();
    }
  }

  /**
   * Copy extra service items for storage request.
   *
   * @param int $nid
   *   Node id.
   * @param array $data
   *   Service data.
   *
   * @return array
   *   Data for write to db.
   */
  public static function copyExtraServiceItemInStorageRequest(int $nid, array $data) {
    $currentExtraService = self::getExtraServices($nid);
    // Delete old value items extra service.
    foreach ($currentExtraService as $key => $currService) {
      if (isset($currService['inventory_item_id'])) {
        unset($currentExtraService[$key]);
      }
    }

    $currentExtraService = array_values($currentExtraService);
    // Copy items extra service.
    foreach ($data as $extraService) {
      if (isset($extraService['inventory_item_id'])) {
        $currentExtraService[] = $extraService;
      }
    }

    return [
      'nid' => $nid,
      'value' => serialize($currentExtraService),
    ];
  }

  /**
   * Get payment's data from request.
   *
   * @param int $nid
   *   Request id.
   *
   * @return array
   *   Unserialized array's with data.
   */
  public static function getExtraServices(int $nid = 0) {
    $result = [];

    if (!empty($nid)) {
      $select = db_select('move_extra_services', 'mp')
        ->fields('mp', ['value'])
        ->condition('mp.nid', $nid)
        ->execute()->fetchField();
      if ($select) {
        $result = unserialize($select);
      }
    }

    return $result;
  }

  /**
   * Calculate equipment fee.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $moveRequest
   *   MoveRequest.
   *
   * @return array
   *   Array with fee services.
   */
  public function getEquipmentFee(MoveRequest $moveRequest) {
    $result = [];

    $equipment_fee = $this->extraServicesCalculate->calculateEquipmentFee($moveRequest->draft);

    if ($equipment_fee) {
      $is_set_equipment_fee_name = isset($this->extraServicesCalculate->basicSettings['equipment_fee']['name']) && $this->extraServicesCalculate->basicSettings['equipment_fee']['name'];
      $equipment_fee_name = $is_set_equipment_fee_name ? $this->extraServicesCalculate->basicSettings['equipment_fee']['name'] : 'Equipment Fee';
      $result = ExtraServices::initBasicService($equipment_fee_name, $equipment_fee, TRUE);
    }

    return $result;
  }

  /**
   * Init basic service.
   *
   * @param string $name
   *   Name basic service.
   * @param mixed $value
   *   Value.
   * @param bool $feeAllServices
   *   Flag.
   *
   * @return array
   *   Basic service.
   */
  public static function initBasicService(string $name, $value, $feeAllServices = FALSE) {
    $basic_service = [];
    $basic_service['name'] = $name;
    $basic_service['autoService'] = TRUE;
    $basic_service['fee_all_services'] = $feeAllServices;

    $extra_service = [];
    $extra_service['services_default_value'] = $value;
    $extra_service['services_name'] = "Cost";
    $extra_service['services_read_only'] = FALSE;
    $extra_service['services_type'] = "Amount";

    $basic_service['extra_services'] = [$extra_service];

    return $basic_service;
  }

}
