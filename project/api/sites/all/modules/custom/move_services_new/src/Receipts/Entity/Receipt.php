<?php

namespace Drupal\move_services_new\Receipts\Entity;

/**
 * Class Receipt.
 *
 * @package Drupal\move_services_new\Receipts\Entity
 */
class Receipt {

  /**
   * Unique entity id: move request, storage request etc.
   *
   * @var int
   */
  public $entity_id;

  /**
   * Entity type: move request, storage request etc.
   *
   * @var int
   */
  public $entity_type = 0;

  /**
   * Amount of payment.
   *
   * @var float
   */
  public $amount = 0.0;

  /**
   * Type credit card: VISA, MC etc.
   *
   * @var int|null
   */

  public $card_type = NULL;

  /**
   * Last 4 digit of credit card.
   *
   * @var string
   */
  public $card_number = '';

  /**
   * Customer first and last name.
   *
   * @var string
   */
  public $customer_name = '';

  /**
   * Customer phone.
   *
   * @var string
   */
  public $phone = '';

  /**
   * Auth code from authorize.
   *
   * @var string
   */
  public $auth_code = '';

  /**
   * The Unix timestamp when the payment was created.
   *
   * @var int
   */
  public $created = 0;

  /**
   * For credit card payment refund.
   *
   * @var string
   */
  public $checkN = '';

  /**
   * Payment description.
   *
   * @var string
   */
  public $description = '';

  /**
   * Customer email.
   *
   * @var string
   */
  public $email = '';

  /**
   * Invoice id.
   *
   * @var int|null
   */
  public $invoice_id = NULL;

  /**
   * Array SIT jobs.
   *
   * @var string
   */
  public $jobs = '';

  /**
   * Payment method: credit card, cash, check.
   *
   * @var int
   */
  public $payment_method;

  /**
   * Custom payment method name.
   *
   * @var string
   */
  public $custom_payment_method = '';

  /**
   * Flag, if money not received - true.
   *
   * @var int
   */
  public $pending = 0;

  /**
   * Id const: Direction of payment.
   *
   * @var int
   */
  public $payment_flag = 1;

  /**
   * Flag, true if refund.
   *
   * @var int
   */
  public $refunds = 0;

  /**
   * Flag, for SIT jobs.
   *
   * @var int
   */
  public $refund_type = 0;

  /**
   * Id transaction from authorize.net.
   *
   * @var string
   */
  public $transaction_id = '';

  /**
   * Hash transaction from authorize.net.
   *
   * @var string
   */
  public $trans_hash = '';

  /**
   * Flag, for SIT jobs to search.
   *
   * @var int
   */
  public $transaction_type = 0;

  /**
   * Amount for SIT jobs.
   *
   * @var float
   */
  public $total_amount = 0.0;

  /**
   * Id SIT trip.
   *
   * @var int
   */
  public $trip_id = NULL;

  /**
   * Zip code.
   *
   * @var string
   */
  public $zip_code = '';

  /**
   * User role.
   *
   * @var int
   */
  public $role = NULL;

  /**
   * User id.
   *
   * @var int
   */
  public $uid = NULL;

  /**
   * Get entity id.
   *
   * @return int|null
   *   EntityId.
   */
  public function getEntityId(): ?int {
    return $this->entity_id;
  }

  /**
   * Set entity id.
   *
   * @param int|null $entity_id
   *   Entity id.
   */
  public function setEntityId(?int $entity_id): void {
    $this->entity_id = $entity_id;
  }

  /**
   * Get entity type.
   *
   * @return int
   *   Entity type.
   */
  public function getEntityType(): int {
    return $this->entity_type;
  }

  /**
   * Set Entity type.
   *
   * @param int $entity_type
   *   Entity type.
   */
  public function setEntityType(int $entity_type): void {
    $this->entity_type = $entity_type;
  }

  /**
   * Get amount.
   *
   * @return float
   *   Amount.
   */
  public function getAmount(): float {
    return $this->amount;
  }

  /**
   * Set amount.
   *
   * @param float $amount
   *   Amount.
   */
  public function setAmount(float $amount): void {
    $this->amount = $amount;
  }

  /**
   * Get card type.
   *
   * @return int|null
   *   card type.
   */
  public function getCardType(): ?int {
    return $this->card_type;
  }

  /**
   * Set card type.
   *
   * @param int $card_type
   *   Card type.
   */
  public function setCardType(int $card_type): void {
    $this->card_type = $card_type;
  }

  /**
   * Get card number.
   *
   * @return string
   *   card number.
   */
  public function getCardNumber(): string {
    return $this->card_number;
  }

  /**
   * Set card number.
   *
   * @param string $card_number
   *   Card number.
   */
  public function setCardNumber(string $card_number): void {
    $this->card_number = $card_number;
  }

  /**
   * Get customer name.
   *
   * @return string
   *   customer name.
   */
  public function getCustomerName(): string {
    return $this->customer_name;
  }

  /**
   * Set customer name.
   *
   * @param string $customer_name
   *   Customer name.
   */
  public function setCustomerName(string $customer_name): void {
    $this->customer_name = $customer_name;
  }

  /**
   * Get phone.
   *
   * @return string
   *   phone.
   */
  public function getPhone(): string {
    return $this->phone;
  }

  /**
   * Set phone.
   *
   * @param string $phone
   *   Phone.
   */
  public function setPhone(string $phone): void {
    $this->phone = $phone;
  }

  /**
   * Get authorize code.
   *
   * @return string
   *   authorize code.
   */
  public function getAuthCode(): string {
    return $this->auth_code;
  }

  /**
   * Set authorize code.
   *
   * @param string $auth_code
   *   Authorize code.
   */
  public function setAuthCode(string $auth_code): void {
    $this->auth_code = $auth_code;
  }

  /**
   * Get created date.
   *
   * @return int
   *   created date.
   */
  public function getCreated(): int {
    return $this->created;
  }

  /**
   * Set created date.
   *
   * @param int $created
   *   Created date.
   */
  public function setCreated(int $created): void {
    $this->created = $created;
  }

  /**
   * Get checkN.
   *
   * @return string
   *   checkN.
   */
  public function getCheckN(): string {
    return $this->checkN;
  }

  /**
   * Set checkN.
   *
   * @param string $checkN
   *   checkN.
   */
  public function setCheckN(string $checkN): void {
    $this->checkN = $checkN;
  }

  /**
   * Get description.
   *
   * @return string
   *   description.
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * Set description.
   *
   * @param string $description
   *   Description.
   */
  public function setDescription(string $description): void {
    $this->description = $description;
  }

  /**
   * Get email.
   *
   * @return string
   *   email.
   */
  public function getEmail(): string {
    return $this->email;
  }

  /**
   * Set email.
   *
   * @param string $email
   *   Email.
   */
  public function setEmail(string $email): void {
    $this->email = strtolower($email);
  }

  /**
   * Get invoice id.
   *
   * @return int|null
   *   invoice id.
   */
  public function getInvoiceId(): ?int {
    return $this->invoice_id;
  }

  /**
   * Set invoice id.
   *
   * @param null|int $invoice_id
   *   Invoice id.
   */
  public function setInvoiceId(?int $invoice_id): void {
    $this->invoice_id = $invoice_id;
  }

  /**
   * Get jobs.
   *
   * @return string
   *   jobs.
   */
  public function getJobs(): string {
    return $this->jobs;
  }

  /**
   * Set jobs.
   *
   * @param string $jobs
   *   Jobs.
   */
  public function setJobs(string $jobs): void {
    $this->jobs = $jobs;
  }

  /**
   * Get payment method.
   *
   * @return int|null
   *   payment method.
   */
  public function getPaymentMethod(): ?int {
    return $this->payment_method;
  }

  /**
   * Set payment method.
   *
   * @param int|null $payment_method
   *   Payment method.
   */
  public function setPaymentMethod(?int $payment_method): void {
    $this->payment_method = $payment_method;
  }

  /**
   * Get pending.
   *
   * @return int
   *   pending.
   */
  public function getPending(): int {
    return $this->pending;
  }

  /**
   * Set pending.
   *
   * @param int $pending
   *   Pending.
   */
  public function setPending(int $pending): void {
    $this->pending = $pending;
  }

  /**
   * Get payment flag.
   *
   * @return int
   *   payment flag.
   */
  public function getPaymentFlag(): int {
    return $this->payment_flag;
  }

  /**
   * Set payment flag.
   *
   * @param string $payment_flag
   *   Payment flag.
   */
  public function setPaymentFlag(string $payment_flag): void {
    $this->payment_flag = $payment_flag;
  }

  /**
   * Get refunds.
   *
   * @return int
   *   refunds.
   */
  public function getRefunds(): int {
    return $this->refunds;
  }

  /**
   * Set refunds.
   *
   * @param int|null $refunds
   *   Refunds.
   */
  public function setRefunds(?int $refunds): void {
    $this->refunds = $refunds;
  }

  /**
   * Get refund type.
   *
   * @return int|null
   *   refund type.
   */
  public function getRefundType(): ?int {
    return $this->refund_type;
  }

  /**
   * Set refund type.
   *
   * @param int|null $refund_type
   *   Refund type.
   */
  public function setRefundType(?int $refund_type): void {
    $this->refund_type = $refund_type;
  }

  /**
   * Get transaction id.
   *
   * @return string
   *   transaction id.
   */
  public function getTransactionId(): string {
    return $this->transaction_id;
  }

  /**
   * Set transaction id.
   *
   * @param string $transaction_id
   *   Transaction id.
   */
  public function setTransactionId(string $transaction_id): void {
    $this->transaction_id = $transaction_id;
  }

  /**
   * Get transaction hash.
   *
   * @return string
   *   transaction hash.
   */
  public function getTransHash(): string {
    return $this->trans_hash;
  }

  /**
   * Set transaction hash.
   *
   * @param string $trans_hash
   *   Transaction hash.
   */
  public function setTransHash(string $trans_hash): void {
    $this->trans_hash = $trans_hash;
  }

  /**
   * Get transaction type.
   *
   * @return string
   *   transaction type.
   */
  public function getTransactionType(): string {
    return $this->transaction_type;
  }

  /**
   * Set transaction type.
   *
   * @param string $transaction_type
   *   Transaction type.
   */
  public function setTransactionType(string $transaction_type): void {
    $this->transaction_type = $transaction_type;
  }

  /**
   * Get total amount.
   *
   * @return float|null
   *   total amount.
   */
  public function getTotalAmount(): ?float {
    return $this->total_amount;
  }

  /**
   * Set total amount.
   *
   * @param float|null $total_amount
   *   Total amount.
   */
  public function setTotalAmount(?float $total_amount): void {
    $this->total_amount = $total_amount;
  }

  /**
   * Get trip id.
   *
   * @return int|null
   *   trip id.
   */
  public function getTripId(): ?int {
    return $this->trip_id;
  }

  /**
   * Set trip id.
   *
   * @param int|null $trip_id
   *   Trip id.
   */
  public function setTripId(?int $trip_id): void {
    $this->trip_id = $trip_id;
  }

  /**
   * Get zip code.
   *
   * @return string
   *   zip code.
   */
  public function getZipCode(): string {
    return $this->zip_code;
  }

  /**
   * Set zip code.
   *
   * @param string $zip_code
   *   Zip code.
   */
  public function setZipCode(string $zip_code): void {
    $this->zip_code = $zip_code;
  }

  /**
   * Get role.
   *
   * @return int|null
   *   role.
   */
  public function getRole(): ?int {
    return $this->role;
  }

  /**
   * Set role.
   *
   * @param int|null $role
   *   Role.
   */
  public function setRole(?int $role): void {
    $this->role = $role;
  }

  /**
   * Get uid.
   *
   * @return int|null
   *   uid.
   */
  public function getUid(): ?int {
    return $this->uid;
  }

  /**
   * Set uid.
   *
   * @param int|null $uid
   *   Uid.
   */
  public function setUid(?int $uid): void {
    $this->uid = $uid;
  }

  /**
   * Get name for custom payment method.
   *
   * @return string
   *   Custom payment method name.
   */
  public function getCustomPaymentMethod(): string {
    return $this->custom_payment_method;
  }

  /**
   * Set name for custom payment method.
   *
   * @param string $custom_payment_method
   *   Custom payment method name.
   */
  public function setCustomPaymentMethod(string $custom_payment_method): void {
    $this->custom_payment_method = $custom_payment_method;
  }

}
