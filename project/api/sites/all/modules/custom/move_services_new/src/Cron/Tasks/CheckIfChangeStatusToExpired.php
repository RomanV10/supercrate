<?php

namespace Drupal\move_services_new\Cron\Tasks;

use Drupal\move_services_new\Services\Cache;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class CheckIfChangeStatusToExpired.
 *
 * @package Drupal\move_services_new\Cron\Tasks
 */
class CheckIfChangeStatusToExpired implements CronInterface {

  /**
   * Execute task by cron.
   *
   * @throws \EntityMetadataWrapperException
   */
  public static function execute(): void {
    $move_date = date("Y-m-d H:i:s");
    $query = db_select('node', 'n');
    $query->join('field_data_field_date', 'ddf', 'n.nid = ddf.entity_id AND ddf.field_date_value < :fv', array(':fv' => $move_date));
    $query->join('field_data_field_approve', 'apr', 'n.nid = apr.entity_id');
    $query->fields('n', array('nid'));
    $query->condition('apr.field_approve_value', [
      RequestStatusTypes::PENDING,
      RequestStatusTypes::NOT_CONFIRMED,
      RequestStatusTypes::INHOME_ESTIMATE,
      RequestStatusTypes::DATE_PENDING,
      RequestStatusTypes::FLATE_RATE_INVENTORY_NEEDED,
      RequestStatusTypes::FLATE_RATE_PROVIDE_OPTIONS,
      RequestStatusTypes::FLATE_RATE_WAIT_OPTOINS,
      RequestStatusTypes::PRE_PENDING,
    ], 'IN');
    $result = $query->execute()->fetchCol();
    /* @var \EntityDrupalWrapper $wrapper */ /* @var \EntityDrupalWrapper $node_wrapper */
    foreach ($result as $i => $nid) {
      $wrapper = entity_metadata_wrapper('node', $nid);
      $wrapper->field_approve->set(RequestStatusTypes::EXPIRED);
      $wrapper->save();
      Cache::updateCacheData($nid);
    }
  }

}
