<?php

use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Services\Cache;
use Drupal\move_parking\Services\Parking;

function move_services_new_payroll_cache($form, &$form_state) {
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Start'),
  );

  return $form;
}

function move_services_new_payroll_cache_submit($form, &$form_state) {
  $payroll_instance = new Payroll();
  $operations = $payroll_instance->cachePayrollPrepareBatch();

  db_truncate('move_payroll_day_cache')->execute();

  $batch = array(
    'operations' => $operations,
    'finished' => 'move_services_new_payroll_batch_finished',
    'file' => drupal_get_path('module', 'move_services_new') . '/move_services_new.admin.inc',
  );

  batch_set($batch);
}

function move_service_new_payroll_batch_execute($date, &$context) {
  $payroll_instance = new Payroll();
  $payroll_instance->cacheCreatePayrollAll($date);
  $context['results']['titles'][] = $date;
  $context['message'] = 'Обновлён пейролл за <em>' . check_plain($date) . '</em>';
}

function move_services_new_payroll_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Cashed ' . count($results['titles']) . ' days.');
  }
  else {
    drupal_set_message('Ended with errors', 'error');
  }
}

function move_services_basic_settings_backup_form() {
  $form = array();

  $form['submit_1'] = array(
    '#type' => 'submit',
    '#value' => t('Create backup'),
    '#submit' => array('createBackupBasicSettings'),
  );

  $form['submit_2'] = array(
    '#type' => 'submit',
    '#value' => t('Restore basic settings backup'),
    '#submit' => array('returnBasicSettingsFromBackup'),
  );

  $form['submit_3'] = array(
    '#type' => 'submit',
    '#value' => t('Print backup settings'),
    '#submit' => array('getBackupBasicSettings'),
  );

  $form['submit_4'] = array(
    '#type' => 'submit',
    '#value' => t('Last 10 settings'),
    '#submit' => array('getSystemSettingsLogs'),
  );

  $form['submit_5'] = array(
    '#type' => 'submit',
    '#value' => t('Delete settings'),
    '#submit' => array('deleteSystemSettingsLogs'),
  );

  $form['submit_6'] = array(
    '#type' => 'submit',
    '#value' => t('Rewrite longDistance settings'),
    '#submit' => array('reWriteLongDistanceVariable'),
  );

  return $form;
}

function createBackupBasicSettings() {
  $basic_settings = json_decode(variable_get('basicsettings'));
  $encode_settings = json_encode($basic_settings);
  variable_set('basicsettings_backup', $encode_settings);
}

function returnBasicSettingsFromBackup() {
  $basic_settings_backup = json_decode(variable_get('basicsettings_backup'));
  $encode_settings = json_encode($basic_settings_backup);
  variable_set('basicsettings', $encode_settings);
}

function getBackupBasicSettings() {
  $basic_settings_backup = json_decode(variable_get('basicsettings_backup'));
  return dpm($basic_settings_backup);
}

function getSystemSettingsLogs() {
  $result = array();
  $system_logs = db_select('system_logs', 'sl')
    ->fields('sl')
    ->orderBy('id', 'DESC')
    ->range(0, 10)
    ->execute()
    ->fetchAll();

  if (!empty($system_logs)) {
    foreach ($system_logs as $key => $system_log) {
      $result[$key] = $system_log;
      $result[$key]->value = json_decode($system_log->value);
    }
  }

  return dpm($result);
}

/**
 * Delete old logs.
 *
 * @param int $range_start
 * @param int $range_length
 */
function deleteSystemSettingsLogs(int $range_start = 0, int $range_length = 10) {
  $last_ids = db_select('system_logs', 'sl')
    ->fields('sl', array('id'))
    ->orderBy('id', 'DESC')
    ->range($range_start, $range_length)
    ->execute()
    ->fetchCol();

  if (!empty($last_ids)) {
    db_delete('system_logs')
      ->condition('id', $last_ids, 'NOT IN')
      ->execute();
  }
}

/**
 * Rewrite long distance settings variable.
 */
function reWriteLongDistanceVariable() {
  $longdistance_settings = variable_get('longdistance', '');
  if (is_array($longdistance_settings)) {
    variable_set('longdistance', json_encode($longdistance_settings));
  }
}

/*
 * Start button for update cache.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function move_services_new_update_move_request_cache($form, &$form_state) {
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Start update cache'),
  );

  return $form;
}

/**
 * Prepare batch for update cache move_request.
 */
function move_services_new_update_move_request_cache_submit() {
  $operations = [];
  $query = db_select('move_services_cache', 'msc');
  $query->fields('msc', ['nid']);
  $nids = $query->execute()->fetchCol();
  foreach ($nids as $nid) {
    $operations[] = ['move_service_new_update_cache_batch_execute', [$nid]];
  }

  $batch = array(
    'operations' => $operations,
    'finished' => 'move_services_new_update_cache_finished',
    'file' => drupal_get_path('module', 'move_services_new') . '/move_services_new.admin.inc',
  );

  batch_set($batch);
}

/**
 * Update cache nid.
 *
 * @param int $nid
 *   Nid.
 * @param array $context
 *   Batch context.
 *
 * @throws \Exception
 */
function move_service_new_update_cache_batch_execute(int $nid, array &$context) {
  if (Parking::isNodeExist($nid)) {
    Cache::updateCacheData($nid);
    $context['results']['titles'][] = $nid;
    $context['message'] = 'Обновлён кеш <em>' . check_plain($nid) . '</em>';
  }
}

/**
 * Batch finished.
 *
 * @param bool $success
 *   Result batch.
 * @param array $results
 *   Result.
 * @param array $operations
 *   Operation.
 */
function move_services_new_update_cache_finished(bool $success, array $results, array $operations) {
  if ($success) {
    drupal_set_message('Cashed ' . count($results['titles']) . ' nids.');
  }
  else {
    drupal_set_message('Ended with errors', 'error');
  }
}
