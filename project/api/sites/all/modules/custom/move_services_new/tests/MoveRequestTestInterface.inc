<?php
/**
 * @file
 * Interface tests.
 */

/**
 * Interface MoveRequestTest.
 */
interface MoveRequestTestInterface {
  /**
   * Save endpoint.
   *
   * @return mixed
   */
  public function saveNewEndpoint();

  /**
   * Get terms of taxonomy Trucs.
   *
   * @return mixed
   */
  public function getTaxonomyTrucks($name);
}
