<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

class MoveRequestUnitTest extends TestCase {

  public function setUp() {
    $GLOBALS['test_user'] = user_load_by_mail('moran049@gmail.com');
    $GLOBALS['test_admin'] = user_load_by_mail('roma4ke@gmail.com');
    $GLOBALS['test_manager'] = user_load_by_mail('bostonflatrate@gmail.com');
    $GLOBALS['users'] = array($GLOBALS['test_user']);
    $GLOBALS['all_users'] = array($GLOBALS['test_user'], $GLOBALS['test_admin'], $GLOBALS['test_manager']);
  }

  public function testCreateRequests() {
    $requestfile_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_services_new') . '/tests/data/requests.json';
    $requestfile_content = file_get_contents($requestfile_path);
    $parse_json = drupal_json_decode($requestfile_content);

    $obj = new CRUDMoveRequest();
    foreach ($parse_json as $item) {
      $result = $obj->create($item, 1);
      _search_api_index_queued_items();
      $this->assertArrayHasKey('nid', $result);
    }
  }

  public function testCreateImage() {
    global $user;
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_services_new') . '/tests/data/requestImage.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);

    foreach ($GLOBALS['users'] as $user1) {
      $user = $user1;
      foreach ($parse_json as $item) {
        $obj = new CRUDMoveRequest();
        $result = $obj->create($item, 1);
        $this->assertArrayHasKey('nid', $result);
      }
    }
  }

  public function testCreateImageTitle() {
    global $user;
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_services_new') . '/tests/data/requestImage.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);

    foreach ($GLOBALS['users'] as $user1) {
      $user = $user1;
      foreach ($parse_json as $item) {
        $obj = new CRUDMoveRequest();
        unset($item['field_move_images']['title']);
        $result = $obj->create($item, 1);
        $this->assertArrayHasKey('nid', $result);
      }
    }
  }

  public function testUpdateImageAdd() {
    global $user;
    $file_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_services_new') . '/tests/data/requestImageUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);

    foreach ($GLOBALS['users'] as $user1) {
      $user = $user1;
      $arguments = array(
        'page' => 0,
        'pagesize' => 5,
        'condition' => array('author:uid' => $user->uid),
      );
      $request = new MoveRequestSearch($arguments);
      $nodes = $request->search();
      $keys = array_keys($nodes);

      foreach ($parse_json as $item) {
        foreach ($keys as $key) {
          $obj = new CRUDMoveRequest($key);
          $result = $obj->update($item);
          $this->assertTrue((bool) $result);
        }
      }
    }
  }

  public function testUpdateImageDel() {
    global $user;
    foreach ($GLOBALS['users'] as $user1) {
      $user = $user1;
      $arguments = array(
        'page' => 0,
        'pagesize' => 5,
        'condition' => array('author:uid' => $user->uid),
      );
      $request = new MoveRequestSearch($arguments);
      $nodes = $request->search();

      foreach ($nodes as $key => $value) {
        $field_image = array();
        $count_image = count($value['field_move_images']);
        if ($count_image) {
          foreach ($value['field_move_images'] as $field_move_image) {
            $field_image['del'][] = array('fid' => $field_move_image['fid']);
          }

          $request = array('field_move_images' => $field_image);
          $obj = new CRUDMoveRequest($key);
          $result = $obj->update($request);
          $this->assertTrue((bool) $result);

          $obj2 = new CRUDMoveRequest($key);
          $result = $obj2->retrieve();
          $new_count_image = count($result['field_move_images']);
          $this->assertLessThan($count_image, $new_count_image);
        }
      }
    }
  }

  public function testFingerprint() {
    $fingerprint = CRUDMoveRequest::fingerprint(rand(1, 200));
    $this->assertArrayHasKey('fingerPrint', $fingerprint);
    $this->assertTrue(strlen($fingerprint['fingerPrint']) == 32);
  }

  public function testPaymentData() {
    global $user;
    $user = $GLOBALS['test_admin'];

    $requestfile_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_services_new') . '/tests/data/requests.json';
    $requestfile_content = file_get_contents($requestfile_path);
    $parse_json = drupal_json_decode($requestfile_content);

    $nids = array();
    $obj = new CRUDMoveRequest();
    foreach ($parse_json as $item) {
      $result = $obj->create($item, 1);
      $this->assertArrayHasKey('nid', $result);
      $nids[] = $result['nid'];
    }

    $test_data = array('test' => array(1, 2));
    // Set payment for requests.
    foreach ($nids as $nid) {
      $request = new CRUDMoveRequest($nid);
      $result = \Drupal\move_services_new\Services\ExtraServices::setExtraServices($nid, $test_data);
      $this->assertTrue((bool) $result, 'Payment was not saved.');

      $result = \Drupal\move_services_new\Services\ExtraServices::getExtraServices($nid);
      $this->assertTrue((bool) $result, 'Payment data is not exist');
    }
  }

}
