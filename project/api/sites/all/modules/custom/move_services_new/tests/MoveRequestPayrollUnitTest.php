<?php

use Drupal\move_services_new\Services\Sales;
use PHPUnit\Framework\TestCase;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\Payroll;
use Drupal\move_services_new\Util\enum\ContractType;
use Drupal\move_services_new\Services\Users;

class MoveRequestPayrollUnitTest extends TestCase {

  public function testCreateUsers() {
    $users = array();
    $file_path = dirname(__FILE__) . '/data/users.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    foreach ($parse_json as $user) {
      $client = new Clients();
      $new_user = $client->create($user);
      $users[] = $new_user->uid;
    }
    $users_count = count($users);
    $this->assertGreaterThanOrEqual(13, $users_count);

    return $users;
  }

  /**
   * @depends testCreateUsers
   */
  public function testCreateRequest($users) {
    global $user;
    $file_path = dirname(__FILE__) . '/data/request.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    $user = user_load($users[0]);

    $obj = new MoveRequest();
    $parse_json['data']['uid'] = $users[0];

    // Materials.
    $packings = array(
      'packings' => array(
        array(
          'name' => 'Custom Packing',
          'rate' => 10,
          'quantity' => 1,
          'custom' => TRUE,
        ),
        array(
          'name' => 'Middium Box',
          'rate' => 9,
          'quantity' => 10,
          'showInContract' => TRUE,
        ),
        array(
          'name' => 'Moving Pad',
          'rate' => 10,
          'laborRate' => 22,
          'LDRate' => 22,
          'quantity' => 1,
        ),
        array(
          'name' => 'King Mattress Bag',
          'rate' => 10,
          'laborRate' => 18,
          'LDRate' => 18,
          'quantity' => 1,
        ),
      ),
    );

    // Additional Services.
    $extra = array(
      array(
        'name' => "Custom Extra",
        'edit' => TRUE,
        'extra_services' => array(
          array(
            'services_default_value' => 1,
            'services_name' => "Cost",
            'services_read_only' => FALSE,
            'services_type' => "Amount",
          ),
          array(
            'services_default_value' => 10,
            'services_name' => "Value",
            'services_read_only' => FALSE,
            'services_type' => "Number",
          ),
          array(
            'services_default_value' => 1,
            'services_name' => "Weight",
            'services_read_only' => FALSE,
            'services_type' => "Weight",
          ),
        ),
      ),
      array(
        'name' => "Estimated Full Packing",
        'extra_services' => array(
          'services_name' => "Cost",
          'services_type' => "Amount",
          'services_read_only' => FALSE,
          'services_default_value' => 90,
        ),
      ),
    );

    $parse_json['all_data']['invoice']['request_data']['value'] = $packings;
    $parse_json['all_data']['invoice']['extraServices'] = json_encode($extra);
    $nid = $obj->create($parse_json, 1);

    $this->assertGreaterThanOrEqual(1, $nid['nid']);
    return $nid['nid'];
  }

  /**
   * @depends testCreateRequest
   */
  public function testCacheAllOld($nid) {
    $node = node_load($nid);
    $date_from = date("Y-m-d", $node->created);
    $date_to = date("Y-m-d", $node->created);
    $payroll_instance = new Payroll();
    $result = $payroll_instance->cacheAllPayroll($date_from, $date_to);
    $this->assertTrue((bool) $result);
    return $result;
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testSetManager($nid, $users) {
    global $user;
    $user = user_load($users[0]);
    $assign = (new Sales($nid))->assingManager($users[1], FALSE, FALSE);
    $this->assertTrue((bool) $assign, "Manager is assigned");
  }

  /**
   * @depends testCreateUsers
   */
  public function testSetManagerCommission($users) {
    $settings = Users::getUserSettings($users[1]);
    $settings['rateCommission'] = array(
      ['input' => 10, 'option' => 'Office Commission'],
      ['input' => 0, 'option' => 'Office With visual'],
      ['input' => 0, 'option' => 'Visual Estimate'],
    );
    $settings['exclude_additional'] = 'Exclude additional';
    $settings['exclude_fuel'] = 'Exclude fuel';
    $settings['exclude_packing'] = '';
    $settings['exclude_valuation'] = 'Exclude valuation';
    Users::writeUserSettings($users[1], $settings);
    $updated_settings = Users::getUserSettings($users[1]);
    $this->assertCount(3, $updated_settings['rateCommission']);
  }

  /**
   * @depends testCreateUsers
   */
  public function testSetForemanCommission($users) {
    $foremans = array(
      $users[7] => array(
        ['input' => 10, 'option' => 'Hourly Rate'],
        ['input' => 5, 'option' => 'Packing Commission'],
        ['input' => 3, 'option' => 'Insurance'],
        ['input' => 5, 'option' => 'Extras Commission'],
        ['input' => 10, 'option' => 'Bonus'],
      ),
    );
    foreach ($foremans as $uid => $rates) {
      $settings = Users::getUserSettings($uid);
      $settings['rateCommission'] = $rates;
      Users::writeUserSettings($uid, $settings);
      $updated_settings = Users::getUserSettings($uid);
      $this->assertCount(count($rates), $updated_settings['rateCommission']);
    }
  }

  /**
   * @depends testCreateUsers
   */
  public function testSetHelperCommission($users) {
    $helpers = array(
      $users[8] => array(
        ['input' => 10, 'option' => 'Hourly Rate'],
        ['input' => 5, 'option' => 'Packing Commission'],
        ['input' => 3, 'option' => 'Insurance'],
        ['input' => 15, 'option' => 'Hourly Rate (as helper)'],
        ['input' => 5, 'option' => 'Extras Commission'],
        ['input' => 10, 'option' => 'Bonus'],
      ),
      $users[10] => array(
        ['input' => 10, 'option' => 'Hourly Rate'],
        ['input' => 5, 'option' => 'Packing Commission'],
        ['input' => 3, 'option' => 'Insurance'],
        ['input' => 5, 'option' => 'Extras Commission'],
      ),
      $users[11] => array(
        ['input' => 10, 'option' => 'Hourly Rate'],
        ['input' => 5, 'option' => 'Packing Commission'],
        ['input' => 3, 'option' => 'Insurance'],
        ['input' => 5, 'option' => 'Extras Commission'],
      ),
      $users[12] => array(
        ['input' => 10, 'option' => 'Hourly Rate'],
        ['input' => 1, 'option' => 'Packing Commission'],
        ['input' => 3, 'option' => 'Insurance'],
        ['input' => 5, 'option' => 'Extras Commission'],
        ['input' => 10, 'option' => 'Bonus'],
      ),
    );

    foreach ($helpers as $uid => $rates) {
      $settings = Users::getUserSettings($uid);
      $settings['rateCommission'] = $rates;
      Users::writeUserSettings($uid, $settings);
      $updated_settings = Users::getUserSettings($uid);
      $this->assertCount(count($rates), $updated_settings['rateCommission']);
    }
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testSetRequestCrew($nid, $users) {
    global $user;
    $user = user_load($users[0]);
    $file_path = dirname(__FILE__) . '/data/requestSetCrew.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    $parse_json['field_foreman'] = array($users[7]);
    $parse_json['field_foreman_assign_time']['value'] = date("Y-m-d h:m:s", time());
    $parse_json['field_helper'] = array(
      $users[8],
      $users[10],
      $users[11],
      $users[12],
    );

    $crews = array(
      'crews' => array(
        'workersCollection' => array(
          $users[8],
          $users[10],
          $users[11],
          $users[12],
        ),
        'type' => 'local',
        'baseCrew' => array(
          'name' => 'Crew',
          'rate' => 0,
          'helpers' => array(
            $users[8],
            $users[10],
            $users[11],
            $users[12],
          ),
          'additionalCrews' => array(),
          'foreman' => $users[7],
        ),
      ),
    );
    $parse_json['request_data']['value'] = $crews;
    $request = new MoveRequest($nid);
    $request->update2($parse_json);
    $updated_request = new MoveRequest($nid);
    $retrieve = $updated_request->retrieve();
    $this->assertTrue((bool) $retrieve['field_foreman']['value']);
    $this->assertCount(4, $retrieve['field_helper']['value']);
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testSetContract($nid, $users) {
    $this->assertTrue((bool) $nid, "Request is created");
    $this->assertCount(13, $users);

    $contract = array(
      'materials' => 100,
      'extra_service_total' => 100,
      'fuel' => 100,
      'tips_total' => 100,
      'discount' => 0,
      'grand_total' => 1000,
      'hours' => 5,
      'progress' => 1,
      'foreman' => array(
        $users[7] => array(
          'hours_to_pay' => 5,
          'switcher' => 0,
          'additional' => 0,
        ),
      ),
      'helper' => array(
        $users[8] => array(
          'hours_to_pay' => 5,
          'switcher' => 0,
          'additional' => 0,
        ),
        $users[10] => array(
          'hours_to_pay' => 5,
          'switcher' => 0,
          'additional' => 0,
        ),
        $users[11] => array(
          'hours_to_pay' => 4,
          'switcher' => $users[12],
          'additional' => 0,
        ),
        $users[12] => array(
          'hours_to_pay' => 1,
          'switcher' => 0,
          'additional' => 0,
        ),
      ),
    );

    $payroll_instance = new Payroll();
    $new_contract = $payroll_instance->setContractInfo($nid, $contract);
    $this->assertTrue((bool) $new_contract);
  }

  /**
   * @depends testCreateRequest
   */
  public function testSetCompletedFlag($nid) {
    $obj = new MoveRequest($nid);
    $completed_flag = Payroll::getCompletedFlag();
    $data = array();
    $data['field_company_flags'] = [$completed_flag];
    $result = $obj->update2($data);
    $this->assertTrue((bool) $result);
  }

  /**
   * @depends testCreateRequest
   */
  public function testSetFieldDate($nid) {
    $obj = new MoveRequest($nid);
    $node = node_load($nid);
    $data = array();
    $data['field_date'] = date("Y-m-d", $node->created);
    $result = $obj->update2($data);
    $this->assertTrue((bool) $result);
    _search_api_index_queued_items();
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testWorkerTips($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid);
    $this->assertEquals(25, $payroll['foreman'][$users[7]]['commissions'][10]['total']);
    $this->assertEquals(25, $payroll['foreman'][$users[8]]['commissions'][10]['total']);
    $this->assertEquals(25, $payroll['helper'][$users[10]]['commissions'][10]['total']);
    $this->assertEquals(20, $payroll['helper'][$users[11]]['commissions'][10]['total']);
    $this->assertEquals(5, $payroll['helper'][$users[12]]['commissions'][10]['total']);
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testManagerCommission($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid);
    // @todo Check it. No commissions in array.
    $this->assertEquals(70, $payroll['manager'][$users[1]]['commissions'][0]['total']);
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testHourlyWorker($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid);
    $this->assertEquals(50, $payroll['foreman'][$users[7]]['commissions'][4]['total']);
    $this->assertArrayNotHasKey(4, $payroll['foreman'][$users[8]]['commissions']);
    $this->assertEquals(50, $payroll['helper'][$users[10]]['commissions'][4]['total']);
    $this->assertEquals(40, $payroll['helper'][$users[11]]['commissions'][4]['total']);
    $this->assertEquals(10, $payroll['helper'][$users[12]]['commissions'][4]['total']);
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testBonusWorker($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid);
    $this->assertEquals(50, $payroll['foreman'][$users[7]]['commissions'][11]['total']);
    $this->assertEquals(50, $payroll['foreman'][$users[8]]['commissions'][11]['total']);
    $this->assertArrayNotHasKey(11, $payroll['helper'][$users[10]]['commissions']);
    $this->assertArrayNotHasKey(11, $payroll['helper'][$users[11]]['commissions']);
    $this->assertEquals(10, $payroll['helper'][$users[12]]['commissions'][11]['total']);
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testForemanAsHelper($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid);
    $this->assertEquals(75, $payroll['foreman'][$users[8]]['commissions'][9]['total']);
    $this->assertEquals(25, $payroll['foreman'][$users[8]]['commissions'][10]['total']);
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testMaterialCommission($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid);
    $this->assertEquals(6, $payroll['foreman'][$users[7]]['commissions'][7]['total']);
    $this->assertArrayNotHasKey(7, $payroll['foreman'][$users[8]]['commissions']);
    $this->assertEquals(6, $payroll['helper'][$users[10]]['commissions'][7]['total']);
    $this->assertEquals(6, $payroll['helper'][$users[11]]['commissions'][7]['total']);
    $this->assertEquals(1.2, $payroll['helper'][$users[12]]['commissions'][7]['total']);
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   */
  public function testExtraCommission($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid);
    $this->assertEquals(5, $payroll['foreman'][$users[7]]['commissions'][2]['total']);
    $this->assertArrayNotHasKey(7, $payroll['foreman'][$users[8]]['commissions']);
    $this->assertEquals(5, $payroll['helper'][$users[10]]['commissions'][2]['total']);
    $this->assertEquals(5, $payroll['helper'][$users[11]]['commissions'][2]['total']);
    $this->assertEquals(5, $payroll['helper'][$users[12]]['commissions'][2]['total']);
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   * @depends testCacheAllOld
   */
  public function testCreatePaycheck($nid, $users, $old_cache) {
    $node = node_load($nid);
    // Test foreman.
    $paycheck = array(
      'date' => date("Y-m-d", $node->created),
      'form' => 0,
      'check_numb' => 1,
      'amount' => 100,
      "note" => "Payment note",
      "jobs" => [array("nid" => $nid, "contract_type" => ContractType::PAYROLL)],
    );
    $payroll_instance = new Payroll();
    $result = $payroll_instance->applyPaycheck($users[7], $paycheck);
    $this->assertTrue((bool) $result);
    $date_from = date("Y-m-d", $node->created);
    $date_to = date("Y-m-d", $node->created);
    $new_payroll = new Payroll($date_from, $date_to);
    $paychecks = $new_payroll->paycheckUserForPeriod($users[7]);
    $new_cache = $payroll_instance->cacheAllPayroll($date_from, $date_to);
    $foreman_paid = (float) $new_cache[3]['paid'] - (float) $old_cache[3]['paid'];
    // After cache updated total sum decremented to paycheck sum.
    $foreman_total = (float) $old_cache[3]['total'] - (float) $new_cache[3]['total'];
    $this->assertEquals(100, $foreman_paid);
    $this->assertEquals(100, $foreman_total);
    $this->assertInstanceOf(stdClass::class, $paychecks[0]);
    $this->assertObjectHasAttribute('payment_id', $paychecks[0]);
  }

  /**
   * @depends testCreateRequest
   * @depends testCreateUsers
   * @depends testCacheAllOld
   */
  public function testPayrollPaid($nid, $users, $old_cache) {
    global $user;
    $user = user_load($users[7]);
    $node = node_load($nid);
    $date_from = date("Y-m-d", $node->created);
    $date_to = date("Y-m-d", $node->created + 3600);
    $payroll_instance = new Payroll($date_from, $date_to);
    $new_cache = $payroll_instance->cacheAllPayroll($date_from, $date_to);
    $foreman_paid = (int) $new_cache[3]['paid'] - (int) $old_cache[3]['paid'];
    $this->assertEquals(100, $foreman_paid);
  }

  ##############################REQUEST FLAT RATE################################

  /**
   * @depends testCreateUsers
   */
  public function testCreateRequestFlat($users) {
    global $user;
    $file_path = dirname(__FILE__) . '/data/request.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    $user = user_load($users[0]);

    $obj = new MoveRequest();
    $parse_json['data']['uid'] = $users[0];
    $parse_json['data']['field_move_service_type'] = 5;

    // Materials.
    $packings = array(
      'packings' => array(
        array(
          'name' => 'Custom Packing',
          'rate' => 10,
          'quantity' => 1,
          'custom' => TRUE,
        ),
        array(
          'name' => 'Middium Box',
          'rate' => 9,
          'quantity' => 10,
          'showInContract' => TRUE,
        ),
        array(
          'name' => 'Moving Pad',
          'rate' => 10,
          'laborRate' => 22,
          'LDRate' => 22,
          'quantity' => 1,
        ),
        array(
          'name' => 'King Mattress Bag',
          'rate' => 10,
          'laborRate' => 18,
          'LDRate' => 18,
          'quantity' => 1,
        ),
      ),
    );

    // Additional Services.
    $extra = array(
      array(
        'name' => "Custom Extra",
        'edit' => TRUE,
        'extra_services' => array(
          array(
            'services_default_value' => 1,
            'services_name' => "Cost",
            'services_read_only' => FALSE,
            'services_type' => "Amount",
          ),
          array(
            'services_default_value' => 10,
            'services_name' => "Value",
            'services_read_only' => FALSE,
            'services_type' => "Number",
          ),
          array(
            'services_default_value' => 1,
            'services_name' => "Weight",
            'services_read_only' => FALSE,
            'services_type' => "Weight",
          ),
        ),
      ),
      array(
        'name' => "Estimated Full Packing",
        'extra_services' => array(
          'services_name' => "Cost",
          'services_type' => "Amount",
          'services_read_only' => FALSE,
          'services_default_value' => 90,
        ),
      ),
    );

    $parse_json['all_data']['invoice']['request_data']['value'] = $packings;
    $parse_json['all_data']['invoice']['extraServices'] = json_encode($extra);
    $result = $obj->create($parse_json, 1);

    $this->assertTrue(is_array($result));
    $this->assertTrue((bool) $result);
    $this->assertArrayHasKey('nid', $result);
    return $result['nid'];
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testSetManagerFlat($nid, $users) {
    global $user;
    $user = user_load($users[0]);
    $assign = (new Sales($nid))->assingManager($users[2], FALSE, FALSE);
    $this->assertTrue((bool) $assign, "Manager is assigned");
  }

  /**
   * @depends testCreateUsers
   */
  public function testSetManagerCommissionFlat($users) {
    $settings = Users::getUserSettings($users[2]);
    $settings['rateCommission'] = array(
      ['input' => 10, 'option' => 'Office Commission'],
      ['input' => 0, 'option' => 'Office With visual'],
      ['input' => 0, 'option' => 'Visual Estimate'],
    );
    $settings['exclude_additional'] = 'Exclude additional';
    $settings['exclude_fuel'] = '';
    $settings['exclude_packing'] = 'Exclude packing';
    $settings['exclude_valuation'] = '';
    Users::writeUserSettings($users[2], $settings);
    $updated_settings = Users::getUserSettings($users[2]);
    $this->assertCount(3, $updated_settings['rateCommission']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testSetRequestCrewFlat($nid, $users) {
    global $user;
    $user = user_load($users[0]);
    $file_path = dirname(__FILE__) . '/data/requestSetCrew.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    $parse_json['field_foreman'] = array($users[7], $users[8]);
    $parse_json['field_foreman_assign_time']['value'] = date("Y-m-d h:m:s", time());
    $parse_json['field_helper'] = array(
      $users[8],
      $users[10],
      $users[10],
      $users[11],
      $users[12],
      $users[12],
    );

    $crews = array(
      'crews' => array(
        'workersCollection' => array(
          $users[8],
          $users[10],
          $users[10],
          $users[11],
          $users[12],
          $users[12],
        ),
        'type' => 'flatRate',
        'pickedUpCrew' => array(
          'name' => 'Pick up crew',
          'foreman' => $users[7],
          'helpers' => array(
            $users[8],
            $users[10],
            $users[11],
            $users[12],
          ),
        ),
        'deliveryCrew' => array(
          'name' => 'Delivery crew',
          'foreman' => $users[8],
          'helpers' => array(
            $users[10],
            $users[12],
          ),
        ),
      ),
    );

    $parse_json['request_data']['value'] = $crews;
    $request = new MoveRequest($nid);
    $request->update2($parse_json);
    $updated_request = new MoveRequest($nid);
    $retrieve = $updated_request->retrieve();
    $this->assertCount(2, $retrieve['field_foreman']['value']);
    $this->assertCount(6, $retrieve['field_helper']['value']);
  }

  ##############################PICKUP PAYROLL##################################

  /**
   * @depends testCreateRequestFlat
   */
  public function testCacheAllOldFlatPickup($nid) {
    $node = node_load($nid);
    $date = date("Y-m-d", $node->created);
    $payroll_instance = new Payroll();
    $cache = $payroll_instance->cacheAllPayroll($date, $date);
    $this->assertTrue(is_array($cache));
    $this->assertTrue((bool) $cache);
    return $cache;
  }

  public function testSetLaborRate() {
    $settings = json_decode(variable_get('basicsettings', array()));
    $settings->packing_settings->packingLabor = TRUE;
    $encode_settings = json_encode($settings);
    variable_set('basicsettings', $encode_settings);
    $settings_updated = json_decode(variable_get('basicsettings'));
    $this->assertTrue(isset($settings_updated->packing_settings->packingLabor));
    $this->assertTrue($settings_updated->packing_settings->packingLabor);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testSetContractFlatPickup($nid, $users) {
    $this->assertTrue((bool) $nid, "Request is created");
    $this->assertCount(13, $users);

    $contract = array(
      'materials' => 100,
      'extra_service_total' => 100,
      'fuel' => 100,
      'tips_total' => 100,
      'discount' => 0,
      'grand_total' => 1000,
      'hours' => 5,
      'progress' => 1,
      'foreman' => array(
        $users[7] => array(
          'hours_to_pay' => 5,
          'switcher' => 0,
          'additional' => 0,
        ),
      ),
      'helper' => array(
        $users[8] => array(
          'hours_to_pay' => 5,
          'switcher' => 0,
          'additional' => 0,
        ),
        $users[10] => array(
          'hours_to_pay' => 5,
          'switcher' => 0,
          'additional' => 0,
        ),
        $users[11] => array(
          'hours_to_pay' => 4,
          'switcher' => $users[12],
          'additional' => 0,
        ),
        $users[12] => array(
          'hours_to_pay' => 1,
          'switcher' => 0,
          'additional' => 0,
        ),
      ),
    );

    $payroll_instance = new Payroll();
    $new_contract = $payroll_instance->setContractInfo($nid, $contract, ContractType::PICKUP_PAYROLL);
    $this->assertTrue((bool) $new_contract);
  }

  /**
   * @depends testCreateRequestFlat
   */
  public function testSetCompletedFlagFlat($nid) {
    $obj = new MoveRequest($nid);
    $data['field_company_flags'] = [Payroll::getCompletedFlag()];
    $result = $obj->update2($data);
    $this->assertTrue((bool) $result);
  }

  /**
   * @depends testCreateRequestFlat
   */
  public function testSetFieldDateFlat($nid) {
    $obj = new MoveRequest($nid);
    $node = node_load($nid);
    $data['field_date'] = date("Y-m-d", $node->created);
    $result = $obj->update2($data);
    $this->assertTrue((bool) $result);
    _search_api_index_queued_items();
  }

  /**
   * @depends testCreateRequestFlat
   */
  public function testCacheAllNewFlatPickup($nid) {
    $node = node_load($nid);
    $date = date("Y-m-d", $node->created);
    $payroll_instance = new Payroll();
    $cache = $payroll_instance->cacheAllPayroll($date, $date);
    $this->assertTrue(is_array($cache));
    $this->assertTrue((bool) $cache);
    return $cache;
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testWorkerTipsFlatPickup($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::PICKUP_PAYROLL);
    $this->assertEquals(25, $payroll['foreman'][$users[7]]['commissions'][10]['total']);
    $this->assertEquals(25, $payroll['foreman'][$users[8]]['commissions'][10]['total']);
    $this->assertEquals(25, $payroll['helper'][$users[10]]['commissions'][10]['total']);
    $this->assertEquals(20, $payroll['helper'][$users[11]]['commissions'][10]['total']);
    $this->assertEquals(5, $payroll['helper'][$users[12]]['commissions'][10]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testManagerCommissionFlat($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::PICKUP_PAYROLL);
    $this->assertEquals(70, $payroll['manager'][$users[2]]['commissions'][0]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testHourlyWorkerFlatPickup($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::PICKUP_PAYROLL);
    $this->assertEquals(50, $payroll['foreman'][$users[7]]['commissions'][4]['total']);
    $this->assertArrayNotHasKey(4, $payroll['foreman'][$users[8]]['commissions']);
    $this->assertEquals(50, $payroll['helper'][$users[10]]['commissions'][4]['total']);
    $this->assertEquals(40, $payroll['helper'][$users[11]]['commissions'][4]['total']);
    $this->assertEquals(10, $payroll['helper'][$users[12]]['commissions'][4]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testBonusWorkerFlatPickup($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::PICKUP_PAYROLL);
    $this->assertEquals(50, $payroll['foreman'][$users[7]]['commissions'][11]['total']);
    $this->assertEquals(50, $payroll['foreman'][$users[8]]['commissions'][11]['total']);
    $this->assertArrayNotHasKey(11, $payroll['helper'][$users[10]]['commissions']);
    $this->assertArrayNotHasKey(11, $payroll['helper'][$users[11]]['commissions']);
    $this->assertEquals(10, $payroll['helper'][$users[12]]['commissions'][11]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testForemanAsHelperFlatPickup($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::PICKUP_PAYROLL);
    $this->assertEquals(75, $payroll['foreman'][$users[8]]['commissions'][9]['total']);
    $this->assertEquals(25, $payroll['foreman'][$users[8]]['commissions'][10]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testMaterialCommissionFlatPickup($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::PICKUP_PAYROLL);
    $this->assertEquals(4, $payroll['foreman'][$users[7]]['commissions'][7]['total']);
    $this->assertArrayNotHasKey(7, $payroll['foreman'][$users[8]]['commissions']);
    $this->assertEquals(4, $payroll['helper'][$users[10]]['commissions'][7]['total']);
    $this->assertEquals(4, $payroll['helper'][$users[11]]['commissions'][7]['total']);
    $this->assertEquals(0.8, $payroll['helper'][$users[12]]['commissions'][7]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testExtraCommissionFlatPickup($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::PICKUP_PAYROLL);
    $this->assertEquals(5, $payroll['foreman'][$users[7]]['commissions'][2]['total']);
    $this->assertArrayNotHasKey(7, $payroll['foreman'][$users[8]]['commissions']);
    $this->assertEquals(5, $payroll['helper'][$users[10]]['commissions'][2]['total']);
    $this->assertEquals(5, $payroll['helper'][$users[11]]['commissions'][2]['total']);
    $this->assertEquals(5, $payroll['helper'][$users[12]]['commissions'][2]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testPaycheckFlatPickup($nid, $users) {
    $node = node_load($nid);
    $paycheck = array(
      'date' => date("Y-m-d", $node->created),
      'form' => 0,
      'check_numb' => 2,
      'amount' => 100,
      "note" => "Payment note",
      "jobs" => [array("nid" => $nid, "contract_type" => 0)],
    );

    // Test foreman.
    $payroll_instance = new Payroll();
    $result = $payroll_instance->applyPaycheck($users[7], $paycheck);
    $this->assertTrue((bool) $result);
    $date_from = date("Y-m-d", $node->created);
    $date_to = date("Y-m-d", $node->created);
    $new_payroll = new Payroll($date_from, $date_to);
    $paychecks = $new_payroll->paycheckUserForPeriod($users[7]);
    $this->assertInstanceOf(stdClass::class, $paychecks[1]);
    $this->assertObjectHasAttribute('payment_id', $paychecks[1]);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCacheAllOldFlatPickup
   * @depends testCacheAllNewFlatPickup
   * @depends testCreateUsers
   */
  public function testCacheAllFlatPickup($nid, $old_cache, $new_cache, $users) {
    $manager_jobs = (float) $new_cache[1]['jobs_count'] - (float) $old_cache[1]['jobs_count'];
    $manager_grand_total = (float) $new_cache[1]['grand_total'] - (float) $old_cache[1]['grand_total'];
    $manager_total = (float) $new_cache[1]['total'] - (float) $old_cache[1]['total'];
    $helper_jobs = (float) $new_cache[2]['jobs_count'] - (float) $old_cache[2]['jobs_count'];
    $helper_hours = (float) $new_cache[2]['hours'] - (float) $old_cache[2]['hours'];
    $helper_hours_pay = (float) $new_cache[2]['hours_pay'] - (float) $old_cache[2]['hours_pay'];
    $helper_grand_total = (float) $new_cache[2]['grand_total'] - (float) $old_cache[2]['grand_total'];
    $helper_materials = (float) $new_cache[2]['materials'] - (float) $old_cache[2]['materials'];
    $helper_extra = (float) $new_cache[2]['extra'] - (float) $old_cache[2]['extra'];
    $helper_tip = (float) $new_cache[2]['tip'] - (float) $old_cache[2]['tip'];
    $helper_paid = (float) $new_cache[2]['paid'] - (float) $old_cache[2]['paid'];
    $helper_total = (float) $new_cache[2]['total'] - (float) $old_cache[2]['total'];
    $foreman_jobs = (float) $new_cache[3]['jobs_count'] - (float) $old_cache[3]['jobs_count'];
    $foreman_hours = (float) $new_cache[3]['hours'] - (float) $old_cache[3]['hours'];
    $foreman_hours_pay = (float) $new_cache[3]['hours_pay'] - (float) $old_cache[3]['hours_pay'];
    $foreman_grand_total = (float) $new_cache[3]['grand_total'] - (float) $old_cache[3]['grand_total'];
    $foreman_materials = (float) $new_cache[3]['materials'] - (float) $old_cache[3]['materials'];
    $foreman_extra = (float) $new_cache[3]['extra'] - (float) $old_cache[3]['extra'];
    $foreman_tip = (float) $new_cache[3]['tip'] - (float) $old_cache[3]['tip'];
    $foreman_paid = (float) $new_cache[3]['paid'] - (float) $old_cache[3]['paid'];
    $foreman_total = (float) $new_cache[3]['total'] - (float) $old_cache[3]['total'];

    $this->assertEquals(1, $manager_jobs);
    $this->assertEquals(1000, $manager_grand_total);
    $this->assertEquals(70, $manager_total);
    $this->assertEquals(3, $helper_jobs);
    $this->assertEquals(10, $helper_hours);
    $this->assertEquals(100, $helper_hours_pay);
    $this->assertEquals(3000, $helper_grand_total);
    $this->assertEquals(13.2, $helper_materials);
    $this->assertEquals(15, $helper_extra);
    $this->assertEquals(50, $helper_tip);
    $this->assertEquals(0, $helper_paid);
    $this->assertEquals(188.2, $helper_total);
    $this->assertEquals(2, $foreman_jobs);
    $this->assertEquals(10, $foreman_hours);
    $this->assertEquals(125, $foreman_hours_pay);
    $this->assertEquals(2000, $foreman_grand_total);
    $this->assertEquals(12, $foreman_materials);
    $this->assertEquals(10, $foreman_extra);
    $this->assertEquals(50, $foreman_tip);
    $this->assertEquals(100, $foreman_paid);
    $this->assertEquals(186, $foreman_total);
  }

  ##############################DELIVERY PAYROLL#############################

  /**
   * @depends testCreateRequestFlat
   */
  public function testSetFieldDateDelivery($nid) {
    $obj = new MoveRequest($nid);
    $data['field_delivery_date'] = date("Y-m-d", time());
    $result = $obj->update2($data);
    $this->assertTrue((bool) $result);
    _search_api_index_queued_items();
  }

  /**
   * @depends testCreateRequestFlat
   */
  public function testCacheAllOldDeliveryPickup($nid) {
    $node_wrapper = entity_metadata_wrapper('node', $nid);
    $date = $node_wrapper->field_delivery_date->raw();
    $date_new = date("Y-m-d", $date);
    $payroll_instance = new Payroll();
    $cache = $payroll_instance->cacheAllPayroll($date_new, $date_new);
    $this->assertTrue(is_array($cache));
    $this->assertTrue((bool) $cache);
    return $cache;
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testSetContractFlatDelivery($nid, $users) {
    $this->assertTrue((bool) $nid, "Request is created");
    $this->assertCount(13, $users);

    $contract = array(
      'materials' => 100,
      'extra_service_total' => 100,
      'fuel' => 100,
      'tips_total' => 100,
      'discount' => 0,
      'grand_total' => 1000,
      'hours' => 10,
      'progress' => 1,
      'foreman' => array(
        $users[8] => array(
          'hours_to_pay' => 10,
          'switcher' => 0,
          'additional' => 0,
        ),
      ),
      'helper' => array(
        $users[10] => array(
          'hours_to_pay' => 10,
          'switcher' => 0,
          'additional' => 0,
        ),
        $users[12] => array(
          'hours_to_pay' => 10,
          'switcher' => 0,
          'additional' => 0,
        ),
      ),
    );

    $payroll_instance = new Payroll();
    $new_contract = $payroll_instance->setContractInfo($nid, $contract, ContractType::DELIVERY_PAYROLL);
    $this->assertTrue((bool) $new_contract);
  }

  /**
   * @depends testCreateRequestFlat
   */
  public function testCacheAllNewDeliveryPickup($nid) {
    $node_wrapper = entity_metadata_wrapper('node', $nid);
    $date = $node_wrapper->field_delivery_date->raw();
    $date_new = date("Y-m-d", $date);
    $payroll_instance = new Payroll();
    $cache = $payroll_instance->cacheAllPayroll($date_new, $date_new);
    $this->assertTrue(is_array($cache));
    $this->assertTrue((bool) $cache);
    return $cache;
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testWorkerTipsFlatDelivery($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::DELIVERY_PAYROLL);
    $this->assertEquals(33.33, round($payroll['foreman'][$users[8]]['commissions'][10]['total'], 2));
    $this->assertEquals(33.33, round($payroll['helper'][$users[10]]['commissions'][10]['total'], 2));
    $this->assertEquals(33.33, round($payroll['helper'][$users[12]]['commissions'][10]['total'], 2));
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testHourlyWorkerFlatDelivery($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::DELIVERY_PAYROLL);
    $this->assertEquals(100, $payroll['foreman'][$users[8]]['commissions'][4]['total']);
    $this->assertEquals(100, $payroll['helper'][$users[10]]['commissions'][4]['total']);
    $this->assertEquals(100, $payroll['helper'][$users[12]]['commissions'][4]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testBonusWorkerFlatDelivery($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::DELIVERY_PAYROLL);
    $this->assertEquals(100, $payroll['foreman'][$users[8]]['commissions'][11]['total']);
    $this->assertArrayNotHasKey(11, $payroll['helper'][$users[10]]['commissions']);
    $this->assertEquals(100, $payroll['helper'][$users[12]]['commissions'][11]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testMaterialCommissionFlatDelivery($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::DELIVERY_PAYROLL);
    $this->assertEquals(4, $payroll['foreman'][$users[8]]['commissions'][7]['total']);
    $this->assertEquals(4, $payroll['helper'][$users[10]]['commissions'][7]['total']);
    $this->assertEquals(0.8, $payroll['helper'][$users[12]]['commissions'][7]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCreateUsers
   */
  public function testExtraCommissionFlatDelivery($nid, $users) {
    $payroll_instance = new Payroll();
    $payroll = $payroll_instance->getAllPayrollInfo($nid, ContractType::DELIVERY_PAYROLL);
    $this->assertEquals(5, $payroll['foreman'][$users[8]]['commissions'][2]['total']);
    $this->assertEquals(5, $payroll['helper'][$users[10]]['commissions'][2]['total']);
    $this->assertEquals(5, $payroll['helper'][$users[12]]['commissions'][2]['total']);
  }

  /**
   * @depends testCreateRequestFlat
   * @depends testCacheAllOldDeliveryPickup
   * @depends testCacheAllNewDeliveryPickup
   */
  public function testCacheAllFlatDelivery($nid, $old_cache, $new_cache) {
    $manager_jobs = (float) $new_cache[1]['jobs_count'] - (float) $old_cache[1]['jobs_count'];
    $manager_grand_total = (float) $new_cache[1]['grand_total'] - (float) $old_cache[1]['grand_total'];
    $manager_total = (float) $new_cache[1]['total'] - (float) $old_cache[1]['total'];

    $helper_jobs = (float) $new_cache[2]['jobs_count'] - (float) $old_cache[2]['jobs_count'];
    $helper_hours = (float) $new_cache[2]['hours'] - (float) $old_cache[2]['hours'];
    $helper_hours_pay = (float) $new_cache[2]['hours_pay'] - (float) $old_cache[2]['hours_pay'];
    $helper_grand_total = (float) $new_cache[2]['grand_total'] - (float) $old_cache[2]['grand_total'];
    $helper_materials = (float) $new_cache[2]['materials'] - (float) $old_cache[2]['materials'];
    $helper_extra = (float) $new_cache[2]['extra'] - (float) $old_cache[2]['extra'];
    $helper_tip = (float) $new_cache[2]['tip'] - (float) $old_cache[2]['tip'];
    $helper_paid = (float) $new_cache[2]['paid'] - (float) $old_cache[2]['paid'];
    $helper_total = (float) $new_cache[2]['total'] - (float) $old_cache[2]['total'];

    $foreman_jobs = (float) $new_cache[3]['jobs_count'] - (float) $old_cache[3]['jobs_count'];
    $foreman_hours = (float) $new_cache[3]['hours'] - (float) $old_cache[3]['hours'];
    $foreman_hours_pay = (float) $new_cache[3]['hours_pay'] - (float) $old_cache[3]['hours_pay'];
    $foreman_grand_total = (float) $new_cache[3]['grand_total'] - (float) $old_cache[3]['grand_total'];
    $foreman_materials = (float) $new_cache[3]['materials'] - (float) $old_cache[3]['materials'];
    $foreman_extra = (float) $new_cache[3]['extra'] - (float) $old_cache[3]['extra'];
    $foreman_tip = (float) $new_cache[3]['tip'] - (float) $old_cache[3]['tip'];
    $foreman_paid = (float) $new_cache[3]['paid'] - (float) $old_cache[3]['paid'];
    $foreman_total = (float) $new_cache[3]['total'] - (float) $old_cache[3]['total'];

    $this->assertEquals(0, $manager_jobs);
    $this->assertEquals(2000, $manager_grand_total);
    $this->assertEquals(70, $manager_total);
    $this->assertEquals(3, $helper_jobs);
    // @todo Check!
    $this->assertEquals(30, $helper_hours);
    // @todo Check!
    $this->assertEquals(300, $helper_hours_pay);
    // @todo Check!
    $this->assertEquals(6000, $helper_grand_total);
    $this->assertEquals(26.4, $helper_materials);
    $this->assertEquals(30, $helper_extra);
    // @todo Check!
    $this->assertEquals(116.66, $helper_tip);
    $this->assertEquals(0, $helper_paid);
    // @todo Check!
    $this->assertEquals(572.06, $helper_total);
    $this->assertEquals(2, $foreman_jobs);
    $this->assertEquals(20, $foreman_hours);
    $this->assertEquals(225, $foreman_hours_pay);
    $this->assertEquals(4000, $foreman_grand_total);
    $this->assertEquals(24, $foreman_materials);
    $this->assertEquals(20, $foreman_extra);
    $this->assertEquals(83.33, $foreman_tip);
    $this->assertEquals(0, $foreman_paid);
    $this->assertEquals(530.33, $foreman_total);
  }

  /**
   * @depends testCreateRequest
   */
  public function testChangeStatusConfirmed($nid) {
    $request = new MoveRequest($nid);
    $data = array("field_approve" => "1");
    $status = $request->update2($data);
    $this->assertTrue((bool) $status);
  }

  /**
   * @depends testCreateRequestFlat
   */
  public function testChangeStatusConfirmedFlat($nid) {
    $request = new MoveRequest($nid);
    $data = array("field_approve" => "1");
    $status = $request->update2($data);
    $this->assertTrue((bool) $status);
  }

  /**
   * @depends testCreateUsers
   * @depends testCreateRequest
   * @depends testCreateRequestFlat
   */
  public function testClearPayrollCache($users, $request_nid, $request_flat_nid) {
    $users_payroll = $users;
    unset($users_payroll[0]);
    $payroll = new Payroll();
    // Removing Payroll users who assigned to crew in dispatch for requests.
    $rnd = $payroll->removeUserPayroll($request_nid, $users_payroll, ContractType::PAYROLL);
    $rnd_values = array_values($rnd);
    $rnd_subset = array(1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1);
    $this->assertArraySubset($rnd_subset, $rnd_values);

    $rfnd = $payroll->removeUserPayroll($request_flat_nid, $users_payroll, ContractType::PICKUP_PAYROLL);
    $rfnd_values = array_values($rfnd);
    $rfnd_subset = array(0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1);
    $this->assertArraySubset($rfnd_subset, $rfnd_values);

    $rdnd = $payroll->removeUserPayroll($request_flat_nid, $users_payroll, ContractType::DELIVERY_PAYROLL);
    $rdnd_values = array_values($rdnd);
    $rdnd_subset = array(0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1);
    $this->assertArraySubset($rdnd_subset, $rdnd_values);
  }

  /**
   * @depends testCreateRequest
   */
  public function testDeleteRequest($nid) {
    $obj = new MoveRequest($nid);
    $obj->delete();
    $node = node_load($nid);
    $this->assertFalse($node);
  }

  /**
   * @depends testCreateRequestFlat
   */
  public function testDeleteRequestFlat($nid) {
    $obj = new MoveRequest($nid);
    $obj->delete();
    $node = node_load($nid);
    $this->assertFalse($node);
  }

  /**
   * @depends testCreateUsers
   */
  public function testDeleteUsers($users) {
    foreach ($users as $uid) {
      $client = new Clients($uid);
      $client->delete();
      $user = user_load($uid);
      $this->assertFalse($user);
    }
  }

  /**
   * @depends testCreateRequest
   */
  public function testDeleteAll($nid) {
    $delete = db_delete('move_contract_info')
      ->condition('nid', $nid)
      ->execute();
    $this->assertEquals(1, $delete);
    $payroll_custom = db_select('move_payroll_custom', 'mpc')
      ->fields('mpc', array('id'))
      ->condition('nid', $nid)
      ->execute()
      ->fetchCol();

    $delete = db_delete('move_payroll_custom')
      ->condition('nid', $nid)
      ->execute();
    // @todo Change to 1
    $this->assertEquals(0, $delete);

    if ($payroll_custom) {
      $delete = db_delete('move_payroll_custom_photo')
        ->condition('pcid', array($payroll_custom), 'IN')
        ->execute();
      // @todo Change to 1
      $this->assertEquals(0, $delete);
    }
  }

  /**
   * @depends testCreateRequestFlat
   */
  public function testDeleteAllFlat($nid) {
    $delete = db_delete('move_contract_info')
      ->condition('nid', $nid)
      ->execute();
    $this->assertEquals(2, $delete);
    $payroll_custom = db_select('move_payroll_custom', 'mpc')
      ->fields('mpc', array('id'))
      ->condition('nid', $nid)
      ->execute()
      ->fetchCol();

    $delete = db_delete('move_payroll_custom')
      ->condition('nid', $nid)
      ->execute();
    // @todo Change to 1
    $this->assertEquals(0, $delete);

    if ($payroll_custom) {
      $delete = db_delete('move_payroll_custom_photo')
        ->condition('pcid', array($payroll_custom), 'IN')
        ->execute();
      // @todo Change to 1
      $this->assertEquals(0, $delete);
    }
  }

}
