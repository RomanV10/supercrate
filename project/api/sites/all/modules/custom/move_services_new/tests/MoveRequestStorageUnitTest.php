<?php
use Drupal\move_services_new\Services\move_request\MoveRequest;

class MoveRequestStorageUnitTest extends PHPUnit_Framework_TestCase {

  public function setUp() {
    $GLOBALS['test_user'] = user_load_by_mail('moran049@gmail.com');
    $GLOBALS['test_admin'] = user_load_by_mail('roma4ke@gmail.com');
    $GLOBALS['test_manager'] = user_load_by_mail('bostonflatrate@gmail.com');
    $GLOBALS['admin'] = array($GLOBALS['test_admin']);
    $GLOBALS['all_users'] = array(
      $GLOBALS['test_user'],
      $GLOBALS['test_admin'],
      $GLOBALS['test_manager'],
    );
  }

  public function testCreateRequests() {
    $requestfile_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_services_new') . '/tests/data/requestsStorage.json';
    $requestfile_content = file_get_contents($requestfile_path);
    $parse_json = drupal_json_decode($requestfile_content);

    global $user;
    foreach ($GLOBALS['all_users'] as $user1) {
      $user = $user1;
      $obj = new MoveRequest();
      $result = $obj->storageRequestCreate($parse_json);
      _search_api_index_queued_items();
      $this->assertInternalType('array', $result);
      foreach ($result as $item) {
        $this->assertArrayHasKey('nid', $item);
      }
    }
  }

}
