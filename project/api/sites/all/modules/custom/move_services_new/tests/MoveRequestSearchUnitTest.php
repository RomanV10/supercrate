<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

class MoveRequestSearchUnitTest extends PHPUnit_Framework_TestCase {

  public function setUp() {
    $GLOBALS['test_user'] = user_load_by_mail('moran049@gmail.com');
    $GLOBALS['test_admin'] = user_load_by_mail('roma4ke@gmail.com');
    $GLOBALS['test_manager'] = user_load_by_mail('bostonflatrate@gmail.com');
    $GLOBALS['users'] = array($GLOBALS['test_admin']);
  }

  public function testSearchRequests() {
    global $user;
    foreach ($GLOBALS['users'] as $user1) {
      $user = $user1;

      // Initialize some variables.
      $statuses = array(1, 10, 17, 16, 9);

      // Get count before requests creation.
      $arguments = array(
        "pageSize" => 500,
        "condition" => array(
          "field_approve" => $statuses,
        ),
      );
      $request = new MoveRequestSearch($arguments);
      $count_before = $request->search(TRUE);

      // Create requests.
      $requestfile_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_services_new') . '/tests/data/requestsForSearch.json';
      $requestfile_content = file_get_contents($requestfile_path);
      $parse_json = drupal_json_decode($requestfile_content);

      $obj = new CRUDMoveRequest();
      foreach ($parse_json as $item) {
        $result = $obj->create($item, 1);
        _search_api_index_queued_items();
        if (is_array($result)) {
          $this->assertArrayHasKey('nid', $result);
        }
      }

      // Get count after requests creation.
      $request = new MoveRequestSearch($arguments);
      $count_after = $request->search(TRUE);

      $diff = $count_after - $count_before;
      if ($diff >= 5) {
        $this->assertTrue(TRUE);
      }
      else {
        $this->fail('Count of requests is wrong = ' . $diff . ' count1=' . $count_before . ' count2=' . $count_after);
      }
    }
  }

}
