<?php
/**
 * @file
 * Interface tests.
 */

/**
 * Class MoveRequestTest.
 */
class MoveRequestTest implements MoveRequestTestInterface {
  /**
   * {@inheritdoc}
   */
  public function saveNewEndpoint() {
    $endpoint = new stdClass();
    $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
    $endpoint->api_version = 3;
    $endpoint->name = 'server';
    $endpoint->server = 'rest_server';
    $endpoint->path = 'server';
    $endpoint->authentication = array();
    $endpoint->server_settings = array(
      'formatters' => array(
        'json' => TRUE,
        'jsonp' => TRUE,
        'php' => TRUE,
        'bencode' => FALSE,
        'xml' => FALSE,
      ),
      'parsers' => array(
        'application/json' => TRUE,
        'application/x-www-form-urlencoded' => TRUE,
        'multipart/form-data' => TRUE,
        'text/xml' => TRUE,
        'application/vnd.php.serialized' => FALSE,
        'application/xml' => FALSE,
      ),
    );
    $endpoint->resources = array(
      'clients' => array(
        'operations' => array(
          'retrieve' => array(
            'enabled' => '1',
          ),
          'create' => array(
            'enabled' => '1',
          ),
          'update' => array(
            'enabled' => '1',
          ),
          'delete' => array(
            'enabled' => '1',
          ),
          'index' => array(
            'enabled' => '1',
          ),
        ),
        'actions' => array(
          'getcurrent' => array(
            'enabled' => '1',
          ),
          'checkunique' => array(
            'enabled' => '1',
          ),
        ),
      ),
      'front' => array(
        'operations' => array(
          'retrieve' => array(
            'enabled' => '1',
          ),
          'create' => array(
            'enabled' => '1',
          ),
        ),
      ),
      'jobs' => array(
        'operations' => array(
          'retrieve' => array(
            'enabled' => '1',
          ),
          'create' => array(
            'enabled' => '1',
          ),
          'update' => array(
            'enabled' => '1',
          ),
          'delete' => array(
            'enabled' => '1',
          ),
        ),
      ),
      'move_request' => array(
        'operations' => array(
          'retrieve' => array(
            'enabled' => '1',
          ),
          'index' => array(
            'enabled' => '1',
          ),
          'delete' => array(
            'enabled' => '1',
          ),
          'create' => array(
            'enabled' => '1',
          ),
          'update' => array(
            'enabled' => '1',
          ),
        ),
        'actions' => array(
          'search' => array(
            'enabled' => '1',
          ),
          'requests' => array(
            'enabled' => '1',
          ),
          'storage_request' => array(
            'enabled' => '1',
          ),
        ),
      ),
      'move_request_comments' => array(
        'operations' => array(
          'retrieve' => array(
            'enabled' => '1',
          ),
          'create' => array(
            'enabled' => '1',
          ),
          'delete' => array(
            'enabled' => '1',
          ),
          'update' => array(
            'enabled' => '1',
          ),
        ),
        'actions' => array(
          'get_all_comments' => array(
            'enabled' => '1',
          ),
          'get_new_comments' => array(
            'enabled' => '1',
          ),
          'set_read_comment' => array(
            'enabled' => '1',
          ),
          'unread_count' => array(
            'enabled' => '1',
          ),
        ),
      ),
      'move_sales_log' => array(
        'operations' => array(
          'retrieve' => array(
            'enabled' => '1',
          ),
          'create' => array(
            'enabled' => '1',
          ),
        ),
      ),
      'movers' => array(
        'operations' => array(
          'create' => array(
            'enabled' => '1',
          ),
          'retrieve' => array(
            'enabled' => '1',
          ),
          'update' => array(
            'enabled' => '1',
          ),
          'delete' => array(
            'enabled' => '1',
          ),
          'index' => array(
            'enabled' => '1',
          ),
        ),
      ),
      'system' => array(
        'actions' => array(
          'connect' => array(
            'enabled' => '1',
          ),
        ),
      ),
      'user' => array(
        'operations' => array(
          'delete' => array(
            'enabled' => '1',
          ),
        ),
        'actions' => array(
          'login' => array(
            'enabled' => '1',
            'settings' => array(
              'services' => array(
                'resource_api_version' => '1.0',
              ),
            ),
          ),
          'logout' => array(
            'enabled' => '1',
            'settings' => array(
              'services' => array(
                'resource_api_version' => '1.0',
              ),
            ),
          ),
          'token' => array(
            'enabled' => '1',
          ),
        ),
      ),
    );
    $endpoint->debug = 0;
    $endpoint->export_type = FALSE;
    services_endpoint_save($endpoint);
    $endpoint = services_endpoint_load($endpoint->name);
    return $endpoint;
  }

  public function getTaxonomyTrucks($name) {
    $taxonomy = taxonomy_vocabulary_machine_name_load($name);
    $tree = taxonomy_get_tree($taxonomy->vid);
    $result = array();

    foreach ($tree as $term) {
      $result[] = $term->tid;
    }

    return $result;
  }
}
