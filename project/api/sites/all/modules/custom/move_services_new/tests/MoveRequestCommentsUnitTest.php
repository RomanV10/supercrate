<?php

use Drupal\move_services_new\Services\Comments;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\Sales;

class MoveRequestCommentsUnitTest extends PHPUnit_Framework_TestCase {

  public function setUp() {
    $GLOBALS['test_user'] = user_load_by_mail('moran049@gmail.com');
    $GLOBALS['test_admin'] = user_load_by_mail('roma4ke@gmail.com');
    $GLOBALS['test_manager'] = user_load_by_mail('bostonflatrate@gmail.com');
  }

  public function testUserComents() {
    global $user;
    // #########################################################################
    // Unread count of user.
    $user = $GLOBALS['test_user'];
    $x = new Comments();
    $user_unread = $x->getUnreadCountUser();

    // Unread count of admin.
    $user = $GLOBALS['test_admin'];
    $x = new Comments();
    $admin_unread = $x->getUnreadCountUser();

    // Unread count of manager.
    $user = $GLOBALS['test_manager'];
    $x = new Comments();
    $manager_unread = $x->getUnreadCountUser();

    // #########################################################################
    // User create a new node.
    $user = $GLOBALS['test_user'];
    $request = array('data' => array("title" => "Test"));
    $obj = new MoveRequest();
    $result = $obj->create($request, 1);
    $this->assertArrayHasKey('nid', $result);

    if (is_array($result) && isset($result['nid'])) {
      $test_user_nid = $result['nid'];
      $this->assertTrue((bool) $result['nid'], 'Request was created.');
    }

    // #########################################################################
    // Set manager for request.
    $user = $GLOBALS['test_admin'];
    $obj = new Sales($test_user_nid);
    $manager = $obj->assingManager($GLOBALS['test_manager']->uid, FALSE, FALSE);
    $this->assertTrue($manager, 'New manager to request.');

    // #########################################################################
    // User create comment.
    $user = $GLOBALS['test_user'];
    $comment = array('message' => 'New mess');
    $obj = new Comments($test_user_nid);
    $result = $obj->create($comment);
    $this->assertTrue((bool) $result, 'Comment from user was created.');

    // #########################################################################
    // Admin sees what increase unread comments.
    $user = $GLOBALS['test_admin'];
    $x = new Comments();
    $admin_unread_new = $x->getUnreadCountUser();
    $this->assertLessThan($admin_unread_new, $admin_unread, 'New comment for admin.');

    // #########################################################################
    // Manager sees what increase unread comments.
    $user = $GLOBALS['test_manager'];
    $x = new Comments();
    $manager_unread_new = $x->getUnreadCountUser();
    $this->assertLessThan($manager_unread_new, $manager_unread, 'New comment for manager.');

    // #########################################################################
    // Unread count from user.
    $user = $GLOBALS['test_user'];
    $x = new Comments();
    $user_unread_new = $x->getUnreadCountUser();
    $this->assertEquals($user_unread_new, $user_unread, 'User unread is equals.');

    // #########################################################################
    // Admin get list of all comments by request.
    $user = $GLOBALS['test_admin'];
    $x = new Comments($test_user_nid);
    $comments = $x->retrieve();
    $this->assertInternalType('array', $comments);

    $cids = array();
    foreach ($comments as $comment) {
      if (!$comment['admin'] && !$comment['read']) {
        $cids[] = $comment['cid'];
      }
    }

    // Admin has unread comments.
    $this->assertTrue((bool) count($cids));
    // #########################################################################
    // Manager get list of all comments by request.
    $user = $GLOBALS['test_manager'];
    $xy = new Comments($test_user_nid);
    $comments = $xy->retrieve();
    $this->assertInternalType('array', $comments);

    $cids = array();
    foreach ($comments as $comment) {
      if (!$comment['admin'] && !$comment['read']) {
        $cids[] = $comment['cid'];
      }
    }

    // Manager has unread comments.
    $this->assertTrue((bool) count($cids));

    foreach ($cids as $cid) {
      $comment = entity_load('comment', array((int) $cid));
      // Set comment to "read" status.
      if ($comment) {
        $comments = new Comments($comment[(int) $cid]->nid);
        $result = $comments->setReadComment($comment[(int) $cid]);
        $this->assertTrue($result, 'Comment was set to read.');
      }
    }

    // #########################################################################
    // Manager create a new comment for request.
    $user = $GLOBALS['test_manager'];
    $comment = new Comments($test_user_nid);
    $data = array('message' => '<p>Manager comment</p>', 'format' => 'full_html');
    $result = $comment->create($data);
    $this->assertTrue((bool) $result, 'Manager created comment to request.');

    // #########################################################################
    // Count of unread comments was increased for user.
    $user = $GLOBALS['test_user'];
    $x = new Comments($test_user_nid);
    $user_unread_new = $x->getUnreadCountUser();
    $this->assertGreaterThan($user_unread, $user_unread_new);

    // Unread count of admin is unchanged.
    $user = $GLOBALS['test_admin'];
    $x = new Comments();
    $admin_unread_new = $x->getUnreadCountUser();
    $this->assertEquals($admin_unread, $admin_unread_new);

    // Unread count of manager is unchanged.
    $user = $GLOBALS['test_manager'];
    $x = new Comments();
    $manager_unread_new = $x->getUnreadCountUser();
    $this->assertEquals($manager_unread, $manager_unread_new);

    // #########################################################################
    // User get list of all comments by request.
    $user = $GLOBALS['test_user'];
    $xy = new Comments($test_user_nid);
    $comments = $xy->retrieve();
    $this->assertInternalType('array', $comments);

    $cids = array();
    foreach ($comments as $comment) {
      if ($comment['admin'] && !$comment['read']) {
        $cids[] = $comment['cid'];
      }
    }

    // Manager has unread comments.
    $this->assertTrue((bool) count($cids));

    foreach ($cids as $cid) {
      $comment = entity_load('comment', array((int) $cid));
      // Set comment to status to "read".
      if ($comment) {
        $comments = new Comments($comment[(int) $cid]->nid);
        $result = $comments->setReadComment($comment[(int) $cid]);
        $this->assertTrue($result, 'Comment was set to read.');
      }
    }

    // #########################################################################
    // Admin create a new comment for request.
    $user = $GLOBALS['test_admin'];
    $comment = new Comments($test_user_nid);
    $data = array('message' => '<p>Manager comment</p>', 'format' => 'full_html');
    $result = $comment->create($data);
    $this->assertTrue((bool) $result, 'Manager created comment to request.');

    // #########################################################################
    // Count of unread comments was increased for user.
    $user = $GLOBALS['test_user'];
    $x = new Comments($test_user_nid);
    $user_unread_new = $x->getUnreadCountUser();
    $this->assertGreaterThan($user_unread, $user_unread_new);

    // Unread count of admin is unchanged.
    $user = $GLOBALS['test_admin'];
    $x = new Comments();
    $admin_unread_new = $x->getUnreadCountUser();
    $this->assertEquals($admin_unread, $admin_unread_new);

    // Unread count of manager is unchanged.
    $user = $GLOBALS['test_manager'];
    $x = new Comments();
    $manager_unread_new = $x->getUnreadCountUser();
    $this->assertEquals($manager_unread, $manager_unread_new);

    // #########################################################################
    // User get list of all comments by request.
    $user = $GLOBALS['test_user'];
    $xy = new Comments($test_user_nid);
    $comments = $xy->retrieve();
    $this->assertInternalType('array', $comments);

    $cids = array();
    foreach ($comments as $comment) {
      if ($comment['admin'] && !$comment['read']) {
        $cids[] = $comment['cid'];
      }
    }

    // Manager has unread comments.
    $this->assertTrue((bool) count($cids));

    foreach ($cids as $cid) {
      $comment = entity_load('comment', array((int) $cid));
      // Set comment to status to "read".
      if ($comment) {
        $comments = new Comments($comment[(int) $cid]->nid);
        $result = $comments->setReadComment($comment[(int) $cid]);
        $this->assertTrue($result, 'Comment was set to read.');
      }
    }
  }

  public function testCountNewComments() {
    global $user;
    // #########################################################################
    // Set to 'read' all comments for user.
    $user = $GLOBALS['test_user'];
    $comments = new Comments();
    $user_new_comments = $comments->getAllNewComments();

    if ($user_new_comments) {
      foreach ($user_new_comments as $nid) {
        $xy = new Comments($nid);
        $comments3 = $xy->retrieve();

        $cids = array();
        foreach ($comments3 as $comment) {
          if ($comment['admin'] && !$comment['read']) {
            $cids[] = $comment['cid'];
          }
        }

        // User has unread comments.
        if ($cids) {
          $this->assertTrue((bool) count($cids));
        }

        foreach ($cids as $cid) {
          $comment = entity_load('comment', array((int) $cid));
          // Set comment to status to "read".
          if ($comment) {
            $comments2 = new Comments($comment[(int) $cid]->nid);
            $result = $comments2->setReadComment($comment[(int) $cid]);
            $this->assertTrue($result, 'Comment was set to read.');
          }
        }
      }
    }

    $user_new_comments = $comments->getAllNewComments();
    $this->assertFalse((bool) count($user_new_comments));

    // #########################################################################
    // Set to 'read' all comments for manager.
    $user = $GLOBALS['test_manager'];
    $comments = new Comments();
    $user_new_comments = $comments->getAllNewComments();

    foreach ($user_new_comments as $request) {
      $xy = new Comments($request['nid']);
      $comments3 = $xy->retrieve();

      $cids = array();
      foreach ($comments3 as $comment) {
        if (!$comment['admin'] && !$comment['read']) {
          $cids[] = $comment['cid'];
        }
      }

      // Manager has unread comments.
      if ($cids) {
        $this->assertTrue((bool) count($cids));
      }

      foreach ($cids as $cid) {
        $comment = entity_load('comment', array((int) $cid));
        // Set comment to status to "read".
        if ($comment) {
          $comments2 = new Comments($comment[(int) $cid]->nid);
          $result = $comments2->setReadComment($comment[(int) $cid]);
          $this->assertTrue($result, 'Comment was set to read.');
        }
      }
    }

    $user_new_comments = $comments->getAllNewComments();
    $this->assertCount(0, $user_new_comments);
  }

  public function testGetAllComments() {
    global $user;
    // #########################################################################
    // Set to 'read' all comments for user.
    $user = $GLOBALS['test_user'];
    $comments = new Comments();
    $user_all_comments = $comments->getAllCommentsByUid();
    $user_comments_count = 0;
    foreach ($user_all_comments as $node) {
      foreach ($node as $comment) {
        $user_comments_count++;
      }
    }

    // Creating new request.
    $request = array('data' => array("title" => "Test"));
    $obj = new MoveRequest();
    $result = $obj->create($request, 1);
    $this->assertArrayHasKey('nid', $result);

    if (is_array($result) && isset($result['nid'])) {
      $test_user_nid = $result['nid'];
      $this->assertTrue((bool) $result['nid'], 'Request was created.');
    }

    // #########################################################################
    // User create comment.
    $comment = array('message' => 'New mess');
    $obj = new Comments($test_user_nid);
    $result = $obj->create($comment);
    $this->assertTrue((bool) $result, 'Comment from user was created.');

    $comments = new Comments();
    $user_new_all_comments = $comments->getAllCommentsByUid();
    $user_new_comments_count = 0;
    foreach ($user_new_all_comments as $node) {
      foreach ($node as $comment) {
        $user_new_comments_count++;
      }
    }

    $this->assertGreaterThan($user_comments_count, $user_new_comments_count);

  }

}
