<?php

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\Users;

class MoveRequestSalesTest extends TestCase {

  public function testCreateUsers() {
    $users = array();
    $file_path = dirname(__FILE__) . '/data/salesUser.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    foreach ($parse_json as $user) {
      $client = new Clients();
      $new_user = $client->create($user);
      $users[] = $new_user->uid;
    }
    variable_set('sales_test_users', $users);

    return $users;
  }

  public function testCreateAuthor() {
    $file_path = dirname(__FILE__) . '/data/salesAuthor.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    $client = new Clients();
    $new_author = $client->create($parse_json);
    $author = $new_author->uid;

    return $author;
  }

  /**
   * @depends testCreateUsers
   *
   */
  public function testAddSalesAssignment($users) {

    foreach ($users as $user) {
      $settings = Users::getUserSettings($user);
      $settings['autoassignment']['isAutoAssign'] = 1;
      $user = Users::writeUserSettings($user, $settings);
    }
  }

  /**
   * @depends testCreateUsers
   */
  public function testAddSalesServiceType($users) {
    foreach ($users as $key => $user) {
      $settings = Users::getUserSettings($user);
      if ($key == 1) {
        $settings['autoassignment']['services'] = [1, 2, 3, 4, 5, 6, 7];
      }
      else {
        $settings['autoassignment']['services'] = [1, 3, 5, 7];
      }
      $user = Users::writeUserSettings($user, $settings);
    }
  }

  /**
   * @depends testCreateAuthor
   * @depends testCreateUsers
   */
  public function testCreateRequest1($author, $users) {
    $all_nid = array();
    $file_path = dirname(__FILE__) . '/data/requestSales.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    $sales = array();
    foreach ($parse_json as $item) {
      $obj = new MoveRequest();
      $item['data']['uid'] = $author;
      $nid = $obj->create($item, 1);
      $all_nid[] = $nid['nid'];
      $sales[] = (int) MoveRequest::getRequestManager($nid['nid']);
    }
    $this->assertEquals($sales[0], $users[0]);
    $this->assertEquals($sales[1], $users[1]);
    $this->assertEquals($sales[2], $users[1]);
    $this->assertEquals($sales[3], $users[1]);
    $this->assertEquals($sales[4], $users[0]);
    $this->assertEquals($sales[5], $users[2]);
    $this->assertEquals($sales[6], $users[1]);
    $this->assertEquals($sales[7], $users[2]);
    return $all_nid;
  }

  /**
   * @depends testCreateUsers
   */
  public function testUpdateUserSettings($users) {
    $settings = Users::getUserSettings($users[1]);
    $settings['autoassignment']['services'] = [1, 3, 4, 5, 6, 7];
    $user = Users::writeUserSettings($users[1], $settings);
    $settings = Users::getUserSettings($users[1]);
    $this->assertCount(6, $settings['autoassignment']['services']);
  }

  /**
   * @depends testCreateAuthor
   * @depends testCreateUsers
   */
  public function testCreateRequest2($author, $users) {
    $file_path = dirname(__FILE__) . '/data/requestSales2.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    $obj = new MoveRequest();
    $parse_json['data']['uid'] = $author;
    $nid2 = $obj->create($parse_json, 1);
    $sales = (int) MoveRequest::getRequestManager($nid2['nid']);
    $this->assertEquals($sales, 0);
    $this->deleteUsers($users);
    return $nid2['nid'];
  }

  /**
   * @depends testCreateRequest1
   * @depends testCreateRequest2
   */
  public function testDeleteAll($all_nid, $nid2) {
    variable_del("old_sales_person_0");
    variable_del("current_sales_person_0");
    variable_del("old_sales_person_1");
    variable_del("current_sales_person_1");
    variable_del("old_sales_person_2");
    variable_del("current_sales_person_2");
    variable_del("old_sales_person_3");
    variable_del("current_sales_person_3");
    variable_del("old_sales_person_4");
    variable_del("current_sales_person_4");
    variable_del("old_sales_person_5");
    variable_del("current_sales_person_5");
    variable_del("old_sales_person_6");
    variable_del("current_sales_person_6");
    variable_del("old_sales_person_7");
    variable_del("current_sales_person_7");
    variable_del("list_sales_person_types");

    foreach ($all_nid as $nid) {
      $obj = new MoveRequest($nid);
      $obj->delete();
      $node = user_load($nid);
      $this->assertFalse($node);
    }
    $obj = new MoveRequest($nid2);
    $obj->delete();
    $node = user_load($nid2);
    $this->assertFalse($node);
  }

  private function deleteUsers($users) {
    foreach ($users as $uid) {
      $client = new Clients($uid);
      $client->delete();
    }
  }

}