<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

class SettingstUnitTest extends PHPUnit_Framework_TestCase {

  public function setUp() {
    $GLOBALS['test_user'] = user_load_by_mail('moran049@gmail.com');
    $GLOBALS['test_admin'] = user_load_by_mail('roma4ke@gmail.com');
    $GLOBALS['test_manager'] = user_load_by_mail('bostonflatrate@gmail.com');
    $GLOBALS['admins'] = array($GLOBALS['test_admin']);
  }

  public function testGetSettings() {
    $available_vars = CRUDSettings::availableVars();
    $settings = CRUDSettings::getSettings();
    foreach ($settings as $name => $setting) {
      $this->assertTrue(in_array($name, $available_vars));
    }
  }

  public function testSetDefaultSettings() {
    global $user;

    foreach ($GLOBALS['admins'] as $admin) {
      $user = $admin;
      $this->assertTrue(CRUDSettings::setDefaultSettings());
    }
  }

}
