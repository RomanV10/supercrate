<?php

namespace Drupal\move_distance_calculate\System;

use Drupal\move_distance_calculate\Services\DistanceCalculate;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'move_distance_calculate' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_distance_calculate\System\Service::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'to_state',
                'type' => 'string',
                'source' => array('data' => 'to_state'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'from_state',
                'type' => 'string',
                'source' => array('data' => 'from_state'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_distance_calculate\System\Service::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_distance_calculate\System\Service::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_distance_calculate\System\Service::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_distance_calculate\System\Service::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'check_travel_time' => array(
            'help' => 'Search move requests',
            'callback' => 'Drupal\move_distance_calculate\System\Service::checkTravelTime',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'zip_from',
                'type' => 'string',
                'source' => array('data' => 'zip_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'zip_to',
                'type' => 'string',
                'source' => array('data' => 'zip_to'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'move_service_type',
                'type' => 'int',
                'source' => array('data' => 'move_service_type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'calculate_fuel_surcharge' => array(
            'help' => 'Search move requests',
            'callback' => 'Drupal\move_distance_calculate\System\Service::calculateFuelSurcharge',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'distance_origin_to_destination' => array(
            'help' => 'Search move requests',
            'callback' => 'Drupal\move_distance_calculate\System\Service::getDistanceAtoB',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'zip_from',
                'type' => 'string',
                'source' => array('data' => 'zip_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'zip_to',
                'type' => 'string',
                'source' => array('data' => 'zip_to'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_request_service_type' => array(
            'help' => 'Search move requests',
            'callback' => 'Drupal\move_distance_calculate\System\Service::getServiceRequestType',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'zip_from',
                'type' => 'string',
                'source' => array('data' => 'zip_from'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'zip_to',
                'type' => 'string',
                'source' => array('data' => 'zip_to'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function create(string $from_state, string $to_state, $data = array()) {
    $al_instance = new DistanceCalculate();
    $data_all = array(
      'from_state' => $from_state,
      'to_state'   => $to_state,
      'data'       => $data,
    );
    return $al_instance->create($data_all);
  }

  public static function retrieve($id) {
    $al_instance = new DistanceCalculate($id);
    return $al_instance->retrieve();
  }

  public static function update($id, $data) {
    $al_instance = new DistanceCalculate($id);
    return $al_instance->update($data);
  }

  public static function delete(int $id) {
    $al_instance = new DistanceCalculate($id);
    return $al_instance->delete();
  }

  public static function index() {
    $al_instance = new DistanceCalculate();
    return $al_instance->index();
  }

  public static function checkTravelTime($zip_from, $zip_to, $move_service_type) {
    $distance = new DistanceCalculate();
    return $distance->checkTravelTime($zip_from, $zip_to, $move_service_type, FALSE);
  }

  public static function calculateFuelSurcharge($data) {
    $distance = new DistanceCalculate();
    return $distance->calcFuelSurcharge($data);
  }

  public static function getDistanceAtoB($zip_from, $zip_to) {
    return DistanceCalculate::googleRequestDistance($zip_from, $zip_to);
  }

  public static function getServiceRequestType($zip_from, $zip_to) {
    return (new DistanceCalculate())->getServiceRequestType($zip_from, $zip_to);
  }

}
