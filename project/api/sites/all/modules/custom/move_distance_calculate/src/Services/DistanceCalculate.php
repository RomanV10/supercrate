<?php

namespace Drupal\move_distance_calculate\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_template_builder\Services\TemplateBuilder;
use Drupal\move_services_new\Util\enum\RequestSizeOfMove;

/**
 * Class DistanceCalculate.
 *
 * @package Drupal\move_distance_calculate\Services
 */
class DistanceCalculate extends BaseService {

  private $from_office_to_zip_from = array();
  private $from_zip_to_to_office = array();
  private $from_zip_from_to_zip_to = array();
  private $AbResult;
  private $useCache;

  /**
   * Long distance settings.
   *
   * @var mixed
   */
  private $longDistanceSettings;

  /**
   * Basic settings.
   *
   * @var mixed
   */
  private $basicSettings;

  /**
   * Calculate settings.
   *
   * @var mixed
   */
  private $calculateSettings;

  /**
   * Get longDistance.
   *
   * @return mixed
   *   LongDistance.
   */
  public function getLongDistanceSettings() {
    return $this->longDistanceSettings;
  }

  /**
   * Set longDistance.
   *
   * @param mixed $longDistanceSettings
   *   LongDistance.
   */
  public function setLongDistanceSettings($longDistanceSettings): void {
    $this->longDistanceSettings = $longDistanceSettings;
  }

  /**
   * Get basicSettings.
   *
   * @return mixed
   *   BasicSettings.
   */
  public function getBasicSettings() {
    return $this->basicSettings;
  }

  /**
   * Set basicSettings.
   *
   * @param mixed $basicSettings
   *   basicSettings.
   */
  public function setBasicSettings($basicSettings): void {
    $this->basicSettings = $basicSettings;
  }

  /**
   * Get calculateSettings.
   *
   * @return mixed
   *   calculateSettings.
   */
  public function getCalculateSettings() {
    return $this->calculateSettings;
  }

  /**
   * Set calculateSettings.
   *
   * @param mixed $calculateSettings
   *   calculateSettings.
   */
  public function setCalculateSettings($calculateSettings): void {
    $this->calculateSettings = $calculateSettings;
  }

  /**
   * DistanceCalculate constructor.
   *
   * @param bool $useCache
   *   Use cache for distance.
   */
  public function __construct(bool $useCache = FALSE) {
    $this->basicSettings = json_decode(variable_get('basicsettings'), TRUE);
    $this->longDistanceSettings = json_decode(variable_get('longdistance', ''));
    $this->calculateSettings = json_decode(variable_get('calcsettings'), TRUE);
    $this->useCache = $useCache;
  }

  public function create($data_all = array()) {

    $result = db_insert('move_distance_calculate')
      ->fields(array(
        'from_state' => $data_all['from_state'],
        'to_state' => $data_all['to_state'],
        'data' => serialize($data_all['data']),
      ))
      ->execute();

    return $result;
  }

  public function retrieve() {}

  public function update($data = array()) {}

  public function delete() {}

  public function index() {
    $result = array();
    $query = db_select('move_distance_calculate', 'mdc')
      ->fields('mdc')
      ->execute()
      ->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

    foreach ($query as $key => $value) {
      $value['data'] = unserialize($value['data']);

      $result[] = array(
        'id' => $value['id'],
        'from_state' => $value['from_state'],
        'to_state' => $value['to_state'],
        'data' => $value['data'],
      );
    }

    return $result;
  }

  public static function checkState(string $from_state, string $to_state) {
    $result = FALSE;
    $query = db_select('move_distance_calculate', 'mdc')
      ->condition('mdc.from_state', $from_state)
      ->condition('mdc.to_state', $to_state)
      ->fields('mdc', array('data'))
      ->execute()
      ->fetchField();

    if ($query) {
      $result = unserialize($query);
    }

    return $result;
  }

  public static function checkExistAreaAndState(&$data) {
    if ($data['field_moving_from']['postal_code']) {
      if (!$data['field_moving_from']['administrative_area'] || !$data['field_moving_from']['locality']) {
        $location_data = static::getLocationDataByZip($data['field_moving_from']['postal_code']);
        $data['field_moving_from']['administrative_area'] = !empty($location_data->state) ? $location_data->state : '';
        $data['field_moving_from']['locality'] = !empty($location_data->city) ? $location_data->city : '';
      }
    }
    if ($data['field_moving_to']['postal_code']) {
      if (!$data['field_moving_to']['administrative_area'] || !$data['field_moving_to']['locality']) {
        $location_data = static::getLocationDataByZip($data['field_moving_to']['postal_code']);
        $data['field_moving_to']['administrative_area'] = !empty($location_data->state) ? $location_data->state : '';
        $data['field_moving_to']['locality'] = !empty($location_data->city) ? $location_data->city : '';
      }
    }
  }

  public static function getLocationDataByZip($zip) {
    $array_zip = entity_load('zip_codes', FALSE, array('zip' => $zip));
    $zip_data = reset($array_zip);

    return $zip_data;
  }

  /**
   * Method for return state by zip code.
   *
   * @param string $zip
   *   String value of zip code.
   *
   * @return string
   *   2 first upper characters of state.
   */
  public static function getStateByZip($zip) {
    $define_state_to = '';
    if ($zip) {
      $array_zip = entity_load('zip_codes', FALSE, array('zip' => $zip));
      $zip_data = reset($array_zip);
      if (!empty($zip_data)) {
        $define_state_to = (property_exists($zip_data, 'state')) ? $zip_data->state : '';
      }
    }

    return $define_state_to;
  }

  /**
   * Determining long distance request.
   *
   * @param array $data
   *   Request array with filled fields.
   * @param int $provider_id
   *   Provider id.
   */
  public function checkLongDistance(array &$data, int $provider_id) {
    $parking_zip = $this->getBasicSettings()['parking_address'];
    $moving_from_zip_city = $data['field_moving_from']['locality'] ?? '';
    $moving_from_adm_area = isset($data['field_moving_from']['administrative_area']) ? strtoupper($data['field_moving_from']['administrative_area']) : '';
    $moving_from_zip = isset($data['field_moving_from']['postal_code']) ? $data['field_moving_from']['postal_code'] : static::getZipByCity($moving_from_zip_city, $moving_from_adm_area);
    $moving_to_zip_city = $data['field_moving_to']['locality'] ?? '';
    $moving_to_adm_area = isset($data['field_moving_to']['administrative_area']) ? strtoupper($data['field_moving_to']['administrative_area']) : '';
    $moving_to_zip = isset($data['field_moving_to']['postal_code']) ? $data['field_moving_to']['postal_code'] : static::getZipByCity($moving_to_zip_city, $moving_to_adm_area);
    $main_state = $this->getBasicSettings()['main_state'] ?? '';
    $long_distance_miles = $this->getBasicSettings()['long_distance_miles'] ?? '';
    $flat_rate_miles = $this->getBasicSettings()['flat_rate_miles'] ?? '';
    $based_state = (array) $this->getLongDistanceSettings()->stateRates;

    if ($moving_from_adm_area && $main_state) {
      $is_based_state = array_key_exists($moving_from_adm_area, $based_state);
      if (($main_state !== $moving_from_adm_area) && !$is_based_state) {
        $data['field_move_service_type'] = RequestServiceType::LONG_DISTANCE;
        $data['all_data'] = array('showQuote' => FALSE, 'isDisplayQuoteEye' => TRUE);
      }
      else {
        $parking_to_origin = static::googleRequestDistance($parking_zip, $moving_from_zip, $this->useCache);
        $parking_to_destination = static::googleRequestDistance($parking_zip, $moving_to_zip, $this->useCache);
        if ($this->getBasicSettings()['islong_distance_miles'] && ($parking_to_origin['distance'] > $long_distance_miles || $parking_to_destination['distance'] > $long_distance_miles)) {
          $data['field_move_service_type'] = RequestServiceType::LONG_DISTANCE;
          $data['field_long_distance_rate'] = $this->getDistancePrice($moving_to_zip, $moving_from_adm_area);

          if ($data['field_long_distance_rate']) {
            $data['all_data'] = $this->checkProviderQuote($provider_id);
          }
          else {
            $data['all_data'] = array('showQuote' => FALSE, 'isDisplayQuoteEye' => TRUE);
          }
          // Fill Delivery Days.
          $zip = entity_load('zip_codes', FALSE, array('zip' => $moving_to_zip));
          if ($zip) {
            $zip = reset($zip);
            $state_to = strtolower($zip->state);
            $exist_delivery_days = isset($this->getLongDistanceSettings()->stateRates->{$moving_from_adm_area}->{$state_to}->delivery_days);
            $data['inventory']['details']['delivery_days'] = $exist_delivery_days ? $this->getLongDistanceSettings()->stateRates->{$moving_from_adm_area}->{$state_to}->delivery_days : NULL;
          }
        }
        elseif ($this->getBasicSettings()['isflat_rate_miles'] && ($parking_to_origin['distance'] > $flat_rate_miles || $parking_to_destination['distance'] > $flat_rate_miles)) {
          $data['field_move_service_type'] = RequestServiceType::FLAT_RATE;
          $data['all_data'] = $this->checkProviderQuote($provider_id);
        }
        else {
          $data['field_move_service_type'] = RequestServiceType::MOVING;
          $data['all_data'] = $this->checkProviderQuote($provider_id);
        }
      }
    }
  }

  public static function checkParkingAddress($moving_from_adm_area, $isparser = FALSE) {
    $basic_settings = json_decode(variable_get('basicsettings', ''), TRUE);
    $parking_address = isset($basic_settings['parking_address']) ? $basic_settings['parking_address'] : '';
    $longdistance_settings = json_decode(variable_get('longdistance', ''));
    $moving_from_adm_area = isset($moving_from_adm_area) ? strtoupper($moving_from_adm_area) : '';
    $main_state = isset($basic_settings['main_state']) ? $basic_settings['main_state'] : '';
    $based_state = property_exists($longdistance_settings, 'stateRates') ? (array) $longdistance_settings->stateRates : array();

    if ($isparser) {
      if ($main_state == $moving_from_adm_area) {
        $parking_address = isset($basic_settings['parking_address']) ? $basic_settings['parking_address'] : '';
      }
      else {
        // TODO. Add validate string parking address.
        $is_based_state = array_key_exists($moving_from_adm_area, $based_state);
        if ($is_based_state && property_exists($based_state[$moving_from_adm_area], 'parkingZip')) {
          $parking_address = $based_state[$moving_from_adm_area]->parkingZip;
        }
      }
    }

    return $parking_address;
  }

  /**
   * Set "showQuote" and "isDisplayQuoteEye" in request.
   *
   * @param int $provider_id
   *   Parser provider id.
   *
   * @return array
   *   Show quote data.
   */
  public function checkProviderQuote(int $provider_id) {
    $data = array();
    if (isset($this->getBasicSettings()['parsers'][$provider_id])) {
      if ($this->getBasicSettings()['parsers'][$provider_id]['showQuote']) {
        $data = array('showQuote' => TRUE, 'isDisplayQuoteEye' => FALSE);
      }
      else {
        $data = array('showQuote' => FALSE, 'isDisplayQuoteEye' => TRUE);
      }
    }

    return $data;
  }

  public function getServiceRequestType(string $zip_from, string $zip_to) {
    $basic_settings = $this->getBasicSettings();
    $long_distance_miles = isset($basic_settings['long_distance_miles']) ? $basic_settings['long_distance_miles'] : '';
    $parking_zip = $basic_settings['parking_address'];
    $parking_to_origin = static::googleRequestDistance($parking_zip, $zip_from, $this->useCache);
    $parking_to_destination = static::googleRequestDistance($parking_zip, $zip_to, $this->useCache);

    if ($basic_settings['islong_distance_miles'] && ($parking_to_origin['distance'] > $long_distance_miles || $parking_to_destination['distance'] > $long_distance_miles)) {
      $service_type = 7;
    }
    elseif ($this->checkFlatRate($zip_from, $zip_to, $parking_to_origin, $parking_to_destination)) {
      $service_type = 5;
    }
    else {
      $service_type = 1;
    }

    return $service_type;
  }

  // A - Origin, B - Destination, C - Parking Address
  // AB - from origin to destination
  // CA from parking to Origin.
  public function calculateFuelSurchargeByMiles($data) {
    $result = 0;
    $is_ld_request = $data['serviceType'] == 7 || $data['serviceType'] == 5;

    try {
      $basic_settings = json_decode(variable_get('basicsettings'), TRUE);

      if ($this->checkFuelSurchargeByMile($is_ld_request)) {
        if ($is_ld_request) {
          $settings_by_mileage = $basic_settings['fuel_surcharge']['settingsByMileageLD'];
        }
        else {
          $settings_by_mileage = $basic_settings['fuel_surcharge']['settingsByMileage'];
        }

        $distance = 0;
        $amount = $settings_by_mileage['amount'];
        $selected_way = $settings_by_mileage['selectedWay'];
        $details = $settings_by_mileage['details'];
        $minimum_distance = $settings_by_mileage['minimumDistance'];
        $distances = $data['request_distance']['distances'];
        $origin_distance = $distances['CA']['distance'] ?? 0;
        $destination_distance = $distances['BC']['distance'] ?? 0;
        $origin_destination_distance = $distances['AB']['distance'] ?? 0;

        switch ($selected_way) {
          // In one direction.
          case '0':
            if ($details) {
              switch ($details) {
                case 'shortest':
                  $distance = min($origin_distance, $destination_distance);
                  break;

                case 'longest':
                  $distance = max($origin_distance, $destination_distance);
                  break;

                case 'origin':
                  $distance = $origin_distance;
                  break;

                case 'destination':
                  $distance = $destination_distance;
                  break;
              }
            }

            break;

          // From C to A plus from B to C.
          case '1':
            $distance = $origin_distance + $destination_distance;
            break;

          // Full distance.
          case '2':
            $distance = $origin_distance + $origin_destination_distance + $destination_distance;
            break;

          // From A to B.
          case '3':
            $distance = $origin_destination_distance;
            break;
        }

        // if distance less than min distance not calculate fuel surcharge
        if ($minimum_distance > $distance) {
          $distance = 0;
        }

        $distance *= $amount;
        $result = round($distance);
      }
    } finally {
      return $result;
    }
  }

  private function checkFuelSurchargeByMile($is_ld_request) {
    $basic_settings = json_decode(variable_get('basicsettings'), TRUE);

    if ($is_ld_request) {
      $result = isset($basic_settings['fuel_surcharge']['settingsByMileageLD'])
        && $basic_settings['fuel_surcharge']['settingsByMileageLD']['amount'];
    }
    else {
      $result = isset($basic_settings['fuel_surcharge']['settingsByMileage'])
        && $basic_settings['fuel_surcharge']['settingsByMileage']['amount'];
    }

    return $result;
  }

  private function getParkingAddress($zip_from, $is_parser = FALSE) {
    $moving_from_adm_area = static::getLocationDataByZip($zip_from);
    $moving_from_adm_area = !empty($moving_from_adm_area->state) ? $moving_from_adm_area->state : '';
    $parking_address = static::checkParkingAddress($moving_from_adm_area, $is_parser);

    return $parking_address;
  }

  public function checkTravelTime(string $zip_from, string $zip_to, int $move_service_type = NULL, $is_parser = FALSE) {
    $parking_address = $this->getParkingAddress($zip_from, $is_parser);
    $full_travel = static::prepareFullTravel($zip_from, $zip_to, $parking_address);
    $isActive = $this->getCalculateSettings()['flatTravelTime']['isActive'] ?? FALSE;
    $center_postalCode = $this->getCalculateSettings()['flatTravelTime']['center_postalCode'] ?? FALSE;
    $radius = $this->getCalculateSettings()['flatTravelTime']['radius'] ?? FALSE;
    $travel_time_is_duration = $this->getCalculateSettings()['isTravelTimeSameAsDuration'];

    if ($parking_address) {
      $full_travel['zipParking'] = $parking_address;
      $full_travel = $this->travelDataByRequestType($zip_from, $zip_to, $parking_address, $move_service_type);
      $full_travel['duration_origin_destination'] = $this->from_zip_from_to_zip_to['duration'];
      $full_travel['distance_origin_destination'] = $this->from_zip_from_to_zip_to['distance'];

      if ($move_service_type == RequestServiceType::MOVING && !$travel_time_is_duration && $this->checkLocalMove()) {
        if ($isActive && $center_postalCode && $radius && isset($this->getCalculateSettings()['flatTravelTime']['amount'])) {
          $full_travel['duration'] = $this->calculateFlatTravelTime($zip_from, $zip_to);
          $this->from_zip_from_to_zip_to = $this->AbResult;
          $full_travel['distance'] = $this->from_zip_from_to_zip_to['distance'];
        }
      }
    }

    return $full_travel;
  }

  /**
   * Prepare full travel array.
   *
   * @param string $zip_from
   *   Origin zip code.
   * @param string $zip_to
   *   Destination zip code.
   * @param string $parking_address
   *   Parking_address.
   *
   * @return array
   *   Full Travel data.
   */
  private static function prepareFullTravel(string $zip_from, string $zip_to, string $parking_address) {
    return $full_travel = [
      'duration' => 0,
      'distance' => 0,
      'max_distance' => 0,
      'zipFrom' => $zip_from,
      'zipTo' => $zip_to,
      'zipParking' => $parking_address,
      'distances' => array(
        'AB' => array('duration' => 0, 'distance' => 0),
        'BC' => array('duration' => 0, 'distance' => 0),
        'CA' => array('duration' => 0, 'distance' => 0),
      ),
    ];
  }

  /**
   * Method check settings custom track speed.
   *
   * @param float $distance
   *   Distance from google.
   * @param float $duration_string
   *   Distance from google.
   *
   * @return array
   *   Custom duration.
   */
  public static function customTrackSpeed(float $distance, $duration_string) {
    $duration = round(($duration_string / 3600), 2);
    $calc_settings = json_decode(variable_get("calcsettings"));
    $customTrackSpeed = isset($calc_settings->customTrackSpeed) ? (array) $calc_settings->customTrackSpeed : array();

    // Check settings enabled, distance more then in settings.
    if (!empty($customTrackSpeed['enabled']) && isset($customTrackSpeed['useWhenDistMoreThan']) && !empty($customTrackSpeed['customSpeed'] && $distance >= $customTrackSpeed['useWhenDistMoreThan'])) {
      $duration = static::roundTimeInterval(round(($distance / $customTrackSpeed['customSpeed']), 2));
    }
    else {
      $duration = static::roundTimeInterval($duration);
    }

    return $duration;
  }

  public function getDistanceAtoB(string $zip_from, string $zip_to) {
    return static::googleRequestDistance($zip_from, $zip_to, $this->useCache);
  }

  private function checkLocalMove() {
    $is_local_move = FALSE;
    $basic_settings = json_decode(variable_get('basicsettings'), TRUE);
    $parking_address = isset($basic_settings['parking_address']) ? $basic_settings['parking_address'] : '';

    if ($parking_address) {
      if ($basic_settings['isflat_rate_miles'] && $basic_settings['flat_rate_miles']) {
        if ($this->from_office_to_zip_from['distance'] < $basic_settings['flat_rate_miles'] && $this->from_zip_to_to_office['distance'] < $basic_settings['flat_rate_miles']) {
          $is_local_move = TRUE;
        }
      }
      elseif ($basic_settings['islong_distance_miles'] && $basic_settings['long_distance_miles']) {
        if ($this->from_office_to_zip_from['distance'] < $basic_settings['long_distance_miles'] && $this->from_zip_to_to_office['distance'] < $basic_settings['long_distance_miles']) {
          $is_local_move = TRUE;
        }
      }
      else {
        $is_local_move = TRUE;
      }
    }

    return $is_local_move;
  }

  public function calculateFlatTravelTime(string $zip_from, string $zip_to) {
    $result = 0;
    $calc_settings = json_decode(variable_get('calcsettings'), TRUE);
    $isActive = isset($calc_settings['flatTravelTime']['isActive']) ? $calc_settings['flatTravelTime']['isActive'] : FALSE;
    $center_postalCode = $calc_settings['flatTravelTime']['center_postalCode'] ? $calc_settings['flatTravelTime']['center_postalCode'] : FALSE;
    $radius = $calc_settings['flatTravelTime']['radius'] ? $calc_settings['flatTravelTime']['radius'] : FALSE;

    if ($isActive && $center_postalCode && $radius && isset($calc_settings['flatTravelTime']['amount'])) {
      $from_office_to_zip_from = static::googleRequestDistance($center_postalCode, $zip_from, $this->useCache);
      $from_zip_to_to_office = static::googleRequestDistance($zip_to, $center_postalCode, $this->useCache);

      if ($from_office_to_zip_from['distance'] <= $radius) {
        $time_central_to_origin = round($calc_settings['flatTravelTime']['amount'] / 60, 2);
      }
      else {
        $time_central_to_origin = $this->from_office_to_zip_from['duration'];
      }

      if ($from_zip_to_to_office['distance'] <= $radius) {
        $time_central_to_destination = round($calc_settings['flatTravelTime']['amount'] / 60, 2);
      }
      else {
        $time_central_to_destination = $this->from_zip_to_to_office['duration'];
      }

      $result = $time_central_to_origin + $time_central_to_destination;
    }

    return self::roundTimeInterval($result);
  }

  public static function setFlatRateTime($zip_from, $zip_to) {
    $full_travel = FALSE;
    $calc_settings = json_decode(variable_get('calcsettings'), TRUE);
    if (isset($calc_settings['flatTravelTime']['isActive']) && $calc_settings['flatTravelTime']['isActive']) {
      $discount_travel_distance = isset($calc_settings['flatTravelTime']['radius']) ? $calc_settings['flatTravelTime']['radius'] : FALSE;
      $discount_travel_time = isset($calc_settings['flatTravelTime']['amount']) ? $calc_settings['flatTravelTime']['amount'] : FALSE;
      $parking_address = isset($calc_settings['flatTravelTime']['center_postalCode']) ? $calc_settings['flatTravelTime']['center_postalCode'] : FALSE;

      if ($parking_address && $discount_travel_distance && $discount_travel_time) {
        $from_office_to_zip_from = static::googleRequestDistance($parking_address, $zip_from);
        $from_zip_to_to_office = static::googleRequestDistance($zip_to, $parking_address);
        if ($from_office_to_zip_from['distance'] <= $discount_travel_distance && $from_zip_to_to_office['distance'] <= $discount_travel_distance) {
          // Set fixed discount time.
          $full_travel = $discount_travel_time;
        }
      }
    }

    return $full_travel;
  }

  public static function calculateCaliforniaFlatTime(string $zip_from) {
    static::getLocationDataByZip($zip_from);
  }

  public function travelDataByRequestType($zip_from, $zip_to, $parking_address, $move_service_type) {
    $travel_time_is_duration = $this->getCalculateSettings()['isTravelTimeSameAsDuration'];
    $this->AbResult = static::googleRequestDistance($zip_from, $zip_to, $this->useCache);
    $full_travel = array(
      'duration' => 0,
      'distance' => 0,
      'max_distance' => 0,
      'zipFrom' => $zip_from,
      'zipTo' => $zip_to,
      'zipParking' => $parking_address,
      'distances' => array(
        'AB' => $this->AbResult,
        'BC' => static::googleRequestDistance($zip_to, $parking_address, $this->useCache),
        'CA' => static::googleRequestDistance($parking_address, $zip_from, $this->useCache),
      ),
    );

    $distances = $full_travel['distances'];

    switch ($move_service_type) {
      case RequestServiceType::MOVING:
      case RequestServiceType::MOVING_AND_STORAGE:
      case RequestServiceType::OVERNIGHT:
        $this->from_office_to_zip_from = $distances['CA'];
        $this->from_zip_to_to_office = $distances['BC'];
        $this->from_zip_from_to_zip_to = $distances['AB'];
        break;

      case RequestServiceType::LOADING_HELP:
      case RequestServiceType::FLAT_RATE:
      case RequestServiceType::LONG_DISTANCE:
      case RequestServiceType::PACKING_DAY:
        $this->from_office_to_zip_from = $distances['CA'];
        $this->from_zip_to_to_office = $this->from_office_to_zip_from;
        $this->from_zip_from_to_zip_to = $this->from_office_to_zip_from;
        break;

      case RequestServiceType::UNLOADING_HELP:
        $this->from_office_to_zip_from = $distances['BC'];
        $this->from_zip_to_to_office = $this->from_office_to_zip_from;
        $this->from_zip_from_to_zip_to = $this->from_office_to_zip_from;
        break;

    }

    $full_travel['distance'] = $this->from_zip_from_to_zip_to['distance'];
    $full_travel['max_distance'] = max($distances['CA']['distance'], $distances['BC']['distance']);

    if ($move_service_type != RequestServiceType::MOVING_AND_STORAGE && $move_service_type != RequestServiceType::OVERNIGHT) {
      $min_duration = round(15 / 60, 2);

      if ($this->equalParkingAndZip($zip_from, $parking_address)) {
        if ($move_service_type == RequestServiceType::FLAT_RATE || $move_service_type == RequestServiceType::LONG_DISTANCE) {
          $this->from_office_to_zip_from['duration'] = $min_duration * 2;
        }
        else {
          $this->from_office_to_zip_from['duration'] = $min_duration;
        }
      }

      if ($this->equalParkingAndZip($zip_to, $parking_address)) {
        $this->from_zip_to_to_office['duration'] = $min_duration;
      }
    }

    $full_travel['duration'] = $travel_time_is_duration ? $this->from_zip_from_to_zip_to['duration'] : $this->from_office_to_zip_from['duration'] + $this->from_zip_to_to_office['duration'];

    return $full_travel;
  }

  public static function roundTimeInterval($time) {
    $result = 0;

    if ($time) {
      $minutes = 0;
      $hour = floor($time);
      $fraction = $time - $hour;

      if ($fraction > 0 && $fraction <= 0.3) {
        $minutes = round(15 / 60, 2);
      }
      if ($fraction > 0.3 && $fraction <= 0.55) {
        $minutes = round(30 / 60, 2);
      }
      if ($fraction > 0.55 && $fraction <= 0.8) {
        $minutes = round(45 / 60, 2);
      }
      if ($fraction > 0.8) {
        $minutes = 0;
        $hour++;
      }
      $result = $hour + $minutes;
    }

    return $result;
  }

  public function equalParkingAndZip($zip_from_or_zip_to, $parking_address) {
    return $zip_from_or_zip_to == $parking_address;
  }

  /**
   * Get distance and duration.
   *
   * @param string $zip_from
   *   Zip from.
   * @param string $zip_to
   *   Zip to.
   * @param bool $useCache
   *   Use cache or not.
   *
   * @return array
   *   Distance and duration.
   */
  public static function googleRequestDistance(string $zip_from, string $zip_to, bool $useCache = FALSE) {
    $result = [
      'distance' => 0,
      'duration' => 0,
    ];
    if (!empty($zip_from) && !empty($zip_to)) {
      if ($useCache) {
        $temp_result = static::getCache($zip_from, $zip_to);
        if (!empty($temp_result)) {
          $result['distance'] = floatval($temp_result['distance']);
          $result['duration'] = floatval($temp_result['duration']);
        }
        else {
          $result = static::sendRequestToGoogle($zip_from, $zip_to);
          static::setCache($zip_from, $zip_to, $result);
        }
      }
      else {
        $result = static::sendRequestToGoogle($zip_from, $zip_to);
      }
    }

    return $result;
  }

  /**
   * Get data from cache.
   *
   * @param string $zip_from
   *   Zip from.
   * @param string $zip_to
   *   Zip to.
   *
   * @return mixed
   *   Result.
   */
  private static function getCache(string $zip_from, string $zip_to) {
    $cache = db_select('move_distance_calculate_cache', 'mdcc');
    $cache->fields('mdcc', ['duration', 'distance']);
    $cache->condition('mdcc.from_zip', $zip_from, '=');
    $cache->condition('mdcc.to_zip', $zip_to, '=');

    return $cache->execute()->fetchAssoc(\PDO::FETCH_ASSOC);
  }

  /**
   * Set cache.
   *
   * @param string $zip_from
   *   Zip from.
   * @param string $zip_to
   *   Zip to.
   * @param array $data
   *   Distance and duration.
   */
  private static function setCache(string $zip_from, string $zip_to, array $data) {
    db_insert('move_distance_calculate_cache')
      ->fields(array(
        'from_zip' => $zip_from,
        'to_zip' => $zip_to,
        'distance' => $data['distance'],
        'duration' => $data['duration'],
      ))
      ->execute();
  }

  /**
   * Send request to google.
   *
   * @param string $zip_from
   *   Zip from.
   * @param string $zip_to
   *   Zip to.
   *
   * @return array
   *   Distance and duration.
   */
  private static function sendRequestToGoogle(string $zip_from, string $zip_to) {
    $result = array('distance' => 0, 'duration' => 0);
    $fromState = static::getStateByZip($zip_from);
    $toState = static::getStateByZip($zip_to);

    $key = variable_get('google_directions_key');
    $url = "https://maps.googleapis.com/maps/api/directions/json?origin={$fromState},{$zip_from}&destination={$toState},{$zip_to}&key={$key}";
    $raw_response = self::curlGoogleDirections($url);
    $response_data = json_decode($raw_response['data']);
    if ($response_data->status === 'OK') {
      if (!empty($response_data) && !empty($response_data->routes[0])
        && !empty($response_data->routes[0]->legs[0])
      ) {
        $distance_string = $response_data->routes[0]->legs[0]->distance->text;
        $result['distance'] = round((float) preg_replace('/,/', '', $distance_string), 2);

        $duration_string = $response_data->routes[0]->legs[0]->duration->value;
        $result['duration'] = static::customTrackSpeed($result['distance'], $duration_string);
      }
    }
    elseif (isset($response_data->status) && $response_data->status == 'NOT_FOUND') {
      watchdog('Distance calculate', "Google response: NOT_FOUND for places: $fromState - $zip_from, $toState - $zip_to");
    }
    else {
      $googleError = $response_data->error_message ?? '';
      $error_message = print_r("Google response from: $fromState - $zip_from, $toState - $zip_to . $googleError", TRUE);
      watchdog('Distance calculate', $error_message);
    }


    if ($result['distance'] == 0 && $result['duration'] == 0) {
      if (isset($raw_response)) {
        $res = print_r($raw_response, TRUE);
        watchdog('Distance calculate',
          "Google response ok. Distance and duration 0 $fromState - $zip_from, $toState - $zip_to. Response: <pre>$res</pre>");
      }
    }

    return $result;
  }

  /**
   * Google directions curl request.
   *
   * @param string $url
   *   Google url with params.
   *
   * @return array
   *   Google response.
   */
  private static function curlGoogleDirections(string $url) : array {
    $options = array(
      CURLOPT_URL => $url,
      CURLOPT_HEADER => FALSE,
      CURLOPT_RETURNTRANSFER => TRUE,
    );

    $ch = curl_init();
    curl_setopt_array($ch, $options);
    $result['data'] = curl_exec($ch);
    $result['error'] = curl_error($ch);
    curl_close($ch);

    return $result;
  }

  /**
   * Get distance price.
   *
   * @param string $zip_to
   *   Zip to.
   * @param string $state_from
   *   Zip from.
   *
   * @return int
   *   Rate.
   */
  private function getDistancePrice(string $zip_to, string $state_from) {
    $rate = 0;
    $state_from = strtoupper($state_from);
    $zip = entity_load('zip_codes', FALSE, array('zip' => $zip_to));

    if ($zip) {
      $zip = reset($zip);
      $area_to = $zip->area_code;
      $state_to = strtolower($zip->state);
      $exist_state_to = isset($this->getLongDistanceSettings()->stateRates->{$state_from}->{$state_to});
      $exist_area_to = isset($this->getLongDistanceSettings()->stateRates->{$state_from}->{$state_to}->rate->{$area_to});
      $exist_long_distance = isset($this->getLongDistanceSettings()->stateRates->{$state_from}->{$state_to}->longDistance);
      $enable_long_distance = $exist_long_distance ? $this->getLongDistanceSettings()->stateRates->{$state_from}->{$state_to}->longDistance : FALSE;
      $state_rate_to = isset($this->getLongDistanceSettings()->stateRates->{$state_from}->{$state_to}->state_rate) ?? FALSE;

      if ($exist_state_to && $enable_long_distance && $state_rate_to) {
        $rate = $this->getLongDistanceSettings()->stateRates->{$state_from}->{$state_to}->state_rate;
      }
      elseif ($exist_state_to && $enable_long_distance && $exist_area_to) {
        $rate = $this->getLongDistanceSettings()->stateRates->{$state_from}->{$state_to}->rate->{$area_to} ?? 0;
      }
    }

    return $rate;
  }

  public static function getZipByCity(string $city, $administrative_area) {
    $array_zips = entity_load('zip_codes', FALSE, array('city' => $city, 'state' => $administrative_area));
    $first_zip = reset($array_zips);
    $zip_code = $first_zip->zip;

    return $zip_code;
  }

  public static function setGoogleDirectionsKey($key) {
    variable_set('google_directions_key', $key);
  }

  public static function distanceRequest($distance, $fuel_surcharge) {
    $result = array(
      'amount' => 0,
      'isExistVariant' => FALSE,
    );

    if ($fuel_surcharge) {
      if (isset($fuel_surcharge['by_mileage'])) {
        foreach ($fuel_surcharge['by_mileage'] as $key => $value) {
          $value_from = isset($value['from']) ? (int) $value['from'] : 0;
          $value_to = isset($value['to']) ? (int) $value['to'] : 0;
          if (($value_from <= $distance) && ($value_to >= $distance)) {
            $result['amount'] = isset($value['amount']) ? round($value['amount'], 2) : 0;
            $result['isExistVariant'] = TRUE;
          }
        }
      }
    }

    return $result;
  }

  /**
   * Calculate Fuel Surcharge for long distance and local move.
   *
   * @param array $data
   *   Data for save.
   *
   * @return array
   *   Array with fuel numbers.
   */
  public function calcFuelSurcharge(array $data) : array {
    $result = array(
      'surcharge_fuel_avg' => 0,
      'surcharge_fuel_perc' => 0,
      'surcharge_fuel' => 0,
    );

    try {
      $quote = $data['quote'] ?? 0;
      $service_type = $data['serviceType'];
      $zip_from = $data['zipFrom'];
      $zip_to = $data['zipTo'];

      if (!isset($data['request_distance'])) {
        $data['request_distance'] = $this->checkTravelTime($zip_from, $zip_to, $service_type);
        $result['request_distance'] = $data['request_distance'];
      }

      $max_distance = $data['request_distance']['max_distance'];

      $basicSettings = json_decode(variable_get('basicsettings', TRUE));
      $surcharge_fuel_avg = isset($basicSettings->fuel_surcharge->def_ld) ? $basicSettings->fuel_surcharge->def_ld : 0;
      $surcharge_fuel_avg_local = isset($basicSettings->fuel_surcharge->def_local) ? $basicSettings->fuel_surcharge->def_local : 0;

      if ($service_type == RequestServiceType::LONG_DISTANCE || $service_type == RequestServiceType::FLAT_RATE) {
        if ($surcharge_fuel_avg) {
          $result['surcharge_fuel_avg'] = $surcharge_fuel_avg;
          $result['surcharge_fuel'] = round($quote * $surcharge_fuel_avg / 100, 2);
        }
        else {
          $dist_calculate = new DistanceCalculate();
          $amount = $dist_calculate->calculateFuelSurchargeByMiles($data);
          $result['surcharge_fuel_perc'] = $amount;
          $result['surcharge_fuel'] = $amount;
        }
      }
      else {
        $basicSettings = json_decode(variable_get('basicsettings', TRUE), TRUE);
        $fuel_surcharge = isset($basicSettings['fuel_surcharge']) ? $basicSettings['fuel_surcharge'] : FALSE;
        $amount_request = static::distanceRequest($max_distance, $fuel_surcharge);

        if ($amount_request['isExistVariant']) {
          $result['surcharge_fuel_perc'] = $amount_request['amount'];
          $result['surcharge_fuel'] = $amount_request['amount'];
        }
        elseif ($surcharge_fuel_avg_local) {
          $result['surcharge_fuel_avg'] = $surcharge_fuel_avg_local;
          $result['surcharge_fuel'] = round($quote * $surcharge_fuel_avg_local / 100, 2);
        }
        else {
          $dist_calculate = new DistanceCalculate();
          $amount = $dist_calculate->calculateFuelSurchargeByMiles($data);
          $result['surcharge_fuel_perc'] = $amount;
          $result['surcharge_fuel'] = $amount;
        }
      }
    }
    catch (\Throwable $t) {
      $message = "Save closing to trip Error: {$t->getMessage()} in {$t->getFile()}: {$t->getLine()} </br> Trace: {$t->getTraceAsString()}";
      watchdog('CalcFuelSurcharge error', $message, [], WATCHDOG_CRITICAL);
    }
    finally{
      return $result;
    }
  }

  public function getMaxDistance($zipFrom, $zipTo) {
    $basicSettings = json_decode(variable_get('basicsettings', TRUE));
    if ($zipFrom == $zipTo) {
      $from_office_to_zip_from = static::googleRequestDistance($basicSettings->parking_address, $zipFrom, $this->useCache);
      $result = $from_office_to_zip_from['distance'];
    }
    else {
      $from_office_to_zip_from = static::googleRequestDistance($basicSettings->parking_address, $zipFrom, $this->useCache);
      $from_zip_to_to_office = static::googleRequestDistance($basicSettings->parking_address, $zipTo, $this->useCache);

      $result = max($from_office_to_zip_from['distance'], $from_zip_to_to_office['distance']);
    }

    return $result;
  }

  /**
   * Get long distance weight.
   *
   * @param array $request_retrieve
   *   Move request data.
   * @param bool $closing
   *   Closed job or not.
   *
   * @return float
   *   Weight.
   */
  public function getLongDistanceWeight(array $request_retrieve, $closing = FALSE) : float {
    $min_weight = $this->getLongDistanceMinWeigth($request_retrieve);
    $weight = $this->getWeightByWeightType($request_retrieve, $closing);

    if (!isset($weight) || $weight == 0) {
      $template_instance = new TemplateBuilder();
      $weight = $template_instance->getCubicFeet($request_retrieve);
      $weight = $weight->weight;

      if ($request_retrieve["field_useweighttype"]["value"] == 3) {
        $weight = (float) $request_retrieve["custom_weight"]["value"];
      }
    }

    if (isset($request_retrieve["request_all_data"]) && isset($request_retrieve["request_all_data"]["min_weight"])) {
      $min_weight = (float) $request_retrieve["request_all_data"]["min_weight"];
    }

    if (!isset($request_retrieve["field_cubic_feet"]["value"])) {
      $request_retrieve["field_cubic_feet"]["value"] = ((int) $this->getLongDistanceSettings()->default_weight) == 1;
    }

    if ($min_weight > $weight) {
      $weight = ($request_retrieve["field_cubic_feet"]["value"]) ? $min_weight : $min_weight * 7;
    }

    return $weight;
  }

  public function getLongDistanceRate(array $request_retrieve, $weight = 0, $closing = FALSE) : float {
    $rate = 0;
    // TODO. Refactor this when create initialization variable BasedState.
    if (!empty($request_retrieve["field_long_distance_rate"]["value"]) && !$closing) {
      $rate = $request_retrieve["field_long_distance_rate"]["value"];
    }
    elseif ($closing && !empty($request_retrieve['request_all_data']['invoice']['field_long_distance_rate']['value'])) {
      $rate = $request_retrieve['request_all_data']['invoice']['field_long_distance_rate']['value'];
    }
    else {
      $longdistance = (object) $this->getLongDistanceSettings();
      $administrative_area_to = isset($request_retrieve["field_moving_to"]["administrative_area"]) ? $request_retrieve["field_moving_to"]["administrative_area"] : FALSE;
      $administrative_area_from = isset($request_retrieve["field_moving_from"]["administrative_area"]) ? $request_retrieve["field_moving_from"]["administrative_area"] : FALSE;
      $area_code = $request_retrieve["field_moving_to"]["premise"];

      if ($administrative_area_to && $administrative_area_from) {
        $state = strtolower($administrative_area_to);
        $baseState = strtoupper($administrative_area_from);
      }
      else {
        return 0;
      }

      // This is setting for "from state" to "to state". Example: From "MA"  to "CA".
      $settings_state = property_exists($longdistance->stateRates, $baseState) && property_exists($longdistance->stateRates->{$baseState}, $state) ?
        $longdistance->stateRates->{$baseState}->{$state} : NULL;
      if (!empty($settings_state->longDistance)|| !empty($longdistance->acceptAllQuotes)) {
        if (!empty($settings_state->state_rate) && $settings_state->state_rate > 0) {
          $rate = $settings_state->state_rate;
          // Calculate discount from front settings form.
          $discounts = $settings_state->discounts ?? NULL;
          if (!empty($discounts)) {
            foreach ($discounts as $key => $item) {
              if ($weight >= $item->startWeight) {
                $rate = (float) $item->rate;
              }
            }
          }
        }
        elseif (!empty($settings_state->rate->{$area_code}) && $settings_state->rate->{$area_code} > 0) {
          $rate = $settings_state->rate->{$area_code};
          $discounts = !empty($settings_state->areaDiscounts->{$area_code}) ? $settings_state->areaDiscounts->{$area_code} : NULL;
          if (!empty($discounts)) {
            foreach ($discounts as $key => $item) {
              if ($weight >= $item->startWeight) {
                $rate = (float) $item->rate;
              }
            }
          }
        }
      }
    }

    return $rate ?? 0;
  }

  public function getLongDistanceQuote(array $request_retrieve, bool $closing = FALSE) : float {
    $long_distance_quote = 0;

    $weight = $this->getLongDistanceWeight($request_retrieve, $closing);
    $rate = $this->getLongDistanceRate($request_retrieve, $weight, $closing);

    $longdistance_settings = (object) $this->getLongDistanceSettings();

    $administrative_area_to = $request_retrieve["field_moving_to"]["administrative_area"] ?? FALSE;
    $administrative_area_from = $request_retrieve["field_moving_from"]["administrative_area"] ?? FALSE;

    if ($administrative_area_to && $administrative_area_from) {
      $state = strtolower($administrative_area_to);
      $baseState = strtoupper($administrative_area_from);
      $all_data = $request_retrieve['request_all_data'];
      $state_settings = property_exists($longdistance_settings->stateRates, $baseState) && property_exists($longdistance_settings->stateRates->{$baseState}, $state) ?
        $longdistance_settings->stateRates->{$baseState}->{$state} : NULL;

      if (isset($all_data['min_price_enabled'])) {
        $min_price_enabled = $all_data['min_price_enabled'];
      }
      else {
        $min_price_enabled = $state_settings->minPriceEnabled ?? FALSE;
      }

      if (isset($all_data['min_price'])) {
        $min_price = $all_data['min_price'];
      }
      else {
        $min_price = $state_settings->minPrice ?? 0;
      }

      if (isset($all_data['min_weight'])) {
        $min_weight = $all_data['min_weight'];
      }
      else {
        $min_weight = $state_settings->min_weight ?? 0;
      }

      if ($min_price && $min_weight && $min_price_enabled) {
        if ($min_weight < $weight) {
          $long_distance_quote = $min_price + $rate * ($weight - $min_weight);
        }
        else {
          $long_distance_quote = $min_price;
        }
      }
      else {
        $long_distance_quote = $rate * $weight;
      }
    }

    return (float) $long_distance_quote;
  }

  public function calcInventoryTotals(array $inventory_list) : array {
    $result = [
      "counts" => 0,
      "cfs" => 0,
      "boxes" => 0,
      "custom" => 0,
    ];

    foreach ($inventory_list as $item) {
      if (isset($item["fid"])) {
        if ($item["fid"] == 14) {
          $result["boxes"] += $item["count"];
        }

        if ($item["fid"] == 23) {
          $result["custom"] += $item["count"];
        }

        $result["counts"] += $item["count"];

        if (isset($item["cf"])) {
          $result["cfs"] = $result["cfs"] + $item["cf"] * $item["count"];
        }
      }
    }

    return $result;
  }

  /**
   * Get long distance minimum weigth from settings.
   *
   * @param array $request_retrieve
   *   Request data.
   *
   * @return int
   *   Min weight.
   */
  private function getLongDistanceMinWeigth(array $request_retrieve) : int {
    $min_weight = 0;
    $to = $request_retrieve['field_moving_to'];
    $from = $request_retrieve['field_moving_from'];
    $longdistance = (object) $this->getLongDistanceSettings();
    $state = strtolower($to["administrative_area"]);
    $baseState = strtoupper($from["administrative_area"]);

    if (property_exists($longdistance->stateRates, $baseState) && property_exists($longdistance->stateRates->{$baseState}, $state)) {
      if ($longdistance->acceptAllQuotes || $longdistance->stateRates->{$baseState}->{$state}->longDistance) {
        if ($longdistance->stateRates->{$baseState}->{$state}->{'min_weight'}) {
          $min_weight = $longdistance->stateRates->{$baseState}->{$state}->{'min_weight'};
        }
      }
    }

    return $min_weight;
  }

  public function doubleTravelTime(array $data) {
    $result = NULL;

    $is_enable_double_drive_time = $this->getCalculateSettings()['doubleDriveTime'];
    $min_ca_travel_time = $this->getBasicSettings()['minCATavelTime'] / 60;
    $service_type = $data['field_move_service_type'];

    $from_post = $data['field_moving_from']['postal_code'] ?? '';
    $to_post = $data['field_moving_to']['postal_code'] ?? '';

    if ($is_enable_double_drive_time && $service_type == RequestServiceType::MOVING && $from_post && $to_post) {
      $distance = static::googleRequestDistance($from_post, $to_post, $this->useCache);
      $round_duration = self::roundTimeInterval($distance['duration']);
      $result = max($round_duration * 2, $min_ca_travel_time);
    }
    elseif ($is_enable_double_drive_time && $service_type == RequestServiceType::MOVING) {
      $result = $min_ca_travel_time;
    }

    return $result;
  }

  public function getLocalDiscountByServiceType($service_type = 1) {
    $result = 0;

    $long_distance_discount_on = $this->getBasicSettings()['longDistanceDistountOn'];
    $long_distance_discount = $this->getBasicSettings()['longDistanceDistount'];
    $local_discount_on = $this->getBasicSettings()['localDistountOn'];
    $local_discount = $this->getBasicSettings()['localDistount'];

    if ($service_type == RequestServiceType::LONG_DISTANCE || $service_type == RequestServiceType::FLAT_RATE) {
      if ($long_distance_discount_on) {
        $result = $long_distance_discount;
      }
    }
    else {
      if ($local_discount_on) {
        $result = $local_discount;
      }
    }

    return $result;
  }

  /**
   * Get request service type with 'intrastate' settings.
   *
   * @param string $zip_from
   *   Origin zip code.
   * @param string $zip_to
   *   Destination zip code.
   * @param array $parking_to_origin
   *   Google direction response.
   * @param array $parking_to_destination
   *   Google direction response.
   *
   * @return bool
   *   Flat rate or not.
   */
  private function checkFlatRate(string $zip_from, string $zip_to, array $parking_to_origin, array $parking_to_destination) : bool {
    $result = FALSE;
    $basic_settings = $this->getBasicSettings();
    $flat_rate_miles = isset($basic_settings['flat_rate_miles']) ? $basic_settings['flat_rate_miles'] : '';

    if ($basic_settings['isflat_rate_miles']) {
      if (!empty($basic_settings['flat_rate_intrastate'])) {
        $state_ori = static::getStateByZip($zip_from);
        $state_dis = static::getStateByZip($zip_to);
        $state_main = $basic_settings['main_state'];

        if ($state_main != $state_ori || $state_main != $state_dis) {
          $result = TRUE;
        }
      }
      elseif ($parking_to_origin['distance'] > $flat_rate_miles || $parking_to_destination['distance'] > $flat_rate_miles) {
        $result = TRUE;
      }
    }

    return $result;
  }

  /**
   * Get request weight by weight type.
   *
   * @param array $request_retrieve
   *   Request fields array.
   *
   * @return float
   *   Weight.
   */
  public function getWeightByWeightType(array $request_retrieve, $closing = FALSE) : float {
    $weight = 0.00;
    if ($closing && isset($request_retrieve['request_all_data']['invoice']['closing_weight']['value'])) {
      $weight = $request_retrieve['request_all_data']['invoice']['closing_weight']['value'];
    }
    else {
      if (!isset($request_retrieve["inventory_weight"]) && isset($request_retrieve["inventory"]["inventory_list"]) && $request_retrieve["inventory"]["inventory_list"]) {
        $request_retrieve["inventory_weight"] = $this->calcInventoryTotals((array) $request_retrieve["inventory"]["inventory_list"]);
      }

      switch ($request_retrieve['field_useweighttype']['value']) {
        case 1:
          $move_size = $this->getCalculateSettings()['size'][$request_retrieve['move_size']['raw']] ?? $this->getCalculateSettings()['size'][RequestSizeOfMove::ROOM_OR_LESS];
          if ($request_retrieve['move_size']['raw'] == 11 && isset($request_retrieve['commercial_extra_rooms']['value'][0])) {
            $weight = (new MoveRequest())->getCommercialItemCubicFeet($request_retrieve['commercial_extra_rooms']['value'][0]);
          }
          else {
            $total_room_weight = 0;
            foreach ($request_retrieve['rooms']['value'] as $room) {
              if (!empty($room)) {
                $total_room_weight += $this->getCalculateSettings()['room_size'][$room];
              }
            }
            $weight = $move_size + $total_room_weight + $this->getCalculateSettings()['room_kitchen'];
          }
          break;

        case 2:
          $weight = !empty($request_retrieve["inventory_weight"]["cfs"]) ? $request_retrieve["inventory_weight"]["cfs"] : 0;
          break;

        case 3:
          $weight = $request_retrieve['custom_weight']['value'];
          break;
      }
    }

    return (float) $weight;
  }

}
