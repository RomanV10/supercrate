<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_services_new\Services\ExtraServices;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class MoveRequestBalance.
 *
 * @package Drupal\move_calculator\Services
 */
class MoveRequestBalance extends MoveCalculator {

  protected $retrieveData = array();
  protected $allData = array();
  protected $closingData = array();
  protected $closingAllData = array();
  private $paymentsSum = 0.0;
  private $travelTime = 0;
  private $doubleDriveTime = 0;
  private $workTimeClosing = 0;
  private $workTimeMin = 0;
  private $workTimeMax = 0;
  protected $serviceType = RequestServiceType::MOVING;
  private $quote = 0.0;
  private $quoteMin = 0.0;
  private $quoteMax = 0.0;
  protected $calculateSettings = [];
  private $flatRateQuote = 0;
  private $packing = 0.0;
  private $packingClosing = 0.0;
  private $additionalServices = 0.0;
  private $additionalServicesClosing = 0.0;

  /**
   * MoveRequestBalance constructor.
   *
   * @param int $nid
   *   Move request id.
   */
  public function __construct(int $nid) {
    parent::__construct($nid);
  }

  private function getCalcSettingsMinHours() : int {
    return $this->calculateSettings['min_hours'] ?? 0;
  }

  /**
   * Calculate move request balance.
   *
   * @return array
   *   Balance.
   */
  public function calculateBalance(): array {
    $payments = $this->calculatePayments();
    $grand_total = $this->calculateGrandTotalOld();

    return array(
      'payments' => $payments,
      'grand_total' => $grand_total,
      'balance' => $grand_total - $payments,
    );
  }

  /**
   * Calculate move request grand total.
   */
  public function calculateGrandTotalOld() {
    if ($this->getTotalHourlyCharge()) {
      return $this->calculateTotalHourlyCharge();
    }
    else {
      switch ($this->serviceType) {
        case RequestServiceType::MOVING:
        case RequestServiceType::MOVING_AND_STORAGE:
        case RequestServiceType::LOADING_HELP:
        case RequestServiceType::UNLOADING_HELP:
        case RequestServiceType::OVERNIGHT:
        case RequestServiceType::PACKING_DAY:
          $this->quote = $this->calculateLocalQuote();
          break;

        case RequestServiceType::FLAT_RATE:
          $this->quote = $this->calculateFlatRateQuote();
          break;

        case RequestServiceType::LONG_DISTANCE:
          $this->quote = $this->calculateLongDistanceQuote();
          break;
      }
    }

    $packing = $this->calculatePacking();
    $fuel_surcharge = $this->getFuelSurcharge();
    $valuation = $this->calculateValuation();
    $additional_services = $this->calculateAdditionalServices();
    $contract_inputs = $this->calculateContractInputs();
    $tips = $this->getTips();

    $discount_fixed = $this->requestFixedDiscount();
    $discount_percent = $this->requestPercentDiscount();
    $discount_contract = $this->contractDiscount();

    return $this->quote + $packing + $fuel_surcharge + $valuation + $additional_services + $tips
      + $contract_inputs - $discount_fixed - $discount_percent - $discount_contract;
  }

  private function sumQuoteAddServicesPacking($quote) {
    return $quote + $this->additionalServices + $this->packing;
  }

  public function calculateGrandTotalMin($quote) : float {
    $fuel_surcharge = $this->getFuelSurcharge();
    $valuation = $this->calculateValuation();
    $discount_fixed = $this->requestFixedDiscount();

    //TODO надо узнать насчет fuel $fuel_surcharge. В template builder стоит проверка, что его можно и не считаь.
    $grand_total = $this->sumQuoteAddServicesPacking($quote);
    $calculate_discount_percent = $this->calculatePercentDiscount($grand_total);
    $grand_total -= $calculate_discount_percent;
    $grand_total -= $discount_fixed;
    $grand_total += $fuel_surcharge +  $valuation;
    return $grand_total;
  }

  public function calculateGrandTotalMax($quote) {
    $fuel_surcharge = $this->getFuelSurcharge();
    $valuation = $this->calculateValuation();
    $discount_fixed = $this->requestFixedDiscount();

    //TODO надо узнать насчет fuel $fuel_surcharge. В template builder стоит проверка, что его можно и не считаь.
    $grand_total = $this->sumQuoteAddServicesPacking($quote);
    $calculate_discount_percent = $this->calculatePercentDiscount($grand_total);
    $grand_total -= $calculate_discount_percent;
    $grand_total -= $discount_fixed;
    $grand_total += $fuel_surcharge +  $valuation;
    return $grand_total;
  }

  public function calculateGrandTotal($quote) {
    $packing = $this->calculatePacking();
    $fuel_surcharge = $this->getFuelSurcharge();
    $valuation = $this->calculateValuation();
    $additional_services = $this->calculateAdditionalServices();
    $discount_fixed = $this->requestFixedDiscount();

    //TODO надо узнать насчет fuel $fuel_surcharge. В template builder стоит проверка, что его можно и не считаь.
    $grand_total = $this->sumQuoteAddServicesPacking($quote);
    $calculate_discount_percent = $this->calculatePercentDiscount($grand_total);
    $grand_total -= $calculate_discount_percent;
    $grand_total -= $discount_fixed;
    $grand_total += $fuel_surcharge + $valuation;
    return $grand_total;
  }

  public function calculateGrandTotalClosing($quote) {
    $packing = $this->calculatePackingClosing();
    $fuel_surcharge = $this->getFuelSurchargeClosing();
    $valuation = $this->calculateValuation();
    $additional_services = $this->calculateAdditionalServicesClosing();
    $tips = $this->getTips();
    $discount_fixed = $this->requestFixedDiscount();
    $cash_discount = $this->getCashDiscount();
    $tax_discount = $this->getCreditTax();

    //TODO надо узнать насчет fuel $fuel_surcharge. В template builder стоит проверка, что его можно и не считаь.
    $grand_total = $quote + $additional_services + $packing - $discount_fixed;
    $calculate_discount_percent = $this->calculatePercentDiscountClosing($grand_total);
    $grand_total -= $calculate_discount_percent;
    $grand_total += $fuel_surcharge + $valuation + $tips - $cash_discount - $tax_discount;
    return $grand_total;
  }

  public function calculateGrandTotalFinal() {
    if ($this->isRequestFlatRate()) {
      $quote_min = $quote_max = $this->calculateFlatRateQuote();
      $grand_total_min = $grand_total_max = $this->calculateGrandTotal($quote_min);
    }
    elseif($this->isRequestLongDistance()) {
      $quote_min = $quote_max = $this->calculateLongDistanceQuote();
      $grand_total_min = $grand_total_max = $this->calculateGrandTotal($quote_min);
    }
    elseif ($this->isRequestLocals()) {
      $quote_min = $this->calculateLocalQuoteMin();
      $quote_max = $this->calculateLocalQuoteMax();

      $grand_total_min = $this->calculateGrandTotalMin($quote_min);
      $grand_total_max = $this->calculateGrandTotalMax($quote_max);
    }

    $result['quote_min'] = $quote_min;
    $result['quote_max'] = $quote_max;
    $result['quote_middle'] = $this->calculateEstimatedQuote($quote_min, $quote_max);

    $result['grand_total_min'] = $grand_total_min;
    $result['grand_total_max'] = $grand_total_max;
    $result['grand_total_middle'] = $this->calculateGrandTotalEstimated($grand_total_min, $grand_total_max);

    return $result;
  }

  public function calculateGrandTotalFinalClosing() {
    if (!empty($this->closingData)) {
      if ($this->isRequestFlatRate()) {
        $result['quote_closing']  = $quote = $this->calculateFlatRateQuoteClosing();
      }
      elseif($this->isRequestLongDistance()) {
        $result['quote_closing']  = $quote = $this->calculateLongDistanceQuoteClosing();
      }
      elseif ($this->isRequestLocals()) {
        $result['quote_closing']  = $quote = $this->calculateLocalQuoteClosing();
      }
      $result['grand_total_closing']  = $grand_total_closing = $this->calculateGrandTotalClosing($quote);
    }

    return $result ?? [];
  }

  /**
   * This middle value between min and max grand total.
   *
   * @param float $grand_total_min
   *   Minimum grand total value.
   * @param float $grand_total_max
   *   Maximum grand total value.
   *
   * @return float
   *   Grand total value.
   */
  public function calculateGrandTotalEstimated(float $grand_total_min, float $grand_total_max) : float {
    return ($grand_total_max + $grand_total_min) / 2;
  }

  private function calculateTotalHourlyCharge() {
    $this->quote = $this->allData['totalHourlyCharge'];

    if ($this->serviceType != RequestServiceType::FLAT_RATE) {
      $packing = $this->calculatePacking();
      $fuel_surcharge = $this->getFuelSurcharge();
      $valuation = $this->calculateValuation();
      $additional_services = $this->calculateAdditionalServices();
      $contract_inputs = $this->calculateContractInputs();
      $tips = $this->getTips();
    }
    else {
      $packing = $this->calculatePacking();
      $fuel_surcharge = 0;
      $valuation = $this->calculateValuation();
      $additional_services = 0;
      $contract_inputs = $this->calculateContractInputs();
      $tips = 0;
    }

    $discount_fixed = $this->requestFixedDiscount();
    $discount_percent = $this->requestPercentDiscount();
    $discount_contract = $this->contractDiscount();

    return $this->quote + $packing + $fuel_surcharge + $valuation + $additional_services + $tips
      + $contract_inputs - $discount_fixed - $discount_percent - $discount_contract;
  }

  private function calculateFlatRateQuote() {
    return $this->flatRateQuote = $this->retrieveData['flat_rate_quote']['value'] ?? 0;
  }

  private function calculateFlatRateQuoteClosing() {
    return $this->closingData['flat_rate_quote']['value'] ?? $this->calculateFlatRateQuote();
  }

  private function calculateLongDistanceQuoteClosing() {
    return (new DistanceCalculate())->getLongDistanceQuote($this->retrieveData, TRUE);
  }


  private function calculateLongDistanceQuote() {
    return (new DistanceCalculate())->getLongDistanceQuote($this->retrieveData);
  }

  private function getLocalRate() : float {
    return $this->retrieveData['rate']['value'] ?? 0;
  }

  // TODO Надо проверить что где и как записывается для closing. Могут ли значения отсутсвовать.
  private function getLocalRateClosing() : float {
    return $this->closingData['rate']['value'] ?? $this->getLocalRate();
  }

  private function calculateTotalTime() {
    $travel_time = $this->checkDoubleDriveTime() ? $this->getDoubleDriveTime() : $this->getTravelTime();
    $work_time = $this->getWorkTimeClosing();
    $total_time = $travel_time + $work_time;

    return $this->getCalcSettingsMinHours() > $total_time ? $this->getCalcSettingsMinHours() : $total_time;
  }

  private function calculateTotalTimeClosing() {
    $travel_time = $this->checkDoubleDriveTimeClosing() ? $this->getDoubleDriveTimeClosing() : $this->getTravelTimeClosing();
    $work_time = $this->getWorkTimeClosing();
    $total_time = $travel_time + $work_time;

    return $this->getCalcSettingsMinHours() > $total_time ? $this->getCalcSettingsMinHours() : $total_time;
  }

  private function calculateTotalTimeMin() : float {
    $travel_time = $this->checkDoubleDriveTime() ? $this->getDoubleDriveTime() : $this->getTravelTime();
    $this->workTimeMin = $this->getWorkTimeMin() + $travel_time;

    if ($this->workTimeMin < $this->getCalcSettingsMinHours()) {
      $this->workTimeMin = $this->getCalcSettingsMinHours();
    }

    return $this->workTimeMin = DistanceCalculate::roundTimeInterval($this->workTimeMin);
  }

  private function calculateTotalTimeMax() : float {
    $travel_time = $this->checkDoubleDriveTime() ? $this->getDoubleDriveTime() : $this->getTravelTime();
    $this->workTimeMax = $this->getWorkTimeMax() + $travel_time;

    if ($this->workTimeMax < $this->getCalcSettingsMinHours()) {
      $this->workTimeMax = $this->getCalcSettingsMinHours();
    }

    return $this->workTimeMax = DistanceCalculate::roundTimeInterval($this->workTimeMax);
  }

  private function getTravelTime() {
    return $this->travelTime = $this->checkTravelTimeOriginToDestination() ? $this->retrieveData['travel_time']['raw'] : 0;
  }

  private function getTravelTimeClosing() {
    return $this->travelTime = $this->checkTravelTimeOriginToDestinationClosing() ? $this->closingData['travel_time']['raw'] : 0;
  }

  /**
   * Check if enable travel time from origin to destination in settings.
   *
   * @return bool
   *   Enable or not.
   */
  private function checkTravelTimeOriginToDestination() : bool {
    return $this->allData['travelTime'] ?? $this->calculateSettings['travelTime'];
  }

  private function checkTravelTimeOriginToDestinationClosing() : bool {
    return $this->closingAllData['travelTime'] ?? $this->calculateSettings['travelTime'];
  }

  private function checkDoubleDriveTime() {
    return $this->retrieveData['field_double_travel_time']['raw'] == 1;
  }

  private function checkDoubleDriveTimeClosing() {
    return $this->closingData['field_double_travel_time']['raw'] == 1;
  }

  private function getDoubleDriveTime() {
    return $this->doubleDriveTime = $this->retrieveData['field_double_travel_time']['raw'];
  }

  private function getDoubleDriveTimeClosing() {
    return $this->doubleDriveTime = $this->closingData['field_double_travel_time']['raw'];
  }

  private function getWorkTimeClosing() : float {
    return $this->workTimeClosing = $this->closingData['work_time_int'] ?? 0;
  }

  private function getWorkTimeMin() : float {
    return  $this->retrieveData['minimum_time']['raw'] ?? 0;
  }

  private function getWorkTimeMax() : float {
    return $this->workTimeMax = $this->retrieveData['maximum_time']['raw'] ?? 0;
  }

  private function getTotalHourlyCharge() {
    return $this->allData['totalHourlyCharge'] ?? 0;
  }

  private function calculateLocalQuoteClosing() {
    $rate = $this->getLocalRateClosing();
    $time = $this->calculateTotalTimeClosing();

    return $this->quote = $rate * $time;
  }

  private function calculateLocalQuoteMin() {
    $rate = $this->getLocalRate();
    $time = $this->calculateTotalTimeMin();

    return $this->quoteMin = $rate * $time;
  }

  private function calculateLocalQuoteMax() {
    $rate = $this->getLocalRate();
    $time = $this->calculateTotalTimeMax();

    return $this->quoteMax = $rate * $time;
  }

  /**
   * Calculate middle quote.
   *
   * @param float $min_quote
   *   Min quote value.
   * @param float $max_quote
   *   Max quote value.
   *
   * @return float
   *   Middle value.
   */
  private function calculateEstimatedQuote(float $min_quote, float $max_quote) : float {
    return ($max_quote + $min_quote) / 2;
  }

  /**
   * Calculate sum payments for move request.
   *
   * @return float
   *   Sum.
   */
  private function calculatePayments(): float {
    $payments_sum = 0;
    $receipts = MoveRequest::getReceipt($this->nid);

    foreach ($receipts as $receipt) {
      if (empty($receipt['pending'])) {
        $payments_sum += $receipt['amount'];
      }
      if (!empty($receipt['refunds'])) {
        $payments_sum -= $receipt['amount'];
      }
    }

    return $this->paymentsSum = $payments_sum;
  }

  private function requestPercentDiscount() {
    return $this->quote * $this->allData['add_percent_discount'] / 100 ?? 0;
  }

  private function getPercentDiscount() : float {
    return $this->allData['add_percent_discount'] ?? 0;
  }

  private function getPercentDiscountClosing() : float {
    return $this->closingAllData['add_percent_discount'] ?? 0;
  }

  private function getCashDiscount() : float {
    return $this->allData['cashDiscount'] ?? 0;
  }

  private function getCreditTax() : float {
    return $this->allData['creditTax'] ?? 0;
  }

  private function calculatePercentDiscount($quote) {
    $discount = $this->getPercentDiscount();
    return $quote * $discount / 100 ?? 0;
  }

  private function calculatePercentDiscountClosing($quote) {
    $discount = $this->getPercentDiscountClosing();
    return $quote * $discount / 100 ?? 0;
  }

  private function requestFixedDiscount() {
    return $this->allData['add_money_discount'] ?? 0;
  }

  private function contractDiscount() {
    return $this->closingData['discount'] ?? 0;
  }

  private function calculateContractInputs() {
    $contract_inputs = $this->allData['contractInputs'] ?? array();
    return array_sum($contract_inputs);
  }

  private function getFuelSurchargeClosing() : float {
    return (float) $this->closingAllData["surcharge_fuel"] ?? 0;
  }

  private function getFuelSurcharge() : float {
    return (float) $this->allData["surcharge_fuel"] ?? 0;
  }

  // TODO check if exist for closing and sales.
  private function calculateValuation() {
    return (new TemplateBuilder())->getValuationCharge($this->retrieveData['request_data']);
  }

  private function calculatePacking(): float {
    return $this->packing = (new TemplateBuilder())->calculatePackingServices($this->retrieveData);
  }

  private function calculatePackingClosing(): float {
    return (new TemplateBuilder())->calculatePackingServices($this->retrieveData, TRUE);
  }

  private function calculateAdditionalServices() {
    $extraServicesCalculator = (new MoveRequestExtraService())->setRequestRetrieve($this->retrieveData)->build()->calculate();
    return $this->additionalServices = $extraServicesCalculator->extraServiceTotal;
  }

  private function calculateAdditionalServicesClosing() {
    $extraServicesCalculator = (new MoveRequestExtraService())->setRequestRetrieve($this->retrieveData)->build()->calculate();
    return $this->additionalServicesClosing = $extraServicesCalculator->extraServiceTotalClosing;
  }

  private function getTips() {
    return $this->retrieve['contract_info'][0]['tips_total'] ?? $this->allData['req_tips'];
  }

  /**
   * Get all completed move request from search_api.
   *
   * @return array
   *   Requests info.
   *
   * @throws \SearchApiException
   */
  public static function getAllCompletedRequest() {
    $query = search_api_query('move_request');
//    $query->condition('field_company_flags', array(3));
//    $query->condition('field_move_service_type', 3, '<>');
    $result = $query->execute();

    return $result;
  }

//  public function setBalance(int $nid) {
//    $node = node_load($nid);
//    $node->field_balance = ;
//    node_save($node);
//  }

}