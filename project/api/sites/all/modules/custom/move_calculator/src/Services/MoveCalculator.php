<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\move_request\MoveRequestRetrieve;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class MoveCalculator.
 *
 * @package Drupal\move_distance_calculate\Services
 */
abstract class MoveCalculator {
  protected $allData = [];
  protected $closingData = [];
  protected $closingAllData = [];
  protected $calculateSettings = [];
  protected $retrieveData = [];
  protected $serviceType = RequestServiceType::MOVING;
  protected $nid = 0;

  public $serviceTypeMethodNames = [
    RequestServiceType::MOVING => 'localCalculation',
    RequestServiceType::MOVING_AND_STORAGE => 'localCalculation',
    RequestServiceType::LOADING_HELP => 'localCalculation',
    RequestServiceType::UNLOADING_HELP => 'localCalculation',
    RequestServiceType::OVERNIGHT => 'localCalculation',
    RequestServiceType::PACKING_DAY => 'localCalculation',
    RequestServiceType::LONG_DISTANCE => 'longDistanceCalculation',
    RequestServiceType::FLAT_RATE => 'flatRateCalculation',

  ];

  public function setRequestRetrieve(array $request_retrieve){
    $this->retrieveData = $request_retrieve;
    return $this;
  }

  public function getRequestRetrieve() {
    return $this->retrieveData;
  }

  public function setRequestRetrieveByNid(int $nid) {
    $this->retrieveData = (new MoveRequest($nid))->retrieve();
    return $this;
  }

  public function setRequestRetrieveByNode($node) {
    $this->retrieveData = (new MoveRequestRetrieve(entity_metadata_wrapper('node', $node)))->nodeFields();
    return $this;
  }

  public function setRequestRetrieveByWrapper($wrapper) {
    $this->retrieveData = (new MoveRequestRetrieve($wrapper))->nodeFields();
    return $this;
  }

  public function build() {
    $this->nid = $this->retrieveData['nid'];
    $this->allData = $this->retrieveData['request_all_data'];

    $this->closingData = $this->allData['invoice'] ?? [];
    $this->closingAllData = $this->allData['invoice']['request_all_data'] ?? [];

    $this->serviceType = $this->retrieveData['service_type']['raw'];
    $this->calculateSettings = json_decode(variable_get('calcsettings', ''), TRUE);
    return $this;
  }

  public function calculate() {
    $this->build();
    return $this;
  }

  public function getServiceNameMethod() {
    // [service_type][row] = NULL default localCalculation.
    return $this->serviceTypeMethodNames[$this->serviceType] ?? $this->serviceTypeMethodNames[RequestServiceType::MOVING];
  }

  public function flatRateCalculation(){}
  public function longDistanceCalculation(){}
  public function localCalculation(){}


}
