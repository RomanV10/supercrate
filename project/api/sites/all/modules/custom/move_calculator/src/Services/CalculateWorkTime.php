<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_template_builder\Services\TemplateBuilder;
use Drupal\move_services_new\Services\move_request\MoveRequest;

/**
 * Class CalculateWorkTime.
 *
 * @package Drupal\move_services_new\Services
 */
class CalculateWorkTime extends MoveCalculator {

  const NO_STAIRS_GROUND_FLOOR = 1;
  const STAIRS_2ND_FLOOR = 2;
  const STAIRS_3ND_FLOOR = 3;
  const STAIRS_4ND_FLOOR = 4;
  const STAIRS_5ND_FLOOR = 5;
  const ELEVATOR = 6;
  const PRIVATE_HOUSE = 7;

  const WT_CALCULATOR = 1;
  const WT_INVENTORY = 2;
  const WT_CUSTOM = 3;

  public $movers_count = 0;
  private $dispertion = 50;
  public $distanceCalculate;
  public $templateBuilder;
  public $moveRequest;
  private $calcSettings = [];
  private $request = [];
  private $params = [
    'service_type' => 0,
    'distance' => 0.00,
    'duration' => 0.00,
    'type_from' => 1,
    'type_to' => 1,
    'rooms' => [],
    'move_size' => 0,
    'field_useweighttype' => 1,
    'request_all_data' => [],
    'field_moving_from' => [],
    'field_moving_to' => [],
  ];

  /**
   * CalculateWorkTime constructor.
   */
  public function __construct() {
    $this->calcSettings = json_decode(variable_get('calcsettings', ''), TRUE) ?? array();
    $this->distanceCalculate = new DistanceCalculate();
    $this->templateBuilder = new TemplateBuilder();
    $this->moveRequest = new MoveRequest();
  }

  /**
   * Setter for calcSettings.
   *
   * @param array $calc_settings
   *   calcSettings variable array.
   */
  public function setCalcSettings(array $calc_settings) : void {
    $this->calcSettings = $calc_settings;
  }

  /**
   * Create work time from request.
   *
   * @param array $request
   *   Request from MoveRequestRetrieve.
   *
   * @return array
   *   Array with min and max time.
   */
  public function buildWorkTimeFromRequest(array $request) : array {
    $result = [];
    if (!empty($request)) {
      $this->request = $request;
      $params['service_type'] = (int) $request['service_type']['raw'];
      $params['distance'] = (float) $request['distance']['value'] ?? 0;
      $params['duration'] = (float) $request['duration']['value'] ?? 0;
      $params['type_from'] = (int) $request['type_from']['raw'] ?? 1;
      $params['type_to'] = (int) $request['type_to']['raw'] ?? 1;
      $params['rooms'] = $request['rooms']['value'];
      $params['move_size'] = (int) $request['move_size']['raw'];
      $params['weight_type'] = (int) $request['field_useweighttype']['value'] ?? 1;
      $params['field_moving_from'] = $request['field_moving_from'] ?? [];
      $params['field_moving_to'] = $request['field_moving_to'] ?? [];
      $params['field_cubic_feet'] = $request['field_cubic_feet'] ?? FALSE;
      $params['request_all_data'] = $request['request_all_data'] ?? [];

      $result = $this->buildWorkTime($params);
    }
    return $result;
  }

  /**
   * Calculate work time by request nid.
   *
   * @param int $nid
   *   Request nid.
   *
   * @return array
   *   Array with max and min time.
   */
  public function buildWorkTimeByNid(int $nid) : array {
    $this->moveRequest = (new MoveRequest($nid));
    $request = $this->moveRequest->retrieve();
    return $this->buildWorkTimeFromRequest($request);
  }

  /**
   * Calculate work time.
   *
   * @param array $params
   *   Params to build work time.
   *
   * @return array
   *   Array with max and min work time.
   */
  public function buildWorkTime(array $params) : array {
    $params = array_merge($this->params, $params);
    $weight = $this->moveRequest->getRequestWeight($this->request);
    $this->movers_count = $this->getMovers($params['type_from'], $params['type_to'], $weight);
    $duration = $this->buildDuration($params['service_type'], $params['distance'], $params['duration']);

    $total = [];

    if ($params['service_type'] == RequestServiceType::FLAT_RATE || $params['service_type'] == RequestServiceType::LONG_DISTANCE) {
      $params['type_to'] = static::NO_STAIRS_GROUND_FLOOR;
    }

    // Convert ENTRANCE from.
    if (empty($params['type_from']) || $params['type_from'] == static::PRIVATE_HOUSE) {
      $type_from = static::NO_STAIRS_GROUND_FLOOR;
    }
    else {
      $type_from = $params['type_from'];
    }

    // Convert ENTRANCE to.
    if (empty($params['type_to']) ||
      $params['type_to'] == static::PRIVATE_HOUSE ||
      $params['service_type'] == RequestServiceType::FLAT_RATE ||
      $params['service_type'] == RequestServiceType::LONG_DISTANCE) {
      $type_to = static::NO_STAIRS_GROUND_FLOOR;
    }
    else {
      $type_to = $params['type_to'];
    }

    // Type from or to by service type.
    $entrance = [
      RequestServiceType::LOADING_HELP  => $type_from,
      RequestServiceType::UNLOADING_HELP => $type_to,
    ];

    // Coefficient to calculate min and max time.
    $kof = [
      RequestServiceType::LOADING_HELP  => 0.6,
      RequestServiceType::UNLOADING_HELP => 0.4,
    ];

    if ($params['service_type'] == RequestServiceType::LOADING_HELP || $params['service_type'] == RequestServiceType::UNLOADING_HELP) {
      $total = $this->getMinMaxTime($weight, $this->movers_count, $entrance[$params['service_type']], $kof[$params['service_type']]);
      $total['min'] = $this->getRoundedTime($total['min']);
      $total['max'] = $this->getRoundedTime($total['max']);
    }
    else {
      $loading_time = $this->getMinMaxTime($weight, $this->movers_count, $entrance[RequestServiceType::LOADING_HELP], $kof[RequestServiceType::LOADING_HELP]);
      $unloading_time = $this->getMinMaxTime($weight, $this->movers_count, $entrance[RequestServiceType::UNLOADING_HELP], $kof[RequestServiceType::UNLOADING_HELP]);

      $total['min'] = $this->getRoundedTime($loading_time['min'] + $unloading_time['min']);
      $total['max'] = $this->getRoundedTime($loading_time['max'] + $unloading_time['max']);
    }

    if (!$this->calcSettings['doubleDriveTime'] && !$this->calcSettings['isTravelTimeSameAsDuration']) {
      $total['min'] = $this->getRoundedTime($total['min'] + $duration);
      $total['max'] = $this->getRoundedTime($total['max'] + $duration);
    }

    return $total;
  }

  /**
   * Calculate min and max time for loading or unloading only.
   *
   * @param float $weight
   *   Weight number.
   * @param int $movers_count
   *   Movers count number.
   * @param int $entrance
   *   Type from or type to.
   * @param float $kof
   *   Coefficient to calculate min and max time.
   *
   * @return array
   *   Min and max time.
   */
  public function getMinMaxTime(float $weight, int $movers_count, int $entrance, float $kof) : array {
    $speed = $this->calcSettings['speed'][$movers_count] * (1 - $this->calcSettings['floor_kof'][$entrance] / 100);
    $result['min'] = (($weight - $this->dispertion) / $speed) * $kof;
    $result['max'] = (($weight + $this->dispertion) / $speed) * $kof;
    return $result;
  }

  /**
   * Build duration.
   *
   * @param int $service_type
   *   Request service type.
   * @param float $distance
   *   Request distance.
   * @param float $duration
   *   Request current duration.
   *
   * @return float
   *   Duration number.
   */
  public function buildDuration(int $service_type, float $distance, float $duration = 0.00) : float {
    $settings = json_decode(variable_get('basicsettings', ''), TRUE) ?? array();
    if (($distance > $settings['local_flat_miles'] && $service_type != RequestServiceType::MOVING_AND_STORAGE) ||
      $service_type == RequestServiceType::FLAT_RATE
    ) {
      $duration = 0;
    }

    return $duration;
  }

  /**
   * Calculate movers count.
   *
   * @param int $type_from
   *   Request type from.
   * @param int $type_to
   *   Request type to.
   * @param float $weight
   *   Requets weight.
   *
   * @return int
   *   Movers count.
   */
  public function getMovers(int $type_from, int $type_to, float $weight) : int {
    // IF MOVE SIZE IS HOME ENTRANCE TYPE_FROM same as 1 FLOOR.
    $type_from = ($type_from == static::PRIVATE_HOUSE) ? static::NO_STAIRS_GROUND_FLOOR : $type_from;
    $type_to = ($type_to == static::PRIVATE_HOUSE) ? static::NO_STAIRS_GROUND_FLOOR : $type_to;

    // Default value for movers count is '2'.
    $movers_count = (int) $this->calcSettings['min_movers'] ?? 2;

    // GET HOW MANY MOVERS.
    for ($i = 3; $i <= 8; $i++) {
      if ($weight >= $this->calcSettings['mover_size'][$i]) {
        $movers_count = $i;
      }
      else {
        if ($weight >= $this->calcSettings['mover_size_with_floor'][$i]
          && ($type_from >= $this->calcSettings['mover_size_floor'][$i] || $type_to >= $this->calcSettings['mover_size_floor'][$i])) {
          $movers_count = $i;
        }
      }
    }
    return $movers_count;
  }

  /**
   * Round time.
   *
   * @param float $time
   *   Time decimal.
   *
   * @return float
   *   Rounded decimal time.
   */
  public function getRoundedTime(float $time) : float {
    return DistanceCalculate::roundTimeInterval($time);
  }

}
