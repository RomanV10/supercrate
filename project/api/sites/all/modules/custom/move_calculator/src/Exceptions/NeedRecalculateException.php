<?php

namespace Drupal\move_calculator\Exceptions;

/**
 * Class NeedRecalculateException.
 *
 * @package Drupal\move_calculator\Exceptions
 */
class NeedRecalculateException extends \Exception {

}
