<?php

namespace Drupal\move_calculator\Tasks;

use Carbon\Carbon;
use Drupal\move_calculator\Exceptions\NeedRecalculateException;
use Drupal\move_services_new\Services\ExtraServices;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class CheckChangedFields.
 *
 * @package Drupal\move_calculator\Tasks
 */
class CheckChangedFields {

  /**
   * Check double travel time.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingDoubleTravelTime(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['field_double_travel_time']['raw'])) {
      if (isset($oldAllData['invoice']['field_double_travel_time']['raw'])) {
        if ($oldAllData['invoice']['field_double_travel_time']['raw'] != $newAllData['invoice']['field_double_travel_time']['raw']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check travel time.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingTravelTime(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['travel_time']['raw'])) {
      if (isset($oldAllData['invoice']['travel_time']['raw'])) {
        if ($oldAllData['invoice']['travel_time']['raw'] != $newAllData['invoice']['travel_time']['raw']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check work time int.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingWorkTime(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['work_time_int'])) {
      if (isset($oldAllData['invoice']['work_time_int'])) {
        if ($oldAllData['invoice']['work_time_int'] != $newAllData['invoice']['work_time_int']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check rate discount.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function addRateDiscount(array $oldAllData, array $newAllData) {
    if (isset($newAllData['add_rate_discount'])) {
      if (isset($oldAllData['add_rate_discount'])) {
        if ($oldAllData['add_rate_discount'] != $newAllData['add_rate_discount']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check closing rate value.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingRate(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['rate']['value'])) {
      if (isset($oldAllData['invoice']['rate']['value'])) {
        if ($oldAllData['invoice']['rate']['value'] != $newAllData['invoice']['rate']['value']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check flat rate quote.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingFlatRateQuote(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['flat_rate_quote']['value'])) {
      if (isset($oldAllData['invoice']['flat_rate_quote']['value'])) {
        if ($oldAllData['invoice']['flat_rate_quote']['value'] != $newAllData['invoice']['flat_rate_quote']['value']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check rate discount expiration.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function rateDiscountExpiration(array $oldAllData, array $newAllData) {
    if (isset($newAllData['add_rate_discount_expiration'])) {
      if (isset($oldAllData['add_rate_discount_expiration'])) {
        $oldDate = Carbon::createFromTimestamp(strtotime($oldAllData['add_rate_discount_expiration']));
        $newDate = Carbon::createFromTimestamp(strtotime($newAllData['add_rate_discount_expiration']));
        if ($oldDate->diffInDays($newDate) >= 1) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check request tis.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function reqTips(array $oldAllData, array $newAllData) {
    if (isset($newAllData['req_tips'])) {
      if (isset($oldAllData['req_tips'])) {
        if ($oldAllData['req_tips'] != $newAllData['req_tips']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check packing total.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   * @param array $request
   *   Request.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingRequestData(array $oldAllData, array $newAllData, array $request) {
    if (isset($newAllData['invoice']['request_data'])) {
      if (isset($oldAllData['invoice']['request_data'])) {
        $type_service = in_array($request["service_type"]["raw"], [RequestServiceType::FLAT_RATE, RequestServiceType::LONG_DISTANCE]);
        $templateBuilder = new TemplateBuilder();
        $totalOld = $templateBuilder->getPackingTotal($oldAllData['invoice']['request_data'], $type_service);
        $totalNew = $templateBuilder->getPackingTotal($newAllData['invoice']['request_data'], $type_service);
        if ($totalOld != $totalNew) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check closing valuation charge.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingValuationCharge(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['request_all_data']['valuation']['selected']['valuation_charge'])) {
      if (isset($oldAllData['invoice']['request_all_data']['valuation']['selected']['valuation_charge'])) {
        if ($oldAllData['invoice']['request_all_data']['valuation']['selected']['valuation_charge'] != $newAllData['invoice']['request_all_data']['valuation']['selected']['valuation_charge']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check surcharge fuel.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingSurchargeFuel(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['request_all_data']['surcharge_fuel'])) {
      if (isset($oldAllData['invoice']['request_all_data']['surcharge_fuel'])) {
        if ($oldAllData['invoice']['request_all_data']['surcharge_fuel'] != $newAllData['invoice']['request_all_data']['surcharge_fuel']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check money discount.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingAddMoneyDiscount(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['request_all_data']['add_money_discount'])) {
      if (isset($oldAllData['invoice']['request_all_data']['add_money_discount'])) {
        if ($newAllData['invoice']['request_all_data']['add_money_discount'] != $oldAllData['invoice']['request_all_data']['add_money_discount']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check cash discount.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingCashDiscount(array $oldAllData, array $newAllData) {
    if (isset($newAllData['cashDiscount'])) {
      if (isset($oldAllData['cashDiscount'])) {
        if ($newAllData['cashDiscount'] != $oldAllData['cashDiscount']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check closing cc discount on contract.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingCreditTax(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['request_all_data']['creditTax'])) {
      if (isset($oldAllData['invoice']['request_all_data']['creditTax'])) {
        if ($newAllData['invoice']['request_all_data']['creditTax'] != $oldAllData['invoice']['request_all_data']['creditTax']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check cc discount.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function creditTax(array $oldAllData, array $newAllData) {
    if (isset($newAllData['creditTax'])) {
      if (isset($oldAllData['creditTax'])) {
        if ($newAllData['creditTax'] != $oldAllData['creditTax']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check closing discount.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingDiscount(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['discount'])) {
      if (isset($oldAllData['invoice']['discount'])) {
        if ($newAllData['invoice']['discount'] != $oldAllData['invoice']['discount']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check custom payments.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function customPayments(array $oldAllData, array $newAllData) {
    if (isset($newAllData['customPayments'])) {
      if (isset($oldAllData['customPayments'])) {
        $newPayments = array_sum(array_column($newAllData['customPayments'], 'value'));
        $oldPayments = array_sum(array_column($oldAllData['customPayments'], 'value'));
        if ($newPayments != $oldPayments) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check contract input.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function contractInputs(array $oldAllData, array $newAllData) {
    $calcInputs = function ($inputs) {
      $sum = 0;
      foreach ($inputs as $inputValue) {
        $sum += $inputValue;
      }

      return $sum;
    };

    if (isset($newAllData['contractInputs'])) {
      if (isset($oldAllData['contractInputs'])) {
        if ($calcInputs($newAllData['contractInputs']) != $calcInputs($oldAllData['contractInputs'])) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check closing percent discount.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingAddPercentDiscount(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['request_all_data']['add_percent_discount'])) {
      if (isset($oldAllData['invoice']['request_all_data']['add_percent_discount'])) {
        if ($newAllData['invoice']['request_all_data']['add_percent_discount'] != $oldAllData['invoice']['request_all_data']['add_percent_discount']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check valuation.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function valuationSelectedCharge(array $oldAllData, array $newAllData) {
    if (isset($newAllData['valuation']['selected']['valuation_charge'])) {
      if (isset($oldAllData['valuation']['selected']['valuation_charge'])) {
        if ($newAllData['valuation']['selected']['valuation_charge'] != $oldAllData['valuation']['selected']['valuation_charge']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check closing weight.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function closingWeight(array $oldAllData, array $newAllData) {
    if (isset($newAllData['invoice']['closing_weight']['value'])) {
      if (isset($oldAllData['invoice']['closing_weight']['value'])) {
        if ($newAllData['invoice']['closing_weight']['value'] != $oldAllData['invoice']['closing_weight']['value']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check min weight.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function minWeight(array $oldAllData, array $newAllData) {
    if (isset($newAllData["min_weight"])) {
      if (isset($oldAllData["min_weight"])) {
        if ($newAllData["min_weight"] != $oldAllData["min_weight"]) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check min price setting.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function minPriceEnabled(array $oldAllData, array $newAllData) {
    if (isset($newAllData['min_price_enabled'])) {
      if (isset($oldAllData['min_price_enabled'])) {
        if ($newAllData['min_price_enabled'] != $oldAllData['min_price_enabled']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Check min price.
   *
   * @param array $oldAllData
   *   Old allData.
   * @param array $newAllData
   *   New allData.
   *
   * @throws \Drupal\move_calculator\Exceptions\NeedRecalculateException
   */
  public static function minPrice(array $oldAllData, array $newAllData) {
    if (isset($newAllData['min_price'])) {
      if (isset($oldAllData['min_price'])) {
        if ($newAllData['min_price'] != $oldAllData['min_price']) {
          throw new NeedRecalculateException();
        }
      }
      else {
        throw new NeedRecalculateException();
      }
    }
  }

  /**
   * Get needed field.
   *
   * @param array $request
   *   Request field.
   *
   * @return array
   *   Needed field.
   */
  public static function getNeedField(array $request) {
    $need = [
      'field_double_travel_time' => '',
      'field_travel_time' => '',
      'field_minimum_move_time' => '',
      'field_maximum_move_time' => '',
      'field_price_per_hour' => '',
      'field_movers_count' => '',
      'field_list_truck' => '',
      'field_estimated_prise' => '',
      'field_long_distance_rate' => '',
      'field_moving_to' => '',
      'field_move_service_type' => '',
      'field_size_of_move' => '',
      'commercial_extra_rooms' => '',
      'field_extra_furnished_rooms' => '',
      'field_customweight' => '',
      'field_cubic_feet' => '',
    ];

    return array_intersect_key($request, $need);
  }

  /**
   * Check field field_moving_to.
   *
   * @param \EntityMetadataWrapper $wrapper
   *   Node wrapper.
   * @param array $value
   *   New value.
   *
   * @return bool
   *   Change field or not.
   */
  public static function checkFieldMovingTo(\EntityMetadataWrapper $wrapper, array $value) {
    if (!isset($value['administrative_area']) || $wrapper->field_moving_to->administrative_area->value() != $value['administrative_area']) {
      return TRUE;
    }
    if (!isset($value['premise']) || $wrapper->field_moving_to->premise->value() != $value['premise']) {
      return TRUE;
    }

    return FALSE;
  }

}
