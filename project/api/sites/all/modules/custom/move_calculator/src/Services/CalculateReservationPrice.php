<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_services_new\Services\Settings;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class CalculateReservationPrice.
 *
 * @package Drupal\move_calculator\Services
 */
class CalculateReservationPrice extends MoveCalculator {
  public $date;
  public $timeZone = 'UTC';
  public $settings = array();
  public $requestServiceType;
  public $requestType;
  public $requestTypeRate;
  public $rateType = 1;
  public $moveType = 0;
  public $types = [
    RequestServiceType::MOVING => ['request_type' => 'localReservation', 'request_type_rate' => 'localReservationRate'],
    RequestServiceType::FLAT_RATE => ['request_type' => 'flatReservation', 'request_type_rate' => 'flatReservationRate'],
    RequestServiceType::LONG_DISTANCE => ['request_type' => 'longReservation', 'request_type_rate' => 'longReservationRate'],
  ];

  /**
   * CalculateReservationPrice constructor.
   *
   * @param string $date
   *   Date to convert in timestamp.
   * @param string $request_type
   *   Type of request.
   */
  public function __construct(string $date, string $request_type) {
    $this->requestServiceType = $request_type;
    $this->date = $this->convertDate($date);
    $this->requestType = $this->types[$request_type]['request_type'];
    $this->requestTypeRate = $this->types[$request_type]['request_type_rate'];

    $schedule_settings = variable_get('scheduleSettings', '');
    if (!empty($schedule_settings)) {
      $this->settings = drupal_json_decode($schedule_settings);
    }
  }

  /**
   * Convert date from string to timestamp.
   *
   * @param string $date
   *   Date string.
   *
   * @return int
   *   Timestamp.
   */
  public function convertDate(string $date) : int {
    $date = new \DateTime($date, new \DateTimeZone($this->timeZone));
    return $date->getTimestamp();
  }

  /**
   * Build price by date, type, settings.
   *
   * @return float
   *   Rate price.
   */
  public function buildReservationPriceForRequest() : float {
    switch ($this->requestServiceType) {
      case RequestServiceType::FLAT_RATE:
        return $this->calcReservationLocal();

      case RequestServiceType::LONG_DISTANCE:
        return $this->calcReservationFlatRate();

      default:
        return $this->calcReservationLongDistance();
    }
  }

  /**
   * Calculate reservation price for local move request.
   *
   * @return float
   *   Reservation price.
   */
  public function calcReservationLocal() {
    $reservation_price = !empty($this->settings[$this->requestTypeRate]) ? $this->settings[$this->requestTypeRate] : 0;
    if (empty($this->settings[$this->requestTypeRate])) {
      $hours = !empty($this->settings[$this->requestType]) ? $this->settings[$this->requestType] : 0;
      $reservation_price = $hours * $this->setPriceCalendarTypeByDate()->getPriceRateType();
    }

    return (float) $reservation_price;
  }

  /**
   * Calculate reservation price for FlatRate move request.
   *
   * @return float
   *   Reservation price.
   */
  public function calcReservationFlatRate() {
    $reservation_price = !empty($this->settings[$this->requestTypeRate]) ? $this->settings[$this->requestTypeRate] : 0;
    if (empty($this->settings[$this->requestTypeRate])) {
      $percentage = !empty($this->settings[$this->requestType]) ? $this->settings[$this->requestType] : 0;
      $reservation_price = $percentage * $this->setPriceCalendarTypeByDate()->getPriceRateType();
    }

    return (float) $reservation_price;
  }

  /**
   * Calculate reservation price for LongDistance move request.
   *
   * @return float
   *   Reservation price.
   */
  public function calcReservationLongDistance() {
    $reservation_price = !empty($this->settings[$this->requestTypeRate]) ? $this->settings[$this->requestTypeRate] : 0;
    if (empty($this->settings[$this->requestTypeRate])) {
      $percentage = !empty($this->settings[$this->requestType]) ? $this->settings[$this->requestType] : 0;
      $reservation_price = $percentage * $this->setPriceCalendarTypeByDate()->getPriceRateType();
    }

    return (float) $reservation_price;
  }

  /**
   * Check if all prices(by hours or manual price) is zero.
   *
   * @return bool
   *   TRUE of FALSE.
   */
  public function isAllPriceZero() {
    return ($this->settings[$this->requestTypeRate] == 0 && $this->settings[$this->requestType] == 0) ? TRUE : FALSE;
  }

  /**
   * Get price from table.
   *
   * @return float|mixed
   *   Price or false.
   */
  private function getPriceFromSettings() {
    return !empty($this->settings[$this->requestTypeRate]) ? $this->settings[$this->requestTypeRate] :
      $this->setPriceCalendarTypeByDate()->getPriceRateType();
  }

  /**
   * Get all prices for calendar.
   *
   * @return mixed
   *   FALSE or array with dates and types.
   */
  public function getPriceCalendar() {
    return db_select('price_calendar_new', 'pcn')
      ->fields('pcn')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Get rate type for date.
   *
   * If no values - will use default "rateType:0", "moveType:0".
   *
   * @return float
   *   Price number.
   */
  public function getPriceRateType() : float {
    if (!empty($price_table = $this->getPriceTable())) {
      return $price_table[$this->rateType][$this->moveType];
    }

    return 0;
  }

  /**
   * Get all prices with request type and rate types.
   *
   * @return array
   *   Array with settings.
   */
  public function getPriceTable() : array {
    return Settings::getPriceTableArray();
  }

  /**
   * Set rate type by date.
   *
   * @return $this|\Drupal\move_services_new\Services\CalculateReservationPrice
   *   Scope of class.
   */
  public function setPriceCalendarTypeByDate() {
    $type = db_select('price_calendar_new', 'pcn')
      ->fields('pcn', array('type'))
      ->condition('date', $this->date)
      ->execute()
      ->fetchField();
    if (!empty($type)) {
      return $this->setRateType((int) $type);
    }
    return $this;
  }

  /**
   * Set rate type.
   *
   * @param int $rate_type
   *   Rate type.
   *
   * @return $this
   */
  public function setRateType(int $rate_type) {
    $this->rateType = $rate_type;
    return $this;
  }

  /**
   * Set move type. "2 Movers + Truck /hr" or "3 Movers + Truck /hr" etc...
   *
   * @param int $move_type
   *   Move type.
   *
   * @return $this
   */
  public function setMoveType(int $move_type) {
    $this->moveType = $move_type;
    return $this;
  }

}
