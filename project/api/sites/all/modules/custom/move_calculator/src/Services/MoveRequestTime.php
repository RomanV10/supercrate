<?php
namespace Drupal\move_calculator\Services;

use Drupal\move_distance_calculate\Services\DistanceCalculate;

class MoveRequestTime extends MoveCalculator {

  private $travelTime = 0;
  private $doubleDriveTime = 0;
  private $workTimeClosing = 0;
  private $workTimeMin = 0;
  private $workTimeMax = 0;
  private $distanceCalculate;

  public function __construct() {
    $this->distanceCalculate = new DistanceCalculate();
  }

  private function getCalcSettingsMinHours() : int {
    return $this->calculateSettings['min_hours'] ?? 0;
  }

  public function calculateTotalTimeClosing() {
    $travel_time = $this->checkDoubleDriveTimeClosing() ? $this->getDoubleDriveTimeClosing() : $this->getTravelTimeClosing();
    $work_time = $this->getWorkTimeClosing();
    $total_time = $travel_time + $work_time;

    return $this->getCalcSettingsMinHours() > $total_time ? $this->getCalcSettingsMinHours() : $total_time;
  }

  public function calculateTotalTimeMin() : float {
    $travel_time = $this->checkDoubleDriveTime() ? $this->getDoubleDriveTime() : $this->getTravelTime();
    $this->workTimeMin = $this->getWorkTimeMin() + $travel_time;

    if ($this->workTimeMin < $this->getCalcSettingsMinHours()) {
      $this->workTimeMin = $this->getCalcSettingsMinHours();
    }

    return $this->workTimeMin = $this->distanceCalculate::roundTimeInterval($this->workTimeMin);
  }

  public function calculateTotalTimeMax() : float {
    $travel_time = $this->checkDoubleDriveTime() ? $this->getDoubleDriveTime() : $this->getTravelTime();
    $this->workTimeMax = $this->getWorkTimeMax() + $travel_time;

    if ($this->workTimeMax < $this->getCalcSettingsMinHours()) {
      $this->workTimeMax = $this->getCalcSettingsMinHours();
    }

    return $this->workTimeMax = $this->distanceCalculate::roundTimeInterval($this->workTimeMax);
  }

  private function getTravelTime() {
    return $this->travelTime = $this->checkTravelTimeOriginToDestination() ? $this->retrieveData['travel_time']['raw'] : 0;
  }

  private function getTravelTimeClosing() {
    return $this->travelTime = $this->checkTravelTimeOriginToDestinationClosing() ? $this->closingData['travel_time']['raw'] ?? 0 : 0;
  }

  /**
   * Check if enable travel time from origin to destination in settings.
   *
   * @return bool
   *   Enable or not.
   */
  private function checkTravelTimeOriginToDestination() : bool {
    return $this->allData['travelTime'] ?? $this->calculateSettings['travelTime'];
  }

  private function checkTravelTimeOriginToDestinationClosing() : bool {
    return $this->closingAllData['travelTime'] ?? $this->calculateSettings['travelTime'];
  }

  private function checkDoubleDriveTime() {
    return $this->calculateSettings['doubleDriveTime'];
  }

  private function checkDoubleDriveTimeClosing() {
    return $this->closingData['field_double_travel_time']['raw'] ?? 0;
  }

  private function getDoubleDriveTime() {
    return $this->doubleDriveTime = $this->retrieveData['field_double_travel_time']['raw'] ?? $this->getTravelTime();
  }

  private function getDoubleDriveTimeClosing() {
    return $this->doubleDriveTime = $this->closingData['field_double_travel_time']['raw'] ?? $this->getTravelTimeClosing();
  }

  private function getWorkTimeClosing() : float {
    return $this->workTimeClosing = $this->closingData['work_time_int'] ?? 0;
  }

  private function getWorkTimeMin() : float {
    return  $this->retrieveData['minimum_time']['raw'] ?? 0;
  }

  private function getWorkTimeMax() : float {
    return $this->workTimeMax = $this->retrieveData['maximum_time']['raw'] ?? 0;
  }

}
