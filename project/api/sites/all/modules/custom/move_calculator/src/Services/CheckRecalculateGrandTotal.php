<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_calculator\Exceptions\NeedRecalculateException;
use Drupal\move_calculator\Tasks\CheckChangedFields;
use Drupal\move_services_new\Services\move_request\MoveRequest;

/**
 * Class CheckRecalculateGrandTotal.
 *
 * @package Drupal\move_calculator\Services
 */
class CheckRecalculateGrandTotal {

  /**
   * Check change in allData.
   *
   * @param array $newAllData
   *   New allData.
   * @param array $request
   *   Request from cache.
   *
   * @return bool
   *   Needed calculate.
   */
  public static function checkAllData(array $newAllData, array $request) {
    $oldAllData = MoveRequest::getRequestAllData($request['nid']);
    try {
      CheckChangedFields::closingDoubleTravelTime($oldAllData, $newAllData);
      CheckChangedFields::closingTravelTime($oldAllData, $newAllData);
      CheckChangedFields::closingWorkTime($oldAllData, $newAllData);
      CheckChangedFields::closingRate($oldAllData, $newAllData);
      CheckChangedFields::closingFlatRateQuote($oldAllData, $newAllData);
      CheckChangedFields::closingRequestData($oldAllData, $newAllData, $request);
      CheckChangedFields::closingValuationCharge($oldAllData, $newAllData);
      CheckChangedFields::closingSurchargeFuel($oldAllData, $newAllData);
      CheckChangedFields::closingAddMoneyDiscount($oldAllData, $newAllData);
      CheckChangedFields::closingCashDiscount($oldAllData, $newAllData);
      CheckChangedFields::closingCreditTax($oldAllData, $newAllData);
      CheckChangedFields::closingDiscount($oldAllData, $newAllData);
      CheckChangedFields::closingAddPercentDiscount($oldAllData, $newAllData);
      CheckChangedFields::closingWeight($oldAllData, $newAllData);

      CheckChangedFields::addRateDiscount($oldAllData, $newAllData);
      CheckChangedFields::rateDiscountExpiration($oldAllData, $newAllData);
      CheckChangedFields::reqTips($oldAllData, $newAllData);
      CheckChangedFields::creditTax($oldAllData, $newAllData);
      CheckChangedFields::customPayments($oldAllData, $newAllData);
      CheckChangedFields::contractInputs($oldAllData, $newAllData);
      CheckChangedFields::valuationSelectedCharge($oldAllData, $newAllData);
      CheckChangedFields::minWeight($oldAllData, $newAllData);
      CheckChangedFields::minPriceEnabled($oldAllData, $newAllData);
      CheckChangedFields::minPrice($oldAllData, $newAllData);
    }
    catch (NeedRecalculateException $exception) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Check node fields.
   *
   * @param int $nid
   *   Node id.
   * @param array $request
   *   Changed field.
   *
   * @return bool
   *   Needed calculate.
   */
  public static function checkRequest(int $nid, array $request) {
    if (!empty($needFields = CheckChangedFields::getNeedField($request))) {
      $wrapper = entity_metadata_wrapper('node', $nid, array('bundle' => 'move_request'));
      foreach ($needFields as $field_name => $value) {
        if ($field_name == 'field_moving_to' && CheckChangedFields::checkFieldMovingTo($wrapper, $value)) {
          return TRUE;
        }
        if ($wrapper->{$field_name}->value() != $value) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

}
