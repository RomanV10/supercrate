<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class MoveRequestBalance.
 *
 * @package Drupal\move_calculator\Services
 */
class MoveRequestGrandTotal extends MoveCalculator {

  private $packing = 0.0;
  private $additionalServices = 0.0;
  private $additionalServicesClosing = 0.0;

  public $quoteMin = 0.0;
  public $quoteMax = 0.0;
  public $quoteClosing = 0.0;
  public $quoteMiddle = 0.0;

  public $grandTotalMin = 0.0;
  public $grandTotalMax = 0.0;
  public $grandTotalMiddle = 0.0;
  public $grandTotalClosing = 0.0;

  /**
   * Calculate move request grand total.
   */
  public function build() {
    parent::build();

    $extraServices = (new MoveRequestExtraService())->setRequestRetrieve($this->retrieveData)->build()->calculate();
    $this->additionalServices = $extraServices->extraServiceTotal;
    $this->additionalServicesClosing = $extraServices->extraServiceTotalClosing;

    $move_request_quote = (new MoveRequestQuote())->setRequestRetrieve($this->retrieveData)->build()->calculate();
    $this->quoteMax = $move_request_quote->quoteMax;
    $this->quoteMin = $move_request_quote->quoteMin;
    $this->quoteClosing = $move_request_quote->quoteClosing;
    $this->quoteMiddle = $move_request_quote->quoteMiddle;

    return $this;
  }

  private function sumQuoteAddServicesPacking($quote) {
    return $quote + $this->additionalServices + $this->calculatePacking();
  }

  private function sumValuationFuelSurcharge() {
    return $this->calculateValuation() + $this->getFuelSurcharge();
  }

  public function calculateGrandTotal($quote) {
    $grand_total = $this->sumQuoteAddServicesPacking($quote);

    $grand_total += $this->sumValuationFuelSurcharge();
    $grand_total -= $this->requestFixedDiscount();
    $grand_total -= $this->contractDiscount();
//    $grand_total -= $this->getCashDiscount();
//    $grand_total -= $this->getCreditTax();
    $grand_total -= $this->calculatePercentDiscount($grand_total);

    return $grand_total;
  }

  public function calculateGrandTotalClosing($quote) {
    $tips = $this->getRequestTips();
    $packing = $this->calculatePackingClosing();
    $valuation = $this->calculateValuationClosing();
    $fuel_surcharge = $this->getFuelSurchargeClosing();

    $discount_fixed = $this->requestFixedDiscountClosing();
    $cash_discount = $this->getCashDiscountClosing();
    $credit_tax = $this->getCreditTaxClosing();
    $contract_discount = $this->contractDiscount();

    $custom_payments_tax_sum = $this->calculateCustomPaymentsTax();

    $calculate_contract_inputs = $this->calculateContractInputs();

    $grand_total = $quote + $this->additionalServicesClosing + $packing + $fuel_surcharge + $valuation - $contract_discount;

    $calculate_discount_percent = $this->calculatePercentDiscountClosing($grand_total);
    $grand_total -= $calculate_discount_percent;
    $grand_total -= $discount_fixed;
    $grand_total -= $cash_discount;
    $grand_total += $custom_payments_tax_sum;
    $grand_total += $credit_tax;
    $grand_total += $tips;

    return $grand_total;
  }

  public function calculate() {
    $serviceTypeMethodName = $this->getServiceNameMethod();
    $this->$serviceTypeMethodName();
    $this->grandTotalClosing = ($this->closingData) ? $this->calculateGrandTotalClosing($this->quoteClosing) : 0;
    $this->grandTotalMiddle = $this->calculateGrandTotalMiddle($this->grandTotalMin, $this->grandTotalMax);
    return $this;
  }

  public function localCalculation() {
    $this->grandTotalMin = $this->calculateGrandTotal($this->quoteMin);
    $this->grandTotalMax = $this->calculateGrandTotal($this->quoteMax);
  }

  public function flatRateCalculation() {
    $this->grandTotalMin = $this->grandTotalMax = $this->calculateGrandTotal($this->quoteMiddle);
  }

  public function longDistanceCalculation() {
    $this->grandTotalMin = $this->grandTotalMax = $this->calculateGrandTotal($this->quoteMiddle);
  }

  /**
   * This middle value between min and max grand total.
   *
   * @param float $grand_total_min
   *   Minimum grand total value.
   * @param float $grand_total_max
   *   Maximum grand total value.
   *
   * @return float
   *   Grand total value.
   */
  public function calculateGrandTotalMiddle(float $grand_total_min, float $grand_total_max) : float {
    return ($grand_total_max + $grand_total_min) / 2;
  }

  private function getPercentDiscount() : float {
    return $this->allData['add_percent_discount'] ?? 0;
  }

  private function getPercentDiscountClosing() : float {
    return $this->closingAllData['add_percent_discount'] ?? 0;
  }

  private function getCashDiscount() : float {
    return $this->allData['cashDiscount'] ?? 0;
  }

  private function getCashDiscountClosing() : float {
    return $this->closingAllData['cashDiscount'] ?? $this->getCashDiscount();
  }

  private function getCreditTax() : float {
    return $this->allData['creditTax'] ?? 0;
  }

  private function getCreditTaxClosing() : float {
    return $this->closingAllData['creditTax'] ?? $this->getCreditTax();
  }

  private function calculatePercentDiscount($quote) {
    $discount = $this->getPercentDiscount();
    return $quote * $discount / 100 ?? 0;
  }

  private function calculatePercentDiscountClosing($quote) {
    $discount = $this->getPercentDiscountClosing();
    return $quote * $discount / 100 ?? 0;
  }

  private function requestFixedDiscount() {
    return $this->allData['add_money_discount'] ?? 0;
  }

  private function requestFixedDiscountClosing() {
    return $this->closingAllData['add_money_discount'] ?? 0;
  }

  private function contractDiscount() {
    return $this->closingData['discount'] ?? 0;
  }

  private function calculateContractInputs() {
    $contract_inputs = $this->allData['contractInputs'] ?? array();
    return array_sum($contract_inputs);
  }

  private function getFuelSurchargeClosing() : float {
    return $this->closingAllData["surcharge_fuel"] ?? 0;
  }

  private function getFuelSurcharge() : float {
    $surcharge_fuel = $this->allData['surcharge_fuel'] ?? 0;
    return (float) $surcharge_fuel;
  }

  private function calculateValuation() {
    return (new TemplateBuilder())->getValuationCharge($this->retrieveData);
  }

  private function calculateValuationClosing() : float {
    return $this->closingAllData['valuation']['valuation_charge'] ?? 0;
  }

  private function calculatePacking(): float {
    return $this->packing = (new TemplateBuilder())->calculatePackingServices($this->retrieveData);
  }

  private function calculatePackingClosing(): float {
    return (new TemplateBuilder())->calculatePackingServices($this->retrieveData, TRUE);
  }

  private function getTips() {
    return $this->getContractTips() ?? $this->getRequestTips();
  }

  private function getContractTips() {
    return $this->retrieve['contract_info'][0]['tips_total'] ?? 0;
  }

  private function getRequestTips() {
    return $this->allData['req_tips'] ?? 0;
  }

  private function getTipsClosing() {
    return $this->closingAllData ?? 0;
  }

  private function calculateCustomPaymentsTax() : float {
    $values = array_column($this->getCustomPaymentsTax(), 'value');
    return array_sum($values);
  }

  private function getCustomPaymentsTax() : array {
    return $this->allData['customPayments'] ?? [];
  }


  /**
   * Get all completed move request from search_api.
   *
   * @return array
   *   Requests info.
   *
   * @throws \SearchApiException
   */
  public static function getAllCompletedRequest() {
    $query = search_api_query('move_request');
    $result = $query->execute();

    return $result;
  }

}