<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_services_new\Services\ExtraServices;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\RequestEntranceFrom;
use Drupal\move_services_new\Util\enum\RequestEntranceTo;

/**
 * Class MoveRequestExtraService.
 *
 * @package Drupal\move_calculator\Services
 */
class MoveRequestExtraService extends MoveCalculator {

  public $extraService = [];
  public $extraServiceClosing = [];

  public $extraServiceTotal = 0.00;
  public $extraServiceTotalClosing = 0.00;

  public $longDistanceSettings = [];
  public $basicSettings = [];

  public $maxDistance = 0;

  /**
   * Set initial values.
   *
   * @return \Drupal\move_calculator\Services\MoveCalculator
   *   MoveRequestExtraService.
   */
  public function build() {
    parent::build();

    $this->longDistanceSettings = json_decode(variable_get('longdistance', ''), TRUE);
    $this->basicSettings = json_decode(variable_get('basicsettings', ''), TRUE);

    return $this;
  }

  /**
   * Calculate extra services.
   *
   * @return $this|\Drupal\move_calculator\Services\MoveCalculator
   *   MoveRequestExtraService.
   */
  public function calculate() {
    $this->extraService = $this->getExtraServices();
    $this->extraServiceClosing = $this->getExtraServicesClosing();

    $this->extraServiceTotal = $this->calculateExtraServicesTotalSales();
    $this->extraServiceTotalClosing = $this->calculateExtraServicesTotalClosing();

    return $this;
  }

  /**
   * Get extra services.
   *
   * @return array
   *   Array with extra services.
   */
  public function getExtraServices() {
    return $this->retrieveData['extraServices'] ?? [];
  }

  /**
   * Get extra services closing.
   *
   * @return array
   *   Array with extra services closing.
   */
  public function getExtraServicesClosing() {
    return $this->closingData['extraServices'] ?? [];
  }

  /**
   * Calculate total for closing extra services.
   *
   * @return float|int
   *   Float or integer.
   */
  public function calculateExtraServicesTotalClosing() {
    $result = 0;

    foreach ($this->extraServiceClosing as $item) {
      if (is_array($item)) {
        $item = (object) $item;
      }

      if (isset($item->extra_services) && $item->extra_services) {
        $result += self::calculateExtraService($item->extra_services);
      }
    }

    return $result;
  }

  /**
   * Calculate total for closing extra services.
   *
   * @return float|int
   *   Float or integer.
   */
  public function calculateExtraServicesTotalSales() {
    $result = 0;

    foreach ($this->extraService as $item) {
      if (is_array($item)) {
        $item = (object) $item;
      }

      if (isset($item->extra_services) && $item->extra_services) {
        $result += self::calculateExtraService($item->extra_services);
      }
    }

    return $result;
  }

  /**
   * Calculate one item from extra services.
   *
   * @param array $extra_services
   *   Extra service item.
   *
   * @return float|int
   *   Float or integer.
   */
  public static function calculateExtraService(array $extra_services = []) {
    foreach ($extra_services as $item) {
      if (is_array($item)) {
        $item = (object) $item;
      }
      if (isset($item->services_default_value)) {
        $item->services_default_value = (float) preg_replace("/[^\d-.]+/", '', $item->services_default_value);
      }
      else {
        $item->services_default_value = 0;
      }

      if (isset($result)) {
        $result *= $item->services_default_value;
      }
      else {
        $result = $item->services_default_value;
      }
    }

    return $result ?? 0;
  }

  /**
   * Calculate Equipment Fee.
   *
   * @param bool $is_draft
   *   Request draft or not.
   *
   * @return int
   *   Amount.
   */
  public function calculateEquipmentFee($is_draft = FALSE) {
    $result = 0;
    $max_distance = $this->getMaxDistance($is_draft);
    $equipment_fee = $this->basicSettings['equipment_fee'] ?? FALSE;
    $amount_request = DistanceCalculate::distanceRequest($max_distance, $equipment_fee);

    if ($amount_request['isExistVariant']) {
      $result = $amount_request['amount'];
    }

    return $result;
  }

  /**
   * Long distance calculation.
   *
   * @return array|void
   *   Array with services.
   *
   * @throws \Exception
   */
  public function longDistanceCalculation() {
    $weight = (new MoveRequest())->getRequestWeight($this->retrieveData);

    $result = [];

    if (isset($this->retrieveData['inventory']) && isset($this->retrieveData['inventory']['move_details'])) {
      if (isset($this->retrieveData['inventory']['move_details']['new_door'])) {
        $fee = 0;
        $cur_feet = $this->retrieveData['inventory']['move_details']['new_door'];
        $extra_feet = (float) $this->longDistanceSettings['extrafeet'];
        $extra_feet_rate = (float) $this->longDistanceSettings['extrafeetrate'];
        $extra_feet_flat_fee = (float) $this->longDistanceSettings['extrafeet_flatfee'];

        if ($cur_feet > $extra_feet) {
          $k = round($cur_feet / $extra_feet);

          if ($extra_feet_rate) {
            $fee = $k * $extra_feet_rate * round($weight / 100);
          }
          elseif ($extra_feet_flat_fee) {
            $fee = $k * $extra_feet_flat_fee;
          }

          if ($fee) {
            $charge = ExtraServices::initBasicService('Long Carry Fee', $fee);
            $result[] = $charge;
          }
        }
      }

      $steps_origin = $this->retrieveData['inventory']['move_details']['steps_origin'];
      $steps_in_flight = (float) $this->longDistanceSettings['stepsInflight'];
      $charge_per_flight = (float) $this->longDistanceSettings['chargePerflight'];
      $free_flights = (float) $this->longDistanceSettings['freeFlights'];

      if ($steps_origin > 2 && $steps_in_flight > 0) {
        $flights_origin = ceil($steps_origin / $steps_in_flight);
        $charge_flights = $flights_origin - $free_flights;
        $fee = $charge_per_flight * $charge_flights;

        if ($fee) {
          $charge = ExtraServices::initBasicService('Extra Charge For Steps (Origin)', $fee);
          $result[] = $charge;
        }
      }

      $steps_origin = $this->retrieveData['inventory']['move_details']['steps_destination'];

      if ($steps_origin > 2 && $steps_in_flight > 0) {
        $flights_origin = ceil($steps_origin / $steps_in_flight);
        $charge_flights = $flights_origin - $free_flights;
        $fee = $charge_per_flight * $charge_flights;

        if ($fee) {
          $charge = ExtraServices::initBasicService('Extra Charge For Steps (Destination)', $fee);
          $result[] = $charge;
        }
      }
    }

    if ($this->retrieveData['type_to']['raw'] != RequestEntranceTo::ELEVATOR
      && $this->retrieveData['type_to']['raw'] != RequestEntranceTo::PRIVATE_HOUSE
      && isset($this->longDistanceSettings['isCalculateFloorMovingTo'])
      && $this->longDistanceSettings['isCalculateFloorMovingTo']
      && $this->retrieveData['type_to']['raw'] > $this->longDistanceSettings['above_floorextra'] - 1) {

      $fee = 0;
      $rate = (float) $this->longDistanceSettings['floorextra'];
      $flat_rate = (float) $this->longDistanceSettings['floorextra_flatrate'];
      $diff = (float) $this->retrieveData['type_to']['raw'] - (float) $this->longDistanceSettings['above_floorextra'] + 1;

      if ($rate) {
        $fee = $diff * $rate * round($weight / 100);
      }
      elseif ($flat_rate) {
        $fee = $diff * $flat_rate;
      }

      if ($fee) {
        $charge = ExtraServices::initBasicService('Floors Extra Fee (Destination)', $fee);
        $result[] = $charge;
      }
    }

    if ($this->retrieveData['type_to']['raw'] == RequestEntranceTo::ELEVATOR) {
      $fee = 0;
      $rate = (float) $this->longDistanceSettings['floorextrafifth'];
      $flat_rate = (float) $this->longDistanceSettings['floorextrafifth_flatRate'];
      $diff = (float) $this->retrieveData['type_to']['raw'] - 5;

      if ($rate) {
        $fee = $diff * $rate * round($weight / 100);
      }
      elseif ($flat_rate) {
        $fee = $diff * $flat_rate;
      }

      if ($fee) {
        $charge = ExtraServices::initBasicService('Elevator Extra Fee (Destination)', $fee);
        $result[] = $charge;
      }
    }

    if ($this->retrieveData['type_from']['raw'] != RequestEntranceFrom::ELEVATOR
      && $this->retrieveData['type_from']['raw'] != RequestEntranceFrom::PRIVATE_HOUSE
      && isset($this->longDistanceSettings['isCalculateFloorMovingFrom'])
      && $this->longDistanceSettings['isCalculateFloorMovingFrom']
      && $this->retrieveData['type_from']['raw'] > $this->longDistanceSettings['above_floorextra'] - 1) {

      $fee = 0;
      $rate = (float) $this->longDistanceSettings['floorextra'];
      $flat_rate = (float) $this->longDistanceSettings['floorextra_flatrate'];
      $diff = (float) $this->retrieveData['type_from']['raw'] - (float) $this->longDistanceSettings['above_floorextra'] + 1;

      if ($rate) {
        $fee = $diff * $rate * round($weight / 100);
      }
      elseif ($flat_rate) {
        $fee = $diff * $flat_rate;
      }

      if ($fee) {
        $charge = ExtraServices::initBasicService('Floors Extra Fee (Origin)', $fee);
        $result[] = $charge;
      }
    }

    if ($this->retrieveData['type_from']['raw'] == RequestEntranceFrom::ELEVATOR) {
      $fee = 0;
      $rate = (float) $this->longDistanceSettings['floorextrafifth'];
      $flat_rate = (float) $this->longDistanceSettings['floorextrafifth_flatRate'];
      $diff = (float) $this->retrieveData['type_from']['raw'] - 5;

      if ($rate) {
        $fee = $diff * $rate * round($weight / 100);
      }
      elseif ($flat_rate) {
        $fee = $diff * $flat_rate;
      }

      if ($fee) {
        $charge = ExtraServices::initBasicService('Elevator Extra Fee (Origin)', $fee);
        $result[] = $charge;
      }
    }

    return $result;
  }

  /**
   * Get max distance.
   *
   * @param bool $is_draft
   *   Is draft or not.
   *
   * @return mixed
   *   Max distance.
   */
  private function getMaxDistance($is_draft = FALSE) {
    $zipFrom = $this->retrieveData['field_moving_from']['postal_code'];
    $zipTo = $this->retrieveData['field_moving_to']['postal_code'];


    return isset($this->retrieveData['request_all_data']['request_distance']['max_distance'])
      ? $this->retrieveData['request_all_data']['request_distance']['max_distance']
      : (new DistanceCalculate($is_draft))->getMaxDistance($zipFrom, $zipTo);
  }

}
