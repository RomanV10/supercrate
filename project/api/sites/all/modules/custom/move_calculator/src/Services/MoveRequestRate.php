<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\RequestServiceType;

/**
 * Class MoveRequestRate.
 *
 * @package Drupal\move_calculator\Services
 */
class MoveRequestRate extends MoveCalculator {
  public $rate = 0.0;
  private $truckCount;
  public $rateClosing = 0.0;

  public function calculate() {
    $serviceTypeMethodName = $this->getServiceNameMethod();
    $this->$serviceTypeMethodName();
    return $this;
  }

  public function localCalculation(){
    $this->rate = $this->getRate();
    $this->rateClosing = isset($this->closingData) ? $this->getRateClosing() : 0;
  }

  public function flatRateCalculation(){
    $this->rate = (float) ($this->retrieveData['flat_rate_quote']['value'] ?? 0);
    $this->rateClosing = (float) ($this->closingData['flat_rate_quote']['value'] ?? 0);
  }

  public function longDistanceCalculation() {
    /* Это уже реализовано  в DistanceCalculate->getLongDistanceRate и будет вызыватся когда будет подсчет квоты вызывать его еще раз здесь пока не вижу смысла. */
  }

  private function getLocalRate() : float {
    return $this->retrieveData['rate']['value'] ?? $this->getTotalRate();
  }

  private function calculateRate() {
    $rate = (new CalculateReservationPrice($this->retrieveData['data']['raw'], RequestServiceType::MOVING))->setPriceCalendarTypeByDate()->getPriceRateType();
  }

  public function getRateClosing() : float {
    return $this->closingData['rate']['value'] ?? $this->getRate();
  }

  private function getRateDiscount() : float {
    return isset($this->allData['add_rate_discount']) && $this->isRateDiscountActive() ? $this->allData['add_rate_discount'] : 0;
  }

  private function isRateDiscountActive() {
    $discount_active = TRUE;
    $rate_discount_expiration = $this->allData['add_rate_discount_expiration'] ?? '';
    if ($rate_discount_expiration) {
      $time_to_discount_end = (new Extra())->getDiffDays(NULL, $rate_discount_expiration);
      if ($time_to_discount_end) {
        $discount_active = (new Extra())->checkDiffDaysMoreZeroPositive($time_to_discount_end);
      }
    }
    return $discount_active;
  }

  public function getRate() {
    return $this->rate = !empty($this->getRateDiscount()) ? $this->getRateDiscount() : $this->getLocalRate();
  }

  private function getPriceTableTypes() {
    return variable_get('price_table_types');
  }

  private function getPriceTable() {
    return variable_get('price_table');
  }

  public function getPriceCalendarTypeByDate($move_date = NULL) {
    $move_date = $this->retrieveData['date']['raw'] ?? $move_date;
    return db_select('price_calendar_new', 'pcn')
      ->fields('pcn', array('type'))
      ->condition('date', $move_date)
      ->execute()
      ->fetchField();
  }

  private function getRateTypeName() {
    return $this->getPriceTableTypes()[$this->getPriceCalendarTypeByDate()];
  }

  private function getMoversCount() {
    $calcSettings = json_decode(variable_get('calcsettings', ''));
    return $this->retrieveData['crew']['value'] ?? $calcSettings->min_movers;
  }

  private function getIdMoversFromTable() {
    $minimum_movers = 2;
    return $this->getMoversCount() - $minimum_movers;
  }

  private function getExtraMover() {
    return $this->getMoversCount() > 4 ? $this->getMoversCount() - 4 : FALSE;
  }

  private function getTruckCount() {
    return $trucks_count = count($this->retrieveData['trucks']['raw']);
  }

  private function checkExtraTruck() {
    $this->truckCount = $this->getTruckCount();
    return $this->truckCount > 1 ? $this->truckCount : 1;
  }

  private function getExtraTruckRate() {
    // Additional truck price hour column 4.
    return $this->getPriceTable()[$this->getRateTypeName()][4] ?? 0;
  }

  private function getExtraMoverRate() {
    // Additional mover price hour column 3.
    $this->getPriceTable()[$this->getRateTypeName()][3];
  }

  private function getRateFromTable() {
    return $this->getPriceTable()[$this->getRateTypeName()][$this->getIdMoversFromTable()];
  }

  private function getTotalRate() {
    $extra_truck_rate = 0;
    $extra_mover_rate = 0;
    $table_rate = $this->getRateFromTable();
    if ($extra_mover_count = $this->getExtraMover()) {
      $extra_mover_rate = $extra_mover_count * $this->getExtraMoverRate();
    }

    if ($truck_count = $this->checkExtraTruck()) {
      $extra_truck_rate = ($truck_count - 1) * $this->getExtraTruckRate();
    }

    return $table_rate + $extra_mover_rate + $extra_truck_rate;
  }

  /**
   * Get rate from table if request not created.
   *
   * @param null|string $move_date
   *   Move date.
   *
   * @return mixed
   *   Price.
   */
  public function getRateWithoutRequest($move_date = NULL) {
    $move_date = $move_date ?? date('Y-m-d', time());
    $move_date = Extra::getDateFrom($move_date);
    $rate_price_name = $this->getPriceTableTypes()[$this->getPriceCalendarTypeByDate($move_date)];
    return $this->getPriceTable()[$rate_price_name][$this->getIdMoversFromTable()];
  }

}
