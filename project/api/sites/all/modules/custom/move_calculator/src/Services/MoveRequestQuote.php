<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_distance_calculate\Services\DistanceCalculate;

/**
 * Class MoveRequestQuote.
 *
 * @package Drupal\move_calculator\Services
 */
class MoveRequestQuote extends MoveCalculator {

  public $quote = 0.0;
  public $quoteLocalClosing = 0.0;
  public $quoteLocalMin = 0.0;
  public $quoteLocalMax = 0.0;
  public $quoteFlatRate = 0.0;
  public $quoteFlatRateClosing = 0.0;
  public $quoteLongDistance = 0.0;
  public $quoteLongDistanceClosing = 0.0;

  public $quoteMiddle = 0.0;
  public $quoteMin = 0.0;
  public $quoteMax = 0.0;
  public $quoteClosing = 0.0;

  public $quoteCurrentMiddle = 0.0;
  public $quoteCurrentMiddleClosing = 0.0;

  private $totalTimeClosing = 0.0;
  private $totalTimeMin = 0.0;
  private $totalTimeMax = 0.0;

  private $rate = 0.0;
  private $rateClosing = 0.0;

  public function build() {
    parent::build();
    $moveRequestTime = (new MoveRequestTime())->setRequestRetrieve($this->retrieveData)->build();
    $this->totalTimeClosing = ($this->closingData) ? $moveRequestTime->calculateTotalTimeClosing() : 0;
    $this->totalTimeMin = $moveRequestTime->calculateTotalTimeMin();
    $this->totalTimeMax = $moveRequestTime->calculateTotalTimeMax();

    $moveRequestRate = (new MoveRequestRate())->setRequestRetrieve($this->retrieveData)->build()->calculate();
    $this->rate = $moveRequestRate->rate;
    $this->rateClosing = $moveRequestRate->rateClosing;

    return $this;
  }

  private function calculateFlatRateQuote() {
    return $this->quoteFlatRate = (float) ($this->retrieveData['flat_rate_quote']['value'] ?? 0);
  }

  private function calculateFlatRateQuoteClosing() {
    return $this->quoteFlatRateClosing = (float) ($this->closingData['flat_rate_quote']['value'] ?? $this->calculateFlatRateQuote());
  }

  private function calculateLongDistanceQuote() {
    return $this->quoteLongDistance = (new DistanceCalculate())->getLongDistanceQuote($this->retrieveData);
  }

  private function calculateLongDistanceQuoteClosing() {
    return $this->quoteLongDistanceClosing = (new DistanceCalculate())->getLongDistanceQuote($this->retrieveData, TRUE);
  }

  private function getTotalHourlyCharge() {
    return $this->allData['totalHourlyCharge'] ?? 0;
  }

  private function calculateLocalQuoteClosing() {
    return $this->quoteLocalClosing = $this->rateClosing * $this->totalTimeClosing;
  }

  private function calculateLocalQuoteMin() {
    return $this->quoteLocalMin = $this->rate * $this->totalTimeMin;
  }

  public function calculateLocalQuoteMax() {
    return $this->quoteLocalMax = $this->rate * $this->totalTimeMax;
  }

  /**
   * Calculate middle quote.
   *
   * @param float $min_quote
   *   Min quote value.
   * @param float $max_quote
   *   Max quote value.
   *
   * @return float
   *   Middle value.
   */
  private function calculateMiddleQuote(float $min_quote, float $max_quote) : float {
    return $this->quoteMiddle = ($max_quote + $min_quote) / 2;
  }

  public function calculate() {
    $serviceTypeMethodName = $this->getServiceNameMethod();
    $this->$serviceTypeMethodName();
    $this->calculateMiddleQuote($this->quoteMin, $this->quoteMax);

    return $this;
  }

  public function localCalculation(){
    $this->quoteMin = $this->calculateLocalQuoteMin();
    $this->quoteMax = $this->calculateLocalQuoteMax();
    $this->quoteClosing = ($this->closingData) ? $this->calculateLocalQuoteClosing() : 0;
  }

  public function flatRateCalculation(){
    $this->quoteMin = $this->quoteMax = $this->calculateFlatRateQuote();
    $this->quoteClosing = ($this->closingData) ? $this->calculateFlatRateQuoteClosing() : 0;
  }

  public function longDistanceCalculation(){
    $this->quoteMin = $this->quoteMax = $this->calculateLongDistanceQuote();
    $this->quoteClosing = ($this->closingData) ? $this->calculateLongDistanceQuoteClosing() : 0;
  }

}