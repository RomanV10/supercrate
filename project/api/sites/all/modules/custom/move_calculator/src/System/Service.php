<?php

namespace Drupal\move_calculator\System;

use Drupal\move_calculator\Services\CalculateWorkTime;
use Drupal\move_services_new\Services\move_request\MoveRequest;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'move_calculator' => array(
        'operations' => array(),
        'actions' => array(
          'calculate_work_time' => array(
            'help' => 'Search move requests',
            'callback' => 'Drupal\move_calculator\System\Service::calculateWorkTimeByNid',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'nid',
                'type' => 'int',
                'source' => array('data' => 'nid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'calculate_work_time_by_request' => array(
            'help' => 'Search move requests',
            'callback' => 'Drupal\move_calculator\System\Service::calculateWorkTimeByRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_distance_calculate',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'request',
                'type' => 'array',
                'source' => array('data' => 'request'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  /**
   * Calculate work with request loading.
   *
   * @param int $nid
   *   Request id.
   *
   * @return array
   *   Array with min and max work time
   */
  public static function calculateWorkTimeByNid(int $nid) : array {
    return (new CalculateWorkTime())->buildWorkTimeByNid($nid);
  }

  /**
   * Calculate work time by params.
   *
   * @param array $request
   *   Request fields.
   *
   * @return array
   *   Array with min and max work time
   */
  public static function calculateWorkTimeByRequest(array $request) : array {
    return (new CalculateWorkTime())->buildWorkTimeFromRequest($request);
  }
}