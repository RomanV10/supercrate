<?php

namespace Drupal\move_calculator\Services;

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Services\move_request\MoveRequestRetrieve;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_template_builder\Services\TemplateBuilder;

/**
 * Class CalculateGrandTotal.
 *
 * @package Drupal\move_calculator\Services
 */
class CalculateGrandTotal extends MoveCalculator {

  protected $request;

  /**
   * Get grand total by request id.
   *
   * @param int $nid
   *   Request id.
   *
   * @return array
   *   Sales and closing grand total.
   */
  public function buildById(int $nid) : array {
    $this->request = (new MoveRequest())->getNode($nid);
    return $this->run();
  }

  /**
   * Get grand total by request retrieve data.
   *
   * @param array $request_retrieve
   *   Request retrieve array.
   *
   * @return array
   *   Sales and closing grand total.
   */
  public function buildFromRequestRetrieve(array $request_retrieve) : array {
    $this->request = $request_retrieve;
    return $this->run();
  }

  /**
   * Get grand total by EntityMetadataWrapper.
   *
   * @param \EntityMetadataWrapper $node_wrapper
   *   Request EntityMetadataWrapper object.
   *
   * @return array
   *   Sales and closing grand total.
   */
  public function buildFromEntityMetaDataWrapper(\EntityMetadataWrapper $node_wrapper) : array {
    $request = new MoveRequestRetrieve($node_wrapper);
    $this->request = $request->nodeFields();
    return $this->run();
  }

  /**
   * Run calculation.
   *
   * @return array
   *   Sales and closing grand total.
   */
  public function run() : array {
    return $this->calculate();
  }


  // TODO this will be logic for other service types.
  public function calculate() : array {
    $template_builder = new TemplateBuilder();
    $result['sales'] = $template_builder->calculateGrandTotal($this->request);
    $result['closing'] = $template_builder->calculateGrandTotal($this->request, TRUE);
    return $result;
  }

}
