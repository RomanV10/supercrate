<?php

/**
 * @file
 * Batch for update request.
 */

use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Balance batch form.
 *
 * @param mixed $form
 *   Form data.
 * @param mixed $form_state
 *   Form state.
 *
 * @return mixed
 *   Form.
 */
function move_calculator_balance_batch($form, &$form_state) {
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Start'),
  );

  return $form;
}

/**
 * Batch submit.
 *
 * @param mixed $form
 *   Form data.
 * @param mixed $form_state
 *   Form state data.
 */
function move_calculator_balance_batch_submit($form, &$form_state) {
  $operations = array();
  $nids = db_select('search_api_db_move_request', 'search')
    ->fields('search', array('nid'))
    ->condition('field_approve',
      [
        RequestStatusTypes::EXPIRED,
        RequestStatusTypes::ARCHIVE,
        RequestStatusTypes::DEAD_LEAD,
        RequestStatusTypes::CANCELLED,
      ],
      'NOT IN')
    ->orderBy('nid', 'DESC')
    ->execute()
    ->fetchCol();

  $parts = array_chunk($nids, 20);

  foreach ($parts as $nids) {
    $operations[] = array('move_calculator_set', array($nids));
  }

  batch_set(array(
    'operations' => $operations,
    'finished' => 'move_calculator_balance_batch_finished',
    'title' => 'Set request balance',
    'init_message' => 'Prepare data',
    'file' => drupal_get_path('module', 'move_calculator') . '/move_calculator.admin.inc',
  ));
}

/**
 * Update move request balance data.
 *
 * @param array $nids
 *   Move request id.
 * @param mixed $context
 *   Context.
 *
 * @throws \Exception
 */
function move_calculator_set(array $nids, &$context) {
  foreach ($nids as $nid) {
    $moveRequest = new MoveRequest($nid);
    $moveRequest->update2(['field_useweighttype' => $moveRequest->getNode($nid)['field_useweighttype']['value']]);

    isset($context['results']['updated_nodes']) ? $context['results']['updated_nodes']++ : $context['results']['updated_nodes'] = 0;
    $context['message'] = 'Баланс реквеста обновлен <em>' . check_plain($nid) . '</em>';
  }

}

/**
 * Batch finish callback.
 */
function move_calculator_balance_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Обновлена дата у ' . $results['updated_nodes'] . ' материалов');
  }
  else {
    drupal_set_message('Завершено с ошибками.', 'error');
  }
}
