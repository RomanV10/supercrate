<?php

namespace Drupal\move_google_api\Google\Calendar\Services;

use Drupal\move_google_api\Google\Calendar\AbstractGoogleCalendarService;
use Drupal\move_google_api\Google\Calendar\Entities\Calendar;
use Drupal\move_google_api\Google\Calendar\Entities\Event;
use Drupal\move_google_api\Google\Calendar\EventsBuilder\EventsDirector;
use Drupal\move_google_api\Google\Calendar\EventsBuilder\MoveRequestEventBuilder;
use Drupal\move_google_api\Helpers\EmailsHelper;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Google_Service_Calendar_EventAttendee as GoogleAttendee;
use LogicException;
use PDOException;
use RuntimeException;

/**
 * Class MoveRequestsEventsService.
 *
 * @package Drupal\move_google_api\GoogleCalendar\Events\Services
 */
class MoveRequestsEventsService extends AbstractGoogleCalendarService {

  /**
   * Metadata table name.
   */
  const METADATA_TABLE = 'move_google_calendars_events';

  /**
   * Events metadata manipulation service.
   *
   * @var \Drupal\move_google_api\Google\Calendar\Services\EventsMetadataService
   */
  private $metadataService;

  /**
   * MoveRequestsEventsService constructor.
   */
  public function __construct() {
    parent::__construct();

    $this->metadataService = new EventsMetadataService();
  }

  /**
   * Creates calendar event from MoveRequest data.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Calendar $calendar
   *   Internal calendar instance.
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   MoveRequest instance.
   *
   * @return \Drupal\move_google_api\Google\Calendar\Entities\Event
   *   Calendar event instance.
   *
   * @throws LogicException
   * @throws PDOException
   */
  public function createInCalendar(Calendar $calendar, MoveRequest $move_request): Event {
    $node_id = $move_request->retrieve()['nid'];

    try {
      $remote_event_id = $this->retrieveFromCalendarByNodeId($calendar, $node_id)
        ->getRemoteEventId();

      throw new LogicException("Remote GoogleEvent with id='{$remote_event_id}'"
        . " already exists for node id='{$node_id}'");
    }
    catch (\Throwable $exception) {
      $unsaved_remote_event = (new EventsDirector)->buildEvent(
        new MoveRequestEventBuilder($move_request)
      );

      $remote_event = $this->googleService->events->insert(
        $calendar->getRemoteCalendarId(),
        $unsaved_remote_event
      );

      $metadata = $this->metadataService->createForCalendarAndRemoteEvent(
        $calendar,
        $remote_event,
        $node_id
      );

      return new Event($calendar, $remote_event, $node_id, $metadata['id']);
    }
  }

  /**
   * Retrieves internal calendar event instance by node identifier.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Calendar $calendar
   *   Internal calendar instance.
   * @param int $node_id
   *   MoveRequest node identifier.
   *
   * @return \Drupal\move_google_api\Google\Calendar\Entities\Event
   *   Internal calendar event instance.
   *
   * @throws PDOException
   * @throws \Throwable
   */
  public function retrieveFromCalendarByNodeId(Calendar $calendar, $node_id): Event {
    $metadata = $this->metadataService->retrieveByNodeId($node_id);

    try {
      $remote_event = $this->googleService->events->get(
        $calendar->getRemoteCalendarId(),
        $metadata['remote_event_id']
      );
    }
    catch (\Throwable $exception) {
      if (count($errors = $exception->getErrors()) === 1
        && $errors[0]['reason'] === 'notFound'
      ) {
        throw new RuntimeException("Remote event with id='{$metadata['remote_event_id']}'"
          . " not found in remote calendar with id='{$calendar->getRemoteCalendarId()}'");
      }

      throw $exception;
    }

    return new Event($calendar, $remote_event, $node_id, $metadata['id']);
  }

  /**
   * Recalculates and updates MoveRequest calendar event.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Event $event
   *   Internal calendar event instance.
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   MoveRequest instance.
   *
   * @return \Drupal\move_google_api\Google\Calendar\Entities\Event
   *   Updated calendar event instance.
   */
  public function updateEvent(Event $event, MoveRequest $move_request): Event {
    $unsaved_remote_event = (new EventsDirector)->buildEvent(
      new MoveRequestEventBuilder($move_request)
    );

    $unsaved_remote_event->setAttendees($event->googleEvent->getAttendees());

    $event->googleEvent = $this->googleService->events->update(
      $event->calendar->getRemoteCalendarId(),
      $event->getRemoteEventId(),
      $unsaved_remote_event
    );

    $this->metadataService->updateForEvent($event);
    $this->updateEventAttendees($event, $move_request);

    return $event;
  }

  /**
   * Update event in google calendar.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Event $event
   *   Calendar event.
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   MoveRequest.
   */
  public function updateEventAttendees(Event $event, MoveRequest $move_request) : void {
    $attendees_emails = $move_request->retrieve()['service_type']['raw'] == RequestServiceType::FLAT_RATE
      ? self::getFlatRateForemansEmails($move_request)
      : self::getPlaneMovingForemansEmails($move_request);

    if ($helpers_email = $this->getRequestHelpersEmails($move_request)) {
      $attendees_emails += $helpers_email;
    }

    $event_attendees = $event->googleEvent->getAttendees();

    foreach ($event_attendees as $key => $event_attendee) {
      if (in_array($event_attendee->email, $attendees_emails, TRUE)) {
        $attendees_emails = array_diff($attendees_emails, [$event_attendee->email]);
      }
      else {
        unset($event_attendees[$key]);
      }
    }

    foreach ($attendees_emails as $attendee_email) {
      if (EmailsHelper::isEmailDomainHasGoogleMxRecords($attendee_email)) {
        $event_attendees[] = new GoogleAttendee(array('email' => $attendee_email));
      }
    }

    $event->googleEvent->setAttendees($event_attendees);

    $patch_options = array('sendNotifications' => TRUE);

    $this->googleService->events->update(
      $event->calendar->getRemoteCalendarId(),
      $event->getRemoteEventId(),
      $event->googleEvent,
      $patch_options
    );
  }

  /**
   * Get emails for foremans.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   MoveRequest.
   *
   * @return array
   *   Array wit emails.
   */
  private static function getRequestForemansEmails(MoveRequest $move_request) : array {
    $data = $move_request->retrieve();
    $foremans_emails = [];

    if (isset($data['field_foreman']['old']) && !empty($foremans_data = $data['field_foreman']['old'])) {
      if (!is_array($foremans_data) || isset($foremans_data['uid'])) {
        $foremans_data = [$foremans_data];
      }

      foreach ($foremans_data as $foreman_data) {
        if (!empty($foreman_data)) {
          try {
            $foreman_metadata = entity_metadata_wrapper('user', $foreman_data);
            $foreman_email = $foreman_metadata->field_google_calendar_email->value();

            if (!empty($foreman_email) && !in_array($foreman_email, $foremans_emails, TRUE)) {
              $foremans_emails[(int) $foreman_metadata->uid->value()] = $foreman_email;
            }
          }
          catch (\Throwable $exception) {
            // Todo log exception?
          }
        }
      }
    }

    return $foremans_emails;
  }

  /**
   * Get foremans ids.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   Move request.
   *
   * @return array
   *   Array with ids.
   */
  private static function getAssignedForemansIdsForFlatRate(MoveRequest $move_request) : array {
    $data = $move_request->retrieve();
    $assigned_foremans_ids = [];

    if (isset($data['request_data']['value']['crews'])) {
      foreach (['deliveryCrew', 'pickedUpCrew'] as $crew_type) {
        if (isset($data['request_data']['value']['crews'][$crew_type]['foreman'])
          && !empty($foreman_id = $data['request_data']['value']['crews'][$crew_type]['foreman'])
          && !in_array($foreman_id, $assigned_foremans_ids, TRUE)
        ) {
          $assigned_foremans_ids[] = (int) $foreman_id;
        }
      }
    }

    return $assigned_foremans_ids;
  }

  /**
   * Get foremans emails.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   MoveRequest.
   *
   * @return array
   *   Array with emails.
   */
  private static function getFlatRateForemansEmails(MoveRequest $move_request) : array {
    $assigned_foremans_ids = self::getAssignedForemansIdsForFlatRate($move_request);
    $foremans_emails = [];

    foreach (self::getRequestForemansEmails($move_request) as $foreman_id => $foreman_email) {
      if (in_array($foreman_id, $assigned_foremans_ids, TRUE)) {
        $foremans_emails[$foreman_id] = $foreman_email;
      }
    }

    return $foremans_emails;
  }

  /**
   * Get request helpers email.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   Move Request.
   *
   * @return array
   *   Array with emails.
   */
  private static function getRequestHelpersEmails(MoveRequest $move_request) : array {
    $helper_emails = [];
    $data = $move_request->retrieve();

    if (!empty($data['field_helper'])) {
      if (is_array($data['field_helper']['old'])) {
        foreach ($data['field_helper']['old'] as $helper) {
          $helper_wrapper = entity_metadata_wrapper('user', $helper);
          if (!empty($email = $helper_wrapper->field_google_calendar_email->value())) {
            $helper_emails[$helper] = $email;
          }
        }
      }
      elseif (is_object($helper = $data['field_helper']['old'])) {
        $helper_wrapper = entity_metadata_wrapper('user', $helper);
        if (!empty($email = $helper_wrapper->field_google_calendar_email->value())) {
          $helper_emails[$helper->uid] = $email;
        }
      }
    }

    return $helper_emails;
  }

  /**
   * Get foremans emails for simple request.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   MoveRequest.
   *
   * @return array
   *   Array with emails.
   */
  private static function getPlaneMovingForemansEmails(MoveRequest $move_request) : array {
    $assigned_foremans_ids = $foremans_emails = array();
    $data = $move_request->retrieve();

    if (isset($data['request_data']['value']['crews']['foreman'])
      && !empty($foreman_id = $data['request_data']['value']['crews']['foreman'])
    ) {
      $assigned_foremans_ids[] = (int) $foreman_id;
    }

    foreach (self::getRequestForemansEmails($move_request) as $foreman_id => $foreman_email) {
      if (in_array($foreman_id, $assigned_foremans_ids, TRUE)) {
        $foremans_emails[$foreman_id] = $foreman_email;
      }
    }

    return $foremans_emails;
  }

  /**
   * Moves event to another calendar.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Event $event
   *   Internal calendar event instance.
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Calendar $calendar
   *   Destination calendar instance.
   */
  public function moveEventToCalendar(Event $event, Calendar $calendar): void {
    $this->googleService->events->move(
      $event->calendar->getRemoteCalendarId(),
      $event->getRemoteEventId(),
      $calendar->getRemoteCalendarId()
    );

    $event->calendar = $calendar;

    $this->metadataService->updateForEvent($event);
  }

  /**
   * Shares GoogleCalendar event to user by email.
   *
   * Adds user to event as attendee.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Event $event
   *   Internal calendar event instance.
   * @param string $email
   *   Email address for sharing.
   */
  public function shareEventToEmail(Event $event, string $email): void {
    /** @var array $attendees */
    $attendees = $event->googleEvent->getAttendees();

    $attendees_emails = array_map(function (GoogleAttendee $attendee) {
      return $attendee->email;
    }, $attendees);

    if (!in_array($email, $attendees_emails, TRUE)) {
      $attendees = array_merge($attendees, array(
        new GoogleAttendee(array('email' => $email)),
      ));

      $event->googleEvent->setAttendees($attendees);

      $patch_options = array('sendNotifications' => TRUE);

      $this->googleService->events->update(
        $event->calendar->getRemoteCalendarId(),
        $event->getRemoteEventId(),
        $event->googleEvent,
        $patch_options
      );
    }
  }

  /**
   * Disables GoogleCalendarEvent sharing for user by email.
   *
   * Removes email from event attendees list.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Event $event
   *   Internal calendar event instance.
   * @param string $email
   *   Attendee email address.
   */
  public function disableSharingForEmail(Event $event, string $email): void {
    /** @var array $attendees */
    $attendees = $event->googleEvent->getAttendees();

    $attendees = array_filter($attendees, function (GoogleAttendee $attendee) use ($email) {
      return $attendee->email !== $email;
    });

    $event->googleEvent->setAttendees($attendees);

    $this->googleService->events->update(
      $event->calendar->getRemoteCalendarId(),
      $event->getRemoteEventId(),
      $event->googleEvent
    );
  }

}
