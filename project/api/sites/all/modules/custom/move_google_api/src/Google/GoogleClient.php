<?php

namespace Drupal\move_google_api\Google;

use Google_Service_Calendar as GoogleCalendarsService;


/**
 * Class AbstractGoogleService.
 *
 * @package Drupal\move_google_api\Google
 */
class GoogleClient extends \Google_Client {

  /**
   * GoogleClient instance.
   *
   * @var GoogleClient
   */
  private static $instance;

  /**
   * Scopes to be requested as part of the OAuth2.0 flow.
   *
   * @var array
   */
  private static $scopes = [
    GoogleCalendarsService::CALENDAR,
  ];

  /**
   * Construct the Google Client.
   *
   * @param array $config
   */
  public function __construct(array $config = array()) {
    parent::__construct($config);
  }

  /**
   * Returns GoogleClient instance.
   *
   * @return GoogleClient
   *   Configured GoogleClient instance.
   */
  public static function instance(): GoogleClient {
    if (NULL === self::$instance) {
      self::$instance = self::init();
    }

    return self::$instance;
  }

  /**
   * Initializes GoogleClient.
   *
   * @return GoogleClient
   *   Configured GoogleClient instance.
   */
  private static function init(): GoogleClient {
    self::setEnvironmentVariables();

    $google_client = new self();

    $google_client->useApplicationDefaultCredentials();

    if (!empty(self::$scopes)) {
      $google_client->addScope(static::$scopes);
    }

    return $google_client;
  }

  /**
   * Sets environment variables for GoogleClient.
   */
  private static function setEnvironmentVariables(): void {
    $credentials = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_google_api') . '/service_acc.json';

    putenv("GOOGLE_APPLICATION_CREDENTIALS={$credentials}");
  }

}
