<?php

namespace Drupal\move_google_api\Google\Calendar\Entities;

use Google_Service_Calendar_Event as GoogleEvent;

/**
 * Class Event.
 *
 * @package Drupal\move_google_api\Google\Calendar
 */
class Event {

  /**
   * Id.
   *
   * @var int
   */
  public $id;

  /**
   * Node id.
   *
   * @var int
   */
  public $node_id;

  /**
   * Calendar.
   *
   * @var \Drupal\move_google_api\Google\Calendar\Entities\Calendar
   */
  public $calendar;

  /**
   * Google event.
   *
   * @var \Google_Service_Calendar_Event
   */
  public $googleEvent;

  /**
   * Event constructor.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Calendar $calendar
   *   Calendar.
   * @param \Google_Service_Calendar_Event $google_event
   *   Google event.
   * @param int $node_id
   *   Node id.
   * @param int $id
   *   Calendar id.
   */
  public function __construct(Calendar $calendar, GoogleEvent $google_event, int $node_id, int $id) {
    $this->googleEvent = $google_event;
    $this->calendar = $calendar;
    $this->node_id = $node_id;
    $this->id = $id;
  }

  /**
   * Get remove google calendar id.
   *
   * @return string
   *   Google remove calendar id.
   */
  public function getRemoteEventId(): string {
    return $this->googleEvent->getId();
  }

}
