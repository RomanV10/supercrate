<?php

namespace Drupal\move_google_api\Google\Calendar\Exceptions;

use RuntimeException;

/**
 * Class MetadataNotFoundException.
 *
 * @package Drupal\move_google_api\Google\Calendar\Exceptions
 */
class MetadataNotFoundException extends RuntimeException {

  public function __construct($node_id) {
    parent::__construct($this->prepareMessage($node_id));
  }

  private function prepareMessage($node_id) {
    return "Calendar event metadata not found for node id='{$node_id}'";
  }

}
