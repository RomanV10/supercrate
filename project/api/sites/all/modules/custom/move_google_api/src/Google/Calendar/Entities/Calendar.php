<?php

namespace Drupal\move_google_api\Google\Calendar\Entities;

use Google_Service_Calendar_Calendar as GoogleCalendar;

/**
 * Class Calendar.
 *
 * @package Drupal\move_google_api\Google\Calendar
 */
class Calendar {

  const TYPE_PENDING = 'pending_calendar';

  const TYPE_CONFIRMED = 'confirmed_calendar';

  const TYPE_NOT_CONFIRMED = 'not_confirmed_calendar';

  const TYPE_ARCHIVE = 'archive_calendar';

  const TYPE_INHOME_ESTIMATE = 'inhome_estimate_calendar';


  private static $pending_statuses_ids = array(
    1,  // Pending
    7,  // Date Pending
    9,  // Flat Rate Step 1
    10, // Flat Rate Step 2
    11, // Flat Rate Step 3
    16, // Pre-pending
    17, // Pending-info
    19, // From Parser"
  );


  /**
   * @var int
   */
  public $id;

  /**
   * @var string
   */
  public $type;

  /**
   * @var GoogleCalendar
   */
  public $googleCalendar;

  /**
   * Calendar constructor.
   *
   * @param int $id
   * @param string $type
   * @param GoogleCalendar
   */
  public function __construct(int $id, string $type, GoogleCalendar $google_calendar) {
    $this->id = $id;
    $this->type = $type;
    $this->googleCalendar = $google_calendar;
  }

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getRemoteCalendarId(): string {
    return $this->googleCalendar->getId();
  }

  /**
   * @param $status_id
   * @return string
   */
  public static function getTypeForMoveRequestStatus(int $status_id): string {
    if (in_array($status_id, self::$pending_statuses_ids, TRUE)) {
      return self::TYPE_PENDING;
    }
    elseif ($status_id === 2) {
      return self::TYPE_NOT_CONFIRMED;
    }
    elseif ($status_id === 3) {
      return self::TYPE_CONFIRMED;
    }
    elseif ($status_id === 4) {
      return self::TYPE_INHOME_ESTIMATE;
    }

    return self::TYPE_ARCHIVE;
  }

}
