<?php

namespace Drupal\move_google_api\System;

/**
 * Class Service.
 *
 * @package Drupal\move_google_api\System
 */
class Service {

  /**
   * Function getResources.
   *
   * @return array
   *   Resources.
   */
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  /**
   * Private function definition.
   *
   * @return array
   *   Operations and actions.
   */
  private static function definition() {
    return array(
      'google_calendars' => array(
        'operations' => array(),
        'actions' => array(
          'get_status' => array(
            'callback' => 'Drupal\move_google_api\Google\Calendar\Controllers\CalendarsSettingsController::isEnabled',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_google_api',
              'name' => 'src/Google/Calendar/Controllers/CalendarsSettingsController',
            ),
            'access arguments' => array('access content'),
          ),
          'update_status' => array(
            'callback' => 'Drupal\move_google_api\Google\Calendar\Controllers\CalendarsSettingsController::setEnabled',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_google_api',
              'name' => 'src/Google/Calendar/Controllers/CalendarsSettingsController',
            ),
            'args' => array(
              array(
                'name' => 'is_enabled',
                'type' => 'bool',
                'source' => array('data' => 'is_enabled'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_shared_calendars' => array(
            'callback' => 'Drupal\move_google_api\Google\Calendar\Controllers\CalendarsSettingsController::getSharedCalendarsForUser',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_google_api',
              'name' => 'src/Google/Calendar/Controllers/CalendarsSettingsController',
            ),
            'args' => array(
              array(
                'name' => 'user_id',
                'type' => 'int',
                'source' => array('data' => 'uid'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

}
