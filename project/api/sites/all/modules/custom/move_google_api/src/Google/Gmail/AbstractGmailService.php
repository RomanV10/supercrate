<?php

namespace Drupal\move_google_api\Google\Gmail;

use Drupal\move_google_api\Google\AbstractGoogleService;
use Google_Service_Gmail as GoogleMailService;

/**
 * Class AbstractGmailService.
 *
 * @package Drupal\move_google_api\Google\Gmail
 */
abstract class AbstractGmailService extends AbstractGoogleService {

  /**
   * Scopes to be requested as part of the OAuth2.0 flow.
   *
   * @var array
   */
  protected static $requestedScopes = array(
    GoogleMailService::MAIL_GOOGLE_COM,
  );

  /**
   * Google service instance.
   *
   * @var GoogleMailService
   */
  protected $gmailService;

  /**
   * AbstractGmailService constructor.
   */
  public function __construct() {
    parent::__construct();

    $this->gmailService = new GoogleMailService($this->googleClient);
  }

}
