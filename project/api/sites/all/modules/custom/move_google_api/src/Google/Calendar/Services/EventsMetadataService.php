<?php

namespace Drupal\move_google_api\Google\Calendar\Services;

use Drupal\move_google_api\Google\Calendar\Entities\Calendar;
use Drupal\move_google_api\Google\Calendar\Entities\Event;
use Drupal\move_google_api\Google\Calendar\Exceptions\NodeEventMetadataNotFoundException;
use Google_Service_Calendar_Event as GoogleEvent;
use PDOException;
use RuntimeException;

/**
 * Class EventsMetadataService.
 *
 * @package Drupal\move_google_api\Google\Calendar\Services
 */
class EventsMetadataService {

  /**
   * Metadata table name.
   */
  const METADATA_TABLE = 'move_google_calendars_events';

  /**
   * Saves event internal metadata.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Calendar $calendar
   *   Internal calendar instance.
   * @param \Google_Service_Calendar_Event $remote_event
   *   Remote calendar event instance.
   * @param int $node_id
   *   Node identifier.
   *
   * @return array
   *   Metadata values.
   */
  public function createForCalendarAndRemoteEvent(Calendar $calendar, GoogleEvent $remote_event, int $node_id): array {
    $metadata = array(
      'calendar_id'     => $calendar->getId(),
      'remote_event_id' => $remote_event->getId(),
      'node_id'         => $node_id,
      'last_updated'    => time(),
    );

    $metadata['id'] = (int) db_insert(self::METADATA_TABLE)
      ->fields($metadata)->execute();

    return $metadata;
  }

  /**
   * Retrieves event internal metadata by node identifier.
   *
   * @param int $node_id
   *   MoveRequest node identifier.
   *
   * @return array
   *   Metadata values.
   *
   * @throws RuntimeException
   * @throws PDOException
   */
  public function retrieveByNodeId(int $node_id): array {
    $condition = db_and()->condition('node_id', $node_id);

    $select_result = db_select(self::METADATA_TABLE, 'meta')
      ->fields('meta')->condition($condition)->execute();

    if ($select_result->rowCount()) {
      return $select_result->fetchAssoc();
    }

    throw new RuntimeException('Internal calendar event metadata not found'
      . " for MoveRequest with id='{$node_id}'");
  }

  /**
   * Updates event internal metadata.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Event $event
   *   Internal calendar event instance.
   *
   * @return array
   *   Metadata values.
   */
  public function updateForEvent(Event $event): array {
    $metadata = array(
      'calendar_id'     => $event->calendar->getId(),
      'remote_event_id' => $event->getRemoteEventId(),
      'node_id'         => $event->node_id,
      'last_updated'    => time(),
    );

    $condition = db_and()->condition('node_id', $event->node_id);
    db_merge(self::METADATA_TABLE)->condition($condition)
      ->fields($metadata)->execute();

    return $metadata;
  }

  /**
   * Get node calendar type.
   *
   * @param int $node_id
   *   Node id.
   *
   * @return mixed
   *   Calendar type.
   */
  public function getNodeCalendarType(int $node_id) {
    $query = db_select('move_google_calendars_events', 'events');
    $query->join('move_google_calendars', 'calendars', 'events.calendar_id = calendars.id');
    $select_result = $query
      ->condition('events.node_id', $node_id)
      ->fields('calendars', array('type'))
      ->execute();

    if ($select_result->rowCount()) {
      return $select_result->fetchField();
    }

    throw new NodeEventMetadataNotFoundException($node_id);
  }

}
