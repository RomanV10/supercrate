<?php

namespace Drupal\move_google_api\Google\Calendar\Controllers;

use Drupal\move_google_api\Google\Calendar\Services\CalendarsMetadataService;
use Drupal\move_google_api\Google\Calendar\Services\CalendarsService;

/**
 * Class CalendarsSettingsController.
 *
 * @package Drupal\move_google_api\Google\Calendar\Controllers
 */
class CalendarsSettingsController {

  /**
   * @return array
   */
  public static function isEnabled(): array {
    return array('result' => array('is_enabled' => CalendarsService::isEnabled()));
  }

  /**
   * @param bool $is_enabled
   */
  public static function setEnabled(bool $is_enabled): void {
    CalendarsService::setEnabled($is_enabled);
  }

  /**
   * @param int $user_id
   *
   * @return array
   */
  public static function getSharedCalendarsForUser(int $user_id): array {
    $shared_types = (new CalendarsMetadataService)->getSharedCalendarsTypesForUser($user_id);

    return array('result' => array('shared_google_calendars' => $shared_types));
  }

}
