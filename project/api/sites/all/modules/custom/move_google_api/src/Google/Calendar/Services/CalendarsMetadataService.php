<?php

namespace Drupal\move_google_api\Google\Calendar\Services;

use Drupal\move_google_api\Google\Calendar\Entities\Calendar;
use PDOException;
use RuntimeException;

/**
 * Class CalendarsMetadataRepository.
 *
 * @package Drupal\move_google_api\Google\Calendar\Services
 */
class CalendarsMetadataService {

  /**
   * Calendars metadata table name.
   */
  const METADATA_TABLE = 'move_google_calendars';

  /**
   * Sharing rules metadata table name.
   */
  const SHARING_METADATA_TABLE = 'move_google_calendars_sharing_rules';


  /**
   * @param array $metadata
   * @return array
   */
  public function create(array $metadata) {
    $insert_result = db_insert(self::METADATA_TABLE)
      ->fields($metadata)->execute();

    $metadata['id'] = (int) $insert_result;

    return $metadata;
  }

  /**
   * Retrieves Calendar metadata by type.
   *
   * @param string $type
   *   Internal calendar type.
   *
   * @return array
   *   Calendar metadata.
   *
   * @throws RuntimeException
   * @throws PDOException
   */
  public function retrieveByType(string $type): array {
    $select_result = db_select(self::METADATA_TABLE, 'meta')
      ->condition('type', $type)
      ->fields('meta', array('id', 'remote_calendar_id'))
      ->execute();

    if ($select_result->rowCount()) {
      return $select_result->fetchAssoc();
    }

    throw new RuntimeException("Metadata for '{$type}' calendar not found ");
  }

  /**
   * Retrieves array of shared calendars types identifiers for user.
   *
   * @param int $user_id
   *   User identifier.
   *
   * @return array
   *   Array of shared calendars types identifiers.
   */
  public function getSharedCalendarsTypesForUser(int $user_id): array {
    return array_keys($this->getCalendarsSharesIdsKeyedByTypesForUser($user_id));
  }

  public function getCalendarsSharesIdsKeyedByTypesForUser(int $user_id): array {
    $query = db_select(self::SHARING_METADATA_TABLE, 'shares');

    $query->join(self::METADATA_TABLE, 'calendars', 'shares.calendar_id = calendars.id');

    $raw_select_result = $query
      ->condition('shares.user_id', $user_id)
      ->fields('calendars', array('type'))
      ->fields('shares', array('remote_rule_id'))
      ->execute();

    if ($raw_select_result->rowCount()) {
      return $raw_select_result->fetchAllKeyed();
    }

    return [];
  }

  /**
   * Retrieves sharing rule identifier from metadata.
   *
   * @param Calendar $calendar
   *   Internal calendar instance.
   * @param int $user_id
   *   User identifier.
   *
   * @return string|null
   *   Sharing rule identifier or NULL if not defined.
   *
   * @throws RuntimeException
   * @throws PDOException
   */
  public function retrieveCalendarSharingRuleIdForUser(Calendar $calendar, int $user_id): ?string {
    $condition = db_and()->condition('calendar_id', $calendar->getId())
      ->condition('user_id', $user_id);

    $select_result = db_select(self::SHARING_METADATA_TABLE, 'rules')
      ->fields('rules', array('remote_rule_id'))
      ->condition($condition)
      ->execute();

    if ($select_result->rowCount()) {
      return $select_result->fetchField();
    }

    throw new RuntimeException("Sharing rule metadata for user with id='{$user_id}' not found ");
  }

  /**
   * Creates GoogleCalendar sharing rule metadata.
   *
   * @param Calendar $calendar
   *   Internal calendar instance.
   * @param int $user_id
   *   User identifier.
   * @param string $remote_rule_id
   *   Remote sharing rule identifier.
   *
   * @throws PDOException
   */
  public function saveCalendarSharingMetadataForUser(Calendar $calendar, int $user_id, string $remote_rule_id): void {
    $metadata = array(
      'remote_rule_id' => $remote_rule_id,
      'calendar_id'    => $calendar->getId(),
      'user_id'        => $user_id,
    );

    db_insert(self::SHARING_METADATA_TABLE)
      ->fields($metadata)
      ->execute();
  }

  /**
   * Removes sharing rule metadata.
   *
   * @param Calendar $calendar
   *   Internal calendar instance.
   * @param int $user_id
   *   User identifier.
   * @param string $remote_rule_id
   *   Remote sharing rule identifier.
   *
   * @throws PDOException
   */
  public function removeCalendarSharingMetadataForUserByRemoteRuleId(Calendar $calendar, int $user_id, string $remote_rule_id): void {
    $condition = db_and()->condition('user_id', $user_id)
      ->condition('remote_rule_id', $remote_rule_id)
      ->condition('calendar_id', $calendar->getId());

    db_delete(self::SHARING_METADATA_TABLE)->condition($condition)->execute();
  }

}
