<?php

namespace Drupal\move_google_api\Helpers;

/**
 * Class EmailsHelper.
 *
 * @package Drupal\move_google_api\Helpers
 */
class EmailsHelper {

  const GOOGLE_MAIL_HOST = 'gmail.com';

  const GOOGLE_MAIL_TARGETS_DOMAINS = [
    '.GOOGLE.com',
    '.google.com',
    '.googlemail.com',
    '.psmtp.com',
  ];

  /**
   * Checks if an email domain has Google mx records.
   *
   * @param string $email
   *   Email address for checking.
   *
   * @return bool
   *   Checking result.
   */
  public static function isEmailDomainHasGoogleMxRecords(string $email): bool {
    if (!empty($email)) {
      [$user, $domain] = explode('@', $email);

      if ($domain === self::GOOGLE_MAIL_HOST) {
        return TRUE;
      }

      foreach (dns_get_record($domain, DNS_MX) as $mx_record) {
        if ($mx_record['host'] === self::GOOGLE_MAIL_HOST) {
          return TRUE;
        }

        foreach (self::GOOGLE_MAIL_TARGETS_DOMAINS as $gmail_domain) {
          if (strpos($mx_record['target'], $gmail_domain) !== FALSE) {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }

}
