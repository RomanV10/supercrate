<?php

namespace Drupal\move_google_api\Google\Calendar\EventsBuilder;

use Google_Service_Calendar_Event as GoogleCalendarEvent;

/**
 * Class BuilderInterface.
 *
 * @package Drupal\move_google_api\Google\Calendar\EventsBuilder
 */
interface BuilderInterface {

  /**
   * @return GoogleCalendarEvent
   */
  public function build(): GoogleCalendarEvent;

}
