<?php

namespace Drupal\move_google_api\Google\Calendar;

use Drupal\move_google_api\Google\AbstractGoogleService;
use Google_Service_Calendar as GoogleCalendarsService;

/**
 * Class AbstractGoogleCalendarService.
 *
 * @package Drupal\move_google_api\Google\Calendar
 */
abstract class AbstractGoogleCalendarService extends AbstractGoogleService {

  /**
   * Scopes to be requested as part of the OAuth2.0 flow.
   *
   * @var array
   */
  protected static $requestedScopes = array(
    GoogleCalendarsService::CALENDAR,
  );

  /**
   * Google service instance.
   *
   * Manipulates events and other calendar data.
   *
   * @var GoogleCalendarsService
   */
  protected $googleService;

  /**
   * AbstractGoogleCalendarService constructor.
   */
  public function __construct() {
    parent::__construct();

    $this->googleService = new GoogleCalendarsService($this->googleClient);
  }

}
