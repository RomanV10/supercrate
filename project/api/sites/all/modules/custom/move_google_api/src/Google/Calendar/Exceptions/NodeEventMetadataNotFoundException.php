<?php

namespace Drupal\move_google_api\Google\Calendar\Exceptions;

use RuntimeException;

/**
 * Class NodeEventMetadataNotFoundException.
 *
 * @package Drupal\move_google_api\Google\Calendar\Exceptions
 */
class NodeEventMetadataNotFoundException extends RuntimeException {

  public function __construct($node_id, $code = 0, \Throwable $previous = NULL) {
    parent::__construct($this->prepareMessage($node_id), $code, $previous);
  }

  private function prepareMessage($node_id) {
    return "Calendar event metadata not found for node id='{$node_id}'";
  }
}
