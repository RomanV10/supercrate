<?php

namespace Drupal\move_google_api\Google;

use Google_Client as GoogleClient;

/**
 * Class AbstractGoogleService.
 *
 * @package Drupal\move_google_api\Google
 */
abstract class AbstractGoogleService {

  /**
   * Scopes to be requested as part of the OAuth2.0 flow.
   *
   * @var array
   */
  protected static $requestedScopes = array();

  protected $googleClient;

  /**
   * AbstractGoogleCalendarService constructor.
   */
  public function __construct() {
    $this->googleClient = self::initGoogleClient();

//    $this->googleCalendarsService = new CalendarsService(self::initGoogleClient());
  }

  /**
   * Initializes GoogleClient.
   *
   * TODO use global singleton.
   *
   * @return GoogleClient
   *   Configured GoogleClient instance.
   */
  private static function initGoogleClient(): GoogleClient {
    self::setEnvironmentVariables();

    $google_client = new GoogleClient();

    $google_client->useApplicationDefaultCredentials();

    if (!empty(static::$requestedScopes)) {
      $google_client->addScope(static::$requestedScopes);
    }

    return $google_client;
  }

  /**
   * Sets environment variables for GoogleClient.
   */
  private static function setEnvironmentVariables(): void {
    $credentials = DRUPAL_ROOT . '/' . drupal_get_path('module', 'move_google_api') . '/service_acc.json';

    putenv("GOOGLE_APPLICATION_CREDENTIALS={$credentials}");
  }

}
