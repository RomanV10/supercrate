<?php

namespace Drupal\move_google_api\Google\Calendar\EventsBuilder;

use Carbon\Carbon;
use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;
use Drupal\move_template_builder\Services\TemplateBuilder;
use Google_Service_Calendar_Event as GoogleCalendarEvent;

/**
 * Class MoveRequestEventBuilder.
 *
 * @package Drupal\move_google_api\Google\Calendar\EventsBuilder
 */
class MoveRequestEventBuilder implements BuilderInterface {

  /**
   * Move request data.
   *
   * @var array|mixed
   */
  protected $data;

  private $startDate = '';
  private $startTime = '';
  private $endDate = '';
  private $endTime = '';

  /**
   * MoveRequestEventBuilder constructor.
   *
   * @param MoveRequest $move_request
   *   Move request data.
   */
  public function __construct(MoveRequest $move_request) {
    $this->data = $move_request->retrieve();
  }

  /**
   * Prepare data to send in calendar.
   *
   * @return GoogleCalendarEvent
   *   Data.
   *
   * @throws \InvalidArgumentException
   */
  public function build(): GoogleCalendarEvent {
    $this->setDates();
    $starts_at = $this->getStartsAt();
    $ends_at = $this->getEndsAt($starts_at);

    return new GoogleCalendarEvent(array(
      'summary' => $this->getSummary(),
      'location' => $this->getShortLocation(),
      'description' => $this->getDescription(),
      'start' => array('dateTime' => $starts_at->toRfc3339String()),
      'end' => array('dateTime' => $ends_at->toRfc3339String()),
    ));
  }

  /**
   * @return Carbon
   *
   * @throws \InvalidArgumentException
   */
  protected function getStartsAt(): Carbon {
    $date = Carbon::createFromTimestampUTC($this->startDate)->startOfDay();
    $time = Carbon::createFromFormat('h:i A', $this->startTime, 'UTC');

    return (new Carbon)->setTimezone(self::getTimezone())->setDate($date->year, $date->month, $date->day)
      ->setTime($time->hour, $time->minute);
  }

  /**
   * @param Carbon $starts_at
   *
   * @return Carbon
   *
   * @throws \InvalidArgumentException
   */
  protected function getEndsAt(Carbon $starts_at): Carbon {
    $end_time = Carbon::createFromFormat('H:i', $this->endTime, 'UTC');

    return (clone $starts_at)->addHours($end_time->hour)->addMinutes($end_time->minute);
  }

  /**
   * Set date and time depend on status.
   */
  private function setDates() : void{
    if ($this->data['status']['raw'] == RequestStatusTypes::INHOME_ESTIMATE) {
      $this->startDate = $this->data['home_estimate_date']['raw'];
      $this->startTime = $this->data['home_estimate_actual_start']['value'];

      $duration = !empty($this->data['home_estimate_time_duration']['raw']) ? $this->data['home_estimate_time_duration']['raw'] : 0.5;
      $this->endTime = Extra::convertDecimalToDate($duration, 'H:i');
    }
    else {
      $this->startDate = $this->data['date']['raw'];
      $this->startTime = $this->data['start_time1']['value'];
      $this->endTime = Extra::convertDecimalToDate($this->data['travel_time']['raw'] + $this->data['maximum_time']['raw'], 'H:i');
    }
  }

  /**
   * @return string
   */
  protected function getSummary() {
    return "#{$this->data['nid']} | {$this->data['name']}";
  }

  /**
   * @return string
   */
  protected function getShortLocation() {
    $move_service_type = $this->data['service_type']['raw'];

    $from = $this->data['field_moving_from'];
    $to = $this->data['field_moving_to'];

    $moving_from = "{$from['locality']}, {$from['administrative_area']}, {$from['postal_code']}";
    $moving_to = "{$to['locality']}, {$to['administrative_area']}, {$to['postal_code']}";

    if ($move_service_type == "2") {
      // Moving & Storage.
      if (filter_var($this->data['request_all_data']['toStorage'], FILTER_VALIDATE_BOOLEAN)) {
        return "Storage -> {$moving_to}";
      }
      else {
        return "{$moving_from} -> Storage";
      }
    }
    elseif ($move_service_type == "3") {
      // Loading help.
      return $moving_from;
    }
    elseif ($move_service_type == "4") {
      // Unloading help.
      return $moving_to;
    }

    return "{$moving_from} -> {$moving_to}";
  }

  /**
   * Get request description in google calendar.
   *
   * @return string
   *   Description.
   *
   * @throws \Exception
   */
  protected function getDescription() : string {
    if ($this->data['status']['raw'] == RequestStatusTypes::INHOME_ESTIMATE) {
      $description = $this->buildDescriptionHomeEstimate();
    }
    else {
      $description = $this->buildDescriptionNotHomeEstimate();
    }

    return $description;
  }

  /**
   * Build description for home estimate status types.
   *
   * @return string
   *   String with description.
   *
   * @throws \Exception
   */
  private function buildDescriptionHomeEstimate() : string {
    $fuel_surcharge = number_format($this->data['request_all_data']['surcharge_fuel'], 2);

    $move_service_type = $this->data['service_type']['raw'];

    if ($move_service_type == "7") {
      $long_distance_rate = (new DistanceCalculate)->getLongDistanceRate($this->data);
      $rate = "{$this->wrapLabel('Rate:')} $ {$long_distance_rate} per c.f.
      ";
    }
    elseif ($move_service_type == "5") {
      $rate = '';
    }
    else {
      $rate = "{$this->wrapLabel('Hourly Rate:')} $ {$this->getHourlyRate()} /hr
      ";
    }

    $notesStripTags = $this->prepareNotes($this->data['nid']);

    return "
      {$this->wrapLabel('Request:')} #{$this->data['nid']}
      {$this->wrapLabel('Status:')} {$this->data['status']['value']}
      {$this->wrapLabel('Move Date:')} {$this->data['date']['value']}
      {$this->wrapLabel('Start Time:')} {$this->data['start_time1']['value']} - {$this->data['start_time2']['value']}
      {$this->wrapLabel('Travel Time:')} {$this->data['travel_time']['value']}
      {$this->wrapLabel('Crew Size:')} {$this->data['crew']['value']} movers
      {$rate}{$this->wrapLabel('Fuel Surcharge:')} $ {$fuel_surcharge}
      {$this->wrapLabel('Type of Service:')} {$this->data['service_type']['value']}
      {$this->wrapLabel('Size of Move:')} {$this->data['move_size']['value']}
      {$this->wrapLabel('Est. Time:')} {$this->data['minimum_time']['value']} - {$this->data['maximum_time']['value']}
      {$this->wrapLabel('Grand Total:')} {$this->getGrandTotal()}
      {$this->wrapLabel('Reservation Rate:')} {$this->data['reservation_rate']['value']}
      {$this->wrapLabel('Reservation Received:')} {$this->data['field_reservation_received']['text']}
      
      {$this->getFullLocation()}
      {$this->wrapLabel('Distance:')} {$this->data['distance']['value']} miles
     
      {$this->wrapLabel('Home Estimate Move Date:')} {$this->data['home_estimate_date']['value']}
      {$this->wrapLabel('Home Estimate Start Time:')} {$this->data['home_estimate_actual_start']['value']} - {$this->data['home_estimate_start_time']['value']}
      {$this->wrapLabel('Home Estimator:')} {$this->data['home_estimator']['first_name']} {$this->data['home_estimator']['last_name']}
      {$this->wrapLabel('Home Estimate assigned by:')} {$this->data['home_estimate_assigned']['value']}
      {$this->wrapLabel('Home Estimate Status:')} {$this->data['home_estimate_status']['raw']}
      {$this->wrapLabel('Home Estimate Duration:')} {$this->data['home_estimate_time_duration']['value']}
      
      {$this->wrapLabel('Customer:')} {$this->data['name']}
      {$this->wrapLabel('Phone:')} {$this->data['phone']}
      {$this->wrapLabel('Email:')} {$this->data['email']}
      
      {$this->wrapLabel('Sales notes:')} {$notesStripTags['sales_notes']}
      {$this->wrapLabel('Foreman notes:')} {$notesStripTags['foreman_notes']}
      {$this->wrapLabel('Client notes:')} {$notesStripTags['client_notes']}
      {$this->wrapLabel('Dispatch notes:')} {$notesStripTags['dispatch_notes']}";
  }

  /**
   * Build description for not home estimate status types.
   *
   * @return string
   *   String with description.
   *
   * @throws \Exception
   */
  private function buildDescriptionNotHomeEstimate() : string {
    $fuel_surcharge = 0.00;
    if (!empty($this->data['request_all_data']['surcharge_fuel'])) {
      $fuel_surcharge = number_format($this->data['request_all_data']['surcharge_fuel'], 2);
    }

    $move_service_type = $this->data['service_type']['raw'];

    if ($move_service_type == "7") {
      $long_distance_rate = (new DistanceCalculate)->getLongDistanceRate($this->data);
      $rate = "{$this->wrapLabel('Rate:')} $ {$long_distance_rate} per c.f.
      ";
    }
    elseif ($move_service_type == "5") {
      $rate = '';
    }
    else {
      $rate = "{$this->wrapLabel('Hourly Rate:')} $ {$this->getHourlyRate()} /hr
      ";
    }

    $notesStripTags = $this->prepareNotes($this->data['nid']);

    return "
      {$this->wrapLabel('Request:')} #{$this->data['nid']} 
      {$this->wrapLabel('Status:')} {$this->data['status']['value']}
      {$this->wrapLabel('Move Date:')} {$this->data['date']['value']}
      {$this->wrapLabel('Start Time:')} {$this->data['start_time1']['value']} - {$this->data['start_time2']['value']}
      {$this->wrapLabel('Travel Time:')} {$this->data['travel_time']['value']}
      {$this->wrapLabel('Crew Size:')} {$this->data['crew']['value']} movers
      {$rate}{$this->wrapLabel('Fuel Surcharge:')} $ {$fuel_surcharge}
      {$this->wrapLabel('Type of Service:')} {$this->data['service_type']['value']}
      {$this->wrapLabel('Size of Move:')} {$this->data['move_size']['value']}
      {$this->wrapLabel('Est. Time:')} {$this->data['minimum_time']['value']} - {$this->data['maximum_time']['value']}
      {$this->wrapLabel('Grand Total:')} {$this->getGrandTotal()}
      {$this->wrapLabel('Reservation Rate:')} {$this->data['reservation_rate']['value']}
      {$this->wrapLabel('Reservation Received:')} {$this->data['field_reservation_received']['text']}
      
      {$this->getFullLocation()}
      {$this->wrapLabel('Distance:')} {$this->data['distance']['value']} miles
      
      {$this->wrapLabel('Customer:')} {$this->data['name']}
      {$this->wrapLabel('Phone:')} {$this->data['phone']}
      {$this->wrapLabel('Email:')} {$this->data['email']}
      
      {$this->wrapLabel('Sales notes:')} {$notesStripTags['sales_notes']}
      {$this->wrapLabel('Foreman notes:')} {$notesStripTags['foreman_notes']}
      {$this->wrapLabel('Client notes:')} {$notesStripTags['client_notes']}
      {$this->wrapLabel('Dispatch notes:')} {$notesStripTags['dispatch_notes']}";
  }

  /**
   * Prepare notes for google calendar.
   *
   * @param int $nid
   *   Move request id.
   *
   * @return array
   *   Prepared notes.
   *
   * @throws \Exception
   */
  private function prepareNotes($nid) {
    $inst = new MoveRequest($nid);
    $notes = $inst->get_notes(
      ['sales_notes', 'foreman_notes', 'client_notes', 'dispatch_notes']);

    return [
      'sales_notes' => strip_tags($notes->sales_notes),
      'foreman_notes' => strip_tags($notes->foreman_notes),
      'client_notes' => strip_tags($notes->client_notes),
      'dispatch_notes' => strip_tags($notes->dispatch_notes),
    ];
  }

  /**
   * Get full location data.
   *
   * @return string
   *   Location data.
   */
  private function getFullLocation() {
    $move_service_type = $this->data['service_type']['raw'];

    $moving_from = "{$this->getFullAddressFrom()}
      {$this->wrapLabel('Ent. Type From:')} {$this->data['type_from']['value']}";

    $moving_to = "{$this->getFullAddressTo()}
      {$this->wrapLabel('Ent. Type To:')} {$this->data['type_to']['value']}";

    $extraPickup = "
      {$this->getExtraPickupAddress()}
      {$this->wrapLabel('Ent. Extra Pickup:')} {$this->data['field_extra_pickup']['entrance']}";

    $extraDropoff = "{$this->getExtraDropoffAddress()}
      {$this->wrapLabel('Ent. Extra Pickup:')} {$this->data['field_extra_dropoff']['entrance']}";

    if ($move_service_type == "2") {
      // Moving & Storage.
      if (filter_var($this->data['request_all_data']['toStorage'], FILTER_VALIDATE_BOOLEAN)) {
        return "{$this->wrapLabel('Moving From: Storage')}
      {$moving_to}";
      }
      else {
        return "{$moving_from}
      {$this->wrapLabel('Moving To: Storage')}";
      }
    }
    elseif ($move_service_type == "3") {
      // Loading help.
      return $moving_from;
    }
    elseif ($move_service_type == "4") {
      // Unloading help.
      return $moving_to;
    }

    return "{$moving_from}
      {$moving_to}
      {$extraPickup}
      {$extraDropoff}";
  }

  /**
   * Build full from address.
   *
   * @return string
   *   Return full address string.
   */
  private function getFullAddressFrom() : string {
    $apt_from = !empty($this->data['apt_from']['value']) ? "(Apt #{$this->data['apt_from']['value']})" : '';
    $adr_from = !empty($this->data['adrfrom']['value']) ? "{$this->data['adrfrom']['value']}" : '';
    $address_from = "$adr_from $apt_from";
    $geo_location_from = "{$this->data['field_moving_from']['locality']} {$this->data['field_moving_from']['administrative_area']} {$this->data['field_moving_from']['postal_code']}";
    if (!empty($apt_from) || !empty($adr_from)) {
      $full_address = "{$this->wrapLabel('Moving From:')}
      {$address_from}
      {$geo_location_from}";
    }
    else {
      $full_address = "{$this->wrapLabel('Moving From:')} $geo_location_from";
    }
    return $full_address;
  }

  /**
   * Build full to address.
   *
   * @return string
   *   Return full address string.
   */
  private function getFullAddressTo()  : string {
    $apt_to = !empty($this->data['apt_to']['value']) ? "(Apt #{$this->data['apt_to']['value']})" : '';
    $adr_to = !empty($this->data['adrto']['value']) ? "{$this->data['adrto']['value']}" : '';
    $address_to = "$adr_to $apt_to";
    $geo_location_to = "{$this->data['field_moving_to']['locality']} {$this->data['field_moving_to']['administrative_area']} {$this->data['field_moving_to']['postal_code']}";
    if (!empty($apt_to) || !empty($adr_to)) {
      $full_address = "{$this->wrapLabel('Moving To:')}
      {$address_to}
      {$geo_location_to}";
    }
    else {
      $full_address = "{$this->wrapLabel('Moving To:')} $geo_location_to";
    }
    return $full_address;
  }

  private function getGrandTotal() {
    // логику скомуниздил с TemplateBuilder на скорую руку.
    $move_service_type = $this->data['service_type']['raw'];

    if ($move_service_type == "7") {
      // Long Distance.
      $grand_total = (new TemplateBuilder())->getLongDistanceGrandTotal($this->data);

      return '$ ' . number_format($grand_total, 2);
    }
    elseif ($move_service_type == "5") {
      // Flat Rate.
      $grand_total = (new TemplateBuilder())->calculateFlatRateGrandTotal($this->data);

      return '$ ' . number_format($grand_total, 2);
    }
    else {
      // Other(Move Requests).
      $calculate_settings = json_decode(variable_get('calcsettings', array()));
      $is_enable_double_drive_time = ($calculate_settings->doubleDriveTime && isset($this->data['field_double_travel_time']['raw']));

      return (new TemplateBuilder())->getEstimatedPrice($this->data, TRUE, TRUE, $is_enable_double_drive_time);
    }

  }

  private function getHourlyRate() {
    if (isset($this->data['request_all_data']['add_rate_discount'])
      && (($rate = $this->data['request_all_data']['add_rate_discount']) > 0)
    ) {
      return $rate;
    }
    elseif (isset($this->data['rate']['value'])) {
      return $this->data['rate']['value'];
    }

    return 0;
  }

  /**
   * Get timezone.
   *
   * @return \DateTimeZone
   *   Timezone.
   */
  private static function getTimezone(): \DateTimeZone {
    // TODO: add env() helper with fallback.
    $tz = empty($tz = getenv('MOVE_GOOGLE_CALENDAR_TIMEZONE')) ? 'UTC' : $tz;

    return new \DateTimeZone($tz);
  }

  /**
   * Prepare wrap label.
   *
   * @param string $label
   *   Data for wrap.
   *
   * @return string
   *   Wrapped data.
   */
  private function wrapLabel(string $label) : string {
    return "<strong>$label</strong>";
  }

  /**
   * Get extra pickup address.
   *
   * @return string
   *   Pickup address data.
   */
  protected function getExtraPickupAddress() {
    $apt_from = !empty($this->data['field_extra_pickup']['premise']) ? "(Apt #{$this->data['field_extra_pickup']['premise']})" : '';
    $adr_from = !empty($this->data['field_extra_pickup']['thoroughfare']) ? "{$this->data['field_extra_pickup']['thoroughfare']}" : '';
    $address_from = "$adr_from $apt_from";
    $geo_location_from = "{$this->data['field_extra_pickup']['locality']} {$this->data['field_extra_pickup']['administrative_area']} {$this->data['field_extra_pickup']['postal_code']}";
    if (!empty($apt_from) || !empty($adr_from)) {
      $full_address = "{$this->wrapLabel('Extra Pickup:')}
      {$address_from}
      {$geo_location_from}";
    }
    else {
      $full_address = "{$this->wrapLabel('Extra Pickup:')} $geo_location_from";
    }
    return $full_address;
  }

  /**
   * Get extra dropoff address.
   *
   * @return string
   *   Dropoff address data.
   */
  protected function getExtraDropoffAddress() {
    $apt_from = !empty($this->data['field_extra_dropoff']['premise']) ? "(Apt #{$this->data['field_extra_dropoff']['premise']})" : '';
    $adr_from = !empty($this->data['field_extra_dropoff']['thoroughfare']) ? "{$this->data['field_extra_dropoff']['thoroughfare']}" : '';
    $address_from = "$adr_from $apt_from";
    $geo_location_from = "{$this->data['field_extra_dropoff']['locality']} {$this->data['field_extra_dropoff']['administrative_area']} {$this->data['field_extra_dropoff']['postal_code']}";
    if (!empty($apt_from) || !empty($adr_from)) {
      $full_address = "{$this->wrapLabel('Extra Dropoff:')}
      {$address_from}
      {$geo_location_from}";
    }
    else {
      $full_address = "{$this->wrapLabel('Extra Dropoff:')} $geo_location_from";
    }
    return $full_address;
  }

}
