<?php

namespace Drupal\move_google_api\Google\Calendar\Services;

use Drupal\move_google_api\Google\Calendar\AbstractGoogleCalendarService;
use Drupal\move_google_api\Google\Calendar\Entities\Calendar;
use Drupal\move_google_api\Google\Calendar\Exceptions\NodeEventMetadataNotFoundException;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Google_Service_Calendar_AclRule as GoogleCalendarAclRule;
use Google_Service_Calendar_AclRuleScope as GoogleCalendarAclRuleScope;
use Google_Service_Calendar_Calendar as GoogleCalendar;
use Google_Service_Exception as GoogleServiceException;
use LogicException;
use PDOException;
use RuntimeException;

/**
 * Class CalendarsService.
 *
 * @package Drupal\move_google_api\Google\Calendar\Services
 */
class CalendarsService extends AbstractGoogleCalendarService {

  /**
   * Is GoogleCalendars enabled flag variable name.
   */
  const IS_ENABLED_FLAG_VARIABLE = 'move_google_calendars_is_enabled';

  /**
   * Default calendars definition.
   *
   * @var array
   */
  public static $defaultCalendars = array(
    Calendar::TYPE_ARCHIVE => 'archive',
    Calendar::TYPE_PENDING => 'pending',
    Calendar::TYPE_CONFIRMED => 'confirmed',
    Calendar::TYPE_NOT_CONFIRMED => 'not confirmed',
    Calendar::TYPE_INHOME_ESTIMATE => 'home estimate',
  );

  /**
   * Calendars metadata manipulation service.
   *
   * @var \Drupal\move_google_api\Google\Calendar\Services\CalendarsMetadataService
   */
  private $metadataService;

  /**
   * CalendarsService constructor.
   */
  public function __construct() {
    parent::__construct();

    $this->metadataService = new CalendarsMetadataService();
  }

  /**
   * Retrieves GoogleCalendars status.
   *
   * @return bool
   *   Is GoogleCalendars enabled flag.
   *
   * @throws PDOException
   */
  public static function isEnabled(): bool {
    return variable_get(self::IS_ENABLED_FLAG_VARIABLE, FALSE);
  }

  /**
   * Updates GoogleCalendars status.
   *
   * @param bool $is_enabled
   *   Is GoogleCalendars enabled flag.
   *
   * @throws \Google_Service_Exception
   */
  public static function setEnabled(bool $is_enabled): void {
    variable_set(self::IS_ENABLED_FLAG_VARIABLE, $is_enabled);

    if ($is_enabled) {
      $company_name = json_decode(variable_get('basicsettings', array()), TRUE)['company_name'];

      (new self)->initDefaults($company_name);
    }
    else {
      (new self())->removeCalendars();
    }
  }

  /**
   * Creates empty default calendars if not exists.
   *
   * @param string $company_name
   *   Calendar title prefix.
   */
  public function initDefaults(string $company_name): void {
    foreach (self::$defaultCalendars as $type => $definition) {
      try {
        $this->createForType($type, "{$company_name} {$definition}");
      }
      catch (\Throwable $exception) {
        continue;
      }
    }
  }

  /**
   * Creates calendar. Returns instance.
   *
   * @param string $type
   *   Internal calendar type.
   * @param string $summary
   *   Calendar summary (company name and definition).
   *
   * @return \Drupal\move_google_api\Google\Calendar\Entities\Calendar
   *   Calendar instance.
   */
  public function createForType(string $type, string $summary): Calendar {
    try {
      $remote_calendar_id = $this->retrieveByType($type)->getRemoteCalendarId();

      throw new LogicException("GoogleCalendar with id='{$remote_calendar_id}'"
        . " and type='{$type}' already exists");
    }
    catch (\Throwable $exception) {
      $remote_calendar = $this->googleService->calendars->insert(
        new GoogleCalendar(array('summary' => $summary))
      );

      $metadata = $this->metadataService->create([
        'remote_calendar_id' => $remote_calendar->getId(),
        'type' => $type,
      ]);

      return new Calendar($metadata['id'], $type, $remote_calendar);
    }
  }

  /**
   * Retrieves calendar instance by internal type.
   *
   * @param string $type
   *   Internal calendar type.
   *
   * @return \Drupal\move_google_api\Google\Calendar\Entities\Calendar
   *   Calendar instance.
   *
   * @throws \Throwable
   */
  public function retrieveByType(string $type): Calendar {
    $metadata = $this->metadataService->retrieveByType($type);

    try {
      $remote_calendar = $this->googleService->calendars->get(
        $metadata['remote_calendar_id']
      );
    }
    catch (\Throwable $exception) {
      $errors_count = count($errors = $exception->getErrors());
      if ($errors_count === 1 && $errors[0]['reason'] === 'notFound') {
        throw new RuntimeException('Remote GoogleCalendar not found by'
          . " id='{$metadata['remote_calendar_id']}'");
      }

      throw $exception;
    }

    return new Calendar($metadata['id'], $type, $remote_calendar);
  }

  /**
   * Updates calendars sharing rules for user.
   *
   * @param int $user_id
   *   User identifier.
   * @param string $email
   *   User email for sharing.
   * @param array $types_for_sharing
   *   Array of types for sharing.
   *
   * @throws GoogleServiceException
   * @throws RuntimeException
   * @throws PDOException
   */
  public function updateCalendarsSharesForUser(int $user_id, string $email, array $types_for_sharing) {
    $calendars_shares = $this->metadataService->getCalendarsSharesIdsKeyedByTypesForUser($user_id);
    $is_changed = $this->isCalendarTypesForSharingWasChanged($calendars_shares, $types_for_sharing, $email);

    if ($is_changed) {
      foreach ($types_for_sharing as $calendar_type) {
        $calendar = $this->retrieveByType($calendar_type);

        if (isset($calendars_shares[$calendar->type])) {
          $remote_rule = $this->googleService->acl->get($calendar->getRemoteCalendarId(), $calendars_shares[$calendar->type]);
          $scope = $remote_rule->getScope()->getValue();
          if ($scope !== $email) {
            $this->disableCalendarSharingForUser($calendar, $user_id);
            $this->shareCalendarForUserByEmail($calendar, $user_id, $email);
          }
        }
        else {
          $this->shareCalendarForUserByEmail($calendar, $user_id, $email);
        }
      }

      foreach (array_diff(array_keys($calendars_shares), $types_for_sharing) as $calendar_type) {
        $this->disableCalendarSharingForUser($this->retrieveByType($calendar_type), $user_id);
      }
    }

  }

  /**
   * Shares Calendar for user by email.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Calendar $calendar

   *   Internal calendar instance.
   * @param int $user_id
   *   User identifier.
   * @param string $email
   *   User email for sharing.
   *
   * @throws PDOException
   */
  public function shareCalendarForUserByEmail(Calendar $calendar, int $user_id, string $email): void {
    $rule = $this->googleService->acl->insert(
      $calendar->getRemoteCalendarId(),
      $this->prepareCalendarSharingRuleForEmail($email)
    );

    $this->metadataService->saveCalendarSharingMetadataForUser(
      $calendar,
      $user_id,
      $rule->getId()
    );
  }

  /**
   * Disable google calendar for users.
   *
   * @param int $user_id
   *   User id.
   * @param array $calendars_types
   *   Calendar types.
   *
   * @throws \Google_Service_Exception
   */
  public function disableCalendarsSharingForUser(int $user_id, array $calendars_types = ['*']): void {
    if (in_array('*', $calendars_types, TRUE)) {
      $calendars_types = $this->metadataService->getSharedCalendarsTypesForUser($user_id);
    }

    foreach ($calendars_types as $calendar_type) {
      $this->disableCalendarSharingForUser($this->retrieveByType($calendar_type), $user_id);
    }
  }

  /**
   * Disables GoogleCalendar sharing for user.
   *
   * @param \Drupal\move_google_api\Google\Calendar\Entities\Calendar $calendar
   *   Calendar instance.
   * @param int $user_id
   *   User id.
   */
  public function disableCalendarSharingForUser(Calendar $calendar, int $user_id): void {
    $remote_rule_id = $this->metadataService->retrieveCalendarSharingRuleIdForUser($calendar, $user_id);
    $this->googleService->acl->delete($calendar->getRemoteCalendarId(), $remote_rule_id);
    $this->metadataService->removeCalendarSharingMetadataForUserByRemoteRuleId($calendar, $user_id, $remote_rule_id);
  }

  /**
   * Prepares sharing rule by email.
   *
   * @param string $email
   *   Email for sharing.
   *
   * @return \Google_Service_Calendar_AclRule
   *   GoogleCalendarAclRule.
   */
  private function prepareCalendarSharingRuleForEmail(string $email): GoogleCalendarAclRule {
    $rule = new GoogleCalendarAclRule(['role' => 'reader']);
    $rule->setScope(new GoogleCalendarAclRuleScope(['type' => 'user', 'value' => $email]));

    return $rule;
  }

  /**
   * Remove calendars.
   */
  private function removeCalendars() : void {
    $environment = variable_get('environment', []);
    foreach (self::$defaultCalendars as $type => $definition) {
      try {
        if ($environment['default'] === 'production') {
          $remote_calendar_id = $this->metadataService->retrieveByType($type)['remote_calendar_id'];
          $this->googleService->calendars->delete($remote_calendar_id);
        }
      }
      catch (\Throwable $exception) {
        continue;
      }
    }

    db_truncate('move_google_calendars')->execute();
    db_truncate('move_google_calendars_sharing_rules')->execute();
    db_truncate('move_google_calendars_events')->execute();
  }

  /**
   * Check if calendar types was changed.
   *
   * @param array $current_types
   *   Current types.
   * @param array $new_types
   *   New types.
   * @param string $email
   *   New email.
   *
   * @return bool
   *   TRUE of FALSE
   */
  public function isCalendarTypesForSharingWasChanged(array $current_types, array $new_types, string $email) : bool {
    $calendar_types = [];
    $this->prepareCalendarSharingRuleForEmail($email);

    $new_share_mail_string = "user:$email";
    foreach ($current_types as $calendar_type => $shared_mail_string) {
      if ($shared_mail_string != $new_share_mail_string || !in_array($calendar_type, $new_types)) {
        $calendar_types[] = $calendar_type;
      }
    }

    $calendar_types += array_diff($new_types, array_keys($current_types));

    return !empty($calendar_types) ? TRUE : FALSE;
  }

  /**
   * Create move request event.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   Move request instance.
   *
   * @throws \Google_Service_Exception
   */
  public function createMoveRequestEvent(MoveRequest $move_request) : void {
    $data = $move_request->retrieve();
    $move_request_calendar_type = Calendar::getTypeForMoveRequestStatus($data['status']['raw']);
    $calendar = $this->retrieveByType($move_request_calendar_type);
    (new MoveRequestsEventsService)->createInCalendar($calendar, $move_request);
  }

  /**
   * Update move request event.
   *
   * @param \Drupal\move_services_new\Services\move_request\MoveRequest $move_request
   *   Move request instance.
   *
   * @throws GoogleServiceException
   * @throws \Throwable
   */
  public function updateMoveRequestEvent(MoveRequest $move_request) {
    $data = $move_request->retrieve();
    $nid = $data['nid'];

    try {
      $calendar_type = (new EventsMetadataService)->getNodeCalendarType($nid);
    }
    catch (\Throwable $exception) {
      return;
    }

    $calendar = $this->retrieveByType($calendar_type);
    $events_service_instance = new MoveRequestsEventsService();
    $event = $events_service_instance->retrieveFromCalendarByNodeId($calendar, $nid);
    $events_service_instance->updateEvent($event, $move_request);
    $move_request_calendar_type = Calendar::getTypeForMoveRequestStatus($data['status']['raw']);

    if ($calendar->type !== $move_request_calendar_type) {
      $destination_calendar = $this->retrieveByType($move_request_calendar_type);
      $events_service_instance->moveEventToCalendar($event, $destination_calendar);
    }
  }

}
