<?php

namespace Drupal\move_google_api\Google\Calendar\EventsBuilder;

use Google_Service_Calendar_Event as GoogleCalendarEvent;

/**
 * Class Director.
 *
 * @package Drupal\move_google_api\Google\Calendar\EventsBuilder
 */
class EventsDirector {

  /**
   * @param BuilderInterface $builder
   *
   * @return GoogleCalendarEvent
   */
  public function buildEvent(BuilderInterface $builder): GoogleCalendarEvent {
    return $builder->build();
  }

}
