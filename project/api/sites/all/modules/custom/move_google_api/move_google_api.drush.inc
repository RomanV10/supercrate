<?php

use Drupal\move_google_api\Google\Calendar\Services\CalendarsService;

/*
 * Implementation of hook_drush_command().
 */
function move_google_api_drush_command() {
  $items['disable-google-calendar'] = array(
    'description' => 'Disable google calendar in system.',
    'callback' => 'drush_move_google_api_disable_google_calendar',
    'arguments' => [
      'is_enable' => 'Enable/Disable google calendar',
    ],
    'examples' => array(
      'Enable google calendar' => 'drush dgc 1',
      'Disable google calendar' => 'drush dgc 0',
    ),
    'aliases' => array('dgc'),
  );

  return $items;
}

/**
 * Callback function drush_move_google_api_disable_google_calendar()
 * The call back function name in the  following format
 *   drush_{module_name}_{item_id_for_command}()
 */
function drush_move_google_api_disable_google_calendar($is_enable = 0) {
  CalendarsService::setEnabled((bool) $is_enable);
}
