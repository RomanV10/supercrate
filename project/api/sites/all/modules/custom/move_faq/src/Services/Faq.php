<?php

namespace Drupal\move_faq\Services;

use Drupal\move_services_new\Services\BaseService;
use Throwable;

class Faq extends BaseService {

  public $faqId = 0;

  /**
   * Faq constructor.
   *
   * @param int $faq_id
   *   FAQ id.
   */
  public function __construct($faq_id = 0) {
    $this->faqId = $faq_id;
  }

  /**
   * Method to create single faq record and save it in db.
   *
   * @param array $data
   *   Array data of faq question and faq answer.
   *
   * @return int
   *   Unique record id in 'move_faq' db table.
   */
  public function create($data = array()) {
    $txn = db_transaction();
    try {
      $faq = db_insert('move_faq')
        ->fields(array(
          'faq_question' => $data['faq_question'],
          'faq_answer' => $data['faq_answer'],
          'created' => time(),
        ))
        ->execute();

      return $faq;
    }
    catch (Throwable $e) {
      $txn->rollback();
      watchdog('move_faq', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Method to retrieve single faq.
   *
   * @return array
   *   Retrieve faq question and faq answer data keyed by faq id.
   */
  public function retrieve() {
    $result = db_select('move_faq', 'mf')
      ->fields('mf', array(
        'id',
        'faq_question',
        'faq_answer',
        'usefulness_upvote',
        'usefulness_downvote'
      ))
      ->condition('mf.id', $this->faqId)
      ->execute()
      ->fetchAll();

    return $result;
  }

  /**
   * Method to update faq.
   *
   * @param array $data
   *   Array data of faq question and faq answer.
   *
   * @return int
   *   Returns number of faq rows affected by update
   */
  public function update($data = array()) {
    $txn = db_transaction();
    try {
      $result = db_update('move_faq')
        ->fields(array(
          'faq_question' => $data['faq_question'],
          'faq_answer' => $data['faq_answer'],
          'created' => time(),
        ))
        ->condition('id', $data['faq_id'])
        ->execute();

      return $result;

    }
    catch (Throwable $e) {
      $txn->rollback();
      watchdog('move_faq', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Method to delete faq from db.
   *
   * @return int
   *   Returns number of faq rows affected bu delete.
   */
  public function delete() {
    $txn = db_transaction();
    try {
      $result = db_delete('move_faq')
        ->condition('id', $this->faqId)
        ->execute();

      return $result;

    }
    catch (Throwable $e) {
      $txn->rollback();
      watchdog('move_faq', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Method return all faqs.
   *
   * @return array
   *   Return all faqs.
   */
  public function index() {
    $faqs = db_select('move_faq', 'mf')
      ->fields('mf')
      ->orderBy('mf.faq_question', 'ASC')
      ->execute()
      ->fetchAll();

    return $faqs;
  }

  /**
   * Method upvote the usefulness of FAQ.
   *
   * @param int $faq_id
   *   Integer FAQ ID.
   *
   * @return int
   *   Returns number of affected faq rows for updating the usefulness.
   */
  public function upvote(int $faq_id) {
    $txn = db_transaction();
    try {
      $result = db_update('move_faq')
        ->expression('usefulness_upvote', 'usefulness_upvote + :amount', array(':amount' => 1))
        ->condition('id', $faq_id)
        ->execute();

      return $result;

    }
    catch (Throwable $e) {
      $txn->rollback();
      watchdog('move_faq', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Method downvote the usefulness of FAQ.
   *
   * @param int $faq_id
   *   Integer FAQ ID.
   *
   * @return int
   *   Returns number of affected faq rows for updating the usefulness.
   */
  public function downvote(int $faq_id) {
    $txn = db_transaction();
    try {
      $result = db_update('move_faq')
        ->expression('usefulness_downvote', 'usefulness_downvote + :amount', array(':amount' => 1))
        ->condition('id', $faq_id)
        ->execute();

      return $result;

    }
    catch (Throwable $e) {
      $txn->rollback();
      watchdog('move_faq', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }

  /**
   * Method return all faqs sorted by their usefulness score.
   *
   * @return array
   *   Return all sorted faqs.
   */
  public function indexByScore() {
    $faqs = db_select('move_faq', 'mf')
      ->fields('mf')
      ->orderBy('mf.usefulness_upvote', 'DESC')
      ->orderBy('mf.usefulness_downvote', 'DESC')
      ->orderBy('mf.faq_question', 'ASC')
      ->execute()
      ->fetchAll();

    return $faqs;
  }

}