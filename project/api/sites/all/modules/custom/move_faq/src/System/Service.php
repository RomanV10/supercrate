<?php

namespace Drupal\move_faq\System;

use Drupal\move_faq\Services\Faq;

/**
 * Class Service.
 *
 * @package Drupal\move_faq\System
 */
class Service {

  /**
   * Function getResources.
   *
   * @return array
   *   Resources.
   */
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  /**
   * Private function definition.
   *
   * @return array
   *   Operations and actions.
   */
  private static function definition() {
    return array(
      'move_faq' => array(
        'operations' => array(
          'create' => array(
            'help' => 'Creates a single FAQ entry',
            'callback' => 'Drupal\move_faq\System\Service::createSingleFaq',
            'file' => array(
              'type' => 'php',
              'module' => 'move_faq',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'faq_question',
                'type' => 'string',
                'source' => array('data' => 'faq_question'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'faq_answer',
                'type' => 'string',
                'source' => array('data' => 'faq_answer'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'help' => 'Retrieves a single FAQ entry',
            'callback' => 'Drupal\move_faq\System\Service::getSingleFaq',
            'file' => array(
              'type' => 'php',
              'module' => 'move_faq',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'faq_id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'help' => 'Updates a single FAQ entry',
            'callback' => 'Drupal\move_faq\System\Service::updateSingleFaq',
            'file' => array(
              'type' => 'php',
              'module' => 'move_faq',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'faq_id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'faq_question',
                'type' => 'string',
                'source' => array('data' => 'faq_question'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'faq_answer',
                'type' => 'string',
                'source' => array('data' => 'faq_answer'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'help' => 'Deletes a single FAQ entry',
            'callback' => 'Drupal\move_faq\System\Service::deleteSingleFaq',
            'file' => array(
              'type' => 'php',
              'module' => 'move_faq',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'faq_id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'help' => 'Retrieves all FAQ entries sorted asc by faq question',
            'callback' => 'Drupal\move_faq\System\Service::indexFaqs',
            'file' => array(
              'type' => 'php',
              'module' => 'move_faq',
              'name' => 'src\System\Service',
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'upvote_faq_usefulness' => array(
            'help' => 'Rates the FAQ usefulness',
            'file' => array('type' => 'php', 'module' => 'move_faq', 'name' => 'src/System/Service'),
            'callback' => 'Drupal\move_faq\System\Service::upvoteFaqUsefulness',
            'access arguments' => array('access content'),
            'args' => array(
              array(
                'name' => 'faq_id',
                'optional' => FALSE,
                'source' => array('data' => 'faq_id'),
                'description' => t('FAQ ID to update'),
                'type' => 'int',
              ),
            ),
          ),
          'downvote_faq_usefulness' => array(
            'help' => 'Rates the FAQ usefulness',
            'file' => array('type' => 'php', 'module' => 'move_faq', 'name' => 'src/System/Service'),
            'callback' => 'Drupal\move_faq\System\Service::downvoteFaqUsefulness',
            'access arguments' => array('access content'),
            'args' => array(
              array(
                'name' => 'faq_id',
                'optional' => FALSE,
                'source' => array('data' => 'faq_id'),
                'description' => t('FAQ ID to update'),
                'type' => 'int',
              ),
            ),
          ),
          'faqs_sorted_by_usefulness' => array(
            'help' => 'Retrieves all FAQ entries sorted desc by usefulness score',
            'file' => array('type' => 'php', 'module' => 'move_faq', 'name' => 'src/System/Service'),
            'callback' => 'Drupal\move_faq\System\Service::indexFaqsByScore',
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  /**
   * Callback function for faq create.
   *
   * @param string $faq_question
   *   FAQ Question.
   * @param string $faq_answer
   *   FAQ Answer.
   *
   * @return int
   *   Integer id of the database record if created successfully.
   */
  public static function createSingleFaq(string $faq_question, string $faq_answer) {
    $data = array(
      'faq_question' => $faq_question,
      'faq_answer' => $faq_answer,
    );
    return (new Faq())->create($data);
  }

  /**
   * Callback function for faq retrieve.
   *
   * @param int $faq_id
   *   FAQ ID.
   *
   * @return array
   *   Array data of retrieve faq.
   */
  public static function getSingleFaq(int $faq_id) {
    return (new Faq($faq_id))->retrieve();
  }

  /**
   * Callback function for faq update.
   *
   * @param int $faq_id
   *   FAQ ID.
   * @param string $faq_question
   *   FAQ Question.
   * @param string $faq_answer
   *   FAQ Answer.
   *
   * @return int
   *   Number of affected rows in the database after update.
   */
  public static function updateSingleFaq(int $faq_id, string $faq_question, string $faq_answer) {
    $data = array(
      'faq_id' => $faq_id,
      'faq_question' => $faq_question,
      'faq_answer' => $faq_answer,
    );
    return (new Faq())->update($data);
  }

  /**
   * Callback function for delete faq.
   *
   * @param int $faq_id
   *   Unique id of faq.
   *
   * @return int
   *   Number of affected rows in database after delete.
   */
  public static function deleteSingleFaq(int $faq_id) {
    return (new Faq($faq_id))->delete();
  }

  /**
   * Callback function retrieve all faqs.
   *
   * @return array
   *   All faqs.
   */
  public static function indexFaqs() {
    return (new Faq())->index();
  }

  /**
   * Callback function to upvote a faq.
   *
   * @param int $faq_id
   *   Unique id of faq.
   *
   * @return int
   *   Update result.
   */
  public static function upvoteFaqUsefulness(int $faq_id) {
    return (new Faq())->upvote($faq_id);
  }

  /**
   * Callback function to downvote a faq.
   *
   * @param int $faq_id
   *   Unique id of faq.
   *
   * @return int
   *   Update result.
   */
  public static function downvoteFaqUsefulness(int $faq_id) {
    return (new Faq())->downvote($faq_id);
  }

  /**
   * Callback function retrieve all faqs sorted by usefulness score.
   *
   * @return array
   *   All faqs sored by usefulness score.
   */
  public static function indexFaqsByScore() {
    return (new Faq())->indexByScore();
  }

}
