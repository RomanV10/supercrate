<?php

namespace Drupal\move_parking\Util\enum;

use Drupal\move_services_new\Util\BaseEnum;

/**
 * Class TruckUnavailableType.
 *
 * @package Drupal\move_parking\Util
 */
class TruckUnavailableType extends BaseEnum {
  const MANUALLY = 0;
  const FROM_REQUEST = 1;
}
