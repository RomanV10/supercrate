<?php

namespace Drupal\move_parking\Services;

use Drupal\move_long_distance\Services\LongDistanceTrip;
use Drupal\move_parking\Util\enum\TruckUnavailableType;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\move_request\MoveRequestRetrieve;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_services_new\System\Extra;
use Drupal\move_services_new\Util\enum\RequestServiceType;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_services_new\Services\Settings;


use Drupal\move_services_new\Util\enum\TripStatus;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use Drupal\move_branch\Services\Trucks;

/**
 * Class Parking.
 *
 * @package Drupal\move_parking\Services
 */
class Parking extends BaseService {
  const TRUCKS_VOCABULARY  = 8;

  private $pid = NULL;
  private $encoders;
  private $normalizers;
  private $serializer;
  public $tripAllowedStatusForTruckUnavailable = [TripStatus::ACTIVE, TripStatus::DELIVERED];

  /**
   * Parking constructor.
   *
   * @param null $pid
   *   Parking id.
   */
  public function __construct($pid = NULL) {
    $this->pid = $pid;
    $this->encoders = [new JsonEncoder()];
    $this->normalizers = [new ObjectNormalizer()];
    $this->serializer = new Serializer($this->normalizers, $this->encoders);

  }

  /**
   * Set parking id.
   *
   * @param int $pid
   *   Parking id.
   */
  public function setId(int $pid) {
    $this->pid = $pid;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function create($data = []) {
    $error_flag = FALSE;
    $error_message = "";
    $result = [];
    try {
      $parking = $data;
      $fill_db = ['move_parking_chosen_trucks' => $parking['parking_truck']];
      unset($parking['parking_truck']);

      $parking_id = db_insert('move_parking')
        ->fields($parking)
        ->execute();

      if (!empty($parking_id)) {
        $this->pid = $parking_id;
        // Filling the related tables.
        db_insert('move_parking_chosen_trucks')
          ->fields(['pid' => $this->pid, 'truck_id' => $data['parking_truck']['truck_id']])
          ->execute();

        $result = $this->retrieve();

        // Add truck unavailable for trip.
        if (!empty($result['truck_id']) && $result['entity_type'] == EntityTypes::LDTRIP) {
          $log_title = 'Truck "' . $data['parking_truck']['truck_name'] . '" was added';
          (new LongDistanceTrip($result['entity_id']))->setLogs($log_title, []);

          $dates = $this->dateDiffInDateArray($result['from_date'], $result['to_date']);
          $trip_status = LongDistanceTrip::getTripStatus($result['entity_id']);

          if (in_array($trip_status, $this->tripAllowedStatusForTruckUnavailable)) {
            $this->addTruckUnavailableForTrip($result['truck_id'], $dates);
          }
        }

        $result['full_info'] = self::getParkingEntityFullInfo($data['entity_id'], $data['entity_type']);
      }
    }
    catch (\Throwable $e) {
      $message = "Create parking Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Create parking', $message, [], WATCHDOG_CRITICAL);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Get all indexes.
   *
   * @param bool $from_date
   *   Date from.
   * @param bool $to_date
   *   Date to.
   * @param bool $by_time
   *   Flag select by time.
   * @param bool $full_info
   *   Flag show full info.
   * @param bool $trucks_only
   *   Flag show only trucks.
   * @param bool $only_trailer
   *   Flag show only trucks.
   * @param array $conditions
   *   Differnt conditions.
   *
   * @return array
   *   Array with data.
   *
   * @throws \InvalidMergeQueryException
   *
   * @throws \ServicesException
   */
  public function index($from_date = FALSE, $to_date = FALSE, bool $by_time = FALSE, bool $full_info = FALSE, bool $trucks_only = TRUE, bool $only_trailer = FALSE, array $conditions = []) {
    $parking = [];
    $temp_parking = [];

    $query = db_select('move_parking', 'mp');
    $query->leftJoin('move_parking_chosen_trucks', 'mpct', 'mp.pid = mpct.pid');
    $query->fields('mp');
    $query->fields('mpct');
    if (empty($to_date)) {
      $to_date = time();
    }
    if (empty($from_date)) {
      $from_date = time();
    }
    if (!empty($from_date) && !empty($to_date)) {
      $db_or = db_or();
      $db_and = db_and();
      $db_and2 = db_and();
      $date_from = strtotime(date('Y-m-d', $from_date) . ' 00:00') - 2;
      $date_to = strtotime(date('Y-m-d', $to_date) . ' 23:59') + 2;

      $db_and->condition('from_date', $date_from, '>=');
      $db_and->condition('from_date', $date_to, '<=');

      $db_and2->condition('from_date', $date_from, '<=');
      $db_and2->condition('to_date', $date_from, '>=');

      $db_or->condition($db_and);
      $db_or->condition($db_and2);

      $query->condition($db_or);

    }
    if (!empty($by_time)) {
      $time_start = date('H:i', $from_date);
      $time_end = date('H:i', $to_date);
      $query->condition('start_time_1', $time_start, '>=');
      $query->condition('end_time', $time_end, '<=');
    }

    if (!empty($conditions)) {
      foreach ($conditions as $field_name => $val) {
        if (!empty($val)) {
          $operator = '=';
          if (is_array($val)) {
            $operator = 'IN';
          }
          $query->condition($field_name, $val, $operator);

        }
      }
    }
    $table = new Settings();
    $all_trucks = $table->getTrucks();
    // If enable branches parklot.
    if (variable_get('is_common_branch_truck', FALSE)) {
      $instance = new Trucks();
      $branch_trucks = $instance->index();
      if (!empty($branch_trucks['data'])) {
        $all_trucks += $branch_trucks['data'];
      }
    }

    $result = $query->execute();
    if (!empty($result)) {
      $request = new MoveRequest();
      foreach ($result as $row) {
        if (!empty($row->pid)) {
          $truck_id = (int) $row->truck_id;
          $row->from_date = !empty($row->from_date) ? $row->from_date : $row->date;
          $row->to_date = !empty($row->to_date) ? $row->to_date : $row->date;
          $row->start_time_1 = !empty($row->start_time_1) ? $row->start_time_1 : '00:00';
          $row->end_time = !empty($row->end_time) ? $row->end_time : '23:59';
          // Convert date to simple date.
          $date_from = date('Y-m-d', $row->from_date) . ' ' . $row->start_time_1;
          $date_to = date('Y-m-d', $row->to_date) . ' ' . $row->end_time;

          $row->from_date = strtotime($date_from);
          $row->to_date = strtotime($date_to);
          $parking[$truck_id]['truck_id'] = $row->truck_id;
          $parking[$truck_id]['truck_name'] = $all_trucks[$row->truck_id]['name'];
          $parking[$truck_id]['field_ld_only'] = $all_trucks[$row->truck_id]['field_ld_only'];

          $parking[$truck_id]['unavailable_from_request'] = $all_trucks[$truck_id]['unavailable_from_request'];
          $parking[$truck_id]['unavailable'] = $all_trucks[$truck_id]['unavailable'];

          $temp[$row->entity_id][$row->entity_type] = $row;
          // If need to show not only trucks name and id.
          // And show full info for request or trip.
          if (empty($trucks_only)) {
            if (!empty($full_info)) {
              $row->full_info = self::getParkingEntityFullInfo($row->entity_id, $row->entity_type, $request);
            }
            $parking[$truck_id]['items'][] = (array) $row;
            $parking[$truck_id]['truck_name'] = $all_trucks[$truck_id]['name'];
          }
        }
      }

    }
    if (!empty($trucks_only)) {
      $parking = array_values($parking);
    }
    else {
      // Convert all trucks array to needed view.
      foreach ($all_trucks as $key => $truck) {
        $all_trucks[$key]['truck_name'] = $truck['name'];
        $all_trucks[$key]['truck_id'] = $key;
        unset($all_trucks[$key]['name']);
      }
      $parking = array_replace($all_trucks, $parking);
      // Sorting items inside array.
      foreach ($parking as $key => $value) {
        if (!empty($value['items'])) {
          uasort($value['items'], ['self', '_cmp']);
          $value['items'] = array_values($value['items']);
        }
        if (!empty($value['field_ld_only'])) {
          $temp_parking['trailers'][$key] = $value;
        }
        else {
          $temp_parking['trucks'][$key] = $value;
        }
      }
      // If don't need to show trailer.
      if ($only_trailer && isset($temp_parking['trucks'])) {
        unset($temp_parking['trucks']);
      }
      $parking = $temp_parking;
    }
    return $parking;
  }

  /**
   * Retrieve on parking info by his id.
   *
   * @return array|mixed
   *   Error or array with values.
   *
   * @throws \ServicesException
   */
  public function retrieve() {
    $error_flag = FALSE;
    $error_message = "";
    $result = [];
    try {
      $query = db_select('move_parking', 'mp');
      $query->leftJoin('move_parking_chosen_trucks', 'mpct', 'mp.pid = mpct.pid');
      $query->fields('mp');
      $query->fields('mpct');
      $query->condition('mp.pid', $this->pid);
      $result = $query->execute()->fetchAssoc(\PDO::FETCH_ASSOC);
    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Parking retrieve', $message, [], WATCHDOG_ERROR);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $result;
    }
  }

  /**
   * Update parking.
   *
   * @param array $data
   *   Array of values for save.
   *
   * @return bool|mixed
   *   True if everything is good.
   *
   * @throws \ServicesException
   */
  public function update($data = []) {
    $error_flag = FALSE;
    $error_message = "";
    try {
      $parking = $data;
      $trucks = $parking['parking_truck'];
      unset($parking['parking_truck']);

      $this->setParkingIdByEntity($data['entity_id'], $data['entity_type']);
      $this->mergeParkingInDb($parking);

      if (!empty($this->pid)) {
        $this->mergeTrucksForParkingDb($trucks);
      }

    }
    catch (\Throwable $e) {
      $message = "Updating Parking {$this->pid}: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Parking update', $message, [], WATCHDOG_ERROR);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : TRUE;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @return mixed|void
   *   Error on nothing.
   */
  public function delete() {
    $fill_db = ['move_parking_chosen_trucks'];

    db_delete('move_parking')
      ->condition('pid', $this->pid)
      ->execute();
    // Remove the related tables.
    foreach ($fill_db as $table_name) {
      db_delete($table_name)
        ->condition('pid', $this->pid)
        ->execute();
    }
  }

  /**
   * @TODO not use nodeFields. Check if cache is available.
   * Add or remove request parking.
   *
   * @param \stdClass $node
   *   Node object.
   *
   * @throws \Exception
   */
  public static function syncRequestParking(\stdClass $node) : void {
    if (!empty($node->field_list_truck)) {
      $node_wrapper = entity_metadata_wrapper('node', $node);
      $request = new MoveRequestRetrieve($node_wrapper);
      $request_retrieve = $request->nodeFields();
      self::addRequestToParking($request_retrieve);
    }
    else {
      self::removeEntityFromParking($node->nid);
    }
  }

  /**
   * Insert or update entity in to parking db.
   *
   * @param array $parking
   *   Array with parking data.
   */
  private function mergeParkingInDb(array $parking) : void {
    if (empty($this->pid)) {
      $pid = db_insert('move_parking')
        ->fields($parking)
        ->execute();
      $this->pid = $pid;
    }
    else {
      db_update('move_parking')
        ->fields($parking)
        ->condition('pid', $this->pid)
        ->execute();
    }
  }

  /**
   * Delete old trucks for parking. Insert new.
   *
   * @param array $trucks
   *   Array with trucks.
   */
  private function mergeTrucksForParkingDb(array $trucks) : void {
    db_delete('move_parking_chosen_trucks')
      ->condition('pid', $this->pid)
      ->execute();

    $insert_query = db_insert('move_parking_chosen_trucks');
    $insert_query->fields(['pid', 'truck_id']);

    foreach ($trucks as $truck) {
      $insert_query->values([
        'pid' => $this->pid,
        'truck_id' => $truck->tid,
      ]);
    }

    $insert_query->execute();
  }

  /**
   * Add one reqeuest to parking.
   *
   * @param array $request_retrieve
   *   Node object.
   *
   * @throws \Exception
   */
  public static function addRequestToParking($request_retrieve) {
    $parking_inst = new Parking();

    if (empty($request_retrieve['trucks']['value'])) {
      throw new \Exception("Track is empty");
    }
    else {
      $trucks = is_array($request_retrieve['trucks']['value']) ? $request_retrieve['trucks']['value'] : [$request_retrieve['trucks']['value']];

      $from_date = empty($request_retrieve['delivery_date_from']['raw']) ? $request_retrieve['date']['raw'] : $request_retrieve['delivery_date_from']['raw'];
      $to_date = empty($request_retrieve['delivery_date_to']['raw']) ? $request_retrieve['date']['raw'] : $request_retrieve['delivery_date_to']['raw'];

      $fields_for_save = [
        'entity_id' => $request_retrieve['nid'],
        'entity_type' => EntityTypes::MOVEREQUEST,
        'date' => (int) $request_retrieve['date']['raw'],
        'from_date' => (int) $from_date,
        'to_date' => (int) $to_date,
        'type' => (int) $request_retrieve['service_type']['raw'],
        'parking_truck' => $trucks,
        'start_time_1' => $request_retrieve['start_time1']['value'],
        'start_time_2' => $request_retrieve['start_time2']['value'],
        'travel_time' => $request_retrieve['travel_time']['value'],
        'max_pickup_time' => $request_retrieve['maximum_time']['value'],
        'status' => $request_retrieve['status']['raw'],
        'end_time' => date('H:i', strtotime($request_retrieve['start_time1']['value']) + strtotime($request_retrieve['travel_time']['value']) + strtotime($request_retrieve['maximum_time']['value']) - strtotime("00:00:00")),
      ];

      $parking_inst->update($fields_for_save);
    }
  }

  /**
   * Set parking id by entity id and type.
   *
   * @param int $entity_id
   *   Id of entity.
   * @param int $entity_type
   *   Type of entity.
   */
  public function setParkingIdByEntity(int $entity_id, int $entity_type) {
    $pid = db_select('move_parking', 'mp')
      ->fields('mp', ['pid'])
      ->condition('entity_id', $entity_id)
      ->condition('entity_type', $entity_type)
      ->execute()
      ->fetchField();
    if (!empty($pid)) {
      $this->pid = $pid;
    }
  }

  /**
   * Get parking for entity with trucks. Dos't work when empty truck.
   *
   * @param int $entity_id
   *   Id for the entity.
   * @param int $entity_type
   *   Type of the entity.
   *
   * @return mixed
   *   FALSE or array with parking.
   */
  public function getEntityParking(int $entity_id, int $entity_type) {
    $select = db_select('move_parking', 'mp');
    $select->leftJoin('move_parking_chosen_trucks', 'mpct', 'mpct.pid = mp.pid');
    $select->fields('mpct', ['truck_id'])
      ->fields('mp')
      ->condition('mp.entity_id', $entity_id)
      ->condition('mp.entity_type', $entity_type)
      ->isNotNull('truck_id');
    $result = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Get parking for day. Get be grouping by date.
   *
   * @param string $date_from
   *   Date. Format 'Y-m-d'.
   * @param string $date_to
   *   Date. Format 'Y-m-d'.
   * @param string $display
   *   Group display type.
   *
   * @return array|mixed
   *   Array with parklot. Of FALSE if not.
   *
   * @throws \ServicesException
   */
  public static function getTripParkingForGeneralParklotByDate(string $date_from, string $date_to, $display = 'default') {
    $error_flag = FALSE;
    $error_message = '';
    $parking = [];
    try {
      $query = db_select('move_parking', 'mp');
      $query->leftJoin('move_parking_chosen_trucks', 'mpct', 'mp.pid = mpct.pid');
      $query->leftJoin('ld_trip_details', 'ltd', 'mp.entity_id = ltd.ld_trip_id');

      $query->fields('mp');
      $query->fields('mpct');

      $db_or = db_or();
      $db_and = db_and();
      $db_and2 = db_and();
      $date_from = strtotime($date_from);
      $date_to = strtotime($date_to);

      $db_and->condition('from_date', $date_from, '>=');
      $db_and->condition('from_date', $date_to, '<=');

      $db_and2->condition('from_date', $date_from, '<=');
      $db_and2->condition('to_date', $date_from, '>=');

      $db_or->condition($db_and);
      $db_or->condition($db_and2);

      $query->condition($db_or);

      $query->condition('entity_type', EntityTypes::LDTRIP);
      $query->condition('ltd.flag', [TripStatus::ACTIVE, TripStatus::DELIVERED], 'IN');

      $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

      $settings = new Settings();

      $condition = ['field_ld_only' => 0];
      $all_trucks = $settings->getTrucks($condition);

      if (!empty($result)) {
        foreach ($result as $row) {
          if (!empty($row['pid'])) {
            $truck_id = (int) $row['truck_id'];
            if (isset($all_trucks[$truck_id])) {
              $entity_id = $row['entity_id'];

              $row['from_date'] = !empty($row['from_date']) ? $row['from_date'] : $row['date'];
              $row['to_date'] = !empty($row['to_date']) ? $row['to_date'] : $row['date'];
              $row['start_time_1'] = !empty($row['start_time_1']) ? $row['start_time_1'] : '00:00';
              $row['end_time'] = !empty($row['end_time']) ? $row['end_time'] : '23:59';

              $row['from_date_string'] = date('Y-m-d', $row['from_date']);
              $row['to_date_string'] = date('Y-m-d', $row['to_date']);
              // Convert date to simple date.
              $date_from = date('Y-m-d', $row['from_date']) . ' ' . $row['start_time_1'];
              $date_to = date('Y-m-d', $row['to_date']) . ' ' . $row['end_time'];


              $row['from_date'] = strtotime($date_from);
              $row['to_date'] = strtotime($date_to);

              if (empty($array_for_check[$entity_id])) {
                if ($display == 'group_by_day' || $display == 'default') {
                  $row['full_info'] = self::getParkingEntityFullInfo($entity_id, $row['entity_type']);
                }

                $parking[$entity_id] = $row;
              }
              if ($display == 'group_by_day' || $display == 'default') {
                if (empty($array_for_check[$entity_id][$truck_id])) {
                  $truck_info = [
                    'truck_id' => $truck_id,
                    'truck_name' => $all_trucks[$truck_id]['name'],
                    'field_ld_only' => $all_trucks[$truck_id]['field_ld_only'],
                    'unavailable_from_request' => $all_trucks[$truck_id]['unavailable_from_request'],
                    'unavailable' => $all_trucks[$truck_id]['unavailable'],
                  ];

                  $parking[$entity_id]['trucks'][] = $truck_info;
                }
              }

              $array_for_check[$entity_id][$truck_id] = $truck_id;
            }
          }
        }
      }

      if (!empty($parking)) {
        if ($display == 'group_by_day' || $display == 'default') {
          $parking = self::prepareForParklot($parking);
          if ($display == 'group_by_day') {
            $parking = self::groupByDay($parking);
          }
        }

        if ($display == 'group_by_full_date') {
          $parking = self::groupByFullDate($parking);
        }

      }

    }
    catch (\Throwable $e) {
      $message = "{$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('Trip Parking for general parking', $message, [], WATCHDOG_ERROR);
      $error_flag = TRUE;
      $error_message = $e->getMessage();
    }
    finally {
      return $error_flag ? services_error($error_message, 406) : $parking;
    }
  }

  /**
   * Get parking for schedule.
   *
   * @param int $date_from
   *   Unix timestamp for paring.
   * @param int $date_to
   *   Unix timestamp for paring.
   *
   * @return array
   *   Array with trips and requests.
   *
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public static function getParkingForScheduleParklotByDate(int $date_from, int $date_to) : array {
    $flatrate_date = date('m/d/Y', $date_from);
    $instance = new MoveRequest();
    $flatrate = $instance->getFlatrateRequests($flatrate_date);

    $date_from = self::convertDateForParking($date_from, '00:00:00');
    $date_to = self::convertDateForParking($date_to, '23:59:59');

    $trip_parking = self::getTripParkingForGeneralParklotByDate($date_from, $date_to, 'group_by_day');
    $result = [];
    if (!empty($flatrate)) {
      foreach ($flatrate as $day => $val) {
        $result[$day] = !empty($trip_parking[$day]) ? array_merge($val, $trip_parking[$day]) : $val;
      }
    }
    else {
      $result = $trip_parking;
    }
    return $result;
  }

  /**
   * Get full information for parking type.
   *
   * @param int $entity_id
   *   Id of the entity.
   * @param int $entity_type
   *   Type of  the entity.
   * @param MoveRequest $request
   *   Move request.
   *
   * @return array
   *   Array of the fields for render.
   *
   * @throws \InvalidMergeQueryException
   * @throws \ServicesException
   */
  public static function getParkingEntityFullInfo(int $entity_id, int $entity_type, MoveRequest $request = NULL) {
    $parking_entity_full_info = [];
    switch ($entity_type) {
      case EntityTypes::MOVEREQUEST:
        $is_node_exist = self::isNodeExist($entity_id);
        if ($is_node_exist) {
          // @TODO we need to know what field must to be used. For now show all fields.
          $parking_entity_full_info = $request->getNode($entity_id);
        }
        break;

      case EntityTypes::LDTRIP:
        $trip = new LongDistanceTrip($entity_id);
        $parking_entity_full_info = $trip->retrieve();
        break;
    }
    return $parking_entity_full_info;
  }

  /**
   * Get truck type. Truck or trailer.
   *
   * @param int $tid
   *   Term id.
   *
   * @return int
   *   Type of truck(0 or 1).
   */
  public static function getTruckType(int $tid) {
    $type = db_select('field_data_field_ld_only', 'f')
      ->fields('f', ['field_ld_only_value'])
      ->condition('entity_id', $tid)
      ->condition('entity_type', 'taxonomy_term')
      ->execute()
      ->fetchField();
    return (int) $type;
  }

  /**
   * Remove entity from parking.
   *
   * @param int $entity_id
   *   Id of entity.
   * @param int $entity_type
   *   Type of entity.
   */
  public static function removeEntityFromParking(int $entity_id, $entity_type = EntityTypes::MOVEREQUEST) {
    $pids = db_select('move_parking', 'mv')
      ->fields('mv', ['pid'])
      ->condition('entity_id', $entity_id)
      ->condition('entity_type', $entity_type)
      ->execute()
      ->fetchCol();
    if (!empty($pids)) {
      db_delete('move_parking_chosen_trucks')
        ->condition('pid', $pids, 'IN')
        ->execute();
    }
  }

  /**
   * Get request count for truck by date.
   *
   * @param array $trucks_id
   *   Array with trucks id.
   * @param string $date
   *   Date where to check. Unix timestamp.
   * @param array $excluded_nids
   *   Nids which hwe don't want to check.
   *
   * @return mixed
   *   Array with trucks ids and nids for this truck.
   */
  public static function getRequestsIdForTruckByDate(array $trucks_id, string $date, array $excluded_nids = []) {
    $result = [];
    $date_format = "Y-m-d H:i:s";
    $move_date_from = date($date_format, Extra::getDateFrom($date));
    $move_date_to = date($date_format, Extra::getDateTo($date));

    $query = db_select('node', 'n');
    $query->leftJoin('field_data_field_date', 'fd', 'fd.entity_id = n.nid');
    $query->leftJoin('field_data_field_approve', 'fa', 'fa.entity_id = n.nid');
    $query->leftJoin('field_data_field_list_truck', 'list_trucks', 'list_trucks.entity_id = n.nid');

    $query->fields('n', ['nid']);
    $query->fields('list_trucks', ['field_list_truck_tid']);
    // If we don't need to check this nid. For example current nid.
    if (!empty($query)) {
      $query->condition('n.nid', $excluded_nids, 'NOT IN');
    }

    $query->condition('n.type', 'move_request')
      ->condition('n.status', 1)
      ->condition('list_trucks.field_list_truck_tid', $trucks_id, 'IN')
      ->condition('fd.field_date_value', [$move_date_from, $move_date_to], 'BETWEEN')
      ->condition('fa.field_approve_value', [RequestStatusTypes::CONFIRMED, RequestStatusTypes::NOT_CONFIRMED], 'IN');
    $query_result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($query_result)) {
      foreach ($query_result as $row) {
        $result[$row['field_list_truck_tid']][$row['nid']] = $row['nid'];
      }
    }
    return $result;
  }

  /**
   * Set parking id by entity id.
   *
   * @param int $entity_id
   *   Entity id.
   * @param int $entity_type
   *   Entity type.
   */
  public function setParkingIdBy(int $entity_id, int $entity_type) : void {
    $pids = db_select('move_parking', 'mp')
      ->fields('mp', ['pid'])
      ->condition('entity_id', $entity_id)
      ->condition('entity_type', $entity_type)
      ->execute()
      ->fetchCol();
    if (!empty($pids)) {
      db_delete('move_parking_chosen_trucks')
        ->condition('pid', $pids, 'IN')
        ->execute();
    }
  }

  /**
   * Delete truck from parking.
   *
   * @param int $truck_id
   *   Id of the truck.
   *
   * @return bool
   *   True if everything is good. DB error if not.
   *
   * @throws \ServicesException
   */
  public function deleteTruck(int $truck_id) {
    $parking = $this->retrieve();

    $truck_name = db_select('taxonomy_term_data', 'taxonomy')
      ->fields('taxonomy', ['name'])
      ->condition('tid', $truck_id)
      ->condition('vid', self::TRUCKS_VOCABULARY)
      ->execute()
      ->fetchField();

    db_delete('move_parking_chosen_trucks')
      ->condition('pid', $this->pid)
      ->condition('truck_id', $truck_id)
      ->execute();

    // Delete truck unavailable for trip.
    if (!empty($parking['truck_id']) && $parking['entity_type'] == EntityTypes::LDTRIP) {

      $log_title = "Truck {$truck_name} was removed";
      (new LongDistanceTrip($parking['entity_id']))->setLogs($log_title, []);

      $dates = $this->dateDiffInDateArray($parking['from_date'], $parking['to_date']);
      $this->removeTruckUnavailableForTrip($parking['truck_id'], $dates);
    }

    return TRUE;
  }

  /**
   * Delete all parking by entity id and entity type.
   *
   * @param int $entity_id
   *   Id of the entity.
   * @param int $entity_type
   *   Type of the entity.
   *
   * @return bool
   *   TRUE if everything is good.
   */
  public static function deleteParkingByEntity(int $entity_id, int $entity_type) {
    $pids = db_select('move_parking', 'mp')
      ->fields('mp', ['pid'])
      ->condition('entity_id', $entity_id)
      ->condition('entity_type', $entity_type)
      ->execute()
      ->fetchCol();
    if (!empty($pids)) {
      db_delete('move_parking')
        ->condition('entity_id', $entity_id)
        ->condition('entity_type', $entity_type)
        ->execute();
      db_delete('move_parking_chosen_trucks')
        ->condition('pid', $pids, 'IN')
        ->execute();
    }
    return TRUE;
  }

  /**
   * Compare callback.
   *
   * @param array $a
   *   First array.
   * @param array $b
   *   Second array.
   *
   * @return int
   *   1 or 0
   */
  public static function _cmp(array $a, array $b) {
    if ($a['from_date'] == $b['from_date']) {
      return 0;
    }
    return ($a['from_date'] < $b['from_date']) ? -1 : 1;
  }

  /**
   * Check if node is exist.
   *
   * @param int $entity_id
   *   Id of request.
   *
   * @return mixed
   *   Request id or FALSE.
   */
  public static function isNodeExist(int $entity_id) {
    $nid = db_select('node', 'n')
      ->fields('n', ['nid'])
      ->condition('nid', $entity_id)
      ->execute()
      ->fetchField();
    return $nid;
  }

  /**
   * Prepare fields for parklot.
   *
   * @param array $trips
   *   Trips.
   *
   * @return array
   *   Array with fields.
   */
  public static function prepareForParklot(array $trips) {
    $modified_data = [];

    foreach ($trips as $key => $trip_info) {
      $city_from = $trip_info['full_info']['start_from']['city'];
      $state_from = $trip_info['full_info']['start_from']['state'];
      $zip_from = $trip_info['full_info']['start_from']['zip'];

      $from_address = '';
      $from_address .= !empty($city_from) ? $city_from . ', ' : '';
      $from_address .= !empty($state_from) ? $state_from . ', ' : '';
      $from_address .= !empty($zip_from) ? $zip_from : '';


      $city_to = $trip_info['full_info']['ends_in']['city'];
      $state_to = $trip_info['full_info']['ends_in']['state'];
      $zip_to = $trip_info['full_info']['ends_in']['zip'];

      $to_address = '';
      $to_address .= !empty($city_to) ? $city_to . ', ' : '';
      $to_address .= !empty($state_to) ? $state_to . ', ' : '';
      $to_address .= !empty($zip_to) ? $zip_to : '';

      $modified_data[$key]['nid'] = $trip_info['entity_id'];

      foreach ($trip_info['trucks'] as $truck) {
        $modified_data[$key]['trucks']['raw'][] = $truck['truck_id'];
        $modified_data[$key]['delivery_trucks']['raw'][] = $truck['truck_id'];
      }

      $modified_data[$key]['service_type']['raw'] = RequestServiceType::FLAT_RATE;

      $modified_data[$key]['delivery_date_from'] = [
        'raw' => $trip_info['from_date'],
        'value' => date('j-m-Y', $trip_info['from_date']),
      ];

      $modified_data[$key]['delivery_date_to'] = [
        'raw' => $trip_info['to_date'],
        'value' => date('j-m-Y', $trip_info['to_date']),
      ];

      $modified_data[$key]['status']['raw'] = RequestStatusTypes::CONFIRMED;

      $modified_data[$key]['start_time1'] = [
        'raw' => 0,
        'value' => "08:00 AM",
      ];

      $modified_data[$key]['start_time2'] = [
        'raw' => 0,
        'value' => "11:00 PM",
      ];

      $modified_data[$key]['minimum_time'] = [
        'raw' => 0,
        'value' => "08:00 AM",
      ];

      $modified_data[$key]['maximum_time'] = [
        'raw' => 24,
        'value' => "11:00 PM",
      ];

      $modified_data[$key]['travel_time'] = [
        'raw' => 0,
        'value' => "00:00",
      ];

      $modified_data[$key]['zip_from'] = [
        'value' => $zip_from,
      ];

      $modified_data[$key]['zip_to'] = [
        'value' => $zip_to,
      ];

      $modified_data[$key]['from'] = $from_address;
      $modified_data[$key]['to'] = $to_address;

      $modified_data[$key]['date'] = [
        'raw' => $trip_info['from_date'],
        'value' => date('m/d/Y', $trip_info['from_date']),
      ];

      $modified_data[$key]['ddate'] = [
        'raw' => $trip_info['to_date'],
        'value' => date('m/d/Y', $trip_info['to_date']),
      ];

      $modified_data[$key]['start'] = [
        'd' => (int) date('d', $trip_info['from_date']),
        'm' => (int) date('m', $trip_info['from_date']),
      ];

      $modified_data[$key]['end'] = [
        'd' => (int) date('d', $trip_info['to_date']),
        'm' => (int) date('m', $trip_info['to_date']),
      ];

      $modified_data[$key]['ld_trip'] = TRUE;
      $modified_data[$key]['name'] = $trip_info['full_info']['foreman']['foreman_name'];
      $modified_data[$key]['delivery_start_time'] = '';
      $modified_data[$key]['phone'] = '';
      $modified_data[$key]['email'] = '';
      $modified_data[$key]['link'] = '';
      $modified_data[$key]['rate'] = '';
      $modified_data[$key]['type_from'] = '';
      $modified_data[$key]['type_to'] = '';
      $modified_data[$key]['distance'] = '';
      $modified_data[$key]['adrfrom'] = '';
      $modified_data[$key]['request_all_data'] = '';
      $modified_data[$key]['field_delivery_crew_size'] = '';
      $modified_data[$key]['request_data'] = '';
      $modified_data[$key]['field_foreman_assign_time'] = '';
      $modified_data[$key]['field_helper'] = '';
      $modified_data[$key]['field_foreman'] = '';
      $modified_data[$key]['field_reservation_received'] = '';
      $modified_data[$key]['move_size'] = '';
      $modified_data[$key]['crew'] = '';
      $modified_data[$key]['trip_info'] = $trip_info['full_info'];
    }

    return array_values($modified_data);
  }

  /**
   * Convert date.
   *
   * @param int $time_stamp_date
   *   Timestamp date.
   * @param string $time
   *   String in time.
   *
   * @return string
   *   Date string.
   */
  public static function convertDateForParking(int $time_stamp_date, string $time = '') {
    $datetime = new \DateTime();
    $datetime->setTimezone(new \DateTimeZone('GMT'));
    $datetime->setTimestamp($time_stamp_date);
    $date = $datetime->format('Y-m-d') . ' ' . $time;
    return $date;
  }

  /**
   * Group parklot by day.
   *
   * @param array $parking
   *   Parking.
   *
   * @return array
   *   Array with grouped data.
   */
  public static function groupByDay(array $parking) {
    $grouping_parking = [];
    $days = [];

    $days_num = 31;
    for ($i = 1; $i <= $days_num; $i++) {
      $days[$i] = [];
    }

    foreach ($parking as $item) {
      $start_day = $item['start']['d'];
      $end_day = $item['end']['d'];
      for ($i = $start_day; $i <= $end_day; $i++) {
        $grouping_parking[$i][$item['nid']] = $item;
      }
    }
    $grouping_parking = array_replace($days, $grouping_parking);
    ksort($grouping_parking);

    return $grouping_parking;
  }

  /**
   * Group parklot by date.
   *
   * @param array $parking
   *   PArking array.
   *
   * @return array
   *   Array with grouped data.
   */
  public static function groupByFullDate(array $parking) {
    $grouping_parking = [];
    foreach ($parking as $item) {
      $start_date = new \DateTime($item['from_date_string']);
      $end_date = new \DateTime($item['to_date_string']);

      $diff = $end_date->diff($start_date);
      $i = 1;
      while ($i <= $diff->d) {
        $date = $start_date->format('Y-m-d');
        $grouping_parking[$date] = TRUE;
        $start_date->modify('+1 day');
        $i++;
      }
    }
    return $grouping_parking;
  }

  /**
   * Convert date to time.
   *
   * @param int $date
   *   Date.
   * @param string $format
   *   Date format.
   *
   * @return int
   *   Timetstamp.
   */
  public static function convertDateToTime(int $date, string $format) {
    $datetime = new \DateTime();
    $datetime->setTimezone(new \DateTimeZone('GMT'));
    $datetime::createFromFormat($format, $date);
    $timestamp = $datetime->getTimestamp();

    return $timestamp;
  }

  /**
   * Date diff from unix time. Return array with days in format "Y-m-d".
   *
   * @param int $from_date
   *   Unix timestamp.
   * @param int $to_date
   *   Unix timestamp.
   *
   * @return array
   *   Array with dates.
   */
  public function dateDiffInDateArray(int $from_date, int $to_date) {
    $dates = [];
    $from_date = new \DateTime(date('Y-m-d', $from_date), new \DateTimeZone('GMT'));
    $to_date = new \DateTime(date('Y-m-d', $to_date), new \DateTimeZone('GMT'));
    $interval = $from_date->diff($to_date);
    if ($interval->days) {
      for ($i = 0; $i <= $interval->days; $i++) {
        $dates[] = $from_date->format('Y-m-d');
        $from_date->modify('+1 day');
      }
    }
    else {
      $dates[] = $from_date->format('Y-m-d');
    }
    return $dates;
  }

  /**
   * Add truck unavailable for dates and for trip.
   *
   * @param int $truck_id
   *   Id of the truck.
   * @param array $dates
   *   Array with dates.
   */
  public function addTruckUnavailableForTrip(int $truck_id, array $dates) : void {
    try {
      if (!empty($dates)) {
        $dates = ['add' => $dates];

        // Part for branch.
        $branch = FALSE;
        if (variable_get('is_common_branch_truck', FALSE)) {
          $truck = new Trucks($truck_id);
          if ($truck->retrieve()) {
            $branch = TRUE;
            $truck->unavailableTruckBranches($dates, TruckUnavailableType::FROM_REQUEST);
          }
        }

        if (!$branch) {
          (new ParkingTrucks())->setOrRemoveUnavailabilityTruckForDates($truck_id, $dates, TruckUnavailableType::FROM_REQUEST);
        }
      }
    }
    catch (\Throwable $e) {
      $message = "addTruckUnavailableForTrip Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('addTruckUnavailableForTrip', $message, [], WATCHDOG_CRITICAL);
    }
  }

  /**
   * Remove truck unavailable for dates and for trip.
   *
   * @param int $truck_id
   *   Id of the truck.
   * @param array $dates
   *   Array with dates.
   */
  public function removeTruckUnavailableForTrip(int $truck_id, array $dates) : void {
    try {
      if (!empty($dates)) {
        $dates = ['remove' => $dates];

        // Part for branch.
        $branch = FALSE;
        if (variable_get('is_common_branch_truck', FALSE)) {
          $truck = new Trucks($truck_id);
          if ($truck->retrieve()) {
            $branch = TRUE;
            $truck->unavailableTruckBranches($dates, TruckUnavailableType::FROM_REQUEST);
          }
        }

        if (!$branch) {
          (new ParkingTrucks())->setOrRemoveUnavailabilityTruckForDates($truck_id, $dates, TruckUnavailableType::FROM_REQUEST);
        }
      }
    }
    catch (\Throwable $e) {
      $message = "removeTruckUnavailableForTrip Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
      watchdog('removeTruckUnavailableForTrip', $message, [], WATCHDOG_CRITICAL);
    }
  }

}
