<?php

namespace Drupal\move_parking\Entity;

/**
 * Class ParkingTruck.
 *
 * @package Drupal\move_parking\Entity
 */
class ParkingTruck {

  /**
   * @var string
   */
  public $truck_id;

  /**
   * @var string
   */
  public $truck_name;

  // Getters.
  public function getTruckId() {
    return $this->truck_id;
  }
  public function getTruckName() {
    return $this->truck_id;
  }

  // Setters.
  public function setTruckId($truck_id) {
    $this->truck_id = (int) $truck_id;
  }
  public function setTruckName($truck_name) {
    $this->truck_name = (int) $truck_name;
  }
}