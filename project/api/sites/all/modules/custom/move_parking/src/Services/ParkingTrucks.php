<?php

namespace Drupal\move_parking\Services;

use Drupal\move_branch\Services\Trucks;
use Drupal\move_services_new\Services\Settings;
use Drupal\move_parking\Util\enum\TruckUnavailableType;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;
/**
 * Class ParkingTrucks.
 *
 * @package Drupal\move_parking\Services
 */
class ParkingTrucks {

  /**
   * Set or remove truck from unavailability table. Used in settings.
   *
   * @param int $truck_id
   *   Id of the truck.
   * @param array $data
   *   Array with dates for remove and add.
   * @param int $type
   *   Type of the unavailable date.
   *   0 - Manually from setting form, 1 - from request.
   *
   * @return array
   *   Array with added or removed dates.
   */
  public function setOrRemoveUnavailabilityTruckForDates(int $truck_id, array $data = array(), int $type = TruckUnavailableType::MANUALLY) {
    $result = array();

    if (!empty($data['add'])) {
      $result['add'] = $this->setUnavailabilityTruckForDate($truck_id, $data['add'], $type);
    }

    if (!empty($data['remove'])) {
      $result['remove'] = $this->removeUnavailabilityTruckForDate($truck_id, $data['remove'], $type);
    }

    return $result;

  }

  /**
   * Set unavailability for truck by dates.
   *
   * @param int $truck_id
   *   Id of the truck.
   * @param array $dates
   *   Array of the dates. Date format "Y-m-d".
   * @param int $type
   *   Type of the unavailable date.
   *   0 - Manually from setting form, 1 - from request.
   *
   * @return array
   *   Array with added dates.
   */
  public function setUnavailabilityTruckForDate(int $truck_id, array $dates = array(), int $type = TruckUnavailableType::MANUALLY) : array {
    $result = array();
    $error_flag = FALSE;
    $error_message = "";
    if (!empty($truck_id)) {
      try {
        if (!empty($dates)) {
          $dates = array_unique($dates);
          foreach ($dates as $date) {
            $date_converted = Settings::priceCalendarDateUnix($date);
            if (!empty($date_converted)) {
              $fields = array(
                'tid' => $truck_id,
                'date' => $date_converted,
                'type' => $type,
              );

              $temp = db_merge('truck_unavailable')
                ->key($fields)
                ->fields($fields)
                ->execute();

              if (variable_get('enable_debug_trucks', FALSE)) {
                watchdog('Trucks', '<pre>result for db merge setUnavailabilityTruckForDate: :a, :b</pre>', [
                  ':a' => print_r($fields, TRUE),
                  ':b' => print_r($temp, TRUE),
                ], WATCHDOG_DEBUG);
              }
              // TODO need check if was added.
              $result[$date] = 1;
            }
            else {
              $result[$date] = 0;
            }
          }
        }
      }
      catch (\Throwable $e) {
        $message = "Set truck unavailable Error: {$e->getMessage()} in {$e->getFile()}: {$e->getLine()} </br> Trace: {$e->getTraceAsString()}";
        watchdog('Set truck unavailable', $message, array(), WATCHDOG_CRITICAL);
        $error_flag = TRUE;
        $error_message = $e->getMessage();
      }
      finally {
        return $error_flag ? services_error($error_message, 406) : $result;
      }
    }
    return $result;
  }

  /**
   * Get unavailable for date.
   *
   * Can br filtered by type and excluded trucks.
   *
   * @param array $dates
   *   Array of the dates.
   * @param array $types
   *   Array of unavailable date types.
   * @param array $exclude_trucks
   *   Ids ofr the trucks which no need to search.
   *
   * @return array
   *   Empty array or array with trucks grouped by dates.
   */
  public function getAllUnavailableTrucksForDates(array $dates, array $types = [TruckUnavailableType::MANUALLY], array $exclude_trucks = array()) : array {
    $trucks = array();
    if (!empty($dates)) {
      foreach ($dates as $date) {

        $query = db_select('truck_unavailable', 'tu');
        $query->fields('tu');

        if (!empty($exclude_trucks)) {
          $query->condition('tid', $exclude_trucks, 'NOT IN');
        }

        $query->condition('date', $date)
          ->condition('type', $types, 'IN');
        $query_result = $query->execute()->fetcAll(\PDO::FETCH_ASSOC);

        if (!empty($query_result)) {
          foreach ($query_result as $row) {
            $trucks[$row['date']] = $row;
          }
        }
      }
    }
    return $trucks;
  }

  /**
   * Remove unavailability for truck by dates.
   *
   * @param int $truck_id
   *   Id of the truck.
   * @param array $dates
   *   Array of the dates. Date format "Y-m-d".
   * @param int $type
   *   Type of the unavailable date.
   *   0 - Manually from setting form, 1 - from request.
   *
   * @return array
   *   Array with removed dates.
   */
  public function removeUnavailabilityTruckForDate(int $truck_id, array $dates, int $type = TruckUnavailableType::MANUALLY) : array {
    $result = array();
    if (!empty($dates)) {
      $dates = array_unique($dates);
      foreach ($dates as $date) {
        $date_converted = Settings::priceCalendarDateUnix($date);
        $result[$date] = db_delete('truck_unavailable')
          ->condition('tid', $truck_id)
          ->condition('type', $type)
          ->condition('date', $date_converted)
          ->execute();

        if (variable_get('enable_debug_trucks', FALSE)) {
          watchdog('Trucks', '<pre>parameters for method removeUnavailabilityTruckForDate: :a, :b, :c, :f</pre>', [
            ':a' => print_r($truck_id, TRUE),
            ':b' => print_r($date_converted, TRUE),
            ':c' => $type,
            ':f' => print_r($result[$date], TRUE),
          ], WATCHDOG_DEBUG);
        }
      }
    }
    return $result;
  }

  /**
   * Remove all unavailability for truck.
   *
   * @param int $truck_id
   *   Truck id.
   * @param int $type
   *   Type of the unavailable date.
   *   0 - Manually from setting form, 1 - from request.
   *
   * @return bool
   *   TRUE if removed.
   */
  public function removeAllUnavailabilityTruck(int $truck_id, int $type = TruckUnavailableType::MANUALLY) : bool {
    $result = db_delete('truck_unavailable')
      ->condition('tid', $truck_id)
      ->condition('type', $type)
      ->execute();
    return (bool) $result;
  }

  /**
   * Set or remove unavailable date for truck by checking request.
   *
   * @param array $data
   *   Array of data for check.
   *   Example:
   *    $data['nid'] = 1232;
   *    $data['date'] = 2017-08-30;
   *    $data['status'] = 1;
   *    $data['trucks_list_ids'] = array(23,12,45);.
   */
  public static function syncUnavailabilityTruckForRequest(array $data) : void {
    $status = 0;
    $nid = 0;
    $date = 0;
    $trucks_list_ids = array();

    // Set arrays keys names to variables.
    extract($data, EXTR_OVERWRITE);

    if (!empty($date) && !empty($trucks_list_ids)) {

      $type = TruckUnavailableType::FROM_REQUEST;

      // If request status is confirmed or not confirmed - set this day for this truck to unavailable.
      if ($status == RequestStatusTypes::CONFIRMED || $status == RequestStatusTypes::NOT_CONFIRMED) {
        if (variable_get('enable_debug_trucks', FALSE)) {
          watchdog('Trucks', '<pre>parameters for method setUnavailabilityMultiplyTrucksForDate: :a, :b, :c</pre>', [
            ':a' => print_r($trucks_list_ids, TRUE),
            ':b' => print_r($date, TRUE),
            ':c' => $type,
          ], WATCHDOG_DEBUG);
        }
        self::setUnavailabilityMultiplyTrucksForDate($trucks_list_ids, array($date), $type);
      }
      // If request status not equal "confirmed" or "not confirmed" - check for other busy request for this truck and day.
      // If no such trucks remove it from unavailable.
      else {
        if (variable_get('enable_debug_trucks', FALSE)) {
          watchdog('Trucks', '<pre>parameters for method removeUnavailabilityTruckForDateWithCheck: :a, :b, :c</pre>', [
            ':a' => print_r($trucks_list_ids, TRUE),
            ':b' => $date,
            ':c' => print_r($nid, TRUE),
          ], WATCHDOG_DEBUG);
        }
        self::removeUnavailabilityTruckForDateWithCheck($trucks_list_ids, $date, array($nid));
      }
    }
  }

  public static function setUnavailabilityMultiplyTrucksForDate(array $trucks_list_ids, array $date, int $type) : void {
    foreach ($trucks_list_ids as $truck_id) {
      $trucks_to_update[] = $truck_id;

      // Check and collect branch trucks.
      if ($external_id = (new Trucks())->getExternalId($truck_id)) {
        $external_ids[$external_id] = $truck_id;
      }
    }

    // Sync branch trucks.
    if (!empty($external_ids)) {
      $branch_response = (new Trucks())->unavailableTruckBranchesFromRequest($external_ids, array('add' => $date));

      // If we have error from brunches - remove external trucks from update.
      if ($branch_response['status'] == 500) {
        $trucks_to_update = array_diff($trucks_to_update, $external_ids);
      }
    }

    $parking_inst = new ParkingTrucks();
    // Update trucks.
    if (!empty($trucks_to_update)) {
      foreach ($trucks_to_update as $truck_id) {
        $parking_inst->setUnavailabilityTruckForDate($truck_id, $date, $type);
      }
    }
  }

  /**
   * Remove unavailability truck for day with check.
   *
   * If some truck unavailable for this day we don't need to remove it.
   *
   * @param array $trucks_list_ids
   *   Array with trucks id's.
   * @param string $date
   *   Date. Format "Y-m-d".
   * @param array $excluded_nids
   *   Nids which no need to check.
   */
  public static function removeUnavailabilityTruckForDateWithCheck(array $trucks_list_ids, string $date, array $excluded_nids) : void {
    $type = TruckUnavailableType::FROM_REQUEST;

    $requests_for_truck = Parking::getRequestsIdForTruckByDate($trucks_list_ids, $date, $excluded_nids);
    foreach ($trucks_list_ids as $truck_id) {
      if (empty($requests_for_truck[$truck_id])) {
        $trucks_to_update[] = $truck_id;

        // Check and collect branch trucks.
        if ($external_id = (new Trucks())->getExternalId($truck_id)) {
          $external_ids[$external_id] = $truck_id;
        }
      }
    }

    // Sync branch trucks.
    if (!empty($external_ids)) {
      $branch_response = (new Trucks())->unavailableTruckBranchesFromRequest($external_ids, array('remove' => array($date)));

      // If we have error from brunches - remove external trucks from update.
      if ($branch_response['status'] == 500) {
        $trucks_to_update = array_diff($trucks_to_update, $external_ids);
      }
    }

    $parking_inst = new ParkingTrucks();
    // Update trucks.
    if (!empty($trucks_to_update)) {
      foreach ($trucks_to_update as $truck_id) {
        $parking_inst->removeUnavailabilityTruckForDate($truck_id, array($date), $type);
      }
    }
  }

}
