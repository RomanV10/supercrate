<?php

namespace Drupal\move_parking\System;

use Drupal\move_parking\Services\Parking;
use Drupal\move_parking\Services\ParkingTrucks;
use Drupal\move_parking\Util\enum\TruckUnavailableType;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  public static function definition() {
    return array(
      'move_parking' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_parking\System\Service::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_parking\System\Service::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_parking\System\Service::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_parking\System\Service::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_parking\System\Service::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'date_from',
                'optional' => TRUE,
                'type' => 'int',
                'source' => array('param' => 'date_from'),
              ),
              array(
                'name' => 'date_to',
                'optional' => TRUE,
                'type' => 'int',
                'source' => array('param' => 'date_to'),
              ),
              array(
                'name' => 'by_time',
                'optional' => TRUE,
                'type' => 'int',
                'source' => array('param' => 'by_time'),
                'default value' => 0,
              ),
              array(
                'name' => 'full_info',
                'optional' => TRUE,
                'type' => 'int',
                'source' => array('param' => 'full_info'),
                'default value' => 0,
              ),
              array(
                'name' => 'trucks_only',
                'optional' => TRUE,
                'type' => 'int',
                'source' => array('param' => 'trucks_only'),
                'default value' => 1,
              ),
              array(
                'name' => 'only_trailers',
                'optional' => TRUE,
                'type' => 'int',
                'source' => array('param' => 'only_trailers'),
                'default value' => 0,
              ),
              array(
                'name' => 'conditions',
                'optional' => TRUE,
                'type' => 'string',
                'source' => array('param' => 'conditions'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'get_trip_driver' => array(
            'callback' => 'Drupal\move_parking\System\Service::getTripDriver',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_schedule_parklot' => array(
            'callback' => 'Drupal\move_parking\System\Service::getScheduleParklot',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'type' => 'string',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete_truck_from_parking' => array(
            'callback' => 'Drupal\move_parking\System\Service::deleteTruckFromParking',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'pid',
                'type' => 'int',
                'source' => array('data' => 'pid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'truck_id',
                'type' => 'int',
                'source' => array('data' => 'truck_id'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete_parking_by_entity' => array(
            'callback' => 'Drupal\move_parking\System\Service::deleteParkingByEntity',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'truck_date_unavailable' => array(
            'callback' => 'Drupal\move_parking\System\Service::truckDateUnavailable',
            'file' => array(
              'type' => 'php',
              'module' => 'move_parking',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'truck_id',
                'type' => 'int',
                'source' => array('data' => 'truck_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'dates_op',
                'type' => 'array',
                'source' => array('data' => 'dates_op'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'int',
                'source' => array('data' => 'type'),
                'optional' => TRUE,
                'default value' => 0,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  public static function create($data = array()) {
    $al_instance = new Parking();
    return $al_instance->create($data);
  }

  public static function retrieve($id) {
    $al_instance = new Parking($id);
    return $al_instance->retrieve();
  }

  public static function update($data) {
    $al_instance = new Parking();
    return $al_instance->update($data);
  }
  public static function index($date_from, $date_to, bool $by_time = FALSE, bool $full_info = FALSE, bool $trucks_only = TRUE, bool $only_trailers = FALSE, $conditions = '') {
    $al_instance = new Parking();
    if (!empty($conditions) && is_string($conditions)) {
      $conditions = drupal_json_decode($conditions);
    }
    else {
      $conditions = array();
    }
    return $al_instance->index($date_from, $date_to, $by_time, $full_info, $trucks_only, $only_trailers, $conditions);
  }
  public static function delete(int $id) {
    $al_instance = new Parking($id);
    return $al_instance->delete();
  }

  public static function deleteTruckFromParking(int $pid, int $truck_id) {
    $al_instance = new Parking($pid);
    return $al_instance->deleteTruck($truck_id);
  }

  public static function deleteParkingByEntity($entity_id, $entiy_type) {
    return  Parking::deleteParkingByEntity($entity_id, $entiy_type);
  }

  public static function getScheduleParklot(string $date) {
    if (!empty($date)) {
      $obj_date = new \DateTime($date);

      // Retrieve first day of month in timestamp.
      $date_from = $obj_date->modify('first day of this month 23:59:59')->getTimestamp();

      // Retrieve last day of month in timestamp.
      $date_to = $obj_date->modify('last day of this month 23:59:59')->getTimestamp();

      return Parking::getParkingForScheduleParklotByDate($date_from, $date_to);
    }
    return FALSE;
  }

  public static function truckDateUnavailable(int $truck_id, array $dates_op = array(), int $type = TruckUnavailableType::MANUALLY) {
    $parking_trucks_inst = new ParkingTrucks();
    return $parking_trucks_inst->setOrRemoveUnavailabilityTruckForDates($truck_id, $dates_op, $type);
  }

}
