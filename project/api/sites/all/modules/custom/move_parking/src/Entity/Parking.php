<?php

namespace Drupal\move_parking\Entity;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EntityParking.
 *
 * @package Drupal\move_parking\Entity
 */
class EntityParking {

  /**
   * @var ParkingTruck
   */
  public $parking_truck;

  /**
   * @var string
   */
  public $entity_id = 0;
  /**
   * @var string
   */
  public $entity_type = 0;
  /**
   * @var string
   */
  public $date = 0;
  /**
   * @var string
   */
  public $from_date = 0;

  /**
   * @var string
   */
  public $to_date = 0;

  /**
   * @var string
   */
  public $type = 0;

  /**
   * Trip constructor.
   */
  public function __construct() {
    $this->parking_truck = new ParkingTruck();
  }

  // Getters.
  public function getParkingTruck(): ParkingTruck {
    return $this->parking_truck;
  }

  public function getEntityId() {
    return (int) $this->entity_id;
  }

  public function getEntityType() {
    return (int) $this->entity_type;
  }

  public function getDate() {
    return (int) $this->date;
  }

  public function getFromDate() {
    return (int) $this->from_date;
  }

  public function getToDate() {
    return (int) $this->to_date;
  }

  public function getType() {
    return (int) $this->type;
  }

  // Setters.
  public function setParkingTruck($parking_truck) {
    $encoders = array(new JsonEncoder());
    $normalizers = array(new ObjectNormalizer());
    $serializer = new Serializer($normalizers, $encoders);
    $this->parking_truck = $serializer->deserialize(json_encode($parking_truck), ParkingTruck::class, 'json');
  }

  public function setEntityId($entity_id) {
    $this->entity_id = (int) $entity_id;
  }

  public function setEntityType($entity_type) {
    $this->entity_type = (int) $entity_type;
  }
  public function setDate($date) {
    $this->date = (int) $date;
  }
  public function setFromDate($from_date) {
    $this->from_date = (int) $from_date;
  }
  public function setToDate($to_date) {
    $this->to_date = (int) $to_date;
  }
  public function setType($type) {
    $this->type = (int) $type;
  }

}
