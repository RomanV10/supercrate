<?php

define('DRUPAL_ROOT', explode('api', __DIR__)[0] . 'api/');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
require_once DRUPAL_ROOT . '/sites/all/modules/services/includes/services.runtime.inc';
set_include_path(DRUPAL_ROOT);
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['REQUEST_METHOD'] = 'POST';

// Bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

use PHPUnit\Framework\TestCase;
use Drupal\move_invoice\Services\Invoice;

class RequestStorageTest extends TestCase {

  protected $fixture;

  protected function setUp() {
    $this->fixture = new Invoice();
  }

  protected function tearDown() {
    $this->fixture = NULL;
  }

  private static function createInvoice() {
    $file_path = dirname(__FILE__) . '/data/invoice.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('invoice', $parse_json['data']);

    $file_path = dirname(__FILE__) . '/data/invoiceUpdate.json';
    $file_content = file_get_contents($file_path);
    $parse_json = drupal_json_decode($file_content);
    variable_set('invoice_update', $parse_json['data']);
  }

  public function testCreateInvoice() {
    self::createInvoice();
    $rsp = variable_get('invoice');
    $rsid = $this->fixture->create($rsp['data']);
    $this->assertGreaterThanOrEqual(1, $rsid);

    return (int) $rsid;
  }

  /**
   * @depends testCreateRequestStoragePayment
   */
  public function testRetriveInvoice($rsid) {
    $this->fixture->setId($rsid);
    $ret = $this->fixture->retrieve();
    $this->assertCount(3, $ret);
  }

  /**
   * @depends testCreateRequestStoragePayment
   */
  public function testUpdateInvoice($rsid) {
    $rs_up = variable_get('invoice_update');
    $rs_update = new Invoice($rsid);
    $upd = $rs_update->update($rs_up, TRUE);
    $this->assertEquals(2, $upd);
  }

  /**
   * @depends testCreateRequestStoragePayment
   */
  public function testDeleteRequestStoragePayment($rsid) {
    $rsc_delete = new Invoice($rsid);
    $del = $rsc_delete->delete();
    $this->assertEquals(1, $del);
  }

}