<?php

namespace Drupal\move_invoice\Services;

use Drupal\move_coupon\Services\Coupon;
use Drupal\move_coupon\Services\CouponUser;
use Drupal\move_long_distance\Services\Actions\SitInvoiceActions;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\move_request\MoveRequestSearch;
use Drupal\move_services_new\System\Extra;
use Drupal\move_storage\Services\RequestStorage;
use Drupal\move_storage\Services\Storage;
use Drupal\move_services_new\Util\enum\InvoiceFlags;
use Drupal\move_services_new\Util\enum\EntityTypes;
use Drupal\move_notification\Services\Notification;
use Drupal\move_services_new\Util\enum\NotificationTypes;


/**
 * Class Invoice.
 *
 * @package Drupal\move_invoice\Services
 */
class Invoice extends BaseService {
  private $id = NULL;
  private $entityId = NULL;
  private $entityType = NULL;
  public $timeZone = 'UTC';
  private $pageSize = 25;

  public function __construct($id = NULL, $entity_id = NULL, $entity_type = NULL) {
    $this->id = $id;
    $this->entityId = $entity_id;
    $this->entityType = $entity_type;
  }

  public function create($data = array()) {
    $invoice_table = 'move_invoice';
    $created = time();
    $invoice_hash = $this->generateHashForInvoice();

    $id = db_insert($invoice_table)
      ->fields(array(
        'entity_type' => $this->entityType,
        'entity_id' => $this->entityId,
        'flag' => $data['flag'],
        'data' => serialize($data),
        'created' => $created,
        'hash' => $invoice_hash,
      ))
      ->execute();

    if ($this->entityType == EntityTypes::STORAGEREQUEST) {
      $this->initReminderInDb($id);
      $balance = new RequestStorage();
      $balance->insertReqStorBalance($this->entityId);
    }

    if ($this->entityType == EntityTypes::LDCARRIER) {
      SitInvoiceActions::processInvoice($data['jobs_id'], $id, $data['amount']);
    }

    return $id;
  }

  public function retrieve() {
    $result = db_select('move_invoice', 'mi')
      ->fields('mi')
      ->condition('mi.id', $this->id)
      ->execute()
      ->fetchAssoc();

    if (isset($result['data']) && $result['data']) {
      $result['data'] = unserialize($result['data']);
    }

    return $result;
  }

  public function update($data = array()) {
    $retrieve_invoice = $this->retrieve();
    $record = array(
      'id' => $this->id,
      'entity_type' => $retrieve_invoice['entity_type'],
      'entity_id' => $retrieve_invoice['entity_id'],
      'flag' => $data['flag'],
      'data' => $data,
      'changed' => time(),
    );
    $result = drupal_write_record('move_invoice', $record, 'id');

    if ($retrieve_invoice['entity_type'] == EntityTypes::STORAGEREQUEST) {
      $balance = new RequestStorage();
      $balance->insertReqStorBalance($retrieve_invoice['entity_id']);
    }

    return $result;
  }

  /**
   * Update only invoice data.
   *
   * @param int $invoice_id
   *   Invoice id.
   * @param array $data
   *   Array og invoice data.
   */
  public static function updateInvoiceData(int $invoice_id, $data = array()) {
    db_update('move_invoice')
      ->fields(array('data' => serialize($data)))
      ->condition('id', $invoice_id)
      ->execute();
  }

  public function delete() {
    $retrieve_invoice = $this->retrieve();

    $result = db_delete('move_invoice')
      ->condition('id', $this->id)
      ->execute();
    if ($retrieve_invoice['entity_type'] == EntityTypes::STORAGEREQUEST) {
      $balance = new RequestStorage();
      $balance->insertReqStorBalance($retrieve_invoice['entity_id']);
    }

    return $result;
  }

  public function index() {
    $result = array();
    $query = db_select('move_invoice', 'mi')
      ->fields('mi')
      ->execute()
      ->fetchAll();
    foreach ($query as $key => $value) {
      $unserialize = unserialize($value->data);

      $result[] = array(
        'id' => $value->id,
        'entity_type' => $value->entity_type,
        'entity_id' => $value->entity_id,
        'data' => $unserialize,
        'created' => $value->created,
      );
    }

    return $result;
  }

  private function initReminderInDb(int $invoice_id) {
    $id = db_insert('request_storage_reminder')
      ->fields(array(
        'invoice_id' => $invoice_id,
        'created' => time(),
      ))
      ->execute();

    return $id;
  }

  private function generateHashForInvoice() {
    return $hash = hash('sha512', user_password(128));
  }

  /**
   * Return invoice data by hash.
   *
   * @param string $hash
   *   Hash.
   *
   * @return mixed
   *   Invoice data.
   */
  public function hashInvoice(string $hash) {
    global $user;
    $query = db_select('move_invoice', 'mi')
      ->fields('mi')
      ->condition('mi.hash', $hash)
      ->execute()
      ->fetchObject();

    if (empty($query)) {
      return [];
    }

    $result['data'] = unserialize($query->data);
    $result['id'] = $query->id;

    if ($query->entity_type == EntityTypes::STORAGEREQUEST) {
      $instance = new RequestStorage($query->entity_id);
      $request_storage = $instance->retrieve();
    }

    if (!empty($request_storage['rentals']['storage'])) {
      $al_instance = new Storage($request_storage['rentals']['storage']);
      $storage = $al_instance->retrieve();
      $result['closeBalance'] = (isset($storage['closeBalance'])) ? $storage['closeBalance'] : FALSE;
    }

    if ($query->entity_type == EntityTypes::STORAGEREQUEST) {
      $balance = new RequestStorage();
      $result['balance'] = $balance->requestStorageBalance($query->entity_id);
      $another_invoices = $this->getInvoiceForEntity($query->entity_id, $query->entity_type);
      foreach ($another_invoices as $key => $value) {
        if ($result['id'] !== $value['id']) {
          $result['another_invoices'][$key] = $value;
        }
      }
    }
    if (!$this->checkHashInvoiceUserRole($user) && ($result['data']['flag'] == InvoiceFlags::SENT || $result['data']['flag'] == InvoiceFlags::DRAFT)) {
      $result['data']['flag'] = InvoiceFlags::VIEWED;
      self::changeInvoiceFlag($result['id'], $result['data']);
    }

    return $result;
  }

  /**
   * Method for change invoice flags.
   *
   * @param int $invoice_id
   *   Invoice id.
   * @param array $data
   *   Invoice data.
   *
   * @return \DatabaseStatementInterface
   *   Result drupal db_update.
   */
  public static function changeInvoiceFlag(int $invoice_id, array $data) {
    $old_flag_int = Invoice::getInvoiceFlag($invoice_id);

    $query = db_update('move_invoice')
      ->fields(array(
        'flag' => $data['flag'],
        'data' => serialize($data),
      ))
      ->condition('id', $invoice_id)
      ->execute();
    switch ($data['flag']) {
      case InvoiceFlags::VIEWED:
        $notification_type = NotificationTypes::VIEW_INVOICE;
        $action = 'viewed';
        break;

      case InvoiceFlags::PAID:
        $notification_type = NotificationTypes::PAY_INVOICE;
        $action = 'paid';
        break;
    }
    // Notification.
    if (!empty($notification_type)) {
      $notification_entity_type = $data['entity_type'];
      $notification_entity_id = $data['entity_id'];
      $entity_name = 'request';
      switch ($data['entity_type']) {
        case EntityTypes::STORAGEREQUEST:
          $storage = RequestStorage::getAllAdditionalInfo($data['entity_id']);
          if (!empty($storage['move_request_id'])) {
            $notification_entity_type = EntityTypes::MOVEREQUEST;
            $notification_entity_id = $storage['move_request_id'];
          }
          $entity_name = 'storage';
          break;
      }
      $notification_text = 'Invoice #' . $invoice_id . ' for ' . $entity_name . '#' . $data['entity_id'] . ' was ' . $action . '. Client name is ' . $data['client_name'];
      $notification_vars = array('notification' => array('text' => $notification_text));
      $notification = new Notification($notification_type, $notification_entity_id, $notification_entity_type);
      $notification->create($notification_vars);
    }

    if (!empty($query)) {
      $date = time();
      $old_flag_str = InvoiceFlags::getKeyByValue($old_flag_int);
      $new_flag = InvoiceFlags::getKeyByValue($data['flag']);
      $title = "Change invoice #$invoice_id flag from $old_flag_str to $new_flag";
      $instance = new RequestStorage();
      $instance->createReqStorLog($data['entity_id'], $title, $date);
    }

    return $query;
  }

  /**
   * Method for return invoice flag by unique invoice id.
   *
   * @param int $invoice_id
   *   Invoice id.
   *
   * @return int
   *   Invoice flag.
   */
  public static function getInvoiceFlag(int $invoice_id) {
    $invoice_flag = FALSE;
    if (isset($invoice_id)) {
      $invoice_flag = db_query("
      SELECT flag
      FROM move_invoice
      WHERE id = $invoice_id")->fetchField();
    }

    return $invoice_flag;
  }

  private function checkHashInvoiceUserRole(\stdClass $user) {
    $hash_roles = array('sales', 'administrator', 'manager');
    $result_array = array_intersect($hash_roles, $user->roles);

    return $result_array ? TRUE : FALSE;
  }

  /**
   * Method get all invoices.
   *
   * @param int $entity_id
   *   Entity id.
   * @param int $entity_type
   *   Entity type.
   *
   * @return array
   *   All invoices data.
   */
  public function getInvoiceForEntity($entity_id, $entity_type) {
    $query = db_select('move_invoice', 'mi')
      ->fields('mi')
      ->condition('mi.entity_id', $entity_id)
      ->condition('mi.entity_type', $entity_type)
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($query as $key => $value) {
      $query[$key]['data'] = unserialize($value['data']);
    }

    return $query;
  }

  /**
   * Method get all not paid invoices.
   *
   * @param int $entity_id
   *   Entity id.
   * @param int $entity_type
   *   Entity type.
   *
   * @return array
   *   All invoices data.
   */
  public function getNotPaidInvoiceForEntity($entity_id, $entity_type) {
    $invoice_flags = array(
      InvoiceFlags::DRAFT,
      InvoiceFlags::SENT,
      InvoiceFlags::VIEWED,
    );

    $query = db_select('move_invoice', 'mi')
      ->fields('mi')
      ->condition('mi.entity_id', $entity_id)
      ->condition('mi.entity_type', $entity_type)
      ->condition('mi.flag', $invoice_flags, 'IN')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    foreach ($query as $key => $value) {
      $query[$key]['data'] = unserialize($value['data']);
    }

    return $query;
  }

  public static function getInvoiceWithFlag(int $entity_type,  $flag = array()) {
    if (!empty($flag)) {
      if (!is_array($flag)) {
        $flag = array($flag);
      }
      $query = db_select('move_invoice', 'mi')
        ->fields('mi')
        ->condition('mi.entity_type', $entity_type)
        ->condition('mi.flag', $flag, 'IN')
        ->execute()
        ->fetchAll();

    }
    return (!empty($query)) ? $query : array();
  }

  /**
   * Method to select all invoices by dates.
   * @param int $date_from
   * @param int $date_to
   * @return mixed
   */
  public static function getInvoiceByDates(int $date_from, int $date_to = 0, $by_storage_info = FALSE) {
    if (empty($date_to)) {
      $date_to = time();
    }
    if (empty($date_from)) {
      $date_from = time();
    }
    if ($by_storage_info) {
      $query = db_select('move_invoice', 'mi');
      $query->leftJoin('request_storage_additional', 'rsa', 'mi.entity_id = rsa.request_storage_id');
      $query->fields('mi')
        ->condition('rsa.start_date', $date_from, '>=')
        ->condition('rsa.next_run_on', $date_to, '<=');
      $result = $query->execute()->fetchAllAssoc('entity_id');
    }
    else {
      $result = db_select('move_invoice', 'mi')
        ->fields('mi')
        ->condition('mi.created', array($date_from, $date_to), 'BETWEEN')
        ->execute()
        ->fetchAll();
    }
    return $result;
  }

  /**
   * Get count move request invoices.
   *
   * @param int $entity_type
   *   Entity type.
   * @param int|null $date_from
   *   Date from.
   * @param int|null $date_to
   *   Date to.
   * @param int|null $flag
   *   Invoice status.
   *
   * @return mixed
   *   Count invoices.
   */
  public function getCountInvoicesForPeriod(int $entity_type = EntityTypes::MOVEREQUEST, ?int $date_from = NULL, ?int $date_to = NULL, ?int $flag = NULL) {
    $time_zone = Extra::getTimezone();
    if (empty($date_from)) {
      $date_from = Extra::getFirstDayCurrentMonth($time_zone);
    }

    if (empty($date_to)) {
      $date_to = Extra::getLastDayCurrentMonth($time_zone);
    }

    $query_invoice = db_select('move_invoice', 'mi');
    $query_invoice->fields('mi');
    $query_invoice->condition('mi.entity_type', $entity_type);
    if (!empty($date_from) && !empty($date_to)) {
      $query_invoice->condition('mi.created', array($date_from, $date_to), 'BETWEEN');
    }
    if (!empty($flag)) {
      $query_invoice->condition('mi.flag', $flag);
    }
    return $query_invoice->execute()->rowCount();
  }

  /**
   * Method return move request invoice.
   *
   * @param string $sort
   *   Sort date created or unique id.
   * @param string $direction
   *   Direction asc or desc.
   * @param int $page
   *   Number page.
   * @param string $field
   *   Additional field for sort.
   * @param array $filter
   *   Filter data.
   *
   * @return array
   *   Sorted move request invoices.
   */
  public function getMoveRequestInvoices(string $sort, string $direction, int $page, string $field = '', array $filter = array()) {
    $storageInstance = new RequestStorage();

    $invoices_data = $storageInstance->getInvoicesQuery($sort, $direction, $page, EntityTypes::MOVEREQUEST, $filter);
    $storageInstance->getInvoiceData($invoices_data['result'], $invoices_data['query']);
    $invoices = $invoices_data['result'];

    if ($this->checkFieldForSortInvoice($field) && !empty($invoices['items'])) {
      $multi_direction = $direction == 'ASC' ? SORT_ASC : SORT_DESC;
      $invoices['items'] = $this->customMultiSort($invoices['items'], $field, $multi_direction);
      $invoices['items'] = array_slice($invoices['items'], $page * $this->pageSize, $this->pageSize);
    }
    else {
      $invoices['items'] = 0;
    }

    return $invoices ?? array();
  }

  /**
   * Custom multi sort array.
   *
   * @param array $array
   *   Array for sort.
   * @param string $field
   *   String for sort.
   * @param int $direction
   *   Sort direction: asc or desc.
   *
   * @return array|false
   *   False if failure.
   */
  public function customMultiSort(array $array, string $field, int $direction = SORT_DESC) {
    $sortArr = array();
    foreach ($array as $key => $val) {
      $sortArr[$key] = $val[$field];
    }

    array_multisort($sortArr, $direction, $array);

    return $array;
  }

  /**
   * Check exist user data field for sort.
   *
   * @param string $field
   *   Field for sort.
   *
   * @return bool
   *   Exist or not.
   */
  private function checkFieldForSortInvoice(string $field) {
    $exist_invoice_fields = array(
      'id',
      'client_name',
      'description',
      'invoice_date',
      'total',
      'status',
      'storage_request_id',
    );

    return in_array($field, $exist_invoice_fields);
  }

}
