<?php

namespace Drupal\move_invoice\System;

use Drupal\move_invoice\Services\Invoice;

class Service {

  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    return $resources;
  }

  private static function definition() {
    return array(
      'move_invoice' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_invoice\System\Service::createInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_invoice',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_invoice\System\Service::retrieveInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_invoice',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_invoice\System\Service::updateInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_invoice',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_invoice\System\Service::deleteInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_invoice',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_invoice\System\Service::indexInvoice',
            'file' => array(
              'type' => 'php',
              'module' => 'move_invoice',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
        'actions' => array(
          'hash_invoice' => array(
            'callback' => 'Drupal\move_invoice\System\Service::hashInvoice',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_invoice',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'hash',
                'type' => 'string',
                'source' => array('data' => 'hash'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_invoice_for_entity' => array(
            'callback' => 'Drupal\move_invoice\System\Service::getInvoiceForEntity',
            'file' => array(
              'type' => 'inc',
              'module' => 'move_invoice',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'entity_id',
                'type' => 'int',
                'source' => array('data' => 'entity_id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'entity_type',
                'type' => 'int',
                'source' => array('data' => 'entity_type'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_move_request_invoices' => array(
            'callback' => 'Drupal\move_invoice\System\Service::getMoveRequestInvoices',
            'file' => array(
              'type' => 'php',
              'module' => 'move_invoice',
              'name' => 'src\System\Service',
            ),
            'args' => array(
              array(
                'name' => 'sort',
                'type' => 'string',
                'source' => array('data' => 'sort'),
                'optional' => TRUE,
                'default value' => 'created',
              ),
              array(
                'name' => 'direction',
                'type' => 'string',
                'source' => array('data' => 'direction'),
                'optional' => TRUE,
                'default value' => 'desc',
              ),
              array(
                'name' => 'page',
                'type' => 'int',
                'source' => array('data' => 'page'),
                'optional' => TRUE,
                'default value' => 0,
              ),
              array(
                'name' => 'field',
                'type' => 'string',
                'source' => array('data' => 'field'),
                'optional' => TRUE,
                'default value' => '',
              ),
              array(
                'name' => 'filter',
                'type' => 'array',
                'source' => array('data' => 'filter'),
                'optional' => TRUE,
                'default value' => array(),
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  public static function createInvoice($data = array(), int $entity_id, int $entity_type) {
    $al_instance = new Invoice(NULL, $entity_id, $entity_type);
    return $al_instance->create($data);
  }

  public static function retrieveInvoice($id) {
    $al_instance = new Invoice($id);
    return $al_instance->retrieve();
  }

  public static function updateInvoice($id, $data) {
    $al_instance = new Invoice($id);
    return $al_instance->update($data);
  }

  public static function deleteInvoice(int $id) {
    $al_instance = new Invoice($id);
    return $al_instance->delete();
  }

  public static function indexInvoice() {
    $al_instance = new Invoice();
    return $al_instance->index();
  }

  public static function hashInvoice($hash) {
    $al_instance = new Invoice();
    return $al_instance->hashInvoice($hash);
  }

  public static function getInvoiceForEntity($entity_id, $entity_type) {
    $al_instance = new Invoice();
    return $al_instance->getInvoiceForEntity($entity_id, $entity_type);
  }

  /**
   * Callback function get move request invoices with sort.
   *
   * @param string $sort
   *   Variable count_stars or created. Default created.
   * @param string $direction
   *   Variable directions: ASC or DESC.
   * @param int $page
   *   Number page.
   * @param string $field
   *   Field for sort.
   * @param array $filter
   *   Filter data.
   *
   * @return array
   *   Sorted array of move request invoices.
   */
  public static function getMoveRequestInvoices(string $sort, string $direction, int $page, string $field = '', array $filter = array()) {
    $al_instance = new Invoice();
    return $al_instance->getMoveRequestInvoices($sort, $direction, $page, $field, $filter);
  }

}