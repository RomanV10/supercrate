<?php

namespace Drupal\move_zip_codes\System;

/**
 * Class definition.
 *
 * @package Drupal\move_zip_codes\System
 */
class Services {

  public function getDefinitions() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += array(
      'get_area_code' => array(
        'operations' => array(
          'retrieve' => array(
            'help' => 'Retrieve area code.',
            'callback' => 'Drupal\move_zip_codes\System\Services::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_zip_codes',
              'name' => 'src/System/Services',
            ),
            'args' => array(
              array(
                'name' => 'zip',
                'type' => 'string',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
      'zip_codes' => array(
        'operations' => array(),
        'actions' => array(
          'find' => array(
            'help' => 'Retrieve zip.',
            'callback' => 'Drupal\move_zip_codes\System\Services::find_zip',
            'file' => array(
              'type' => 'php',
              'module' => 'move_zip_codes',
              'name' => 'src/System/Services',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'string',
                'description' => 'Any request data.',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'filed_name',
                'type' => 'string',
                'source' => array('data' => 'field_name'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'page',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'The zero-based index of the page to get, defaults to 0.',
                'default value' => 0,
                'source' => array('data' => 'page'),
              ),
              array(
                'name' => 'pagesize',
                'optional' => TRUE,
                'type' => 'int',
                'description' => 'Number of records to get per page.',
                'default value' => variable_get('services_user_index_page_size', 20),
                'source' => array('data' => 'pagesize'),
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );

    return $resources;
  }

  public static function retrieve(string $zip) {
    $result = FALSE;
    $zip = entity_load('zip_codes', FALSE, array('zip' => $zip));
    if ($zip) {
      $zip = reset($zip);
      $result = $zip->area_code;
    }

    return $result;
  }

  public static function find_zip(string $data, string $field_name, int $page, int $page_size) {
    $result = array();
    $trusted_fields = array('city', 'state', 'zip', 'area_code', 'county');
    if (in_array($field_name, $trusted_fields)) {
      $result = entity_load('zip_codes', FALSE, array($field_name => $data));
    }

    return !empty($result) ? array_slice($result, $page * $page_size, $page_size) : $result;
  }

}
