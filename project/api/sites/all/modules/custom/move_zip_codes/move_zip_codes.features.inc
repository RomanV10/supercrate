<?php
/**
 * @file
 * move_zip_codes.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function move_zip_codes_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}
