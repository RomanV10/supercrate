<?php
/**
 * @file
 * move_zip_codes.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function move_zip_codes_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'extra';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'extra';
  $endpoint->authentication = array(
    'move_acrossdomain_auth' => array(
      'allow_domains' => 'http://movecalc.local',
    ),
  );
  $endpoint->server_settings = array(
    'formatters' => array(
      'json' => TRUE,
      'bencode' => FALSE,
      'jsonp' => FALSE,
      'php' => FALSE,
      'xml' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/x-www-form-urlencoded' => TRUE,
      'application/vnd.php.serialized' => FALSE,
      'application/xml' => FALSE,
      'multipart/form-data' => FALSE,
      'text/xml' => FALSE,
    ),
  );
  $endpoint->resources = array(
    'get_area_code' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['extra'] = $endpoint;

  return $export;
}
