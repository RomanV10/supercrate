<?php

/**
 * Implements hook_form().
 */
function move_zip_codes_form($form, &$form_state, $zip_codes, $op = 'edit') {
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $zip_codes->label,
    '#description' => t('The human-readable name of this profile type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($zip_codes->type) ? $zip_codes->type : '',
    '#maxlength' => 32,
    '#disabled' => $zip_codes->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'profile2_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this profile type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['data']['#tree'] = TRUE;
  $form['data']['registration'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show during user account registration.'),
    '#default_value' => !empty($zip_codes->data['registration']),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save profile type'),
    '#weight' => 40,
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $zip_codes->weight,
    '#description' => t('When showing profiles, those with lighter (smaller) weights get listed before profiles with heavier (larger) weights.'),
    '#weight' => 10,
  );

  if (!$zip_codes->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete profile type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('move_zip_codes_form_submit_delete'),
    );
  }
}

/**
 * Form API submit callback for the type form.
 */
function move_zip_codes_form_submit(&$form, &$form_state) {
  $profile_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $profile_type->save();
  $form_state['redirect'] = 'admin/structure/zip';
}

/**
 * Form API submit callback for the delete button.
 */
function move_zip_codes_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/zip/manage/' . $form_state['zip_codes']->type . '/delete';
}
