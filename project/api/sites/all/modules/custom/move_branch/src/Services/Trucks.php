<?php

namespace Drupal\move_branch\Services;

use Drupal\move_parking\Services\ParkingTrucks;
use Drupal\move_parking\Util\enum\TruckUnavailableType;
use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Parklot;
use Drupal\move_services_new\Services\Settings;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_parking\Services\Parking;
use Exception;
use Drupal\move_services_new\Util\enum\RequestStatusTypes;

/**
 * Class Trucks.
 *
 * @package Drupal\move_branch\Services
 */
class Trucks extends BaseService {
  private $tid               = NULL;
  private $branch            = NULL;
  private $createBrunchTruck = "/server/move_branch_trucks/create_branch_truck/";
  private $truckEditBranch   = "/server/move_branch_trucks/truck_edit_branch/";
  private $checkRemoveTruck  = "/server/move_branch_trucks/check_remove_truck/";
  private $removeBranchTruck = "/server/move_branch_trucks/remove_branch_truck/";
  private $getParklot        = "/server/move_branch_trucks/get_parklot_truck_edition/";
  private $saveSetting       = "/server/move_branch_trucks/save_setting_branch";

  private $truckUnavailable = "/server/move_branch_trucks/change_truck_unavailable";
  private $truckUnavailableFromRequest = "/server/move_branch_trucks/change_truck_unavailable_from_request";

  private $unavRollBackTruck = "/server/move_branch_trucks/truck_unavailable_roll_back";
  private $unavRollBackTruckFromRequest = "/server/move_branch_trucks/truck_unavailable_roll_back_from_request";

  private $getCalendarDispat = "/server/move_branch_trucks/get_calendar_dispatch";
  private $checkOverbooking = "/server/move_branch_trucks/check_overbooking";

  /**
   * Trucks constructor.
   *
   * @param mixed $tid
   *   Terms id.
   */
  public function __construct($tid = NULL) {
    $this->tid = $tid;
    $this->branch = new Branch(variable_get('current_branch', NULL));
  }

  /**
   * Create truck and save.
   *
   * @param mixed $data
   *   Truck data.
   *
   * @return mixed
   *   .
   */
  public function create($data = array()) {
    $temp_result = array();
    $result = 0;
    $branches = $this->branch->index();
    // New format.
    $new_data[] = $data;
    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        $res = $this->createTruck($new_data);
        if ($res['tid'] > 0) {
          $temp_result[] = array(
            'url' => $branch->api_url,
            'tid' => $res['tid'],
            'id' => $res['id'],
          );
          $result = $res['tid'];
        }
        continue;
      }
      $post_data = json_encode($new_data);
      $options = $this->generateOptions('POST', $post_data);
      $url = $branch->api_url . $this->createBrunchTruck;
      $response = drupal_http_request($url, $options);
      if ($response->code == 200 && !empty($response->data)) {
        $res = json_decode($response->data);
        if (intval($res->tid) > 0) {
          $temp_result[] = array(
            'url' => $branch->api_url,
            'tid' => $res->tid,
            'id' => $res->id,
          );
        }
      }
      else {
        watchdog('Branch', "<pre>Failed created truck<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }
    // Check if everyone branch worked successfully.
    if (count($temp_result) != count($branches)) {
      foreach ($temp_result as $branch) {
        if ($this->branch->isCurrentBranch($branch['url'])) {
          $this->removeBranchTruck($branch['id']);
          continue;
        }
        $options = $this->generateOptions('POST', json_encode(array('id' => $branch['id'])));
        $url = $branch['api_url'] . $this->removeBranchTruck;
        drupal_http_request($url, $options);
      }
      return array(
        'status' => 500,
        'statusText' => 'Unable to create truck',
        'data' => NULL,
      );
    }

    return array(
      'status' => 200,
      'statusText' => 'OK',
      'data' => $result,
    );
  }

  /**
   * Retrieve tid.
   *
   * @return mixed
   *   Array.
   */
  public function retrieve() {
    return $this->getExternalId($this->tid);
  }

  /**
   * Edit truck on all branches.
   *
   * @param mixed $data
   *   Tid, name.
   *
   * @return bool|mixed
   *   .
   */
  public function update($data = array()) {
    $temp_result = array();
    $settings = new Settings();
    $term = $settings->termLoad($this->tid);
    $old_name_trucks = $term->name;
    $branches = $this->branch->index();
    $externalId = $this->retrieve();

    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        $temp = $settings->editTermTruck(array(
          'tid' => $this->tid,
          'name' => $data['name'],
        ));
        if ($temp) {
          $temp_result[] = array(
            'url' => $branch->api_url,
            'id' => $externalId,
          );
        }
        continue;
      }
      $post_data = json_encode(array('name' => $data['name'], 'id' => $externalId));
      $options = $this->generateOptions('POST', $post_data);
      $url = $branch->api_url . $this->truckEditBranch;
      $response = drupal_http_request($url, $options);
      if ($response->code == 200 && !empty(json_decode($response->data))) {
        $temp_result[] = array(
          'url' => $branch->api_url,
          'id' => $externalId,
        );
      }
      else {
        watchdog('Branch', "<pre>Update truck Failed<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }

    // Check if everyone branch worked successfully.
    if (count($branches) != count($temp_result)) {
      foreach ($temp_result as $branch) {
        if ($this->branch->isCurrentBranch($branch['url'])) {
          $this->updateBranchTruck(array('name' => $old_name_trucks, 'id' => $branch['id']));
          continue;
        }
        $post_data = json_encode(array('name' => $old_name_trucks, 'id' => $branch['id']));
        $options = $this->generateOptions('POST', $post_data);
        $url = $branch->api_url . $this->truckEditBranch;
        drupal_http_request($url, $options);
      }
      return array(
        'status' => 500,
        'statusText' => 'Unable to update truck',
        'data' => NULL,
      );
    }

    return array(
      'status' => 200,
      'statusText' => 'OK',
      'data' => NULL,
    );
  }

  /**
   * Delete trucks on all branches.
   *
   * @return mixed
   *   .
   */
  public function delete() {
    $we_can_delete = 0;
    $branches = $this->branch->index();
    $externalId = $this->retrieve();

    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        $settings = new Settings();
        if ($settings->checkPeggingTruck($this->tid)) {
          $we_can_delete++;
        }
        continue;
      }
      $post_data = json_encode(array('id' => $externalId));
      $options = $this->generateOptions('POST', $post_data);
      $url = $branch->api_url . $this->checkRemoveTruck;
      $response = drupal_http_request($url, $options);
      $responseData = json_decode($response->data);
      if ($response->code == 200 && reset($responseData)) {
        $we_can_delete++;
      }
      else {
        watchdog('Branch', "<pre>Failed check delete truck<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }
    // Check if we can delete terms on all branches.
    if (count($branches) == $we_can_delete) {
      foreach ($branches as $branch) {
        if ($this->branch->isCurrentBranch($branch->api_url)) {
          $this->removeBranchTruck($externalId);
          continue;
        }
        $post_data = json_encode(array('id' => $externalId));
        $options = $this->generateOptions('POST', $post_data);
        $url = $branch->api_url . $this->removeBranchTruck;
        $response = drupal_http_request($url, $options);
        if ($response->code != 200) {
          watchdog('Branch', "<pre>Failed delete truck<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
        }
      }
      return array(
        'status' => 200,
        'statusText' => 'OK',
        'data' => NULL,
      );
    }
    else {
      return array(
        'status' => 500,
        'statusText' => 'Unable to delete truck',
        'data' => NULL,
      );
    }
  }

  /**
   * Get trucks for front.
   *
   * @return array
   *   .
   */
  public function index() {
    $trucks = array();
    $result = self::getTrucksBranches();

    try {
      if (!empty($result)) {
        $terms_unavailble_db = db_select('truck_unavailable', 'tu')
          ->fields('tu')
          ->condition('tu.tid', $result)
          ->execute()
          ->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($terms_unavailble_db as $un) {
          $type = (int) $un['type'];
          if ($type === 0) {
            $terms_unavailble_manually[$un['tid']][] = $un['date'];
          }
          else {
            $terms_unavailble_from_request[$un['tid']][] = $un['date'];
          }
        }

        foreach ($result as $key => $value) {
          $term = entity_load('taxonomy_term', array((int) $value));
          $term = reset($term);
          $un_date = (isset($terms_unavailble_manually[$term->tid])) ? $terms_unavailble_manually[$term->tid] : NULL;
          $un_date_from_request = (isset($terms_unavailble_from_request[$term->tid])) ? $terms_unavailble_from_request[$term->tid] : NULL;

          $trucks[$term->tid] = array(
            'name' => $term->name,
            'field_ld_only' => 0,
            'unavailable' => $un_date,
            'unavailable_from_request' => $un_date_from_request,
          );
        }
      }

      return array(
        'status' => 200,
        'statusText' => 'OK',
        'data' => $trucks,
      );
    }
    catch (\Throwable $e) {
      return array(
        'status' => $e->getCode(),
        'statusText' => $e->getMessage(),
        'data' => NULL,
      );
    }
  }

  /**
   * Create truck and save.
   *
   * @param array $data
   *   Truck data.
   *
   * @return array
   *   Tid and id.
   */
  public function createTruck(array $data) {
    $settings = new Settings();
    $tid = 0; $id = 0;
    if (!empty($data)) {
      foreach ($data as $truck) {
        $tid = $settings->insertNewTruck($truck);
        $id = db_insert('move_branch_trucks')
          ->fields(array(
            'tid' => $tid,
          ))
          ->execute();
      }
    }
    return array('tid' => $tid, 'id' => $id);
  }

  /**
   * Update current branch truck.
   *
   * @param array $data
   *   Name and external truck id.
   *
   * @return bool
   *   True or false.
   */
  public function updateBranchTruck(array $data) {
    $tid = $this->getThisBranchTid($data['id']);
    $settings = new Settings();
    return $settings->editTermTruck(array('tid' => $tid, 'name' => $data['name']));
  }

  /**
   * Check if we can delete truck.
   *
   * @param int $id
   *   External id truck.
   *
   * @return bool
   *   True or false.
   */
  public function checkRemoveTruck(int $id) {
    $tid = $this->getThisBranchTid($id);
    $settings = new Settings();
    return $settings->checkPeggingTruck($tid);
  }

  /**
   * Remove in current branch truck.
   *
   * @param int $id
   *   External id.
   */
  public function removeBranchTruck(int $id) {
    $tid = $this->getThisBranchTid($id);
    $settings = new Settings();
    $delete = $settings->removeTruck($tid);
    if ($delete) {
      db_delete('move_branch_trucks')
        ->condition('id', $id, '=')
        ->execute();
    }
  }

  /**
   * Get branches trucks.
   *
   * @return mixed
   *   .
   */
  public static function getTrucksBranches() {
    $result = db_select('move_branch_trucks', 'mbt')
      ->fields('mbt', array('tid'))
      ->execute()
      ->fetchAll(\PDO::FETCH_COLUMN);

    return $result;
  }

  /**
   * Get parklot requests.
   *
   * @param int $date
   *   Timestamp.
   * @param array $condition
   *   Condition.
   *
   * @return array|mixed
   *   Nodes.
   */
  public function getAllParklot(int $date, array $condition = array('field_approve' => [2, 3])) {
    $result = array();
    $branches = $this->branch->index();

    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        $result = $this->getResult((new Parklot(new MoveRequest()))->get($date, $condition), FALSE, $result);
        continue;
      }
      $token = $this->branch->authorization($branch->api_url);
      $data = json_encode(array(
        'date' => $date,
        'condition' => $condition,
        'period' => 0,
      ));
      $options = $this->generateOptions('POST', $data, array('X-CSRF-Token' => $token));
      $url = $branch->api_url . $this->getParklot;
      $response = drupal_http_request($url, $options);
      if ($response->code == 200) {
        $result = $this->getResult(json_decode($response->data), array('url' => $branch->front_url, 'name' => $branch->name), $result);
      }
      else {
        watchdog('Branch', "<pre>Failed get all parklot<br> :a</pre>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }
    return $result;
  }

  /**
   * Format result for front.
   *
   * @param array $nodes
   *   Array  Node.
   * @param mixed $branch
   *   Branch data.
   * @param array $result
   *   Current result.
   *
   * @return array
   *   Result.
   */
  private function getResult(array $nodes, $branch, array &$result) {
    if (!empty($nodes)) {
      foreach ($nodes as $node) {
        if (!empty($node)) {
          $node = (object) $node;
          if (!empty($branch)) {
            // If common truck change externalIds this tids.
            if (!empty($node->trucks->common_truck)) {
              $tids = array();
              foreach ($node->trucks->raw as $truck) {
                $tids[] = $this->getThisBranchTid($truck);
              }
              $node->trucks->raw = $tids;
              $node->trucks->old = $tids;
              $settings = new Settings();

              if (count($tids) == 1) {
                $node->trucks->value = $settings->termLoad($tids[0]);
              }
              else {
                $node->trucks->value = [];
                foreach ($tids as $tid) {
                  $node->trucks->value[] = $settings->termLoad($tid);
                }
              }

              unset($node->trucks->common_truck);
            }
            // Delivery trucks.If common truck change externalIds this tids.
            if (!empty($node->delivery_trucks->common_truck)) {
              $tids = array();
              foreach ($node->delivery_trucks->raw as $truck) {
                $tids[] = $this->getThisBranchTid($truck);
              }
              $node->delivery_trucks->raw = $tids;
              $node->delivery_trucks->old = $tids;
              $settings = new Settings();

              if (count($tids) == 1) {
                $node->delivery_trucks->value = $settings->termLoad($tids[0]);
              }
              else {
                $node->delivery_trucks->value = [];
                foreach ($tids as $tid) {
                  $node->delivery_trucks->value[] = $settings->termLoad($tid);
                }
              }

              unset($node->delivery_trucks->common_truck);
            }
            $node->branch = $branch;
          }
          else {
            $node->branch = FALSE;
          }
        }
        $result[] = $node;
      }
    }

    return $result;
  }

  /**
   * Save setting on all branches.
   *
   * @param string $name
   *   Name.
   * @param bool $value
   *   Value.
   *
   * @return array
   *   Custom answer.
   */
  public function settingsTrucksSave(string $name, bool $value) {
    $count_save = array();
    $branches = $this->branch->index();
    $old_value = variable_get($name, FALSE);

    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        if ($this->saveSetting($name, $value)) {
          $count_save[] = array('url' => $branch->api_url);
        }
        continue;
      }

      $data = json_encode(array('name' => $name, 'value' => $value));
      $options = $this->generateOptions('POST', $data);
      $url = $branch->api_url . $this->saveSetting;
      $response = drupal_http_request($url, $options);
      if ($response->code == 200 && !empty($response->data)) {
        $responseData = json_decode($response->data);
        $res = reset($responseData);
        if ($res) {
          $count_save[] = array('url' => $branch->api_url);
        }
      }
      else {
        watchdog('Branch', "<pre>Failed save settings<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }

    if (count($branches) != count($count_save)) {
      foreach ($branches as $branch) {
        if ($this->branch->isCurrentBranch($branch->api_url)) {
          $this->saveSetting($name, $old_value);
          continue;
        }

        $data = json_encode(array('name' => $name, 'value' => $old_value));
        $options = $this->generateOptions('POST', $data);
        $url = $branch->api_url . $this->saveSetting;
        drupal_http_request($url, $options);
      }
      return array(
        'status' => 500,
        'statusText' => 'Unable to save setting truck',
        'data' => NULL,
      );
    }
    return array(
      'status' => 200,
      'statusText' => 'OK',
      'data' => NULL,
    );
  }

  /**
   * Save setting in variable.
   *
   * @param string $name
   *   Variable name.
   * @param bool $value
   *   Variable value.
   *
   * @return bool
   *   TRUE.
   */
  public function saveSetting(string $name, bool $value) {
    try {
      variable_set($name, $value);
    }
    catch (\Throwable $e) {
      watchdog('Branch', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Unavailable truck on Branches.
   *
   * @param array $dates
   *   Dates.
   * @param int $type
   *   Type od unavailable date.
   *
   * @return array
   *   Custom answer.
   */
  public function unavailableTruckBranches(array $dates = array(), int $type = TruckUnavailableType::MANUALLY) {
    $external_id = $this->retrieve();
    $branches = $this->branch->index();
    $temp_result = array();

    $terms_unavailble_db_old = db_select('truck_unavailable', 'tu')
      ->fields('tu')
      ->condition('tu.tid', $this->tid)
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);


    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        $res = $this->unavailableTruck($external_id, $dates, $type);
        if ($res) {
          $temp_result[] = array('url' => $branch->api_url);
        }
        continue;
      }

      $data = json_encode(array(
        'id' => $external_id,
        'date' => $dates,
        'type' => $type,
      )
      );

      $options = $this->generateOptions('POST', $data);
      $url = $branch->api_url . $this->truckUnavailable;
      $response = drupal_http_request($url, $options);

      if ($response->code == 200 && !empty(json_decode($response->data))) {
        $temp_result[] = array('url' => $branch->api_url);
      }
      else {
        watchdog('Branch', "<pre>Failed unavailable truck save<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }

    if (count($temp_result) != count($branches)) {
      foreach ($temp_result as $branch) {
        if ($this->branch->isCurrentBranch($branch['url'])) {
          $this->unavailableRollBack($external_id, $terms_unavailble_db_old);
        }
        $data = json_encode(array('id' => $external_id, 'data' => $terms_unavailble_db_old));
        $options = $this->generateOptions('POST', $data);
        $url = $branch->api_url . $this->unavRollBackTruck;
        drupal_http_request($url, $options);
      }
      return array(
        'status' => 500,
        'statusText' => 'Cannot save truck availability setting',
        'data' => NULL,
      );
    }
    return array(
      'status' => 200,
      'statusText' => 'OK',
      'data' => NULL,
    );
  }

  /**
   * Unavailable truck on Branches from request.
   *
   * @param array $external_ids
   *   External ids.
   * @param array $date
   *   Date.
   *
   * @return array
   *   Custom answer.
   */
  public function unavailableTruckBranchesFromRequest(array $external_ids, array $date) {
    $branches = $this->branch->index();
    $temp_result = array();
    $terms_unavailble_db_old = array();
    $string_date = current($date)[0];
    if (!empty($string_date) && !empty($external_ids)) {
      $query = db_select('truck_unavailable', 'tu');
      $query->leftJoin('move_branch_trucks', 'mbt', 'mbt.tid = tu.tid');
      $query->fields('tu')
        ->fields('mbt', array('id'))
        ->condition('tu.tid', $external_ids, 'IN')
        ->condition('tu.type', TruckUnavailableType::FROM_REQUEST);
      $query_result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
      if (!empty($query_result)) {
        foreach ($query_result as $row) {
          $terms_unavailble_db_old[$row['id']] = $row;
        }
      }
    }


    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        $temp_result[] = array('url' => $branch->api_url);
        continue;
      }

      $data = json_encode(array(
        'ids' => array_keys($external_ids),
        'date' => $date,
      )
      );

      $options = $this->generateOptions('POST', $data);
      $url = $branch->api_url . $this->truckUnavailableFromRequest;
      $response = drupal_http_request($url, $options);

      if ($response->code == 200 && !empty(json_decode($response->data))) {
        $temp_result[] = array('url' => $branch->api_url);
      }
      else {
        watchdog('Branch', "<pre>Failed unavailable truck from request save<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }

    $count_branches = count($branches);
    if (count($temp_result) != $count_branches) {
      foreach ($temp_result as $branch) {
        if ($this->branch->isCurrentBranch($branch['url'])) {
          continue;
        }
        $data = json_encode(array('data' => $terms_unavailble_db_old));
        $options = $this->generateOptions('POST', $data);
        $url = $branch->api_url . $this->unavRollBackTruckFromRequest;
        drupal_http_request($url, $options);
      }
      return array(
        'status' => 500,
        'statusText' => 'Cannot save truck availability from request setting',
        'data' => NULL,
      );
    }
    return array(
      'status' => 200,
      'statusText' => 'OK',
      'data' => NULL,
    );
  }

  /**
   * Unavailable truck.
   *
   * @param int $id
   *   External id.
   * @param array $dates
   *   Dates.
   * @param int $type
   *   Type of Unavailable date.
   *
   * @return bool
   *
   */
  public function unavailableTruck(int $id, array $dates, int $type = TruckUnavailableType::MANUALLY) {
    try {
      $tid = $this->getThisBranchTid($id);
      (new ParkingTrucks())->setOrRemoveUnavailabilityTruckForDates($tid, $dates, $type);
      return TRUE;
    }
    catch (\Throwable $e) {
      watchdog('Branch', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Unavailable truck from request.
   *
   * @param array $ids
   *   External ids from request.
   * @param array $date
   *   Array of dates.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function unavailableTruckFromRequest(array $ids, array $date) : bool {
    try {

      $tids = $this->getThisBranchTids($ids);
      $type = TruckUnavailableType::FROM_REQUEST;

      if (!empty($tids)) {
        $parking_truck_inst = new ParkingTrucks();
        foreach ($tids as $truck_id) {
          $parking_truck_inst->setOrRemoveUnavailabilityTruckForDates($truck_id, $date, $type);
        }
      }

      return TRUE;
    }
    catch (\Throwable $e) {
      watchdog('Branch', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Roll back trucks unv.
   *
   * @param int $id
   *   External id.
   * @param array $data
   *   Data.
   */
  public function unavailableRollBack(int $id, array $data) {
    $tid = $this->getThisBranchTid($id);

    db_delete('truck_unavailable')
      ->condition('tid', $tid)
      ->execute();

    foreach ($data as $row) {
      db_insert('truck_unavailable')
        ->fields(array('tid', 'date'), array($tid, $row['date']))
        ->execute();
    }
  }

  /**
   * Roll back trucks unv.
   *
   * @param array $data
   *   Array with external ids and date.
   */
  public function unavailableRollBackFromRequest(array $data = array()) : void {
    if (!empty($data)) {
      foreach ($data as $id => $val) {
        $tid = $this->getThisBranchTid($id);
        db_delete('truck_unavailable')
          ->condition('tid', $tid)
          ->execute();

        db_insert('truck_unavailable')
          ->fields(array(
            'tid' => $tid,
            'date' => $val['date'],
            'type' => $val['type'],
          )
          )
          ->execute();
      }
    }
  }

  /**
   * Get parklot month.
   *
   * @param string $date
   *   Date.
   * @param array $condition
   *   Conditions.
   *
   * @return array
   *   Nodes.
   */
  public function getParklotMonths(string $date, array $condition) {
    $result = array();
    $branches = $this->branch->index();

    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        $result = $this->getResult((new Parklot(new MoveRequest()))->getByMonth($date, $condition), FALSE, $result);
        continue;
      }

      $data = json_encode(array(
        'date' => $date,
        'condition' => $condition,
        'period' => 1,
      ));
      $options = $this->generateOptions('POST', $data);
      $url = $branch->api_url . $this->getParklot;
      $response = drupal_http_request($url, $options);
      if ($response->code == 200) {
        $result = $this->getResult(json_decode($response->data), array('url' => $branch->front_url, 'name' => $branch->name), $result);
      }
      else {
        watchdog('Branch', "<pre>Failed get parklot months<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }

    return $result;
  }

  /**
   * Get parklot and change truck object.
   *
   * @param int $date
   *   Date.
   * @param array $condition
   *   Condition.
   * @param int $period
   *   Month or day.
   *
   * @return array
   *   Requests.
   */
  public function getParklotTruckEdition(int $date, array $condition = array('field_approve' => [2, 3]), int $period = 0) {
    if ($period) {
      $result =(new Parklot(new MoveRequest()))->getByMonth($date, $condition);
    }
    else {
      $result = (new Parklot(new MoveRequest()))->get($date, $condition);
    }

    foreach ($result as &$node) {
      $trucks = [];
      $trucks_delivery = [];
      // Trucks. Check if common truck and change tids externalIds.
      foreach ($node['trucks']['raw'] as $truck) {
        $id = $this->getExternalId($truck);
        if (!empty($id)) {
          $trucks[] = $id;
          $node['trucks']['common_truck'] = TRUE;
        }
      }
      // Delivery trucks. Check if common truck and change tids externalIds.
      foreach ($node['delivery_trucks']['raw'] as $truck) {
        $id = $this->getExternalId($truck);
        if (!empty($id)) {
          $trucks_delivery[] = $id;
          $node['delivery_trucks']['common_truck'] = TRUE;
        }
      }

      if (!empty($trucks)) {
        $node['trucks']['raw'] = $trucks;
      }

      if (!empty($trucks_delivery)) {
        $node['delivery_trucks']['raw'] = $trucks_delivery;
      }

    }
    return $result;
  }

  /**
   * Get dispatch calendar.
   *
   * @param int $date
   *   Date.
   * @param array $condition
   *   Condition.
   *
   * @return array
   *   .
   */
  public function getDispatchCalendar(int $date, array $condition) {
    $result = array();
    $datetime = new \DateTime();
    $datetime->setTimezone(new \DateTimeZone('GMT'));
    $datetime->setTimestamp($date);

    $first_day_of_month = clone $datetime->modify('first day of this month 00:00:00');
    $last_day_of_month = clone $datetime->modify('last day of this month 23:59:59');
    $date_from = $first_day_of_month->format('Y-m-d') . ' 00:00:00';
    $date_to = $last_day_of_month->format('Y-m-d') . ' 23:59:59';
    $period = new \DatePeriod($first_day_of_month, new \DateInterval('P1D'), $last_day_of_month);
    foreach ($period as $day) {
      $result[$day->format('Y-m-d')] = FALSE;
    }
    // FlatRate.
    $flatrate_query = db_select('node', 'n');
    $flatrate_query->leftJoin('field_data_field_delivery_date_from', 'delivery_from', 'n.nid = delivery_from.entity_id');
    $flatrate_query->leftJoin('field_data_field_delivery_date_to', 'delivery_to', 'n.nid = delivery_to.entity_id');
    $flatrate_query->fields('delivery_from', array('field_delivery_date_from_value'));
    $flatrate_query->fields('delivery_to', array('field_delivery_date_to_value'));
    $db_or = db_or();
    $db_and = db_and();
    $db_and2 = db_and();
    $db_and->condition('delivery_from.field_delivery_date_from_value', $date_from, '>=');
    $db_and->condition('delivery_to.field_delivery_date_to_value', $date_to, '<=');

    $db_and2->condition('delivery_from.field_delivery_date_from_value', $date_from, '<=');
    $db_and2->condition('delivery_to.field_delivery_date_to_value', $date_from, '>=');

    $db_or->condition($db_and);
    $db_or->condition($db_and2);

    $flatrate_query->condition($db_or);
    if (!empty($condition)) {
      foreach ($condition as $field_name => $val) {
        $db_field_name = 'field_data_' . $field_name;
        $flatrate_query->leftJoin($db_field_name, $field_name, 'n.nid = ' . $field_name . '.entity_id');

        $db_field_value_name = $field_name . '_value';
        $operator = '';
        if (is_array($val)) {
          $operator = 'IN';
        }
        $flatrate_query->condition($db_field_value_name, $val, $operator);
      }
    }
    $flatrate_nids = $flatrate_query->execute()->fetchALL();

    // Request.
    $request_query = db_select('node', 'n');
    $request_query->leftJoin('field_data_field_date', 'date', 'n.nid = date.entity_id');
    $request_query->fields('date', array('field_date_value'));
    $request_query->condition('date.field_date_value', array($date_from, $date_to), 'BETWEEN');
    if (!empty($condition)) {
      foreach ($condition as $field_name => $val) {
        $db_field_name = 'field_data_' . $field_name;
        $request_query->leftJoin($db_field_name, $field_name, 'n.nid = ' . $field_name . '.entity_id');
        $db_field_value_name = $field_name . '_value';
        $operator = '';
        if (is_array($val)) {
          $operator = 'IN';
        }
        $request_query->condition($db_field_value_name, $val, $operator);
      }
    }
    $request_query->groupBy('date.field_date_value');
    $request_nids = $request_query->execute()->fetchALL();

    if (!empty($flatrate_nids)) {
      foreach ($flatrate_nids as $row) {
        $datetime = new \DateTime($row->field_delivery_date_from_value);
        $result[$datetime->format('Y-m-d')] = TRUE;
        unset($datetime);
      }
    }

    if (!empty($request_nids)) {
      foreach ($request_nids as $row) {
        $datetime = new \DateTime($row->field_date_value);
        $result[$datetime->format('Y-m-d')] = TRUE;
        unset($datetime);
      }
    }

    $trips = Parking::getTripParkingForGeneralParklotByDate($date_from, $date_to, 'group_by_full_date');
    if (!empty($trips)) {
      $result = array_merge($result, $trips);
    }

    return $result;
  }

  /**
   * Get calendar all branches.
   *
   * @param int $date
   *   Date.
   * @param array $condition
   *   Condition.
   *
   * @return array
   *   .
   */
  public function getCalendarDiaspatchBranch(int $date, array $condition) {
    $result = array();
    $branches = $this->branch->index();
    // Lambda function.
    $merge_result = function ($temp_res) use (&$result) {
      foreach ($temp_res as $day => $value) {
        $result[$day] = !empty($result[$day]) || $value;
      }
    };
    // If delete all branches, but we got here, get local dispatch.
    if (empty($branches)) {
      return $this->getDispatchCalendar($date, $condition);
    }

    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        $temp_res = $this->getDispatchCalendar($date, $condition);
        $merge_result($temp_res);
        continue;
      }

      $data = json_encode(array(
        'date' => $date,
        'condition' => $condition,
      ));
      $options = $this->generateOptions('POST', $data);
      $url = $branch->api_url . $this->getCalendarDispat;
      $response = drupal_http_request($url, $options);
      if ($response->code == 200) {
        $response_data = (array) json_decode($response->data);
        $merge_result($response_data);
      }
      else {
        watchdog('Branch', "<pre>Failed get dispatch calendar<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }

    return $result;
  }

  /**
   * Handler conflict request.
   *
   * @param mixed $node_wrapper
   *   Node id.
   * @param mixed $data
   *   Node data.
   *
   * @return bool
   *   Result.
   *
   * @throws \Exception
   *   Input data.
   */
  public function handlerConflictRequest($node_wrapper, $data = FALSE) {
    // Lambda function.
    $getExternalIds = function (array $list_truck) {
      $res = array();
      foreach ($list_truck as $truck) {
        $res[] = $this->getExternalId($truck->tid ?? $truck);
      }
      return $res;
    };
    $result = FALSE;
    $branches = $this->branch->index();

    if (empty($node_wrapper) && !empty($data)) {
      if (!empty($data['field_list_truck'])) {
        $data['field_list_truck'] = $getExternalIds(array_unique($data['field_list_truck']));
      }
      $data['field_date'] = strtotime($data['field_date']);
      $data['branch'] = $this->branch->retrieve();
    }
    elseif (!empty($node_wrapper) && empty($data)) {
      $data = array(
        'nid' => $node_wrapper->nid->value(),
        'field_date' => $node_wrapper->field_date->value(),
        'field_travel_time' => $node_wrapper->field_travel_time->value(),
        'field_double_travel_time' => $node_wrapper->field_double_travel_time->value(),
        'field_maximum_move_time' => $node_wrapper->field_maximum_move_time->value(),
        'field_actual_start_time' => $node_wrapper->field_actual_start_time->value(),
        'field_list_truck' => $getExternalIds($node_wrapper->field_list_truck->value()),
        'branch' => $this->branch->retrieve(),
      );
    }
    else {
      throw new \Exception("Input data is empty.");
    }

    foreach ($branches as $branch) {
      if ($this->branch->isCurrentBranch($branch->api_url)) {
        $result = $result || $this->checkOverbooking($data);
        continue;
      }

      $postData = json_encode(array('node' => $data));
      $options = $this->generateOptions('POST', $postData);
      $url = $branch->api_url . $this->checkOverbooking;
      $response = drupal_http_request($url, $options);
      if ($response->code != 200) {
        watchdog('Branch', "<pre>Failed overbooking<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
      else {
        $responseData = json_decode($response->data);
        $result = $result || reset($responseData);
      }
    }
    return $result;
  }

  /**
   * Check conflict on branch.
   *
   * @param array $node
   *   Data node.
   *
   * @return bool
   *   Result.
   */
  public function checkOverbooking(array $node) {
    try {
      $overbooking = FALSE;

      $request_date = $node['field_date'];
      // Get Park_lot array of unconfirmed requests.
      $park_lot = move_admin_get_same_day_request($request_date);

      $travel_time = $node['field_travel_time'];

      if (!empty($node['field_double_travel_time'])) {
        $travel_time += (float) $node['field_double_travel_time'] / 2;
      }

      $work_time = ($node['field_maximum_move_time'] + $travel_time) * 60;
      $work_time = round($work_time, 1, PHP_ROUND_HALF_UP);
      if (!$work_time) {
        throw new ActualWorkTimeException("For request {$node['nid']} can't be calculated work_time: $work_time ");
      }

      $start_time = $node['field_actual_start_time'] ? $node['field_actual_start_time'] : '8:00 AM';

      // Lambda function.
      // Add time to request move_date.
      $time_odd = function (string $time, int $date) : int {
        $time_date = new \DateTime();
        $time_date->setDate(date("Y", $date), date("m", $date), date("d", $date));
        $time_date->setTime(date("H", strtotime($time)), date("i", strtotime($time)));
        return $time_date->getTimestamp();
      };

      // Lambda function.
      // To the $time will add $add_time hours.
      // Example: if $time "09:00AM" and $add_time is "2" - result will be "11:00AM".
      $time_even = function (int $time, int $add_time) : int {
        $time_date = new \DateTime();
        $time_date->setTimestamp($time);
        $time_date->add(new \DateInterval("PT{$add_time}M"));
        return $time_date->getTimestamp();
      };

      $time_1_time = $start_time ? $start_time : "05:00 AM";
      // Get date with time when pickup will start.
      $time_1 = $time_odd($time_1_time, $request_date);
      // Get time when delivery will end.
      $time_2 = $time_even($time_1, $work_time);
      if (!$time_1) {
        throw new Exception("\$time_1 of nid: {$node['nid']} is not calculated!");
      }
      if (!$time_2) {
        throw new Exception("\$time_2 of nid: {$node['nid']} is not calculated!");
      }
      // Get truck for this branch.
      $current_trucks = array();
      if (!empty($node['field_list_truck'])) {
        foreach ($node['field_list_truck'] as $truck) {
          $current_trucks[] = $this->getThisBranchTid($truck);
        }
      }
      $curr_branch = $this->branch->retrieve()['api_url'];
      foreach ($current_trucks as $truck) {
        if (!empty($park_lot[$truck])) {
          foreach ($park_lot[$truck] as $request) {
            try {
              // $request have only status "Not Confirmed".
              if ($request['nid'] == $node['nid'] && !empty($node['branch']['api_url']) && $node['branch']['api_url'] == $curr_branch) {
                continue;
              }

              if (empty($request['sT1'])) {
                throw new Exception("'\$request[\'sT1\']\' of nid: {$request['nid']} is empty!");
              }
              $request['work_time'] = $request['work_time'] * 60;
              $request['work_time'] = round($request['work_time'], 1, PHP_ROUND_HALF_UP);

              if (!$request['maximum_time']) {
                throw new Exception("maximum_time of nid: {$request['nid']} is empty!");
              }

              // Get date with time when pickup will start.
              $time_3 = $time_odd($request['sT1'], $request_date);
              // Get time when delivery will end.
              $time_4 = $time_even($time_3, $request['work_time']);
              if (!$time_3) {
                throw new Exception("\$time_3 of nid: {$request['nid']} is not calculated!");
              }
              if (!$time_4) {
                throw new Exception("\$time_4 of nid: {$request['nid']} is not calculated!");
              }


              $conflict = _move_is_intersect($time_1, $time_2, $time_3, $time_4);
              if ($conflict && $request['approve'] == RequestStatusTypes::CONFIRMED) {
                $overbooking = TRUE;
              }

              if ($conflict && ($request['field_reservation_received'] && $request['approve'] != RequestStatusTypes::PENDING_INFO)) {
                $overbooking = TRUE;
              }
            }
            catch (\Throwable $e) {
              $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
              watchdog("overbooking", $error_text, array(), WATCHDOG_ERROR);
            }
          }
        }

      }
    }
    catch (\Throwable $e) {
      $error_text = "Text: {$e->getMessage()}; Code: {$e->getCode()}; File: {$e->getFile()}; Line: {$e->getLine()}; </br> Trace: {$e->getTraceAsString()}";
      watchdog("overbooking check", $error_text, array(), WATCHDOG_ERROR);
    }
    finally {
      return $overbooking;
    }
  }

  /**
   * Helper method get external id.
   *
   * @param int $tid
   *   Terms id.
   *
   * @return mixed
   *   Id or false.
   */
  public function getExternalId(int $tid) {
    $id = db_select('move_branch_trucks', 'mbt')
      ->fields('mbt', array('id'))
      ->condition('mbt.tid', $tid)
      ->execute()
      ->fetchAssoc();

    if (!empty($id['id'])) {
      return $id['id'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Helper method get tid.
   *
   * @param int $id
   *   External id.
   *
   * @return int
   *   Tid.
   */
  private function getThisBranchTid(int $id) {
    $tid = db_select('move_branch_trucks', 'mbt')
      ->fields('mbt', array('tid'))
      ->condition('mbt.id', $id)
      ->execute()
      ->fetchAssoc();

    if (!empty($tid['tid'])) {
      return $tid['tid'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Helper method get tid.
   *
   * @param array $ids
   *   External id.
   *
   * @return array
   *   Ids with tids.
   */
  private function getThisBranchTids(array $ids) : array {
    $result = array();
    $query = db_select('move_branch_trucks', 'mbt')
      ->fields('mbt', array('tid', 'id'))
      ->condition('mbt.id', array($ids), 'IN')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    if (!empty($query)) {
      foreach ($query as $row) {
        $result[$row['id']] = $row['tid'];
      }
    }

    return $result;
  }

  /**
   * Helper method generate options for request.
   *
   * @param string $method
   *   POST or GET.
   * @param string $data
   *   Data for POST.
   * @param array $headers
   *   Headers.
   *
   * @return array
   *   Options.
   */
  private function generateOptions(string $method, string $data, array $headers = array()) {
    $headers += array(
      'Content-Type' => 'application/json',
      'accept' => 'application/json',
    );
    $options = array(
      'method' => $method,
      'data' => $data,
      'headers' => $headers,
    );
    return $options;
  }

}

class ActualWorkTimeException extends \Exception {}
