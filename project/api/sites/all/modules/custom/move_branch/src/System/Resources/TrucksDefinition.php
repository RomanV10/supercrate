<?php

namespace Drupal\move_branch\System\Resources;

use Drupal\move_branch\Services\Trucks;
use Drupal\move_parking\Util\enum\TruckUnavailableType;
use JMS\Serializer\Annotation\Type;

/**
 * Class TrucksDefinition.
 *
 * @package Drupal\move_branch\System
 */
class TrucksDefinition {

  /**
   * Get definition.
   *
   * @return array
   *   $resources
   */
  public static function getDefinition() {
    return array(
      'move_branch_trucks' => array(
        'operations' => array(
          'retrieve' => array(),
          'create' => array(
            'help' => 'Create common truck on all branches',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of new truck',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete' => array(
            'help' => 'Delete a common truck on all branches',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'tid',
                'type' => 'int',
                'description' => 'The terms id.',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'help' => 'Common truck update on all branches',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'tid',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of taxonomy trucks term.',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'index' => array(
            'help' => 'Get all common trucks',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'create_branch_truck' => array(
            'help'   => 'Create сommon truck',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::createBranchTruck',
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of new taxonomy trucks term.',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'truck_edit_branch' => array(
            'help' => 'Edit common truck',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::truckEditBranch',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'The data of taxonomy trucks term.',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'check_remove_truck' => array(
            'help' => 'Check remove branch truck',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::checkRemoveTruck',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'optional' => FALSE,
                'source' => array('data' => 'id'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'remove_branch_truck' => array(
            'help' => 'Remove truck in current branch',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::removeBranchTruck',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'optional' => FALSE,
                'source' => array('data' => 'id'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'save_trucks_setting' => array(
            'help' => 'Save setting truck on branches',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::saveTrucksSetting',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'name',
                'optional' => FALSE,
                'source' => array('data' => 'name'),
                'type' => 'string',
              ),
              array(
                'name' => 'value',
                'optional' => FALSE,
                'source' => array('data' => 'value'),
                'type' => 'bool',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'save_setting_branch' => array(
            'help' => 'Save setting truck on branches',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::saveSettingBranch',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'name',
                'optional' => FALSE,
                'source' => array('data' => 'name'),
                'type' => 'string',
              ),
              array(
                'name' => 'value',
                'optional' => FALSE,
                'source' => array('data' => 'value'),
                'type' => 'bool',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'truck_unavailable_branches' => array(
            'help' => 'Add unavailable date for truck on branches',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::truckUnavailableBranches',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'tid',
                'type' => 'int',
                'source' => array('data' => 'tid'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date',
                'type' => 'array',
                'description' => 'Date unavailable truck.',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'change_truck_unavailable' => array(
            'help' => 'Change unavailable date for truck on branch',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::changeTruckUnavailable',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date',
                'type' => 'array',
                'description' => 'Date unavailable truck.',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'type',
                'type' => 'int',
                'description' => 'Type unavailable date.',
                'source' => array('data' => 'type'),
                'optional' => TRUE,
                'default value' => 0,

              ),
            ),
            'access arguments' => array('access content'),
          ),
          'change_truck_unavailable_from_request' => array(
            'help' => 'Change unavailable date for truck on branch',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::changeTruckUnavailableFromRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'ids',
                'type' => 'array',
                'source' => array('data' => 'ids'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'date',
                'type' => 'array',
                'description' => 'Date unavailable truck.',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'truck_unavailable_roll_back' => array(
            'help' => 'Change unavailable date for truck on branch',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::truckUnavailableRollBack',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('data' => 'id'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'Date unavailable truck.',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'truck_unavailable_roll_back_from_request' => array(
            'help' => 'Change unavailable date for truck on branch from request',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::truckUnavailableRollBackFromRequest',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'description' => 'Date unavailable truck.',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_parklot_truck_edition' => array(
            'help' => 'Get parklot',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::getParklotTruckEdition',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'optional' => FALSE,
                'source' => array('data' => 'date'),
                'type' => 'string',
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'source' => array('data' => 'condition'),
                'type' => 'array',
                'default value' => FALSE,
              ),
              array(
                'name' => 'period',
                'optional' => FALSE,
                'source' => array('data' => 'period'),
                'type' => 'int',
                'default value' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_month_parklot_branch' => array(
            'help' => 'Get parklot month',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::getMonthParklotBranch',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'optional' => FALSE,
                'source' => array('data' => 'date'),
                'type' => 'string',
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'source' => array('data' => 'condition'),
                'type' => 'array',
                'default value' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_calendar_dispatch' => array(
            'help' => 'Get calendar for dispatch',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::getCalendarDispatch',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'optional' => FALSE,
                'source' => array('data' => 'date'),
                'type' => 'int',
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'source' => array('data' => 'condition'),
                'type' => 'array',
                'default value' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_calendar_dispatch_branch' => array(
            'help' => 'Get calendar for dispatch',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::getCalendarDispatchBranch',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'optional' => FALSE,
                'source' => array('data' => 'date'),
                'type' => 'int',
              ),
              array(
                'name' => 'condition',
                'optional' => TRUE,
                'source' => array('data' => 'condition'),
                'type' => 'array',
                'default value' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'check_overbooking' => array(
            'help' => 'Get request',
            'callback' => 'Drupal\move_branch\System\Resources\TrucksDefinition::checkOverbooking',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Resources/TrucksDefinition',
            ),
            'args' => array(
              array(
                'name' => 'date',
                'optional' => FALSE,
                'source' => array('data' => 'node'),
                'type' => 'array',
              ),
            ),
            'access arguments' => array('access content'),
          ),
        ),
      ),
    );
  }

  /**
   * Create trucks on branches and save.
   *
   * @param array $data
   *   Data.
   *
   * @return mixed
   *   Return id.
   */
  public static function create(array $data) {
    $trucks = new Trucks();
    return $trucks->create($data);
  }

  /**
   * Delete trucks on all branches.
   *
   * @param int $tid
   *   Terms id.
   *
   * @return mixed
   *   Custom answer.
   */
  public static function delete(int $tid) {
    $trucks = new Trucks($tid);
    return $trucks->delete();
  }

  /**
   * Edit truck on all branches.
   *
   * @param int $tid
   *   Terms.
   * @param array $data
   *   Data.
   *
   * @return bool|mixed
   *   Custom answer.
   */
  public static function update(int $tid, array $data) {
    $trucks = new Trucks($tid);
    return $trucks->update($data);
  }

  /**
   * Get trucks for front.
   *
   * @return array
   *   Trucks.
   */
  public static function index() {
    $trucks = new Trucks();
    return $trucks->index();
  }

  /**
   * Create truck and save.
   *
   * @param array $data
   *   Truck data.
   *
   * @return mixed
   *   Tid id.
   */
  public static function createBranchTruck(array $data) {
    $trucks = new Trucks();
    return $trucks->createTruck($data);
  }

  /**
   * Update current branch truck.
   *
   * @param array $data
   *   Name and external truck id.
   *
   * @return bool
   *   True.
   */
  public static function truckEditBranch(array $data) {
    $trucks = new Trucks();
    return $trucks->updateBranchTruck($data);
  }

  /**
   * Check if we can delete truck.
   *
   * @param int $id
   *   External id truck.
   *
   * @return bool
   *   True or false,
   */
  public static function checkRemoveTruck($id) {
    if (isset($id)) {
      $trucks = new Trucks();
      return $trucks->checkRemoveTruck($id);
    }
  }

  /**
   * Remove in current branch truck.
   *
   * @param int $id
   *   External id.
   */
  public static function removeBranchTruck($id) {
    if (isset($id)) {
      $trucks = new Trucks();
      $trucks->removeBranchTruck($id);
    }
  }

  /**
   * Save setting on all branches.
   *
   * @param string $name
   *   Name.
   * @param bool $value
   *   Value.
   *
   * @return array
   *   Custom answer.
   */
  public static function saveTrucksSetting(string $name, bool $value) {
    $truck = new Trucks();
    return $truck->settingsTrucksSave($name, $value);
  }

  /**
   * Save setting in variable.
   *
   * @param string $name
   *   Variable name.
   * @param bool $value
   *   Variable value.
   *
   * @return bool
   *   TRUE.
   */
  public static function saveSettingBranch(string $name, bool $value) {
    $truck = new Trucks();
    return $truck->saveSetting($name, $value);
  }

  /**
   * Unavailable truck on Branches.
   *
   * @param int $tid
   *   Terms id.
   * @param array $dates
   *   Dates.
   *
   * @return array
   *   Custom answer.
   */
  public static function truckUnavailableBranches(int $tid, array $dates) {
    $truck = new Trucks($tid);
    return $truck->unavailableTruckBranches($dates);
  }

  /**
   * Unavailable truck.
   *
   * @param int $id
   *   External id.
   * @param array $dates
   *   Dates.
   * @param int $type
   *   Type of unavailable date.
   *
   * @return bool
   *   .
   */
  public static function changeTruckUnavailable(int $id, array $dates, int $type = TruckUnavailableType::MANUALLY) {
    $truck = new Trucks();
    return $truck->unavailableTruck($id, $dates, $type);
  }

  /**
   * Unavailable truck.
   *
   * @param array $ids
   *   External ids.
   * @param array $date
   *   Dates.
   * @param int $type
   *   Type of unavailable date.
   *
   * @return bool
   *
   */
  public static function changeTruckUnavailableFromRequest(array $ids, array $date) {
    return (new Trucks())->unavailableTruckFromRequest($ids, $date);
  }

  /**
   * Roll back trucks unv.
   *
   * @param int $id
   *   External id.
   * @param array $data
   *   Data.
   */
  public static function truckUnavailableRollBack(int $id, array $data) {
    $truck = new Trucks();
    $truck->unavailableRollBack($id, $data);
  }

  /**
   * Roll back trucks unv from request.
   *
   * @param array $data
   *   Data.
   */
  public static function truckUnavailableRollBackFromRequest(array $data) {
    $truck = new Trucks();
    $truck->unavailableRollBackFromRequest($data);
  }


  /**
   * Get parklot and change truck object.
   *
   * @param int $date
   *   Date.
   * @param array $condition
   *   Condition.
   * @param int $period
   *   Month or day.
   *
   * @return array
   *   Requests.
   */
  public static function getParklotTruckEdition(int $date, array $condition = array(), int $period = 0) {
    $truck = new Trucks();
    return $truck->getParklotTruckEdition($date, $condition, $period);
  }

  /**
   * Get parklot month.
   *
   * @param string $date
   *   Date.
   * @param array $condition
   *   Conditions.
   *
   * @return array
   *   Nodes.
   */
  public static function getMonthParklotBranch(string $date, array $condition) {
    $truck = new Trucks();
    return $truck->getParklotMonths($date, $condition);
  }

  /**
   * Get dispatch calendar.
   *
   * @param int $date
   *   Date.
   * @param array $condition
   *   Condition.
   *
   * @return array
   *   .
   */
  public static function getCalendarDispatch(int $date, array $condition) {
    $truck = new Trucks();
    return $truck->getDispatchCalendar($date, $condition);
  }

  /**
   * Get calendar all branches.
   *
   * @param int $date
   *   Date.
   * @param array $condition
   *   Condition.
   *
   * @return array
   *   .
   */
  public static function getCalendarDispatchBranch(int $date, array $condition) {
    $truck = new Trucks();
    return $truck->getCalendarDiaspatchBranch($date, $condition);
  }

  /**
   * Check conflict on branch.
   *
   * @param array $node
   *   Node data.
   */
  public static function checkOverbooking(array $node) {
    $truck = new Trucks();
    return $truck->checkOverbooking($node);
  }

}
