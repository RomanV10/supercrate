<?php

namespace Drupal\move_branch\System;

use Drupal\move_branch\Services\Branch;
use Drupal\move_branch\System\Resources\TrucksDefinition;

/**
 * Class Service.
 *
 * @package Drupal\move_branch\System
 */
class Service {

  /**
   * Get definition.
   *
   * @return array
   *   $resources
   */
  public function getResources() {
    $resources = array(
      '#api_version' => 3002,
    );

    $resources += self::definition();
    $resources += TrucksDefinition::getDefinition();
    return $resources;
  }

  /**
   * Definition schema.
   *
   * @return array
   *   .
   */
  private static function definition() {
    return array(
      'move_branch' => array(
        'operations' => array(
          'create' => array(
            'callback' => 'Drupal\move_branch\System\Service::create',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'retrieve' => array(
            'callback' => 'Drupal\move_branch\System\Service::retrieve',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update' => array(
            'callback' => 'Drupal\move_branch\System\Service::update',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'default value' => NULL,
                'optional' => FALSE,
              ),
              array(
                'name' => 'data',
                'type' => 'array',
                'source' => array('data' => 'data'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'delete' => array(
            'callback' => 'Drupal\move_branch\System\Service::delete',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Service',
            ),
            'args' => array(
              array(
                'name' => 'id',
                'type' => 'int',
                'source' => array('path' => 0),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'index' => array(
            'callback' => 'Drupal\move_branch\System\Service::index',
            'file' => array(
              'type' => 'php',
              'module' => 'move_branch',
              'name' => 'src/System/Service',
            ),
            'access arguments' => array('access content'),
          ),
        ),
        'actions' => array(
          'make_migrate' => array(
            'help'   => 'Returns the value of a system variable using variable_get().',
            'access arguments' => array('access content'),
            'callback' => 'Drupal\move_branch\System\Service::makeMigrate',
            'args' => array(
              array(
                'name' => 'nid',
                'optional' => FALSE,
                'source' => array('data' => 'nid'),
                'description' => t('Node id.'),
                'type' => 'int',
              ),
              array(
                'name' => 'branch_id',
                'optional' => FALSE,
                'source' => array('data' => 'branch_id'),
                'description' => t('Branch id.'),
                'type' => 'int',
              ),
            ),
          ),
          'check_belong' => array(
            'help'   => 'Check belong branch.',
            'callback' => 'Drupal\move_branch\System\Service::checkBelongLink',
            'args' => array(
              array(
                'name' => 'url',
                'optional' => FALSE,
                'source' => array('data' => 'url'),
                'description' => t('Api url.'),
                'type' => 'string',
              ),
              array(
                'name' => 'r_s',
                'optional' => FALSE,
                'source' => array('data' => 'r_s'),
                'description' => t('Receiver or Sender. 1 = receiver, 0 = sender.'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'synchronize' => array(
            'help'   => 'Delete all values from seetings on other branches and new',
            'callback' => 'Drupal\move_branch\System\Service::synchronizeBranches',
            'access arguments' => array('access content'),
          ),
          'accept_synchronize' => array(
            'help'   => 'Accept Synchronize data from remote server',
            'callback' => 'Drupal\move_branch\System\Service::acceptSynchronize',
            'args' => array(
              array(
                'name' => 'data',
                'optional' => FALSE,
                'source' => array('data' => 'data'),
                'description' => t('Array of branches settings.'),
                'type' => 'array',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_closest_branch' => array(
            'help'   => 'Get closest branch.',
            'callback' => 'Drupal\move_branch\System\Service::getClosestBranch',
            'args' => array(
              array(
                'name' => 'zip_from',
                'optional' => FALSE,
                'source' => array('data' => 'zip_from'),
                'description' => t('ZIP from'),
                'type' => 'string',
              ),
              array(
                'name' => 'zip_to',
                'optional' => FALSE,
                'source' => array('data' => 'zip_to'),
                'description' => t('ZIP to'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_parking_zip' => array(
            'help'   => 'Update parking zip in current branch',
            'callback' => 'Drupal\move_branch\System\Service::updateParkingZip',
            'args' => array(
              array(
                'name' => 'zip',
                'optional' => FALSE,
                'source' => array('data' => 'zip'),
                'description' => t('string parking zip'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_based_state' => array(
            'help'   => 'Update based state in current branch',
            'callback' => 'Drupal\move_branch\System\Service::updateBasedState',
            'args' => array(
              array(
                'name' => 'based_state',
                'optional' => FALSE,
                'source' => array('data' => 'based_state'),
                'description' => t('based state'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'get_parking_state' => array(
            'help'   => 'Get parking zip and state.',
            'callback' => 'Drupal\move_branch\System\Service::getParkingState',
            'access arguments' => array('access content'),
          ),
          'clone_manager_to_branch' => array(
            'help'   => 'Clone manager or sales on other branch.',
            'callback' => 'Drupal\move_branch\System\Service::cloneManager',
            'args' => array(
              array(
                'name' => 'uid',
                'optional' => FALSE,
                'source' => array('data' => 'uid'),
                'description' => t('User id.'),
                'type' => 'int',
              ),
              array(
                'name' => 'branch_id',
                'optional' => FALSE,
                'source' => array('data' => 'branch_id'),
                'description' => t('Branch id.'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'update_user_hash' => array(
            'help'   => 'Update user hash on new branch.',
            'callback' => 'Drupal\move_branch\System\Service::UpdateUserHash',
            'args' => array(
              array(
                'name' => 'uid',
                'optional' => FALSE,
                'source' => array('data' => 'uid'),
                'description' => t('User id.'),
                'type' => 'int',
              ),
              array(
                'name' => 'hash',
                'optional' => FALSE,
                'source' => array('data' => 'hash'),
                'description' => t('Old hash.'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'save_logo' => array(
            'help'   => 'Save logo for branch',
            'callback' => 'Drupal\move_branch\System\Service::saveLogo',
            'args' => array(
              array(
                'name' => 'image',
                'optional' => FALSE,
                'source' => array('data' => 'image'),
                'description' => t('Base64 picture'),
                'type' => 'string',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'accept_settings' => array(
            'help'   => 'Save settings',
            'callback' => 'Drupal\move_branch\System\Service::acceptSettings',
            'args' => array(
              array(
                'name' => 'name',
                'optional' => FALSE,
                'source' => array('data' => 'name'),
                'type' => 'string',
              ),
              array(
                'name' => 'value',
                'optional' => FALSE,
                'source' => array('data' => 'value'),
                'type' => 'int',
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'create_wrapper_for_client' => array(
            'help'   => 'Create client',
            'callback' => 'Drupal\move_branch\System\Service::createWrapperForClient',
            'args' => array(
              array(
                'name' => 'account',
                'type' => 'array',
                'description' => 'The user object',
                'source' => 'data',
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('access content'),
          ),
          'set_parklot_note' => array(
            'callback' => 'Drupal\move_branch\System\Service::addParklotNote',
            'args' => array(
              array(
                'name' => 'date',
                'type' => 'string',
                'source' => array('data' => 'date'),
                'optional' => FALSE,
              ),
              array(
                'name' => 'note',
                'type' => 'int',
                'source' => array('data' => 'note'),
                'optional' => FALSE,
              ),
            ),
            'access arguments' => array('administer site configuration'),
          ),
        ),
      ),
    );
  }

  /**
   * Create branch.
   *
   * @param mixed $data
   *   Branch data.
   *
   * @return mixed
   *   result.
   */
  public static function create($data = array()) {
    $branch = new Branch();
    return $branch->create($data);
  }

  /**
   * Retrieve branch.
   *
   * @param int $id
   *   Id branch.
   *
   * @return mixed
   *   Array branch.
   */
  public static function retrieve(int $id) {
    $branch = new Branch($id);
    return $branch->retrieve();
  }

  /**
   * Update branch.
   *
   * @param int $id
   *   Id branch.
   * @param array $data
   *   Array update.
   *
   * @return mixed
   *   .
   */
  public static function update(int $id, array $data) {
    $branch = new Branch($id);
    return $branch->update($data);
  }

  /**
   * Delete branch.
   *
   * @param int $id
   *   Id branch.
   *
   * @return mixed
   *   .
   */
  public static function delete(int $id) {
    $branch = new Branch($id);
    return $branch->delete();
  }

  /**
   * Get all branching.
   */
  public static function index() {
    $branch = new Branch();
    return $branch->index();
  }

  /**
   * Check belongs link.
   *
   * @param string $url
   *   URL to api.
   * @param int $r_s
   *   Receiver or sender.
   *
   * @return bool
   *   True or false.
   */
  public static function checkBelongLink(string $url, int $r_s) {
    $branch = new Branch();
    return $branch->checkBelongsLink($url, $r_s);
  }

  /**
   * Make migrate.
   *
   * @param int $nid
   *   Id request.
   * @param int $branch_id
   *   Id branche.
   *
   * @return array|mixed
   *   Data about create request.
   */
  public static function makeMigrate($nid, $branch_id) {
    $branch = new Branch($branch_id, $nid);
    return $branch->makeMigrate();
  }

  /**
   * Synchronize current brach with other.
   */
  public static function synchronizeBranches() {
    $branch = new Branch();
    return $branch->synchronizeBranches();
  }

  /**
   * Accept Synchronize.
   *
   * @param mixed $data
   *   Array data.
   *
   * @return array
   *   .
   */
  public static function acceptSynchronize($data) {
    $branch = new Branch();
    return $branch->acceptSynchronize($data);
  }

  /**
   * Get closest branch.
   *
   * @param string $zip_from
   *   ZIP from.
   * @param string $zip_to
   *   ZIP to.
   *
   * @return bool
   *   .
   */
  public static function getClosestBranch(string $zip_from, string $zip_to) {
    $branch = new Branch();
    return $branch->checkClosestBranch($zip_from, $zip_to);
  }

  /**
   * Update parking zip.
   *
   * @param string $zip
   *   Parking zip.
   *
   * @return array
   *   Result operation synchronize branches.
   */
  public static function updateParkingZip(string $zip) {
    $branch = new Branch();
    return $branch->updateCurrentBranchParkingZip($zip);
  }

  /**
   * Update based state.
   *
   * @param string $basedState
   *   Based state.
   *
   * @return array
   *   Result operation synchronize branches.
   */
  public static function updateBasedState(string $basedState) {
    $branch = new Branch();
    return $branch->updateCurrentBranchBasedState($basedState);
  }

  /**
   * Get parking zip state.
   *
   * @return array
   *   .
   */
  public static function getParkingState() {
    $branch = new Branch();
    return $branch->getParkingState();
  }

  /**
   * Clone manager on other branch.
   *
   * @param int $uid
   *   User id.
   * @param int $id
   *   Id branch.
   *
   * @return array|mixed
   *   .
   */
  public static function cloneManager(int $uid, int $id) {
    $branch = new Branch($id);
    return $branch->cloneManagerToBranch($uid);
  }

  /**
   * Update user hash.
   *
   * @param int $uid
   *   User id.
   * @param string $hash
   *   Hash pass.
   */
  public static function updateUserHash(int $uid, string $hash) {
    $branch = new Branch();
    $branch->setUserHash($uid, $hash);
  }

  /**
   * Save logo.
   *
   * @param string $value
   *   Base64.
   *
   * @return string
   *   Path.
   */
  public static function saveLogo(string $value) {
    $branch = new Branch();
    return $branch->saveLogo($value);
  }

  /**
   * Accept settings closest branch.
   *
   * @param string $name
   *   Name.
   * @param int $value
   *   Value.
   */
  public static function acceptSettings(string $name, int $value) {
    (new Branch())->acceptSettings($name, $value);
  }

  /**
   * Wrapper for create user.
   *
   * @param array $data
   *   User data.
   *
   * @return mixed
   *   User.
   *
   * @throws \Exception
   */
  public static function createWrapperForClient(array $data) {
    return (new Branch())->createClientWrapper($data);
  }

  /**
   * Parklot to branch.
   *
   * @param $date
   *   Date checked in parklot.
   * @param $note
   *   Text note.
   */
  public static function addParklotNote($date, $note) {
    (new Branch())->updateDispatchNote($date, $note);
  }

}
