<?php

namespace Drupal\move_branch\Services;

use Drupal\move_services_new\Services\BaseService;
use Drupal\move_services_new\Services\Clients;
use Drupal\move_services_new\Services\move_request\MoveRequest;
use Drupal\move_distance_calculate\Services\DistanceCalculate;
use Drupal\move_services_new\Util\enum\SendingRequestBranchSettings;
use Drupal\move_services_new\Services\Users;

/**
 * Class Branch.
 *
 * @package Drupal\move_branch\Services
 */
class Branch extends BaseService {
  private $id = NULL;
  private $node = NULL;
  private $actionMethod = '/server/move_request';
  private const PARKLOT_NOTE_URL = '/server/move_branch/set_parklot_note';

  /**
   * Branch constructor.
   *
   * @param mixed $id
   *   Id branch.
   * @param mixed $nid
   *   Id node.
   */
  public function __construct($id = NULL, $nid = NULL) {
    $this->id = $id;
    if (!empty($nid)) {
      $this->node = node_load($nid);
    }
  }

  /**
   * Create branch.
   *
   * @param mixed $data
   *   Branch data.
   *
   * @return \DatabaseStatementInterface|mixed
   *   Id.
   *
   * @throws \ServicesException
   * @throws \Exception
   */
  public function create($data = array()) {
    // Validation api url and front url.
    $api_url = $this->checkUrl($data['api_url']);
    $front_url = $this->checkUrl($data['front_url']);

    if (empty($api_url) || empty($front_url)) {
      watchdog('Branch', "<pre>Incorrect ulr: <br> :a <br> :b</pre>", array(':a' => $data['api_url'], ':b' => $data['front_url']), WATCHDOG_INFO);
      return services_error("Incorrect links", 406);
    }

    if (!$this->checkExistBranch($data['api_url']) && $this->checkBelongsLink($data['api_url'], 0)) {
      $returned_data = new \stdClass();
      $options = array(
        'method' => 'POST',
        'headers' => array('Content-Type' => 'application/json', 'accept' => 'application/json'),
      );
      $url = $data['api_url'] . "/server/move_branch/get_parking_state";
      $result = drupal_http_request($url, $options);
      if ($result->code == 200) {
        $returned_data = (object) json_decode($result->data);
      }
      else {
        watchdog('Branch', "<pre>Failed to get parking_zip and based_state <br> :a</pre>>", array(':a' => print_r($result, TRUE)), WATCHDOG_ERROR);
      }
      $id = db_insert('move_branch_settings')
        ->fields(array(
          'name' => $data['name'],
          'api_url' => $api_url,
          'front_url' => $front_url,
          'logo' => !empty($data['logo']) ? $data['logo'] : NULL,
          'parking_zip' => !empty($returned_data->parking_zip) ? $returned_data->parking_zip : '',
          'based_state' => !empty($returned_data->based_state) ? $returned_data->based_state : '',
          'updated' => time(),
        ))
        ->execute();
      if ($id > 0) {
        $returned_data->id = $id;
        $post_data = array();
        // Add rows in table move_branch_trucks.
        $trucks = db_select('move_branch_trucks', 'mbt')
          ->fields('mbt', array())
          ->execute()
          ->fetchAll();
        foreach ($trucks as $truck) {
          $term = entity_load('taxonomy_term', array((int) $truck->tid));
          $term = reset($term);
          $post_data[] = array('name' => $term->name);
        }
        $options = array(
          'method' => 'POST',
          'data' => json_encode($post_data),
          'headers' => array(
            'Content-Type' => 'application/json',
            'accept' => 'application/json',
          ),
        );
        $url = $api_url . "/server/move_branch/create_branch_truck";
        $response = drupal_http_request($url, $options);
        if ($response->code != 200) {
          watchdog('Branch', "<pre>Failed<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
        }
      }
    }
    else {
      $message = "This branch has already been created or incorrectly specified branch URL";
      watchdog('Branch', $message, array(), WATCHDOG_INFO);
      return services_error($message, 400);
    }

    return $returned_data;
  }

  /**
   * Retrieve branch.
   *
   * @return mixed
   *   array.
   */
  public function retrieve() {
    $result = db_select('move_branch_settings', 'mbs')
      ->fields('mbs')
      ->condition('mbs.id', $this->id)
      ->execute()
      ->fetchAssoc();

    return $result;
  }

  /**
   * Update branch.
   *
   * @param mixed $data
   *   Branch data.
   *
   * @return int|object
   *   .
   *
   * @throws \ServicesException
   * @throws \Exception
   */
  public function update($data = array()) {
    $currBranch = $this->retrieve();
    if (empty($currBranch)) {
      $message = "Brunch not found.";
      watchdog('Branch', $message, array(), WATCHDOG_ERROR);
      return services_error($message, 400);
    }
    // Validation api url and front url.
    if (!empty($data['api_url'])) {
      $api_url = $this->checkUrl($data['api_url']);
    }
    if (!empty($data['api_url'])) {
      $front_url = $this->checkUrl($data['front_url']);
    }
    $result = db_merge("move_branch_settings")
      ->key(array('id' => $this->id))
      ->fields(array(
        'name' => !empty($data['name']) ? $data['name'] : $currBranch['name'],
        'api_url' => !empty($api_url) ? $api_url : $currBranch['api_url'],
        'front_url' => !empty($front_url) ? $front_url : $currBranch['front_url'],
        'logo' => !empty($data['logo']) ? ($data['logo']) : $currBranch['logo'],
        'parking_zip' => !empty($data['parking_zip']) ? ($data['parking_zip']) : $currBranch['parking_zip'],
        'based_state' => !empty($data['based_state']) ? ($data['based_state']) : $currBranch['based_state'],
        'updated' => time(),
      ))
      ->execute();

    return $result;
  }

  /**
   * Delete branch.
   *
   * @return bool|\DatabaseStatementInterface|object
   *   .
   *
   * @throws \Exception
   */
  public function delete() {
    $branches = $this->index();
    $result = db_delete('move_branch_settings')
      ->condition('id', $this->id)
      ->execute();

    db_truncate('move_branch_trucks')->execute();

    $this->synchronizeBranches($branches);
    // Update variable common truck.
    if (!$this->checkBranchingIsActive()) {
      variable_set('is_common_branch_truck', FALSE);
    }
    return $result;
  }

  /**
   * Get all data about branches.
   *
   * @return mixed
   *   .
   */
  public function index() {
    $result = db_select('move_branch_settings', 'mbs')
      ->fields('mbs')
      ->execute()
      ->fetchAll();

    return $result;
  }

  /**
   * Check for existence the link.
   *
   * @param string $url
   *   URL to.
   *
   * @return bool
   *   .
   */
  public function checkExistBranch(string $url) {
    $result = db_select('move_branch_settings', 'mbs')
      ->fields('mbs')
      ->condition('mbs.api_url', $url)
      ->execute()
      ->fetchAssoc();

    return $result ? TRUE : FALSE;
  }

  /**
   * Check belong link.
   *
   * @param string $url
   *   Api url.
   * @param int $r_s
   *   Receiver or sender. 1 = receiver, 0 = sender.
   *
   * @return bool|string
   *   Bool when sender, string when receiver.
   */
  public function checkBelongsLink(string $url, int $r_s) {
    if ($r_s) {
      $data['url'] = $url;
      return $data;
    }
    else {
      $returned_data = new \stdClass();
      $url = $url . "/server/move_branch/check_belong";
      $data['url'] = $url;
      $data['r_s'] = 1;
      $json_data = json_encode($data);
      $options = array(
        'method' => 'POST',
        'data' => $json_data,
        'headers' => array('Content-Type' => 'application/json', 'accept' => 'application/json'),
      );
      $result = drupal_http_request($url, $options);
      if (!empty($result) && $result->code == '200') {
        $returned_data = (object) json_decode($result->data);
      }
      else {
        watchdog('Branch', "Failed to check belong link <pre>:log</pre>", array(":log" => print_r($result, TRUE)), WATCHDOG_ERROR);
        return FALSE;
      }

      return !empty($returned_data->url) ? TRUE : FALSE;
    }
  }

  /**
   * Transfer request on other branches.
   *
   * @return array|mixed
   *   Array data about request.
   *
   * @throws \ServicesException
   * @throws \Exception
   */
  public function makeMigrate() {
    // Check if request status not pending.
    $emv_node = entity_metadata_wrapper('node', $this->node);
    if ($emv_node->field_approve->raw() != 1) {
      $message = "You can only transfer requests with status pending";
      watchdog('Branch', $message, array(), WATCHDOG_ERROR);
      return services_error($message, 400);
    }
    $returned_data = array();
    $currBranch = $this->retrieve();
    $urlToMigrate = $currBranch['api_url'] . $this->actionMethod;
    if (!empty($urlToMigrate) && !$this->isCurrentBranch($currBranch['api_url'])) {
      $needed_fields = $this->neededFields();
      $request_data = $this->node;
      if (!empty($request_data)) {
        $field_list = field_info_instances('node', 'move_request');
        $data['data'] = array();

        // Collect data from fields for request.
        foreach ($needed_fields as $field_name) {
          if (!empty($request_data->{$field_name})) {
            if (is_array($request_data->{$field_name})) {
              $widget_type = $field_list[$field_name]['widget']['type'];
              $field_info = field_info_field($field_name);
              $field_type = $field_info['type'];
              $value = $request_data->{$field_name}[LANGUAGE_NONE];
              $field_value = $this->getFieldArray($value, $field_type, $widget_type, $field_info);
              $data['data'][$field_name] = $field_value[$field_name];
            }
          }
        }

        // Add accout data to request info.
        $data['data']['account'] = array(
          'name' => $data['data']['field_e_mail'],
          'fields' => array(
            'field_user_first_name' => $data['data']['field_first_name'],
            'field_user_last_name' => $data['data']['field_last_name'],
            'field_primary_phone' => $data['data']['field_phone'],
          ),
          'mail' => $data['data']['field_e_mail'],
        );

        // Status and title add inside of loop. If future need refactor.
        $data['data']['title'] = $request_data->title;
        $data['data']['status'] = $request_data->status;
        $json_data = json_encode($data);
        $options = array(
          'method' => 'POST',
          'data' => $json_data,
          'headers' => array('Content-Type' => 'application/json', 'accept' => 'application/json'),
        );
        $result = drupal_http_request($urlToMigrate, $options);
        if (!empty($result) && !empty($result->data) && $result->code == '200') {
          $returned_data = (object) json_decode($result->data);
          if (!empty($returned_data->nid)) {
            global $base_url;
            $data_for_insert = array(
              'from_nid' => $request_data->nid,
              'to_nid' => $returned_data->nid,
              'from_url' => $base_url,
              'to_url' => $currBranch['api_url'],
              'user_mail' => $data['data']['field_e_mail'],
              'created' => time(),
            );
            self::insertToMigrateLog($data_for_insert);
            // Update request status archive = 22.
            $emv_node->field_approve->set(22);
            $emv_node->save();
          }
        }
        else {
          watchdog('Branch', "<pre>Failed to clone request <br> :a></pre>", array(":a" => print_r($result, TRUE)), WATCHDOG_ERROR);
        }
      }
    }
    return $returned_data;
  }

  /**
   * Helper function.
   *
   * @return array
   *   Need fields.
   */
  public function neededFields() {
    $fields = array(
      "status",
      "title",
      "field_e_mail",
      "field_moving_to",
      "field_phone",
      "field_moving_from",
      "field_date",
      "field_approve",
      "field_size_of_move",
      "field_type_of_entrance_from",
      "field_type_of_entrance_to_",
      "field_start_time",
      "field_move_service_type",
      "field_extra_furnished_rooms",
      "field_poll",
      "field_first_name",
      "field_last_name",
      "field_delivery_date_from",
      "field_delivery_date_to",
      "field_delivery_date",
      "field_price_per_hour",
      "field_movers_count",
      "field_request_completeness",
      "field_delivery_crew_size",
      "field_has_alert",
      "field_new_request",
      "field_crew",
      "field_from_storage_move",
      "field_distance",
      "field_move_service_type",
      "field_parser_provider_name",
    );
    return $fields;
  }

  /**
   * Method save log in db.
   *
   * @param mixed $data_for_insert
   *   Data for insert.
   *
   * @throws \Exception
   */
  public static function insertToMigrateLog($data_for_insert = array()) {
    if (!empty($data_for_insert)) {
      db_insert('move_branch_log')
        ->fields($data_for_insert)
        ->execute();
    }
  }

  /**
   * Helper function validate url.
   *
   * @param string $url
   *   Url.
   *
   * @return bool|string
   *   url or false.
   */
  public function checkUrl(string $url) {
    $res = filter_var($url, FILTER_VALIDATE_URL);
    if (!$res) {
      return FALSE;
    }

    return $url;
  }

  /**
   * Set data field to $form_state. Only for formValidate().
   *
   * @param string $field_name
   *   Name of field.
   * @param string $type
   *   Field type.
   * @param mixed $form_state
   *   A keyed array containing the current state of the form.
   * @param mixed $value
   *   Data with value.
   */
  protected function getValueField($field_name, $type, &$form_state = array(), $value = array(), \stdClass &$node, $widget_type) {
    $field_info = field_info_field($field_name);
    $field_value = $this->getFieldArray($value, $type, $widget_type, $field_info);
    $form_state['values'][$field_name] = $field_value;
    $node->{$field_name} = $field_value;
    $form_state['node'] = $node;
  }

  /**
   * Helper function.
   *
   * @param mixed $values
   *   Value os field.
   * @param string $type
   *   Field Type.
   * @param string $widget_type
   *   The widget type.
   * @param mixed $field_info
   *   Information about field.
   *
   * @return array
   *   .
   */
  protected function getFieldArray($values = array(), $type, $widget_type, $field_info) {
    switch ($type) {
      case 'list_boolean':
        $context = new FieldArrayContext(new FieldBoolStrategy());
        break;

      case 'email':
        $context = new FieldArrayContext(new FieldEmailStrategy());
        break;

      case 'addressfield':
        $context = new FieldArrayContext(new FieldAdressStrategy());
        break;

      case 'list_text':
        $context = new FieldArrayContext(new FieldOptionsStrategy());
        break;

      case 'datetime':
        $context = new FieldArrayContext(new FieldDateStrategy());
        break;

      case 'entityreference':
        $context = new FieldArrayContext(new FieldEntityRefStrategy());
        break;

      case 'taxonomy_term_reference':
        $context = new FieldArrayContext(new FieldTaxonomyRefStrategy());
        break;

      case 'image':
        $context = new FieldArrayContext(new FieldImageFileStrategy());
        break;

      default:
        // text, date_popup, number_integer, number_decimal, text_long.
        $context = new FieldArrayContext(new FieldTextStrategy());
        break;
    }

    return $context->execute($values, $widget_type, $field_info);
  }

  /**
   * Method for check closest branch.
   *
   * @param string $zip_from
   *   Zip from.
   * @param string $zip_to
   *   Zip to.
   *
   * @return bool|mixed
   *   object.
   */
  public function checkClosestBranch(string $zip_from, string $zip_to) {
    $closest_branch = FALSE;
    $exist_zip = FALSE;
    $distance = array();
    // Get variable settings closest branch.
    $settings_closest_branch = variable_get('settingsclosestbranch');
    // Get parking_zip of all branches.
    $branches = $this->getAllBranchesParkingZip();

    switch ($settings_closest_branch) {
      case SendingRequestBranchSettings::BY_CLOSEST_DISTANCE_FROM_PARKING_ZIP_BASE_ON_DRIVING_MILES:
        if (!empty($zip_to) && !empty($zip_from)) {
          foreach ($branches as $branch) {
            $parking_zip = $branch->parking_zip;
            $distance[$branch->id] = $this->closestDrivingMiles($parking_zip, $zip_from, $zip_to);
          }
        }
        else {
          watchdog('Branch', "<pre>Empty parameter zip, settings :c <br> :a <br> :b </pre>", array(
            ":a" => $zip_to,
            ":b" => $zip_from,
            ":c" => $settings_closest_branch,
          ), WATCHDOG_INFO);
          $exist_zip = TRUE;
        }
        break;

      case SendingRequestBranchSettings::BY_CLOSEST_TO_ORIGIN:
        if (!empty($zip_from)) {
          foreach ($branches as $branch) {
            $parking_zip = $branch->parking_zip;
            $distance[$branch->id] = $this->closestDrivingToOrigin($parking_zip, $zip_from);
          }
        }
        else {
          watchdog('Branch', "<pre>Empty parameter zip, settings :c <br> :b </pre>", array(
            ":b" => $zip_from,
            ":c" => $settings_closest_branch,
          ), WATCHDOG_INFO);
          $exist_zip = TRUE;
        }
        break;

      case SendingRequestBranchSettings::BY_CLOSEST_TO_DESTINATION:
        if (!empty($zip_to)) {
          foreach ($branches as $branch) {
            $parking_zip = $branch->parking_zip;
            $distance[$branch->id] = $this->closestDrivingToDestination($parking_zip, $zip_to);
          }
        }
        else {
          watchdog('Branch', "<pre>Empty parameter zip, settings :c <br> :a</pre>", array(
            ":a" => $zip_to,
            ":c" => $settings_closest_branch,
          ), WATCHDOG_INFO);
          $exist_zip = TRUE;
        }
        break;

      case SendingRequestBranchSettings::BY_CLOSEST_TO_DESTINATION_AND_ORIGIN:
        if (!empty($zip_from) && !empty($zip_to)) {
          foreach ($branches as $branch) {
            $parking_zip = $branch->parking_zip;
            $distance[$branch->id] = $this->closestDrivingToDestinationAndToOrigin($parking_zip, $zip_from, $zip_to);
          }
        }
        else {
          watchdog('Branch', "<pre>Empty parameter zip, settings :c <br> :a <br> :b </pre>", array(
            ":a" => $zip_to,
            ":b" => $zip_from,
            ":c" => $settings_closest_branch,
          ), WATCHDOG_INFO);
          $exist_zip = TRUE;
        }
        break;

      case SendingRequestBranchSettings::BY_SMALLEST_TRAVEL_TIME:
        if (!empty($zip_from) && !empty($zip_to)) {
          foreach ($branches as $branch) {
            $parking_zip = $branch->parking_zip;
            $distance[$branch->id] = $this->closestBySmallestTravelTime($parking_zip, $zip_from, $zip_to);
          }
        }
        else {
          watchdog('Branch', "<pre>Empty parameter zip, settings :c <br> :a <br> :b </pre>", array(
            ":a" => $zip_to,
            ":b" => $zip_from,
            ":c" => $settings_closest_branch,
          ), WATCHDOG_INFO);
          $exist_zip = TRUE;
        }
        break;
    }

    // Search closest value distance or smallest time
    // and return key closest settings.
    if ($exist_zip) {
      $this->id = $branches[0]->id;
    }
    else {
      $this->id = array_search(min($distance), $distance);
    }

    // Get branch data.
    $closest_branch = $this->retrieve();
    // What return?
    return $closest_branch;
  }

  /**
   * Method get parking zip each branches.
   */
  public function getAllBranchesParkingZip() {
    return $this->index();
  }

  /**
   * Check branching is active.
   */
  public function checkBranchingIsActive() {
    $result = FALSE;
    $count_branches = db_select('move_branch_settings', 'mbs')
      ->fields('mbs')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($count_branches > 1) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * Method update parking zip in current branch table for all branches.
   *
   * @param string $zip
   *   Parking zip.
   *
   * @return array
   *   Result operation synchronize branches.
   *
   * @throws \Exception
   */
  public function updateCurrentBranchParkingZip(string $zip) {
    $id = variable_get('current_branch');
    if (empty($id)) {
      $message = "The current branch is not defined";
      watchdog('Branch', 'The current branch is not defined when update parking zip', array(), WATCHDOG_ERROR);
      return services_error($message, 400);
    }
    db_merge("move_branch_settings")
      ->key(array('id' => $id))
      ->fields(array(
        'parking_zip' => $zip,
        'updated' => time(),
      ))
      ->execute();

    return $this->synchronizeBranches();
  }

  /**
   * Method update based state in current branch table fo all branches.
   *
   * @param string $basedState
   *   Based state.
   *
   * @return array
   *   Result operation synchronize branches.
   *
   * @throws \Exception
   */
  public function updateCurrentBranchBasedState(string $basedState) {
    $id = variable_get('current_branch');
    if (empty($id)) {
      $message = "The current branch is not defined";
      watchdog('Branch', 'The current branch is not defined when update based state', array(), WATCHDOG_ERROR);
      return services_error($message, 400);
    }
    db_merge("move_branch_settings")
      ->key(array('id' => $id))
      ->fields(array(
        'based_state' => $basedState,
        'updated' => time(),
      ))
      ->execute();

    return $this->synchronizeBranches();
  }

  /**
   * Synchronize branches.
   *
   * @param mixed $branches_old
   *   Array branches.
   *
   * @return array
   *   ids array.
   *
   * @throws \Exception
   */
  public function synchronizeBranches($branches_old = array()) {
    $response_data = array();
    $branches_new = $this->index();
    $currVariable = variable_get('settingsclosestbranch', NULL);
    // Don`t use empty function here.
    if ($currVariable === NULL) {
      watchdog('Branch', "Variable settings closest branch does not defined", array($currVariable), WATCHDOG_ERROR);
      return services_error('Variable settings closest branch does not defined', 406);
    }
    // When we delete branch write data = branch_new but foreach for branch_old.
    if (!empty($branches_old)) {
      $array_branches = $branches_old;
    }
    else {
      $array_branches = $branches_new;
    }

    foreach ($array_branches as $branch) {
      // CHeck for current branch.
      if (!$this->isCurrentBranch($branch->api_url)) {
        $res[$branch->id] = $this->syncSettingsClosest($branch->api_url, $currVariable);
        watchdog('Branch', "<pre>Result synchronize settings closest:<br>:a<br>:b </pre>", array(
          ":a" => print_r($res[$branch->id], TRUE),
          ":b" => $branch->api_url,
        ), WATCHDOG_INFO);

        $data_for_sync = array('data' => $branches_new);
        $url = $branch->api_url . '/server/move_branch/accept_synchronize';
        $json_data = drupal_json_encode($data_for_sync);
        $options = array(
          'method' => 'POST',
          'data' => $json_data,
          'headers' => array('Content-Type' => 'application/json', 'accept' => 'application/json'),
        );
        $result = drupal_http_request($url, $options);
        // If success, receive collected id's.
        if ($result->code == 200) {
          $response_data[$branch->id] = (object) json_decode($result->data, TRUE);
        }
        else {
          watchdog('Branch', "<pre>Failed synchronize branch with id: <br>:a <br>:b </pre>", array(
            ":a" => $branch->id,
            ":b" => print_r($result, TRUE),
          ), WATCHDOG_ERROR);
          $response_data[$branch->id] = array(
            'error_text'       => $result->error,
            'error_code'       => $result->code,
            'branch_name'      => $branch->name,
            'branch_api_url'   => $branch->api_url,
            'branch_front_url' => $branch->front_url,
          );
        }
      }
    }
    return $response_data;
  }

  /**
   * Accept Synchronize data settings from api.
   *
   * @param array $data
   *   Array of branches settings.
   *
   * @return array
   *   Inserted branches ids.
   *
   * @throws \Exception
   */
  public function acceptSynchronize(array $data) {
    $inserted_ids = array();
    if (!empty($data)) {
      if (!$this->deleteAllSettings()) {
        foreach ($data as $branch) {
          $inserted_ids[] = db_insert('move_branch_settings')
            ->fields((array) $branch)
            ->execute();
        }
      }
    }
    return $inserted_ids;
  }

  /**
   * Method authorization on api.
   *
   * @param string $api_url
   *   Api where authorization.
   *
   * @return null
   *   Null or token.
   */
  public function authorization($api_url) {
    $login = "gabrielblare";
    $pass = "Lyuq3y26XAdn";
    $data_aut = array('name' => $login, 'pass' => $pass);
    $json_data = json_encode($data_aut);
    $options = array(
      'method' => 'POST',
      'data' => $json_data,
      'headers' => array('Content-Type' => 'application/json', 'accept' => 'application/json'),
    );
    $url = $api_url . "/server/user/login";
    $result = drupal_http_request($url, $options);
    if ($result->code == 200) {
      $returned_data = (object) json_decode($result->data);
      return $returned_data->token;
    }
    else {
      watchdog('Branch', "<pre>Failed to authorization:<br>:a </pre>", array(":a" => print_r($result, TRUE)), WATCHDOG_ERROR);
      return NULL;
    }
  }

  public function updateDispatchNote($date, $note) {
    return db_merge('move_dispatch_notes')
      ->insertFields(array(
        'date' => $date,
        'note' => $note,
      ))
      ->updateFields(array(
        'note' => $note,
      ))
      ->key(array('date' => $date))
      ->execute();
  }

  public function updateDispatchNotes($date, $note) {
    $branches = $this->index();
    foreach ($branches as $branch) {
      if ($this->isCurrentBranch($branch->api_url)) {
        continue;
      }
      $token = $this->authorization($branch->api_url);

      $json_data = drupal_json_encode(array(
        'date' => $date,
        'note' => $note,
      ));

      $options = array(
        'method' => 'POST',
        'data' => $json_data,
        'headers' => array(
          'Content-Type' => 'application/json',
          'accept' => 'application/json',
          'X-CSRF-Token' => $token
        ),
      );
      $url = $branch->api_url . self::PARKLOT_NOTE_URL;
      $response = drupal_http_request($url, $options);
      if ($response->code != 200) {
        watchdog('Branch', "<pre>Update Dispatch Notes Failed<br> :a</pre>>", array(':a' => print_r($response, TRUE)), WATCHDOG_ERROR);
      }
    }
  }

  /**
   * Get parking zip and based state.
   *
   * @return array
   *   Parking_zip and based_state.
   */
  public function getParkingState() {
    $result = array();
    $settings = json_decode(variable_get('basicsettings', array()));
    if (!empty($settings)) {
      // parking_address main_state.
      $result['parking_zip'] = $settings->parking_address;
      $result['based_state'] = $settings->main_state;
    }
    return $result;
  }

  /**
   * Method synchronize settings closest branch.
   *
   * @param string $apiUrl
   *   Api url.
   * @param int $value
   *   Current settings.
   *
   * @return object
   *   .
   */
  public function syncSettingsClosest(string $apiUrl, int $value) {
    $result = FALSE;

    $data_for_sync = array('name' => 'settingsclosestbranch', 'value' => $value);
    $json_data = drupal_json_encode($data_for_sync);
    $options = array(
      'method' => 'POST',
      'data' => $json_data,
      'headers' => array(
        'Content-Type' => 'application/json',
        'accept' => 'application/json',
      ),
    );
    $url = $apiUrl . '/server/move_branch/accept_settings';
    $result = drupal_http_request($url, $options);
    if ($result->code == 200) {
      $result = TRUE;
    }
    else {
      watchdog('Branch', "<pre>Failed to synchronize setting closest branch:<br>:a</pre>", array(":a" => print_r($result, TRUE)), WATCHDOG_ERROR);
    }

    return $result;
  }

  /**
   * Accept settings closest branch.
   *
   * @param string $name
   *   Name.
   * @param int $value
   *   Value.
   */
  public function acceptSettings(string $name, int $value) {
    variable_set($name, $value);
  }

  /**
   * Check for current branch.
   *
   * @param string $branch_api_url
   *   Branch api url from db.
   *
   * @return bool
   *   If current branch return TRUE.
   */
  public function isCurrentBranch($branch_api_url) {
    $http_host = $GLOBALS['base_url'];
    return $branch_api_url == $http_host ? TRUE : FALSE;
  }

  /**
   * Clone user to other branch.
   *
   * @param int $uid
   *   User id.
   *
   * @return array|mixed
   *   .
   *
   * @throws \ServicesException
   * @throws \Exception
   */
  public function cloneManagerToBranch(int $uid) {
    $returned_data = array();
    $data = $this->prepareUserForMigrate($uid);
    $branch = $this->retrieve();
    $token = $this->authorization($branch['api_url']);
    $check_exist_user = $this->checkExistUser($data['account']['mail'], $branch['api_url']);
    if (!empty($token) && !$check_exist_user) {
      $options = array(
        'method' => 'POST',
        'data' => json_encode($data),
        'headers' => array(
          'Content-Type' => 'application/json',
          'accept' => 'application/json',
          'X-CSRF-Token' => $token,
        ),
      );
      $url = $branch['api_url'] . '/server/move_branch/create_wrapper_for_client';
      $result = drupal_http_request($url, $options);
      if ($result->code == 200) {
        $returned_data = (object) json_decode($result->data);
        if (!empty($returned_data->uid)) {
          $hash = $this->getUserHash($uid);
          $up_data = array('hash' => $hash, 'uid' => $returned_data->uid);
          $options = array(
            'method' => 'POST',
            'data' => json_encode($up_data),
            'headers' => array(
              'Content-Type' => 'application/json',
              'accept' => 'application/json',
              'X-CSRF-Token' => $token,
            ),
          );
          $url = $branch['api_url'] . "/server/move_branch/update_user_hash";
          $update_hash = drupal_http_request($url, $options);
          if ($update_hash->code == 200) {
            watchdog('Branch', "<pre>Clone manager, update hash password success:<br> :a</pre>", array(":a" => print_r($update_hash, TRUE)), WATCHDOG_INFO);
          }
          else {
            watchdog('Branch', "<pre>Clone manager, update hash password failed:<br> :a</pre>", array(":a" => print_r($update_hash, TRUE)), WATCHDOG_ERROR);
          }
        }
      }
      else {
        watchdog('Branch', "<pre>Failed to clone manager when send request:<br> :a</pre>", array(":a" => print_r($result, TRUE)), WATCHDOG_ERROR);
      }
    }
    else {
      watchdog('Branch', "<pre>Failed to clone manager check token and exist user:<br> :a<br> :b</pre>", array(":a" => $token, ":b" => $check_exist_user), WATCHDOG_ERROR);
      return services_error("A user with this email already exists on another branch", 400);
    }
    return $returned_data;
  }

  /**
   * Wrapper for create user.
   *
   * @param array $data
   *   User data.
   *
   * @return mixed
   *   User.
   *
   * @throws \Exception
   */
  public function createClientWrapper(array $data) {
    $clients = new Clients();
    return $clients->create($data);
  }

  /**
   * Get user data.
   *
   * @param int $uid
   *   User id.
   *
   * @return string
   *   .
   *
   * @throws \Exception
   */
  public function prepareUserForMigrate(int $uid) {
    $client = new Clients($uid);
    $user = $client->retrieve();
    $result['settings'] = $user['settings'];
    $result['roles'] = $user['roles'];
    $result['fields'] = array(
      'field_user_first_name' => $user['field_user_first_name'],
      'field_user_last_name' => $user['field_user_last_name'],
      'field_primary_phone' => $user['field_primary_phone'],
      'field_user_additional_phone' => $user['field_user_additional_phone'],
    );
    $result['name'] = $user['name'];
    $result['mail'] = $user['mail'];
    // Default pass only clone user.
    $result['pass'] = '123';
    $account['account'] = $result;
    return $account;
  }

  /**
   * Get user pass hash.
   *
   * @param int $uid
   *   User id.
   *
   * @return mixed
   *   .
   */
  public function getUserHash(int $uid) {
    $result = db_select('users', 'u')
      ->fields('u', array('pass'))
      ->condition('uid', $uid)
      ->execute()
      ->fetchAll();

    return $result[0]->pass;
  }

  /**
   * Update user pass hash.
   *
   * @param int $uid
   *   User id.
   * @param string $hash
   *   Hash pass.
   */
  public function setUserHash(int $uid, string $hash) {
    db_update('users')
      ->fields(array('pass' => $hash))
      ->condition('uid', $uid)
      ->execute();
  }

  /**
   * Check if user exist on branch.
   *
   * @param string $email
   *   Email user.
   * @param string $api_url
   *   Branch url.
   *
   * @return bool|mixed
   *   true = exist.
   */
  public function checkExistUser(string $email, string $api_url) {
    $returned_data = FALSE;
    $data = array('email' => $email);
    $options = array(
      'method' => 'POST',
      'data' => json_encode($data),
      'headers' => array(
        'Content-Type' => 'application/json',
        'accept' => 'application/json',
      ),
    );
    $url = $api_url . "/server/clients/checkunique";
    $result = drupal_http_request($url, $options);
    if ($result->code == 200) {
      $returned_data = (array) json_decode($result->data);
    }
    else {
      watchdog('Branch', "<pre>Failed to check if user exist on branch: <br> :a</pre>", array(":a" => print_r($result, TRUE)), WATCHDOG_ERROR);
    }
    return $returned_data[0];
  }

  /**
   * Save logo for branch.
   *
   * @param string $value
   *   Base64.
   *
   * @return mixed|string
   *   path.
   *
   * @throws \ServicesException
   * @throws \Exception
   */
  public function saveLogo(string $value) {
    $path = "";
    $id = variable_get('current_branch');
    if (empty($id)) {
      $message = "The current branch is not defined when we save logo";
      watchdog('Branch', $message, array(), WATCHDOG_ERROR);
      return services_error($message, 400);
    }
    $file_saved = Users::savePicture($value, 'branch_image');
    if (!empty($file_saved->fid)) {
      $path = "/sites/default/files/branch_image/" . $file_saved->filename;
      $data = array('logo' => $path);
      $this->id = $id;
      $this->update($data);
    }
    else {
      watchdog('Branch', "<pre>Failed to save picture: <br> :a</pre>", array(":a" => print_r($file_saved, TRUE)), WATCHDOG_ERROR);
      services_error("Failed to save picture", 406);
    }
    return $path;
  }

  /**
   * Helper method to delete all rows from settings.
   */
  private function deleteAllSettings() {
    return db_truncate('move_branch_settings')->execute();
  }

  /**
   * Method for determine closest branch by distance form parking zip base.
   *
   * On driving miles.
   *
   * @param string $parking_zip
   *   Parking_zip.
   * @param string $zip_from
   *   Zip from.
   * @param string $zip_to
   *   Zip to.
   *
   * @return mixed
   *   distance.
   */
  public function closestDrivingMiles(string $parking_zip, string $zip_from, string $zip_to) {
    $parkingzip_to_zipfrom = DistanceCalculate::googleRequestDistance($parking_zip, $zip_from);
    $zipfrom_to_zipto = DistanceCalculate::googleRequestDistance($zip_from, $zip_to);
    $zipto_to_parkingzip = DistanceCalculate::googleRequestDistance($zip_to, $parking_zip);

    return $parkingzip_to_zipfrom['distance'] + $zipfrom_to_zipto['distance'] + $zipto_to_parkingzip['distance'];
  }

  /**
   * Method for determine closest branch by distance to origin.
   *
   * @param string $parking_zip
   *   Parking zip.
   * @param string $zip_from
   *   Zip from.
   *
   * @return mixed
   *   Distance.
   */
  public function closestDrivingToOrigin(string $parking_zip, string $zip_from) {
    $parkingzip_to_zipfrom = DistanceCalculate::googleRequestDistance($parking_zip, $zip_from);

    return $parkingzip_to_zipfrom['distance'];
  }

  /**
   * Method for determine closest branch by distance to destination.
   *
   * @param string $parking_zip
   *   Parking zip.
   * @param string $zip_to
   *   Zip to.
   *
   * @return mixed
   *   Distance.
   */
  public function closestDrivingToDestination(string $parking_zip, string $zip_to) {
    $parkingzip_to_zipto = DistanceCalculate::googleRequestDistance($parking_zip, $zip_to);

    return $parkingzip_to_zipto['distance'];
  }

  /**
   * Method for determine closest branch by distance to destination + to origin.
   *
   * @param string $parking_zip
   *   Parking zip.
   * @param string $zip_from
   *   Zip from.
   * @param string $zip_to
   *   Zip to.
   *
   * @return mixed
   *   Distance.
   */
  public function closestDrivingToDestinationAndToOrigin(string $parking_zip, string $zip_from, string $zip_to) {
    $parkingzip_to_zipfrom = DistanceCalculate::googleRequestDistance($parking_zip, $zip_from);
    $zipto_to_parkingzip = DistanceCalculate::googleRequestDistance($zip_to, $parking_zip);

    return $parkingzip_to_zipfrom['distance'] + $zipto_to_parkingzip['distance'];
  }

  /**
   * Method for determine closest branch by smallest travel time.
   *
   * @param string $parking_zip
   *   Parking zip.
   * @param string $zip_from
   *   Zip from.
   * @param string $zip_to
   *   Zip to.
   *
   * @return mixed
   *   Duration.
   */
  public function closestBySmallestTravelTime(string $parking_zip, string $zip_from, string $zip_to) {
    $parkingzip_to_zipfrom = DistanceCalculate::googleRequestDistance($parking_zip, $zip_from);
    $zipfrom_to_zipto = DistanceCalculate::googleRequestDistance($zip_from, $zip_to);
    $zipto_to_parkingzip = DistanceCalculate::googleRequestDistance($zip_to, $parking_zip);

    return $parkingzip_to_zipfrom['duration'] + $zipfrom_to_zipto['duration'] + $zipto_to_parkingzip['duration'];
  }

}

/**
 * Interface FieldArrayStrategyInterface.
 */
interface FieldArrayStrategyInterface {

  /**
   * Method for create structured array.
   *
   * @param mixed $values
   *   Value os field.
   * @param bool $multi
   *   Indicated that field is multi.
   * @param string $widget_type
   *   The widget type.
   * @param int $count_values
   *   Count values.
   * @param mixed $field_info
   *   Information about field.
   *
   * @return mixed
   *   .
   */
  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array());

}

/**
 * Class fieldTextStrategy.
 *
 * @package Drupal\move_branch\Services
 */
class FieldTextStrategy implements FieldArrayStrategyInterface {

  /**
   * Create Data.
   *
   * @param mixed $values
   *   Value.
   * @param bool $multi
   *   Multi.
   * @param string $widget_type
   *   Type.
   * @param int $count_values
   *   Count.
   * @param mixed $field_info
   *   Info.
   * @return mixed
   *   .
   */
  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array()) {
    $data = array();
    if ($multi) {
      for ($i = 0; $i < $count_values; $i++) {
        $data[$field_info['field_name']][$i] = $values[$i]['value'];
      }
    }
    else {
      $data[$field_info['field_name']] = $values[0]['value'];
    }

    return $data;
  }

}

/**
 * Class FieldBoolStrategy.
 *
 * @package Drupal\move_branch\Services
 */
class FieldBoolStrategy implements FieldArrayStrategyInterface {

  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array()) {
    $data = array();
    if ($multi) {
      for ($i = 0; $i < $count_values; $i++) {
        $data[$field_info['field_name']][$i] = $values[$i]['value'];
      }
    }
    else {
      $data[$field_info['field_name']] = $values[0]['value'];
    }
  }

}

/**
 * Class FieldEmailStrategy.
 *
 * @package Drupal\move_branch\Services
 */
class FieldEmailStrategy implements FieldArrayStrategyInterface {

  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array()) {
    $data = array();
    if ($multi) {
      for ($i = 0; $i < $count_values; $i++) {
        $data[$field_info['field_name']][$i] = $values[$i]['email'];
      }
    }
    else {
      $data[$field_info['field_name']] = $values[0]['email'];
    }

    return $data;
  }

}

/**
 * Class FieldAdressStrategy.
 *
 * @package Drupal\move_branch\Services
 */
class FieldAdressStrategy implements FieldArrayStrategyInterface {

  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array()) {
    $data = array();
    if ($multi) {
      for ($i = 0; $i < $count_values; $i++) {
        $data[$field_info['field_name'][$i]] = $values[$i];
      }
    }
    else {
      $data[$field_info['field_name']] = $values[0];
    }

    return $data;
  }

}

/**
 * Class FieldOptionsStrategy.
 *
 * @package Drupal\move_branch\Services
 */
class FieldOptionsStrategy implements FieldArrayStrategyInterface {

  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array()) {
    $data = array();
    if ($multi) {
      for ($i = 0; $i < $count_values; $i++) {
        $data[$field_info['field_name']][$i] = $values[$i]['value'];
      }
    }
    else {
      $data[$field_info['field_name']] = $values[0]['value'];
    }

    return $data;
  }

}

/**
 * Class FieldDateStrategy.
 *
 * @package Drupal\move_branch\Services
 */
class FieldDateStrategy implements FieldArrayStrategyInterface {

  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array()) {
    $data = array();
    if ($multi) {
      for ($i = 0; $i < $count_values; $i++) {
        $exploded_date = explode(' ', $values[$i]['value']);
        $data[$field_info['field_name']][$i] = array('date' => $exploded_date[0], 'time' => $exploded_date[1]);
      }
    }
    else {
      $exploded_date = explode(' ', $values[0]['value']);
      $data[$field_info['field_name']] = array('date' => $exploded_date[0], 'time' => $exploded_date[1]);
    }

    return $data;
  }

}

/**
 * Class FieldEntityRefStrategy.
 *
 * @package Drupal\move_branch\Services
 */
class FieldEntityRefStrategy implements FieldArrayStrategyInterface {

  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array()) {
    $data = array();
    if ($widget_type == 'options_select' || $widget_type == 'options_buttons') {
      for ($i = 0; $i < $count_values; $i++) {
        $data[LANGUAGE_NONE][] = $multi ? $values[$i] : $values;
      }
    }
    if ($widget_type == 'entityreference_autocomplete') {
      for ($i = 0; $i < $count_values; $i++) {
        $value = $multi ? $values[$i] : $values;
        switch ($field_info['settings']['target_type']) {
          case 'node':
            $data[LANGUAGE_NONE][$i]['target_id'] = "Move request ($value)";
            break;

          case 'user':
            $user = user_load((int) $value);
            if ($user) {
              $mail = $user->mail;
              $data[LANGUAGE_NONE][$i]['target_id'] = "$mail ($value)";
            }
            break;
        }
      }
    }

    return $data;
  }

}

/**
 * Class FieldTaxonomyRefStrategy.
 *
 * @package Drupal\move_branch\Services
 */
class FieldTaxonomyRefStrategy implements FieldArrayStrategyInterface {

  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array()) {
    $data = array();
    if ($multi && $count_values >= 2) {
      for ($i = 0; $i < $count_values; $i++) {
        $data[LANGUAGE_NONE][] = $values[$i];
      }
    }
    else {
      $data[LANGUAGE_NONE] = $values;
    }

    return $data;
  }

}

/**
 * Class FieldImageFileStrategy.
 *
 * @package Drupal\move_branch\Services
 */
class FieldImageFileStrategy implements FieldArrayStrategyInterface {
  private $file_types = array(
    'image/jpeg' => 'jpg',
    'image/png' => 'png',
    'image/gif' => 'gif',
  );

  public function createData($values, $multi = FALSE, $widget_type, $count_values, $field_info = array()) {
    $data = array();
    $values = (array_key_exists(0, $values)) ? $values : array($values);
    for ($i = 0; $i < $count_values; $i++) {
      if (isset($values[$i]['uri'])) {
        $file_saved = $this->saveFile($values[$i], $field_info['field_name']);
        if ($file_saved) {
          MoveRequest::$files[$i] = (array) $file_saved;
          $data[LANGUAGE_NONE][] = (array) $file_saved;
        }
      }
    }
    return $data;
  }

  protected function saveFile($value, $field_name) {
    $field_instance = field_info_instance('node', $field_name, 'move_request');
    $file_exp = explode(',', $value['uri']);
    $file_data = array(
      'title' => (isset($value['title'])) ? $value['title'] : '',
      'data' => $file_exp[1],
    );

    $exp_code = explode(';', $file_exp[0]);
    $exp_mimetype = explode(':', $exp_code[0]);
    $destination = file_default_scheme() . '://' . $field_instance['settings']['file_directory'] . '/' . user_password(8) . '.' . $this->file_types[$exp_mimetype[1]];

    $dir = drupal_dirname($destination);
    // Build the destination folder tree if it doesn't already exists.
    if (!file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
      return services_error(t("Could not create destination directory for file."), 500);
    }

    // Write the file.
    $file_saved = file_save_data(base64_decode($file_data['data']), $destination);
    if ($file_saved) {
      file_usage_add($file_saved, 'move_services_new', 'files', $file_saved->fid);
      $file_saved->filemime = $exp_mimetype[1];
      file_save($file_saved);
      if ($file_data['title']) {
        $file_saved->title = $file_data['title'];
      }
    }

    return $file_saved;
  }

}

/**
 * Class FieldArrayContext.
 *
 * @package Drupal\move_branch\Services
 */
class FieldArrayContext {
  private $strategy_name;
  private $data = array();
  private $count_values = 1;

  /**
   * FieldArrayContext constructor.
   *
   * @param \Drupal\move_branch\Services\FieldArrayStrategyInterface $strategy
   *   Interface.
   */
  public function __construct(FieldArrayStrategyInterface $strategy) {
    $this->strategy_name = $strategy;
  }

  public function execute($values, $widget_type, $field_info = array()) {
    $multi = (isset($field_info['cardinality']) && ($field_info['cardinality'] == -1 || $field_info['cardinality'] >= 2)) ? TRUE : FALSE;
    if (is_array($values)) {
      $this->count_values = count($values);
    }
    elseif (($multi && !is_array($values)) || (!$multi && $field_info['cardinality'] >= 2 && !is_array($values))) {
      $values = array($values);
    }

    $this->data = $this->strategy_name->createData($values, $multi, $widget_type, $this->count_values, $field_info);
    return $this->data;
  }

}
