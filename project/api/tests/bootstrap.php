<?php

define('DRUPAL_ROOT', '/app/project/api/');
require_once DRUPAL_ROOT . 'includes/bootstrap.inc';
require_once DRUPAL_ROOT . 'sites/all/modules/services/includes/services.runtime.inc';
$_SERVER['HTTP_HOST'] = 'localhost';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['X-Real-IP'] = '127.0.0.1';
$_SERVER['X-Forwarded-For'] = '127.0.0.1';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

require_once DRUPAL_ROOT . 'tests/psr0.module';
psr0_autoloader('Drupal\move_services_new\Services\Clients');

require_once DRUPAL_ROOT . 'includes/entity.inc';
drupal_load('module', 'user');
