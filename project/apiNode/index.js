'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const server = require('http');
const securityServer = require('https');
const fs = require('fs');
const PdfService = require('./service/pdfService');

const app = express();
const path = require('path');
const port = process.env.PORT || 9999;
const security = process.env.SECURITY || false;
let mainServer;

if(security) {
	let keyName, certName;

	let files = fs.readdirSync(path.join(__dirname, '/'));

	files.forEach((file) => {
		if(file.match(/.key$/)) {
			keyName = file;
		} else if(file.match(/.crt$/)) {
			certName = file;
		}
	});

	if(keyName && certName) {
		let key = fs.readFileSync(path.join(__dirname, `/${keyName}`));
		let cert = fs.readFileSync(path.join(__dirname, `/${certName}`));

		let options = {
			key: key,
			cert: cert
		};

		mainServer = securityServer.createServer(options, app);
	} else {
		console.log('Missed key or cert file !\n' +
					'Server run on http !');

		httpCreateServer();
	}
} else {
	httpCreateServer();
}

const listener = mainServer.listen(port, () => {
    console.log('Server listening at port ' + listener.address().port);
});

let pdfService = new PdfService();

app.use(bodyParser.json({limit: '50mb'}))
    .use(bodyParser.urlencoded({limit: '50mb', extended: true}))
    .use(function(req, res, next) {
        res.header("access-control-max-age", 86400);
        res.header("Content-Type", "application/json");
        res.header('Access-Control-Allow-Credentials', true);
        res.header('Access-Control-Allow-Headers', 'Authorization,Content-Type,Accept,Origin,User-Agent,DNT,Cache-Control,X-Mx-ReqToken,Keep-Alive,X-Requested-With,If-Modified-Since,X-CSRF-Token');
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        res.header('Cache-Control', 'no-cache, must-revalidate');
        res.header('Connection', 'keep-alive');
        next();
    })
    .use(express.static(__dirname + '/pdfFiles'))
    .post("/api/pdf", (req, res) => {
        pdfService.createPdf(req.body.html, req.body.entity_id, req.body.entity_type, req.body.page_id,
                             req.body.allow_attach, req.body.serverUrl, req.headers.origin, req.headers, (err, success) => {
            if (err) {
                res.send(err);
            } else {
                res.send(success);
            }
        });
    });

function httpCreateServer() {
	mainServer = server.createServer(app);
}
