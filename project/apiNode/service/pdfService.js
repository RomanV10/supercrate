'use strict';

const commandExists = require('command-exists'),
      wkhtmltopdf 	= require('./../wkhtmltopdf'),
      MemoryStream 	= require('memorystream'),
      fs = require('fs'),
      Q = require('q'),
      request = require('request'),
      stylesUrl = '../content/pdf.css';

let htmlToPdfStart = `<!DOCTYPE html>
                      <html lang="en">
                      <head>
                            <meta http-equiv="content-type" content="text/html; charset=UTF-8">
                            <meta charset="utf-8">
                            <meta http-equiv="Cache-control" content="no-cache, no-store, must-revalidate">
                            <meta http-equiv="Pragma" content="no-cache">
                            <meta http-equiv="Expires" content="0">
                            <title>Account Page</title>
                            <meta name="viewport" >
                            <link type="text/css" rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&lang=en">
                            <link type="text/css" rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700">`,
    htmlTCloseHead = `</head>`,
    htmlOpenBody   = `<body class="md-skin contract contract-page">`,
    htmlToPdfEnd   = `</body></html>`;

process.env['PATH'] = process.env['PATH'] + ':' + __dirname + '/../wkhtmltopdf';

module.exports = class PdfService {
    createPdf(html, entity_id, entity_type, page_id, allow_attach, serverUrl, origin, headers, cb) {
            this.getStyles()
                .then((styles) => {
                    let htmlToPdf = htmlToPdfStart + styles + htmlTCloseHead + htmlOpenBody + html + htmlToPdfEnd;

                    return this.wkhtmltopdfPromise(htmlToPdf, {marginTop: "10mm", marginBottom: "10mm"});
                })
                .then((base64PDF) => {
                    return this.sendPDFToDrupal(serverUrl, headers, {
                                                                    "html": base64PDF, 
                                                                    "entity_id": entity_id, 
                                                                    "entity_type": entity_type,  
                                                                    "page_id": page_id,
                                                                    "allow_attach": allow_attach
                                                                });
                })
                .then((data) => cb(null, data))
                .catch((err) => cb(err, null));
    }

    getStyles() {
        let deferred = Q.defer();
        fs.readFile(`${__dirname}/${stylesUrl}`, '', (err, data) => {
            deferred.resolve(`<style>${data}</style>`);
        });
        
        return deferred.promise;
    }

    sendPDFToDrupal(serverUrl, headers, data) {
        let deferred = Q.defer();
        let options = {
            url: serverUrl + 'server/move_mpdf',
            headers: {
                'Content-Type' : 'application/json',
                'X-CSRF-Token' : headers['x-csrf-token'],
            },
            body: JSON.stringify(data),
            method: "POST"
        };

        request(options, (err, httpResponse, body) => {
            if(httpResponse || body) {
                deferred.resolve(body);
            } else {
                deferred.reject(err);
            }
        });

        return deferred.promise;
    }

    wkhtmltopdfPromise(html, options) {
        return new Promise(function (resolve, reject) {
            let memStream = new MemoryStream();
    
            wkhtmltopdf(html, options, function (code, signal) {
                resolve(memStream.read().toString('base64'));
            }).pipe(memStream);
        });
    }
}
