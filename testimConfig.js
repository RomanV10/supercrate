exports.config = {

    project: "roma4ke@gmail,com_15f0e17d-9a78-4cc9-871c-d3e2811101e9",
    token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2IjowLCJkIjp7InVpZCI6ImNpOnJvbWE0a2VAZ21haWwsY29tXzE1ZjBlMTdkLTlhNzgtNGNjOS04NzFjLWQzZTI4MTExMDFlOSIsImVtYWlsIjoicm9tYTRrZUBnbWFpbC5jb20ifSwiaWF0IjoxNDc5NzY5NjUxfQ.ik9xjgVkJgsUFhGdBQJ6TI54l7W8evsqabCkb1SDBAk",
    //
    // Specify which label you would like to run. All tests with the specified label will be executed
    label: "sanity",
    //
    // Your Selenium server host and port
    host: "http://192.168.8.181:60753/wd/hub",
    port: 4444,
    //
    // Override the base URL defined in the test in order to run it again a different envinronment
    baseUrl: 'http://staging.example.com',

    // =====
    // Hooks
    // =====
    // Testim provides several hooks you can use to interfere the test process in order to enhance
    // it and build services around it.
    //
    // Hook that gets executed before the suite starts
    beforeSuite: function (suite) {
        console.log("beforeSuite", suite);
    },
    //
    // Function to be executed before a test starts.
    beforeTest: function (test) {
        console.log("beforeTest", test);
    },
    //
    // Function to be executed after a test starts.
    afterTest: function (test) {
        console.log("afterTest", test);
    },
    //
    // Hook that gets executed after the suite has ended
    afterSuite: function (suite) {
        console.log("afterSuite", suite);
    }
};