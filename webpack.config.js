module.exports = env => {
	console.log(env);
	const CLI_PARAMS = process.argv;
	global.NODE_ENV = env || 'production';
	global.isProduction = global.NODE_ENV === 'production';
	let targets = [];
	global.rootPath = __dirname;

	if (~CLI_PARAMS.indexOf('--noisy')) {
		global.noisyMode = true;
	}

	if (~CLI_PARAMS.indexOf('--moveboard')) {
		targets.push(require('./webpack_configs/moveboard/webpack.config.moveboard'));
	}

	if (~CLI_PARAMS.indexOf('--account')) {
		targets.push(require('./webpack_configs/account/webpack.config.account'));
	}

	if (~CLI_PARAMS.indexOf('--shared')) {
		targets.push(require('./webpack_configs/shared/webpack.config.shared'));
	}

	if (~CLI_PARAMS.indexOf('--frontpage')) {
		targets.push(require('./webpack_configs/frontpage/webpack.config.frontpage'));
	}

	if (~CLI_PARAMS.indexOf('--frontpage-mannual')) {
		targets.push(...require('./webpack_configs/frontpage/webpack.config.frontpage_dimka'));
	}
	
	return targets;
};
