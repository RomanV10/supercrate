let path = require('path');
global.sharedPath = path.join(global.rootPath, 'project', 'front', 'shared');

let defaultLoaders = require('../defaults/default_loaders');
let productionLoaders = require('../defaults/production_loaders');
let loaders = global.isProduction
	? defaultLoaders.concat(productionLoaders)
	: defaultLoaders;

let defaultPlugins = require('../defaults/default_plugins');
let sharedPlugins = require('./shared_plugins');
let plugins = defaultPlugins.concat(sharedPlugins);

let defaultConfig = require('../defaults/webpack.config.defaults');
let sharedConfig = Object.assign({
	entry: {
		'app.test': path.join(global.sharedPath, 'test.js'),
	},
	output: {
		filename: '[name].js',
		path: path.join(global.sharedPath, 'dist'),
	},
	module: {
		rules: loaders
	},
	plugins: plugins
}, defaultConfig);

module.exports = sharedConfig;