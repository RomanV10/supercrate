let path = require('path');
global.accountPath = path.join(global.rootPath, 'project', 'front', 'account');

let defaultLoaders = require('../defaults/default_loaders');
let productionLoaders = require('../defaults/production_loaders');
let developmentLoaders = require('../defaults/development_loaders');
let loaders = global.isProduction
	? defaultLoaders.concat(productionLoaders)
	: defaultLoaders.concat(developmentLoaders);

let defaultPlugins = require('../defaults/default_plugins');
let accountPlugins = require('./account_plugins');
let plugins = defaultPlugins.concat(accountPlugins);

let defaultConfig = require('../defaults/webpack.config.defaults');
let accountConfig = Object.assign({
	entry: {
		'app.account.source': path.join(global.accountPath, 'include.js'),
		'app.account.import': path.join(global.accountPath, 'vendor.js'),
		'app.account.test': path.join(global.accountPath, 'test.js'),
	},
	output: {
		filename: '[name].js',
		path: path.join(global.accountPath, 'dist'),
	},

	module: {
		rules: loaders
	},
	plugins: plugins
}, defaultConfig);


module.exports = accountConfig;
