const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');

module.exports = [
	new MiniCssExtractPlugin({
		filename: '[name].css',
	}),
	new HtmlWebpackPlugin({
		template: './project/front/account/indexTemplate.html',
		filename: '../index.html',
		inject: false
	}),
	new webpack.DefinePlugin({
		'IS_ACCOUNT': true
	})
];