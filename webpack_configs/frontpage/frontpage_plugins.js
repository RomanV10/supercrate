const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [
	new MiniCssExtractPlugin({
		filename: 'elromco.css',
	}),
];