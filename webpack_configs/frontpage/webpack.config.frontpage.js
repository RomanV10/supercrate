let path = require('path');
global.frontpagePath = path.join(global.rootPath, 'project', 'front', 'app');

let defaultLoaders = require('../defaults/default_loaders');
let productionLoaders = require('../defaults/production_loaders');
let developmentLoaders = require('../defaults/development_loaders');
let frontpageLoaders = require('./frontpage_loaders');
let loaders = global.isProduction
	? frontpageLoaders.concat(productionLoaders)
	: frontpageLoaders.concat(developmentLoaders);

let defaultPlugins = require('../defaults/default_plugins');
let frontpagePlugins = require('./frontpage_plugins');
let plugins = defaultPlugins.concat(frontpagePlugins);

let defaultConfig = require('../defaults/webpack.config.defaults');
let frontpageConfig = Object.assign({
	entry: {
		'app.import': path.join(global.frontpagePath, 'include.js'),
	},
	output: {
		filename: '[name].js',
		path: path.join(global.frontpagePath, 'build'),
	},
	module: {
		rules: loaders
	},
	plugins: plugins
}, defaultConfig);

module.exports = frontpageConfig;
