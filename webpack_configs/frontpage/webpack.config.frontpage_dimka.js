let path = require('path');
global.companiesFrontpagePath = path.join(global.rootPath, 'project', 'front', 'app', 'companies');

let defaultLoaders = require('../defaults/default_loaders');
let productionLoaders = require('../defaults/production_loaders');
let developmentLoaders = require('../defaults/development_loaders');
let frontpageLoaders = require('./frontpage_loaders');
let companiesSettings = require('./manualStage.settings.json');
let loaders = global.isProduction
	? frontpageLoaders.concat(productionLoaders)
	: frontpageLoaders.concat(developmentLoaders);

let defaultPlugins = require('../defaults/default_plugins');
let frontpagePlugins = require('./frontpage_plugins');
let plugins = defaultPlugins.concat(frontpagePlugins);

let defaultConfig = require('../defaults/webpack.config.defaults');

let fronpageConfigs = [];

companiesSettings.companyNames
	.forEach((name) => {
		let config = Object.assign(
			{
				entry: {
					'app.import': path.join(global.companiesFrontpagePath, name, 'include.js'),
				},
				output: {
					filename: '[name].js',
					path: path.join(global.companiesFrontpagePath, name, 'build'),
				},
				module: {
					rules: loaders
				},
				plugins: plugins
			},
			defaultConfig);

		fronpageConfigs.push(config);
	});

module.exports = fronpageConfigs;
