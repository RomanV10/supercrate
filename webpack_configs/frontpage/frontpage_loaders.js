const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [
	{
		test: /\.html$/,
		loader: 'ngtemplate-loader!html-loader'
	},
	{
		test: /\.css$/,
		use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader']
	},
];