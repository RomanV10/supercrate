const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const OfflinePlugin = require('offline-plugin');
const moment = require('moment');

const OFFLINE_CONFIG = new OfflinePlugin({
	version: `moveBoard-Service-Worker-v${moment().unix().toString()}`,
	excludes: ['**/*.map'],
	publicPath: '/moveBoard/',
	caches: {
		main: [
			'/moveBoard/',
			'/moveBoard/index.html',
			'/moveBoard/dist/app.source.js',
			'/moveBoard/dist/app.import.js',
			'*.png',
			'*.jpg',
			'*.jpeg',
			'*.woff',
			'*.woff2',
		]
	},
	ServiceWorker: {
		output: '../sw.js',
		events: true,
		publicPath: '/moveBoard/sw.js',
		prefetchRequest: {
			credentials: 'same-origin',
		},
	},
	AppCache: false,
});

const MOVEBOARD_PLUGINS = [
	new MiniCssExtractPlugin({
		filename: '[name].css',
	}),
	new HtmlWebpackPlugin({
		template: './project/front/moveBoard/indexTemplate.html',
		filename: '../index.html',
		inject: false
	}),
	new webpack.DefinePlugin({
		'IS_ACCOUNT': false
	}),
];

if (global.isProduction) MOVEBOARD_PLUGINS.push(OFFLINE_CONFIG);

module.exports = MOVEBOARD_PLUGINS;
