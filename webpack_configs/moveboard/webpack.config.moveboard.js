let path = require('path');
global.moveboardPath = path.join(global.rootPath, 'project', 'front', 'moveBoard');

let defaultLoaders = require('../defaults/default_loaders');
let productionLoaders = require('../defaults/production_loaders');
let developmentLoaders = require('../defaults/development_loaders');
let loaders = global.isProduction
	? defaultLoaders.concat(productionLoaders)
	: defaultLoaders.concat(developmentLoaders);

let defaultPlugins = require('../defaults/default_plugins');
let moveboardPlugins = require('./moveboard_plugins');
let plugins = defaultPlugins.concat(moveboardPlugins);

let defaultConfig = require('../defaults/webpack.config.defaults');
let moveBoardConfig = Object.assign({
	entry: {
		'app.import': path.join(global.moveboardPath, 'import.js'),
		'app.source': path.join(global.moveboardPath, 'source.js'),
		'app.test': path.join(global.moveboardPath, 'test.js'),
	},
	output: {
		filename: '[name].js',
		path: path.join(global.moveboardPath, 'dist'),
		publicPath: './moveBoard/content/',
	},
	module: {
		rules: loaders
	},
	plugins: plugins
}, defaultConfig);

module.exports = moveBoardConfig;
