module.exports = [
	{
		test: /\.js$/,
		exclude: /(node_modules|bower_components)/,
		use: [
			{
				loader: 'ng-annotate-loader',
				options: {
					add: true,
					sourcemap: false,
				}
			},
			{
				loader: 'babel-loader',
			}
		]
	},
];
