let path = require('path');

module.exports = {
	context: global.rootPath,
	watchOptions: {
		aggregateTimeout: 100
	},
	devServer: {
		contentBase: path.join(global.rootPath, 'project', 'front'),
		compress: true,
		port: 9000
	},
	devtool: global.isProduction
		? false
		: 'cheap-module-eval-source-map',
};
