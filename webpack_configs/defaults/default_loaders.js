let moment = require('moment');
module.exports = [
	{
		test: /\.html$/,
		'use': [
			{
				loader: 'html-loader',
				options: {
					minimize: false
				}
			},
			{
				loader: 'string-replace-loader',
				query: {
					multiple: [
						{
							search: 'timestamvariable',
							replace: moment().format('YYYY-MM-DD-HH_mm'),
							flags: 'g'
						},
						{
							search: 'isProductionVersion',
							replace: global.isProduction
						}
					]
				}
			}
		]
	},
	{
		test: /\.jpg$/,
		'use': ['file-loader']
	},
	{
		test: /\.png$/,
		'use': ['url-loader?mimetype=image/png']
	},
	{
		test: /\.gif$/,
		'use': ['url-loader?mimetype=image/gif']
	},
	{
		test: /\.(pug|jade)$/,
		loaders: ['html-loader', 'pug-html-loader']
	},
	{
		test: /\.scss$/,
		use: [
			{
				loader: 'style-loader'
			}, {
				loader: 'css-loader'
			}, {
				loader: 'sass-loader'
			}
		]
	},
	{
		test: /\.styl$/,
		use: [
			{loader: 'style-loader'},
			{
				loader: 'css-loader',
				options: {minimize: true}
			},
			{
				loader: 'postcss-loader',
				options: {
					sourceMap: !global.isProduction,
					plugins: () => [
						require('autoprefixer'),
					]
				}
			},
			{loader: 'stylus-loader'},
		],
	},
	{
		test: /\.(eot|ttf|woff|woff2)$/,
		use: [
			{loader: 'raw-loader'},
			{
				loader: 'url-loader',
				options: {
					limit: 8192
				}
			}
		]
	},
	{
		test: /\.svg$/,
		use: [
			{
				loader: 'url-loader',
				options: {
					limit: 8192
				}
			}
		]
	}
];
