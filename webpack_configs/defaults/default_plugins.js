let path = require('path');
const webpack = require('webpack');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');

let defaultPlugins = [
	new ProgressBarPlugin(),
	new webpack.DefinePlugin({
		NODE_ENV: JSON.stringify(global.NODE_ENV)
	}),
	new WebpackNotifierPlugin(
		{
			title: 'Webpack',
			alwaysNotify: global.noisyMode,
			contentImage: path.join(__dirname, 'webpack.png')
		}),
];
module.exports = defaultPlugins;
